/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCmsVendorRatingMaster;
import com.cptu.egp.eps.service.serviceinterface.CmsVendorRatingMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsVendorRatingMasterServiceBean {

    final Logger logger = Logger.getLogger(CmsVendorRatingMasterServiceBean.class);
    private final static CmsVendorRatingMasterService cmsVendorRatingMasterService =
            (CmsVendorRatingMasterService) AppContext.getSpringBean("CmsVendorRatingMasterService");
    private String logUserId = "0";
    private static final String LOGGERStart = " Starts";
    private static final String LOGGEREnd = " Ends";

    public void setLogUserId(String logUserId) {
        cmsVendorRatingMasterService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /***
     * This method inserts a TblCmsVendorRatingMaster object in database
     * @param tblCmsVendorRatingMaster
     * @return int
     */
    public int insertCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster) {
        logger.debug("insertCmsVendorRatingMaster : " + logUserId + LOGGERStart);
        int id = 0;
        try {
            id = cmsVendorRatingMasterService.insertCmsVendorRatingMaster(tblCmsVendorRatingMaster);
        } catch (Exception e) {
            logger.error("insertCmsVendorRatingMaster : " + logUserId + " : " + e);
        }
        logger.debug("insertCmsVendorRatingMaster : " + logUserId + LOGGEREnd);
        return id;
    }

    /****
     * This method updates an TblCmsVendorRatingMaster object in DB
     * @param tblCmsVendorRatingMaster
     * @return boolean
     */
    public boolean updateCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster) {
        logger.debug("updateCmsVendorRatingMaster : " + logUserId + LOGGERStart);
        boolean isUpdate = false;
        try {
            isUpdate = cmsVendorRatingMasterService.updateCmsVendorRatingMaster(tblCmsVendorRatingMaster);
        } catch (Exception e) {
            logger.error("updateCmsVendorRatingMaster : " + logUserId + " : " + e);
        }
        logger.debug("updateCmsVendorRatingMaster : " + logUserId + LOGGEREnd);
        return isUpdate;
    }

    /****
     * This method deletes an TblCmsVendorRatingMaster object in DB
     * @param tblCmsVendorRatingMaster
     * @return boolean
     */
    public boolean deleteCmsVendorRatingMaster(TblCmsVendorRatingMaster tblCmsVendorRatingMaster) {
        logger.debug("deleteCmsVendorRatingMaster : " + logUserId + LOGGERStart);
        boolean isDeleted = false;
        try {
            isDeleted = cmsVendorRatingMasterService.deleteCmsVendorRatingMaster(tblCmsVendorRatingMaster);
        } catch (Exception e) {
            logger.error("deleteCmsVendorRatingMaster : " + logUserId + " : " + e);
        }
        logger.debug("deleteCmsVendorRatingMaster : " + logUserId + LOGGEREnd);
        return isDeleted;
    }

    /****
     * This method returns all TblCmsVendorRatingMaster objects from database
     * @return List<TblCmsVendorRatingMaster>
     */
    public List<TblCmsVendorRatingMaster> getAllCmsVendorRatingMaster() {
        logger.debug("getAllCmsVendorRatingMaster : " + logUserId + LOGGERStart);
        List<TblCmsVendorRatingMaster> cmsVendorRatingList = new ArrayList<TblCmsVendorRatingMaster>();
        try {
            cmsVendorRatingList = cmsVendorRatingMasterService.getAllCmsVendorRatingMaster();
        } catch (Exception e) {
            logger.error("getAllCmsVendorRatingMaster : " + logUserId + " : " + e);
        }
        logger.debug("getAllCmsVendorRatingMaster : " + logUserId + LOGGEREnd);
        return cmsVendorRatingList;

    }

    /***
     *  This method returns no. of TblCmsVendorRatingMaster objects from database
     * @return long
     */
    public long getCmsVendorRatingMasterCount() {
        logger.debug("getCmsVendorRatingMasterCount : " + logUserId + LOGGERStart);
        long count = 0;
        try {
            count = cmsVendorRatingMasterService.getCmsVendorRatingMasterCount();
        } catch (Exception e) {
            logger.error("getCmsVendorRatingMasterCount : " + logUserId + " : " + e);
        }
        logger.debug("getCmsVendorRatingMasterCount : " + logUserId + LOGGEREnd);
        return count;

    }

    /***
     * This method returns TblCmsVendorRatingMaster for the given Id
     * @param int vendorRatingId
     * @return TblCmsVendorRatingMaster
     */
    public TblCmsVendorRatingMaster getCmsVendorRatingMaster(int vendorRatingId) {
        logger.debug("getCmsVendorRatingMaster : " + logUserId + LOGGERStart);
        TblCmsVendorRatingMaster tblCmsVendorRatingMaster = new TblCmsVendorRatingMaster();
        try {
            tblCmsVendorRatingMaster = cmsVendorRatingMasterService.getCmsVendorRatingMaster(vendorRatingId);
        } catch (Exception e) {
            logger.error("getCmsVendorRatingMaster : " + logUserId + " : " + e);
        }
        logger.debug("getCmsVendorRatingMaster : " + logUserId + LOGGEREnd);
        return tblCmsVendorRatingMaster;

    }

    /***
     * This method gives the TblCmsVendorRatingMaster object for the given Star Rating
     * @param String rating
     * @return TblCmsVendorRatingMaster
     */
    public TblCmsVendorRatingMaster getCmsVendorRatingMasterForRating(String rating) {
        logger.debug("getCmsVendorRatingMasterForRating : " + logUserId + LOGGERStart);
        TblCmsVendorRatingMaster tblCmsVendorRatingMaster = new TblCmsVendorRatingMaster();
        try {
            tblCmsVendorRatingMaster = cmsVendorRatingMasterService.getCmsVendorRatingMasterForRating(rating);
        } catch (Exception e) {
            logger.error("getCmsVendorRatingMasterForRating : " + logUserId + " : " + e);
        }
        logger.debug("getCmsVendorRatingMasterForRating : " + logUserId + LOGGEREnd);
        return tblCmsVendorRatingMaster;

    }
}
