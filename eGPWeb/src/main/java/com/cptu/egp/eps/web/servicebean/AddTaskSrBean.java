/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblToDoList;
import com.cptu.egp.eps.service.serviceimpl.AddTaskServiceImpl;
import com.cptu.egp.eps.web.databean.AddTaskDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class AddTaskSrBean {

    private AddTaskServiceImpl addTaskSevice = (AddTaskServiceImpl) AppContext.getSpringBean("AddTaskService");
    static final Logger logger = Logger.getLogger(AddTaskSrBean.class);
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String logUserId = "0";

    public String getLogUserId() {
        return logUserId;
    }
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        addTaskSevice.setAuditTrail(auditTrail);
    }

    public void setLogUserId(String logUserId) {
        addTaskSevice.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    public void addAddTask(AddTaskDtBean addTaskDtBean, int uid) {
        logger.debug("addAddTask : " + logUserId + loggerStart);
        try {
            String status = "No";
            TblToDoList tblToDoList = _toTblTodDoList(addTaskDtBean);
            addTaskSevice.addAddTask(tblToDoList, uid, status);
        } catch (Exception e) {
            logger.error("addAddTask : " + logUserId,e);
        }
        logger.debug("addAddTask : " + logUserId + loggerEnd);
    }

    public TblToDoList _toTblTodDoList(AddTaskDtBean addTaskDtBean) {
        logger.debug("_toTblTodDoList : " + logUserId + loggerStart);
        TblToDoList tblToDoList = new TblToDoList();
        try {
        BeanUtils.copyProperties(addTaskDtBean, tblToDoList);
        } catch (Exception e) {
            logger.error("_toTblTodDoList : " + logUserId,e);
        }
        logger.debug("_toTblTodDoList : " + logUserId + loggerEnd);
        return tblToDoList;
    }
      List<TblToDoList> data = new ArrayList<TblToDoList>();

    public List<TblToDoList> getData(int userId, String status ) {
        logger.debug("getData : " + logUserId + loggerStart);
        try {
            data = addTaskSevice.getTaskBrief(userId,status);
        } catch (Exception e) {
            logger.error("getData : " + logUserId,e);
        }
        logger.debug("getData : " + logUserId + loggerEnd);
        return data;
    }

    public void setData(List<TblToDoList> data) {
        this.data = data;
    }

    public List<TblToDoList> searchTasks(int offcet,int rows,Date startDate, Date endDate, String status, int userId) {
        logger.debug("searchTasks : " + logUserId + loggerStart);
     List<TblToDoList> search = new ArrayList<TblToDoList>();
        try {
            search = addTaskSevice.searchTasks(offcet,rows,startDate, endDate, status, userId);
        } catch (Exception e) {
            logger.error("searchTasks : " + logUserId,e);
        }
        /*TblToDoList tdList=new TblToDoList();
     if(search.isEmpty())
        
     else
     {
         Iterator it=search.iterator();
         while(it.hasNext())
         {
              tdList=(TblToDoList)it.next();
         }
        }*/
        logger.debug("searchTasks : " + logUserId + loggerEnd);
     return search;
 }

    public List<TblToDoList> getTasksBytaskId(int taskid) {
        logger.debug("getTasksBytaskId : " + logUserId + loggerStart);
     List<TblToDoList> todoList = null;
        try {
    todoList = addTaskSevice.getTasksBytaskId(taskid);
        } catch (Exception e) {
            logger.error("getTasksBytaskId : " + logUserId,e);
        }
        logger.debug("getTasksBytaskId : " + logUserId + loggerEnd);
    return todoList;
    }

    public void updateTask(TblToDoList tblToDoList) {
        logger.debug("updateTask : " + logUserId + loggerStart);
        try {
    addTaskSevice.updateTask(tblToDoList);
        } catch (Exception e) {
            logger.error("updateTask : " + logUserId,e);
        }
        logger.debug("updateTask : " + logUserId + loggerEnd);
    }
}
