/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.OfficeMemberDtBean;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblBiddingPermission;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.model.table.TblDebarmentDetails;
import com.cptu.egp.eps.model.table.TblDebarmentDocs;
import com.cptu.egp.eps.model.table.TblDebarmentReq;
import com.cptu.egp.eps.model.table.TblDebarmentResp;
import com.cptu.egp.eps.model.table.TblDebarmentTypes;
import com.cptu.egp.eps.model.table.TblDebarredBiddingPermission;
import com.cptu.egp.eps.service.serviceimpl.CommitteMemberService;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.BiddingPermissionService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.DebarredBiddingPermissionService;
import com.cptu.egp.eps.service.serviceinterface.InitDebarmentService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.servicebean.InitDebarmentSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author TaherT
 */
public class InitDebarment extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private File destinationDir;
    DataInputStream dis = null;
    BufferedInputStream bis = null;
    FileInputStream fis = null;
    String logUSerId = "0";
    Logger LOGGER = Logger.getLogger(InitDebarment.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    /**
     * Handles post and get of request
     *
     * @param request Http Request
     * @param response Http Response
     * @throws ServletException Exception
     * @throws IOException Exception
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        logUSerId = request.getSession().getAttribute("userId") != null ? request.getSession().getAttribute("userId").toString() : "0";
        LOGGER.debug("processRequest Status " + logUSerId + "starts");
        String redirectPage = null;
        try {
            String action = request.getParameter("action");
            if (action.equalsIgnoreCase("initiateDebarment")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                redirectPage = "admin/InitiateDebarmentProcessNew.jsp?flag=" + initiateDebarmentNew(request);
                LOGGER.debug(action + "Status " + logUSerId + "Ends");
            }
            if (action.equalsIgnoreCase("ReinstateDebarment")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                redirectPage = "admin/DebarmentListingNew.jsp?flag=" + ReinstateDebarment(request);
                LOGGER.debug(action + "Status " + logUSerId + "Ends");
            }if (action.equalsIgnoreCase("UpdateDebarment")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                redirectPage = "admin/DebarmentAction.jsp?" + UpdateDebarment(request);
                LOGGER.debug(action + "Status " + logUSerId + "Ends");
            } else if (action.equalsIgnoreCase("bidderResponse")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                redirectPage = "tenderer/" + (bidderResponse(request) ? "TendererDebarListing.jsp?flag=success&isprocess=y" : "DebarmentClarification.jsp?flag=fail");
                LOGGER.debug(action + "Status " + logUSerId + "Ends");
            } else if (action.equals("fetchData")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                PrintWriter out = response.getWriter();
                response.setContentType("text/xml;charset=UTF-8");
                out.print(fetchData(request));
                out.close();
                LOGGER.debug(action + "Status " + logUSerId + "Ends");
            } else if (action.equals("getDebarData")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                PrintWriter out = response.getWriter();
                out.print(getDebarData(request));
                out.close();
            } else if (action.equalsIgnoreCase("memberSearch")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                PrintWriter out = response.getWriter();
                out.print(memberSearch(request.getParameter("officeId")));
                out.close();
                LOGGER.debug(action + "Status " + logUSerId + "Ends");
            } else if (action.equalsIgnoreCase("debarDocUpload")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                response.setContentType("text/html");
                redirectPage = debarDocUpload(request);
                //response.sendRedirectFilter(pageName+"?debId="+debarId+"&stat="+status+queryString);
            } else if ("debarDocDelete".equalsIgnoreCase(action)) {
                LOGGER.debug(action + "Status " + logUSerId + " Starts");
                redirectPage = debarDocDelete(request);
                //response.sendRedirectFilter(pageName+"?debId="+debarId+"&stat="+status);
                LOGGER.debug(action + "Status " + logUSerId + " Ends");
            } else if ("debarDocDownload".equalsIgnoreCase(action)) {
                LOGGER.debug(action + "Status " + logUSerId + " Starts");
                redirectPage = debarDocDownload(request, response);
                //response.sendRedirectFilter(pageName+"?debId="+debarId+"&stat="+status);
                LOGGER.debug(action + "Status " + logUSerId + " Ends");
            } else if (action.equals("getDebarListCommon")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                PrintWriter out = response.getWriter();
                out.print(getDebarDataCommon(request));
                out.close();
            }else if (action.equals("getDebarDataListing")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                PrintWriter out = response.getWriter();
                out.print(getDebarDataListing(request));
                out.close();
            } else if (action.equals("fetchDebarmentData")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                PrintWriter out = response.getWriter();
                out.print(fetchDebarmentData(request));
                out.close();
            } else if (action.equals("loadWorkCategory")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                PrintWriter out = response.getWriter();
                out.print(loadWorkCategoryData(request));
                out.close();
            } else if (action.equals("loadWorkCategoryReinstate")) {
                LOGGER.debug(action + "Status " + logUSerId + "starts");
                PrintWriter out = response.getWriter();
                out.print(loadWorkCategoryReinstateData(request));
                out.close();
            }

        } catch (Exception ex) {
            LOGGER.error("processRequest " + logUSerId + " Error" + ex);
        }
        if (redirectPage != null) {
            response.sendRedirect(redirectPage);
        }
        LOGGER.debug("processRequest " + logUSerId + " Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String memberSearch(String officeId) {
        LOGGER.debug("memberSearch " + logUSerId + " Starts");
        CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");
        List<OfficeMemberDtBean> list = committeMemberService.findOfficeMember(officeId, "GetDebarmentComUser");
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (OfficeMemberDtBean officeMemberDtBean : list) {
            sb.append("<tr>");
            sb.append("<td class='t-align-center'>");
            sb.append("<label><input id='chk" + i + "' type='checkbox' value='" + officeMemberDtBean.getUserid() + "'/></label></td>");
            sb.append("<td class='t-align-center'><input type='hidden' id='spn" + i + "' value='" + officeMemberDtBean.getEmpName() + "'/>" + officeMemberDtBean.getEmpName() + "</td>");
            sb.append("<td class='t-align-center'><input type='hidden' id='memstat" + i + "' value='" + officeMemberDtBean.getProcureRole() + "'/>" + officeMemberDtBean.getDesgName() + "</td>");
            sb.append("<td class='t-align-center'>" + officeMemberDtBean.getProcureRole() + "</td>");
            sb.append("</tr>");
            i++;
        }
        LOGGER.debug("memberSearch " + logUSerId + " Ends");
        return sb.toString();
    }

    /**
     * File Extension allowed
     *
     * @param extn file extension
     * @param size size of file
     * @param userType UserType Id
     * @return true of false for allowed or restrict
     */
    public boolean checkExtension(String extn, long size, String userType) {
        LOGGER.debug("checkExtension " + logUSerId + " Starts");
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);

        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        LOGGER.debug("checkExtension " + logUSerId + " Ends");
        return chextn;
    }

    /**
     * Message for document Size
     *
     * @param userId for the userId
     * @return OK for allow
     */
    public String docSizeMsg(int userId) {
        LOGGER.debug("docSizeMsg " + logUSerId + " Starts");
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        LOGGER.debug("docSizeMsg " + logUSerId + " Ends");
        return userRegisterService.docSizeCheck(userId);
    }

    /**
     * Size of the document
     *
     * @param extn files extension
     * @param size document size
     * @param userType user type Id
     * @return true or false for success
     */
    public boolean checkSize(String extn, long size, String userType) {
        LOGGER.debug("checkSize " + logUSerId + " Starts");
        float fsize = 0.0f;
        float dsize = 0.0f;
        boolean chextn = false;
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        fsize = size / (1024 * 1024);
        dsize = configurationMaster.getFileSize();
        if (dsize > fsize) {
            chextn = true;
        } else {
            chextn = false;
        }
        LOGGER.debug("checkSize " + logUSerId + " Ends");
        return chextn;
    }

    private boolean initiateDebarment(HttpServletRequest request) {
        boolean flag = false;
        try {
            TblDebarmentReq tblDebarmentReq = new TblDebarmentReq();
            String userId = request.getParameter("companyName");
            String debarmentType = request.getParameter("debarmentType");
            String debarmentReason = request.getParameter("clarification");
            String lastDateOfResponse = request.getParameter("lastResponseDate");

            tblDebarmentReq.setClarification(debarmentReason);
            tblDebarmentReq.setUserId(Integer.parseInt(userId));
            tblDebarmentReq.setDebarmentType(debarmentType);
            tblDebarmentReq.setLastResponseDt(DateUtils.formatStdString(lastDateOfResponse));
            tblDebarmentReq.setDebarmentBy(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
            tblDebarmentReq.setClarificationReqDt(new Date());
            tblDebarmentReq.setDebarmentStatus("withpe");
            tblDebarmentReq.setComments("");
            tblDebarmentReq.setHopeId(0);
            tblDebarmentReq.setTblDebarmentTypes(new TblDebarmentTypes(Integer.parseInt(request.getParameter("debarType"))));

            TblDebarmentDetails tblDebarmentDetails = new TblDebarmentDetails();
            if ("".equals(request.getParameter("debarIds"))) {
                tblDebarmentDetails.setDebarIds("");
            } else {
                tblDebarmentDetails.setDebarIds(request.getParameter("debarIds").substring(0, request.getParameter("debarIds").length() - 1));
            }
            tblDebarmentDetails.setDebarStatus("bype");
            tblDebarmentDetails.setDebarTypeId(Integer.parseInt(request.getParameter("debarType")));
            tblDebarmentDetails.setDebarmentBy((Integer) request.getSession().getAttribute("userId"));
            tblDebarmentDetails.setDebarmentDt(new Date());
            InitDebarmentService initDebarmentService = (InitDebarmentService) AppContext.getSpringBean("InitDebarmentService");
            initDebarmentService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
            flag = initDebarmentService.saveInitDebarmentRequest(tblDebarmentReq, tblDebarmentDetails);

        } catch (Exception ex) {
            LOGGER.error(request.getParameter("action") + " " + logUSerId + "Error:" + ex);
        }
        return flag;
    }

    private boolean bidderResponse(HttpServletRequest request) {
        boolean flag = false;
        try {
            String bidderResponse = request.getParameter("response");
            TblDebarmentResp tblDebarmentResp = new TblDebarmentResp();
            tblDebarmentResp.setResponseTxt(bidderResponse);
            tblDebarmentResp.setTblDebarmentReq(new TblDebarmentReq(Integer.parseInt(request.getParameter("debarmentId"))));
            tblDebarmentResp.setResponseDt(new Date());
            tblDebarmentResp.setUserId(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
            InitDebarmentService initDebarmentService = (InitDebarmentService) AppContext.getSpringBean("InitDebarmentService");
            initDebarmentService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
            if (initDebarmentService.saveBidderResponse(tblDebarmentResp)) {
                MailContentUtility mailContentUtility = new MailContentUtility();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                InitDebarmentSrBean srBean = new InitDebarmentSrBean();
                srBean.sendMailMsgBox(initDebarmentService.getPEEmailId(request.getParameter("debarmentId")).toString(), "e-GP: Debarment request", mailContentUtility.tendererResponseToPE(request.getParameter("cmpName")), msgBoxContentUtility.tendererResponseToPE(request.getParameter("cmpName")), initDebarmentService.getTendererEmailId(request.getParameter("debarmentId")).toString());
                srBean = null;
                mailContentUtility = null;
                msgBoxContentUtility = null;
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error(request.getParameter("action") + "Status " + logUSerId + " " + ex);
        }
        return flag;
    }

    private String fetchData(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        try {
            InitDebarmentService debarmentService = (InitDebarmentService) AppContext.getSpringBean("InitDebarmentService");
            String status = request.getParameter("status");
            String rows = request.getParameter("rows");
            String sord = request.getParameter("sord");
            String sidx = request.getParameter("sidx");
            String page = request.getParameter("page");
            String depId = (request.getParameter("depId") != null && !"".equals(request.getParameter("depId"))) ? request.getParameter("depId") : null;
            String officeId = (request.getParameter("offId") != null && !"0".equals(request.getParameter("offId"))) ? request.getParameter("offId") : null;
            String mailId = (request.getParameter("mailId") != null && !"".equals(request.getParameter("mailId"))) ? request.getParameter("mailId") : null;
            String cmpName = (request.getParameter("cmpName") != null && !"".equals(request.getParameter("cmpName"))) ? request.getParameter("cmpName") : null;
            StringBuffer query = new StringBuffer();
            if (mailId != null) {
                query.append(" and tm.tblLoginMaster.userId in (select lm.userId from TblLoginMaster lm where lm.emailId='" + mailId + "')");
            }
            if (cmpName != null) {
                query.append(" and tcm.companyName like '%" + cmpName + "%'");
            }
            if (officeId != null) {
                query.append(" and tdr.debarmentBy in (select em.tblLoginMaster.userId from TblEmployeeOffices eo,TblEmployeeMaster em where em.employeeId = eo.tblEmployeeMaster.employeeId and eo.tblOfficeMaster.officeId=" + officeId + ")");
            } else if (depId != null) {
                query.append(" and tdr.debarmentBy in (select c.tblLoginMaster.userId from TblOfficeMaster a, TblEmployeeOffices b, TblEmployeeMaster c, TblProcurementRole d,TblEmployeeRoles e");
                query.append(" where a.officeId = b.tblOfficeMaster.officeId and b.tblEmployeeMaster.employeeId = c.employeeId and e.tblProcurementRole.procurementRoleId = d.procurementRoleId");
                query.append(" and d.procurementRoleId = 1 and e.tblEmployeeMaster.employeeId = c.employeeId and a.departmentId = " + depId + ")");
            }
            status = status + query.toString();
            int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
            int totalPages = 0;
            int totalCount = (int) debarmentService.debarMentListingCount(status);
            if ("".equals(sidx)) {
                status = status + " order by tdr.debarmentId desc";
            } else {
                status = status + " order by " + sidx + " " + sord;
            }
            List<Object[]> data = debarmentService.findDebarMentListing(status, offset, Integer.parseInt(rows));
            if (totalCount > 0) {
                if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                    totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                } else {
                    totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                }

            } else {
                totalPages = 0;
            }
            sb.append("<?xml version='1.0' encoding='utf-8'?>\n");
            sb.append("<rows>");
            sb.append("<page>" + request.getParameter("page") + "</page>");

            sb.append("<total>" + totalPages + "</total>");
            sb.append("<records>" + totalCount + "</records>");
            int srNo = offset + 1;
            int rowId = 0;
            for (Object[] obj : data) {
                sb.append("<row id='" + rowId + "'>");
                sb.append("<cell><![CDATA[" + srNo + "]]></cell>");
                String compName = "";
                if (obj[4].toString().equals("-")) {
                    compName = obj[5] + " " + obj[6];
                    sb.append("<cell><![CDATA[" + obj[5] + "&nbsp;" + obj[6] + "]]></cell>");

                } else {
                    compName = obj[4].toString();
                    sb.append("<cell><![CDATA[" + obj[4] + "]]></cell>");
                }
                sb.append("<cell><![CDATA[" + obj[1] + "]]></cell>");
                sb.append("<cell><![CDATA[" + DateUtils.gridDateToStrWithoutSec((Date) obj[2]) + "]]></cell>");
                String statusString = null;

                if (obj[3].equals("withpe")) {
                    statusString = "Requested By PA";
                } else if (obj[3].equals("pending")) {
                    statusString = "Pending";
                } else if (obj[3].equals("pesatisfy")) {
                    statusString = "Satisfactory Response From Bidder";
                } else if (obj[3].equals("sendtohope")) {
                    statusString = "Sent To HOPA";
                } else if (obj[3].equals("hopesatisfy")) {
                    statusString = "Satisfactory Response From Bidder";
                } else if (obj[3].equals("sendtocom")) {
                    statusString = "Committee Review";
                } else if (obj[3].equals("comsatisfy")) {
                    statusString = "Satisfactory Response From Bidder";
                } else if (obj[3].equals("sendtoegp")) {
                    statusString = "Sent To eGP Admin";
                } else if (obj[3].equals("egpsatisfy")) {
                    statusString = "Satisfactory Response From Bidder";
                } else if (obj[3].equals("appdebaregp")) {
                    statusString = "Debared By eGP Admin";
                } else if (obj[3].equals("appdebarhope")) {
                    statusString = "Debared By HOPA";
                } else if (obj[3].equals("appdebarcomhope")) {
                    statusString = "Debared By HOPA and Committee";
                } else {
                    statusString = "";
                }
                sb.append("<cell><![CDATA[" + statusString + "]]></cell>");
                String pageName = null;
                int p = Integer.parseInt(request.getParameter("p"));
                String isprocess = "";
                if (p == 1) {//(Must show to the particular PE from tbl_DebarmentReq debarmentBy)
                    pageName = "ViewDebarClarification";
                } else if (p == 2) {//(Must show to the particular HOPA from tbl_DebarmentReq debarmentBy hopeId)
                    pageName = "ProcessDebarmentReq";
                } else if (p == 3) {
                    pageName = "ProcessComDebarReq";
                } else if (p == 4) {
                    pageName = "ProcessEgpDebarReq";
                } else if (p == 5) {
                    pageName = "DebarmentClarification";
                    if (status.indexOf("tdr.debarmentId in (") != -1) {
                        isprocess = "&isprocess=y";
                    }
                }
                String label = null;
                if ("withpe".equals(obj[3])) {
                    label = "Send to Bidder";
                } else {
                    label = "View";
                }
                sb.append("<cell><![CDATA[");
                sb.append("<a href='" + pageName + ".jsp?debId=" + obj[0] + "&stat=" + obj[3] + isprocess + "'>" + label + "</a>");
                sb.append("]]></cell>");
                List<Object[]> comList = debarmentService.isCommitteeExist(obj[0].toString());
                if (p == 2) {
                    sb.append("<cell><![CDATA[");
                    if ((obj[3].equals("sendtocom") && comList.isEmpty())) {
                        sb.append("<a href='DebarmentCommittee.jsp?debId=" + obj[0] + "'>Create</a>");
                    } else if (!comList.isEmpty()) {
                        if (comList.get(0)[1].toString().equalsIgnoreCase("pending")) {
                            sb.append("<a href='DebarmentCommittee.jsp?debId=" + obj[0] + "&comId=" + comList.get(0)[0] + "&cName=" + comList.get(0)[2] + "&isedit=y'>Edit</a>");
                            sb.append("&nbsp;|&nbsp;");
                            sb.append("<a href='DebarmentCommittee.jsp?debId=" + obj[0] + "&comId=" + comList.get(0)[0] + "&cName=" + comList.get(0)[2] + "&isview=y'>View</a>");
                            sb.append("&nbsp;|&nbsp;");
                            sb.append("<a href='DebarmentCommittee.jsp?debId=" + obj[0] + "&comId=" + comList.get(0)[0] + "&cName=" + comList.get(0)[2] + "&ispub=y&compname=" + compName + "'>Publish</a>");
                        }
                        if (comList.get(0)[1].toString().equalsIgnoreCase("published")) {
                            sb.append("<a href='DebarmentCommittee.jsp?debId=" + obj[0] + "&comId=" + comList.get(0)[0] + "&cName=" + comList.get(0)[2] + "&isview=y'>View</a>");
                        }
                    } else {
                        sb.append("<center>-</center>");
                    }
                    sb.append("]]></cell>");
                }
                sb.append("</row>   ");
                srNo++;
                rowId++;
            }
            sb.append("</rows>");
        } catch (Exception e) {
            LOGGER.error(request.getParameter("action"), e);
        }
        return sb.toString();
    }

    private String getDebarData(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        InitDebarmentSrBean srBean = new InitDebarmentSrBean();
        List<SPTenderCommonData> data = srBean.searchDataForDebarType(request.getParameter("debarType"), request.getSession().getAttribute("userId").toString());
        int[] colWidth = request.getParameter("debarType").equals("1") ? new int[]{10, 15, 25, 50} : new int[]{10, 0, 45, 45};
        for (SPTenderCommonData sptcd : data) {
            sb.append("<tr>");
            sb.append("<td class='t-align-center' width='" + colWidth[0] + "%'><input type='checkbox' name='debarId' id='debarId' value='" + sptcd.getFieldName1() + "'/></td>");
            if (request.getParameter("debarType").equals("1")) {
                sb.append("<td mysearch='1' width='" + colWidth[1] + "%'>" + sptcd.getFieldName1() + "</td>");
            }
            sb.append("<td mysearch='2' width='" + colWidth[2] + "%'>" + sptcd.getFieldName2() + "</td>");
            if (request.getParameter("debarType").equals("1")) {
                String viewLink = "<a onclick=\"javascript:window.open('" + request.getContextPath() + "/resources/common/ViewTender.jsp?id=" + sptcd.getFieldName1() + "', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'>" + sptcd.getFieldName3() + "</a>";
                sb.append("<td width='" + colWidth[3] + "%'>" + viewLink + "</td>");
            } else {
                sb.append("<td width='" + colWidth[3] + "%'>" + sptcd.getFieldName3() + "</td>");
            }
            sb.append("</tr>");
        }
        return sb.toString();
    }

    private String debarDocUpload(HttpServletRequest request) {
        //////Taher Upload Docsss.......
        String DESTINATION_DIR_PATH = null;
        int userTypeId = 0;
        String userType = null;
        String pageName = null;
        String queryString = "";
        String status = request.getParameter("stat");
        String debarId = request.getParameter("debId");
        String user = request.getParameter("user");
        if (user.equals("pe")) {
            pageName = "ViewDebarClarification.jsp";
        } else if (user.equals("hope")) {
            pageName = "ProcessDebarmentReq.jsp";
        } else if (user.equals("tend")) {
            pageName = "DebarmentClarification.jsp";
        }
        if (request.getSession() != null) {
            userTypeId = Integer.parseInt(request.getSession().getAttribute("userTypeId").toString());
        }
        if (userTypeId == 2) {
            DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("DebarmentTenderer");
            userType = "tenderer";
        }
        if (userTypeId == 3) {
            DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("DebarmentOfficer");
            userType = "officer";
        }
        String realPath = DESTINATION_DIR_PATH + debarId + "_" + request.getSession().getAttribute("userId");//request.getSession().getAttribute("userId")
        destinationDir = new File(realPath);
        if (!destinationDir.isDirectory()) {
            destinationDir.mkdir();
        }
        try {
            //UploadListener listener = new UploadListener(request, 30);
            //DiskFileItemFactory fileItemFactory = new MonitoredDiskFileItemFactory(listener);
            DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
            /*
             *Set the size threshold, above which content will be stored on disk.
             */
            fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB
            /*
             * Set the temporary directory to store the uploaded files of size above threshold.
             */
            fileItemFactory.setRepository(tmpDir);

            ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
            TestProgressListener testProgressListener = new TestProgressListener();
            uploadHandler.setProgressListener(testProgressListener);
            HttpSession session = request.getSession();
            session.setAttribute("testProgressListener", testProgressListener);
            try {
                /*
                 * Parse the request
                 */
                List items = uploadHandler.parseRequest(request);
                Iterator itr = items.iterator();
                String docSizeMsg = "";
                boolean checkExt = false;
                boolean checkSize = false;
                boolean sameFile = false;
                TblDebarmentDocs docs = new TblDebarmentDocs();
                while (itr.hasNext()) {
                    FileItem item = (FileItem) itr.next();
                    //For Supporting Document
                    /*
                     * Handle Form Fields.
                     */
                    if (item.isFormField()) {
                        if (item.getFieldName().equals("documentBrief")) {
                            docs.setDocumentDesc(item.getString());
                        }
                    } else {

                        String fileName = "";
                        if (item.getName().lastIndexOf("\\") != -1) {
                            fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                        } else {
                            fileName = item.getName();
                        }
                        //fileName  = fileName.replaceAll(" ", "");
                        File f = new File(destinationDir + "\\" + fileName);
                        if (f.isFile()) {
                            sameFile = true;
                            break;
                        }
                        docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                        if (!docSizeMsg.equals("ok")) {
                            break;
                        }
                        checkExt = checkExtension(fileName, item.getSize(), userType);
                        if (!checkExt) {
                            break;
                        }
                        checkSize = checkSize(fileName, item.getSize(), userType);
                        if (!checkSize) {
                            break;
                        }
                        docs.setDocumentName(fileName);
                        docs.setUploadedBy(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                        docs.setUploadedDate(new Date());
                        docs.setUploadedRole(user);
                        docs.setDebarmentId(Integer.parseInt(debarId));
                        File file = new File(destinationDir, fileName);
                        item.write(file);
                        //File Encyrption starts
                        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                        docs.setDocSize(String.valueOf(fileEncryptDecryptUtil.fileEncryptUtil(file, (int) item.getSize())));
                        fileEncryptDecryptUtil = null;
                        //File Encyrption ends
                        //fis.close();
                    }
                }

                if (sameFile) {
                    queryString = "&sf=y";//IMP note : sf means same file upload message.
                } else if (!docSizeMsg.equals("ok")) {
                    queryString = "&fq=" + docSizeMsg;//IMP note : fq means if total file quota of user increases than it is check through SP p_getappcommondat(checkQuota) and message is given.
                } else if (!checkExt) {
                    CheckExtension ext = new CheckExtension();
                    TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
                    queryString = "&ft=" + configurationMaster.getAllowedExtension();//IMP note : fs means single file size and ft means single file type if does'nt match in DB than this message is fired.
                } else {
                    CheckExtension ext = new CheckExtension();
                    TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
                    if (!checkSize) {
                        queryString = "&fs=" + configurationMaster.getFileSize();//IMP note : fs means single file size and ft means single file type if does'nt match in DB than this message is fired.
                    } else {
                        InitDebarmentService initDebarmentService = (InitDebarmentService) AppContext.getSpringBean("InitDebarmentService");
                        File file = new File(destinationDir, docs.getDocumentName());
                        if (file.isFile()) {
                            initDebarmentService.saveDocument(docs);
                        } else {
                            queryString = "&fq=Error occurred during file upload";
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.error(request.getParameter("action") + "Status " + logUSerId + "Error" + e);
            }
        } catch (Exception ex) {
            LOGGER.error(request.getParameter("action") + "Status " + logUSerId + "Error" + ex);
            log("Error encountered while uploading file", ex);
        }
        LOGGER.debug(request.getParameter("action") + "Status " + logUSerId + " Ends");
        return userType + "/" + pageName + "?debId=" + debarId + "&stat=" + status + queryString;
    }

    private String debarDocDelete(HttpServletRequest request) {
        String status = request.getParameter("stat");
        String debarId = request.getParameter("debId");
        String pageName = null;
        int userTypeId = 0;
        String pathName = null;
        String userId = request.getParameter("userId");
        String userType = null;
        String user = request.getParameter("user");
        String docId = request.getParameter("docId");
        String docName = request.getParameter("docName");
        if (user.equals("pe")) {
            pageName = "ViewDebarClarification.jsp";
        } else if (user.equals("hope")) {
            pageName = "ProcessDebarmentReq.jsp";
        } else if (user.equals("tend")) {
            pageName = "DebarmentClarification.jsp";
        }
        if (request.getSession() != null) {
            userTypeId = Integer.parseInt(request.getSession().getAttribute("userTypeId").toString());
        }
        if (userTypeId == 2) {
            pathName = FilePathUtility.getFilePath().get("DebarmentTenderer");
            userType = "tenderer";
        }
        if (userTypeId == 3) {
            pathName = FilePathUtility.getFilePath().get("DebarmentOfficer");
            userType = "officer";
        }
        File f = new File(pathName + debarId + "_" + userId + "\\" + docName);
        f.delete();
        InitDebarmentService initDebarmentService = (InitDebarmentService) AppContext.getSpringBean("InitDebarmentService");
        initDebarmentService.deleteDebarDocs(docId);
        return userType + "/" + pageName + "?debId=" + debarId + "&stat=" + status;
    }

    private String debarDocDownload(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String status = request.getParameter("stat");
        String debarId = request.getParameter("debId");
        String pageName = null;
        int userTypeId = Integer.parseInt(request.getParameter("uTid"));
        String pathName = null;
        String userId = request.getParameter("userId");
        String userType = null;
        String user = request.getParameter("user");
        String docSize = request.getParameter("docSize");
        String docName = request.getParameter("docName");
        if (user.equals("pe")) {
            pageName = "ViewDebarClarification.jsp";
        } else if (user.equals("hope")) {
            pageName = "ProcessDebarmentReq.jsp";
        } else if (user.equals("tend")) {
            pageName = "DebarmentClarification.jsp";
        }
        /*if(request.getSession()!=null){
            userTypeId=Integer.parseInt(request.getSession().getAttribute("userTypeId").toString());
        }*/
        if (userTypeId == 2) {
            pathName = FilePathUtility.getFilePath().get("DebarmentTenderer");
            userType = "tenderer";
        }
        if (userTypeId == 3) {
            pathName = FilePathUtility.getFilePath().get("DebarmentOfficer");
            userType = "officer";
        }
        File file = new File(pathName + debarId + "_" + userId + "\\" + docName);
        fis = new FileInputStream(file);
        byte[] buf = new byte[Integer.valueOf(docSize)];
        int offset = 0;
        int numRead = 0;
        while ((offset < buf.length)
                && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

            offset += numRead;

        }
        fis.close();
        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
        buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
        fileEncryptDecryptUtil = null;
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=\"" + docName + "\"");
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(buf);
        outputStream.flush();
        outputStream.close();
        return userType + "/" + pageName + "?debId=" + debarId + "&stat=" + status;
    }

    private String getDebarDataCommon(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        /* 2. individual / company - searchFor
            3. companyname or firstname - cmpName or firName
            4. lastname - lastName
            5. 3 equals - like or firNameSearch
            6. 4 equals - lasNameSearch
            7. debarstartdate - dtFrom
            8. debarenddate - dtTo
            9.country - cntry
            10. State - state */
        try {

            String searchFor = request.getParameter("searchFor");
            String cmpfirName = request.getParameter("cmpName");
            String debarstartdate = request.getParameter("dtFrom");
            String debarenddate = request.getParameter("dtTo");

            String strPageNo = request.getParameter("pageNo");
            int pageNo = Integer.parseInt(strPageNo);

            String strOffset = request.getParameter("size");
            int recordOffset = Integer.parseInt(strOffset);

            CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            List<SPCommonSearchDataMore> list = dataMoreService.geteGPDataMore("getDebarmentList", searchFor, cmpfirName, debarstartdate, debarenddate, strPageNo, strOffset);
            String styleClass = "";
            if (list != null && !list.isEmpty()) {
                for (int i = 0; i < list.size(); i++) {
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    sb.append("<tr class='" + styleClass + "'>");
                    sb.append("<td class=\"t-align-center\">" + (((pageNo - 1) * recordOffset) + (i + 1)) + "</td>");
                    sb.append("<td class=\"t-align-left\">" + list.get(i).getFieldName3() + "</td>");
                    sb.append("<td class=\"t-align-left\">" + list.get(i).getFieldName8() + "</td>");
                    sb.append("<td class=\"t-align-left\">" + list.get(i).getFieldName1() + "</td>");
                    sb.append("<td class=\"t-align-center\">" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(list.get(i).getFieldName5(), "MMM dd yyyy h:mma")) + "</td>");
                    sb.append("<td class=\"t-align-center\">" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(list.get(i).getFieldName4(), "MMM dd yyyy h:mma")) + "</td>");
                    sb.append("<td class=\"t-align-center\">" + list.get(i).getFieldName7() + "</td>");
                    sb.append("</tr>");
                }
                int totalPages = 1;

                if (list.size() > 0) {
                    int count = Integer.parseInt(list.get(0).getFieldName9());
                    totalPages = (int) Math.ceil(Math.ceil(count) / recordOffset);
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                sb.append("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

            } else {
                sb.append("<tr>");
                sb.append("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"8\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                sb.append("</tr>");
            }

        } catch (Exception e) {
            LOGGER.error(request.getParameter("action"), e);
        }
        return sb.toString();
    }

    private String fetchDebarmentData(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        String pageNo = request.getParameter("pageNo");
        int pageNoInt = Integer.parseInt(pageNo);

        String pageSize = request.getParameter("size");
        int pageSizeInt = Integer.parseInt(pageSize);

        String email = request.getParameter("email");
        String companyName = request.getParameter("companyName");

        String viewType = request.getParameter("viewtype");
        CommonSearchService commonService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        List<SPCommonSearchData> list = new ArrayList<SPCommonSearchData>();
        if (viewType.equalsIgnoreCase("All")) {
            list = commonService.searchData("GetAllUserForDebarment", pageNo, pageSize, email, companyName, null, null, null, null, null);
        } else if (viewType.equalsIgnoreCase("Debarred")) {
            list = commonService.searchData("GetDebarredUserForDebarment", pageNo, pageSize, email, companyName, null, null, null, null, null);
        }

        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                String styleClass = "";
                if (i % 2 == 0) {
                    styleClass = "bgColor-white";
                } else {
                    styleClass = "bgColor-Green";
                }
                String link = request.getContextPath() + "/admin/InitDebarmentListingNew.jsp?companyId=" + list.get(i).getFieldName2() + "&debarredUser=" + list.get(i).getFieldName1();
                String action = "<a href='" + link + "'>Process</a>";
                sb.append("<tr class='" + styleClass + "'>");
                sb.append("<td class=\"t-align-center\">" + ((pageNoInt - 1) * 10 + (i + 1)) + "</td>");
                sb.append("<td class=\"t-align-left\">" + list.get(i).getFieldName4() + "</td>");
                sb.append("<td class=\"t-align-left\">" + list.get(i).getFieldName3() + "</td>");
                sb.append("<td class=\"t-align-center\">" + action + "</td>");
                sb.append("</tr>");
                int totalPages = 1;

                if (list.size() > 0) {
                    int count = Integer.parseInt(list.get(0).getFieldName5());
                    totalPages = (int) Math.ceil(Math.ceil(count) / pageSizeInt);
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                sb.append("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            }
        } else {
            sb.append("<tr>");
            sb.append("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
            sb.append("</tr>");
        }
        return sb.toString();
    }

    private String loadWorkCategoryData(HttpServletRequest request) throws JSONException {
        StringBuilder sb = new StringBuilder();
        StringBuilder alreadyDebar = new StringBuilder();

        String debarUserId = request.getParameter("objectId");
        List<SPCommonSearchData> userInfo = new ArrayList<SPCommonSearchData>();
        InitDebarmentSrBean debarmentSrBean = new InitDebarmentSrBean();
        userInfo = debarmentSrBean.getUserCompanyInfo(debarUserId);
        String compnayEmail = userInfo.get(0).getFieldName3();
        List<SPCommonSearchData> list = debarmentSrBean.getWorkCategory(userInfo.get(0).getFieldName1());
        int debarCount = 0;
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getFieldName2().equalsIgnoreCase("no")) {
                    sb.append("<input type='checkbox' value='" + list.get(i).getFieldName1() + "' name='workCategoryName' id='workCategoryName" + i + "' title='" + list.get(i).getFieldName1() + "' ");
                    sb.append(" />&nbsp;" + list.get(i).getFieldName1() + "&nbsp;&nbsp;");
                    sb.append("<input type='hidden' value='" + list.get(i).getFieldName1() + "' id='debarType' />");
                } else if (list.get(i).getFieldName2().equalsIgnoreCase("yes")) {
                    if (debarCount > 0) {
                        alreadyDebar.append(", ");
                    }
                    alreadyDebar.append(list.get(i).getFieldName1() + ": Debarred upto " + DateUtils.formatStdDate(DateUtils.convertStringtoDate(list.get(i).getFieldName3(), "MMM dd yyyy h:mma")));
                    debarCount++;
                }
            }
        } else {
            sb.append("<input type='hidden' value='' id='debarType' />");
        }
        List<SPCommonSearchData> debarInfoList = debarmentSrBean.getDebarmentInfoByUser(debarUserId);
        JSONObject obj = new JSONObject();
        obj.put("ProcCat", sb.toString());
        obj.put("alreadyDebar", debarCount > 0 ? alreadyDebar.toString() : "N/A");
        obj.put("compnayEmail", compnayEmail);
        if (debarInfoList.size() > 0) {
            obj.put("debarReason", debarInfoList.get(0).getFieldName6());
            obj.put("grounds", debarInfoList.get(0).getFieldName4());
            obj.put("debarFrom", DateUtils.formatStdDate(DateUtils.convertStringtoDate(debarInfoList.get(0).getFieldName2(), "MMM dd yyyy h:mma")));
            obj.put("debarTo", DateUtils.formatStdDate(DateUtils.convertStringtoDate(debarInfoList.get(0).getFieldName3(), "MMM dd yyyy h:mma")));
            
        }

        return obj.toString();
    }

    private String initiateDebarmentNew(HttpServletRequest request) {
        String msg = "fail";
        String debaruserId = request.getParameter("companyName");
        String[] workCategories = request.getParameterValues("workCategoryName");
        String debarmentType = request.getParameter("debarmentType");
        String debarmentReason = request.getParameter("clarification");
        String fromDate = request.getParameter("textfield_2");
        String toDate = request.getParameter("textfield_1");
        String debarDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date()).toString();
        List<SPCommonSearchData> userInfo = new ArrayList<SPCommonSearchData>();
        InitDebarmentSrBean debarmentSrBean = new InitDebarmentSrBean();
        userInfo = debarmentSrBean.getUserCompanyInfo(debaruserId);
        DebarredBiddingPermissionService debar = (DebarredBiddingPermissionService) AppContext.getSpringBean("DebarredBiddingPermissionService");
        BiddingPermissionService nonDebar = (BiddingPermissionService) AppContext.getSpringBean("BiddingPermissionService");
        List<TblDebarredBiddingPermission> debarBidPermissions = new ArrayList<TblDebarredBiddingPermission>();
        List<TblBiddingPermission> bidPermissions = new ArrayList<TblBiddingPermission>();
        List<TblDebarredBiddingPermission> tempDebarBidPermissions = new ArrayList<TblDebarredBiddingPermission>();
        List<TblBiddingPermission> tempBidPermissions = new ArrayList<TblBiddingPermission>();
        try {
            //tempDebarBidPermissions = debar.findTblDebarredBiddingPermission("userId", Operation_enum.EQ, Integer.parseInt(userInfo.get(0).getFieldName1()), "companyId", Operation_enum.EQ, Integer.parseInt(userInfo.get(0).getFieldName2()));
            tempBidPermissions = nonDebar.findTblBiddingPermission("userId", Operation_enum.EQ, Integer.parseInt(userInfo.get(0).getFieldName1()), "companyId", Operation_enum.EQ, Integer.parseInt(userInfo.get(0).getFieldName2()));

        } catch (Exception e) {
            System.out.print(e);
        }
        Date uptoDate = new Date();
        Date fDate = new Date();

        for (int i = 0; i < tempBidPermissions.size(); i++) {
            int isDebar = 0;
            for (String wCategory : workCategories) {
                if (tempBidPermissions.get(i).getProcurementCategory().equalsIgnoreCase(wCategory)) {
                    isDebar = 1;
                }
            }
            if (isDebar > 0) {
                fDate = DateUtils.formatStdString(fromDate);
                uptoDate = DateUtils.formatStdString(toDate);
                debarBidPermissions.add(new TblDebarredBiddingPermission(Long.parseLong("0"), tempBidPermissions.get(i).getProcurementCategory(),
                        tempBidPermissions.get(i).getWorkType(), tempBidPermissions.get(i).getWorkCategroy(), tempBidPermissions.get(i).getCompanyId(),
                        tempBidPermissions.get(i).getUserId(), tempBidPermissions.get(i).getIsReapplied(), uptoDate, fDate,
                        debarmentReason, DateUtils.formatStdString(debarDate), Integer.parseInt(logUSerId), debarmentType, "", 1));

            } else {
                bidPermissions.add(tempBidPermissions.get(i));
            }
        }

        msg = debar.updateDebarredBiddingList(debarBidPermissions, userInfo.get(0).getFieldName1(), userInfo.get(0).getFieldName2());
        if (msg.equalsIgnoreCase("success")) {
            msg = nonDebar.updateNonDebarredBiddingList(bidPermissions, userInfo.get(0).getFieldName1(), userInfo.get(0).getFieldName2());
        }
        if (msg.equalsIgnoreCase("success")) {
            String details = "";
            for (String wCategory : workCategories) {
                details += wCategory + ": From " + fromDate + " to " + toDate + "&nbsp;&nbsp;<br/>";
            }
            MailContentUtility mailContentUtility = new MailContentUtility();
            String mails[] = {userInfo.get(0).getFieldName3()};
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailTo(mails);
            sendMessageUtil.setEmailSub("e-GP: Debarment.");
            String mailText = mailContentUtility.mailForNotifyDebarredBidder(details);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            mails = null;
            sendMessageUtil = null;
        }
        return msg;
    }

    private String loadWorkCategoryReinstateData(HttpServletRequest request) throws JSONException {
        StringBuilder alreadyDebar = new StringBuilder();
        StringBuilder noDebar = new StringBuilder();

        String debarUserId = request.getParameter("objectId");
        InitDebarmentSrBean debarmentSrBean = new InitDebarmentSrBean();
        List<SPCommonSearchData> list = debarmentSrBean.getWorkCategory(debarUserId);
        int debarCount = 0;
        int noDebarCount = 0;
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getFieldName2().equalsIgnoreCase("no")) {
                    if (noDebarCount > 0) {
                        noDebar.append(", ");
                    }
                    noDebar.append(list.get(i).getFieldName1());
                    noDebarCount++;
                } else if (list.get(i).getFieldName2().equalsIgnoreCase("yes")) {
                    if (debarCount > 0) {
                        alreadyDebar.append(", ");
                    }
                    alreadyDebar.append(list.get(i).getFieldName1() + ": Debarred upto " + DateUtils.formatStdDate(DateUtils.convertStringtoDate(list.get(i).getFieldName3(), "MMM dd yyyy h:mma")));
                    debarCount++;
                }
            }
        }
        List<SPCommonSearchData> debarInfoList = debarmentSrBean.getDebarmentInfoByUser(debarUserId);
        JSONObject obj = new JSONObject();
        obj.put("ProcCat", noDebar.toString());
        obj.put("alreadyDebar", debarCount > 0 ? alreadyDebar.toString() : "N/A");
        if (debarInfoList.size() > 0) {
            obj.put("debarReason", debarInfoList.get(0).getFieldName6());
            obj.put("grounds", debarInfoList.get(0).getFieldName4());
            obj.put("debarFrom", DateUtils.formatStdDate(DateUtils.convertStringtoDate(debarInfoList.get(0).getFieldName2(), "MMM dd yyyy h:mma")));
            obj.put("debarTo", DateUtils.formatStdDate(DateUtils.convertStringtoDate(debarInfoList.get(0).getFieldName3(), "MMM dd yyyy h:mma")));
        }

        return obj.toString();
    }

    private String ReinstateDebarment(HttpServletRequest request) throws JSONException {
        String msg = "fail";
        String debaruserId = request.getParameter("userId");
        String category = request.getParameter("category");
        String reinstateComments = request.getParameter("grounds");
        
        List<SPCommonSearchData> userInfo = new ArrayList<SPCommonSearchData>();
        InitDebarmentSrBean debarmentSrBean = new InitDebarmentSrBean();
        userInfo = debarmentSrBean.getUserCompanyInfo(debaruserId);

        DebarredBiddingPermissionService debar = (DebarredBiddingPermissionService) AppContext.getSpringBean("DebarredBiddingPermissionService");
        BiddingPermissionService nonDebar = (BiddingPermissionService) AppContext.getSpringBean("BiddingPermissionService");
        List<TblDebarredBiddingPermission> debarBidPermissions = new ArrayList<TblDebarredBiddingPermission>();
        List<TblBiddingPermission> bidPermissions = new ArrayList<TblBiddingPermission>();
        try {
            debarBidPermissions = debar.findTblDebarredBiddingPermission("userId", Operation_enum.EQ, Integer.parseInt(userInfo.get(0).getFieldName1()), "companyId", Operation_enum.EQ, Integer.parseInt(userInfo.get(0).getFieldName2()), "procurementCategory", Operation_enum.EQ, category, "isActive", Operation_enum.EQ, 1);

            for (int i = 0; i < debarBidPermissions.size(); i++) {
                nonDebar.addTblBiddingPermission(new TblBiddingPermission(Long.parseLong("0"), debarBidPermissions.get(i).getProcurementCategory(),
                        debarBidPermissions.get(i).getWorkType(), debarBidPermissions.get(i).getWorkCategroy(), debarBidPermissions.get(i).getCompanyId(),
                        debarBidPermissions.get(i).getUserId(), debarBidPermissions.get(i).getIsReapplied()), Integer.parseInt(logUSerId));
            }
            if(debar.deleteDebarredBiddingPermission(Integer.parseInt(userInfo.get(0).getFieldName1()), Integer.parseInt(userInfo.get(0).getFieldName2()), Integer.parseInt(logUSerId), reinstateComments, category))
            {
                msg = "success";
                MailContentUtility mailContentUtility = new MailContentUtility();
                String mails[] = {userInfo.get(0).getFieldName3()};
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                sendMessageUtil.setEmailTo(mails);
                sendMessageUtil.setEmailSub("e-GP: Debarment.");
                String mailText = mailContentUtility.mailForNotifyDebarmentReinstate(category+ "&nbsp;&nbsp;<br/>");
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                mails = null;
                sendMessageUtil = null;
            }
        } catch (Exception ex) {
            System.out.print(ex);
        }
        return msg;
    }
    private String getDebarDataListing(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        /* 2. individual / company - searchFor
            3. companyname or firstname - cmpName or firName
            4. lastname - lastName
            5. 3 equals - like or firNameSearch
            6. 4 equals - lasNameSearch
            7. debarstartdate - dtFrom
            8. debarenddate - dtTo
            9.country - cntry
            10. State - state */
        try {

            String searchFor = request.getParameter("searchFor");
            String cmpfirName = request.getParameter("cmpName");
            String debarstartdate = request.getParameter("dtFrom");
            String debarenddate = request.getParameter("dtTo");

            String strPageNo = request.getParameter("pageNo");
            int pageNo = Integer.parseInt(strPageNo);

            String strOffset = request.getParameter("size");
            int recordOffset = Integer.parseInt(strOffset);

            CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            List<SPCommonSearchDataMore> list = dataMoreService.geteGPDataMore("getDebarmentList", searchFor, cmpfirName, debarstartdate, debarenddate, strPageNo, strOffset);
            String styleClass = "";
            String view, update, reinstate;
            if (list != null && !list.isEmpty()) {
                for (int i = 0; i < list.size(); i++) {
                    view = "<a href='DebarmentAction.jsp?userId="+list.get(i).getFieldName2()+"&action=view&category="+list.get(i).getFieldName1()+"'>View</a>";
                    update = "<a href='DebarmentAction.jsp?userId="+list.get(i).getFieldName2()+"&action=update&category="+list.get(i).getFieldName1()+"'>Update</a>";
                    reinstate = "<a href='DebarmentAction.jsp?userId="+list.get(i).getFieldName2()+"&action=reinstate&category="+list.get(i).getFieldName1()+"'>Reinstate</a>";
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    sb.append("<tr class='" + styleClass + "'>");
                    sb.append("<td class=\"t-align-center\">" + (((pageNo - 1) * recordOffset) + (i + 1)) + "</td>");
                    sb.append("<td class=\"t-align-left\">" + list.get(i).getFieldName3() + "</td>");
//                    sb.append("<td class=\"t-align-left\">" + list.get(i).getFieldName8() + "</td>");
                    sb.append("<td class=\"t-align-left\">" + list.get(i).getFieldName1() + "</td>");
                    sb.append("<td class=\"t-align-center\">" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(list.get(i).getFieldName5(), "MMM dd yyyy h:mma")) + "</td>");
                    sb.append("<td class=\"t-align-center\">" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(list.get(i).getFieldName4(), "MMM dd yyyy h:mma")) + "</td>");
                    sb.append("<td class=\"t-align-left\">" + list.get(i).getFieldName7() + "</td>");
                    sb.append("<td class=\"t-align-center\">" + view +" | "+ update +" | "+ reinstate + "</td>");
                    sb.append("</tr>");
                }
                int totalPages = 1;

                if (list.size() > 0) {
                    int count = Integer.parseInt(list.get(0).getFieldName9());
                    totalPages = (int) Math.ceil(Math.ceil(count) / recordOffset);
                    if (totalPages == 0) {
                        totalPages = 1;
                    }
                }
                sb.append("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

            } else {
                sb.append("<tr>");
                sb.append("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"7\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                sb.append("</tr>");
            }

        } catch (Exception e) {
            LOGGER.error(request.getParameter("action"), e);
        }
        return sb.toString();
    }
    
    private String UpdateDebarment(HttpServletRequest request) throws JSONException {
        String msg = "fail";
        String debaruserId = request.getParameter("userId");
        String category = request.getParameter("category");
        String emailId = request.getParameter("emailId");

        String debarDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date()).toString();
        DebarredBiddingPermissionService debar = (DebarredBiddingPermissionService) AppContext.getSpringBean("DebarredBiddingPermissionService");
        Date fDate = DateUtils.formatStdString(request.getParameter("textfield_2"));
        Date uptoDate = DateUtils.formatStdString(request.getParameter("textfield_1"));
        String fromDate = DateUtils.convertDatetoString(fDate, "yyyy-MM-dd HH:mm:ss");
        String toDate = DateUtils.convertDatetoString(uptoDate, "yyyy-MM-dd HH:mm:ss");
        if(debar.UpdateDebarredBiddingPermission(debaruserId, category, fromDate, toDate)){
            msg = "success";
            String details = "";
            details += category + ": From " + request.getParameter("textfield_2") + " to " + request.getParameter("textfield_1") + "&nbsp;&nbsp;<br/>";
            MailContentUtility mailContentUtility = new MailContentUtility();
            String mails[] = {emailId};
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailTo(mails);
            sendMessageUtil.setEmailSub("e-GP: Debarment.");
            String mailText = mailContentUtility.mailForNotifyDebarmentUpdate(details);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            mails = null;
            sendMessageUtil = null;
        }
        
        return "userId="+debaruserId+"&action=update&category="+category+"&flag="+msg;
    }
}
