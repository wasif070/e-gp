/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

/**
 *
 * @author TaherT
 */
public class ReportGenerateDtBean {

    private String bidderId;
    private String[] companyName;
    private String[] companyNameColId;
    //private String[] autoNumber;
    private String[] autoNumberColId;
    private String[] autoNumber;
    private String govColumn;
    private String govColumnColId;
    private String[] autoColumn;    
    private String[] autoColumnColId;
    private String[] estCost;
    private String[] estCostId;

    public String[] getAutoColumnColId() {
        return autoColumnColId;
    }

    public void setAutoColumnColId(String[] autoColumnColId) {
        this.autoColumnColId = autoColumnColId;
    }

    public String[] getAutoNumberColId() {
        return autoNumberColId;
    }

    public void setAutoNumberColId(String[] autoNumberColId) {
        this.autoNumberColId = autoNumberColId;
    }

    public String[] getCompanyNameColId() {
        return companyNameColId;
    }

    public void setCompanyNameColId(String[] companyNameColId) {
        this.companyNameColId = companyNameColId;
    }

    public String getGovColumnColId() {
        return govColumnColId;
    }

    public void setGovColumnColId(String govColumnColId) {
        this.govColumnColId = govColumnColId;
    }

    /*public String[] getAutoNumber() {
        return autoNumber;
    }

    public void setAutoNumber(String[] autoNumber) {
        this.autoNumber = autoNumber;
    }*/

    public String getBidderId() {
        return bidderId;
    }

    public void setBidderId(String bidderId) {
        this.bidderId = bidderId;
    }

    public String[] getAutoColumn() {
        return autoColumn;
    }

    public void setAutoColumn(String[] autoColumn) {
        this.autoColumn = autoColumn;
    }

    public String[] getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String[] companyName) {
        this.companyName = companyName;
    }

    public String getGovColumn() {
        return govColumn;
    }

    public void setGovColumn(String govColumn) {
        this.govColumn = govColumn;
    }

    public String[] getEstCost() {
        return estCost;
    }

    public void setEstCost(String[] estCost) {
        this.estCost = estCost;
    }

    public String[] getEstCostId() {
        return estCostId;
    }

    public void setEstCostId(String[] estCostId) {
        this.estCostId = estCostId;
    }

    public String[] getAutoNumber() {
        return autoNumber;
    }

    public void setAutoNumber(String[] autoNumber) {
        this.autoNumber = autoNumber;
    }    

    public ReportGenerateDtBean(String bidderId, String[] companyName, String[] companyNameColId, String[] autoNumberColId, String govColumn, String govColumnColId, String[] autoColumn, String[] autoColumnColId, String[] estCost, String[] estCostId) {
        this.bidderId = bidderId;
        this.companyName = companyName;
        this.companyNameColId = companyNameColId;
        this.autoNumberColId = autoNumberColId;
        this.govColumn = govColumn;
        this.govColumnColId = govColumnColId;
        this.autoColumn = autoColumn;
        this.estCost = estCost;
        this.estCostId = estCostId;
        this.autoColumnColId = autoColumnColId;
    }


    /*public ReportGenerateDtBean(String bidderId, String[] companyName, String[] companyNameColId, String[] autoNumberColId, String govColumn, String govColumnColId, String[] autoColumn, String[] autoColumnColId) {
        this.bidderId = bidderId;
        this.companyName = companyName;
        this.companyNameColId = companyNameColId;        
        this.autoNumberColId = autoNumberColId;
        this.govColumn = govColumn;
        this.govColumnColId = govColumnColId;
        this.autoColumn = autoColumn;
        this.autoColumnColId = autoColumnColId;
    }*/

    public ReportGenerateDtBean() {
    }
    
}
