/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.web.servicebean.PEOfficeCreationSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */

//Search Formation changed by Emtaz on 15/May/2016

public class EditPEOfficeServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        final Logger logger = Logger.getLogger(EditPEOfficeServlet.class);
        PEOfficeCreationSrBean peOfficeCreationSrBean = new PEOfficeCreationSrBean();
        //response.setContentType("text/xml;charset=UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        //HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        String logUserId = "0";
        if (request.getSession().getAttribute("userId") != null) {
                   logUserId = request.getSession().getAttribute("userId").toString();
                   peOfficeCreationSrBean.setLogUserId(logUserId);
           }
        try {
         // <editor-fold>  
            /* if (request.getParameter("action").equals("fetchData")) {
                logger.debug(request.getParameter("action") + " : " + logUserId + " Starts");
                response.setContentType("text/xml;charset=UTF-8");
                
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                String deptType = request.getParameter("deptType");
                int userTypeId = Integer.parseInt(request.getParameter("userTypeId"));
                int userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
                peOfficeCreationSrBean.setSearch(_search);
                peOfficeCreationSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));

                peOfficeCreationSrBean.setOffset(offset);
                peOfficeCreationSrBean.setSortOrder(sord);
                peOfficeCreationSrBean.setSortCol(sidx);


                PrintWriter out = response.getWriter();
                String searchField = "", searchString = "", searchOper = "";
                if (userTypeId == 5) {
                    List<Object[]> tblOfficeMasterListOrg = null;
                    if (_search) {
                        searchField = request.getParameter("searchField");
                        searchString = request.getParameter("searchString");
                        searchOper = request.getParameter("searchOper");
                        tblOfficeMasterListOrg = peOfficeCreationSrBean.getTblOfficeMasterListForOrg_Search(deptType, userId, searchField, searchString, searchOper);
                    } else {
                        String ascClause = null;
                        if (sidx.equalsIgnoreCase("")) {
                            ascClause = "creationDate desc";
                        } else {
                            ascClause = sidx + " " + sord;
                        }
                        tblOfficeMasterListOrg = peOfficeCreationSrBean.getTblOfficeMasterListForOrg(deptType, userId,ascClause);
                    }

                    int totalPages = 0;

                    int totalCount = 0;
                    if (_search) {
                        totalCount = (int) peOfficeCreationSrBean.getSearchCountOfPEMasterOrg(deptType, userId, searchField, searchString, searchOper);
                    } else {
                        totalCount = (int) peOfficeCreationSrBean.getAllCountOfPEMasterOrg(deptType, userId);
                    }

                    if (totalCount > 0) {
                        if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                            totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                        } else {
                            totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                        }

                    } else {
                        totalPages = 0;
                    }

                    out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                    out.print("<rows>");
                    out.print("<page>" + request.getParameter("page") + "</page>");

                    out.print("<total>" + totalPages + "</total>");
                    out.print("<records>" + tblOfficeMasterListOrg.size() + "</records>");
                    int j = 0;
                    int no = Integer.parseInt(request.getParameter("page"));
                    if (no == 1) {
                        j = 1;
                    } else {
                        if (Integer.parseInt(rows) == 30) {
                            j = ((no - 1) * 30) + 1;
                        } else if (Integer.parseInt(rows) == 20) {
                            j = ((no - 1) * 20) + 1;
                        } else {
                            j = ((no - 1) * 10) + 1;
                        }
                    }
                    // be sure to put text data in CDATA
                    for (int i = 0; i < tblOfficeMasterListOrg.size(); i++) {
                        out.print("<row id='" + tblOfficeMasterListOrg.get(i)[0] + "'>");
                        out.print("<cell>" + j + "</cell>");
                        out.print("<cell><![CDATA[" + tblOfficeMasterListOrg.get(i)[4] + "]]></cell>");
                        out.print("<cell><![CDATA[" + tblOfficeMasterListOrg.get(i)[1] + "]]></cell>");
                        out.print("<cell><![CDATA[" + tblOfficeMasterListOrg.get(i)[3] + "]]></cell>");
                        String link = "<a href=\"EditPEOffice.jsp?officeid=" + tblOfficeMasterListOrg.get(i)[0] + "&deptType=" + deptType + "\">Edit</a> | <a href=\"PEOfficeView.jsp?officeid=" + tblOfficeMasterListOrg.get(i)[0] + "&departmentType=" + deptType + "&action=view&msg=\">View</a>";
                        out.print("<cell><![CDATA[" + link + "]]></cell>");
                        out.print("</row>");
                        j++;
                    }
                    out.print("</rows>");

                } else {
                    List<Object[]> tblOfficeMasterList = null;
                    if (_search) {
                        searchField = request.getParameter("searchField");
                        searchString = request.getParameter("searchString");
                        searchOper = request.getParameter("searchOper");
                        tblOfficeMasterList = peOfficeCreationSrBean.getTblOfficeMasterList(deptType, searchField, searchString, searchOper);
                    } else {
                        String ascClause = null;
                        if (sidx.equalsIgnoreCase("")) {
                            ascClause = "creationDate desc";
                        } else {
                            ascClause = sidx + " " + sord;
                        }
                        tblOfficeMasterList = peOfficeCreationSrBean.getTblOfficeMasterList(deptType, ascClause);
                    }

                    int totalPages = 0;
                    int totalCount = 0;
                    if (_search) {
                        totalCount = (int) peOfficeCreationSrBean.getSearchCountOfPEMaster(deptType, searchField, searchString, searchOper);
                    } else {
                        totalCount = (int) peOfficeCreationSrBean.getAllCountOfPEMaster(deptType);
                    }

                    if (totalCount > 0) {
                        if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                            totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                        } else {
                            totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                        }

                    } else {
                        totalPages = 0;
                    }

                    out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                    out.print("<rows>");
                    out.print("<page>" + request.getParameter("page") + "</page>");

                    out.print("<total>" + totalPages + "</total>");
                    out.print("<records>" + tblOfficeMasterList.size() + "</records>");
                    int j = 0;
                    int no = Integer.parseInt(request.getParameter("page"));
                    if (no == 1) {
                        j = 1;
                    } else {
                        if (Integer.parseInt(rows) == 30) {
                            j = ((no - 1) * 30) + 1;
                        } else if (Integer.parseInt(rows) == 20) {
                            j = ((no - 1) * 20) + 1;
                        } else {
                            j = ((no - 1) * 10) + 1;
                        }
                    }
                    // be sure to put text data in CDATA
                    for (int i = 0; i < tblOfficeMasterList.size(); i++) {
                        out.print("<row id='" + tblOfficeMasterList.get(i)[0] + "'>");
                        out.print("<cell>" + j + "</cell>");
                        out.print("<cell><![CDATA[" + tblOfficeMasterList.get(i)[4] + "]]></cell>");
                        out.print("<cell><![CDATA[" + tblOfficeMasterList.get(i)[1] + "]]></cell>");
                        out.print("<cell><![CDATA[" + tblOfficeMasterList.get(i)[3] + "]]></cell>");
                        String link = "<a href=\"EditPEOffice.jsp?officeid=" + tblOfficeMasterList.get(i)[0] + "&deptType=" + deptType + "\">Edit</a> | <a href=\"PEOfficeView.jsp?officeid=" + tblOfficeMasterList.get(i)[0] + "&departmentType=" + deptType + "&action=view&msg=\">View</a>";
                        out.print("<cell><![CDATA[" + link + "]]></cell>");
                        out.print("</row>");
                        j++;
                    }
                    out.print("</rows>");
                }
                logger.debug(request.getParameter("action") + " : " + logUserId + " Ends");
            } */
            // </editor-fold>
            if (request.getParameter("action").equals("fetchData")) {
                logger.debug(request.getParameter("action") + " : " + logUserId + " Starts");
                
                int pageNo = Integer.parseInt(request.getParameter("pageNo"));
                int Size = Integer.parseInt(request.getParameter("size"));
                String deptType = request.getParameter("deptType");
                int userTypeId = Integer.parseInt(request.getParameter("userTypeId"));
                int userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
               
                peOfficeCreationSrBean.setLimit(1000000);
                int offset = 0;

                peOfficeCreationSrBean.setOffset(offset);
                
                String DeptName = request.getParameter("DeptName");
                String OfficeName = request.getParameter("OfficeName");
                String District = request.getParameter("District");   
                
                if (userTypeId == 5) {
                    List<Object[]> tblOfficeMasterListOrg = null;
                    List<Object[]> tblOfficeMasterListOrgSearched = new ArrayList<Object[]>();
                    
                    tblOfficeMasterListOrg = peOfficeCreationSrBean.getTblOfficeMasterListForOrg(deptType, userId, "creationDate desc");
           
                    for(int j=0;j<tblOfficeMasterListOrg.size();j++)
                    {
                        boolean ToAdd = true;
                        if(DeptName!=null && !DeptName.equalsIgnoreCase(""))
                        {
                            if(!DeptName.equalsIgnoreCase(tblOfficeMasterListOrg.get(j)[4].toString()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(OfficeName!=null && !OfficeName.equalsIgnoreCase(""))
                        {
                            if(!tblOfficeMasterListOrg.get(j)[1].toString().toLowerCase().contains(OfficeName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(District!=null && !District.equalsIgnoreCase(""))
                        {
                            if(!District.equalsIgnoreCase(tblOfficeMasterListOrg.get(j)[3].toString()))
                            {
                                ToAdd = false;
                            }
                        }
                       
                        if(ToAdd)
                        {
                            //SPCommonSearchData commonAppData = getGovData.get(j);
                            tblOfficeMasterListOrgSearched.add(tblOfficeMasterListOrg.get(j));
                        }
                    }
                    
                    int RecordFrom = (pageNo-1)*Size;
                    int k= 0;
                    String styleClass = "";
                    if (tblOfficeMasterListOrgSearched != null && !tblOfficeMasterListOrgSearched.isEmpty()) {
                        for(k=RecordFrom;k<RecordFrom+Size && k<tblOfficeMasterListOrgSearched.size();k++)
                        {
                            if(k%2==0){
                                styleClass = "bgColor-white";
                            }else{
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='"+styleClass+"'>");
                            out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + tblOfficeMasterListOrgSearched.get(k)[4].toString() + "</td>");
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + tblOfficeMasterListOrgSearched.get(k)[1].toString() + "</td>");
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + tblOfficeMasterListOrgSearched.get(k)[3].toString() + "</td>");
                            String link = "<a href=\"EditPEOffice.jsp?officeid=" + tblOfficeMasterListOrgSearched.get(k)[0].toString() + "&deptType=" + deptType + "\">Edit</a> | <a href=\"PEOfficeView.jsp?officeid=" + tblOfficeMasterListOrgSearched.get(k)[0].toString() + "&departmentType=" + deptType + "&action=view&msg=\">View</a>";
                            out.print("<td width=\"20%\" class=\"t-align-center\">" + link + "</td>");
                            out.print("</tr>");
                        }

                    }
                    else
                    {
                        out.print("<tr>");
                        out.print("<td colspan=\"5\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }  
                    int totalPages = 1;
                    if (tblOfficeMasterListOrgSearched.size() > 0) {
                        totalPages = (int) (Math.ceil(Math.ceil(tblOfficeMasterListOrgSearched.size()) / Size));
                        System.out.print("totalPages--"+totalPages+"records "+ tblOfficeMasterListOrgSearched.size());
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    
                    
                } else {
                    List<Object[]> tblOfficeMasterList = null;     
                    List<Object[]> tblOfficeMasterListSearched = new ArrayList<Object[]>();     
                    tblOfficeMasterList = peOfficeCreationSrBean.getTblOfficeMasterList(deptType, "creationDate desc");
                   
                    for(int j=0;j<tblOfficeMasterList.size();j++)
                    {
                        boolean ToAdd = true;
                        if(DeptName!=null && !DeptName.equalsIgnoreCase(""))
                        {
                            if(!DeptName.equalsIgnoreCase(tblOfficeMasterList.get(j)[4].toString()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(OfficeName!=null && !OfficeName.equalsIgnoreCase(""))
                        {
                            if(!tblOfficeMasterList.get(j)[1].toString().toLowerCase().contains(OfficeName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(District!=null && !District.equalsIgnoreCase(""))
                        {
                            if(!District.equalsIgnoreCase(tblOfficeMasterList.get(j)[3].toString()))
                            {
                                ToAdd = false;
                            }
                        }
                       
                        if(ToAdd)
                        {
                            //SPCommonSearchData commonAppData = getGovData.get(j);
                            tblOfficeMasterListSearched.add(tblOfficeMasterList.get(j));
                        }
                    }
                    
                    int RecordFrom = (pageNo-1)*Size;
                    int k= 0;
                    String styleClass = "";
                    if (tblOfficeMasterListSearched != null && !tblOfficeMasterListSearched.isEmpty()) {
                        for(k=RecordFrom;k<RecordFrom+Size && k<tblOfficeMasterListSearched.size();k++)
                        {
                            if(k%2==0){
                                styleClass = "bgColor-white";
                            }else{
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='"+styleClass+"'>");
                            out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + tblOfficeMasterListSearched.get(k)[4].toString() + "</td>");
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + tblOfficeMasterListSearched.get(k)[1].toString() + "</td>");
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + tblOfficeMasterListSearched.get(k)[3].toString() + "</td>");
                            String link = "<a href=\"EditPEOffice.jsp?officeid=" + tblOfficeMasterListSearched.get(k)[0].toString() + "&deptType=" + deptType + "\">Edit</a> | <a href=\"PEOfficeView.jsp?officeid=" + tblOfficeMasterListSearched.get(k)[0].toString() + "&departmentType=" + deptType + "&action=view&msg=\">View</a>";
                            out.print("<td width=\"20%\" class=\"t-align-center\">" + link + "</td>");
                            out.print("</tr>");
                        }

                    }
                    else
                    {
                        out.print("<tr>");
                        out.print("<td colspan=\"5\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }  
                    int totalPages = 1;
                    if (tblOfficeMasterListSearched.size() > 0) {
                        totalPages = (int) (Math.ceil(Math.ceil(tblOfficeMasterListSearched.size()) / Size));
                        System.out.print("totalPages--"+totalPages+"records "+ tblOfficeMasterListSearched.size());
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    
                }
                logger.debug(request.getParameter("action") + " : " + logUserId + " Ends");
            }

        } catch (Exception e) {
            logger.error(request.getParameter("action") + " : " + logUserId + e);
        }
        
         finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            
        }
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
