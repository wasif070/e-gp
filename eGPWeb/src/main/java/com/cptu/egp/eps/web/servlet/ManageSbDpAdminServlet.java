/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Search Formation changed by Emtaz on 16/May/2016

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.view.VwGetSbDevPartner;
import com.cptu.egp.eps.web.servicebean.ManageSbDpAdminSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ManageSbDpAdminServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        final Logger logger = Logger.getLogger(ManageSbDpAdminServlet.class);
        String logUserId = "0";
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String userTypeSessionId =  request.getSession().getAttribute("userTypeId").toString();
        try {
            //<editor-fold>
            /*if (request.getParameter("action").equals("fetchData")) {
                logger.debug(request.getParameter("action") + " : " + logUserId + " Starts");
                ManageSbDpAdminSrBean manageSbDpAdminSrBean = new ManageSbDpAdminSrBean();
                if (request.getSession().getAttribute("userId") != null) {
                    logUserId = request.getSession().getAttribute("userId").toString();
                    manageSbDpAdminSrBean.setLogUserId(logUserId);
                }
                response.setContentType("text/xml;charset=UTF-8");
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                String type = request.getParameter("userType");
                String forBranchAdmin = "";
                String snUserType = "";
                boolean isBranch = false;
                int snUserId  = 0;
                if (request.getParameter("isBranch") != null) {
                forBranchAdmin = request.getParameter("isBranch");
                    HttpSession hs = request.getSession();
                snUserType = hs.getAttribute("userTypeId").toString();
                snUserId = Integer.parseInt(hs.getAttribute("userId").toString());
                }

//                if (sidx.equals("")) {
//                    sidx = "fullName";
//                }
                String userTypeId = "";
                if (type.equalsIgnoreCase("Development")) {
                    userTypeId = "6";
                } else {
                    userTypeId = "7";
                }
                //dcBean.setColName("");
                manageSbDpAdminSrBean.setSearch(_search);
                manageSbDpAdminSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));

                manageSbDpAdminSrBean.setOffset(offset);
                manageSbDpAdminSrBean.setSortOrder(sord);
                manageSbDpAdminSrBean.setSortCol(sidx);



                List<VwGetSbDevPartner> vwGetAdminList = null;
                List<Object[]> viewList = null;
                String searchField = "", searchString = "", searchOper = "";
                if (_search) {
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    if (forBranchAdmin.equalsIgnoreCase("branch")) {
                        if (snUserType.equalsIgnoreCase("7")) {
                            isBranch = true;
                            viewList = manageSbDpAdminSrBean.getUserListAdmin(type, snUserId, searchField, searchString, searchOper);
                        } else {
                            vwGetAdminList = manageSbDpAdminSrBean.getBranchAdminList(type, searchField, searchString, searchOper);
                        }
                    } else {
                        vwGetAdminList = manageSbDpAdminSrBean.getAdminList(type, searchField, searchString, searchOper);
                    }
                } else {
                    if (forBranchAdmin.equalsIgnoreCase("branch")) {
                        if (snUserType.equalsIgnoreCase("7")) {
                            isBranch = true;
                            viewList = manageSbDpAdminSrBean.getUserListAdmin(type, snUserId, searchField, searchString, searchOper);
                        } else {
                            vwGetAdminList = manageSbDpAdminSrBean.getBranchAdminList(type);
                        }
                    } else {
                        vwGetAdminList = manageSbDpAdminSrBean.getAdminList(type);
                    }
                }
                int totalPages = 0;
                long totalCount = 0;
                if (_search) {
                    if (forBranchAdmin.equalsIgnoreCase("branch")) {
                        if (snUserType.equalsIgnoreCase("7")) {
                            totalCount = manageSbDpAdminSrBean.getAllCountOfBranchAdmin(type, snUserId);
                        } else {
                            totalCount = manageSbDpAdminSrBean.getAllCountOfBranchDept(type, searchField, searchString, searchOper);
                        }
                    } else {
                        totalCount = manageSbDpAdminSrBean.getAllCountOfDept(type, searchField, searchString, searchOper);
                    }
                } else {
                    if (forBranchAdmin.equalsIgnoreCase("branch")) {
                        if (snUserType.equalsIgnoreCase("7")) {
                            totalCount = manageSbDpAdminSrBean.getAllCountOfBranchAdmin(type, snUserId);
                        } else {
                            totalCount = manageSbDpAdminSrBean.getAllCountOfBranchDept(type);
                        }
                    } else {
                        totalCount = manageSbDpAdminSrBean.getAllCountOfDept(type);
                    }
                }
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(request.getParameter("rows")));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(request.getParameter("rows"))) + 1);
                    }
                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");
                if (isBranch) {
                        out.print("<total>" + totalPages + "</total>");
                        out.print("<records>" + viewList.size() + "</records>");

                    int j = 0;
                        int no = Integer.parseInt(request.getParameter("page"));
                    if (no == 1) {
                            j = 1;
                    } else {
                        if (Integer.parseInt(rows) == 30) {
                            j = ((no - 1) * 30) + 1;
                        } else if (Integer.parseInt(rows) == 20) {
                            j = ((no - 1) * 20) + 1;
                        } else {
                            j = ((no - 1) * 10) + 1;
                            }
                            }
                    // be sure to put text data in CDATA
                    String editLink = null;
                    String viewLink = null;
                    String actDeactLink = null;
                    for (int i = 0; i < viewList.size(); i++) {
                        out.print("<row id='" + viewList.get(i)[2] + "'>");
                        out.print("<cell><![CDATA[" + j + "]]></cell>");
                        out.print("<cell><![CDATA[" + viewList.get(i)[0] + "]]></cell>");
                        out.print("<cell><![CDATA[" + viewList.get(i)[1] + "]]></cell>");
                        out.print("<cell><![CDATA[" + viewList.get(i)[3] + "]]></cell>");
                        editLink = "<a href=\"EditSbBranchAdmin.jsp?userId=" + viewList.get(i)[2] + "&partnerType=" + type + "&userTypeId=15&mode=edit\">Edit</a>";
                        viewLink = "<a href=\"ViewSbBranchAdmin.jsp?userId=" + viewList.get(i)[2] + "&partnerType=" + type + "&userTypeId=15&mode=view\">View</a>";
                        if ("approved".equalsIgnoreCase(viewList.get(i)[5].toString())) {
                            actDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=deactive&userId=" + viewList.get(i)[2] + "&partnerType=" + type + "&userTypeId=15\">Deactivate</a>";
                        } else {
                            actDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=active&userId=" + viewList.get(i)[2] + "&partnerType=" + type + "&userTypeId=15\">Activatate</a>";
                        }
                        if("1".equalsIgnoreCase(userTypeSessionId)){
                           out.print("<cell><![CDATA[" + editLink + " | " + viewLink + "|" + actDeactLink + " ]]></cell>");
                        }
                         else{
                            out.print("<cell><![CDATA[" + editLink + " | " + viewLink + " ]]></cell>");
                         }
                        
                        j++;
                        out.print("</row>");
                    }
                } else {
                        out.print("<total>" + totalPages + "</total>");
                        out.print("<records>" + vwGetAdminList.size() + "</records>");

                    int j = 0;
                    int no = Integer.parseInt(request.getParameter("page"));
                    if (no == 1) {
                            j = 1;
                    } else {
                        if (Integer.parseInt(rows) == 30) {
                            j = ((no - 1) * 30) + 1;
                        } else if (Integer.parseInt(rows) == 20) {
                            j = ((no - 1) * 20) + 1;
                        } else {
                            j = ((no - 1) * 10) + 1;
                            }
                            }
                    // be sure to put text data in CDATA
                    String editLink = null;
                    String viewLink = null;
                    String ActDeactLink = null;
                    for (int i = 0; i < vwGetAdminList.size(); i++) {
                        out.print("<row id='" + vwGetAdminList.get(i).getId().getUserId() + "'>");
                        out.print("<cell><![CDATA[" + j + "]]></cell>");
                        out.print("<cell><![CDATA[" + vwGetAdminList.get(i).getId().getFullName() + "]]></cell>");
                        out.print("<cell><![CDATA[" + vwGetAdminList.get(i).getId().getEmailId() + "]]></cell>");
                        out.print("<cell><![CDATA[" + vwGetAdminList.get(i).getId().getSbDevelopName() + "]]></cell>");
                        if (forBranchAdmin.equalsIgnoreCase("branch")) {
                            editLink = "<a href=\"EditSbBranchAdmin.jsp?userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId=15&mode=edit\">Edit</a>";
                            viewLink = "<a href=\"ViewSbBranchAdmin.jsp?userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId=15&mode=view\">View</a>";
                            if ("approved".equalsIgnoreCase(vwGetAdminList.get(i).getId().getStatus())) {
                                ActDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=deactive&userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId=15\">Deactivate</a>";
                            } else {
                                ActDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=active&userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId=15\">Activate</a>";
                        }
                        } else {
                            editLink = "<a href=\"EditSbDevPartAdmin.jsp?userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId="+userTypeId + "&mode=edit\">Edit</a>";
                            viewLink = "<a href=\"ViewSbDevPartAdmin.jsp?userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId="+userTypeId +"&mode=view\">View</a>";
                            if ("approved".equalsIgnoreCase(vwGetAdminList.get(i).getId().getStatus())) {
                                ActDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=deactive&userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType="+ type + "&userTypeId="+ userTypeId +"\">Deactivate</a>";
                            } else {
                                ActDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=active&userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId="+userTypeId +"\">Activate</a>";
                            }

                        }
                        if("1".equalsIgnoreCase(userTypeSessionId)){
                           out.print("<cell><![CDATA[" + editLink + " | " + viewLink + "|" + ActDeactLink + " ]]></cell>");
                        }
                         else{
                            out.print("<cell><![CDATA[" + editLink + " | " + viewLink + " ]]></cell>");
                         }
                        j++;
                        out.print("</row>");
                    }
                }
                out.print("</rows>");
                logger.debug(request.getParameter("action") + " : " + logUserId + " Ends");
            }*/
            //</editor-fold>
            
            if (request.getParameter("action").equals("fetchData")) {
                logger.debug(request.getParameter("action") + " : " + logUserId + " Starts");
                ManageSbDpAdminSrBean manageSbDpAdminSrBean = new ManageSbDpAdminSrBean();
                if (request.getSession().getAttribute("userId") != null) {
                    logUserId = request.getSession().getAttribute("userId").toString();
                    manageSbDpAdminSrBean.setLogUserId(logUserId);
                }
                
                String type = request.getParameter("userType");
                String forBranchAdmin = "";
                String snUserType = "";
                boolean isBranch = false;
                int snUserId  = 0;
                if (request.getParameter("isBranch") != null) {
                forBranchAdmin = request.getParameter("isBranch");
                    HttpSession hs = request.getSession();
                    snUserType = hs.getAttribute("userTypeId").toString();
                    snUserId = Integer.parseInt(hs.getAttribute("userId").toString());
                }

                String userTypeId = "";
                if (type.equalsIgnoreCase("Development")) {
                    userTypeId = "6";
                } else {
                    userTypeId = "7";
                }
                
                manageSbDpAdminSrBean.setLimit(1000000);
                int offset = 0;

                manageSbDpAdminSrBean.setOffset(offset);
                
                int pageNo = Integer.parseInt(request.getParameter("pageNo"));
                int Size = Integer.parseInt(request.getParameter("size"));
                String FullName = request.getParameter("FullName");
                String EmailID = request.getParameter("EmailID");
                String DpName = request.getParameter("DpName");   
              
                List<VwGetSbDevPartner> vwGetAdminList = null;
                List<Object[]> viewList = null;
                //String searchField = "", searchString = "", searchOper = "";
              
                if (forBranchAdmin.equalsIgnoreCase("branch")) 
                {
                    if (snUserType.equalsIgnoreCase("7"))
                    {
                        isBranch = true;
                        viewList = manageSbDpAdminSrBean.getUserListAdmin(type, snUserId, "", "", "");
                    } 
                    else 
                    {
                        vwGetAdminList = manageSbDpAdminSrBean.getBranchAdminList(type);
                    }
                } 
                else 
                {
                    vwGetAdminList = manageSbDpAdminSrBean.getAdminList(type);
                }
                
                if(isBranch) {
                    
                    List<Object[]> viewListSearched = new ArrayList<Object[]>();
                    for(int j=0;j<viewList.size();j++)
                    {
                        boolean ToAdd = true;
                        if(FullName!=null && !FullName.equalsIgnoreCase(""))
                        {
                            if(!viewList.get(j)[0].toString().toLowerCase().contains(FullName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(EmailID!=null && !EmailID.equalsIgnoreCase(""))
                        {
                            if(!viewList.get(j)[1].toString().toLowerCase().contains(EmailID.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(DpName!=null && !DpName.equalsIgnoreCase(""))
                        {
                            if(!viewList.get(j)[3].toString().toLowerCase().contains(DpName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                       
                        if(ToAdd)
                        {
                            //SPCommonSearchData commonAppData = getGovData.get(j);
                            viewListSearched.add(viewList.get(j));
                        }
                    }
                    
                    int RecordFrom = (pageNo-1)*Size;
                    int k= 0;
                    String styleClass = "";
                    if (viewListSearched != null && !viewListSearched.isEmpty()) {
                        for(k=RecordFrom;k<RecordFrom+Size && k<viewListSearched.size();k++)
                        {
                            if(k%2==0){
                                styleClass = "bgColor-white";
                            }else{
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='"+styleClass+"'>");
                            out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                            out.print("<td width=\"20%\" class=\"t-align-center\">" + viewListSearched.get(k)[0].toString() + "</td>");
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + viewListSearched.get(k)[1].toString() + "</td>");
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + viewListSearched.get(k)[3].toString() + "</td>");
                            String editLink = null;
                            String viewLink = null;
                            String actDeactLink = null;
                            editLink = "<a href=\"EditSbBranchAdmin.jsp?userId=" + viewListSearched.get(k)[2] + "&partnerType=" + type + "&userTypeId=15&mode=edit\">Edit</a>";
                            viewLink = "<a href=\"ViewSbBranchAdmin.jsp?userId=" + viewListSearched.get(k)[2] + "&partnerType=" + type + "&userTypeId=15&mode=view\">View</a>";
                            if ("approved".equalsIgnoreCase(viewListSearched.get(k)[5].toString())) {
                                actDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=deactive&userId=" + viewListSearched.get(k)[2] + "&partnerType=" + type + "&userTypeId=15\">Deactivate</a>";
                            } else {
                                actDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=active&userId=" + viewListSearched.get(k)[2] + "&partnerType=" + type + "&userTypeId=15\">Activatate</a>";
                            }
                            if("1".equalsIgnoreCase(userTypeSessionId)){
                               out.print("<td width=\"25%\" class=\"t-align-center\">" + editLink + " | " + viewLink + " | " + actDeactLink + "</td>");
                            }
                            else{
                               out.print("<td width=\"25%\" class=\"t-align-center\">" + editLink + " | " + viewLink + "</td>");
                            }  
                            out.print("</tr>");
                        }

                    }
                    else
                    {
                        out.print("<tr>");
                        out.print("<td colspan=\"5\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }  
                    int totalPages = 1;
                    if (viewListSearched.size() > 0) {
                        totalPages = (int) (Math.ceil(Math.ceil(viewListSearched.size()) / Size));
                        System.out.print("totalPages--"+totalPages+"records "+ viewListSearched.size());
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                   
                } else {
                      
                    List<VwGetSbDevPartner> vwGetAdminListSearched = new ArrayList<VwGetSbDevPartner>();
                    for(int j=0;j<vwGetAdminList.size();j++)
                    {
                        boolean ToAdd = true;
                        if(FullName!=null && !FullName.equalsIgnoreCase(""))
                        {
                            if(!vwGetAdminList.get(j).getId().getFullName().toLowerCase().contains(FullName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(EmailID!=null && !EmailID.equalsIgnoreCase(""))
                        {
                            if(!vwGetAdminList.get(j).getId().getEmailId().toLowerCase().contains(EmailID.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(DpName!=null && !DpName.equalsIgnoreCase(""))
                        {
                            if(!vwGetAdminList.get(j).getId().getSbDevelopName().toLowerCase().contains(DpName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                       
                        if(ToAdd)
                        {
                            //SPCommonSearchData commonAppData = getGovData.get(j);
                            vwGetAdminListSearched.add(vwGetAdminList.get(j));
                        }
                    }
                    
                    int RecordFrom = (pageNo-1)*Size;
                    int k= 0;
                    String styleClass = "";
                    if (vwGetAdminListSearched != null && !vwGetAdminListSearched.isEmpty()) {
                        for(k=RecordFrom;k<RecordFrom+Size && k<vwGetAdminListSearched.size();k++)
                        {
                            if(k%2==0){
                                styleClass = "bgColor-white";
                            }else{
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='"+styleClass+"'>");
                            out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                            out.print("<td width=\"20%\" class=\"t-align-center\">" + vwGetAdminListSearched.get(k).getId().getFullName() + "</td>");
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + vwGetAdminListSearched.get(k).getId().getEmailId() + "</td>");
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + vwGetAdminListSearched.get(k).getId().getSbDevelopName() + "</td>");
                            String editLink = null;
                            String viewLink = null;
                            String ActDeactLink = null;
                            if (forBranchAdmin.equalsIgnoreCase("branch")) {
                            editLink = "<a href=\"EditSbBranchAdmin.jsp?userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId=15&mode=edit\">Edit</a>";
                            viewLink = "<a href=\"ViewSbBranchAdmin.jsp?userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId=15&mode=view\">View</a>";
                            if ("approved".equalsIgnoreCase(vwGetAdminListSearched.get(k).getId().getStatus())) {
                                ActDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=deactive&userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId=15\">Deactivate</a>";
                            } else {
                                ActDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=active&userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId=15\">Activate</a>";
                            }
                            } else {
                                editLink = "<a href=\"EditSbDevPartAdmin.jsp?userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId="+userTypeId + "&mode=edit\">Edit</a>";
                                viewLink = "<a href=\"ViewSbDevPartAdmin.jsp?userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId="+userTypeId +"&mode=view\">View</a>";
                                if ("approved".equalsIgnoreCase(vwGetAdminListSearched.get(k).getId().getStatus())) {
                                    ActDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=deactive&userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType="+ type + "&userTypeId="+ userTypeId +"\">Deactivate</a>";
                                } else {
                                    ActDeactLink = "<a href=\"ActDeactAdminUser.jsp?status=active&userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId="+userTypeId +"\">Activate</a>";
                                }

                            }
                            if("1".equalsIgnoreCase(userTypeSessionId)){
                                out.print("<td width=\"25%\" class=\"t-align-center\">" + editLink + " | " + viewLink + " | " + ActDeactLink + "</td>");   
                            }
                             else{
                                out.print("<td width=\"25%\" class=\"t-align-center\">" + editLink + " | " + viewLink + "</td>");
                            }
                            out.print("</tr>");
                        }

                    }
                    else
                    {
                        out.print("<tr>");
                        out.print("<td colspan=\"5\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }  
                    int totalPages = 1;
                    if (vwGetAdminListSearched.size() > 0) {
                        totalPages = (int) (Math.ceil(Math.ceil(vwGetAdminListSearched.size()) / Size));
                        System.out.print("totalPages--"+totalPages+"records "+ vwGetAdminListSearched.size());
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                logger.debug(request.getParameter("action") + " : " + logUserId + " Ends");
                }
            }
        } catch (Exception ex) {
            logger.error(request.getParameter("action") + " : " + logUserId + ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
