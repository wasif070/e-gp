/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppPkgDetails;
import com.cptu.egp.eps.service.serviceimpl.APPReportService;
import com.cptu.egp.eps.web.databean.AppReportDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sanjay Prajapati
 */
public class AppReportSrBean {

    private List<AppReportDtBean> goodsDevList = new ArrayList<AppReportDtBean>();
    private List<AppReportDtBean> goodsRevList = new ArrayList<AppReportDtBean>();
    private List<AppReportDtBean> goodsOwnList = new ArrayList<AppReportDtBean>();
    private List<AppReportDtBean> serviceDevList = new ArrayList<AppReportDtBean>();
    private List<AppReportDtBean> serviceRevList = new ArrayList<AppReportDtBean>();
    private List<AppReportDtBean> serviceOwnList = new ArrayList<AppReportDtBean>();
    private List<AppReportDtBean> workDevList = new ArrayList<AppReportDtBean>();
    private List<AppReportDtBean> workRevList = new ArrayList<AppReportDtBean>();
    private List<AppReportDtBean> workOwnList = new ArrayList<AppReportDtBean>();
    private boolean goodsDev = false;
    private boolean goodsRev = false;
    private boolean goodsOwn = false;
    private boolean serviceDev = false;
    private boolean serviceRev = false;
    private boolean serviceOwn = false;
    private boolean workDev = false;
    private boolean workRev = false;
    private boolean workOwn = false;
    private boolean reportData = false;

    public boolean isReportData() {
        return reportData;
    }

    public void setReportData(boolean reportData) {
        this.reportData = reportData;
    }

    public List<AppReportDtBean> getGoodsDevList() {
        return goodsDevList;
    }

    public void setGoodsDevList(List<AppReportDtBean> goodsDevList) {
        this.goodsDevList = goodsDevList;
    }

    public List<AppReportDtBean> getGoodsOwnList() {
        return goodsOwnList;
    }

    public void setGoodsOwnList(List<AppReportDtBean> goodsOwnList) {
        this.goodsOwnList = goodsOwnList;
    }

    public List<AppReportDtBean> getGoodsRevList() {
        return goodsRevList;
    }

    public void setGoodsRevList(List<AppReportDtBean> goodsRevList) {
        this.goodsRevList = goodsRevList;
    }

    public List<AppReportDtBean> getServiceDevList() {
        return serviceDevList;
    }

    public void setServiceDevList(List<AppReportDtBean> serviceDevList) {
        this.serviceDevList = serviceDevList;
    }

    public List<AppReportDtBean> getServiceOwnList() {
        return serviceOwnList;
    }

    public void setServiceOwnList(List<AppReportDtBean> serviceOwnList) {
        this.serviceOwnList = serviceOwnList;
    }

    public List<AppReportDtBean> getServiceRevList() {
        return serviceRevList;
    }

    public void setServiceRevList(List<AppReportDtBean> serviceRevList) {
        this.serviceRevList = serviceRevList;
    }

    public List<AppReportDtBean> getWorkDevList() {
        return workDevList;
    }

    public void setWorkDevList(List<AppReportDtBean> workDevList) {
        this.workDevList = workDevList;
    }

    public List<AppReportDtBean> getWorkOwnList() {
        return workOwnList;
    }

    public void setWorkOwnList(List<AppReportDtBean> workOwnList) {
        this.workOwnList = workOwnList;
    }

    public APPReportService getAppReportService() {
        return appReportService;
    }

    public void setAppReportService(APPReportService appReportService) {
        this.appReportService = appReportService;
    }

    public boolean isGoodsDev() {
        return goodsDev;
    }

    public void setGoodsDev(boolean goodsDev) {
        this.goodsDev = goodsDev;
    }

    public boolean isGoodsOwn() {
        return goodsOwn;
    }

    public void setGoodsOwn(boolean goodsOwn) {
        this.goodsOwn = goodsOwn;
    }

    public boolean isGoodsRev() {
        return goodsRev;
    }

    public void setGoodsRev(boolean goodsRev) {
        this.goodsRev = goodsRev;
    }

    public boolean isServiceDev() {
        return serviceDev;
    }

    public void setServiceDev(boolean serviceDev) {
        this.serviceDev = serviceDev;
    }

    public boolean isServiceOwn() {
        return serviceOwn;
    }

    public void setServiceOwn(boolean serviceOwn) {
        this.serviceOwn = serviceOwn;
    }

    public boolean isServiceRev() {
        return serviceRev;
    }

    public void setServiceRev(boolean serviceRev) {
        this.serviceRev = serviceRev;
    }

    public boolean isWorkDev() {
        return workDev;
    }

    public void setWorkDev(boolean workDev) {
        this.workDev = workDev;
    }

    public boolean isWorkOwn() {
        return workOwn;
    }

    public void setWorkOwn(boolean workOwn) {
        this.workOwn = workOwn;
    }

    public boolean isWorkRev() {
        return workRev;
    }

    public void setWorkRev(boolean workRev) {
        this.workRev = workRev;
    }

    public List<AppReportDtBean> getWorkRevList() {
        return workRevList;
    }

    public void setWorkRevList(List<AppReportDtBean> workRevList) {
        this.workRevList = workRevList;
    }
    private APPReportService appReportService = (APPReportService) AppContext.getSpringBean("APPReportService");
    private static final Logger LOGGER = Logger.getLogger(AppReportSrBean.class);
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private String logUserId = "0";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        appReportService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * Get All APP List.
     * @param deptId
     * @param peOfficeId
     * @param financialYear
     * @param budTypeId
     * @param prjId
     * @param procNature
     * @param appId
     * @return
     */
    public boolean getAllList(int deptId, int peOfficeId, String financialYear, int budTypeId, int prjId, String procNature, int appId) {
        LOGGER.debug("getAllList : " + logUserId + LOGGERSTART);
        boolean retVal = false;
        short totDays = 0;
        AppReportDtBean appReportDt;
        
        NumberFormat formatter = new DecimalFormat();
        formatter = new DecimalFormat("0.######E0");
        
        try {
            for (CommonAppPkgDetails list : appReportService.getAppDetails("searchData", deptId, peOfficeId, financialYear.trim(), budTypeId, prjId, procNature, 0, appId)) {
                if ("Goods".equalsIgnoreCase(list.getProcurementNature()) && "Development".equalsIgnoreCase(list.getBudgetType())) {
                        totDays = 0;

                        appReportDt = new AppReportDtBean();
                        appReportDt.setAppId(list.getAppId());
                        appReportDt.setProjectNameCode(list.getProjectNameCode());
                        
                        appReportDt.setMinistryDetails(list.getMinistryDetails());
                        appReportDt.setPackageId(list.getPackageId());
                        appReportDt.setPackageNo(list.getPackageNo());
                        appReportDt.setProcMethod(list.getProcurementMethod());
                        appReportDt.setProcType(list.getProcurementType());
                        appReportDt.setAppAut(list.getProcurementRole());
                        appReportDt.setSrcFund(list.getSourceOfFund());
                        appReportDt.setEstCost(list.getEstimatedCost());
                        appReportDt.setFormattedEstCost(formatter.format(list.getEstimatedCost().longValue() ));
                        
                        appReportDt.setgApplstdt(list.getAppLstDt());
                        appReportDt.setgAdvtDt(list.getAdvtDt());
                        appReportDt.setgAdvtDy(list.getAdvtDays());
                        appReportDt.setIsPQRequired(list.getIsPQRequired());
                        appReportDt.setgPlanDtInAdvtDt(list.getTenderAdvertDt());
                        appReportDt.setgPlanDyInAdvtDy(list.getTenderAdvertDays());
                        appReportDt.setgActDtInAdvtDt(list.getActAdvtDt());

                        totDays += list.getTenderAdvertDays();

                        appReportDt.setgPlanDtTenderOpenDt(list.getTenderOpenDt());
                        appReportDt.setgPlanDyTenderOpenDy(list.getTenderOpenDays());
                        appReportDt.setgActDtTenderOpenDt(list.getActTenderOpenDt());

                        totDays += list.getTenderOpenDays();

                        appReportDt.setgPlanDtTenderEvalDt(list.getTenderEvalRptDt());
                        appReportDt.setgPlanDyTenderEvalDy(list.getTenderEvalRptdays());
                        appReportDt.setgActDtTenderEvalDt(list.getActTenderEvalRptDt());

                        totDays += list.getTenderEvalRptdays();

                        appReportDt.setgPlanDtAppAwardDt(list.getTenderContractAppDt());
                        appReportDt.setgPlanDyAppAwardDy(list.getTenderContractAppDays());
                        appReportDt.setgActDtAppAwardDt(list.getActTenderContractAppDt());

                        totDays += list.getTenderContractAppDays();
                        //Code by Proshanto Kumar Saha
                        appReportDt.setgPlanDtLintAwardDt(list.getTenderLetterIntentDt());
                        appReportDt.setgPlanDyLintAwardDy(list.getTnderLetterIntentDays());
                        appReportDt.setgActDtLintAwardDt(list.getActTenderLetterIntentDt());
                        //appReportDt.setgActDtNtiAwardDt(list.getActTenderNoaIssueDt());
                        

                        totDays += list.getTnderLetterIntentDays();
                        //Code end Proshanto
                        appReportDt.setgPlanDtNtiAwardDt(list.getTenderNoaIssueDt());
                        appReportDt.setgPlanDyNtiAwardDy(list.getTenderNoaIssueDays());
                        appReportDt.setgActDtNtiAwardDt(list.getActTenderNoaIssueDt());

                        totDays += list.getTenderNoaIssueDays();

                        appReportDt.setgPlanDtSignCtcDt(list.getTenderContractSignDt());
                        appReportDt.setgPlanDySignCtcDy(list.getTenderContractSignDays());
                        appReportDt.setgActDtSignCtcDt(list.getActTenderContractSignDt());

                        totDays += list.getTenderContractSignDays();

                        appReportDt.setgPlanDyTotTimeSignContractDy(totDays);
                        appReportDt.setgPlanDtTotTimeComContractDt(list.getTenderContractCompDt());
                        appReportDt.setgPlanDyTotTimeComContractDy(Short.valueOf("0"));

                        this.getGoodsDevList().add(appReportDt);
                        appReportDt = null;
                        retVal = true;
                }
                if ("Goods".equalsIgnoreCase(list.getProcurementNature()) && "Revenue".equalsIgnoreCase(list.getBudgetType())) {
                        totDays = 0;

                        appReportDt = new AppReportDtBean();
                        appReportDt.setAppId(list.getAppId());
                        appReportDt.setProjectNameCode(list.getProjectNameCode());
                        appReportDt.setMinistryDetails(list.getMinistryDetails());
                        appReportDt.setPackageId(list.getPackageId());
                        appReportDt.setPackageNo(list.getPackageNo());
                        appReportDt.setProcMethod(list.getProcurementMethod());
                        appReportDt.setProcType(list.getProcurementType());
                        appReportDt.setAppAut(list.getProcurementRole());
                        appReportDt.setSrcFund(list.getSourceOfFund());
                        appReportDt.setEstCost(list.getEstimatedCost());
                        appReportDt.setFormattedEstCost(formatter.format(list.getEstimatedCost().longValue() ));
                        appReportDt.setgApplstdt(list.getAppLstDt());
                        appReportDt.setIsPQRequired(list.getIsPQRequired());
                        
                        appReportDt.setgAdvtDt(list.getAdvtDt());
                        appReportDt.setgAdvtDy(list.getAdvtDays());
                        appReportDt.setgPlanDtInAdvtDt(list.getTenderAdvertDt());
                        appReportDt.setgPlanDyInAdvtDy(list.getTenderAdvertDays());
                        appReportDt.setgActDtInAdvtDt(list.getActAdvtDt());

                        totDays += list.getTenderAdvertDays();

                        appReportDt.setgPlanDtTenderOpenDt(list.getTenderOpenDt());
                        appReportDt.setgPlanDyTenderOpenDy(list.getTenderOpenDays());
                        appReportDt.setgActDtTenderOpenDt(list.getActTenderOpenDt());

                        totDays += list.getTenderOpenDays();

                        appReportDt.setgPlanDtTenderEvalDt(list.getTenderEvalRptDt());
                        appReportDt.setgPlanDyTenderEvalDy(list.getTenderEvalRptdays());
                        appReportDt.setgActDtTenderEvalDt(list.getActTenderEvalRptDt());

                        totDays += list.getTenderEvalRptdays();

                        appReportDt.setgPlanDtAppAwardDt(list.getTenderContractAppDt());
                        appReportDt.setgPlanDyAppAwardDy(list.getTenderContractAppDays());
                        appReportDt.setgActDtAppAwardDt(list.getActTenderContractAppDt());

                        totDays += list.getTenderContractAppDays();
                        //Code by Proshanto Kumar Saha
                        appReportDt.setgPlanDtLintAwardDt(list.getTenderLetterIntentDt());
                        appReportDt.setgPlanDyLintAwardDy(list.getTnderLetterIntentDays());
                        appReportDt.setgActDtLintAwardDt(list.getActTenderLetterIntentDt());
                        //appReportDt.setgActDtNtiAwardDt(list.getActTenderNoaIssueDt());

                        totDays += list.getTnderLetterIntentDays();
                        //Code end Proshanto
                        appReportDt.setgPlanDtNtiAwardDt(list.getTenderNoaIssueDt());
                        appReportDt.setgPlanDyNtiAwardDy(list.getTenderNoaIssueDays());
                        appReportDt.setgActDtNtiAwardDt(list.getActTenderNoaIssueDt());

                        totDays += list.getTenderNoaIssueDays();

                        appReportDt.setgPlanDtSignCtcDt(list.getTenderContractSignDt());
                        appReportDt.setgPlanDySignCtcDy(list.getTenderContractSignDays());
                        appReportDt.setgActDtSignCtcDt(list.getActTenderContractSignDt());

                        totDays += list.getTenderContractSignDays();

                        appReportDt.setgPlanDyTotTimeSignContractDy(totDays);
                        appReportDt.setgPlanDtTotTimeComContractDt(list.getTenderContractCompDt());
                        appReportDt.setgPlanDyTotTimeComContractDy(Short.valueOf("0"));

                        goodsRevList.add(appReportDt);
                        appReportDt = null;
                        retVal = true;
                }
                if ("Goods".equalsIgnoreCase(list.getProcurementNature()) && "Own fund".equalsIgnoreCase(list.getBudgetType())) {
                        totDays = 0;

                        appReportDt = new AppReportDtBean();
                        appReportDt.setAppId(list.getAppId());
                        appReportDt.setProjectNameCode(list.getProjectNameCode());
                        appReportDt.setMinistryDetails(list.getMinistryDetails());
                        appReportDt.setPackageId(list.getPackageId());
                        appReportDt.setPackageNo(list.getPackageNo());
                        appReportDt.setProcMethod(list.getProcurementMethod());
                        appReportDt.setProcType(list.getProcurementType());
                        appReportDt.setAppAut(list.getProcurementRole());
                        appReportDt.setSrcFund(list.getSourceOfFund());
                        appReportDt.setEstCost(list.getEstimatedCost());
                        appReportDt.setFormattedEstCost(formatter.format(list.getEstimatedCost().longValue() ));
                        appReportDt.setgApplstdt(list.getAppLstDt());
                        appReportDt.setIsPQRequired(list.getIsPQRequired());
                        
                        appReportDt.setgAdvtDt(list.getAdvtDt());
                        appReportDt.setgAdvtDy(list.getAdvtDays());
                        appReportDt.setgPlanDtInAdvtDt(list.getTenderAdvertDt());
                        appReportDt.setgPlanDyInAdvtDy(list.getTenderAdvertDays());
                        appReportDt.setgActDtInAdvtDt(list.getActAdvtDt());

                        totDays += list.getTenderAdvertDays();

                        appReportDt.setgPlanDtTenderOpenDt(list.getTenderOpenDt());
                        appReportDt.setgPlanDyTenderOpenDy(list.getTenderOpenDays());
                        appReportDt.setgActDtTenderOpenDt(list.getActTenderOpenDt());

                        totDays += list.getTenderOpenDays();

                        appReportDt.setgPlanDtTenderEvalDt(list.getTenderEvalRptDt());
                        appReportDt.setgPlanDyTenderEvalDy(list.getTenderEvalRptdays());
                        appReportDt.setgActDtTenderEvalDt(list.getActTenderEvalRptDt());

                        totDays += list.getTenderEvalRptdays();

                        appReportDt.setgPlanDtAppAwardDt(list.getTenderContractAppDt());
                        appReportDt.setgPlanDyAppAwardDy(list.getTenderContractAppDays());
                        appReportDt.setgActDtAppAwardDt(list.getActTenderContractAppDt());

                        totDays += list.getTenderContractAppDays();
                        
                        //Code by Proshanto Kumar Saha
                        appReportDt.setgPlanDtLintAwardDt(list.getTenderLetterIntentDt());
                        appReportDt.setgPlanDyLintAwardDy(list.getTnderLetterIntentDays());
                        appReportDt.setgActDtLintAwardDt(list.getActTenderLetterIntentDt());
                        //appReportDt.setgActDtNtiAwardDt(list.getActTenderNoaIssueDt());

                        totDays += list.getTnderLetterIntentDays();
                        //Code end Proshanto
                        
                        appReportDt.setgPlanDtNtiAwardDt(list.getTenderNoaIssueDt());
                        appReportDt.setgPlanDyNtiAwardDy(list.getTenderNoaIssueDays());
                        appReportDt.setgActDtNtiAwardDt(list.getActTenderNoaIssueDt());

                        totDays += list.getTenderNoaIssueDays();

                        appReportDt.setgPlanDtSignCtcDt(list.getTenderContractSignDt());
                        appReportDt.setgPlanDySignCtcDy(list.getTenderContractSignDays());
                        appReportDt.setgActDtSignCtcDt(list.getActTenderContractSignDt());

                        totDays += list.getTenderContractSignDays();

                        appReportDt.setgPlanDyTotTimeSignContractDy(totDays);
                        appReportDt.setgPlanDtTotTimeComContractDt(list.getTenderContractCompDt());
                        appReportDt.setgPlanDyTotTimeComContractDy(Short.valueOf("0"));

                        goodsOwnList.add(appReportDt);
                        appReportDt = null;
                        retVal = true;
                }

                if ("Works".equalsIgnoreCase(list.getProcurementNature()) && "Development".equalsIgnoreCase(list.getBudgetType())) {
                        totDays = 0;

                        appReportDt = new AppReportDtBean();
                        appReportDt.setAppId(list.getAppId());
                        appReportDt.setProjectNameCode(list.getProjectNameCode());
                        appReportDt.setMinistryDetails(list.getMinistryDetails());
                        appReportDt.setPackageId(list.getPackageId());
                        appReportDt.setPackageNo(list.getPackageNo());
                        appReportDt.setProcMethod(list.getProcurementMethod());
                        appReportDt.setProcType(list.getProcurementType());
                        appReportDt.setAppAut(list.getProcurementRole());
                        appReportDt.setSrcFund(list.getSourceOfFund());
                        appReportDt.setEstCost(list.getEstimatedCost());
                        appReportDt.setFormattedEstCost(formatter.format(list.getEstimatedCost().longValue()));
                        appReportDt.setgApplstdt(list.getAppLstDt());
                        appReportDt.setIsPQRequired(list.getIsPQRequired());
                    
                        appReportDt.setwAdvtDt(list.getAdvtDt());
                        appReportDt.setwAdvtDy(list.getAdvtDays());
                        appReportDt.setwPlanDtAdvtPreDt("");
                        appReportDt.setwPlanDyAdvtPreDy(Short.valueOf("0"));
                        appReportDt.setwActDtAdvtPreDt("");

                        totDays += 0;

                        appReportDt.setwPlanDtInitAdvtDt(list.getTenderAdvertDt());
                        appReportDt.setwPlanDyInitAdvtDy(list.getTenderAdvertDays());
                        appReportDt.setwActDtAdvtDt(list.getActTenderAdvertDt());

                        totDays += list.getTenderAdvertDays();

                        appReportDt.setwPlanDtTenderOpenDt(list.getTenderOpenDt());
                        appReportDt.setwPlanDyTenderOpenDy(list.getTenderOpenDays());
                        appReportDt.setwActDtTenderOpenDt(list.getActTenderOpenDt());

                        totDays += list.getTenderOpenDays();

                        appReportDt.setwPlanDtTenderEvalDt(list.getTenderEvalRptDt());
                        appReportDt.setwPlanDyTenderEvalDy(list.getTenderEvalRptdays());
                        appReportDt.setwActDtTenderEvalDt(list.getActTenderEvalRptDt());

                        totDays += list.getTenderEvalRptdays();

                        if("TSTM".equalsIgnoreCase(list.getProcurementMethod())){
                            
                            appReportDt.setwPlanDtAppAwardDt(list.getTenderEvalRptAppDt());
                            appReportDt.setwPlanDyAppAwardDy(list.getTenderEvalRptdays());
                            appReportDt.setwActDtAppAwardDt(list.getActtenderEvalRptAppDt());
                             totDays += list.getTenderEvalRptdays();
                            
                        }else{
                            appReportDt.setwPlanDtAppAwardDt(list.getTenderContractAppDt());
                            appReportDt.setwPlanDyAppAwardDy(list.getTenderContractAppDays());
                            appReportDt.setwActDtAppAwardDt(list.getActTenderContractAppDt());
                             totDays += list.getTenderContractAppDays();
                        }
                        
                        

                       //Code by Proshanto Kumar Saha
                        appReportDt.setwPlanDtLintAwardDt(list.getTenderLetterIntentDt());
                        appReportDt.setwPlanDyLintAwardDy(list.getTnderLetterIntentDays());
                        appReportDt.setwActDtLintAwardDt(list.getActTenderLetterIntentDt());
                        //appReportDt.setwActDtNtiAwardDt(list.getActTenderNoaIssueDt());

                        totDays += list.getTnderLetterIntentDays();
                       //End 
                        
                        appReportDt.setwPlanDtNtiAwardDt(list.getTenderNoaIssueDt());
                        appReportDt.setwPlanDyNtiAwardDy(list.getTenderNoaIssueDays());
                        appReportDt.setwActDtNtiAwardDt(list.getActTenderNoaIssueDt());

                        totDays += list.getTenderNoaIssueDays();

                        appReportDt.setwPlanDtSignCtcDt(list.getTenderContractSignDt());
                        appReportDt.setwPlanDySignCtcDy(list.getTenderContractSignDays());
                        appReportDt.setwActDtSignCtcDt(list.getActTenderContractSignDt());

                        totDays += list.getTenderContractSignDays();

                        appReportDt.setwPlanDyTotTimeSignContractDy(totDays);
                        appReportDt.setwPlanDtTotTimeComContractDt(list.getTenderContractCompDt());
                        appReportDt.setwPlanDyTotTimeSignContractDy(Short.MIN_VALUE);

                        workDevList.add(appReportDt);
                        appReportDt = null;
                        retVal = true;
                }
                if ("Works".equalsIgnoreCase(list.getProcurementNature()) && "Revenue".equalsIgnoreCase(list.getBudgetType())) {
                        totDays = 0;

                        appReportDt = new AppReportDtBean();
                        appReportDt.setAppId(list.getAppId());
                        appReportDt.setProjectNameCode(list.getProjectNameCode());
                        appReportDt.setProjectNameCode(list.getProjectNameCode());
                        appReportDt.setPackageId(list.getPackageId());
                        appReportDt.setMinistryDetails(list.getMinistryDetails());
                        appReportDt.setPackageNo(list.getPackageNo());
                        appReportDt.setProcMethod(list.getProcurementMethod());
                        appReportDt.setProcType(list.getProcurementType());
                        appReportDt.setAppAut(list.getProcurementRole());
                        appReportDt.setSrcFund(list.getSourceOfFund());
                        appReportDt.setEstCost(list.getEstimatedCost());
                        appReportDt.setFormattedEstCost(formatter.format(list.getEstimatedCost().longValue()));
                        appReportDt.setgApplstdt(list.getAppLstDt());
                        appReportDt.setIsPQRequired(list.getIsPQRequired());
                    
                        appReportDt.setwAdvtDt(list.getAdvtDt());
                        appReportDt.setwAdvtDy(list.getAdvtDays());
                        
                        appReportDt.setwPlanDtAdvtPreDt("");
                        appReportDt.setwPlanDyAdvtPreDy(Short.valueOf("0"));
                        appReportDt.setwActDtAdvtPreDt("");

                        totDays += 0;

                        appReportDt.setwPlanDtInitAdvtDt(list.getTenderAdvertDt());
                        appReportDt.setwPlanDyInitAdvtDy(list.getTenderAdvertDays());
                        appReportDt.setwActDtAdvtDt(list.getActTenderAdvertDt());

                        totDays += list.getTenderAdvertDays();

                        appReportDt.setwPlanDtTenderOpenDt(list.getTenderOpenDt());
                        appReportDt.setwPlanDyTenderOpenDy(list.getTenderOpenDays());
                        appReportDt.setwActDtTenderOpenDt(list.getActTenderOpenDt());

                        totDays += list.getTenderOpenDays();

                        appReportDt.setwPlanDtTenderEvalDt(list.getTenderEvalRptDt());
                        appReportDt.setwPlanDyTenderEvalDy(list.getTenderEvalRptdays());
                        appReportDt.setwActDtTenderEvalDt(list.getActTenderEvalRptDt());

                        totDays += list.getTenderEvalRptdays();
                        
                        if("TSTM".equalsIgnoreCase(list.getProcurementMethod())){
                            
                            appReportDt.setwPlanDtAppAwardDt(list.getTenderEvalRptAppDt());
                            appReportDt.setwPlanDyAppAwardDy(list.getTenderEvalRptdays());
                            appReportDt.setwActDtAppAwardDt(list.getActtenderEvalRptAppDt());
                             totDays += list.getTenderEvalRptdays();
                            
                        }else{
                            
                            appReportDt.setwPlanDtAppAwardDt(list.getTenderContractAppDt());
                            appReportDt.setwPlanDyAppAwardDy(list.getTenderContractAppDays());
                            appReportDt.setwActDtAppAwardDt(list.getActTenderContractAppDt());
                            totDays += list.getTenderContractAppDays();
                        }

                        //Code by Proshanto Kumar Saha
                        appReportDt.setwPlanDtLintAwardDt(list.getTenderLetterIntentDt());
                        appReportDt.setwPlanDyLintAwardDy(list.getTnderLetterIntentDays());
                        appReportDt.setwActDtLintAwardDt(list.getActTenderLetterIntentDt());
                        //appReportDt.setwActDtNtiAwardDt(list.getActTenderNoaIssueDt());

                        totDays += list.getTnderLetterIntentDays();
                       //End 
                       
                        appReportDt.setwPlanDtNtiAwardDt(list.getTenderNoaIssueDt());
                        appReportDt.setwPlanDyNtiAwardDy(list.getTenderNoaIssueDays());
                        appReportDt.setwActDtNtiAwardDt(list.getActTenderNoaIssueDt());

                        totDays += list.getTenderNoaIssueDays();

                        appReportDt.setwPlanDtSignCtcDt(list.getTenderContractSignDt());
                        appReportDt.setwPlanDySignCtcDy(list.getTenderContractSignDays());
                        appReportDt.setwActDtSignCtcDt(list.getActTenderContractSignDt());

                        totDays += list.getTenderContractSignDays();

                        appReportDt.setwPlanDyTotTimeSignContractDy(totDays);
                        appReportDt.setwPlanDtTotTimeComContractDt(list.getTenderContractCompDt());
                        appReportDt.setwPlanDyTotTimeSignContractDy(Short.MIN_VALUE);

                        workRevList.add(appReportDt);
                        appReportDt = null;
                        retVal = true;
                }
                if ("Works".equalsIgnoreCase(list.getProcurementNature()) && "Own fund".equalsIgnoreCase(list.getBudgetType())) {
                        totDays = 0;

                        appReportDt = new AppReportDtBean();
                        appReportDt.setAppId(list.getAppId());
                        appReportDt.setPackageId(list.getPackageId());
                        appReportDt.setProjectNameCode(list.getProjectNameCode());
                        appReportDt.setMinistryDetails(list.getMinistryDetails());
                        appReportDt.setPackageNo(list.getPackageNo());
                        appReportDt.setProcMethod(list.getProcurementMethod());
                        appReportDt.setProcType(list.getProcurementType());
                        appReportDt.setAppAut(list.getProcurementRole());
                        appReportDt.setSrcFund(list.getSourceOfFund());
                        appReportDt.setEstCost(list.getEstimatedCost());
                        appReportDt.setFormattedEstCost(formatter.format(list.getEstimatedCost().longValue() ));
                        appReportDt.setgApplstdt(list.getAppLstDt());
                        appReportDt.setIsPQRequired(list.getIsPQRequired());
                        
                        appReportDt.setwAdvtDt(list.getAdvtDt());
                        appReportDt.setwAdvtDy(list.getAdvtDays());
                        
                        appReportDt.setwPlanDtAdvtPreDt("");
                        appReportDt.setwPlanDyAdvtPreDy(Short.valueOf("0"));
                        appReportDt.setwActDtAdvtPreDt("");

                        totDays += 0;

                        appReportDt.setwPlanDtInitAdvtDt(list.getTenderAdvertDt());
                        appReportDt.setwPlanDyInitAdvtDy(list.getTenderAdvertDays());
                        appReportDt.setwActDtAdvtDt(list.getActTenderAdvertDt());

                        totDays += list.getTenderAdvertDays();

                        appReportDt.setwPlanDtTenderOpenDt(list.getTenderOpenDt());
                        appReportDt.setwPlanDyTenderOpenDy(list.getTenderOpenDays());
                        appReportDt.setwActDtTenderOpenDt(list.getActTenderOpenDt());

                        totDays += list.getTenderOpenDays();

                        appReportDt.setwPlanDtTenderEvalDt(list.getTenderEvalRptDt());
                        appReportDt.setwPlanDyTenderEvalDy(list.getTenderEvalRptdays());
                        appReportDt.setwActDtTenderEvalDt(list.getActTenderEvalRptDt());

                        totDays += list.getTenderEvalRptdays();

                        if("TSTM".equalsIgnoreCase(list.getProcurementMethod())){
                            
                            appReportDt.setwPlanDtAppAwardDt(list.getTenderEvalRptAppDt());
                            appReportDt.setwPlanDyAppAwardDy(list.getTenderEvalRptdays());
                            appReportDt.setwActDtAppAwardDt(list.getActtenderEvalRptAppDt());
                             totDays += list.getTenderEvalRptdays();
                            
                        }else{
                            appReportDt.setwPlanDtAppAwardDt(list.getTenderContractAppDt());
                            appReportDt.setwPlanDyAppAwardDy(list.getTenderContractAppDays());
                            appReportDt.setwActDtAppAwardDt(list.getActTenderContractAppDt());
                             totDays += list.getTenderContractAppDays();
                        }
                        
                        //Code by Proshanto Kumar Saha
                        appReportDt.setwPlanDtLintAwardDt(list.getTenderLetterIntentDt());
                        appReportDt.setwPlanDyLintAwardDy(list.getTnderLetterIntentDays());
                        appReportDt.setwActDtLintAwardDt(list.getActTenderLetterIntentDt());
                        //appReportDt.setwActDtNtiAwardDt(list.getActTenderNoaIssueDt());

                        totDays += list.getTnderLetterIntentDays();
                       //End 
                       
                        appReportDt.setwPlanDtNtiAwardDt(list.getTenderNoaIssueDt());
                        appReportDt.setwPlanDyNtiAwardDy(list.getTenderNoaIssueDays());
                        appReportDt.setwActDtNtiAwardDt(list.getActTenderNoaIssueDt());

                        totDays += list.getTenderNoaIssueDays();

                        appReportDt.setwPlanDtSignCtcDt(list.getTenderContractSignDt());
                        appReportDt.setwPlanDySignCtcDy(list.getTenderContractSignDays());
                        appReportDt.setwActDtSignCtcDt(list.getActTenderContractSignDt());

                        totDays += list.getTenderContractSignDays();

                        appReportDt.setwPlanDyTotTimeSignContractDy(totDays);
                        appReportDt.setwPlanDtTotTimeComContractDt(list.getTenderContractCompDt());
                        appReportDt.setwPlanDyTotTimeSignContractDy(Short.MIN_VALUE);

                        workOwnList.add(appReportDt);
                        appReportDt = null;
                        retVal = true;
                }

                if ("Services".equalsIgnoreCase(list.getProcurementNature()) && "Development".equalsIgnoreCase(list.getBudgetType())) {
                        totDays = 0;

                        appReportDt = new AppReportDtBean();
                        appReportDt.setAppId(list.getAppId());
                        appReportDt.setMinistryDetails(list.getMinistryDetails());
                        appReportDt.setProjectNameCode(list.getProjectNameCode());
                        appReportDt.setPackageId(list.getPackageId());
                        appReportDt.setPackageNo(list.getPackageNo());
                        appReportDt.setProcMethod(list.getProcurementMethod());
                        appReportDt.setProcType(list.getProcurementType());
                        appReportDt.setAppAut(list.getProcurementRole());
                        appReportDt.setSrcFund(list.getSourceOfFund());
                        
                        appReportDt.setEstCost(list.getEstimatedCost());
                        appReportDt.setFormattedEstCost(formatter.format(list.getEstimatedCost().longValue() ));
                        appReportDt.setgApplstdt(list.getAppLstDt());
                        appReportDt.setIsPQRequired(list.getIsPQRequired());
                        appReportDt.setsAdvtDt(list.getAdvtDt());
                        appReportDt.setsAdvtDy(list.getAdvtDays());
                    
                    if("SSS".equalsIgnoreCase(list.getProcurementMethod()) || "CSO".equalsIgnoreCase(list.getProcurementMethod())
                       || "IC".equalsIgnoreCase(list.getProcurementMethod()) ||"SBCQ".equalsIgnoreCase(list.getProcurementMethod()) 
                       || "DC".equalsIgnoreCase(list.getProcurementMethod())){

                        appReportDt.setsPlanDtAdvtEOIDt(list.getRfaAdvertDt());
                        appReportDt.setsPlanDyAdvtEOIDy(list.getRfaAdvertDays());
                        appReportDt.setsActDtAdvtEOIDt(list.getActRfaAdvertDt());
                        totDays += list.getAdvtDays();
                        
                        appReportDt.setsPlanDtIsuRFPDt(list.getRfaReceiptDt());
                        appReportDt.setsPlanDyIsuRFPDy(list.getRfaReceiptDays());
                        appReportDt.setsActDtIsuRFPDt(list.getActRfaReceiptDt());

                        totDays += list.getTenderAdvertDays();

                        appReportDt.setsPlanDtTechProOpenDt(list.getRfaEvalDt());
                        appReportDt.setsPlanDyTechProOpenDy(list.getRfaEvalDays());
                        appReportDt.setsActDtTechProOpenDt(list.getActRfaEvalDt());

                        totDays += list.getTenderOpenDays();

                        appReportDt.setsPlanDtTechProEvalDt(list.getRfaInterviewDt());
                        appReportDt.setsPlanDyTechProEvalDy(list.getRfaInterviewDays());
                        appReportDt.setsActDtTechProEvalDt("-");

                        totDays += list.getRfpTechEvalDays();

                        appReportDt.setsPlanDtFinProEvalDt(list.getRfaFinalSelDt());
                        appReportDt.setsPlanDyFinProEvalDy(list.getRfaFinalSelDays());
                        appReportDt.setsActDtFinProEvalDt(list.getActRfaFinalSelDt());

                        totDays += list.getTenderEvalRptdays();

                        appReportDt.setsPlanDtNegoDt(list.getRfaEvalRptSubDt());
                        appReportDt.setsPlanDyNegoDy(list.getRfaEvalRptSubDays());
                        appReportDt.setsActDtNegoDt(list.getActRfaEvalRptSubDt());

                        totDays += list.getRfpNegCompDays();

                        appReportDt.setsPlanDtAppDt(list.getRfaAppConsultantDt());
                        appReportDt.setsPlanDyAppDy((short) 0);
                        appReportDt.setsActDtAppDt(list.getActRfaAppConsultantDt());

                        totDays += list.getRfpContractAppDays();

                        appReportDt.setsPlanDtSignCtcDt(list.getTenderContractSignDt());
                        appReportDt.setsPlanDySignCtcDy(list.getTenderContractSignDays());
                        appReportDt.setsActDtSignCtcDt(list.getActTenderContractSignDt());

                        totDays += list.getTenderContractSignDays();
                        
                    }else{
                        appReportDt.setsPlanDtAdvtEOIDt(list.getAdvtDt());
                        appReportDt.setsPlanDyAdvtEOIDy(list.getAdvtDays());
                        appReportDt.setsActDtAdvtEOIDt(list.getActAdvtDt());
                        totDays += list.getAdvtDays();
                        
                        appReportDt.setsPlanDtIsuRFPDt(list.getTenderAdvertDt());
                        appReportDt.setsPlanDyIsuRFPDy(list.getTenderAdvertDays());
                        appReportDt.setsActDtIsuRFPDt(list.getActTenderAdvertDt());

                        totDays += list.getTenderAdvertDays();

                        appReportDt.setsPlanDtTechProOpenDt(list.getTenderOpenDt());
                        appReportDt.setsPlanDyTechProOpenDy(list.getTenderOpenDays());
                        appReportDt.setsActDtTechProOpenDt(list.getActTenderOpenDt());

                        totDays += list.getTenderOpenDays();

                        appReportDt.setsPlanDtTechProEvalDt(list.getRfpTechEvalDt());
                        appReportDt.setsPlanDyTechProEvalDy(list.getRfpTechEvalDays());
                        appReportDt.setsActDtTechProEvalDt(list.getActRfpTechEvalDt());

                        totDays += list.getRfpTechEvalDays();

                        appReportDt.setsPlanDtFinProEvalDt(list.getTenderEvalRptDt());
                        appReportDt.setsPlanDyFinProEvalDy(list.getTenderEvalRptdays());
                        appReportDt.setsActDtFinProEvalDt(list.getActTenderEvalRptDt());

                        totDays += list.getTenderEvalRptdays();

                        appReportDt.setsPlanDtNegoDt(list.getRfpNegCompDt());
                        appReportDt.setsPlanDyNegoDy(list.getRfpNegCompDays());
                        appReportDt.setsActDtNegoDt(list.getActRfpNegComDt());

                        totDays += list.getRfpNegCompDays();

                        appReportDt.setsPlanDtAppDt(list.getRfpContractAppDt());
                        appReportDt.setsPlanDyAppDy(list.getRfpContractAppDays());
                        appReportDt.setsActDtAppDt(list.getActRfpContractAppDt());

                        totDays += list.getRfpContractAppDays();

                        appReportDt.setsPlanDtSignCtcDt(list.getTenderContractSignDt());
                        appReportDt.setsPlanDySignCtcDy(list.getTenderContractSignDays());
                        appReportDt.setsActDtSignCtcDt(list.getActTenderContractSignDt());

                        totDays += list.getTenderContractSignDays();
                    }
                        

                        

                        appReportDt.setsPlanDyTotTimeSignContractDy(totDays);
                        appReportDt.setsPlanDtTotTimeComContractDt(list.getTenderContractCompDt());
                        appReportDt.setsPlanDyTotTimeComContractDy(Short.MIN_VALUE);

                        serviceDevList.add(appReportDt);
                        appReportDt = null;
                        retVal = true;
                }
                if ("Services".equalsIgnoreCase(list.getProcurementNature()) && "Revenue".equalsIgnoreCase(list.getBudgetType())) {
                        totDays = 0;

                        appReportDt = new AppReportDtBean();
                        appReportDt.setAppId(list.getAppId());
                        appReportDt.setMinistryDetails(list.getMinistryDetails());
                        appReportDt.setProjectNameCode(list.getProjectNameCode());
                        appReportDt.setPackageId(list.getPackageId());
                        appReportDt.setPackageNo(list.getPackageNo());
                        appReportDt.setProcMethod(list.getProcurementMethod());
                        appReportDt.setProcType(list.getProcurementType());
                        appReportDt.setAppAut(list.getProcurementRole());
                        appReportDt.setSrcFund(list.getSourceOfFund());
                        appReportDt.setEstCost(list.getEstimatedCost());
                        appReportDt.setFormattedEstCost(formatter.format(list.getEstimatedCost().longValue() ));
                    appReportDt.setgApplstdt(list.getAppLstDt());
                    appReportDt.setIsPQRequired(list.getIsPQRequired());
                        
                        appReportDt.setsAdvtDt(list.getAdvtDt());
                        appReportDt.setsAdvtDy(list.getAdvtDays());
                        appReportDt.setsPlanDtAdvtEOIDt(list.getAdvtDt());
                        appReportDt.setsPlanDyAdvtEOIDy(list.getAdvtDays());
                        appReportDt.setsActDtAdvtEOIDt(list.getActAdvtDt());

                        totDays += list.getAdvtDays();

                        appReportDt.setsPlanDtIsuRFPDt(list.getTenderAdvertDt());
                        appReportDt.setsPlanDyIsuRFPDy(list.getTenderAdvertDays());
                        appReportDt.setsActDtIsuRFPDt(list.getActTenderAdvertDt());

                        totDays += list.getTenderAdvertDays();

                        appReportDt.setsPlanDtTechProOpenDt(list.getTenderOpenDt());
                        appReportDt.setsPlanDyTechProOpenDy(list.getTenderOpenDays());
                        appReportDt.setsActDtTechProOpenDt(list.getActTenderOpenDt());

                        totDays += list.getTenderOpenDays();

                        appReportDt.setsPlanDtTechProEvalDt(list.getRfpTechEvalDt());
                        appReportDt.setsPlanDyTechProEvalDy(list.getRfpTechEvalDays());
                        appReportDt.setsActDtTechProEvalDt(list.getActRfpTechEvalDt());

                        totDays += list.getRfpTechEvalDays();

                        appReportDt.setsPlanDtFinProEvalDt(list.getTenderEvalRptDt());
                        appReportDt.setsPlanDyFinProEvalDy(list.getTenderEvalRptdays());
                        appReportDt.setsActDtFinProEvalDt(list.getActTenderEvalRptDt());

                        totDays += list.getTenderEvalRptdays();

                        appReportDt.setsPlanDtNegoDt(list.getRfpNegCompDt());
                        appReportDt.setsPlanDyNegoDy(list.getRfpNegCompDays());
                        appReportDt.setsActDtNegoDt(list.getActRfpNegComDt());

                        totDays += list.getRfpNegCompDays();

                        appReportDt.setsPlanDtAppDt(list.getRfpContractAppDt());
                        appReportDt.setsPlanDyAppDy(list.getRfpContractAppDays());
                        appReportDt.setsActDtAppDt(list.getActRfpContractAppDt());

                        totDays += list.getRfpContractAppDays();

                        appReportDt.setsPlanDtSignCtcDt(list.getTenderContractSignDt());
                        appReportDt.setsPlanDySignCtcDy(list.getTenderContractSignDays());
                        appReportDt.setsActDtSignCtcDt(list.getActTenderContractSignDt());

                        totDays += list.getTenderContractSignDays();

                        appReportDt.setsPlanDyTotTimeSignContractDy(totDays);
                        appReportDt.setsPlanDtTotTimeComContractDt(list.getTenderContractCompDt());
                        appReportDt.setsPlanDyTotTimeComContractDy(Short.MIN_VALUE);

                        serviceRevList.add(appReportDt);
                        appReportDt = null;
                        retVal = true;
                }
                if ("Services".equalsIgnoreCase(list.getProcurementNature()) && "Own fund".equalsIgnoreCase(list.getBudgetType())) {
                        totDays = 0;

                        appReportDt = new AppReportDtBean();
                        appReportDt.setAppId(list.getAppId());
                        appReportDt.setMinistryDetails(list.getMinistryDetails());
                        appReportDt.setProjectNameCode(list.getProjectNameCode());
                        appReportDt.setPackageId(list.getPackageId());
                        appReportDt.setPackageNo(list.getPackageNo());
                        appReportDt.setProcMethod(list.getProcurementMethod());
                        appReportDt.setProcType(list.getProcurementType());
                        appReportDt.setAppAut(list.getProcurementRole());
                        appReportDt.setSrcFund(list.getSourceOfFund());
                        appReportDt.setEstCost(list.getEstimatedCost());
                        appReportDt.setFormattedEstCost(formatter.format(list.getEstimatedCost().longValue() ));
                    appReportDt.setgApplstdt(list.getAppLstDt());
                    appReportDt.setIsPQRequired(list.getIsPQRequired());
                        
                        appReportDt.setsAdvtDt(list.getAdvtDt());
                        appReportDt.setsAdvtDy(list.getAdvtDays());
                        
                        appReportDt.setsPlanDtAdvtEOIDt(list.getAdvtDt());
                        appReportDt.setsPlanDyAdvtEOIDy(list.getAdvtDays());
                        appReportDt.setsActDtAdvtEOIDt(list.getActAdvtDt());

                        totDays += list.getAdvtDays();

                        appReportDt.setsPlanDtIsuRFPDt(list.getTenderAdvertDt());
                        appReportDt.setsPlanDyIsuRFPDy(list.getTenderAdvertDays());
                        appReportDt.setsActDtIsuRFPDt(list.getActTenderAdvertDt());

                        totDays += list.getTenderAdvertDays();

                        appReportDt.setsPlanDtTechProOpenDt(list.getTenderOpenDt());
                        appReportDt.setsPlanDyTechProOpenDy(list.getTenderOpenDays());
                        appReportDt.setsActDtTechProOpenDt(list.getActTenderOpenDt());

                        totDays += list.getTenderOpenDays();

                        appReportDt.setsPlanDtTechProEvalDt(list.getRfpTechEvalDt());
                        appReportDt.setsPlanDyTechProEvalDy(list.getRfpTechEvalDays());
                        appReportDt.setsActDtTechProEvalDt(list.getActRfpTechEvalDt());

                        totDays += list.getRfpTechEvalDays();

                        appReportDt.setsPlanDtFinProEvalDt(list.getTenderEvalRptDt());
                        appReportDt.setsPlanDyFinProEvalDy(list.getTenderEvalRptdays());
                        appReportDt.setsActDtFinProEvalDt(list.getActTenderEvalRptDt());

                        totDays += list.getTenderEvalRptdays();

                        appReportDt.setsPlanDtNegoDt(list.getRfpNegCompDt());
                        appReportDt.setsPlanDyNegoDy(list.getRfpNegCompDays());
                        appReportDt.setsActDtNegoDt(list.getActRfpNegComDt());

                        totDays += list.getRfpNegCompDays();

                        appReportDt.setsPlanDtAppDt(list.getRfpContractAppDt());
                        appReportDt.setsPlanDyAppDy(list.getRfpContractAppDays());
                        appReportDt.setsActDtAppDt(list.getActRfpContractAppDt());

                        totDays += list.getRfpContractAppDays();

                        appReportDt.setsPlanDtSignCtcDt(list.getTenderContractSignDt());
                        appReportDt.setsPlanDySignCtcDy(list.getTenderContractSignDays());
                        appReportDt.setsActDtSignCtcDt(list.getActTenderContractSignDt());

                        totDays += list.getTenderContractSignDays();

                        appReportDt.setsPlanDyTotTimeSignContractDy(totDays);
                        appReportDt.setsPlanDtTotTimeComContractDt(list.getTenderContractCompDt());
                        appReportDt.setsPlanDyTotTimeComContractDy(Short.MIN_VALUE);

                        serviceOwnList.add(appReportDt);
                        appReportDt = null;
                        retVal = true;
                }
            }


            if (this.getGoodsDevList().size() > 0) {
            this.setGoodsDev(true);
        }

            if (this.getGoodsRevList().size() > 0) {
            this.setGoodsRev(true);
        }

            if (this.getGoodsOwnList().size() > 0) {
            this.setGoodsOwn(true);
        }

            if (this.getWorkDevList().size() > 0) {
            this.setWorkDev(true);
        }

            if (this.getWorkRevList().size() > 0) {
            this.setWorkRev(true);
        }

            if (this.getWorkOwnList().size() > 0) {
            this.setWorkOwn(true);
        }

            if (this.getServiceDevList().size() > 0) {
            this.setServiceDev(true);
        }

            if (this.getServiceRevList().size() > 0) {
            this.setServiceRev(true);
        }

            if (this.getServiceOwnList().size() > 0) {
            this.setServiceOwn(true);
        }

            if (this.isGoodsDev() || this.isGoodsRev() || this.isGoodsOwn()
                    || this.isServiceDev() || this.isServiceRev() || this.isServiceOwn()
                    || this.isWorkDev() || this.isWorkRev() || this.isWorkOwn()) {
            this.setReportData(true);
        }
        } catch (Exception e) {
            LOGGER.error("getAllList : " + logUserId,e);
        }
        LOGGER.debug("getAllList : " + logUserId + LOGGEREND);
        return retVal;
    }

    /**
     * Get All APP details from app ID.
     * @param appId
     * @return Object Array having detail of App.
     */
    public Object[] getAllDetailByAppId(int appId) {
        LOGGER.debug("getAllDetailByAppId : " + logUserId + LOGGERSTART);
        Object obj[] = null;
        try {
            obj = appReportService.getDetailByAppId(appId);
        } catch (Exception e) {
            LOGGER.error("getAllDetailByAppId : " + logUserId,e);
        }
        LOGGER.debug("getAllDetailByAppId : " + logUserId + LOGGEREND);
            return obj;
        }

    /**
     * Get APP Details from the APP Id.
     * @param appId
     * @return
     */
    public boolean getDetailByAppId(int appId) {
        LOGGER.debug("getDetailByAppId : " + logUserId + LOGGERSTART);
        boolean retVal = false;
        try {
            Object obj[] = appReportService.getDetailByAppId(appId);

            retVal = this.getAllList((Short) obj[0], (Integer) obj[1], (String) obj[2], Integer.parseInt(String.valueOf((Byte) obj[3])), (Integer) obj[4], "", appId);

        } catch (Exception e) {
            LOGGER.error("getDetailByAppId : " + logUserId,e);
        }
        LOGGER.debug("getDetailByAppId : " + logUserId + LOGGEREND);
        return retVal;
    }

    /**
     * Get Date difference between two dates.
     * @param date1
     * @param date2
     * @return time difference in time stamp.
     */
    public long getDateDiff(String date1,String date2){
        LOGGER.debug("getDateDiff : " + logUserId + LOGGERSTART);
        long lngDatediff = 0;
        try{
            if(date1!=null && date2!=null && !"-".equalsIgnoreCase(date1) && !"-".equalsIgnoreCase(date2)){
               Date dt1 = DateUtils.StrtoDate(date1);
               Date dt2 = DateUtils.StrtoDate(date2);
               lngDatediff = DateUtils.calculateDays(dt1,dt2);
            }
        }catch(Exception e){
            LOGGER.error("getDateDiff : " + logUserId,e);
        }
    
        LOGGER.debug("getDateDiff : " + logUserId + LOGGEREND);
        return lngDatediff;
    }
}
