/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

/**
 *
 * @author test
 */
public class WorkFlowDtBean {

    private String moduleName;
    private String eventName;
    private String startsBy;
    private String endsBy;
    private String isPublishDateRequire;

    /**
     * @return the moduleName
     */
    public String getModuleName() {
        return moduleName;
    }

    /**
     * @param moduleName the moduleName to set
     */
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    /**
     * @return the eventName
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * @param eventName the eventName to set
     */
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    /**
     * @return the startsBy
     */
    public String getStartsBy() {
        return startsBy;
    }

    /**
     * @param startsBy the startsBy to set
     */
    public void setStartsBy(String startsBy) {
        this.startsBy = startsBy;
    }

    /**
     * @return the endsBy
     */
    public String getEndsBy() {
        return endsBy;
    }

    /**
     * @param endsBy the endsBy to set
     */
    public void setEndsBy(String endsBy) {
        this.endsBy = endsBy;
    }

    /**
     * @return the isPublishDateRequire
     */
    public String getIsPublishDateRequire() {
        return isPublishDateRequire;
    }

    /**
     * @param isPublishDateRequire the isPublishDateRequire to set
     */
    public void setIsPublishDateRequire(String isPublishDateRequire) {
        this.isPublishDateRequire = isPublishDateRequire;
    }

    
}
