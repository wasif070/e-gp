/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblContractAwardedOffline;
import com.cptu.egp.eps.model.table.TblCorrigendumDetailOffline;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblTenderDetailsOffline;
import com.cptu.egp.eps.model.table.TblTenderLotPhasingOffline;
import com.cptu.egp.eps.service.serviceimpl.CorrigendumDetailsOfflineService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.DepartmentCreationService;
import com.cptu.egp.eps.service.serviceinterface.OfflineDataService;
import com.cptu.egp.eps.service.serviceinterface.ContractAwardOfflineService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CommonUtils;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;


/**
 *
 * @author Administrator
 */
public class OfflineDataSrBean {

private final CorrigendumDetailsOfflineService corrigendumDetailsOfflineService = (CorrigendumDetailsOfflineService) AppContext.getSpringBean("CorrigendumDetailsOfflineService");
private final OfflineDataService offlineDataService = (OfflineDataService) AppContext.getSpringBean("OfflineDataService");
 private static final Logger LOGGER = Logger.getLogger(OfflineDataSrBean.class);
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private String logUserId = "0";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        System.out.println("setting log userid");
        offlineDataService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }
    public void createOfflineData(TblTenderDetailsOffline tenderDetailOfflineData){
        System.out.println("tenderDetailOfflineData >> "+tenderDetailOfflineData);
         LOGGER.debug("createing offline data : " + logUserId + LOGGERSTART);
        try{
        offlineDataService.saveTblTenderDetailOfflineData(tenderDetailOfflineData);
        }catch(Exception ex){
        LOGGER.error("creating offline data : " + logUserId,ex);
        }
        LOGGER.debug("creating offline data : " + logUserId + LOGGEREND);
    }

     public void createContractAwardOfflineData(TblContractAwardedOffline contractAwardedOffline){
        System.out.println("createContractAwardOfflineData >> "+contractAwardedOffline);
         LOGGER.debug("createContractAwardOfflineData : " + logUserId + LOGGERSTART);
        try{
        offlineDataService.saveTblContractAwardedOffline(contractAwardedOffline);
        }catch(Exception ex){
        LOGGER.error("createContractAwardOfflineData : " + logUserId,ex);
        }
        LOGGER.debug("createContractAwardOfflineData : " + logUserId + LOGGEREND);
    }

    public void deleteofflineData(TblTenderDetailsOffline tenderDetailOfflineData){
         System.out.println("deleteofflineData >> "+tenderDetailOfflineData);
         LOGGER.debug("deleteofflineData : " + logUserId + LOGGERSTART);
        try{
            offlineDataService.deleteTblTenderDetailOfflineData(tenderDetailOfflineData);
        }catch(Exception ex){
            LOGGER.error("deleteofflineData : " + logUserId,ex);
        }
          LOGGER.debug("deleteofflineData : " + logUserId + LOGGEREND);
    }

     public void deleteContractAwardedOffline(TblContractAwardedOffline contractAwardedOffline){
         System.out.println("deleteContractAwardedOffline >> "+contractAwardedOffline);
         LOGGER.debug("deleteContractAwardedOffline : " + logUserId + LOGGERSTART);
        try{
            offlineDataService.deleteTblContractAwardedOffline(contractAwardedOffline);
        }catch(Exception ex){
            LOGGER.error("deleteContractAwardedOffline : " + logUserId,ex);
        }
          LOGGER.debug("deleteContractAwardedOffline : " + logUserId + LOGGEREND);
    }


    public TblTenderDetailsOffline getTblTenderDetailOfflineDataById(int tenderOLId){
         System.out.println("getTblTenderDetailOfflineDataById >> "+tenderOLId);
         LOGGER.debug("getTblTenderDetailOfflineDataById : " + logUserId + LOGGERSTART);
         TblTenderDetailsOffline tenderDetailsOffline = null;
        try{

           List<TblTenderDetailsOffline> detailOfflineDatas =  offlineDataService.getTblTenderDetailOfflineDataById(tenderOLId);
           System.err.println("detailOfflineDatas >>>  "+detailOfflineDatas);
           if(detailOfflineDatas != null && detailOfflineDatas.size() >0){
            tenderDetailsOffline = detailOfflineDatas.get(0);
           }
        }catch(Exception ex){
            LOGGER.error("getTblTenderDetailOfflineDataById : " + logUserId,ex);
        }
          LOGGER.debug("getTblTenderDetailOfflineDataById : " + logUserId + LOGGEREND);
          return tenderDetailsOffline;
    }

     public TblContractAwardedOffline getTblContractAwardedOfflineById(int awardId){
         System.out.println("getTblContractAwardedOfflineById >> "+awardId);
         LOGGER.debug("getTblContractAwardedOfflineById : " + logUserId + LOGGERSTART);
         TblContractAwardedOffline awardedOffline = null;
        try{

           List<TblContractAwardedOffline> awardedOfflines =  offlineDataService.getTblContractAwardedOfflineById(awardId);
           System.err.println("awardedOfflines >>>  "+awardedOfflines);
           if(awardedOfflines != null && awardedOfflines.size() >0){
            awardedOffline = awardedOfflines.get(0);
           }
        }catch(Exception ex){
            LOGGER.error("getTblContractAwardedOfflineById : " + logUserId,ex);
        }
          LOGGER.debug("getTblContractAwardedOfflineById : " + logUserId + LOGGEREND);
          return awardedOffline;
    }


    public boolean updateTenderDetailsOfflineData(TblTenderDetailsOffline tenderDetailsOffline,int tenderOLId){
        boolean isUpdated = false;
        LOGGER.debug("updateTenderDetailsOfflineData : " + logUserId + LOGGERSTART);
        try{
          // TblTenderDetailsOffline updatetenderDetailsOffline = getTblTenderDetailOfflineDataById(tenderOLId);
          // System.out.println("existing obj >> "+updatetenderDetailsOffline);
           if(tenderDetailsOffline != null){
               //offlineDataService.updateTblTenderDetailOfflineData(updatetenderDetailsOffline);
               offlineDataService.updateTblTenderDetailOfflineData(tenderDetailsOffline);
                isUpdated = true;
           }
        }catch(Exception ex){
            LOGGER.error("updateTenderDetailsOfflineData : " + logUserId,ex);
        }
        LOGGER.debug("updateTenderDetailsOfflineData : " + logUserId + LOGGEREND);
        return isUpdated;
    }

     public boolean updateTblContractAwardedOfflineData(TblContractAwardedOffline awardedOffline,int tenderOLId){
        boolean isUpdated = false;
        LOGGER.debug("updateTenderDetailsOfflineData : " + logUserId + LOGGERSTART);
        try{
          // TblTenderDetailsOffline updatetenderDetailsOffline = getTblTenderDetailOfflineDataById(tenderOLId);
          // System.out.println("existing obj >> "+updatetenderDetailsOffline);
           if(awardedOffline != null){
               //offlineDataService.updateTblTenderDetailOfflineData(updatetenderDetailsOffline);
               offlineDataService.updateTblContractAwardedOffline(awardedOffline);
                isUpdated = true;
              //  response.sendRedirect("SearchAwardedContractOffline.jsp");
           }
        }catch(Exception ex){
            LOGGER.error("updateTenderDetailsOfflineData : " + logUserId,ex);
        }
        LOGGER.debug("updateTenderDetailsOfflineData : " + logUserId + LOGGEREND);
        return isUpdated;
    }

    
    public PrequalificationExcellBean editTenderWithPQForm(int tenderOLId){
        PrequalificationExcellBean excellBean = new PrequalificationExcellBean();

         LOGGER.debug("editTenderWithPQForm : " + logUserId + LOGGERSTART);
        try{
           TblTenderDetailsOffline tenderDetailsOffline  =  getTblTenderDetailOfflineDataById(tenderOLId);
           System.out.println("tenderDetailsOffline exists >> "+tenderDetailsOffline);
           if(tenderDetailsOffline != null){
                excellBean.setMinistryName(CommonUtils.checkNull(tenderDetailsOffline.getMinistryOrDivision()));
                excellBean.setAgency(CommonUtils.checkNull(tenderDetailsOffline.getAgency()));

                excellBean.setProcuringEntityName(CommonUtils.checkNull(tenderDetailsOffline.getPeName()));
                excellBean.setProcuringEntityCode(CommonUtils.checkNull(tenderDetailsOffline.getPeCode()));
                excellBean.setProcuringEntityDistrict(CommonUtils.checkNull(tenderDetailsOffline.getPeDistrict()));
                excellBean.setProcurementNature(CommonUtils.checkNull(tenderDetailsOffline.getProcurementNature()));
                excellBean.setInvitationFor(CommonUtils.checkNull(tenderDetailsOffline.getInvitationFor()));
                excellBean.setInvitationRefNo(CommonUtils.checkNull(tenderDetailsOffline.getReoiRfpRefNo()));
                if(tenderDetailsOffline.getIssueDate() != null){
                    excellBean.setTenderissuingdate(CommonUtils.checkNull(DateUtils.formatStdDate(tenderDetailsOffline.getIssueDate())));
                }
                
                excellBean.setProcurementType(CommonUtils.checkNull(tenderDetailsOffline.getProcurementType()));
                excellBean.setProcurementMethod(CommonUtils.checkNull(tenderDetailsOffline.getProcurementMethod()));
                excellBean.setCbobudget(CommonUtils.checkNull(tenderDetailsOffline.getBudgetType()));
                excellBean.setCbosource(CommonUtils.checkNull(tenderDetailsOffline.getSourceOfFund()));
                excellBean.setDevelopmentPartner(CommonUtils.checkNull(tenderDetailsOffline.getDevPartners()));
                excellBean.setProjectOrProgrammeCode(CommonUtils.checkNull(tenderDetailsOffline.getProjectCode()));
                excellBean.setProjectOrProgrammeName(CommonUtils.checkNull(tenderDetailsOffline.getProjectName()));
                excellBean.setTenderPackageNo(CommonUtils.checkNull(tenderDetailsOffline.getPackageNo()));
                excellBean.setTenderPackageName(CommonUtils.checkNull(tenderDetailsOffline.getPackageName()));
                if(tenderDetailsOffline.getTenderPubDate() != null){
                    excellBean.setPrequalificationPublicationDate(CommonUtils.checkNull(DateUtils.formatStdDate(tenderDetailsOffline.getTenderPubDate())));
                }
                if(tenderDetailsOffline.getClosingDate() != null){
                    excellBean.setPrequalificationClosingDateandTime(CommonUtils.checkNull(DateUtils.convertStrToDate(tenderDetailsOffline.getClosingDate())));
                }
                excellBean.setSellingPrequalificationDocumentPrincipal(CommonUtils.checkNull(tenderDetailsOffline.getSellingAddPrinciple()));
                excellBean.setSellingPrequalificationDocumentOthers(CommonUtils.checkNull(tenderDetailsOffline.getSellingAddOthers()));
                excellBean.setReceivingPrequalification(CommonUtils.checkNull(tenderDetailsOffline.getReceivingAdd()));
                excellBean.setPlace_Date_TimeofPrequalificationMeeting(CommonUtils.checkNull(tenderDetailsOffline.getPreTenderReoiplace()));
                
                if(tenderDetailsOffline.getPreTenderReoidate() != null){
                    excellBean.setDateTimeofPrequalificationMeeting(CommonUtils.checkNull(DateUtils.convertStrToDate(tenderDetailsOffline.getPreTenderReoidate())));
                }                
                excellBean.setEligibilityofApplicant(CommonUtils.checkNull(tenderDetailsOffline.getEligibilityCriteria()));
                excellBean.setBriefDescriptionofGoodsorWorks(CommonUtils.checkNull(tenderDetailsOffline.getBriefDescription()));
                excellBean.setBriefDescriptionofRelatedServices(CommonUtils.checkNull(tenderDetailsOffline.getRelServicesOrDeliverables()));
                BigDecimal bd = tenderDetailsOffline.getDocumentPrice().setScale(2, 0);
                excellBean.setPrequalificationDocumentPrice(CommonUtils.checkNull(bd.toString()));
                List<TblTenderLotPhasingOffline> tenderLotsAndPhases = tenderDetailsOffline.getTenderLotsAndPhases();
                 if(tenderDetailsOffline.getInvitationFor().equals("Single Lot")){
                     if(tenderLotsAndPhases != null){
                        TblTenderLotPhasingOffline lotPhasingOffline  =  tenderLotsAndPhases.get(0);
                        if(lotPhasingOffline != null){
                            excellBean.setLotNo_1(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                            excellBean.setLocation_1(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                            excellBean.setIdentificationOfLot_1(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                            excellBean.setCompletionTimeInWeeksORmonths_1(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                        }
                     }
                 }else if(tenderDetailsOffline.getInvitationFor().equals("Multiple Lot")){
                       if(tenderLotsAndPhases != null){
                           TblTenderLotPhasingOffline lotPhasingOffline = null;
                           if(tenderLotsAndPhases.size() > 0 && tenderLotsAndPhases.get(0) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(0);
                                 excellBean.setLotNo_1(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 excellBean.setLocation_1(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 excellBean.setIdentificationOfLot_1(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 excellBean.setCompletionTimeInWeeksORmonths_1(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 1 && tenderLotsAndPhases.get(1) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(1);
                                 excellBean.setLotNo_2(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 excellBean.setLocation_2(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 excellBean.setIdentificationOfLot_2(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 excellBean.setCompletionTimeInWeeksORmonths_2(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 2 && tenderLotsAndPhases.get(2) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(2);
                                 excellBean.setLotNo_3(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 excellBean.setLocation_3(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 excellBean.setIdentificationOfLot_3(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 excellBean.setCompletionTimeInWeeksORmonths_3(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 3 && tenderLotsAndPhases.get(3) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(3);
                                 excellBean.setLotNo_4(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 excellBean.setLocation_4(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 excellBean.setIdentificationOfLot_4(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 excellBean.setCompletionTimeInWeeksORmonths_4(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                        }
                 }
                
                excellBean.setNameofOfficialInvitingPrequalification(CommonUtils.checkNull(tenderDetailsOffline.getPeOfficeName()));
                excellBean.setDesignationofOfficialInvitingPrequalification(CommonUtils.checkNull(tenderDetailsOffline.getPeDesignation()));
                excellBean.setAddressofOfficialInvitingPrequalification(CommonUtils.checkNull(tenderDetailsOffline.getPeAddress()));
                excellBean.setContactdetailsofOfficialInvitingPrequalification(CommonUtils.checkNull(tenderDetailsOffline.getPeContactDetails()));

               }
           
        }catch(Exception ex){
            System.out.println("editTenderWithPQForm : " + ex);
        }
        LOGGER.debug("editTenderWithPQForm : " + logUserId + LOGGEREND);
        return excellBean;
    }

    public TenderFormExcellBean editTenderWithoutPQForm(int tenderOLId){
        TenderFormExcellBean excellBean = new TenderFormExcellBean();

         LOGGER.debug("editTenderWithPQForm : " + logUserId + LOGGERSTART);
        try{
           TblTenderDetailsOffline tenderDetailsOffline  =  getTblTenderDetailOfflineDataById(tenderOLId);
           System.out.println("tenderDetailsOffline exists >> "+tenderDetailsOffline);
           if(tenderDetailsOffline != null){
                excellBean.setMinistryName(CommonUtils.checkNull(tenderDetailsOffline.getMinistryOrDivision()));
                excellBean.setAgency(CommonUtils.checkNull(tenderDetailsOffline.getAgency()));
                excellBean.setProcuringEntityName(CommonUtils.checkNull(tenderDetailsOffline.getPeName()));
                excellBean.setProcuringEntityCode(CommonUtils.checkNull(tenderDetailsOffline.getPeCode()));
                excellBean.setProcuringEntityDistrict(CommonUtils.checkNull(tenderDetailsOffline.getPeDistrict()));
                excellBean.setProcurementNature(CommonUtils.checkNull(tenderDetailsOffline.getProcurementNature()));
                excellBean.setInvitationFor(CommonUtils.checkNull(tenderDetailsOffline.getInvitationFor()));
                excellBean.setInvitationRefNo(CommonUtils.checkNull(tenderDetailsOffline.getReoiRfpRefNo()));
                if(tenderDetailsOffline.getIssueDate() != null){
                    excellBean.setTenderissuingdate(CommonUtils.checkNull(DateUtils.formatStdDate(tenderDetailsOffline.getIssueDate())));
                }

                excellBean.setProcurementType(CommonUtils.checkNull(tenderDetailsOffline.getProcurementType()));
                excellBean.setProcurementMethod(CommonUtils.checkNull(tenderDetailsOffline.getProcurementMethod()));
                excellBean.setCbobudget(CommonUtils.checkNull(tenderDetailsOffline.getBudgetType()));
                excellBean.setCbosource(CommonUtils.checkNull(tenderDetailsOffline.getSourceOfFund()));
                excellBean.setDevelopmentPartner(CommonUtils.checkNull(tenderDetailsOffline.getDevPartners()));
                excellBean.setProjectOrProgrammeCode(CommonUtils.checkNull(tenderDetailsOffline.getProjectCode()));
                excellBean.setProjectOrProgrammeName(CommonUtils.checkNull(tenderDetailsOffline.getProjectName()));
                excellBean.setTenderPackageNo(CommonUtils.checkNull(tenderDetailsOffline.getPackageNo()));
                excellBean.setTenderPackageName(CommonUtils.checkNull(tenderDetailsOffline.getPackageName()));
                if(tenderDetailsOffline.getTenderPubDate() != null){
                    excellBean.setTenderPublicationDate(CommonUtils.checkNull(DateUtils.formatStdDate(tenderDetailsOffline.getTenderPubDate())));
                }
                if(tenderDetailsOffline.getClosingDate() != null){
                    excellBean.setTenderClosingDate(CommonUtils.checkNull(DateUtils.convertStrToDate(tenderDetailsOffline.getClosingDate())));
                }

                if(tenderDetailsOffline.getLastSellingDate() != null){
                    excellBean.setTenderLastSellingDate(CommonUtils.checkNull(DateUtils.convertStrToDate(tenderDetailsOffline.getLastSellingDate())));
                }

                if(tenderDetailsOffline.getOpeningDate() != null){
                    excellBean.setTenderOpeningDate(CommonUtils.checkNull(DateUtils.convertStrToDate(tenderDetailsOffline.getOpeningDate())));
                }

                excellBean.setSellingTenderDocumentPrincipal(CommonUtils.checkNull(tenderDetailsOffline.getSellingAddPrinciple()));
                excellBean.setSellingTenderDocumentOthers(CommonUtils.checkNull(tenderDetailsOffline.getSellingAddOthers()));
                excellBean.setReceivingTenderDocument(CommonUtils.checkNull(tenderDetailsOffline.getReceivingAdd()));
                excellBean.setOpeningTenderDocument(CommonUtils.checkNull(tenderDetailsOffline.getOpeningAdd()));
                excellBean.setPlaceofTenderMeeting(CommonUtils.checkNull(tenderDetailsOffline.getPreTenderReoiplace()));

                if(tenderDetailsOffline.getClosingDate() != null){
                    excellBean.setDateTimeofTenderMeeting(CommonUtils.checkNull(DateUtils.convertStrToDate(tenderDetailsOffline.getPreTenderReoidate())));
                }
                excellBean.setEligibilityofTender(CommonUtils.checkNull(tenderDetailsOffline.getEligibilityCriteria()));
                excellBean.setBriefDescriptionofGoodsorWorks(CommonUtils.checkNull(tenderDetailsOffline.getBriefDescription()));
                excellBean.setBriefDescriptionofRelatedServices(CommonUtils.checkNull(tenderDetailsOffline.getRelServicesOrDeliverables()));
                excellBean.setEventType(CommonUtils.checkNull(tenderDetailsOffline.getEventType()));
                BigDecimal bd = tenderDetailsOffline.getDocumentPrice().setScale(2, 0);
                excellBean.setTenderDocumentPrice(CommonUtils.checkNull(bd.toString()));
                List<TblTenderLotPhasingOffline> tenderLotsAndPhases = tenderDetailsOffline.getTenderLotsAndPhases();
                 if(tenderDetailsOffline.getInvitationFor().equals("Single Lot")){
                     if(tenderLotsAndPhases != null){
                        TblTenderLotPhasingOffline lotPhasingOffline  =  tenderLotsAndPhases.get(0);
                        if(lotPhasingOffline != null){
                            excellBean.setLotNo_1(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                            excellBean.setLocation_1(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                            excellBean.setIdentificationOfLot_1(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                            System.out.println(" securityamt >> "+lotPhasingOffline.getTenderSecurityAmt().setScale(2, 0));
                            BigDecimal secamt = lotPhasingOffline.getTenderSecurityAmt().setScale(2, 0);
                            excellBean.setTenderSecurityAmount_1(CommonUtils.checkNull(secamt.toString()));
                            excellBean.setCompletionTimeInWeeksORmonths_1(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                        }
                     }
                 }else if(tenderDetailsOffline.getInvitationFor().equals("Multiple Lot")){
                       if(tenderLotsAndPhases != null){
                           TblTenderLotPhasingOffline lotPhasingOffline = null;
                           if(tenderLotsAndPhases.size() > 0 && tenderLotsAndPhases.get(0) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(0);
                                 excellBean.setLotNo_1(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 excellBean.setLocation_1(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 excellBean.setIdentificationOfLot_1(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 BigDecimal secamt1 = lotPhasingOffline.getTenderSecurityAmt().setScale(2, 0);
                                 excellBean.setTenderSecurityAmount_1(CommonUtils.checkNull(secamt1.toString()));
                                 excellBean.setCompletionTimeInWeeksORmonths_1(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 1 && tenderLotsAndPhases.get(1) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(1);
                                 excellBean.setLotNo_2(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 excellBean.setLocation_2(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 excellBean.setIdentificationOfLot_2(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 BigDecimal secamt2 = lotPhasingOffline.getTenderSecurityAmt().setScale(2, 0);
                                 excellBean.setTenderSecurityAmount_2(CommonUtils.checkNull(secamt2.toString()));
                                 excellBean.setCompletionTimeInWeeksORmonths_2(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 2 && tenderLotsAndPhases.get(2) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(2);
                                 excellBean.setLotNo_3(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 excellBean.setLocation_3(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 excellBean.setIdentificationOfLot_3(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 BigDecimal secamt3 = lotPhasingOffline.getTenderSecurityAmt().setScale(2, 0);
                                 excellBean.setTenderSecurityAmount_3(CommonUtils.checkNull(secamt3.toString()));
                                 excellBean.setCompletionTimeInWeeksORmonths_3(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 3 && tenderLotsAndPhases.get(3) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(3);
                                 excellBean.setLotNo_4(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 excellBean.setLocation_4(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 excellBean.setIdentificationOfLot_4(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 BigDecimal secamt4 = lotPhasingOffline.getTenderSecurityAmt().setScale(2, 0);
                                 excellBean.setTenderSecurityAmount_4(CommonUtils.checkNull(secamt4.toString()));
                                 excellBean.setCompletionTimeInWeeksORmonths_4(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                        }
                 }
                excellBean.setNameofOfficialInvitingTender(CommonUtils.checkNull(tenderDetailsOffline.getPeOfficeName()));
                excellBean.setDesignationofOfficialInvitingTender(CommonUtils.checkNull(tenderDetailsOffline.getPeDesignation()));
                excellBean.setAddressofOfficialInvitingTender(CommonUtils.checkNull(tenderDetailsOffline.getPeAddress()));
                excellBean.setContactdetailsofOfficialInvitingTender(CommonUtils.checkNull(tenderDetailsOffline.getPeContactDetails()));
                
               }

        }catch(Exception ex){
            System.out.println("editTenderWithPQForm : " + ex);
        }
        LOGGER.debug("editTenderWithPQForm : " + logUserId + LOGGEREND);
        return excellBean;
    }


     public EOIExcellBean editEOIForm(int tenderOLId){
        EOIExcellBean eOIExcellBean = new EOIExcellBean();
         LOGGER.debug("editTenderWithPQForm : " + logUserId + LOGGERSTART);
        try{
           TblTenderDetailsOffline tenderDetailsOffline  =  getTblTenderDetailOfflineDataById(tenderOLId);
           System.out.println("tenderDetailsOffline exists >> "+tenderDetailsOffline);
           if(tenderDetailsOffline != null){

                eOIExcellBean.setMinistryName(CommonUtils.checkNull(tenderDetailsOffline.getMinistryOrDivision()));
                eOIExcellBean.setAgency(CommonUtils.checkNull(tenderDetailsOffline.getAgency()));
                eOIExcellBean.setProcuringEntityName(CommonUtils.checkNull(tenderDetailsOffline.getPeName()));
                eOIExcellBean.setProcuringEntityCode(CommonUtils.checkNull(tenderDetailsOffline.getPeCode()));
                eOIExcellBean.setProcuringEntityDistrict(CommonUtils.checkNull(tenderDetailsOffline.getPeDistrict()));
                eOIExcellBean.setInterestForSelectionOfConsult(CommonUtils.checkNull(tenderDetailsOffline.getReoiRfpFor()));
                eOIExcellBean.setInterestForSelectionOfBased(CommonUtils.checkNull("Time based"));
                eOIExcellBean.setEoiRefNo(CommonUtils.checkNull(tenderDetailsOffline.getReoiRfpRefNo()));
                if(tenderDetailsOffline.getIssueDate() != null){
                    eOIExcellBean.setEoiDate(CommonUtils.checkNull(DateUtils.formatStdDate(tenderDetailsOffline.getIssueDate())));
                }

                eOIExcellBean.setProcurementSubmethod(CommonUtils.checkNull(tenderDetailsOffline.getProcurementMethod()));
                eOIExcellBean.setBudgetFund(CommonUtils.checkNull(tenderDetailsOffline.getBudgetType()));
                eOIExcellBean.setSourceFund(CommonUtils.checkNull(tenderDetailsOffline.getSourceOfFund()));
                eOIExcellBean.setDevelopmentPartner(CommonUtils.checkNull(tenderDetailsOffline.getDevPartners()));
                eOIExcellBean.setProjectOrProgramCode(CommonUtils.checkNull(tenderDetailsOffline.getProjectCode()));
                eOIExcellBean.setProjectOrProgrammeName(CommonUtils.checkNull(tenderDetailsOffline.getProjectName()));
              
               
                if(tenderDetailsOffline.getClosingDate() != null){
                    eOIExcellBean.setEoiClosingDateandtime(CommonUtils.checkNull(DateUtils.convertStrToDate(tenderDetailsOffline.getClosingDate())));
                }

             
                eOIExcellBean.setBriefDescriptionoftheAssignment(CommonUtils.checkNull(tenderDetailsOffline.getBriefDescription()));
                eOIExcellBean.setExpResDeliverCapacityRequired(CommonUtils.checkNull(tenderDetailsOffline.getRelServicesOrDeliverables()));
                eOIExcellBean.setOtherDetails(CommonUtils.checkNull(tenderDetailsOffline.getOtherDetails()));
                eOIExcellBean.setAssociationwithforeignfirms(CommonUtils.checkNull(tenderDetailsOffline.getForeignFirm()));
                eOIExcellBean.setNameoftheOfficialInvitingEOI(CommonUtils.checkNull(tenderDetailsOffline.getPeOfficeName()));
                eOIExcellBean.setDesignationoftheOfficialInvitingEOI(CommonUtils.checkNull(tenderDetailsOffline.getPeDesignation()));
                eOIExcellBean.setAddressoftheOfficialInvitingEOI(CommonUtils.checkNull(tenderDetailsOffline.getPeAddress()));
                eOIExcellBean.setContactDetailsoftheOfficialInvitingEOI(CommonUtils.checkNull(tenderDetailsOffline.getPeContactDetails()));

                List<TblTenderLotPhasingOffline> tenderLotsAndPhases = tenderDetailsOffline.getTenderLotsAndPhases();
              
                       if(tenderLotsAndPhases != null && tenderLotsAndPhases.size() >0){
                           TblTenderLotPhasingOffline lotPhasingOffline = null;
                           if(tenderLotsAndPhases.size() > 0 && tenderLotsAndPhases.get(0) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(0);
                                 eOIExcellBean.setRefNo_1(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 eOIExcellBean.setLocation_1(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 eOIExcellBean.setPhasingOfServices_1(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 eOIExcellBean.setIndicativeStartDateMonOrYer_1(CommonUtils.checkNull(lotPhasingOffline.getStartDateTime()));
                                 eOIExcellBean.setIndicativeCompletionDateMonOrYer_1(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 1 && tenderLotsAndPhases.get(1) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(1);
                                 eOIExcellBean.setRefNo_2(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 eOIExcellBean.setLocation_2(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 eOIExcellBean.setPhasingOfServices_2(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 eOIExcellBean.setIndicativeStartDateMonOrYer_2(CommonUtils.checkNull(lotPhasingOffline.getStartDateTime()));
                                 eOIExcellBean.setIndicativeCompletionDateMonOrYer_2(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 2 && tenderLotsAndPhases.get(2) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(2);
                                 eOIExcellBean.setRefNo_3(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 eOIExcellBean.setLocation_3(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 eOIExcellBean.setPhasingOfServices_3(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 eOIExcellBean.setIndicativeStartDateMonOrYer_3(CommonUtils.checkNull(lotPhasingOffline.getStartDateTime()));
                                 eOIExcellBean.setIndicativeCompletionDateMonOrYer_3(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 3 && tenderLotsAndPhases.get(3) != null){
                                lotPhasingOffline = tenderLotsAndPhases.get(3);
                                 eOIExcellBean.setRefNo_4(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 eOIExcellBean.setLocation_4(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 eOIExcellBean.setPhasingOfServices_4(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 eOIExcellBean.setIndicativeStartDateMonOrYer_4(CommonUtils.checkNull(lotPhasingOffline.getStartDateTime()));
                                 eOIExcellBean.setIndicativeCompletionDateMonOrYer_4(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 4 && tenderLotsAndPhases.get(4) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(4);
                                 eOIExcellBean.setRefNo_5(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 eOIExcellBean.setLocation_5(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 eOIExcellBean.setPhasingOfServices_5(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 eOIExcellBean.setIndicativeStartDateMonOrYer_5(CommonUtils.checkNull(lotPhasingOffline.getStartDateTime()));
                                 eOIExcellBean.setIndicativeCompletionDateMonOrYer_5(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 5 && tenderLotsAndPhases.get(5) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(5);
                                 eOIExcellBean.setRefNo_6(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 eOIExcellBean.setLocation_6(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 eOIExcellBean.setPhasingOfServices_6(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 eOIExcellBean.setIndicativeStartDateMonOrYer_6(CommonUtils.checkNull(lotPhasingOffline.getStartDateTime()));
                                 eOIExcellBean.setIndicativeCompletionDateMonOrYer_6(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 6 && tenderLotsAndPhases.get(6) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(6);
                                 eOIExcellBean.setRefNo_7(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 eOIExcellBean.setLocation_7(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 eOIExcellBean.setPhasingOfServices_7(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 eOIExcellBean.setIndicativeStartDateMonOrYer_7(CommonUtils.checkNull(lotPhasingOffline.getStartDateTime()));
                                 eOIExcellBean.setIndicativeCompletionDateMonOrYer_7(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 7 && tenderLotsAndPhases.get(7) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(7);
                                 eOIExcellBean.setRefNo_8(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 eOIExcellBean.setLocation_8(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 eOIExcellBean.setPhasingOfServices_8(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 eOIExcellBean.setIndicativeStartDateMonOrYer_8(CommonUtils.checkNull(lotPhasingOffline.getStartDateTime()));
                                 eOIExcellBean.setIndicativeCompletionDateMonOrYer_8(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                           if(tenderLotsAndPhases.size() > 8 && tenderLotsAndPhases.get(8) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(8);
                                 eOIExcellBean.setRefNo_9(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 eOIExcellBean.setLocation_9(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 eOIExcellBean.setPhasingOfServices_9(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 eOIExcellBean.setIndicativeStartDateMonOrYer_9(CommonUtils.checkNull(lotPhasingOffline.getStartDateTime()));
                                 eOIExcellBean.setIndicativeCompletionDateMonOrYer_9(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                            if(tenderLotsAndPhases.size() > 9 && tenderLotsAndPhases.get(9) != null){
                                 lotPhasingOffline = tenderLotsAndPhases.get(9);
                                 eOIExcellBean.setRefNo_10(CommonUtils.checkNull(lotPhasingOffline.getLotOrRefNo()));
                                 eOIExcellBean.setLocation_10(CommonUtils.checkNull(lotPhasingOffline.getLocation()));
                                 eOIExcellBean.setPhasingOfServices_10(CommonUtils.checkNull(lotPhasingOffline.getLotIdentOrPhasingServ()));
                                 eOIExcellBean.setIndicativeStartDateMonOrYer_10(CommonUtils.checkNull(lotPhasingOffline.getStartDateTime()));
                                 eOIExcellBean.setIndicativeCompletionDateMonOrYer_10(CommonUtils.checkNull(lotPhasingOffline.getCompletionDateTime()));
                            }
                        }
               
               }

        }catch(Exception ex){
            System.out.println("editTenderWithPQForm : " + ex);
        }
        LOGGER.debug("editTenderWithPQForm : " + logUserId + LOGGEREND);
        return eOIExcellBean;
    }

      public ContractAwardExcellBean editAwardForm(int awardolId){
        ContractAwardExcellBean  awardExcellBean = new ContractAwardExcellBean();
         LOGGER.debug("editTenderWithPQForm : " + logUserId + LOGGERSTART);
        try{
           TblContractAwardedOffline contractAwardedOffline =  getTblContractAwardedOfflineById(awardolId);
           System.out.println("contractAwardedOffline exists >> "+contractAwardedOffline);
           if(contractAwardedOffline != null){
                awardExcellBean.setMinistryName(CommonUtils.checkNull(contractAwardedOffline.getMinistry()));
                awardExcellBean.setAgency(CommonUtils.checkNull(contractAwardedOffline.getAgency()));
                awardExcellBean.setProcuringEntityName(CommonUtils.checkNull(contractAwardedOffline.getPeOfficeName()));
                awardExcellBean.setProcuringEntityCode(CommonUtils.checkNull(contractAwardedOffline.getPeCode()));
                awardExcellBean.setProcuringEntityDistrict(CommonUtils.checkNull(contractAwardedOffline.getPeDistrict()));
                awardExcellBean.setContractawardfor(CommonUtils.checkNull(contractAwardedOffline.getAwardForPnature()));
                awardExcellBean.setInvitPropRefNo(CommonUtils.checkNull(contractAwardedOffline.getRefNo()));
                awardExcellBean.setProcurementMethod(CommonUtils.checkNull(contractAwardedOffline.getProcurementMethod()));
                awardExcellBean.setBudgetFund(CommonUtils.checkNull(contractAwardedOffline.getBudgetType()));
                awardExcellBean.setSourceFund(CommonUtils.checkNull(contractAwardedOffline.getSourceOfFund()));
                awardExcellBean.setDevelopmentPartner(CommonUtils.checkNull(contractAwardedOffline.getDevPartners()));
                awardExcellBean.setProjectOrProgrammeCode(CommonUtils.checkNull(contractAwardedOffline.getProjectCode()));
                awardExcellBean.setProjectOrProgrammeName(CommonUtils.checkNull(contractAwardedOffline.getProjectName()));
                awardExcellBean.setTenderPackageName(CommonUtils.checkNull(contractAwardedOffline.getPackageName()));
                awardExcellBean.setTenderPackageNo(CommonUtils.checkNull(contractAwardedOffline.getPackageNo()));
                
                if(contractAwardedOffline.getDateofAdvertisement() != null)
                awardExcellBean.setDateOfAdvertisement(CommonUtils.checkNull(DateUtils.formatStdDate(contractAwardedOffline.getDateofAdvertisement())));
                if(contractAwardedOffline.getDateofNoa() != null)
                awardExcellBean.setDateofNotificationofAward(CommonUtils.checkNull(DateUtils.formatStdDate(contractAwardedOffline.getDateofNoa())));
                if(contractAwardedOffline.getDateofContractSign() != null)
                awardExcellBean.setDateofContractSigning(CommonUtils.checkNull((DateUtils.formatStdDate(contractAwardedOffline.getDateofContractSign()))));
                if(contractAwardedOffline.getDateofPccompletion() != null)
                awardExcellBean.setProposedDateofContractCompletion(CommonUtils.checkNull((DateUtils.formatStdDate(contractAwardedOffline.getDateofPccompletion()))));
                
                awardExcellBean.setNoofTendersProposalsSold(CommonUtils.checkNull(contractAwardedOffline.getSold()));
                awardExcellBean.setNoofTendersProposalsReceived(CommonUtils.checkNull(contractAwardedOffline.getReceived()));
                awardExcellBean.setNoofResponsiveTendersProposals(CommonUtils.checkNull(contractAwardedOffline.getResponse()));
                awardExcellBean.setBriefDescriptionofContract(CommonUtils.checkNull(contractAwardedOffline.getDescriptionofContract()));
                
                if(contractAwardedOffline.getContractValue() != null && !"".equals(contractAwardedOffline.getContractValue())){
                    BigDecimal convalue = contractAwardedOffline.getContractValue().setScale(0, 0);
                    awardExcellBean.setContractValue(CommonUtils.checkNull(convalue.toString()));
                }
                awardExcellBean.setNameofSupplierORContractorORConsultant(CommonUtils.checkNull(contractAwardedOffline.getNameofTenderer()));
                awardExcellBean.setLocationofSupplierORContractorORConsultant(CommonUtils.checkNull(contractAwardedOffline.getAddressofTenderer()));

                awardExcellBean.setLocationofDeliveryORWorksORConsultancy(CommonUtils.checkNull(contractAwardedOffline.getDeliveryPlace()));
                if(contractAwardedOffline.getIsSamePersonNoa() != null){
                    String sameperson = "No";
                    if(contractAwardedOffline.getIsSamePersonNoa() == 1)
                        sameperson = "Yes";
                     awardExcellBean.setIstheContractSignedwiththesamepersonstatedintheNOA(CommonUtils.checkNull(sameperson));
               }
               
                awardExcellBean.setIfNogivereasonwhy_NOA(CommonUtils.checkNull(contractAwardedOffline.getNoareason()));

                
                if(contractAwardedOffline.getIsPsecurityDueTime() != null ){
                    String psduetime = "No";
                    if(contractAwardedOffline.getIsPsecurityDueTime() == 1){
                         psduetime = "Yes";
                    }
                  awardExcellBean.setWasthePerformanceSecurityprovidedinduetime(CommonUtils.checkNull(psduetime));
                }
                
                awardExcellBean.setIfNoGiveReasonWhy_Performance_Security(CommonUtils.checkNull(contractAwardedOffline.getPsecurityReason()));

                if(contractAwardedOffline.getIsSignedDueTime() != null){
                    String signedduetime = "No";
                     if(contractAwardedOffline.getIsSignedDueTime() == 1){
                         signedduetime = "Yes";
                    }
                  awardExcellBean.setWastheContractSignedinduetime(signedduetime);
                }

                awardExcellBean.setIfNoGiveReasonWhy_Contract_signed(CommonUtils.checkNull(contractAwardedOffline.getSignedReason()));
                awardExcellBean.setNameofAuthorisedOfficer(CommonUtils.checkNull(contractAwardedOffline.getOfficerName()));
                awardExcellBean.setDesignationofAuthorisedOfficer(CommonUtils.checkNull(contractAwardedOffline.getOfficerDesignation()));
               }
             
        }catch(Exception ex){
            System.out.println("editTenderWithPQForm : " + ex);
        }
        LOGGER.debug("editTenderWithPQForm : " + logUserId + LOGGEREND);
        return awardExcellBean;
    }



    public String getOrganizationByMinistry(String miniName,String excelagency){
    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
    DepartmentCreationService departmentCreationService = (DepartmentCreationService) AppContext.getSpringBean("DepartmentCreationService");
    String orgName = "";
     if(miniName != null && !"".equals(miniName) && !miniName.equals("<select>") && !miniName.equals("</select>")){
        List<TblDepartmentMaster> ministries =  departmentCreationService.getMinstriesByDeptyTypeAndDeptName("Ministry", miniName);
          if(ministries != null && ministries.size() > 0){
            TblDepartmentMaster ministry = ministries.get(0);
            List<TblDepartmentMaster> departmentMasters = commonService.getOrganization(ministry.getDepartmentId());
            if(departmentMasters != null && departmentMasters.size() > 0){
                StringBuffer sb = new StringBuffer();
                 for (TblDepartmentMaster tblDepartmentMaster : departmentMasters) {
                     if(excelagency != null && excelagency.equals(tblDepartmentMaster.getDepartmentName()))
                         sb.append("<option selected=\"selected\" value='" + tblDepartmentMaster.getDepartmentId() + "'>" + tblDepartmentMaster.getDepartmentName() + "</option>");
                         else
                        sb.append("<option value='" + tblDepartmentMaster.getDepartmentId() + "'>" + tblDepartmentMaster.getDepartmentName() + "</option>");
                    }
                 orgName = sb.toString();
                 //request.setAttribute("agencycombo", sb.toString());
              }else{
                StringBuffer sb = new StringBuffer();
                sb.append("<option value=\"select\" selected=\"selected\">--- Please Select ---</option>");
                orgName = sb.toString();
               // request.setAttribute("agencycombo", sb.toString());
              }
        }else{
            StringBuffer sb = new StringBuffer();
            sb.append("<option value=\"select\" selected=\"selected\">--- Please Select ---</option>");
            orgName = sb.toString();
        }
    }else{
            StringBuffer sb = new StringBuffer();
            sb.append("<option value=\"select\" selected=\"selected\">--- Please Select ---</option>");
            orgName = sb.toString();
        }
    return orgName;
    }

    public List<TblCorrigendumDetailOffline> corrigendumDetails(int tenderOfflineID) throws Exception {
        LOGGER.debug("corrigendumDetails : " + logUserId + "starts");

        List<TblCorrigendumDetailOffline> corrigendumDetails = null;
        try {
            corrigendumDetails = corrigendumDetailsOfflineService.findCorrigendumDetailOffline("tblCorrigendumDetailOffline", Operation_enum.EQ, new TblTenderDetailsOffline(tenderOfflineID)) ;
        } catch (Exception e) {
            LOGGER.error("corrigendumDetails : " + e);
    }

        LOGGER.debug("corrigendumDetails : " + logUserId + "ends");
        return corrigendumDetails;

    }

}
