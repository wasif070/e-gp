/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblFinancialYear;
import com.cptu.egp.eps.service.serviceinterface.FinancialYearService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class FinancialYearServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger LOGGER = Logger.getLogger(FinancialYearServlet.class);
    FinancialYearService financialyearservice = (FinancialYearService) AppContext.getSpringBean("financialYearService");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String logUserId = "0";
        try {
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                financialyearservice.setLogUserId(logUserId);
            }
            LOGGER.debug("processRequest : "+logUserId+" Starts");
            String funName = request.getParameter("funName");
            if (funName.equalsIgnoreCase("viewfinancialyeardetails")) {
                LOGGER.debug("viewfinancialyeardetails : "+logUserId+" Starts");
                String styleClass = "";
                String strPageNo = request.getParameter("pageNo");

                int pageNo = Integer.parseInt(strPageNo);
                int recordOffset = Integer.parseInt(request.getParameter("size"));            
                int offset = ((pageNo - 1) * recordOffset);


                List<TblFinancialYear> list = financialyearservice.getallFinancialYearDetails(offset,recordOffset);
                int totalcount =  (int)financialyearservice.countallFinancialYearDetails();
                if (list != null && !list.isEmpty()) {
                    for (int i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }                            
                            String action ="<a href=FinancialYear.jsp?id="+list.get(i).getFinancialYearId()+">Edit</a>";
                            out.print("<tr class=" + styleClass + ">");
                            out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 20 + (i + 1)) + "</td>");
                            out.print("<td class=\"t-align-left\">" + list.get(i).getFinancialYear() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getIsCurrent() + "</td>");
                            out.print("<td class=\"t-align-center\">" + action + "</td>");
                            out.print("</tr>");
                    }
                    int totalPages = 1;
                    if (!list.isEmpty()) {
                        int count = totalcount;
                        totalPages = (int) Math.ceil(Math.ceil(count) / recordOffset);
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                } else {
                    out.print("<tr>");

                    out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
               LOGGER.debug("viewfinancialyeardetails : "+logUserId+" Ends");
            }

        } finally { 
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
