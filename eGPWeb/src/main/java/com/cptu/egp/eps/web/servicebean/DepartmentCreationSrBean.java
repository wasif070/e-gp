/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblStateMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.DepartmentCreationService;
import com.cptu.egp.eps.web.databean.DepartmentCreationDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.BanglaNameUtils;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author rishita
 */
public class DepartmentCreationSrBean {

    private String logUserId = "0";
    final static Logger LOGGER = Logger.getLogger(DepartmentCreationSrBean.class);
    private final CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
    private final DepartmentCreationService departmentCreationService = (DepartmentCreationService) AppContext.getSpringBean("DepartmentCreationService");
    List<SelectItem> countryList = new ArrayList<SelectItem>();
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        departmentCreationService.setAuditTrail(auditTrail);
    }

    /**
     * For logging purpose
     *
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        commonService.setUserId(logUserId);
        departmentCreationService.setUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * get listing of ministry
     *
     * @param deptId primary key
     * @return list of TblDepartmentMaster
     */
    public List<TblDepartmentMaster> getDefaultMinistryList(short deptId) {
        return commonService.getMinistryList(deptId);
    }

    /**
     * get listing of division
     *
     * @return list of TblDepartmentMaster
     */
    public List<TblDepartmentMaster> getDefaultDivisionList() {
        return commonService.getDivisionDefaultList();
    }

    /**
     * get country listing from Tbl_CountryMaster
     *
     * @return list of countries
     */
    public List<SelectItem> getCountryMaster() {
        LOGGER.debug("getCountryMaster : " + logUserId + "starts");
        if (countryList.isEmpty()) {
            try {
                for (TblCountryMaster tblCountryMaster : commonService.countryMasterList()) {
                    countryList.add(new SelectItem(tblCountryMaster.getCountryId(), tblCountryMaster.getCountryName()));
                }
            } catch (Exception e) {
                LOGGER.error("getCountryMaster : " + e);
            }
        }
        LOGGER.debug("getCountryMaster : " + logUserId + "ends");
        return countryList;
    }

    public void setCountryMaster(List<SelectItem> countryList) {
        this.countryList = countryList;
    }

    /**
     * Create Department (Ministry,Division,Organization)
     *
     * @param departmentCreationDtBean
     * @return true if department created else false
     * @throws UnsupportedEncodingException
     */
    public boolean deparementCreation(DepartmentCreationDtBean departmentCreationDtBean) throws UnsupportedEncodingException {
        LOGGER.debug("deparementCreation : " + logUserId + "starts");
        boolean flag = false;
        try {
            TblDepartmentMaster tblDepartmentMaster = _toTblDepartmentMaster(departmentCreationDtBean);
            short pid;
            if (departmentCreationDtBean.getDepartmentType().equals("Ministry")) {
                pid = 1;
                tblDepartmentMaster.setParentDepartmentId(pid);
                tblDepartmentMaster.setDepartmentHirarchy(departmentCreationDtBean.getDepartmentName());
                tblDepartmentMaster.setOrganizationType("Large");
            } else if (departmentCreationDtBean.getDepartmentType().equals("Autonomus")) {
                pid = 2;
                tblDepartmentMaster.setParentDepartmentId(pid);
                tblDepartmentMaster.setDepartmentHirarchy(departmentCreationDtBean.getDepartmentName());
                tblDepartmentMaster.setOrganizationType("Large");
            } else if (departmentCreationDtBean.getDepartmentType().equals("Division")) {
                tblDepartmentMaster.setParentDepartmentId(departmentCreationDtBean.getDeptParentId());
                String deptHirarchy = departmentCreationDtBean.getDepartmentName() + ">" + departmentCreationDtBean.getDeptHirarchy();
                tblDepartmentMaster.setDepartmentHirarchy(deptHirarchy);
                tblDepartmentMaster.setOrganizationType("Large");
            }else if (departmentCreationDtBean.getDepartmentType().equals("District")) {
                tblDepartmentMaster.setParentDepartmentId(departmentCreationDtBean.getDeptParentId());
                String deptHirarchy = departmentCreationDtBean.getDepartmentName() + ">" + departmentCreationDtBean.getDeptHirarchy();
                tblDepartmentMaster.setDepartmentHirarchy(deptHirarchy);
                tblDepartmentMaster.setOrganizationType("Large");
            }else if (departmentCreationDtBean.getDepartmentType().equals("SubDistrict")) {
                tblDepartmentMaster.setParentDepartmentId(departmentCreationDtBean.getDeptParentId());
                String deptHirarchy = departmentCreationDtBean.getDepartmentName() + ">" + departmentCreationDtBean.getDeptHirarchyMD();
                tblDepartmentMaster.setDepartmentHirarchy(deptHirarchy);
                tblDepartmentMaster.setOrganizationType("Large");
            }else if (departmentCreationDtBean.getDepartmentType().equals("Gewog")) {
                tblDepartmentMaster.setParentDepartmentId(departmentCreationDtBean.getDeptParentId());
                String deptHirarchy = departmentCreationDtBean.getDepartmentName() + ">" + departmentCreationDtBean.getDeptHirarchyMD();
                tblDepartmentMaster.setDepartmentHirarchy(deptHirarchy);
                tblDepartmentMaster.setOrganizationType("Large");
            } else {
                tblDepartmentMaster.setParentDepartmentId(departmentCreationDtBean.getDeptParentId());
                String deptHirarchy = departmentCreationDtBean.getDepartmentName() + ">" + departmentCreationDtBean.getDeptHirarchyMD();
                tblDepartmentMaster.setDepartmentHirarchy(deptHirarchy);
                tblDepartmentMaster.setOrganizationType(departmentCreationDtBean.getOrganizationType());//default
            }
            if (departmentCreationDtBean.getPhoneNo() == null || departmentCreationDtBean.getPhoneNo().trim().equals("")) {
                tblDepartmentMaster.setPhoneNo("");
            }
            tblDepartmentMaster.setCreatedBy(1);
            tblDepartmentMaster.setCreatedDate(new Date());
            tblDepartmentMaster.setStatus("Approved");
            //tblDepartmentMaster.setApprovingAuthorityId(departmentCreationDtBean.getApprovingAuthId());
            tblDepartmentMaster.setApprovingAuthorityId(0);
            tblDepartmentMaster.setTblCountryMaster(new TblCountryMaster(departmentCreationDtBean.getCountryId()));
            tblDepartmentMaster.setTblStateMaster(new TblStateMaster(departmentCreationDtBean.getStateId()));
            if (departmentCreationDtBean.getDeptBanglaString() == null || departmentCreationDtBean.getDeptBanglaString().trim().equals("")) {
                tblDepartmentMaster.setDeptNameInBangla(null);
            } else {
                tblDepartmentMaster.setDeptNameInBangla(BanglaNameUtils.getBytes(departmentCreationDtBean.getDeptBanglaString()));
            }
            if (departmentCreationDtBean.getFaxNo() == null || departmentCreationDtBean.getFaxNo().trim().equals("")) {
                tblDepartmentMaster.setFaxNo("");
            }
            if (departmentCreationDtBean.getCity() == null || departmentCreationDtBean.getCity().trim().equals("")) {
                tblDepartmentMaster.setCity("");
            }
            if (departmentCreationDtBean.getPostCode() == null || departmentCreationDtBean.getPostCode().trim().equals("")) {
                tblDepartmentMaster.setPostCode("");
            }
            if (departmentCreationDtBean.getMobileNo() == null || departmentCreationDtBean.getMobileNo().trim().equals("")) {
                tblDepartmentMaster.setMobileNo("");
            }
            if (departmentCreationDtBean.getWebsite() == null || departmentCreationDtBean.getWebsite().trim().equals("")) {
                tblDepartmentMaster.setWebsite("");
            }
            if (departmentCreationService.addDepartmentCreation(tblDepartmentMaster)) {
                flag = true;
                setDeptid(tblDepartmentMaster.getDepartmentId());
            }
        } catch (Exception e) {
            LOGGER.error("deparementCreation : " + logUserId + e);
        }
        LOGGER.debug("deparementCreation : " + logUserId + "ends");
        return flag;
    }
    private short deptid;

    public short getDeptid() {
        return deptid;
    }

    public void setDeptid(short deptid) {
        this.deptid = deptid;
    }

    /**
     * copies properties
     *
     * @param departmentCreationDtBean
     * @return Object with copied properties
     */
    public TblDepartmentMaster _toTblDepartmentMaster(DepartmentCreationDtBean departmentCreationDtBean) {
        LOGGER.debug("_toTblDepartmentMaster : " + logUserId + "starts");
        TblDepartmentMaster tblDepartmentMaster = new TblDepartmentMaster();
        try {
            BeanUtils.copyProperties(departmentCreationDtBean, tblDepartmentMaster);
        } catch (Exception e) {
            LOGGER.error("_toTblDepartmentMaster : " + logUserId + e);
        }

        LOGGER.debug("_toTblDepartmentMaster : " + logUserId + "ends");
        return tblDepartmentMaster;
    }
    private List<SelectItem> stateList = new ArrayList<SelectItem>();

    /**
     * get listing of state from Tbl_StateMaster
     *
     * @return list of states
     */
    public List<SelectItem> getStateList() {
        LOGGER.debug("getStateList : " + logUserId + "starts");
        try {
            if (stateList.isEmpty()) {
                List<TblStateMaster> stateMasters = commonService.getState((short) 150);
                for (TblStateMaster stateMaster : stateMasters) {
                    stateList.add(new SelectItem(stateMaster.getStateId(), stateMaster.getStateName()));
                }
            }
        } catch (Exception e) {
            LOGGER.error("getStateList : " + logUserId + e);
        }
        LOGGER.debug("getStateList : " + logUserId + "ends");
        return stateList;
    }

    public void setStateList(List<SelectItem> stateList) {
        this.stateList = stateList;
    }
}
