/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//Search formation changed by Emtaz on 16/May/2016

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.view.VwGetSbDevPartner;
import com.cptu.egp.eps.web.servicebean.ManageSbDpUserSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.web.utility.AppContext;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class ManageSbDpUserServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            //<editor-fold>
            /*if (request.getParameter("action").equals("fetchData")) {
                response.setContentType("text/xml;charset=UTF-8");
                System.out.println(">" + request.getQueryString());
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                String type = request.getParameter("userType");
//                if (sidx.equals("")) {
//                    sidx = "fullName";
//                }
                String userTypeId = "";
                if (type.equalsIgnoreCase("Development")) {
                    userTypeId = "6";
                } else {
                    userTypeId = "7";
                }
                //dcBean.setColName("");
                ManageSbDpUserSrBean manageSbDpAdminSrBean = new ManageSbDpUserSrBean();
                manageSbDpAdminSrBean.setSearch(_search);
                manageSbDpAdminSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                System.out.println("offset:" + +offset);

                manageSbDpAdminSrBean.setOffset(offset);
                manageSbDpAdminSrBean.setSortOrder(sord);
                manageSbDpAdminSrBean.setSortCol(sidx);

                System.out.println("queryString:" + request.getQueryString());

                HttpSession session = request.getSession();
                Object objUserTypeId = session.getAttribute("userTypeId");
                Object objUserId = session.getAttribute("userId");

                List<VwGetSbDevPartner> vwGetAdminList = null;
                List<Object[]> bankAdminList = null;
                String searchField = "", searchString = "", searchOper = "";
                if (_search) {
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    if (Integer.parseInt(objUserId.toString()) > 1) {

                        bankAdminList = manageSbDpAdminSrBean.getUserListAdmin(type, Integer.parseInt(objUserTypeId.toString()), Integer.parseInt(objUserId.toString()), searchField, searchString, searchOper);
                    } else {
                        vwGetAdminList = manageSbDpAdminSrBean.getUserList(type, Integer.parseInt(objUserTypeId.toString()), Integer.parseInt(objUserId.toString()), searchField, searchString, searchOper);
                    }
                } else {
                    if (Integer.parseInt(objUserId.toString()) > 1) {
                        bankAdminList = manageSbDpAdminSrBean.getUserListAdmin(type, Integer.parseInt(objUserTypeId.toString()), Integer.parseInt(objUserId.toString()), searchField, searchString, searchOper);
                    bankAdminList.size();
                    } else {
                        vwGetAdminList = manageSbDpAdminSrBean.getUserList(type, Integer.parseInt(objUserTypeId.toString()), Integer.parseInt(objUserId.toString()));
                    }
                }

                //System.out.println("SB-DP UserList  size:" + vwGetAdminList.size());
                int totalPages = 0;
                long totalCount = 0;
                if (_search) {
                    totalCount = manageSbDpAdminSrBean.getAllCountOfDept(type, searchField, searchString, searchOper);
                } else {
                    totalCount = manageSbDpAdminSrBean.getAllCountOfDept(type, Integer.parseInt(objUserId.toString()), Integer.parseInt(objUserTypeId.toString()));
                }

                System.out.println("totalCount : " + totalCount);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(request.getParameter("rows")));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(request.getParameter("rows"))) + 1);
                    }
                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                if (session.getAttribute("userId").toString().trim().equals("1")) {
                    out.print("<records>" + vwGetAdminList.size() + "</records>");
                    // be sure to put text data in CDATA
                    int j = 0;
                    int no = Integer.parseInt(request.getParameter("page"));
                    if (no == 1) {
                        j = 1;
                    } else {
                        if (Integer.parseInt(rows) == 30) {
                            j = ((no - 1) * 30) + 1;
                        } else if (Integer.parseInt(rows) == 20) {
                            j = ((no - 1) * 20) + 1;
                        } else {
                            j = ((no - 1) * 10) + 1;
                        }
                        }
                    String str_role = null;
                    for (int i = 0; i < vwGetAdminList.size(); i++) {

                        out.print("<row id='" + vwGetAdminList.get(i).getId().getUserId() + "'>");
                        out.print("<cell><![CDATA[" + j + "]]></cell>");
                        out.print("<cell><![CDATA[" + vwGetAdminList.get(i).getId().getFullName() + "]]></cell>");
                        if(!"6".equals(userTypeId)){
                            if("BranchMaker".equalsIgnoreCase(vwGetAdminList.get(i).getId().getIsMakerChecker())){
                               str_role = "Branch Maker"; 
                               out.print("<cell><![CDATA[" + str_role + "]]></cell>");
                            }
                            else if("BranchChecker".equalsIgnoreCase(vwGetAdminList.get(i).getId().getIsMakerChecker())){
                               str_role = "Branch Checker"; 
                               out.print("<cell><![CDATA[" + str_role + "]]></cell>");
                            }else{
                        out.print("<cell><![CDATA[" + vwGetAdminList.get(i).getId().getIsMakerChecker() + "]]></cell>");
                            }
                        }
                        out.print("<cell><![CDATA[" + vwGetAdminList.get(i).getId().getEmailId() + "]]></cell>");
                        out.print("<cell><![CDATA[" + vwGetAdminList.get(i).getId().getSbDevelopName() + "]]></cell>");
                        String editLink = "<a href=\"EditSbDevPartUser.jsp?userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&mode=edit\">Edit</a>";
                        String viewLink = "<a href=\"ViewSbDevPartUser.jsp?userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&mode=View\">View</a>";
                        String replaceLink = "<a href=\"ReplaceUser.jsp?userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId=" + userTypeId + "\">Transfer</a> | ";
                        String strUserStatus = vwGetAdminList.get(i).getId().getStatus();
                        String strManageSbDevUserLink = "";
                        if ("approved".equalsIgnoreCase(strUserStatus)) {
                            strManageSbDevUserLink = "<a href=\"ManageSbDevPartUser.jsp?userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&userOldStatus=" + strUserStatus + "&partnerId="+vwGetAdminList.get(i).getId().getPartnerId()+ "&fullName="+vwGetAdminList.get(i).getId().getFullName()+ "\">Deactivate</a>";
                        } else {
                            strManageSbDevUserLink = "<a href=\"ManageSbDevPartUser.jsp?userId=" + vwGetAdminList.get(i).getId().getUserId() + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&userOldStatus=" + strUserStatus + "&partnerId="+vwGetAdminList.get(i).getId().getPartnerId()+ "&fullName="+vwGetAdminList.get(i).getId().getFullName()+ "\">Activate</a>";
                        }

                        out.print("<cell><![CDATA[" + editLink + " | " + viewLink + " | " + replaceLink + "<br />" + strManageSbDevUserLink + "]]></cell>");
                        out.print("</row>");
                        j++;
                    }
                } else {

                    out.print("<records>" + bankAdminList.size() + "</records>");
                    // be sure to put text data in CDATA
                    int j = 0;
                    int no = Integer.parseInt(request.getParameter("page"));
                    if (no == 1) {
                        j = 1;
                    } else {
                        if (Integer.parseInt(rows) == 30) {
                            j = ((no - 1) * 30) + 1;
                        } else if (Integer.parseInt(rows) == 20) {
                            j = ((no - 1) * 20) + 1;
                        } else {
                            j = ((no - 1) * 10) + 1;
                        }
                        }
                    for (int i = 0; i < bankAdminList.size(); i++) {

                        out.print("<row id='" + bankAdminList.get(i)[2] + "'>");
                         out.print("<cell><![CDATA[" + j + "]]></cell>");
                        out.print("<cell><![CDATA[" + bankAdminList.get(i)[0] + "]]></cell>");
                        if(!"Development".equalsIgnoreCase(type)){
                        out.print("<cell><![CDATA[" + bankAdminList.get(i)[4] + "]]></cell>");
                        }
                        out.print("<cell><![CDATA[" + bankAdminList.get(i)[1] + "]]></cell>");
                        out.print("<cell><![CDATA[" + bankAdminList.get(i)[3] + "]]></cell>");
                        String editLink = "<a href=\"EditSbDevPartUser.jsp?userId=" + bankAdminList.get(i)[2] + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&mode=edit\">Edit</a>";
                        String viewLink = "<a href=\"ViewSbDevPartUser.jsp?userId=" + bankAdminList.get(i)[2] + "&partnerType=" + type + "&userTypeId=" + userTypeId + "\">View</a>";
                        String replaceLink = "<a href=\"ReplaceUser.jsp?userId=" + bankAdminList.get(i)[2] + "&partnerType=" + type + "&userTypeId=" + userTypeId + "\">Transfer</a>";
                        String strUserStatus = (String) bankAdminList.get(i)[5];
                        String strManageSbDevUserLink = "";
                        if ("approved".equalsIgnoreCase(strUserStatus)) {
                            strManageSbDevUserLink = "<a href=\"ManageSbDevPartUser.jsp?userId=" + bankAdminList.get(i)[2] + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&userOldStatus=" + strUserStatus + "&partnerId="+bankAdminList.get(i)[6]+ "&fullName="+bankAdminList.get(i)[0]+ "\">Deactivate</a>";
                        } else {
                            strManageSbDevUserLink = "<a href=\"ManageSbDevPartUser.jsp?userId=" +bankAdminList.get(i)[2] + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&userOldStatus=" + strUserStatus + "&partnerId="+bankAdminList.get(i)[6]+ "&fullName="+bankAdminList.get(i)[0]+ "\">Activate</a>";
                        }
                        out.print("<cell><![CDATA[" + editLink + " | " + viewLink + " | " + replaceLink + "<br />" + strManageSbDevUserLink + "]]></cell>");
                        out.print("</row>");
                        j++;
                    }

                }
                out.print("</rows>");
                //out.flush();
            }*/
            //</editor-fold>
            if (request.getParameter("action").equals("fetchData")) {
                
                System.out.println(">" + request.getQueryString());
              
                String type = request.getParameter("userType");

                String userTypeId = "";
                if (type.equalsIgnoreCase("Development")) {
                    userTypeId = "6";
                } else {
                    userTypeId = "7";
                }
                ManageSbDpUserSrBean manageSbDpAdminSrBean = new ManageSbDpUserSrBean();
                manageSbDpAdminSrBean.setLimit(1000000);
                int offset = 0;

                manageSbDpAdminSrBean.setOffset(offset);
                
                // Find all bank names START
                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> BankList = tenderCommonService.returndata("getBankBranchList", "0", "");
                // Find all bank names END
                
                int pageNo = Integer.parseInt(request.getParameter("pageNo"));
                int Size = Integer.parseInt(request.getParameter("size"));
                String FullName = request.getParameter("FullName");
                String EmailID = request.getParameter("EmailID");
                String DpName = request.getParameter("DpName");
                String DpHeadName = request.getParameter("DpHeadName");
                int DpHeadId = -1;
                
                if(DpHeadName!= null && !DpHeadName.equalsIgnoreCase(""))
                {
                    for(int lv1=0;lv1<BankList.size();lv1++)
                    {
                        if(BankList.get(lv1).getFieldName1().equalsIgnoreCase(DpHeadName))
                        {
                            DpHeadId = Integer.parseInt(BankList.get(lv1).getFieldName2());
                            break;
                        }
                    }
                }
             
                System.out.println("offset:" + +offset);
                manageSbDpAdminSrBean.setOffset(offset);
              
                System.out.println("queryString:" + request.getQueryString());

                HttpSession session = request.getSession();
                Object objUserTypeId = session.getAttribute("userTypeId");
                Object objUserId = session.getAttribute("userId");

                List<VwGetSbDevPartner> vwGetAdminList = null;
                List<Object[]> bankAdminList = null;
                
                if (Integer.parseInt(objUserId.toString()) > 1) {
                    bankAdminList = manageSbDpAdminSrBean.getUserListAdmin(type, Integer.parseInt(objUserTypeId.toString()), Integer.parseInt(objUserId.toString()), "", "", "");
                   
                } else {
                    vwGetAdminList = manageSbDpAdminSrBean.getUserList(type, Integer.parseInt(objUserTypeId.toString()), Integer.parseInt(objUserId.toString()));
                }
                
                if (session.getAttribute("userId").toString().trim().equals("1")) {
                    
                    List<VwGetSbDevPartner> vwGetAdminListSearched = new ArrayList<VwGetSbDevPartner>();
                    
                    for(int j=0;j<vwGetAdminList.size();j++)
                    {
                        boolean ToAdd = true;
                        if(FullName!=null && !FullName.equalsIgnoreCase(""))
                        {
                            if(!vwGetAdminList.get(j).getId().getFullName().toLowerCase().contains(FullName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(EmailID!=null && !EmailID.equalsIgnoreCase(""))
                        {
                            if(!vwGetAdminList.get(j).getId().getEmailId().toLowerCase().contains(EmailID.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(DpName!=null && !DpName.equalsIgnoreCase(""))
                        {
                            if(!vwGetAdminList.get(j).getId().getSbDevelopName().toLowerCase().contains(DpName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(DpHeadId != -1)
                        {
                            if(vwGetAdminList.get(j).getId().getSbankDevelHeadId() != DpHeadId)
                            {
                                ToAdd = false;
                            }
                        }
                       
                        if(ToAdd)
                        {
                            //SPCommonSearchData commonAppData = getGovData.get(j);
                            vwGetAdminListSearched.add(vwGetAdminList.get(j));
                        }
                    }
                    
                    int RecordFrom = (pageNo-1)*Size;
                    int k= 0;
                    String styleClass = "";
                    if (vwGetAdminListSearched != null && !vwGetAdminListSearched.isEmpty()) {
                        for(k=RecordFrom;k<RecordFrom+Size && k<vwGetAdminListSearched.size();k++)
                        {
                            if(k%2==0){
                                styleClass = "bgColor-white";
                            }else{
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='"+styleClass+"'>");
                            out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                            out.print("<td width=\"15%\" class=\"t-align-center\">" + vwGetAdminListSearched.get(k).getId().getFullName() + "</td>");
                            String str_role = "";
                            if(!"6".equals(userTypeId)){
                                if("BranchMaker".equalsIgnoreCase(vwGetAdminListSearched.get(k).getId().getIsMakerChecker())){
                                   str_role = "Branch Maker"; 
                                   out.print("<td width=\"8%\" class=\"t-align-center\">" + str_role + "</td>");
                                }
                                else if("BranchChecker".equalsIgnoreCase(vwGetAdminListSearched.get(k).getId().getIsMakerChecker())){
                                   str_role = "Branch Checker"; 
                                   out.print("<td width=\"8%\" class=\"t-align-center\">" + str_role + "</td>");
                                }else{
                                    out.print("<td width=\"8%\" class=\"t-align-center\">" + vwGetAdminListSearched.get(k).getId().getIsMakerChecker() + "</td>");
                                }
                                out.print("<td width=\"17%\" class=\"t-align-center\">" + vwGetAdminListSearched.get(k).getId().getEmailId() + "</td>");
                                
                                if(DpHeadId != -1)
                                {
                                    out.print("<td width=\"15%\" class=\"t-align-center\">" + DpHeadName + "</td>");
                                }
                                else
                                {
                                    for(int lv1=0;lv1<BankList.size();lv1++)
                                    {
                                        if(vwGetAdminListSearched.get(k).getId().getSbankDevelHeadId() == Integer.parseInt(BankList.get(lv1).getFieldName2()))
                                        {
                                            out.print("<td width=\"15%\" class=\"t-align-center\">" + BankList.get(lv1).getFieldName1() + "</td>");
                                        }
                                    }
                                }
                                
                                out.print("<td width=\"20%\" class=\"t-align-center\">" + vwGetAdminListSearched.get(k).getId().getSbDevelopName() + "</td>");
                            }
                            else
                            {
                                out.print("<td width=\"25%\" class=\"t-align-center\">" + vwGetAdminListSearched.get(k).getId().getEmailId() + "</td>");
                                out.print("<td width=\"25%\" class=\"t-align-center\">" + vwGetAdminListSearched.get(k).getId().getSbDevelopName() + "</td>");
                            }
                            String editLink = "<a href=\"EditSbDevPartUser.jsp?userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&mode=edit\">Edit</a>";
                            String viewLink = "<a href=\"ViewSbDevPartUser.jsp?userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&mode=View\">View</a>";
                            String replaceLink = "<a href=\"ReplaceUser.jsp?userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId=" + userTypeId + "\">Transfer</a> | ";
                            String strUserStatus = vwGetAdminListSearched.get(k).getId().getStatus();
                            String strManageSbDevUserLink = "";
                            if ("approved".equalsIgnoreCase(strUserStatus)) {
                                strManageSbDevUserLink = "<a href=\"ManageSbDevPartUser.jsp?userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&userOldStatus=" + strUserStatus + "&partnerId="+vwGetAdminListSearched.get(k).getId().getPartnerId()+ "&fullName="+vwGetAdminListSearched.get(k).getId().getFullName()+ "\">Deactivate</a>";
                            } else {
                                strManageSbDevUserLink = "<a href=\"ManageSbDevPartUser.jsp?userId=" + vwGetAdminListSearched.get(k).getId().getUserId() + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&userOldStatus=" + strUserStatus + "&partnerId="+vwGetAdminListSearched.get(k).getId().getPartnerId()+ "&fullName="+vwGetAdminListSearched.get(k).getId().getFullName()+ "\">Activate</a>";
                            }

                            out.print("<td width=\"20%\" class=\"t-align-center\">" + editLink + " | " + viewLink + " | " + replaceLink + "<br />" + strManageSbDevUserLink + "</td>");
                            out.print("</tr>");
                        }

                    }
                    else
                    {
                        out.print("<tr>");
                        if(!"6".equals(userTypeId))
                        {
                            out.print("<td colspan=\"7\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        }
                        else
                        {
                            out.print("<td colspan=\"5\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        }
                            out.print("</tr>");
                    }  
                    int totalPages = 1;
                    if (vwGetAdminListSearched.size() > 0) {
                        totalPages = (int) (Math.ceil(Math.ceil(vwGetAdminListSearched.size()) / Size));
                        System.out.print("totalPages--"+totalPages+"records "+ vwGetAdminListSearched.size());
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                   
                 
                } else {
                        
                    List<Object[]> bankAdminListSearched = new ArrayList<Object[]>();
                    for(int j=0;j<bankAdminList.size();j++)
                    {
                        boolean ToAdd = true;
                        if(FullName!=null && !FullName.equalsIgnoreCase(""))
                        {
                            if(!bankAdminList.get(j)[0].toString().toLowerCase().contains(FullName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(EmailID!=null && !EmailID.equalsIgnoreCase(""))
                        {
                            if(!bankAdminList.get(j)[1].toString().toLowerCase().contains(EmailID.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                        if(DpName!=null && !DpName.equalsIgnoreCase(""))
                        {
                            if(!bankAdminList.get(j)[3].toString().toLowerCase().contains(DpName.toLowerCase()))
                            {
                                ToAdd = false;
                            }
                        }
                       
                        if(ToAdd)
                        {
                            //SPCommonSearchData commonAppData = getGovData.get(j);
                            bankAdminListSearched.add(bankAdminList.get(j));
                        }
                    }
                    
                    int RecordFrom = (pageNo-1)*Size;
                    int k= 0;
                    String styleClass = "";
                    if (bankAdminListSearched != null && !bankAdminListSearched.isEmpty()) {
                        for(k=RecordFrom;k<RecordFrom+Size && k<bankAdminListSearched.size();k++)
                        {
                            if(k%2==0){
                                styleClass = "bgColor-white";
                            }else{
                                styleClass = "bgColor-Green";
                            }
                            out.print("<tr class='"+styleClass+"'>");
                            out.print("<td width=\"5%\" class=\"t-align-center\">" + (k+1) + "</td>");
                            out.print("<td width=\"20%\" class=\"t-align-center\">" + bankAdminListSearched.get(k)[0].toString() + "</td>");
                            
                            if(!"Development".equalsIgnoreCase(type)){
                                out.print("<td width=\"10%\" class=\"t-align-center\">" + bankAdminListSearched.get(k)[4].toString() + "</td>");
                                out.print("<td width=\"20%\" class=\"t-align-center\">" + bankAdminListSearched.get(k)[1].toString() + "</td>");
                                out.print("<td width=\"20%\" class=\"t-align-center\">" + bankAdminListSearched.get(k)[3].toString() + "</td>");
                            }   
                            else
                            {
                               out.print("<td width=\"25%\" class=\"t-align-center\">" + bankAdminListSearched.get(k)[1].toString() + "</td>");
                               out.print("<td width=\"25%\" class=\"t-align-center\">" + bankAdminListSearched.get(k)[3].toString() + "</td>");
                            }
                            String editLink = "<a href=\"EditSbDevPartUser.jsp?userId=" + bankAdminListSearched.get(k)[2] + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&mode=edit\">Edit</a>";
                            String viewLink = "<a href=\"ViewSbDevPartUser.jsp?userId=" + bankAdminListSearched.get(k)[2] + "&partnerType=" + type + "&userTypeId=" + userTypeId + "\">View</a>";
                            String replaceLink = "<a href=\"ReplaceUser.jsp?userId=" + bankAdminListSearched.get(k)[2] + "&partnerType=" + type + "&userTypeId=" + userTypeId + "\">Transfer</a>";
                            String strUserStatus = (String) bankAdminListSearched.get(k)[5];
                            String strManageSbDevUserLink = "";
                            if ("approved".equalsIgnoreCase(strUserStatus)) {
                                strManageSbDevUserLink = "<a href=\"ManageSbDevPartUser.jsp?userId=" + bankAdminListSearched.get(k)[2] + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&userOldStatus=" + strUserStatus + "&partnerId="+bankAdminListSearched.get(k)[6]+ "&fullName="+bankAdminListSearched.get(k)[0]+ "\">Deactivate</a>";
                            } else {
                                strManageSbDevUserLink = "<a href=\"ManageSbDevPartUser.jsp?userId=" +bankAdminListSearched.get(k)[2] + "&partnerType=" + type + "&userTypeId=" + userTypeId + "&userOldStatus=" + strUserStatus + "&partnerId="+bankAdminListSearched.get(k)[6]+ "&fullName="+bankAdminListSearched.get(k)[0]+ "\">Activate</a>";
                            }
                            
                            out.print("<td width=\"25%\" class=\"t-align-center\">" + editLink + " | " + viewLink + " | " + replaceLink + "<br />" + strManageSbDevUserLink + "</td>");
                            out.print("</tr>");
                        }

                    }
                    else
                    {
                        out.print("<tr>");
                        if(!"Development".equalsIgnoreCase(type))
                        {
                            out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        }
                        else
                        {
                            out.print("<td colspan=\"5\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        }
                        out.print("</tr>");
                    }  
                    int totalPages = 1;
                    if (bankAdminListSearched.size() > 0) {
                        totalPages = (int) (Math.ceil(Math.ceil(bankAdminListSearched.size()) / Size));
                        System.out.print("totalPages--"+totalPages+"records "+ bankAdminListSearched.size());
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                   
                }
                //out.flush();
            }
        } catch (Exception ex) {
            System.out.println("Exception :" + ex);
        } finally {
            //out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
