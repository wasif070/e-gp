/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.webservices;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.WebServiceUtil;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import org.apache.log4j.Logger;

/**
 * WebService for accessing news and event's details.
 * @author Sreenu
 */
@WebService()
@XmlSeeAlso({SPTenderCommonData.class})
public class NewsAndEvents {

    private static final Logger LOGGER = Logger.getLogger(AdvanceSearchTenders.class);
    private String logUserId = "0";
    private static final int WEB_SERVICE_ID = 4;
    private static final String SEARCH_SERVICE_NAME = "TenderCommonService";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /***
     * This method gives the List of String objects of <b>News and Event Details</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1. Brief <br>
     * 2. Details <br>
     * 3. Important News or not (Yes/No) <br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getNewsAndEvents")
    public WSResponseObject getNewsAndEvents(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getNewsAndEvents : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = "";
        //insert record in log table
        WebServiceUtil.insertLogRecord(WEB_SERVICE_ID, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WEB_SERVICE_ID,
                SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> tenderCommonDataList = new ArrayList<String>();
            try {
                tenderCommonDataList = getNewsAndEventsDataList();
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getNewsAndEvents : " + logUserId + e);
            }
            if (tenderCommonDataList != null) {
                if (tenderCommonDataList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(tenderCommonDataList);
                }
            }
        }
        LOGGER.debug("getNewsAndEvents : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method give the required objects as a list of objects.
     * @return List<SPTenderCommonData>
     */
    private List<String> getNewsAndEventsDataList() {
        LOGGER.debug("getNewsAndEventsDataList : " + logUserId + WSResponseObject.LOGGER_START);
        List<String> tenderCommonDataStringList = new ArrayList<String>();
        List<SPTenderCommonData> tenderCommonDataList = new ArrayList<SPTenderCommonData>();
        TenderCommonService tenderCommonServiceData =
                (TenderCommonService) AppContext.getSpringBean(SEARCH_SERVICE_NAME);
        tenderCommonDataList = tenderCommonServiceData.returndata("getAnnouncements", null, null);
        if (tenderCommonDataList != null) {
            for (SPTenderCommonData tenderCommonData : tenderCommonDataList) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(tenderCommonData.getFieldName5());
                stringBuilder.append(WSResponseObject.APPEND_CHAR);
                stringBuilder.append(tenderCommonData.getFieldName6());
                stringBuilder.append(WSResponseObject.APPEND_CHAR); 
                // changed by Darshan on 20012012
                if (tenderCommonData.getFieldName4().equalsIgnoreCase("Yes")) {
                    stringBuilder.append("<b style='color:red;'>Important</b>");
                } else {
                    stringBuilder.append(" ");
                }
                stringBuilder.append(WSResponseObject.APPEND_CHAR);   //Commended by Sudhir 08072011
                // Changed by shreyansh shah 27022012 : To pass created news date to webservice
                stringBuilder.append(tenderCommonData.getFieldName7());
                tenderCommonDataStringList.add(stringBuilder.toString());
            }//end For loop
        }//end if condition
        LOGGER.debug("getNewsAndEventsDataList : " + logUserId + WSResponseObject.LOGGER_END);
        return tenderCommonDataStringList;
    }
}
