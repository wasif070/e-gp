/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public class TenderLotsPhasingOffline {

    private String lotOrRefNo;
     private String lotIdentOrPhasingServ;
     private String location;
     private BigDecimal tenderSecurityAmt;
     private String startDateTime;
     private String completionDateTime;

    /**
     * @return the lotOrRefNo
     */
    public String getLotOrRefNo() {
        return lotOrRefNo;
    }

    /**
     * @param lotOrRefNo the lotOrRefNo to set
     */
    public void setLotOrRefNo(String lotOrRefNo) {
        this.lotOrRefNo = lotOrRefNo;
    }

    /**
     * @return the lotIdentOrPhasingServ
     */
    public String getLotIdentOrPhasingServ() {
        return lotIdentOrPhasingServ;
    }

    /**
     * @param lotIdentOrPhasingServ the lotIdentOrPhasingServ to set
     */
    public void setLotIdentOrPhasingServ(String lotIdentOrPhasingServ) {
        this.lotIdentOrPhasingServ = lotIdentOrPhasingServ;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the tenderSecurityAmt
     */
    public BigDecimal getTenderSecurityAmt() {
        return tenderSecurityAmt;
    }

    /**
     * @param tenderSecurityAmt the tenderSecurityAmt to set
     */
    public void setTenderSecurityAmt(BigDecimal tenderSecurityAmt) {
        this.tenderSecurityAmt = tenderSecurityAmt;
    }

    /**
     * @return the startDateTime
     */
    public String getStartDateTime() {
        return startDateTime;
    }

    /**
     * @param startDateTime the startDateTime to set
     */
    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    /**
     * @return the completionDateTime
     */
    public String getCompletionDateTime() {
        return completionDateTime;
    }

    /**
     * @param completionDateTime the completionDateTime to set
     */
    public void setCompletionDateTime(String completionDateTime) {
        this.completionDateTime = completionDateTime;
    }

}
