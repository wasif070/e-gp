/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblTenderForms;
import com.cptu.egp.eps.service.serviceimpl.TemplateSectionFormImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderFormService;
import com.cptu.egp.eps.service.serviceimpl.TenderServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yanki
 */
public class TenderFormSrBean {
    TenderFormService tenderFormService = (TenderFormService)  AppContext.getSpringBean("TenderAddFormService");

    /**
     * Get Form Details
     * @param formId
     * @return Tender form details
     */
    public List<TblTenderForms> getFormDetail(int formId) {
        return tenderFormService.getSingleForms(formId);
    }

    /**
     * Get Form Type.
     * @param tenderFormId
     * @return list of object having Form Type details.
     */
    public List<Object> getFormType(int tenderFormId){
        List<Object> list = new ArrayList<Object>();
        TemplateSectionFormImpl templateSectionForm = (TemplateSectionFormImpl)  AppContext.getSpringBean("AddFormService");
        list = templateSectionForm.getFormType(tenderFormId);
        return list;
    }

    /**
     * Get Tender Details.
     * @param tenderId
     * @return list of TblTenderDetails objects.
     */
    public List<TblTenderDetails> getTenderDetails(int tenderId){
        
        List<TblTenderDetails> list = new ArrayList<TblTenderDetails>();
        TenderServiceImpl tenderServiceImpl = (TenderServiceImpl) AppContext.getSpringBean("TenderService");
        list = tenderServiceImpl.getTenderDetails(tenderId);
        return list;
        
    }

    /**
     * Check Grand summary exist into system for given formid, table id, tender id.
     * @param tableId
     * @param formId
     * @param columnId
     * @param userId
     * @param bidId
     * @param tenderId
     * @param funName
     * @return
     */
    public int CheckExistInGrandSum(int tableId, int formId, int tenderId, String funName)
    {
        return tenderFormService.CheckExistInGrandSum(tableId,formId,tenderId,funName);
    }

    /**
     * Check Grand summary exist into system for given formid, table id, tender id for given corrigendum.
     * @param tableId
     * @param formId
     * @param columnId
     * @param userId
     * @param bidId
     * @param tenderId
     * @param funName
     * @return
     */
    public int CheckExistInGrandSum(int tableId, int formId, int tenderId, String funName,int corriId)
    {
        return tenderFormService.CheckExistInGrandSum(tableId,formId,tenderId,funName,corriId);
    }
    
    /**
     * Check Grand summary Need to Edit at Corrigendum time?
     * @param tenderId
     * @param corriId
     * @param funName
     * @return
     */
    public String checkGrandSumNeedAtCorri( int tenderId,int corriId, String funName)
    {
        return tenderFormService.checkGrandSumNeedAtCorri(tenderId,corriId,funName);
    }

    /*
     * Retrun list of canceled forms in corriendum, execpt those which is yet to create.
     * @param  sectionId
     */
    public List<Object[]> getFormsCancelledInCorriForSrv(int sectionId){
        return tenderFormService.getFormsCancelledInCorriForSrv(sectionId);
    }

    /*
     * Return type of forms for service, procurement anture.
     * @param formId
     * @return String
     */
    public String getTypeOfFormForService(int formId){
        return tenderFormService.getTypeOfFormForService(formId);
    }

}
