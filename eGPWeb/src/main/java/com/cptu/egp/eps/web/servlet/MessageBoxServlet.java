package com.cptu.egp.eps.web.servlet;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.cptu.egp.eps.dao.storedprocedure.MsgListing;
import com.cptu.egp.eps.web.servicebean.MessageProcessSrBean;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author test
 */
public class MessageBoxServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
                response.setContentType("text/xml;charset=UTF-8");
                Integer recPerPage = Integer.parseInt(request.getParameter("rows"));
                Integer pageNumber =Integer.parseInt(request.getParameter("page"));
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                if(sidx.equals("")){
                    sidx = "creationDate";
                } else if (sidx.equals("fromMailId")) {
                    sidx = "fromLogin.emailid";
                    if("Sent".equals(request.getParameter("inbox")) || "Draft".equals(request.getParameter("inbox"))) {
                        sidx = "toLogin.emailid";
                    }
                } else if (sidx.equals("subject")|| sidx.equals("isPriority")){
                   if(sord.equals(""))
                       sord = "desc";
                }

                if(sord.equals("") || sord.equalsIgnoreCase("asc")){
                     sord = "desc";
                } else {
                     sord = "asc";
                }
                //new code
                
                       
                       String colName = null;
                       String colHeader = null;
                       String str = "Inbox";
                       Integer totalPages = 0 ;
                       String messageType = null;
                       messageType = "Live";
                       Integer folderid = 0;
                       String searching ="";
                       String keywords = "";
                       String emailId = "";
                       String dateFrom = "";
                       String dateTo = "";
                       String viewmsg = "NO";
                       Integer msgid =0;
                       int totalRecords = 0;
                       String searchOpt = "";
                      

                       MessageProcessSrBean mpsrb = new MessageProcessSrBean();

                       if("Inbox".equals(request.getParameter("inbox"))) {
                           str = request.getParameter("inbox");
                           
                           }
                       else if("Sent".equals(request.getParameter("inbox"))) {
                            str = "Sent";
                            
                            }
                       else if("InboxUnRead".equals(request.getParameter("inbox"))) {
                            str = "InboxUnRead";

                            }
                       else if("InboxRead".equals(request.getParameter("inbox"))) {
                            str = "InboxRead";

                            }
                       else if("Trash".equals(request.getParameter("inbox"))) {
                            str = request.getParameter("inbox");
                            messageType = "Trash";
                           
                            }
                      else if("Draft".equals(request.getParameter("inbox"))){
                          str = request.getParameter("inbox");
                          messageType = "Draft";
                          
                          }
                       else if("Folder".equals(request.getParameter("inbox"))) {
                           str = "Inbox";
                           folderid = Integer.valueOf(request.getParameter("folderid"));
                          
                           }
                        if("Search".equals(request.getParameter("srchbutton"))) {
                           str =  mpsrb.getMessageBoxType();
                           searching = request.getParameter("select");
                           if(searching.equals("--Select--"))
                               searching="Subject";
                           keywords = request.getParameter("textfield5");
                           emailId = request.getParameter("textfield6");
                           if(emailId!=null && !"".equalsIgnoreCase(emailId)){
                               searchOpt = request.getParameter("searchOpt");
                           }
                           dateFrom =  request.getParameter("datefrom");
                           dateTo = request.getParameter("dateto");
                           
                          
                           
                       } else if("Reset".equals(request.getParameter("resbutton"))) {
                             str =  mpsrb.getMessageBoxType();
                           }

                       int uid = 0;
                       if(request.getSession().getAttribute("userId") != null){
                           Integer ob1 = (Integer)request.getSession().getAttribute("userId");
                           uid = ob1.intValue();
                       }
                       
                       
                       //this is static if you have id in session delete below line
                       // and enable above two lines
                      // int uid = 1;
                       String data = null;
                       List<MsgListing> listdata = null;
                       
                       listdata = mpsrb.getInboxData(str, uid,messageType,totalPages,
                               pageNumber,recPerPage,folderid,searching,keywords,emailId,dateFrom,dateTo,viewmsg,msgid,sidx,sord,searchOpt);
                       //List strList=mpsrb.convertToList(listdata,str);
                       MsgListing msgListing = null;
                       if(listdata.size()>0){
                          msgListing =  listdata.get(0);
                           totalRecords =  msgListing.getTotalRecords().intValue();
                          if(totalRecords%recPerPage == 0){
                              totalPages = totalRecords/recPerPage;
                          }else{
                               totalPages = totalRecords/recPerPage+1;
                              }
                       }
                       mpsrb.setMessageBoxType(str);
                       

                       System.out.println("qry String:" + request.getQueryString());
                        response.setContentType("text/xml");
                      
                        System.out.println("totalpages"+totalPages+"totalrecords="+totalRecords);
                        StringBuffer sb = new StringBuffer();
                               
                                 sb.append("<?xml version='1.0' encoding='utf-8'?>");
                                 sb.append("<rows>");

                                 sb.append("<page>" + pageNumber + "</page>");
                                 sb.append("<total>" + totalPages + "</total>");
                                 sb.append("<records>" +totalRecords+ "</records>");
                                 int j = ((pageNumber-1)*recPerPage) + 1;
                                 if(listdata.size()>0){
                                 for (int i = 0; i < listdata.size(); i++) {
                                    sb.append("<row>");
                                    if(listdata.get(i).getIsMsgRead().equalsIgnoreCase("No") && str.equalsIgnoreCase("Inbox")){

                                     sb.append("<cell><![CDATA[<b>"+j+"</b>]]></cell>");
                                     if(str.equalsIgnoreCase("Inbox"))
                                    sb.append("<cell><![CDATA[<b>" + listdata.get(i).getFromMailId() + "</b>]]></cell>");
                                     else sb.append("<cell> <![CDATA[<b>" + listdata.get(i).getToMailId() + " </b>]]></cell>");
                                    if(!str.equalsIgnoreCase("Draft")){
                                        String link ="<cell><a  href='ViewMessage.jsp?messageid="+listdata.get(i).getMsgId()+"&messageBoxType="+str+"pre&messageInboxid="+listdata.get(i).getMsgBoxId()+"&folderId="+folderid+"'><b>"+listdata.get(i).getSubject()+"</b></a></cell>";
                                        sb.append("<cell><![CDATA[" + link + "]]></cell>");
                                     }else sb.append("<cell><![CDATA[" +listdata.get(i).getSubject() + "]]></cell>");
                                    sb.append("<cell><![CDATA[<b>" + listdata.get(i).getIsPriority()+ "</b>]]></cell>");
                                    String str3 = DateUtils.gridDateToStr(listdata.get(i).getCreationDate());
                                    sb.append("<cell><![CDATA[<b>" + str3+ "</b>]]></cell>");
                                    if(str.equalsIgnoreCase("Draft")){
                                     String link1 = "<cell><b><a  href='ComposeMail.jsp?messageid="+listdata.get(i).getMsgId()+"&messageBoxType="+str+"'>Edit</a></b></cell>";
                                     sb.append("<cell><![CDATA[" + link1 + "]]></cell>");
                                    }

                                   }if(listdata.get(i).getIsMsgRead().equalsIgnoreCase("No") && str.equalsIgnoreCase("InboxUnRead")){

                                         sb.append("<cell><![CDATA[<b>"+j+"</b>]]></cell>");
                                         if(str.equalsIgnoreCase("Inbox"))
                                        sb.append("<cell><![CDATA[<b>" + listdata.get(i).getFromMailId() + "</b>]]></cell>");
                                         else sb.append("<cell> <![CDATA[<b>" + listdata.get(i).getToMailId() + " </b>]]></cell>");
                                        if(!str.equalsIgnoreCase("Draft")){
                                            String link ="<cell><a  href='ViewMessage.jsp?messageid="+listdata.get(i).getMsgId()+"&messageBoxType="+str+"pre&messageInboxid="+listdata.get(i).getMsgBoxId()+"&folderId="+folderid+"'><b>"+listdata.get(i).getSubject()+"</b></a></cell>";
                                            sb.append("<cell><![CDATA[" + link + "]]></cell>");
                                         }else sb.append("<cell><![CDATA[" +listdata.get(i).getSubject() + "]]></cell>");
                                        sb.append("<cell><![CDATA[<b>" + listdata.get(i).getIsPriority()+ "</b>]]></cell>");
                                        String str3 = DateUtils.gridDateToStr(listdata.get(i).getCreationDate());
                                        sb.append("<cell><![CDATA[<b>" + str3+ "</b>]]></cell>");
                                        if(str.equalsIgnoreCase("Draft")){
                                         String link1 = "<cell><b><a  href='ComposeMail.jsp?messageid="+listdata.get(i).getMsgId()+"&messageBoxType="+str+"'>Edit</a></b></cell>";
                                         sb.append("<cell><![CDATA[" + link1 + "]]></cell>");
                                        }

                                   }else{

                                         sb.append("<cell>"+j+"</cell>");
                                     if(str.equalsIgnoreCase("Inbox"))
                                    sb.append("<cell>" + listdata.get(i).getFromMailId() + "</cell>");
                                     else sb.append("<cell>" + listdata.get(i).getToMailId() + "</cell>");
                                    if(!str.equalsIgnoreCase("Draft")){
                                        String link ="<cell><a  href='ViewMessage.jsp?messageid="+listdata.get(i).getMsgId()+"&messageBoxType="+str+"pre&messageInboxid="+listdata.get(i).getMsgBoxId()+"&folderId="+folderid+"'>"+listdata.get(i).getSubject()+"</a></cell>";
                                        sb.append("<cell><![CDATA[" + link + "]]></cell>");
                                     }else sb.append("<cell><![CDATA[" +listdata.get(i).getSubject() + "]]></cell>");
                                    sb.append("<cell>" + listdata.get(i).getIsPriority()+ "</cell>");
                                    String str3 = DateUtils.gridDateToStr(listdata.get(i).getCreationDate());
                                    sb.append("<cell>" + str3+ "</cell>");
                                    if(str.equalsIgnoreCase("Draft")){
                                     String link1 = "<cell><a  href='ComposeMail.jsp?messageid="+listdata.get(i).getMsgId()+"&messageBoxType="+str+"'>Edit</a></cell>";
                                     sb.append("<cell><![CDATA[" + link1 + "]]></cell>");
                                    }
                                   }
                                   // sb.append(link);
                                    sb.append("</row>");
                                   j++;
                                }
                      }else{
                                sb.append("<row>");
                            sb.append("<cell>"+j+"</cell>");
                            sb.append("<cell>No Data Found</cell>");
                            sb.append("<cell>No Data Found</cell>");
                            sb.append("<cell>No Data Found</cell>");
                            sb.append("<cell>No Data Found</cell>");
                            sb.append("<cell>No Data Found</cell>");
                             if(str.equalsIgnoreCase("Draft")){
                                  sb.append("<cell>No Data Found</cell>");
                             }
                            sb.append("</row>");
                      }
                                       sb.append("</rows>");
                                  String url = sb.toString();
                                  out.print(sb.toString());
                                 System.out.println("xml generation = "+url);
                                 // request.setAttribute("url", url);

              // new code end
               


        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
