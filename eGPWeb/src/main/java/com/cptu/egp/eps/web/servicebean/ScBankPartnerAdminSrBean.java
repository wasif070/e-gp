/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.service.serviceinterface.AdminRegistrationService;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.databean.LoginMasterDtBean;
import com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ScBankPartnerAdminSrBean {

    static final Logger LOGGER = Logger.getLogger(ScBankPartnerAdminSrBean.class);
    private String logUserId = "0";
    private AdminRegistrationService adminRegService = (AdminRegistrationService) AppContext.getSpringBean("AdminRegistrationService");
    ScBankDevpartnerService scBankDevpartnerService = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
    List<SelectItem> bankList = new ArrayList<SelectItem>();
    List<SelectItem> dpList = new ArrayList<SelectItem>();
    private AuditTrail auditTrail;
    /**
     * For Audit purpose
     * @param Object of AuditTrail
     */
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        scBankDevpartnerService.setAuditTrail(auditTrail);
        adminRegService.setAuditTrail(auditTrail);
    }

    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        adminRegService.setLogUserId(logUserId);
        scBankDevpartnerService.setUserId(logUserId);
        this.logUserId = logUserId;
    }

    int bankId;

    /**
     * Getter of bankId
     * @return
     */
    public int getBankId() {
        return bankId;
    }

    /**
     * Setter of bankId
     * @param bankId
     */
    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    /**
     * Copy data to partnerAdmin databean to tbl_partnerAdmin
     * @param partnerAdminMasterDtBean
     * @return
     */
    public TblPartnerAdmin _toPartnerAdmin(PartnerAdminMasterDtBean partnerAdminMasterDtBean){
        TblPartnerAdmin partnerAdmin = new TblPartnerAdmin();
        BeanUtils.copyProperties(partnerAdminMasterDtBean, partnerAdmin);
        return partnerAdmin;
    }

    /**
     * Copy data to loginMasterDtBean to tbl_LoginMaster
     * @param loginMasterDtBean
     * @return
     */
    public TblLoginMaster _toLoginMaster(LoginMasterDtBean loginMasterDtBean) {
        TblLoginMaster loginMaster = new TblLoginMaster();
        BeanUtils.copyProperties(loginMasterDtBean, loginMaster);
        //loginMaster.setPassword(""+loginMasterDtBean.getPassword().hashCode());
        return loginMaster;
    }

    /**
     * Get the List of bank for admin users and egpAdmin
     * @return
     */
    public List<SelectItem> getbankList() {
        LOGGER.debug("getbankList : " + logUserId + " Starts");
        if (bankList.isEmpty()) {
            try {
                for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : adminRegService.findTblBankMaster("partnerType", Operation_enum.LIKE, "ScheduleBank", "sbDevelopName", Operation_enum.ORDERBY, Operation_enum.ASC, "isBranchOffice", Operation_enum.LIKE, "No")) {
                    bankList.add(new SelectItem(tblScBankDevPartnerMaster.getSbankDevelopId(), tblScBankDevPartnerMaster.getSbDevelopName()));
                }
            } catch (Exception e) {
                LOGGER.error("getbankList : " + logUserId + e);
            }
        }
        LOGGER.debug("getbankList : " + logUserId + " Ends");
        return bankList;
    }

    /**
     * setter of bankList
     * @param bankList
     */
    public void setbankList(List<SelectItem> bankList) {
        this.bankList = bankList;
    }

    /**
     * Get all the developmentPartnerList
     * @return
     */
    public List<SelectItem> getdpList() {
        LOGGER.debug("getdpList : " + logUserId + " Starts");
        if (dpList.isEmpty()) {
            try {
                for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : adminRegService.findTblBankMaster("partnerType", Operation_enum.LIKE, "Development", "sbDevelopName", Operation_enum.ORDERBY, Operation_enum.ASC, "isBranchOffice", Operation_enum.LIKE, "No")) {
                    dpList.add(new SelectItem(tblScBankDevPartnerMaster.getSbankDevelopId(), tblScBankDevPartnerMaster.getSbDevelopName()));
                }
            } catch (Exception e) {
                LOGGER.error("getdpList : " + logUserId + e);
            }
        }
        LOGGER.debug("getdpList : " + logUserId + " Ends");
        return dpList;
    }

    /**
     * setter of developmentPartner List
     * @param dpList
     */
    public void setdpList(List<SelectItem> dpList) {
        this.dpList = dpList;
    }

    /**
     * This method is for upate records of scBank and development admin and branchadmin (Update tbl_loginmaster and TblPartnerAdmin)
     * @param partnerAdminMasterDtBean
     * @param loginMasterDtBean
     * @param type
     * @param pwd
     * @return
     */
    public Integer scBankDevPartnerAdminReg(PartnerAdminMasterDtBean partnerAdminMasterDtBean, LoginMasterDtBean loginMasterDtBean, String type, String pwd) {
        LOGGER.debug("scBankDevPartnerAdminReg : " + logUserId + " Starts");
        int userId = 0;
        TblLoginMaster loginMaster = _toLoginMaster(loginMasterDtBean);
        loginMaster.setBusinessCountryName("");
        loginMaster.setRegistrationType("officer");
        loginMaster.setIsJvca("no");
        loginMaster.setBusinessCountryName("bangladesh");
        loginMaster.setNextScreen("ChangePassword");
        loginMaster.setIsEmailVerified("yes");
        loginMaster.setFailedAttempt((byte) 0);
        loginMaster.setValidUpTo(null);
        loginMaster.setFirstLogin("yes");
        loginMaster.setIsPasswordReset("no");
        loginMaster.setResetPasswordCode("0");
        loginMaster.setStatus("approved");
        loginMaster.setRegisteredDate(new Date());
        loginMaster.setNationality("bangladeshi");
        loginMaster.setHintQuestion("");
        loginMaster.setHintAnswer("");
        String branchAdmin = "";
        if ("dev".equalsIgnoreCase(type)) {
            loginMaster.setTblUserTypeMaster(new TblUserTypeMaster((byte) 6));
        } else if ("branch".equalsIgnoreCase(type)) {
            loginMaster.setTblUserTypeMaster(new TblUserTypeMaster((byte) 15));
            branchAdmin = "BranchAdmin";
        } else if ("sb".equalsIgnoreCase(type)) {
            loginMaster.setTblUserTypeMaster(new TblUserTypeMaster((byte) 7));
        }

        TblPartnerAdmin partnerAdmin = _toPartnerAdmin(partnerAdminMasterDtBean);
        partnerAdmin.setIsAdmin("Yes");
        partnerAdmin.setIsMakerChecker(branchAdmin);
        partnerAdmin.setSbankDevelopId(this.getBankId());

        try {
            userId = adminRegService.registerScBankDevPartnerAdmin(loginMaster, partnerAdmin);
            if (userId > 0) {
                if (!sendMailAndSMS(loginMasterDtBean.getEmailId(), pwd, partnerAdminMasterDtBean.getMobileNo(), type)) {
                    userId = 0;
                }
            }
        } catch (Exception ex) {
            LOGGER.error("scBankDevPartnerAdminReg : " + logUserId + ex);
            userId = 0;
        }
        LOGGER.debug("scBankDevPartnerAdminReg : " + logUserId + " Ends");
        return userId;

    }

    /**
     * update tblPartnerAdmin
     * @param partnerAdminMasterDtBean
     * @return boolean
     */
    public boolean updateDevelopmentScBankAdmin(PartnerAdminMasterDtBean partnerAdminMasterDtBean) {
        LOGGER.debug("updateDevelopmentScBankAdmin : " + logUserId + " Starts");
        boolean bSuccess = false;
        TblPartnerAdmin tblPartnerAdmin = new TblPartnerAdmin();

        tblPartnerAdmin.setPartnerId(partnerAdminMasterDtBean.getPartnerId());
        tblPartnerAdmin.setTblLoginMaster(new TblLoginMaster(partnerAdminMasterDtBean.getUserId()));
        tblPartnerAdmin.setFullName(partnerAdminMasterDtBean.getFullName());
        tblPartnerAdmin.setIsAdmin(partnerAdminMasterDtBean.getIsAdmin());

        if (partnerAdminMasterDtBean.getMobileNo() == null) {
            partnerAdminMasterDtBean.setMobileNo("");
        }

        tblPartnerAdmin.setMobileNo(partnerAdminMasterDtBean.getMobileNo());

        if (partnerAdminMasterDtBean.getNationalId() == null) {
            partnerAdminMasterDtBean.setNationalId("");
        }

        if (partnerAdminMasterDtBean.getIsMakerChecker() == null) {
            partnerAdminMasterDtBean.setIsMakerChecker("");
        }

        tblPartnerAdmin.setIsMakerChecker(partnerAdminMasterDtBean.getIsMakerChecker());
        tblPartnerAdmin.setNationalId(partnerAdminMasterDtBean.getNationalId());
        if (partnerAdminMasterDtBean.getIsAdmin().equalsIgnoreCase("No")) {
        tblPartnerAdmin.setSbankDevelopId(Integer.parseInt(partnerAdminMasterDtBean.getOfficeName()));  //Done by rikin
        } else {
            tblPartnerAdmin.setSbankDevelopId(partnerAdminMasterDtBean.getSbankDevelopId());
        }
        tblPartnerAdmin.setDesignation(partnerAdminMasterDtBean.getDesignation());
        tblPartnerAdmin.setIsMakerChecker(partnerAdminMasterDtBean.getIsMakerChecker());
        try {
            scBankDevpartnerService.updateDevelopmentScBankAdmin(tblPartnerAdmin,String.valueOf(partnerAdminMasterDtBean.getUserTypeId()));
            bSuccess = true;
        } catch (Exception ex) {
            LOGGER.error("updateDevelopmentScBankAdmin : " + logUserId + ex);
            bSuccess = false;
        }
        LOGGER.debug("updateDevelopmentScBankAdmin : " + logUserId + " Ends");
        return bSuccess;
        }

    private boolean sendMailAndSMS(String mailId, String password, String mobNo, String type) {
        LOGGER.debug("sendMailAndSMS : " + logUserId + " Starts");
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            MailContentUtility mailContentUtility = new MailContentUtility();
            List list = null;

            if ("dev".equalsIgnoreCase(type)) {
                list = mailContentUtility.getscBankDevPartnerAdminRegContent(mailId, password);
            } else {
                list = mailContentUtility.getscBankAdminRegContent(mailId, password);
            }
            String mailSub = list.get(0).toString();
            String mailText = list.get(1).toString();
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);
            try {
                sendMessageUtil.sendEmail();
            } catch (Exception e) {
                LOGGER.error("sendMailAndSMS : " + logUserId + e);
            }

            try {
                sendMessageUtil.setSmsNo("+975" + mobNo);
                sendMessageUtil.setSmsBody("Dear User,%0Aprofile has been created Login information is sent to your registered Email ID.%0Ae-GP User Registration Desk");
                sendMessageUtil.sendSMS();
            } catch (Exception e) {
                LOGGER.error("sendMailAndSMS : " + logUserId + e);
            }
            mailSent = true;
        } catch (Exception ex) {
            LOGGER.error("sendMailAndSMS : " + logUserId + ex);
        }
        LOGGER.debug("sendMailAndSMS : " + logUserId + " Ends");
        return mailSent;
    }

    /**
     * Get developmentPartner List
     * @return List of TblScBankDevPartnerMaster
     */
    public List<TblScBankDevPartnerMaster> getDevelopPartnerList(){
        LOGGER.debug("getDevelopPartnerList : " + logUserId + " Starts");
        List<TblScBankDevPartnerMaster> list = null;
        try{
           list =  scBankDevpartnerService.getDevelopPartnerList();
        }catch(Exception ex){
            LOGGER.error("getDevelopPartnerList : "+logUserId+" : "+ex);
        }
        LOGGER.debug("getDevelopPartnerList : " + logUserId + " Ends");
        return list;
    }

    /**
     * Get schedule bank and development Partner information
     * @param id(tblScBankDevPartnerMaster)
     * @return String
     */
    public String getSbDevInfo(int id){
        LOGGER.debug("getSbDevInfo : " + logUserId + " Starts");
        String SbBankDevName = "";
        TblScBankDevPartnerMaster tblScBankDevPartnerMaster = new TblScBankDevPartnerMaster();
        try{
            tblScBankDevPartnerMaster = scBankDevpartnerService.getSbDevInfo(id);
            SbBankDevName = tblScBankDevPartnerMaster.getSbDevelopName();
        }catch(Exception ex){
            LOGGER.error("getSbDevInfo : "+logUserId+" : "+ex);
        }
        LOGGER.debug("getSbDevInfo : " + logUserId + " Ends");
        return SbBankDevName;
    }
}
