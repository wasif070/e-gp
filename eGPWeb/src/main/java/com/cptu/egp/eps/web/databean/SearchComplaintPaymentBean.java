/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import java.util.Date;

/**
 *
 * @author RadhaRani
 */
public class SearchComplaintPaymentBean {
    private int complaintId;
    private String complaintSubject;
    private int tenderId;
    private String tendererEmailId;
    private int createdBy;
    private String status;
    private String isLive;
    private String isVerified;
    private Date createdDate;
    private Date dtOfAction;
    
	public int getComplaintId() {
		return complaintId;
	}
	public void setComplaintId(int complaintId) {
		this.complaintId = complaintId;
	}
	public String getComplaintSubject() {
		return complaintSubject;
	}
	public void setComplaintSubject(String complaintSubject) {
		this.complaintSubject = complaintSubject;
	}
	public int getTenderId() {
		return tenderId;
	}
	public void setTenderId(int tenderId) {
		this.tenderId = tenderId;
	}
	public String getTendererEmailId() {
		return tendererEmailId;
	}
	public void setTendererEmailId(String tendererEmailId) {
		this.tendererEmailId = tendererEmailId;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsLive() {
		return isLive;
	}
	public void setIsLive(String isLive) {
		this.isLive = isLive;
	}
	public String getIsVerified() {
		return isVerified;
	}
	public void setIsVerified(String isVerified) {
		this.isVerified = isVerified;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getDtOfAction() {
		return dtOfAction;
	}
	public void setDtOfAction(Date dtOfAction) {
		this.dtOfAction = dtOfAction;
	}
    
   }
