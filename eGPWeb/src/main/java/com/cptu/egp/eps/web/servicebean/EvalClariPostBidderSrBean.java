/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.EvalClariPostBidderService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Krishnraj
 */
public class EvalClariPostBidderSrBean {

    String logUserId = "0";
    final Logger logger = Logger.getLogger(EvalClariPostBidderSrBean.class);
    private CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
    AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");

    AuditTrail auditTrail;

    public void setLogUserId(String logUserId) {
        commonXMLSPService.setLogUserId(logUserId);
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
    public boolean updEvalFormQues(String questionId){
        logger.debug("updEvalFormQues "+logUserId+" Starts:");
        boolean flag = false;
        try{
        CommonMsgChk commonMsgChk = new CommonMsgChk();
        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalFormQues", "queSentByTec='1'", "evalQueId=" + questionId).get(0);
            flag = commonMsgChk.getFlag();
        }catch(Exception e){
            logger.error("updEvalFormQues "+logUserId+" :"+e);
    }
        logger.debug("updEvalFormQues "+logUserId+" Ends:");
        return flag;
    }

    public boolean  updateEvalFormQue(String question,String questionId) throws Exception{
        logger.debug("updateEvalFormQue "+logUserId+" Starts:");
        boolean flag = false;
        try{
        CommonMsgChk commonMsgChk = new CommonMsgChk();
        String updateData = "question = '"+question+"'";
        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalFormQues", updateData, "evalQueId=" + questionId).get(0);
        commonMsgChk.getMsg();
            flag =  commonMsgChk.getFlag();
        }catch(Exception e){
            logger.error("updateEvalFormQue "+logUserId+" :"+e);
    }
        logger.debug("updateEvalFormQue "+logUserId+" Ends:");
        return flag;
    }

    public boolean insBidderResponse(String tenderId, String userId, String sentBy,
            String remarks, String respDt,String s_tenRefNo){
        logger.debug("insBidderResponse "+logUserId+" Starts:");
        String action = "Post queries to tenderer by TEC CP";
        boolean flag =false;
        try{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy");
        //CommonMsgChk commonMsgChk = null;

        /*String bidderRespXml = "<root><tbl_EvalBidderResp tenderId=\"" + tenderId
                + "\" userId=\"" + userId + "\" sentBy=\"" + sentBy + "\" "
                + "expectedComplDt=\"" + respDt + "\" "
                + "evalstatus=\"Evaluation\" isClarificationComp=\"no\" "
                + "sentDate=\"" + sdf.format(new Date()) + "\" remarks=\""
                + HandleSpecialChar.handleSpecialChar(remarks) + "\" /></root>";*/
        //    commonMsgChk = commonXMLSPService.insertDataBySP("insert","tbl_EvalBidderResp",bidderRespXml,"").get(0);
        //Insert by dohatec
        CommonMsgChk commonMsgChk =addUpdate.addUpdOpeningEvaluation("PostQuestionsToTenderer",tenderId,userId,sentBy,respDt,"Evaluation","no",sdf.format(new Date()),HandleSpecialChar.handleSpecialChar(remarks),"","","","","","","","","","","").get(0);

            flag =  commonMsgChk.getFlag();
            if(flag){
                CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                 EvalClariPostBidderService evalClariPostBidderService = (EvalClariPostBidderService) AppContext.getSpringBean("EvalClariPostBidderService");
                String[] s_emailId= {commonService.getEmailId(userId)};
                String s_officeName = evalClariPostBidderService.getPeOffice(Integer.parseInt(tenderId));
                Date date = format1.parse(respDt.trim());
                sendMsgSeekClari(tenderId, s_tenRefNo, s_officeName, format2.format(date), s_emailId);
            }
        }catch(Exception e){
            logger.debug("insBidderResponse "+logUserId+" :"+e);
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(tenderId), "TenderId", EgpModule.Evaluation.getName(), action, remarks);
            action = null;
        }
        logger.debug("insBidderResponse "+logUserId+" ends:");
        return flag;
        
    }

    public boolean isChecked(String tenderId, String userId,String lotId){
        logger.debug("isChecked "+logUserId+" Starts:");
        boolean flag = false;
        try{
        EvalClariPostBidderService evalClariPostBidderService = (EvalClariPostBidderService) AppContext.getSpringBean("EvalClariPostBidderService");
            flag = evalClariPostBidderService.isChecked(tenderId,userId,lotId);
         }catch(Exception e){
            logger.error("isChecked "+logUserId+" :"+e);
    }
        logger.debug("isChecked "+logUserId+" Ends:");
        return flag;
    }
    
     public boolean sendMsgSeekClari(String tenderId, String refNo, String peOfficeName, String date,String[] emailTo) {
        logger.debug("sendMsgSeekClari : " + logUserId + " Starts :");
        boolean flag = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            MailContentUtility mailContentUtility = new MailContentUtility();
            String mailConte = "";
            mailConte = mailContentUtility.cpClariBidder(tenderId, refNo, peOfficeName, date);
            sendMessageUtil.setEmailTo(emailTo);
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
            sendMessageUtil.setEmailSub("e-GP: Tender evaluation related queries");
            sendMessageUtil.setEmailMessage(mailConte);
            sendMessageUtil.sendEmail();
            UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            registerService.contentAdmMsgBox(emailTo[0], XMLReader.getMessage("emailIdNoReply"), "Tender evaluation related queries", msgBoxContentUtility.cpClariBidder(tenderId, refNo, peOfficeName, date));
            flag = true;
        } catch (Exception e) {
            logger.error("sendMsgSeekClari : " + logUserId + e);
        }
        logger.debug("sendMsgSeekClari : " + logUserId + " Ends:");
        return flag;
    }
}
