/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.webservices;

import com.cptu.egp.eps.dao.storedprocedure.PromisIntegrationDetails;
import com.cptu.egp.eps.service.serviceinterface.PromisIntegrationDetailsInfoService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.WebServiceUtil;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import org.apache.log4j.Logger;

/**
 * web services for integration of Promis with e-GP System
 * @author sreenu
 */
@WebService()
public class PromisIntegration {
    private static final Logger LOGGER = Logger.getLogger(PromisIntegration.class);
    private String logUserId = "0";
    private static final int WEB_SERVICE_ID = 5;
    private static final String SEARCH_SERVICE_NAME = "PromisIntegrationDetailsInfoService";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     * This method gives the List of String Objects of <b>PromiseIntegration</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.Ministry Name <br>
     * 2.Division Name <br>
     * 3.Organization Name <br>
     * 4.PE office Name <br>
     * 5.PE ID <br>
     * 6.District<br>
     *
     * @param String username
     * @param String password
     * @return WSResponseObject With list data of List<String>
     */
    @WebMethod(operationName = "getSharedPEDetailForPromisIntegration")
    public WSResponseObject getSharedPEDetailForPromisIntegration(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getSharedPEDetailForPromisIntegration : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = "";
        //insert record in log table
        WebServiceUtil.insertLogRecord(WEB_SERVICE_ID, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WEB_SERVICE_ID,
                SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> requiredpromisIntegrationDetailsList = new ArrayList<String>();
            try {
                // get the list of PE Office Detail
                requiredpromisIntegrationDetailsList = getRequiredList();
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.error("getSharedPEDetailForPromisIntegration : " + logUserId + e);
            }
            if (requiredpromisIntegrationDetailsList != null) {
                if (requiredpromisIntegrationDetailsList.size() <= 0) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(requiredpromisIntegrationDetailsList);
                }
            }//end IF condition with NULL checking
        }//end IF condition with "isListDataRequired" checking
        LOGGER.debug("getSharedPEDetailForPromisIntegration : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    private List<String> getRequiredList() throws Exception {
        LOGGER.debug("getRequiredList : " + logUserId + WSResponseObject.LOGGER_START);
        List<String> requiredpromisIntegrationDetailsList = new ArrayList<String>();
        List<PromisIntegrationDetails> promisIntegrationDetailsList = new ArrayList<PromisIntegrationDetails>();
        PromisIntegrationDetailsInfoService promisIntegrationDetailsInfoService =
                (PromisIntegrationDetailsInfoService) AppContext.getSpringBean(SEARCH_SERVICE_NAME);
        promisIntegrationDetailsList = promisIntegrationDetailsInfoService.getSharedPEDetailForPromis();
        if (promisIntegrationDetailsList != null) {
            for (PromisIntegrationDetails promisIntegrationDetails : promisIntegrationDetailsList) {
                StringBuilder sbCSV = new StringBuilder();
                sbCSV.append(promisIntegrationDetails.getPEId());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(promisIntegrationDetails.getPEOfficeName());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(promisIntegrationDetails.getDeptType());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(promisIntegrationDetails.getMinistryName());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(promisIntegrationDetails.getDivisionName());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(promisIntegrationDetails.getOrganizationName());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(promisIntegrationDetails.getDistrictName());
               // sbCSV.append(WSResponseObject.APPEND_CHAR);  Commended by Sudhir 08072011
                requiredpromisIntegrationDetailsList.add(sbCSV.toString());
            }//end FOR loop
        }//end IF condition
        LOGGER.debug("getRequiredList : " + logUserId + WSResponseObject.LOGGER_END);
        return requiredpromisIntegrationDetailsList;
    }
}
