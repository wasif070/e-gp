package com.cptu.egp.eps.web.utility;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author test
 */
import java.io.File;
import java.util.HashMap;
import java.util.Hashtable;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.log4j.Logger;
import org.w3c.dom.*;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLReader {
    static final Logger logger = Logger.getLogger(XMLReader.class);
    private static java.util.HashMap<String, String> hmMsg = null;
    private static java.util.Hashtable<String, String> bnkMsg = null;
    public static HashMap<String, String> getHmMsg(){
        return hmMsg;
    }
    public static void setHmMsg(HashMap<String, String> hmMsg){
        XMLReader.hmMsg = hmMsg;
    }

    public static Hashtable<String, String> getBnkMsg() {
        return bnkMsg;
    }

    public static void setBnkMsg(Hashtable<String, String> bnkMsg) {
        XMLReader.bnkMsg = bnkMsg;
    }


    public XMLReader(){
        logger.debug("XMLReader Constructor : Start");
        if(hmMsg == null && bnkMsg == null){
            logger.debug("XMLReader Constructor : hmMsg is null");
            try{
                hmMsg = new java.util.HashMap<String, String>();
                bnkMsg = new java.util.Hashtable<String, String>();
                
                File file = new File("C:\\eGP\\Document\\ConfigParams.xml");
                logger.debug("XMLReader Constructor : file = " + file);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                
                if(file!=null){
                    
                    Document doc = db.parse(file);
                    doc.getDocumentElement().normalize();
                    NodeList nodeLst = doc.getElementsByTagName("config");
                    
                    for (int s = 0; s < nodeLst.getLength(); s++) {
                        
                        Node fstNode = nodeLst.item(s);
                        if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element fstElmnt = (Element) fstNode;
                            NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("*");
                            
                            for(int i=0;i<fstNmElmntLst.getLength();i++){
                                if(i%2==1){
                                    Element fstNmElmntValue = (Element) fstNmElmntLst.item(i);
                                    NodeList fstNmValue = fstNmElmntValue.getChildNodes();
                                    Element fstNmElmntKey = (Element) fstNmElmntLst.item(i-1);
                                    NodeList fstNmKey = fstNmElmntKey.getChildNodes();
                                    hmMsg.put(((Node) fstNmKey.item(0)).getNodeValue(), ((Node) fstNmValue.item(0)).getNodeValue());
                                }
                            }
                        }
                    }
                     NodeList bnkLst = doc.getElementsByTagName("bank");
                    // loop to read bank config e.g bank node
                    for (int s = 0; s < bnkLst.getLength(); s++) {

                        Node fstNode = bnkLst.item(s);
                        if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element fstElmnt = (Element) fstNode;
                            NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("*");

                            for(int i=0;i<fstNmElmntLst.getLength();i++){
                                if(i%2==1){
                                    Element fstNmElmntValue = (Element) fstNmElmntLst.item(i);
                                    NodeList fstNmValue = fstNmElmntValue.getChildNodes();
                                    Element fstNmElmntKey = (Element) fstNmElmntLst.item(i-1);
                                    NodeList fstNmKey = fstNmElmntKey.getChildNodes();
                                    bnkMsg.put(((Node) fstNmKey.item(0)).getNodeValue(), ((Node) fstNmValue.item(0)).getNodeValue());
                                }
                            }
                        }
                    }
                    System.out.print("bkmap " +bnkLst);
                }
            }catch(Exception ex){
                ex.printStackTrace();
                logger.error("XMLReader Constructor : Catch " + ex.toString());
            }
        }
        logger.debug("XMLReader Constructor : Ends");
    }
    public static String getMessage(String stringMsg) {
        logger.debug("getMessage : Start");
        logger.debug("getMessage : stringMsg = " + stringMsg);
        String strToRetVal = "";
        try{
            XMLReader xmlReader = new XMLReader();
            java.util.HashMap<String, String> hmget = xmlReader.getHmMsg();
            strToRetVal = hmget.get(stringMsg);
            hmget = null;
            xmlReader = null;
        }catch(Exception ex){
            logger.error("getMessage : " + ex.toString());
        }
        logger.debug("getMessage : Ends");
        return strToRetVal;
    }
        public static String getBankMessage(String stringMsg) {
        logger.debug("getBankMessage : Start");
        logger.debug("getBankMessage : stringMsg = " + stringMsg);
        String strToRetVal = "";
        try{
            XMLReader xmlReader = new XMLReader();
            java.util.Hashtable<String, String> hmget = xmlReader.getBnkMsg();
            strToRetVal = hmget.get(stringMsg);
            hmget = null;
            xmlReader = null;
        }catch(Exception ex){
            logger.error("getBankMessage : " + ex.toString());
        }
        logger.debug("getBankMessage : Ends");
        return strToRetVal;
    }
}
