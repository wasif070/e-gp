/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.service.serviceinterface.SubContractingService;
import com.cptu.egp.eps.web.databean.ProcessSubContractInvDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.model.table.TblSubContracting;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import java.util.List;
import org.apache.log4j.Logger;
/**
 *
 * @author Administrator
 */
public class ProcessSubContractInvSrBean
{
    final Logger logger = Logger.getLogger(ProcessSubContractInvSrBean.class);
    private String logUserId ="0";
    
    SubContractingService subContractingService =(SubContractingService) AppContext.getSpringBean("SubContractingService");

    public void setLogUserId(String logUserId) {
        subContractingService.setUserId(logUserId);
        this.logUserId = logUserId;
    }
  public ProcessSubContractInvDtBean setSubcontractDetails(ProcessSubContractInvDtBean processSubContractInvDtBean) {

      logger.debug("setSubcontractDetails : "+logUserId+" Starts");
      try{
      TblSubContracting details=subContractingService.getSubcontractDetails(processSubContractInvDtBean.getTenderId());
      processSubContractInvDtBean.setSubConId(details.getSubConId());
      processSubContractInvDtBean.setInvToUserId(details.getInvToUserId());
      processSubContractInvDtBean.setInvFromUserId(details.getInvFromUserId());
      processSubContractInvDtBean.setRemarks(details.getRemarks());
      processSubContractInvDtBean.setLastAcceptDt(details.getLastAcceptDt());
      processSubContractInvDtBean.setInvSentDt(details.getInvSentDt());
      processSubContractInvDtBean.setInvAcceptDate(details.getInvAcceptDate());
      }catch(Exception ex){
        logger.error("setSubcontractDetails : "+logUserId+" : "+ex.toString());
      }
      
      logger.debug("setSubcontractDetails : "+logUserId+" Ends");
      return processSubContractInvDtBean;

  }
  
   public ProcessSubContractInvDtBean setSubContEmailId(ProcessSubContractInvDtBean processSubContractInvDtBean){
       logger.debug("setSubContEmailId : "+logUserId+" Starts");
       try{
       TblLoginMaster details=subContractingService.getSubContEmailId(processSubContractInvDtBean.getTenderId());
       processSubContractInvDtBean.setEmailId(details.getEmailId());
       }catch(Exception ex){
           logger.error("setSubContEmailId : "+logUserId+" : "+ex.toString());
       }
       logger.debug("setSubContEmailId : "+logUserId+" Ends");
       return processSubContractInvDtBean;
   }

    public boolean sendMail(String emaiId,String name,String tenderId,String tenderRefNo,String invAcceptStatus, List<Object[]> listMailDtl)
     {
          logger.debug("sendMail : "+logUserId+" Starts");
          boolean mailSent = false;
          try
          {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {emaiId};
            MailContentUtility mailContentUtility = new MailContentUtility();

             List list = mailContentUtility.getProcessSubContractInv(emaiId,name,tenderId,tenderRefNo,invAcceptStatus,listMailDtl);
             String mailSub = list.get(0).toString();
             String mailText = list.get(1).toString();
             sendMessageUtil.setEmailTo(mailTo);
             sendMessageUtil.setEmailSub(mailSub);
             sendMessageUtil.setEmailMessage(mailText);
             sendMessageUtil.sendEmail();
             mailSent = true;
          }
          catch(Exception ex)
          {
              logger.error("sendMail : "+logUserId+" : "+ex.toString());
          }
          logger.debug("sendMail : "+logUserId+" Ends");
          return mailSent;
     }
     public boolean sendMail(String emaiId,String invAcceptStatus,String acceptComments,String name, List<Object[]> listMailDtl)
     {
          logger.debug("sendMail : "+logUserId+" Starts");
          boolean mailSent = false;
          try
          {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {emaiId};
            MailContentUtility mailContentUtility = new MailContentUtility();

             List list = mailContentUtility.getSubContractInvtDetails(emaiId,invAcceptStatus,acceptComments,name,listMailDtl);
             String mailSub = list.get(0).toString();
             String mailText = list.get(1).toString();
             sendMessageUtil.setEmailTo(mailTo);
             sendMessageUtil.setEmailSub(mailSub);
             sendMessageUtil.setEmailMessage(mailText);
             sendMessageUtil.sendEmail();
             mailSent = true;
          }
          catch(Exception ex)
          {
                logger.error("sendMail : "+logUserId+" : "+ex.toString());
          }
          logger.debug("sendMail : "+logUserId+" Ends");
          return mailSent;
     }
}
