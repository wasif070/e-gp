/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.security;

/**
 *
 * @author darshan
 */
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

class SendRedirectOverloadedResponse extends HttpServletResponseWrapper {

    //private HttpServletRequest m_request;
    //private HttpServletResponse m_response;
    private String prefix = null;
    private String url = null;
    //private String reqHeader = null;

    public SendRedirectOverloadedResponse(HttpServletRequest inRequest, HttpServletResponse response) {
        super(response);
        url = inRequest.getRequestURL().toString().substring(0, inRequest.getRequestURL().toString().indexOf(inRequest.getRequestURI()));
        //reqHeader = inRequest.getHeader("referer");
        //m_request = inRequest;
        //m_response = response;
        prefix = getPrefix(inRequest)+"/";
        //System.out.println("SendRedirectOverloadedResponse() :: prefix ---->" + prefix);
    }

    @Override
    public void sendRedirect(String location) throws IOException {
        //System.out.println("sendRedirect() :: Going originally to:" + location);
        String finalurl = null;
        // Handaling of sessiontimeoutpage and stacktrace page i.e. error page
        if(isSessionTimeOutPage(location)){
            finalurl = url + location;
        }else{
            if (isUrlAbsolute(location)) {
                //System.out.println("sendRedirect() :: This url is absolute. No scheme changes will be attempted");
                finalurl = location;
            } else {
                //System.out.println(" aaaaaaaaaaaaaaaaaaaaaaaa " + prefix + location);
                finalurl = fixForScheme(prefix + location);
                //System.out.println("sendRedirect() :: Going to absolute url:" + finalurl);
            }
        }
        super.sendRedirect(finalurl);
    }

    public boolean isUrlAbsolute(String url) {
        String lowercaseurl = url.toLowerCase();
        if (lowercaseurl.startsWith("http") == true) {
            return true;
        } else {
            return false;
        }
    }

    public String fixForScheme(String url) {
        //alter the url here if you were to change the scheme
        //System.out.println("*-----> URL " + url);
        if(url.contains("http")){
           url = url.replace("http://", "https://");
        }
        //System.out.println(" fixForScheme() :: final url " + url);
        return url;
    }

    public boolean isSessionTimeOutPage(String location) {
        boolean boo = false;
        if(location.indexOf("SessionTimedOut") > 0 || location.indexOf("StackTrace") > 0){
            boo = true;
        }
        return boo;
    }

    public String getPrefix(HttpServletRequest request) {
        StringBuffer str = request.getRequestURL();
        //System.out.println(" getPrefix() :: " + str);
        String url = str.toString();
        //String uri = request.getRequestURI();
        int offset = url.lastIndexOf("/");
        String prefix_t = url.substring(0, offset);
        return prefix_t;
    }
}