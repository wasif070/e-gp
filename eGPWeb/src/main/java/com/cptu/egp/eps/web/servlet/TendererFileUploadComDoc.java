/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.databean.TempCompanyDocumentsDtBean;
import com.cptu.egp.eps.web.servicebean.DocumentBriefcaseSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HashUtil;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

/**
 *
 * @author parag
 */
public class TendererFileUploadComDoc extends HttpServlet
{

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("FileUploadServlet");
    private File destinationDir;
    DataInputStream dis = null;
    BufferedInputStream bis = null;
    FileInputStream fis = null;
    String logUSerId = "0";
    Logger LOGGER = Logger.getLogger(InitDebarment.class);

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory())
        {
            tmpDir.mkdir();
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

        String hashVal = "";
        String realPath = DESTINATION_DIR_PATH + request.getSession().getAttribute("userId");//request.getSession().getAttribute("userId")
        destinationDir = new File(realPath);
        if (!destinationDir.isDirectory())
        {
            destinationDir.mkdir();
        }
        response.setContentType("text/html");
        try
        {


            DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
            /*
             *Set the size threshold, above which content will be stored on disk.
             */
            fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB
		/*
             * Set the temporary directory to store the uploaded files of size above threshold.
             */
            fileItemFactory.setRepository(tmpDir);

            ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
            try
            {
                /*
                 * Parse the request
                 */
                List items = uploadHandler.parseRequest(request);
                Iterator itr = items.iterator();
                //For Supporting Document
                TempCompanyDocumentsDtBean tempCompanyDocumentsDtBean = new TempCompanyDocumentsDtBean();
                int tendererid = 0;
                String funName = "";
                int tenderId = 0;
                int formId = 0;
                int userId = 0;
                String lotId = "0";
                String strpg = "";
                String manDocId = null;
                String docSizeMsg = "";
                boolean hashfail = false;
                boolean checkret = false;
                boolean sameFile = false;
                String hashval = "";
                while (itr.hasNext())
                {
                    FileItem item = (FileItem) itr.next();
                    //For Supporting Document
                    /*
                     * Handle Form Fields.
                     */
                    if (item.isFormField())
                    {
                        try
                        {
                            if (item.getFieldName().equals("documentBrief"))
                            {
                                if (item.getString() == null)
                                {
                                    tempCompanyDocumentsDtBean.setDocumentBrief("");
                                }
                                else
                                {
                                    tempCompanyDocumentsDtBean.setDocumentBrief(item.getString());
                                }
                            }
                            else if (item.getFieldName().equals("documentType"))
                            {
                                tempCompanyDocumentsDtBean.setDocumentType(item.getString());
                            }
                            else if (item.getFieldName().equals("tendererId"))
                            {
                                tendererid = Integer.parseInt(item.getString());
                            }
                            else if (item.getFieldName().equals("funName"))
                            {
                                funName = item.getString();
                            }
                            else if (item.getFieldName().equals("tenderId"))
                            {
                                tenderId = Integer.parseInt(item.getString());
                            }
                            else if (item.getFieldName().equals("formId"))
                            {
                                formId = Integer.parseInt(item.getString());
                            }
                            else if (item.getFieldName().equals("userId"))
                            {
                                userId = Integer.parseInt(item.getString());
                            }
                            else if (item.getFieldName().equals("manDocId"))
                            {
                                manDocId = item.getString();
                            }
                            else if (item.getFieldName().equals("lotId"))
                            {
                                if (item.getString() != null)
                                {
                                    lotId = item.getString();
                                }
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }

                }
                itr = null;
                List items1 = items;
                itr = items1.iterator();
                while (itr.hasNext())
                {
                    try
                    {
                        FileItem item = (FileItem) itr.next();

                        String fileName = "";
                        if (item.getName().lastIndexOf("\\") != -1)
                        {
                            fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                        }
                        else
                        {
                            fileName = item.getName();
                        }
                        //fileName  = fileName.replaceAll(" ", "");
                        File f = new File(destinationDir + "\\" + fileName);
                        if (f.isFile())
                        {
                            sameFile = true;
                            break;
                        }
                        docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                        if (!docSizeMsg.equals("ok"))
                        {
                            break;
                        }
                        checkret = checkExnAndSize(fileName, item.getSize(), "tenderer");
                        if (!checkret)
                        {
                            break;
                        }
                        tempCompanyDocumentsDtBean.setDocumentName(fileName);
                        //tempCompanyDocumentsDtBean.setDocumentSize("" + item.getSize());
                        File file = new File(destinationDir, fileName);
                        item.write(file);

                        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                        tempCompanyDocumentsDtBean.setDocumentSize(String.valueOf(fileEncryptDecryptUtil.fileEncryptUtil(file, (int) item.getSize())));
                        fileEncryptDecryptUtil = null;

                        try
                        {
                            fis = new FileInputStream(file);
                            byte[] buf = new byte[(int) file.length()];

                            int offset = 0;
                            int numRead = 0;
                            while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0))
                            {

                                offset += numRead;
                            }
                            fis.close();

                            hashval = HashUtil.getHash(new String(buf), "SHA-1");
                            if (hashval == null || "".equals(hashval.trim()))
                            {
                                file.delete();
                                hashfail = true;
                            }
                            tempCompanyDocumentsDtBean.setDocHash(hashval);

                        }
                        catch (Exception e)
                        {
                            LOGGER.error("Error1 in TendererFileUploadComDoc for user id:"+userId+" error is:"+e);
                        }

                    }
                    catch (Exception e)
                    {
                       LOGGER.error("Error2 in TendererFileUploadComDoc for user id:"+userId+" error is:"+e);
                    }
                }
//                if(sameFile){
//                    docSizeMsg="";
//                }

                String queryString = "";
                boolean flag = false;
                String pageName = request.getHeader("referer").substring(request.getHeader("referer").lastIndexOf("/") + 1, request.getHeader("referer").lastIndexOf(".jsp") + 4);
                if (sameFile)
                {
                    queryString = "?sf=y";//IMP note : sf means same file upload message.
                }
                else
                {
                    if (!docSizeMsg.equals("ok"))
                    {
                        queryString = "?fq=" + docSizeMsg;//IMP note : fq means if total file quota of user increases than it is check through SP p_getappcommondat(checkQuota) and message is given.
                        //response.sendRedirect("SupportingDocuments.jsp?fq="+docSizeMsg);
                    }
                    else
                    {
                        if (!checkret)
                        {
                            CheckExtension ext = new CheckExtension();
                            TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("tenderer");
                            queryString = "?fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension();//IMP note : fs means single file size and ft means single file type if does'nt match in DB than this message is fired.
                            //response.sendRedirect("SupportingDocuments.jsp?fs="+configurationMaster.getFileSize()+"&ft="+configurationMaster.getAllowedExtension());
                        }
                        else
                        {
                            String idType="userId";
                            int auditId=Integer.parseInt(session.getAttribute("userId").toString());
                            String auditAction="Bidder has upload document to Common Documents Library";
                            String moduleName=EgpModule.CDL.getName();
                            String remarks=tempCompanyDocumentsDtBean.getDocumentBrief();
                            try
                            {                                
                                //For Supporting Document                        
                                if (funName.equals("brifcasedoc") && !hashfail)
                                {
                                    DocumentBriefcaseSrBean documentBriefcaseSrBean = new DocumentBriefcaseSrBean();
                                    documentBriefcaseSrBean.uploadDoc(tempCompanyDocumentsDtBean, tendererid);
                                }
                                else if (funName.equals("mapDocForm") && !hashfail)
                                {                                   
                                    int comDocId = 0;
                                    //("  in file Upload Servlet --------- mapDocForm");
                                    DocumentBriefcaseSrBean documentBriefcaseSrBean = new DocumentBriefcaseSrBean();                                   
                                    boolean isFileExist = false;
                                     List items2 = items;
                                    itr = items2.iterator();
                                  while (itr.hasNext())
                                   {
                                      FileItem item3 = (FileItem) itr.next();
                                      if(item3.getName() !=null)
                                      {
                                        String fileName = "";
                                        if (item3.getName().lastIndexOf("\\") != -1)
                                        {
                                             fileName = item3.getName().substring(item3.getName().lastIndexOf("\\") + 1, item3.getName().length());
                                        }
                                        else
                                        {   fileName = item3.getName(); }
                                            File file = new File(destinationDir, fileName);
                                            if (file.exists()) { isFileExist = true;  }
                                            else {
                                                    isFileExist = false;
                                                    queryString = "?fup=y";
                                                    break;
                                                }
                                      }
                                    }
                                    if(isFileExist)
                                        {
                                        comDocId = documentBriefcaseSrBean.uploadDoc(tempCompanyDocumentsDtBean, tendererid);
                                    }
                                    else
                                    {comDocId =0;}
                                    if(comDocId!=0){
                                        String strMessage = documentBriefcaseSrBean.formMapDocData(tenderId, formId, userId, comDocId, manDocId);//taher
                                        flag = true;
                                    }                                    
                                }
                            }
                            catch(Exception ex)
                            {
                                auditAction="Error in Bidder has upload document to Common Documents Library "+ex.getMessage();
                            }
                            finally
                            {
                                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);  
                            }
                        }
                    }
                }
                if ("TendererFormDocMap.jsp".equalsIgnoreCase(pageName))
                {
                    if (queryString.equals(""))
                    {
                        queryString += "?tenderId=" + tenderId + "&formId=" + formId + "&lotId=" + lotId + "&flag=" + flag;
                    }
                    else
                    {
                        queryString += "&tenderId=" + tenderId + "&formId=" + formId + "&lotId=" + lotId + "&flag=" + flag;
                    }
                }

                if (hashfail)
                {
                    //response.sendRedirect(request.getContextPath()+"/tenderer/TendererFormDocMap.jsp?msg=hashfail");
                    response.sendRedirect("tenderer/TendererFormDocMap.jsp?msg=hashfail");
                }
                else
                {
                    //response.sendRedirect("tenderer/"+pageName+queryString);
                    response.sendRedirect("tenderer/" + pageName + queryString);
                }

            }
            catch (FileUploadException ex)
            {
                log("Error encountered while parsing the request", ex);
            }
            catch (Exception ex)
            {
                log("Error encountered while uploading file", ex);
            }

        }
        finally
        {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

    public boolean checkExnAndSize(String extn, long size, String userType)
    {
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);

        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++)
        {
            if (str1[i].trim().equalsIgnoreCase(lst))
            {
                chextn = true;
            }
        }
        if (chextn)
        {
            fsize = size / (1024 * 1024);
            dsize = configurationMaster.getFileSize();
            if (dsize > fsize)
            {
                chextn = true;
            }
            else
            {
                chextn = false;
            }
        }
        return chextn;
    }

    public String docSizeMsg(int userId)
    {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        return userRegisterService.docSizeCheck(userId);
    }
}
