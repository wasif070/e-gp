/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblTscstatus;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.TblTscstatusService;
import com.cptu.egp.eps.web.servicebean.TscApptoAaSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class EvalTscAppToAa extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EvalTscAppToAa</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EvalTscAppToAa at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
            String tenderId = request.getParameter("tenderId");
            String userMail = "";
            String aaMail = "0";
            String action = "";
            HttpSession session = request.getSession();
            TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            if (request.getParameter("action") != null) {
                action = request.getParameter("action");
                if (action.equalsIgnoreCase("create")) {
                    for (SPTenderCommonData sptcd : tenderCS.returndata("getTenderConfigData", tenderId, null)) {
                        aaMail = sptcd.getFieldName7();
                        userMail = sptcd.getFieldName2();
                        if (sptcd != null) {
                            sptcd = null;
                        }
                    }
                } else {
                    for (SPTenderCommonData sptcd : tenderCS.returndata("GetTecCpEmail", tenderId, null)) {
                        aaMail = sptcd.getFieldName1();
                        //userMail = sptcd.getFieldName2();
                        if (sptcd != null) {
                            sptcd = null;
                        }
                    }
                }

                String userId = session.getAttribute("userId").toString();
                boolean flag = false;

                TblTscstatusService tblTscstatusService = (TblTscstatusService) AppContext.getSpringBean("TblTscstatusService");
                TblTscstatus tblTscstatus = insertData(request, aaMail);

                if (tblTscstatus != null) {
                    if (action.equalsIgnoreCase("create")) {
                        flag = tblTscstatusService.addData(tblTscstatus);
                    } else {
                        flag = tblTscstatusService.updateData(tblTscstatus);
                    }
                }
                if (flag) {

                    for (SPTenderCommonData sptcd : tenderCS.returndata("getEmailIdfromUserId", aaMail, null)) {
                        aaMail = sptcd.getFieldName1();
                        if (sptcd != null) {
                            sptcd = null;
                        }
                    }
                    boolean mailSent = sendMail(aaMail, tenderId);
                    if (mailSent) {
                        if (action.equalsIgnoreCase("create")) {
                            String activityId = request.getParameter("activityid");
                            String objectid = request.getParameter("objectid");
                            String childId = request.getParameter("childid");
                            String eventid = request.getParameter("eventid");
                            response.sendRedirect("officer/FileProcessing.jsp?activityid="+activityId+"&objectid="+objectid+"&childid="+childId+"&eventid="+eventid+"&fromaction=tsceval");
                        } else {
                            response.sendRedirect("officer/EvalCommTSC.jsp?tenderid=" + tenderId);
                        }
                    }
                }

            }
        } finally {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private TblTscstatus insertData(HttpServletRequest request,String aaMail){
        TblTscstatus tblTscstatus = new TblTscstatus();
        int tenderId= Integer.parseInt(request.getParameter("tenderId"));
        String action="";
        HttpSession session = request.getSession();
        
        int userId = Integer.parseInt(session.getAttribute("userId").toString());
        if(request.getParameter("action")!=null){
            int aaId = Integer.parseInt(aaMail);
                action = request.getParameter("action");
                if(action.equalsIgnoreCase("create")){
                    tblTscstatus.setSendToAa("Yes");
                    tblTscstatus.setStatus("Pending");
                    tblTscstatus.setPesentDt(new Date());
                    tblTscstatus.setAasentDt(null);
                    tblTscstatus.setPeUserId(userId);
                    tblTscstatus.setAauserId(aaId);
                    tblTscstatus.setIsTeccp("");
                }else{
                    TscApptoAaSrBean tscApptoAaSrBean = new TscApptoAaSrBean();
                    tblTscstatus = tscApptoAaSrBean.getDataforUpdate(tenderId);
                    tblTscstatus.setAasentDt(new Date());
                    tblTscstatus.setIsTeccp("Yes");
                    if ("Approve".equals(request.getParameter("Approve"))) {
                    tblTscstatus.setStatus("Approve");
                    }if ("Reject".equals(request.getParameter("Reject"))) {
                        tblTscstatus.setStatus("Reject");
                    }
                }
        }
        tblTscstatus.setTenderId(tenderId);
        
        
        return tblTscstatus;
    }

     public boolean sendMail(String emaiId,String tenderId)
     {
          boolean mailSent = false;
          try
          {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {emaiId};
            MailContentUtility mailContentUtility = new MailContentUtility();

             List list = mailContentUtility.evalAppToAa(tenderId);
             String mailSub = list.get(0).toString();
             String mailText = list.get(1).toString();
             sendMessageUtil.setEmailTo(mailTo);
             sendMessageUtil.setEmailSub(mailSub);
             sendMessageUtil.setEmailMessage(mailText);
             sendMessageUtil.sendEmail();
             mailSent = true;
          }
          catch(Exception ex)
          {
               System.out.println("Exception:" +ex);
          }

          return mailSent;
     }
}
