/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.service.serviceinterface.ForgotPasswordService;
import com.cptu.egp.eps.service.serviceinterface.ResetCodeService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ResetPasswordSrBean extends HttpServlet {

    static final Logger logger = Logger.getLogger(ResetPasswordSrBean.class);
    private String logUserId = "0";
    ForgotPasswordService forgotPasswordService = (ForgotPasswordService) AppContext.getSpringBean("ForgotPasswordService");
    ResetCodeService resetCodeService = (ResetCodeService) AppContext.getSpringBean("ResetCodeService");

    /**
     *For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
        forgotPasswordService.setLogUserId(logUserId);
        resetCodeService.setUserId(logUserId);
    }

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.debug("processRequest : " + logUserId + " Starts");
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Object objUserId = session.getAttribute("userId");
        if(objUserId != null){
            logUserId = objUserId.toString();
        }
        setLogUserId(logUserId);
        forgotPasswordService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
        //try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
        out.println("<title>Servlet ResetPasswordSrBean</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ResetPasswordSrBean at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
             */
        // } finally {
        //  out.close();
//        }
        logger.debug("processRequest : " + logUserId + " Ends");
        }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {
            String functionName = request.getParameter("funName");

            if (functionName != null && functionName.equals("chkMailId")) {
                try {
                    String emailId = request.getParameter("forgetemailId");
                    String str = chkMailId(emailId);
                    //String action = request.getParameter("action");
                    if (!str.startsWith("E:")) {
                        int userId = Integer.parseInt(str);
                        sendMail(emailId, userId);
                        str = "Verification mail has been sent to your e-mail ID!!";
                    }
                    out.print(str);
                    out.flush();
                } catch (Exception ex) {
                    logger.error("doPost : " + logUserId + " : " + ex);
                    response.sendRedirect("ResetPassword.jsp?msg=fail");
                }
            } else if (functionName.equals("verifyCode")) {
                String emailId = request.getParameter("emailId");
                String verificationCode = request.getParameter("verificationCode");
                HttpSession session = request.getSession(true);
                if (verifyCode(emailId, verificationCode)) {
                   // HttpSession session = request.getSession(true);
                    session.setAttribute("emailId", emailId);
                    session.setAttribute("previous", "reset");
                    response.sendRedirect(request.getContextPath() + "/admin/ChngPassword.jsp");
                } else {
                    session.setAttribute("email", emailId);
                    response.sendRedirect("VerifyCode.jsp?msg=fail");
                }
            }

        } catch (Exception ex) {
            logger.error("doPost : " + logUserId + " : " + ex);
            response.sendRedirect("ResetPassword.jsp?msg=fail");
        } finally {
            out.close();
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String chkMailId(String mailId) {
        logger.debug("chkMailId : " + logUserId + " Starts");
        String strStatus = "";
        if (mailId == null || mailId.trim().length() == 0) {
            strStatus = "Blank";
        } else {
        try {
                strStatus = forgotPasswordService.chkMailId(mailId, "reset");
            } catch (Exception ex) {
                logger.error("chkMailId : " + logUserId + " : " + ex);
                strStatus = "";
        }
        }
        logger.debug("chkMailId : " + logUserId + " Ends");
        return strStatus;
        }

    private boolean sendMail(String mailId, int userId) {
        logger.debug("sendMail : " + logUserId + " Starts");
        boolean mailSent = false, resetCodeFlag = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            MailContentUtility mailContentUtility = new MailContentUtility();
            String resetCode = sendMessageUtil.getRandomName().toString();
            resetCodeFlag = setResetCode(resetCode, userId);

            if (resetCodeFlag) {

                List list = mailContentUtility.getResetMailContent(resetCode);
                String mailSub = list.get(0).toString();
                String mailText = list.get(1).toString();
                sendMessageUtil.setEmailTo(mailTo);
                sendMessageUtil.setEmailSub(mailSub);
                sendMessageUtil.setIsForgotPassword(true);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                mailSent = true;
            }
        } catch (Exception ex) {
            logger.error("sendMail : " + logUserId + " : " + ex);
        }
        logger.debug("sendMail : " + logUserId + " Ends");
        return mailSent;
    }

    private boolean setResetCode(String resetCode, int userId) {
        logger.debug("setResetCode : " + logUserId + " Starts");
        boolean bSuccess = false;
        try {
            resetCodeService.storeResetCode(userId, resetCode);
            bSuccess = true;
        } catch (Exception ex) {
            logger.error("setResetCode : " + logUserId + " : " + ex);
            bSuccess = false;
        }
        logger.debug("setResetCode : " + logUserId + " Ends");
        return bSuccess;
        }

    private boolean verifyCode(String mailId, String verificationCode) {
        logger.debug("verifyCode : " + logUserId + " Starts");
        boolean verifiedFlag = false;
        try {
            verifiedFlag = resetCodeService.verifyResetCode(mailId, verificationCode);

        } catch (Exception ex) {
            logger.error("verifyCode : " + logUserId + " : " + ex);
            verifiedFlag = false;
        }
        logger.debug("verifyCode : " + logUserId + " Ends");
        return verifiedFlag;
    }
}
