/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rishita
 */
public class CommListingServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String userId, comType = "", status = "";
            userId = request.getSession().getAttribute("userId").toString();
            comType = request.getParameter("comType");
            status = request.getParameter("status");

            String strPageNo = request.getParameter("pageNo");
            int pageNo = Integer.parseInt(strPageNo);
            String strOffset = request.getParameter("size");
            int recordOffset = Integer.parseInt(strOffset);
            String deptId = "";
            if (request.getParameter("deptId") != null) {
                deptId = request.getParameter("deptId");
            }
            String officeId = "";
            if (request.getParameter("officeId") != null) {
                officeId = request.getParameter("officeId");
            }
            String tenderId = "";
            if (request.getParameter("tenderId") != null) {
                tenderId = request.getParameter("tenderId");
            }
            String openingDt = "";
            if (request.getParameter("deptId") != null) {
                openingDt = request.getParameter("openingDt");
            }
            String refNo = "";
            if (request.getParameter("refNo") != null) {
                refNo = request.getParameter("refNo");
            }
            if (openingDt != null) {
                if (!"".equalsIgnoreCase(openingDt)) {
                    String[] dtFrmArr = openingDt.split("/");
                    openingDt = dtFrmArr[2] + "-" + dtFrmArr[1] + "-" + dtFrmArr[0];
                }
            }
            String funName = "";

            if(status != null){
                if("pending".equalsIgnoreCase(status)){
                    funName = "getPendingCommListing";
                }else{
                    funName = "getApprovedCommListing";
                }
            }else{
                funName = "getCommListing";
            }
            String openStatus = "";
            if(comType.equalsIgnoreCase("TOC"))
            {
                if (request.getParameter("openStatus") != null) {
                    openStatus = request.getParameter("openStatus");
                }
            comType+=openStatus;
            }
            
            String styleClass = "";
            HttpSession session = request.getSession();

            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            List<SPCommonSearchData> listing = commonSearchService.searchData(funName, comType, userId, strPageNo, strOffset, deptId, officeId, tenderId, refNo, openingDt);
            
            if (!listing.isEmpty()) {
                for (int i = 0; i < listing.size(); i++) {
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    SPCommonSearchData commonSearchListing = listing.get(i);
                    out.print("<tr class='" + styleClass + " mytr'>");
                    out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                    out.print("<td class=\"t-align-center\">" + commonSearchListing.getFieldName1() + "</td>");
                    out.print("<td class=\"t-align-center\">" + commonSearchListing.getFieldName5() + "</td>");
                    out.print("<td class=\"t-align-left\">" + commonSearchListing.getFieldName3() + "</td>");
                    out.print("<td class=\"t-align-left\">" + commonSearchListing.getFieldName6() + "</td>");
                    out.print("<td class=\"t-align-left\">" + commonSearchListing.getFieldName7() + "</td>");
                    out.print("<td class=\"t-align-center\">" + commonSearchListing.getFieldName4() + "</td>");
                    out.print("<td class=\"t-align-center\">");
                    if (comType.equals("TEC")) {
                        out.print("<a href=\"EvalComm.jsp?tenderid=" + commonSearchListing.getFieldName1() + "&comType=TEC\" title=\"Dashboard\"><img src=\"../resources/images/Dashboard/dashBoardIcn.png\" alt=\"Dashboard\" /></a>");
                    } else if (comType.equals("TSC")) {

                        if ("14".equalsIgnoreCase(session.getAttribute("userTypeId").toString())) {
                            // External Member Case
                            //out.print("<a href=\"Evalclarify.jsp?tenderId=" + commonSearchListing.getFieldName1() + "&comType=TSC&st=cl\" title=\"Dashboard\"><img src=\"../resources/images/Dashboard/dashBoardIcn.png\" alt=\"Dashboard\" /></a>");
                            out.print("<a href=\"EvalTSC.jsp?tenderId=" + commonSearchListing.getFieldName1() + "&comType=TSC\" title=\"Dashboard\"><img src=\"../resources/images/Dashboard/dashBoardIcn.png\" alt=\"Dashboard\" /></a>");
                        } else {
                            out.print("<a href=\"EvalTSC.jsp?tenderId=" + commonSearchListing.getFieldName1() + "&comType=TSC\" title=\"Dashboard\"><img src=\"../resources/images/Dashboard/dashBoardIcn.png\" alt=\"Dashboard\" /></a>");
                            //out.print("<a href=\"Evalclarify.jsp?tenderId=" + commonSearchListing.getFieldName1() + "&comType=TSC&st=cl\" title=\"Dashboard\"><img src=\"../resources/images/Dashboard/dashBoardIcn.png\" alt=\"Dashboard\" /></a>");
                        }


                        //out.print("<a href=\"EvalTSC.jsp?tenderid=" + commonSearchListing.getFieldName1() + "\" title=\"Dashboard\"><img src=\"../resources/images/Dashboard/dashBoardIcn.png\" alt=\"Dashboard\" /></a>");
                    } else if (comType.contains("TOC")) {

                        List<SPCommonSearchData> isSentTpPE = commonSearchService.searchData("getIsSentToPEStatus", commonSearchListing.getFieldName1(), "", "", "", "", "", "", "", "");

                        if ("yes".equalsIgnoreCase(isSentTpPE.get(0).getFieldName1())) {
                            out.print("<img onclick=\"ShowAlertMsg()\" style=\"cursor: pointer\" src=\"../resources/images/Dashboard/dashBoardIcn.png\" alt=\"Dashboard\" />");
                        } else {
                            out.print("<a href=\"OpenComm.jsp?tenderid=" + commonSearchListing.getFieldName1() + "\" title=\"Dashboard\"><img src=\"../resources/images/Dashboard/dashBoardIcn.png\" alt=\"Dashboard\" /></a>");
                        }

                    } else if(comType.equals("TC"))
                    {
                        out.print("<a href=\"TenderComm.jsp?tenderid=" + commonSearchListing.getFieldName1() + "&comType=TC\" title=\"Dashboard\"><img src=\"../resources/images/Dashboard/dashBoardIcn.png\" alt=\"Dashboard\" /></a>");
                    }
                    out.print("</td>");
                    out.print("</tr>");
                }
            } else {
                out.print("<tr>");
                out.print("<td id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\" colspan=\"8\">No Record Found</td>");
                out.print("</tr>");
            }
            int totalPages = 1;
            if (listing.size() > 0) {
                int rowCount = Integer.parseInt(listing.get(0).getFieldName9());
                totalPages = (int) Math.ceil(Math.ceil(rowCount) / recordOffset);
                if (totalPages == 0) {
                    totalPages = 1;
                }
            }
            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
