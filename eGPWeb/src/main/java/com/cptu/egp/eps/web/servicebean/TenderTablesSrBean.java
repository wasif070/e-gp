/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblTenderCells;
import com.cptu.egp.eps.model.table.TblTenderColumns;
import com.cptu.egp.eps.model.table.TblTenderFormula;
import com.cptu.egp.eps.model.table.TblTenderListCellDetail;
import com.cptu.egp.eps.model.table.TblTenderTables;
import com.cptu.egp.eps.service.serviceimpl.TenderFormService;
import com.cptu.egp.eps.service.serviceimpl.TenderTableService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author yagnesh
 */
public class TenderTablesSrBean {

    private String logUserId = "0";
    final Logger logger = Logger.getLogger(TenderTablesSrBean.class);
        TenderTableService tenderTableService = (TenderTableService) AppContext.getSpringBean("TenderAddFormTableService");
    TenderFormService tenderFormService = (TenderFormService) AppContext.getSpringBean("TenderAddFormService");
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        tenderTableService.setAuditTrail(auditTrail);
        tenderFormService.setAuditTrail(auditTrail);
    }
    

    public void setLogUserId(String logUserId) {
        tenderTableService.setUserId(logUserId);
        tenderFormService.setLogUserId(logUserId);
        this.logUserId = logUserId;
        }

    /**
     * Get Tender Table Details
     * @param formId
     * @param values
     * @return List of Tender Table Details
     */
    public List<TblTenderTables> getTenderTables(int formId,String...values) {
        logger.debug("getTenderTables : " + logUserId + "starts");
        List<TblTenderTables> getTenderTables = null;


        try {
            getTenderTables = tenderTableService.getFormTables(formId,values);
        } catch (Exception ex) {

            logger.error("getTenderTables : " + ex);

    }
        logger.debug("getTenderTables : " + logUserId + "ends");
        return getTenderTables;
    }

    /**
     * Get Tender Table Details
     * @param tableId
     * @return List of Tender Table Details
     */
    public List<TblTenderTables> getTenderTablesDetail(int tableId) {
        logger.debug("getTenderTablesDetail : " + logUserId + "starts");
        List<TblTenderTables> getTenderTablesDetail = null;
        try {
            getTenderTablesDetail = tenderTableService.getFormTablesDetails(tableId);
        } catch (Exception ex) {

            logger.error("getTenderTablesDetail : " + ex);

        }
        logger.debug("getTenderTablesDetail : " + logUserId + "ends");
        return getTenderTablesDetail;
    }

    /**
     * Check form is Price bid form or not
     * @param formId
     * @return true if form is price bid form else return false.
     */
    public boolean isPriceBidForm(int formId) {
        logger.debug("isPriceBidForm : " + logUserId + "starts");
        boolean isPriceBidFrm = false;
        try {

            isPriceBidFrm = tenderFormService.isBOQForm(formId);

        } catch (Exception ex) {

            logger.error("isPriceBidForm : " + ex);

        }
        logger.debug("isPriceBidForm : " + logUserId + "ends");
            return isPriceBidFrm;
        }

    /**
     * Get Form Name
     * @param formId
     * @return Form Name in String object
     */
    public String getFormName(int formId) {
        String formName = "";
        try {
            formName = "";

        } catch (Exception ex) {

            logger.error("getFormName : " + ex);

        }
        logger.debug("getFormName : " + logUserId + "ends");
            return formName;
        }

    /**
     * Check if entry in tender column exist for given table?
     * @param tableId
     * @return ture if it found any single or more than one column exist else return false.
     */
    public boolean isEntryPresentForCols(int tableId) {
        logger.debug("isEntryPresentForCols : " + logUserId + "starts");
        boolean isEntryPresent = false;
        try {

            isEntryPresent = tenderTableService.isEntryPresentInTableCols(tableId);

        } catch (Exception ex) {

            logger.error("isEntryPresentForCols : " + ex);

        }
        logger.debug("isEntryPresentForCols : " + logUserId + "ends");
            return isEntryPresent;
        }

    /**
     * Get No of Table in Form as count value
     * @param formId
     * @return no of table count in given form
     */
    public short getNoOfTablesInForm(int formId) {
        logger.debug("getNoOfTablesInForm : " + logUserId + "starts");
        short nos = 0;
        try {
            nos = tenderTableService.getNoOfTablesInForm(formId);

        } catch (Exception ex) {

            logger.error("getNoOfTablesInForm : " + ex);

        }
        logger.debug("getNoOfTablesInForm : " + logUserId + "ends");
            return nos;
        }

    /**
     * Get No. of column count in given table.
     * @param tableId
     * @return no. of. column count.
     */
    public short getNoOfColsInTable(int tableId) {
        logger.debug("getNoOfColsInTable : " + logUserId + "starts");
        short nos = 0;
        try {
            nos = tenderTableService.getNoOfColsInTable(tableId);

        } catch (Exception ex) {

            logger.error("getNoOfColsInTable : " + ex);

        }
        logger.debug("getNoOfColsInTable : " + logUserId + "ends");
            return nos;
        }

    /**
     * Get No. of Rows in table.
     * @param tableId
     * @param columnId
     * @return no. of.rows in table.
     */
    public short getNoOfRowsInTable(int tableId, short columnId) {
        logger.debug("getNoOfRowsInTable : " + logUserId + "starts");
        short nos = 0;
        try {
            nos = tenderTableService.getNoOfRowsInTable(tableId, columnId);

        } catch (Exception ex) {

            logger.error("getNoOfRowsInTable : " + ex);

        }
        logger.debug("getNoOfRowsInTable : " + logUserId + "ends");
            return nos;
        }

    /**
     * GEt column details like fill by, data type, size, etc.
     * @param tableId
     * @param inSortOrder
     * @return
     */
    public List<TblTenderColumns> getColumnsDtls(int tableId, boolean inSortOrder) {
        logger.debug("getColumnsDtls : " + logUserId + "starts");
        List<TblTenderColumns> cols = null;
        try {
            cols = tenderTableService.getTableColumns(tableId, inSortOrder);

        } catch (Exception ex) {

            logger.error("getColumnsDtls : " + ex);

        }
        logger.debug("getColumnsDtls : " + logUserId + "ends");
            return cols;
        }

    /**
     * Get Auto column details.
     * @param tableId
     * @return Get list of AUto column details
     */
    public List<TblTenderColumns> getAutoColumnsDtls(int tableId) {
        logger.debug("getAutoColumnsDtls : " + logUserId + "starts");
        List<TblTenderColumns> cols = null;
        try {
            cols = tenderTableService.getTableAutoColumns(tableId);

        } catch (Exception ex) {

            logger.error("getAutoColumnsDtls : " + ex);

        }
        logger.debug("getAutoColumnsDtls : " + logUserId + "ends");
            return cols;
        }

    /**
     * Get Tender Table Cell Details for Test Form
     * @param tableId
     * @return Tender Cell details list.
     */
    public List<TblTenderCells> getCellsDtlsTestForm(int tableId) {
        logger.debug("getCellsDtlsTestForm : " + logUserId + "starts");
        List<TblTenderCells> cells = null;
        try {
            cells = tenderTableService.getTableCellsTestForm(tableId);

        } catch (Exception ex) {

            logger.error("getCellsDtlsTestForm : " + ex);

        }
        logger.debug("getCellsDtlsTestForm : " + logUserId + "ends");
            return cells;
        }

    /**
     * Get Table Cell details
     * @param tableId
     * @return Cell Detail list
     */
    public List<TblTenderCells> getCellsDtls(int tableId) {
        logger.debug("getCellsDtls : " + logUserId + "starts");
        List<TblTenderCells> cells = null;
        try {
            cells = tenderTableService.getTableCells(tableId);

        } catch (Exception ex) {

            logger.error("getCellsDtls : " + ex);

        }
        logger.debug("getCellsDtls : " + logUserId + "ends");
            return cells;
        }


    /**
     * Get Cell details for Consolidate.
     * @param tableId
     * @return Cell Details
     */
    public List<TblTenderCells> getCelldetailsForConsolidate(int tableId) {
        logger.debug("getCelldetailsForConsolidate : " + logUserId + "starts");
        List<TblTenderCells> cells = null;
        try {
            cells = tenderTableService.getCelldetailsForConsolidate(tableId);

        } catch (Exception ex) {

            logger.error("getCelldetailsForConsolidate : " + ex);

        }
        logger.debug("getCelldetailsForConsolidate : " + logUserId + "ends");
            return cells;
        }

    /**
     * Get Cell Values row order wise
     * @param tenderId
     * @param formId
     * @param userId
     * @return cell values
     */
    public List<TblTenderCells> getCellValueForRow(int tenderId,int formId,int userId) {
        logger.debug("getCellValueForRow : " + logUserId + "starts");
        List<TblTenderCells> cells = null;
        try {
            cells = tenderTableService.getCellValueForRow(tenderId,formId,userId);

        } catch (Exception ex) {

            logger.error("getCellValueForRow : " + ex);

        }
        logger.debug("getCellValueForRow : " + logUserId + "ends");
            return cells;
        }


    /**
     * Get Tender Table formula Details for given table id.
     * @param tableId
     * @return Tender formula details
     */
    public List<TblTenderFormula> getTableFormulas(int tableId) {
        logger.debug("getTableFormulas : " + logUserId + "starts");
        List<TblTenderFormula> formulas = null;
        try {
            formulas = tenderTableService.getTableFormulas(tableId);

        } catch (Exception ex) {

            logger.error("getTableFormulas : " + ex);

        }
        logger.debug("getTableFormulas : " + logUserId + "ends");
            return formulas;
        }

    /**
     * Check if auto column exist in given table id?
     * @param tableId
     * @return true if auto column exist else return false.
     */
    public boolean isAutoColumnPresent(int tableId) {
        logger.debug("isAutoColumnPresent : " + logUserId + "starts");
        boolean isAutoColPresent = false;
        try {
            isAutoColPresent = tenderTableService.isAutoColumnPresent(tableId);

        } catch (Exception ex) {

            logger.error("isAutoColumnPresent : " + ex);

        }
        logger.debug("isAutoColumnPresent : " + logUserId + "ends");
            return isAutoColPresent;
        }

    /**
     * Check at least one formula created for given table id?
     * @param tableId
     * @return true if at least one formula exist else return false.
     */
    public boolean atLeastOneFormulaCreated(int tableId) {
        logger.debug("atLeastOneFormulaCreated : " + logUserId + "starts");
        boolean isFormulaCre = false;
        try {
            isFormulaCre = tenderTableService.atLeastOneFormulaCreated(tableId);

        } catch (Exception ex) {

            logger.error("atLeastOneFormulaCreated : " + ex);

        }
        logger.debug("atLeastOneFormulaCreated : " + logUserId + "ends");
            return isFormulaCre;
        }

    /**
     * Check if Formula type 'TOTAL' exist for given table id.
     * @param tableId
     * @return true if exist else return false.
     */
    public boolean isTotalFormulaCreated(int tableId) {
        logger.debug("isTotalFormulaCreated : " + logUserId + "starts");
        boolean isTotFormulaCre = false;
        try {
            isTotFormulaCre = tenderTableService.isTotalFormulaCreated(tableId);

        } catch (Exception ex) {

            logger.error("isTotalFormulaCreated : " + ex);

        }
        logger.debug("isTotalFormulaCreated : " + logUserId + "ends");
            return isTotFormulaCre;
        }

    /**
     * Get Grand Total Column for given table id.
     * @param tableId
     * @return column id, isGrandTotal field of Grand Total.
     */
    public HashMap<Integer, Integer> getGTColumns(int tableId) {
        logger.debug("getGTColumns : " + logUserId + "starts");
        HashMap<Integer, Integer> hmCols = null;
        try {
            hmCols = tenderTableService.getGTColumns(tableId);

        } catch (Exception ex) {

            logger.error("getGTColumns : " + ex);

        }
        logger.debug("getGTColumns : " + logUserId + "ends");
            return hmCols;
        }

    /**
     * Get count for 'Total' type formula
     * @param tableId
     * @return count of 'Total' type formula.
     */
    public short getFormulaCountOfTotal(int tableId) {
        logger.debug("getFormulaCountOfTotal : " + logUserId + "starts");
        short cnt = 0;
        try {
            cnt = tenderTableService.getFormulaCountOfTotal(tableId);

        } catch (Exception ex) {
            logger.error("getFormulaCountOfTotal : " + ex);
        }
        logger.debug("getFormulaCountOfTotal : " + logUserId + "ends");
            return cnt;
        }

    /**
     * Delete Total Formula.
     * @param tableId
     * @return true if successfully deleted else return false
     */
    public boolean deleteTotalFormula(int tableId) {
        logger.debug("deleteTotalFormula : " + logUserId + "starts");
        boolean flag = false;

        List<Object[]> obj = tenderTableService.getFormulaColIds(tableId);
        String colIds = "";
        try {
            if (obj != null) {
                if (!obj.isEmpty()) {
                    for (int o = 0; o < obj.size(); o++) {
                        
                        Integer colId = 0;
                        if (obj.get(o)[2] != null) {
                           colId = (Integer) obj.get(o)[2];
                            colIds += colId + ",";
                        }

                        
                    }
                }
            }
            colIds = colIds.substring(0, colIds.length() - 1);
            short totFormulaCnt = tenderTableService.getFormulaCountOfTotal(tableId);

            
            if (totFormulaCnt >= 1) {
               // flag = tenderTableService.deleteFormulaEffectsFromTenderCells(tableId);
               // flag = tenderTableService.deleteFormulaEffectsFromTenderTables(tableId);
               // if (flag) {
                    
                    tenderTableService.deleteTableFormula(colIds, tableId);
                //}
            }
            flag = true;

        } catch (Exception ex) {
            logger.error("deleteTotalFormula : " + ex);
            flag = false;

        } finally {
            tenderTableService = null;
        }
        logger.debug("deleteTotalFormula : " + logUserId + "ends");
        return flag;
    }

    /**
     * Get Tender cell Details
     * @param tableId
     * @param columnId
     * @param cellId
     * @return Tender cell details.
     */
    public List<TblTenderListCellDetail> getTenderListCellDetail(int tableId, short columnId, int cellId) {
        logger.debug("getTenderListCellDetail : " + logUserId + "starts");
        List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
        try {
            listCellDetail = tenderTableService.getTenderListCellDetail(tableId, columnId, cellId);
        } catch (Exception ex) {
            logger.error("getTenderListCellDetail : " + ex);
        }
        logger.debug("getTenderListCellDetail : " + logUserId + "ends");
        return listCellDetail;
    }

    public String getTenderType(int tableId) {
        logger.debug("getTenderType : " + logUserId + "starts");
        String type = "";
        List<Object> tenderType = null;
        try {
            tenderType = tenderTableService.getTenderType(tableId);
            if(tenderType.size()>0)
                type = tenderType.get(0).toString();
            else
                type = "NCT";
            
            
               
            //System.out.print(tenderType.get(0).toString());
            
        } catch (Exception ex) {
            logger.error("getTenderType : " + ex);
        }
        logger.debug("getTenderType : " + logUserId + "ends");
        return type;
    }

   
}
