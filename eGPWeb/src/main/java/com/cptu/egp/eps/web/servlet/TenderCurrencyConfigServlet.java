/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblTenderCurrency;
import com.cptu.egp.eps.service.serviceimpl.PublicForumPostService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService;
import com.cptu.egp.eps.web.servicebean.ConfigureCurrencySrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class TenderCurrencyConfigServlet extends HttpServlet {

    final int MAX_CURRENCY_PER_TENDER = 4; //Maximum how many Currencies can be added to a particular ICT Tender including BTN

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            HttpSession session = request.getSession();
            int  iUserid = Integer.parseInt(session.getAttribute("userId").toString());
            String action = request.getParameter("action");

            if(action.equalsIgnoreCase("configureTenderCurrency"))
            {

                int iTotalCurrency = Integer.parseInt(request.getParameter("hidTotalCurrency"));
                String strDefaultCurrencyId = request.getParameter("hidDefaultCurrencyId");
                String strTenderId = request.getParameter("tenderid");
                boolean bSuccess = false;

                TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");

                bSuccess = tenderCurrencyService.removeTenderCurreny(strTenderId,strDefaultCurrencyId);

                if (bSuccess) {
                    for (int i = 1; i <= iTotalCurrency; i++) {
                        TblTenderCurrency tblTenderCurrency = new TblTenderCurrency();
                        String strAtive = request.getParameter("chk_" + i);
                        if (strAtive != null) {
                            // check box is selected
                            int iCurrencyId = Integer.parseInt(request.getParameter("chk_" + i));
                            BigDecimal bigExchangeRate = new BigDecimal(request.getParameter("txt_" + i));

                            tblTenderCurrency.setCurrencyId(iCurrencyId);
                            tblTenderCurrency.setTenderId(Integer.parseInt(strTenderId));
                            tblTenderCurrency.setExchangeRate(bigExchangeRate);
                            tblTenderCurrency.setCreatedBy(iUserid);
                            tblTenderCurrency.setCreatedDate(new Date());
                            bSuccess = tenderCurrencyService.addTblTenderCurrency(tblTenderCurrency);
                        }
                    }
                    response.sendRedirect("officer/Notice.jsp?tenderid="+strTenderId);
                    //response.sendRedirectFilter("Notice.jsp?tenderid="+strTenderId);
                }
            }
            if(action.equalsIgnoreCase("AddTenderCurrency")){ //Dohatec ICT Start

                int iTenderId = Integer.parseInt(request.getParameter("tenderId"));
                int iCurrencyId = Integer.parseInt(request.getParameter("currencyId"));
                BigDecimal bigExchangeRate = new BigDecimal(0.00); //actual value will be added later;
                boolean bSuccess = false;

                TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");

                //if the Currency already added to the tender, no need to add again
                if(tenderCurrencyService.getCurrencyCountForTender(iTenderId, iCurrencyId) == 0 && tenderCurrencyService.getCurrencyCountForTender(iTenderId) < MAX_CURRENCY_PER_TENDER){

                    TblTenderCurrency tblTenderCurrency = new TblTenderCurrency();
                    tblTenderCurrency.setCurrencyId(iCurrencyId);
                    tblTenderCurrency.setTenderId(iTenderId);
                    tblTenderCurrency.setExchangeRate(bigExchangeRate);
                    tblTenderCurrency.setCreatedBy(iUserid);
                    tblTenderCurrency.setCreatedDate(new Date());
                    bSuccess = tenderCurrencyService.addTblTenderCurrency(tblTenderCurrency);
                }

                // Code added by Khaled Ben Islam for Audit Trail Log.
                String requestedUrl = request.getRequestURL().toString(); //e.g. we get http://development.eprocure.gov.bd/TenderCurrencyConfigServlet but we need http://development.eprocure.gov.bd/officer/AddCurrency.jsp
                int index = requestedUrl.indexOf("/", 10);
                String requestingUrl = requestedUrl.substring(0, index) + "/officer/AddCurrency.jsp"; //this extra work needed since in case of ajax call, request.getRequestUrl() does not return the requesting jsp page url

                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), requestingUrl);
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                String idType="tenderId";
                int auditId=0;
                String auditAction="PE User added the Currency";
                String moduleName=EgpModule.Tender_Notice.getName();
                String remarks="";
                auditId = iTenderId;

                if(bSuccess){
                    remarks="User Id:"+session.getAttribute("userId")+" add One Currency with Id:" + iCurrencyId +  " for Tender id:"+iTenderId;
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

                    out.print(iCurrencyId);
                }
                else {
                    auditAction="Error in PE User added the Currency";
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

                    out.print("0"); //currency addition failed
                }
            }
            if(action.equalsIgnoreCase("DeleteTenderCurrency")){

                String strTenderId = request.getParameter("tenderId");
                String strCheckedCurrencyIds = request.getParameter("currencyIds");
                boolean bSuccess = false;

                TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");

                String[] strCurrencyId = strCheckedCurrencyIds.split(",");
                int iTotalCount = strCurrencyId.length;
                String strCurrencyShortName = "";

                // Code added by Khaled Ben Islam for Audit Trail Log.
                String requestedUrl = request.getRequestURL().toString(); //e.g. http://development.eprocure.gov.bd/officer/AddCurrency.jsp
                int index = requestedUrl.indexOf("/", 10);
                String requestingUrl = requestedUrl.substring(0, index) + "/officer/AddCurrency.jsp"; //this extra work needed since in case of ajax call, request.getRequestUrl() does not return the requesting jsp page url

                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), requestingUrl);
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                String idType="tenderId";
                int auditId=0;
                String auditAction="PE User Delete the Currency";
                String moduleName=EgpModule.Tender_Notice.getName();
                String remarks="";
                auditId = Integer.parseInt(strTenderId);

                for(int i=0;i<iTotalCount;i++){

                    ConfigureCurrencySrBean configureCurrencySrBean = new ConfigureCurrencySrBean();
                    strCurrencyShortName = configureCurrencySrBean.getCurrencyShortName(Integer.parseInt(strCurrencyId[i]));
                    if(!("BTN".equalsIgnoreCase(strCurrencyShortName))){ //BTN is the default currency & must not be deleted
                        bSuccess = tenderCurrencyService.removeSpecificTenderCurreny(strTenderId, strCurrencyId[i]);

                        if(bSuccess){
                            remarks="User Id:"+session.getAttribute("userId")+" delete One Currency with Id:" + strCurrencyId[i] +  " for Tender id:"+strTenderId;
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                        }
                        else {
                            auditAction="Error in PE User Delete the Currency";
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                        }
                    }
                }

                //Load updated data
                String currencyRows = "";
                String addedCurrencies = ""; //comma separated IDs of Currencies added to the tender. Will be used for JavaScript Validation

                List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
                listCurrencyObj = tenderCurrencyService.getCurrencyTenderwise(Integer.parseInt(strTenderId));

                if(listCurrencyObj.size() > 0){
                    int rowNo = 0;
                    for(Object[] obj :listCurrencyObj){
                        rowNo +=1;
                        currencyRows+= "<tr>";
                        currencyRows+= "<td width='15%' class='t-align-center'>" + rowNo + "</td>";
                        currencyRows+= "<td width='70%' class='t-align-left'>" + obj[3] + " - " + obj[0] + "</td>";
                        if(obj[3].equals("BTN") || obj[3].equals("bdt")){//obj[3] holds the CurrencyShortName (e.g. BTN), BTN is the default currency and BTN must not allowed to be deleted
                            currencyRows+= "<td width='15%' class='t-align-center'><input class='checkboxDelete' type='checkbox' value='"+ obj[2] + "' disabled='disabled'></td>";
                        }
                        else {
                            currencyRows+= "<td width='15%' class='t-align-center'><input class='checkboxDelete' type='checkbox' value='"+ obj[2] + "'></td>";
                        }

                        currencyRows+= "</tr>";
                        addedCurrencies += obj[2] + ",";
                    }
                    currencyRows += "<input type='hidden' name='addedCurrencies' value=" + addedCurrencies + " id='addedCurrencies'>";
                    currencyRows += "<input type='hidden' name='totalCurrencyAdded' value=" + listCurrencyObj.size() + " id='totalCurrencyAdded'>";
                }

                out.print(currencyRows);
            }

            if(action.equalsIgnoreCase("updateCurrencyExchangeRate") && "Submit".equalsIgnoreCase(request.getParameter("submit"))){

                ConfigureCurrencySrBean configureCurrencySrBean = new ConfigureCurrencySrBean();
                int totalCurrency = Integer.parseInt(request.getParameter("count"));
                int iTenderId = Integer.parseInt(request.getParameter("tenderid"));
                BigDecimal exchangeRate = new BigDecimal(0);

                // Code added by Khaled Ben Islam for Audit Trail Log.
                String requestedUrl = request.getRequestURL().toString(); //e.g. http://development.eprocure.gov.bd/officer/AddCurrencyDetails.jsp
                int index = requestedUrl.indexOf("/", 10);
                String requestingUrl = requestedUrl.substring(0, index) + "/officer/AddCurrencyDetails.jsp"; //this extra work needed to retrive the requesting jsp page url

                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), requestingUrl);
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                String idType="tenderId";
                int auditId=0;
                String auditAction="PE User Update the Currency";
                String moduleName=EgpModule.Tender_Opening.getName();
                String remarks="";
                auditId = iTenderId;

                boolean bSuccess = false;
                if(totalCurrency > 0){
                    String[] ids = request.getParameter("addedCurrencies").split(",");
                    for(int i=0; i<totalCurrency; i++)
                    {
                        if(request.getParameter("currencyRate_" + ids[i]) != null){
                            exchangeRate = new BigDecimal(request.getParameter("currencyRate_" + ids[i]));
                        }
                        if(! exchangeRate.equals(0)){
                            bSuccess = configureCurrencySrBean.updateTenderCurrencyExchangeRate(iTenderId, Integer.parseInt(ids[i]), exchangeRate, iUserid);

                            if(bSuccess){
                                remarks="User Id:"+session.getAttribute("userId")+" Update One Currency Exchange with Currency Id:" + ids[i] +  " for Tender id:"+iTenderId;
                                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            }
                            else {
                                auditAction="Error in PE User Update the Currency";
                                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            }
                        }
                        exchangeRate = BigDecimal.ZERO; //reset to check the exchange rate of next currency
                    }
                }
                TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                if(tenderCommonService1.getSBDCurrency(iTenderId).equalsIgnoreCase("Yes")) {
                    response.sendRedirect("officer/Notice.jsp?tenderid="+iTenderId);
                }else {
                    response.sendRedirect("officer/OpenComm.jsp?tenderid="+iTenderId);
                }
            } //Dohatec ICT End

            out.flush();

        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
