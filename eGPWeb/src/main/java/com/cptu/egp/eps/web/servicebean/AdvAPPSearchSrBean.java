/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.table.TblProcurementMethod;
import com.cptu.egp.eps.service.serviceinterface.APPService;
import com.cptu.egp.eps.service.serviceinterface.DesignationMasterService;
import com.cptu.egp.eps.service.serviceinterface.PEOfficeCreationService;
import com.cptu.egp.eps.service.serviceinterface.TenderService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.SelectItem;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class AdvAPPSearchSrBean {

    private final DesignationMasterService designationMasterService = (DesignationMasterService) AppContext.getSpringBean("DesignationMasterService");
    private final PEOfficeCreationService officeCreationService = (PEOfficeCreationService) AppContext.getSpringBean("PEOfficeCreationService");
    private final APPService appService = (APPService) AppContext.getSpringBean("APPService");
    private final TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
    public List<SelectItem> ministryList = new ArrayList<SelectItem>();
    private String defaultFinYear;
    private static final Logger LOGGER = Logger.getLogger(AdvAPPSearchSrBean.class);
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String logUserId = "0";

    /**
     * 
     * @return
     */
    public String getLogUserId() {
        return logUserId;
    }

    /**
     * 
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        designationMasterService.setUserId(logUserId);
        officeCreationService.setLogUserId(logUserId);
        appService.setLogUserId(logUserId);
        tenderService.setUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * 
     * @return
     */
    public String getDefaultFinYear() {
        return defaultFinYear;
    }

    /**
     * 
     * @param defaultFinYear
     */
    public void setDefaultFinYear(String defaultFinYear) {
        this.defaultFinYear = defaultFinYear;
    }

    /**
     * 
     * @return
     */
    public List<SelectItem> getMinistryList() {
        LOGGER.debug("getMinistryList : " + logUserId + loggerStart);
        try {
        if (ministryList.isEmpty()) {
            List<TblDepartmentMaster> departmentMasters = designationMasterService.getMinistry();
            for (TblDepartmentMaster departmentMaster : departmentMasters) {
                ministryList.add(new SelectItem(departmentMaster.getDepartmentId(), departmentMaster.getDepartmentName()));
            }
        }
        } catch (Exception e) {
            LOGGER.error("getMinistryList : " + logUserId,e);
        }
        LOGGER.debug("getMinistryList : " + logUserId + loggerEnd);
        return ministryList;
    }

    public void setMinistryList(List<SelectItem> ministryList) {
        this.ministryList = ministryList;
    }
    private List<SelectItem> officeList = new ArrayList<SelectItem>();

    /**
     * fetch office list/
     * @return
     */
    public List<SelectItem> getOfficeList() {
        LOGGER.debug("getOfficeList : " + logUserId + loggerStart);
        try {
        if (officeList.isEmpty()) {
            List<TblOfficeMaster> officeMasters = officeCreationService.getAllOffices();
            for (TblOfficeMaster officeMaster : officeMasters) {
                officeList.add(new SelectItem(officeMaster.getOfficeId(), officeMaster.getOfficeName()));
            }
        }
        } catch (Exception e) {
            LOGGER.error("getOfficeList : " + logUserId,e);
        }
        LOGGER.debug("getOfficeList : " + logUserId + loggerEnd);

        return officeList;
    }

    /**
     * 
     * @param officeList
     */
    public void setOfficeList(List<SelectItem> officeList) {
        this.officeList = officeList;
    }
    private List<SelectItem> projectList = new ArrayList<SelectItem>();
    private List<SelectItem> financialYearList = new ArrayList<SelectItem>();

    /**
     * Get financial year list
     * @return List of financial year list.
     */
    public List<SelectItem> getFinancialYearList() {
        LOGGER.debug("getFinancialYearList : " + logUserId + loggerStart);
        List<SelectItem> items = null;
        try {
            if (financialYearList.isEmpty()) {
                List<CommonAppData> appDatas = appService.getAPPDetailsBySP("FinancialYear", "", "");
                for (CommonAppData appData : appDatas) {
                    if (appData.getFieldName3().equals("Yes")) {
                        this.defaultFinYear = appData.getFieldName1();
                    }
                    financialYearList.add(new SelectItem(appData.getFieldName1(), appData.getFieldName2()));
                }
            }
            items = financialYearList;
        } catch (Exception ex) {
            LOGGER.error("getFinancialYearList : " + logUserId,ex);
        }
        LOGGER.debug("getFinancialYearList : " + logUserId + loggerEnd);
        return items;
        }

    public void setFinancialYearList(List<SelectItem> financialYearList) {
        this.financialYearList = financialYearList;
    }

    /**
     * Get Project List.
     * @return List of Projects
     */
    public List<SelectItem> getProjectList() {
        LOGGER.debug("getProjectList : " + logUserId + loggerStart);
        List<SelectItem> items = null;
        try {
            if (projectList.isEmpty()) {
                List<CommonAppData> appDatas = appService.getAPPDetailsBySP("Project", "", "");
                for (CommonAppData appData : appDatas) {
                    projectList.add(new SelectItem(appData.getFieldName1(), appData.getFieldName2()));
                }
            }
            items = projectList;
        } catch (Exception ex) {
            LOGGER.error("getProjectList : " + logUserId,ex);
        }
        LOGGER.debug("getProjectList : " + logUserId + loggerEnd);
        return items;
        }

    public void setProjectList(List<SelectItem> projectList) {
        this.projectList = projectList;
    }
    private List<SelectItem> procMethodList = new ArrayList<SelectItem>();

    /**
     * Get Procurement method list
     * @return List of Procurement method.
     */
    public List<SelectItem> getProcMethodList() {
        LOGGER.debug("getProcMethodList : " + logUserId + loggerStart);
        try {
        List<TblProcurementMethod> procMethods = tenderService.getAllProcurementMethod();
        if (procMethods != null && !procMethods.isEmpty()) {
            for (TblProcurementMethod procMethod : procMethods) {
                if("OTM".equalsIgnoreCase(procMethod.getProcurementMethod()) ||
                       "LTM".equalsIgnoreCase(procMethod.getProcurementMethod()) ||            
                       "RFQ".equalsIgnoreCase(procMethod.getProcurementMethod()) ||                    
                       "FC".equalsIgnoreCase(procMethod.getProcurementMethod()) ||
                       "DPM".equalsIgnoreCase(procMethod.getProcurementMethod())  ||             
                        "QCBS".equalsIgnoreCase(procMethod.getProcurementMethod()) ||
                       "LCS".equalsIgnoreCase(procMethod.getProcurementMethod()) ||                     
                       "SBCQ".equalsIgnoreCase(procMethod.getProcurementMethod()) ||
                       "SFB".equalsIgnoreCase(procMethod.getProcurementMethod()) ||                    
                       "SSS".equalsIgnoreCase(procMethod.getProcurementMethod()) ||
                       "IC".equalsIgnoreCase(procMethod.getProcurementMethod())){
                        procMethodList.add(new SelectItem(procMethod.getProcurementMethodId(), procMethod.getProcurementMethod()));
            }
            }
        }
        } catch (Exception e) {
            LOGGER.error("getProcMethodList : " + logUserId,e);
        }
        LOGGER.debug("getProcMethodList : " + logUserId + loggerEnd);
        return procMethodList;
    }

    public void setProcMethodList(List<SelectItem> procMethodList) {
        this.procMethodList = procMethodList;
    }
}
