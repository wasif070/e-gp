/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import java.math.BigDecimal;
import java.util.Comparator;

/**
 *
 * @author Administrator
 */
public class CompareReportT1 implements Comparator<Object[]> {

    @Override
    public int compare(Object[] o1, Object[] o2) {
        int i = 0;
        if (o1[0] != null && o2[0] != null) {
            //i = (int) (Double.parseDouble(o2[0].toString()) - Double.parseDouble());
            i = new BigDecimal(o2[0].toString()).compareTo(new BigDecimal(o1[0].toString()));
        }
        return i;
    }
}
