/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Chalapathi.Bavisetti
 */
public class BudgetTypes {
     private byte budgetTypeId;
     private String budgetType;

    /**
     * @return the budgetTypeId
     */
     @XmlElement(nillable = true)
    public byte getBudgetTypeId() {
        return budgetTypeId;
    }

    /**
     * @param budgetTypeId the budgetTypeId to set
     */
    public void setBudgetTypeId(byte budgetTypeId) {
        this.budgetTypeId = budgetTypeId;
    }

    /**
     * @return the budgetType
     */
    @XmlElement(nillable = true)
    public String getBudgetType() {
        return budgetType;
    }

    /**
     * @param budgetType the budgetType to set
     */
    public void setBudgetType(String budgetType) {
        this.budgetType = budgetType;
    }

}
