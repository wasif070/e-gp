/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SearchMyAppData;
import com.cptu.egp.eps.model.table.TblAppEngEstDoc;
import com.cptu.egp.eps.model.table.TblAppMaster;
import com.cptu.egp.eps.model.table.TblAppPackages;
import com.cptu.egp.eps.service.serviceinterface.APPService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class APPSrBean {

    private final APPService appService = (APPService) AppContext.getSpringBean("APPService");
    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String getSortOrder = "";
    private String sortCol = "";

    private static final Logger LOGGER = Logger.getLogger(APPSrBean.class);
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private String logUserId = "0";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        appService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    public String getSortOrder() {
        return getSortOrder;
    }

    public void setSortOrder(String getSortOrder) {
        this.getSortOrder = getSortOrder;
    }

    public String getSortCol() {
        return sortCol;
    }

    public void setSortCol(String sortCol) {
        this.sortCol = sortCol;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public boolean isSearch() {
        return _search;
    }

    public void setSearch(boolean _search) {
        this._search = _search;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getOp_ENUM() {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM) {
        this.op_ENUM = op_ENUM;
    }
    private List<SearchMyAppData> appList = new ArrayList<SearchMyAppData>();

    /**
     * Get ALl APP Details
     * @param financialYear
     * @param projectName
     * @param budgetType
     * @param status
     * @param userId
     * @param appId
     * @param appCode
     * @return List of APP Search Data
     */
    public List<SearchMyAppData> getAppDetails(String financialYear, String projectName, String activityName, String budgetType, String status, Integer userId, Integer appId, String appCode, Integer createdBy, String sortColumn,String sortType) {
        LOGGER.debug("getAppDetails : " + logUserId + LOGGERSTART);
        try {
            if ("".equals(projectName)) {
                projectName = " ";
            }
            //for (SearchMyAppData searchMyAppData : appService.getAPPSearchDetailsBySP(financialYear, budgetType, projectName, status, getOffset(), getLimit())) {
            for (SearchMyAppData searchMyAppData : appService.getAPPSearchDetailsBySP(financialYear, budgetType, projectName, activityName, status, getOffset(), getLimit(), userId, appId, appCode, createdBy, sortColumn,sortType)) {
                appList.add(searchMyAppData);
            }
        } catch (Exception ex) {
            LOGGER.error("getAppDetails : " + logUserId,ex);
        }
        LOGGER.debug("getAppDetails : " + logUserId + LOGGEREND);
        return appList;
    }

    /**
     * Get ALl APP Eng. Est Docs.
     * @param pckId as Package Id.
     * @return App Eng Est Docs List.
     * @throws Exception
     */
    public List<TblAppEngEstDoc> getAllListing(int pckId) throws Exception {
        return appService.getAllAppEngEstDocDetails(pckId);
    }

    /**
     * Add APP Eng. Estimated Doc.
     * @param docsBrief
     * @param appid
     * @param pckid
     * @param userid
     * @param FileName
     * @param FileSize
     * @return
     */
    public int addAppEngEstDoc(String docsBrief, int appid, int pckid, int userid, String FileName, long FileSize) {//rishita - insert into AppEngEstDoc
        LOGGER.debug("addAppEngEstDoc : " + logUserId + LOGGERSTART);
        TblAppEngEstDoc tblAppEngEstDoc = new TblAppEngEstDoc();
        try {
        tblAppEngEstDoc.setDocumentDesc(docsBrief);
        tblAppEngEstDoc.setDocumentName(FileName);
        tblAppEngEstDoc.setUploadedBy(userid);
        tblAppEngEstDoc.setUploadedDate(new Date());
        tblAppEngEstDoc.setTblAppMaster(new TblAppMaster(appid));
        tblAppEngEstDoc.setTblAppPackages(new TblAppPackages(pckid));
        tblAppEngEstDoc.setDocSize(String.valueOf(FileSize));

        appService.addAppEngEstDoc(tblAppEngEstDoc);
        } catch (Exception e) {
            LOGGER.error("addAppEngEstDoc : " + logUserId,e);
        }
        LOGGER.debug("addAppEngEstDoc : " + logUserId + LOGGEREND);
        return tblAppEngEstDoc.getAppEngEstId();
    }

    /**
     * Get Package Download Details.
     * @param pckid
     * @return
     * @throws Exception
     */
    public List<TblAppEngEstDoc> getDownloadDetails(int pckid) throws Exception {
        LOGGER.debug("getDownloadDetails : " + logUserId + LOGGERSTART);
        List<TblAppEngEstDoc> lst =null;
        try {
            //if (lst.isEmpty()) {
                //for (TblAppEngEstDoc tblAppEngEstDoc :
                    lst = appService.getAppEngEstDocDetails("tblAppPackages", Operation_enum.EQ, new TblAppPackages(pckid));//) {
              //      lst.add(tblAppEngEstDoc);
               // }
            
        } catch (Exception e) {
            LOGGER.error("getDownloadDetails : " + logUserId,e);
            }
        LOGGER.debug("getDownloadDetails : " + logUserId + LOGGEREND);
        return lst;
    }

    /**
     * check if Business Rule config is done for package or not.
     * @param pkgId
     * @return
     */
    public boolean isBusRuleConfig(int pkgId){
        LOGGER.debug("isBusRuleConfig : " + logUserId + LOGGERSTART);
        boolean flag = false;
        try {
            flag = appService.isBusRuleConfig(pkgId);
        } catch (Exception e) {
            LOGGER.error("isBusRuleConfig : " + logUserId,e);
        }
        LOGGER.debug("isBusRuleConfig : " + logUserId + LOGGEREND);
        return flag;
    }

    /**
     * check if proc method rule is configures or not.
     * @param pkgId
     * @return
     */
    public boolean isprocMethodRuleConfig(int pkgId){
        LOGGER.debug("isprocMethodRuleConfig : " + logUserId + LOGGERSTART);
        boolean flag = false;
        try {
            flag = appService.isprocMethodRuleConfig(pkgId);
        } catch (Exception e) {
            LOGGER.error("isprocMethodRuleConfig : " + logUserId,e);
        }
        LOGGER.debug("isprocMethodRuleConfig : " + logUserId + LOGGEREND);
        return flag;
    }
    
   
  
    public String[] getMinMax(String pkgNo, String dptType){
        LOGGER.debug("getMinMax : " + logUserId + LOGGERSTART);
        String[] flag = new String[7];
        try {
            flag = appService.getMinMax(pkgNo,dptType);
        } catch (Exception e) {
            LOGGER.error("getMinMax : " + logUserId,e);
        }
        LOGGER.debug("getMinMax : " + logUserId + LOGGEREND);
        return flag;
    }
    
    
    
    
    
    public boolean isHopaExist(){
        LOGGER.debug("isHopaExist : " + logUserId + LOGGERSTART);
        boolean flag = false;
        try {
            flag = appService.isHopaExist(logUserId);
        } catch (Exception e) {
            LOGGER.error("isHopaExist : " + logUserId,e);
        }
        LOGGER.debug("isHopaExist : " + logUserId + LOGGEREND);
        return flag; 
    }
    
    /**
     * Get CPV category list
     * @param appId
     * @return list of CPV Category.
     */
    public List<TblAppPackages> getCPVCodeByappId(int appId) {
        return appService.getCPVCodeByappId(appId);
    }
    
    /**
     * Get Work flow History link by condition.
     * @param appId
     * @return
     */
    public List<Object[]> getWorkflowHistorylink(int appId) {
        LOGGER.debug("getWorkflowHistorylink : " + logUserId + LOGGERSTART);
        List<Object[]> list = new ArrayList<Object[]>();
        
        try{
             list = appService.getWorkflowHistorylink(appId);   
        }catch(Exception ex){
             LOGGER.error("getWorkflowHistorylink : " + logUserId + ex);
        }
        
        LOGGER.debug("getWorkflowHistorylink : " + logUserId + LOGGEREND);
        return list;
    }

    /**
     * check if file on hand or not.
     * @param ObjectId
     * @param childId
     * @param userId
     * @param eventId
     * @return
     */
    public List<Object> getFileOnHandStatus(int ObjectId,int childId,int userId,int eventId) {
        LOGGER.debug("getFileOnHandStatus : " + logUserId + LOGGERSTART);
        List<Object> list = new ArrayList<Object>();

        try{
             list = appService.getFileOnHandStatus(ObjectId,childId,userId,eventId);
        }catch(Exception ex){
             LOGGER.error("getFileOnHandStatus : " + logUserId + ex);
        }

        LOGGER.debug("getFileOnHandStatus : " + logUserId + LOGGEREND);
        return list;
    }
}
