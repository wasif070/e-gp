/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class TimeExtTenderServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if (request.getParameter("action").equals("fetchData")) {
                response.setContentType("text/xml;charset=UTF-8");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                int totalPages = 0;
                int totalCount = 0;
                String status = "";
                if(request.getParameter("status") != null){
                    status = request.getParameter("status");
                }
                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                List<SPCommonSearchData> getApprovalUsers = commonSearchService.searchData("TenderOpenExt", request.getSession().getAttribute("userId").toString(),rows, page, status, null, null, null, null, null);
                if(!getApprovalUsers.isEmpty()){
                    totalCount = Integer.parseInt(getApprovalUsers.get(0).getFieldName6());
                }
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                    } else {
                        totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                    }

                } else {
                    totalPages = 0;
                }
                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getApprovalUsers.size() + "</records>");
                int srNo = 1;
                int rowId = 0;
                for (SPCommonSearchData details : getApprovalUsers) {
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell><![CDATA[" + details.getFieldName8() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName1() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName2() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName3() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName4() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName5() + "]]></cell>");
                    if(status.equalsIgnoreCase("pending")){
                        out.print("<cell><![CDATA[<a href='AppRejTOExtReq.jsp?tenderId="+details.getFieldName1()+"&extId=" + details.getFieldName7()+"'>Process</a>]]></cell>");
                    }else{
                        out.print("<cell><![CDATA[<a href='ViewAppRejTOExtReq.jsp?tenderId="+details.getFieldName1()+"&extId=" + details.getFieldName7()+"'>View</a>]]></cell>");
                    }
                    out.print("</row>");
                    srNo++;
                    rowId++;
                }
                out.print("</rows>");
            }
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
