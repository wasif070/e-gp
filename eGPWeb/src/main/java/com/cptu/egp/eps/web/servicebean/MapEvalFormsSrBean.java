/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Karan
 */
public class MapEvalFormsSrBean {

    final Logger logger = Logger.getLogger(MapEvalFormsSrBean.class);
    private String logUserId ="0";
    
    private TenderCommonService tenderCommonServiceFrm = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    private CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");

    public void setLogUserId(String logUserId) {
        tenderCommonServiceFrm.setLogUserId(logUserId);
        commonXMLSPService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }
    
    public List<SPTenderCommonData> mnethod(String tenderId) {
        logger.debug("mnethod : "+logUserId+" Starts");
        List<SPTenderCommonData> list = null;
        
        try{
            list = tenderCommonServiceFrm.returndata("gettenderlotbytenderid", tenderId, null);
        }catch(Exception ex){
            logger.error("mnethod : "+logUserId+" : "+ex.toString());
    }
        logger.debug("mnethod : "+logUserId+" Ends");
        return list;
    }

    public List<SPTenderCommonData> getLotOrPackageData(String tenderId, String strCondition) {
        logger.debug("getLotOrPackageData : "+logUserId+" Starts");
        List<SPTenderCommonData> list = null;
        try{
            list = tenderCommonServiceFrm.returndata("getlotorpackagebytenderid", tenderId, strCondition);
        }catch(Exception ex){
            logger.error("getLotOrPackageData : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getLotOrPackageData : "+logUserId+" Ends");
        return list;
    }

    public List<SPTenderCommonData> getLotOrPackageForms(String tenderId, String pkgLotId) {
        logger.debug("getLotOrPackageForms : "+logUserId+" Starts");
        List<SPTenderCommonData> list = null;
        try{
            list = tenderCommonServiceFrm.returndata("getFormsForEvlauationMapping", tenderId, pkgLotId);
        }catch(Exception ex){
             logger.error("getLotOrPackageForms : "+logUserId+" : "+ex.toString());
    }
        logger.debug("getLotOrPackageForms : "+logUserId+" Ends");
        return list;
    }

    public boolean mapEvaluationForms(String strXML, String thisTenderId, String pkgLotId) throws Exception {
        logger.debug("mapEvaluationForms : "+logUserId+" Starts");
        CommonMsgChk commonMsgChk = new CommonMsgChk();
        boolean flag = false;
        String delCondition = null;
        try{
        if (!"".equalsIgnoreCase(strXML)) {
            strXML = "<root>" + strXML + "</root>";

                if (!"0".equalsIgnoreCase(pkgLotId)) {
                    delCondition = "tenderId=" + thisTenderId + " And pkgLotId=" + pkgLotId;
                } else {
                    delCondition = "tenderId=" + thisTenderId;
                    }

            commonMsgChk = commonXMLSPService.insertDataBySP("insdel", "tbl_EvalMapForms", strXML, delCondition).get(0);
                flag = commonMsgChk.getFlag();
            delCondition=null;
        }
        }catch(Exception ex){
             logger.error("mapEvaluationForms : "+logUserId+" : "+ex.toString());
    }
        logger.debug("mapEvaluationForms : "+logUserId+" Ends");
        return flag;
    }
}
