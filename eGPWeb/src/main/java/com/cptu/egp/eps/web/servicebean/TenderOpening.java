/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.model.table.TblTenderHash;
import com.cptu.egp.eps.service.serviceimpl.TenderOpeningServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Iterator;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import org.apache.log4j.Logger;
import sun.misc.BASE64Decoder;

/**
 *
 * @author test
 */
public class TenderOpening {

    private String logUserId = "0";
    final Logger logger = Logger.getLogger(TenderOpening.class);
    private String password;
    private String encrypt;
    TenderOpeningServiceImpl tenderOpeningService =
            (TenderOpeningServiceImpl) AppContext.getSpringBean("TenderOpeningService");

    public void setLogUserId(String logUserId) {
        tenderOpeningService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * Decrypt Data of Tender Form
     * @param funName
     * @param tid
     * @param fid
     * @param field4
     * @param field5
     * @param field6
     * @param field7
     * @param field8
     * @param field9
     * @param field10
     * @return true or false for success or fail
     */
    public boolean getTenderEncriptionData(String funName, String tid, String fid, String field4,
            String field5, String field6, String field7, String field8, String field9, String field10) {
        logger.debug("getTenderEncriptionData : " + logUserId + "starts");
        List<SPCommonSearchData> spcommonSearchData = null;
        String hashKey = "";
        String repVal = "";

        boolean responseMsg = false;
        try {
            spcommonSearchData = tenderOpeningService.getTenderEncriptionData(funName, tid, fid, field4, field5, field6, field7, field8, field9, field10);
            Iterator it = spcommonSearchData.iterator();
            List<TblTenderHash> tenderHash = getTenderHash(Integer.parseInt(tid));
            if (tenderHash.size() > 0) {
                TblTenderHash tblTenderHash = tenderHash.get(0);
                hashKey = tblTenderHash.getTenderHash();
            }
            logger.debug(" hash key " + hashKey);
            StringBuilder tenderEnc = new StringBuilder();
            if (hashKey != null && !("".equals(hashKey))) {
                tenderEnc.append("<root>");
                while (it.hasNext()) {
                    SPCommonSearchData searchdata = (SPCommonSearchData) it.next();
                    tenderEnc.append("<tbl_TenderBidPlainData ");
                    tenderEnc.append(" bidTableId=\"" + searchdata.getFieldName1() + "\" ");
                    tenderEnc.append(" tenderColId=\"" + searchdata.getFieldName2() + "\" ");
                    tenderEnc.append(" tenderTableId=\"" + searchdata.getFieldName3() + "\" ");
                    this.setEncrypt(searchdata.getFieldName4());
                    if (searchdata.getFieldName4() == null || "null".equals(searchdata.getFieldName4())) {
                        tenderEnc.append(" cellValue=\"null\" ");
                    } else {
                        repVal = this.getSymDecrypt(hashKey.replaceAll(" ", ""));
                        tenderEnc.append(" cellValue=\"" + HandleSpecialChar.handleSpecialChar(repVal.replaceAll("\r\n", "<br />")) + "\" ");
                    }
                    tenderEnc.append(" rowId=\"" + searchdata.getFieldName5() + "\" ");
                    tenderEnc.append(" tenderCelId=\"" + searchdata.getFieldName6() + "\" ");
                    tenderEnc.append(" tenderFormId=\"" + fid + "\" />");
                }
                tenderEnc.append("</root>");
            }
            if (tenderEnc.length() > 0) {
                long count = getTenderBidPlainDataByFormId("TblTenderBidPlainData", "tenderFormId=" + fid);
                if (count == 0) {
                    List<CommonMsgChk> commonmsgChk = insertTenderBidPlainData("Insert", "tbl_TenderBidPlainData", tenderEnc.toString(), "");
                    if (commonmsgChk.size() > 0) {
                        CommonMsgChk cmgc = commonmsgChk.get(0);
                        if (cmgc.getFlag() == true) {
                            responseMsg = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("getTenderEncriptionData : " + e);
        }



        logger.debug("getTenderEncriptionData : " + logUserId + "ends");
        return responseMsg;
    }

    /**
     *Get List of TenderHash for Tender
     * @param tenderId from tbl_TenderMaster
     * @return List of TblTenderHash
     */
    public List<TblTenderHash> getTenderHash(int tenderId) {
        logger.debug("getTenderHash : " + logUserId + "starts");
        List<TblTenderHash> tenderHash = null;
        try {
            tenderHash = tenderOpeningService.getTenderHash(tenderId);
        } catch (Exception e) {
            logger.error("getTenderHash : " + e);
        }



        logger.debug("getTenderHash : " + logUserId + "ends");
        return tenderHash;
    }

    /**
     *Inserting Bidding plain data
     * @param action
     * @param tableName
     * @param xmlData
     * @param conditions
     * @return List of CommonMsgChk
     */
    public List<CommonMsgChk> insertTenderBidPlainData(String action, String tableName, String xmlData, String conditions) {
        logger.debug("insertTenderBidPlainData : " + logUserId + "starts");
        List<CommonMsgChk> commonmsgChk = null;
        try {
            commonmsgChk = tenderOpeningService.insertTenderBidPlainData(action, tableName, xmlData, conditions);
        } catch (Exception e) {
            logger.error("insertTenderBidPlainData : " + e);
        }



        logger.debug("insertTenderBidPlainData : " + logUserId + "ends");
        return commonmsgChk;
    }

    /**
     *Decrypts data of the Tender Form
     * @param password2 by which decryption is done
     * @return decrypted data
     */
    public String getSymDecrypt(String password2) {
        logger.debug("getSymDecrypt : " + logUserId + "starts");
        this.setPassword(password2);
        String decrypted = null;
        try {
            String salt = getEncrypt().substring(0, 12);
            String ciphertext = getEncrypt().substring(12, getEncrypt().length());
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] saltArray = decoder.decodeBuffer(salt);
            byte[] ciphertextArray = decoder.decodeBuffer(ciphertext);
            PBEKeySpec keySpec = new PBEKeySpec(getPassword().toCharArray());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("pbewithmd5anddes");
            SecretKey key = keyFactory.generateSecret(keySpec);
            PBEParameterSpec paramSpec = new PBEParameterSpec(saltArray, 1000);
            Cipher cipher = Cipher.getInstance("pbewithmd5anddes");
            cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            byte[] plaintextArray = cipher.doFinal(ciphertextArray);
            decrypted = new String(plaintextArray);
        } catch (IllegalBlockSizeException ex) {
            logger.error("getSymDecrypt : " + ex);
        } catch (BadPaddingException ex) {
            logger.error("getSymDecrypt : " + ex);
        } catch (InvalidKeyException ex) {
            logger.error("getSymDecrypt : " + ex);
        } catch (InvalidAlgorithmParameterException ex) {
            logger.error("getSymDecrypt : " + ex);
        } catch (NoSuchPaddingException ex) {
            logger.error("getSymDecrypt : " + ex);
        } catch (InvalidKeySpecException ex) {
            logger.error("getSymDecrypt : " + ex);
        } catch (NoSuchAlgorithmException ex) {
            logger.error("getSymDecrypt : " + ex);
        } catch (IOException ex) {
            logger.error("getSymDecrypt : " + ex);
        }
        logger.debug("getSymDecrypt : " + logUserId + "ends");
        return decrypted;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the encrypt
     */
    public String getEncrypt() {
        return encrypt;
    }

    /**
     * @param encrypt the encrypt to set
     */
    public void setEncrypt(String encrypt) {
        this.encrypt = encrypt;
    }

    /**
     *Get Bidder Plain Data count
     * @param tabName
     * @param condition
     * @return count of Plain Data
     */
    public long getTenderBidPlainDataByFormId(String tabName, String condition) {
        logger.debug("getTenderBidPlainDataByFormId : " + logUserId + "starts");
        long cont = 0;
        try {
            cont = tenderOpeningService.getTenderBidPlainDataByFormId(tabName, condition);
        } catch (Exception e) {
            logger.error("getTenderBidPlainDataByFormId : " + e);
        }



        logger.debug("getTenderBidPlainDataByFormId : " + logUserId + "ends");
        return cont;
    }

    /**
     *Get Forms for the Bidder of the Tender
     * @param tenderId  from tbl_TenderMaster
     * @param stat
     * @param pkgLotId  from tbl_TenderLotSecurity
     * @return array of Forms
     */
    public Object[] getBidFormIds(int tenderId, String stat, String pkgLotId) {
        logger.debug("getBidFormIds : " + logUserId + "starts");
        Object[] Result = null;
        try {
            Result = tenderOpeningService.getBidFormIds(tenderId, stat, pkgLotId).toArray();
        } catch (Exception e) {
            logger.error("getBidFormIds :  " + e.toString());

        }
        logger.debug("getBidFormIds : " + logUserId + "ends");
        return Result;
    }

    /**
     *Count of TblEvalBidderStatus
     * @param tenderId from tbl_TenderMaster
     * @return count of bidder status
     * @throws Exception
     */
    public long getBidderStatusCount(String tenderId) throws Exception {
        logger.debug("getBidderStatusCount : " + logUserId + "starts");
        long getBidderStatusCount = 0;
        try {
            getBidderStatusCount = tenderOpeningService.getBidderStatusCount(tenderId);
        } catch (Exception e) {
            logger.error("getBidderStatusCount : " + e);
        }

        logger.debug("getBidderStatusCount : " + logUserId + "ends");
        return getBidderStatusCount;
    }

    /**
     *Count of BidPlainData
     * @param formId for the formId
     * @return count of BidPlainData
     * @throws Exception
     */
    public long getBidPlainDataCount(String formId) throws Exception {
        logger.debug("getBidPlainDataCount : " + logUserId + "starts");
        long getBidPlainDataCount = 0;
        try {
            getBidPlainDataCount = tenderOpeningService.getBidPlainDataCount(formId);
        } catch (Exception e) {
            logger.error("getBidPlainDataCount : " + e);
        }

        logger.debug("getBidPlainDataCount : " + logUserId + "ends");
        return getBidPlainDataCount;
    }

    /**
     *Get Eval and Open committe common member count
     * @param tenderId from tbl_TenderMaster
     * @param userId
     * @return count of common member
     * @throws Exception
     */
    public long getCommonMemberCount(String tenderId, String userId) throws Exception {
        logger.debug("getCommonMemberCount : " + logUserId + "starts");
        long getCommonMemberCount = 0;
        try {
            getCommonMemberCount = tenderOpeningService.getCommonMemberCount(tenderId, userId);
        } catch (Exception e) {
            logger.error("getCommonMemberCount : " + e);
        }

        logger.debug("getCommonMemberCount : " + logUserId + "ends");
        return getCommonMemberCount;
    }

    public int checkCellValue(List<String> cellValues,String tenderId){
        int count=0;
        String hashKey=null;
        List<TblTenderHash> tenderHash = getTenderHash(Integer.parseInt(tenderId));
        if (tenderHash.size() > 0) {
            TblTenderHash tblTenderHash = tenderHash.get(0);
            hashKey = tblTenderHash.getTenderHash();
        }
        for (String cell : cellValues) {
            if (cell != null && !"null".equals(cell)) {
                this.setEncrypt(cell);
                String val = this.getSymDecrypt(hashKey.replaceAll(" ", ""));
                if(val==null || "".equals(val) || "null".equalsIgnoreCase(val)){
                    count++;
                }
            }
        }
        return count;
    }
}
