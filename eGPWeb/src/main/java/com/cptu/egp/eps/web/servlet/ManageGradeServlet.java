/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblGradeMaster;
import com.cptu.egp.eps.web.servicebean.ManageGradeSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ManageGradeServlet extends HttpServlet {
   static final Logger LOGGER = Logger.getLogger(ManageGradeServlet.class);
   private String logUserId = "";
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
        }
        try {
              if (request.getParameter("action").equals("fetchData")) {
                response.setContentType("text/xml;charset=UTF-8");
                LOGGER.debug(">" + request.getQueryString());
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                String type=request.getParameter("userType");
//                if (sidx.equals("")) {
//                    sidx = "gradeId";
//                }
                String userTypeId="";
                
                //dcBean.setColName("");
                ManageGradeSrBean manageGradeSrBean=new ManageGradeSrBean();
                manageGradeSrBean.setLogUserId(logUserId);
                manageGradeSrBean.setSearch(_search);
                manageGradeSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                LOGGER.debug("offset:" + +offset);

                manageGradeSrBean.setOffset(offset);
                manageGradeSrBean.setSortOrder(sord);
                manageGradeSrBean.setSortCol(sidx);


                LOGGER.debug("queryString:" + request.getQueryString());

                List<TblGradeMaster> gradeMasters = manageGradeSrBean.getGradeList();
                LOGGER.debug("GradeMasterList size:" + gradeMasters.size());
                int totalPages = 0;
                long totalCount = manageGradeSrBean.getAllCountOfGrades();
                LOGGER.debug("totalCount : "+totalCount);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(request.getParameter("rows")));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(request.getParameter("rows"))) + 1);
                    }
                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");
                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + totalCount + "</records>");
                
                int j =0;
                int no = Integer.parseInt(request.getParameter("page"));
                if(no == 1){
                    j = 1;
                }else{
                    if(Integer.parseInt(rows) == 30){
                        j = ((no-1)*30)+1;
                    }else if(Integer.parseInt(rows) == 20){
                        j = ((no-1)*20)+1;
                    }
                    else{
                        j = ((no-1)*10)+1;
                    }
                }
                // be sure to put text data in CDATA
                for (int i = 0; i < gradeMasters.size(); i++) {
                    out.print("<row id='" + gradeMasters.get(i).getGradeId() + "'>");
                    out.print("<cell>" + j + "</cell>");
                    out.print("<cell><![CDATA[" + gradeMasters.get(i).getGrade() + "]]></cell>");
                    out.print("<cell>" + gradeMasters.get(i).getGradeLevel() + "</cell>");
                    String editLink="<a href=\"EditGrade.jsp?gradeId="+ gradeMasters.get(i).getGradeId()+"\">Edit</a>";
                    String viewLink="<a href=\"ViewGrade.jsp?gradeId="+ gradeMasters.get(i).getGradeId()+"&action=view\">View</a>";
                    out.print("<cell><![CDATA[" + editLink+ " | "+viewLink+"]]></cell>");
                    out.print("</row>");
                    LOGGER.debug("</row>");
                j++;
                }
                out.print("</rows>");
                LOGGER.debug("</rows>");
            }
               LOGGER.debug("processRequest "+logUserId+" Ends:");
        } catch (Exception ex) {
            LOGGER.error("processRequest "+logUserId+":" + ex);
        } finally {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
