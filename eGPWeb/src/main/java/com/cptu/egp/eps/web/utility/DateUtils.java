/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class DateUtils {

    private static final Logger LOGGER = Logger.getLogger(DateUtils.class);
    
    public static String SqlStringDatetoApplicationStringDate(String Date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd yyyy hh:mmaaa");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date d = new Date();
        String ApplicationdateString = "";
        try {
            d = dateFormat.parse(Date);
            ApplicationdateString = dateFormat2.format(d);
        } catch (Exception ex) {
            LOGGER.error("Error in Date conversion : " + ex);
        }
        return ApplicationdateString;
    }
    
    public static String SqlStringDatetoApplicationStringDateWithoutTime(String Date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd yyyy hh:mmaaa");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
        Date d = new Date();
        String ApplicationdateString = "";
        try {
            d = dateFormat.parse(Date);
            ApplicationdateString = dateFormat2.format(d);
        } catch (Exception ex) {
            LOGGER.error("Error in Date conversion : " + ex);
        }
        return ApplicationdateString;
    }

    public static Date convertDateToStr(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date d = new Date();
        try {
            d = dateFormat.parse(date);
        } catch (Exception ex) {
            LOGGER.error("Error in Date conversion : " + ex);
        }
        return d;
    }

    public static String convertStrToDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return dateFormat.format(date);
    }

    public static String convertStrToDatenew(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(date);
    }
    
    public static String formatStdDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            String dt = dateFormat.format(date);
            return dt;
        } catch (Exception ex) {
            LOGGER.error("Date Exception Generated: " + ex.getMessage());
        }
        return  date.toString();
    }

    public static Date formatStdString(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dt = null;
        try {
            dt = dateFormat.parse(date);

        } catch (Exception ex) {
            LOGGER.error("Date Exception Generated: " + ex.getMessage());
        }
        return  dt;
    }

    public static String formatDate(Date date) {
        if (date == null) {
            return "";
        }
        String strDate = String.valueOf(date);
        String[] splitSpace =  strDate.split(" ");
        String[] splitDash = splitSpace[0].split("-");
        String[] splitColun = splitSpace[1].split(":");

        return splitDash[2] + "/" + splitDash[1] + "/" + splitDash[0] + " " + splitColun[0] + ":" + splitColun[1];
    }

     /**
      * it takes String like dd/mm/yyyy and
      * gives output string is like yyyy-mm-dd
      * @param date
      * @return
      */
    public static String formatStrToStr(String date) {
       String[] indate = date.split("/");
        String retdate = indate[2] + "-" + indate[1] + "-" + indate[0];
       return retdate;
    }

    public static Date gridStrToDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        Date d = new Date();
        try {
            d = dateFormat.parse(date);
        } catch (Exception ex) {
            LOGGER.error("Error in Date conversion : " + ex);
        }
        return d;
    }
    public static Date convertStringtoDate(String date,String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date d = new Date();
        try {
            d = dateFormat.parse(date);
        } catch (Exception ex) {
            LOGGER.error("Error in Date conversion : " + ex);
        }
        return d;
    }
    
    public static String convertDatetoString(Date date, String formate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(formate);
        try {
            String dt = dateFormat.format(date);
            return dt;
        } catch (Exception ex) {
            LOGGER.error("Date Exception Generated: " + ex.getMessage());
        }
        return  date.toString();
    }

    public static Date gridStrToDateWithoutSec(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        Date d = new Date();
        try {
            d = dateFormat.parse(date);
        } catch (Exception ex) {
            LOGGER.error("Error in Date conversion : " + ex);
        }
        return d;
    }

    public static String gridDateToStr(Date date) {
        String strDate="-";
        if(date!=null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
            strDate = dateFormat.format(date);
        }
        return strDate;
    }

    public static String gridDateToStrWithoutSec(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        return dateFormat.format(date);
    }

    public static String customDateFormate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        return dateFormat.format(date);
    }

    public static Date StrtoDate(String dt){
         DateFormat formatter ;
         Date date = null ;
         formatter = new SimpleDateFormat("dd-MMM-yy");
         try {
            date = (Date) formatter.parse(dt);
         } catch (ParseException ex) {
             System.out.println("Exception found : "+ex);
         }
         return date;
    }
    public static long calculateDays(Date dateEarly, Date dateLater) {
        return (dateLater.getTime() - dateEarly.getTime()) / (24 * 60 * 60 * 1000);
    }
}
