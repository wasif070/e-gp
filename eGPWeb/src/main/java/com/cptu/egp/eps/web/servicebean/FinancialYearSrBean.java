/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblFinancialYear;
import com.cptu.egp.eps.service.serviceinterface.FinancialYearService;
import com.cptu.egp.eps.web.databean.FinancialYearDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Naresh.Akurathi
 */
public class FinancialYearSrBean {

    private final FinancialYearService financialYearService=(FinancialYearService)AppContext.getSpringBean("financialYearService");

    private static final Logger LOGGER = Logger.getLogger(FinancialYearSrBean.class);

    private String logUserId = "0";

    private AuditTrail auditTrail;

    public FinancialYearSrBean() {
        financialYearService.setLogUserId(logUserId);
        financialYearService.setAuditTrail(auditTrail);
    }

    /**
     *
     * Method for Setting userId in LOGGER.
     * @param logUserId
     * @return logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /**
     *
     * Method for adding the FinancialYear details.
     * @param financialDtBean
     * @return
     */
    public String addFinancialYearDetails(FinancialYearDtBean financialDtBean) {
        LOGGER.debug("addFinancialYearDetails "+logUserId+" Starts");
        String val = "";
        TblFinancialYear tblFinancialYear=new TblFinancialYear();
        try{
            tblFinancialYear.setFinancialYear(financialDtBean.getFinancialYear());
            tblFinancialYear.setIsCurrent(financialDtBean.getIsCurrent());
            String str=financialDtBean.getIsCurrent();
            financialYearService.setAuditTrail(auditTrail);
            financialYearService.setLogUserId(logUserId);
            val =  financialYearService.addFinancialYearService(str, tblFinancialYear); //calling Sevice Layer method for adding the record.
        }catch(Exception e){
            LOGGER.debug("addFinancialYearDetails "+logUserId+" :"+e);
        }
        LOGGER.debug("addFinancialYearDetails "+logUserId+" Ends");
        return  val;
        
    }

    /**
     *
     * Method to get all FinancialYear Details.
     * @return List<TblFinancialYear>
     */
    public List<TblFinancialYear> getFinancialYearDetails(){
        LOGGER.debug("getFinancialYearDetails "+logUserId+" Starts");
        List<TblFinancialYear> list = new ArrayList<TblFinancialYear>();
        try {
                list = financialYearService.getFinancialYearDetails(); //calling the Service mehod the get the details.
        } catch (Exception e) {
            LOGGER.error("getFinancialYearDetails "+logUserId+" :"+e);
    }
            LOGGER.debug("getFinancialYearDetails "+logUserId+" Ends");
            return list;
    }

    /**
     * Get financial year data as per financialYearId
     * @param id
     * @return List<TblFinancialYear>
     */
    public List<TblFinancialYear> getFinancialYearData(int id){
        LOGGER.debug("getFinancialYearData "+logUserId+" Starts");
         List<TblFinancialYear> list = new ArrayList<TblFinancialYear>();
         try {
            list = financialYearService.getFinancialYearData(id);
        } catch (Exception e) {
            LOGGER.error("getFinancialYearData "+logUserId+" :"+e);
    }
         LOGGER.debug("getFinancialYearData "+logUserId+" Ends");
            return list;
    }

    /**
     *Update Tabel finalcialYearData
     * @param financialDtBean
     * @param id FinancialYearId
     * @return if sucsess then value updateded
     */
    public String updateFinancialYear(FinancialYearDtBean financialDtBean, int id){
        LOGGER.debug("updateFinancialYear "+logUserId+" Starts");
        String val = "";
        try {
         TblFinancialYear tblFinancialYear=new TblFinancialYear();
         tblFinancialYear.setFinancialYearId(id);
         tblFinancialYear.setFinancialYear(financialDtBean.getFinancialYear());
         tblFinancialYear.setIsCurrent(financialDtBean.getIsCurrent());
         financialYearService.setLogUserId(logUserId);
         financialYearService.setAuditTrail(auditTrail);
                 val = financialYearService.updateFinancialYear(tblFinancialYear);
        } catch (Exception e) {
            LOGGER.error("updateFinancialYear "+logUserId+" :"+e);
    }
        LOGGER.debug("updateFinancialYear "+logUserId+" Ends");
        return val;
         
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    
}
