/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCmsContractTermination;
import com.cptu.egp.eps.service.serviceinterface.CmsContractTerminationService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsContractTerminationBean {

    final Logger logger = Logger.getLogger(CmsAitConfigBean.class);
    private final static CmsContractTerminationService cmsContractTerminationService =
            (CmsContractTerminationService) AppContext.getSpringBean("CmsContractTerminationService");
    private String logUserId = "0";
    private static final String STARTS = " Starts";
    private static final String ENDS = " Ends";
    private static final String SPACE = " : ";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        cmsContractTerminationService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /***
     * This method inserts a TblCmsContractTermination object in database
     * @param tblCmsContractTermination
     * @return int
     */
    public int insertCmsContractTermination(TblCmsContractTermination tblCmsContractTermination) {
        logger.debug("insertCmsContractTermination : " + logUserId + STARTS);
        int id = 0;
        try {
            id = cmsContractTerminationService.insertCmsContractTermination(tblCmsContractTermination);
        } catch (Exception ex) {
            logger.error("insertCmsContractTermination : " + logUserId + SPACE + ex);
        }
        logger.debug("insertCmsContractTermination : " + logUserId + ENDS);
        return id;
    }

    /****
     * This method updates an TblCmsContractTermination object in DB
     * @param tblCmsContractTermination
     * @return boolean
     */
    public boolean updateCmsContractTermination(TblCmsContractTermination tblCmsContractTermination) {
        logger.debug("updateCmsContractTermination : " + logUserId + STARTS);
        boolean flag = false;
        try {
            flag = cmsContractTerminationService.updateCmsContractTermination(tblCmsContractTermination);
        } catch (Exception ex) {
            logger.error("updateCmsContractTermination : " + logUserId + SPACE + ex);
        }
        logger.debug("updateCmsContractTermination : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method deletes an TblCmsContractTermination object in DB
     * @param tblCmsContractTermination
     * @return boolean
     */
    public boolean deleteCmsContractTermination(TblCmsContractTermination tblCmsContractTermination) {
        logger.debug("deleteCmsContractTermination : " + logUserId + STARTS);
        boolean flag = false;
        try {
            flag = cmsContractTerminationService.deleteCmsContractTermination(tblCmsContractTermination);
        } catch (Exception ex) {
            logger.error("deleteCmsContractTermination : " + logUserId + SPACE + ex);
            flag = false;
        }
        logger.debug("deleteCmsContractTermination : " + logUserId + ENDS);
        return flag;
    }

    /****
     * This method returns all TblCmsContractTermination objects from database
     * @return List<TblCmsContractTermination>
     */
    public List<TblCmsContractTermination> getAllCmsContractTermination() {
        logger.debug("getAllCmsContractTermination : " + logUserId + STARTS);
        List<TblCmsContractTermination> cmsContractTerminationList = new ArrayList<TblCmsContractTermination>();
        try {
            cmsContractTerminationList = cmsContractTerminationService.getAllCmsContractTermination();
        } catch (Exception ex) {
            logger.error("getAllCmsContractTermination : " + logUserId + SPACE + ex);
        }
        logger.debug("getAllCmsContractTermination : " + logUserId + ENDS);
        return cmsContractTerminationList;
    }

    /***
     *  This method returns no. of TblCmsContractTermination objects from database
     * @return long
     */
    public long getCmsContractTerminationCount() {
        logger.debug("getCmsContractTerminationCount : " + logUserId + STARTS);
        long cmsContractTerminationCount = 0;
        try {
            cmsContractTerminationCount = cmsContractTerminationService.getCmsContractTerminationCount();
        } catch (Exception ex) {
            logger.error("getCmsContractTerminationCount : " + logUserId + SPACE + ex);
        }
        logger.debug("getCmsContractTerminationCount : " + logUserId + ENDS);
        return cmsContractTerminationCount;
    }

    /***
     * This method returns TblCmsContractTermination for the given Id
     * @param int id
     * @return TblCmsContractTermination
     */
    public TblCmsContractTermination getCmsContractTermination(int contractTerminationId) {
        logger.debug("getCmsContractTermination : " + logUserId + STARTS);
        TblCmsContractTermination tblCmsContractTermination = null;
        try {
            tblCmsContractTermination = cmsContractTerminationService.getCmsContractTermination(contractTerminationId);
        } catch (Exception ex) {
            logger.error("getCmsContractTermination : " + logUserId + SPACE + ex);
        }
        logger.debug("getCmsContractTermination : " + logUserId + ENDS);
        return tblCmsContractTermination;
    }

    /***
     * This method returns TblCmsContractTermination for the given contractSignId
     * @param contractSignId
     * @return TblCmsContractTermination
     */
    public TblCmsContractTermination getCmsContractTerminationForContractSignId(int contractSignId) {
        logger.debug("getCmsContractTerminationForContractSignId : " + logUserId + STARTS);
        TblCmsContractTermination tblCmsContractTermination = null;
        try {
            tblCmsContractTermination = cmsContractTerminationService.getCmsContractTerminationForContractSignId(contractSignId);
        } catch (Exception ex) {
            logger.error("getCmsContractTerminationForContractSignId : " + logUserId + SPACE + ex);
        }
        logger.debug("getCmsContractTerminationForContractSignId : " + logUserId + ENDS);
        return tblCmsContractTermination;
    }

    /***
     *  This method changes the status of TblCmsContractTermination object for the given contractSignId
     * @param contractSignId
     * @param columnType either <b> status </b> or <b>work flow status</b>
     * @param status
     * @return TblCmsContractTermination
     */
    public TblCmsContractTermination changeCTStatus(int contractSignId, String columnType, String status) {
        logger.debug("changeCTStatus : " + logUserId + STARTS);
        TblCmsContractTermination tblCmsContractTermination = null;
        try {
            tblCmsContractTermination = cmsContractTerminationService.changeCTStatus(contractSignId, columnType, status);
        } catch (Exception ex) {
            logger.error("changeCTStatus : " + logUserId + SPACE + ex);
        }
        logger.debug("changeCTStatus : " + logUserId + ENDS);
        return tblCmsContractTermination;
    }
}
