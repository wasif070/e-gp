/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Administrator
 */
public class AppContext {

    private AppContext() {
    }

    static {
         try {
            context =  new ClassPathXmlApplicationContext("ApplicationContext.xml");
        } catch (Throwable ex) {
            System.err.println("Initial BeanFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        
    }
    
    public static Object getSpringBean(String beanName) {        
        return context.getBean(beanName);
    }
    private static final ApplicationContext context;
}
