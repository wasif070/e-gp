/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;
import com.cptu.egp.eps.dao.storedprocedure.ContractAwardOfflineDetails;
import com.cptu.egp.eps.service.serviceinterface.ContractAwardOfflineService;
import com.cptu.egp.eps.service.serviceimpl.ContractAwardOfflineServiceImpl;

import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


/**
 *
 * @author Ahsan
 */
public class AwardedContractOfflineSrBean {

    private String logUserId = "0";
    //private static final Logger LOGGER = Logger.getLogger(null, null)
    private static final Logger LOGGER = Logger.getLogger(AwardedContractOfflineSrBean.class);

   // private final ContractAwardOfflineService offlineService = (ContractAwardOfflineService) AppContext.getSpringBean("TenderService");
    ContractAwardOfflineService offlineService = (ContractAwardOfflineService) AppContext.getSpringBean("ContractAwardOfflineService");
  
        public List<ContractAwardOfflineDetails> getContractAwardedOfflineDetails(String refNo) throws Exception {
     //   LOGGER.debug("getAPPDetails : " + logUserId + "starts");
        List<ContractAwardOfflineDetails> contractAwardedList = new ArrayList<ContractAwardOfflineDetails>();
        try {
            for (ContractAwardOfflineDetails contractAwardOfflineDetails : offlineService.getContractAwardOfflineData("false","","","",refNo,"","","","","search","")) {
                contractAwardedList.add(contractAwardOfflineDetails);
            }

        } catch (Exception e) {
            LOGGER.error("getContractAwardedOfflineDetails : " + e);
        }
        LOGGER.debug("getContractAwardedOfflineDetails : " + logUserId + "ends");
        return contractAwardedList;
    }

    public boolean approveAwardedContract(String refNo,String comment)
    {
    boolean isApproved = false;
    try {
            if(!comment.equals(""))
            {
              offlineService.getContractAwardOfflineData("","","","",refNo,"","","","","approve",comment);
              isApproved = true;
            }

        } catch (Exception e) {
            LOGGER.error("approveAwardedContract : " + e);
        }
        LOGGER.debug("approveAwardedContract : " + logUserId + "ends");
        return isApproved;
    }

     public List<Object[]> getMinistryForAwardedContractOffline() throws Exception {
        List<Object[]> ministryList = null;
        LOGGER.debug("getMinistryList : " + logUserId + "starts");
        try {
            ministryList =  offlineService.getMinistryForAwardedContractOffline();
        } catch (Exception e) {
            LOGGER.error("getMinistryList : " + e);
        }

        LOGGER.debug("getMinistryList : " + logUserId + "ends");
        return ministryList;
    }

     public List<Object[]> getMinistryForAwardedContractOfflineHome() throws Exception {
        List<Object[]> ministryList = null;
        LOGGER.debug("getMinistryList : " + logUserId + "starts");
        try {
            ministryList =  offlineService.getMinistryForAwardedContractOfflineHome();
        } catch (Exception e) {
            LOGGER.error("getMinistryList : " + e);
        }

        LOGGER.debug("getMinistryList : " + logUserId + "ends");
        return ministryList;
    }

      public List<Object[]> getOfflineAwardedOrganizationByMinistry(String ministry) {
       List<Object[]> orgList = null;
       try {
         orgList = offlineService.getOrgForAwardedContractOffline(ministry);
        } catch (Exception e) {
            LOGGER.error("getOrganization : " + logUserId + e);
        }
        return orgList;
    }
}
