/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import com.cptu.egp.eps.model.table.TblPartnerAdminTransfer;
import com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.RandomPassword;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author shreyansh
 */
public class UserTransferServlet extends HttpServlet {

    private TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
    final static Logger LOGGER = Logger.getLogger(UserTransferServlet.class);
    private String logUserId = "0";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                transferEmployeeServiceImpl.setLogUserId(logUserId);
            }
            LOGGER.debug("processRequest" + " : " + logUserId + " Starts");
            String mobile = "", ismaker = "";
            String action = request.getParameter("action");
            if (action == null) {
                action = "";
            }
            if (action.equalsIgnoreCase("retrive")) {
                int userTypeId = Integer.parseInt(request.getParameter("userTypeId"));
                out.print(emailInfo(request.getParameter("emailId"), userTypeId));

            } else {
                String natId = "";
                String replacemail = "";
                String email = request.getParameter("emailid");
                String desig = request.getParameter("desig");
                String isadmin = request.getParameter("isadmin");
                if (request.getParameter("makerchecker") != null) {
                    ismaker = request.getParameter("makerchecker");
                }
                String replaceby = request.getParameter("fullName");
                if (request.getParameter("mobileNo") != null) {
                    mobile = request.getParameter("mobileNo");
                }
                String comments = request.getParameter("comments");
                String fulname = request.getParameter("fulname");
                if (request.getParameter("ntnlId") != null) {
                    natId = request.getParameter("ntnlId");
                }
                int bankId = Integer.parseInt(request.getParameter("sbdevid"));
                int pid = Integer.parseInt(request.getParameter("partnetid"));
                if (request.getParameter("mailId") != null) {
                    replacemail = request.getParameter("mailId");
                }
                String transferedby = request.getParameter("userid");
                boolean flags = false;
                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");

                String userId = commonService.getUserId(email);

                TblPartnerAdminTransfer tblpartneradmin = new TblPartnerAdminTransfer();
                tblpartneradmin.setDesignation(desig);
                tblpartneradmin.setReplacedBy(fulname);
                tblpartneradmin.setReplacedByEmailId(replacemail);
                tblpartneradmin.setFullName(replaceby);
                tblpartneradmin.setTransferedBy(Integer.parseInt(transferedby));
                tblpartneradmin.setIsMakerChecker(ismaker);
                tblpartneradmin.setNationalId(natId);
                tblpartneradmin.setSbankDevelopId(bankId);
                tblpartneradmin.setPartnerId(pid);
                tblpartneradmin.setIsAdmin(isadmin);
                tblpartneradmin.setEmailId(email);
                tblpartneradmin.setTranseredDt(new java.sql.Date(new java.util.Date().getTime()));
                tblpartneradmin.setIsCurrent("yes");
                tblpartneradmin.setComments(comments);
                tblpartneradmin.setTblLoginMaster(new TblLoginMaster(Integer.parseInt(userId)));
                TransferEmployeeServiceImpl parteradmintransfer = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                RandomPassword rd = new RandomPassword();
                String password = rd.getRandomString(10);
                flags = parteradmintransfer.replaceuser(tblpartneradmin, pid, Integer.parseInt(userId), SHA1HashEncryption.encodeStringSHA1(password));
                TblPartnerAdmin tblpartneradn = new TblPartnerAdmin();
                tblpartneradn.setDesignation(desig);
                tblpartneradn.setFullName(replaceby);
                tblpartneradn.setIsMakerChecker(ismaker);
                tblpartneradn.setMobileNo(mobile);
                tblpartneradn.setNationalId(natId);
                tblpartneradn.setPartnerId(pid);
                tblpartneradn.setSbankDevelopId(bankId);
                tblpartneradn.setIsAdmin(isadmin);
                tblpartneradn.setTblLoginMaster(new TblLoginMaster(Integer.parseInt(userId)));
                parteradmintransfer.updatePartnerAdmin(tblpartneradn);
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                String[] mailTo = {email};
                MailContentUtility mailContentUtility = new MailContentUtility();
                List list = null;
                if ("".equalsIgnoreCase(ismaker)) {
                    list = mailContentUtility.getGovtDevelopmentContent(email, password);
                } else {
                    list = mailContentUtility.getGovtScheduleBankContent(email, password);
                }
                String mailSub = list.get(0).toString();
                String mailText = list.get(1).toString();
                sendMessageUtil.setEmailTo(mailTo);
                sendMessageUtil.setEmailSub(mailSub);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if (flags) {
                    if("".equalsIgnoreCase(ismaker)){
                        response.sendRedirect("admin/ManageSbDpUser.jsp?replaceFlag=y&partnerType=Development");
                        //response.sendRedirectFilter("ManageSbDpUser.jsp?replaceFlag=y&partnerType=Development");
                    }
                     else{
                        response.sendRedirect("admin/ManageSbDpUser.jsp?replaceFlag=y");
                        //response.sendRedirectFilter("ManageSbDpUser.jsp?replaceFlag=y");
                     }
                    
                } else {
                }
            }
             LOGGER.debug("preocessRequest : " + logUserId + " Ends");
        } finally {
            out.close();
        }
    }

    private String emailInfo(String emailId, int userTypeId) {
        LOGGER.debug("emailInfo : " + logUserId + " Starts");
        StringBuilder stringBuilder = new StringBuilder();
        List<Object[]> obj = null;
        try {
            obj = transferEmployeeServiceImpl.getDetailByEmailId(emailId, userTypeId);
            for (Object[] objects : obj) {
                stringBuilder.append("<tr>");
                stringBuilder.append("<td class='ff'>Full Name : <span>*</span></td>");
                stringBuilder.append("<td><input class='formTxtBox_1' id='txtFullName' name='fullName' onfocus='$('span.#spnFullName').hide();' style='width: 200px;' maxlength='101' value ='" + objects[0] + "' type='text'/></td>");
                stringBuilder.append("<span class='reqF_1' id='spnFullName' style='display: none;'></span>");
                stringBuilder.append("</tr>");
                stringBuilder.append("<tr>");
                stringBuilder.append("<td class='ff'>CID :</td>");
                stringBuilder.append("<td><input name='ntnlId' id='txtNtnlId' type='text' class='formTxtBox_1' onfocus='$('span.#NtnlIdMsg').hide();' value = '" + objects[1] + "' style='width:200px;' maxlength='26' />");
                stringBuilder.append("<span id='NtnlIdMsg' class='reqF_1' style='display: none;'></span></td>");
                stringBuilder.append("</tr>");
                stringBuilder.append("<tr>");
                stringBuilder.append("<td class='ff'>Mobile No. : </td>");
                stringBuilder.append("<td><input name='mobileNo' id='txtMob' type='text' class='formTxtBox_1' onfocus='$('span.#MobileNoMsg').hide();' style='width:200px;' value ='" + objects[2] + "' maxlength='16' /><span id='mNo' style='color: grey;'> (Mobile No. format should be e.g 12345678)</span>");
                stringBuilder.append("<span id='MobileNoMsg' class='reqF_1' style='display: none;'></span></td>");
                stringBuilder.append("</tr>");
            }
        } catch (Exception e) {
            LOGGER.error("emailInfo : " + logUserId + e);
        }
        LOGGER.debug("emailInfo : " + logUserId + " Ends");
        return stringBuilder.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
