/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.security;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Enumeration;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.Logger;
  /**
   * Filters all requests for application resources and uses an AuthorizationManager to authorize access.
   *
   * @author Darshan
   */
  public class SecurityFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(SecurityFilter.class);
    private String logUserId = "0";
    private FilterConfig filterConfig;

   /**Filter should be configured with an system error page.*/
   public void init (FilterConfig filterConfig) throws ServletException {
       this.filterConfig = filterConfig;
//        if (filterConfig != null) {
//           errorPage = filterConfig.getInitParameter("error_page");
//           LOGGER.debug("errorPage "+errorPage );
//        }
   }

   /**Obtain user from current session and invoke a singleton AuthorizationManager to determine if
    * user is authorized for the requested resource.  If not, forward them to a standard error page.
    */
   public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain)
        throws ServletException, IOException {
        
        String headerName = "";
        String headerValue = "";
        int userTypeId = 0;
        boolean isReferarNull = false;
        boolean isReqQryStrInvalid;
        boolean isByPassPage;

//         FilterServletResponseWrapper httpRes   = new FilterServletResponseWrapper((HttpServletResponse)response);
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;
        HttpSession session = req.getSession(false);

       LOGGER.debug("*---> req.getParameterMap() "+req.getParameterMap());
        LOGGER.debug("*---> req.getRequestURL() "+req.getRequestURL());
        LOGGER.debug("*---> req.getHeader(referer) "+req.getHeader("referer"));
        LOGGER.debug("*---> req.getQueryString() "+req.getQueryString());

        isReferarNull = (req.getHeader("referer") == null);
       // isReqQryStrInvalid = (req.getQueryString() != null &&
         //                     (req.getQueryString().indexOf(">") > 0 || req.getQueryString().indexOf("<") > 0 || req.getQueryString().indexOf("(") > 0 || req.getQueryString().indexOf(")") > 0));
        //isReqQryStrInvalid = (isReqQryStrInvalid? isReqQryStrInvalid : (req.getRequestURL().indexOf("?") > 0 &&
        //                                                               (req.getRequestURL().indexOf(">") > 0 || req.getRequestURL().indexOf("<") > 0 || req.getRequestURL().indexOf("(") > 0 || req.getRequestURL().indexOf(")") > 0)));

        isReqQryStrInvalid = isInvalidString(req.getQueryString());
        isReqQryStrInvalid = (isReqQryStrInvalid? isReqQryStrInvalid : isInvalidString(req.getRequestURL().toString()));

        isByPassPage = ( req.getRequestURL().toString().endsWith("/") ||
                        req.getRequestURL().toString().endsWith("//") ||
                        req.getRequestURL().toString().endsWith("/Index.jsp") ||
                        req.getRequestURL().toString().endsWith("/EmailVerification.jsp") ||
                        req.getRequestURL().toString().endsWith("/VerifyCode.jsp") ||
                        req.getRequestURL().toString().endsWith("/MemScheduleBank.jsp") ||
                        req.getRequestURL().toString().endsWith("/MandRegDoc.jsp") ||
                        req.getRequestURL().toString().endsWith("/TermsNConditions.jsp") ||
                        req.getRequestURL().toString().endsWith("/Terms.jsp") ||
                        req.getRequestURL().toString().endsWith("/PrivacyPolicy.jsp") ||
                        req.getRequestURL().toString().endsWith("/ViewPackageDetail.jsp") ||
                        req.getRequestURL().toString().endsWith("/HelpDesk.jsp") ||
                        req.getRequestURL().toString().endsWith("/ViewTender.jsp") ||
                        req.getRequestURL().toString().endsWith("/FAQ.jsp") ||
                        req.getRequestURL().toString().endsWith("/eGpGuidelines.jsp") ||
                        req.getRequestURL().toString().endsWith(".pdf") ||
                        req.getRequestURL().toString().endsWith("cptuEgpLogo.gif") ||
                        req.getRequestURL().toString().endsWith("Service") ||
                        req.getRequestURL().toString().endsWith("a.jsp") ||
                        req.getRequestURL().toString().endsWith("b.jsp") ||
                        req.getRequestURL().toString().endsWith("MakeAdvertisements.jsp") ||
                        req.getRequestURL().toString().endsWith(req.getHeader("host")+"/SessionTimedOut.jsp"));


        
        LOGGER.debug("\tHeader Name\t\tHeader Value\n");
        Enumeration eNames = req.getHeaderNames();
        while (eNames.hasMoreElements()) {
           headerName = (String) eNames.nextElement();
           headerValue = req.getHeader(headerName);
           LOGGER.debug("\t" +headerName+"\t\t"+headerValue+"\n");
           if(headerName.startsWith("Acunetix-"))
           {
               System.out.print("Request from "+headerName + " header with value "+headerValue);
               returnError(req,res,"User is not authorized to access this area!",userTypeId);
           }
         }
        
        if(session != null)
        {
            Object objUserTypeId = session.getAttribute("userTypeId");
            if (objUserTypeId != null) {
                userTypeId = Integer.parseInt(objUserTypeId.toString());
            }
        }

        LOGGER.debug("*---> userTypeId "+userTypeId+ " isReferarNull "+isReferarNull + " isReqReyStrInvalid "+isReqQryStrInvalid +" isByPassPage "+isByPassPage);
        if(isReqQryStrInvalid || (!isByPassPage && isReferarNull)) {
            LOGGER.debug("User id "+userTypeId);
            returnError(req,res,"User is not authorized to access this area!",userTypeId);
        } else {
            System.out.print("default");
            //chain.doFilter(req, new SendRedirectOverloadedResponse(req, res));
            chain.doFilter(req, res);
        }
  }

   public void destroy(){}

   /**Accepts error string, forwards to error page with error.*/
   private void returnError(HttpServletRequest request, HttpServletResponse response,String errorString, int userTypeID)
              throws ServletException, IOException {
        request.setAttribute("error",errorString);
       LOGGER.debug(" response "+(response == null));

        String strURL = "/SessionTimedOut.jsp?rs=t";
        if(userTypeID == 0)
        {
            strURL =  "/Index.jsp";
        }
        LOGGER.debug("Redirect URL "+strURL);
        response.sendRedirect(request.getContextPath()+strURL);
   }

   private boolean isInvalidString(String strValue)
   {
       LOGGER.debug("isInvalidString "+logUserId+" : Starts");
        if(strValue != null)
        {
            try {

            strValue = URLDecoder.decode(strValue, "UTF-8").toLowerCase();

            return ((strValue.indexOf("<") > 0) ||
                    (strValue.indexOf(">") > 0) ||
                    (strValue.indexOf("(") > 0) ||
                    (strValue.indexOf(")") > 0) ||
                    (strValue.indexOf("\"") > 0));
            }
            catch (UnsupportedEncodingException uee) {
                       // should never happen
                LOGGER.error("URLDecoder.decode(UTF8) failed.  :"+uee);
                   LOGGER.debug("URLDecoder.decode(UTF8) failed. "+ uee);
              }
        }
        LOGGER.debug("isInvalidString "+logUserId+" : Ends");
        return false;
   }
}
        /*

            if (userTypeId > 0) {
               //Get relevant URI.
               // boolean authorized = true;
               //String URI = ((HttpServletRequest)request).getRequestURI();

               //Obtain AuthorizationManager singleton from Spring ApplicationContext.
               //ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
               //AuthorizationManager authMgr = (AuthorizationManager)ctx.getBean("AuthorizationManager");

               //Invoke AuthorizationManager method to see if user can access resource.
//
                LOGGER.debug("*----------> userTypeId "+userTypeId +" req.getRequestURL() "+req.getRequestURL());
//
//                if(userTypeId == 2 && (req.getRequestURL().toString().contains("/officer/") || req.getRequestURL().toString().contains("/admin/"))) {
//                    authorized = false;
//                } else if(userTypeId == 3 && (req.getRequestURL().toString().contains("/tenderer/") || req.getRequestURL().toString().contains("/admin/"))) {
//                    authorized = false;
//                } else if((userTypeId == 4 || userTypeId == 5 || userTypeId == 8 || userTypeId == 11 || userTypeId == 15) && (req.getRequestURL().toString().contains("/tenderer/") || req.getRequestURL().toString().contains("/officer/"))) {
//                    authorized = false;
//                }
//
//                if (authorized) {
//                    chain.doFilter(req,res);
//                } else {
//                    LOGGER.debug("Unauthoriserd");
//                    returnError(req,res,"User is not authorized to access this area!");
//                }
                if(req.getHeader("referer") == null){
                    LOGGER.debug("Referer null");
                    returnError(req,res,"User is not authorized to access this area!",AFTER_LOGIN);
                } else {
                    chain.doFilter(req,res);
                }
            } else {
                chain.doFilter(req,res);
            }

        }
/*

        if (userTypeId != 0 && session != null ) {

           //Get relevant URI.
           //String URI = ((HttpServletRequest)request).getRequestURI();

           //Obtain AuthorizationManager singleton from Spring ApplicationContext.
           //ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
           //AuthorizationManager authMgr = (AuthorizationManager)ctx.getBean("AuthorizationManager");

           //Invoke AuthorizationManager method to see if user can access resource.
           String accessFolder = (req.getPathInfo() == null?"": req.getPathInfo().substring(0, req.getPathInfo().indexOf("/",2)));


           if(userTypeId == 2 && accessFolder.equals("tenderer")) {
                authorized = true;
           } else if(userTypeId == 3 && accessFolder.equals("officer")) {
                authorized = true;
           } else if((userTypeId == 1 || userTypeId == 4 || userTypeId == 5 || userTypeId == 8 || userTypeId == 11 || userTypeId == 15) && accessFolder.equals("admin")) {
                authorized = true;
           }

           if (authorized) {
                chain.doFilter(req,res);
           } else {
                log = Logger.getLogger("request+*+*+*+*+ ");
                returnError(req,res,"User is not authorized to access this area!");
           }
        }
        */
 