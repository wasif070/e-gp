/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblTenderListBox;
import com.cptu.egp.eps.model.table.TblTenderListDetail;
import com.cptu.egp.eps.service.serviceinterface.TendererComboService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class TenderComboSrBean {

    private String logUserId = "0";
    final Logger logger = Logger.getLogger(TenderComboSrBean.class);
    TendererComboService tendererComboService = (TendererComboService) AppContext.getSpringBean("TendererComboService");

    /**
     * Set user id for log purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {

        tendererComboService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * gives the combo data list category wise
     * @param formId
     * @param Category is the string which defines category in the case of yes or no
     * @return combo data list
     */
    public List<Object[]> getTenderListBoxCatWise(int formId, String Category) {

        logger.debug("getTenderListBoxCatWise : " + logUserId + "starts");


        List<TblTenderListBox> listTenderListBoxs = new ArrayList<TblTenderListBox>();
        List<Object[]> listComboObj = new ArrayList<Object[]>();

        try {
            listTenderListBoxs = tendererComboService.getTenderListBoxCatWise(formId, Category);
            Object[] cmbText = new Object[listTenderListBoxs.size() + 1];
            Object[] cmbValue = new Object[listTenderListBoxs.size() + 1];

            cmbText[0] = "--Select--";
            cmbValue[0] = "0";

            if (listTenderListBoxs.size() > 0) {
                int i = 1;
                for (TblTenderListBox TblTenderListBox : listTenderListBoxs) {
                    cmbText[i] = TblTenderListBox.getListBoxName();
                    cmbValue[i] = TblTenderListBox.getTenderListId();
                    i++;
                }
            }
            listComboObj.add(cmbText);
            listComboObj.add(cmbValue);
        } catch (Exception ex) {
            logger.error("getTenderListBoxCatWise : " + ex);
        }

        logger.debug("getTenderListBoxCatWise : " + logUserId + "ends");
        return listComboObj;
    }

    /**
     * gives the combo data list category wise for Payment Schedule Form
     * @param formId
     * @param Category
     * @return combo data list
     */
    public List<Object[]> getTenderListBoxForPS(int formId, String Category) {

        logger.debug("getTenderListBoxCatWise : " + logUserId + "starts");
        List<Object[]> returnBox = new ArrayList<Object[]>();
        List<Object[]> listTenderListBoxs = new ArrayList<Object[]>();

        try {
            listTenderListBoxs = tendererComboService.getTenderListBoxForPS(formId, Category);
            Object[] cmbText = new Object[listTenderListBoxs.size()+1];
            Object[] cmbValue = new Object[listTenderListBoxs.size()+1];

            cmbText[0] = "--Select--";
            cmbValue[0] = "Select";

            if(listTenderListBoxs.size() >1){
                    int i =1;
                  for(Object[] obj : listTenderListBoxs){
                      cmbText[i] = obj[0];
                      cmbValue[i] = obj[1];
                      i++;
                  }
            }

            returnBox.add(cmbText);
            returnBox.add(cmbValue);
        } catch (Exception ex) {
            logger.error("getTenderListBoxCatWise : " + ex);
        }

        logger.debug("getTenderListBoxCatWise : " + logUserId + "ends");
        return returnBox;
    }

    /**
     * gives combo data value list
     * @param id
     * @return  combo data value list
     */
    public List<TblTenderListDetail> getTenderListBoxDetail(int id) {

        logger.debug("getTenderListBoxDetail : " + logUserId + "starts");

        List<TblTenderListDetail> list = new ArrayList<TblTenderListDetail>();
        list = tendererComboService.getTenderListBoxDetail(id);

        logger.debug("getTenderListBoxDetail : " + logUserId + "ends");
        return list;
    }
}
