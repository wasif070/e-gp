/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

/**
 *
 * @author Administrator
 */
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Administrator
 */
//@WebServlet(name = "PreTenderQueryDocServlet", urlPatterns = {"/PreTenderQueryDocServlet"})
public class PreTenderReplyUploadServlet extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("PreTenderReplyUploadServlet");
    private File destinationDir;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int tenderId = 0;

        String docSizeMsg = "";
        boolean checkret = false;
        boolean flag = false;
        String documentBrief = "";
        String fileName = "";
        long fileSize = 0;
        String queryString = "";
        String pageName = "officer/PreTenderReplyUpload.jsp";
        File file = null;
        String queryId = "0";
        boolean dis = false;
         String docType="";

        response.setContentType("text/html");
        HttpSession session = request.getSession();
        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {
            try {
                try {
                    if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("remove")) {
                        String tender = "";
                        int docId = 0;
                        String id = "";
                        String docName = "";
                        if (request.getParameter("docId") != null) {
                            docId = Integer.parseInt(request.getParameter("docId"));

                        }
                        if (request.getParameter("tender") != null) {
                            tender = (request.getParameter("tender"));

                        }
                        if (request.getParameter("queryId") != null) {
                            queryId = (request.getParameter("queryId"));

                        }

                        if (request.getParameter("docName") != null) {
                            docName = request.getParameter("docName");
                        }
                        
                        if (request.getParameter("docType") != null) {
                            docType = request.getParameter("docType");
                        }

                        String whereContition = "";
                        whereContition = "replyDocId= " + docId;

                        fileName = DESTINATION_DIR_PATH + tender + "\\" + docName;
                        checkret = deleteFile(fileName);
                        if (checkret) {
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk;
                            commonMsgChk = commonXMLSPService.insertDataBySP("delete", "tbl_PreTenderReplyDocs", "", whereContition).get(0);
                            queryString = "?&tender=" + tender+"&queryId="+queryId+"&docType="+docType+"&rm=su";
                            response.sendRedirect(pageName + queryString);
                        } else {
                        }

                    } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("download")) {
                        file = new File(DESTINATION_DIR_PATH + request.getParameter("tender") + "\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                        InputStream fis = new FileInputStream(file);
                        byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                        int offset = 0;
                        int numRead = 0;
                        while ((offset < buf.length)
                                && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                            offset += numRead;

                        }
                        fis.close();
                        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                        buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName") + "\"");
                        ServletOutputStream outputStream = response.getOutputStream();
                        outputStream.write(buf);
                        outputStream.flush();
                        outputStream.close();
                    } else {
                        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                        /*
                         *Set the size threshold, above which content will be stored on disk.
                         */
                        fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB
		/*
                         * Set the temporary directory to store the uploaded files of size above threshold.
                         */
                        fileItemFactory.setRepository(tmpDir);

                        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                        /*
                         * Parse the request
                         */
                        List items = uploadHandler.parseRequest(request);
                        Iterator itr = items.iterator();
                        //For Supporting Document
                        while (itr.hasNext()) {
                            FileItem item = (FileItem) itr.next();
                            //For Supporting Document
                    /*
                             * Handle Form Fields.
                             */
                            if (item.isFormField()) {

                                if (item.getFieldName().equals("documentBrief")) {

                                    if (item.getString() == null || item.getString().trim().length() == 0) {
                                        dis = true;
                                        break;
                                    }
                                    documentBrief = item.getString();
                                } else if (item.getFieldName().equals("tenderId")) {
                                    tenderId = Integer.parseInt(item.getString());
                                } else if (item.getFieldName().equals("queryId")) {
                                    queryId = item.getString();
                                }else if (item.getFieldName().equals("preDocType")) {
                                    docType = item.getString();
                                }

                            } else {
                                if (item.getName().lastIndexOf("\\") != -1) {
                                    fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                } else {
                                    fileName = item.getName();
                                }
                                //fileName  = fileName.replaceAll(" ", "");
                                String realPath = DESTINATION_DIR_PATH + tenderId;//request.getSession().getAttribute("userId")
                                destinationDir = new File(realPath);
                                if (!destinationDir.isDirectory()) {
                                    destinationDir.mkdir();
                                }
                                docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                                //     docSizeMsg = docSizeMsg(1);
                                if (!docSizeMsg.equals("ok")) {
                                    //response.sendRedirect("SupportingDocuments.jsp?fq="+docSizeMsg);
                                } else {
                                    fileSize = item.getSize();
                                    checkret = checkExnAndSize(fileName, item.getSize(), "tenderer");
                                    if (!checkret) {
                                        break;
                                    } else {
                                        file = new File(destinationDir, fileName);
                                        if (file.isFile()) {
                                            flag = true;
                                            break;

                                        }
                                        item.write(file);
                                        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                                        fileSize = fileEncryptDecryptUtil.fileEncryptUtil(file, (int)fileSize);
                                    }
                                }
                            }

                        }
                    }

                } catch (FileUploadException ex) {
                    log("Error encountered while parsing the request", ex);
                } catch (Exception ex) {
                    log("Error encountered while uploading file", ex);
                }

                //     String pageName="officer/UploadAmendDoc.jsp";
                if (!docSizeMsg.equals("ok")) {
                    queryString = "?fq=" + docSizeMsg;
                    //response.sendRedirect("SupportingDocuments.jsp?fq="+docSizeMsg);
                } else {
                    if (dis) {
                        docSizeMsg = "File already Exists";
                        queryString = "?fq=" + docSizeMsg + "&tender=" + tenderId + "&queryId=" + queryId;
                        response.sendRedirect(pageName + queryString);
                    } else {
                        if (flag) {
                            docSizeMsg = "File already Exists";
                            queryString = "?fq=" + docSizeMsg + "&tender=" + tenderId + "&queryId=" + queryId;
                            response.sendRedirect(pageName + queryString);
                        } else {
                            if (!checkret) {
                                CheckExtension ext = new CheckExtension();
                                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                queryString = "?fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension();
                                response.sendRedirect(pageName + queryString);

                            } else {
                                if (documentBrief != null || documentBrief != "") {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    // Date dt = new Date();
                                    String userId = request.getSession().getAttribute("userId").toString();
                                    String xmldata = "";
                                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                                    documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    xmldata = "<tbl_PreTenderReplyDocs queryId=\"" + queryId + "\" documentName=\"" + fileName + "\" docDescription=\"" + documentBrief + "\" docSize=\"" + fileSize + "\" uploadedDate=\"" + format.format(new Date()) + "\" uploadedBy=\"" + userId + "\" tenderId=\"" + tenderId + "\" docType = \""+docType+ "\" />";
                                    xmldata = "<root>" + xmldata + "</root>";
                                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    CommonMsgChk commonMsgChk;
                                    try {
                                        commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_PreTenderReplyDocs", xmldata, "").get(0);
                                        queryString = "?&tender=" + tenderId + "&queryId=" + queryId+"&docType="+docType+"&up=su";
                                        response.sendRedirect(pageName + queryString);
                                         if(commonMsgChk!=null||handleSpecialChar!=null){
                                            commonMsgChk = null;
                                            handleSpecialChar = null;
                                        }

                                    } catch (Exception ex) {
                                        Logger.getLogger(PreTenderReplyUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                }
                            }
                        }
                    }
                }
            } finally {
                //      out.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public boolean checkExnAndSize(String extn, long size, String userType) {
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
            dsize = configurationMaster.getFileSize();
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        return chextn;
    }

    public String docSizeMsg(int userId) {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        return userRegisterService.docSizeCheck(userId);
    }

    public boolean deleteFile(String filePath) {

        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }

    }
}
