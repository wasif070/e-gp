/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblHelpManual;
import com.cptu.egp.eps.web.servicebean.HelpManualSrBean;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class HelpGrid extends HttpServlet {
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final Logger LOGGER = Logger.getLogger(HelpGrid.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";
    HelpManualSrBean helpManualSrBean = new HelpManualSrBean();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        PrintWriter out = response.getWriter();
        try {
            if (request.getParameter("action").equals("fetchData")) {
                LOGGER.debug("processRequest : action : fetchData : "+logUserId+ LOGGERSTART);
                response.setContentType("text/xml;charset=UTF-8");
                
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = "asc";
                String sidx = "helpManualId";
            
                 if(request.getParameter("sord")!=null){
                    sord = request.getParameter("sord");
                }
                if (request.getParameter("sidx") != null && !"".equalsIgnoreCase(request.getParameter("sidx"))){
                    sidx = request.getParameter("sidx");
                }
                
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                
                //TblHelpManual tblHelpManual = new TblHelpManual();
                HelpManualSrBean helpManualSrBean = new HelpManualSrBean();
                helpManualSrBean.setLogUserId(logUserId);
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                
                String searchField = "", searchString = "", searchOper = "";
                List<TblHelpManual> listhelpManual = new ArrayList<TblHelpManual>();
                int totalPages = 0;
                int totalCount = 0;
                
                if(_search){
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    listhelpManual = helpManualSrBean.searchHelpManual(offset, Integer.parseInt(rows), searchField, searchString, searchOper, sord,sidx);
                    totalCount = (int) helpManualSrBean.getSearchCount(searchField, searchString, searchOper);
                } else {
                    listhelpManual = helpManualSrBean.getAllHelpManuals(offset, Integer.parseInt(rows),sord,sidx);
                    totalCount = (int) helpManualSrBean.getHelpCount();
                }
                
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(rows) == 0) {
                        totalPages = totalCount / Integer.parseInt(rows);
                    } else {
                        totalPages = (totalCount / Integer.parseInt(rows)) + 1;
                    }
                } else {
                    totalPages = 0;
                }
                
                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");
                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + totalCount + "</records>");
                int srNo = offset +1;

                for (int i=0;i<listhelpManual.size();i++) {
                    out.print("<row id='" + i + "'>");
                    out.print("<cell>" + srNo + "</cell>");
                    out.print("<cell><![CDATA["+listhelpManual.get(i).getHelpUrl()+"]]></cell>");
                    out.print("<cell><![CDATA[<a href='HelpContent.jsp?action=View&id="+listhelpManual.get(i).getHelpManualId()+"'>View</a>&nbsp;|&nbsp;<a href='HelpContent.jsp?action=Edit&id="+listhelpManual.get(i).getHelpManualId()+"'>Edit</a>&nbsp;|&nbsp;<a href='javascript:void(0);' onclick='return confirmDelete("+listhelpManual.get(i).getHelpManualId()+");'>Delete</a>]]></cell>");
                    out.print("</row>");
                    srNo++;
                }
                out.print("</rows>");
                LOGGER.debug("processRequest : action : fetchData : "+logUserId+ LOGGEREND);
            }

            if("checkPage".equalsIgnoreCase(request.getParameter("action"))){
                LOGGER.debug("processRequest : action : checkPage : "+logUserId+ LOGGERSTART);
                String pageName = request.getParameter("pageName");
                HelpManualSrBean helpManualSrBean = new HelpManualSrBean();

                TblHelpManual listhelpManual =  helpManualSrBean.getHelpManuals(pageName);
                if(listhelpManual==null){
                    out.write("avail");
                }else{
                    out.write("notavail");
                }
                LOGGER.debug("processRequest : action : checkPage : "+logUserId+ LOGGEREND);
            }
            if("changeStatus".equalsIgnoreCase(request.getParameter("action"))){
                LOGGER.debug("processRequest : action : changeStatus : "+logUserId+ LOGGERSTART);
                String pageId = request.getParameter("pageId");
                helpManualSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                helpManualSrBean.changeStatus(Integer.parseInt(pageId));
                LOGGER.debug("processRequest : action : changeStatus : "+logUserId+ LOGGEREND);
            }

            if("undoChange".equalsIgnoreCase(request.getParameter("action"))){
                LOGGER.debug("processRequest : action : undoChange : "+logUserId+ LOGGERSTART);
                String pageId = request.getParameter("pageId");
                String[] pages = pageId.split("\\,");

                for(String id:pages){
                    helpManualSrBean.undoStatus(Integer.parseInt(id));
                }
                LOGGER.debug("processRequest : action : undoChange : "+logUserId+ LOGGEREND);
            }
        }catch(Exception ex){
            LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
        }finally {
            out.close();
        }
        LOGGER.debug("processRequest  : "+logUserId+ LOGGEREND);
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
