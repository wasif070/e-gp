/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblStateMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.DepartmentCreationService;
import com.cptu.egp.eps.web.databean.DepartmentCreationDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.eps.service.audit.AuditTrail;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author rishita
 */
public class ManageEmployeeGridSrBean {

    final Logger logger = Logger.getLogger(ManageEmployeeGridSrBean.class);
    
    private DepartmentCreationService departmentCreationService = (DepartmentCreationService) AppContext.getSpringBean("DepartmentCreationService");
    private CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        departmentCreationService.setAuditTrail(auditTrail);
    }

    private String logUserId="0";

    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        departmentCreationService.setUserId(logUserId);
        commonService.setUserId(logUserId);
        this.logUserId = logUserId;
    }

     /**
     * fetching count for row
     * @param deptType
     * @return number of records
     */
    public long getAllCountOfDept(String deptType){
        logger.debug("getAllCountOfDept : "+logUserId+" Starts ");
        long lng =0;
        try {
            lng = departmentCreationService.getAllCountOfDept(deptType);
        } catch (Exception e) {
            logger.error("getAllCountOfDept : "+logUserId+" : "+e);
    }
        logger.debug("getAllCountOfDept : "+logUserId+" Ends ");
        return lng;
    }
    /**
     * fetching count for row
     * @param deptType
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     */
    public long getSearchCountOfDept(String deptType,String searchField,String searchString,String searchOper)  {
        logger.debug("getSearchCountOfDept : "+logUserId+" Starts ");
        long lng = 0;
        try {
            lng = departmentCreationService.getSearchCountOfDept(deptType, searchField, searchString, searchOper);
        } catch (Exception e) {
            logger.error("getSearchCountOfDept : "+logUserId+" : "+e);
        }
        logger.debug("getSearchCountOfDept : "+logUserId+" Ends ");
        return lng;
    }

    List<TblDepartmentMaster> departmentMasterList = new ArrayList<TblDepartmentMaster>();
    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String getSortOrder = "";
    private String sortCol = "";
    
    public String getSortOrder() {
        return getSortOrder;
    }

    public void setSortOrder(String getSortOrder) {
        this.getSortOrder = getSortOrder;
    }

    public String getSortCol() {
        return sortCol;
    }

    public void setSortCol(String sortCol) {
        this.sortCol = sortCol;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public boolean isSearch() {
        return _search;
    }

    public void setSearch(boolean _search) {
        this._search = _search;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getOp_ENUM() {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM) {
        this.op_ENUM = op_ENUM;
    }

    /**
     * fetching information for Department
     * @param deptType
     * @return Object with data
     */
    public List<TblDepartmentMaster> getDepartmentMasterList(String deptType){
        logger.debug("getDepartmentMasterList : "+logUserId+" Starts ");
        try{
        if (departmentMasterList.isEmpty()) {
//            if (_search) {
//                for (TblDepartmentMaster tblDeptMaster : departmentCreationService.findDeptCreatMaster(getOffset(), getLimit(), "departmentName", Operation_enum.LIKE, "MINISTRY", getSortCol(), Operation_enum.ORDERBY, getSortOrder())) {
//                    departmentMasterList.add(tblDeptMaster);
//                }
//            } else {
                if(sortCol.equalsIgnoreCase("")){
                    for (TblDepartmentMaster tblDeptMaster : departmentCreationService.findDeptCreatMaster(getOffset(), getLimit(),"departmentType",Operation_enum.LIKE,deptType, "createdDate", Operation_enum.ORDERBY, Operation_enum.DESC)) {
                        departmentMasterList.add(tblDeptMaster);
                    }
                }else{
                if (getSortOrder().equalsIgnoreCase("asc")) {
                    for (TblDepartmentMaster tblDeptMaster : departmentCreationService.findDeptCreatMaster(getOffset(), getLimit(),"departmentType",Operation_enum.LIKE,deptType, "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC)) {
                        departmentMasterList.add(tblDeptMaster);
                    }
                } else if (getSortOrder().equalsIgnoreCase("desc")) {
                    for (TblDepartmentMaster tblDeptMaster : departmentCreationService.findDeptCreatMaster(getOffset(), getLimit(), getSortCol(), Operation_enum.ORDERBY, Operation_enum.DESC)) {
                        departmentMasterList.add(tblDeptMaster);
                    }
                }
            }
//            }
        }
        }catch(Exception e)
        {
             logger.error("getDepartmentMasterList : "+logUserId+" : "+e);
        }
        logger.debug("getDepartmentMasterList : "+logUserId+" Ends ");
        return departmentMasterList;
    }

    /**
     * fetching information for Department
     * @param deptType
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return Object with data
     */
    public List<TblDepartmentMaster> getDepartmentMasterList_search(String deptType,String searchField,String searchString,String searchOper)  {
        logger.debug("getDepartmentMasterList_search : "+logUserId+" Starts ");
        try{
        if (departmentMasterList.isEmpty()) {
//            if (_search) {
//                for (TblDepartmentMaster tblDeptMaster : departmentCreationService.findDeptCreatMaster(getOffset(), getLimit(), "departmentName", Operation_enum.LIKE, "MINISTRY", getSortCol(), Operation_enum.ORDERBY, getSortOrder())) {
//                    departmentMasterList.add(tblDeptMaster);
//                }
//            } else {
                if (getSortOrder().equalsIgnoreCase("asc")) {
                    if(searchOper.equalsIgnoreCase("EQ")) {
                        for (TblDepartmentMaster tblDeptMaster : departmentCreationService.
                                findDeptCreatMaster(getOffset(), getLimit(),"departmentType",Operation_enum.LIKE,deptType,searchField, Operation_enum.EQ, searchString, "createdDate", Operation_enum.ORDERBY, Operation_enum.ASC)) {
                            departmentMasterList.add(tblDeptMaster);
                        }
                    } else if(searchOper.equalsIgnoreCase("CN")) {
                        for (TblDepartmentMaster tblDeptMaster : departmentCreationService.
                                findDeptCreatMaster(getOffset(), getLimit(),"departmentType",Operation_enum.LIKE,deptType,searchField, Operation_enum.LIKE, "%"+searchString+"%", "createdDate", Operation_enum.ORDERBY, Operation_enum.ASC)) {
                            departmentMasterList.add(tblDeptMaster);
                        }
                    }
                } else if (getSortOrder().equalsIgnoreCase("desc")) {
                    if(searchOper.equalsIgnoreCase("EQ")) {
                        for (TblDepartmentMaster tblDeptMaster : departmentCreationService.
                                findDeptCreatMaster(getOffset(), getLimit(),"departmentType",Operation_enum.LIKE,deptType,searchField, Operation_enum.EQ, searchString, "createdDate", Operation_enum.ORDERBY, Operation_enum.DESC)) {
                            departmentMasterList.add(tblDeptMaster);
                        }
                    } else if(searchOper.equalsIgnoreCase("CN")) {
                        for (TblDepartmentMaster tblDeptMaster : departmentCreationService.
                                findDeptCreatMaster(getOffset(), getLimit(),"departmentType",Operation_enum.LIKE,deptType,searchField, Operation_enum.LIKE, "%"+searchString+"%", "createdDate", Operation_enum.ORDERBY, Operation_enum.DESC)) {
                            departmentMasterList.add(tblDeptMaster);
                        }
                    }
                }
//            }
        }
        }catch(Exception e)
        {
            logger.error("getDepartmentMasterList_search : "+logUserId+" : "+e);
        }
         logger.debug("getDepartmentMasterList_search : "+logUserId+" Ends ");
        return departmentMasterList;
    }

    public void setDepartmentMasterList(List<TblDepartmentMaster> departmentMasterList) {
        this.departmentMasterList = departmentMasterList;
    }

    public JSONArray getJSONArray(List<TblDepartmentMaster> list) {
        logger.debug("getJSONArray : "+logUserId+" Starts ");
        JSONArray jSONArray = null;
        try{
        jSONArray = new JSONArray();
        String para[] = {"DepartmentId", "DepartmentName", "Address", "CreatedDate", "Division", "Operation"};
        List l = new ArrayList();
        for (TblDepartmentMaster tblDeptMaster : list) {
            l.add(tblDeptMaster.getDepartmentId());
            l.add(tblDeptMaster.getDepartmentName());
            l.add(tblDeptMaster.getAddress());
            l.add(tblDeptMaster.getCreatedDate());
            l.add(tblDeptMaster.getDepartmentType());
        }
        for (int j = 0; j < (l.size() / para.length); j++) {
            JSONObject jSONObject = new JSONObject();
            for (int i = 0; i < para.length; i++) {
                int c = (para.length * j) + i;
                jSONObject.put(para[i], l.get(c));
            }
            jSONArray.put(jSONObject);
        }
        }catch(Exception e)
        {
            logger.error("getJSONArray : "+logUserId+" : "+e);
        }
        logger.debug("getJSONArray : "+logUserId+" Ends ");
        return jSONArray;
    }

    /**
     * fetching information for Department
     * @param departmentId
     * @return Object with Data
     */
    public List<TblDepartmentMaster> getFindUser(short departmentId) {
        logger.debug("getFindUser : "+logUserId+" Starts ");
        List<TblDepartmentMaster> tblList = null;
        try{
        tblList = new ArrayList<TblDepartmentMaster>();
        for (TblDepartmentMaster tblDeptMaster : departmentCreationService.findDeptUserMaster("departmentId", Operation_enum.EQ, departmentId)) {
            setStateId(tblDeptMaster.getTblCountryMaster().getCountryId());
            for(TblDepartmentMaster test : departmentCreationService.findDeptUserMaster("departmentId",Operation_enum.EQ,tblDeptMaster.getParentDepartmentId())){
                setParentDeptName(test.getDepartmentName());
            }
            tblList.add(tblDeptMaster);
        }
        }catch(Exception e)
        {
            logger.error("getFindUser : "+logUserId+" : "+e);
        }
        logger.debug("getFindUser : "+logUserId+" Ends ");
        return tblList;
    }
    private String parentDeptName;

    public String getParentDeptName() {
        return parentDeptName;
    }

    public void setParentDeptName(String parentDeptName) {
        this.parentDeptName = parentDeptName;
    }

    short stateId;

    public short getStateId() {
        return stateId;
    }

    public void setStateId(short stateId) {
        this.stateId = stateId;
    }
    private List<SelectItem> stateList = new ArrayList<SelectItem>();

    /**
     * fetching states from Tbl_StateMaster
     * @return list of states
     */
    public List<SelectItem> getStateList() {
        logger.debug("getStateList : "+logUserId+" Starts ");
        try {
        if (stateList.isEmpty()) {
            List<TblStateMaster> stateMasters = commonService.getState((short) stateId);
            for (TblStateMaster stateMaster : stateMasters) {
                stateList.add(new SelectItem(stateMaster.getStateId(), stateMaster.getStateName()));
            }
        }
        } catch (Exception e) {
            logger.error("getStateList : "+logUserId+" : "+e);
        }
         logger.debug("getStateList : "+logUserId+" Ends ");
        return stateList;
    }

    public void setStateList(List<SelectItem> stateList) {
        this.stateList = stateList;
    }

    public List<TblDepartmentMaster> getDefaultParentDept(String deptType) {
        logger.debug("getDefaultParentDept : "+logUserId+" Starts ");
        List<TblDepartmentMaster> list = null;
        try {
            list = departmentCreationService.findDeptUserMaster("departmentType", Operation_enum.LIKE, deptType,"departmentName", Operation_enum.ORDERBY, Operation_enum.ASC);
        } catch (Exception e) {
            logger.error("getDefaultParentDept : "+logUserId+" : "+e);
    }
        logger.debug("getDefaultParentDept : "+logUserId+" Ends ");
        return list;
    }

    /**
     * Update data
     * @param departmentCreationDtBean
     * @return true if update else false
     */
    public boolean updateDepartmentMaster(DepartmentCreationDtBean departmentCreationDtBean){
        boolean flag = false;
        logger.debug("updateDepartmentMaster : "+logUserId+" Starts ");
        try{
        TblDepartmentMaster tblDepartmentMaster = _toTblDepartmentMaster(departmentCreationDtBean);
        short pid;
        if (departmentCreationDtBean.getDepartmentType().equals("Ministry")) {
            pid = 2;
            tblDepartmentMaster.setParentDepartmentId(pid);
            tblDepartmentMaster.setDepartmentHirarchy(departmentCreationDtBean.getDepartmentName());
            tblDepartmentMaster.setOrganizationType("Large");
        }else if (departmentCreationDtBean.getDepartmentType().equals("Division")) {
            tblDepartmentMaster.setParentDepartmentId(departmentCreationDtBean.getDeptParentId());
            tblDepartmentMaster.setDepartmentHirarchy(departmentCreationDtBean.getDeptHirchy());
            tblDepartmentMaster.setOrganizationType("Large");
        } else{
            tblDepartmentMaster.setParentDepartmentId(departmentCreationDtBean.getDeptParentId());
            tblDepartmentMaster.setDepartmentHirarchy(departmentCreationDtBean.getDeptHirchy());
            tblDepartmentMaster.setOrganizationType(departmentCreationDtBean.getOrganizationType());
          //  tblDepartmentMaster.setOrganizationType("Large");//default
        }
        if(departmentCreationDtBean.getPhoneNo() == null || departmentCreationDtBean.getPhoneNo().trim().equals("")){
            tblDepartmentMaster.setPhoneNo("");
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date d = dateFormat.parse(departmentCreationDtBean.getStrCreatedDate());
        tblDepartmentMaster.setCreatedBy(1);
        tblDepartmentMaster.setStatus("Approved");
        tblDepartmentMaster.setCreatedDate(d);
        //tblDepartmentMaster.setApprovingAuthorityId(departmentCreationDtBean.getApprovingAuthId());
        tblDepartmentMaster.setApprovingAuthorityId(0);
        tblDepartmentMaster.setTblCountryMaster(new TblCountryMaster(departmentCreationDtBean.getCountryId()));
        tblDepartmentMaster.setTblStateMaster(new TblStateMaster(departmentCreationDtBean.getStateId()));
        //tblDepartmentMaster.setDeptNameInBangla(departmentCreationDtBean.getDeptBanglaString().getBytes("UTF-8"));
        if (departmentCreationDtBean.getDeptBanglaString() == null || departmentCreationDtBean.getDeptBanglaString().trim().equals("")) {
            tblDepartmentMaster.setDeptNameInBangla(null);
        } else {
            tblDepartmentMaster.setDeptNameInBangla(departmentCreationDtBean.getDeptBanglaString().getBytes("UTF-8"));
        }
        if (departmentCreationDtBean.getFaxNo() == null || departmentCreationDtBean.getFaxNo().trim().equals("")) {
            tblDepartmentMaster.setFaxNo("");
        }
        if (departmentCreationDtBean.getCity() == null || departmentCreationDtBean.getCity().trim().equals("")) {
            tblDepartmentMaster.setCity("");
        }
        if (departmentCreationDtBean.getPostCode() == null || departmentCreationDtBean.getPostCode().trim().equals("")) {
            tblDepartmentMaster.setPostCode("");
        }
        if (departmentCreationDtBean.getMobileNo() == null || departmentCreationDtBean.getMobileNo().trim().equals("")) {
            tblDepartmentMaster.setMobileNo("");
        }
        
        if (departmentCreationDtBean.getWebsite() == null || departmentCreationDtBean.getWebsite().trim().equals("")) {
            tblDepartmentMaster.setWebsite("");
        } 
        
        if(departmentCreationService.updateDepartmentCreation(tblDepartmentMaster)){
            flag = true;
        }
        }catch(Exception e)
        {
             logger.error("updateDepartmentMaster : "+logUserId+" : "+e);
    }
        logger.debug("updateDepartmentMaster : "+logUserId+" Ends ");
        return flag;
    }
     /**
     * copies properties
     * @param departmentCreationDtBean
     * @return Object with copied properties
     */
    public TblDepartmentMaster _toTblDepartmentMaster(DepartmentCreationDtBean departmentCreationDtBean) {
        logger.debug("_toTblDepartmentMaster : "+logUserId+" Starts ");
        TblDepartmentMaster tblDepartmentMaster = null;
        try {
            tblDepartmentMaster = new TblDepartmentMaster();
        BeanUtils.copyProperties(departmentCreationDtBean, tblDepartmentMaster);
        } catch (Exception e) {
            logger.error("_toTblDepartmentMaster : "+logUserId+" : "+e);
        }
        logger.debug("_toTblDepartmentMaster : "+logUserId+" Ends ");
        return tblDepartmentMaster;
    }
}
