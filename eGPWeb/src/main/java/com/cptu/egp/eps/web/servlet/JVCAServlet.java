
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblJointVenture;
import com.cptu.egp.eps.model.table.TblJvcapartners;
import com.cptu.egp.eps.service.serviceinterface.JointVentureService;
import com.cptu.egp.eps.service.serviceinterface.JvcapartnersService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.servicebean.JvcaSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



/**
 *
 * @author Administrator
 */
public class JVCAServlet extends HttpServlet {

    JointVentureService jointVentureService = (JointVentureService) AppContext.getSpringBean("JointVentureService");
    JvcapartnersService jvcapartnersService = (JvcapartnersService) AppContext.getSpringBean("JvcapartnersService");
    
    /** 
     * This servlet handles all the request of jvca
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        jointVentureService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
        jvcapartnersService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
        try {
            if (request.getSession().getAttribute("userId") == null) {
                response.sendRedirect("SessionTimedOut.jsp");
            }else{
            String action = "";
            String userId = "";

            HttpSession hs = request.getSession();
            userId = hs.getAttribute("userId").toString();

            if(request.getParameter("funName")!=null && !"".equalsIgnoreCase(request.getParameter("funName"))){
                action = request.getParameter("funName");
            }

            if("partnerSearch".equalsIgnoreCase(action)){
                String companyName = "";
                String emailId = "";
                String searchData = "";

                if(request.getParameter("companyName")!=null && !"".equalsIgnoreCase(request.getParameter("companyName"))){
                    companyName = request.getParameter("companyName");
                }
                if(request.getParameter("emailId")!=null && !"".equalsIgnoreCase(request.getParameter("emailId"))){
                    emailId = request.getParameter("emailId");
                }
                searchData = SearchData(companyName, emailId);
                out.println(searchData);
            }

             if("completeJVCA".equalsIgnoreCase(action)){

                String jvcId = "";
                String emailId = "";
                String searchData = "";

                if(request.getParameter("jvcId")!=null && !"".equalsIgnoreCase(request.getParameter("jvcId"))){
                    jvcId = request.getParameter("jvcId");
                }
                List<Object[]> listEmailId = new ArrayList<Object[]>();
                listEmailId = jvcapartnersService.getJvcaPartnerNameAndEmail(Integer.parseInt(jvcId),Integer.parseInt(userId));
                String partnerName = listEmailId.get(0)[0].toString();
                  String emailFrom = listEmailId.get(0)[1].toString();
                  List<Object[]> listMailContent = new ArrayList<Object[]>();
                  listMailContent = jointVentureService.forMailContent(Integer.parseInt(jvcId),Integer.parseInt(userId));
                int status = jointVentureService.updateJVCA(Integer.parseInt(jvcId),Integer.parseInt(userId),listMailContent.get(0)[6].toString());
                
                List<Object> listemailTo = new ArrayList<Object>();
                listemailTo = jointVentureService.forMailEmail(Integer.parseInt(jvcId),Integer.parseInt(userId));
                emailId = "";
                for(int i=0;i<listemailTo.size();i++){
                    emailId += listemailTo.get(i)+",";
                }
                
                
                
                UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                userRegisterService.contentAdmMsgBox(emailId, emailFrom, "JVCA Invitation received", msgBoxContentUtility.JvcaStatusComplete(listMailContent,partnerName));
                listMailContent.clear();
                listEmailId.clear();
                listMailContent = null;
                listEmailId = null;
                if(status==1){
                    response.sendRedirect("tenderer/JvcaList.jsp?msg=Complete");
                    //response.sendRedirectFilter("JvcaList.jsp?msg=Complete");
                }else if(status==0){
                    response.sendRedirect("tenderer/JvcaList.jsp?msg=CompleteFail");
                    //response.sendRedirectFilter("JvcaList.jsp?msg=CompleteFail");
                }
            }
            
            if("createJVCA".equalsIgnoreCase(action)){

                String JVCAName = "";
                String[] companyIds = null;
                String[] userIds = null;
                String[] cmbPartnerType = null;
                String[] nominatedPartner = null;

                if(request.getParameter("txtJVCAName")!=null && !"".equalsIgnoreCase(request.getParameter("txtJVCAName"))){
                    JVCAName = request.getParameter("txtJVCAName");
                }
                if(request.getParameter("companyIds")!=null){
                    companyIds = request.getParameterValues("companyIds");
                }
                if(request.getParameter("userIds")!=null){
                    userIds = request.getParameterValues("userIds");
                }
                if(request.getParameter("cmbPartnerType")!=null){
                    cmbPartnerType = request.getParameterValues("cmbPartnerType");
                }
                if(request.getParameter("radioisNominated")!=null){
                    nominatedPartner = request.getParameterValues("radioisNominated");
                }

                TblJointVenture tblJointVenture = new TblJointVenture();
                tblJointVenture.setJvname(JVCAName);
                tblJointVenture.setCreationDate(new java.util.Date());
                tblJointVenture.setCreatedBy(Integer.parseInt(userId));
                tblJointVenture.setJvstatus("Pending");
                tblJointVenture.setNewJvuserId(0);
                
                List<TblJvcapartners> listJvcaPartner = new ArrayList<TblJvcapartners>();
                int i = 0;
                
                for(String Company:companyIds){
                    TblJvcapartners tblJvcapartners = new TblJvcapartners();
                    tblJvcapartners.setUserId(Integer.parseInt(userIds[i]));
                    tblJvcapartners.setCompanyId(Integer.parseInt(companyIds[i]));
                    tblJvcapartners.setJvRole(cmbPartnerType[i]);
                    tblJvcapartners.setSentRequestDt(new java.util.Date());

                    if(nominatedPartner[0].equals(userIds[i])){
                        tblJvcapartners.setIsNominatedPartner("Yes");
                    }else{
                        tblJvcapartners.setIsNominatedPartner("No");
                    }
                    
                    tblJvcapartners.setJvAcceptRejStatus("Pending");
                    tblJvcapartners.setJvAccpetDatetime(new java.util.Date());
                    listJvcaPartner.add(tblJvcapartners);
                    i++;
                }
                        jointVentureService.createJVCA(tblJointVenture,listJvcaPartner);
                
                response.sendRedirect("tenderer/JvcaList.jsp");
                //response.sendRedirectFilter("JvcaList.jsp");
            }

            if("updateJVCA".equalsIgnoreCase(action)){

                String JVCAName = "";
                String jvcId = "";
                String[] companyIds = null;
                String[] userIds = null;
                String[] cmbPartnerType = null;
                String[] nominatedPartner = null;

                if(request.getParameter("txtJVCAName")!=null && !"".equalsIgnoreCase(request.getParameter("txtJVCAName"))){
                    JVCAName = request.getParameter("txtJVCAName");
                }
                if(request.getParameter("companyIds")!=null){
                    companyIds = request.getParameterValues("companyIds");
                }
                if(request.getParameter("userIds")!=null){
                    userIds = request.getParameterValues("userIds");
                }
                if(request.getParameter("cmbPartnerType")!=null){
                    cmbPartnerType = request.getParameterValues("cmbPartnerType");
                }
                if(request.getParameter("radioisNominated")!=null){
                    nominatedPartner = request.getParameterValues("radioisNominated");
                }
                if(request.getParameter("jvcId")!=null){
                    jvcId = request.getParameter("jvcId");
                }

                List<TblJvcapartners> listJvcaPartner = new ArrayList<TblJvcapartners>();
                int i = 0;

                for(String Company:companyIds){
                    TblJvcapartners tblJvcapartners = new TblJvcapartners();
                    tblJvcapartners.setUserId(Integer.parseInt(userIds[i]));
                    tblJvcapartners.setCompanyId(Integer.parseInt(companyIds[i]));
                    tblJvcapartners.setJvRole(cmbPartnerType[i]);
                    tblJvcapartners.setSentRequestDt(new java.util.Date());

                    if(nominatedPartner[0].equals(userIds[i])){
                        tblJvcapartners.setIsNominatedPartner("Yes");
                    }else{
                        tblJvcapartners.setIsNominatedPartner("No");
                    }

                    tblJvcapartners.setJvAcceptRejStatus("Pending");
                    tblJvcapartners.setJvAccpetDatetime(new java.util.Date());
                    listJvcaPartner.add(tblJvcapartners);
                    i++;
                }
                
                int status = jointVentureService.updateJVCAName(Integer.parseInt(jvcId), JVCAName);
                
                if(status==1){
                    jvcapartnersService.deleteJVCAPartner(Integer.parseInt(jvcId));
                    jvcapartnersService.updatePartner(Integer.parseInt(jvcId),listJvcaPartner);
                    response.sendRedirect("tenderer/JvcaList.jsp?msg=Update");
                    //response.sendRedirectFilter("JvcaList.jsp?msg=Update");
                }else{
                    response.sendRedirect("tenderer/JvcaList.jsp?msg=UpdateFail");
                    //response.sendRedirectFilter("JvcaList.jsp?msg=UpdateFail");
                }
            }
            
            if("viewjvca".equalsIgnoreCase(action)){
            response.setContentType("text/xml;charset=UTF-8");
                JvcaSrBean jvcaSrBean = new JvcaSrBean();
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                //System.out.println(" page is ::: "+page);
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                String rowList = request.getParameter("rowList");
                List<Object[]> listPrposedJvca = null;
                //System.out.println(" viewGovData  ---------------------->>>"+action);
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                
                String searchField = "", searchString = "", searchOper = "";
                 if(_search){
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    //System.out.println("before calling getProjectData1");
                    listPrposedJvca =jvcaSrBean.preposedJvca(Integer.parseInt(userId),searchField,searchOper,searchString);
                    //System.out.println("after calling getProjectData1");
                } else {
                    //System.out.println("search  == false");
                    /*if("".equalsIgnoreCase(sidx)){
                            sidx = "srno";
                            sord = "desc";
                        }*/
                    listPrposedJvca = jvcaSrBean.preposedJvca(Integer.parseInt(userId),sidx,sord);
                }
                 int totalPages = 0;
                long totalCount = listPrposedJvca.size();
                //System.out.println("totalCount : "+totalCount);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(rows) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(rows));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(rows)) + 1);
                    }
                } else {
                    totalPages = 0;
                }

                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                 out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + listPrposedJvca.size() + "</records>");
                int srNo = offset+1;
                for (int i = (Integer.parseInt(page)-1)*10 ; i < listPrposedJvca.size(); i++) {
                    out.print("<row id='" + listPrposedJvca.get(i)[0] + "'>");
                    out.print("<cell>" + srNo + "</cell>");
                    out.print("<cell> <![CDATA[" + listPrposedJvca.get(i)[1] + "]]> </cell>");
                    if("Approved".equalsIgnoreCase(listPrposedJvca.get(i)[2].toString())){
                    out.print("<cell> <![CDATA[ Accepted ]]> </cell>");
                    }else{
                        out.print("<cell> <![CDATA["+ listPrposedJvca.get(i)[2] +"]]> </cell>");
                    }
                    String viewLink="<a href=\"ViewJVCA.jsp?jvcId="+ listPrposedJvca.get(i)[0]+"\">View</a>";
                    if("pending".equalsIgnoreCase(listPrposedJvca.get(i)[2].toString())){
                    String editLink="<a href=\"CreateJVCA.jsp?jvcId="+ listPrposedJvca.get(i)[0]+"\">Edit</a>";
                    String completeLink = "<a href=\"ViewJVCA.jsp?jvcId="+ listPrposedJvca.get(i)[0]+"&action=complete\">Send Invitation</a>";
                    out.print("<cell> <![CDATA[" + editLink + "]]> | <![CDATA[" + viewLink + "]]> | <![CDATA[" + completeLink + "]]> </cell>");
                    }else{
                        String createJvcLink = null;
                        String pageName  = null;
                        
                        out.print("<cell> <![CDATA[");
                        if("Approved".equalsIgnoreCase(listPrposedJvca.get(i)[2].toString()) && "Yes".equalsIgnoreCase(listPrposedJvca.get(i)[3].toString())){
                            
                            if(listPrposedJvca.get(i)[5].toString().trim().equalsIgnoreCase("0")){
                                    pageName = "RegisterJVCA.jsp?jvcId="+ listPrposedJvca.get(i)[0];
                                    createJvcLink = "<a href=\""+pageName+"\">Form JVCA</a>";
                                }else{
                                    Object [] obj  = jvcaSrBean.getNextScreen(listPrposedJvca.get(i)[5].toString());
                                    if( obj[1].toString().equalsIgnoreCase("pending")){
                                        pageName  = obj[0].toString()+".jsp?uId="+listPrposedJvca.get(i)[5];
                                        createJvcLink = "<a href=\""+pageName+"\">Form JVCA</a>";
                                    }else{
                                       pageName = "ViewJVCADetails.jsp?uId="+listPrposedJvca.get(i)[5];
                                       createJvcLink = "<a href=\""+pageName+"\">View JVCA Detail</a>";
                                    }
                                }
                            
                            /*if("0".equalsIgnoreCase(listPrposedJvca.get(i)[5].toString())){
                                pageName = "RegisterJVCA.jsp?jvcId="+ listPrposedJvca.get(i)[0];
                                createJvcLink = "<a href=\""+pageName+"\">Form JVCA</a>";
                            }else{
                                
                                pageName = "ViewJVCADetails.jsp?uId="+listPrposedJvca.get(i)[5];
                                createJvcLink = "<a href=\""+pageName+"\">View JVCA Detail</a>";
                            }*/
                            out.print( createJvcLink +" | ");
                        }
                    out.print(viewLink + "]]> </cell>");
                    }
                    out.print("</row>");
                    srNo++;
                }
                 out.print("</rows>");
            }
            if("viewpartner".equalsIgnoreCase(action)){
            response.setContentType("text/xml;charset=UTF-8");
                JvcaSrBean jvcaSrBean = new JvcaSrBean();
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                //System.out.println(" page is ::: "+page);
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                String rowList = request.getParameter("rowList");
                List<Object []> listPartner = null;
                //System.out.println(" viewGovData  ---------------------->>>"+action);
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String searchField = "", searchString = "", searchOper = "";
                 if(_search){
                    searchField = request.getParameter("searchField");
                    searchString = request.getParameter("searchString");
                    searchOper = request.getParameter("searchOper");
                    //System.out.println("before calling getProjectData1");
                    listPartner =jvcaSrBean.partnerRequestJvcaList(Integer.parseInt(userId),searchField,searchOper,searchString);
                    //System.out.println("after calling getProjectData1");
                } else {
                    //System.out.println("search  == false");
                    if(sidx.equalsIgnoreCase("")||sidx.equalsIgnoreCase("srno")){
                        sidx = "jp.jvAccpetDatetime";
                        sord = "desc";
                    }
                    listPartner = jvcaSrBean.partnerRequestJvcaList(Integer.parseInt(userId),sidx,sord);
                }
                 int totalPages = 0;
                long totalCount = listPartner.size();
                //System.out.println("totalCount : "+totalCount);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(rows) == 0) {
                        totalPages = (int) (totalCount / Integer.parseInt(rows));
                    } else {
                        totalPages = (int) ((totalCount / Integer.parseInt(rows)) + 1);
                    }
                } else {
                    totalPages = 0;
                }

                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                 out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + listPartner.size() + "</records>");
                int srNo = offset+1;
                for (int i = (Integer.parseInt(page)-1)*10 ; i < listPartner.size(); i++) {
                  //  System.out.println(projData.get(i).getFieldName5());
                    out.print("<row id='" + listPartner.get(i)[1] + "'>");
                    out.print("<cell>" + srNo + "</cell>");
                    out.print("<cell> <![CDATA[" + listPartner.get(i)[0] + "]]> </cell>");
                    out.print("<cell> <![CDATA[" + listPartner.get(i)[2] + "]]> </cell>");
                    //out.print("<cell> <![CDATA[" + listPartner.get(i)[5] + "]]> </cell>");
                    String viewLink="<a href=\"ViewJVCA.jsp?jvcId="+ listPartner.get(i)[1]+"\">View JVCA Invitation</a>";
                    if(listPartner.get(i)[2].toString().trim().equalsIgnoreCase("pending")){
                        String editLink="<a href=\"JVCARequest.jsp?JVCId="+ listPartner.get(i)[1]+"\">Process</a>";
                        out.print("<cell> <![CDATA[" + editLink + "]]> </cell>");
                    }else{
                        String pageName = "";
                        String createJvcLink = null;
                        if(listPartner.get(i)[5].toString().trim().equalsIgnoreCase("Approved")||listPartner.get(i)[5].toString().trim().equalsIgnoreCase("Approve")){
                            //List of object in object[0] jvName, [1] JvId, [2] jvAcceptRejStatus,[3] creationDate,[4] jvAccpetDatetime,[5] jvstatus,[6] isNominatedPartner,[7] newJvuserId 
                            if(listPartner.get(i)[6].toString().trim().equalsIgnoreCase("yes")){
                                if(listPartner.get(i)[7].toString().trim().equalsIgnoreCase("0")){
                                    pageName = "RegisterJVCA.jsp?jvcId="+ listPartner.get(i)[1];
                                    createJvcLink = "<a href=\""+pageName+"\">Form JVCA</a>";
                                }else{
                                    Object [] obj  = jvcaSrBean.getNextScreen(listPartner.get(i)[7].toString());
                                    if( "pending".equalsIgnoreCase(obj[1].toString())){
                                        pageName  = obj[0].toString()+".jsp?uId="+listPartner.get(i)[7];
                                        createJvcLink = "<a href=\""+pageName+"\">Form JVCA</a>";
                                    }else{
                                       pageName = "ViewJVCADetails.jsp?uId="+listPartner.get(i)[7];
                                       createJvcLink = "<a href=\""+pageName+"\">View JVCA Detail</a>";
                                    }
                                }
                                //createJvcLink = "<a href=\""+pageName+"\">Form JVCA</a>";
                                out.print("<cell> <![CDATA["+createJvcLink + "|" + viewLink +"]]> </cell>");
                            }else{
                                if(!listPartner.get(i)[7].toString().trim().equalsIgnoreCase("0")){
                                    pageName = "ViewJVCADetails.jsp?uId="+listPartner.get(i)[7];
                                       createJvcLink = "<a href=\""+pageName+"\">View JVCA Detail</a>";
                                       out.print("<cell> <![CDATA["+createJvcLink+"|" +viewLink+"]]> </cell>");
                                }else{
                                    out.print("<cell> <![CDATA["+viewLink+"]]> </cell>");
                                }
                                
                            }
                        }else{
                             out.print("<cell> <![CDATA["+viewLink+"]]> </cell>");
                        }
                    }
                    out.print("</row>");
                    srNo++;
                }
                 out.print("</rows>");
            }
        }
        } finally { 
            out.close();
        }
    }

    

    private String SearchData(String companyName, String emailId){

        StringBuilder sb = new StringBuilder();
        List<Object[]> listSearchData = jointVentureService.getCompanyList(companyName,emailId);
        int i=0;
        if(listSearchData.size() > 0){
            for(Object[] searchData : listSearchData){
                sb.append("<tr>"
                            + "<td class='t-align-center' style='display: none;'>"
                                    + "<input type='checkbox'  name='chk"+i+"' id='chk"+i+"' value='"+searchData[0]+"'  checked/>"
                                    + "<input type='hidden' name='companyName"+i+"' id='companyName"+i+"' value='"+searchData[1]+"' />"
                            + "</td>"
                            + "<td>"+searchData[1]+"</td>"
                            + "<td>"+searchData[2]+"<input type='hidden' name='emailId"+i+"' id='emailId"+i+"' value='"+searchData[2]+"' /><input type='hidden' name='userId"+i+"' id='userId"+i+"' value='"+searchData[3]+"' /></td>"
                        + "</tr>");
                i++;
            }
        }else{
            sb.append("<tr><td class='t-align-center' colspan='2'>No Records Found</td></tr>");
        }
        return sb.toString();
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
