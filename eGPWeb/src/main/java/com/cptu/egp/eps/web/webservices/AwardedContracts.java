/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.webservices;

import com.cptu.egp.eps.dao.storedprocedure.SearchNOAData;
import com.cptu.egp.eps.service.serviceimpl.SearchNOAService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.WebServiceUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import org.apache.log4j.Logger;

/**
 * web service for Awarded contracts
 * @author Sreenu.Durga
 */
@WebService()
@XmlSeeAlso({SearchNOAData.class})
public class AwardedContracts {

    private static final Logger LOGGER = Logger.getLogger(AwardedContracts.class);
    private String logUserId = "0";
    private static final int WEB_SERVICE_ID = 1;
    private static final String SEARCH_SERVICE_NAME = "SearchNOAService";

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /***
     * This method gives the lList of String Objects of the <b>AwardedContracts</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1. Ministry, Division, Organization<br>
     * 2. Procuring Entity, States<br>
     * 3. Tender ID,Reference No,Contract No,Advertisement Date <br>
     * 4. Title,Procurement Method <br>
     * 5. Value (In BD Tk.),Date of Notification of Award <b>
     * 6. Contract Award to <br>
     *
     * @param String username
     * @param String password
     * @param String keyword
     * @param Integer ministry_Division_Organization
     * @param String contractDateFrom as dd/mm/yyyy
     * @param String contractDateTo as dd/mm/yyyy
     * @param Integer contractId
     * @param String contractNo
     * @param Integer tenderId
     * @param String tenderReferenceNo
     * @param String contractAmount
     * @param String category
     * @param String stateName
     * @param Integer pageNumber
     * @param Integer pageSize
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getAwardContracts")
    public WSResponseObject getAwardContracts(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password,
            @WebParam(name = "keyword") String keyword,
            @WebParam(name = "ministry_Division_Organization") Integer ministry_Division_Organization,
            @WebParam(name = "contractDateFrom") String contractDateFrom,
            @WebParam(name = "contractDateTo") String contractDateTo,
            @WebParam(name = "contractId") Integer contractId,
            @WebParam(name = "contractNo") String contractNo,
            @WebParam(name = "tenderId") Integer tenderId,
            @WebParam(name = "tenderReferenceNo") String tenderReferenceNo,
            @WebParam(name = "contractAmount") String contractAmount,
            @WebParam(name = "category") String category,
            @WebParam(name = "stateName") String stateName /*,
            @WebParam(name = "pageNumber") Integer pageNumber, Commented by Sudhir 08072011 for no need pagination concept.
            @WebParam(name = "pageSize") Integer pageSize */) {
        LOGGER.debug("getAwardContracts : " + logUserId + WSResponseObject.LOGGER_START);
        //generate search key for log table
        Integer pageNumber = 1;
        Integer pageSize = 100000;    // Added by Sudhir 08072011 for no need pagination concept.

        String searchKey = getAwardedContractsSearchKey(keyword,
                ministry_Division_Organization, contractDateFrom,
                contractDateTo, contractId, contractNo,
                tenderId, tenderReferenceNo, contractAmount,
                category, stateName, pageNumber, pageSize);
        //insert record in log table
        WebServiceUtil.insertLogRecord(WEB_SERVICE_ID, username, searchKey);
        //getting data
        WSResponseObject wSResponseObject = new WSResponseObject();
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WEB_SERVICE_ID,
                SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> requiredList = null;
            try {
                requiredList = getRequiredAwardedContractsList(keyword,
                        ministry_Division_Organization, contractDateFrom, contractDateTo,
                        contractId, contractNo, tenderId, tenderReferenceNo, contractAmount,
                        category, stateName, pageNumber, pageSize);
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getAwardContracts : " + logUserId + e);
            }
            if (requiredList != null) {
                if (requiredList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(requiredList);
                }
            }
        }
        LOGGER.debug("getAwardContracts : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method returns the required list of objects
     * @param keyword
     * @param ministry_Division_Organization
     * @param contractDateFrom
     * @param contractDateTo
     * @param contractId
     * @param contractNo
     * @param tenderId
     * @param tenderReferenceNo
     * @param contractAmount
     * @param category
     * @param stateName
     * @param pageNumber
     * @param pageSize
     * @return List<SearchNOAData> 
     */
    private List<String> getRequiredAwardedContractsList(String keyword, Integer ministry_Division_Organization,
            String contractDateFrom, String contractDateTo, Integer contractId,
            String contractNo, Integer tenderId, String tenderReferenceNo,
            String contractAmount, String category, String stateName, Integer pageNumber,
            Integer pageSize) {
        LOGGER.debug("getRequiredAwardedContractsList : " + logUserId + WSResponseObject.LOGGER_START);
        List<SearchNOAData> noaSearchResultList = new ArrayList<SearchNOAData>();
        List<String> getReqStringList = new ArrayList<String>();
        //getting list data
        int officeIdVal = 0;
        String packageUserID = "";
        List<Object[]> packageDetails;
        String keyWordString = WebServiceUtil.getDefaultStringValue(keyword);
        String contractDateFromString = WebServiceUtil.getDefaultStringValue(contractDateFrom);
        String contractDateToString = WebServiceUtil.getDefaultStringValue(contractDateTo);
        String contractNoString = WebServiceUtil.getDefaultStringValue(contractNo);
        String tenderReferenceNoString = WebServiceUtil.getDefaultStringValue(tenderReferenceNo);
        String contractAmountString = WebServiceUtil.getDefaultStringValue(contractAmount);
        String categoryString = WebServiceUtil.getDefaultStringValue(category); //cpvcode = category
        String stateNameString = WebServiceUtil.getDefaultStringValue(stateName);
        int ministry_Division_OrganizationVal =
                WebServiceUtil.getDefaultIntValue(ministry_Division_Organization);
        int contractIdVal = WebServiceUtil.getDefaultIntValue(contractId);
        int tenderIdVal = WebServiceUtil.getDefaultIntValue(tenderId);
        int pageNumberVal = WebServiceUtil.getDefaultIntValue(pageNumber);
        int pageSizeVal = WebServiceUtil.getDefaultIntValue(pageSize);
        SearchNOAService noaService = (SearchNOAService) AppContext.getSpringBean(SEARCH_SERVICE_NAME);
        //Added 2 parameter as null for Advertisement Date To and Notification of Award Date To by Proshanto Kumar Saha
        noaSearchResultList = noaService.returndata(keyWordString, contractDateFromString,
                contractDateToString, ministry_Division_OrganizationVal, contractIdVal,
                contractNoString, tenderIdVal, tenderReferenceNoString, categoryString,
                contractAmountString, officeIdVal, pageNumberVal, pageSizeVal, stateNameString, "", "", "", "","","");
        if (noaSearchResultList != null) {
            for (SearchNOAData searchNOAData : noaSearchResultList) {
                // modification in all out put by sudhir 20072011
                packageDetails = noaService.getPackageDetailsByTenderId(searchNOAData.getTenderId());
                StringBuilder sbCSV = new StringBuilder();
                sbCSV.append(searchNOAData.getMDADetails());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(searchNOAData.getProcuringEntity());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                // sbCSV.append(searchNOAData.getTenderRefNo()); Comments by shreyansh to pass tender reference no with href
                sbCSV.append("<a href=\"" + XMLReader.getMessage("urltoredirect") + "resources/common/ViewAwardedContracts.jsp?pkgLotId=" + packageDetails.get(0)[1] + "&tenderid=" + searchNOAData.getTenderId() + "&userId=" +packageDetails.get(0)[0] + ">" + searchNOAData.getTenderRefNo() + "</a>");
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(searchNOAData.getKeyword());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(searchNOAData.getNOADate());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(searchNOAData.getContractAmt());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(searchNOAData.getContractAwardedTo());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(searchNOAData.getOfficeId());
                //sbCSV.append(WSResponseObject.APPEND_CHAR);
                //sbCSV.append(searchNOAData.getContractNo());
                //sbCSV.append(WSResponseObject.APPEND_CHAR);   Commended by Sudhir 08072011
                getReqStringList.add(sbCSV.toString());
            }
        }
        LOGGER.debug("getRequiredAwardedContractsList : " + logUserId + WSResponseObject.LOGGER_END);
        return getReqStringList;
    }

    /***
     * This method returns search criteria as a string for logging.
     * @param keyword
     * @param ministry_Division_Organization
     * @param contractDateFrom
     * @param contractDateTo
     * @param contractId
     * @param contractNo
     * @param tenderId
     * @param tenderReferenceNo
     * @param contractAmount
     * @param category
     * @param stateName
     * @param pageNumber
     * @param pageSize
     * @return String searchKey
     */
    private String getAwardedContractsSearchKey(String keyword, Integer ministry_Division_Organization,
            String contractDateFrom, String contractDateTo, Integer contractId,
            String contractNo, Integer tenderId, String tenderReferenceNo,
            String contractAmount, String category, String stateName, Integer pageNumber,
            Integer pageSize) {
        LOGGER.debug("getAwardedContractsSearchKey : " + logUserId + WSResponseObject.LOGGER_START);
        StringBuffer searchString = new StringBuffer();
        if (keyword != null) {
            searchString.append("Keyword:");
            searchString.append(keyword);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (ministry_Division_Organization != null) {
            searchString.append("Ministry_Division_Organization:");
            searchString.append(ministry_Division_Organization);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (contractDateFrom != null) {
            searchString.append("ContractDateFrom:");
            searchString.append(contractDateFrom);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (contractDateTo != null) {
            searchString.append("ContractDateTo:");
            searchString.append(contractDateTo);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (contractId != null) {
            searchString.append("ContractId:");
            searchString.append(contractId);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (contractNo != null) {
            searchString.append("ContractNo:");
            searchString.append(contractNo);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (tenderId != null) {
            searchString.append("TenderId:");
            searchString.append(tenderId);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (tenderReferenceNo != null) {
            searchString.append("TenderReferenceNo:");
            searchString.append(tenderReferenceNo);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (contractAmount != null) {
            searchString.append("ContractAmount:");
            searchString.append(contractAmount);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (category != null) {
            searchString.append("Category:");
            searchString.append(category);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (stateName != null) {
            searchString.append("StateName:");
            searchString.append(stateName);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (pageNumber != null) {
            searchString.append("PageNumber:");
            searchString.append(pageNumber);
            searchString.append(WebServiceUtil.APPEND_CHARACTER);
        }
        if (pageSize != null) {
            searchString.append("PageSize:");
            searchString.append(pageSize);
        }
        LOGGER.debug("getAwardedContractsSearchKey : " + logUserId + WSResponseObject.LOGGER_END);
        return searchString.toString();
    }
    //Commented by Sreenu.
    //Every service has only one search method.
    /***
     * This method gives awarded contracts
     * @param String keyword
     * @param Integer pageNumber
     * @param Integer pageSize
     * @return List<SearchNOAData>
     */
    /*
    @WebMethod(operationName = "getAwardedContracts")
    public List<SearchNOAData> getAwardedContracts(
    @WebParam(name = "keyword") String keyword,
    @WebParam(name = "pageNumber") String pageNumber,
    @WebParam(name = "pageSize") String pageSize) {
    int pgNumber = 0;
    int pgSize = 0;
    //handling number format exception
    try {
    pgNumber = Integer.parseInt(pageNumber);
    pgSize = Integer.parseInt(pageSize);
    } catch (NumberFormatException exception) {
    //return an empty List
    return noaSearchResultList;
    }
    noaSearchResultList = noaService.returndata(keyword, "", "", 0, 0, "", 0,
    "", "", "", 0, pgNumber, pgSize);
    return noaSearchResultList;
    }*/
}
