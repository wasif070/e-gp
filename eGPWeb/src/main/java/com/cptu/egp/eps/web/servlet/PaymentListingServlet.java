/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsInvRemarks;
import com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.web.servicebean.TenderTablesSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class PaymentListingServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
    private static final Logger LOGGER = Logger.getLogger(PaymentListingServlet.class);
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String logUserId = "0";
        String userTypeId = null;
        TenderTablesSrBean beanCommon = new TenderTablesSrBean();
        

        try {
            String funName = request.getParameter("funName");
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                accPaymentService.setLogUserId(logUserId);
            }
            LOGGER.debug("processRequest : "+logUserId+" Starts");
            if("getGeneratedInvoiceListing".equalsIgnoreCase(funName))
            {
                LOGGER.debug("getGeneratedInvoiceListing : "+logUserId+" Starts");
                String styleClass = "";
                String strPageNo = request.getParameter("pageNo");

                int pageNo = Integer.parseInt(strPageNo);
                String strOffset = request.getParameter("size");
                int recordOffset = Integer.parseInt(strOffset);

                if("Processing".equalsIgnoreCase(request.getParameter("status")))
                {
                    List<Object[]> list = null;
                    List<TblCmsInvoiceAccDetails> getinvoicedetails = null;
                    int offecet = ((pageNo - 1) * (recordOffset));
                    list = accPaymentService.getPaymentListing("sendtope",Integer.parseInt(logUserId),offecet,recordOffset);
                    int rowcount = (int)accPaymentService.getPaymentListingCount("sendtope",Integer.parseInt(logUserId));
                   
                    if (list != null && !list.isEmpty()) {
                        for (int i = 0; i < list.size(); i++) {

                            Object[] obj = list.get(i);

                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            String tenderType = beanCommon.getTenderType(Integer.parseInt(obj[0].toString()));
                            String ProcessingViewCase = "";
                            String processingEditViewCase = "";
                             if(tenderType.equals("ICT"))
                            {
                            ProcessingViewCase = "<a href=AccPaymentView.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+"&invoiceNo="+obj[11]+">View</a>";
                            processingEditViewCase = "<a href=AccPayment.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+"&invoiceNo="+obj[11]+">Edit</a>&nbsp;|&nbsp;<a href=AccPaymentView.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+"&invoiceNo="+obj[11]+">View</a>";

                            }
                            else{
                            ProcessingViewCase = "<a href=AccPaymentView.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+">View</a>";
                            processingEditViewCase = "<a href=AccPayment.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+">Edit</a>&nbsp;|&nbsp;<a href=AccPaymentView.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+">View</a>";
                            }
                            String RemarksView = "<a href='javascript:void(0);' onclick=openWin("+obj[0]+","+obj[3]+","+obj[2]+")>View</a>";
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                            out.print("<td class=\"t-align-left\">" + obj[0]+"<br />"+ obj[4]+"</td>");
                            if("1".equals(obj[10].toString())){
                            out.print("<td class=\"t-align-left\">" + obj[8].toString()+" "+obj[9].toString() +"</td>");
                            }else{out.print("<td class=\"t-align-left\">" + obj[7].toString() +"</td>");}
                            out.print("<td class=\"t-align-left\">" + obj[5] + "</td>");
                            out.print("<td class=\"t-align-center\">" + obj[6] + "</td>");  
                            if(obj[11]!=null)
                            {    
                                out.print("<td class=\"t-align-center\">" + obj[11] + "</td>");
                            }else{
                                out.print("<td class=\"t-align-center\">" + "NA" + "</td>");
                            }    
                            getinvoicedetails =  accPaymentService.getInvoiceAccDetails(Integer.parseInt(obj[3].toString()));
                            List<Object> invStatus = accPaymentService.getInvoiceStatus(Integer.parseInt(obj[3].toString()));
                            if(!getinvoicedetails.isEmpty())
                            {
                                if(!invStatus.isEmpty())
                                {
                                    if("remarksbype".equalsIgnoreCase(invStatus.get(0).toString()))
                                    {
                                        out.print("<td class=\"t-align-center\">" + RemarksView + "</td>");
                                        out.print("<td class=\"t-align-center\">" + processingEditViewCase + "</td>");
                                    }
                                    else
                                    {
                                        out.print("<td class=\"t-align-center\">" + " - " + "</td>");
                                        out.print("<td class=\"t-align-center\">" + ProcessingViewCase + "</td>");
                                    }
                                }

                                
                            }                           
                            out.print("</tr>");
                        }
                        int totalPages = 1;
                        if (rowcount > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(strOffset));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    }else {
                        out.print("<tr>");

                        out.print("<td colspan=\"8\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }
                }
                else if("Processed".equalsIgnoreCase(request.getParameter("status")))
                {
                    List<Object[]> list = null;
                    List<TblCmsInvoiceAccDetails> getinvoicedetails = null;
                    int offecet = ((pageNo - 1) * (recordOffset));
                    list = accPaymentService.getPaymentListing("sendtotenderer",Integer.parseInt(logUserId),offecet,recordOffset);
                    int rowcount = (int)accPaymentService.getPaymentListingCount("sendtotenderer",Integer.parseInt(logUserId));
                    if (list != null && !list.isEmpty()) {
                        for (int i = 0; i < list.size(); i++) {

                            Object[] obj = list.get(i);

                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            String tenderType = beanCommon.getTenderType(Integer.parseInt(obj[0].toString()));
                            String ProcessedViewCase = "";
                            
                            if(tenderType.equals("ICT"))
                                      ProcessedViewCase = "<a href=AccPaymentView.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+"&invoiceNo="+obj[11]+">View</a>";
                            else
                                      ProcessedViewCase = "<a href=AccPaymentView.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+">View</a>";
                            String RemarksView = "<a href='javascript:void(0);' onclick=openWin("+obj[0]+","+obj[3]+","+obj[2]+","+obj[12]+")>View</a>";
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                            out.print("<td class=\"t-align-left\">" + obj[0]+"<br />"+ obj[4]+"</td>");
                            if("1".equals(obj[10].toString())){
                            out.print("<td class=\"t-align-left\">" + obj[8].toString()+" "+obj[9].toString() +"</td>");
                            }else{out.print("<td class=\"t-align-left\">" + obj[7].toString() +"</td>");}
                            out.print("<td class=\"t-align-left\">" + obj[5] + "</td>");
                            out.print("<td class=\"t-align-center\">" + obj[6] + "</td>");
                            if(obj[11]!=null)
                            {    
                                out.print("<td class=\"t-align-center\">" + obj[11] + "</td>");
                            }else{
                                out.print("<td class=\"t-align-center\">" + "NA" + "</td>");
                            }
                            List<TblCmsInvRemarks> RemarksDetails = accPaymentService.getRemarksDetails(Integer.parseInt(obj[3].toString()));
                            if(!RemarksDetails.isEmpty())
                            {
                                out.print("<td class=\"t-align-center\">" + RemarksView + "</td>");
                            }else
                            {
                                out.print("<td class=\"t-align-center\">" + "-" + "</td>");
                            }
                            getinvoicedetails =  accPaymentService.getInvoiceAccDetails(Integer.parseInt(obj[3].toString()));
                            if(!getinvoicedetails.isEmpty())
                            {
                                out.print("<td class=\"t-align-center\">" + ProcessedViewCase + "</td>");
                            }
                            else
                            {
                                out.print("<td class=\"t-align-center\">" + " - " + "</td>");
                            }
                            out.print("</tr>");
                        }
                        int totalPages = 1;
                        if (rowcount > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(strOffset));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    }else {
                        out.print("<tr>");

                        out.print("<td colspan=\"8\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }
                }
                else
                {
                    List<Object[]> list = null;
                    List<TblCmsInvoiceAccDetails> getinvoicedetails = null;
                    int offecet = ((pageNo - 1) * (recordOffset));
                     String PendingAction ="";
                     String PendingViewCase ="";
                    list = accPaymentService.getPaymentListing("acceptedbype",Integer.parseInt(logUserId),offecet,recordOffset);
                    int rowcount = (int)accPaymentService.getPaymentListingCount("acceptedbype",Integer.parseInt(logUserId));
                    if (list != null && !list.isEmpty()) {
                        for (int i = 0; i < list.size(); i++) {

                            Object[] obj = list.get(i);

                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }
                            String tenderType = beanCommon.getTenderType(Integer.parseInt(obj[0].toString()));
                            if(tenderType.equals("ICT"))
                            {
                            PendingAction = "<a href=AccPayment.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+"&invoiceNo="+obj[11]+">Process</a>";
                            PendingViewCase = "<a href=AccPayment.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+"&invoiceNo="+obj[11]+">Edit</a>&nbsp;|&nbsp;<a href=AccPaymentView.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+"&invoiceNo="+obj[11]+">View</a>";

                            }
                            else
                            {
                            PendingAction = "<a href=AccPayment.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+">Process</a>";
                            PendingViewCase = "<a href=AccPayment.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+">Edit</a>&nbsp;|&nbsp;<a href=AccPaymentView.jsp?tenderId="+obj[0]+"&InvoiceId="+obj[3]+"&wpId="+obj[2]+"&lotId="+obj[1]+"&cntId="+obj[12]+">View</a>";
                            }

                                                   out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                            out.print("<td class=\"t-align-left\">" + obj[0]+"<br />"+ obj[4]+"</td>");
                            if("1".equals(obj[10].toString())){
                            out.print("<td class=\"t-align-left\">" + obj[8].toString()+" "+obj[9].toString() +"</td>");
                            }else{out.print("<td class=\"t-align-left\">" + obj[7].toString() +"</td>");}
                            out.print("<td class=\"t-align-left\">" + obj[5] + "</td>");                            
                            out.print("<td class=\"t-align-center\">" + obj[6] + "</td>");
                            if(obj[11]!=null)
                            {    
                                out.print("<td class=\"t-align-center\">" + obj[11] + "</td>");
                            }else{
                                out.print("<td class=\"t-align-center\">" + "NA" + "</td>");
                            }
                            out.print("<td class=\"t-align-center\">" + "-" + "</td>");
                            getinvoicedetails =  accPaymentService.getInvoiceAccDetails(Integer.parseInt(obj[3].toString()));
                            if(!getinvoicedetails.isEmpty())
                            {
                                out.print("<td class=\"t-align-center\">" + PendingViewCase + "</td>");
                            }
                            else
                            {
                                out.print("<td class=\"t-align-center\">" + PendingAction + "</td>");
                            }
                            out.print("</tr>");
                        }
                        int totalPages = 1;                        
                        if (rowcount > 0) {
                            int cc = (int) rowcount;
                            totalPages = (int) Math.ceil(Math.ceil(cc) / Integer.parseInt(strOffset));
                            if (totalPages == 0) {
                                totalPages = 1;
                            }
                        }
                        out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                    }else {
                        out.print("<tr>");

                        out.print("<td colspan=\"8\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                        out.print("</tr>");
                    }
                }
                LOGGER.debug("getGeneratedInvoiceListing : "+logUserId+" Ends");
            }
            else if("sdf".equalsIgnoreCase(funName))
            {
             LOGGER.debug("viewArchiveNewsEventList : "+logUserId+" Ends");
             LOGGER.debug("viewArchiveNewsEventList : "+logUserId+" Ends");
            }
           LOGGER.debug("processRequest : "+logUserId+" Starts");
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
