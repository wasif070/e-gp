/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCmsWcCertiHistory;
import com.cptu.egp.eps.model.table.TblCmsWcCertificate;
import com.cptu.egp.eps.service.serviceinterface.CmsWcCertificateService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsWcCertificateServiceBean {

    final Logger logger = Logger.getLogger(CmsWcCertificateServiceBean.class);
    private final static CmsWcCertificateService cmsWcCertificateService =
            (CmsWcCertificateService) AppContext.getSpringBean("CmsWcCertificateService");
    
    private String logUserId = "0";
    private static final String LOGGERStart = " Starts";
    private static final String LOGGEREnd = " Ends";

    public void setLogUserId(String logUserId) {
        cmsWcCertificateService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /***
     * This method inserts a TblCmsWcCertificate object in database
     * @param tblCmsWcCertificate
     * @return int
     */
    public int insertCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate) {
        logger.debug("insertCmsWcCertificate : " + logUserId + LOGGERStart);
        int id = 0;
        try {
            id = cmsWcCertificateService.insertCmsWcCertificate(tblCmsWcCertificate);
        } catch (Exception e) {
            logger.error("insertCmsWcCertificate : " + logUserId + " : " + e);
        }
        logger.debug("insertCmsWcCertificate : " + logUserId + LOGGEREnd);
        return id;

    }

    /****
     * This method updates an TblCmsWcCertificate object in DB
     * @param tblCmsWcCertificate
     * @return boolean
     */
    public boolean updateCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate) {
        logger.debug("updateCmsWcCertificate : " + logUserId + LOGGERStart);
        boolean isUpdate = false;
        try {
            isUpdate = cmsWcCertificateService.updateCmsWcCertificate(tblCmsWcCertificate);
        } catch (Exception e) {
            logger.error("updateCmsWcCertificate : " + logUserId + " : " + e);
        }
        logger.debug("updateCmsWcCertificate : " + logUserId + LOGGEREnd);
        return isUpdate;

    }

    /****
     * This method deletes an TblCmsWcCertificate object in DB
     * @param tblCmsWcCertificate
     * @return boolean
     */
    public boolean deleteCmsWcCertificate(TblCmsWcCertificate tblCmsWcCertificate) {
        logger.debug("deleteCmsWcCertificate : " + logUserId + LOGGERStart);
        boolean isDeleted = false;
        try {
            isDeleted = cmsWcCertificateService.deleteCmsWcCertificate(tblCmsWcCertificate);
        } catch (Exception e) {
            logger.error("deleteCmsWcCertificate : " + logUserId + " : " + e);
        }
        logger.debug("deleteCmsWcCertificate : " + logUserId + LOGGEREnd);
        return isDeleted;

    }

    /****
     * This method returns all TblCmsWcCertificate objects from database
     * @return List<TblCmsWcCertificate>
     */
    public List<TblCmsWcCertificate> getAllCmsWcCertificate() {
        logger.debug("getAllCmsWcCertificate : " + logUserId + LOGGERStart);
        List<TblCmsWcCertificate> cmsWcCertificateList = new ArrayList<TblCmsWcCertificate>();
        try {
            cmsWcCertificateList = cmsWcCertificateService.getAllCmsWcCertificate();
        } catch (Exception e) {
            logger.error("getAllCmsWcCertificate : " + logUserId + " : " + e);
        }
        logger.debug("getAllCmsWcCertificate : " + logUserId + LOGGEREnd);
        return cmsWcCertificateList;

    }

    /***
     * This method returns no. of TblCmsWcCertificate objects from database
     * @return long
     */
    public long getCmsWcCertificateCount() {
        logger.debug("getCmsWcCertificateCount : " + logUserId + LOGGERStart);
        long count = 0;
        try {
            count = cmsWcCertificateService.getCmsWcCertificateCount();
        } catch (Exception e) {
            logger.error("getCmsWcCertificateCount : " + logUserId + " : " + e);
        }
        logger.debug("getCmsWcCertificateCount : " + logUserId + LOGGEREnd);
        return count;

    }

    /***
     * This method returns TblCmsWcCertificate for the given wcCertId
     * @param int wcCertId
     * @return TblCmsWcCertificate
     */
    public TblCmsWcCertificate getCmsWcCertificate(int wcCertId) {
        logger.debug("getCmsWcCertificate : " + logUserId + LOGGERStart);
        TblCmsWcCertificate tblCmsWcCertificate = new TblCmsWcCertificate();
        try {
            tblCmsWcCertificate = cmsWcCertificateService.getCmsWcCertificate(wcCertId);
        } catch (Exception e) {
            logger.error("getCmsWcCertificate : " + logUserId + " : " + e);
        }
        logger.debug("getCmsWcCertificate : " + logUserId + LOGGEREnd);
        return tblCmsWcCertificate;
    }

    /***
     * This method gives the TblCmsWcCertificate object for the given contractSignId
     * @param contractSignId
     * @return TblCmsWcCertificate
     */
    public TblCmsWcCertificate getCmsWcCertificateForContractSignId(int contractSignId) {
        logger.debug("getCmsWcCertificateForContractSignId : " + logUserId + LOGGERStart);
        TblCmsWcCertificate tblCmsWcCertificate = new TblCmsWcCertificate();
        try {
            tblCmsWcCertificate = cmsWcCertificateService.getCmsWcCertificateForContractSignId(contractSignId);
        } catch (Exception e) {
            logger.error("getCmsWcCertificateForContractSignId : " + logUserId + " : " + e);
        }
        logger.debug("getCmsWcCertificateForContractSignId : " + logUserId + LOGGEREnd);
        return tblCmsWcCertificate;
    }
    
    /***
     * This method inserts a TblCmsWcCertificateHistory object in database
     * @param tblCmsWcCertificate
     * @return int
     */
    public int insertCmsWcCertihistory(TblCmsWcCertiHistory tblCmsWcCertiHistory) {
        logger.debug("insertCmsWcCertihistory : " + logUserId + LOGGERStart);
        int id = 0;
        try {
            id = cmsWcCertificateService.insertCmsWcCertihistory(tblCmsWcCertiHistory);
        } catch (Exception e) {
            logger.error("insertCmsWcCertihistory : " + logUserId + " : " + e);
        }
        logger.debug("insertCmsWcCertihistory : " + logUserId + LOGGEREnd);
        return id;

    }
    
    public int getMaxHistoryCount(int WcCertId){
        logger.debug("insertCmsWcCertihistory : " + logUserId + LOGGERStart);
        int id = 0;
        try {
            id = cmsWcCertificateService.getMaxHistoryCount(WcCertId);
        } catch (Exception e) {
            logger.error("insertCmsWcCertihistory : " + logUserId + " : " + e);
        }
        logger.debug("insertCmsWcCertihistory : " + logUserId + LOGGEREnd);
        return id;
    }
    
     public List<Object[]> getWcCetificateDetails(int wcCertiId){
         return cmsWcCertificateService.getWcCetificateDetails(wcCertiId);
     }
     
     public List<Object[]> getWcCetificateHistoryDetails(int wcCertHistId){
         return cmsWcCertificateService.getWcCetificateHistoryDetails(wcCertHistId);
     }
}
