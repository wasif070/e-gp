/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.web.Charts.ChartServices;
import java.io.PrintWriter;
import java.math.BigDecimal;
import javax.servlet.http.HttpSession;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.servlet.ServletUtilities;

/**
 *
 * @author shreyansh Jogi
 */
public class GenerateChart {

    ChartServices chart = new ChartServices();
    ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());

    public String genPieChartForPE(HttpSession session, PrintWriter pw, BigDecimal processedbyPE,BigDecimal CV,BigDecimal percent) {
        String str = "";
        try {
            str = ServletUtilities.saveChartAsPNG(chart.PieChartForPE(processedbyPE,CV,percent), 400, 300, info, session);
            ChartUtilities.writeImageMap(pw, str, info, false);
            pw.flush();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return str;
    }

    public String genPieChartForTen(HttpSession session, PrintWriter pw, BigDecimal processedbyTen,BigDecimal CV,BigDecimal percent) {
        String str = "";
        try {
            ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
            str = ServletUtilities.saveChartAsPNG(chart.PieChartForTen(processedbyTen,CV,percent), 400, 300, info, session);
            ChartUtilities.writeImageMap(pw, str, info, false);
            pw.flush();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return str;
    }

    public String genPieChartForAcc(HttpSession session, PrintWriter pw, BigDecimal processedbyAcc,BigDecimal CV,BigDecimal percent) {
        String str = "";
        try {
            str = ServletUtilities.saveChartAsPNG(chart.PieChartForAcc(processedbyAcc,CV,percent), 400, 300, info, session);
            ChartUtilities.writeImageMap(pw, str, info, false);
            pw.flush();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return str;
    }
    public String BarChart(HttpSession session, PrintWriter pw,int tenderId,int lotId) {
        String str = "";
        try {
            str = ServletUtilities.saveChartAsPNG(chart.Barchart(tenderId,lotId), 800, 600, info, session);
            ChartUtilities.writeImageMap(pw, str, info, false);
            pw.flush();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return str;
    }
    public String GanttChart(HttpSession session, PrintWriter pw,int formMapId) {
        String str = "";
        try {
            str = ServletUtilities.saveChartAsPNG(chart.GanttChart(formMapId) , 800, 600, info, session);
            ChartUtilities.writeImageMap(pw, str, info, false);
            pw.flush();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return str;
    }
    public String LineChart(HttpSession session, PrintWriter pw,int formMapId,String formType) {
        String str = "";
        try {

            str = ServletUtilities.saveChartAsPNG(chart.LineChart(formMapId,formType) , 400, 300, info, session);
            ChartUtilities.writeImageMap(pw, str, info, false);
            pw.flush();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return str;
    }
}
