/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.MsgListing;
import com.cptu.egp.eps.model.table.TblMessageDocument;
import com.cptu.egp.eps.service.serviceimpl.AddTaskServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.MessageProcessingServiceImpl;
import com.cptu.egp.eps.web.databean.ViewMessageDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Chalapathi.Bavisetti
 */
public class MessageProcessSrBean {

    final Logger logger = Logger.getLogger(MessageProcessSrBean.class);
    private String logUserId ="0";
    
    // here getting service object through spring container
    // which injects all dependencies regarding that service
    MessageProcessingServiceImpl messageProcServiceImpl =
           (MessageProcessingServiceImpl)AppContext.getSpringBean("messageprocessService");
    AddTaskServiceImpl taskServiceImpl = (AddTaskServiceImpl)AppContext.getSpringBean("AddTaskService");

    private static String messageBoxType;
    private AuditTrail auditTrail;

    /**
     *
     * @param auditTrail
     */
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        messageProcServiceImpl.setAuditTrail(auditTrail);
    }


    /**
     * @return the messageBoxType
     */
    public static String getMessageBoxType() {
        return messageBoxType;
    }

    /**
     *
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        messageProcServiceImpl.setLogUserId(logUserId);
        taskServiceImpl.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }
     
    /**
     * @param aMessageBoxType the messageBoxType to set
     */
    public static void setMessageBoxType(String aMessageBoxType) {
        messageBoxType = aMessageBoxType;
    }

    /**
     * To get data based on messageboxtype like(Inbox,Sent,Draft,Trash)
     * @param Type
     * @param id
     * @param MessageType
     * @param totalPages
     * @param pageNumber
     * @param noOfRecords
     * @param folderId
     * @param searching
     * @param keywords
     * @param emialId
     * @param dateFrom
     * @param dateTo
     * @param viewMsg
     * @param msgid
     * @param columnName
     * @param order
     * @param sortOpt
     * @return it returns msgListing List
     */
    public List<MsgListing> getInboxData(String Type,int id,
            String MessageType,Integer totalPages,Integer pageNumber,
            Integer noOfRecords,Integer folderId,String searching,String keywords,
            String emialId,String dateFrom,String dateTo,String viewMsg,Integer msgid,
            String columnName,String order,String sortOpt){
        
          logger.debug("getInboxData : "+logUserId+" Starts");
           List messData = null;

          try{              
          messData =  messageProcServiceImpl.getInboxData(id, Type, MessageType,
                  noOfRecords, pageNumber, totalPages,folderId,searching,keywords,
            emialId,dateFrom,dateTo,viewMsg,msgid,columnName,order,sortOpt);
          
          }catch(Exception ex){
            logger.error("getInboxData : "+logUserId+" : "+ex.toString());
          }
          logger.debug("getInboxData : "+logUserId+" Ends");
          return messData;
    }

    /**
     * To get Folders data by loginuserid
     * @param uid current login userid
     * @return list of folders based on userid
     */
    public List getFolderDataByUserId(int uid) {
        logger.debug("getFolderDataByUserId : "+logUserId+" Starts");
        List folderData = null;
        try{
            folderData = messageProcServiceImpl.getFolderDataByUserid(uid);
        }catch(Exception ex){
            logger.error("getFolderDataByUserId : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getFolderDataByUserId : "+logUserId+" Ends");
        return folderData;
    }


    
    /**
     * gives to do list data
     * @param uid
     * @return list of data
     */
    public List getToDoList(int uid) {
        logger.debug("chkMailId : "+logUserId+" Starts");
        List list = null;
        try {
            list = taskServiceImpl.getPendingToDoList(uid);
        } catch (Exception ex) {
             logger.error("chkMailId : "+logUserId+" : "+ex.toString());
        }
        logger.debug("chkMailId : "+logUserId+" Ends");
        return list;
    }

    /**
     * To compose mail or Editing Draft and view message
     * @param id
     * @param to
     * @param from
     * @param cc
     * @param sub
     * @param priority
     * @param mstxt
     * @param procURL
     * @param msfBoxType
     * @param action
     * @param msgStatus
     * @param folderid
     * @param msgReadStatus
     * @param isMsgReply
     * @param msgDocId
     * @param updmsgId
     * @param messageInboxId
     * @return it returns curent mail message
     */
    public List composeMail(int id,String to,String from ,String cc,String sub,String priority,
            String mstxt,String procURL,String msfBoxType,String action,
            String msgStatus,Integer folderid,String msgReadStatus,String isMsgReply,
            String msgDocId,Integer updmsgId,Integer messageInboxId){
        List status = new ArrayList();
        logger.debug("composeMail : "+logUserId+" Starts");
        try{
        status =  messageProcServiceImpl.composeEmail(id,to,from, cc, sub, priority, mstxt,
                   procURL,msfBoxType,action,msgStatus,folderid,msgReadStatus,isMsgReply,
                   msgDocId,updmsgId,messageInboxId);
        }catch(Exception ex){
            logger.error("composeMail : "+logUserId+" : "+ex.toString());
        }
        logger.debug("composeMail : "+logUserId+" Ends");
          return status;
    }

    /**
     *
     * @param msgInboxList
     * @param msgBoxType
     * @return
     */
    public List convertToList(List<MsgListing> msgInboxList,String msgBoxType)
    {
        logger.debug("convertToList : "+logUserId+" Starts");
        List list = new ArrayList();
        try{
        DateFormat formatter;
        for (MsgListing msglisting : msgInboxList) {
            if(msgBoxType.equals("Sent"))
                list.add(msglisting.getToMailId());
            else list.add(msglisting.getFromMailId());
            if(!msgBoxType.equals("Draft"))
            list.add("<a target='_blank' href=ViewMessage.jsp?messageid="+msglisting.getMsgId()+"&messageBoxType="+msgBoxType+"pre&messageInboxid="+msglisting.getMsgBoxId()+">"+msglisting.getSubject()+"</a>");
            else list.add(msglisting.getSubject());
            list.add(msglisting.getIsPriority());
             formatter = new SimpleDateFormat("dd/MM/yy");
             String str = formatter.format(msglisting.getCreationDate());
            list.add(str);
                if(msgBoxType.equals("Draft")){
            list.add("<a target='_blank' href=ComposeMail.jsp?messageid="+msglisting.getMsgId()+"&messageBoxType="+msgBoxType+">Edit</a>");
        }
                
            }
        }catch(Exception ex){
            logger.error("convertToList : "+logUserId+" : "+ex.toString());
        }
        logger.debug("convertToList : "+logUserId+" Ends");
        return list;
    }
    /**
     *
     * @param list
     * @param para
     * @return
     * @throws JSONException
     */
    public JSONArray getJSONArray(List list,String para[]) throws JSONException
    {
        logger.debug("getJSONArray : "+logUserId+" Starts");
        JSONArray jSONArray = new JSONArray();
        try{
        for (int j = 0; j < (list.size() / para.length); j++) {
            JSONObject jSONObject = new JSONObject();
            for (int i = 0; i < para.length; i++) {
                int c = (para.length * j) + i;
                jSONObject.put(para[i], list.get(c));
            }
            jSONArray.put(jSONObject);
        }
        }catch(Exception ex){
            logger.error("getJSONArray : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getJSONArray : "+logUserId+" Ends");
        return jSONArray;
    }

    /**
     * adding listdata to viewMessageDtBean pojo
     * to show view message on page
     * @param listData
     * @param viewMessageDtBean
     * @return
     */
    public ViewMessageDtBean addToViewMessageDtBean(List listData,ViewMessageDtBean viewMessageDtBean) {

      logger.debug("addToViewMessageDtBean : "+logUserId+" Starts");
      
      Iterator it = listData.iterator();
      StringBuffer fromsb = new StringBuffer();
      StringBuffer tosb = new StringBuffer();
      StringBuffer ccsb = new StringBuffer();
      try{
      while(it.hasNext()){
         MsgListing msgListing = (MsgListing) it.next();
         viewMessageDtBean.setViewMsgDate(msgListing.getCreationDate());
         viewMessageDtBean.setSubject(msgListing.getSubject());
         viewMessageDtBean.setTextArea(msgListing.getMsgText());
         viewMessageDtBean.setPriority(msgListing.getIsPriority());
         String from = checkDuplicate(msgListing,fromsb,"from");
             if(!from.equals("duplicate")){
         fromsb.append(from+",");
             }
         if(msgListing.getMsgToCCReply().equalsIgnoreCase("to")){
         String to = checkDuplicate(msgListing, tosb,"to");
                if(!to.equals("duplicate")){
          tosb.append(to+",");
                 }
          }else if(msgListing.getMsgToCCReply().equalsIgnoreCase("cc")){
         String cc = checkDuplicate(msgListing, ccsb,"cc");
              if(!cc.equals("duplicate")){
              ccsb.append(cc+","); }
         }
      }
      if(fromsb.length()>0) {
       fromsb.deleteCharAt(fromsb.length()-1);
        }
      if(tosb.length()>0){
       tosb.deleteCharAt(tosb.length()-1);
       viewMessageDtBean.setToCcReply("to");
        }
      if(ccsb.length()>0){
       ccsb.deleteCharAt(ccsb.length()-1);
       viewMessageDtBean.setToCcReply("cc");
        }


       viewMessageDtBean.setFrom(fromsb.toString());
       viewMessageDtBean.setTo(tosb.toString());
       viewMessageDtBean.setCc(ccsb.toString());

      }catch(Exception ex){
          logger.error("addToViewMessageDtBean : "+logUserId+" : "+ex.toString());
      }
       logger.debug("addToViewMessageDtBean : "+logUserId+" Ends");
       return viewMessageDtBean;
  }


  /**
   * checking multiple mails at a time
   * @param listing
   * @param sb
   * @param action
   * @return
   */
  public String checkDuplicate(MsgListing listing,StringBuffer sb,String action) {
      String returnstring = null;
      logger.debug("checkDuplicate : "+logUserId+" Starts");
      try{
             if(action.equalsIgnoreCase("from")){
          returnstring = listing.getFromMailId();
              } else{
          returnstring = listing.getToMailId();
                  }
         if(sb.length()>0) {
              String str =  sb.toString();
              String[] sarray = str.split(",");
              boolean check = false;
              for(int i=0;i<sarray.length;i++) {
                 if(returnstring.equals(sarray[i])) {
                     check = true;
                 }
              }
                  if(check){
                      returnstring = "duplicate";
                  }
             }
      }catch(Exception ex){
          logger.error("checkDuplicate : "+logUserId+" : "+ex.toString());
      }
      logger.debug("checkDuplicate : "+logUserId+" Ends");
            return returnstring;
         }
  /**
   * it adds message document data to the database
   * @param tblMessageDocumetn
   */
  public void addMessageDocument(TblMessageDocument tblMessageDocumetn){
      logger.debug("addMessageDocument : "+logUserId+" Starts");
      try{
               messageProcServiceImpl.addMessageDocument(tblMessageDocumetn);
      }catch(Exception ex){
        logger.error("addMessageDocument : "+logUserId+" : "+ex.toString());
     }
      logger.debug("addMessageDocument : "+logUserId+" Ends");
  }

  /**
   * gives email id
   * @param uid
   * @return list
   */
  public List getLoginmailIdByuserId(int uid){
      logger.debug("getLoginmailIdByuserId : "+logUserId+" Starts");
      List loginMastor = null;
      try{
        loginMastor =  messageProcServiceImpl.getLoginmailIdByuserId(uid);
      }catch(Exception ex){
          logger.error("getLoginmailIdByuserId : "+logUserId+" : "+ex.toString());
      }
      logger.debug("getLoginmailIdByuserId : "+logUserId+" Ends");
        return loginMastor;
  }

  /**
   * gives updated read message count
   * @param sqlquery
   * @return integer, return 0 - if data is not exist otherwise return else count
   */
  public int updateReadMessage(String sqlquery){
        logger.debug("updateReadMessage : "+logUserId+" Starts");
         int ret = 0;
        try{
        ret =  messageProcServiceImpl.updateReadMessage(sqlquery);
        }catch(Exception ex){
             logger.error("updateReadMessage : "+logUserId+" : "+ex.toString());
        }
        logger.debug("updateReadMessage : "+logUserId+" Ends");
         return ret;
    }

  /**
   * gives total recieved message data
   * @param from
   * @param where
   * @return long, return 0 - if data is not exist otherwise return else count
   */
  public long getTotalRecievedMsgs(String from , String where){
       logger.debug("getTotalRecievedMsgs : "+logUserId+" Starts");
        long ret = 0;
        try{
        ret =  messageProcServiceImpl.getTotalRecievedMsgs(from, where);
        }catch(Exception ex){
            logger.error("getTotalRecievedMsgs : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getTotalRecievedMsgs : "+logUserId+" Ends");
         return ret;
   }

   /**
    * gives total unread message data
    * @param from
    * @param where
    * @return long, return 0 - if data is not exist otherwise return else count
    */
   public long getTotalUnreadMsgs(String from , String where){
        logger.debug("getTotalUnreadMsgs : "+logUserId+" Starts");
        long ret = 0;
        try{
        ret =  messageProcServiceImpl.getTotalUnreadMsgs(from, where);
        }catch(Exception ex){
            logger.error("getTotalUnreadMsgs : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getTotalUnreadMsgs : "+logUserId+" Ends");
         return ret;
   }

    /**
     * Generate Default Archive Folder for all user who does not have generated Archive Folder.
     * @param userId = tbl_MessageFolder.userId
     * @return = tbl_MessageFolder.msgFolderId
     */
   public int createDefaultArchiveFolder(int userId)
   {
       logger.debug("createDefaultArchiveFolder : "+logUserId+" Starts");
        int folderId=0;
        try{
        folderId =  messageProcServiceImpl.createDefaultArchiveFolder(userId); 
        }catch(Exception ex){
            logger.error("createDefaultArchiveFolder : "+logUserId+" : "+ex.toString());
        }
        logger.debug("createDefaultArchiveFolder : "+logUserId+" Ends");
         return folderId;
   }
}
