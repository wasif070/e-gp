/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.model.table.TblStateMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.PEOfficeCreationService;
import com.cptu.egp.eps.web.databean.PEOfficeCreationDtBean;
import static com.cptu.egp.eps.web.servicebean.TendererMasterSrBean.logger;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;


/**
 *
 * @author rishita
 */
public class PEOfficeCreationSrBean {

    private CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
    private PEOfficeCreationService peOfficeCreationService = (PEOfficeCreationService) AppContext.getSpringBean("PEOfficeCreationService");
    List<SelectItem> countryList = new ArrayList<SelectItem>();
    List<SelectItem> subDistrictList = new ArrayList<SelectItem>();
    List<SelectItem> subDistrictList2 = new ArrayList<SelectItem>();
    List<SelectItem> subDistrictList4 = new ArrayList<SelectItem>();
    final static Logger LOGGER = Logger.getLogger(PEOfficeCreationSrBean.class);
    private String logUserId = "0";
    private static final String START = " Starts";
    private static final String END = " Ends";
    private AuditTrail auditTrail;

    
    
    
    
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        peOfficeCreationService.setAuditTrail(auditTrail);
    }

    
    public List<SelectItem> getSubdistrictList(String stateName) {
        logger.debug("getSub-districtList : " + logUserId + START);
        subDistrictList.clear();
        for (TblDepartmentMaster subdistrictMaster : commonService.getSubdistrictByStateId(stateName)) {
            subDistrictList.add(new SelectItem(subdistrictMaster.getDepartmentName(), subdistrictMaster.getDepartmentName()));
        }
        logger.debug("getSub-districtList : " + logUserId + END);
        return subDistrictList;
    }
    public List<SelectItem> getSubdistrictList2(String stateName) {
        logger.debug("getSub-districtList : " + logUserId + START);
        subDistrictList2.clear();
        for (TblDepartmentMaster subdistrictMaster : commonService.getSubdistrictByStateId2(stateName)) {
            subDistrictList2.add(new SelectItem(subdistrictMaster.getDepartmentName(), subdistrictMaster.getDepartmentName()));
        }
        logger.debug("getSub-districtList : " + logUserId + END);
        return subDistrictList2;
    }
    public List<SelectItem> getSubdistrictList4(String stateName) {
        logger.debug("getSub-districtList : " + logUserId + START);
        subDistrictList4.clear();
        for (TblDepartmentMaster subdistrictMaster : commonService.getSubdistrictByStateId4(stateName)) {
            subDistrictList4.add(new SelectItem(subdistrictMaster.getDepartmentName(), subdistrictMaster.getDepartmentName()));
        }
        logger.debug("getSub-districtList : " + logUserId + END);
        return subDistrictList4;
    }
    
    
    
    
    
    /**
     * get departmentId
     * @return departmentId
     */
    
    public short getDzongkhagId(String dzongkhag) throws Exception {
        //LOOGER.debug("getDzongkhagId : " + logUserId + " Starts");
        short i = 0;
        try {
            i = peOfficeCreationService.getDzongkhagId(dzongkhag);
        } catch (Exception ex) {
            //LOOGER.error("getDepartmentIdByUserId : " + logUserId + ex);
    }
        //LOOGER.debug("getDepartmentIdByUserId : " + logUserId + " Ends");
        return i;
    }
    
    
    
    
    
    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        commonService.setUserId(logUserId);
        peOfficeCreationService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * get countries list
     * @return list of countries
     */
    public List<SelectItem> getCountryList() {
        LOGGER.debug("getCountryList : " + logUserId + START);
        if (countryList.isEmpty()) {
            try {
                for (TblCountryMaster tblCountryMaster : commonService.countryMasterList()) {
                    countryList.add(new SelectItem(tblCountryMaster.getCountryId(), tblCountryMaster.getCountryName()));
                }
            } catch (Exception ex) {
                LOGGER.error("getCountryList : " + logUserId + ex);
            }
        }
        LOGGER.debug("getCountryList : " + logUserId + END);
        return countryList;
    }

    public void setCountryList(List<SelectItem> countryList) {
        this.countryList = countryList;
    }

    
    /**
     * Create PE Office (Ministry,Division,Organization)
     * @param peOfficeCreationDtBean
     * @return true if PE Office created else false
     * @throws UnsupportedEncodingException
     */
    public boolean peOfficeCreation(PEOfficeCreationDtBean peOfficeCreationDtBean) throws UnsupportedEncodingException {
        LOGGER.debug("peOfficeCreation : " + logUserId + START);
        boolean flag = false;
        try {
        TblOfficeMaster tblOMaster = _toTblOfficeMaster(peOfficeCreationDtBean);
            tblOMaster.setCreatedBy(1);
            tblOMaster.setCreationDate(new Date());
            tblOMaster.setStatus("Approved");
            tblOMaster.setTblCountryMaster(new TblCountryMaster(peOfficeCreationDtBean.getCountryId()));
            tblOMaster.setTblStateMaster(new TblStateMaster(peOfficeCreationDtBean.getStateId()));
            if (!(peOfficeCreationDtBean.getDeptBanglaString() == null || peOfficeCreationDtBean.getDeptBanglaString().trim().equals(""))) {
                tblOMaster.setOfficeNameInBangla(peOfficeCreationDtBean.getDeptBanglaString().getBytes("UTF-8"));
            }
            if (peOfficeCreationDtBean.getFaxNo() == null || peOfficeCreationDtBean.getFaxNo().trim().equals("")) {
                tblOMaster.setFaxNo("");
            }
            if (peOfficeCreationDtBean.getCity() == null || peOfficeCreationDtBean.getCity().trim().equals("")) {
                tblOMaster.setCity("");
            }
            if (peOfficeCreationDtBean.getPostCode() == null || peOfficeCreationDtBean.getPostCode().trim().equals("")) {
                tblOMaster.setPostCode("");
            }
            if (peOfficeCreationDtBean.getPeCode() == null || peOfficeCreationDtBean.getPeCode().trim().equals("")) {
                tblOMaster.setPeCode("");
            }
            if (peOfficeCreationDtBean.getUpjilla() == null || peOfficeCreationDtBean.getUpjilla().trim().equals("")) {
            tblOMaster.setUpjilla("");
            }
            tblOMaster.setDepartmentId(peOfficeCreationDtBean.getTxtdepartmentid());
            if(peOfficeCreationService.addPEOfficeCreation(tblOMaster)){
                flag = true;
            }
            setOfficeid(tblOMaster.getOfficeId());
        } catch (Exception ex) {
            LOGGER.error("peOfficeCreation : " + logUserId + ex);
    }
        LOGGER.debug("peOfficeCreation : " + logUserId + END);
        return flag;
    }
    private int officeid;

    public int getOfficeid() {
        return officeid;
    }

    /**
     * set Office Id for created PE Office
     * @param officeid
     */
    public void setOfficeid(int officeid) {
        this.officeid = officeid;
    }

    /**
     * copies properties
     * @param peOfficeCreationDtBean
     * @return Object with copied properties
     */
    public TblOfficeMaster _toTblOfficeMaster(PEOfficeCreationDtBean peOfficeCreationDtBean) {
        TblOfficeMaster tblOfficeM = new TblOfficeMaster();
        BeanUtils.copyProperties(peOfficeCreationDtBean, tblOfficeM);
        return tblOfficeM;
    }
    private List<SelectItem> stateList = new ArrayList<SelectItem>();

    public List<SelectItem> getStateList() {
        LOGGER.debug("getStateList : " + logUserId + START);
        try {
        if (stateList.isEmpty()) {
            List<TblStateMaster> stateMasters = commonService.getState((short) 150);
            for (TblStateMaster stateMaster : stateMasters) {
                stateList.add(new SelectItem(stateMaster.getStateId(), stateMaster.getStateName()));
            }
        }
        } catch (Exception ex) {
            LOGGER.error("getStateList : " + logUserId + ex);
        }
        LOGGER.debug("getStateList : " + logUserId + END);
        return stateList;
    }

    public void setStateList(List<SelectItem> stateList) {
        this.stateList = stateList;
    }
    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String getSortOrder = "";
    private String sortCol = "";

    public String getSortOrder() {
        return getSortOrder;
    }

    public void setSortOrder(String getSortOrder) {
        this.getSortOrder = getSortOrder;
    }

    public String getSortCol() {
        return sortCol;
    }

    public void setSortCol(String sortCol) {
        this.sortCol = sortCol;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public boolean isSearch() {
        return _search;
    }

    public void setSearch(boolean _search) {
        this._search = _search;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getOp_ENUM() {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM) {
        this.op_ENUM = op_ENUM;
    }

    /**
     * count records
     * @param deptType
     * @return number of records
     * throws Exception
     */
    public long getAllCountOfPEMaster(String deptType) throws Exception {
        LOGGER.debug("getAllCountOfPEMaster : " + logUserId + START);
        long lng = 0;
        try {
            lng = peOfficeCreationService.getAllCountOfPEMasterList(deptType);
        } catch (Exception ex) {
            LOGGER.error("getAllCountOfPEMaster : " + logUserId + ex);
    }
        LOGGER.debug("getAllCountOfPEMaster : " + logUserId + END);
        return lng;
    }

    /**
     * count records
     * @param deptType
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     * throws Exception
     */
    public long getSearchCountOfPEMaster(String deptType, String searchField, String searchString, String searchOper) throws Exception {
        LOGGER.debug("getSearchCountOfPEMaster : " + logUserId + START);
        long lng = 0;
        try {
            lng = peOfficeCreationService.getSearchCountOfPEMasterList(deptType, searchField, searchString, searchOper);
        } catch (Exception ex) {
            LOGGER.error("getSearchCountOfPEMaster : " + logUserId + ex);
    }
        LOGGER.debug("getSearchCountOfPEMaster : " + logUserId + END);
        return lng;
    }

    /**
     * count records
     * @param deptType
     * @param userId
     * @return number of records
     * throws Exception
     */
    public long getAllCountOfPEMasterOrg(String deptType, int userId) throws Exception {
        LOGGER.debug("getAllCountOfPEMasterOrg : " + logUserId + START);
        long lng = 0;
        try {
            lng = peOfficeCreationService.getAllCountOfPEMasterListOrg(deptType, userId);
        } catch (Exception ex) {
            LOGGER.error("getAllCountOfPEMasterOrg : " + logUserId + ex);
    }
        LOGGER.debug("getAllCountOfPEMasterOrg : " + logUserId + END);
        return lng;
    }

    /**
     * count records
     * @param deptType
     * @param userId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     * throws Exception
     */
    public long getSearchCountOfPEMasterOrg(String deptType, int userId, String searchField, String searchString, String searchOper) throws Exception {
        LOGGER.debug("getSearchCountOfPEMasterOrg : " + logUserId + START);
        long lng = 0;
        try {
            lng = peOfficeCreationService.getSearchCountOfPEMasterListOrg(deptType, userId, searchField, searchString, searchOper);
        } catch (Exception ex) {
            LOGGER.error("getSearchCountOfPEMasterOrg : " + logUserId + ex);
    }
        LOGGER.debug("getSearchCountOfPEMasterOrg : " + logUserId + END);
        return lng;
    }
    List<TblOfficeMaster> tblOfficeMaster = new ArrayList<TblOfficeMaster>();

    /**
     *List of TblOfficeMaster
     * @return List of TblOfficeMaster
     */
    public List<TblOfficeMaster> getTblOfficeMaster() throws Exception {
        LOGGER.debug("getTblOfficeMaster : " + logUserId + START);
        try {
        if (tblOfficeMaster.isEmpty()) {
            if (_search) {
                for (TblOfficeMaster tblOfMaster : peOfficeCreationService.findPEOfficeMaster(getOffset(), getLimit(), "officeName", Operation_enum.LIKE, "ABC", getSortCol(), Operation_enum.ORDERBY, getSortOrder())) {
                    tblOfficeMaster.add(tblOfMaster);
                }
            } else {
                if (getSortOrder().equalsIgnoreCase("asc")) {

                    for (TblOfficeMaster tblOM : peOfficeCreationService.findPEOfficeMaster(getOffset(), getLimit(), getSortCol(), Operation_enum.ORDERBY, Operation_enum.ASC)) {
                        tblOfficeMaster.add(tblOM);
                    }
                } else if (getSortOrder().equalsIgnoreCase("desc")) {
                    for (TblOfficeMaster tblofficeMaster : peOfficeCreationService.findPEOfficeMaster(getOffset(), getLimit(), getSortCol(), Operation_enum.ORDERBY, Operation_enum.DESC)) {
                        tblOfficeMaster.add(tblofficeMaster);
                    }
                }
            }
        }
        } catch (Exception ex) {
            LOGGER.error("getTblOfficeMaster : " + logUserId + ex);
        }
        LOGGER.debug("getTblOfficeMaster : " + logUserId + END);
        return tblOfficeMaster;
    }

    /**
     * fetching data for PE Office
     * @param deptType
     * @param ascClause
     * @return information for PE office
     * throws Exception
     */
    public List<Object[]> getTblOfficeMasterList(String deptType, String ascClause) throws Exception {
        LOGGER.debug("getTblOfficeMasterList : " + logUserId + START);
        List<Object[]> list = null;
        try {
            list = peOfficeCreationService.findPEOfficeMasterList(getOffset(), getLimit(), deptType, ascClause);
        } catch (Exception ex) {
            LOGGER.error("getTblOfficeMasterList : " + logUserId + ex);
    }
        LOGGER.debug("getTblOfficeMasterList : " + logUserId + END);
        return list;
    }

    /**
     * fetching data for PE Office
     * @param deptType
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return information for PE office
     * throws Exception
     */
    public List<Object[]> getTblOfficeMasterList(String deptType, String searchField, String searchString, String searchOper) throws Exception {
        LOGGER.debug("getTblOfficeMasterList : " + logUserId + START);
        List<Object[]> list = null;
        try {
            list = peOfficeCreationService.findPEOfficeMasterList(getOffset(), getLimit(), deptType, searchField, searchString, searchOper);
        } catch (Exception ex) {
            LOGGER.error("getTblOfficeMasterList : " + logUserId + ex);
    }
        LOGGER.debug("getTblOfficeMasterList : " + logUserId + END);
        return list;
    }

    /**
     * fetching data for PE Office
     * @param deptType
     * @param userId
     * @param ascClause
     * @return information for PE office
     * throws Exception
     */
    public List<Object[]> getTblOfficeMasterListForOrg(String deptType, int userId, String ascClause) throws Exception {
        LOGGER.debug("getTblOfficeMasterListForOrg : " + logUserId + START);
        List<Object[]> list = null;
        try {
            list = peOfficeCreationService.findPEOfficeMasterListORG(getOffset(), getLimit(), deptType, userId, ascClause);
        } catch (Exception ex) {
            LOGGER.error("getTblOfficeMasterListForOrg : " + logUserId + ex);
    }
        LOGGER.debug("getTblOfficeMasterListForOrg : " + logUserId + END);
        return list;
    }

    /**
     * fetching data for PE Office
     * @param deptType
     * @param userId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return information for PE office
     * throws Exception
     */
    public List<Object[]> getTblOfficeMasterListForOrg_Search(String deptType, int userId, String searchField, String searchString, String searchOper) throws Exception {
        LOGGER.debug("getTblOfficeMasterListForOrg_Search : " + logUserId + START);
        List<Object[]> list = null;
        try {
            list = peOfficeCreationService.findPEOfficeMasterListORG(getOffset(), getLimit(), deptType, userId, searchField, searchString, searchOper);
        } catch (Exception ex) {
            LOGGER.error("getTblOfficeMasterListForOrg_Search : " + logUserId + ex);
        }
        LOGGER.debug("getTblOfficeMasterListForOrg_Search : " + logUserId + END);
        return list;
    }

    public void setTblOfficeMaster(List<TblOfficeMaster> tblOfficeMaster) {
        this.tblOfficeMaster = tblOfficeMaster;
    }

    /**
     * fetching data for PE Office for particular Office id
     * @param officeId
     * @return information for PE office
     * throws Exception
     */
    public List<TblOfficeMaster> getFindUser(int officeId) throws Exception {
        LOGGER.debug("getFindUser : " + logUserId + START);
        List<TblOfficeMaster> tblList = new ArrayList<TblOfficeMaster>();
        try {
        for (TblOfficeMaster tblOfficeMasterList : peOfficeCreationService.findOfficeMaster("officeId", Operation_enum.EQ, officeId)) {
            setStateId(tblOfficeMasterList.getTblCountryMaster().getCountryId());
            tblList.add(tblOfficeMasterList);
        }
        } catch (Exception ex) {
            LOGGER.error("getFindUser : " + logUserId + ex);
        }
        LOGGER.debug("getFindUser : " + logUserId + END);
        return tblList;
    }
    private List<SelectItem> stateDefaultList = new ArrayList<SelectItem>();

    /**
     * get listing for states
     * @return states list
     */
    public List<SelectItem> getStateDefaultList() {
        LOGGER.debug("getStateDefaultList : " + logUserId + START);
        try {
        if (stateDefaultList.isEmpty()) {
            List<TblStateMaster> stateMasters = commonService.getState((short) stateId);
            for (TblStateMaster stateMaster : stateMasters) {
                stateDefaultList.add(new SelectItem(stateMaster.getStateId(), stateMaster.getStateName()));
            }
        }
        } catch (Exception ex) {
            LOGGER.error("getStateDefaultList : " + logUserId + ex);
        }
        LOGGER.debug("getStateDefaultList : " + logUserId + END);
        return stateDefaultList;
    }

    public void setStateDefaultList(List<SelectItem> stateDefaultList) {
        this.stateDefaultList = stateDefaultList;
    }
    short stateId;

    public short getStateId() {
        return stateId;
    }

    public void setStateId(short stateId) {
        this.stateId = stateId;
    }

     /**
     * TblDepartmentMaster List
     * @param deptid - departmentId of Tbl_DepartmentMaster
     * @return List of TblDepartmentMaster 
     */
    public List<TblDepartmentMaster> getDefaultDept(int deptid) {
        LOGGER.debug("getDefaultDept : " + logUserId + START);
        String deptname[] = null;
        List<TblDepartmentMaster> dept = null;
        short shortdeptid = (short) deptid;
        try {
            dept = peOfficeCreationService.findTblOfficeMaster("departmentId", Operation_enum.EQ, shortdeptid);
          //  for (TblDepartmentMaster tblOfficeMasterList : peOfficeCreationService.findTblOfficeMaster("departmentId", Operation_enum.EQ, shortdeptid)) {
              //  dept.add(tblOfficeMasterList.getDepartmentName());
              //  dept.add(tblOfficeMasterList.getOrganizationType());
           // }
        } catch (Exception ex) {
            LOGGER.error("getDefaultDept : " + logUserId + ex);
        }
        LOGGER.debug("getDefaultDept : " + logUserId + END);
        return dept;
    }

    /**
     * updating data
     * @param peOfficeCreationDtBean
     * @return true if updated else false
     * throws UnsupportedEncodingException
     */
    public boolean  updateOfficeMaster(PEOfficeCreationDtBean peOfficeCreationDtBean) throws UnsupportedEncodingException, ParseException {
        boolean flag = false;
        LOGGER.debug("updateOfficeMaster : " + logUserId + START);
        try {
        TblOfficeMaster tblOMaster = _toTblOfficeMaster(peOfficeCreationDtBean);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date d = dateFormat.parse(peOfficeCreationDtBean.getStrCreatedDate());
        tblOMaster.setDepartmentId(peOfficeCreationDtBean.getTxtdepartmentid());
        tblOMaster.setCreatedBy(1);
        tblOMaster.setStatus("Approved");
        tblOMaster.setCreationDate(d);
        tblOMaster.setTblCountryMaster(new TblCountryMaster(peOfficeCreationDtBean.getCountryId()));
        tblOMaster.setTblStateMaster(new TblStateMaster(peOfficeCreationDtBean.getStateId()));
            if (!(peOfficeCreationDtBean.getDeptBanglaString() == null || peOfficeCreationDtBean.getDeptBanglaString().trim().equals(""))) {
            tblOMaster.setOfficeNameInBangla(peOfficeCreationDtBean.getDeptBanglaString().getBytes("UTF-8"));
        }
        if (peOfficeCreationDtBean.getFaxNo() == null || peOfficeCreationDtBean.getFaxNo().trim().equals("")) {
            tblOMaster.setFaxNo("");
        }
        if (peOfficeCreationDtBean.getPeCode() == null || peOfficeCreationDtBean.getPeCode().trim().equals("")) {
            tblOMaster.setPeCode("");
        }
        if (peOfficeCreationDtBean.getCity() == null || peOfficeCreationDtBean.getCity().trim().equals("")) {
            tblOMaster.setCity("");
        }
        if (peOfficeCreationDtBean.getPostCode() == null || peOfficeCreationDtBean.getPostCode().trim().equals("")) {
            tblOMaster.setPostCode("");
        }
        if (peOfficeCreationDtBean.getUpjilla() == null || peOfficeCreationDtBean.getUpjilla().trim().equals("")) {
            tblOMaster.setUpjilla("");
        }
        
            if(peOfficeCreationService.updatePEOfficeMaster(tblOMaster)){
                flag = true;
    }
        } catch (Exception ex) {
            LOGGER.error("updateOfficeMaster : " + logUserId + ex);
        }
        LOGGER.debug("updateOfficeMaster : " + logUserId + END);
        return flag;
    }

    /**
     * List of TblDepartmentMaster - get Department name
     * @param values variable arguments containing conditions
     * @return List of TblDepartmentMaster
     */
    public List<TblDepartmentMaster> getDepartmentName(int approvingAuthorityId) throws Exception {
        LOGGER.debug("getDepartmentName : " + logUserId + START);
        List<TblDepartmentMaster> tblDepartmentMaster = new ArrayList<TblDepartmentMaster>();
        try {
        if (tblDepartmentMaster.isEmpty()) {
            for (TblDepartmentMaster tDepartmentMaster : peOfficeCreationService.findDepartmentName("approvingAuthorityId", Operation_enum.EQ, approvingAuthorityId)) {
                tblDepartmentMaster.add(tDepartmentMaster);
            }
        }
        } catch (Exception ex) {
            LOGGER.error("getDepartmentName : " + logUserId + ex);
        }
        LOGGER.debug("getDepartmentName : " + logUserId + END);
        return tblDepartmentMaster;
    }

    /**
     * List of TblDepartmentMaster - get Department Type
     * @param values variable arguments containing conditions
     * @return List of TblDepartmentMaster
     */
    public List<TblDepartmentMaster> getDepartmentType(String deptType) throws Exception {
        LOGGER.debug("getDepartmentType : " + logUserId + START);
        List<TblDepartmentMaster> tblDepartmentMaster = new ArrayList<TblDepartmentMaster>();
        try {
            if (tblDepartmentMaster.isEmpty()) {
                for (TblDepartmentMaster tblDMaster : peOfficeCreationService.findDepartmentName("departmentType", Operation_enum.LIKE, deptType, "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC)) {
                tblDepartmentMaster.add(tblDMaster);
            }
        }
        } catch (Exception ex) {
            LOGGER.error("getDepartmentType : " + logUserId + ex);
        }
        LOGGER.debug("getDepartmentType : " + logUserId + END);
        return tblDepartmentMaster;
    }    
}
