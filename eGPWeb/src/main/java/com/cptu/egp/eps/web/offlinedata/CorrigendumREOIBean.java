/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

/**
 *
 * @author salahuddin
 */

import com.cptu.egp.eps.model.table.TblTenderDetailsOffline;
import com.cptu.egp.eps.model.table.TblCorrigendumDetailOffline;
import com.cptu.egp.eps.model.table.TblTenderLotPhasingOffline;
import com.cptu.egp.eps.service.serviceimpl.CorrigendumDetailsOfflineService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CommonUtils;
import java.util.ArrayList;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import java.lang.String;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.lang.reflect.Method;
import java.math.BigDecimal;

public class CorrigendumREOIBean {

    private static final Logger LOGGER = Logger.getLogger(CorrigendumREOIBean.class);
    //PrequalificationExcellBean preqExcellBean = null;
    EOIExcellBean eoiExcellBean = null;
    TenderFormExcellBean tenderExcelBean = null;
    List<TblCorrigendumDetailOffline> corrigendumDetailOffline = null;
    private final CorrigendumDetailsOfflineService corrigendumDetailsOfflineService  = (CorrigendumDetailsOfflineService) AppContext.getSpringBean("CorrigendumDetailsOfflineService");
    private String corriStatus = "Pending";
    Date date = new Date();
    private int CorNo=0;

    private String oldDate = "";
    private String newDate = "";

    public void createCorrigendum(TblTenderDetailsOffline tenderDetailsOffline,int tenderOLId, int userID, int corrigendumNo, boolean isEdit) throws Exception {


        LOGGER.debug("createCorri : " + userID + "starts");

        CorNo = corrigendumNo;

        OfflineDataSrBean offlineDataSrBean = new OfflineDataSrBean();
        eoiExcellBean = (EOIExcellBean) offlineDataSrBean.editEOIForm(tenderOLId);

        List<TblCorrigendumDetailOffline> list = new ArrayList<TblCorrigendumDetailOffline>();

        /*if(!CommonUtils.checkNull(eoiExcellBean.getMinistryName()).equals(tenderDetailsOffline.getMinistryOrDivision()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Ministry",CommonUtils.checkNull(eoiExcellBean.getMinistryName()), tenderDetailsOffline.getMinistryOrDivision(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getAgency()).equals(tenderDetailsOffline.getAgency()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Agency",CommonUtils.checkNull(eoiExcellBean.getAgency()), tenderDetailsOffline.getAgency(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getProcuringEntityName()).equals(tenderDetailsOffline.getPeName()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Procuring Entity Name",CommonUtils.checkNull(eoiExcellBean.getProcuringEntityName()), tenderDetailsOffline.getPeName(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getProcuringEntityCode()).equals(tenderDetailsOffline.getPeCode()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Procuring Entity Code",CommonUtils.checkNull(eoiExcellBean.getProcuringEntityCode()), tenderDetailsOffline.getPeCode(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getProcuringEntityDistrict()).equals(tenderDetailsOffline.getPeDistrict()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Procuring Entity District",CommonUtils.checkNull(eoiExcellBean.getProcuringEntityDistrict()), tenderDetailsOffline.getPeDistrict(), corriStatus, CorNo, userID, date));
        }*/

        //CONTRACT TYPE NEEDS TO BE CHECKED
        /*if(!CommonUtils.checkNull(eoiExcellBean.getProcuringEntityDistrict()).equals(tenderDetailsOffline.getPeDistrict()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Contract Type",CommonUtils.checkNull(eoiExcellBean.getProcuringEntityDistrict()), tenderDetailsOffline.getPeDistrict(), corriStatus, CorNo, userID, date));
        }*/

        if(!CommonUtils.checkNull(eoiExcellBean.getInterestForSelectionOfBased()).equals(tenderDetailsOffline.getInvitationFor()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Contract Type",CommonUtils.checkNull(eoiExcellBean.getInterestForSelectionOfBased()), tenderDetailsOffline.getInvitationFor(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getEoiRefNo()).equals(tenderDetailsOffline.getReoiRfpRefNo()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "EOI Reference No.",CommonUtils.checkNull(eoiExcellBean.getEoiRefNo()), tenderDetailsOffline.getReoiRfpRefNo(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getInterestForSelectionOfConsult()).equals(tenderDetailsOffline.getReoiRfpFor()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "EOI For",CommonUtils.checkNull(eoiExcellBean.getInterestForSelectionOfConsult()), tenderDetailsOffline.getReoiRfpFor(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getProcurementSubmethod()).equals(tenderDetailsOffline.getProcurementMethod()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Procurement Method",CommonUtils.checkNull(eoiExcellBean.getProcurementSubmethod()), tenderDetailsOffline.getProcurementMethod(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getBudgetFund()).equals(tenderDetailsOffline.getBudgetType()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Budget Type",CommonUtils.checkNull(eoiExcellBean.getBudgetFund()), tenderDetailsOffline.getBudgetType(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getSourceFund()).equals(tenderDetailsOffline.getSourceOfFund()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Source of Fund",CommonUtils.checkNull(eoiExcellBean.getSourceFund()), tenderDetailsOffline.getSourceOfFund(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getDevelopmentPartner()).equals(tenderDetailsOffline.getDevPartners()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Development Partners",CommonUtils.checkNull(eoiExcellBean.getDevelopmentPartner()), tenderDetailsOffline.getDevPartners(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getProjectOrProgramCode()).equals(tenderDetailsOffline.getProjectCode()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Project Code",CommonUtils.checkNull(eoiExcellBean.getProjectOrProgramCode()), tenderDetailsOffline.getProjectCode(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getProjectOrProgrammeName()).equals(tenderDetailsOffline.getProjectName()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Project Name",CommonUtils.checkNull(eoiExcellBean.getProjectOrProgrammeName()), tenderDetailsOffline.getProjectName(), corriStatus, CorNo, userID, date));
        }

        oldDate = CommonUtils.checkNull(eoiExcellBean.getEoiDate());
        DateFormat dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
        newDate = dtNewDate.format(tenderDetailsOffline.getIssueDate());
        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Issue Date",oldDate, newDate, corriStatus, CorNo, userID, date));
        }

        oldDate = CommonUtils.checkNull(eoiExcellBean.getEoiClosingDateandtime()); //preqExcellBean.getPrequalificationClosingDateandTime();
        DateFormat dtNewDate1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        newDate = dtNewDate1.format(tenderDetailsOffline.getClosingDate());
        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Closing Date",oldDate, newDate, corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getBriefDescriptionoftheAssignment()).equals(tenderDetailsOffline.getBriefDescription()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Brief Description",CommonUtils.checkNull(eoiExcellBean.getBriefDescriptionoftheAssignment()), tenderDetailsOffline.getBriefDescription(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getExpResDeliverCapacityRequired()).equals(tenderDetailsOffline.getRelServicesOrDeliverables()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Other Requirement",CommonUtils.checkNull(eoiExcellBean.getExpResDeliverCapacityRequired()), tenderDetailsOffline.getRelServicesOrDeliverables(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getOtherDetails()).equals(tenderDetailsOffline.getOtherDetails()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Other Details",CommonUtils.checkNull(eoiExcellBean.getOtherDetails()), tenderDetailsOffline.getOtherDetails(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getAssociationwithforeignfirms()).equals(tenderDetailsOffline.getForeignFirm()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Foreign Firms",CommonUtils.checkNull(eoiExcellBean.getAssociationwithforeignfirms()), tenderDetailsOffline.getForeignFirm(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getNameoftheOfficialInvitingEOI()).equals(tenderDetailsOffline.getPeOfficeName()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Name of the official",CommonUtils.checkNull(eoiExcellBean.getNameoftheOfficialInvitingEOI()), tenderDetailsOffline.getPeOfficeName(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getDesignationoftheOfficialInvitingEOI()).equals(tenderDetailsOffline.getPeDesignation()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Designation of the official",CommonUtils.checkNull(eoiExcellBean.getDesignationoftheOfficialInvitingEOI()), tenderDetailsOffline.getPeDesignation(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getAddressoftheOfficialInvitingEOI()).equals(tenderDetailsOffline.getPeAddress()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Address of the official", CommonUtils.checkNull(eoiExcellBean.getAddressoftheOfficialInvitingEOI()), tenderDetailsOffline.getPeAddress(), corriStatus, CorNo, userID, date));
        }

        if(!CommonUtils.checkNull(eoiExcellBean.getContactDetailsoftheOfficialInvitingEOI()).equals(tenderDetailsOffline.getPeContactDetails()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A",  "Contact Details",CommonUtils.checkNull(eoiExcellBean.getContactDetailsoftheOfficialInvitingEOI()), tenderDetailsOffline.getPeContactDetails(), corriStatus, CorNo, userID, date));
        }

        String[][] phaseDetails = {
                                        {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"},
                                        {"Reference Number", "Phasing of service", "Location", "Indicative start date", "Indicative completion date"}
                                    };
        List<TblTenderLotPhasingOffline> tenderLotsAndPhases = tenderDetailsOffline.getTenderLotsAndPhases();

        //>>>>>>>>>>>>>> PHASES >>>>>>>>>>>>>>>>>>
        if(tenderLotsAndPhases.size()>0)
        {
            if(!CommonUtils.checkNull(eoiExcellBean.getRefNo_1()).equals(tenderLotsAndPhases.get(0).getLotOrRefNo()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][0],  phaseDetails[1][0] , CommonUtils.checkNull(eoiExcellBean.getRefNo_1()), tenderLotsAndPhases.get(0).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_1()).equals(tenderLotsAndPhases.get(0).getLotIdentOrPhasingServ()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][0],  phaseDetails[1][1] , CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_1()), tenderLotsAndPhases.get(0).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getLocation_1()).equals(tenderLotsAndPhases.get(0).getLocation()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][0],  phaseDetails[1][2] , CommonUtils.checkNull(eoiExcellBean.getLocation_1()), tenderLotsAndPhases.get(0).getLocation(),  corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeStartDateMonOrYer_1());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(0).getStartDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][0],  phaseDetails[1][3], oldDate, newDate, corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeCompletionDateMonOrYer_1());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(0).getCompletionDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][0],  phaseDetails[1][4], oldDate, newDate, corriStatus, CorNo, userID, date));
            }
        }

        if(tenderLotsAndPhases.size()>1)
        {
            if(!CommonUtils.checkNull(eoiExcellBean.getRefNo_2()).equals(tenderLotsAndPhases.get(1).getLotOrRefNo()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][1],  phaseDetails[1][0] , CommonUtils.checkNull(eoiExcellBean.getRefNo_2()), tenderLotsAndPhases.get(1).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_2()).equals(tenderLotsAndPhases.get(1).getLotIdentOrPhasingServ()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][1],  phaseDetails[1][1] , CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_2()), tenderLotsAndPhases.get(1).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getLocation_2()).equals(tenderLotsAndPhases.get(1).getLocation()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][1],  phaseDetails[1][2] , CommonUtils.checkNull(eoiExcellBean.getLocation_2()), tenderLotsAndPhases.get(1).getLocation(),  corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeStartDateMonOrYer_2());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(1).getStartDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][1],  phaseDetails[1][3], oldDate, newDate, corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeCompletionDateMonOrYer_2());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(1).getCompletionDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][1],  phaseDetails[1][4], oldDate, newDate, corriStatus, CorNo, userID, date));
            }
        }

        if(tenderLotsAndPhases.size()>2)
        {
            if(!CommonUtils.checkNull(eoiExcellBean.getRefNo_3()).equals(tenderLotsAndPhases.get(2).getLotOrRefNo()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][2],  phaseDetails[1][0] , CommonUtils.checkNull(eoiExcellBean.getRefNo_3()), tenderLotsAndPhases.get(2).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_3()).equals(tenderLotsAndPhases.get(2).getLotIdentOrPhasingServ()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][2],  phaseDetails[1][1] , CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_3()), tenderLotsAndPhases.get(2).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getLocation_3()).equals(tenderLotsAndPhases.get(2).getLocation()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][2],  phaseDetails[1][2] , CommonUtils.checkNull(eoiExcellBean.getLocation_3()), tenderLotsAndPhases.get(2).getLocation(),  corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeStartDateMonOrYer_3());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(2).getStartDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][2],  phaseDetails[1][3], oldDate, newDate, corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeCompletionDateMonOrYer_3());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(2).getCompletionDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][2],  phaseDetails[1][4], oldDate, newDate, corriStatus, CorNo, userID, date));
            }
        }

        if(tenderLotsAndPhases.size()>3)
        {
            if(!CommonUtils.checkNull(eoiExcellBean.getRefNo_4()).equals(tenderLotsAndPhases.get(3).getLotOrRefNo()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][3],  phaseDetails[1][0] , CommonUtils.checkNull(eoiExcellBean.getRefNo_4()), tenderLotsAndPhases.get(3).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_4()).equals(tenderLotsAndPhases.get(3).getLotIdentOrPhasingServ()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][3],  phaseDetails[1][1] , CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_4()), tenderLotsAndPhases.get(3).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getLocation_4()).equals(tenderLotsAndPhases.get(3).getLocation()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][3],  phaseDetails[1][2] , CommonUtils.checkNull(eoiExcellBean.getLocation_4()), tenderLotsAndPhases.get(3).getLocation(),  corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeStartDateMonOrYer_4());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(3).getStartDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][3],  phaseDetails[1][3], oldDate, newDate, corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeCompletionDateMonOrYer_4());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(3).getCompletionDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][3],  phaseDetails[1][4], oldDate, newDate, corriStatus, CorNo, userID, date));
            }
        }

        if(tenderLotsAndPhases.size()>4)
        {
            if(!CommonUtils.checkNull(eoiExcellBean.getRefNo_5()).equals(tenderLotsAndPhases.get(4).getLotOrRefNo()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][4],  phaseDetails[1][0] , CommonUtils.checkNull(eoiExcellBean.getRefNo_5()), tenderLotsAndPhases.get(4).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_5()).equals(tenderLotsAndPhases.get(4).getLotIdentOrPhasingServ()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][4],  phaseDetails[1][1] , CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_5()), tenderLotsAndPhases.get(4).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getLocation_5()).equals(tenderLotsAndPhases.get(4).getLocation()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][4],  phaseDetails[1][2] , CommonUtils.checkNull(eoiExcellBean.getLocation_5()), tenderLotsAndPhases.get(4).getLocation(),  corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeStartDateMonOrYer_5());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(4).getStartDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][4],  phaseDetails[1][3], oldDate, newDate, corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeCompletionDateMonOrYer_5());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(4).getCompletionDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][4],  phaseDetails[1][4], oldDate, newDate, corriStatus, CorNo, userID, date));
            }
        }

        if(tenderLotsAndPhases.size()>5)
        {
            if(!CommonUtils.checkNull(eoiExcellBean.getRefNo_6()).equals(tenderLotsAndPhases.get(5).getLotOrRefNo()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][5], phaseDetails[1][0] , CommonUtils.checkNull(eoiExcellBean.getRefNo_6()), tenderLotsAndPhases.get(5).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_6()).equals(tenderLotsAndPhases.get(5).getLotIdentOrPhasingServ()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][5],  phaseDetails[1][1] , CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_6()), tenderLotsAndPhases.get(5).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getLocation_6()).equals(tenderLotsAndPhases.get(5).getLocation()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][5], phaseDetails[1][2] , CommonUtils.checkNull(eoiExcellBean.getLocation_6()), tenderLotsAndPhases.get(5).getLocation(),  corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeStartDateMonOrYer_6());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(5).getStartDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][5],  phaseDetails[1][3], oldDate, newDate, corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeCompletionDateMonOrYer_6());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(5).getCompletionDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][5],  phaseDetails[1][4], oldDate, newDate, corriStatus, CorNo, userID, date));
            }
        }

        if(tenderLotsAndPhases.size()>6)
        {
            if(!CommonUtils.checkNull(eoiExcellBean.getRefNo_7()).equals(tenderLotsAndPhases.get(6).getLotOrRefNo()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][6],  phaseDetails[1][0] , CommonUtils.checkNull(eoiExcellBean.getRefNo_7()), tenderLotsAndPhases.get(6).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_7()).equals(tenderLotsAndPhases.get(6).getLotIdentOrPhasingServ()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][6],  phaseDetails[1][1] , CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_7()), tenderLotsAndPhases.get(6).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getLocation_7()).equals(tenderLotsAndPhases.get(6).getLocation()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][6],  phaseDetails[1][2] , CommonUtils.checkNull(eoiExcellBean.getLocation_7()), tenderLotsAndPhases.get(6).getLocation(),  corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeStartDateMonOrYer_7());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(6).getStartDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][6],  phaseDetails[1][3], oldDate, newDate, corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeCompletionDateMonOrYer_7());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(6).getCompletionDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][6],  phaseDetails[1][4], oldDate, newDate, corriStatus, CorNo, userID, date));
            }
        }

        if(tenderLotsAndPhases.size()>7)
        {
            if(!CommonUtils.checkNull(eoiExcellBean.getRefNo_8()).equals(tenderLotsAndPhases.get(7).getLotOrRefNo()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][7], phaseDetails[1][0] , CommonUtils.checkNull(eoiExcellBean.getRefNo_8()), tenderLotsAndPhases.get(7).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_8()).equals(tenderLotsAndPhases.get(7).getLotIdentOrPhasingServ()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][7],  phaseDetails[1][1] , CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_8()), tenderLotsAndPhases.get(7).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getLocation_8()).equals(tenderLotsAndPhases.get(7).getLocation()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][7],  phaseDetails[1][2] , CommonUtils.checkNull(eoiExcellBean.getLocation_8()), tenderLotsAndPhases.get(7).getLocation(),  corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeStartDateMonOrYer_8());
            newDate = tenderLotsAndPhases.get(7).getStartDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][7],  phaseDetails[1][3], oldDate, newDate, corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeCompletionDateMonOrYer_8());
            newDate = tenderLotsAndPhases.get(7).getCompletionDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][7],  phaseDetails[1][4], oldDate, newDate, corriStatus, CorNo, userID, date));
            }
        }

        if(tenderLotsAndPhases.size()>8)
        {
            if(!CommonUtils.checkNull(eoiExcellBean.getRefNo_9()).equals(tenderLotsAndPhases.get(8).getLotOrRefNo()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][8],  phaseDetails[1][0] , CommonUtils.checkNull(eoiExcellBean.getRefNo_9()), tenderLotsAndPhases.get(8).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_9()).equals(tenderLotsAndPhases.get(8).getLotIdentOrPhasingServ()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][8],  phaseDetails[1][1] , CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_9()), tenderLotsAndPhases.get(8).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getLocation_9()).equals(tenderLotsAndPhases.get(8).getLocation()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][8],  phaseDetails[1][2] , CommonUtils.checkNull(eoiExcellBean.getLocation_9()), tenderLotsAndPhases.get(8).getLocation(),  corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeStartDateMonOrYer_9());
            newDate = tenderLotsAndPhases.get(8).getStartDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][8],  phaseDetails[1][3], oldDate, newDate, corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeCompletionDateMonOrYer_9());
            newDate = tenderLotsAndPhases.get(8).getCompletionDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][8],  phaseDetails[1][4], oldDate, newDate, corriStatus, CorNo, userID, date));
            }
        }

        if(tenderLotsAndPhases.size()>9)
        {
            if(!CommonUtils.checkNull(eoiExcellBean.getRefNo_10()).equals(tenderLotsAndPhases.get(9).getLotOrRefNo()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][9],  phaseDetails[1][0] , CommonUtils.checkNull(eoiExcellBean.getRefNo_10()), tenderLotsAndPhases.get(9).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_10()).equals(tenderLotsAndPhases.get(9).getLotIdentOrPhasingServ()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][9],  phaseDetails[1][1] , CommonUtils.checkNull(eoiExcellBean.getPhasingOfServices_10()), tenderLotsAndPhases.get(9).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
            }
            if(!CommonUtils.checkNull(eoiExcellBean.getLocation_10()).equals(tenderLotsAndPhases.get(9).getLocation()))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][9],  phaseDetails[1][2] , CommonUtils.checkNull(eoiExcellBean.getLocation_10()), tenderLotsAndPhases.get(9).getLocation(),  corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeStartDateMonOrYer_10());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(9).getStartDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId),  phaseDetails[0][9],  phaseDetails[1][3], oldDate, newDate, corriStatus, CorNo, userID, date));
            }

            oldDate = CommonUtils.checkNull(eoiExcellBean.getIndicativeCompletionDateMonOrYer_10());
            //dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
            newDate = tenderLotsAndPhases.get(9).getCompletionDateTime();
            if(!oldDate.equals(newDate))
            {
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), phaseDetails[0][9],  phaseDetails[1][4], oldDate, newDate, corriStatus, CorNo, userID, date));
            }
        }
                        
      if (!list.isEmpty())
      {
        //corrigendumDetailsOfflineService.insertCorriDetailsOffline(list, userID, tenderOLId, isEdit);

          boolean success;
          if(isEdit)
          {
              corrigendumDetailOffline = offlineDataSrBean.corrigendumDetails(tenderOLId);

              success= corrigendumDetailsOfflineService.deleteCorriDetailsOffline(corrigendumDetailOffline, userID, tenderOLId);
              corrigendumDetailsOfflineService.insertCorriDetailsOffline(list, userID);
          }
          else
          {
            corrigendumDetailsOfflineService.insertCorriDetailsOffline(list, userID);
          }
      }
        LOGGER.debug("createCorri : " + userID + "ends");
    }

}
