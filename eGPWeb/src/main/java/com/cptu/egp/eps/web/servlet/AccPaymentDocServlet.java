/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsInvoiceDocument;
import com.cptu.egp.eps.model.table.TblCmsInvoiceMaster;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class AccPaymentDocServlet extends HttpServlet {

    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");    
    private final UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

    private static final Logger LOGGER = Logger.getLogger(AccPaymentDocServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;    
    private File destinationDir;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //PrintWriter out = response.getWriter();
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");        
        String DESTINATION_DIR_PATH = "";
        int conId = 0;
        File file = null;
        String tenderId = "";
        String docSizeMsg = "";
        boolean checkret = false;
        boolean flag = false;
        boolean addDocsData = false;                
        int InvoiceId = 0;
        String wpId = "";
        String lotId = "";
        String fileName = "";
        String documentBrief = "";
        long fileSize = 0;
        String queryString = "";        
        String pageName = "";        
        response.setContentType("text/html");
        boolean dis = false;
        String userTypeId = "";
        HttpSession session = request.getSession();
        String actionPerform = "";
        String type = "";
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            accPaymentService.setLogUserId(logUserId);
            userRegisterService.setUserId(logUserId);
        }
        if(request.getSession().getAttribute("userTypeId")!=null){
            userTypeId = request.getSession().getAttribute("userTypeId").toString();
        }
        if("3".equalsIgnoreCase(userTypeId))
        {    type = "officer";
             pageName = "officer/AccPaymentDocUpload.jsp";
        }else
        {    type = "tenderer";
             pageName = "tenderer/TendererPaymentDocUpload.jsp";
        }
        if("3".equalsIgnoreCase(userTypeId))
        {
            if("25".equalsIgnoreCase(accPaymentService.getProcurementRoleID(logUserId))){
                DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AccPaymentDocServlet");
            }else{
                DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("OfficerPaymentDocServlet");
            }
        }
        else
        {
            DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AccPaymentDocServletforBidder");
        }
        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {
            try {
                try {
                    if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("remove")) {
                        int InvoicedocId = 0;
                        String docName = "";
                      //  makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Remove Document", "Tenderer side");
//                        if(request.getParameter("supplier")!=null && !"".equalsIgnoreCase(request.getParameter("supplier")))
//                        {
//                            DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AccPaymentDocServletforBidder");
//                        }
//                        if(request.getParameter("accountant")!=null && !"".equalsIgnoreCase(request.getParameter("accountant")))
//                        {
//                            DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AccPaymentDocServlet");
//                        }
                        if(request.getParameter("str")!=null && !"".equalsIgnoreCase(request.getParameter("str")))
                        {
                            if("supplier".equalsIgnoreCase(request.getParameter("str")))
                            {
                                DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AccPaymentDocServletforBidder");
                            }else if("accountant".equalsIgnoreCase(request.getParameter("str")))
                            {
                                DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AccPaymentDocServlet");
                            }else if("PE".equalsIgnoreCase(request.getParameter("str")))
                            {
                                DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("OfficerPaymentDocServlet");
                            }
                        }
                        if (request.getParameter("invoiceDocId") != null) {
                            InvoicedocId = Integer.parseInt(request.getParameter("invoiceDocId"));
                        }
                        if (request.getParameter("tenderId") != null) {
                            tenderId = request.getParameter("tenderId");
                        }
                        conId = service.getContractId(Integer.parseInt(tenderId));
                        if (request.getParameter("invoiceId") != null) {
                            InvoiceId = Integer.parseInt(request.getParameter("invoiceId"));
                        }
                        if (request.getParameter("InvoiceId") != null) {
                            InvoiceId = Integer.parseInt(request.getParameter("InvoiceId"));
                        }else{InvoiceId = Integer.parseInt(request.getParameter("invoiceId"));}
                        if(request.getParameter("wpId")!=null)
                        {
                            wpId = request.getParameter("wpId");
                        }                        
                        if(request.getParameter("lotId")!=null)
                        {
                            lotId = request.getParameter("lotId");
                        }
                        if (request.getParameter("docName") != null) {
                            docName = request.getParameter("docName");
                        }                        
                        file = new File(DESTINATION_DIR_PATH + InvoiceId + "\\" + docName);
                        fileName = file.toString();
                        checkret = deleteFile(fileName);
                       // makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Upload Document", "Tenderer side");
                        actionPerform = "Remove Document";
                        if (checkret) {
                            accPaymentService.deleteInvoiceDocsDetails(InvoicedocId);
                            queryString = "?tenderId=" + tenderId + "&InvoiceId=" + InvoiceId +"&wpId="+wpId+"&lotId="+lotId+"&fq=Removed";
                            response.sendRedirect(pageName + queryString);
                        }

                    } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("download")) {                        
//                        if(request.getParameter("supplier")!=null && !"".equalsIgnoreCase(request.getParameter("supplier")))
//                        {
//                            DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AccPaymentDocServletforBidder");
//                        }
//                        if(request.getParameter("accountant")!=null && !"".equalsIgnoreCase(request.getParameter("accountant")))
//                        {
//                            DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AccPaymentDocServlet");
//                        }
                      //  makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Download Document", "Tenderer side");
                        if("2".equalsIgnoreCase(userTypeId))
                        {
                            actionPerform = "Download Document (Uploaded by Account Officer & PE";
                        }else{    
                            actionPerform = "Download Document";
                        }
                        
                        if(request.getParameter("str")!=null && !"".equalsIgnoreCase(request.getParameter("str")))
                        {
                            if("supplier".equalsIgnoreCase(request.getParameter("str")))
                            {
                                DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AccPaymentDocServletforBidder");
                            }else if("accountant".equalsIgnoreCase(request.getParameter("str")))
                            {
                                DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AccPaymentDocServlet");
                            }else if("PE".equalsIgnoreCase(request.getParameter("str")))
                            {
                                DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("OfficerPaymentDocServlet");
                            }
                        }
                        String docName = request.getParameter("docName");
                        String docId = request.getParameter("invoiceDocId");
                        if (request.getParameter("tenderId") != null) {
                            tenderId = request.getParameter("tenderId");
                        }
                        conId = service.getContractId(Integer.parseInt(tenderId));
                        file = new File(DESTINATION_DIR_PATH + request.getParameter("invoiceId") + "\\" + docName);
                        FileInputStream fis = new FileInputStream(file);
                        byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                        int offset = 0;
                        int numRead = 0;
                        while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                            offset += numRead;
                        }
                        fis.close();
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition", "attachment;filename=\"" + docName + "\"");
                        ServletOutputStream outputStream = response.getOutputStream();
                        outputStream.write(buf);
                        outputStream.flush();
                        outputStream.close();
                    } else {
                        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                        /*
                         *Set the size threshold, above which content will be stored on disk.
                         */
                        fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
                        /*
                         * Set the temporary directory to store the uploaded files of size above threshold.
                         */
                        fileItemFactory.setRepository(tmpDir);

                        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                        /*
                         * Parse the request
                         */
                        List items = uploadHandler.parseRequest(request);
                        Iterator itr = items.iterator();
                        //For Supporting Document
                        while (itr.hasNext()) {
                            FileItem item = (FileItem) itr.next();
                            //For Supporting Document
                            /*
                             * Handle Form Fields.
                             */
                            if (item.isFormField()) {

                                if (item.getFieldName().equals("documentBrief")) {
                                    if (item.getString() == null || item.getString().trim().length() == 0) {
                                        dis = true;
                                        break;
                                    }
                                    documentBrief = item.getString();
                                } else if (item.getFieldName().equals("InvoiceId")) {
                                    InvoiceId = Integer.parseInt(item.getString());
                                }else if (item.getFieldName().equals("invoiceId")) {
                                    InvoiceId = Integer.parseInt(item.getString());
                                }else if (item.getFieldName().equals("tenderId")) {
                                    tenderId = item.getString();
                                }else if (item.getFieldName().equals("wpId")) {
                                    wpId = item.getString();
                                }else if (item.getFieldName().equals("lotId")) {
                                    lotId = item.getString();
                                }
                                conId = service.getContractId(Integer.parseInt(tenderId));
                            } else {
                                //Handle Uploaded files.
                                /*
                                 * Write file to the ultimate location.
                                 */
                                if (item.getName().lastIndexOf("\\") == -1) {
                                    fileName = item.getName();
                                } else {
                                    fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                }
                                fileName  = fileName.replaceAll(" ", "");
                                String realPath = DESTINATION_DIR_PATH + InvoiceId;
                                destinationDir = new File(realPath);
                                if (!destinationDir.isDirectory()) {
                                    destinationDir.mkdir();
                                }
                                docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));

                                if (docSizeMsg.equals("ok")){

                                    fileSize = item.getSize();
                                    checkret = checkExnAndSize(fileName, item.getSize(),type);

                                    if (!checkret) {
                                        break;
                                    } else {
                                        file = new File(destinationDir, fileName);
                                        if (file.isFile()) {
                                            flag = true;
                                            break;
                                        }
                                        item.write(file);
                                    }
                                }

                            }

                        }
                        if("2".equalsIgnoreCase(userTypeId))
                        {
                            actionPerform = "Upload Document (At the time of invoice generation)";
                        }else{    
                            actionPerform = "Upload Document";
                        }
                    }

                } catch (FileUploadException ex) {
                     LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
                } catch (Exception ex) {
                     LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
                }
                
                    if (dis) {
                        docSizeMsg = "Please Enter Discription";
                        queryString = "?fq=" + docSizeMsg + "&InvoiceId=" + InvoiceId + "&tenderId="+ tenderId +"&wpId="+wpId+"&lotId="+lotId+"";
                        response.sendRedirect(pageName + queryString);
                    } else {
                        if (flag) {
                            docSizeMsg = "File already Exists";
                            queryString = "?fq=" + docSizeMsg + "&InvoiceId=" + InvoiceId + "&tenderId="+ tenderId +"&wpId="+wpId+"&lotId="+lotId+"";
                            response.sendRedirect(pageName + queryString);
                        } else {
                            if (!checkret) {
                                CheckExtension ext = new CheckExtension();
                                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(type);
                                queryString = "?fs="+configurationMaster.getFileSize()+"&ft="+configurationMaster.getAllowedExtension()+"&InvoiceId="+InvoiceId+"&tenderId="+tenderId+"&wpId="+wpId+"&lotId="+lotId+"";
                                response.sendRedirect(pageName + queryString);
                            } else {
                                if (documentBrief != null && !"".equals(documentBrief)) {
                                     if (file.isFile()) { // for Document Upload added by Dohatec
                                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                                    documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    int userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());                                    
                                    TblCmsInvoiceDocument tblCmsInvoiceDocument = new TblCmsInvoiceDocument();
                                    tblCmsInvoiceDocument.setTblCmsInvoiceMaster(new TblCmsInvoiceMaster(InvoiceId));
                                    tblCmsInvoiceDocument.setDocumentName(fileName);
                                    tblCmsInvoiceDocument.setDocSize(Integer.toString((int)fileSize));
                                    tblCmsInvoiceDocument.setDocDescription(documentBrief);
                                    tblCmsInvoiceDocument.setUploadedBy(userId);
                                    tblCmsInvoiceDocument.setUploadedDate(new java.sql.Date(new java.util.Date().getTime()));
                                    tblCmsInvoiceDocument.setUserTypeId(Integer.parseInt(userTypeId));
                                    addDocsData = accPaymentService.addInvoiceDocsDetails(tblCmsInvoiceDocument);
                                    if (addDocsData) {

                                        if("3".equalsIgnoreCase(userTypeId))
                                        {
                                            response.sendRedirect("officer/AccPaymentDocUpload.jsp?tenderId=" + tenderId + "&InvoiceId=" + InvoiceId +"&wpId="+wpId+"&lotId="+lotId+"&fq=Uploaded");
                                        }else{
                                            response.sendRedirect("tenderer/TendererPaymentDocUpload.jsp?tenderId=" + tenderId + "&InvoiceId=" + InvoiceId +"&wpId="+wpId+"&lotId="+lotId+"&fq=Uploaded");
                                        }
                                    }
                                    }
                                     /* for Document Upload added by Dohatec Start */
                                    else{
                                        docSizeMsg = "Error occurred during file upload.";
                                        queryString = "?fq=" + docSizeMsg + "&InvoiceId=" + InvoiceId + "&tenderId="+ tenderId +"&wpId="+wpId+"&lotId="+lotId+"";
                                        response.sendRedirect(pageName + queryString);
                                    }
                                    /* for Document Upload added by Dohatec End */
                                }
                            }
                        }
                    }              
            } finally {
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), actionPerform, "Tenderer side");
                //   out.close();
            }
        }
        LOGGER.debug("processRequest  : "+logUserId+ LOGGEREND);
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public boolean checkExnAndSize(String extn, long size, String userType) {

        LOGGER.debug("checkExnAndSize  : "+logUserId+ LOGGERSTART);
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);

            dsize = configurationMaster.getFileSize();

            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        LOGGER.debug("checkExnAndSize  : "+logUserId+ LOGGEREND);
        return chextn;
    }

    public String docSizeMsg(int userId) {
        String str = "";
        LOGGER.debug("docSizeMsg  : "+logUserId+ LOGGERSTART);
        try{
            str = userRegisterService.docSizeCheck(userId);
        }catch(Exception ex){
             LOGGER.error("docSizeMsg "+logUserId+" : "+ex.toString());
    }
        LOGGER.debug("docSizeMsg  : "+logUserId+ LOGGEREND);
        return str;
    }

    public boolean deleteFile(String filePath) {
        LOGGER.debug("deleteFile  : "+logUserId+ LOGGERSTART);
        boolean flag = false;
        File f = new File(filePath);
        if (f.delete()) {
            flag = true;
        }
        LOGGER.debug("deleteFile  : "+logUserId+ LOGGEREND);
        return flag;
    }
}

