/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.PreTendQueryImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceimpl.TenderCorriService;
import com.cptu.egp.eps.service.serviceinterface.TenderPostQueConfigService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.web.databean.PreTendQueryDtBean;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 *
 * @author parag
 */
public class PreTendQuerySrBean extends HttpServlet {


   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    PreTendQueryImpl preTendQuery = (PreTendQueryImpl) AppContext.getSpringBean("PreTendQueryImpl");
    private final TenderCorriService tenderCorriService = (TenderCorriService) AppContext.getSpringBean("TenderCorriService");
    private final UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    TenderPostQueConfigService tenderPostQueConfigService = (TenderPostQueConfigService) AppContext.getSpringBean("TenderPostQueConfigService");
    TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
    final Logger logger = Logger.getLogger(PostQueryGridSrBean.class);
    private String logUserId ="0";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        logger.debug("processRequest : "+logUserId+" Starts");
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            preTendQuery.setLogUserId(logUserId);
            tenderCorriService.setLogUserId(logUserId);
            userRegisterService.setUserId(logUserId);
            tenderPostQueConfigService.setLogUserId(logUserId);
        }
        
        String action = request.getParameter("action");
        int userId=0;
        byte suserTypeId=0;

        if (session.getAttribute("userTypeId") != null) {
            suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
        }
        if (session.getAttribute("userId") != null) {
            userId = Integer.parseInt(session.getAttribute("userId").toString());
        }

        logger.debug(" action  :: "+action);
        try {

            if("postQuery".equalsIgnoreCase(action) || "postQuestion".equalsIgnoreCase(action)){
                logger.debug("processRequest action : postQuery : "+logUserId+" Starts");
                int tenderId=0;
                String queryText = "";
                String querySign="";
                int queryId=0;
                //String commentText = "";

                if(request.getParameter("hidTenderId") != null){
                    tenderId = Integer.parseInt(request.getParameter("hidTenderId"));
                }
                if(request.getParameter("txtQuery") != null){
                    queryText = request.getParameter("txtQuery");
                }                
               /* if(request.getParameter("txtComment") != null){
                    commentText = request.getParameter("txtComment");
                }*/

                String  queryDate= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) ;

                StringBuilder forXmlStr=new StringBuilder();
                forXmlStr.append("<root>");
                forXmlStr.append("<tbl_PreTenderQuery tenderId = \""+tenderId+"\"");
                forXmlStr.append(" queryText = \""+URLEncoder.encode(queryText,"UTF-8")+"\"" );
                forXmlStr.append(" userId = \""+userId+"\"" );
                forXmlStr.append(" queryDt = \""+queryDate+"\"" );
                forXmlStr.append(" querySign = \""+querySign+"\"" );
                forXmlStr.append(" queryStatus = \"pending\"" );              
                if("postQuery".equalsIgnoreCase(action)){
                    forXmlStr.append(" queryType = \"Prebid\"" );
                }else{
                    forXmlStr.append(" queryType = \"Question\"" );
                }
                forXmlStr.append("  /> ");
                forXmlStr.append("</root>");
                boolean flag=false;
                String auditAction = "Post Queries";
                try {
                    List<CommonMsgChk> commonMsgChks = preTendQuery.InsPreTendQueryOperationDetailsByXML("insert",
                            "tbl_PreTenderQuery",forXmlStr.toString(),"");
                    queryId = commonMsgChks.get(0).getId();
                    
                    String setData = "queryId = "+queryId;
                    String whereCond = "";
                    if("postQuery".equalsIgnoreCase(action)){
                       whereCond = "userId="+userId+" and tenderId = "+tenderId+" and queryId=0 and docType = 'Prebid'";
                    }else{
                       whereCond = "userId="+userId+" and tenderId = "+tenderId+" and queryId=0 and docType = 'Question'";
                    }

                    List<CommonMsgChk> commonMsgChks1 = preTendQuery.InsPreTendQueryOperationDetailsByXML("updatetable",
                            "tbl_PreTenderQryDocs",setData,whereCond);
                    flag=true;

                } catch (Exception ex) {
                   logger.error("processRequest action : postQuery : "+logUserId+" : "+ex.toString());
                   auditAction = "Error in "+auditAction+" "+ex.getMessage();
                } finally{
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), userId, "userId", EgpModule.Pre_Tender_Query_Meeting.getName(), auditAction, "");
                }
                logger.debug("processRequest action : postQuery : "+logUserId+" Ends");
                if("postQuery".equalsIgnoreCase(action)){
                    response.sendRedirect("tenderer/PreTenTenderer.jsp?flag="+flag+"&action="+action+"&tenderId="+tenderId);
                }else{
                    response.sendRedirect("tenderer/QusTenderer.jsp?flag="+flag+"&action="+action+"&tenderId="+tenderId);
                }

            }
            else if("replyQuery".equalsIgnoreCase(action) || "replyQuestion".equalsIgnoreCase(action)){
                logger.debug("processRequest action : replyQuery : "+logUserId+" Starts");
                int queryId=0;
                String rephraseQuery = "";
                String replyText = "";
                String eSignature="";
                String rephraseQryText = "";
                String replyAction="";
                int tenderId=0;

                if(request.getParameter("hidTenderId") != null){
                    tenderId = Integer.parseInt(request.getParameter("hidTenderId"));
                }
                if(request.getParameter("hidQueryId") != null){
                    queryId = Integer.parseInt(request.getParameter("hidQueryId"));
                }
                if(request.getParameter("txtReply") != null){
                    replyText = request.getParameter("txtReply");
                }


                if ("replyQuery".equalsIgnoreCase(action)) {
                    if (request.getParameter("checkbox") != null) {
                        rephraseQuery = "Yes";
                        if (request.getParameter("txtRepQuery") != null) {
                            rephraseQryText = request.getParameter("txtRepQuery");
                        }
                    } else {
                        rephraseQuery = "No";
                    }

                    if (request.getParameter("cmbPostQuery") != null) {
                        replyAction = request.getParameter("cmbPostQuery");
                    }
                }else{
                    rephraseQuery = "No";
                    replyAction ="Replied";
                }

                String  repliedDate= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) ;
                StringBuilder forXmlStr=new StringBuilder();
                forXmlStr.append("<root>");

                forXmlStr.append("<tbl_PreTenderReply queryId = \""+queryId+"\"");
                forXmlStr.append(" rephraseQuery = \""+rephraseQuery+"\"" );
                forXmlStr.append(" replyText = \""+URLEncoder.encode(replyText,"UTF-8")+"\"" );
                forXmlStr.append(" userId = \""+userId+"\"" );
                forXmlStr.append(" repliedDate = \""+repliedDate+"\"" );
                forXmlStr.append(" eSignature = \""+eSignature+"\"" );
                forXmlStr.append(" rephraseQryText = \""+rephraseQryText+"\"" );
                forXmlStr.append(" replyAction = \""+replyAction+"\"" );
                forXmlStr.append("  /> ");

                forXmlStr.append("</root>");
                boolean flag=false;
                String auditAction = "Reply to Query";

                try {
                    List<CommonMsgChk> deleteInReply =  preTendQuery.InsPreTendQueryOperationDetailsByXML("insdel",
                            "tbl_PreTenderReply", "", " queryId = " + queryId);
                    List<CommonMsgChk> commonMsgChks = preTendQuery.InsPreTendQueryOperationDetailsByXML("insert",
                            "tbl_PreTenderReply",forXmlStr.toString(),"");
                    flag=true;
                } catch (Exception ex) {
                    logger.error("processRequest action : replyQuery : "+logUserId+" : "+ex.toString());
                    auditAction = "Error in "+auditAction+" "+ex.getMessage();
                }finally{
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), userId, "userId", EgpModule.Pre_Tender_Query_Meeting.getName(), auditAction, "");
                }
                logger.debug("processRequest action : replyQuery : "+logUserId+" Ends");
                if("replyQuery".equalsIgnoreCase(action)){
                    response.sendRedirect("officer/PreTenderMeeting.jsp?flag="+flag+"&replyAction="+replyAction+"&tenderId="+tenderId);
                }else{
                    response.sendRedirect("officer/AnsTenderer.jsp?flag="+flag+"&replyAction="+replyAction+"&tenderId="+tenderId);
                }
            }
            else if("getQueryData".equalsIgnoreCase(action)){
                PrintWriter out = response.getWriter();
               logger.debug("processRequest action : getQueryData : "+logUserId+" Starts");
               String auditAction = "View Query";
                try {

                    PreTendQueryDtBean preTendDtBean = new PreTendQueryDtBean();
                    int tenderId = 0;
                    int queryId = 0;
                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    if (request.getParameter("queryId") != null) {
                        queryId = Integer.parseInt(request.getParameter("queryId"));
                    }
                    String queryAction="";

                    if (request.getParameter("queryAction") != null) {
                        queryAction = request.getParameter("queryAction");
                    }
                    List<SPTenderCommonData> checkPE = preTendDtBean.getDataFromSP("CheckPE",tenderId,0);
                    boolean isPreBidPublished = preTendDtBean.isPrebidPublished(tenderId);
                    
                    boolean isUserISPE = false;
                    if (checkPE.size() > 0) {
                        if (userId == Integer.parseInt(checkPE.get(0).getFieldName1())) {
                            isUserISPE = true;
                        }
                    }
                    List<SPTenderCommonData>  getPreBidDates = preTendDtBean.getDataFromSP("GetPrebidDate",tenderId,0);
                    //java.text.SimpleDateFormat sd= new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");
                    java.text.SimpleDateFormat sd= new java.text.SimpleDateFormat("dd-MMM-yyyy HH:mm");

                    List<SPTenderCommonData> getConfigPrebid = preTendDtBean.getDataFromSP("GetConfigPrebid",tenderId,0);
                    java.util.Calendar calendar = java.util.Calendar.getInstance();
                    java.util.Calendar currentDate = java.util.Calendar.getInstance();
                    int preBidPublishDate =0;
                    java.util.Date edate = null;

                    if(getPreBidDates.size() > 0){
                        if(getPreBidDates != null){
                            if (getPreBidDates.get(0).getFieldName2() != null) {
                                 calendar.setTime(sd.parse(getPreBidDates.get(0).getFieldName2()));
                                 edate = sd.parse(getPreBidDates.get(0).getFieldName2());
                            }
                        }
                    }
                    
//                    if (getConfigPrebid.size() > 0) {
//                         if (getConfigPrebid != null) {
//                             if (getConfigPrebid.get(0).getFieldName4() != null) {
//                                    preBidPublishDate = Integer.parseInt(getConfigPrebid.get(0).getFieldName4());
//                                    calendar.add(Calendar.DATE, +preBidPublishDate);
//                             }
//                         }
//                    }

                    List<SPTenderCommonData> queryData = null;
                    if("getmyprebidquery".equalsIgnoreCase(queryAction) ){
                        queryData = preTendDtBean.getDataFromSP(""+queryAction,tenderId,userId);
                    }
                    else if("GetAllPreBidQuery".equalsIgnoreCase(queryAction)){
                        queryData = preTendDtBean.getDataFromSP(""+queryAction,tenderId,userId);
                    }
                    else{
                        queryData = preTendDtBean.getDataFromSP(""+queryAction,tenderId,0);
                    }
                    
                    StringBuffer strQueryData = new StringBuffer();
                    strQueryData.append("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                    strQueryData.append("<tr>");
                    strQueryData.append("<th width='4%' class='t-align-center'>Sl.  No.</th>");
                    strQueryData.append("<th class='t-align-left' width='60%'>Query</th>");
                    if("getmyprebidquery".equalsIgnoreCase(queryAction) || "GetAllPreBidQuery".equalsIgnoreCase(queryAction)){
                        strQueryData.append(" <th class='t-align-center' width='20%'>Status</th>");
                    }
                    strQueryData.append("<th class='t-align-center' >Action</th>");
                    strQueryData.append("</tr>");

                    
                    int count=0;
                     if (queryData.size() > 0) {
                        for (SPTenderCommonData qData : queryData) {
                            count++;
                            strQueryData.append("<tr>");
                            strQueryData.append("<td class='t-align-center' width='8%'>  " + count + "</td>");
                            strQueryData.append("<td class='t-align-left' width='60%'>" + URLDecoder.decode( qData.getFieldName2() ,"UTF-8") + "</td>");
                            if("getmyprebidquery".equalsIgnoreCase(queryAction) || "GetAllPreBidQuery".equalsIgnoreCase(queryAction)){
                                strQueryData.append("<td class='t-align-center' width='10%'>" + qData.getFieldName3()  + "</td>");
                            }
                            //strQueryData.append("<td class='t-align-left'>" + qData.getFieldName2() + "</td>");
                            String linkView = "";
                            if("reply".equalsIgnoreCase(qData.getFieldName3())){
                                if(suserTypeId == 3){
                                linkView  = "<a href='../officer/PreTenderReplyView.jsp?queryId="+qData.getFieldName1()+"&tenderId="+tenderId+"&cuserId="+qData.getFieldName6()+"&Prebid=Prebid'>View</a>";
                                }else{
                                    linkView  = "<a href='../tenderer/PreTenderReplyView.jsp?queryId="+qData.getFieldName1()+"&tenderId="+tenderId+"&cuserId="+qData.getFieldName6()+"&Prebid=Prebid'>View</a>";
                                }
                            }else{
                                if(!isPreBidPublished){                                    
                                    if(suserTypeId != 2 && isUserISPE && currentDate.getTime().before(edate)){
                                        linkView  = "<a href='../officer/PreTenderReply.jsp?queryId="+qData.getFieldName1()+"&tenderId="+tenderId+"&cuserId="+qData.getFieldName6()+"'>Process</a>";
                                    }else{
                                        linkView = "<a href='../tenderer/PreTenderReplyView.jsp?queryId="+qData.getFieldName1()+"&tenderId="+tenderId+"&cuserId="+qData.getFieldName6()+"'>View</a>";
                                    }
                                }
                                else{
                                        linkView = "<a href='../officer/PreTenderReplyView.jsp?queryId="+qData.getFieldName1()+"&tenderId="+tenderId+"&cuserId="+qData.getFieldName6()+"&Prebid=Prebid'>View</a>";
                                    }
                            }
                            strQueryData.append("<td class='t-align-center' >" + linkView + "</td>");
                            strQueryData.append("</tr>");
                        }
                    } else {
                        strQueryData.append("<tr>");
                        strQueryData.append("<td colspan='4' class='t-align-center'>No records found.</td>");
                        strQueryData.append("</tr>");
                    }                   
                    strQueryData.append("</table>");
                    out.print(strQueryData.toString());
                    
                } catch (Exception ex) {
                    logger.error("processRequest action : getQueryData : "+logUserId+" : "+ex.toString());
                    auditAction = "Error in "+auditAction+" "+ex.getMessage();
                }finally{
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), userId, "userId", EgpModule.Pre_Tender_Query_Meeting.getName(), auditAction, "");
                    out.flush();
                    out.close();
                }
                logger.debug("processRequest action : getQueryData : "+logUserId+" Ends");
            }
            else if("getQuestionData".equalsIgnoreCase(action)){
                logger.debug("processRequest action : getQuestionData : "+logUserId+" Starts");
                PrintWriter out = response.getWriter();
                try{
                    PreTendQueryDtBean preTendDtBean = new PreTendQueryDtBean();
                    int tenderId = 0;
                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    String queryAction="";
                    if (request.getParameter("queryAction") != null) {
                        queryAction = request.getParameter("queryAction");
                    }
                    List<SPTenderCommonData> queryData = null;
                    if("GetMyPreBidQuestion".equalsIgnoreCase(queryAction) ){
                        queryData = preTendDtBean.getDataFromSP(""+queryAction,tenderId,userId);
                    }
                    else{
                        queryData = preTendDtBean.getDataFromSP(""+queryAction,tenderId,0);
                    }
                    StringBuffer strQueryData = new StringBuffer();
                    strQueryData.append("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                    
                    strQueryData.append("<tr>");
                    strQueryData.append("<th width='4%' class='t-align-left'>S.No.</th>");
                    strQueryData.append("<th class='t-align-left' width='76%'>Query</th>");
                    if("GetMyPreBidQuestion".equalsIgnoreCase(queryAction)){
                        strQueryData.append(" <th class='t-align-left' width='10%'>Status</th>");
                    }
                    strQueryData.append("<th class='t-align-left' width='10%'>Action</th>");
                    strQueryData.append("</tr>");

                    int count=0;
                     if (queryData.size() > 0) {
                        for (SPTenderCommonData qData : queryData) {
                            count++;
                            strQueryData.append("<tr>");
                            strQueryData.append("<td class='t-align-center' width='8%'>  " + count + "</td>");
                            strQueryData.append("<td class='t-align-left' width='60%'>" + URLDecoder.decode(qData.getFieldName2() ,"UTF-8") + "</td>");
                            if("GetMyPreBidQuestion".equalsIgnoreCase(queryAction)){                                
                                strQueryData.append("<td class='t-align-center' width='10%'>" +qData.getFieldName3()+ "</td>");
                            }
                            String viewlink = "-";
                            if("GetPendingQuestion".equalsIgnoreCase(queryAction))
                            {
                                List<SPTenderCommonData> getClosingDt = preTendDtBean.getDataFromSP("GetClosingDt", tenderId, 0);                                                                
                                List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights",Integer.toString(userId),Integer.toString(tenderId));
                                if(!checkuserRights.isEmpty())
                                {
                                    if(!getClosingDt.isEmpty())
                                    {
                                        Date lastdate = DateUtils.convertStringtoDate(getClosingDt.get(0).getFieldName2(),"dd-MMM-yyyy hh:mm");
                                        Date todaysdate = new Date();
                                        if(lastdate.after(todaysdate))
                                        {
                                                viewlink = "<a href='../officer/QuertionReply.jsp?queryId=" + qData.getFieldName1() + "&tenderId=" + tenderId + "&cuserId=" + qData.getFieldName6() + "'>Reply</a>";
                                        }else{
                                            viewlink = "Reply";
                                        }
                                    }
                                }
                            }
                            if("Replied".equalsIgnoreCase(qData.getFieldName3())){
                                viewlink  = "<a href='../officer/QuestionReplyView.jsp?queryId="+qData.getFieldName1()+"&tenderId="+tenderId+"&cuserId="+qData.getFieldName6()+"' target='_blank'>View</a>";
                            }
                            strQueryData.append("<td class='t-align-center' >"+viewlink+"</td>");
                            strQueryData.append("</tr>");
                        }
                    } else {
                        strQueryData.append("<tr>");
                        strQueryData.append("<td colspan='4' class='t-align-center'>No records found.</td>");
                        strQueryData.append("</tr>");
                    }
                    strQueryData.append("</table>");
                    out.print(strQueryData.toString());

                    
                }catch(Exception e)
                {
                    logger.debug("Exception action : getQuestionData : "+logUserId+" : "+e);
                }
                /*logger.debug("processRequest action : getQuestionData : "+logUserId+" Starts");
                PrintWriter out = response.getWriter();
                try {

                    PreTendQueryDtBean preTendDtBean = new PreTendQueryDtBean();
                    int tenderId = 0;
                    int queryId = 0;
                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    if (request.getParameter("queryId") != null) {
                        queryId = Integer.parseInt(request.getParameter("queryId"));
                    }
                    String queryAction="";

                    if (request.getParameter("queryAction") != null) {
                        queryAction = request.getParameter("queryAction");
                    }
                    //---
                   // List<SPTenderCommonData> postQueryData = preTendDtBean.getDataFromSP("GetMyPreBidQuestion",tenderId,userId);
                    List<SPTenderCommonData> getPreBidDates = preTendDtBean.getDataFromSP("GetPrebidDate", tenderId, 0);
                    java.util.Calendar currentDate = java.util.Calendar.getInstance();
                    SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
                    //SimpleDateFormat sd1 = new SimpleDateFormat("dd/MM/yyyy");
                    //java.text.SimpleDateFormat sd = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");
                    java.util.Date edate = null;
                    boolean isQuestionStart = false;
                    if (getPreBidDates != null) {
                        if (getPreBidDates.size() > 0) {
                            if (getPreBidDates.get(0).getFieldName2() != null) {
                                edate = sd.parse(getPreBidDates.get(0).getFieldName2());
                                logger.debug(" end date :: " + edate);
                                if (currentDate.getTime().after(edate)) {
                                    isQuestionStart = true;
                                } else {
                                    isQuestionStart = false;
                                }
                            } else {
                                isQuestionStart = true;
                            }
                        }
                    }

                    List<SPTenderCommonData> getConfigQus = preTendDtBean.getDataFromSP("GetConfigPrebid",tenderId,0);
                    List<SPTenderCommonData> getClosingDt = preTendDtBean.getDataFromSP("GetClosingDt",tenderId,0);

                    java.util.Calendar calendar = java.util.Calendar.getInstance();
                    int ansEndDate = 0;

                    java.util.Date tenderClosingDate = null;

                    if (getClosingDt.size() > 0) {
                        if (getClosingDt != null) {
                            tenderClosingDate = sd.parse(getClosingDt.get(0).getFieldName2());
                        }
                    }

                    if (getConfigQus.size() > 0) {
                        if (getConfigQus != null) {
                            if(getClosingDt.size() > 0){
                                if(getClosingDt != null){
                                    if (getClosingDt.get(0).getFieldName2() != null) {
                                        calendar.setTime(sd.parse(getClosingDt.get(0).getFieldName2()));
                                        if (getConfigQus.get(0).getFieldName2() != null) {
                                            ansEndDate = Integer.parseInt(getConfigQus.get(0).getFieldName2());
                                            calendar.add(java.util.Calendar.DATE, -ansEndDate);
                                        }
                                    }
                                }
                            }
                        }
                    }

                                      
                    //--

                    List<SPTenderCommonData> queryData = null;
                    if("GetMyPreBidQuestion".equalsIgnoreCase(queryAction) ){
                        queryData = preTendDtBean.getDataFromSP(""+queryAction,tenderId,userId);
                    }
                    else{
                        queryData = preTendDtBean.getDataFromSP(""+queryAction,tenderId,0);
                    }

                    StringBuffer strQueryData = new StringBuffer();
                    strQueryData.append("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                    strQueryData.append("<tr>");
                    strQueryData.append("<th width='8%' class='t-align-left'>Sr.  No.</th>");
                    strQueryData.append("<th class='t-align-left' width='60%'>Query</th>");
                    if("GetMyPreBidQuestion".equalsIgnoreCase(queryAction)){
                        strQueryData.append(" <th class='t-align-left' width='20%'>Status</th>");
                    }
                    strQueryData.append("<th class='t-align-left' >Action</th>");
                    strQueryData.append("</tr>");


                    int count=0;
                     if (queryData.size() > 0) {
                        for (SPTenderCommonData qData : queryData) {
                            count++;
                            strQueryData.append("<tr>");
                            strQueryData.append("<td class='t-align-left' width='8%'>  " + count + "</td>");
                            strQueryData.append("<td class='t-align-left' width='60%'>" + qData.getFieldName2()  + "</td>");
                            if("GetMyPreBidQuestion".equalsIgnoreCase(queryAction)){
                                strQueryData.append("<td class='t-align-left' width='20%'>" + qData.getFieldName3()  + "</td>");
                            }
                            //strQueryData.append("<td class='t-align-left'>" + qData.getFieldName2() + "</td>");
                            String linkView = "";
                            String ruleAlert="";  
                            
                            if("reply".equalsIgnoreCase(qData.getFieldName3())){
                                linkView  = "<a href='../officer/QuestionReplyView.jsp?queryId="+qData.getFieldName1()+"&tenderId="+tenderId+"&cuserId="+qData.getFieldName6()+"' target='_blank'>View</a>";
                            }
                            else{
                                if(isQuestionStart) {
                                    if (ansEndDate > 0) {
                                        if (calendar.getTime().before(currentDate.getTime())) {
                                            ruleAlert = "onclick=\"ruleAlert('"+new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").format(calendar.getTime())+"');\"";                                           
                                            linkView = "<a href='#' " + ruleAlert + " >Process</a>";
                                        }
                                        else{
                                            if (suserTypeId != 2) {
                                                linkView = "<a href='../officer/QuertionReply.jsp?queryId=" + qData.getFieldName1() + "&tenderId=" + tenderId + "&cuserId=" + qData.getFieldName6() + "'>Process</a>";
                                            } else {
                                                linkView = " - ";
                                            }
                                        }
                                    }                                    
                                    else if(tenderClosingDate.before(currentDate.getTime())){
                                        if (suserTypeId != 2) {
                                            linkView = "<a href='../officer/QuertionReply.jsp?queryId=" + qData.getFieldName1() + "&tenderId=" + tenderId + "&cuserId=" + qData.getFieldName6() + "'>Process</a>";
                                        } else {
                                            linkView = " - ";
                                        }
                                    }else{
                                         linkView = " - ";
                                    }
                                }else{
                                    linkView = " - ";
                                }
                            }
                                                            
                                                     
                            strQueryData.append("<td class='t-align-left' >" + linkView + "</td>");
                            strQueryData.append("</tr>");
                        }
                    } else {
                        strQueryData.append("<tr>");
                        strQueryData.append("<td colspan='4' class='t-align-center'>No records found.</td>");
                        strQueryData.append("</tr>");
                    }
                    strQueryData.append("</table>");
                    out.print(strQueryData.toString());

                } catch (Exception e) {
                    logger.debug("processRequest action : getQuestionData : "+logUserId+" Ends");
                }finally{
                    out.flush();
                    out.close();
                }
                logger.debug("processRequest action : getQuestionData : "+logUserId+" Ends");
            */}
            else if("docData".equalsIgnoreCase(action)){
                logger.debug("processRequest action : docData : "+logUserId+" Starts");
                PrintWriter out = response.getWriter();
                try {
                    PreTendQueryDtBean preTendDtBean = new PreTendQueryDtBean();
                    int tenderId = 0;
                    int queryId = 0;
                    String viewAction="";

                    // for bidder
                    int formId=0;

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    if (request.getParameter("queryId") != null) {
                        queryId = Integer.parseInt(request.getParameter("queryId"));
                    }
                    String docAction="PreTenderQueDocs";

                    if (request.getParameter("docAction") != null) {
                        docAction = request.getParameter("docAction");
                    }
                    if (request.getParameter("viewAction") != null) {
                        viewAction = request.getParameter("viewAction");
                    }
                    if (request.getParameter("formId") != null) {
                        formId = Integer.parseInt(request.getParameter("formId"));
                    }

                    List<SPTenderCommonData> checkPE = preTendDtBean.getDataFromSP("CheckPE",tenderId,0);
                    boolean isPreBidPublished = preTendDtBean.isPrebidPublished(tenderId);
                    List<SPTenderCommonData> getDocFeesModeData = preTendDtBean.getDataFromSP("GetDocFeesMode",tenderId,0);
                    boolean isDocFeesPaid=false;

                    /*boolean isUserISPE = false;
                    if (checkPE.size() > 0) {
                        if (userId == Integer.parseInt(checkPE.get(0).getFieldName1())) {
                            isUserISPE = true;
                        }
                    }*/

                    List<SPTenderCommonData> getUserPaidDocFees=null;
                   // boolean isUserPaidDocFees = false;

                    if ("PreTenderMetDocs".equalsIgnoreCase(docAction) || ("PreTenderReplyDocs".equalsIgnoreCase(docAction))) {
                        if (suserTypeId == 2) {
                            if (getDocFeesModeData != null) {
                                if (getDocFeesModeData.get(0).getFieldName1() != null) {
                                    if ("paid".equalsIgnoreCase(getDocFeesModeData.get(0).getFieldName1())) {
                                        getUserPaidDocFees = preTendDtBean.getDataFromSP("CheckDocFeesPaid", tenderId, userId);
                                        if (getUserPaidDocFees != null) {
                                            if (getUserPaidDocFees.get(0).getFieldName1() != null) {
                                                isDocFeesPaid = true;
                                            } else {
                                                isDocFeesPaid = false;
                                            }
                                        } else {
                                            isDocFeesPaid = false;
                                        }
                                    } else {
                                        isDocFeesPaid = true;
                                    }
                                } else {
                                    isDocFeesPaid = true;
                                }
                            } else {
                                isDocFeesPaid = true;
                            }
                        } else {
                            isDocFeesPaid = true;
                        }
                    }else{
                       isDocFeesPaid = true;
                    }

                    StringBuffer strDocData = new StringBuffer();
                    strDocData.append("<table id='tblresponseDoc' name='tblresponseDoc' width='100%' cellspacing='0' class='tableList_1 t_space'>");
                    strDocData.append("<tr>");
                    strDocData.append("<th width='4%' class='t-align-center'>Sl.  No.</th>");
                    strDocData.append("<th class='t-align-left' width='23%'>File Name</th>");
                    strDocData.append(" <th class='t-align-left' width='28%'>File Description</th>");
                    strDocData.append("<th class='t-align-center' width='7%'>File Size <br />(in KB)</th>");
                    strDocData.append("<th class='t-align-center' width='18%'>Action</th>");
                    strDocData.append("</tr>");

                    int docCnt = 0;
                    boolean hideDoc=false;
                    List<SPTenderCommonData> getPreBidDocumentData = null;

                    if("PreTenderMetDocs".equalsIgnoreCase(docAction)){
                        getPreBidDocumentData = preTendDtBean.getDataFromSP(""+docAction,tenderId,0);
                        if("view".equalsIgnoreCase(viewAction)){
                            hideDoc = true;
                        }
                    }
                    if("ViewPreTenderMetsDocs".equalsIgnoreCase(docAction)){
                        getPreBidDocumentData = preTendDtBean.getDataFromSP(""+docAction,tenderId,0);
                        if("view".equalsIgnoreCase(viewAction)){
                            hideDoc = true;
                        }
                    }
                    if("PreTenderReplyMetDocs".equalsIgnoreCase(docAction)){
                        getPreBidDocumentData = preTendDtBean.getDataFromSP(""+docAction,tenderId,0);
                        if("view".equalsIgnoreCase(viewAction)){
                            hideDoc = true;
                        }
                    }
                    else if("PreTenderReplyDocs".equalsIgnoreCase(docAction)){
                        getPreBidDocumentData = preTendDtBean.getDataFromSP(""+docAction,queryId,0);
                        if(suserTypeId == 2){
                            hideDoc = true;
                        }
                    }else if("QuestionDocsMore".equalsIgnoreCase(docAction)){
                        getPreBidDocumentData = preTendDtBean.getDataFromSP(""+docAction,queryId,0);
                        if(suserTypeId == 2 || "view".equalsIgnoreCase(viewAction)){
                            hideDoc = true;
                        }
                    }
                    else if("QuestionReplyDocs".equalsIgnoreCase(docAction)){
                        getPreBidDocumentData = preTendDtBean.getDataFromSP(""+docAction,queryId,0);
                        if(suserTypeId == 2 || "view".equalsIgnoreCase(viewAction)){
                            hideDoc = true;
                        }
                    }
                    else if("QuestionDocsBy".equalsIgnoreCase(docAction)){
                        getPreBidDocumentData = preTendDtBean.getDataFromSP(""+docAction,tenderId,userId);
                    }
                    else if("GetBidderDocuments".equalsIgnoreCase(docAction)){
                         getPreBidDocumentData = preTendDtBean.getDataFromSP(""+docAction,userId,formId);
                         hideDoc = true;
                    }
                    else{
                        getPreBidDocumentData = preTendDtBean.getDataFromSP(""+docAction,tenderId,userId);
                    }
                    logger.debug(" size of getPreBidDocumentData :: "+getPreBidDocumentData.size());
                    if (getPreBidDocumentData.size() > 0) {
                        for (SPTenderCommonData preDocData : getPreBidDocumentData) {
                            docCnt++;
                            strDocData.append("<tr>");
                            strDocData.append("<td class='t-align-center'>" + docCnt + "</td>");
                            
                            if("GetBidderDocuments".equalsIgnoreCase(docAction)){
                                strDocData.append("<td class='t-align-center'>" + preDocData.getFieldName2() + "</td>");
                                strDocData.append("<td class='t-align-left'>" + preDocData.getFieldName4() + "</td>");
                            }else{
                              strDocData.append("<td class='t-align-left'>" + preDocData.getFieldName1() + "</td>");
                              strDocData.append("<td class='t-align-center'>" + preDocData.getFieldName2() + "</td>");
                            }
                            
                            strDocData.append("<td class='t-align-center'>" + Long.parseLong(preDocData.getFieldName3())/1024  + "</td>");
                            String linkDownloadDoc = "";
                                    //"<a href='#' onclick=\"downloadFile('" + preDocData.getFieldName1() + "','" + preDocData.getFieldName3() + "','" + tenderId + "')\" ><img src='../resources/images/Dashboard/Download.png' alt='Download' /></a>";
                            String linkDeleteDoc = "";
                            if(!isPreBidPublished && !hideDoc){
                                if(!docAction.equalsIgnoreCase("ViewPreTenderMetsDocs")){
                               linkDeleteDoc = "<a href='#' onclick=\"deleteFile('" + preDocData.getFieldName1() + "','" + preDocData.getFieldName4() + "','" + tenderId + "')\" ><img src='"+request.getContextPath()+"/resources/images/Dashboard/Delete.png' alt='Remove' /></a>";
                            }
                            }
                            if("GetBidderDocuments".equalsIgnoreCase(docAction)){
                                linkDownloadDoc = "<a href='#' onclick=\"downloadFile('" + preDocData.getFieldName2() + "','" + preDocData.getFieldName3() + "','" + tenderId + "')\" ><img src='"+request.getContextPath()+"/resources/images/Dashboard/Download.png' alt='Download' /></a>";
                                strDocData.append("<td class='t-align-center' width='18%'>" + linkDownloadDoc + "&nbsp;" + linkDeleteDoc + "</td>");
                            }
                            else if(isDocFeesPaid){
                                linkDownloadDoc = "<a href='#' onclick=\"downloadFile('" + preDocData.getFieldName1() + "','" + preDocData.getFieldName3() + "','" + tenderId + "')\" ><img src='"+request.getContextPath()+"/resources/images/Dashboard/Download.png' alt='Download' /></a>";
                                strDocData.append("<td class='t-align-center' width='18%'>" + linkDownloadDoc + "&nbsp;" + linkDeleteDoc + "</td>");
                            }

                            strDocData.append("</tr>");
                        }
                    } else {
                        strDocData.append("<tr>");
                        strDocData.append("<td colspan='5' class='t-align-center'>No records found.</td>");
                        strDocData.append("</tr>");
                    }
                    strDocData.append("</table>");
                        out.print(strDocData.toString());
                } catch (Exception ex) {
                    logger.error("processRequest action : docData : "+logUserId+" : "+ex.toString());
                }finally{
                    out.flush();
                    out.close();
                }
                logger.debug("processRequest action : docData : "+logUserId+" Ends");
            }
            else if("publishQuery".equalsIgnoreCase(action)){
                logger.debug("processRequest action : publishQuery : "+logUserId+" Starts");
                int tenderId=0;               
                int queryId=0;
                boolean flag=false;
                String txtRemarks="";

                 if(request.getParameter("hidTenderId") != null){
                    tenderId = Integer.parseInt(request.getParameter("hidTenderId"));
                }                 
                if(request.getParameter("txtRemarks") != null){
                    txtRemarks = request.getParameter("txtRemarks");
                }

                String  actionDate= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) ;

                StringBuilder forXmlStr=new StringBuilder();
                forXmlStr.append("<root>");

                forXmlStr.append("<tbl_PreTenderAuditTrail tenderId = \""+tenderId+"\"");
                forXmlStr.append(" actionDate = \""+actionDate+"\"" );
                forXmlStr.append(" actionBy = \""+userId+"\"" );
                forXmlStr.append(" remarks = \""+txtRemarks+"\"" );
                forXmlStr.append("  /> ");

                forXmlStr.append("</root>");
                             
                try{
                    List<CommonMsgChk> commonMsgChks = preTendQuery.InsPreTendQueryOperationDetailsByXML("insert",
                            "tbl_PreTenderAuditTrail",forXmlStr.toString(),"");

                    String setData = "prebidStatus = 'Approved' , eSignature = ' "+SHA1HashEncryption.encodeStringSHA1(txtRemarks)+" ' " ;
                    String whereCond = "tenderId = " + tenderId;
                    
                    List<CommonMsgChk> commonMsgChks1 = preTendQuery.InsPreTendQueryOperationDetailsByXML("updatetable",
                            "tbl_PreTenderMetDocs", setData, whereCond);
                    flag = true;
                    sendPreTendMeetingPublishMail(String.valueOf(tenderId));
                }catch(Exception ex){
                   logger.error("processRequest : "+logUserId+" : "+ex.toString());
                }
                logger.debug("processRequest action : publishQuery : "+logUserId+" Ends");
                response.sendRedirect("officer/PreTenderMeeting.jsp?flag="+flag+"&replyAction="+action+"&tenderId="+tenderId);
            }

        }catch (Exception e) {
                    logger.error(" Error in PreTendQuerySrBean ::"+e);
                }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void sendPreTendMeetingPublishMail(String tenderid) {

        logger.debug("sendPreTendMeetingPublishMail : " + logUserId + "starts");
        String[] tmp = tenderCorriService.getEmailSMSforCorri(tenderid);
        String refNo = tmp[2];
        String peName = tmp[3];
        String closingDate = tmp[4];
        String tendBrief = tmp[5];
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        if (tmp[0] != null) {
            String mails[] = tmp[0].split(",");
            MailContentUtility mailContentUtility = new MailContentUtility();
            String mailText = mailContentUtility.getPreTenderMeetingPublishMailContent(tenderid, refNo,peName,closingDate,tendBrief);
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String msgBox = msgBoxContentUtility.getPreTenMeetingPubMsgBox(tenderid, refNo,peName,closingDate,tendBrief);
            sendMessageUtil.setEmailSub("e-GP: Pretender meeting MOM published");
            sendMessageUtil.setEmailMessage(mailText);
            for (int i = 0; i < mails.length; i++) {
                String mail[] = {mails[i]};
                userRegisterService.contentAdmMsgBox(mails[i], XMLReader.getMessage("msgboxfrom"), sendMessageUtil.getEmailSub(), msgBox);
                sendMessageUtil.setEmailTo(mail);
                sendMessageUtil.sendEmail();
                mail=null;
            }
            mailContentUtility = null;
            msgBoxContentUtility=null;
        }
        sendMessageUtil=null;
        logger.debug("sendPreTendMeetingPublishMail : " + logUserId + "ends");
    }
    }

