/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.webservices;

import com.cptu.egp.eps.dao.storedprocedure.CorrigendumInfoDetails;
import com.cptu.egp.eps.service.serviceinterface.CorrigendumDetailsService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.WebServiceUtil;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import org.apache.log4j.Logger;

/**
 * web service for Amendement/Corrigendum details.
 * @author Sreenu
 */
@WebService()
@XmlSeeAlso({CorrigendumInfoDetails.class})
public class Amendments {

    private static final Logger LOGGER = Logger.getLogger(Amendments.class);
    private String logUserId = "0";
    private static final int WEB_SERVICE_ID = 2;
    private static final String SEARCH_SERVICE_NAME = "CorrigendumDetailsService";

    /***
     * 
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /***
     * This method gives the List of String Objects of <b>Amendement/Corrigendum Details</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.Amendment ID<br>
     * 2.Amendment No.<br>
     * 3.Amendment Text<br>
     * 4.Filed name<br>
     * 5.Old value<br>
     * 6.New value<br>
     *
     * @param String username
     * @param String password
     * @return WSResponseObject with list data as List<String>
     */
    @WebMethod(operationName = "getApprovedAmendmentDetails")
    public WSResponseObject getApprovedAmendmentDetails(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getApprovedAmendmentDetails : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = "";
        //insert record in log table
        WebServiceUtil.insertLogRecord(WEB_SERVICE_ID, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WEB_SERVICE_ID,
                SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> corrigendumDetailsList = new ArrayList<String>();
            try {
                corrigendumDetailsList = getRequiredAmendmentList();
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getFinancialYears : " + logUserId + e);
            }
            if (corrigendumDetailsList != null) {
                if (corrigendumDetailsList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(corrigendumDetailsList);
                }
            }
        }
        LOGGER.debug("getApprovedAmendmentDetails : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method returns the required list for response object.
     * @return List<String>
     */
    private List<String> getRequiredAmendmentList() {
        LOGGER.debug("getRequiredAmendmentList : " + logUserId + WSResponseObject.LOGGER_START);
        List<CorrigendumInfoDetails> corrigendumDetailsList = new ArrayList<CorrigendumInfoDetails>();
        List<String> reqList = new ArrayList<String>();
        CorrigendumDetailsService corrigendumDetailService =
                (CorrigendumDetailsService) AppContext.getSpringBean(SEARCH_SERVICE_NAME);
        corrigendumDetailsList = corrigendumDetailService.getAllCorrigendumDetails();
        if (corrigendumDetailsList != null) {
            for (CorrigendumInfoDetails infoDetails : corrigendumDetailsList) {
                StringBuilder sbCSV = new StringBuilder();
                sbCSV.append(infoDetails.getCorriDetailId());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(infoDetails.getTenderId());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(infoDetails.getAmendmentText());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(infoDetails.getFiledName());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(infoDetails.getOldValue());
                sbCSV.append(WSResponseObject.APPEND_CHAR);
                sbCSV.append(infoDetails.getNewValue());
                //sbCSV.append(WSResponseObject.APPEND_CHAR);   Commended by Sudhir 08072011
                reqList.add(sbCSV.toString());
            }
        }
        LOGGER.debug("getRequiredAmendmentList : " + logUserId + WSResponseObject.LOGGER_START);
        return reqList;
    }
}
