/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.model.view.VwGetSbDevPartner;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;

/**
 *
 * @author Administrator
 */
public class ScBankDevPartUserDtBean {

    private int userId;
    private String fullname;
    private String nationalID;
    private String office;
    private String mobileNum;
    private String mailId;
    private String branch;
    private int partnerId;
    private String isAdmin;
    private int sdBankDevId;

    public String getIsAdmin()
    {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin)
    {
        this.isAdmin = isAdmin;
    }

    public int getSdBankDevId()
    {
        return sdBankDevId;
    }

    public void setSdBankDevId(int sdBankDevId)
    {
        this.sdBankDevId = sdBankDevId;
    }
    
    public String getFullname()
    {
        return fullname;
    }

    public void setFullname(String fullname)
    {
        this.fullname = fullname;
    }

    public String getMobileNum()
    {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum)
    {
        this.mobileNum = mobileNum;
    }

    public String getNationalID()
    {
        return nationalID;
    }

    public void setNationalID(String nationalID)
    {
        this.nationalID = nationalID;
    }

    public String getOffice()
    {
        return office;
    }

    public void setOffice(String office)
    {
        this.office = office;
    }

    public String getBranch()
    {
        return branch;
    }

    public void setBranch(String branch)
    {
        this.branch = branch;
    }

    public String getMailId()
    {
        return mailId;
    }

    public void setMailId(String mailId)
    {
        this.mailId = mailId;
    }

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public int getPartnerId()
    {
        return partnerId;
    }

    public void setPartnerId(int partnerId)
    {
        this.partnerId = partnerId;
    }

    public void populateInfo()
    {
        ScBankDevpartnerService bankService=(ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
        VwGetSbDevPartner sbDevPartner= bankService.getUserInfo(getUserId());
        
        setMailId(sbDevPartner.getId().getEmailId());
        setBranch(sbDevPartner.getId().getSbDevelopName());
        setFullname(sbDevPartner.getId().getFullName());
        setMobileNum(sbDevPartner.getId().getMobileNo());
        setNationalID(sbDevPartner.getId().getNationalId());
        setPartnerId(sbDevPartner.getId().getPartnerId());
        setIsAdmin(sbDevPartner.getId().getIsAdmin());
        setSdBankDevId(sbDevPartner.getId().getSbankDevelopId());
    }
}
