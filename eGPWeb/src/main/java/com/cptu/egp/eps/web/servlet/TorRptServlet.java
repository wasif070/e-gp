/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.service.serviceimpl.ReportCreationService;
import com.cptu.egp.eps.web.servicebean.GenreatePdfCmd;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.util.FileSystemUtils;

/**
 *
 * @author nishit
 */
public class TorRptServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String tenderId = "";
            String lotId = "";
            String action = "";
            boolean isDevelopment = false;
            boolean isStaging = false;
            boolean isProduction = false;
            boolean isTraining = false;
            String strDriveLetter =  XMLReader.getMessage("driveLatter");
            String path;
            String reqURL = request.getRequestURL().toString();
            if(reqURL.contains("development.eprocure.gov.bd") || reqURL.contains("10.1.3.21")){
                isDevelopment = true;
            }
            if(reqURL.contains("staging.eprocure.gov.bd") || reqURL.contains("10.1.4.21")){
                isStaging = true;
            }
            if(reqURL.contains("training.eprocure.gov.bd") || reqURL.contains("10.1.40.25")){
                isTraining = true;
            }
            if(reqURL.contains("www.eprocure.gov.bd") || reqURL.contains("10.1.2.26") || reqURL.contains("10.1.2.27")){
                isProduction = true;
            }

          /*  if(isDevelopment || isTraining)
            {
                strDriveLetter = "D:";
            }
            else if(isStaging)
            {
                strDriveLetter = "E:";
            }
            else if(isProduction)
            {
                strDriveLetter = "\\\\egpprdblog\\AppSharedDrive";
            }
*/
            path = strDriveLetter+"/eGP/PDF/";
            if (request.getParameter("action") != null) {
                action = request.getParameter("action");
            }
            if ("TOR1".equalsIgnoreCase(action)) {
                tenderId = request.getParameter("tenderId");
                lotId = request.getParameter("lotId");
                String requrl = request.getHeader("referer");
                int queryLen = requrl.indexOf("?");
                reqURL = requrl.substring(0, queryLen);
                String reqQuery = "tenderid=" + tenderId + "&lotId=" + lotId;
                reportGeneration(tenderId, lotId, "TOR1_" + tenderId, reqURL, reqQuery, response,path,tenderId + "_" + lotId);
            }
            if ("TOR2".equalsIgnoreCase(action)) {
                tenderId = request.getParameter("tenderId");
                lotId = request.getParameter("lotId");
                String requrl = request.getHeader("referer");
                int queryLen = requrl.indexOf("?");
                reqURL = requrl.substring(0, queryLen);
                String reqQuery = "tenderid=" + tenderId + "&lotId=" + lotId + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                reportGeneration(tenderId, lotId, "TOR2_" + tenderId, reqURL, reqQuery, response,path,tenderId + "_" + lotId);
            }
            if ("TER1".equalsIgnoreCase(action)) {
                tenderId = request.getParameter("tenderId");
                lotId = request.getParameter("lotId");
                String requrl = request.getHeader("referer");
                int queryLen = requrl.indexOf("?");
                reqURL = requrl.substring(0, queryLen);
                //String reqQuery = "tenderid=" + tenderId + "&lotId=" + lotId + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                String reqQuery = requrl.substring(queryLen+1, requrl.length()) + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                reportGeneration(tenderId, lotId, "TER1_" + tenderId, reqURL, reqQuery, response,path,tenderId + "_" + lotId+"_"+request.getSession().getAttribute("userId"));
            }
            if ("TER2".equalsIgnoreCase(action)) {
                tenderId = request.getParameter("tenderId");
                lotId = request.getParameter("lotId");
                String requrl = request.getHeader("referer");
                int queryLen = requrl.indexOf("?");
                reqURL = requrl.substring(0, queryLen);
                //String reqQuery = "tenderid=" + tenderId + "&lotId=" + lotId + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                String reqQuery = requrl.substring(queryLen+1, requrl.length()) + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                reportGeneration(tenderId, lotId, "TER2_" + tenderId, reqURL, reqQuery, response,path,tenderId + "_" + lotId+"_"+request.getSession().getAttribute("userId"));
            }
            if ("TER3".equalsIgnoreCase(action)) {
                tenderId = request.getParameter("tenderId");
                lotId = request.getParameter("lotId");
                String requrl = request.getHeader("referer");
                int queryLen = requrl.indexOf("?");
                reqURL = requrl.substring(0, queryLen);
                //String reqQuery = "tenderid=" + tenderId + "&lotId=" + lotId + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                String reqQuery = requrl.substring(queryLen+1, requrl.length()) + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                reportGeneration(tenderId, lotId, "TER3_" + tenderId, reqURL, reqQuery, response,path,tenderId + "_" + lotId+"_"+request.getSession().getAttribute("userId"));
            }
            if ("TER4".equalsIgnoreCase(action)) {
                tenderId = request.getParameter("tenderId");
                lotId = request.getParameter("lotId");
                String requrl = request.getHeader("referer");
                int queryLen = requrl.indexOf("?");
                reqURL = requrl.substring(0, queryLen);
                //String reqQuery = "tenderid=" + tenderId + "&lotId=" + lotId + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                String reqQuery = requrl.substring(queryLen+1, requrl.length()) + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                reportGeneration(tenderId, lotId, "TER4_" + tenderId, reqURL, reqQuery, response,path,tenderId + "_" + lotId+"_"+request.getSession().getAttribute("userId"));
            }
            if ("IndReport".equalsIgnoreCase(action)) {
                tenderId = request.getParameter("tenderId");
                lotId = request.getParameter("formId");
                String requrl = request.getHeader("referer");
                int queryLen = requrl.indexOf("?");
                reqURL = requrl.substring(0, queryLen);
                String reqQuery = "tenderId=" + tenderId + "&formId=" + lotId;
                reportGeneration(tenderId, lotId, "Indvidual_Report_" + tenderId, reqURL, reqQuery, response,path,tenderId + "_" + lotId);
            }
            if ("ComReport".equalsIgnoreCase(action)) {
                tenderId = request.getParameter("tenderId");
                lotId = request.getParameter("formId");
                String requrl = request.getHeader("referer");
                int queryLen = requrl.indexOf("?");
                reqURL = requrl.substring(0, queryLen);
                String reqQuery = "tenderId=" + tenderId + "&formId=" + lotId;
                reportGeneration(tenderId, lotId, "Comparative_Report_" + tenderId, reqURL, reqQuery, response,path,tenderId + "_" + lotId);
            }
            if("checkPriceBid".equalsIgnoreCase(action)){
                PrintWriter out = response.getWriter();                
                out.print(checkPriceBid(request.getParameter("tId"), request.getParameter("lId")));
                out.flush();
                out.close();
            }
            if ("pdfL1".equals(action)) {                
                String requrl = request.getHeader("referer");
                tenderId = request.getParameter("tenderId");
                lotId = request.getParameter("repId");
                int queryLen = requrl.indexOf("?");
                reqURL = requrl.substring(0, queryLen);                
                String reqQuery = requrl.substring(queryLen+1, requrl.length()) + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                reportGeneration(tenderId, lotId, "PCRL1_" + tenderId, reqURL, reqQuery, response,path,tenderId + "_" + lotId);
            }
            if ("pdfT1L1".equals(action)) {
                String requrl = request.getHeader("referer");
                tenderId = request.getParameter("tenderId");
                lotId = request.getParameter("repId");
                int queryLen = requrl.indexOf("?");
                reqURL = requrl.substring(0, queryLen);
                String reqQuery = requrl.substring(queryLen+1, requrl.length()) + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                reportGeneration(tenderId, lotId, "T1L1Report_" + tenderId, reqURL, reqQuery, response,path,tenderId + "_" + lotId);
            }
            if ("pdfT1".equals(action)) {
                String requrl = request.getHeader("referer");
                tenderId = request.getParameter("tenderId");                
                int queryLen = requrl.indexOf("?");
                reqURL = requrl.substring(0, queryLen);
                String reqQuery = requrl.substring(queryLen+1, requrl.length()) + "&tendrepUserId=" + request.getSession().getAttribute("userId");
                reportGeneration(tenderId, lotId, "T1Report_" + tenderId, reqURL, reqQuery, response,path,tenderId);
            }
            if ("PTRpt".equals(action)) {
                String requrl = request.getHeader("referer");
                tenderId = request.getParameter("tenderId");
                reqURL = requrl;
                String reqQuery = request.getQueryString()+"&suserId="+ request.getSession().getAttribute("userId");
                File file = new File(path+"/Report/"+request.getParameter("financialyear"));
                boolean bool = file.exists() ?  FileSystemUtils.deleteRecursively(file) : true;
                reportGeneration(request.getParameter("financialyear"), lotId, "ProcuremntTransactionReport_" + request.getParameter("financialyear"), reqURL, reqQuery, response,path,request.getParameter("financialyear"));
            }
            if ("PTRpt2".equals(action)) {
                String requrl = request.getHeader("referer");
                tenderId = request.getParameter("tenderId");
                reqURL = requrl;
                String reqQuery = request.getQueryString()+"&suserId="+ request.getSession().getAttribute("userId");
                File file = new File(path+"/Report/"+request.getParameter("financialyear"));
                boolean bool = file.exists() ?  FileSystemUtils.deleteRecursively(file) : true;
                reportGeneration(request.getParameter("financialyear"), lotId, "ProcuremntTranOverallReport", reqURL, reqQuery, response,path,request.getParameter("financialyear"));
            }
            if ("saveCalc".equals(action)) {
                String requrl = request.getHeader("referer");
                tenderId = request.getParameter("tenderId");
                reqURL = requrl;
                String reqQuery = request.getQueryString()+"&suserId="+ request.getSession().getAttribute("userId");
                File file = new File(path+"/Report/SaveCalculation");
                boolean bool = file.exists() ?  FileSystemUtils.deleteRecursively(file) : true;
                reportGeneration(request.getParameter("financialyear"), lotId, "SavingCalculationReport", reqURL, reqQuery, response,path,"SaveCalculation");
            }
            if ("annualProcRpt".equals(action)) {
                String requrl = request.getHeader("referer");
                tenderId = request.getParameter("tenderId");
                reqURL = requrl;
                String reqQuery = request.getQueryString()+"&suserId="+ request.getSession().getAttribute("userId");
                File file = new File(path+"/Report/"+request.getParameter("financialyear"));
                boolean bool = file.exists() ?  FileSystemUtils.deleteRecursively(file) : true;
                reportGeneration(request.getParameter("financialyear"), lotId, "AnnualProcReport", reqURL, reqQuery, response,path,request.getParameter("financialyear"));
            }
        } catch (Exception e) {
            System.out.println("Error : " + e);
        } finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void reportGeneration(String tenderId, String lotId, String fName, String reqURL, String reqQuery, HttpServletResponse response,String path,String id) throws Exception {
        boolean flag = false;
        String folderName = "Report";
        String fileName = fName;
        GenreatePdfCmd genreatePdfCmd = new GenreatePdfCmd();
        File file = new File(path + "\\" + folderName + "\\" + id + "\\" + fileName + ".pdf");
        
        /*
        if (!file.exists()) {
            flag = genreatePdfCmd.genrateCmd(reqURL, reqQuery, folderName, id, fileName);
        }
        */

        // Dohatec start : 22 April 2015 : 11:20 AM : Md. Wahid Abdullah
        // Issue Details : TER1 and TER2 have save as PDF option but TER3(Financial Evaluation) and TER3(Final evaluation) don't have "Save as PDF" option. Thus TER3 and TER4 can't be downloaded as PDF.
        /* If TER3 and TER4 Report already exists in the respective path then New TER3 and TER4 Report does not save in that path.
         * That's why it shows the previous TER3 and TER4 Report although New TER3 and TER4 Report is quit Differ from the previous one.
         * In New code first of all it check that TER3 and TER4 pdf file is exists or not. If file is exists then it delete it and after that it save new
         * TER3 and TER4 Report in that path. As a result it always shows the latest TER3 and TER4 Report.
         */
        if(("TER3_" + tenderId).equalsIgnoreCase(fName) || ("TER4_" + tenderId).equalsIgnoreCase(fName)) {
            if (file.exists()) {
              file.setWritable(true);
              file.delete();
            }
            flag = genreatePdfCmd.genrateCmd(reqURL, reqQuery, folderName, id, fileName);
        }
        else {
           if (!file.exists()) {
              flag = genreatePdfCmd.genrateCmd(reqURL, reqQuery, folderName, id, fileName);
           }
        }        
        // Dohatec end : 22 April 2015 : 11:20 AM : Md. Wahid Abdullah
        
        InputStream fis = new FileInputStream(file);
        byte[] buf = new byte[(int) file.length()];
        int offset = 0;
        int numRead = 0;
        while ((offset < buf.length)
                && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

            offset += numRead;

        }
        fis.close();
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName()+"\"");
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(buf);
        outputStream.flush();
        outputStream.close();
    }

    private String checkPriceBid(String tenderId,String lotId){
        ReportCreationService creationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");                
        return String.valueOf(creationService.checkPriceBidMade(tenderId, lotId));
    }
}
