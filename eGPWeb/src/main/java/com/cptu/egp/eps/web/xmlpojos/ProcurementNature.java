/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.xmlpojos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Chalapathi.Bavisetti
 */
@XmlRootElement
public class ProcurementNature {
    private byte procurementNatureId;
    private String procurementNature;

    /**
     * @return the procurementNatureId
     */
     @XmlElement(nillable = true)
    public byte getProcurementNatureId() {
        return procurementNatureId;
    }

    /**
     * @param procurementNatureId the procurementNatureId to set
     */
    public void setProcurementNatureId(byte procurementNatureId) {
        this.procurementNatureId = procurementNatureId;
    }

    /**
     * @return the procurementNature
     */
     @XmlElement(nillable = true)
    public String getProcurementNature() {
        return procurementNature;
    }

    /**
     * @param procurementNature the procurementNature to set
     */
    public void setProcurementNature(String procurementNature) {
        this.procurementNature = procurementNature;
    }

    
}
