/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.model.table.TblTempBiddingPermission;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author SRISTY
 */
public class TempBiddingPermission {

    public TempBiddingPermission() {
    }
    private String ProcurementCategory = "";
    private String WorkType = "";
    private String WorkCategroy = "";
    private int CompanyID;
    private int UserId;
    
    public String getProcurementCategory() {
        return ProcurementCategory;
    }

    public void setProcurementCategory(String ProcurementCategory) {
        this.ProcurementCategory = ProcurementCategory;
    }
    
    public String getWorkType() {
        return WorkType;
    }

    public void setWorkType(String WorkType) {
        this.WorkType = WorkType;
    }
    
    public String getWorkCategroy() {
        return WorkCategroy;
    }

    public void setWorkCategroy(String WorkCategroy) {
        this.WorkCategroy = WorkCategroy;
    }
    
    public int getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(int CompanyID) {
        this.CompanyID = CompanyID;
    }
    
    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }
}
