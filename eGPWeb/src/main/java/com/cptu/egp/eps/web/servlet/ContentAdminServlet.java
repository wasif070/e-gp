/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.dao.storedprocedure.UserApprovalBean;
import com.cptu.egp.eps.service.serviceimpl.ContentAdminService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TaherT
 */
public class ContentAdminServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            
            if (request.getParameter("action").equals("fetchData")) {
                response.setContentType("text/xml;charset=UTF-8");
                String regType = null;
                String cmpRegNo = null;
                String cmpName = null;
                String email = null;
                Date regDateTo = null;
                Date regDateFrom = null;

                if (request.getParameter("regDateTo") == null) {
                } else {
                    if (!"".equals(request.getParameter("regDateTo"))) {
                        regDateTo = DateUtils.formatStdString(request.getParameter("regDateTo"));
                    }
                }
                if (request.getParameter("regDateFrom") == null) {
                } else {
                    if (!"".equals(request.getParameter("regDateFrom"))) {
                        regDateFrom = DateUtils.formatStdString(request.getParameter("regDateFrom"));
                    }
                }
                if (request.getParameter("cmpRegNo") == null) {
                } else {
                    if (!"".equals(request.getParameter("cmpRegNo"))) {
                        cmpRegNo = request.getParameter("cmpRegNo");
                    }
                }
                if (request.getParameter("cmpName") == null) {
                } else {
                    if (!"".equals(request.getParameter("cmpName"))) {
                        cmpName = request.getParameter("cmpName");
                    }
                }
                if (request.getParameter("email") == null) {
                } else {
                    if (!"".equals(request.getParameter("email"))) {
                       email = request.getParameter("email");
                    }
                }
                if (request.getParameter("regType") == null) {
                } else {
                    if (!"".equals(request.getParameter("regType"))) {
                        regType = request.getParameter("regType");
                    }
                }
                
                String status = request.getParameter("status");

                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                
                String sortString =null;
                if("".equals(sidx)){
                    sortString = "lm.registeredDate desc";//lm.userId desc
                }else{
                    sortString = sidx+" "+sord;
                }
                ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");

                Integer companyId=null;
                if(request.getParameter("cId")!=null){
                    companyId=Integer.parseInt(request.getParameter("cId"));
                }
                List<UserApprovalBean> getApprovalUsers = contentAdminService.userApprovalList(email, cmpRegNo, cmpName, status, regType, regDateFrom, regDateTo, Integer.parseInt(page), Integer.parseInt(rows),companyId, sortString);

                int totalPages = 0;
                int totalCount = 0;
                if(!getApprovalUsers.isEmpty()){
                    totalCount = getApprovalUsers.get(0).getTotalRecords();
                }
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                    } else {
                        totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                    }

                } else {
                    totalPages = 0;
                }                                
                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");
                
                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getApprovalUsers.size() + "</records>");                
                int j =0;
                int no = Integer.parseInt(request.getParameter("page"));
                if(no == 1){
                j = 1;
                }else{
                    if(Integer.parseInt(rows) == 30){
                        j = ((no-1)*30)+1;
                    }else if(Integer.parseInt(rows) == 20){
                        j = ((no-1)*20)+1;
                    }
                    else{
                        j = ((no-1)*10)+1;
                    }
                }
                int rowId = 0;
                for (UserApprovalBean userApprovalBean : getApprovalUsers) {
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell><![CDATA[" + j + "]]></cell>");
                    if(userApprovalBean.getRegistrationType().equals("contractor")){
                        out.print("<cell><![CDATA[Bidder / Consultant]]></cell>");
                    }else if(userApprovalBean.getRegistrationType().equals("individualconsultant")){
                        out.print("<cell><![CDATA[Individual Consultant]]></cell>");
                    }else if(userApprovalBean.getRegistrationType().equals("govtundertaking")){
                        out.print("<cell><![CDATA[Government owned Enterprise]]></cell>");
                    }else if(userApprovalBean.getRegistrationType().equals("media")){
                        out.print("<cell><![CDATA[Media]]></cell>");
                    }else{
                        out.print("<cell><![CDATA[]]></cell>");
                    }
                    out.print("<cell><![CDATA[" + userApprovalBean.getCompanyName() + "]]></cell>");
                    if(status.equalsIgnoreCase("rejected"))
                    {
                        String comments=userApprovalBean.getComments();
                        //comments=comments.substring(comments.indexOf('@')+1);
                        out.print("<cell><![CDATA[" + comments.substring(comments.indexOf('@')+1) + "]]></cell>");
                    }
                    else
                        out.print("<cell><![CDATA[" + userApprovalBean.getEmailId() + "]]></cell>");
                    out.print("<cell><![CDATA[" + userApprovalBean.getCountry() + "]]></cell>");
                    out.print("<cell><![CDATA[" + userApprovalBean.getState() + "]]></cell>");
                    String date = DateUtils.gridDateToStr(userApprovalBean.getRegisteredDate());
                    out.print("<cell><![CDATA[" + date.substring(0, date.lastIndexOf(":")) + "]]></cell>");
                    TenderCommonService service = (TenderCommonService)AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> list = service.returndata("SearchEmailForContentAdmin",userApprovalBean.getEmailId(), null);
                    if(status.equalsIgnoreCase("reapply"))
                    {
                        out.print("<cell><![CDATA[<a href='TendererCompDetails.jsp?uId=" + userApprovalBean.getUserid() + "&tId=" + userApprovalBean.getTendererId() + "&cId=" + userApprovalBean.getCompanyId() + "&jv=n&s="+String.valueOf(status.charAt(0))+"&payId=0&status=reapply'>View</a>]]></cell>");
                    }
                    else if(status.equalsIgnoreCase("pending"))
                    {
                        out.print("<cell><![CDATA[<a href='TendererRegDetails.jsp?uId=" + userApprovalBean.getUserid() + "&tId=" + userApprovalBean.getTendererId() + "&cId=" + userApprovalBean.getCompanyId() + "&jv=n&s="+String.valueOf(status.charAt(0))+"&payId=0&status=pending'>View</a>]]></cell>");
                    }
                    else if(list.isEmpty()){
                        //out.print("<cell><![CDATA[<a href='TendererRegDetails.jsp?uId=" + userApprovalBean.getUserid() + "&tId=" + userApprovalBean.getTendererId() + "&cId=" + userApprovalBean.getCompanyId() + "&s="+String.valueOf(status.charAt(0))+"&payId=0'>View</a>&nbsp;|&nbsp;<a href='DocReceipt.jsp?uId=" + userApprovalBean.getUserid() + "&tId=" + userApprovalBean.getTendererId() + "&cId=" + userApprovalBean.getCompanyId()+"'>Docs. Receipt</a>]]></cell>");
                        out.print("<cell><![CDATA[<a href='TendererRegDetails.jsp?uId=" + userApprovalBean.getUserid() + "&tId=" + userApprovalBean.getTendererId() + "&cId=" + userApprovalBean.getCompanyId() + "&jv=n&s="+String.valueOf(status.charAt(0))+"&payId=0'>View</a>]]></cell>"); //doc receipt hidden for bhutan egp 
                    }else{
                        if(list.get(0).getFieldName1().equals("0")){
                            //out.print("<cell><![CDATA[<a href='TendererRegDetails.jsp?uId=" + userApprovalBean.getUserid() + "&tId=" + userApprovalBean.getTendererId() + "&cId=" + userApprovalBean.getCompanyId() + "&s="+String.valueOf(status.charAt(0))+"&payId=0'>View</a>&nbsp;|&nbsp;<a href='DocReceipt.jsp?uId=" + userApprovalBean.getUserid() + "&tId=" + userApprovalBean.getTendererId() + "&cId=" + userApprovalBean.getCompanyId()+"'>Docs. Receipt</a>]]></cell>");
                            out.print("<cell><![CDATA[<a href='TendererRegDetails.jsp?uId=" + userApprovalBean.getUserid() + "&tId=" + userApprovalBean.getTendererId() + "&cId=" + userApprovalBean.getCompanyId() + "&jv=n&s="+String.valueOf(status.charAt(0))+"&payId=0'>View</a>]]></cell>"); //doc receipt hidden for bhutan egp 
                        }else{
                            //out.print("<cell><![CDATA[<a href='TendererPaymentDetail.jsp?uId=" + userApprovalBean.getUserid() + "&tId=" + userApprovalBean.getTendererId() + "&cId=" + userApprovalBean.getCompanyId() + "&s="+String.valueOf(status.charAt(0))+"&payId="+list.get(0).getFieldName1()+"'>View</a>&nbsp;|&nbsp;<a href='DocReceipt.jsp?uId=" + userApprovalBean.getUserid() + "&tId=" + userApprovalBean.getTendererId() + "&cId=" + userApprovalBean.getCompanyId()+"'>Docs. Receipt</a>]]></cell>");
                            out.print("<cell><![CDATA[<a href='TendererRegDetails.jsp?uId=" + userApprovalBean.getUserid() + "&tId=" + userApprovalBean.getTendererId() + "&cId=" + userApprovalBean.getCompanyId() + "&jv=n&s="+String.valueOf(status.charAt(0))+"&payId="+list.get(0).getFieldName1()+"'>View</a>]]></cell>"); //doc receipt hidden for bhutan egp 
                        }
                    }
                    out.print("</row>");
                    j++;
                    rowId++;
                }
                //No Record Found Code
//                if(getApprovalUsers.isEmpty()){
//                    out.print("<row id='" + rowId + "'>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("<cell><![CDATA[No data found]]></cell>");
//                    out.print("</row>");
//                }
                out.print("</rows>");
            }

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
