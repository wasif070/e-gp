/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.EvalCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblBidRankDetail;
import com.cptu.egp.eps.model.table.TblBidderRank;
import com.cptu.egp.eps.model.table.TblEvalRoundMaster;
import com.cptu.egp.eps.model.table.TblReportColumnMaster;
import com.cptu.egp.eps.model.table.TblReportForms;
import com.cptu.egp.eps.model.table.TblReportFormulaMaster;
import com.cptu.egp.eps.model.table.TblReportLots;
import com.cptu.egp.eps.model.table.TblReportMaster;
import com.cptu.egp.eps.model.table.TblReportTableMaster;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceimpl.EvaluationService;
import com.cptu.egp.eps.service.serviceimpl.ReportCreationService;
import com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.web.databean.ReportGenerateDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import org.jboss.logging.Logger;
/**
 *
 * @author TaherT
 */
public class ReportCreationSrBean {

    static final Logger logger = Logger.getLogger(ReportCreationSrBean.class);
    private String logUserId = "0";
    private ReportCreationService reportCreationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");

    public void setLogUserId(String logUserId) {
        reportCreationService.setUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * Creates PC Report
     * @param tblReportMaster to be inserted
     * @param noCols No of columns in the report
     * @param tenderid from tbl_TenderMaster
     * @return reportId
     */
    public int reportCreation(TblReportMaster tblReportMaster, String noCols, String tenderid) {
        logger.debug("reportCreation : " + logUserId + " Starts");
        if(tblReportMaster.getReportHeader()==null){
            tblReportMaster.setReportHeader("");
        }
        if(tblReportMaster.getReportFooter()==null){
            tblReportMaster.setReportFooter("");
        }
        tblReportMaster.setTblTenderMaster(new TblTenderMaster(Integer.parseInt(tenderid)));
        TblReportTableMaster tableMaster = new TblReportTableMaster(0, null, Integer.parseInt(noCols), "", "");
        logger.debug("reportCreation : " + logUserId + " Ends");
        return reportCreationService.createReport(tblReportMaster, tableMaster);
    }

    /**
     * Array of reportTableId,noOfCols,reportName,reportHeader,reportFooter,evalType
     * @param reportid search criteria
     * @return Array of reportTableId,noOfCols,reportName,reportHeader,reportFooter,evalType
     */
    public Object[] getReportTabIdNoOfCols(String reportid) {
        return reportCreationService.getReportTabIdNoOfCols(reportid);
    }
    
    public Object[] getReportTabIdNoOfColsLots(String reportid,String PkgLotID) {
        return reportCreationService.getReportTabIdNoOfColsLots(reportid,PkgLotID);
    }

    /**
     * Report Column Creation
     * @param id reportId
     * @param colId column Ids
     * @param colHeader column Headers
     * @param filledBy contains govCol yes no
     * @param isPackgeWise
     */
    public void createReportColumns(int id, String[] colId, String[] colHeader, String[] filledBy, boolean isPackgeWise) {
        logger.debug("createReportColumns : " + logUserId + " Starts");
        List<TblReportColumnMaster> columnMasters = new ArrayList<TblReportColumnMaster>();
        for (int i = 0; i < colId.length; i++) {
            String govCol = "no";
            if (!isPackgeWise) {
                if (filledBy[i].equals("2")) {
                    govCol = "yes";
                }
            }
            columnMasters.add(new TblReportColumnMaster(0, new TblReportTableMaster(id), Integer.parseInt(colId[i]), colHeader[i], Byte.parseByte(filledBy[i]), Integer.parseInt(colId[i]), govCol));
        }
        reportCreationService.createReportColumns(columnMasters);
        columnMasters = null;
        logger.debug("createReportColumns : " + logUserId + " Ends");
    }

    /**
     * Get Report Columns
     * @param reportMasterId reportId
     * @return List of TblReportColumnMaster
     * @throws Exception
     */
    public List<TblReportColumnMaster> getReportColumns(int reportMasterId) throws Exception {
        return reportCreationService.getReportColumns(reportMasterId);
    }

     /**
     * Get Forms for Report
     * @param tenderid from tbl_TenderMaster
     * @param packageLotId from tbl_TenderLotSecurity
     * @return List of {tenderFormId,formName,evalType}
     */
    public List<Object[]> findFormsForReport(String tenderid, String packageLotId) {
        return reportCreationService.findFormsForReport(tenderid, packageLotId);
    }

    /**
     * Updating Report Structure
     * @param id reportId
     * @param repColId reportColumnId
     * @param colId column Id(s)
     * @param colHeader column Header(s)
     * @param filledBy
     * @param isPackgeWise
     * @param isNot
     */
    public void updateReportColumns(int id, String[] repColId, String[] colId, String[] colHeader, String[] filledBy, boolean isPackgeWise,boolean isNot) {
        logger.debug("updateReportColumns : " + logUserId + " Starts");
        if(isNot){
            reportCreationService.delRepFormula(String.valueOf(id));
        }
        List<TblReportColumnMaster> columnMasters = new ArrayList<TblReportColumnMaster>();
        for (int i = 0; i < colId.length; i++) {
            String govCol = "no";
            if (!isPackgeWise) {
                if (filledBy[i].equals("2")) {
                    govCol = "yes";
                }
            }
            columnMasters.add(new TblReportColumnMaster(Integer.parseInt(repColId[i]), new TblReportTableMaster(id), Integer.parseInt(colId[i]), colHeader[i], Byte.parseByte(filledBy[i]), Integer.parseInt(colId[i]), govCol));
        }
        reportCreationService.createReportColumns(columnMasters);
        columnMasters = null;
        logger.debug("updateReportColumns : " + logUserId + " Ends");
    }

    /**
     * Updates report Governing column
     * @param repColId
     */
    public void updateGovernCol(String repColId) {
        reportCreationService.updateGovernCol(repColId);
    }

    /**
     * Inserts Formula Created for the report
     * @param reportTableId on which formula is created
     * @param formula contains Formula
     * @param columnId on which formula is created
     * @param dispFormula Display for client
     */
    public void addRepColFormula(String reportTableId, String formula, String columnId, String dispFormula) {
        reportCreationService.addRepColFormula(reportTableId, formula, columnId, dispFormula);
    }

    /**
     * Get Report Formula for the report
     * @param repTableId from tbl_ReportTableMaster
     * @return List of {reportFormulaId, .displayFormula, colHeader,reportColId}
     */
    public List<Object[]> getReportFormula(String repTableId) {
        return reportCreationService.getReportFormula(repTableId);
    }

    /**
     * Deletes Report Formula for the Report Table
     * @param reportFormulaId formula to be deleted
     * @param repColId report Column Id
     */
    public void delRepColFormula(String[] reportFormulaId, String[] repColId) {
        reportCreationService.delRepColFormula(reportFormulaId, repColId);
    }

    /**
     * Get Tender evalType
     * @param tenderid from tbl_TenderMaster
     * @return Tender Evaluation Type
     */
    public String getTenderEvalType(String tenderid) {
        return reportCreationService.getTenderEvalType(tenderid);
    }

    /**
     * Tender Forms on which Formula is to be created
     * @param repId reportId
     * @param formId formId(s)
     * @param isNot boolean
     */
    public void selectFormForRep(String repId, String[] formId,boolean isNot) {
        logger.debug("selectFormForRep : " + logUserId + " Starts");
        List<TblReportForms> list = new ArrayList<TblReportForms>();
        for (int i = 0; i < formId.length; i++) {
            list.add(new TblReportForms(0, new TblReportMaster(Integer.parseInt(repId)), Integer.parseInt(formId[i])));
        }
        reportCreationService.selectFormForRep(list,isNot);
        list = null;
        logger.debug("selectFormForRep : " + logUserId + " Ends");
    }

    /**
     * Get Forms from tbl_ReportForms for Reports
     * @param repId from tbl_ReportMaster
     * @return List of {tenderFormId}
     */
    public List<Object> getFormsForReport(String repId) {
        return reportCreationService.getFormsForReport(repId);
    }

    /**
     * Create Formula for Report in case of Item wise
     * @param repId from tbl_ReportMaster
     * @param repForm name of the form
     * @param repHead report Header(s)
     */
    public void createItemWiseFormula(String repId, String repForm, String[] repHead) {
        logger.debug("createItemWiseFormula : " + logUserId + " Starts");
        List<String> repHeadIds = new ArrayList<String>();
        List<String> repHeadNames = new ArrayList<String>();
        for (int i = 0; i < repHead.length; i++) {
            repHeadNames.add(repHead[i].split("@#")[0]);
            repHeadIds.add(repHead[i].split("@#")[1]);
        }
        reportCreationService.createItemWiseFormula(repId, repForm, repHeadIds, repHeadNames);
        repHeadIds = null;
        repHeadNames = null;
        logger.debug("createItemWiseFormula : " + logUserId + " Starts");
        logger.debug("createItemWiseFormula : " + logUserId + " Ends");
    }

    /**
     * Delete Report Column Formula
     * @param reportFormulaId to be deleted(s)
     */
    public void delRepColFormula(String[] reportFormulaId) {
        reportCreationService.delRepColFormula(reportFormulaId);
    }

    /**
     * Get reportTable Id from reportId
     * @param repId from tbl_ReportMaster
     * @return reportTableId
     */
    public String getReportTableId(String repId) {
        return reportCreationService.getReportTableId(repId);
    }

    /**
     * List of Formula Created for ItemWise
     * @param repTableId from tbl_ReportTableMaster
     * @return List of TblReportFormulaMaster
     * @throws Exception
     */
    public List<TblReportFormulaMaster> getRepFormulaItemWise(int repTableId) throws Exception {
        return reportCreationService.getRepFormulaItemWise(repTableId);
    }

    /**
     * Get TenderLots for the tender
     * @param tenderid from tbl_TenderMaster
     * @return List of {appPkgLotId,lotNo,lotDesc}
     */
    public List<Object[]> findLotPkgForReport(String tenderid) {
        return reportCreationService.findLotPkgForReport(tenderid);
    }

    /**
     * Get Lot selected for the report
     * @param tenderid from tbl_TenderMaster
     * @param repId from tbl_ReportMaster
     * @return List of {appPkgLotId,lotNo,lotDesc}
     */
    public List<Object[]> findLotPkgForReport(String tenderid,String repId) {
        return reportCreationService.findLotPkgForReport(tenderid,repId);
    }

    /**
     * Get Report Data for display
     * @param tenderId from tbl_TenderMaster
     * @param reportId from tbl_ReportMaster
     * @param userId session userId
     * @param isTos flag checking
     * @return List of ReportGenerateDtBean
     * @throws Exception
     */
public List<ReportGenerateDtBean> getReportData(String tenderId, String reportId, String userId, boolean isTos,boolean isTOR,String winnerID,String lotId) throws Exception {
        logger.debug("getReportData : " + logUserId + " Starts");
        //List<SPTenderCommonData> list = null;
        List<EvalCommonSearchData> list = null;
        List<ReportGenerateDtBean> data = new ArrayList<ReportGenerateDtBean>();
        List<ReportGenerateDtBean> dataWinner = new ArrayList<ReportGenerateDtBean>();
        if (reportCreationService.isReportRoundSaved(roundId)==0 && finalroundId.equals("0")){
            int reportTabId = 0;
            if(lotId.equalsIgnoreCase("0")){
                reportTabId = Integer.parseInt(reportCreationService.getReportTableId(reportId));
            } else{
                reportTabId = Integer.parseInt(reportCreationService.getReportTableIdLot(reportId,lotId));
            }
            List<TblReportColumnMaster> columnMasters = getReportColumns(reportTabId);

            List<Integer> autoColumns = new ArrayList<Integer>();
            List<Integer> cmpColumns = new ArrayList<Integer>();
            List<Integer> autoNumberCols = new ArrayList<Integer>();
            List<Integer> estCosts = new ArrayList<Integer>();
            int govColId = 0;

            for (TblReportColumnMaster tRCM : columnMasters) {
                if (tRCM.getFilledBy() == 2 && tRCM.getGovCol().equals("no")) {
                    autoColumns.add(tRCM.getColumnId());
                }
                if (tRCM.getFilledBy() == 2 && tRCM.getGovCol().equals("yes")) {
                    govColId = tRCM.getColumnId();
                }
                if (tRCM.getFilledBy() == 1) {
                    cmpColumns.add(tRCM.getColumnId());
                }
                if (tRCM.getFilledBy() == 3) {
                    autoNumberCols.add(tRCM.getColumnId());
                }
                if (tRCM.getFilledBy() == 4) {
                    estCosts.add(tRCM.getColumnId());
                }
            }

            if (isTos) {
                if(isTOR){ // For Tender opening report
                    if(lotId.equalsIgnoreCase("0")){
                    list = reportCreationService.getAllBiddersForTORReport(tenderId, reportId);
                    } else{
                         list = reportCreationService.getAllBiddersForTORReport(tenderId, reportId,lotId);
                    }
                } else {
                    if(lotId.equalsIgnoreCase("0")){
                    list = reportCreationService.getAllBiddersForReport(tenderId, reportId);
                    } else {
                         list = reportCreationService.getAllBiddersForReport(tenderId, reportId,lotId);
                    }
                }
            } else {
                if(lotId.equalsIgnoreCase("0")){
                list = reportCreationService.getBiddersForReport(tenderId, reportId);
                } else{
                    list = reportCreationService.getBiddersForReport(tenderId, reportId,lotId);
                }
            }

            String getCREstCost = getCREstCost(tenderId, reportId);

            for (EvalCommonSearchData sPTenderCommonData : list) {
                String[] cmpName = new String[cmpColumns.size()];
                for (int j = 0; j < cmpName.length; j++) {
                    cmpName[j] = sPTenderCommonData.getFieldName2();
                }
                String[] cmpColumnsTmp = new String[cmpColumns.size()];
                for (int j = 0; j < cmpColumnsTmp.length; j++) {
                    cmpColumnsTmp[j] = cmpColumns.get(j).toString();
                }
                String[] autoNumberColsTmp = new String[autoNumberCols.size()];
                for (int j = 0; j < autoNumberColsTmp.length; j++) {
                    autoNumberColsTmp[j] = autoNumberCols.get(j).toString();
                }
                String[] autoColumnsTmp = new String[autoColumns.size()];
                for (int j = 0; j < autoColumnsTmp.length; j++) {
                    autoColumnsTmp[j] = autoColumns.get(j).toString();
                }
                String[] estCost = new String[estCosts.size()];
                for (int j = 0; j < estCost.length; j++) {
                    estCost[j] = getCREstCost;
                }
                String[] estCostsTmp = new String[estCosts.size()];
                for (int j = 0; j < estCostsTmp.length; j++) {
                    estCostsTmp[j] = estCosts.get(j).toString();
                }
                if(isTos)
                data.add(new ReportGenerateDtBean(sPTenderCommonData.getFieldName1(), cmpName, cmpColumnsTmp, autoNumberColsTmp, reportCreationService.getBidderDataForReport(reportTabId, govColId, Integer.parseInt(sPTenderCommonData.getFieldName1())), String.valueOf(govColId), getAutoColData(reportTabId, autoColumns, Integer.parseInt(sPTenderCommonData.getFieldName1())), autoColumnsTmp,estCost,estCostsTmp));
                else if(!isTos && !sPTenderCommonData.getFieldName1().equals(winnerID))
                    data.add(new ReportGenerateDtBean(sPTenderCommonData.getFieldName1(), cmpName, cmpColumnsTmp, autoNumberColsTmp, reportCreationService.getBidderDataForReport(reportTabId, govColId, Integer.parseInt(sPTenderCommonData.getFieldName1())), String.valueOf(govColId), getAutoColData(reportTabId, autoColumns, Integer.parseInt(sPTenderCommonData.getFieldName1())), autoColumnsTmp,estCost,estCostsTmp));
                else if(!isTos && sPTenderCommonData.getFieldName1().equals(winnerID))
                    dataWinner.add(new ReportGenerateDtBean(sPTenderCommonData.getFieldName1(), cmpName, cmpColumnsTmp, autoNumberColsTmp, reportCreationService.getBidderDataForReport(reportTabId, govColId, Integer.parseInt(sPTenderCommonData.getFieldName1())), String.valueOf(govColId), getAutoColData(reportTabId, autoColumns, Integer.parseInt(sPTenderCommonData.getFieldName1())), autoColumnsTmp,estCost,estCostsTmp));
                cmpName = null;
                cmpColumnsTmp = null;
                autoNumberColsTmp = null;
                autoColumnsTmp = null;
                estCost = null;
                estCostsTmp = null;
            }
            if (reportCreationService.getReportType(reportId).equals("l1")) {
                Collections.sort(data, new CompareLotBidderL1());
            } else {
                Collections.sort(data, new CompareLotBidderH1());
            }
            
            if (!isTos) {
                data.add(0, dataWinner.get(0));
                saveReportData(data, tenderId, reportId, columnMasters, userId,Integer.parseInt(lotId));
            }
            columnMasters = null;
            autoColumns = null;
            cmpColumns = null;
            autoNumberCols = null;
            list = null;
        } else {
            data =  getSavedReportData(tenderId, reportId);
        }
        logger.debug("getReportData : " + logUserId + " Ends");
        return data;
    }

    /**
     * Fetch Auto Column Data other than gov column
     * @param repTabId from tbl_ReportColumnMaster
     * @param autoCols auto col id(s)
     * @param userId bidder userId
     * @return Array of AutoCol Data for the bidder
     */
    public String[] getAutoColData(int repTabId, List<Integer> autoCols, int userId) {
        logger.debug("getAutoColData : " + logUserId + " Starts");
        String[] data = new String[autoCols.size()];
        int i = 0;
        for (Integer aCols : autoCols) {
            data[i] = reportCreationService.getBidderDataForReport(repTabId, aCols, userId);
            i++;
        }
        logger.debug("getAutoColData : " + logUserId + " Ends");
        return data;
    }

    /**
     * Save report Data
     * @param rgdbs to be saved
     * @param tenderId from tbl_TenderMaster
     * @param reportId from tbl_ReportMaster
     * @param columnMasters List of report Columns
     * @param userId session userId
     * @param pkgLotId session userId
     */
    public void saveReportData(List<ReportGenerateDtBean> rgdbs, String tenderId, String reportId, List<TblReportColumnMaster> columnMasters, String userId,int pkgLotId) {
        logger.debug("saveReportData : " + logUserId + " Starts");
        EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
        //int pkgLotId = Integer.parseInt(reportCreationService.getPkgLotId(tenderId, reportId));
        int counter=1;
        int rank=1;
        int fRank=0;
        List<TblBidderRank> list1 = new ArrayList<TblBidderRank>();
        List<TblBidRankDetail> list2 = new ArrayList<TblBidRankDetail>();
        double L1amount = 0;
        double amount = 0;
        boolean isLottery = false;
        boolean isFirstDisq = true;
        for (ReportGenerateDtBean rgdb : rgdbs) {
            if (rgdb.getGovColumn() == null) {
                rgdb.setGovColumn("0");
            }
            amount = new BigDecimal(rgdb.getGovColumn()).setScale(3, 0).doubleValue();
            CommonSearchDataMoreService moreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
           
            List<SPCommonSearchDataMore> pqBidder = moreService.geteGPData("PostDisqualifyUsers",tenderId,reportId);
            int tempFlag=0;
            for(SPCommonSearchDataMore pqUser : pqBidder){
                if(pqUser.getFieldName1().equals(rgdb.getBidderId())){
                    fRank=-1;
                    tempFlag++;
                }
            }
            if(counter==1){
                if(fRank!=-1){
                    fRank = rank;
                }
                L1amount = amount;
            }else{
                if(amount!=L1amount){
                    L1amount = amount;
                  if(fRank==-1){
                        fRank = rank;
                        rank++;
                    }else{
                        rank++;
                        fRank = rank;
                    }
                }else{
                    if(fRank==-1){
                       L1amount = amount;
                    }else if(fRank!=-1){
                        if(fRank==rank && counter==2 && isFirstDisq){
                            isLottery = true;
                        }
                    }
                }
           }
            if(fRank==-1){
                fRank = rank;
                if(counter==1){
                    isFirstDisq = false;
                 }
            }
            if(tempFlag!=0){
                counter++;
                continue;
            }

            list1.add(new TblBidderRank(0, new TblTenderMaster(Integer.parseInt(tenderId)), pkgLotId, Integer.parseInt(rgdb.getBidderId()), new Date(), Integer.parseInt(userId), rank, Integer.parseInt(reportId), new BigDecimal(rgdb.getGovColumn()), 0, 0, ""));
            for (TblReportColumnMaster master : columnMasters) {
                if (master.getFilledBy() == 1) {
                    for (int r = 0; r < rgdb.getCompanyNameColId().length; r++) {
                        if (Integer.parseInt(rgdb.getCompanyNameColId()[r]) == master.getColumnId()) {
                            list2.add(new TblBidRankDetail(0, null, master.getColumnId(), counter - 1, rgdb.getCompanyName()[r]));
                        }
                    }
                }
                if (master.getFilledBy() == 2 && master.getGovCol().equals("no")) {
                    for (int p = 0; p < rgdb.getAutoColumnColId().length; p++) {
                        if (Integer.parseInt(rgdb.getAutoColumnColId()[p]) == master.getColumnId()) {
                            String govno=null;
                            if(rgdb.getAutoColumn()[p]==null || "".equals(rgdb.getAutoColumn()[p])){
                                govno =  "0.00";
                            }else{
                                govno =  rgdb.getAutoColumn()[p];
                            }
                            list2.add(new TblBidRankDetail(0, null, master.getColumnId(), counter - 1, govno));
                        }
                    }
                }
                if (master.getFilledBy() == 2 && master.getGovCol().equals("yes")) {
                    if (Integer.parseInt(rgdb.getGovColumnColId()) == master.getColumnId()) {
                        String govyes=null;
                        if(rgdb.getGovColumn()==null || "".equals(rgdb.getGovColumn())){
                            govyes =  "0.00";
                        }else{
                            govyes =  rgdb.getGovColumn();
                        }
                        list2.add(new TblBidRankDetail(0, null, master.getColumnId(), counter - 1, govyes));
                    }
                }
                if (master.getFilledBy() == 3) {
                    for (int q = 0; q < rgdb.getAutoNumberColId().length; q++) {
                        if (Integer.parseInt(rgdb.getAutoNumberColId()[q]) == master.getColumnId()) {                            
                           list2.add(new TblBidRankDetail(0, null, master.getColumnId(), counter - 1, String.valueOf(rank)));
                        }
                    }
                }
                if (master.getFilledBy() == 4) {
                    for (int r = 0; r < rgdb.getEstCostId().length; r++) {
                        if (Integer.parseInt(rgdb.getEstCostId()[r]) == master.getColumnId()) {
                            list2.add(new TblBidRankDetail(0, null, master.getColumnId(), counter - 1, rgdb.getEstCost()[r]));
                        }
                    }
                }                
            }
            counter++;
        }
        TblEvalRoundMaster roundMaster = null;
        int evalCount = evalService.getEvaluationNo(Integer.parseInt(tenderId));
        if(!list1.isEmpty()){
            int winnerBidder = 0;
            if(isLottery){
                List<Integer> bidders = new ArrayList<Integer>();
                int finalOne = list1.get(0).getRank();
                for (TblBidderRank bidderRank : list1) {
                    if(bidderRank.getRank()==finalOne){
                        bidders.add(bidderRank.getUserId());
                    }
                }
                winnerBidder = getLotteryWinner(bidders);
            }else{
                winnerBidder = list1.get(0).getUserId();
            }
            roundMaster =  new TblEvalRoundMaster(0, Integer.parseInt(tenderId), pkgLotId, "L1", Integer.parseInt(reportId),winnerBidder, new Date(), Integer.parseInt(userId),evalCount);
        }
        reportCreationService.saveReportData(roundMaster,list1, list2, columnMasters.size(),isLottery);// Modified By Dohatec to Resolve LTM multiple entry 
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
        addUpdate.addUpdOpeningEvaluation("PostQualification",String.valueOf(list1.get(0).getUserId()),tenderId,"Yes",format.format(new Date()),String.valueOf(pkgLotId),"ok",format.format(new Date()),userId,"Qualify",format.format(new Date()),"Satisfactory","pending","ok","1").get(0);
        list1 = null;
        list2 = null;
        logger.debug("saveReportData : " + logUserId + " Ends");
    }

    /**
     * Get Report Data from saved report
     * @param tenderId from tbl_TenderMaster
     * @param reportId from tbl_ReportMaster
     * @return List of ReportGenerateDtBean
     * @throws Exception
     */
    public List<ReportGenerateDtBean> getSavedReportData(String tenderId, String reportId) throws Exception {
        logger.debug("getSavedReportData : " + logUserId + " Starts");
        List<ReportGenerateDtBean> data = new ArrayList<ReportGenerateDtBean>();
        List<TblBidderRank> list1 = new ArrayList<TblBidderRank>();
        CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
        List<SPCommonSearchDataMore> list = null;
        if(!roundId.equals("0")){
            list1 = reportCreationService.findBidderRank(tenderId, reportId,roundId);
        }
        if(!finalroundId.equals("0")){
            list1 = reportCreationService.findBidderRank(tenderId, reportId,finalroundId);
            list = dataMoreService.geteGPData("PostDisqualifyUsers",tenderId,reportId);
        }
        int reportTabId = Integer.parseInt(reportCreationService.getReportTableId(reportId));
        List<TblReportColumnMaster> columnMasters = getReportColumns(reportTabId);
        List<Integer> autoColumns = new ArrayList<Integer>();
        List<Integer> cmpColumns = new ArrayList<Integer>();
        List<Integer> autoNumberCols = new ArrayList<Integer>();
        List<Integer> estCosts = new ArrayList<Integer>();
        int govColId = 0;

        for (TblReportColumnMaster tRCM : columnMasters) {
            if (tRCM.getFilledBy() == 2 && tRCM.getGovCol().equals("no")) {
                autoColumns.add(tRCM.getColumnId());
            }
            if (tRCM.getFilledBy() == 2 && tRCM.getGovCol().equals("yes")) {
                govColId = tRCM.getColumnId();
            }
            if (tRCM.getFilledBy() == 1) {
                cmpColumns.add(tRCM.getColumnId());
            }
            if (tRCM.getFilledBy() == 3) {
                autoNumberCols.add(tRCM.getColumnId());
            }
            if (tRCM.getFilledBy() == 4) {
                estCosts.add(tRCM.getColumnId());
            }
        }        
        int counter = 0;
        for (TblBidderRank tbr : list1) {
            if(!finalroundId.equals("0") && counter==0){
                counter++;
                continue;
            }
            if(!finalroundId.equals("0") && counter!=0){
                int flag = 0;
                for (SPCommonSearchDataMore dataMore : list) {
                    if(Integer.parseInt(dataMore.getFieldName1())==tbr.getUserId()){
                        flag++;
                    }                    
                }
                if(flag!=0){
                        continue;
                }
            }
            ReportGenerateDtBean dtBean = new ReportGenerateDtBean();
            //dtBean.setBidderId(String.valueOf(tbr.getBidderRankId()));
            dtBean.setBidderId(String.valueOf(tbr.getUserId()));
            String[] tmpCmpName = new String[cmpColumns.size()];
            String[] tmpCmpColId = new String[cmpColumns.size()];
            String[] tmpEstCost = new String[estCosts.size()];
            String[] tmpEstCostId = new String[estCosts.size()];
            String[] tmpAutoCols = new String[autoColumns.size()];
            String[] tmpAutoColsIds = new String[autoColumns.size()];
            String govColValue = "";
            String[] tmpAutoNumIds = new String[autoNumberCols.size()];
            String[] tmpAutoNumber = new String[autoNumberCols.size()];
            for (TblBidRankDetail tbrd : reportCreationService.findBidRankDetail(tbr.getBidderRankId())) {
                for (int r = 0; r < cmpColumns.size(); r++) {
                    if (cmpColumns.get(r) == tbrd.getReportColumnId()) {
                        tmpCmpName[r] = tbrd.getCellValue();
                        tmpCmpColId[r] = String.valueOf(tbrd.getReportColumnId());
                    }
                }
                for (int r = 0; r < autoColumns.size(); r++) {
                    if (autoColumns.get(r) == tbrd.getReportColumnId()) {
                        tmpAutoCols[r] = tbrd.getCellValue();
                        tmpAutoColsIds[r] = String.valueOf(tbrd.getReportColumnId());
                    }
                }
                if (govColId == tbrd.getReportColumnId()) {
                    govColValue = tbrd.getCellValue();
                    govColId = tbrd.getReportColumnId();
                }
                for (int r = 0; r < autoNumberCols.size(); r++) {
                    if (autoNumberCols.get(r) == tbrd.getReportColumnId()) {
                        tmpAutoNumIds[r] = String.valueOf(tbrd.getReportColumnId());
                        tmpAutoNumber[r] = tbrd.getCellValue();
                    }
                }
                for (int r = 0; r < estCosts.size(); r++) {
                    if (estCosts.get(r) == tbrd.getReportColumnId()) {
                        tmpEstCost[r] = tbrd.getCellValue();
                        tmpEstCostId[r] = String.valueOf(tbrd.getReportColumnId());
                    }
                }
            }
            dtBean.setCompanyName(tmpCmpName);
            dtBean.setCompanyNameColId(tmpCmpColId);
            dtBean.setEstCost(tmpEstCost);
            dtBean.setEstCostId(tmpEstCostId);
            dtBean.setAutoColumn(tmpAutoCols);
            dtBean.setAutoColumnColId(tmpAutoColsIds);
            dtBean.setGovColumn(govColValue);
            dtBean.setGovColumnColId(String.valueOf(govColId));
            dtBean.setAutoNumberColId(tmpAutoNumIds);
            dtBean.setAutoNumber(tmpAutoNumber);
            data.add(dtBean);
            tmpAutoCols = null;
            tmpAutoColsIds = null;
            tmpAutoNumIds = null;
            tmpCmpColId = null;
            tmpCmpName = null;
            tmpEstCost = null;
            tmpEstCostId = null;
            govColValue = null;
            dtBean = null;
            counter++;
        }
        list1 = null;
        columnMasters = null;
        autoColumns = null;
        estCosts = null;
        cmpColumns = null;
        autoNumberCols = null;        
        logger.debug("getSavedReportData : " + logUserId + " Ends");
        return data;
    }

    /**
     * Get Item wise Report Data
     * @param repId from tbl_ReportMaster
     * @return List of {cellvalue,rowId,tenderTableId}
     */
    public List<Object[]> getItemWiseReportHeader(String repId) {
        return reportCreationService.getItemWiseReportHeader(repId);
    }

    /**
     * Get Report Data for Item wise
     * @param tenderId from tbl_TenderMaster
     * @param reportId from tbl_ReportMaster
     * @param userId
     * @return List of TblBidderRank
     * @throws Exception
     */
    public List<TblBidderRank> getItemWiseReport(String tenderId, String reportId, String userId) throws Exception {
        logger.debug("getItemWiseReport : " + logUserId + " Starts");
        List<TblBidderRank> list = new ArrayList<TblBidderRank>();
        if (reportCreationService.isReportSaved(tenderId, reportId) == 0) {
            List<Object[]> objects = getItemWiseReportHeader(reportId);
            ListIterator<Object[]> mainObj = objects.listIterator();
            long mainCnt = getCountForItemDisplay(reportId);
            int calCnt = objects.size() / (int) mainCnt;
            for (int p = 0; p < calCnt; p++) {
                int rowId = 0;
                int tableId = 0;
                for (int j = 0; j < mainCnt; j++) {
                    if (mainObj.hasNext()) {
                        Object[] o1 = mainObj.next();
                        rowId = (Integer) o1[1];
                        tableId = (Integer) o1[2];
                    }
                }
                int i = 1;
                for (Object[] obj2 : reportCreationService.getCellValueItemWiseReport(reportId, String.valueOf(rowId))) {
                    String cmpName = null;
                    if (obj2[2].toString().equals("-")) {
                        cmpName = obj2[3].toString() + " " + obj2[4].toString();
                    } else {
                        cmpName = obj2[2].toString();
                    }
                    list.add(new TblBidderRank(0, new TblTenderMaster(Integer.parseInt(tenderId)), 0, (Integer) obj2[1], new Date(), Integer.parseInt(userId), i, Integer.parseInt(reportId), new BigDecimal(obj2[0].toString()), rowId, tableId, cmpName));
                    i++;
                }
            }
            reportCreationService.saveItemWiseReport(list);
            objects = null;
            mainObj = null;
        } else {
            list =  getSavedItemReport(tenderId, reportId);
        }
        logger.debug("getItemWiseReport : "+logUserId+" Ends");
            return list;
        }

    /**
     * Get Saved Report Data for Item wise
     * @param tenderId from tbl_TenderMaster
     * @param reportId from tbl_ReportMaster
     * @return List of TblBidderRank
     * @throws Exception
     */
    public List<TblBidderRank> getSavedItemReport(String tenderId, String reportId) throws Exception {
        return reportCreationService.getSavedItemReport(tenderId, reportId);
    }

    /**
     * Count For Item Display
     * @param repId from tbl_ReportMaster
     * @return count
     * @throws Exception
     */
    public long getCountForItemDisplay(String repId) throws Exception {
        return reportCreationService.getCountForItemDisplay(repId);
    }

    /**
     * Inserts Lot information for report
     * @param reportLots to be inserted
     * @param isNot flag
     * @return true or false for success or fail
     */
    public boolean insertReportLots(TblReportLots reportLots,boolean isNot){
        return reportCreationService.insertReportLots(reportLots,isNot);
    }

    /**
     * Update Report Name
     * @param repId
     * @param repName
     * @return true or false for success or fail
     */
    public boolean updateRepName(String repId,String repName){
        return reportCreationService.updateRepName(repId,repName);
    }

    /**
     * Lot Id for the report
     * @param repId from tbl_ReportMaster
     * @return LotId
     */
    public Object getReportLots(String repId){
        return reportCreationService.getReportLots(repId);
    }

    public List<SPCommonSearchDataMore> getReportFormDetail(String tenderId,String repId){
        return reportCreationService.getReportFormDetail(tenderId, repId);
    }

    /**
     * Estimation cost for the Lot of Tender
     * @param tenderId from tbl_TenderMaster
     * @param repId from tbl_ReportMaster
     * @return Estimation cost
     */
    public String getCREstCost(String tenderId,String repId){
        return reportCreationService.getCREstCost(tenderId, repId);
    }

    /**
     * Shuffles the List of Bidder and returns a single bidder randomly
     * @param bidders List of bidders
     * @return winner
     */
    private int getLotteryWinner(List<Integer> bidders) {
        Collections.shuffle(bidders);
        int index = new Random().nextInt(bidders.size());
        return bidders.get(index);
    }

    /**
     * Save Report of bidder roundwise
     * @param bidderIds array of bidder
     * @param roundId bidder who has shortlisted
     * @return true or false for success or fail
     */
    public boolean  saveReportRoundWise(String[] bidderIds,String roundId){
        boolean flag = false;
        List<Integer> users = new ArrayList<Integer>();
        int winnerBidder = 0;
        if(bidderIds!=null && bidderIds.length>1){
            for(String userId : bidderIds) {
                users.add(Integer.parseInt(userId));
            }
            winnerBidder = getLotteryWinner(users);
        }else if(bidderIds!=null && bidderIds.length==1){
            winnerBidder = Integer.parseInt(bidderIds[0]);
        }
        flag = reportCreationService.saveReportRoundWise(winnerBidder, roundId).equals("1") ? true : false;
        return flag;
    }

    private String roundId="0";

    public String getRoundId() {
        return roundId;
    }

    public void setRoundId(String roundId) {
        this.roundId = roundId;
    }
    private String finalroundId="0";

    public String getFinalroundId() {
        return finalroundId;
    }

    public void setFinalroundId(String finalroundId) {
        this.finalroundId = finalroundId;
    }

    public List<Object[]> findFormsForReportForTenderForms(String tenderid, String packageLotId) {
        return reportCreationService.findFormsForReportForTenderForms(tenderid, packageLotId);
    }
}
