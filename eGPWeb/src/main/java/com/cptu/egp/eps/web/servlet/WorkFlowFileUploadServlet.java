/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.model.table.TblWorkFlowDocuments;
import com.cptu.egp.eps.web.servicebean.WorkFlowSrBean;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
//@WebServlet(name = "WorkFlowFileUploadServlet", urlPatterns = {"/WorkFlowFileUploadServlet"})
public class WorkFlowFileUploadServlet extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private File destinationDir;
    String logUSerId = "0";
    Logger LOGGER = Logger.getLogger(WorkFlowFileUploadServlet.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         Map<String, String> m=  FilePathUtility.getFilePath();
         int uid = 0;
           if(request.getSession().getAttribute("userId") != null){
               logUSerId=  request.getSession().getAttribute("userId").toString();
               Integer ob1 = (Integer)request.getSession().getAttribute("userId");
               uid = ob1.intValue();
           }
        LOGGER.debug("processRequest "+logUSerId+" Starts");           
        String realPath = m.get("WorkFlowFileUploadServlet")+uid;
        destinationDir = new File(realPath);
        if (!destinationDir.isDirectory()) {
            destinationDir.mkdir();
        }
        String msg = "";
        TblWorkFlowDocuments tblWorkFlowDocuments = new TblWorkFlowDocuments();
        WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
        String objectid = null;
        String activityid = null;
        String userid = null;
        String description = null;
        String docName = null;
        Long docSize = null;
        String childid = null;
        Date uploaddate  = new Date();
        String checkret = "";
        response.setContentType("text/html");
        try {

        if(request.getSession().getAttribute("userId") != null){
            DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
            /*
             *Set the size threshold, above which content will be stored on disk.
             */
            fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB
		/*
             * Set the temporary directory to store the uploaded files of size above threshold.
             */
            fileItemFactory.setRepository(tmpDir);

            ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
            try {
                /*
                 * Parse the request
                 */
                List items = uploadHandler.parseRequest(request);
                Iterator itr = items.iterator();
                //For Supporting Document
              
                while (itr.hasNext()) {
                    FileItem item = (FileItem) itr.next();
                    //For Supporting Document                   
                    /*
                     * Handle Form Fields.
                     */
                    if (item.isFormField()) {
                        if (item.getFieldName().equals("objectid")) {
                            objectid =item.getString();
                        }else if(item.getFieldName().equals("activityid")){
                            activityid = item.getString();
                        }else if(item.getFieldName().equals("userid")){
                            userid = item.getString();
                        }else if(item.getFieldName().equals("childid")){
                             childid = item.getString();
                        }else if(item.getFieldName().equals("description")){
                            description = item.getString();
                        }else if(item.getFieldName().equals("userid")){
                            userid = item.getString();
                        }
                    } else {
                        checkret = checkExnAndSize(item.getName(), item.getSize(), "officer");
                        //Handle Uploaded files.
                               docSize = item.getSize();
                               String fileName = "";
                        if (item.getName().lastIndexOf("\\") != -1) {
                             fileName = item.getName().substring(item.getName().lastIndexOf("\\")+1, item.getName().length());
                        } else {
                             fileName = item.getName();
                        }
                        //fileName  = fileName.replaceAll(" ", "");

                               docName = fileName;

                        /*
                         * Write file to the ultimate location.
                         */
                        if(checkret.equals("Yes")){
                        File file = new File(destinationDir, fileName);
                        item.write(file);
                        File file1 = new File(destinationDir, fileName);
                        if(!file1.isFile()){
                            checkret = "UploadProblem";
                        }
                        }

                    }
                }
                if(checkret.equals("Yes")){
                tblWorkFlowDocuments.setActivityId(Integer.parseInt(activityid));
                tblWorkFlowDocuments.setChildId(Integer.parseInt(childid));
                tblWorkFlowDocuments.setObjectId(Integer.parseInt(objectid));
                tblWorkFlowDocuments.setChildId(Integer.parseInt(childid));
                tblWorkFlowDocuments.setDocSize(docSize.toString());
                tblWorkFlowDocuments.setDocumentName(docName);
                tblWorkFlowDocuments.setDescription(description);
                tblWorkFlowDocuments.setUserId(Integer.parseInt(userid));
                tblWorkFlowDocuments.setUploadedDate(uploaddate);
              workFlowSrBean.upLoadWorkFlowFile(tblWorkFlowDocuments);
             int wfdocid =  tblWorkFlowDocuments.getWfDocumentId();
             File oldfile = new File(destinationDir+"\\"+docName);
             int j =  docName.lastIndexOf('.');
            String lst =  docName.substring(j+1);
            String fst = docName.substring(0, j);
             File newFile = new File(destinationDir+"\\"+fst+"_"+wfdocid+"."+lst);
             boolean sus =   oldfile.renameTo(newFile);
                }
                if(checkret.equals("type"))
                   msg = "File type not supported";
                else if(checkret.equals("size"))
                    msg = "Max file size 2MB. Please try again";
                else if(checkret.equals("UploadProblem"))
                    msg = "Error occured during file upload. not uploaded successfully";
                else msg="File uploaded successfully";
                //response.sendRedirectFilter(request.getContextPath()+"/officer/UploadFileDialog.jsp?objectid="+objectid+"&childid="+childid+"&activityid="+activityid+"&userid="+userid+"&msg="+msg);
                response.sendRedirect("officer/UploadFileDialog.jsp?objectid="+objectid+"&childid="+childid+"&activityid="+activityid+"&userid="+userid+"&msg="+msg);
            } catch (FileUploadException ex) {
                LOGGER.error("processRequest "+logUSerId+" "+ex);           
            } catch (Exception ex) {
                LOGGER.error("processRequest "+logUSerId+" "+ex);           
            }
        }
         else
         {
             response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
         }
        } finally {
        }
        LOGGER.debug("processRequest "+logUSerId+" Ends");           
    }

    public String checkExnAndSize(String extn,long size,String userType){
        LOGGER.debug("checkExnAndSize "+logUSerId+" Starts");           
           String filecheck = "Yes";
           boolean chextn = false;
           float fsize = 0.0f;
           float dsize = 0.0f;
           int j =  extn.lastIndexOf('.');
            String lst =  extn.substring(j+1);

        CheckExtension ext = new CheckExtension();
                            TblConfigurationMaster configurationMaster =  ext.getConfigurationMaster(userType);
                           String str = configurationMaster.getAllowedExtension();
                           String[] str1 = str.split(",");
                           for(int i=0;i<str1.length;i++){
                               if(str1[i].trim().equalsIgnoreCase(lst)){
                                   chextn = true;
                               }
                           }
                           if(chextn){
                             fsize =  size/(1024*1024);
                             dsize = configurationMaster.getFileSize();
                             if(dsize > fsize){
                                 chextn=true;
                               } else{
                                 filecheck = "size";
                                   chextn = false;
                               }
                           }else {
                           filecheck = "type";}
                           LOGGER.debug("checkExnAndSize "+logUSerId+" Ends");
             return filecheck;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
