/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblEmployeeTrasfer;
import com.cptu.egp.eps.model.table.TblPartnerAdminTransfer;
import com.cptu.egp.eps.model.table.TblUserActivationHistory;
import com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author shreyansh
 */
public class AdminActDeactServlet extends HttpServlet {

    private TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
    final static Logger LOGGER = Logger.getLogger(AdminActDeactServlet.class);
    private String logUserId = "0";
    String strUsername = "";

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (session.getAttribute("userId") != null) {
            logUserId = session.getAttribute("userId").toString();
            transferEmployeeServiceImpl.setLogUserId(logUserId);
            transferEmployeeServiceImpl.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
        }
        strUsername = session.getAttribute("userName").toString();
        LOGGER.debug("processRequest" + " : " + logUserId + " Starts");
        try {
            String userIdSession = request.getSession().getAttribute("userId").toString();
            String username = request.getSession().getAttribute("userName").toString();
            String action = request.getParameter("action");
            String status = request.getParameter("status");
            String userId = request.getParameter("userid");
            String userTypeId = request.getParameter("userTypeId");
            String partnerType = "";
            String partnerId = "";

            String strEmpId = "";
            String adminId = "";
            if (request.getParameter("partnerType") != null) {
                partnerType = request.getParameter("partnerType");
            }
            if (request.getParameter("partnerId") != null) {
                partnerId = request.getParameter("partnerId");
            }
            if (request.getParameter("empId") != null) {
                strEmpId = request.getParameter("empId");
            }
            if (request.getParameter("adminId") != null) {
                adminId = request.getParameter("adminId");
            }
            String fullName = request.getParameter("fullName");
            String emailId = request.getParameter("emailId");
            String comments = request.getParameter("txtarea_comments");

            String[] mailTo = {emailId};
            if ("changestatus".equalsIgnoreCase(action)) {
                comments = request.getParameter("comments");
                boolean flag = transferEmployeeServiceImpl.changeStatus(userId, status,partnerType,comments,userTypeId);
                partnerId = transferEmployeeServiceImpl.getPartnerId(userId);
                addActivationHistory(Integer.parseInt(partnerId), Integer.parseInt(userTypeId), status, Integer.parseInt(userIdSession), username, Integer.parseInt(userId), fullName, comments);
                sendMail(mailTo, status, comments);
                if (flag) {
                    if (userTypeId.equalsIgnoreCase("6") || userTypeId.equalsIgnoreCase("7")) {
                        response.sendRedirect("admin/ManageSbDpAdmin.jsp?partnerType=" + partnerType + "&userStatus=" + status + "&succFlag=y");
                        //response.sendRedirect("ManageSbDpAdmin.jsp?partnerType=" + partnerType + "&userStatus=" + status + "&succFlag=y");
                    } else {
                        response.sendRedirect("admin/ManageSbBranchAdmin.jsp?userStatus=" + status + "&succFlag=y");
                        //response.sendRedirectFilter("ManageSbBranchAdmin.jsp?userStatus=" + status + "&succFlag=y");
                    }




                } else {
                    response.sendRedirect(request.getContextPath() + "/StackTrace.jsp");
                }

            } else if ("changeStatusSbDevUser".equalsIgnoreCase(action)) {
                boolean bSuccess = transferEmployeeServiceImpl.changeStatus(userId, status,"user",comments,userTypeId);
                if (bSuccess) {
                    List<TblPartnerAdminTransfer> listTblPartnerAdminTransfer = transferEmployeeServiceImpl.getPartnerTransferDetail(userId, partnerId);
                    int id = listTblPartnerAdminTransfer.get(0).getPartTransId();
                    addActivationHistory(id, Integer.valueOf(userTypeId), status, Integer.valueOf(logUserId), strUsername, Integer.valueOf(userId), fullName, comments);
                    sendMail(mailTo, status, comments);
                    if (bSuccess) {
                        //response.sendRedirect(request.getContextPath() + "/admin/ManageSbDpUser.jsp?partnerType=" + partnerType + "&userStatus=" + status + "&succFlag=y");
                        response.sendRedirect("admin/ManageSbDpUser.jsp?partnerType=" + partnerType + "&userStatus=" + status + "&succFlag=y");
                    } else {
                        response.sendRedirect(request.getContextPath() + "/StackTrace.jsp");
                    }
                }
            } else if ("changeStatusGovtUser".equalsIgnoreCase(action)) {
                boolean bSuccess = transferEmployeeServiceImpl.changeStatus(String.valueOf(userId), status,partnerType,comments,"3");
                if (bSuccess) {
                    List<TblEmployeeTrasfer> listTblEmpTransfer = transferEmployeeServiceImpl.getGovtUserDetail(String.valueOf(userId), strEmpId);
                    int id = listTblEmpTransfer.get(0).getGovUserId();
                    addActivationHistory(id, Integer.valueOf(userTypeId), status, Integer.valueOf(logUserId), strUsername, Integer.valueOf(userId), fullName, comments);
                    sendMail(mailTo, status, comments);
                    if (bSuccess) {
                        response.sendRedirect("admin/ManageGovtUser.jsp?userStatus=" + status + "&succFlag=y");
                    } else {
                        response.sendRedirect(request.getContextPath() + "/StackTrace.jsp");
                    }
                }
            } else if ("changeStatusAdminUser".equalsIgnoreCase(action)) {
                String actionAudit = "";
                if (userTypeId.equals("5")) {
                    if ("deactive".equalsIgnoreCase(status)) {
                        actionAudit = "Deactivate Organization Admin";
                    } else if ("approved".equals(status)) {
                        actionAudit = "Activate Organization Admin";
                    }
                    transferEmployeeServiceImpl.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                } else if (userTypeId.equals("8")) {
                    if ("deactive".equalsIgnoreCase(status)) {
                        actionAudit = "Deactivate Content Admin";
                    } else if ("approved".equals(status)) {
                        actionAudit = "Activate Content Admin";
                    }
                    transferEmployeeServiceImpl.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                }else if (userTypeId.equals("12")) {
                    if ("deactive".equalsIgnoreCase(status)) {
                        actionAudit = "Deactivate Procurement Expert User";
                    } else if ("approved".equals(status)) {
                        actionAudit = "Activate Procurement Expert User";
                    }
                    transferEmployeeServiceImpl.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                }else if (userTypeId.equals("19")) {
                    if ("deactive".equalsIgnoreCase(status)) {
                        actionAudit = "Deactivate O & M Admin";
                    } else if ("approved".equals(status)) {
                        actionAudit = "Activate O & M Admin";
                    }
                    transferEmployeeServiceImpl.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                }
                else if (userTypeId.equals("20")) {
                    if ("deactive".equalsIgnoreCase(status)) {
                        actionAudit = "Deactivate O & M User";
                    } else if ("approved".equals(status)) {
                        actionAudit = "Activate O & M User";
                    }
                    transferEmployeeServiceImpl.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                }
                boolean bSuccess = transferEmployeeServiceImpl.changeStatus(String.valueOf(userId), status, actionAudit);
                if (bSuccess) {
                    addActivationHistory(Integer.valueOf(adminId), Integer.valueOf(userTypeId), status, Integer.valueOf(logUserId), strUsername, Integer.valueOf(userId), fullName, comments);
                    sendMail(mailTo, status, comments);
                    if (bSuccess) {
                        if ("5".equalsIgnoreCase(userTypeId)) { // Org Admin Users
                            response.sendRedirect("admin/OrganizationAdminGrid.jsp?userStatus=" + status + "&userTypeid=" + userTypeId + "&succFlag=y");
                            //response.sendRedirectFilter("OrganizationAdminGrid.jsp?userStatus=" + status + "&userTypeid=" + userTypeId + "&succFlag=y");

                        } else if ("8".equalsIgnoreCase(userTypeId)) {//Content Admin Users
                            response.sendRedirect("admin/ContentAdminGrid.jsp?userStatus=" + status + "&userTypeid=" + userTypeId + "&succFlag=y");
                            //response.sendRedirect(request.getContextPath() + "/admin/ContentAdminGrid.jsp?userStatus=" + status + "&userTypeid=" + userTypeId + "&succFlag=y");
                        } else if ("19".equalsIgnoreCase(userTypeId)) {//Content Admin Users
                            response.sendRedirect("admin/OAndMAdminGrid.jsp?userStatus=" + status + "&userTypeid=" + userTypeId + "&succFlag=y");
                            //response.sendRedirect(request.getContextPath() + "/admin/ContentAdminGrid.jsp?userStatus=" + status + "&userTypeid=" + userTypeId + "&succFlag=y");
                        } else if ("20".equalsIgnoreCase(userTypeId)) {//Content Admin Users
                            response.sendRedirect("admin/OAndMAdminGrid.jsp?userStatus=" + status + "&userTypeid=" + userTypeId + "&succFlag=y&isUser=Yes");
                            //response.sendRedirect(request.getContextPath() + "/admin/ContentAdminGrid.jsp?userStatus=" + status + "&userTypeid=" + userTypeId + "&succFlag=y");
                        }
                        else {
                            response.sendRedirect("admin/ProcExpertAdminGrid.jsp?userStatus=" + status + "&userTypeid=" + userTypeId + "&succFlag=y");
                            //response.sendRedirectFilter("ProcExpertAdminGrid.jsp?userStatus=" + status + "&userTypeid=" + userTypeId + "&succFlag=y");
                        }
                    } else {
                        response.sendRedirect(request.getContextPath() + "/StackTrace.jsp");
                    }
                }

            } else if ("changePEAdminStatus".equalsIgnoreCase(action)) {
                comments = request.getParameter("comments");
                String peAdminId = "";
                boolean flag = false;
                if (userTypeId.equals("4")) {
                    transferEmployeeServiceImpl.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
                }
                String actionAudit = "";
                if ("deactivate".equalsIgnoreCase(status)) {
                    actionAudit = "Deactivate PE Admin";
                } else if ("approved".equalsIgnoreCase(status)) {
                    actionAudit = "Activate PE Admin";
                }
                flag = transferEmployeeServiceImpl.changeStatus(userId, status, actionAudit);
                peAdminId = transferEmployeeServiceImpl.getPeAdminId(userId);
                flag = addActivationHistory(Integer.parseInt(peAdminId), Integer.parseInt(userTypeId), status, Integer.parseInt(userIdSession), username, Integer.parseInt(userId), fullName, comments);
                sendMail(mailTo, status, comments);
                if (flag) {
                    response.sendRedirect("admin/PEAdminGrid.jsp?userTypeid=" + userTypeId + "&userStatus=" + status + "&succFlag=y");
                    //response.sendRedirectFilter("PEAdminGrid.jsp?userTypeid=" + userTypeId + "&userStatus=" + status + "&succFlag=y");
                } else {
                    response.sendRedirect(request.getContextPath() + "/StackTrace.jsp");
                }
            }
        } catch (Exception e) {
            LOGGER.error("processRequest : " + e);


        }
        LOGGER.debug("processRequest" + " : " + logUserId + " ends");


    }

    /**
     * sending mail and sms
     * @param mailTo
     * @param status
     * @param strReason
     */
    private void sendMail(String[] mailTo, String status, String strReason) {
        LOGGER.debug("sendMail" + " : " + logUserId + " Starts");
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        MailContentUtility mailContentUtility = new MailContentUtility();
        List list = null;


        if ("approved".equalsIgnoreCase(status)) {
            list = mailContentUtility.getUserActivatationContent();


        } else {
            list = mailContentUtility.getUserDeactivationContent(strReason);


        }
        String mailSub = list.get(0).toString();
        String mailText = list.get(1).toString();
        sendMessageUtil.setEmailTo(mailTo);
        sendMessageUtil.setEmailSub(mailSub);
        sendMessageUtil.setEmailMessage(mailText);
        sendMessageUtil.sendEmail();
        LOGGER.debug("sendMail" + " : " + logUserId + " Ends");


    }

    /**
     * inserting values
     * @param id
     * @param iUserTypeId
     * @param strAction
     * @param actionby
     * @param actionByName
     * @param userId
     * @param strFullName
     * @param strComments
     * @return true if inserted else false
     */
    private boolean addActivationHistory(int id, int iUserTypeId, String strAction,
            int actionby, String actionByName, int userId, String strFullName, String strComments) {
        LOGGER.debug("addActivationHistory" + " : " + logUserId + " Starts");


        boolean bSuccess = false;
        TblUserActivationHistory tblUserActivationHistory = new TblUserActivationHistory();
        tblUserActivationHistory.setId(id);
        tblUserActivationHistory.setUserTypeId(iUserTypeId);
        tblUserActivationHistory.setActionDt(new java.sql.Date(new java.util.Date().getTime()));
        tblUserActivationHistory.setAction(strAction);
        tblUserActivationHistory.setActionBy(actionby);
        tblUserActivationHistory.setActionByName(actionByName);
        tblUserActivationHistory.setUserId(userId);
        tblUserActivationHistory.setFullName(strFullName);
        tblUserActivationHistory.setComments(strComments);
        bSuccess = transferEmployeeServiceImpl.addActivationHistory(tblUserActivationHistory);
        LOGGER.debug("addActivationHistory" + " : " + logUserId + " Ends");


        return bSuccess;


    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);


    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);


    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";

    }// </editor-fold>
}
