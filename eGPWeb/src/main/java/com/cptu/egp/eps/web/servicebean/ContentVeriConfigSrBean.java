/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblContentVerification;
import com.cptu.egp.eps.service.serviceinterface.ContentVeriConfigService;
import com.cptu.egp.eps.web.databean.ContentVeriConfigDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class ContentVeriConfigSrBean {

    static final Logger logger = Logger.getLogger(ContentVeriConfigSrBean.class);
    private ContentVeriConfigService contentVeriConfigService = (ContentVeriConfigService) AppContext.getSpringBean("contentVeriConfigService");
    private String logUserId = "0";

    public void setLogUserId(String logUserId) {
        contentVeriConfigService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    public String addContent(ContentVeriConfigDtBean con) {
        logger.debug("addContent : " + logUserId + " Starts");
        String msg = null;
        try {
            TblContentVerification content = new TblContentVerification();
            content.setIsProcureExpert(con.getIsProcureExpert());
            content.setIsPublicForum(con.getIsPublicForum());
            contentVeriConfigService.addContent(content);
            msg = "Values Added";
        } catch (Exception e) {
            logger.error("addContent : " + logUserId + " "+e);
            msg = "Error";
        }
        logger.debug("addContent : " + logUserId + " Ends");
        return msg;
    }

    public List<TblContentVerification> getContentVerification() {
        logger.debug("getContentVerification : " + logUserId + " Starts");
        logger.debug("getContentVerification : " + logUserId + " Ends");
        return contentVeriConfigService.getTblContentVerification();
    }
}
