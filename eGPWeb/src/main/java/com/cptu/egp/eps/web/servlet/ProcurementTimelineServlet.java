/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.ProcurementTimelineData;
import com.cptu.egp.eps.service.serviceinterface.ProcurementTimelineService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Istiak (Dohatec) - 26.May.15
 */
public class ProcurementTimelineServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            
            ProcurementTimelineService oProcurementTimelineService = (ProcurementTimelineService) AppContext.getSpringBean("ProcurementTimelineService");

            String action = request.getParameter("action").trim();

            if(action.equalsIgnoreCase("ViewTimeline")) //  Show Table
            {
                String flag = request.getParameter("flag").trim();

                String id = "";
                if(request.getParameter("id") != null)
                    id = request.getParameter("id").trim();

                String cType = "";
                if(request.getParameter("configureType") != null)
                    cType = request.getParameter("configureType").trim();
                
                List<ProcurementTimelineData> timeline = oProcurementTimelineService.procTimelineData(action, cType, id, "", "", "", "", "", "", "", "", "", "", "", "");

                if("configuration".equalsIgnoreCase(flag))
                    ShowApprovalTimelineForConfiguration(out, timeline);
                else
                    ShowApprovalTimeline(out, timeline);
            }
            else if(action.equalsIgnoreCase("DeleteRows"))  // Delete Rules
            {
                String id = request.getParameter("rowNo").trim();
                String flag = request.getParameter("flag").trim();
                List<ProcurementTimelineData> timelineAfterDel = oProcurementTimelineService.procTimelineData(action, id, "", "", "", "", "", "", "", "", "", "", "", "", "");
                if("false".equalsIgnoreCase(timelineAfterDel.get(0).getFieldName1()) || timelineAfterDel == null)
                    out.print("false");
                else
                    out.print("true");
            }
            else if(action.equalsIgnoreCase("AddUpdate"))   // Insert or Update a new Rule
            {
                try{
                    boolean isSuccess = RulesAddUpdate(request, oProcurementTimelineService);
                    if(isSuccess)
                        response.sendRedirect("/admin/ProcurementApprovalTimelineConfig.jsp?isEdit=y&msg=y");
                    else
                        response.sendRedirect("/admin/ProcurementApprovalTimelineConfig.jsp?isEdit=y&msg=n");
                }catch(Exception e){
                    response.sendRedirect("/admin/ProcurementApprovalTimelineConfig.jsp?isEdit=y&msg=n");
                }
            }
        } catch(Exception e){
            String msg = "No records found";
            ErrorMessage(out, msg);
            System.out.print("\nProcurementTimelineServlet : " + e);
        }finally {
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void ShowApprovalTimeline(PrintWriter out, List<ProcurementTimelineData> timeline) {
        if(timeline.size() > 0)
        {
            for(ProcurementTimelineData data : timeline)
            {
                /* Column Name 
                 * FieldName1() = Rule Id
                 * FieldName2() = Approval Authority
                 * FieldName3() = Procurement Nature
                 * FieldName4() = No of Days For TSC
                 * FieldName5() = No of Days For TEC/PEC
                 * FieldName6() = Total No of Days For Evamuation
                 * FieldName7() = No of Days For Approval
                 */
                
                //Changed by Emtaz on 19/April/2016
                String ChangedApprovalAuthorityForBhutan = "";
                if(data.getFieldName2().equalsIgnoreCase("HOPE"))
                {
                    ChangedApprovalAuthorityForBhutan = "HOPA";
                }
                else
                {
                    ChangedApprovalAuthorityForBhutan = data.getFieldName2();
                }
                
                out.print("<tr>");
                out.print("<td class='t-align-center' width='15%'>" + ChangedApprovalAuthorityForBhutan + "</td>");   
                out.print("<td class='t-align-center' width='15%'>" + data.getFieldName3() + "</td>");   
                out.print("<td class='t-align-center' width='10%'>" + data.getFieldName4() + "</td>");  
                out.print("<td class='t-align-center' width='15%'>" + data.getFieldName5() + "</td>"); 
                out.print("<td class='t-align-center' width='15%'>" + data.getFieldName6() + "</td>");

                String daysForApproval = data.getFieldName7();   
                if(daysForApproval.equalsIgnoreCase("-1"))
                    daysForApproval = "Upto Tender Validity";
                out.print("<td class='t-align-center' width='15%'>" + daysForApproval + "</td>");

                String edit = "<a href='ProcurementApprovalTimelineConfig.jsp?id=" + data.getFieldName1() + "&cType=single&isEdit=y'>Edit</a>";
                String delete = "<a href='javascript:void(0);' id='del_" + data.getFieldName1() + "' onclick='DeleteRow(" + data.getFieldName1() + ")'>Delete</a>";
                out.print("<td class='t-align-center' width='15%'>" + edit + " | " + delete + "</td>");
                out.print("</tr>");
            }
        }
        else
        {
            String msg = "No records found";
            ErrorMessage(out, msg);
        }
    }

    private void ShowApprovalTimelineForConfiguration(PrintWriter out, List<ProcurementTimelineData> timeline) {
        if(timeline.size() > 0)
        {
            for(ProcurementTimelineData data : timeline)
            {
                 /* Column Name
                 * FieldName1() = Rule Id
                 * FieldName2() = Approval Authority
                 * FieldName3() = Procurement Nature
                 * FieldName4() = No of Days For TSC
                 * FieldName5() = No of Days For TEC/PEC
                 * FieldName6() = Total No of Days For Evamuation
                 * FieldName7() = No of Days For Approval
                 */
                
                out.print("<tr>");
                out.print("<td class='t-align-center' width='15%'>");
                out.print("<input id='chk_" + data.getFieldName1() + "' name='RulesNo' type='checkbox' value='" + data.getFieldName1() + "' />");
                out.print("<input type='hidden' name='ruleId' id='ruleId_"+ data.getFieldName1() + "' value='" + data.getFieldName1() + "'>");
                out.print("<input type='hidden' name='NewOldEdit' id='NewOldEdit_"+ data.getFieldName1() + "' value='Old'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='15%'>" + LoadApprovalAuthority(data.getFieldName1(), data.getFieldName2()).toString() + " ");
                out.print("<input type='hidden' name='AAOld' id='AAOld_" + data.getFieldName1() + "' value='" + data.getFieldName2() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='15%'>" + LoadProcurementNature(data.getFieldName1(), data.getFieldName3()).toString() + " ");
                out.print("<input type='hidden' name='PNOld' id='PNOld_" + data.getFieldName1() + "' value='" + data.getFieldName3() + "'>");
                out.print("</td>");

                out.print("<td class='t-align-center' width='15%'><input type='text' class='formTxtBox_1 noOfDaysForTSC' onblur='CalculateDaysForEvaluation(" + data.getFieldName1() + ")' name='NoOfDaysForTSC' id='NoOfDaysForTSC_" + data.getFieldName1() + "' value='" + data.getFieldName4() + "'></td>");
                out.print("<td class='t-align-center' width='15%'><input type='text' class='formTxtBox_1 noOfDaysForTECorPEC' onblur='CalculateDaysForEvaluation(" + data.getFieldName1() + ")' name='NoOfDaysForTECorPEC' id='NoOfDaysForTECorPEC_" + data.getFieldName1() + "' value='" + data.getFieldName5() + "'></td>");

                out.print("<td class='t-align-center' width='15%'><input type='text' class='formTxtBox_1 totalNoOfDaysForEvaluation' name='TotalNoOfDaysForEvaluation' id='TotalNoOfDaysForEvaluation_" + data.getFieldName1() + "' value='" + data.getFieldName6() + "' readonly='readonly'> ");
                out.print("<input type='hidden' name='TDEvalOld' id='TDEvalOld_" + data.getFieldName1() + "' value='" + data.getFieldName6() + "'>");
                out.print("</td>");
                
                String daysForApproval = data.getFieldName7();
                String readonly = "";
                if(daysForApproval.equalsIgnoreCase("-1"))
                {
                    daysForApproval = "Upto Tender Validity";
                    readonly = " readonly='readonly'";
                }
                out.print("<td class='t-align-center' width='15%'><input type='text' class='formTxtBox_1 noOfDaysForApproval' onblur='CheckApprovalDays(" + data.getFieldName1() + ")' name='NoOfDaysForApproval' id='NoOfDaysForApproval_" + data.getFieldName1() + "' value='" + daysForApproval + "'" + readonly + ">");
                out.print(" <input type='hidden' name='DAOld' id='DAOld_" + data.getFieldName1() + "' value='" + daysForApproval + "'>");
                out.print("</td>");

                out.print("</tr>");
            }
        }
        else
        {
            String msg = "No records found";
            ErrorMessage(out, msg);
        }
    }

    private StringBuilder LoadProcurementNature(String id, String procNature) {
        StringBuilder ddlProcNature = new StringBuilder();
        String selected = " selected='selected'";
        String goods = "";
        String works = "";
        String services = "";

        if("Goods".equalsIgnoreCase(procNature))
            goods = selected;
        else if("Works".equalsIgnoreCase(procNature))
            works = selected;
        else if("Services".equalsIgnoreCase(procNature))
            services = selected;

        ddlProcNature.append("<select name='ProcurementNature' class='formTxtBox_1 procurementNature' id='ProcurementNature_").append(id).append("' style='width:100px;' onChange='NewOrEdit(").append(id).append(")'>");
        ddlProcNature.append("<option value='Goods'").append(goods).append(">Goods</option>");        
	ddlProcNature.append("<option value='Works'").append(works).append(">Works</option>");        
	ddlProcNature.append("<option value='Services'").append(services).append(">Services</option>");
        ddlProcNature.append("</select>");
        return ddlProcNature;
    }

    private StringBuilder LoadApprovalAuthority(String id, String approvalAuthority) {
        StringBuilder ddlApprovalAuthority = new StringBuilder();
        String selected = " selected='selected'";
        String ao = "";
        String bod = "";
        String ccgp = "";
        String hope = "";
        String minister = "";
        String pd = "";

        if("ao".equalsIgnoreCase(approvalAuthority))
            ao = selected;
        else if("bod".equalsIgnoreCase(approvalAuthority))
            bod = selected;
        else if("ccgp".equalsIgnoreCase(approvalAuthority))
            ccgp = selected;
        else if("hope".equalsIgnoreCase(approvalAuthority))
            hope = selected;
        else if("minister".equalsIgnoreCase(approvalAuthority))
            minister = selected;
        else if("pd".equalsIgnoreCase(approvalAuthority))
            pd = selected;

        ddlApprovalAuthority.append("<select name='ApprovalAuthority' class='formTxtBox_1 approvalAuthority' id='ApprovalAuthority_").append(id).append("' style='width:100px;' onChange='ChangeApprovalAuthority(").append(id).append(")'>");
        ddlApprovalAuthority.append("<option value='AO'").append(ao).append(">AO</option>");
        ddlApprovalAuthority.append("<option value='BOD'").append(bod).append(">BOD</option>");
        ddlApprovalAuthority.append("<option value='CCGP'").append(ccgp).append(">CCGP</option>");
        ddlApprovalAuthority.append("<option value='HOPE'").append(hope).append(">HOPA</option>");
        ddlApprovalAuthority.append("<option value='Minister'").append(minister).append(">Minister</option>");
        ddlApprovalAuthority.append("<option value='PD'").append(pd).append(">PD</option>");
        ddlApprovalAuthority.append("</select>");
        return ddlApprovalAuthority;
    }

    private void ErrorMessage(PrintWriter out, String msg) {
        out.print("<tr>");
        out.print("<td colspan=\"7\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\">" + msg + "</td>");
        out.print("</tr>");
    }

    private boolean RulesAddUpdate(HttpServletRequest request, ProcurementTimelineService oProcurementTimelineService) {
        String[] ruleId = request.getParameterValues("ruleId");
        String[] NewOldEdit = request.getParameterValues("NewOldEdit");
        String[] appAuthority = request.getParameterValues("ApprovalAuthority");
        String[] procNature = request.getParameterValues("ProcurementNature");
        String[] noOfDaysForTSC = request.getParameterValues("NoOfDaysForTSC");
        String[] noOfDaysForTECorPEC = request.getParameterValues("NoOfDaysForTECorPEC");
        String[] noOfDaysForApproval = request.getParameterValues("NoOfDaysForApproval");

        return oProcurementTimelineService.insertOrUpdate(ruleId, NewOldEdit, appAuthority, procNature, noOfDaysForTSC, noOfDaysForTECorPEC, noOfDaysForApproval);
    }

}
