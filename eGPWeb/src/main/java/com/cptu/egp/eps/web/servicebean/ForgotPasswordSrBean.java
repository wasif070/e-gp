/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblForgotPassword;
import com.cptu.egp.eps.service.serviceinterface.ForgotPasswordService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Administrator
 */
public class ForgotPasswordSrBean extends HttpServlet
{

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String logUserId = "0";
    ForgotPasswordService forgotPasswordService = (ForgotPasswordService) AppContext.getSpringBean("ForgotPasswordService");

    static final Logger LOGGER = Logger.getLogger(ForgotPasswordSrBean.class);

    
    /**
     * This servlet is used for forgot password request
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        
        }

    // <editor-fold defaultstate="expanded" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        LOGGER.debug("doPost "+logUserId+" Starts");
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
        }
        forgotPasswordService.setLogUserId(logUserId);
        
        PrintWriter out = response.getWriter();
        try {
            String functionName = request.getParameter("funName");
            if (functionName!= null &&functionName.equals("chkMailId")) {
                LOGGER.debug("doPost "+logUserId+" chkMailId Starts");
                String emailId = request.getParameter("forgetemailId");
                String str = chkMailId(emailId);
                String action=request.getParameter("action");
                out.print(str);
                LOGGER.debug("doPost "+logUserId+" chkMailId Ends");
                out.flush();
            }
            else if(functionName!= null &&functionName.equals("chkPwd"))
            {
                LOGGER.debug("doPost "+logUserId+" chkPwd Starts");
                String newPass=request.getParameter("newPass");
                String emailId = request.getParameter("forgetemailId");
                boolean isVerified=forgotPasswordService.verifyPassword(emailId, SHA1HashEncryption.encodeStringSHA1(newPass));
                if(isVerified){
                    out.print("New Password MUST be different than the Current Password");
                }
                LOGGER.debug("doPost "+logUserId+" chkPwd Ends");
                out.flush();
            }
            else
            {
                String hintAns=request.getParameter("hintAns");
                String emailId = request.getParameter("forgetemailId");
                boolean isVerified=forgotPasswordService.verifyHintAnswer(hintAns, emailId);

                if(isVerified)
                {
                    LOGGER.debug("doPost "+logUserId+" isVerified Starts");
                    session.setAttribute("emailId", emailId);                   
                    String password="egp@forgotpwd";
                    String data=null;
                    try {
                    char[] bpassword = password.toCharArray();
                    byte[] salt = new byte[8];
                    Random r = new Random();
                    r.nextBytes(salt);
                    PBEKeySpec kspec = new PBEKeySpec(bpassword);
                    SecretKeyFactory kfact = SecretKeyFactory.getInstance("pbewithmd5anddes");
                    SecretKey key = kfact.generateSecret(kspec);
                    PBEParameterSpec pspec = new PBEParameterSpec(salt, 1000);
                    Cipher cipher = Cipher.getInstance("pbewithmd5anddes");
                    cipher.init(Cipher.ENCRYPT_MODE, key, pspec);
                    byte[] enc = cipher.doFinal(emailId.getBytes());
                    BASE64Encoder encoder = new BASE64Encoder();
                    String saltString = encoder.encode(salt);
                    String ciphertextString = encoder.encode(enc);
                    data =  saltString + ciphertextString;
                    } catch (IllegalBlockSizeException ex) {
                        LOGGER.error("doPost "+logUserId+" :"+ex);
                    } catch (BadPaddingException ex) {
                        LOGGER.error("doPost "+logUserId+" :"+ex);
                    } catch (InvalidKeyException ex) {
                        LOGGER.error("doPost "+logUserId+" :"+ex);
                    } catch (InvalidAlgorithmParameterException ex) {
                        LOGGER.error("doPost "+logUserId+" :"+ex);
                    } catch (NoSuchPaddingException ex) {
                        LOGGER.error("doPost "+logUserId+" :"+ex);
                    } catch (InvalidKeySpecException ex) {
                        LOGGER.error("doPost "+logUserId+" :"+ex);
                    } catch (NoSuchAlgorithmException ex) {
                        LOGGER.error("doPost "+logUserId+" :"+ex);
                    }                    
                    String link=request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/"))+"/resources/common/ChangeForgotPassward.jsp?mId="+data;
                    String mails[]={emailId};
                    SendMessageUtil sendMessageUtil = new SendMessageUtil();
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    String mailText = mailContentUtility.getForgotPassMailContent(link);
                    sendMessageUtil.setEmailTo(mails);
                    sendMessageUtil.setEmailSub("e-GP: Forgot Password.");
                    sendMessageUtil.setIsForgotPassword(true);
                    sendMessageUtil.setEmailMessage(mailText);
                    
                    boolean flag =false;
                    TblForgotPassword tblForgotPassword = new TblForgotPassword();
                    tblForgotPassword.setEmailId(emailId);
                    tblForgotPassword.setUserHash(data);
                    tblForgotPassword.setReqDt(new Date());
                    forgotPasswordService.deleteRandomeCode(emailId);
                    flag = forgotPasswordService.addRandomeCode(tblForgotPassword);
                    if(flag)
                    {
                        sendMessageUtil.sendEmail();
                    }                    
                    sendMessageUtil=null;
                    mailContentUtility=null;
                    
                    response.sendRedirect("ForgotPassword.jsp?sucfpmsg=y");
                }
                else{
                    response.sendRedirect("ForgotPassword.jsp?msg=fail");
            }
        }
            LOGGER.debug("doPost "+logUserId+" isVerified Ends");
        }
        catch(Exception ex)
        {
            LOGGER.error("doPost "+logUserId+" :"+ex);
            response.sendRedirect("admin/ForgotPassword.jsp?msg=fail");
        }
        finally {
            out.close();
        }

    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

    /**
     * ChkMailId checks mailId
     * @param mailId EmailId to be Checked
     * @return
     */
    private String chkMailId(String mailId)
    {
        LOGGER.debug("chkMailId "+logUserId+" Starts");
        String result = "";
        try {
            
            result = forgotPasswordService.chkMailId(mailId,"forgot");
        }
        catch (Exception ex) {
            LOGGER.error("chkMailId "+logUserId+" :"+ex);
        }
        LOGGER.debug("chkMailId "+logUserId+" Starts");
        return result;
    }
    
}