/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCompanyDocuments;
import com.cptu.egp.eps.model.table.TblTempCompanyDocuments;
import com.cptu.egp.eps.model.table.TblTempTendererMaster;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class SupportDocServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    final static Logger logger = Logger.getLogger(SupportDocServlet.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        PrintWriter out = response.getWriter();
        String logUserId = null;
        try {
            logUserId = request.getSession().getAttribute("userId").toString();
            logger.debug("SupportDocServlet : " + logUserId + " Starts");
            if (request.getParameter("funName").equals("download")) {
                logger.debug("SupportDocServlet : " + logUserId + " funName : " + request.getParameter("funName"));
                File file = null;
                if ("y".equals(request.getParameter("iscontent"))) {
                    file = new File(FilePathUtility.getFilePath().get("FileUploadServlet") + request.getParameter("uId") + "\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                } else {
                    file = new File(FilePathUtility.getFilePath().get("FileUploadServlet") + request.getSession().getAttribute("userId").toString() + "\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                }
                InputStream fis = new FileInputStream(file);
                byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                int offset = 0;
                int numRead = 0;
                while ((offset < buf.length)
                        && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                    offset += numRead;

                }
                fis.close();
                FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
                fileEncryptDecryptUtil = null;
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName") + "\"");
                ServletOutputStream outputStream = response.getOutputStream();
                outputStream.write(buf);
                outputStream.flush();
                outputStream.close();
            } else if (request.getParameter("funName").equals("remove")) {
                logger.debug("SupportDocServlet : " + logUserId + " funName : " + request.getParameter("funName"));
                TempCompanyDocumentsSrBean tcdsb = new TempCompanyDocumentsSrBean();
                TblTempCompanyDocuments documents = new TblTempCompanyDocuments();
                documents.setCompanyDocId(Integer.parseInt(request.getParameter("docId")));
                documents.setDocumentBrief("");
                documents.setDocumentName(request.getParameter("docName"));
                documents.setDocumentSize("");
                documents.setDocumentType("");
                documents.setTblTempTendererMaster(new TblTempTendererMaster(1));
                documents.setUploadedDate(new Date());
                String qString = "";
                if (!tcdsb.deleteDoc(documents, Integer.parseInt(request.getSession().getAttribute("userId").toString()))) {
                    qString = "?msg=fail";
                }
                response.sendRedirect("SupportingDocuments.jsp" + qString);
            } else if (request.getParameter("funName").equals("reapplydownload")) {
                logger.debug("SupportDocServlet : " + logUserId + " funName : " + request.getParameter("funName"));
                File file = null;
                if ("y".equals(request.getParameter("iscontent"))) {
                    file = new File(FilePathUtility.getFilePath().get("FileUploadServlet") + request.getParameter("uId") + "\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                } else {
                    file = new File(FilePathUtility.getFilePath().get("FileUploadServlet") + request.getSession().getAttribute("userId").toString() + "\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                }
                InputStream fis = new FileInputStream(file);
                byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                int offset = 0;
                int numRead = 0;
                while ((offset < buf.length)
                        && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                    offset += numRead;

                }
                fis.close();
                FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
                fileEncryptDecryptUtil = null;
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName") + "\"");
                ServletOutputStream outputStream = response.getOutputStream();
                outputStream.write(buf);
                outputStream.flush();
                outputStream.close();
            } else if (request.getParameter("funName").equals("reapplyremove")) {
                logger.debug("SupportDocServlet : " + logUserId + " funName : " + request.getParameter("funName"));

                int tendererId = Integer.parseInt(request.getParameter("tId"));
                int companyId = Integer.parseInt(request.getParameter("cId"));

                TempCompanyDocumentsSrBean tcdsb = new TempCompanyDocumentsSrBean();
                TblCompanyDocuments documents = new TblCompanyDocuments();
                documents.setCompanyDocId(Integer.parseInt(request.getParameter("docId")));
                documents.setDocumentBrief("");
                documents.setDocumentName(request.getParameter("docName"));
                documents.setDocumentSize("");
                documents.setDocumentTypeId("");
                documents.setTblTendererMaster(new TblTendererMaster(tendererId));
                documents.setUploadedDate(new Date());
                String qString = "";
                if (!tcdsb.deleteDocReapply(documents, Integer.parseInt(request.getSession().getAttribute("userId").toString()))) {
                    qString = "?msg=fail";
                }
                response.sendRedirect("tenderer/NewSupportingDocuments.jsp?tId=" + tendererId + "&cId=" + companyId);
            } else if (request.getParameter("funName").equals("removeEsign")) {
                logger.debug("SupportDocServlet : " + logUserId + " funName : " + request.getParameter("funName"));
                TempCompanyDocumentsSrBean tcdsb = new TempCompanyDocumentsSrBean();
                tcdsb.deleteEsign(Integer.parseInt(request.getParameter("docId").toString()), request.getParameter("docName"), 1);
                response.sendRedirect("Authenticate.jsp");
            }
        } finally {
            logger.debug("SupportDocServlet : " + logUserId + " Ends");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
