/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails;
import com.cptu.egp.eps.service.serviceinterface.TenderBidService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class MsgBoxContentUtility {
    static final Logger LOGGER = Logger.getLogger(MsgBoxContentUtility.class);
    public static final String URL = XMLReader.getMessage("url");
    
    public String contAdmMsgFinalSub(String emailId) {
        LOGGER.debug("contAdmMsgFinalSub Starts:");
        StringBuffer sb = new StringBuffer();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "Dear User," + "<br/><br/><span>"
                + "A new user with e-mail ID : "+emailId+" has created his profile on e-GP portal.<br/><br/>"
                + "Please verify user profile to approve or reject it.<br/><br/>"
                + "Please note that the profile of user will remain PENDING till the time you do not approve / reject it.<br/><br/>"
                + "<b>e-GP User Registration Desk.</b><br/><br/>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contAdmMsgFinalSub :"+e);
            AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        
        LOGGER.debug("contAdmMsgFinalSub Ends:");
        return sb.toString();
    }

    public String contAppPublish(String pkgNo[],String pkgDesc[], int pkgId[],int appId) {
        LOGGER.debug("contAppPublish Starts:");
       StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "Dear User," + "<br/><br/><span>"
                + "This is to inform you that new package of your choice has been published on e-GP portal.<br/><br/>"
                + "<b>Project Detail(s) :</b>");
                for(int i=0;i<pkgNo.length;i++){
                    sb.append("<br/><b>Package No. :</b>&nbsp;&nbsp;").append(pkgNo[i]);
                    sb.append("<br/><b>Package Description :</b>&nbsp;&nbsp;<a href='"+XMLReader.getMessage("url")+"resources/common/ViewPackageDetail.jsp?appId="+appId+"&pkgId="+pkgId[i]+"&from=home' target=\"_blank\">").append(pkgDesc[i]).append("</a><br/>");
                }
                sb.append("</br><b>Government Procurement and Property Management Division (GPPMD)<br/>Department of National Properties<br/>Ministry of Finance .</b></b><br/><br/></span></td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contAppPublish :"+e);
            AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        
                LOGGER.debug("contAppPublish Ends:");
        return sb.toString();
    }

    /**
     * Publish Corrigendum
     * Tenderer/Consultant
     * @param tenderid
     * @param refNo
     * @return String
     */
    public String tenderCorriPubMsgBox(String tenderid,String refNo){
        LOGGER.debug("tenderCorriPubMsgBox Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that an Amendment /Corrigendum is being published on <br/>"
                + "e-GP portal for below given Tender/Proposal.<br/><br/><br/>"
                + "Id: "+tenderid+"<br/>"
                + "Ref No: "+refNo+"<br/><br/>"
                + "<br/>Warm Regards,<br/>"
                + "<b>Government Procurement and Property Management Division (GPPMD)<br/>"
                    +"Department of National Properties<br/>" 
                   + "Ministry of Finance .</b><br/><br/>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("tenderCorriPubMsgBox :"+e);
        }
       
        LOGGER.debug("tenderCorriPubMsgBox Ends:");
        return sb.toString();
    }
    /**
     * Publish Tenders
     * Tenderer /Consultant’s business category and Tender’s category matches
     * @param tenderid
     * @param refNo
     * @param peName
     * @param closingDate
     * @param tendBrief
     * @return String
     */
    public String tenderPubMsgBox(String tenderid,String refNo,String peName,String closingDate,String tendBrief,String tenderCap){
        LOGGER.debug("tenderCorriPubMsgBox Starts:");
        StringBuilder sb = new StringBuilder();
        try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that a "+tenderCap+" of your choice is published on e-GP system. Brief detail of the "+tenderCap+" "
                    + "is as mentioned below: <br/><br/><br/>"
                    + "<b>"+tenderCap+" ID:</b> " + tenderid + "<br/>"
                    + "<b>"+tenderCap+" Reference No:</b> " + refNo + "<br/>"
                    + "<b>"+tenderCap+" Closing Date:</b> " + closingDate + "<br/><br/>"
                    + "<b>"+tenderCap+" Brief / Title:</b> <a onclick=\"javascript:window.open('" + XMLReader.getMessage("url") + "/resources/common/ViewTender.jsp?id=" + tenderid + "', '', 'width=1200px,height=600px,scrollbars=1','');\" href=\"javascript:void(0);\">" + tendBrief + "</a><br/>"
                    + "<b>Procuring Entity:</b> " + peName + "<br/><br/>"
                    //+ "Please go to Tender Dashboard, to prepare and Lodge e-Tender.<br/><br/>"
                    + "<br/>e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("tenderCorriPubMsgBox :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }

        LOGGER.debug("tenderCorriPubMsgBox Ends:");
        return sb.toString();
    }
    
    /**
     * Publish Tenders Only for RFQU and RFQL Goods Tenders
     * @param tenderid
     * @param refNo
     * @param peName
     * @param closingDate
     * @param tendBrief
     * @return String
     */
    public String tenderPubMsgBoxRFQUORRFQLGoods(String tenderid,String refNo,String peName,String closingDate,String tendBrief, String tenderValDays,String nameOfBidder,String peAddress,String peOfficeName,String peDesignation){
        LOGGER.debug("tenderPubMsgBoxRFQUORRFQLGoods Starts:");
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
        try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body>");
            sb.append("<table cellspacing='0' cellpadding='0' border='0' width='96%' style='margin-left: 2%; margin-top: 20px;'>");
            sb.append("<tr><td colspan='2' align='center'> "+peName+",<br />"+peAddress+"</td></tr>");
            sb.append("<tr><td colspan='2' align='center'>");
            sb.append("<strong>e-REQUEST FOR QUOTATION <br />for <br /> </strong> "+tendBrief+"</strong>");
            sb.append("</td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr>");
            sb.append("<tr><td align='left'> <strong>e-RFQ No: </strong> "+refNo+"</td>");
            sb.append("<td align='right'> <strong>Date: </strong> "+dateFormat.format(new Date())+"</td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr>");
            sb.append("<tr><td align='left' colspan='2'><strong> To</strong></td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr>");
            sb.append("<tr><td align='left' colspan='2'>"+nameOfBidder+"</td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr>");
            sb.append("<tr><td align='left' colspan='2'><ol style='list-style-type: decimal; list-style-position: outside; float: left; line-height: 16px !important; text-align: justify !important; margin-left: 30px;'>");
            sb.append("<li >The "+peName+" has been allocated public funds and intends to apply a portion of the funds to eligible payments under the Contract for which this e-Quotation Document is issued. </li>");
            sb.append("<li style='margin-top: 5px;'>Detailed Specifications and, Design & Drawings for the intended Goods and related services are available at <a href='"+XMLReader.getMessage("url")+"'>"+XMLReader.getMessage("url")+"</a></li>");
            sb.append("<li style='margin-top: 5px;'>e-Quotation shall be prepared and submitted using the forms provided in e-RFQ.</li>");
            sb.append("<li style='margin-top: 5px;'>e-Quotation shall be completed properly, duly electronically signed and dated each form  by the authorized signatory and submitted on or before "+closingDate+" to Procurement Entity through e-GP System.</li>");
            sb.append("<li style='margin-top: 5px;'>No Securities such as Quotation Security (i.e. the traditionally termed Earnest Money, Tender/Proposal Security) and Performance Security shall be required for submission of the Quotation and delivery of the Goods (if awarded) respectively.</li>");
            sb.append("<li style='margin-top: 5px;'>e-Quotations received through e-GP System shall be sent to the Evaluation Committee for evaluation, after opening, by the same date of closing of the e-Quotation submission.</li>");
            sb.append("<li style='margin-top: 5px;'>The Procuring Entity may extend the deadline for submission of e-Quotations on justifiably acceptable grounds duly recorded subject to threshold of ten (10) days pursuant to Rule 71 (4) of the Public Procurement Rules and Regulations, 2009.</li>");
            sb.append("<li style='margin-top: 5px;'>All e-Quotations must be valid for a period of at least "+tenderValDays+" days from the closing date of the e-Quotation submission.</li>");
            sb.append("<li style='margin-top: 5px;'>Quotationer&#39;s rates or prices shall be inclusive of profit and overhead and, all kinds of taxes, duties, fees, levies, and other charges to be paid under the Applicable Law, if the Contract is awarded.</li>");
            sb.append("<li style='margin-top: 5px;'>Rates shall be quoted and, subsequent payments under this Contract shall be made in Nu. . The price offered by the Quotationer, if accepted shall remain fixed for the duration of the Contract</li>");
            sb.append("<li style='margin-top: 5px;'>Quotationer shall have legal capacity to enter into Contract. Quotationer, should make sure that the documents uploaded at the time of Registration such as Trade License, Tax Payment Number (TIN), VAT Registration Number are valid and up to date, without which the e-Quotation may be considered non-responsive.</li>");
            sb.append("<li style='margin-top: 5px;'>e-Quotations shall be evaluated based on information and documents submitted with the e-Quotations, by the Evaluation Committee and, at least three (3) responsive e-Quotations will be required to determine the lowest evaluated responsive e-Quotations for award of the Contract. </li>");
            sb.append("<li style='margin-top: 5px;'>The supply of Goods and related services shall be completed within the days from the date of issuing the e-Purchase Order and as mentioned in the e-Purchase Order.</li>");
            sb.append("<li style='margin-top: 5px;'>The e-Purchase Order that constitutes the Contract binding upon the Supplier and the Procuring Entity shall be issued within <strong>7 days </strong> of receipt of approval from the Approving Authority.</li>");
            sb.append("<li style='margin-top: 5px;'>Payment for the supply of goods and related services shall be made in 100% within the days after submission and acceptance of the goods and as mentioned the e-RFQ Document.</li>");
            sb.append("<li style='margin-top: 5px;'>The Procuring Entity reserves the right to reject all the e-Quotations or annul the procurement proceedings.</li>");
            sb.append("<li style='margin-top: 5px;'>This letter for e-QUOTATION constitutes as an integral part of <strong>e-REQUEST FOR QUOTATION<strong>.</li></ol></td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr>");
            sb.append("<tr><td align='left' colspan='2' align='left' style='line-height: 16px;'>");
            sb.append("<strong>Electronically signed by</strong> <br />");
            sb.append("<strong>Name: </strong> "+peName+" <br />");
            sb.append("<strong>Designation: </strong>"+peDesignation+" <br />");
            sb.append(" <strong>PE Office Name </strong> "+peOfficeName+" <br />");
            sb.append("<strong>PE Office Address: </strong>"+peAddress+"<br />");
            sb.append(" <strong>Date: </strong> "+dateFormat.format(new Date())+"</td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr></table>"
                + "</body></html>");
        } catch (Exception e) {
            LOGGER.error("tenderPubMsgBoxRFQUORRFQLGoods :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }

        LOGGER.debug("tenderPubMsgBoxRFQUORRFQLGoods Ends:");
        return sb.toString();
    }
    
    /**
     * Publish Tenders Only for RFQU and RFQL Works Tenders
     * @param tenderid
     * @param refNo
     * @param peName
     * @param closingDate
     * @param tendBrief
     * @return String
     */
    public String tenderPubMsgBoxRFQUORRFQLWorks(String tenderid,String refNo,String peName,String closingDate,String tendBrief, String tenderValDays,String nameOfBidder,String peAddress,String peOfficeName,String peDesignation){
        LOGGER.debug("tenderPubMsgBoxRFQUORRFQLWorks Starts:");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");
        StringBuilder sb = new StringBuilder();
        try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body>");
            sb.append("<table cellspacing='0' cellpadding='0' border='0' width='96%' style='margin-left: 2%; margin-top: 20px;'>");
            sb.append("<tr><td colspan='2' align='center'> "+peName+",<br />"+peAddress+"</td></tr>");
            sb.append("<tr><td colspan='2' align='center'>");
            sb.append("<strong>e-REQUEST FOR QUOTATION <br />for <br /> </strong> "+tendBrief+"</strong>");
            sb.append("</td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr>");
            sb.append("<tr><td align='left'> <strong>e-RFQ No: </strong> "+refNo+"</td>");
            sb.append("<td align='right'> <strong>Date: </strong> "+dateFormat.format(new Date())+"</td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr>");
            sb.append("<tr><td align='left' colspan='2'><strong> To</strong></td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr>");
            sb.append("<tr><td align='left' colspan='2'>"+nameOfBidder+"</td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr>");
            sb.append("<tr><td align='left' colspan='2'><ol style='list-style-type: decimal; list-style-position: outside; float: left; line-height: 16px !important; text-align: justify !important; margin-left: 30px;'>");
            sb.append("<li >The "+peOfficeName+" has been allocated public funds and intends to apply a portion of the funds to eligible payments under the Contract for which this e-Quotation Document is issued. </li>");
            sb.append("<li style='margin-top: 5px;'>Detailed Specifications and, Design & Drawings for the intended Goods and related services are available at <a href='"+XMLReader.getMessage("url")+"'>"+XMLReader.getMessage("url")+"</a></li>");
            sb.append("<li style='margin-top: 5px;'>e-Quotation is being requested as per the procurement method mentioned in e-RFQ document.</li>");
            sb.append("<li style='margin-top: 5px;'>e-Quotation shall be prepared and submitted using the forms provided in e-RFQ.</li>");
            sb.append("<li style='margin-top: 5px;'>e-Quotation shall be completed properly, duly electronically signed and dated each form  by the authorized signatory and submitted on or before  "+closingDate+" to Procurement Entity through e-GP System. </li>");
            sb.append("<li style='margin-top: 5px;'>No Securities such as Quotation Security (i.e. the traditionally termed Earnest Money, Tender/Proposal Security) and Performance Security shall be required for submission of the Quotation and execution of works (if awarded) respectively.</li>");
            sb.append("<li style='margin-top: 5px;'>e-Quotations received through e-GP System shall be sent to the Evaluation Committee for evaluation, after opening, by the same date of closing of the e-Quotation submission.</li>");
            sb.append("<li style='margin-top: 5px;'>The Procuring Entity may extend the deadline for submission of e-Quotations on justifiably acceptable grounds duly recorded subject to threshold of ten (10) days pursuant to Rule 71 (4) of the Public Procurement Rules and Regulations, 2009.</li>");
            sb.append("<li style='margin-top: 5px;'>e-Quotation shall be submitted as per Bill of Quantities of Works and physical services.</li>");
            sb.append("<li style='margin-top: 5px;'>All e-Quotations must be valid for a period of at least "+tenderValDays+" days from the closing date of the e-Quotation submission.</li>");
            sb.append("<li style='margin-top: 5px;'>Quotationer&#39;s rates or prices shall be inclusive of profit and overhead and, all kinds of taxes, duties, fees, levies, and other charges to be paid under the Applicable Law, if the Contract is awarded. </li>");
            sb.append("<li style='margin-top: 5px;'>Rates shall be quoted and, subsequent payments under this Contract shall be made in Nu. . The price offered by the Quotationer, if accepted shall remain fixed for the duration of the Contract.</li>");
            sb.append("<li style='margin-top: 5px;'>Quotationer shall have legal capacity to enter into Contract. Quotationer, should make sure that the documents uploaded at the time of Registration such as Trade License, Tax Payment Number (TIN), VAT Registration Number are valid and up to date, without which the e-Quotation may be considered non-responsive. </li>");
            sb.append("<li style='margin-top: 5px;'>e-Quotations shall be evaluated based on information and documents submitted with the e-Quotations, by the Evaluation Committee and, at least three (3) responsive e-Quotations will be required to determine the lowest evaluated responsive e-Quotations for award of the Contract. </li>");
            sb.append("<li style='margin-top: 5px;'>The execution of Works and physical services shall be completed within the days from the date of commencement and as mentioned in the letter of Invitation.</li>");
            sb.append("<li style='margin-top: 5px;'>The e-Purchase Order that constitutes the Contract binding upon the Supplier and the Procuring Entity shall be issued within <strong>7 days</strong> of receipt of approval from the Approving Authority. </li>");
            sb.append("<li style='margin-top: 5px;'>Payment for the supply of goods and related services shall be made in 100% within the days after submission and acceptance of the goods and as mentioned the e-RFQ Document.</li>");
            sb.append("<li style='margin-top: 5px;'>The Procuring Entity reserves the right to reject all the e-Quotations or annul the procurement proceedings.</li>");
            sb.append("<li style='margin-top: 5px;'>This letter for e-QUOTATION constitutes as an integral part of <strong>e-REQUEST FOR QUOTATION<strong>.</li></ol></td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr>");
            sb.append("<tr><td align='left' colspan='2' align='left' style='line-height: 16px;'>");
            sb.append("<strong>Electronically signed by</strong> <br />");
            sb.append("<strong>Name: </strong> "+peName+" <br />");
            sb.append("<strong>Designation: </strong>"+peDesignation+" <br />");
            sb.append(" <strong>PE Office Name </strong> "+peOfficeName+" <br />");
            sb.append("<strong>PE Office Address: </strong>"+peAddress+"<br />");
            sb.append(" <strong>Date: </strong> "+dateFormat.format(new Date())+"</td></tr>");
            sb.append("<tr><td align='left' colspan='2' height='10'></td></tr></table>"
                + "</body></html>");
        } catch (Exception e) {
            LOGGER.error("tenderPubMsgBoxRFQUORRFQLWorks :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }

        LOGGER.debug("tenderPubMsgBoxRFQUORRFQLWorks Ends:");
        return sb.toString();
    }
    /**
     * Publish Tenders Only for RFQ Tenders
     * @param tenderid
     * @param refNo
     * @param closingDate
     * @param tendBrief
     * @param peofficeName
     * @return String
     */
    public String tenderPubMsgBoxRFQTender(String tenderid,String refNo,String closingDate,String tendBrief,String peOfficeName){
        LOGGER.debug("tenderPubMsgBoxRFQTender Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that a e-Limited Enquiry Method (e-LEM) of your interest is published on e-GP system. Detail notice with terms and conditions is available with the e-LEM document. Brief detail of the e-LEM is as mentioned below: <br /><br />"
                    + "<b>e-LEM ID:</b> " + tenderid + "<br/>"
                    + "<b>Reference No:</b> " + refNo + "<br/>"
                    + "<b>e-LEM Closing Date:</b> " + closingDate + "<br/><br/>"
                    + "<b>e-LEM Brief / Title:</b> <a href=\"" + XMLReader.getMessage("url") + "/resources/common/ViewTender.jsp?id=" + tenderid + "\" target=\"_blank\">" + tendBrief + "</a><br/>"
                    + "<b>Procuring Entity:</b> " + peOfficeName + "<br/><br/>"
                    + "<br/>Thanks,<br/>"
                    //+ "Please go to Tender Dashboard, to prepare and Lodge e-Tender.<br/><br/>"
                    + "<br/>e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
        }catch(Exception e){
            LOGGER.error("tenderPubMsgBoxRFQTender :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        LOGGER.debug("tenderPubMsgBoxRFQTender Ends:");
        return sb.toString();
    }
    /**
     * Publish Tenders Only for RFP Tenders
     * @param tenderid
     * @param refNo
     * @param closingDate
     * @param tendBrief
     * @param peofficeName
     * @return String
     */
    public String tenderPubMsgBoxRFPTender(String tenderid,String refNo,String closingDate,String tendBrief,String peOfficeName,String[] name){
        LOGGER.debug("tenderPubMsgBoxRFQTender Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                    + "The Royal Government of &#39;s Bhutan has allocated public funds and intends to apply a portion of the funds to eligible payments under the Contract for which this e-RFP Document is issued through e-GP system. <br /><br />"
                    + "The " + peOfficeName + " now invites proposals to provide the following consulting services: <a href=\"" + XMLReader.getMessage("url") + "/resources/common/ViewTender.jsp?id=" + tenderid + "\" target=\"_blank\">" + tendBrief + "</a><br/>"
                    + "<br /><br />More details on the services are provided in the Terms of Reference (TOR).<br /><br />"
                    + "This is to inform you that an e-Request For Proposal (e-RFP) of your interest is published on e-GP system. Detailed notice with terms and conditions is available with the e-RFP document.<br /><br />"
                    + "<b>e-RFP ID:</b> " + tenderid + "<br/>"
                    + "<b>Reference No:</b> " + refNo + "<br/>"
                    + "<b>e-RFP Closing Date:</b> " + closingDate + "<br/><br/>"
                    + "<b>Name of Consultants :</b>");
                    if(name!=null && name.length>0){
                        sb.append("<br /><br />");
                        sb.append("<table width='100%' class='tableList_1' align='center'>");
                        int i = 0;
                        for(String bidderName : name){
                            i++;
                            sb.append("<tr><td width='10%'>"+i+"</td><td>"+bidderName+"</td></tr>");
                        }
                        sb.append("</table><br />");
                    }
                    sb.append("<br/>Thanks,<br/>"
                    +"<br/>e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
        }catch(Exception e){
            LOGGER.error("tenderPubMsgBoxRFQTender :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        LOGGER.debug("tenderPubMsgBoxRFQTender Ends:");
        return sb.toString();
    }
    /**
     * A day prior to Pre-tender meeting start date
     * PE of the tender
     * @param tenderid
     * @param refNo
     * @param peName
     * @param closingDate
     * @param tendBrief
     * @return
     */
    public String getPreTenMeetingPubMsgBox(String tenderid,String refNo,String peName,String closingDate,String tendBrief){
        LOGGER.debug("getPreTenMeetingPubMsgBox Starts:");
        StringBuilder sb = new StringBuilder();
        try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that MOM of Pre-Tender/Proposal Meeting is published for the below mentioned tender/proposal:<br/><br/>"
                + "<b>Tender/Proposal ID:</b> "+tenderid+"<br/>"
                + "<b>Reference No:</b> "+refNo+"<br/>"
                + "<b>Tender/Proposal Closing Date:</b> "+closingDate+"<br/>"
                + "<b>Tender/Proposal Description:</b> <a onclick=\"javascript:window.open('"+XMLReader.getMessage("url")+"/resources/common/ViewTender.jsp?id="+tenderid+"', '', 'width=1200px,height=600px,scrollbars=1','');\" href=\"javascript:void(0);\">"+tendBrief+"</u></em></strong></p></a><br/>"
                + "<b>Procuring Entity:</b> "+peName+"<br/>"
                + "<br/>Thanks,<br/>"
                + "<b>e-GP System.</b><br/><br/>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getPreTenMeetingPubMsgBox :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }

        LOGGER.debug("getPreTenMeetingPubMsgBox Ends:");
        return sb.toString();
    }
    /**
     * Final submission of a tender/proposal
     * Tender /Bid/Proposal withdrawal
     * Tender/Bid//Proposal modification/substitution
     * Tenderer / Consultant 
     * @param tenderid
     * @param userId
     * @param flag
     * @return
     */
    public String bidTenderSubMsg(String tenderid,String userId,String flag) {
        LOGGER.debug("bidTenderSubMsg Starts:");
        String refNo = null;
        StringBuffer sb = new StringBuffer();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "Dear User," + "<br/><br/><span>"
                + "This is to inform you that you have ");
                if("final".equals(flag)){
                     sb.append("successfully completed final submission "
                        +"(Dropping of e-Tender/Proposal into Time Stamped Secured Electronic Box) "
                        +"in a below mentioned tender/proposal:<br/><br/>");
                }else if("withdraw".equals(flag)){
            sb.append("successfully withdrawn below mentioned tender/proposal:<br/><br/>");
                }else if("modify".equals(flag)){
                    sb.append("successfully modified below<br/><br/>"
                        +"mentioned Tender/Proposal.<br/><br/>");
        }
        sb.append("<br/>");
            TenderBidService service = (TenderBidService)AppContext.getSpringBean("TenderBidService");
            //0. tom.officeName,1. ttd.packageNo,2. ttd.packageDescription,3. tfs.ipAddress,
            //4. tfs.tenderHash,5. tfs.finalSubmissionDt
            Object data1[] = service.finalSubTenderDetails(tenderid, userId);
            if(data1!=null){
                sb.append("<table border='1'>");
                    sb.append("<tr>");
                        sb.append("<td width='40%'><b>PE Name :</b></td>");
                        sb.append("<td width='60%'>"+data1[0]+"</td>");
                    sb.append("</tr>");
                    sb.append("<tr>");
                        sb.append("<td><b>Tender/Proposal Notice ID :</b></td>");
                        sb.append("<td>"+tenderid+"</td>");
                    sb.append("</tr>");
                    sb.append("<tr>");
                        sb.append("<td><b>Package No.<br/>and Description :</b></td>");
                        sb.append("<td>"+data1[1]+" & "+data1[2]+"</td>");
                    sb.append("</tr>");
                    sb.append("<tr>");
                        sb.append("<td><b>Ref. No.</b></td>");
                        sb.append("<td>"+data1[6]+"</td>");
                    sb.append("</tr>");
                    List<Object[]> data2 = service.finalSubLotDetails(tenderid, userId);
                    for (Object[] objects : data2) {
                        sb.append("<tr>");
                            sb.append("<td><b>Lot No. and<br/>Description :</b></td>");
                            sb.append("<td>"+objects[0]+" &<br/>"+objects[1]+"</td>");
                        sb.append("</tr>");
                    }
                    sb.append("<tr>");
                        sb.append("<td><b>Submitted from which IP :</b></td>");
                        sb.append("<td>"+data1[3]+"</td>");
                    sb.append("</tr>");
                    sb.append("<tr>");
                        sb.append("<td><b>Date & Time of Submission :</b></td>");
                        sb.append("<td>"+DateUtils.gridDateToStr((Date)data1[5])+"</td>");
                    sb.append("</tr>");
                    if(("final".equals(flag)) && data1[4]!=null && (!data1[4].toString().equals("-1"))){
                        sb.append("<tr>");
                            sb.append("<td><b>Bidder's/Consultant's Mega Hash :</b></td>");
                            sb.append("<td>"+data1[4]+"</td>");
                        sb.append("</tr>");
                    }
                sb.append("</table>");
            }
            sb.append("<br/>");
                if("modify".equals(flag)){
                    sb.append("Please note that you need to do final submission (Dropping of an e-Tender/Proposal<br/><br/>"
                        +"into Secured Time stamped Electronic Tender/Proposal box) once you modify your tender/proposal.<br/><br/>");
        }
        sb.append("<br/>Thanking you,<br/>"
                + "<b>e-GP System</b><br/><br/>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.debug("bidTenderSubMsg :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        LOGGER.debug("bidTenderSubMsg Ends:");
        return sb.toString();
    }

    /**
     * PE requests the tender/proposal validity and security validity extension to the HOPE/Secretary
     * HOPE or Secretary as the case may be
     * @param tenderid
     * @param refNo
     * @param peOffice
     * @return String
     */
    public String contTOExtReq(String tenderid,String refNo, String peOffice){
        LOGGER.debug("contTOExtReq Starts:");
        StringBuilder sb = new StringBuilder();
         try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that you have received an opening date and time extension request for the below mentioned Tender/Proposal:<br/><br/><br/>"
                + "Tender/Proposal Id: "+tenderid+"<br/>"
                + "Ref No: "+refNo+"<br/>"
                + "Procuring Entity: "+peOffice+"<br/><br/>"
                + "<br/>Click on Evaluation Tab -> Opening Date Ext. Req. to process the request.<br/><br/>"
                + "<br/>Thanks.<br/>"
                + "<b>e-GP System.</b><br/><br/>"
                + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("contTOExtReq :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
         }
        LOGGER.debug("contTOExtReq Ends:");
        return sb.toString();
    }
    /**
     * When HOPE accepts the extension of opening date and time
     * @param tenderid
     * @param refNo
     * @param PEName
     * @param newDate
     * @return String
     */ 
     public String contOEReqApproval(String tenderid,String refNo,String PEName,String newDate){
         LOGGER.debug("contOEReqApproval Starts:");
        StringBuilder sb = new StringBuilder();
         try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you the new Opening Date and Time is as mentioned below:<br/><br/>"
                + "<b>Tender/Proposal Id:</b> "+tenderid+"<br/>"
                + "<b>Ref No:</b> "+refNo+"<br/>"
                + "<b>PE:</b> "+PEName+"<br/>"
                + "<b>New Date and Time:</b> "+newDate+"<br/><br/>"
                + "<br/>Thanks.<br/>"
                + "e-GP System.<br/><br/>"
                + "</td></tr></table></body></html>");
             
         } catch (Exception e) {
             LOGGER.error("contOEReqApproval :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
         }
            LOGGER.debug("contOEReqApproval Ends:");
        return sb.toString();
    }

     /**
     * When PE issues NOA to the concern Tenderer
     * Tenderer to whom the NOA is issued
     * @param tenderId
     * @param refNo
     * @param contno
     * @param contid
     * @param status
     * @param name
     * @param lotNo
     * @param lotDesc
     * @return String
     */
     public String contApproval(String tenderid,String refNo,String contno,String contid,String status,String name,String lotNo,String lotDesc){
         LOGGER.debug("contApproval Starts:");
        StringBuilder sb = new StringBuilder();
         try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td><br/>"
                + "Dear User,<br/><br/><span>"
                + name +" has " + status + " Letter of Acceptance (LOA) for a below mentioned Tender/Proposal:"
                + "<br/><br/>Tender/Proposal ID: " + tenderid + ""
                + "<br/>Reference No.: " + refNo + ""
                + "<br/>Lot No.: " + lotNo + ""
                + "<br/>Lot Description: " + lotDesc + ""
                + "<br/><br/>Thanks<br/>"
                + "<br/>e-GP System<br/>"
                + "</b><br/><br/></td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("contApproval :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
         }
         LOGGER.debug("contApproval Ends:");
        return sb.toString();
    }

      public String contTenderPayment(String tenderid,String refNo, String paymentFor, String action){
          LOGGER.debug("contApproval Starts:");
        StringBuilder sb = new StringBuilder();
          try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that your "+paymentFor+" in a below <br/>"
                + "mentioned tender/proposal has been "+action+":<br/><br/><br/>"
                + "Id: "+tenderid+"<br/>"
                + "Ref No: "+refNo+"<br/><br/>"
                + "<br/>Warm Regards,<br/>"
                + "<b>Government Procurement and Property Management Division (GPPMD)<br/>"
                    +"Department of National Properties<br/>" 
                   + "Ministry of Finance.</b><br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("contApproval :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
          }
        LOGGER.debug("contApproval Ends:");
        return sb.toString();
    }

     /**
     * When a branch checker releases or forfeits the tender security payment detail OR
     * When a branch checker releases or forfeits the tender security or performance security
     * @param tenderid
     * @param refNo
     * @param lotNo
     * @param lotDescription
     * @param paymentFor
     * @param action
     * @param companyName
     * @param peOffice
     * @return
     */
      public String contBankUser_ReleaseForfeit(String tenderid, String refNo, String lotNo, String lotDescription, String paymentFor, String action, String companyName, String peOffice) {
          LOGGER.debug("contBankUser_ReleaseForfeit Starts:");
        StringBuilder sb = new StringBuilder();
          try {
              if(!"".equalsIgnoreCase(lotNo)){
                   sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that " + paymentFor + " of " + companyName + " <br/>"
                    + "has been " + action + " for below mentioned Tender/Proposal:<br/><br/><br/>"
                    + "Tender/Proposal ID: " + tenderid + "<br/>"
                    + "Reference No.: " + refNo + "<br/>"
                    + "Lot No.: " + lotNo + "<br/>"
                    + "PE: " + peOffice + "<br/><br/>"
                    + "<br/>Thanks.<br/>"
                    + "<b>e-GP System</b><br/><br/>"
                + "</td></tr></table></body></html>");
              } else {
                     sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that " + paymentFor + " of " + companyName + " <br/>"
                    + "has been " + action + " for below mentioned Tender/Proposal:<br/><br/><br/>"
                    + "Tender/Proposal ID: " + tenderid + "<br/>"
                    + "Reference No.: " + refNo + "<br/>"
                    + "PE: " + peOffice + "<br/><br/>"
                    + "<br/>Thanks.<br/>"
                    + "<b>e-GP System</b><br/><br/>"
                + "</td></tr></table></body></html>");

              }
          } catch (Exception e) {
              LOGGER.error("contBankUser_ReleaseForfeit :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
          }
        LOGGER.debug("contBankUser_ReleaseForfeit Ends:");
        return sb.toString();
    }

   /**
     * When PE request for relese or forfit TenderSecurity or Perfomance Sercurity
     * Branch Checker who has verified the payment detail earlier
     * @param tenderid
     * @param refNo
     * @param lotNo
     * @param lotDescription
     * @param paymentFor
     * @param action
     * @param companyName
     * @param remarks
     * @param peOffice
     * @return mail content
     */
        public String contRequest_ReleaseForfeit(String tenderid,String refNo, String lotNo, String lotDescription, String paymentFor, String action, String companyName, String remarks, String peOffice,String userTypeId,String peName,String BankName,String BranchName,String Amount){
           LOGGER.debug("contRequest_ReleaseForfeit Starts:");
        StringBuilder sb = new StringBuilder();
            try {

                if("forfeited".equalsIgnoreCase(action)){
                      sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that PE has requested to forfeit "+paymentFor+" for below mentioned Tender/Proposal:<br/><br/><br/>"
                + "Tender/Proposal Id: "+tenderid+"<br/>"
                + "Reference No: "+refNo+"<br/>"
                + "Lot No: "+lotNo+"<br/>"
                + "Lot Description: "+lotDescription+"<br/>"
                + "Bidder / Consultant Name: "+companyName+"<br/>"
                + "PE Office Name: "+peOffice+"<br/><br/>"                
                + "<br/>Thanks<br/>"
                + "<b>e-GP System</b><br/><br/>"
                + "</td></tr></table></body></html>");
                } else {
                    if("7".equalsIgnoreCase(userTypeId))
                    {    
                              //if("Tender Security".equalsIgnoreCase(paymentFor)){
                            if("released".equalsIgnoreCase(paymentFor)){
                            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                        + "<br/>Dear User," + "<br/><br/><span>"
                        + "This is to inform you that PE has requested to Release the "+paymentFor+" for the below mentioned Tender/Proposal:<br/><br/><br/>"
                        + "Tender/Proposal Id: "+tenderid+"<br/>"
                        + "Reference No: "+refNo+"<br/>"
                        + "Lot No: "+lotNo+"<br/>"
                        + "Lot Description: "+lotDescription+"<br/>"
                        + "Bidder / Consultant Name: "+companyName+"<br/>"
                        + "PE Office Name: "+peOffice+"<br/><br/>"
                        + "Reason for Return :  "+remarks+"<br/><br/>"
                        + "<br/>Thanks<br/>"
                        + "<b>e-GP System</b><br/><br/>"
                        + "</td></tr></table></body></html>");
                        } else {
                            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                        + "<br/>Dear User," + "<br/><br/><span>"
                        + "This is to inform you that "+paymentFor+" of "+companyName+" <br/>"
                        + "for below mentioned tender/proposal has to be "+action+":<br/><br/><br/>"
                        + "Tender/Proposal Id: "+tenderid+"<br/>"
                        + "Reference No: "+refNo+"<br/><br/>"
                        + "<br/>Thanks<br/>"
                        + "<b>e-GP System</b><br/><br/>"
                        + "</td></tr></table></body></html>");

                        }
                    }else{
                        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                        + "<br/>Dear User," + "<br/><br/><span>"
                        + "Be informed that PE has requested Financial Institute to release your "+paymentFor+" for below mentioned Tender/Proposal:<br/><br/><br/>"
                        + "Tender/Proposal Id: "+tenderid+"<br/>"
                        + "Reference No: "+refNo+"<br/>"
                        + "PE Name: "+peName+"<br/>"
                        + "Financial Institute and Branch Detail: "+BranchName+"<br/>"
                        + "Tender/Proposal Security Amount: "+Amount+"<br/>"
                        + "<br/><br/>"
                        + "You are requested to visit the Financial Institute for collection of "+paymentFor+".<br/><br/>"
                        + "<br/>Thanks<br/>"
                        + "<b>e-GP System</b><br/><br/>"
                        + "</td></tr></table></body></html>");
                    }         
                }
           } catch (Exception e) {
               LOGGER.error("contRequest_ReleaseForfeit :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
           }
        LOGGER.debug("contRequest_ReleaseForfeit Ends:");
        return sb.toString();
    }

       public String ValidationRequestToHope(){
        LOGGER.debug("contRequest_ReleaseForfeit Starts:");
        StringBuilder sb = new StringBuilder();
           try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User,<br/><br/><span>"
                + "This is to inform you that  of  <br/>"
                + "for below mentioned tender/proposal has to be :<br/><br/><br/>"
                + "Id: <br/>"
                + "Ref No: <br/><br/>"
                + "<br/>Warm Regards,<br/>"
                + "<b>eGP Support Team.</b><br/><br/>"
                + "<span style=\"color : red\"><b>To ensure you receive future emails, please add <a href=\"mailto:auto-email@cptu.gov.bd\">auto-email@cptu.gov.bd</a> email "
                + "address of procuring entities to your Safe Sender list.\"</b></span>"
                + "</td></tr></table></body></html>");
           } catch (Exception e) {
               LOGGER.error("contRequest_ReleaseForfeit :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
           }
        LOGGER.debug("contRequest_ReleaseForfeit Ends:");
        return sb.toString();
    }

       public String ValidationAppRequestToBidder(){
        LOGGER.debug("ValidationAppRequestToBidder Starts:");
        StringBuilder sb = new StringBuilder();
           try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User,<br/><br/><span>"
                + "This is to inform you that  of  <br/>"
                + "for below mentioned tender/proposal has to be :<br/><br/><br/>"
                + "Id: <br/>"
                + "Ref No: <br/><br/>"
                + "<br/>Warm Regards,<br/>"
                + "<b>eGP Support Team.</b><br/><br/>"
                + "<span style=\"color : red\"><b>To ensure you receive future emails, please add <a href=\"mailto:auto-email@cptu.gov.bd\">auto-email@cptu.gov.bd</a> email "
                + "address of procuring entities to your Safe Sender list.\"</b></span>"
                + "</td></tr></table></body></html>");
           } catch (Exception e) {
                       LOGGER.error("ValidationAppRequestToBidder :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
           }
        LOGGER.debug("ValidationAppRequestToBidder Ends:");
        return sb.toString();
    }

       public String ValidationRejRequestToBidder(){
           LOGGER.debug("ValidationRejRequestToBidder Starts:");
        StringBuilder sb = new StringBuilder();
           try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User,<br/><br/><span>"
                + "This is to inform you that  of  <br/>"
                + "for below mentioned tender/proposal has to be :<br/><br/><br/>"
                + "Id: <br/>"
                + "Ref No: <br/><br/>"
                + "<br/>Warm Regards,<br/>"
                + "<b>eGP Support Team.</b><br/><br/>"
                + "<span style=\"color : red\"><b>To ensure you receive future emails, please add <a href=\"mailto:auto-email@cptu.gov.bd\">auto-email@cptu.gov.bd</a> email "
                + "address of procuring entities to your Safe Sender list.\"</b></span>"
                + "</td></tr></table></body></html>");
           } catch (Exception e) {
               LOGGER.error("ValidationRejRequestToBidder :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
           }
        
        LOGGER.debug("ValidationRejRequestToBidder Ends:");
        return sb.toString();
    }

       /**
        * 
        * @param amount
        * @param currencySymbol
        * @param ValidityYrPeriod
        * @param ValidityYrPeriodInWords
        * @return
        */
       public String contRegistrationPayment(String amount, String currencySymbol, String ValidityYrPeriod, String ValidityYrPeriodInWords){
           LOGGER.debug("contRegistrationPayment Starts:");
        StringBuilder sb = new StringBuilder();
        String URL = "http://122.170.96.192:8080/eGPWeb/";
                    try {
         sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "As a part of the registration process, your Registration Fee of " +currencySymbol + amount+" has been received.<br/><br/>"
                + "However, ONLY after satisfactory post verification (Please read <a href=\""+URL+"FAQ.jsp\">FAQ</a> to know about post verification) of "
                + "your uploaded mandatory credential documents by the \"e-GP User Registration Desk\", your "
                + "\"User Registration\" will be approved. Validity of the registration is for " + ValidityYrPeriodInWords + " (" + ValidityYrPeriod + ") year from the date of approval of user registration. You must renew your registration annually, for uninterrupted service and getting access to e-GP System.<br/><br/>"
                + "<b>What next?</b><br/><br/>"
                + "Financial Institute Payment/Slip is one of the mandatory documents for completing your Profile details for the Registration.<br/><br/>"
                + "To complete your Registration, you need to scan the Financial Institute Payment/Slip, and upload the scanned file along with other <a href=\""+URL+"MandRegDoc.jsp\"> mandatory  documents</a> through Profile details page.<br/><br/>"
               +"<strong>Important links:</strong><br/>"
                +"<a href=\""+URL+"help/manuals/eGP_NewUserRegManual_English.pdf\" target=\"_blank\">User Manual for Tenderers and Consultants</a><br/>"
                +"<a href=\""+URL+"GuideLines.jsp\" target=\"_blank\">e-GP Guidelines</a><br/>"
                +"<a href=\""+URL+"MemScheduleBank.jsp\" target=\"_blank\">List of Member Scheduled Banks</a><br/>"
                +"<a href=\""+URL+"MandRegDoc.jsp\" target=\"_blank\">List of Mandatory Documents for Registration</a><br/>"
                +"<a href=\""+URL+"TermsNConditions.jsp\" target=\"_blank\">Terms and Conditions</a><br/>"
                +"<a href=\""+URL+"PrivacyPolicy.jsp\" target=\"_blank\">Disclaimer and Privacy Policy</a><br/><br/>"
                +"<strong><b>Public Procureemnt Policy Division (GPPMD)</strong></b><br/>"
                +"Department of National Properties, Ministry of Finanace, Royal Government of Bhutan<br />"
                +"Address: Box 116, Thimphu, Bhutan, Bhutan. <br />"
                +"GPPMD Office Contact Details<br />"
                +"<strong>Phone:</strong> +975 02 - 336962 | <strong>Email:</strong> <a href>info.bhutan@dohatec.com.bd</a><br />"
                +"<a href>http://www.egp.gov.bt/ </a>"
               // +"<strong>Help Desk Tel: </strong>88-02-9144252-53, Help Desk e-mail: <a href=\"mailto:helpdesk@eprocure.gov.bd\">helpdesk@eprocure.gov.bd</a>"
                + "</td></tr></table></body></html>");
           } catch (Exception e) {
               LOGGER.error("contRegistrationPayment :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
           }
            LOGGER.debug("contRegistrationPayment Ends:");
        return sb.toString();
    }

    public String contentAdminApprove(){
        LOGGER.debug("contentAdminApprove Starts:");
        StringBuilder sb = new StringBuilder();
        /*sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User,<br/><br/><span>"
                + "Welcome to e-GP Portal.<br/><br/>"
                + "Your profile details and mandatory credential documents have been primarily verified, and your Registration has been APPROVED.<br/>"
                + "Procuring Entities’ will require and verify the same documents during the post qualification process of a particular e-Tender you may be participating.<br/><br/>"
                + "<br/>Warm Regards,<br/>"
                + "<b>e-GP user Registration Desk.</b><br/><br/>"
                + "</td></tr></table></body></html>");*/
                try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                + "Dear User," + "<br/><br/><span>"
                +"Congratulations!<br /><br />"
                +"Your profile details and mandatory credential documents have been verified, and your registration has been APPROVED.<br /><br />"
                +"Procuring Entities will require and verify the same documents during the post qualification process of a particular e-Tender/Proposal you may be participating.<br/><br/>"
                //+"Now, you can log-on to e-GP System using your e-mail address(ID) and Password.<br/>"
                +"<strong>Important links:</strong><br/>"
                +"<a href=\""+XMLReader.getMessage("url")+"/help/manuals/eGP_NewUserRegManual_English.pdf\" target=\"_blank\">User Manual for Tenderers and consultants</a><br/>"
                +"<a href=\""+XMLReader.getMessage("url")+"/MemScheduleBank.jsp\" target=\"_blank\">List of Scheduled Banks</a><br/>"
                +"<a href=\""+XMLReader.getMessage("url")+"/GuideLines.jsp\" target=\"_blank\">e-GP Guidelines</a><br/>"
                +"<a href=\""+XMLReader.getMessage("url")+"/MandRegDoc.jsp\" target=\"_blank\">List of Mandatory Documents for Registration</a><br/>"
                +"<a href=\""+XMLReader.getMessage("url")+"/TermsNConditions.jsp\" target=\"_blank\">Terms and Conditions</a><br/>"
                +"<a href=\""+XMLReader.getMessage("url")+"/PrivacyPolicy.jsp\" target=\"_blank\">Disclaimer and Privacy Policy</a><br/><br/>"
                +"<strong>Thank you for using e-GP System.</strong><br/><br/>"
                +"<strong>Government Procurement and Property Management Division (GPPMD)</strong><br/>"
                +"Department of National Properties, Ministry of Finance, Royal Government of Bhutan <br />"
                +"Ministry of Finance Box 116, Thimphu, Bhutan <br /><br />"
                +"<div align=\"center\">e-GP Help Desk Contact Details</div><br />"
                +"<div align=\"center\"><strong>Phone:</strong> +975 02 - 336962 | <strong>Email:</strong> <a href>info.bhutan@dohatec.com.bd</a></div><br />"
                +"<div align=\"center\"><a href='"+XMLReader.getMessage("url")+"'>"+XMLReader.getMessage("url")+"</a></div>"
                + "</td></tr></table></body></html>");
                } catch (Exception e) {
                    LOGGER.error("contentAdminApprove :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
                }
        LOGGER.debug("contentAdminApprove Ends:");
        return sb.toString();
    }
    public String contentRegExt(){
        LOGGER.debug("contentRegExt Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                + "Dear User," + "<br/><br/><span>"
                +"Welcome to e-GP Portal. You may visit the following links for carrying out procurement related tasks in<br/>e-GP system.<br /><br />"
                +"<strong>Important links:</strong><br/>"
                //+"<a href=\""+XMLReader.getMessage("url")+"/eGP_NewUserRegManual_English.pdf\">User Manual for Tenderers and consultants</a><br/>"
                +"<a href=\""+XMLReader.getMessage("url")+"/MemScheduleBank.jsp\" target=\"_blank\">List of Scheduled Banks</a><br/>"
                +"<a href=\""+XMLReader.getMessage("url")+"/GuideLines.jsp\" target=\"_blank\">e-GP Guidelines</a><br/>"
                +"<a href=\""+XMLReader.getMessage("url")+"/delegationOfFinancial.jsp\" target=\"_blank\">Delegation of Financial Power</a><br/>"
                +"<a href=\""+XMLReader.getMessage("url")+"/TermsNConditions.jsp\" target=\"_blank\">Terms and Conditions</a><br/>"
                +"<a href=\""+XMLReader.getMessage("url")+"/PrivacyPolicy.jsp\" target=\"_blank\">Disclaimer and Privacy Policy</a><br/><br/>"
                +"<strong>Important links:</strong><br/>"
                +"e-GP system will keep a record of your login details and all the activities that you carry out in the e-GP system and hence restrain from sharing your Login ID and Password with anyone else as per e-GP security policy."
                +"<div align=\"center\">GPPMD Office Contact Details</div>"
                +"<div align=\"center\"><strong>Phone:</strong> +975-002-336962 | <strong>Email:</strong> <a href>info.bhutan@dohatec.com.bd</a></div>"
                +"<div align=\"center\"><a href='"+XMLReader.getMessage("url")+"'>"+XMLReader.getMessage("url")+"</a></div>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contentRegExt :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        LOGGER.debug("contentRegExt Ends:");
        return sb.toString();
    }

    public String regRenewalContent(String expirydate){
        LOGGER.debug("regRenewalContent Starts:");
        StringBuilder sb = new StringBuilder();
        /*sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User,<br/><br/><span>"
                + "Welcome to e-GP Portal.<br/><br/>"
                + "Your profile details and mandatory credential documents have been primarily verified, and your Registration has been APPROVED.<br/>"
                + "Procuring Entities’ will require and verify the same documents during the post qualification process of a particular e-Tender you may be participating.<br/><br/>"
                + "<br/>Warm Regards,<br/>"
                + "<b>e-GP user Registration Desk.</b><br/><br/>"
                + "</td></tr></table></body></html>");*/
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                + "Dear User," + "<br/><br/>"
                + "This is to inform you that your e-GP account will expire on "+expirydate+". Kindly renew "
                + "your registration before the expiry date mentioned above for uninterupted e-GP System access.<br /><br />"
                + "To renew your registration, you can pay renewal fee through any of the <b><i><a target='_blank' href='"+URL+"MemScheduleBank.jsp'>member scheduled banks.</a></i></b><br /><br />"
                +"<div align=\"center\"><strong>GPPMD Office Contact Details</strong></div>"
                +"<div align=\"center\"><strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href>info.bhutan@dohatec.com.bd</a></div>"
                +"<div align=\"center\"><a href='"+XMLReader.getMessage("url")+"' target=\"_blank\">"+XMLReader.getMessage("url")+"</a></div>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("regRenewalContent :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        LOGGER.debug("regRenewalContent Ends:");
        return sb.toString();
    }
    
    public String firstLogin(){
        LOGGER.debug("firstLogin Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User,<br/><br/><span>"
                + "Welcome to e-GP Portal.<br/><br/>"
                //+ "Please refer help section for using this portal.<br/><br/>"
                + "<br/>Warm Regards,<br/>"
                + "<b>eGP Support Team.</b><br/><br/>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("firstLogin :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        LOGGER.debug("firstLogin Ends:");
        return sb.toString();
    }
    /**
     * Contract Signing
     * Contract is signed successfully
     * @param tenderId
     * @param refNo
     * @param PEName
     * @param contractSignDate
     * @return String
     */
    public String contentContractSign(String tenderId, String refNo, String PEName, String contractSignDate) {
        LOGGER.debug("firstLogin Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User,<br/><br/><span>"
                + "This is to inform you that Contract has been signed successfully for below mentioned tender/proposal:<br/><br/>"
                + "Tender/Proposal ID : " + tenderId + "<br/>"
                + "Ref No : " + refNo + "<br/>"
                + "Procuring Entity : " + PEName + "<br/>"
                + "Date of Contract Signing : " + contractSignDate + "<br/>"
                + "<br/>Thanks.<br/>"
                + "e-GP System<br/>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("firstLogin :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        LOGGER.debug("firstLogin Ends:");
        return sb.toString();
    }

    public String firstAdminLogin(int userTypeId){
        LOGGER.debug("firstAdminLogin Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User,<br/><br/><span>"
                + "Welcome to e-GP Portal. You may visit the following links for carrying out procurement "
                + "related tasks in e-GP system.<br/><br/>"
                + "<b>Important links:</b><br/><br/>");

            if(!(userTypeId==6 || userTypeId==7)){
                sb.append("<a href='#'>User Manual for Procuring Entities</a><br/>");
            }

       sb.append("<a href='"+URL+"MemScheduleBank.jsp' target=\"_blank\">List of Scheduled Banks</a><br/>"
                + "<a href='"+URL+"help/guidelines/eGP_Guidelines.pdf' target=\"_blank\">e-GP Guidelines</a><br/>");

            if(!(userTypeId==6 || userTypeId==7)){
                sb.append("<a href='#'>Delegation of Financial Power</a><br/>");
            }

       sb.append("<a href='"+URL+"TermsNConditions.jsp' target=\"_blank\">Terms and Conditions</a><br/>"
                + "<a href='"+URL+"PrivacyPolicy.jsp' target=\"_blank\">Disclaimer and Privacy Policy</a><br/><br />"
                + "<b>Important Note:</b><br/><br/>"
                + "e-GP system will keep a record of your login details and all the activities that "
                + "you carry out in the e-GP system and hence restrain from sharing your Login ID and "
                + "Password with anyone else as per e-GP security policy.<br /><br />"
                + "<div align=\"center\"><strong>GPPMD Office Contact Details</strong></div>"
                + "<div align=\"center\"><strong>Phone:</strong> +975-02-336962  | <strong>Email:</strong> <a href>info.bhutan@dohatec.com.bd</a></div>"
                + "<div align=\"center\"><a href='"+XMLReader.getMessage("url")+"' target=\"_blank\">"+XMLReader.getMessage("url")+"</a></div>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("firstAdminLogin :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        LOGGER.debug("firstAdminLogin Ends:");
        return sb.toString();
    }
    public String multipleUsers(){
        LOGGER.debug("multipleUsers Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User,<br/><br/><span>"
                + "Welcome to e-GP System. You may visit the following links for carrying out procurement related tasks in e-GP system.<br/><br/>"
                + "<b>Important links:</b><br/><br/>"
                + "<a href='#'>User Manual for Tenderers / Consultants</a><br/>"
                + "<a href='"+URL+"MemScheduleBank.jsp' target=\"_blank\">List of Member Scheduled Banks</a><br/>"
                + "<a href='"+URL+"help/guidelines/eGP_Guidelines.pdf' target=\"_blank\">e-GP Guidelines</a><br/>"
                + "<a href='"+URL+"TermsNConditions.jsp' target=\"_blank\">Terms and Conditions</a><br/>"
                + "<a href='"+URL+"PrivacyPolicy.jsp' target=\"_blank\">Disclaimer and Privacy Policy</a><br/><br />"
                + "<b>Important Note:</b><br/><br/>"
                + "e-GP system will keep a record of your login details and all the activities that you carry out in the "
                + "e-GP system and hence restrain from sharing your Login ID and Password with anyone else as "
                + "per e-GP security policy.<br /><br />"
                + "<div align=\"center\"><strong>GPPMD Office Contact Details</strong></div>"
                + "<div align=\"center\"><strong>Phone:</strong> +975-02-336962  | <strong>Email:</strong> <a href>info.bhutan@dohatec.com.bd</a></div>"
                + "<div align=\"center\"><a href>"+XMLReader.getMessage("url")+"</a></div>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("multipleUsers :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        LOGGER.debug("multipleUsers Ends:");
        return sb.toString();
    }

    public String firstGovtLogin(){
        LOGGER.debug("firstGovtLogin Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User,<br/><br/><span>"
                + "Welcome to e-GP Portal. You may visit the following links for carrying out "
                + "procurement related tasks in e-GP system and in case of any difficulties please get "
                + "in touch with your Procuring Entity (PE) Admin.<br /><br />"
                + "<b>Important links:</b><br/><br/>"
                + "<a target='_blank' href='#'>User Manual for Procuring Entities</a><br/>"
                + "<a target='_blank' href='"+URL+"MemScheduleBank.jsp'>List of Scheduled Banks</a><br/>"
                + "<a target='_blank' href='"+URL+"help/guidelines/eGP_Guidelines.pdf'>e-GP Guidelines</a><br/>"
                + "<a target='_blank' href='#'>Delegation of Financial Power</a><br/>"
                + "<a target='_blank' href='"+URL+"TermsNConditions.jsp'>Terms and Conditions</a><br/>"
                + "<a target='_blank' href='"+URL+"PrivacyPolicy.jsp'>Disclaimer and Privacy Policy</a><br/><br />"
                + "<b>Important Note:</b><br/><br/>"
                + "e-GP system will keep a record of your login details and all the activities that you carry out in "
                + "the e-GP system and hence restrain from sharing your Login ID and Password with anyone else as per "
                + "e-GP security policy.<br /><br />"
                + "<div align=\"center\"><strong>GPPMD Office  Contact Details</strong></div>"
                + "<div align=\"center\"><strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href>info.bhutan@dohatec.com.bd</a></div>"
                + "<div align=\"center\"><a href ='"+XMLReader.getMessage("url")+"'>"+XMLReader.getMessage("url")+"</a></div>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
             LOGGER.error("firstGovtLogin :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        LOGGER.debug("firstGovtLogin Ends:");
        return sb.toString();
    }

    public String contTOS_POS_SentToTEC_CP(String tenderId, String refNo, String peEmail, String peOffice){
        LOGGER.debug("contTOS_POS_SentToTEC_CP Ends:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "PE has sent TOS/POS report for the below mentioned tender/proposal:<br/><br/>"
                + "Tender/Proposal ID : "+tenderId+"<br/>"
                + "Reference No : "+refNo+"<br/>"
                + "PE : "+peOffice+"<br/>"
                + "e-mail ID of PE : "+peEmail+"<br/><br/>"
                +"For more details, Kindly logon to e-GP System.<br/><br/>"
                + "<br/>Thanks,<br/>"
                + "<b>e-GP System</b><br/><br/>"
                +"<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                +"<strong>Phone:</strong> +975-02-336962  | <strong>Email:</strong> <a href="+XMLReader.getMessage("emailIdHelp")+" target=\"_blank\">"+XMLReader.getMessage("emailIdHelp")+"</a><br />"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contTOS_POS_SentToTEC_CP :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
        }
        LOGGER.debug("contTOS_POS_SentToTEC_CP Ends:");
        return sb.toString();
    }

    /**
     * When PE User sends opening report to the Evaluation Committee Chairperson
     * @param tenderId
     * @param refNo
     * @param peEmail
     * @param peOffice
     * @return
     */
    public String contTOR_POR_SentToTEC_CP(String tenderId, String refNo, String peEmail, String peOffice){
          LOGGER.debug("contTOR_POR_SentToTEC_CP Starts:");
        StringBuilder sb = new StringBuilder();
          try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "PA has sent TOR report for the below mentioned tender/proposal:<br/><br/>"
                + "Tender/Proposal ID : "+tenderId+"<br/>"
                + "Reference No : "+refNo+"<br/>"
                + "PA : "+peOffice+"<br/>"
                + "e-mail ID of PA : "+peEmail+"<br/><br/>"
                +"For more details, Kindly logon to e-GP System.<br/><br/>"
                + "<br/>Thanks,<br/>"
                + "<b>e-GP System</b><br/><br/>"                
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("contTOR_POR_SentToTEC_CP :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
          }
        LOGGER.debug("contTOR_POR_SentToTEC_CP Ends:");
        return sb.toString();
    }

    public String contTOS_POS_SentToPE(String tenderId, String refNo, String email_TOC_CP, String officePE, String Organization){
        LOGGER.debug("contTOS_POS_SentToPE Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "This is to inform you that Opening Committee chairperson has sent the TOS / POS for the below mentioned tender/Proposal::<br/><br/>"
                + "Tender/Proposal ID : "+tenderId+"<br/>"
                + "Reference No : "+refNo+"<br/>"
                + "PA : "+officePE+"<br/>"
                + "Organization : "+Organization+"<br/><br/>"
                + "Thanks,<br/>"
                + "<b>e-GP System</b><br/><br/>"
                +"<div align=\"center\"><br/><strong>GPPMD Office  Contact Details</strong><br />"
                +"<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href="+XMLReader.getMessage("emailIdHelp")+" target=\"_blank\">"+XMLReader.getMessage("emailIdHelp")+"</a><br />"
                + "</td></tr></table></body></html>");
        
        } catch (Exception e) {
            LOGGER.error("contTOS_POS_SentToPE :"+e);
        }
        LOGGER.debug("contTOS_POS_SentToPE Ends:");
        return sb.toString();
    }

    /**
     * When Opening Committee Chairperson sends a opening reports to the PE
     * @param tenderId
     * @param refNo
     * @param email_TOC_CP
     * @param officePE
     * @param Organization
     * @return
     */
    public String contTOR_POR_SentToPE(String tenderId, String refNo, String email_TOC_CP, String officePE, String Organization){
         LOGGER.debug("contTOR_POR_SentToPE Starts:");
        StringBuilder sb = new StringBuilder();
         try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                 + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that Opening Committee chairperson has sent the TOR for the below mentioned tender/Proposal::<br/><br/>"
                + "Tender/Proposal ID : "+tenderId+"<br/>"
                + "Reference No : "+refNo+"<br/>"
                + "PA : "+officePE+"<br/>"
                + "Organization : "+Organization+"<br/><br/>"
                + "Thanks,<br/>"
                + "<b>e-GP System</b><br/><br/>"
                + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("contTOR_POR_SentToPE :"+e);
         }
        LOGGER.debug("contTOR_POR_SentToPE Ends:");
        return sb.toString();
    }

    /**
     * When TEC Chairperson requests to the PE for formation of Technical Sub Committee (TSC)
     * PE user of the tender
     * @param tenderId
     * @param refNo
     * @param peOffice
     * @return List Content of mail
     */
    public String contTSCFormationRequest(String tenderId, String refNo, String peOffice){
        LOGGER.debug("contTSCFormationRequest Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "TEC Chairperson has requested to form a Technical Sub Committee (TSC) in below mentioned Tender/Proposal ID.<br/><br/>"
                + "Tender/Proposal ID : "+tenderId+"<br/>"
                + "Reference No : "+refNo+"<br/>"
                + "PE : "+peOffice+"<br/><br/>"
                + "Thanks,<br/>"
                + "<b>e-GP System</b><br/>"
                //+"<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                //+"<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href="+XMLReader.getMessage("emailIdHelp")+">"+XMLReader.getMessage("emailIdHelp")+"</a><br />"
               // +"<a href="+XMLReader.getMessage("url")+" target=\"_blank\">"+XMLReader.getMessage("url")+"</a></div>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contTOR_POR_SentToPE :"+e);
        }
        LOGGER.debug("contTSCFormationRequest Ends:");
        return sb.toString();
    }

    public String contRegistrationRenewal(String amount, String currencySymbol, String ValidityYrPeriod, String ValidityYrPeriodInWords, String renewalDt, String expiryDt){
        LOGGER.debug("contRegistrationRenewal Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        //String URL = "http://122.170.96.192:8080/eGPWeb/";
         sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
               +"This is to inform you that we have received your e-GP account renewal fee "+currencySymbol + amount+" for "+ValidityYrPeriod+"Year(s) validity on "+renewalDt+". You need to renew your account again before "+expiryDt
               +"<br/><br/>"
               +"Thank you for using e-GP System."
               +"<br/><br/>"
                +"<strong><b>Government Procurement and Property Management Division (GPPMD)</strong></b><br/>"
                +"Department of National Properties, Ministry of Finance, Royal Government of Bhutan<br />"
                +"Address: Box 116, Thimphu, Bhutan. <br />"
                +"GPPMD Office Contact Details<br />"
                +"<strong>Phone:</strong> +975-02-336962  | <strong>Email:</strong> <a href>info.bhutan@dohatec.com.bd</a><br />"
                +"<a href>eprocure.gov.bd </a>"
               // +"<strong>Help Desk Tel: </strong>88-02-9144252-53, Help Desk e-mail: <a href=\"mailto:helpdesk@eprocure.gov.bd\">helpdesk@eprocure.gov.bd</a>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contRegistrationRenewal :"+e);
        }
        LOGGER.debug("contRegistrationRenewal Ends:");
        return sb.toString();
    }

    /**
     * Evaluation type configuration by Evaluation Committee Chair Person (CP)
     * All Evaluation Committee members except CP
     * @param configType
     * @param tenderId
     * @param refNo
     * @param selectedMember
     * @param peEmail
     * @param peOffice
     * @return List Content of mail
     */
    public String contConfigurationNotification(String configType, String tenderId, String refNo, String selectedMember, String peEmail, String peOffice){
        StringBuilder sb = new StringBuilder();
        LOGGER.debug("contConfigurationNotification Ends:");
        try {
        if("Team".equalsIgnoreCase(configType)){
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that Evaluation Committee Chairperson has configured Team Evaluation in evaluation type for below mentioned tender/proposal and hence your consent is required to allow nominated member to start evaluation process.<br/><br/><br/>"
                + "Tender/Proposal ID : "+tenderId+"<br/>"
                + "Reference No : "+refNo+"<br/>"
                + "PE : "+peOffice+"<br/>"
                + "e-mail ID of PE : "+peEmail+"<br/><br/>"
                + "For more details, Kindly logon to e-GP System and follow below mentioned steps to go to Evaluation dashboard:<br/><br/>"
                + "-Logon to e-GP System<br/>"
                + "-Click on Evaluation menu<br/>"
                + "-Click on Evaluation committee sub menu<br/>"
                + "-Click on Dashboard of the tender/proposal<br/>"
               + "<br/>Thanks,<br/>"
                + "<b>e-GP System</b><br/><br/>"
                + "</td></tr></table></body></html>");
        } else {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                 + "This is to inform you that Evaluation Committee Chairperson has configured evaluation methodology in below mentioned tender/proposal and you are requested to start evaluation process.<br/><br/><br/>"
                + "Tender/Proposal ID : "+tenderId+"<br/>"
                + "Reference No : "+refNo+"<br/>"
                + "PE : "+peOffice+"<br/>"
                + "e-mail ID of PE : "+peEmail+"<br/><br/>"
                + "For more details, Kindly logon to e-GP System and follow below mentioned steps to go to Evaluation dashboard:<br/><br/>"
                + "-Logon to e-GP System<br/>"
                + "-Click on Evaluation menu<br/>"
                + "-Click on Evaluation committee sub menu<br/>"
                + "-Click on Dashboard of the tender/proposal<br/>"
               + "<br/>Thanks,<br/>"
                + "<b>e-GP System</b><br/><br/>"
                + "</td></tr></table></body></html>");
        }
        } catch (Exception e) {
            LOGGER.error("contConfigurationNotification :"+e);
        }
        LOGGER.debug("contConfigurationNotification Ends:");
        return sb.toString();
    }

    public String contConfigurationModification(String configType, String tenderId, String refNo, String selectedMember, String peEmail, String peOffice){
        LOGGER.debug("contConfigurationModification Starts:");
        StringBuilder sb = new StringBuilder();
        try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                 + "This is to inform you that Evaluation Committee Chairperson has configured evaluation methodology in below mentioned tender/proposal and you are requested to start evaluation process.<br/><br/><br/>"
                + "Tender/Proposal ID : "+tenderId+"<br/>"
                + "Reference No : "+refNo+"<br/>"
                + "PE : "+peOffice+"<br/>"
                + "e-mail ID of PE : "+peEmail+"<br/><br/>"
                + "For more details, Kindly logon to e-GP System and follow below mentioned steps to go to Evaluation dashboard:<br/><br/>"
                + "-Logon to e-GP System<br/>"
                + "-Click on Evaluation menu<br/>"
                + "-Click on Evaluation committee sub menu<br/>"
                + "-Click on Dashboard of the tender/proposal<br/>"
               + "<br/>Thanks,<br/>"
                + "<b>e-GP System</b><br/><br/>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contConfigurationModification :"+e);
        }
        LOGGER.debug("contConfigurationModification Ends:");
        return sb.toString();
    }
    /**
     * When secondary partner accepts or rejects the JVCA invitation
     * To Nominated and other partners
     * @param partnerNamed
     * @param Status
     * @return
     */
     public String JvcaStatusUpdate(List<Object[]> list){
        LOGGER.debug("JvcaStatusUpdate Starts:");
        StringBuilder sb = new StringBuilder();
         try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that the JVCA partner has processed the JVCA invitation. JVCA invitation and its status detail are as mentioned below<br/><br/><br/>"
                + "<strong>JVCA Name :</strong>"+list.get(0)[6]+"<br /><br />"
                + "<table width='100%' cellspacing='0' class='tableList_1 t_space'>"
                + "<tr>"
                + "<th>Company Name</th>"
                + "<th>JVCA Role</th>"
                + "<th>JVCA Status</th>"
                + "<th>Nominated Partner</th>"
                + "</tr>");
                for(int i=0;i<list.size();i++){
                   sb.append( "<tr>"
                    + "<td>"+list.get(i)[1]+"</td>"
                    + "<td>"+list.get(i)[4]+"</td>"
                    + "<td>"+list.get(i)[2]+"</td>"
                    + "<td>"+list.get(i)[3]+"</td>"
                    + "</tr>");
                }
                sb.append("</Table><br /><br />"
                + "<span class='reqF_1'><strong>If all partners have agreed to the proposed JVCA, the nominated partner needs to register a JVCA.</strong></span><br /><br />"        
                + "<br/>Thanks.<br/>"
                + "<b>e-GP System</b><br/><br/>"
                + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("JvcaStatusUpdate :"+e);
             System.out.println("---------e------------->"+e.toString());
         }
        LOGGER.debug("JvcaStatusUpdate Ends:");
        return sb.toString();
    }

     public String peClariToTenderer(String peOffice,String respDate){
         LOGGER.debug("peClariToTenderer Starts:");
        StringBuilder sb = new StringBuilder();
         try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that PE has sought clarification for debarment. You are requested to respond to the PE before last date of response mentioned below:<br/><br/>"
                + "<b>PE&nbsp;&nbsp;:</b>&nbsp;" + peOffice + "<br/>"
                + "<b>Last Date of Response&nbsp;&nbsp;:</b>&nbsp;" + respDate + "<br/><br/>"                
                + "Thanks.<br/><br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("peClariToTenderer :"+e);
         }
        LOGGER.debug("peClariToTenderer Ends:");
        return sb.toString();
    }
     
     public String tendererResponseToPE(String tendererName){
         LOGGER.debug("tendererResponseToPE Starts:");
        StringBuilder sb = new StringBuilder();
         try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that below mentioned bidder/consultant has responded to your debarment related queries:<br/><br/>"
                + "<b>Name of Bidder / Consultant &nbsp;&nbsp;:</b>&nbsp;" + tendererName + "<br/><br/>"
                + "Thanks.<br/><br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("tendererResponseToPE :"+e);
         }
        LOGGER.debug("tendererResponseToPE Ends:");
        return sb.toString();
    }

     public String debarmentRequestToHope(String peName,String tendererName){
         LOGGER.debug("tendererResponseToPE Starts:");
        StringBuilder sb = new StringBuilder();
         try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that below mentioned PE has sent a debarment request for below mentioned Bidder/consultant:<br/><br/>"
                + "<b>Name of PE&nbsp;&nbsp;:</b>&nbsp;" + peName + "<br/><br/>"
                + "<b>Name of Bidder / Consultant&nbsp;&nbsp;:</b>&nbsp;" + tendererName + "<br/><br/>"                
                + "Thanks.<br/><br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("tendererResponseToPE :"+e);
         }
                LOGGER.debug("tendererResponseToPE Ends:");
        return sb.toString();
    }

     public String hopeFormsDC(String hope,String org,String tendererName,String comName){
         LOGGER.debug("hopeFormsDC Starts:");
        StringBuilder sb = new StringBuilder();
         try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that you have been selected as a member of below mentioned Debarment Committee:<br/><br/>"
                + "<b>Debarment Committee&nbsp;&nbsp;:</b>&nbsp;" + comName + "<br/><br/>"
                + "<b>HOPA&nbsp;&nbsp;:</b>&nbsp;" + hope + "<br/><br/>"
                + "<b>Organization&nbsp;&nbsp;:</b>&nbsp;" + org + "<br/><br/>"
                + "<b>Name of Bidder / Consultant&nbsp;&nbsp;:</b>&nbsp;" + tendererName + "<br/><br/>"                
                + "Thanks.<br/><br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("tendererResponseToPE :"+e);
         }
                LOGGER.debug("hopeFormsDC Ends:");
        return sb.toString();
    }
     public String hopeNotifiesForDebarment (String tendererName){
         LOGGER.debug("hopeNotifiesForDebarment Starts:");
        StringBuilder sb = new StringBuilder();
         try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that hopa has requested you to debar a below mentioned bidder / consultant:<br/><br/>"
                + "<b>Name of Bidder / Consultant&nbsp;&nbsp;:</b>&nbsp;" + tendererName + "<br/><br/>"
                + "Thanks.<br/><br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("hopeNotifiesForDebarment :"+e);
         }
        LOGGER.debug("hopeNotifiesForDebarment Ends:");
        return sb.toString();
    }

     public String debaresTCJ(String debaredFor,String details){
         LOGGER.debug("debaresTCJ Starts:");
        StringBuilder sb = new StringBuilder();
         try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that you have been debarred by the e-GP Admin on e-GP System. Debarment detail is as mentioned below:<br/><br/>"
                + "<b>Debared for &nbsp;&nbsp;:</b>&nbsp;" + debaredFor + "<br/><br/>"
                + "<b>Detail &nbsp;&nbsp;:</b>&nbsp;" + details + "<br/><br/>"
                + "Thanks.<br/><br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("debaresTCJ :"+e);
         }
            LOGGER.debug("debaresTCJ Ends:");
        return sb.toString();
    }
     /**
     * When a branch checker verifies the document fees payment detail or 
     * When a branch checker verifies the tender security payment detail or
     * When a branch checker verifies the performance security payment detail 
     * Tenderer who has paid performance security
     * @param tenderid
     * @param refNo
     * @param paymentFor
     * @param status
     * @param action
     * @param companyName
     * @param bankName
     * @param branchName
     * @param amount
     * @return 
     */
     public String contTenderPaymentVerification(String tenderid,String refNo, String paymentFor, String status, String action, String companyName, String bankName, String branchName, String amount){
         LOGGER.debug("contTenderPaymentVerification Starts:");
        StringBuilder sb = new StringBuilder();
         try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that payment for "+paymentFor+" has been received for below mentioned tender/proposal:<br/><br/>"
                + "Tender/Proposal Id : "+tenderid+"<br/>"
                + "Reference No : "+refNo+"<br/>"
                + "Amount : "+amount+"<br/>"
                + "Financial Institute Name : "+bankName+"<br/>"
                + "Branch Name: "+branchName+"<br/><br/>"
                + "<br/>Thanks.<br/>"
                +"e-GP System<br />"
                + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("contTenderPaymentVerification :"+e);
         }
        LOGGER.debug("contTenderPaymentVerification Ends:");
        return sb.toString();
    }
     /**
     * Publish pre-tender meeting  document
     * Tenderer / Consultant who have purchased tender documents or have posted 
     * @param tenderId
     * @param refNo
     * @param responseTime
     * @return
     */
    public String getPreBidMeetingContent(String tenderId, String refNo, String responseTime){
        LOGGER.debug("getPreBidMeetingContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                +"This is to inform you that Pre-Tender/Proposal Meeting is scheduled tomorrow in below mentioned tender/proposal:"
                +"<br/><br/>"
                +"<b>Tender/Proposal ID :</b>"+tenderId+"<br />"
                +"<b>Reference No. :</b>"+refNo+"<br />"
                +"<b>Pre-Tender/Proposal Meeting Start Date and Time :</b>"+responseTime+"<br /><br />"
                +"For more details, Kindly logon to e-GP System<br/><br />"
                +"Thanks.<br />"
                +"e-GP System<br />"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getPreBidMeetingContent :"+e);
        }
        LOGGER.debug("getPreBidMeetingContent Ends:");
        return sb.toString();
    }

    /**
     * 1 day prior to the tender/proposal opening
     * @param tenderId
     * @param refNo
     * @param responseTime
     * @return
     */
    public String getIntTenderOpenContent(String tenderId, String refNo, String responseTime){
        LOGGER.debug("getIntTenderOpenContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    +"This is to inform you that Tender/Proposal Opening is scheduled tomorrow in below mentioned tender/proposal:"
                    +"<br/><br/>"
                    +"<b>Tender/Proposal ID :</b>"+tenderId+"<br />"
                    +"<b>Reference No. :</b>"+refNo+"<br />"
                    +"<b>Tender/Proposal Opening Start Date and Time :</b>"+responseTime+"<br /><br />"
                    +"For more details, Kindly logon to e-GP System<br/><br />"
                    +"Thanks.<br />"
                    +"e-GP System<br />"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.debug("getIntTenderOpenContent :"+e);
        }
        LOGGER.debug("getIntTenderOpenContent Ends:");
        return sb.toString();
    }

    public String getIntTenderEvalContent(String tenderId, String refNo, String responseTime){

        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                +"This is to inform you that Tender/Proposal Evaluation is scheduled tomorrow in below mentioned tender/proposal:"
                +"<br/><br/>"
                +"<b>Tender/Proposal ID :</b>"+tenderId+"<br />"
                +"<b>Reference No. :</b>"+refNo+"<br />"
                +"<b>Tender/Proposal Evaluation Start Date and Time :</b>"+responseTime+"<br /><br />"
                +"For more details, Kindly logon to e-GP System<br/><br />"
                +"Thanks.<br />"
                +"e-GP System<br />"
                +"<div align=\"center\"><br/><strong>e-GP Help Desk Contact Details</strong><br />"
                +"<strong>Phone:</strong> +880-2-9144 252/53 | <strong>Email:</strong> <a href="+XMLReader.getMessage("emailIdHelp")+">"+XMLReader.getMessage("emailIdHelp")+"</a><br />"
                +"<a href="+XMLReader.getMessage("url")+" target=\"_blank\">"+XMLReader.getMessage("url")+"</a></div>"
                + "</td></tr></table></body></html>");

        return sb.toString();
    }

    public String getMediaUserContent(){
        StringBuilder sb = new StringBuilder();

        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User,<br/><br/>Congratulations!<br /><br /><span>"
                + "Your profile details and mandatory credential documents have been verified, and your registration has been APPROVED.<br/><br/>"
                + "<b>Important links:</b><br/><br/>"
                + "<a href='"+URL+"MemScheduleBank.jsp' target=\"_blank\">List of Member Scheduled Banks</a><br/>"
                + "<a href='"+URL+"help/guidelines/eGP_Guidelines.pdf' target=\"_blank\">e-GP Guidelines</a><br/>"
                + "<a href='"+URL+"TermsNConditions.jsp' target=\"_blank\">Terms and Conditions</a><br/>"
                + "<a href='"+URL+"PrivacyPolicy.jsp' target=\"_blank\">Disclaimer and Privacy Policy</a><br/><br />"
                + "<b>Important Note:</b><br/><br/>"
                + "e-GP system will keep a record of your login details and all the activities that you carry out in the "
                + "e-GP system and hence restrain from sharing your Login ID and Password with anyone else as "
                + "per e-GP security policy.<br /><br />"
                + "<b>Thank you for using e-GP System.</b><br /><br />"
                + "<b>Government Procurement and Property Management Division (GPPMD)</</b>"
                + "Department of National Properties, Ministry of Finance, Royal Government of Bhutan <br /> Address: Box 116, Thimphu, Bhutan.<br /><br />"
                + "<div align=\"center\"><strong>GPPMD Office Contact Details</strong></div>"
                + "<div align=\"center\"><strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href>info.bhutan@dohatec.com.bd</a></div>"
                + "<div align=\"center\"><a href>"+XMLReader.getMessage("url")+"</a></div>"
                + "</td></tr></table></body></html>");

        return sb.toString();
    }

    public String cancelTenderContent(String tenderId,String refNo,String peName){
        LOGGER.debug("cancelTenderContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                +"This is to inform you that below mentioned tender/proposal is cancelled by Procuring Entity:"
                +"<br/><br/>"
                +"<b>Tender/Proposal ID :</b>"+tenderId+"<br />"
                +"<b>Reference No. :</b>"+refNo+"<br />"
                +"<b>Procuring Entity :</b>"+peName+"<br /><br />"
                +"For more details, please log on to e-GP system.<br/><br />"
                +"Thanking you.<br /><br />"
                +"<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                +"<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href="+XMLReader.getMessage("emailIdHelp")+">"+XMLReader.getMessage("emailIdHelp")+"</a><br />"
                +"<a href="+XMLReader.getMessage("url")+" target=\"_blank\">"+XMLReader.getMessage("url")+"</a></div>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("cancelTenderContent :"+e);
        }

        LOGGER.debug("cancelTenderContent Ends:");
        return sb.toString();
    }


     public String contEvalApprovalNeeded(String strTenderId, String strRefNo, String strProcEntity){
        LOGGER.debug("contRegistrationRenewal Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            String URL = "http://122.170.96.192:8080/eGPWeb/";
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                   +"This is to inform you that Evaluation Committee Chairperson has sent Evaluation Report for approval for below mentioned tender/proposal :"
                   +"<br/><br/>"
                  + "Tender/Proposal ID : " + strTenderId
                     + "<br />Reference No. : " + strRefNo
                     + "<br />Procuring Entity : " + strProcEntity
                     + "<br /><br />You are requested to review the evaluation report."
                       + "<br /><br />Thanks."
                     + "<br /><br />e-GP System"
                     + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                     + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                     + "<a href=" + XMLReader.getMessage("url") + " target=\"_blank\">" + XMLReader.getMessage("url") + "</a></div><br />"
                     + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contRegistrationRenewal :"+e);
        }
        LOGGER.debug("contRegistrationRenewal Ends:");
        return sb.toString();
    }

     /**
      * Completion of JVCA registration
      * All the partners except the nominated partner who has registered JVCA
      * @param email
      * @param jvName
      * @param list
      * @return
      */
     public String jvcaFinalSubMsgBox(String email,Object jvName,List<Object[]> list){
             LOGGER.debug("jvcaFinalSubMsgBox Starts:");
        StringBuilder sb = new StringBuilder();
            try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                     + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                     + "<br/>Dear User," + "<br/><br/><span>"
                     + "This is to inform you that the JVCA registration process is completed for below mentioned JVCA:<br/>"
                     + "<br/><br/>"
                     + "<b>JVCA Name</b>		: "+jvName+"<br/><br/>"
                     + "<table width='100%' cellspacing='0' class='tableList_1 t_space'>"
                     + "<tr>"
                     + "<th width='25%'align='center'>Company Name</th>"
                     + "<th width='25%'align='center'>JVCA Role</th>"
                     + "<th width='25%'align='center'>JVCA Status</th>"
                     + "<th width='25%'align='center'>Nominated Partner</th>"
                     + "</tr>");
             int cnt_p=1;
             for (Object[] object : list) {
                sb.append("<tr>"
                     + "<td>"+object[0]+"</td>"
                     + "<td align='center'>"+object[1]+"</td>"
                     + "<td align='center'>"+object[2]+"</td>"
                     + "<td align='center'>"+object[3]+"</td>"
                     + "</tr>");
                cnt_p++;
             }
            sb.append("</table><br/><br/>"
                     + "<span class='reqF_1'><strong>Important Note:<br/>In a Tender/Proposal, either JVCA or individual JVCA Partner can participate.<strong></span><br/>"
                     + "<br /><br />Thanks."
                     + "<br />e-GP System"
                     + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("jvcaFinalSubMsgBox :" + e);
         }
            LOGGER.debug("jvcaFinalSubMsgBox Ends:");
            return sb.toString();
    }
     
      public String conWorkflow(String eventName,String moduleName,String appCode,String officialName , String desig,String fromEmailid){
        LOGGER.debug("conWorkflow Starts:");
           StringBuilder sb  = new StringBuilder();
           try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                + "Dear User," + "<br/><br /><span>"
                + "This is to inform you that you have been included in the following workflow process: <br/><br />"
                +"Module Name: "+moduleName+"<br/>"
                +"Process: "+eventName+"<br/>"
                +"ID / Ref No: "+appCode+"<br/>"
                +"Official Name - Designation: "+officialName+"-"+desig+"<br/><br />"
                +"You will be notified once the file is being sent to you for processing."
                + "<br /><br/><b>Thanks,</b><br/>"
                + "<b>e-GP System.</b><br/>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("conWorkflow :"+e);
        }
        LOGGER.debug("conWorkflow Ends:");
        return sb.toString();
    }
      
    public String committeePublishContent(String tenderId,String offName,String orgName,String commType){
             LOGGER.debug("committeePublishContent Starts:");
        StringBuilder sb = new StringBuilder();
            try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                     + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                     + "<br/>Dear User," + "<br/><br/><span>"
                     + "This is to inform you that you have been selected as a ");
             if(commType.equals("eval")){
                sb.append("Evaluation");
             }else if(commType.equals("open")){
                sb.append("Opening");
             }else if(commType.equals("tsc")){
                sb.append("Technical Sub");
             }
            sb.append(" Committee Member in below mentioned Tender/Proposal : "
                     + "<br/><br/>"
                     + "Tender/Proposal ID : " + tenderId
                     + "<br />PA. : " + offName
                     + "<br />Organization : " + orgName
                     + "<br /><br />Thanks."
                     + "<br /><strong>e-GP System</strong>"
                     + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("committeePublishContent :" + e);
         }
            LOGGER.debug("committeePublishContent Ends:");
            return sb.toString();
    }
    
    /**
     * When Lead partner invites secondary partners to form a JVCA
     * Secondary Partner / Tenderer 
     * @param list
     * @param companyName
     * @return
     */
    public String JvcaStatusComplete(List<Object[]> list, String companyName){
        LOGGER.debug("JvcaStatusUpdate Starts:");
        StringBuilder sb = new StringBuilder();
         try {
             sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that you have been invited by &nbsp;&nbsp;"+companyName+" to form a JVCA. Below mentioned is the JVCA detail:<br/><br/><br/>"
                + "<strong>JVCA Name :</strong>"+list.get(0)[6]+"<br /><br />"
                + "<table width='100%' cellspacing='0' class='tableList_1 t_space'>"
                + "<tr>"
                + "<th>Company Name</th>"
                + "<th>JVCA Role</th>"
                + "</tr>");
                for(int i=0;i<list.size();i++){
                   sb.append( "<tr>"
                    + "<td>"+list.get(i)[1]+"</td>"
                    + "<td>"+list.get(i)[4]+"</td>"
                    + "</tr>");
                }
                sb.append("</table></td></tr><tr><td>"
                + "<br />You may perform below mentioned steps to process JVCA request :<br />"
                + "<div class='t_space' style='padding-left:15px;'><ul>"
                + "<li> Click on Tender -> Propose JVCA menu.<br />"
                + "<li> Click on JVCA Partner Requests tab<br />"
                + "<li> Click on Process link available under action column<br />"
                + "</ul></div>"
                + "<br/><br/>Thanks.<br/>"
                + "<b>e-GP System</b><br/><br/>"
                + "</td></tr></table></body></html>");
         } catch (Exception e) {
             LOGGER.error("JvcaStatusUpdate :"+e);
         }
        LOGGER.debug("JvcaStatusUpdate Ends:");
        return sb.toString();
    }


    /**
     * If no tender/proposal/bid is received then opening committee member notifies PE of the tender
     * @param tenderid
     * @param refNo
     * @return
     */
    public String contOpening_NoBidMsg(String tenderid,String refNo){
          LOGGER.debug("contOpening_NoBidMsg Starts:");
        StringBuilder sb = new StringBuilder();
          try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that there is no Tender/Proposal received in the below mentioned tender/proposal:<br/>"
                + "<br/>"
                + "Tender/Proposal Id: "+tenderid+"<br/>"
                + "Ref No: "+refNo+"<br/><br/>"
                + "You may go for Re-Tendering following the below steps:<br/>"
                + "<br/>"
                +"<br/>&nbsp;&nbsp;&nbsp;1.&nbsp;&nbsp;Click on My Tender."
                +"<br/>&nbsp;&nbsp;&nbsp;2.&nbsp;&nbsp;Click on Archive Tab and search for the Tender/Proposal."
                +"<br/>&nbsp;&nbsp;&nbsp;3.&nbsp;&nbsp;Click on Tender/Proposal Dashboard."
                +"<br/>&nbsp;&nbsp;&nbsp;4.&nbsp;&nbsp;Click on Notice Tab."
                +"<br/>&nbsp;&nbsp;&nbsp;5.&nbsp;&nbsp;Click on 'Re-tender' link and change the necessary information to Re-Tender the same."                
                + "<br/><br/>Thanks.<br/>"
                + "<b>e-GP System.</b><br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contOpening_NoBidMsg :"+e);
          }
        LOGGER.debug("contOpening_NoBidMsg Ends:");
        return sb.toString();
    }

    /**
     * AA seeks clarification from Evaluation Comm. CP
     * Evaluation Committee Chair Person
     * @param tenderId
     * @param refNo
     * @param lotPkgNo
     * @param lotPckNoTxt
     * @param lotPkgDesc
     * @param lotPckDescTxt
     * @param lastDateNTime
     * @param role
     * @return String
     */
    public String contentSeekClari(String tenderId, String refNo, String lotPkgNo, String lotPckNoTxt, String lotPkgDesc, String lotPckDescTxt,String lastDateNTime,String role) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + role + " has sought clarification in an Evaluation Report for a below mentioned Tender/Proposal:<br/><br/>"
                + "Tender/Proposal ID: "+tenderId+"<br/>"
                + "Reference No.: "+refNo+"<br/>"
                + lotPkgNo + ": "+lotPckNoTxt+"<br/>"
                + lotPkgDesc +": "+lotPckDescTxt+"<br/>"
                + "Last date for Response: "+lastDateNTime+"<br/><br/>"
                + "You are requested to give clarification till last date for response."
                + "<br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contentSeekClari :"+e);
          }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }
    /**
     * Evaluation Committee CP gives clarification to the AA
     * Approving Authority
     * @param tenderId
     * @param refNo
     * @param lotPkgNo
     * @param lotPckNoTxt
     * @param lotPkgDesc
     * @param lotPckDescTxt
     * @param lastDateNTime
     * @return Content
     */
    public String contentGiveClari(String tenderId, String refNo, String lotPkgNo, String lotPckNoTxt, String lotPkgDesc, String lotPckDescTxt,String lastDateNTime) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "Evaluation Committee Chairperson has given clarification a below mentioned Tender/Proposal:<br/><br/>"
                + "Tender/Proposal ID: "+tenderId+"<br/>"
                + "Reference No.: "+refNo+"<br/>"
                + lotPkgNo + ": "+lotPckNoTxt+"<br/>"
                + lotPkgDesc +": "+lotPckDescTxt+"<br/>"
                + "Last date for Response: "+lastDateNTime+"<br/><br/>"
                + "Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contentSeekClari :"+e);
          }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }
    public String SendReportAA(String tenderId, String refNo, String lotPckNoTxt, String lotPckDescTxt,String lable) {
        LOGGER.debug("SendReportAA Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that Evaluation Committee Chairperson has sent Evaluation Report for approval/consideration for below mentioned tender/proposal : <br/><br/>"
                + "Tender/Proposal ID: "+tenderId+"<br/>"
                + "Reference No.: "+refNo+"<br/>"
                + lable+" No.: "+lotPckNoTxt+"<br/>"
                + lable +" Desc. : "+lotPckDescTxt+"<br/><br/>"
                + "You are requested to process the evaluation report."
                + "<br /><br />Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("SendReportAA :"+e);
          }
        LOGGER.debug("SendReportAA Ends:");
        return sb.toString();
    }
    
    /**
     * AA Reject Evaluation Report
     * @param tenderId
     * @param refNo
     * @param lotPckNo
     * @param lotPckDesc
     * @param lable
     * @param role
     * @return
     */
    public String aARejectRpt(String tenderId, String refNo, String lotPckNo, String lotPckDesc,String lable, String role) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that "+role+" has Rejected an Evaluation Report in a below mentioned tender/proposal:<br/><br/>"
                + "Tender/Proposal ID: "+tenderId+"<br/>"
                + "Reference No.: "+refNo+"<br/>"
                + lable+" No.: "+lotPckNo+"<br/>"
                + lable +" Desc. : "+lotPckDesc+"<br/><br/>"
                + "Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contentSeekClari :"+e);
          }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }

    /**
     * AA ReTendering Evaluation Report
     * @param tenderId
     * @param refNo
     * @param lotPckNo
     * @param lotPckDesc
     * @param lable
     * @param role
     * @return
     */
    public String aAReTenderRpt(String tenderId, String refNo, String lotPckNo, String lotPckDesc,String lable, String role) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + role+" has recommended for Re-Tendering for below mentioned Tender/Proposal:<br/><br/>"
                + "Tender/Proposal ID: "+tenderId+"<br/>"
                + "Reference No.: "+refNo+"<br/>"
                + lable+" No.: "+lotPckNo+"<br/>"
                + lable +" Desc. : "+lotPckDesc+"<br/><br/>"
                + "Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contentSeekClari :"+e);
          }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }
    /**
     * When HOPE/AO/AA approves the final Evaluation report then system should notify rejected tenderers
     * @param tenderId
     * @param refNo
     * @param lotPckNo
     * @param lotPckDesc
     * @param lable
     * @param role
     * @return
     */
    public String aAApproveRptTend(String tenderId, String refNo, String lotPckNo, String lotPckDesc,String lable, String role) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that your tender/proposal have been rejected in a below tender/proposal::<br/><br/>"
                + "Tender/Proposal ID: "+tenderId+"<br/>"
                + "Reference No.: "+refNo+"<br/>"
                + lable+" No.: "+lotPckNo+"<br/>"
                + lable +" Desc. : "+lotPckDesc+"<br/><br/>"
                + "Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contentSeekClari :"+e);
          }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }
    
    /**
     * AA Approves Evaluation Report and send it to tenderer when case is reoi
     * @param tenderId
     * @param refNo
     * @param tenderBrief
     * @param peOfficeName
     * @return
     */
    public String aAApproveRptSucessTen(String tenderId, String refNo, String tendBrief, String peOffice) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that you have been shortlisted in the following REOI:<br/><br/>"
                + "REOI ID : <strong>" + tenderId + "</strong><br/>"
                + "REOI Reference No. : <strong>" + refNo + "</strong><br/>"
                + "REOI Brief : <strong>" + tendBrief + "</strong><br/>"
                + "PE : <strong>" + peOffice + "</strong><br/><br />"
                + "Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contentSeekClari :"+e);
          }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }
    
    public String aAApproveRpt(String tenderId, String refNo, String lotPckNo, String lotPckDesc,String lable, String role) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "Evaluation Report has been approved for below mentioned Tender/Proposal:<br/><br/>"
                + "Tender/Proposal ID: "+tenderId+"<br/>"
                + "Reference No.: "+refNo+"<br/>"
                + lable+" No.: "+lotPckNo+"<br/>"
                + lable +" Desc. : "+lotPckDesc+"<br/><br/>"
                + "Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contentSeekClari :"+e);
          }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }
    /**
     * Evaluation committee CP notifies other evaluation committee members to sign the TERs report
     * @param tenderId
     * @param refNo
     * @param lotPckNo
     * @param lotPckDesc
     * @param lable
     * @param rptType
     * @return
     */
    public String notifyTECMember(String tenderId, String refNo, String lotPckNo, String lotPckDesc,String lable,String rptType) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
        if("ter1".equalsIgnoreCase(rptType)){
            rptType = "Tender Evaluation Report 1";
        }
        if("ter2".equalsIgnoreCase(rptType)){
            rptType = "Tender Evaluation Report 2";
        }
        if("ter3".equalsIgnoreCase(rptType)){
            rptType = "Tender Evaluation Report 3";
        }
        if("ter4".equalsIgnoreCase(rptType)){
            rptType = "Tender Evaluation Report 4";
        }
        if("per1".equalsIgnoreCase(rptType)){
            rptType = "Proposal Evaluation Report 1";
        }
        if("per2".equalsIgnoreCase(rptType)){
            rptType = "Proposal Evaluation Report 2";
        }
        if("per3".equalsIgnoreCase(rptType)){
            rptType = "Proposal Evaluation Report 3";
        }
        if("per4".equalsIgnoreCase(rptType)){
            rptType = "Proposal Evaluation Report 4";
        }
      try {
          
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "Evaluation Committee Chairperson has requested you to sign the Evaluation report <strong> "+rptType+" </strong> for below mentioned Tender/Proposal:<br/><br/>"
                + "Tender/Proposal ID: "+tenderId+"<br/>"
                + "Reference No.: "+refNo+"<br/>"
                + lable+" No.: "+lotPckNo+"<br/>"
                + lable +" Desc. : "+lotPckDesc+"<br/><br/>"
                + "Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contentSeekClari :"+e);
          }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
        
     
    }
    /*Dohatec Start*/
        /**
     * Evaluation committee CP notifies PE to create contract approval workflow
     * @param tenderId
     * @param refNo
     * @param lotPckNo
     * @param lotPckDesc
     * @param lable
     * @return
     */
    public String notifyPE(String tenderId, String refNo, String lotPckNo, String lotPckDesc,String lable,String Remarks) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
      try {

        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "Evaluation Committee Chairperson has requested you to Create <strong> Contract Approval Workflow </strong> for below mentioned Tender/Proposal:<br/><br/>"
                + "Tender/Proposal ID: "+tenderId+"<br/>"
                + "Reference No.: "+refNo+"<br/>"
                + lable+" No.: "+lotPckNo+"<br/>"
                + lable +" Desc. : "+lotPckDesc+"<br/><br/>"
                +" Comments : "+Remarks+"<br/><br/>"
                + "Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contentSeekClari :"+e);
          }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();


    }
    /*Dohatec End*/
    /**
     * When TEC Chairperson post queries to the tenderers/consultant  as part of clarification round
     * @param tenderId
     * @param refNo
     * @param peOfficeName
     * @param date
     * @return
     */
    public String cpClariBidder(String tenderId, String refNo, String peOfficeName,String date) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "TEC Chairperson has raised queries while evaluating your below mentioned tender/proposal : <br /><br />"
                + "<strong>Tender/Proposal ID :</strong>  "+tenderId+"<br/>"
                + "<strong>Reference No. :</strong>  "+refNo+""
                + "<br /><strong>PE :</strong>  " + peOfficeName
                + "<br /><strong>Last Date of Response :</strong>  " + date
                + "<br /><br />You are requested to respond back to the queries before above mentioned Last Date and Time to enable Evaluation Committee to complete evaluation process.<br />"
                + "<br />Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contentSeekClari :"+e);
          }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }
    
    /**
     * When tenderer/consultant gives response to all required queries and notifies TEC chairperson 
     * Evaluation Committee Chairperson
     * @param tenderId
     * @param refNo
     * @param peOfficeName
     * @param tenderName
     * @return List Content of mail
     */
    public String bidderAnsCp(String tenderId, String refNo, String peOfficeName,String tenderName) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "Below mentioned Bidder/Consultant has provided clarifications for the queries being raised by the Evaluation Committee for below mentioned Tender/Proposal: <br /><br />"
                + "<strong>Tender/Proposal ID :</strong>  "+tenderId+"<br/>"
                + "<strong>Reference No. :</strong>  "+refNo
                + "<br /><strong>PE :</strong>  " + peOfficeName
                + "<br /><strong>Bidder / Consultant&#39;s Name :</strong>  " + tenderName
                + "<br /><br />Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("contentSeekClari :"+e);
          }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }
    
    /**
     * When Individual Evaluation Committee Member notifies Chairperson after posting queries to be sought to the tenderers/consultants as a part of tender/proposal clarification
     * Evaluation Committee Chairperson
     * @param tenderId
     * @param refNo
     * @return List Content of message
     */
    public String evalClari(String tenderid, String refNo) {
        LOGGER.debug("evalClari Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that evaluation committee member has posted queries with respect to tender/proposal received for below mentioned tender/proposal id as a part of evaluation process:<br/><br/>"
                    + "Id: " + tenderid + "<br/>"
                    + "Ref No: " + refNo + "<br/>"
                    + "<br />Thanks<br/>"
                    + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("evalClari :" + e);
        }
        LOGGER.debug("evalClari Ends:");
        return sb.toString();
    }
    
    /**
     * When Individual member completes the evaluation process and notifies TEC chairperson
     * Evaluation Committee Chairperson
     * @param tenderId
     * @param refNo
     * @param peName
     * @return List Content of mail
     */
    public String evalClarification(String tenderid, String refNo,String peName) {
        LOGGER.debug("evalClarification Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "One of the Evaluation Committee members has completed the Evaluation Process in a below mentioned Tender/Proposal:<br/><br/>"
                    + "Tender/Proposal ID: " + tenderid + "<br/>"
                    + "Ref No: " + refNo + "<br/>"
                    + "PE: "+peName+"<br/><br/>"
                    + "You are requested to proceed further.<br/>"
                    + "<br />Thanks<br/>"
                    + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("evalClarification :" + e);
        }
        LOGGER.debug("evalClarification Ends:");
        return sb.toString();
    }


    public String deBriefPostQuestionContent(String tenderId, String refNo, String peOfficeName, String tendererConsultatName) {
        LOGGER.debug("deBriefPostQuestionContent Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + tendererConsultatName+" has sought clarification in a below mentioned Tender/Proposal:<br/><br/>"
                + "Tender/Proposal ID: "+tenderId+"<br/>"
                + "Reference No.: "+refNo+"<br/><br/>"
                + "You are requested to reply to his/her query at earliest.<br/>"
                + "<br />Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("deBriefPostQuestionContent :"+e);
          }
        LOGGER.debug("deBriefPostQuestionContent Ends:");
        return sb.toString();


    }

    public String deBriefPostReplyContent(String tenderId, String refNo, String peOfficeName) {
        LOGGER.debug("deBriefPostQuestionContent Starts:");
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "PE has given clarification in a below mentioned Tender/Proposal:<br/><br/>"
                + "Tender/Proposal ID: "+tenderId+"<br/>"
                + "Reference No.: "+refNo+"<br/><br/>"
                + "<br />Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");

          } catch (Exception e) {
              LOGGER.error("deBriefPostQuestionContent :"+e);
          }
        LOGGER.debug("deBriefPostQuestionContent Ends:");
        return sb.toString();


    }
    /**
     * When Lead partner invites sub contractor or sub consultant
     * Tenderer /Consultant
     * @param emaiId
     * @param name
     * @param tenderId
     * @param tenderRefNo
     * @param list
     * @return
     */
    public String getSubContractInvtDetails(String emaiId, String name, String tenderId, String tenderRefNo, List<Object[]> list) {
        LOGGER.debug("getSubContractInvtDetails Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title> </head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that you have an invitation for Sub Contracting / Sub Consultant from " + name + " for the below mentioned Tender/Proposal.<br/>"
                    //+ "<br/><b>Login ID:</b>&nbsp;&nbsp;" + emaiId
                    + "<br/><table width='100%' cellspacing='0' cellpading='0' style = 'font-family:arial;font-size:14px;line-height:19px;'>"
                    + "<tr><td style='font-weight:bold; vertical-align:middle;' width='150'> Tender/Proposal Id:</td>"
                    + "<td valign='middle' >" + tenderId + "</td></tr>"

                    + "<tr><td style='font-weight:bold; vertical-align:middle;'>Tender/Proposal Ref No:</td><td valign='middle'>" + tenderRefNo + "</td></tr>");
                     if(!list.isEmpty()){
                        sb.append("<tr><td style='font-weight:bold; vertical-align:middle;'>Lot No:</td><td valign='middle'>" + list.get(0)[0] + "</td></tr>"
                            + "<tr><td style='font-weight:bold; vertical-align:middle;'>Lot Description:</td><td valign='middle'>" + list.get(0)[1] + "</td></tr>");
                        }
                    sb.append("</table>"
                    + "<br />You may perform the below mentioned steps to process the invitation:");
                    sb.append( "<br/>1.&nbsp;&nbsp;&nbsp;Login to e-GP website"
                    + "<br/>2.&nbsp;&nbsp;&nbsp;Click on Tender -> All Tenders"
                    + "<br/>3.&nbsp;&nbsp;&nbsp;Search Tender/Proposal using search criteria"
                    + "<br/>4.&nbsp;&nbsp;&nbsp;Click on Tender/Proposal Dashboard icon"
                    //+ "<br/>5.&nbsp;&nbsp;&nbsp;Accept the Terms and conditions of the first tab.<br/>"
                    + "<br/>6.&nbsp;&nbsp;&nbsp;Click on Sub-Contracting tab"
                    + "<br/>7.&nbsp;&nbsp;&nbsp;Click on received Invitations tab"
                    + "<br/>8.&nbsp;&nbsp;&nbsp;Click on accept / reject link to accept or reject the invitation<br/>"
                    + "<br/>Thanks,<br/>"
                    + "<b>e-GP System</b><br/>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getSubContractInvtDetails :" + e);
        }
        list.clear();
        list=null;
        LOGGER.debug("getSubContractInvtDetails Ends:");
        return sb.toString();
    }
    /**
     * When sub contractor or sub consultant accepts or rejects the invitation
     * Main tenderer/consultant who has invited sub contractor / consultant
     * @param emaiId
     * @param tenderId
     * @param tenderRefNo
     * @param invAcceptStatus
     * @param name
     * @param list
     * @return
     */
    public String getProcessSubContractInv(String emaiId, String tenderId, String tenderRefNo, String invAcceptStatus, String name, List<Object[]> list) {
        LOGGER.debug("getProcessSubContractInv Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            
            String s_status = "rejected";
            if ("Approved".equals(invAcceptStatus)) {
                s_status = "accepted";
            }
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title> </head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                        + "Dear User," + "<br/><br/><span>"
                        + "This is to inform you that your Sub Contracting  request has been "+s_status+" by " + name + " for a below mentioned Tender/Proposal: <br/>"
                        //+ "<br/><b>Login ID:</b>&nbsp;&nbsp;" + emaiId
                        + "<br/><table width='100%' cellspacing='0' cellpading='0' style = 'font-family:arial;font-size:14px;line-height:19px;'>"
                        + "<tr><td style='font-weight:bold; vertical-align:middle;' width='150'> Tender/Proposal Id:</td>"
                        + "<td valign='middle' >" + tenderId + "</td></tr>"
                        
                        + "<tr><td style='font-weight:bold; vertical-align:middle;'>Tender/Proposal Ref No:</td><td valign='middle'>" + tenderRefNo + "</td></tr>");
                         if(!list.isEmpty()){
                            sb.append("<tr><td style='font-weight:bold; vertical-align:middle;'>Lot No:</td><td valign='middle'>" + list.get(0)[0] + "</td></tr>"
                                + "<tr><td style='font-weight:bold; vertical-align:middle;'>Lot Description:</td><td valign='middle'>" + list.get(0)[1] + "</td></tr>");
                            }
                         sb.append("</table>");
            sb.append( "<br/><br/>Thanks,<br/>e-Gp System</b><br/>"
                        + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getProcessSubContractInv :" + e);
        }
        list.clear();
        list=null;
        LOGGER.debug("getProcessSubContractInv Ends:");
        return sb.toString();
    }
    
    ResourceBundle resbdl = ResourceBundle.getBundle("properties.cmsproperty");
    /**
     * it sends mail notification to tenderer on configuring the commencement date by PE
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @return string 
     */
    public String ConfiguredCommencementDt(String Operation,boolean flag,String peName,Object[] obj) {
        LOGGER.debug("ConfiguredCommencementDt Starts:");
        String str_consName = "";
        String str_dcaps = "";
            if("1".equals(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
                str_dcaps = "Delivery";
            }else if("2".equalsIgnoreCase(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.worksName");
                str_dcaps = "Contract End";
            }else{
                str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
                str_dcaps = "Completion";
            }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that Procuring Entity "+peName+" "+Operation+" the "
                + "Commencement Start Date and "+str_dcaps+" Date of below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }                
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
               if(!"3".equals(obj[6].toString())){
                    sb.append("<tr><td class='ff'>Payment Terms :</td>");
                    String strPayTerms = "";
                    if(obj[5]==null)
                    {
                        strPayTerms = "-";
                    }
                    else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                    {
                        strPayTerms = "Any Item Any Percent";
                    }
                    else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                    {
                        strPayTerms = "All Item 100 Percent";
                    }
                    else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                    {
                        strPayTerms = "Item Wise 100 Percent";
                    }
                    sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
                }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view Commencement Date and "+str_dcaps+" Date, you are required to follow the"
                + " mentioned steps:<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab >> Commencement Date Tab</li>"
                + "<li style='padding-top:5px;'>System will display Contract Information bar in any view page</li></ol><br/>"
                + "<br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("ConfiguredCommencementDt :"+e);
          }
        LOGGER.debug("ConfiguredCommencementDt Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on editing the delivery schedule date by PE
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @return string 
     */
    public String EditDeliveryScheduleDt(String Operation,boolean flag,String peName,Object[] obj) {
        LOGGER.debug("EditDeliveryScheduleDt Starts:");
        String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that Procuring Entity "+peName+" "+Operation+" "
                + "Delivery Schedule Dates of below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>"
                + "<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>"
                + "<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view Revised Delivery Schedule Date, you are required to follow the mentioned steps:"
                + "<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Delivery Schedule Tab >> Click on View link</li></ol><br/>"
                + "<br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("EditDeliveryScheduleDt :"+e);
          }
        LOGGER.debug("EditDeliveryScheduleDt Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on finalizing the progress report by PE
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @return string 
     */
    public String finalizingProgressReport(String Operation,boolean flag,String peName,Object[] obj) {
        LOGGER.debug("finalizingProgressReport Starts:");
        String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that Procuring Entity "+peName+" "+Operation+" "
                + "the Progress Report of below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
                if(!"3".equals(obj[6].toString())){
                    sb.append("<tr><td class='ff'>Payment Terms :</td>");
                    String strPayTerms = "";
                    if(obj[5]==null)
                    {
                        strPayTerms = "-";
                    }
                    else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                    {
                        strPayTerms = "Any Item Any Percent";
                    }
                    else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                    {
                        strPayTerms = "All Item 100 Percent";
                    }
                    else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                    {
                        strPayTerms = "Item Wise 100 Percent";
                    }
                    sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
                }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view the Finalized Progress Report, you are required to follow the mentioned steps:"
                + "<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Progress Report Tab >> Click on View link</li></ol><br/>"
                + "<br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("finalizingProgressReport :"+e);
          }
        LOGGER.debug("finalizingProgressReport Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on issuance of work completion certificate by PE
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @param UsertypeId
     * @return string 
     */
    public String issueneOfWorkCompletionCertificate(String Operation,boolean flag,String peName,Object[] obj,String UsertypeId) {
        LOGGER.debug("issueneOfWorkCompletionCertificate Starts:");
        String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that Procuring Entity "+peName+" "+Operation+" "
                + "Work Completion Certificate of below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
                if(!"3".equals(obj[6].toString())){
                    sb.append("<tr><td class='ff'>Payment Terms :</td>");
                    String strPayTerms = "";
                    if(obj[5]==null)
                    {
                        strPayTerms = "-";
                    }
                    else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                    {
                        strPayTerms = "Any Item Any Percent";
                    }
                    else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                    {
                        strPayTerms = "All Item 100 Percent";
                    }
                    else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                    {
                        strPayTerms = "Item Wise 100 Percent";
                    }
                    sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
                }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
                if("2".equalsIgnoreCase(UsertypeId))
                {
                    sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view and Download Work Completion Certificate, you are required to follow the mentioned steps:"
                    + "<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Progress Report Tab >> Click on View link >> Download Work Completion Certificate</li></ol><br/>");
                }
                else
                {
                    sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view Work Completion Certificate Status, you are required to follow the mentioned steps:"
                    + "<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                    + "<li style='padding-top:5px;'>Click on Payment tab</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>System will display Work Completion Certificate Status (Issued) in Contract Table</li></ol><br/>");
                }
                sb.append("<br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("issueneOfWorkCompletionCertificate :"+e);
          }
        LOGGER.debug("issueneOfWorkCompletionCertificate Ends:");
        return sb.toString();
    }
    /**
     * it sends mail notification to pe on requesting the issuance of work completion certificate by tenderer
     * @param flag
     * @param obj
     * @return string 
     */
    public String RequestforissueneOfWorkCompletionCertificate(boolean flag,Object[] obj) {
        LOGGER.debug("RequestforissueneOfWorkCompletionCertificate Starts:");
        String str_consName = "";
        String strTenderer ="";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that "+str_consName+" ");
                if("1".equals(obj[12].toString()))
                {
                    strTenderer = obj[10].toString()+" "+obj[11].toString();
                }else{
                    strTenderer = obj[8].toString();
                }
                sb.append(strTenderer+" has Requested "
                + "for Issuing Work Completion Certificate of below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
            if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
          }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To Issue Work Completion Certificate, you are required to follow the mentioned steps:"
                + "<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Progress Report Tab >> Click on Issue Work Completion Certificate link</li></ol><br/>"
                + "<br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("RequestforissueneOfWorkCompletionCertificate :"+e);
          }
        LOGGER.debug("RequestforissueneOfWorkCompletionCertificate Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to pe on generating invoice by tenderer
     * @param Operation
     * @param flag
     * @param obj
     * @return string 
     */
    public String invoiceGeneratedbySupplie(String Operation,boolean flag,Object[] obj,String InvoiceAmt,String MileStoneName,String InvoiceNo) {
        LOGGER.debug("invoiceGeneratedbySupplie Starts:");
        String str_consName = "";
        String SupplierName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        if("1".equals(obj[12].toString()))
        {
            SupplierName = obj[10].toString()+" "+obj[11].toString();
        }
        else{SupplierName = obj[8].toString();}
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that "+str_consName+" "+SupplierName+" "+Operation+" "
                + "Invoice of below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
            if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td class='ff'>Invoice Amount :</td><td>"+InvoiceAmt+"</td></tr>");
            }else{
                sb.append("<tr><td class='ff'>Invoice Amount :</td><td>"+InvoiceAmt+"</td>");
                if(MileStoneName!=null && !"".equalsIgnoreCase(MileStoneName))
                {
                    sb.append("<td class='ff'>MileStone Name :</td><td>"+MileStoneName+"</td></tr>");
                }else{sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr>");}
            }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td class='ff'>Invoice No :</td><td>"+InvoiceNo+"</td></tr></table><br/><br/>To Process Invoice, you are required to follow the mentioned steps:"
                + "<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Payment Tab >> Click on Invoice link (System will display status as Invoice Generated by "+str_consName+")</li>"
                + "<li style='padding-top:5px;'>Add Comments</li>"
                + "<li style='padding-top:5px;'>Click on Accept / Reject button</li>"                               
                + "</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("invoiceGeneratedbySupplie :"+e);
          }
        LOGGER.debug("invoiceGeneratedbySupplie Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to PE on generating invoice calculation by accountant
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @return string 
     */
    public String invoiceNotificationToPEfromAc(String Operation,boolean flag,String peName,Object[] obj,String InvoiceAmt,String MileStoneName) {
        LOGGER.debug("invoiceNotificationToPEfromAc Starts:");
        String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that Accounts Officer "+peName+" "+Operation+" "
                + "the Payment Details of below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td class='ff'>Invoice Amount :</td><td>"+InvoiceAmt+"</td></tr>");
        }else{
                sb.append("<tr><td class='ff'>Invoice Amount :</td><td>"+InvoiceAmt+"</td>");
                if(MileStoneName!=null && !"".equalsIgnoreCase(MileStoneName))
                {
                    sb.append("<td class='ff'>MileStone Name :</td><td>"+MileStoneName+"</td></tr>");
                }else{sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr>");}
        }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view the Invoice Details (Configuration), you are required to follow the mentioned steps:"
                + "<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Payment Tab >> Click on Invoice link (System will display status as In Process)</li>"
                + "<li style='padding-top:5px;'>Add Remarks</li>"
                + "<li style='padding-top:5px;'>Click on Send to Accounts Officer Button</li>"
                + "</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("invoiceNotificationToPEfromAc :"+e);
          }
        LOGGER.debug("invoiceNotificationToPEfromAc Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on sending generated invoice by accountant
     * @param Operation
     * @param flag
     * @param peName
     * @param PeOfficeName
     * @param obj
     * @param IAccDetails
     * @return
     */
    public String invoiceNotificationToSupplierfromAc(String Operation,boolean flag,String peName,String PeOfficeName,Object[] obj,List<TblCmsInvoiceAccDetails> IAccDetails,String InvoiceAmt,String MileStoneName) {
        LOGGER.debug("invoiceNotificationToSupplierfromAc Starts:");
        String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that Accounts Officer "+peName+" from PE Office "+PeOfficeName+" "+Operation+" "
                + "the Payment Details of Cheque / DD / Cash Account to Account Transfer of below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td class='ff'>Invoice Amount :</td><td>"+InvoiceAmt+"</td></tr>");
       }else{
            sb.append("<tr><td class='ff'>Invoice Amount :</td><td>"+InvoiceAmt+"</td>");
            if(MileStoneName!=null && !"".equalsIgnoreCase(MileStoneName))
            {
                sb.append("<td class='ff'>MileStone Name :</td><td>"+MileStoneName+"</td></tr>");
            }else{sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr>");}
       }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>"
                +"<table width='35%' class='tableList_1 infoBarBorder'>"
                +"<tr><th colspan='2'>Details of Payment as mention below:</th></tr>"
                +"<tr><td>Invoice No:</td><td>"+IAccDetails.get(0).getTblCmsInvoiceMaster().getInvoiceId()+"</td></tr>"
                +"<tr><td>Mode of Payment:</td><td>"+IAccDetails.get(0).getModeOfPayment()+"</td></tr>"
                +"<tr><td>Date of Payment:</td><td>"+IAccDetails.get(0).getDateOfPayment()+"</td></tr>"
                +"<tr><td>Financial Institute Name:</td><td>"+IAccDetails.get(0).getBankName()+"</td></tr>"
                +"<tr><td>Branch Name:</td><td>"+IAccDetails.get(0).getBranchName()+"</td></tr>"
                +"<tr><td>Instrument Number:</td><td>"+IAccDetails.get(0).getInstrumentNo()+"</td></tr></table><br/><br/>"
                +"<table width='30%' class='tableList_1'>"
                +"<tr><th colspan='2'>Gross Amount (In Nu.)</th>"
                + "<td>"+IAccDetails.get(0).getGrossAmt()+"</td></tr>"
                +"</table>"
                +"<br/><br/>"
                +"To view the Payment Details (Configuration), you are required to follow the mentioned steps:"
                + "<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Payment Tab >> Click on \"Invoice\" link (System will display status as Processed by Accounts Officer)</li>"
                + "<li style='padding-top:5px;'>Click on Print Icon (if require)</li>"
                + "</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("invoiceNotificationToSupplierfromAc :"+e);
          }
        LOGGER.debug("invoiceNotificationToSupplierfromAc Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to PE/accountant on generating invoice by accountant
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @param UserTypeId
     * @return string 
     */
    public String invoiceNotificationToSuppliandAcfromSupplier(String Operation,boolean flag,String peName,Object[] obj,String UserTypeId,String InvoiceAmt,String MileStoneName,String InvoiceNo) {
        LOGGER.debug("invoiceNotificationToSuppliandAcfromSupplier Starts:");
         String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that PE "+peName+" "+Operation+" "
                + "Invoice of below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td class='ff'>Invoice Amount :</td><td>"+InvoiceAmt+"</td></tr>");
      }else{
            sb.append("<tr><td class='ff'>Invoice Amount :</td><td>"+InvoiceAmt+"</td>");
            if(MileStoneName!=null && !"".equalsIgnoreCase(MileStoneName))
            {
                sb.append("<td class='ff'>MileStone Name :</td><td>"+MileStoneName+"</td></tr>");
            }else{sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr>");}
      }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td class='ff'>Invoice No :</td><td>"+InvoiceNo+"</td></tr></table><br/><br/>To view the Invoice Details (Configuration), you are required to follow the mentioned steps:"
                + "<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>");
                if("2".equalsIgnoreCase(UserTypeId))
                {  
                    sb.append("<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>");
                    String invStr  ="";
                    if("has Rejected".equalsIgnoreCase(Operation))
                    {   
                        invStr = "Invoice Rejected by PE";
                    }else{invStr = "Invoice Generated";}    
                    sb.append("<li style='padding-top:5px;'>Click on Payment Tab >> Click on View Invoice link (System will display status as "+invStr+")</li>");
                }
                else
                {
                    sb.append("<li style='padding-top:5px;'>Click on Payment Tab >> Click on Process link (System will display status Processed by PE)</li>");
                }
                sb.append("</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("invoiceNotificationToSuppliandAcfromSupplier :"+e);
          }
        LOGGER.debug("invoiceNotificationToSuppliandAcfromSupplier Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer/PE on terminating the contarct by PE/tenderer
     * @param flag
     * @param peName
     * @param obj
     * @param ProcurementroleId
     * @param userTypeId
     * @param AcceptReject
     * @return string 
     */
    public String contractTermination(boolean flag,String peName,Object[] obj,String ProcurementroleId,String userTypeId,String AcceptReject) {
        LOGGER.debug("contractTermination Starts:");
         String str_consName = "";
         String supplierName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that ");
                if("3".equalsIgnoreCase(userTypeId))
                {
                    if("Terminate".equalsIgnoreCase(AcceptReject))
                    {
                        sb.append("PE "+peName+"  has Terminated ");
                    }
                    else if("Accept".equalsIgnoreCase(AcceptReject))
                    {
                        sb.append("PE "+peName+"  has Accepted Contract Terminatiion Request for ");
                    }
                    else if("Reject".equalsIgnoreCase(AcceptReject))
                    {
                        sb.append("PE "+peName+"  has Rejected Contract Terminatiion Request for ");
                    }
                }else
                {
                    if("1".equals(obj[12].toString()))
                    {supplierName = obj[10].toString()+" "+obj[11].toString();}else
                    {supplierName = obj[8].toString();}
                    sb.append(str_consName+""+supplierName+"has Initiated for Contract Terminatiion for");
                }
                sb.append("below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
        }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>");
                if("Reject".equalsIgnoreCase(AcceptReject))
                {
                    sb.append("To view the Remarks / Comments, you are required to follow the mentioned steps:");
                }else
                {
                    sb.append("To view the Contract Termination details, you are required to follow the mentioned steps:");
                }
               sb.append("<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>");
                if("25".equalsIgnoreCase(ProcurementroleId))
                {
                    sb.append("<li style='padding-top:5px;'>Contract Termination Status(Contract Terminated) will be displayed in Contract Information bar</li>");
                }else
                {
                    sb.append("<li style='padding-top:5px;'>Click on Contract Termination Tab >> Click on View link </li>");
                }
                sb.append("</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("contractTermination :"+e);
          }
        LOGGER.debug("contractTermination Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on placing the variation oreder by PE
     * @param flag
     * @param peName
     * @param obj
     * @return string 
     */
    public String variationOrderFromPEToContractor(boolean flag,String peName,Object[] obj) {
        LOGGER.debug("variationOrderFromPEToContractor Starts:");
         String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that PE "+peName+" has placed "
                + "Variation Order for below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
      }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view the Variation Order Details, you are required to follow the mentioned steps:"
                + "<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
               if("3".equals(obj[6].toString())){
                    sb.append("<li style='padding-top:5px;'>Click on CMS Tab >> Click on Work Schedule Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Variation Order link</li>");
               }else{
                    sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    +"<li style='padding-top:5px;'>Click on Work Program Tab >> Click on Variation Order link</li>"
                    + "<li style='padding-top:5px;'>System will display the changes which has been made by PE (legend has been defined on Work Program page)</li>");
               }
       sb.append("<li style='padding-top:5px;'>Click on Accept button</li>"
                + "</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("variationOrderFromPEToContractor :"+e);
          }
        LOGGER.debug("variationOrderFromPEToContractor Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to PE on accepting/rejecting by contractor
     * @param flag
     * @param peName
     * @param obj
     * @return string 
     */
    public String variationOrderFromContractorToPE(boolean flag,String peName,Object[] obj) {
        LOGGER.debug("variationOrderFromPEToContractor Starts:");
         String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that "+str_consName+" "+peName+" has accepted "
                + "Variation Order request for below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
       }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view the Variation Order Details, you are required to follow the mentioned steps:"
                + "<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>");
               if("3".equals(obj[6].toString())){
                sb.append("<li style='padding-top:5px;'>Click on Work Schedule Tab >> Click on View link</li>");                   
               }else{
                sb.append("<li style='padding-top:5px;'>Click on Work Program Tab >> Click on View link</li>");                
               }         
       sb.append("</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("variationOrderFromPEToContractor :"+e);
          }
        LOGGER.debug("variationOrderFromPEToContractor Ends:");
        return sb.toString();
    }
    /**
     * it sends mail notification to tenderer on publishing the approved variation order by PE
     * @param flag
     * @param peName
     * @param obj
     * @return string
     */
    public String variationOrderPublished(boolean flag,String peName,Object[] obj) {
        LOGGER.debug("variationOrderPublished Starts:");
        String str_consName = "";
        String str_dcaps = "";
            if("1".equals(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
                str_dcaps = "Delivery";
            }else if("2".equalsIgnoreCase(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.worksName");
                str_dcaps = "Contract End";
            }else{
                str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
            }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that Procuring Entity "+peName+" has Issued Variation Order for the "
                + "below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
      }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view The Variation Order, you are required to follow the"
                + " mentioned steps:<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Work Program >> click on Variation Order History</li></ol><br/>"
                + "<br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("variationOrderPublished :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
          }
        LOGGER.debug("variationOrderPublished Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on editing the delivery schedule date by PE
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @param UserTypeId
     * @param ProcRoleId
     * @param strfor
     * @return
     */
    public String mailForReleaseRequest(boolean flag,String peName,Object[] obj,String userTypeId,String UserTypeId,String ProcRoleId,String strfor) {
        LOGGER.debug("mailForReleaseForefeit Starts:");
         String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>");
                if("7".equalsIgnoreCase(UserTypeId))
                {
                    sb.append("This is to inform you that Financial Institute "+peName+" has "
                    + "Released "+strfor+" for below mention Contract:<br/><br/>");

                }else
                {
                    sb.append("This is to inform you that PA "+peName+" has Requested To "
                    + "Release "+strfor+" for below mention Contract:<br/><br/>");
                }
                sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
       }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>");
                if("3".equalsIgnoreCase(UserTypeId))
                {
                    if("2".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("To view the "+strfor+" Details, you are required to follow the mentioned steps:");
                    }else if("7".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("To Release "+strfor+", you are required to follow the mentioned steps:");
                    }
                    if("2".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                        if("Bid Security".equalsIgnoreCase(strfor)){ 
                            sb.append("<li style='padding-top:5px;'>Click on Payment Tab</li>");
                        }else{               
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                        + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");}
                    }else if("7".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Click on Payment Tab >> Select Tender Payment option</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract for which "+strfor+" need to be Released</li>"
                        + "<li style='padding-top:5px;'>Click on Verify Link</li>"
                        + "<li style='padding-top:5px;'>Add Remarks</li>"
                        + "<li style='padding-top:5px;'>Click On Verify Button</li>");
                    }
                }else{                    
                    if(!"25".equalsIgnoreCase(ProcRoleId))
                    {
                        sb.append("To view the "+strfor+" Details, you are required to follow the mentioned steps:");
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                        if("Bid Security".equalsIgnoreCase(strfor)){ 
                        }else{
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                        + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");}
                    }        
                }
                sb.append("</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("mailForReleaseForefeit :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
          }
        LOGGER.debug("mailForReleaseForefeit Ends:");
        return sb.toString();
    }

    //Code Start by Proshanto Kumar Saha,Dohatec
    /**
     * it sends mail notification to Tenderer and BranchChecker without Contract Signing by PE
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @param UserTypeId
     * @param ProcRoleId
     * @param strfor
     * @param tenderId
     * @return
     */
    public String mailForReleaseRequestWithoutConSign(boolean flag,String peName,Object[] obj,String userTypeId,String UserTypeId,String ProcRoleId,String strfor,String tenderId) {
        LOGGER.debug("mailForReleaseRequestWithoutConSign Starts:");
         String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>");
                if("7".equalsIgnoreCase(UserTypeId))
                {
                    sb.append("This is to inform you that Financial Institute"+peName+" has "
                    + "Released "+strfor+" for below mention Contract:<br/><br/>");

                }else
                {
                    sb.append("This is to inform you that PA "+peName+" has Requested To "
                    + "Release "+strfor+" for below mention Contract:<br/><br/>");
                }
                sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Tender ID :</td><td>"+tenderId+"</td><td class='ff'>Performance Security Amount :</td><td>"+obj[4].toString()+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       sb.append("</table><br/><br/>");
                if("3".equalsIgnoreCase(UserTypeId))
                {
                    if("2".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("To view the "+strfor+" Details, you are required to follow the mentioned steps:");
                    }else if("7".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("To Release "+strfor+", you are required to follow the mentioned steps:");
                    }
                    if("2".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                        if("Bid Security".equalsIgnoreCase(strfor)){
                            sb.append("<li style='padding-top:5px;'>Click on Payment Tab</li>");
                        }else{
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                        + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");}
                    }else if("7".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Click on Payment Tab >> Select Tender Payment option</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract for which "+strfor+" need to be Released</li>"
                        + "<li style='padding-top:5px;'>Click on Verify Link</li>"
                        + "<li style='padding-top:5px;'>Add Remarks</li>"
                        + "<li style='padding-top:5px;'>Click On Verify Button</li>");
                    }
                }else{
                    if(!"25".equalsIgnoreCase(ProcRoleId))
                    {
                        sb.append("To view the "+strfor+" Details, you are required to follow the mentioned steps:");
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                        if("Bid Security".equalsIgnoreCase(strfor)){
                        }else{
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                        + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");}
                    }
                }
                sb.append("</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("mailForReleaseRequestWithoutConSign :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
          }
        LOGGER.debug("mailForReleaseRequestWithoutConSign Ends:");
        return sb.toString();
    }
    //Code End by Proshanto Kumar Saha,Dohatec

    /**
     * it sends mail notification to maker/tenderer on forfeiting/releasing the ps/bg/ts by PE
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @param UserTypeId
     * @param ProcRoleId
     * @param strfor
     * @param strtyp
     * @return string
     */
    public String mailForForfeitRequest(boolean flag,String peName,Object[] obj,String userTypeId,String UserTypeId,String ProcRoleId,String strfor,String strtyp) {
        LOGGER.debug("mailForForfeitRequest Starts:");
         String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>");
                if("7".equalsIgnoreCase(UserTypeId))
                {
                    sb.append("This is to inform you that Financial Institute"+peName+" has "
                    +strtyp+" for below mention Contract:<br/><br/>");

                }else
                {
                    sb.append("This is to inform you that PA "+peName+" has Requested to "
                    +strtyp+" for below mention Contract:<br/><br/>");
                }
                sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
      }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>");
                if("3".equalsIgnoreCase(UserTypeId))
                {
                    if("2".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("To view the "+strfor+" Details, you are required to follow the mentioned steps:");
                    }else if("7".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("To "+strfor+" , you are required to follow the mentioned steps:");
                    }
                    if("2".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                        if("Bid Security".equalsIgnoreCase(strfor)){         
                            sb.append("<li style='padding-top:5px;'>Click on Payment Tab</li>");
                        }else{
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                                + "<li style='padding-top:5px;'>Click on Payment Tab</li>"
                                + "<li style='padding-top:5px;'>Click on Payment Details link</li>");
                        }
                    }else if("7".equalsIgnoreCase(userTypeId)) {
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Click on Payment Tab >> Select Tender Payment option</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract for which "+strfor+" needs to be Compensate</li>"
                        + "<li style='padding-top:5px;'>Click on Verify Link</li>"
                        + "<li style='padding-top:5px;'>Add Remarks</li>"
                        + "<li style='padding-top:5px;'>Click On Verify Button</li>");
                    }
                }else{
                    if(!"25".equalsIgnoreCase(ProcRoleId))
                    {
                        sb.append("To view the "+strfor+" Details, you are required to follow the mentioned steps:");
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                        if("Bid Security".equalsIgnoreCase(strfor)){               
                        }else{  
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                        + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");}
                    }
                }
                sb.append("</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("mailForForfeitRequest :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
          }
        LOGGER.debug("mailForForfeitRequest Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to PE/tenderer on forfeiting/releasing the ps/bg/ts by bank
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @param UserTypeId
     * @param ProcRoleId
     * @return
     */
    public String mailForBankGuaranteeRequest(boolean flag,String peName,Object[] obj,String userTypeId,String UserTypeId,String ProcRoleId) {
        LOGGER.debug("mailForBankGuaranteeRequest Starts:");
         String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>");
                if("7".equalsIgnoreCase(UserTypeId))
                {
                    sb.append("This is to inform you that Financial Institute "+peName+" has "
                    + "Issued New Performance Security for below mention Contract:<br/><br/>");
                }else
                {
                    sb.append("This is to inform you that PA "+peName+" has Requested for "
                    + " New Performance Security for below mention Contract:<br/><br/>");
                }
                sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
          }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>");
                if("3".equalsIgnoreCase(UserTypeId))
                {
                    if("2".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("To view the New Performance Security Details, you are required to follow the mentioned steps:");
                    }
                    if("2".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                        + "<li style='padding-top:5px;'>Click on CMS Tab >> Click on Payment Tab</li>"
                        + "<li style='padding-top:5px;'>Click on New Performance Security Details link</li>");
                    }
                }else{
                    if(!"25".equalsIgnoreCase(ProcRoleId))
                    {
                        sb.append("To view the New Performance Security Details, you are required to follow the mentioned steps:");
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                        + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                        + "<li style='padding-top:5px;'>Click on New Performance Security Details link</li>");
                    }else
                    {
                        sb.append("To view the New Performance Security Details, you are required to follow the mentioned steps:");
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Click on Make Payment Tab</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                        + "<li style='padding-top:5px;'>New Performance Security Status (Issued / Pending) will be Displayed in Contract Information Bar</li>");
                    }
                }
                sb.append("</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("mailForBankGuaranteeRequest :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
          }
        LOGGER.debug("mailForBankGuaranteeRequest Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to PE on forfeiting/releasing the ps/bg/ts by supplier
     * @param flag
     * @param obj
     * @param strps
     * @return string 
     */
    public String ReleasePerfomanceSecurityToPE(boolean flag,Object[] obj,String strps) {
        LOGGER.debug("ReleasePerformanceSecurityToPE Starts:");
        String str_consName = "";
         String strTenderer = "";
        String str_dcaps = "";
            if("1".equals(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
                str_dcaps = "Delivery";
            }else if("2".equalsIgnoreCase(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.worksName");
                str_dcaps = "Contract End";
            }else{
                str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
            }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that "+str_consName+" ");
                if("1".equals(obj[12].toString()))
                {
                    strTenderer = obj[10].toString()+" "+obj[11].toString();
                }else{
                    strTenderer = obj[8].toString();
                }
                sb.append(""+strTenderer+" has Requested for Release "+strps+" for below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
          }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>");
        sb.append("To view the "+strps+" Details, you are required to follow the mentioned steps:");
                sb.append("<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Payment Tab</li>");
                sb.append("<br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("ReleasePerformanceSecurityToPE :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
          }
        LOGGER.debug("ReleasePerformanceSecurityToPE Ends:");
        return sb.toString();
    }

     //Code Start by Proshanto Kumar Saha,Dohatec
     /**
     * it sends mail notification to PE without Contract Signing on forfeiting/releasing the ps/bg/ts by supplier
     * @param flag
     * @param obj
     * @param strps
     * @param tenderId
     * @return string
     */
    public String ReleasePerfomanceSecurityToPEWithoutConSign(boolean flag,Object[] obj,String strps,String tenderId) {
        LOGGER.debug("ReleasePerfomanceSecurityToPEWithoutConSign Starts:");
        String str_consName = "";
         String strTenderer = "";
        String str_dcaps = "";
            if("1".equals(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
                str_dcaps = "Delivery";
            }else if("2".equalsIgnoreCase(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.worksName");
                str_dcaps = "Contract End";
            }else{
                str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
            }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that "+str_consName+" ");
                if("1".equals(obj[12].toString()))
                {
                    strTenderer = obj[10].toString()+" "+obj[11].toString();
                }else{
                    strTenderer = obj[8].toString();
                }
                sb.append(""+strTenderer+" has Requested for Release "+strps+" for below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Tender ID :</td><td>"+tenderId+"</td><td class='ff'>Performance Security Amount :</td><td>"+obj[4].toString()+"</td></tr>"
                + "<tr><td class='ff'>");
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       sb.append("</table><br/><br/>");
        sb.append("To view the "+strps+" Details, you are required to follow the mentioned steps:");
                sb.append("<br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Payment Tab</li>");
                sb.append("<br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("ReleasePerfomanceSecurityToPEWithoutConSign :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
          }
        LOGGER.debug("ReleasePerfomanceSecurityToPEWithoutConSign Ends:");
        return sb.toString();
    }
   //Code End by Proshanto Kumar Saha,Dohatec

    /**
     * Get Message Content for Tenderer COmplite his negotiation and send mail to CP.
     * @param = tenderId
     * @param = refNo
     * @return = String having Message Content.
     */

    public String messageSendTOCPForTendererCompliteNegFillForm(String tenderId,String refNo) {
        LOGGER.debug("mailForTendererCompliteNegFillForm Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            //sb.append("e-GP System:  User Registration &#45; e-mail Verification and Profile Submission Information");
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    +"This is to inform you that Bidder/Consultant has revised the tender/proposal forms as a part of Negotiation for below mentioned Tender/proposal:<br/><br/>"
                    + "<b>Tender/Proposal ID: </b>"+tenderId+" <br/>"
                    + "<b>Reference No.: </b>"+refNo+" <br/><br/>"
                    + "To view the revised tender/proposal forms, please logon to <a href='"+XMLReader.getMessage("url")+"'>"+XMLReader.getMessage("url")+"</a> <br/><br/>"
                    + "<br/><br/> Thanks,"
                    + "<br/><br/> <u>e-GP System</u>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForTendererCompliteNegFillForm :" + e);
        }
        LOGGER.debug("mailForTendererCompliteNegFillForm Ends:");
        return sb.toString();
    }

    /**
     * Get Message Content for tenderer accept or reject neogiation Request.
     * @param tenderId
     * @param refNo
     * @param accrejStatus
     * @return = String having Message Content.
     */
    public String messageSendToCPForTendererAcceptRejectNegoRequest(String tenderId,String refNo,String accrejStatus) {
        LOGGER.debug("mailForTendererCompliteNegFillForm Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            //sb.append("e-GP System:  User Registration &#45; e-mail Verification and Profile Submission Information");
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    +"This is to inform you that Bidder/Consultant has "+accrejStatus+" Negotiation for below mentioned tender/proposal:<br/><br/>"
                    + "<b>Tender/Proposal ID: </b>"+tenderId+" <br/>"
                    + "<b>Reference No.: </b>"+refNo+" <br/>"
                    + "<br/><br/> Thanks,"
                    + "<br/><br/> <u>e-GP System</u>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForTendererCompliteNegFillForm :" + e);
        }
        LOGGER.debug("mailForTendererCompliteNegFillForm Ends:");
        return sb.toString();
    }
    /**
     * it sends mail notification to tenderer on editing work schedule main (forms) by PE
     * @param srvBoqType
     * @param flag
     * @param peName
     * @param obj
     * @return string 
     */
    public String MailforEditWorkSch(String srvBoqType,boolean flag,String peName,Object[] obj) {
        LOGGER.debug("MailforEditWorkSch Starts:");
        String str_consName = "";
        String str_dcaps = "";
            if("1".equals(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
                str_dcaps = "Delivery";
            }else if("2".equalsIgnoreCase(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.worksName");
                str_dcaps = "Contract End";
            }else{
                str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
                str_dcaps = "Completion";
            }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>"
                + "This is to inform you that Procuring Entity "+peName+" has edited "
                + "Work Schedule("+srvBoqType+") of below mention Contract:<br/><br/>"
                + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }                
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
          }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view Revised Work Schedule, you are required to follow the mentioned steps:"
                + " <br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Work Schedule Tab >> Click on View History link</li></ol><br/>"
                + "<br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("MailforEditWorkSch :"+e);
          }
        LOGGER.debug("MailforEditWorkSch Ends:");
        return sb.toString();
    }
    /**
     * it sends mail notification to PE on creating the AttanSheet by supplier
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @return string 
     */
    public String CreatedAttanSheetbySupplier(boolean flag,String peName,Object[] obj,String userTypeId) {
        LOGGER.debug("CreatedAttanSheetbySupplier Starts:");
        String str_consName = "";
        String str_dcaps = "";
        String SupplierName = "";
            if("1".equals(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
                str_dcaps = "Delivery";
            }else if("2".equalsIgnoreCase(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.worksName");
                str_dcaps = "Contract End";
            }else{
                str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
                str_dcaps = "Completion";
            }
            if("1".equals(obj[12].toString()))
            {
                SupplierName = obj[10].toString()+" "+obj[11].toString();
            }
            else{SupplierName = obj[8].toString();}
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>");
                if("2".equalsIgnoreCase(userTypeId))
                {    
                    sb.append("This is to inform you that "+str_consName+" "+SupplierName+" has created Attendance Sheet "
                    + "for below mentioned Contract:<br/><br/>");
                }else{
                    sb.append("This is to inform you that PE "+peName+" has Accepted / Rejected Attendance Sheet "
                    + "for below mentioned Contract:<br/><br/>");
                }
        sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }                
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
          }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view the Attandance Sheet details, you are required to follow the mentioned steps:"
                + " <br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Progress Report Tab >> Click on View Attendance Sheet link</li></ol><br/>"
                + "<br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("CreatedAttanSheetbySupplier :"+e);
          }
        LOGGER.debug("CreatedAttanSheetbySupplier Ends:");
        return sb.toString();
    }
    /**
     * it sends mail notification to tenderer on finalizing repeat order by PE
     * @param flag
     * @param peName
     * @param obj
     * @return string 
     */
    public String MailForFinaliseRo(boolean flag,String peName,Object[] obj) {
        LOGGER.debug("MailForFinaliseRo Starts:");
        String str_consName = "";
        String str_dcaps = "";
        String SupplierName = "";
            if("1".equals(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
                str_dcaps = "Delivery";
            }else if("2".equalsIgnoreCase(obj[6].toString())){
                str_consName = resbdl.getString("CMS.works.mailContent.worksName");
                str_dcaps = "Contract End";
            }else{
                str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
                str_dcaps = "Completion";
            }
            if("1".equals(obj[12].toString()))
            {
                SupplierName = obj[10].toString()+" "+obj[11].toString();
            }
            else{SupplierName = obj[8].toString();}
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>");
                sb.append("This is to inform you that PE "+peName+" has finalized Repeat Order "
                + "for below mention Contract:<br/><br/>");
        sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append(str_consName);
                    sb.append("</td><td>"+str+"</td>");
                }                
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
       if(!"3".equals(obj[6].toString())){
                sb.append("<tr><td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
            }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>To view the Repeat Order details, you are required to follow the mentioned steps:"
                + " <br/><ol style='padding-left:35px;'>"
                + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                + "<li style='padding-top:5px;'>Click on Repeat Order Tab</li></ol><br/>"
                + "<br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("MailForFinaliseRo :"+e);
          }
        LOGGER.debug("MailForFinaliseRo Ends:");
        return sb.toString();
    }
    /*Dohatec Start For extend performance security validity*/
     /**
     * it sends mail notification to tenderer on editing the delivery schedule date by PE
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @param UserTypeId
     * @param ProcRoleId
     * @param strfor
     * @return
     */
    public String mailForExtendValidity(boolean flag,String peName,Object[] obj,String userTypeId,String UserTypeId,String ProcRoleId,String strfor,String tenderId) {
        LOGGER.debug("mailForExtendValidity Starts:");
         String str_consName = "";
        if("1".equals(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        }else if("2".equalsIgnoreCase(obj[6].toString())){
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        }else{
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
      try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                + "<br/>Dear User," + "<br/><br/><span>");
                if("7".equalsIgnoreCase(UserTypeId))
                {
                    sb.append("This is to inform you that Financial Institute"+peName+" has "
                    + "Extended "+strfor+" for below mention Contract:<br/><br/>");

                } else if("2".equalsIgnoreCase(UserTypeId))
                {
                    sb.append("This is to inform you that tenderer has accepted validity extension request of "+strfor+" from PE for below mention Contract:<br/><br/>");
                }
                else
                {
                    sb.append("This is to inform you that PE "+peName+" has Requested To "
                    + "Extend validity of "+strfor+" for below mention Contract:<br/><br/>");
                }
                sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                + "<tr><th colspan='4'>Contract Details</th></tr>"
                + "<tr><td class='ff'>Contract No :</td><td>"+obj[1].toString()+"</td><td class='ff'>Contract Value :</td><td>"+obj[2].toString()+"</td></tr>"
                + "<tr><td class='ff'>Contract Start Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td><td class='ff'>Contract End Date :</td><td>"+DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))+"</td></tr>"
                + "<tr><td class='ff'>");
                sb.append(str_consName);
                if("1".equals(obj[12].toString()))
                {
                    String str = obj[10].toString()+" "+obj[11].toString();
                    sb.append("</td><td>"+str+"</td>");
                }else{
                    String str = obj[8].toString();
                    sb.append("</td><td>"+str+"</td>");
                }
       sb.append("<td class='ff'>email ID :</td><td>"+obj[9].toString()+"</td></tr>");
     //  sb.append("<tr><td class='ff'>Tender ID :</td><td>"+tenderId+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
       sb.append("<tr><td class='ff'>Tender ID :</td><td>"+tenderId+"</td>");

       if(!"3".equals(obj[6].toString())){
               // sb.append("<tr><td class='ff'>Payment Terms :</td>");
           sb.append("<td class='ff'>Payment Terms :</td>");
                String strPayTerms = "";
                if(obj[5]==null)
                {
                    strPayTerms = "-";
                }
                else if("anyitemanyp".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Any Item Any Percent";
                }
                else if("allitem100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "All Item 100 Percent";
                }
                else if("itemwise100p".equalsIgnoreCase(obj[5].toString()))
                {
                    strPayTerms = "Item Wise 100 Percent";
                }
               // sb.append("<td>"+strPayTerms+"</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
                sb.append("<td>"+strPayTerms+"</td></tr>");
       }
                sb.append("<tr><td class='ff'>Work Status :</td>");
                if(flag)
                {sb.append("<td>Completed</td>");}else{sb.append("<td>Pending</td>");}
       sb.append("<td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><br/>");
                if("3".equalsIgnoreCase(UserTypeId))
                {
                    if("2".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("To view the "+strfor+" Details, you are required to follow the mentioned steps:");
                    }else if("7".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("To extend validity of "+strfor+", you are required to follow the mentioned steps:");
                    }
                    if("2".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                        if("Bid Security".equalsIgnoreCase(strfor)){
                            sb.append("<li style='padding-top:5px;'>Click on Payment Tab</li>");
                        }else{
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                        + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");}
                    }else if("7".equalsIgnoreCase(userTypeId))
                    {
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>"
                        + "<li style='padding-top:5px;'>Click on Payment Tab >> Select Tender Payment option</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract for which validity of "+strfor+" need to be extended</li>"
                        + "<li style='padding-top:5px;'>Click on Extension Link</li>"
                        + "<li style='padding-top:5px;'>Change Date</li>"
                        + "<li style='padding-top:5px;'>Add Remarks</li>"
                        + "<li style='padding-top:5px;'>Click On Submit Button</li>");
                    }
                }else{
                    if(!"25".equalsIgnoreCase(ProcRoleId))
                    {
                        sb.append("To view the "+strfor+" Details, you are required to follow the mentioned steps:");
                        sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to e-GP system</li>");
                         if(!("7".equalsIgnoreCase(userTypeId))){
                        sb.append("<li style='padding-top:5px;'>Select Tender/Contract</li>");
                        if("Bid Security".equalsIgnoreCase(strfor)){
                        }else{
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                        + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");}
                         } else {
                            sb.append("<li style='padding-top:5px;'>Click on Payment Tab >> Select Tender Payment option</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract for which validity of "+strfor+" need to be extended</li>"
                        + "<li style='padding-top:5px;'>Click on Extension Link</li>"
                        + "<li style='padding-top:5px;'>Change Date</li>"
                        + "<li style='padding-top:5px;'>Add Remarks</li>"
                        + "<li style='padding-top:5px;'>Click On Submit Button</li>");
                         }
                    }
                }
                sb.append("</ol><br/><br/>Thanks<br/>"
                + "e-GP System<br/><br/>"
                + "</td></tr></table></body></html>");
          } catch (Exception e) {
              LOGGER.error("mailForExtendValidity :"+e);
             AppExceptionHandler expHanble = new AppExceptionHandler();
            expHanble.stack2string(e);
          }
        LOGGER.debug("mailForExtendValidity Ends:");
        return sb.toString();
    }
    /*Dohatec End for extend performance security validity*/
        public String EvalReportSentToTC(String tenderId, String email_TOC_CP){
        LOGGER.debug("EvalReportSentToTC Starts:");
        StringBuilder sb = new StringBuilder();
        try {
        sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                + "This is to inform you that Evaluation Committee chairperson has sent the Evaluation Report for the below mentioned tender/Proposal::<br/><br/>"
                + "Tender/Proposal ID : "+tenderId+"<br/>"
                + "Thanks,<br/>"
                + "<b>e-GP System</b><br/><br/>"
                +"<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                +"<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href="+XMLReader.getMessage("emailIdHelp")+" target=\"_blank\">"+XMLReader.getMessage("emailIdHelp")+"</a><br />"
                + "</td></tr></table></body></html>");
        
        } catch (Exception e) {
            LOGGER.error("EvalReportSentToTC :"+e);
        }
        LOGGER.debug("EvalReportSentToTC Ends:");
        return sb.toString();
    }
}
