/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblGradeMaster;
import com.cptu.egp.eps.service.serviceinterface.CreateGradeService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class GradeMasterSrBean extends HttpServlet
{
    static final Logger LOGGER = Logger.getLogger(GradeMasterSrBean.class);
    String logUserId = "0";
    
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
       
        }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        LOGGER.debug("doPost "+logUserId+" Starts:");
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
        }
        
        PrintWriter out=response.getWriter();
        CreateGradeService createGradeService = (CreateGradeService) AppContext.getSpringBean("CreateGradeService");
        createGradeService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")));
        createGradeService.setUserId(logUserId);
        
        
        try {
            String funName = request.getParameter("funName");
            if (funName != null && funName.equals("verifyGrade")) {
                LOGGER.debug("verifyGrade "+logUserId+" Starts:");
                String grade=request.getParameter("grade");
                boolean gradeExsits = createGradeService.verifyGrade(grade);
                if(gradeExsits){
                    out.print("Class Already Exsits");
                }else{
                    out.print("Ok");
                }
                LOGGER.debug("verifyGrade "+logUserId+" Ends:");
                out.flush();
            }
            else {
                String action = request.getParameter("action");

                String grade = request.getParameter("grade");
                byte gradeLevel = Byte.parseByte(request.getParameter("gradeLevel"));
                short gradeId=0;

                TblGradeMaster tblGradeMaster = new TblGradeMaster();
                if (action.equalsIgnoreCase("update")) {
                    LOGGER.debug("update "+logUserId+" Starts");
                    gradeId = Short.parseShort(request.getParameter("gradeId"));
                    tblGradeMaster.setGradeId(gradeId);
                    LOGGER.debug("update "+logUserId+" Starts");
                }
                tblGradeMaster.setGrade(grade);
                tblGradeMaster.setGradeLevel(gradeLevel);

                if (action.equalsIgnoreCase("create")) {
                    LOGGER.debug("create "+logUserId+" Starts");
                    try {
                        gradeId=createGradeService.createGrade(tblGradeMaster);
                    }
                    catch (Exception ex) {
                        LOGGER.error("Create Grade "+logUserId+" : " + ex.getMessage());
                        response.sendRedirect("admin/CreateGrade.jsp?msg=failure");
                    }
                    LOGGER.debug("create "+logUserId+" Ends");
                    response.sendRedirect("admin/ViewGrade.jsp?gradeId="+gradeId+"&msg=Success&from=Created");
                }
                else {
                    try {
                        createGradeService.updateGrade(tblGradeMaster);
                    }
                    catch (Exception ex) {
                        LOGGER.error("doPost "+logUserId+": " + ex.getMessage());
                        response.sendRedirect("admin/EditGrade.jsp?msg=failure&gradeId="+gradeId);
                    }
                    response.sendRedirect("admin/ViewGrade.jsp?gradeId="+gradeId+"&msg=Success&from=Updated");
                }
            }
        }
        finally {
            createGradeService = null;
            out=null;
        }
        LOGGER.debug("doPost "+logUserId+": ends");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
