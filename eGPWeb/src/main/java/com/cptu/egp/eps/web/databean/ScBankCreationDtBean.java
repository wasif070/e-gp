/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class ScBankCreationDtBean {

     private String state;
     private String country;
     private String bankName;
     private String officeAddress;
     private String district;
     private String city;
     private String thanaUpzilla;
     private String postCode;
     private String phoneNo;
     private String faxNo;
     private int sbankDevelHeadId;
     private String swiftCode;
     private String webSite;
     private int createdBy;
     private Date createdDate;
     private String isBranchOffice;
     private String partnerType;
     private int officeId;
     private String headOfficeName;
     private short stateId;
     private short countryId;

    public short getCountryId()
    {
        return countryId;
    }

    public void setCountryId(short countryId)
    {
        this.countryId = countryId;
    }

    public short getStateId()
    {
        return stateId;
    }

    public void setStateId(short stateId)
    {
        this.stateId = stateId;
    }

    public String getBankName()
    {
        return bankName;
    }

    public void setBankName(String bankName)
    {
        this.bankName = bankName;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public int getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(int createdBy)
    {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getDistrict()
    {
        return district;
    }

    public void setDistrict(String district)
    {
        this.district = district;
    }

    public String getFaxNo()
    {
        return faxNo;
    }

    public void setFaxNo(String faxNo)
    {
        this.faxNo = faxNo;
    }

    public String getIsBranchOffice()
    {
        return isBranchOffice;
    }

    public void setIsBranchOffice(String isBranchOffice)
    {
        this.isBranchOffice = isBranchOffice;
    }

    public String getOfficeAddress()
    {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress)
    {
        this.officeAddress = officeAddress;
    }

    public String getPartnerType()
    {
        return partnerType;
    }

    public void setPartnerType(String partnerType)
    {
        this.partnerType = partnerType;
    }

    public String getPhoneNo()
    {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo)
    {
        this.phoneNo = phoneNo;
    }

    public String getPostCode()
    {
        return postCode;
    }

    public void setPostCode(String postCode)
    {
        this.postCode = postCode;
    }

    public int getSbankDevelHeadId()
    {
        return sbankDevelHeadId;
    }

    public void setSbankDevelHeadId(int sbankDevelHeadId)
    {
        this.sbankDevelHeadId = sbankDevelHeadId;
    }

    public String getSwiftCode()
    {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode)
    {
        this.swiftCode = swiftCode;
    }

    public String getThanaUpzilla()
    {
        return thanaUpzilla;
    }

    public void setThanaUpzilla(String thanaUpzilla)
    {
        this.thanaUpzilla = thanaUpzilla;
    }

    public String getWebSite()
    {
        return webSite;
    }

    public void setWebSite(String webSite)
    {
        this.webSite = webSite;
    }

    public int getOfficeId()
    {
        return officeId;
    }

    public void setOfficeId(int officeId)
    {
        this.officeId = officeId;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getState()
    {
        return state;
    }

    public String getHeadOfficeName()
    {
        return headOfficeName;
    }

    public void setHeadOfficeName(String headOfficeName)
    {
        this.headOfficeName = headOfficeName;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public void populateInfo()
    {
        ScBankDevpartnerService scBankDevpartnerService=(ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
        TblScBankDevPartnerMaster tblScBankDevPartnerMaster=scBankDevpartnerService.getSbDevInfo(officeId);
        setBankName(tblScBankDevPartnerMaster.getSbDevelopName());
        setOfficeAddress(tblScBankDevPartnerMaster.getOfficeAddress().trim());
        setState(tblScBankDevPartnerMaster.getTblStateMaster().getStateName());
        setStateId(tblScBankDevPartnerMaster.getTblStateMaster().getStateId());
        setCountryId(tblScBankDevPartnerMaster.getTblCountryMaster().getCountryId());
        setCountry(tblScBankDevPartnerMaster.getTblCountryMaster().getCountryName());
        setCity(tblScBankDevPartnerMaster.getCityTown());
        setThanaUpzilla(tblScBankDevPartnerMaster.getThanaUpzilla());
        setCreatedBy(tblScBankDevPartnerMaster.getCreatedBy());
        setCreatedDate(tblScBankDevPartnerMaster.getCreatedDate());
        setPhoneNo(tblScBankDevPartnerMaster.getPhoneNo());
        setFaxNo(tblScBankDevPartnerMaster.getFaxNo());
        setPostCode(tblScBankDevPartnerMaster.getPostCode());
        setIsBranchOffice(tblScBankDevPartnerMaster.getIsBranchOffice());
        setSwiftCode(tblScBankDevPartnerMaster.getSwiftCode());
        setWebSite(tblScBankDevPartnerMaster.getWebsite());
        int headOffice=tblScBankDevPartnerMaster.getSbankDevelHeadId();
        if(headOffice!=0)
        {
            setSbankDevelHeadId(headOffice);
            tblScBankDevPartnerMaster=scBankDevpartnerService.getSbDevInfo(headOffice,"2nd");
            setHeadOfficeName(tblScBankDevPartnerMaster.getSbDevelopName());
        }
    }
}