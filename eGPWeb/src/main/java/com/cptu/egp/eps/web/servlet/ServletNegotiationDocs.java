/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.*;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *Uploading downloading and removing of file in negotiation module
 * @author Administrator
 */
public class ServletNegotiationDocs extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("ServletNegotiationDocs");
    private File destinationDir;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }
    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer"));
        HttpSession session = request.getSession();
        int negId = 0;
        int tenderId = 0;
        File file = null;
        String docSizeMsg = "";
        boolean checkret = false;
        boolean flag = false;
        String documentBrief = "";
        String fileName = "";
        long fileSize = 0;
        String queryString = "";
        String auditAction = "";
        response.setContentType("text/html");
        boolean dis = false;
        String userId = session.getAttribute("userId") + "";
        String userTypeId = session.getAttribute("userTypeId").toString();
        String docType = "revisedoc";
        String uId = null;
        
        String pageName = "officer/NegotiationDocsUpload.jsp";
        
        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {
            
            try {
                try {
                    if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("remove")) {
                        if (userTypeId.equals("2")) {
                            pageName = "resources/common/ViewNegTendDocs.jsp";
                        }
                        int docId = 0;
                        String id = "";
                        String docName = "";
                        if (request.getParameter("docId") != null) {
                            docId = Integer.parseInt(request.getParameter("docId"));

                        }
                        if (request.getParameter("docType") != null) {
                            docType = request.getParameter("docType");

                        }
                        if (request.getParameter("negId") != null) {
                            negId = Integer.parseInt(request.getParameter("negId"));

                        }
                        if (request.getParameter("tenderId") != null) {
                            tenderId = Integer.parseInt(request.getParameter("tenderId"));

                        }
                        if (request.getParameter("docName") != null) {
                            docName = request.getParameter("docName");
                        }
                        if("2".equalsIgnoreCase(userTypeId)){
                            DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("ServletNegReplyDocs");
                        }else{
                            DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("ServletNegotiationDocs");
                        }
                        String whereContition = "";
                        whereContition = "negDocId= " + docId;
                        fileName = DESTINATION_DIR_PATH + negId+"_"+docType.trim() + "\\" + docName;
                        checkret = deleteFile(fileName);
                        if (checkret) {
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk;
                            commonMsgChk = commonXMLSPService.insertDataBySP("delete", "tbl_NegotiationDocs", "", whereContition).get(0);
                            queryString = "?negId=" + negId + "&tenderId=" + tenderId+"&uId="+uId+"&docType="+docType;
                            if(request.getHeader("referer")!=null){
                                response.sendRedirect(request.getHeader("referer"));
                            }else{
                                response.sendRedirect(pageName + queryString);
                            }
                        }

                    } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("download")) {
                        if (request.getParameter("docType") != null) {
                            docType = request.getParameter("docType");

                        }
                        String usId = userTypeId;
                        if(request.getParameter("ub")!=null){
                            usId = request.getParameter("ub");
                            
                        }
                        if("2".equalsIgnoreCase(usId) ){
                            DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("ServletNegReplyDocs");
                        }else{
                            DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("ServletNegotiationDocs");
                        }
                        //fileName = DESTINATION_DIR_PATH + negId+"_"+docType.trim() + "\\" ;
                        file = new File(DESTINATION_DIR_PATH + request.getParameter("negId")+"_"+docType + "\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                        InputStream fis = new FileInputStream(file);
                        byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                        int offset = 0;
                        int numRead = 0;
                        while ((offset < buf.length)
                                && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                            offset += numRead;

                        }
                        fis.close();
                        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                        buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName") + "\"");
                        ServletOutputStream outputStream = response.getOutputStream();
                        outputStream.write(buf);
                        outputStream.flush();
                        outputStream.close();
                    } else {
                        if("2".equalsIgnoreCase(userTypeId)){
                            pageName = "tenderer/NegotiationDocsUpload.jsp";
                        }else{
                            pageName = "officer/NegotiationDocsUpload.jsp";
                        }
                        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                        /*
                         *Set the size threshold, above which content will be stored on disk.
                         */
                        fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
		/*
                         * Set the temporary directory to store the uploaded files of size above threshold.
                         */
                        fileItemFactory.setRepository(tmpDir);

                        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                        /*
                         * Parse the request
                         */
                        List items = uploadHandler.parseRequest(request);
                        Iterator itr = items.iterator();
                        //For Supporting Document
                        while (itr.hasNext()) {
                            FileItem item = (FileItem) itr.next();
                            //For Supporting Document
                    /*
                             * Handle Form Fields.
                             */
                            if (item.isFormField()) {

                                if (item.getFieldName().equals("documentBrief")) {
                                    if (item.getString() == null || item.getString().trim().length() == 0) {
                                        dis = true;
                                        break;
                                    }
                                    documentBrief = item.getString();
                                } else if (item.getFieldName().equals("negId")) {
                                    negId = Integer.parseInt(item.getString());
                                } else if (item.getFieldName().equals("tenderId")) {
                                    tenderId = Integer.parseInt(item.getString());
                                } else if (item.getFieldName().equals("uId") && item.getString()!=null) {
                                    uId = item.getString();
                                } else if (item.getFieldName().equals("docType") && item.getString()!=null) {
                                    docType = item.getString();
                                }
                            } else {
                                if (item.getName().lastIndexOf("\\") != -1) {
                                    fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                } else {
                                    fileName = item.getName();
                                }
                                //fileName  = fileName.replaceAll(" ", "");
                                if (userTypeId.equals("2") && "revisedoc".equalsIgnoreCase(docType)){
                                    DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("ServletNegReplyDocs");
                                }else{
                                    DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("ServletNegotiationDocs");
                                }
                                String realPath = DESTINATION_DIR_PATH + negId+"_"+docType.trim();//request.getSession().getAttribute("userId")
                                destinationDir = new File(realPath);
                                if (!destinationDir.isDirectory()) {
                                    destinationDir.mkdir();
                                }
                                docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                                // docSizeMsg = docSizeMsg(1);//userID.
                                if (!docSizeMsg.equals("ok")) {
                                    //response.sendRedirect("SupportingDocuments.jsp?fq="+docSizeMsg);
                                } else {
                                    fileSize = item.getSize();
                                    checkret = checkExnAndSize(fileName, item.getSize(), "common");

                                    if (!checkret) {

                                        break;
                                    } else {
                                        file = new File(destinationDir, fileName);
                                        if (file.isFile()) {
                                            flag = true;
                                            break;

                                        }
                                        item.write(file);
                                        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                                        fileSize = fileEncryptDecryptUtil.fileEncryptUtil(file, (int)fileSize);
                                    }
                                }
                            }

                        }
                    }

                } catch (FileUploadException ex) {
                    log("Error encountered while parsing the request", ex);
                } catch (Exception ex) {
                    log("Error encountered while uploading file", ex);
                }


                if (!docSizeMsg.equals("ok")) {
                    queryString = "?fq=" + docSizeMsg;

                } else {
                    if (dis) {
                        docSizeMsg = "Please Enter Discription";
                        queryString = "?fq=" + docSizeMsg + "&negId=" + negId + "&tenderId=" + tenderId+"&uId="+uId+"&docType="+docType;

                        response.sendRedirect(pageName + queryString);
                    } else {
                        if (flag) {
                            docSizeMsg = "File already Exists";
                            queryString = "?fq=" + docSizeMsg + "&negId=" + negId + "&tenderId=" + tenderId+"&uId="+uId+"&docType="+docType;
                            response.sendRedirect(pageName + queryString);
                        } else {

                            if (!checkret) {
                                CheckExtension ext = new CheckExtension();
                                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                queryString = "?fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension() + "&negId=" + negId + "&tenderId=" + tenderId+"&uId="+uId+"&docType="+docType;
                                response.sendRedirect(pageName + queryString);

                            } else {
                                
                                if (documentBrief != null && documentBrief != "") {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                                    documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    String xmldata = "";
                                    if(uId.equalsIgnoreCase(session.getAttribute("userId").toString())){
                                        uId = "0";
                                    }
                                    xmldata = "<tbl_NegotiationDocs negId=\"" + negId + "\" documentName=\"" + fileName + "\" docDescription=\"" + documentBrief + "\" docSize=\"" + fileSize + "\" uploadedDate=\"" + format.format(new Date()) + "\" uploadedBy=\"" + session.getAttribute("userId") + "\" userId=\"" +uId+ "\" uploadedWhen=\""+docType+ "\" />";
                                    xmldata = "<root>" + xmldata + "</root>";
                                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    CommonMsgChk commonMsgChk;
                                    try {
                                        commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_NegotiationDocs", xmldata, "").get(0);
                                        queryString = "?negId=" + negId + "&tenderId=" + tenderId+"&uId="+uId+"&docType="+docType;
                                        response.sendRedirect(pageName + queryString);
                                        if (commonMsgChk != null || handleSpecialChar != null) {
                                            commonMsgChk = null;
                                            handleSpecialChar = null;
                                        }
                                        auditAction = "Document Upload by TEC-CP";
                                    } catch (Exception ex) {
                                        Logger.getLogger(ServletNegotiationDocs.class.getName()).log(Level.SEVERE, null, ex);
                                        auditAction = "Error in "+auditAction+" : "+ex.getMessage();
                                    }finally{
                                        makeAuditTrailService.generateAudit(objAuditTrail, tenderId, "tenderId", EgpModule.Negotiation.getName(), auditAction, documentBrief); 
                                    }

                                }
                            }

                        }
                    }
                }
            } finally {
                //   out.close();
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
/*chking size and extesion*/
    public boolean checkExnAndSize(String extn, long size, String userType) {
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
            dsize = configurationMaster.getFileSize();
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        return chextn;
    }
/*Doc size msg*/
    public String docSizeMsg(int userId) {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        return userRegisterService.docSizeCheck(userId);
    }
/*For deleting file*/
    public boolean deleteFile(String filePath) {

        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }

    }
}
