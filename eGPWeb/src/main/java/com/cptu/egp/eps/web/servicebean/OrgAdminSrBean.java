/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblAdminMaster;
import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.service.serviceinterface.AdminRegistrationService;
import com.cptu.egp.eps.web.databean.AdminMasterDtBean;
import com.cptu.egp.eps.web.databean.LoginMasterDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author Administrator
 */
public class OrgAdminSrBean {

    private AdminRegistrationService adminRegService = (AdminRegistrationService) AppContext.getSpringBean("AdminRegistrationService");
    List<SelectItem> organizationList = new ArrayList<SelectItem>();
    static final Logger LOGGER = Logger.getLogger(OrgAdminSrBean.class);
    private String logUserId = "0";
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        adminRegService.setAuditTrail(auditTrail);
    }

    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        adminRegService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }
    int officeId;

    public int getOfficeId() {
        return officeId;
    }

    public void setOfficeId(int officeId) {
        this.officeId = officeId;
    }

    /**
     * copies properties
     * @param adminMasterDtBean
     * @return Object with copied properties
     */
    public TblAdminMaster _toAdminMaster(AdminMasterDtBean adminMasterDtBean) {
        TblAdminMaster adminMaster = new TblAdminMaster();
        BeanUtils.copyProperties(adminMasterDtBean, adminMaster);
        return adminMaster;
    }

    /**
     * copies properties
     * @param loginMasterDtBean
     * @return Object with copied properties
     */
    public TblLoginMaster _toLoginMaster(LoginMasterDtBean loginMasterDtBean) {
        TblLoginMaster loginMaster = new TblLoginMaster();
        BeanUtils.copyProperties(loginMasterDtBean, loginMaster);
        //loginMaster.setPassword(""+loginMasterDtBean.getPassword().hashCode());
        return loginMaster;
    }

    /**
     * Fetching information from Tbl_DepartmentMaster
     * @param values where condition
     * @return list of TblDepartmentMaster
     */
    public List<SelectItem> getOrganizationList() {
        LOGGER.debug("getOrganizationList : " + logUserId + " Starts");
        if (organizationList.isEmpty()) {
            try {
                for (TblDepartmentMaster tblDepartmentMaster : adminRegService.findTblOrgMaster("departmentType", Operation_enum.LIKE, "Organization", "approvingAuthorityId", Operation_enum.EQ, 0, "departmentName", Operation_enum.ORDERBY, Operation_enum.ASC)) {
                    organizationList.add(new SelectItem(tblDepartmentMaster.getDepartmentId(), tblDepartmentMaster.getDepartmentName()));
                }
            } catch (Exception ex) {
                LOGGER.error("getOrganizationList : " + logUserId + ex);
            }
        }
        LOGGER.debug("getOrganizationList : " + logUserId + " Ends");
        return organizationList;
    }

    public void setOrganizationList(List<SelectItem> organizationList) {
        this.organizationList = organizationList;
    }

    /*
     * Author : Sanjay Prajapati
     * Method : orgAdminReg() - To Register Organization Admin
     * @param : Object AdminMasterDtBean,Object LoginMasterDtBean
     * @return : integer 0 in case org admin not created else userId
     */
    public Integer orgAdminReg(AdminMasterDtBean adminMasterDtBean, LoginMasterDtBean loginMasterDtBean, String pwd) {
        LOGGER.debug("orgAdminReg : " + logUserId + " Starts");
        TblLoginMaster loginMaster = _toLoginMaster(loginMasterDtBean);
        loginMaster.setBusinessCountryName("");
        loginMaster.setRegistrationType("officer");
        loginMaster.setIsJvca("no");
        loginMaster.setBusinessCountryName("bangladesh");
        loginMaster.setNextScreen("ChangePassword");
        loginMaster.setIsEmailVerified("yes");
        loginMaster.setFailedAttempt((byte) 0);
        loginMaster.setValidUpTo(null);
        loginMaster.setFirstLogin("yes");
        loginMaster.setIsPasswordReset("no");
        loginMaster.setResetPasswordCode("0");
        loginMaster.setStatus("approved");
        loginMaster.setRegisteredDate(new Date());
        loginMaster.setNationality("bangladeshi");
        loginMaster.setHintQuestion("");
        loginMaster.setHintAnswer("");
        loginMaster.setTblUserTypeMaster(new TblUserTypeMaster((byte) 5));
        TblAdminMaster adminMaster = _toAdminMaster(adminMasterDtBean);
            int userId = 0;
        try {
            userId =  adminRegService.registerOrganizationAdmin(adminMaster, loginMaster, this.getOfficeId());
            if (userId > 0) {
                sendMailAndSMS(loginMasterDtBean.getEmailId(), pwd, adminMasterDtBean.getMobileNo());
                }
        } catch (Exception ex) {
            LOGGER.error("orgAdminReg : " + logUserId + ex);
            }
        LOGGER.debug("orgAdminReg : " + logUserId + " Ends");
            return userId;
        }

    /**
     * send mail and sms for registered Org admin
     * @param mailId
     * @param password
     * @param mobNo
     * Return : Boolean 
     */
    private boolean sendMailAndSMS(String mailId, String password, String mobNo) {
        LOGGER.debug("sendMailAndSMS : " + logUserId + " Starts");
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            MailContentUtility mailContentUtility = new MailContentUtility();

            List list = mailContentUtility.getGovtMailContent(mailId, password);
            String mailSub = list.get(0).toString();
            String mailText = list.get(1).toString();
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);
            try {
                sendMessageUtil.sendEmail();
            } catch (Exception ex) {
                LOGGER.error("sendMailAndSMS : " + logUserId + ex);
            }

            try {
                SendMessageUtil sendSMS = new SendMessageUtil();
                sendSMS.setSmsNo("+975" + mobNo);
                sendSMS.setSmsBody("Dear User,%0Aprofile has been created Login information is sent to your registered Email ID.%0Ae-GP User Registration Desk");
                sendSMS.sendSMS();
            } catch (Exception ex) {
                LOGGER.error("sendMailAndSMS : " + logUserId + ex);
            }
            mailSent = true;
        } catch (Exception ex) {
            LOGGER.error("sendMailAndSMS : " + logUserId + ex);
        }
        LOGGER.debug("sendMailAndSMS : " + logUserId + " Ends");
        return mailSent;
    }
}
