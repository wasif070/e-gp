/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblBudgetType;
import com.cptu.egp.eps.model.table.TblConfigAmendment;
import com.cptu.egp.eps.model.table.TblConfigNoa;
import com.cptu.egp.eps.model.table.TblConfigPreTender;
import com.cptu.egp.eps.model.table.TblConfigProcurement;
import com.cptu.egp.eps.model.table.TblConfigStd;
import com.cptu.egp.eps.model.table.TblConfigTec;
import com.cptu.egp.eps.model.table.TblFinancialPowerByRole;
import com.cptu.egp.eps.model.table.TblFinancialYear;
import com.cptu.egp.eps.model.table.TblProcurementMethod;
import com.cptu.egp.eps.model.table.TblProcurementNature;
import com.cptu.egp.eps.model.table.TblProcurementRole;
import com.cptu.egp.eps.model.table.TblProcurementTypes;
import com.cptu.egp.eps.model.table.TblTenderTypes;
import com.cptu.egp.eps.model.table.TblTemplateMaster;
import com.cptu.egp.eps.service.serviceinterface.ConfigPreTenderRuleService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Naresh.Akurathi
 */
public class ConfigPreTenderRuleSrBean {

    static final Logger logger = Logger.getLogger(ConfigPreTenderRuleSrBean.class);

    private ConfigPreTenderRuleService preTenderRulesevice=(ConfigPreTenderRuleService)AppContext.getSpringBean("configPreTenderRuleService");

    private String logUserId="0";

    public void setLogUserId(String logUserId) {
        preTenderRulesevice.setLogUserId(logUserId);
        this.logUserId = logUserId;
     }
     /**
      * List of all TenderTypes
      * @return List of TblTenderTypes
      */
     public List<TblTenderTypes> getTenderNames(){
        logger.debug("getTenderNames : "+logUserId+" Starts ");
        List<TblTenderTypes> list = null;
        try {
            list = preTenderRulesevice.getTenderTypes();
        } catch (Exception e) {
            logger.error("getTenderNames : "+logUserId+" : "+e);
    }
        logger.debug("getTenderNames : "+logUserId+" Ends ");
        return list;
    }

    /**
     *List of all ProcurementMethod
     * @return List of TblProcurementMethod
     */
    public List<TblProcurementMethod> getProcurementMethod(){
        logger.debug("getProcurementMethod : "+logUserId+" Starts ");
        List<TblProcurementMethod> list = null;
        try {
            list = preTenderRulesevice.getProcurementMethod();
        } catch (Exception e) {
             logger.error("getProcurementMethod : "+logUserId+" : "+e);
    }
        logger.debug("getProcurementMethod : "+logUserId+" Ends ");
        return list;
    }

    /**
     *List of all ProcurementNature
     * @return List of TblProcurementNature
     */
    public List<TblProcurementNature> getProcurementNature(){
       logger.debug("getProcurementNature : "+logUserId+" Starts ");
       List<TblProcurementNature> list = null;
        try {
            list = preTenderRulesevice.getProcurementNature();
        } catch (Exception e) {
            logger.error("getProcurementNature : "+logUserId+" : "+e);
    }
       logger.debug("getProcurementNature : "+logUserId+" Ends ");
       return list;
    }

    /**
     *List of all ProcurementTypes
     * @return List of TblProcurementTypes
     */
    public List<TblProcurementTypes> getProcurementTypes(){
        logger.debug("getProcurementTypes : "+logUserId+" Starts ");
        List<TblProcurementTypes>  list = null;
        try {
            list = preTenderRulesevice.getProcurementTypes();
        } catch (Exception e) {
            logger.error("getProcurementTypes : "+logUserId+" : "+e);
    }
        logger.debug("getProcurementTypes : "+logUserId+" Ends ");
        return list;
    }

    /**
     *List of all Tender Templates
     * @return List of TblTemplateMaster
     */
    public List<TblTemplateMaster> getTemplateMaster(){
        logger.debug("getTemplateMaster : "+logUserId+" Starts ");
        List<TblTemplateMaster> list = null;
        try {
            list = preTenderRulesevice.getAllTemplateMaster();
        } catch (Exception e) {
            logger.error("getTemplateMaster : "+logUserId+" : "+e);
    }
        logger.debug("getTemplateMaster : "+logUserId+" Ends ");
        return list;
    }

    /**
     *List of all Accepted Templates
     * @return List of TblTemplateMaster
     */
    public List<TblTemplateMaster> getAcceptTemplateMaster() {
        logger.debug("getAcceptTemplateMaster : "+logUserId+" Starts ");
        List<TblTemplateMaster> list = null;
        try {
            list = preTenderRulesevice.getAcceptTemplateMaster();
        } catch (Exception e) {
               logger.error("getAcceptTemplateMaster : "+logUserId+" : "+e);
    }
        logger.debug("getAcceptTemplateMaster : "+logUserId+" Ends ");
        return list;
    }

   
    /**
     *Inserts PreTender Configuration
     * @param master
     * @return if error than returns error else nothing
     */
    public String addConfigPreTenderRule(TblConfigPreTender master){
        logger.debug("addConfigPreTenderRule : "+logUserId+" Starts ");
        String str = "";
        try{
            str = preTenderRulesevice.addConfigPreTenderRule(master);
        }catch(Exception e){
            logger.error("addConfigPreTenderRule : "+logUserId+" : "+e);
            str = "Error";
        }
        logger.debug("addConfigPreTenderRule : "+logUserId+" Ends ");
        return str;
    }

    /**
     *Deletes all Pre Tender Configuration
     * @return if error than returns error else nothing
     */
    public String deleteAllConfigPreTenderRule(){
        String str ="";
        logger.debug("deleteAllConfigPreTenderRule : "+logUserId+" Starts ");
        try{
           str = preTenderRulesevice.delAllConfigPreTenderrule();
        }catch(Exception e){
          logger.error("deleteAllConfigPreTenderRule : "+logUserId+" : "+e);
           str = "Error";
        }
        logger.debug("deleteAllConfigPreTenderRule : "+logUserId+" Ends ");
        return str;
    }

    /**
     *Delete selected Pre Tender Configuration
     * @param master to be deleted
     * @return if error than returns error else nothing
     */
    public String dellConfigPreTenderrule(TblConfigPreTender master){
        logger.debug("dellConfigPreTenderrule : "+logUserId+" Starts ");
        String str="";
        try {
            str = preTenderRulesevice.dellConfigPreTenderrule(master);
        } catch (Exception e) {
            logger.error("dellConfigPreTenderrule : "+logUserId+" : "+e);
    }
        logger.debug("dellConfigPreTenderrule : "+logUserId+" Ends ");
        return str;
    }

    /**
     *List of all Pre Tender Configuration
     * @return List of TblConfigPrefTender
     */
    public List<TblConfigPreTender> getConfigPreTenderRuleDetails(){
        logger.debug("getConfigPreTenderRuleDetails : "+logUserId+" Starts ");
        List<TblConfigPreTender> list = null;
        try{
            list = preTenderRulesevice.getAllConfigPreTenderRule();
        }catch(Exception e){
            logger.error("getConfigPreTenderRuleDetails : "+logUserId+" : "+e);
        }
        logger.debug("getConfigPreTenderRuleDetails : "+logUserId+" Ends ");
        return list;
    }

    /**
     *List of Pre Tender Configuration based on id
     * @param id search criteria
     * @return List of TblConfigPrefTender
     */
    public List<TblConfigPreTender> getConfigPreTenderRule(int id){
         logger.debug("getConfigPreTenderRule : "+logUserId+" Starts ");
         List<TblConfigPreTender> list = null;
         try {
             list = preTenderRulesevice.getConfigPreTenderRule(id);
         } catch (Exception e) {
            logger.error("getConfigPreTenderRule : "+logUserId+" : "+e);
     }
         logger.debug("getConfigPreTenderRule : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Updates Pre Tender Configuration
      * @param master row to be saved in tbl_ConfigPreTender
      * @return if error than returns error else nothing
      */
     public String updateConfigPreTenderRule(TblConfigPreTender master){
        logger.debug("updateConfigPreTenderRule : "+logUserId+" Starts ");
        String str ="";
        try {
            str = preTenderRulesevice.updateConfigPreTenderRule(master);
        } catch (Exception e) {
            logger.error("updateConfigPreTenderRule : "+logUserId+" : "+e);
    }
        logger.debug("updateConfigPreTenderRule : "+logUserId+" Ends ");
        return str;
    }

    /**
     *Inserting STD configuration
     * @param configStd value of tbl_ConfigStd
     * @return if error than returns error else nothing
     */
    public String addConfigStd(TblConfigStd configStd){
         logger.debug("addConfigStd : "+logUserId+" Starts ");
         String str = "";
        try{
            str = preTenderRulesevice.addTblConfigStd(configStd);
        }catch(Exception e){
             logger.error("addConfigStd : "+logUserId+" : "+e);
        }
        logger.debug("addConfigStd : "+logUserId+" Ends ");
        return str;
    }

    /**
     *Delete all STD configuration
     * @return if error than returns Not Deleted else Deleted
     */
    public String delAllConfigStd(){
         logger.debug("delAllConfigStd : "+logUserId+" Starts ");
         String str ="";
         try{
             str = preTenderRulesevice.delAllConfigStd();
         }catch(Exception e){
             logger.error("delAllConfigStd : "+logUserId+" : "+e);
         }
          logger.debug("delAllConfigStd : "+logUserId+" Ends ");
         return str;

     }

     /**
      *Deletes particular STD configuration
      * @param configStd instance to be deleted
      * @return if error than returns Not Deleted else Deleted
      */
     public String delConfigStd(TblConfigStd configStd){
         logger.debug("delConfigStd : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.delConfigStd(configStd);
         } catch (Exception e) {
              logger.error("delConfigStd : "+logUserId+" : "+e);
     }
         logger.debug("delConfigStd : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Fetch all STD Configuration
      * @return List of TblConfigStd
      */
     public List<TblConfigStd> getAllConfigStd(){
         logger.debug("getAllConfigStd : "+logUserId+" Starts ");
         List<TblConfigStd> list = null;
         try{
             list = preTenderRulesevice.getAllConfigStd();
         }catch(Exception e){
            logger.error("getAllConfigStd : "+logUserId+" : "+e);
         }
         logger.debug("getAllConfigStd : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Fetch STD Configuration by id
      * @param id search is to be done
      * @return List of TblConfigStd
      */
     public List<TblConfigStd> getConfigStd(int id){
         logger.debug("getConfigStd : "+logUserId+" Starts ");
         List<TblConfigStd> list = null;
         try {
             list = preTenderRulesevice.getConfigStd(id);
         } catch (Exception e) {
            logger.error("getConfigStd : "+logUserId+" : "+e);
     }
         logger.debug("getConfigStd : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Update TblConfigStd
      * @param configStd to be updated
      * @return if error than returns Updated else Not Updated
      */
     public String updateConfigStd(TblConfigStd configStd){
          logger.debug("updateConfigStd : "+logUserId+" Starts ");
          String str  ="";
          try {
              str = preTenderRulesevice.updateConfigStd(configStd);
          } catch (Exception e) {
              logger.error("updateConfigStd : "+logUserId+" : "+e);
      }
          logger.debug("updateConfigStd : "+logUserId+" Ends ");
          return str;
      }
     
      /**
       *Inserting TEC Configuration
       * @param configTec to be inserted
       * @return if error than returns Not Added else Values Added
       */
      public String addConfigTec(TblConfigTec configTec){
         logger.debug("addConfigTec : "+logUserId+" Starts ");
         String str ="";
         try {
             str = preTenderRulesevice.addTblConfigTec(configTec);
         } catch (Exception e) {
             logger.error("addConfigTec : "+logUserId+" : "+e);
     }
         logger.debug("addConfigTec : "+logUserId+" Ends ");
         return str;
     }

     /**
      *List of TblConfigTec based on id
      * @param id to be searched on
      * @return List of TblConfigTec
      */
     public List<TblConfigTec> getConfigTec(int id){
         logger.debug("addConfigTec : "+logUserId+" Starts ");
         List<TblConfigTec> list = null;
         try {
             list = preTenderRulesevice.getTblConfigTec(id);
         } catch (Exception e) {
             logger.error("addConfigTec : "+logUserId+" : "+e);
     }
         logger.debug("addConfigTec : "+logUserId+" Ends ");
         return list;
     }

     /**
      *List of TEC configuration based on committee type
      * @param comitteType committee type on which search is to be done
      * @return List of TblConfigTec
      */
     public List<TblConfigTec> getCommiteeTypeData(String comitteType){
         logger.debug("getCommiteeTypeData : "+logUserId+" Starts ");
         List<TblConfigTec> list = null;
         try{
             String[] str = comitteType.split(",");
            Object[] obj = new Object[str.length];
             for(int i=0;i<str.length;i++){
                 obj[i]  = str[i];
                 logger.debug("ComiteType===="+obj[i]);
             }
            logger.debug("List Contains at SrBean==="+preTenderRulesevice.getCommiteeTypeData(obj));
             list =  preTenderRulesevice.getCommiteeTypeData(obj);
         }catch(Exception e){
           logger.error("getCommiteeTypeData : "+logUserId+" : "+e);
         }
         logger.debug("getCommiteeTypeData : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Delete TEC configuration based on configTec
      * @param configTec to be deleted
      * @return if error than returns Not Deleted else Deleted
      */
     public String delConfigTec(TblConfigTec configTec){
         logger.debug("delConfigTec : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.delConfigTec(configTec);
         } catch (Exception e) {
             logger.error("delConfigTec : "+logUserId+" : "+e);
     }
         logger.debug("delConfigTec : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Delete all TEC Configuration
      * @param value condition on which deleted
      * @return if error than returns Not Deleted else Values Deleted
      */
     public String delAllConfigTec(String value){
         logger.debug("delAllConfigTec : "+logUserId+" Starts ");
         String str ="";
         try{
             List<TblConfigTec> lt=this.getCommiteeTypeData(value);
             int isqure=0;
             if( lt.isEmpty() && lt == null ){
                 str = "No Data found in this CommitteType";
             }else{

                 Iterator it=lt.iterator();
                 TblConfigTec ctec = new TblConfigTec();

                 while(it.hasNext()){
                     ctec=(TblConfigTec)it.next();
                     preTenderRulesevice.delAllConfigTec(ctec);
                     isqure++;
                 }



             }
             
             if(isqure > 0)
                str = "Deleted";
             else
                str = "Not Deleted";

         }catch(Exception e){
             logger.error("delAllConfigTec : "+logUserId+" : "+e);
             str = "Not Deleted due to Error";
         }
         logger.debug("delAllConfigTec : "+logUserId+" Ends ");
         return str;

     }

     /**
      *Updates TEC Configuration based on configTec
      * @param configTec to be updated
      * @return if error than returns Not Deleted else Values Deleted
      */
     public String updateConfigTec(TblConfigTec configTec){
         logger.debug("updateConfigTec : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.updateConfigTec(configTec);
         } catch (Exception e) {
               logger.error("updateConfigTec : "+logUserId+" : "+e);
     }
         logger.debug("updateConfigTec : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Insert Amendment/Corrigendum Configuration
      * @param configAmend to be inserted
      * @return if error than returns Not Added else Values Value Added
      */
     public String addConfigAmendment(TblConfigAmendment configAmend){
         logger.debug("updateConfigTec : "+logUserId+" Starts ");
         String str = "";
         try{
             str = preTenderRulesevice.addTblConfigAmendment(configAmend);
         }catch(Exception e){
             logger.error("updateConfigTec : "+logUserId+" : "+e);
             str = "Not Added";
         }
         logger.debug("updateConfigTec : "+logUserId+" Ends ");
         return str;

     }

     /**
      *Get all Amendment/Corrigendum Configuration
      * @return List of TblConfigAmendment
      */
     public List<TblConfigAmendment> getConfigAmendment(){
         logger.debug("getConfigAmendment : "+logUserId+" Starts ");
         List<TblConfigAmendment> list = null;
         try {
             list = preTenderRulesevice.getTblConfigAmendment();
         } catch (Exception e) {
             logger.error("getConfigAmendment : "+logUserId+" : "+e);
     }
         logger.debug("getConfigAmendment : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Get Amendment/Corrigendum Configuration based on id
      * @param id on which search is done
      * @return List of TblConfigAmendment
      */
     public List<TblConfigAmendment> getConfigAmendment(int id){
         logger.debug("getConfigAmendment : "+logUserId+" Starts ");
         List<TblConfigAmendment> list = null;
         try {
             list = preTenderRulesevice.getTblConfigAmendment(id);
         } catch (Exception e) {
             logger.error("getConfigAmendment : "+logUserId+" : "+e);
     }
         logger.debug("getConfigAmendment : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Delete Amendment/Corrigendum Configuration
      * @param configAmnd which is to be deleted
      * @return if error than returns Not Deleted else Values Deleted
      */
     public String delConfingAmendment(TblConfigAmendment configAmnd){////////////////
         logger.debug("delConfingAmendment : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.delConfingAmendment(configAmnd);
         } catch (Exception e) {
             logger.error("delConfingAmendment : "+logUserId+" : "+e);
     }
         logger.debug("delConfingAmendment : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Delete All Amendment/Corrigendum Configuration
      * @return if error than returns Not Deleted else Values Deleted
      */
     public String delAllConfigAmendment(){
         logger.debug("delAllConfigAmendment : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.delAllConfingAmendment();
         } catch (Exception e) {
             logger.error("delAllConfigAmendment : "+logUserId+" : "+e);
     }
         logger.debug("delAllConfigAmendment : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Update Amendment/Corrigendum Configuration
      * @param configAmnd to be updated
      * @return if error than returns Not Updated else Values Updated
      */
     public String updateConfingAmendment(TblConfigAmendment configAmnd){
         logger.debug("updateConfingAmendment : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.updateConfingAmendment(configAmnd);
         } catch (Exception e) {
             logger.error("updateConfingAmendment : "+logUserId+" : "+e);
     }
         logger.debug("updateConfingAmendment : "+logUserId+" Ends ");
         return str;
     }

     /**
      *List of Budget Types
      * @return List of TblBudgetType
      */
     public List<TblBudgetType> getBudgetType(){
         logger.debug("getBudgetType : "+logUserId+" Starts ");
         List<TblBudgetType>  list = null;
         try {
             list = preTenderRulesevice.getBudgetType();
         } catch (Exception e) {
            logger.error("getBudgetType : "+logUserId+" : "+e);
     }
         logger.debug("getBudgetType : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Insert Procurement Configuration
      * @param configProcurement to be inserted
      * @return if error than returns Not Added else Values Value Added
      */
     public String addConfigProcurement(TblConfigProcurement configProcurement) {
         logger.debug("addConfigProcurement : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.addConfigProcurement(configProcurement);
         } catch (Exception e) {
             logger.error("addConfigProcurement : "+logUserId+" : "+e);
     }
         logger.debug("addConfigProcurement : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Get all Procurement Configuration
      * @return List of TblConfigProcurement
      */
     public List<TblConfigProcurement> getConfigProcurement(){
         logger.debug("getConfigProcurement : "+logUserId+" Starts ");
         List<TblConfigProcurement> list = null;
         try {
             list = preTenderRulesevice.getConfigProcurement();
         } catch (Exception e) {
             logger.error("getConfigProcurement : "+logUserId+" : "+e);
     }
         logger.debug("getConfigProcurement : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Get Procurement Configuration based on id
      * @param id to be searched
      * @return List of TblConfigProcurement
      */
     public List<TblConfigProcurement> getConfigProcurement(int id) {
         logger.debug("getConfigProcurement : "+logUserId+" Starts ");
         List<TblConfigProcurement> list = null;
         try {
             list = preTenderRulesevice.getConfigProcurement(id);
         } catch (Exception e) {
             logger.error("getConfigProcurement : "+logUserId+" : "+e);
     }
         logger.debug("getConfigProcurement : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Updates Procurement Configuration
      * @param tblConfigProcurement to be updated
      * @return if error than returns Not Updated else Values Updated
      */
     public String updateConfigProcurement(TblConfigProcurement tblConfigProcurement){
         logger.debug("updateConfigProcurement : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.updateConfigProcurement(tblConfigProcurement);
         } catch (Exception e) {
             logger.error("updateConfigProcurement : "+logUserId+" : "+e);
     }
         logger.debug("updateConfigProcurement : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Deletes Procurement Configuration
      * @param tblConfigProcurement to be deleted
      * @return if error than returns Not Deleted else Values Deleted
      */
     public String delConfigProcurement(TblConfigProcurement tblConfigProcurement){
         logger.debug("delConfigProcurement : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.delConfigProcurement(tblConfigProcurement);
         } catch (Exception e) {
             logger.error("delConfigProcurement : "+logUserId+" : "+e);
     }
         logger.debug("delConfigProcurement : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Delete all Procurement Configuration
      * @return if error than returns Not Deleted else Values Deleted
      */
     public String delAllConfigProcurement(){
         logger.debug("delAllConfigProcurement : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.delAllConfigProcurement();
         } catch (Exception e) {
             logger.error("delAllConfigProcurement : "+logUserId+" : "+e);
     }
         logger.debug("delAllConfigProcurement : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Inserts NOA configuration
      * @param tblConfigNoa to be inserted
      * @return if error than returns Not Added else Values Value Added
      */
     public String addConfigNoa(TblConfigNoa tblConfigNoa){
         logger.debug("addConfigNoa : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.addConfigNoa(tblConfigNoa);
         } catch (Exception e) {
             logger.error("addConfigNoa : "+logUserId+" : "+e);
     }
         logger.debug("addConfigNoa : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Get all NOA configuration
      * @return List of TblConfigNoa
      */
     public List<TblConfigNoa> getAllConfigNoa() {
         logger.debug("getAllConfigNoa : "+logUserId+" Starts ");
         List<TblConfigNoa> list = null;
         try {
             list = preTenderRulesevice.getAllConfigNoa();
         } catch (Exception e) {
             logger.error("getAllConfigNoa : "+logUserId+" : "+e);
     }
         logger.debug("getAllConfigNoa : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Deletes all NOA configuration
      * @return if error than returns Not Deleted else Values Deleted
      */
     public String delAllConfigNoa(){
         logger.debug("delAllConfigNoa : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.delAllConfigNoa();
         } catch (Exception e) {
             logger.error("delAllConfigNoa : "+logUserId+" : "+e);
     }
         logger.debug("delAllConfigNoa : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Get all NOA configuration based on id
      * @param id to be searched
      * @return List of TblConfigNoa
      */
     public List<TblConfigNoa> getConfigNoa(int id){
         logger.debug("getConfigNoa : "+logUserId+" Starts ");
         List<TblConfigNoa> list = null;
         try {
             list=preTenderRulesevice.getConfigNoa(id);
         } catch (Exception e) {
             logger.error("getConfigNoa : "+logUserId+" : "+e);
     }
         logger.debug("getConfigNoa : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Updates NOA configuration
      * @param tblConfigNoa to be updated
      * @return if error than returns Not Updated else Values Updated
      */
     public String updateConfigNoa(TblConfigNoa tblConfigNoa){
        logger.debug("updateConfigNoa : "+logUserId+" Starts ");
        String str = "";
        try {
            str = preTenderRulesevice.updateConfigNoa(tblConfigNoa);
        } catch (Exception e) {
            logger.error("updateConfigNoa : "+logUserId+" : "+e);
    }
        logger.debug("updateConfigNoa : "+logUserId+" Ends ");
        return str;
    }

     /**
      *Deletes NOA configuration
      * @param tblConfigNoa to be deleted
      * @return if error than returns Not Deleted else Values Deleted
      */
     public String delConfigNoa(TblConfigNoa tblConfigNoa){
        logger.debug("delConfigNoa : "+logUserId+" Starts ");
        String str = "";
        try {
            str = preTenderRulesevice.delConfigNoa(tblConfigNoa);
        } catch (Exception e) {
            logger.error("delConfigNoa : "+logUserId+" : "+e);
    }
        logger.debug("delConfigNoa : "+logUserId+" Ends ");
        return str;
    }

    /**
     *Get all Financial Years
     * @return List of TblFinancialYear
     */
    public List<TblFinancialYear> getFinancialYear() {
         logger.debug("getFinancialYear : "+logUserId+" Starts ");
         List<TblFinancialYear> list = null;
         try {
             list = preTenderRulesevice.getFinancialYear();
         } catch (Exception e) {
             logger.error("getFinancialYear : "+logUserId+" : "+e);
    }
         logger.debug("getFinancialYear : "+logUserId+" Ends ");
        return list;
    }

    /**
     *Count of existing FinancialYear in DB
     * @return no of entries in DB
     */
    public long getFinancialYearCount() {
         logger.debug("getFinancialYearCount : "+logUserId+" Starts ");
         long lng =0;
         try {
             lng = preTenderRulesevice.getFinancialPowerByRoleCnt();
         } catch (Exception e) {
             logger.error("getFinancialYearCount : "+logUserId+" : "+e);
     }
         logger.debug("getFinancialYearCount : "+logUserId+" Ends ");
         return lng;
     }

     /**
      *Get Procurement Roles
      * @return List of TblProcurementRole
      */
     public List<TblProcurementRole> getProcureRole(){
         logger.debug("getProcureRole : "+logUserId+" Starts ");
         List<TblProcurementRole> list = null;
         try {
             list = preTenderRulesevice.getProcureRole();
         } catch (Exception e) {
             logger.error("getProcureRole : "+logUserId+" : "+e);
     }
         logger.debug("getProcureRole : "+logUserId+" Ends ");
         return list;
     }

     /**
      *Deletes all Financial Power Based on Role
      * @return if error than returns Not Deleted else Values Deleted
      */
     public String delAllFinancialPowerByRole() {
         logger.debug("delAllFinancialPowerByRole : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.delAllFinancialPowerByRole();
         } catch (Exception e) {
             logger.error("delAllFinancialPowerByRole : "+logUserId+" : "+e);
     }
         logger.debug("delAllFinancialPowerByRole : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Inserts  Financial Power Based on Role
      * @param tblFinancialPowerByRole to be inserted
      * @return if error than returns Not Added else Values Value Added
      */
     public String addTblFinancialPowerByRole(TblFinancialPowerByRole tblFinancialPowerByRole) {
         logger.debug("addTblFinancialPowerByRole : "+logUserId+" Starts ");
         String str = "";
         try {
             str = preTenderRulesevice.addTblFinancialPowerByRole(tblFinancialPowerByRole);
         } catch (Exception e) {
             logger.error("addTblFinancialPowerByRole : "+logUserId+" : "+e);
     }
         logger.debug("addTblFinancialPowerByRole : "+logUserId+" Ends ");
         return str;
     }

     /**
      *Get all Financial Power Based on Role
      * @return List of TblFinancialPowerByRole
      */
     public List<TblFinancialPowerByRole> getAllFinancialPowerByRole() {
          logger.debug("getAllFinancialPowerByRole : "+logUserId+" Starts ");
          List<TblFinancialPowerByRole>  list = null;
          try {
              list = preTenderRulesevice.getAllFinancialPowerByRole();
          } catch (Exception e) {
              logger.error("getAllFinancialPowerByRole : "+logUserId+" : "+e);
    }
          logger.debug("getAllFinancialPowerByRole : "+logUserId+" Ends ");
        return list;
    }


     /**
       *Get Financial Power Based on Role based on id
       * @param id to be searched on
       * @return List of TblFinancialPowerByRole
       */
      public List<TblFinancialPowerByRole> getFinancialPowerByRole(int id) {
          logger.debug("getFinancialPowerByRole : "+logUserId+" Starts ");
          List<TblFinancialPowerByRole> list = null;
          try {
              list = preTenderRulesevice.getFinancialPowerByRole(id);
          } catch (Exception e) {
              logger.error("getFinancialPowerByRole : "+logUserId+" : "+e);
      }
          logger.debug("getFinancialPowerByRole : "+logUserId+" Ends ");
          return list;
      }

      /**
       *Deletes Financial Power Based on Role
       * @param tblFinancialPowerByRole to be deleted
       * @return if error than returns Not Deleted else Values Deleted
       */
      public String delFinancialPowerByRole(TblFinancialPowerByRole tblFinancialPowerByRole) {
          logger.debug("delFinancialPowerByRole : "+logUserId+" Starts ");
          String str = "";
          try {
              str = preTenderRulesevice.delFinancialPowerByRole(tblFinancialPowerByRole);
          } catch (Exception e) {
              logger.error("delFinancialPowerByRole : "+logUserId+" : "+e);
      }
          logger.debug("delFinancialPowerByRole : "+logUserId+" Ends ");
          return str;
      }

      /**
       *Updates Financial Power Based on Role
       * @param tblFinancialPowerByRole to be updated
       * @return if error than returns Not Updated else Values Updated
       */
      public String updateFinancialPowerByRole(TblFinancialPowerByRole tblFinancialPowerByRole) {
          logger.debug("updateFinancialPowerByRole : "+logUserId+" Starts ");
          String str = "";
          try {
              str = preTenderRulesevice.updateFinancialPowerByRole(tblFinancialPowerByRole);
          } catch (Exception e) {
              logger.error("updateFinancialPowerByRole : "+logUserId+" : "+e);
      }
          logger.debug("updateFinancialPowerByRole : "+logUserId+" Ends ");
          return str;
      }
      /**
       *Get Tender Types based on id
       * @param tenderTypeId to be searched on
       * @return Tender Type
       */
      public String getTenderType(int tenderTypeId) {
        logger.debug("getTenderType : " + logUserId + "starts");
        String tenderType = "";
        try {

            tenderType = preTenderRulesevice.getTenderType(tenderTypeId);

        } catch (Exception e) {
            logger.error("getTenderType : " + logUserId + " " + e);

        }
        logger.debug("getTenderType : " + logUserId + "ends");
        return tenderType;
    }

}
