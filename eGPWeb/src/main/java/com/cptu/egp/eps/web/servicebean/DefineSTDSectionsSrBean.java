/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblTemplateSections;
import com.cptu.egp.eps.service.serviceinterface.TemplateSection;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class DefineSTDSectionsSrBean extends HttpServlet {
    TemplateSection templateMaster = (TemplateSection) AppContext.getSpringBean("TemplateSectionService");
    private AuditTrail auditTrail;
    public void setAuditTrail(AuditTrail auditTrail) {
            this.auditTrail  = auditTrail;
            templateMaster.setAuditTrail(auditTrail);
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        final Logger logger = Logger.getLogger(DefineSTDSectionsSrBean.class);
        String logUserId = "0";
        try {
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                templateMaster.setUserId(logUserId);
            }
            logger.debug("DefineSTDSectionsSrBean : " + logUserId + " Starts");
            logger.debug("DefineSTDSectionsSrBean : " + logUserId + " TemplateId:" + request.getParameter("hdTemplateId"));
            logger.debug("DefineSTDSectionsSrBean : " + logUserId + " NoOfSection:" + request.getParameter("hdNoOfSection"));

            int templateId = Integer.parseInt(request.getParameter("hdTemplateId"));
            byte noOfSection = Byte.parseByte(request.getParameter("hdNoOfSection"));
            TblTemplateSections tblTemplateSections[] = new TblTemplateSections[noOfSection];

            for (int i = 0; i < noOfSection; i++) {
                if(i==noOfSection-1){
                    setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                }else{
                    setAuditTrail(null);
                }
                tblTemplateSections[i] = new TblTemplateSections();
                tblTemplateSections[i].setSectionName(request.getParameter("txtSecName" + (i + 1)).trim());
                tblTemplateSections[i].setTemplateId((short) templateId);
                tblTemplateSections[i].setContentType(request.getParameter("selContentType" + (i + 1)));
                tblTemplateSections[i].setIsSubSection("1");
                tblTemplateSections[i].setStatus("active");
                boolean flag = templateMaster.createTemplateSection(tblTemplateSections[i]);
                logger.debug("DefineSTDSectionsSrBean : " + logUserId + " Template Insertion Flag :" + flag + " @ section No:" + (i + 1));
            }
            tblTemplateSections = null;
            logger.debug("DefineSTDSectionsSrBean : " + logUserId + " Ends");
            response.sendRedirect("admin/DefineSTDInDtl.jsp?templateId=" + templateId + "&");
        } catch (Exception ex) {
            logger.error("DefineSTDSectionsSrBean : " + logUserId + " "+ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    //public int getCount
}
