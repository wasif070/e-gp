/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblTemplateSectionForm;
import com.cptu.egp.eps.service.serviceimpl.TemplateSectionFormImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class TemplateFormSrBean {

    private static final Logger LOGGER = Logger.getLogger(TemplateFormSrBean.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";
    private TemplateSectionFormImpl templateSectionFormImpl = (TemplateSectionFormImpl) AppContext.getSpringBean("AddFormService");
    
     public void setLogUserId(String logUserId) {
          this.logUserId = logUserId;
     }

     /**
      * Get Form Details from formId
      * @param formId
      * @return  List of Template Section forms
      */
    public List<TblTemplateSectionForm> getFormDetail(int formId) {
        LOGGER.debug("getFormDetail : " + logUserId + " Starts");
        List<TblTemplateSectionForm> list = null;
        try {
            list = templateSectionFormImpl.getSingleForms(formId);
        } catch (Exception e) {
            LOGGER.error("getFormDetail : " + logUserId + e);
    }
        LOGGER.debug("getFormDetail : " + logUserId + " Ends");
        return list;
    }
}
