/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.view.VwGetAdmin;
import com.cptu.egp.eps.web.servicebean.ManageAdminSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class ManageAdminServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            if (request.getParameter("action").equals("fetchData")) {
                response.setContentType("text/xml;charset=UTF-8");
                ManageAdminSrBean manageAdminSrBean = new ManageAdminSrBean();
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");
                if (sidx.equals("")) {
                    sidx = "departmentName";
                }

                //dcBean.setColName("");
                manageAdminSrBean.setSearch(_search);
                manageAdminSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                System.out.println("offset:" + +offset);

                manageAdminSrBean.setOffset(offset);
                manageAdminSrBean.setSortOrder(sord);
                manageAdminSrBean.setSortCol(sidx);

                PrintWriter out = response.getWriter();
                System.out.println("queryString:" + request.getQueryString());

                List<VwGetAdmin> vwGetAdminList = manageAdminSrBean.getOrgMasterList();
                System.out.println("departmentMasterList size:" + vwGetAdminList.size());
                int totalPages = 0;
                int totalCount = (int) manageAdminSrBean.getAllCountOfDept();
                System.out.println("totalCount : "+totalCount);
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                    } else {
                        totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                    }
                } else {
                    totalPages = 0;
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + vwGetAdminList.size() + "</records>");
                // be sure to put text data in CDATA
                for (int i = 0; i < vwGetAdminList.size(); i++) {
                    out.print("deptname : "+vwGetAdminList.get(i).getId().getDepartmentName());
                    out.print("<row id='" + vwGetAdminList.get(i).getId().getDepartmentName() + "'>");
                    out.print("<cell>" + vwGetAdminList.get(i).getId().getEmailId() + "</cell>");
                    out.print("<cell>" + vwGetAdminList.get(i).getId().getFullName() + "</cell>");
                    out.print("<cell>" + vwGetAdminList.get(i).getId().getMobileNo() + "</cell>");
                    out.print("</row>");
                }
                out.print("</rows>");
            }
        } catch (Exception ex) {
            System.out.println("Exceotion :" + ex);
        } finally {
//            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
