/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblTenderEstCost;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceimpl.TenderEstCost;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author shreyansh
 */
public class TenderEstCostServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            TenderEstCost estcost = (TenderEstCost) AppContext.getSpringBean("TenderEstCost");
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            HttpSession session = request.getSession();
            Object user = session.getAttribute("userId");
            String userId = null;
            if (user == null) {
                userId = "";
            } else {

                userId = user.toString();
            }
           
            int count = Integer.parseInt(request.getParameter("count"));
            int tendid = Integer.parseInt(request.getParameter("tenderid"));


            String proNature = request.getParameter("proNature");
            List<Object[]> list = commonService.getLotDetails(tendid + "");
            int k=0;
            int AppPkgLotId[] = new int[list.size()];
            for (Object[] obj : list) {
                AppPkgLotId[k] = Integer.parseInt(obj[0].toString());
                k++;
            }
            List<TblTenderEstCost> tblTenderEstCosts = new ArrayList<TblTenderEstCost>();
            String action = null;
            for (int i = 0; i < count; i++) {
                TblTenderEstCost tblTenderEstCost = new TblTenderEstCost();
                tblTenderEstCost.setCreatedDt(new java.util.Date());
                MathContext mc = new MathContext(8);
                tblTenderEstCost.setEstCost(new BigDecimal(request.getParameter("taka_"+i)).multiply(new BigDecimal("1000000"),mc));
                tblTenderEstCost.setCreatedBy(Integer.parseInt(userId));
                if("Services".equalsIgnoreCase(proNature)){
                    tblTenderEstCost.setPkgLotId(0);
                }else{
                    tblTenderEstCost.setPkgLotId(AppPkgLotId[i]);
                }
                tblTenderEstCost.setTblTenderMaster(new TblTenderMaster(tendid));
                action = "Add Official estimate cost";
                if(request.getParameter("isedit")!=null && request.getParameter("pckgId_"+i) != null){
                    tblTenderEstCost.setEstCostLotId(Integer.parseInt(request.getParameter("pckgId_"+i)));
                    action = "Edit Official estimate cost";
                }
                tblTenderEstCosts.add(tblTenderEstCost);
                tblTenderEstCost=null;

            }
            estcost.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
            //change by ahsan for fixing double entry issue
            estcost.addEstCost(tblTenderEstCosts ,String.valueOf(tendid),action);
            response.sendRedirect("officer/Notice.jsp?tenderid="+tendid+"&infomsg=yes");
           


        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
