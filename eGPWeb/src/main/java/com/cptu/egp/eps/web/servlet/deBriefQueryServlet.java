/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.model.table.TblTenderDebriefdetailDoc;
import com.cptu.egp.eps.model.table.TblTenderDebriefing;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceimpl.PreTendQueryImpl;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.TenderDebriefingService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;
import javax.servlet.ServletConfig;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author dixit
 */
public class deBriefQueryServlet extends HttpServlet {
    
    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("Debriefing");
    private File destinationDir;


    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }
    /**this servlet handles all the request for debrifing jsp pages
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private TenderDebriefingService tenderDebriefingService = (TenderDebriefingService) AppContext.getSpringBean("TenderDebriefingService");    
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    private PreTendQueryImpl pretendQueryImpl = (PreTendQueryImpl) AppContext.getSpringBean("PreTendQueryImpl");
    private UserRegisterService  registerService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
    private static final Logger LOGGER = Logger.getLogger(deBriefQueryServlet.class);
    private String logUserId = "0";
    private String userTypeId = "";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                tenderDebriefingService.setLogUserId(logUserId);
                registerService.setUserId(logUserId);
                pretendQueryImpl.setLogUserId(logUserId);
            }
        
        // Coad added by Dipal for Audit Trail Log.
         HttpSession session= request.getSession(true);
        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"),  request.getHeader("referer"));
        String idType="tenderId";
        int auditId=0;
        String auditAction=null;
        String moduleName=EgpModule.Evaluation.getName();
        String remarks="";
        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        
        
        
        if(request.getSession().getAttribute("userTypeId")!=null){
            userTypeId = request.getSession().getAttribute("userTypeId").toString();
        }
        LOGGER.debug("processRequest : "+logUserId+" Starts");
        try {
             HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
             String action = request.getParameter("action");
             String queryAction = request.getParameter("queryAction");
             String tenderid ="";
             String refNo ="";
             if(request.getParameter("tenderId")!=null)
                {
                    tenderid = request.getParameter("tenderId");
                }
             if(request.getParameter("refNo")!=null)
                {
                    refNo = request.getParameter("refNo");
                }
             if("addQuery".equalsIgnoreCase(action))
             {
                    File file = null;
                    String docSizeMsg = "";
                    boolean checkret = false;
                    boolean flag = false;
                    boolean bool = false;
                    String documentBrief = "";
                    String fileName = "";
                    long fileSize = 0;
                    String queryString = "";
                    boolean isDocumentUpladed=false;
                    int debriefId = 0;

                    TblTenderDebriefing tblTenderDebriefing = new TblTenderDebriefing();
                    tblTenderDebriefing.setTblTenderMaster(new TblTenderMaster(Integer.parseInt(tenderid)));
                    tblTenderDebriefing.setPostDate(new Date());
                    tblTenderDebriefing.setQuePostedBy(Integer.parseInt(logUserId));
                    tblTenderDebriefing.setAnsweredDt(null);
                    tblTenderDebriefing.setAnsweredBy(0);
                    tblTenderDebriefing.setReplyText("");

                    TblTenderDebriefdetailDoc tblTenderDebriefdetailDoc =null;
                    String pageName = "tenderer/deBriefQuery.jsp";
                    boolean dis = false;
                    try
                    {
                        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                        fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
                        fileItemFactory.setRepository(tmpDir);
                        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
                        List items = uploadHandler.parseRequest(request);
                        Iterator itr = items.iterator();
                        while (itr.hasNext())
                        {
                            FileItem item = (FileItem) itr.next();
                            if (item.isFormField())
                            {
                                if (item.getFieldName().equals("txtQuery"))
                                {
                                    tblTenderDebriefing.setQueryText(handleSpecialChar.handleSpecialChar(item.getString()));
                                }
                                if (item.getFieldName().equals("documentBrief"))
                                {
                                    if (item.getString() != null || item.getString().trim().length() > 0)
                                    {
                                        documentBrief = item.getString();
                                        documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    }
                                }
                            }
                            else
                            {
                               if (item.getName().lastIndexOf("\\") != -1)
                               {
                                    fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                               }
                               else
                               {
                                    fileName = item.getName();
                               }
                               debriefId = tenderDebriefingService.saveTenderDebriefing(tblTenderDebriefing);
                               if(fileName!= null && !fileName.equalsIgnoreCase(""))
                               {
                                   isDocumentUpladed=true;
                                   tblTenderDebriefdetailDoc = new TblTenderDebriefdetailDoc();
                                   tblTenderDebriefdetailDoc.setUploadedBy(Integer.parseInt(logUserId));
                                   tblTenderDebriefdetailDoc.setKeyId(debriefId);
                                   tblTenderDebriefdetailDoc.setDocumentName(fileName);
                                   tblTenderDebriefdetailDoc.setUploadedDate(new Date());
                                   tblTenderDebriefdetailDoc.setProcess("Debriefing");
                                   tblTenderDebriefdetailDoc.setUserTypeId(Integer.parseInt(userTypeId));
                                   String realPath = DESTINATION_DIR_PATH + tenderid;

                                   destinationDir = new File(realPath);
                                   if (!destinationDir.isDirectory())
                                   {
                                       destinationDir.mkdir();
                                   }
                                   realPath = DESTINATION_DIR_PATH + tenderid+"\\"+debriefId;

                                   destinationDir = new File(realPath);
                                   if (!destinationDir.isDirectory())
                                   {
                                       destinationDir.mkdir();
                                   }
                                   docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                                   if (!docSizeMsg.equals("ok"))
                                   {
                                   }
                                   else
                                   {
                                        fileSize = item.getSize();
                                        checkret = checkExnAndSize(fileName, item.getSize(), "common");
                                        if (!checkret)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            file = new File(destinationDir, fileName);
                                            if (file.isFile()) {
                                                flag = true;
                                                break;
                                            }
                                            item.write(file);
                                            FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                                            fileSize = fileEncryptDecryptUtil.fileEncryptUtil(file, (int)fileSize);
                                            tblTenderDebriefdetailDoc.setDocSize(fileSize+"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (FileUploadException ex)
                    {
                       ex.printStackTrace();
                    }
                    catch (Exception ex)
                    {
                       ex.printStackTrace();
                    }
                    if(isDocumentUpladed) 
                    {
                        tblTenderDebriefdetailDoc.setDocDescription(documentBrief);
                        if (!docSizeMsg.equals("ok"))
                        {
                            queryString = "?fq=" + docSizeMsg + "&tenderId="+tenderid;
                        }
                        else
                        {
                            if (flag)
                            {
                                docSizeMsg = "File already Exists";
                                queryString = "?fq=" + docSizeMsg + "&tenderId="+tenderid;
                                pageName = "tenderer/deBriefPostQuery.jsp";
                                response.sendRedirect(pageName + queryString);
                            }
                            else
                            {
                                if (!checkret) {
                                    flag=true;
                                    CheckExtension ext = new CheckExtension();
                                    TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                    queryString = "?fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension()+ "&tenderId="+tenderid;
                                    pageName = "tenderer/deBriefPostQuery.jsp";
                                    response.sendRedirect(pageName + queryString);
                                }
                            }
                        }
                    }
                    if(!flag) 
                    {
                        try
                        {
                            pageName = "tenderer/deBriefQuery.jsp";
                            auditId=Integer.parseInt(request.getParameter("tenderId"));
                            idType="tenderId";
                            auditAction="Tendere Seek Clarification for Debriefing on Tender";
                            bool = tenderDebriefingService.saveDebriefingDocsDetail(tblTenderDebriefdetailDoc);
                            List<SPTenderCommonData> listt = pretendQueryImpl.getDataPreTenderQueryFromSP("getPEOfficerUserIdfromTenderId",tenderid, null);
                            String strTo[] = {listt.get(0).getFieldName5()};
                            String strFrom = commonservice.getEmailId(logUserId);
                            String officeName = commonservice.getConsultantName(logUserId);
                            SendMessageUtil sendMessageUtil = new SendMessageUtil();
                            MailContentUtility mailContentUtility = new MailContentUtility();
                            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                            registerService.contentAdmMsgBox(strTo[0], strFrom, "e-GP: Debriefing on a Tender", msgBoxContentUtility.deBriefPostQuestionContent(tenderid,refNo,listt.get(0).getFieldName3(),officeName));
                            String mailText = mailContentUtility.deBriefPostQuestionContent(tenderid,refNo,listt.get(0).getFieldName3(),officeName);
                            sendMessageUtil.setEmailTo(strTo);
                            sendMessageUtil.setEmailFrom(strFrom);
                            sendMessageUtil.setEmailSub("e-GP: Debriefing on a Tender");
                            sendMessageUtil.setEmailMessage(mailText);
                            sendMessageUtil.sendEmail();
                            strTo =null;
                            sendMessageUtil=null;
                            mailContentUtility=null;
                            msgBoxContentUtility=null;
                            if(debriefId>0 && bool)
                            {
                                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                                queryString = "?tenderId="+tenderid+"&message=success";
                            }else
                            {
                                auditAction="Error in "+auditAction;
                                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                                queryString = "?tenderId="+tenderid+"&message=fail";
                            }
                            response.sendRedirect(pageName+queryString);
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }
             }
//             if("addQuery".equalsIgnoreCase(action))
//             {
//                String query ="";
//                boolean flag = false;
//                if(request.getParameter("txtQuery")!=null)
//                {
//                    query = request.getParameter("txtQuery");
//                }
//                int userid = Integer.parseInt(logUserId);
//                int tenderID = Integer.parseInt(tenderid);
//                Date date = new Date();
//
//                auditId=Integer.parseInt(request.getParameter("tenderId"));
//                idType="tenderId";
//                auditAction="Tendere Seek Clarification for Debriefing on Tender";
//
//                TblTenderDebriefing tblTenderDebriefing = new TblTenderDebriefing();
//
//                tblTenderDebriefing.setTblTenderMaster(new TblTenderMaster(tenderID));
//                tblTenderDebriefing.setPostDate(date);
//                tblTenderDebriefing.setQuePostedBy(userid);
//                tblTenderDebriefing.setQueryText(handleSpecialChar.handleSpecialChar(query));
//                tblTenderDebriefing.setAnsweredDt(null);
//                tblTenderDebriefing.setAnsweredBy(0);
//                tblTenderDebriefing.setReplyText("");
//                flag = tenderDebriefingService.saveTenderDebriefing(tblTenderDebriefing);
//
//                /* sending mail to pe logic starts */
//                List<SPTenderCommonData> listt = pretendQueryImpl.getDataPreTenderQueryFromSP("getPEOfficerUserIdfromTenderId",tenderid, null);
//                String strTo[] = {listt.get(0).getFieldName5()};
//                String strFrom = commonservice.getEmailId(logUserId);
//                String officeName = commonservice.getConsultantName(logUserId);
//                SendMessageUtil sendMessageUtil = new SendMessageUtil();
//                MailContentUtility mailContentUtility = new MailContentUtility();
//                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
//                registerService.contentAdmMsgBox(strTo[0], strFrom, "e-GP: Debriefing on a Tender", msgBoxContentUtility.deBriefPostQuestionContent(tenderid,refNo,listt.get(0).getFieldName3(),officeName));
//                String mailText = mailContentUtility.deBriefPostQuestionContent(tenderid,refNo,listt.get(0).getFieldName3(),officeName);
//                sendMessageUtil.setEmailTo(strTo);
//                sendMessageUtil.setEmailFrom(strFrom);
//                sendMessageUtil.setEmailSub("e-GP: Debriefing on a Tender");
//                sendMessageUtil.setEmailMessage(mailText);
//                sendMessageUtil.sendEmail();
//                strTo =null;
//                sendMessageUtil=null;
//                mailContentUtility=null;
//                msgBoxContentUtility=null;
//                /* ends */
//
//                if(flag)
//                {
//                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
//                    response.sendRedirect("tenderer/deBriefQuery.jsp?tenderId="+tenderid+"&message=success");
//                }else
//                {
//                    auditAction="Error in "+auditAction;
//                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
//                    response.sendRedirect("tenderer/deBriefQuery.jsp?tenderId="+tenderid+"&message=fail");
//                }
//             }
             else if("addreply".equalsIgnoreCase(action))
             {
                    File file = null;
                    String docSizeMsg = "";
                    boolean checkret = false;
                    boolean flag = false;
                    String documentBrief = "";
                    String fileName = "";
                    long fileSize = 0;
                    boolean bool = false;
                    String queryString = "";
                    String replytext ="";
                    boolean isDocumentUpladed=false;
                    String debriefid ="";
                    int i=0;
                    if (request.getParameter("debriefid") != null && !"null".equalsIgnoreCase(request.getParameter("debriefid"))) {
                        debriefid = request.getParameter("debriefid");
                    }
                    TblTenderDebriefdetailDoc tblTenderDebriefdetailDoc =null;
                    String pageName = "officer/deBriefQueryView.jsp";
                    boolean dis = false;
                    try
                    {
                        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                        fileItemFactory.setSizeThreshold(4 * 1024 * 1024); //1 MB
                        fileItemFactory.setRepository(tmpDir);
                        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
                        List items = uploadHandler.parseRequest(request);
                        Iterator itr = items.iterator();
                        while (itr.hasNext())
                        {
                            FileItem item = (FileItem) itr.next();
                            if (item.isFormField())
                            {
                                if (item.getFieldName().equals("txtReply"))
                                {
                                    replytext = handleSpecialChar.handleSpecialChar(item.getString());
                                }
                                if (item.getFieldName().equals("documentBrief"))
                                {
                                    if (item.getString() != null || item.getString().trim().length() > 0)
                                    {
                                        documentBrief = item.getString();
                                        documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    }
                                }
                            }
                            else
                            {
                               if (item.getName().lastIndexOf("\\") != -1)
                               {
                                    fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                               }
                               else
                               {
                                    fileName = item.getName();
                               }
                               i = tenderDebriefingService.updateDeriefData(Integer.parseInt(debriefid),Integer.parseInt(logUserId),handleSpecialChar.handleSpecialChar(replytext));
                               if(fileName!= null && !fileName.equalsIgnoreCase(""))
                               {
                                   isDocumentUpladed=true;
                                   tblTenderDebriefdetailDoc = new TblTenderDebriefdetailDoc();
                                   tblTenderDebriefdetailDoc.setUploadedBy(Integer.parseInt(logUserId));
                                   tblTenderDebriefdetailDoc.setKeyId(Integer.parseInt(debriefid));
                                   tblTenderDebriefdetailDoc.setDocumentName(fileName);
                                   tblTenderDebriefdetailDoc.setUploadedDate(new Date());
                                   tblTenderDebriefdetailDoc.setProcess("Debriefing");
                                   tblTenderDebriefdetailDoc.setUserTypeId(Integer.parseInt(userTypeId));
                                   String realPath = DESTINATION_DIR_PATH + tenderid;
                                   destinationDir = new File(realPath);
                                   if (!destinationDir.isDirectory())
                                   {
                                       destinationDir.mkdir();
                                   }
                                   realPath = DESTINATION_DIR_PATH + tenderid+"\\"+debriefid;
                                   destinationDir = new File(realPath);
                                   if (!destinationDir.isDirectory())
                                   {
                                       destinationDir.mkdir();
                                   }
                                   docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                                   if (!docSizeMsg.equals("ok"))
                                   {
                                   }
                                   else
                                   {
                                        fileSize = item.getSize();
                                        checkret = checkExnAndSize(fileName, item.getSize(), "common");
                                        if (!checkret)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            file = new File(destinationDir, fileName);
                                            if (file.isFile()) {
                                                flag = true;
                                                break;
                                            }
                                            item.write(file);
                                            FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                                            fileSize = fileEncryptDecryptUtil.fileEncryptUtil(file, (int)fileSize);
                                            tblTenderDebriefdetailDoc.setDocSize(fileSize+"");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (FileUploadException ex)
                    {
                       ex.printStackTrace();
                    }
                    catch (Exception ex)
                    {
                       ex.printStackTrace();
                    }
                    if(isDocumentUpladed)
                    {
                        tblTenderDebriefdetailDoc.setDocDescription(documentBrief);
                        if (!docSizeMsg.equals("ok"))
                        {
                            queryString = "?fq=" + docSizeMsg + "&tenderId="+tenderid;
                        }
                        else
                        {
                            if (flag)
                            {
                                docSizeMsg = "File already Exists";
                                queryString = "?fq=" + docSizeMsg + "&tenderId="+tenderid;
                                pageName = "officer/deBriefQueryReply.jsp";
                                response.sendRedirect(pageName + queryString);
                            }
                            else
                            {
                                if (!checkret) {
                                    flag=true;
                                    CheckExtension ext = new CheckExtension();
                                    TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                                    queryString = "?fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension()+ "&tenderId="+tenderid;
                                    pageName = "officer/deBriefQueryReply.jsp";
                                    response.sendRedirect(pageName + queryString);
                                }
                            }
                        }
                    }
                    if(!flag)
                    {
                        try
                        {
                            pageName = "officer/deBriefQueryView.jsp";
                            auditId=Integer.parseInt(request.getParameter("tenderId"));
                            idType="tenderId";
                            auditAction="Reply given by PE for Debriefing on Tender";
                            bool = tenderDebriefingService.saveDebriefingDocsDetail(tblTenderDebriefdetailDoc);
                            List<TblTenderDebriefing> list =tenderDebriefingService.getDebriefData(Integer.parseInt(debriefid));
                            String strTo[] = {commonservice.getEmailId(Integer.toString(list.get(0).getQuePostedBy()))};
                            String strFrom = commonservice.getEmailId(logUserId);
                            String officeName = commonservice.getConsultantName(logUserId);
                            SendMessageUtil sendMessageUtil = new SendMessageUtil();
                            MailContentUtility mailContentUtility = new MailContentUtility();
                            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                            registerService.contentAdmMsgBox(strTo[0], strFrom, "e-GP: Debriefing on a Tender", msgBoxContentUtility.deBriefPostReplyContent(tenderid,refNo,officeName));
                            String mailText = mailContentUtility.deBriefPostReplyContent(tenderid,refNo,officeName);
                            sendMessageUtil.setEmailTo(strTo);
                            sendMessageUtil.setEmailFrom(strFrom);
                            sendMessageUtil.setEmailSub("e-GP: Debriefing on a Tender");
                            sendMessageUtil.setEmailMessage(mailText);
                            sendMessageUtil.sendEmail();
                            strTo =null;
                            sendMessageUtil=null;
                            mailContentUtility=null;
                            msgBoxContentUtility=null;
                            if(bool && i>0)
                            {
                                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                                queryString = "?tenderId="+tenderid+"&message=success";
                            }else
                            {
                                auditAction="Error in "+auditAction;
                                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                                queryString = "?tenderId="+tenderid+"&message=fail";
                            }
                            response.sendRedirect(pageName+queryString);
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }
             }
//             else if("addreply".equalsIgnoreCase(action))
//             {
//                    String debriefid ="";
//                    String replytext ="";
//                    int i=0;
//                    if (request.getParameter("debriefid") != null && !"null".equalsIgnoreCase(request.getParameter("debriefid"))) {
//                        debriefid = request.getParameter("debriefid");
//                    }
//                    if(request.getParameter("txtReply")!=null)
//                    {
//                        replytext = request.getParameter("txtReply");
//                    }
//                    int userid = Integer.parseInt(logUserId);
//
//                    auditId=Integer.parseInt(request.getParameter("tenderId"));
//                    idType="tenderId";
//                    auditAction="Reply given by PE for Debriefing on Tender";
//
//                    i = tenderDebriefingService.updateDeriefData(Integer.parseInt(debriefid),userid,handleSpecialChar.handleSpecialChar(replytext));
//
//                    /* sending mail to tenderer logic starts */
//                    List<TblTenderDebriefing> list =tenderDebriefingService.getDebriefData(Integer.parseInt(debriefid));
//                    String strTo[] = {commonservice.getEmailId(Integer.toString(list.get(0).getQuePostedBy()))};
//                    String strFrom = commonservice.getEmailId(logUserId);
//                    String officeName = commonservice.getConsultantName(logUserId);
//                    SendMessageUtil sendMessageUtil = new SendMessageUtil();
//                    MailContentUtility mailContentUtility = new MailContentUtility();
//                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
//                    registerService.contentAdmMsgBox(strTo[0], strFrom, "e-GP: Debriefing on a Tender", msgBoxContentUtility.deBriefPostReplyContent(tenderid,refNo,officeName));
//                    String mailText = mailContentUtility.deBriefPostReplyContent(tenderid,refNo,officeName);
//                    sendMessageUtil.setEmailTo(strTo);
//                    sendMessageUtil.setEmailFrom(strFrom);
//                    sendMessageUtil.setEmailSub("e-GP: Debriefing on a Tender");
//                    sendMessageUtil.setEmailMessage(mailText);
//                    sendMessageUtil.sendEmail();
//                    strTo =null;
//                    sendMessageUtil=null;
//                    mailContentUtility=null;
//                    msgBoxContentUtility=null;
//                    /* ends */
//
//                    if(i>0)
//                    {
//                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
//                        response.sendRedirect("officer/deBriefQueryView.jsp?tenderId="+tenderid+"&message=success");
//                    }
//                    else
//                    {
//                        auditAction="Error in "+auditAction;
//                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
//                        response.sendRedirect("officer/deBriefQueryView.jsp?tenderId="+tenderid+"&message=fail");
//                    }
//             }
             else if("getQuestionData".equalsIgnoreCase(action))
             {
                    StringBuffer strQueryData = new StringBuffer();
                    strQueryData.append("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                    strQueryData.append("<tr>");
                    strQueryData.append("<th width='4%' class='t-align-center'>Sl.  No.</th>");
                    if("replyQuery".equalsIgnoreCase(queryAction))
                    {
                        strQueryData.append("<th class='t-align-left' width='20%'>Company Name</th>");
                    }
                    strQueryData.append("<th class='t-align-left' width='60%'>Query</th>");
                    strQueryData.append(" <th class='t-align-center' width='8%'>Status</th>");
                    strQueryData.append("<th class='t-align-center' width='8%'>Action</th>");
                    strQueryData.append("</tr>");
                    List<TblTenderDebriefing> list =null;
                    String colspan = "";
                    if("3".equalsIgnoreCase(userTypeId))
                    {
                        colspan = "colspan='5'";
                        list = tenderDebriefingService.getDebriefQuestionDataForPE(Integer.parseInt(tenderid));
                    }
                    else
                    {
                        colspan = "colspan='4'";
                        list = tenderDebriefingService.getDebriefQuestionData(Integer.parseInt(tenderid),Integer.parseInt(logUserId));
                    }
                    
                    if(!list.isEmpty())
                    {
                        for(int i=0; i<list.size(); i++)
                        {
                            strQueryData.append("<tr>");
                            strQueryData.append("<td class='t-align-center' width='4%'>  " + (i+1) + "</td>");
                            if("postQuery".equalsIgnoreCase(queryAction))
                            {
                                strQueryData.append("<td class='t-align-left' width='60%' nowrap>" + list.get(i).getQueryText() + "</td>");
                                String status ="Pending";
                                if(list.get(i).getReplyText()!=null && !"".equalsIgnoreCase(list.get(i).getReplyText()))
                                {
                                    status ="Replied";
                                }
                                strQueryData.append("<td class='t-align-center' width='8%'>" +status+ "</td>");
                                String viewlink ="-";
                                //if(list.get(i).getReplyText()!=null && !"".equalsIgnoreCase(list.get(i).getReplyText()))
                                //{
                                    viewlink ="<a href='"+request.getContextPath()+"/resources/common/deBriefView.jsp?debriefId=" + list.get(i).getDebriefId() + "&tenderId=" + tenderid +"' target='_blank'>View</a>";
                                //}
                                strQueryData.append("<td class='t-align-center' width='8%'>"+viewlink+"</td>");
                            }
                            else if("replyQuery".equalsIgnoreCase(queryAction))
                            {
                                String strCompConsultantName  = commonservice.getConsultantName(Integer.toString(list.get(i).getQuePostedBy()));
                                if(strCompConsultantName!=null && !"".equalsIgnoreCase(strCompConsultantName))
                                {
                                    strQueryData.append("<td class='t-align-left' width='20%'>" + strCompConsultantName+ "</td>");
                                }
                                else
                                {
                                    strQueryData.append("<td class='t-align-left' width='20%'>"+" - "+"</td>");
                                }
//                                List<Object[]> companyName = tenderDebriefingService.getCompanyName(list.get(i).getQuePostedBy());
//                                if(!companyName.isEmpty())
//                                {
//                                    strQueryData.append("<td class='t-align-left' width='20%'>" + companyName.get(0)+ "</td>");
//                                }else{
//                                    strQueryData.append("<td class='t-align-left' width='20%'>"+" - "+"</td>");
//                                }

                                strQueryData.append("<td class='t-align-left' width='60%' nowrap>" + list.get(i).getQueryText() + "</td>");
                                String status ="Pending";
                                if(list.get(i).getReplyText()!=null && !"".equalsIgnoreCase(list.get(i).getReplyText()))
                                {
                                    status ="Replied";
                                }
                                strQueryData.append("<td class='t-align-center' width='8%'>" +status+ "</td>");
                                //String viewlink ="";
                                StringBuffer viewlink = new StringBuffer();
                                if("".equalsIgnoreCase(list.get(i).getReplyText()))
                                {
                                    viewlink.append("<a href='../officer/deBriefQueryReply.jsp?debriefId=" + list.get(i).getDebriefId() + "&tenderId=" + tenderid + "'>Reply</a>");
                                }
                                else
                                {
                                    viewlink.append("<a href='"+request.getContextPath()+"/resources/common/deBriefView.jsp?debriefId=" + list.get(i).getDebriefId() + "&tenderId=" + tenderid +"' target='_blank'>View</a>");

                                }                                //if(list.get(i).getReplyText()!=null && !"".equalsIgnoreCase(list.get(i).getReplyText()))
                                //{
                                    //viewlink ="<a href='../officer/deBriefQueryReply.jsp?debriefId=" + list.get(i).getDebriefId() + "&tenderId=" + tenderid + "'>Reply</a>&nbsp;|&nbsp;<a href='../officer/deBriefQueryView.jsp?debriefId=" + list.get(i).getDebriefId() + "&tenderId=" + tenderid +"'>View</a>";
                                //}
                                strQueryData.append("<td class='t-align-center' width='8%'>"+viewlink+"</td>");
                            }
                            
                            strQueryData.append("</tr>");
                        }
                    }
                    else
                    {
                        strQueryData.append("<tr>");
                        strQueryData.append("<td "+colspan+" class='t-align-center' style='font-weight: bold; color: red;'>No records found.</td>");
                        strQueryData.append("</tr>");
                    }
                    strQueryData.append("</table>");
                    out.print(strQueryData.toString());
            }
            else if("downloadDocument".equalsIgnoreCase(action))
            {
                File file = null;
                file = new File(DESTINATION_DIR_PATH + tenderid+"\\"+request.getParameter("debriefId")+ "\\"+request.getParameter("docName"));
                InputStream fis = new FileInputStream(file);
                byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                int offset = 0;
                int numRead = 0;
                while ((offset < buf.length)
                        && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                    offset += numRead;
                }
                fis.close();
                FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName") + "\"");
                ServletOutputStream outputStream = response.getOutputStream();
                outputStream.write(buf);
                outputStream.flush();
                outputStream.close();
            }
            else if("removeDocument".equalsIgnoreCase(action))
            {
                File file = null;
                String fileName = "";
                String DetailDocId = "";
                String debriefId = "";
                String queryString = "";
                String pageName = "resources/common/deBriefView.jsp";
                boolean checkret = false;
                if(request.getParameter("DetailDocId")!=null)
                {
                    DetailDocId = request.getParameter("DetailDocId");
                }
                if(request.getParameter("debriefId")!=null)
                {
                    debriefId = request.getParameter("debriefId");
                }
                file = new File(DESTINATION_DIR_PATH + tenderid+"\\"+debriefId+ "\\"+request.getParameter("docName"));
                fileName = file.toString();
                checkret = deleteFile(fileName);
                if (checkret) {
                    tenderDebriefingService.deleteDebriefDocsDetails(Integer.parseInt(DetailDocId));
                    queryString = "?tenderId=" + tenderid + "&debriefId=" + debriefId + "&fq=Removed";
                    response.sendRedirect(pageName + queryString);
                }
            }
        } catch(Exception e) {
            System.out.println("-----------------------------------------"+e);
            LOGGER.error("exception : "+logUserId+" : "+e);           
        }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
    }    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public boolean checkExnAndSize(String extn, long size, String userType) {
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
            dsize = configurationMaster.getFileSize();
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        return chextn;
    }
/*For doc size msg*/
    public String docSizeMsg(int userId)
    {

        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        return userRegisterService.docSizeCheck(userId);
    }
/*For deleting file*/
    public boolean deleteFile(String filePath)
    {

        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }
    }
}
