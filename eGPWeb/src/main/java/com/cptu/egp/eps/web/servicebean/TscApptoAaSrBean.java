/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblTscstatus;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.TblTscstatusService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class TscApptoAaSrBean {

    private String logUserId = "0";
    private static final Logger LOGGER = Logger.getLogger(TscApptoAaSrBean.class);
    private final TblTscstatusService tblTscstatusService = (TblTscstatusService) AppContext.getSpringBean("TblTscstatusService");
    private final TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

    public void setLogUserId(String logUserId) {
        tblTscstatusService.setUserId(logUserId);
        tenderCS.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    public int getTscStatusId(int tenderId) {
        return tblTscstatusService.tscStatusId(tenderId);
    }

    public String getStatus(int tenderId) {
        return tblTscstatusService.getStatus(tenderId);
    }

    public TblTscstatus getDataforUpdate(int tenderId) {
        return tblTscstatusService.getDataforUpdate(tenderId).get(0);
    }

    public boolean insertData(int tenderId, int userId) {
        LOGGER.debug("insertData : " + logUserId + "starts");
        boolean flag = false;
        int aaId = 0;
        try {
        for (SPTenderCommonData sptcd : tenderCS.returndata("getTenderConfigData", tenderId + "", null)) {
            aaId = Integer.parseInt(sptcd.getFieldName7());
            if (sptcd != null) {
                sptcd = null;
            }
        }
        TblTscstatus tblTscstatus = new TblTscstatus();
        tblTscstatus.setSendToAa("Yes");
        tblTscstatus.setStatus("Pending");
        tblTscstatus.setPesentDt(new Date());
        tblTscstatus.setAasentDt(null);
        tblTscstatus.setPeUserId(userId);
        tblTscstatus.setAauserId(aaId);
        tblTscstatus.setIsTeccp("");
        tblTscstatus.setTenderId(tenderId);
        flag = tblTscstatusService.addData(tblTscstatus);
        if (flag) {
            String aaMail = "";
            for (SPTenderCommonData sptcd : tenderCS.returndata("getEmailIdfromUserId", aaId + "", null)) {
                aaMail = sptcd.getFieldName1();
                if (sptcd != null) {
                    sptcd = null;
                }
            }
            flag = sendMail(aaMail, tenderId);
        }
        } catch (Exception e) {
            LOGGER.error("insertData : " + e);
        }


        LOGGER.debug("insertData : " + logUserId + "ends");
        return flag;
    }

    public boolean updateData(int tenderId, String status) {
        LOGGER.debug("updateData : " + logUserId + "starts");
        boolean flag = false;
        TblTscstatus tblTscstatus = new TblTscstatus();
        try {
        tblTscstatus = getDataforUpdate(tenderId);
        tblTscstatus.setAasentDt(new Date());
        tblTscstatus.setIsTeccp("Yes");
        tblTscstatus.setStatus(status);
        flag = tblTscstatusService.updateData(tblTscstatus);
        if (flag) {
            String cpMail = "";
            for (SPTenderCommonData sptcd : tenderCS.returndata("GetTecCpEmail", tenderId + "", null)) {
                cpMail = sptcd.getFieldName1();
                if (sptcd != null) {
                    sptcd = null;
                }
            }
            flag = sendMail(cpMail, tenderId);
        }
        } catch (Exception e) {
            LOGGER.error("updateData : " + e);
        }


        LOGGER.debug("updateData : " + logUserId + "ends");
        return flag;
    }

    private boolean sendMail(String emaiId, int tenderId) {
        LOGGER.debug("sendMail : " + logUserId + "starts");
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {emaiId};
            MailContentUtility mailContentUtility = new MailContentUtility();

            List list = mailContentUtility.evalAppToAa(tenderId + "");
            String mailSub = list.get(0).toString();
            String mailText = list.get(1).toString();
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            mailSent = true;
        } catch (Exception ex) {
            LOGGER.error("sendMail : " + ex);
        }
        LOGGER.debug("sendMail : " + logUserId + "ends");
        return mailSent;
    }
}
