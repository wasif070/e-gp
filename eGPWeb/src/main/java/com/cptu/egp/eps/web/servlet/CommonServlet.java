/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.model.table.TblCompanyMaster;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.service.serviceimpl.AdminRegistrationServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceimpl.MessageProcessingServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.AdminRegistrationService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.ProgrammeMasterService;
import com.cptu.egp.eps.service.serviceinterface.TenderBidService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.servicebean.ConfigMasterSrBean;
import com.cptu.egp.eps.web.servicebean.EvalClariPostBidderSrBean;
import com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean;
import com.cptu.egp.eps.web.servicebean.TempCompanyDocumentsSrBean;
import com.cptu.egp.eps.web.servicebean.TendererMasterSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class CommonServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private final CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
    MessageProcessingServiceImpl messageprocessService = (MessageProcessingServiceImpl) AppContext.getSpringBean("messageprocessService");
    private static final Logger LOGGER = Logger.getLogger(CommonServlet.class);
    String logUserId = "0";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();}

        String flag = request.getParameter("forcrosscheck");
        String mailid = request.getParameter("mailId");
        String functionName = request.getParameter("funName");

        try {

            if(functionName.equalsIgnoreCase("check")) {
                String msg = checkMail(mailid);
                out.print(msg);
                out.flush();
            }  else if (functionName.equals("verifyMail")) {
                String msg = verifyMail(mailid, flag);
                out.print(msg);
                out.flush();
            } else if (functionName.equals("verifyMulitipleMails")) {

                String msg = "";
                if (mailid.contains(";")) {
                    msg = "Mail ids Should be comma saperated";
                } else {
                    msg = verifyMulitipleMails(mailid);
                }
                out.print(msg);
                out.flush();

            }  else if (functionName.equals("MsgBoxDelete")) {
                String msgId = request.getParameter("messageid");
                boolean bul =  messageprocessService.deleteTrashMessages(Integer.parseInt(msgId));
                //boolean bul =  messageprocessService.deleteTrashMessages(msgId);
                /*StringBuffer sb = new StringBuffer();
                response.setContentType("text/xml");
                sb.append("<?xml version='1.0' encoding='utf-8'?>");

                if(bul)
                {
                    sb.append("<cell>Message Deleted successfully</cell>");
                   // response.sendRedirect("resources/common/InboxMessage.jsp?inbox=Trash&folderid=0&msg=succ");
                }else{
                    sb.append("<cell>Message Deletion Failed</cell>");
                    //response.sendRedirect("resources/common/InboxMessage.jsp?inbox=Trash&folderid=0&msg=fail");
                }
                 out.print(sb.toString());*/
                 if(bul)
                {
                    response.sendRedirect("resources/common/InboxMessage.jsp?inbox=Trash&folderid=0&msg=succ");
                }else{
                    response.sendRedirect("resources/common/InboxMessage.jsp?inbox=Trash&folderid=0&msg=fail");
                }
            }
            if (functionName.equals("companySearch")) {
                String msg = "";
                try {
                    msg = findCompanyName(request.getParameter("searchBy"), request.getParameter("searchVal"));
                } catch (Exception ex) {

                    LOGGER.error("Error : "+ex);
                }
                out.print(msg);
                out.flush();
            }
            if (functionName.equals("verifyNationalId")) {
                out.print(verifyNationalId(request.getParameter("nationalId")));
                out.flush();
            }
            if ("verifyMobileNo".equalsIgnoreCase(functionName)) {
                out.print(verifyMobileNo(request.getParameter("mobileNo")));
                out.flush();
            }

            if (functionName.equals("verifyUserEmployeeId")) {
                out.print(verifyUserEmployeeId(request.getParameter("userEmployeeId")));
                out.flush();
            }
            if (functionName.equals("verifyDeptName")) {
                String depName = request.getParameter("depName").trim();
                String depType = request.getParameter("depType");
                if (!depName.trim().equals("")) {
                    String msg = verifyDeptName(depName, depType);
                    out.print(msg);
                    out.flush();
                }
            }
            if (functionName.equals("verifyContractNo")) {
                String contractNo = request.getParameter("contractNo").trim();
                if (!contractNo.equals("")) {
                    String msg = verifyContractNo(contractNo);
                    out.print(msg);
                    out.flush();
                }
            }
            if (functionName.equals("verifyOfficeName")) {
                String officeName = request.getParameter("officeName").trim();
                int deptId = Integer.parseInt(request.getParameter("deptId"));
                if (!officeName.trim().equals("")) {
                    String msg = verifyOfficeName(officeName, deptId);
                    out.print(msg);
                    out.flush();
                }
            }
            if (functionName.equals("verifyOffice")) {
                int officeId = Integer.parseInt(request.getParameter("officeId"));
                String msg = verifyOffice(officeId);
                out.print(msg);
                out.flush();
                
            }
            if (functionName.equals("verifyDebar")) {
                int userId = Integer.parseInt(request.getParameter("userId"));
                String msg = verifyDebar(userId);
                out.print(msg);
                out.flush();
                
            }
            if (functionName.equals("compNoChk")) {
                try {
                    out.print(compRegNoCheck(request.getParameter("compRegNo")));
                } catch (Exception ex) {

                    LOGGER.error("Error : "+ex);
                }
                out.flush();
            }
            //companyNameCheck
            if (functionName.equals("compNameChk")) {
                try {
                    out.print(companyNameCheck(request.getParameter("companyName")));
                } catch (Exception ex) {

                    LOGGER.error("Error : "+ex);
                }
                out.flush();
            }

            if (functionName.equals("tmpCmpInsert")) {
                try {
                    out.print(tmpCmpInsert(request.getParameter("objectId"), Integer.parseInt(request.getSession().getAttribute("userId").toString())));//
                } catch (Exception ex) {
                    LOGGER.error("Error : "+ex);
                }
                out.flush();
            }

            if (functionName.equals("checkpCode")) {
                String msg = "";
                String pCode = request.getParameter("pCode").trim();
                String action = request.getParameter("action");
                int pId = Integer.parseInt(request.getParameter("editId"));
                if (action.equals("edit")) {
                    msg = editProgCode(pId, pCode);
                } else {
                    msg = getProgCode(pCode);
                }
                out.print(msg);
                out.flush();
            }

            if (functionName.equals("checkpName")) {
                String msg = "";
                String pName = request.getParameter("pName").trim();
                String action = request.getParameter("action");
                int pId = Integer.parseInt(request.getParameter("editId"));
                if (action.equals("edit")) {
                     msg = editProgName(pId, pName);
                } else {
                     msg = getProgName(pName);
                }
                out.print(msg);
                out.flush();
            }

            if (functionName.equals("checkpOwner")) {
                String msg = "";
                String pOwner = request.getParameter("pOwner").trim();
                String action = request.getParameter("action");
                int pId = Integer.parseInt(request.getParameter("editId"));
                if (action.equals("edit")) {
                    msg = "OK";//editProgOwner(pId, pOwner);
                } else {
                    msg = "OK";//getProgOwner(pOwner);
                }
                out.print(msg);
                out.flush();
            }

            if (functionName.equals("verifyMobile")) {
                try {
                    out.print(checkUniqueMobile(request.getParameter("mobileNo")));
                } catch (Exception ex) {
                    LOGGER.error("Error Mobile no : "+ex);
                }
                out.flush();
            }
            if (functionName.equals("verifyNationId")) {
                try {
                    out.print("");
                    //out.print(checkUniqueNationalId(request.getParameter("nationalId")));
                } catch (Exception ex) {
                    LOGGER.error("Error National Id : "+ex);
                }
                out.flush();
            }
            if (functionName.equals("updateNextScr")) {
                try {
                    out.print(updateNextScreenDetail(Integer.parseInt(request.getSession().getAttribute("userId").toString()), request.getParameter("pageName")));
                } catch (Exception ex) {
                    LOGGER.error("Error UpdateNextScreen : "+ex);
                }
                out.flush();
            }
            if (functionName.equals("addUser")) {
                try {
                    out.print(addUser(Integer.parseInt(request.getSession().getAttribute("userId").toString())));

                    LOGGER.error("UserID : "+request.getSession().getAttribute("userId").toString());
                } catch (Exception ex) {
                    LOGGER.error("Error AddUser : "+ex);
                }
                out.flush();
            }
            if (functionName.equals("captchaValid")) {
                try {
                    String privKey = "6Lfw4r0SAAAAAN_Pr3YGPA39kRIyNsuMCDJIpUsS";
                    StringBuffer reqURL = request.getRequestURL();
                    if (reqURL.toString().substring(reqURL.toString().indexOf("//") + 2, reqURL.toString().lastIndexOf("/")).equals("www.eprocure.gov.bd")) {
                        privKey = "6Lc0ssISAAAAAJf6jPcfSBElxYSRk1LB46MBsWUk";
                    }
                    out.print(captchaValid(request.getParameter("challenge"), request.getParameter("userInput"), request.getRemoteAddr(), privKey));

                } catch (Exception ex) {
                    LOGGER.error("Error UpdateNextScreen : "+ex);
                }
                out.flush();
            }
            if (functionName.equals("countryCode")) {
                try {
                    out.print(countryCode(request.getParameter("countryId")));

                } catch (Exception ex) {
                    LOGGER.error("Error UpdateNextScreen : "+ex);
                }
                out.flush();
            }
            if (functionName.equals("officeList")) {
                try {
                    out.print(getOfficeList(request.getParameter("deptId")));

                } catch (Exception ex) {
                    LOGGER.error("Error UpdateNextScreen : "+ex);
                }
                out.flush();
            }
            if (functionName.equals("AllOfficeList")) {
                try {
                    out.print(getAllOfficeList(request.getParameter("deptId")));

                } catch (Exception ex) {
                    LOGGER.error("Error UpdateNextScreen : "+ex);
                }
                out.flush();
            }
            if (functionName.equals("officeListByStateId")) {
                try {
                    out.print(getOfficeListByStateId(request.getParameter("stateId"), request.getParameter("deptId")));

                } catch (Exception ex) {
                    LOGGER.error("Error UpdateNextScreen : "+ex);
                }
                out.flush();
            }

            if (functionName.equals("configDetail")) {
                String msg = "";
                String userType = request.getParameter("userType").trim();
                     msg = getConfigDetail(userType);
                out.print(msg);
                out.flush();
            }
            if (functionName.equals("bankChecker")) {
                String msg = "";
                String sBankDevelHeadId = request.getParameter("officeId").trim();
                String role = request.getParameter("role").trim();
                msg = getBankChecker(sBankDevelHeadId, role);
                out.print(msg);
                out.flush();
            }
             if (functionName.equals("updateQue")) {
                String msg = "";
                String currVal = request.getParameter("updateQuest");
                String queId = request.getParameter("queId");
                msg = getUpdateStatus(currVal, queId);
                out.print(msg);
                out.flush();
            }
             if (functionName.equals("cancelPkgRevision")) {
                String msg = "";
                String packageId = request.getParameter("packageId");
                String appId = request.getParameter("appId");
               TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
               if(tenderCommonService.cancelPackageRevision(packageId))
                    response.sendRedirect("officer/APPDashboard.jsp?appID="+appId+"&cancelPkgRevision=succ");
               else
                   response.sendRedirect("officer/APPDashboard.jsp?appID="+appId+"&cancelPkgRevision=fail");

            }
            if (functionName.equals("checkOrgType")) {
               int deptId = Integer.parseInt(request.getParameter("deptId"));
                //if (!officeName.trim().equals("")) {
                    String msg = checkOrgType( deptId);
                    out.print(msg);
                    out.flush();
                //}

            }
            
            if (functionName.equals("updateMandatoryFormGoods")) {
              List<SPCommonSearchData> list = null;
              String tenderid = request.getParameter("tenderid");
              String userid = request.getParameter("userid");
              String formNameGoods = request.getParameter("formNameGoods");
              String lotNo = request.getParameter("lotNo");
              list = cmnSearchService.searchData("updateMandatoryFormGoods", formNameGoods, tenderid, userid, lotNo, null, null, null, null, null);
              System.out.print(list.get(0).getFieldName1());
              out.print(list.get(0).getFieldName1());
              out.flush();  
            }
            if (functionName.equals("otpOptionSet")) {
                String jspOtpOption = request.getParameter("jspOtpOption");
                HttpSession session = request.getSession();
                session.setAttribute("sessionOtpOption",jspOtpOption);
            }
            if (functionName.equals("updateOTP")) {
              HttpSession session = request.getSession();
              String selectedOtpOption = session.getAttribute("sessionOtpOption").toString();
              String sessionEmail = session.getAttribute("emailId").toString();
              String status = "" ;  
              String otp = "" ;
              List<SPCommonSearchData> list = null;
              SendMessageUtil smu = new SendMessageUtil();
              String tenderid = request.getParameter("tenderid");
              String userid = request.getParameter("userid"); 
              String event = request.getParameter("event");
              list = cmnSearchService.searchData("findOTP", null, tenderid, userid, null, null, null, null, null, null);
              String OTP = list.get(0).getFieldName1();
              if (OTP == null || Integer.parseInt(list.get(0).getFieldName2()) > 5)
              {               
              otp = smu.getOTP().toString();

              list = cmnSearchService.searchData("updateOTP", otp, tenderid, userid, null, null, null, null, null, null);
              status = list.get(0).getFieldName1();
              System.out.print(status);
              
              //sending otp by sms
                if(status.equals("Success")){
                    
                    if(selectedOtpOption.equals("email"))
                    {
                     
                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                        String mailTo[] = {sessionEmail};
                        MailContentUtility mailContentUtility = new MailContentUtility();

                        List otpList = mailContentUtility.getOtpMailContent(event, tenderid, otp);
                        String mailSub = otpList.get(0).toString();
                        String mailText = otpList.get(1).toString();
                        sendMessageUtil.setEmailTo(mailTo);
                        sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
                        sendMessageUtil.setEmailSub(mailSub);
                        sendMessageUtil.setEmailMessage(mailText);
                        sendMessageUtil.sendEmail();
                    }
                    else
                    {
                        //request.getSession().setAttribute("OTP", otp);
                        TenderBidService tenderBidService = (TenderBidService)AppContext.getSpringBean("TenderBidService");
                        String data[]= tenderBidService.getTendererMobileNEmail(userid);
                        smu.setSmsNo(data[1]+data[2]);

                        String smsBody = event.equalsIgnoreCase("withdrawal")? "You have requested BID WITHDRAWAL / MODIFICATION in e-GP System for Tender ID: "+tenderid+". Your OTP is "+otp+".":"You have requested BID SUBMISSION in e-GP System for Tender ID: "+tenderid+". Your OTP is "+otp+".";
                        smu.setSmsBody(smsBody); 
                        smu.sendSMS();
                    }
                    
                }
              }
              else{
                  otp = OTP;
              }
             out.print(status);
              out.flush();  
            }

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String verifyDeptName(String deptName, String deptType) {
        String msg = "";
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");

        commonService.setUserId(logUserId);
        msg=commonService.verifyDeptName(deptName,deptType);
        return msg;
    }

    private String verifyOfficeName(String offficeName, int deptId) {
        String msg = "";
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");

        commonService.setUserId(logUserId);
        msg=commonService.verifyOfficeName(offficeName,deptId);
        return msg;
    }
    
    private String verifyOffice(int officeId) {
        String msg = "";
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");

        commonService.setUserId(logUserId);
        if(officeId!=0)
        {
            msg=commonService.verifyOffice(officeId);
        }
        return msg;
    }
    
    private String verifyDebar(int userId) {
        String msg = "";
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");

        commonService.setUserId(logUserId);
        msg=commonService.verifyDebar(userId);
        return msg;
    }
    

    private String verifyMail(String mailId, String flag) {
        String msg = "";
        if (!mailId.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9\\-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            return msg = "Please enter valid e-mail ID";
        }
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");

        commonService.setUserId(logUserId);
        msg=commonService.verifyMail(mailId);
        return msg;
    }

    private String checkMail(String mailId) {
        String msg = "";
        if (!mailId.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            return msg = "Please enter valid e-mail ID";
        }
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        msg = commonService.checkForMail(mailId);
        return msg;
    }

    private String verifyNationalId(String nationalId) {
        return "<font color=\"white\">OK</font>";
//        StringBuilder msg= new StringBuilder();
//        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
//        msg.append(commonService.verifyNationalId(nationalId));
//        return msg.toString();
    }

    private String verifyMobileNo(String mobileNo) {
        StringBuilder msg = new StringBuilder();
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        commonService.setUserId(logUserId);
        msg.append(commonService.verifyMobileNo(mobileNo));
        return msg.toString();
    }

    private String verifyUserEmployeeId(String userEmployeeId) {
        StringBuilder msg = new StringBuilder();
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        commonService.setUserId(logUserId);
        msg.append(commonService.verifyUserEmployeeId(userEmployeeId));
        return msg.toString();
    }

    private String editProgCode(int pId, String pCode) {


     String msg = "";
     if(pCode.isEmpty()){
         return "Please enter Programme Information Code";}
     ProgrammeMasterService prgMasterService = (ProgrammeMasterService)AppContext.getSpringBean("programmeMasterService");
     prgMasterService.setUserId(logUserId);
     msg = prgMasterService.editCheckCode(pId, pCode);
     return msg;
    }

    private String getProgCode(String pCode) {


     String msg = "";
     if(pCode.isEmpty()){
         return "Please enter Programme Information Code";}
     ProgrammeMasterService prgMasterService = (ProgrammeMasterService)AppContext.getSpringBean("programmeMasterService");
     prgMasterService.setUserId(logUserId);
     msg = prgMasterService.checkForCode(pCode);
     return msg;


    }

    private String getProgName(String pName) {
         String msg = "";
        if (pName.isEmpty()) {
             return "Please enter Programme Information Name";
        }
        ProgrammeMasterService prgMasterService = (ProgrammeMasterService) AppContext.getSpringBean("programmeMasterService");
         msg = prgMasterService.checkForName(pName);
         return msg;
    }

    private String editProgName(int pId, String pName) {


     String msg = "";
     if(pName.isEmpty()){
         return "Please enter Programme Information Name";}
     ProgrammeMasterService prgMasterService = (ProgrammeMasterService)AppContext.getSpringBean("programmeMasterService");
     prgMasterService.setUserId(logUserId);
     msg = prgMasterService.editCheckName(pId, pName);
     return msg;

    }


    private String getProgOwner(String pOwner){
         String msg = "";
         if(pOwner.isEmpty()){
             return "Please enter Programme Owner";}
         ProgrammeMasterService prgMasterService = (ProgrammeMasterService)AppContext.getSpringBean("programmeMasterService");
         prgMasterService.setUserId(logUserId);
         msg = prgMasterService.checkForOwner(pOwner);
         return msg;
    }

    private String editProgOwner(int pId, String pOwner) {


     String msg = "";
     if(pOwner.isEmpty()){
         return "Please enter Programme Owner";}
     ProgrammeMasterService prgMasterService = (ProgrammeMasterService)AppContext.getSpringBean("programmeMasterService");
     prgMasterService.setUserId(logUserId);
     msg = prgMasterService.editCheckOwner(pId, pOwner);
     return msg;
    }

    private String findCompanyName(String searchBy, String searchVal) throws Exception {
        StringBuilder sb = new StringBuilder();
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        userRegisterService.setUserId(logUserId);
        List<String> message = new ArrayList<String>();
        message = userRegisterService.searchCompanyName(searchBy, searchVal);
        for (String ans : message) {
            StringTokenizer stringTokenizer = new StringTokenizer(ans, ",");
            int i = stringTokenizer.countTokens();
            if (i == 2) {
                sb.append(ans.substring(0, ans.length() - 2));
            } else {
                String temp1 = "";
                String temp2 = "";
                while (stringTokenizer.hasMoreElements()) {
                    if (i == 1) {
                        temp2 = (String) stringTokenizer.nextElement();
                    } else {
                        temp1 += (String) stringTokenizer.nextElement() + " ,";
                    }
                    i--;
                 }
                sb.append("<option value='").append(temp2).append("'>").append(temp1.substring(0, temp1.length() - 1)).append("</option>");
            }
        }
        return sb.toString();
    }

    private String compRegNoCheck(String compRegNo) throws Exception {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

        userRegisterService.setUserId(logUserId);
        if(compRegNo.equals("")){
            return "";
        }
        List<TblCompanyMaster> companyMasters = new ArrayList<TblCompanyMaster>();
        companyMasters = userRegisterService.companyRegNoCheck(compRegNo, 0);
        StringBuilder sb = new StringBuilder();
        if (companyMasters.isEmpty()) {
            sb.append("OK");
        } else {
            sb.append("Company Registration No. already exist. Please contact your company admin for your further registration process.,").append(companyMasters.get(0).getCompanyId()).append("@" + companyMasters.get(0).getCompanyName());
        }
        return sb.toString();
    }
    
    private String companyNameCheck(String companyName) throws Exception {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

        userRegisterService.setUserId(logUserId);
        if(companyName.equals("")){
            return "";
        }
        List<TblCompanyMaster> companyMasters = new ArrayList<TblCompanyMaster>();
        companyMasters = userRegisterService.companyNameCheck(companyName, 0);
        StringBuilder sb = new StringBuilder();
        if (companyMasters.isEmpty()) {
            sb.append("OK");
        } else {
            sb.append("Company Name already exist. ");
        }
        return sb.toString();
    }

    private String tmpCmpInsert(String cmpRegNo, int userId) {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

        userRegisterService.setUserId(logUserId);
        return String.valueOf(userRegisterService.insertTmpCmpMaster(cmpRegNo,userId));
    }

    private String checkUniqueMobile(String mobile) throws Exception {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

        userRegisterService.setUserId(logUserId);
        long count = userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.mobileNo='"+mobile+"'");
        if(count!=0){
            return "Mobile no. already exist";
        } else {
            return "";
        }
    }

    private String checkUniqueNationalId(String nationalId) throws Exception {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

        userRegisterService.setUserId(logUserId);
        long count = userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.nationalIdNo='"+nationalId+"'");
        if(count!=0){
            return "National Id./Passport No./Driving License No. already exist";
        } else {
            return "";
        }
    }

    private String updateNextScreenDetail(int userId, String pageName) throws Exception {
        TendererMasterSrBean srBean = new TendererMasterSrBean();
        srBean.setLogUserId(logUserId);
        srBean.updateNextScreenDetail(userId, pageName);
        srBean = null;
        return "";
    }

    private String addUser(int userId) throws Exception {
        TempCompanyDocumentsSrBean srBean = new TempCompanyDocumentsSrBean();
        srBean.setLogUserId(logUserId);
        CommonMsgChk msgChk = srBean.addUser(userId, "FinalSubmission","","");
        //srBean=null;
        return msgChk.getMsg();
    }

    private String captchaValid(String challenge, String userInput, String remoteAddr, String privKey) {
        ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
        reCaptcha.setPrivateKey(privKey);
        ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challenge, userInput);
        if (reCaptchaResponse.isValid()) {
            return "OK";
        } else {
            return "Verification code is invalid";
        }
    }

      private String verifyMulitipleMails(String mailIds) {
        String response = null;
        String[] mids = mailIds.split(",");
        if (mids.length > 0) {
            for (int i = 0; i < mids.length; i++) {
             response =  verifyComposeMail(mids[i]);
                if ("Mail-ID NOT Valid".equals(response)) {
                    break;
            }
            }
        } else {
           response = verifyComposeMail(mids[0]);
        }
        return response;
    }

   

    private String verifyComposeMail(String comMail) {

        String msg = "";
        // [A-Za-z0-9._]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}
        if (!comMail.matches("^[a-zA-Z][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$")) {
            return msg = "Mail-ID NOT Valid";
        }
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");

        commonService.setUserId(logUserId);
        msg=commonService.verifyComposeMail(comMail);
        return msg;
    }

    private String countryCode(String countryId) {
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        commonService.setUserId(logUserId);
        return commonService.countryCode(countryId, true);
    }

    private String getOfficeList(String deptId) throws Exception {
        AdminRegistrationService adminRegService = (AdminRegistrationService) AppContext.getSpringBean("AdminRegistrationService");
        adminRegService.setLogUserId(logUserId);
        String str = "";
        List<Object[]> list = adminRegService.getAdminOfficeList(Integer.parseInt(deptId));
        if (list.size() > 0) {
            for (Object[] obj : list) {
                str += "<option value='" + (Integer) obj[0] + "'>" + (String) obj[1] + "</option>";
            }
        } else {
            str = "<option value='0'> No Office Found.</option>";
        }
        return str;
    }
 private String getAllOfficeList(String deptId) throws Exception {
        AdminRegistrationService adminRegService = (AdminRegistrationService) AppContext.getSpringBean("AdminRegistrationService");
        adminRegService.setLogUserId(logUserId);
        String str = "";
        List<Object[]> list = adminRegService.getOfficeList(Integer.parseInt(deptId));
        if (list.size() > 0) {
            for (Object[] obj : list) {
                str += "<option value='" + (Integer) obj[0] + "'>" + (String) obj[1] + "</option>";
            }
        } else {
            str = "<option value='0'> No Office Found.</option>";
        }
        return str;
    }
    public String getOfficeListByStateId(String stateId, String deptid) {
        AdminRegistrationServiceImpl adminRegService = (AdminRegistrationServiceImpl) AppContext.getSpringBean("AdminRegistrationService");
        adminRegService.setLogUserId(logUserId);
        String str = "";
        short statId = Short.parseShort(stateId);

         List<TblOfficeMaster> tblOfficeMasters = null;
         LOGGER.debug("stateid == "+stateId+"deptid = "+deptid);
         str += "<option value=''>-- Select Office --</option>";
        tblOfficeMasters = adminRegService.getOfficesByStateId(statId,Integer.parseInt(deptid));
        if(tblOfficeMasters.size()>0){
            Iterator it = tblOfficeMasters.iterator();
            while (it.hasNext()) {
                TblOfficeMaster master = (TblOfficeMaster) it.next();
                try {
                    if (!adminRegService.chkOfficeAdminCreated(master.getOfficeId())) {
                        str += "<option value='" + master.getOfficeId() + "'>" + master.getOfficeName() + "</option>";
                    }
                } catch (Exception e) {
                }
            }
        }
        return str;
    }

    public String getConfigDetail(String userType) {
        ConfigMasterSrBean configMasterSrBean = new ConfigMasterSrBean();
        configMasterSrBean.setLogUserId(logUserId);
        List<TblConfigurationMaster> list = configMasterSrBean.getConfigMasterDetails(userType);

        String detail = list.get(0).getAllowedExtension()+"@$"+list.get(0).getFileSize()+"@$"+list.get(0).getTotalSize()+"@$"+list.get(0).getConfigId();
        LOGGER.debug("detail"+detail);
        return detail;
    }

    public String getBankChecker(String sBankDevelHeadId, String role) {
        String msg = "";
        ScBankDevpartnerSrBean scBankDevpartnerSrBean = new ScBankDevpartnerSrBean();

        scBankDevpartnerSrBean.setLogUserId(logUserId);
        msg = scBankDevpartnerSrBean.getBankChecker(sBankDevelHeadId,role);
        return msg;
    }

    private String getUpdateStatus(String question, String queId) {
         String msg = "";
         EvalClariPostBidderSrBean evalClariPostBidderSrBean = new EvalClariPostBidderSrBean();
         evalClariPostBidderSrBean.setLogUserId(logUserId);
        try {
            boolean flag = evalClariPostBidderSrBean.updateEvalFormQue(question, queId);
            if (flag) {
                return "OK";
            } else {
                return "Error";
            }
        } catch (Exception ex) {
            LOGGER.error("detail"+ex);
             return "Error";
        }


     }

    private String verifyContractNo(String contractNo) {
        String msg = "";
        NOAServiceImpl noaServiceImpl = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");

        noaServiceImpl.setLogUserId(logUserId);
        msg=noaServiceImpl.verifyContractNo(contractNo);
        return msg;
    }

    private String checkOrgType(int deptId) {
        String msg = "";
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        msg=commonService.checkOrgType(deptId).toString();
        return msg;
    }
}