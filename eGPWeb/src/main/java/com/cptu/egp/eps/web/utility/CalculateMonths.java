/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.utility;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;

public class CalculateMonths {

    public BigDecimal getMonths(int year, int month, BigDecimal noOfDays) {
        BigDecimal noOfMonths = BigDecimal.ZERO;
        noOfMonths = noOfDays.divide(new BigDecimal(getMonthDays(month, year)), 2, RoundingMode.HALF_UP);
        return noOfMonths;
    }

    public BigDecimal getMonthsBetweenTwoDates(Date startDt, Date endDt) {
        BigDecimal noOfMonths = BigDecimal.ZERO;
        Calendar startDtCl = Calendar.getInstance();
        Calendar endDtCl = Calendar.getInstance();
        int startYr = 0, endYr = 0, startMth = 0, endMth = 0, startDate = 0, endDate = 0, monthCnt;
        try {
            startDtCl.setTime(startDt);
            endDtCl.setTime(endDt);

            startYr = startDtCl.get(Calendar.YEAR);
            endYr = endDtCl.get(Calendar.YEAR);

            startMth = startDtCl.get(Calendar.MONTH);
            endMth = endDtCl.get(Calendar.MONTH);

            startDate = startDtCl.get(Calendar.DAY_OF_MONTH);
            endDate = endDtCl.get(Calendar.DAY_OF_MONTH);

            int yearDiff = endYr - startYr;
            int monthDiff, dateDiff;

            switch (yearDiff) {
                case 0:
                    monthDiff = endMth - startMth;
                    switch (monthDiff) {
                        case 0:
                            dateDiff = (endDate - startDate) + 1;
                            noOfMonths = noOfMonths.add(getMonths(startYr, startMth, new BigDecimal(dateDiff))).setScale(2);
                            break;
                        case 1:
                            noOfMonths = noOfMonths.add(getMonths(startYr, startMth, new BigDecimal(getMonthDays(startMth, startYr) - startDate + 1))).setScale(2);
                            noOfMonths = noOfMonths.add(getMonths(startYr, endMth, new BigDecimal(endDate))).setScale(2);
                            break;
                        default:
                            noOfMonths = noOfMonths.add(getMonths(startYr, startMth, new BigDecimal(getMonthDays(startMth, startYr) - startDate + 1))).setScale(2);
                            noOfMonths = noOfMonths.add(getMonths(startYr, endMth, new BigDecimal(endDate))).setScale(2);
                            noOfMonths = noOfMonths.add(new BigDecimal(monthDiff - 1)).setScale(2);
                            break;
                    }
                    break;
                case 1:
                    monthDiff = endMth - startMth;
                    noOfMonths = noOfMonths.add(getMonths(startYr, startMth, new BigDecimal(getMonthDays(startMth, startYr) - startDate + 1))).setScale(2);
                    noOfMonths = noOfMonths.add(getMonths(startYr, endMth, new BigDecimal(endDate))).setScale(2);
                    if (startMth > endMth) {
                     noOfMonths =   noOfMonths.add(new BigDecimal(((endMth + 12) - startMth) - 1));
                    } else {
                      noOfMonths =  noOfMonths.add(new BigDecimal((endMth - startMth) - 1 + 12) );
                    }
                    break;
                default:
                    monthDiff = endMth - startMth;
                    noOfMonths = noOfMonths.add(getMonths(startYr, startMth, new BigDecimal(getMonthDays(startMth, startYr) - startDate + 1))).setScale(2);
                    noOfMonths = noOfMonths.add(getMonths(startYr, endMth, new BigDecimal(endDate))).setScale(2);
                    if (startMth > endMth) {
                     noOfMonths =   noOfMonths.add(new BigDecimal(((endMth + (12 * yearDiff)) - startMth) - 1));
                    } else {
                      noOfMonths =  noOfMonths.add(new BigDecimal((endMth - startMth) - 1 + (12 * yearDiff)) );
                    }
                    break;
            }
        } catch (Exception e) {
            System.out.println("Exception in getMonths " + e.toString());
        }
        return noOfMonths;
    }

    public int getMonthDays(int month, int year) {
        month = month + 1;
        int days = 0;
        if (year % 4 == 0) {
            if (month == 2) {
                days = 29;
            } else {
                days = 28;
            }
        }
        switch (month) {
            case 1:
                days = 31;
                break;
            case 2:
                if (year % 4 == 0) {
                    days = 29;
                } else {
                    days = 28;
                }
                break;
            case 3:
                days = 31;
                break;
            case 4:
                days = 30;
                break;
            case 5:
                days = 31;
                break;
            case 6:
                days = 30;
                break;
            case 7:
                days = 31;
                break;
            case 8:
                days = 31;
                break;
            case 9:
                days = 30;
                break;
            case 10:
                days = 31;
                break;
            case 11:
                days = 30;
                break;
            case 12:
                days = 31;
                break;

        }
        return days;
    }

 }
