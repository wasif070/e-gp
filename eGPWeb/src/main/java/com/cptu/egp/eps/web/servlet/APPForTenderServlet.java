/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppData;
import com.cptu.egp.eps.service.serviceinterface.APPService;
import com.cptu.egp.eps.web.servicebean.APPSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class APPForTenderServlet extends HttpServlet {

    private final APPService appService = (APPService) AppContext.getSpringBean("APPService");
   
    private static final Logger LOGGER = Logger.getLogger(APPForTenderServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";

    /**
     * This servlet is use to check bussiness rule configure at the time of Tender Creation.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        APPSrBean aPPSrBean  = new APPSrBean();
        
        try {
            HttpSession session = request.getSession();
            if(session.getAttribute("userId")!=null){
                logUserId = session.getAttribute("userId").toString();
                appService.setLogUserId(logUserId);
            }
            
            int i=0;
            String packageId = request.getParameter("packageId");
            String AppPackage = request.getParameter("appPck");
            String action = request.getParameter("action");
            
            if(action!=null && "checkBusRule".equalsIgnoreCase(action)){
                boolean busflag = false;    
                boolean  procflag = false;
                
                if(packageId!=null && !"".equalsIgnoreCase(packageId)){
                    busflag = aPPSrBean.isBusRuleConfig(Integer.parseInt(packageId));
                }
                if(!busflag){
                    out.print("Tender payment business rules not yet configured");
                }else{
                    procflag = aPPSrBean.isprocMethodRuleConfig(Integer.parseInt(packageId));
                    if(!procflag){
                        out.print("Procurement Method business rules not yet configured");
                    }
                }
            }
            else
            {
            List<CommonAppData> packageDetails = appService.getAPPDetailsBySP(AppPackage, packageId, "");
            if(packageDetails.isEmpty()){
                out.print("<tr>");
                out.print("<td colspan='3' class=\"t-align-center\">No approved packages found.</td>");
                out.print("</tr>");
                out.print("<tr style='display: none;'>");
                out.print("<td colspan='3' class=\"t-align-left\"><input type='hidden' name='counter' value='" + i + "' id='counter'/></td>");
                out.print("</tr>");
            }else{
            for (; i < packageDetails.size(); i++) {
                out.print("<tr>");
                out.print("<td class=\"t-align-center\"><input type='radio' value='" + packageDetails.get(i).getFieldName1() + "' name='pckradio' /></td>");
                out.print("<td class=\"t-align-left\">" + packageDetails.get(i).getFieldName2() + "</td>");
                out.print("<td class=\"t-align-left\">" + packageDetails.get(i).getFieldName3() + "</td>");
                out.print("</tr>");
            }
                out.print("<tr style='display: none;'>");
                out.print("<td colspan='3' class=\"t-align-center\"><input type='hidden' name='counter' value='" + i + "' id='counter'/></td>");
                out.print("</tr>");
        }
            }
        }catch(Exception ex){
            LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
        }finally {
            out.close();
        }
        LOGGER.debug("processRequest  : "+logUserId+ LOGGEREND);
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
