/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblTenderCells;
import com.cptu.egp.eps.model.table.TblTenderCoriGrandSumDetail;
import com.cptu.egp.eps.model.table.TblTenderForms;
import com.cptu.egp.eps.model.table.TblTenderGrandSum;
import com.cptu.egp.eps.model.table.TblTenderGrandSumDetail;
import com.cptu.egp.eps.service.serviceimpl.GrandSummaryService;
import com.cptu.egp.eps.service.serviceimpl.TenderCoriGrandSumDetailServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.TenderFormService;
import com.cptu.egp.eps.service.serviceimpl.TenderTableService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author yagnesh
 */
public class GrandSummarySrBean {
    String logUserId = "0";
    TenderFormService tenderFormService = (TenderFormService)  AppContext.getSpringBean("TenderAddFormService");
    TenderTableService tenderTableService = (TenderTableService)  AppContext.getSpringBean("TenderAddFormTableService");
    GrandSummaryService grandSummaryService = (GrandSummaryService)  AppContext.getSpringBean("GrandSummaryService");
    TenderCoriGrandSumDetailServiceImpl tenderCoriGrandSumDetailService = (TenderCoriGrandSumDetailServiceImpl) AppContext.getSpringBean("TenderCoriGrandSumDetailServiceImpl");  ;
    final  Logger logger = Logger.getLogger(GrandSummarySrBean.class);
    AuditTrail auditTrail;
    
    public void setLogUserId(String logUserId) {
        tenderFormService.setLogUserId(logUserId);
        tenderTableService.setUserId(logUserId);
        grandSummaryService.setLogUserId(logUserId);
    }
    /**
     * Set audit trail object for audit trail purpose
     * @param auditTrail
     */
    public void setAuditTrail(AuditTrail auditTrail) {
         this.auditTrail = auditTrail;
     }

    /**
     * Get price bid form list for given section id
     * @param sectionId
     * @return list of price bid form
     */
    public List<TblTenderForms> getTenderPriceBidForm(int sectionId) {
        logger.debug("getTenderPriceBidForm  Starts:");
        List<TblTenderForms> list = new ArrayList<TblTenderForms>();
        try {
                list = tenderFormService.getPriceBidForms(sectionId);
        } catch (Exception e) {
            logger.error("getTenderPriceBidForm  :"+e);
        }
        logger.debug("getTenderPriceBidForm  Ends:");
        return list;
    }

    /**
     * Get price bid form list for given section id and lot id
     * @param sectionId
     * @param pckLotId
     * @return price bid form list
     */
    public List<TblTenderForms> getTenderPriceBidForm(int sectionId,int pckLotId) {
        logger.debug("getTenderPriceBidForm  Starts:");
        List<TblTenderForms> list = new ArrayList<TblTenderForms>();
        try {
                list = tenderFormService.getPriceBidForms(sectionId,pckLotId);
        } catch (Exception e) {
            logger.error("getTenderPriceBidForm  :"+e);
    }
        logger.debug("getTenderPriceBidForm  Ends:");
        return list;
    }

    /**
     * Get grand total column for given form id
     * @param formId
     * @return grand total column detail
     */
    public List<TblTenderCells> getColumnsForGS(int formId){
        logger.debug("getColumnsForGS  Starts:");
        List<TblTenderCells> list = new ArrayList<TblTenderCells>();
        try {
            list = tenderTableService.getColumnsForGS(formId);
        } catch (Exception e) {
            logger.error("getColumnsForGS  :"+e);
    }
        logger.debug("getColumnsForGS  Ends:");
        return list;
    }

    /**
     * Add grand summary details
     * @param gs Grand summary master information
     * @param lstGsDtl Grand summary detail information
     * @param delFlag true if delete/insert perform else insert operation perform
     * @return no of records affected from operation
     */
    public int insertGrandSummary(TblTenderGrandSum gs, List<TblTenderGrandSumDetail> lstGsDtl, int delFlag){
        logger.debug("insertGrandSummary  Starts:");
        int count = 0;
        try {
                count = grandSummaryService.insertDataForGrandSummary(gs, lstGsDtl, delFlag);
        } catch (Exception e) {
            logger.error("insertGrandSummary  :"+e);
    }
        logger.debug("insertGrandSummary  Ends:");
        return count;
    }

    /**
     * check if grand summary created or not
     * @param tenderId
     * @param porlId
     * @return 0 if not created else return non 0 value
     */
    public int isGrandSummaryCreated(int tenderId, int porlId){
        logger.debug("isGrandSummaryCreated  Starts:");
        if(porlId == -1)
        {
            porlId = 0;
        }
        int count = 0;
        try {
                count = grandSummaryService.isGrandSummaryCreated(tenderId, porlId);
        } catch (Exception e) {
            logger.error("isGrandSummaryCreated  :"+e);
    }
        logger.debug("isGrandSummaryCreated  Ends:");
        return count;
    }

    /**
     * Get Grand summary Name
     * @param tenderGSId
     * @return Grand Summary Name
     */
    public String getGrandSummaryName(int tenderGSId){
        logger.debug("getGrandSummaryName  Starts:");
        String val = "";
        try {
                val = grandSummaryService.getGrandSummaryName(tenderGSId);
        } catch (Exception e) {
            logger.error("getGrandSummaryName  :"+e);
    }
        logger.debug("getGrandSummaryName  Ends:");
        return val;
    }

    /**
     * Get Grand Summary Form id 
     * @param tenderGSId
     * @param getFormId if true then it will return tenderformid Map else return CellId Map
     * @return tenderformid / cellid
     */
    public Map<Integer, Integer> getGSFormsDtl(int tenderGSId, boolean getFormId) {
        logger.debug("getGSFormsDtl  Starts:");
        Map<Integer, Integer> map = null;
        try {
                map  = grandSummaryService.getGSFormsDtl(tenderGSId, getFormId);
        } catch (Exception e) {
            logger.error("getGSFormsDtl  :"+e);
    }
        logger.debug("getGSFormsDtl  Ends:");
        return map;
    }

    /**
     * Get Grand Summary Form id and table id 
     * @param tenderGSId
     * @return map having formid, table id
     */
     public Map<Integer, Integer> getGSFormsTblDtl(int tenderGSId) {
        logger.debug("getGSFormsDtl  Starts:");
        Map<Integer, Integer> map = null;
        try {
                map  = grandSummaryService.getGSFormsTblDtl(tenderGSId);
        } catch (Exception e) {
            logger.error("getGSFormsDtl  :"+e);
    }
        logger.debug("getGSFormsDtl  Ends:");
        return map;
    }

     /**
      * @param tenderGSId
      * @param corriId
      * @return
      */
      public boolean checkCoriExistInCoriDetailTbl(String tenderGSId,String corriId)
    {
         logger.debug("checkCoriExistInCoriDetailTbl  Starts:");
       return tenderCoriGrandSumDetailService.checkCoriExistInCoriDetailTbl(tenderGSId, corriId);
    }

      /**
       * This method is use for get Grand summary Forms Table and cell details of given corrigendum.
       * @param tenderGSId
       * @param corriId
       * @return map having values as  formid, table id
       */
      public Map<Integer, Integer> getCorriGSFormsDtl(String tenderGSId,String corriId) {
        logger.debug("getGSFormsDtl  Starts:");
        Map<Integer, Integer> map = null;
        try {
                map  = tenderCoriGrandSumDetailService.getCorriGSFormsDtl(tenderGSId,corriId);
        } catch (Exception e) {
            logger.error("getGSFormsDtl  :"+e);
    }
        logger.debug("getGSFormsDtl  Ends:");
        return map;
    }

      /**
       * This method is use for get Grand summary Form list of given corrigendum.
       * @param tenderGSId
       * @param corriId
       * @return Map having values as tableid, cellid
       */
     public Map<Integer, Integer> getCorriGSFormsTblDtl(String tenderGSId,String corriId) {
        logger.debug("getGSFormsDtl  Starts:");
        Map<Integer, Integer> map = null;
        try {
                map  = tenderCoriGrandSumDetailService.getCorriGSFormsTblDtl(tenderGSId,corriId);
        } catch (Exception e) {
            logger.error("getGSFormsDtl  :"+e);
    }
        logger.debug("getGSFormsDtl  Ends:");
        return map;
    }

     /**
      * Insert for Corrigendum Grand Summary
      * @param lstGsDtl List of Corrigendum Grand SUmmary details
      * @param gsId Grand SUmmary Id
      * @param corriId  Corrigendum Id
      * @return 0 if success else 1
      */
     public int insertCorriGrandSummary(List<TblTenderCoriGrandSumDetail> lstGsDtl,int gsId,String corriId)
     {
        logger.debug("insertCorriGrandSummary  Starts:");
                        
        int count = 0;
        try {
                count = tenderCoriGrandSumDetailService.insertDataForGrandSummary(lstGsDtl, gsId,corriId);
        } catch (Exception e) {
            logger.error("insertCorriGrandSummary  :"+e);
    }
        logger.debug("insertCorriGrandSummaryO  Ends:");
        return count;
    }

     /**
      * Insert for Corrigendum Grand Summary
      * @param gs Grand SUm Detail object
      * @param lstGsDtl List of Corrigendum Grand SUmmary Object
      * @param corriId Corrigendum id
      * @return 0 if success else 1
      */
     public int insertCorriGrandSummary(TblTenderGrandSum gs, List<TblTenderCoriGrandSumDetail> lstGsDtl,String corriId)
     {
        logger.debug("insertCorriGrandSummary  Starts:");
        // System.out.println("********************* insert corri sum");                                 
        int count = 0;
        try {
                count = tenderCoriGrandSumDetailService.insertCorriGrandSummary(gs, lstGsDtl, corriId);
        } catch (Exception e) {
            logger.error("insertCorriGrandSummary  :"+e);
    }
        logger.debug("insertCorriGrandSummaryO  Ends:");
        return count;
    }
     
}
