/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;
import com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails;
import com.cptu.egp.eps.dao.storedprocedure.EvalCommonSearchData;
import com.cptu.egp.eps.service.serviceinterface.EvalCommonSearchService;
import com.cptu.egp.eps.service.serviceinterface.TenderService;
import com.cptu.egp.eps.web.utility.AppContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Istiak (Dohatec) - 21.Apr.15
 */
public class EvalCommonServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        try {

            String funName = request.getParameter("funName");

            if(funName.equalsIgnoreCase("PreviousAllTenders")){
                String userId = request.getParameter("userId").trim();
                String tenderId = request.getParameter("tenderId").trim();
                String pkgLotId = request.getParameter("pkgLotId").trim();
                String size = request.getParameter("size").trim();
                String pageNo = request.getParameter("pageNo").trim();
                String flag = request.getParameter("flag").trim();  // flag = s : Service || flag = gw : Goods - Works

                String departmentId = request.getParameter("departmentId").trim();
                if(departmentId != null && departmentId.equals("0")){
                    departmentId = "";
                }

                String procEntity = request.getParameter("procEntity").trim();
                if(procEntity != null && procEntity.equals("0")){
                    procEntity = "";
                }

                String procNature = request.getParameter("procNature").trim();
                if(procNature != null && procNature.equals("0")){
                    procNature = "";
                }

                String procMethod = request.getParameter("procMethod").trim();
                if(procMethod != null && procMethod.equals("0")){
                    procMethod = "";
                }

                String procType = request.getParameter("procType").trim();
                if(procType != null && procType.equals("0")){
                    procType = "";
                }

                String oldTenderId = request.getParameter("oldTenderId").trim();

                EvalCommonSearchService evalService = (EvalCommonSearchService) AppContext.getSpringBean("EvalCommonSearchService");
                List<EvalCommonSearchData> pAllTender = evalService.evalSearchData(funName, userId, departmentId, procEntity, procNature, procMethod, procType, oldTenderId, "", "", "", "", "", pageNo, size);
                printTable(out, response, request, pAllTender, tenderId, pkgLotId, userId, flag);
            }
            else if (funName.equalsIgnoreCase("PreviousAllMappedTenders")) {
                String userId = request.getParameter("userId").trim();
                String tenderId = request.getParameter("tenderId").trim();
                String pkgLotId = request.getParameter("pkgLotId").trim();
                String docId = request.getParameter("docId").trim();
                String size = request.getParameter("size").trim();
                String pageNo = request.getParameter("pageNo").trim();
                String flag = request.getParameter("flag").trim();  // flag = s : Service || flag = gw : Goods - Works

                String departmentId = request.getParameter("departmentId").trim();
                if(departmentId != null && departmentId.equals("0")){
                    departmentId = "";
                }

                String procEntity = request.getParameter("procEntity").trim();
                if(procEntity != null && procEntity.equals("0")){
                    procEntity = "";
                }

                String procNature = request.getParameter("procNature").trim();
                if(procNature != null && procNature.equals("0")){
                    procNature = "";
                }

                String procMethod = request.getParameter("procMethod").trim();
                if(procMethod != null && procMethod.equals("0")){
                    procMethod = "";
                }

                String procType = request.getParameter("procType").trim();
                if(procType != null && procType.equals("0")){
                    procType = "";
                }

                String oldTenderId = request.getParameter("oldTenderId").trim();

                EvalCommonSearchService evalService = (EvalCommonSearchService) AppContext.getSpringBean("EvalCommonSearchService");
                List<EvalCommonSearchData> pAllTender = evalService.evalSearchData(funName, userId, departmentId, procEntity, procNature, procMethod, procType, oldTenderId, docId, "", "", "", "", pageNo, size);
                printTable(out, response, request, pAllTender, tenderId, pkgLotId, userId, docId ,flag);
            }

        } catch(Exception e){
            out.print("<tr>");
            out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No records found</td>");
            out.print("</tr>");
            System.out.print("\n"+e);
        }finally {
            out.close();
        }
    }

    private void printTable(PrintWriter out, HttpServletResponse response, HttpServletRequest request, List<EvalCommonSearchData> data, String tenderId, String pkgLotId, String userId, String flag) {
        if(data.size() > 0){

            TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");

            for(int i = 0; i< data.size(); i++){
                out.println("<tr>");
                out.println("<td width='5%' class='t-align-center'>" + data.get(i).getFieldName12().trim() + "</td>");

                //out.println("<td width='15%' class='t-align-center'>" + data.get(i).getFieldName1() + ", <br />" + data.get(i).getFieldName2() + ", <br /><span style='color: red;font-weight: bold;'><blink>" + data.get(i).getFieldName3() + "</blink></span></td>"); 
                String tenderStatus = "";
                try{
                    //added 2 null value for district and thana by Proshanto Kumar Saha
                    List<CommonTenderDetails> list = tenderService.gettenderinformationSP("get details", "tender", Integer.parseInt(data.get(i).getFieldName1().trim()), null, null, null, null, null, null, null, null, "Pending", null, null, null, null, null, null, 1, 10,null,null);
                    
                    if(list.size() > 0){
                        if ("Awarded".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Contract Awarded";
                        }  else if("Cancelled".equalsIgnoreCase(list.get(0).getTenderStatus().trim())){
                                tenderStatus = "Cancelled";
                        }  else if ("Re-Tendered".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "To be Re-Tendered ";
                        }  else if ("Rejected".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Rejected";
                        }  else if ("Approved".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Approved";
                        }  else if ("NOA declined".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "NOA declined";
                        }  else if ("Being evaluated".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Being evaluated";
                        }  else if ("Clarification Requested".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Clarification Requested";
                        }  else if ("Opening Completed".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Opening Completed";
                        }  else if ("ReTendered".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Re-Tendered";
                        }
                    }else{
                        tenderStatus = "";
                    }
                }catch(Exception e){
                    tenderStatus = "";
                    System.out.println("\n" + e);
                }
                out.println("<td width='15%' class='t-align-center'>" + data.get(i).getFieldName1().trim() + ", <br />" + data.get(i).getFieldName2().trim() + ", <br /><span style='color: red;font-weight: bold;'><blink>" + tenderStatus + "</blink></span></td>");

                //String viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + data.get(i).getFieldName1() + "&h="+request.getParameter("h")+"', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'><span id='tenderBrief_"+ i +"'>" + data.get(i).getFieldName5() + "</span></a>";
                String viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + data.get(i).getFieldName1().trim() + "&h=null', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'><span id='tenderBrief_"+ i +"'>" + data.get(i).getFieldName5() + "</span></a>";
                out.println("<td width='35%' class='t-align-left'>" + data.get(i).getFieldName4().trim() + ", <br />" + viewLink + "</td>");

                //out.println("<td width='20%' class='t-align-left'>" + data.get(i).getFieldName6().trim() + ", <br />" + data.get(i).getFieldName7().trim() + ", <br />" + data.get(i).getFieldName8().trim() + ", <br />" + data.get(i).getFieldName9().trim() + "</td>");
                StringBuilder dept = new StringBuilder();
                if(!"".equalsIgnoreCase(data.get(i).getFieldName6().trim()) && data.get(i).getFieldName6().trim() != null){
                    dept.append(data.get(i).getFieldName6().trim() + ",<br />");
                }
                if(!"".equalsIgnoreCase(data.get(i).getFieldName7().trim()) && data.get(i).getFieldName7().trim() != null){
                    dept.append(data.get(i).getFieldName7().trim() + ",<br />");
                }
                if(!"".equalsIgnoreCase(data.get(i).getFieldName8().trim()) && data.get(i).getFieldName8().trim() != null){
                    dept.append(data.get(i).getFieldName8().trim() + ",<br />");
                }
                if(!"".equalsIgnoreCase(data.get(i).getFieldName9().trim()) && data.get(i).getFieldName9().trim() != null){
                    dept.append(data.get(i).getFieldName9().trim());
                }
                out.println("<td width='20%' class='t-align-left'>" + dept.toString() + "</td>");

                out.println("<td width='15%' class='t-align-center'> " + data.get(i).getFieldName10().trim() + ", <br />" + data.get(i).getFieldName11().trim() + "</td>");
                
                if(flag.equalsIgnoreCase("s")){
                    out.println("<td width='10%' class='t-align-center'><a href='ViewWinningDocuments.jsp?oldTenderId=" + data.get(i).getFieldName1().trim() + "&tenderId=" + tenderId + "&cuId=" + userId + "&puId="+ data.get(i).getFieldName14() +"&flag=s'>View</a></td>");
                }else{
                    out.println("<td width='10%' class='t-align-center'><a href='ViewWinningDocuments.jsp?oldTenderId=" + data.get(i).getFieldName1().trim() + "&tenderId=" + tenderId + "&cuId=" + userId + "&puId="+ data.get(i).getFieldName14() +"&pkgLotId=" + pkgLotId + "'>View</a></td>");
                }
                out.println("</tr>");
            }

            String strOffset = request.getParameter("size");
            int recordOffset =0;
            if(strOffset!=null && !"".equalsIgnoreCase(strOffset)){
                recordOffset = Integer.parseInt(strOffset);
            }

            int totalPages = 0;
            if (data.size() > 0) {
                totalPages = (int) (Math.ceil(Math.ceil(Integer.parseInt(data.get(0).getFieldName13())) / recordOffset));
                if (totalPages == 0) {
                    totalPages = 1;
                }
            }
            out.println("<tr style='display: none'>");
            out.print("<input type=\"hidden\" id=\"cntTenBrief\" value=\"" + data.size() + "\">");
            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            out.println("</tr>");
        }else{
            out.print("<tr>");
            out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No records found</td>");
            out.print("</tr>");
        }
    }

        private void printTable(PrintWriter out, HttpServletResponse response, HttpServletRequest request, List<EvalCommonSearchData> data, String tenderId, String pkgLotId, String userId, String DocId,String flag) {
        if(data.size() > 0){

            TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");

            for(int i = 0; i< data.size(); i++){
                out.println("<tr>");
                out.println("<td width='5%' class='t-align-center'>" + data.get(i).getFieldName12().trim() + "</td>");

                //out.println("<td width='15%' class='t-align-center'>" + data.get(i).getFieldName1() + ", <br />" + data.get(i).getFieldName2() + ", <br /><span style='color: red;font-weight: bold;'><blink>" + data.get(i).getFieldName3() + "</blink></span></td>");
                String tenderStatus = "";
                try{
                    //Added 2 null values for district and thana by Proshanto Kumar Saha
                    List<CommonTenderDetails> list = tenderService.gettenderinformationSP("get details", "tender", Integer.parseInt(data.get(i).getFieldName1().trim()), null, null, null, null, null, null, null, null, "Pending", null, null, null, null, null, null, 1, 10,null,null);

                    if(list.size() > 0){
                        if ("Awarded".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Contract Awarded";
                        }  else if("Cancelled".equalsIgnoreCase(list.get(0).getTenderStatus().trim())){
                                tenderStatus = "Cancelled";
                        }  else if ("Re-Tendered".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "To be Re-Tendered ";
                        }  else if ("Rejected".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Rejected";
                        }  else if ("Approved".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Approved";
                        }  else if ("NOA declined".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "NOA declined";
                        }  else if ("Being evaluated".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Being evaluated";
                        }  else if ("Clarification Requested".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Clarification Requested";
                        }  else if ("Opening Completed".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Opening Completed";
                        }  else if ("ReTendered".equalsIgnoreCase(list.get(0).getTenderevalstatus().trim())) {
                                tenderStatus = "Re-Tendered";
                        }
                    }else{
                        tenderStatus = "";
                    }
                }catch(Exception e){
                    tenderStatus = "";
                    System.out.println("\n" + e);
                }
                out.println("<td width='15%' class='t-align-center'>" + data.get(i).getFieldName1().trim() + ", <br />" + data.get(i).getFieldName2().trim() + ", <br /><span style='color: red;font-weight: bold;'><blink>" + tenderStatus + "</blink></span></td>");

                //String viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + data.get(i).getFieldName1() + "&h="+request.getParameter("h")+"', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'><span id='tenderBrief_"+ i +"'>" + data.get(i).getFieldName5() + "</span></a>";
                String viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + data.get(i).getFieldName1().trim() + "&h=null', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'><span id='tenderBrief_"+ i +"'>" + data.get(i).getFieldName5() + "</span></a>";
                out.println("<td width='35%' class='t-align-left'>" + data.get(i).getFieldName4().trim() + ", <br />" + viewLink + "</td>");

                //out.println("<td width='20%' class='t-align-left'>" + data.get(i).getFieldName6().trim() + ", <br />" + data.get(i).getFieldName7().trim() + ", <br />" + data.get(i).getFieldName8().trim() + ", <br />" + data.get(i).getFieldName9().trim() + "</td>");
                StringBuilder dept = new StringBuilder();
                if(!"".equalsIgnoreCase(data.get(i).getFieldName6().trim()) && data.get(i).getFieldName6().trim() != null){
                    dept.append(data.get(i).getFieldName6().trim() + ",<br />");
                }
                if(!"".equalsIgnoreCase(data.get(i).getFieldName7().trim()) && data.get(i).getFieldName7().trim() != null){
                    dept.append(data.get(i).getFieldName7().trim() + ",<br />");
                }
                if(!"".equalsIgnoreCase(data.get(i).getFieldName8().trim()) && data.get(i).getFieldName8().trim() != null){
                    dept.append(data.get(i).getFieldName8().trim() + ",<br />");
                }
                if(!"".equalsIgnoreCase(data.get(i).getFieldName9().trim()) && data.get(i).getFieldName9().trim() != null){
                    dept.append(data.get(i).getFieldName9().trim());
                }
                out.println("<td width='20%' class='t-align-left'>" + dept.toString() + "</td>");

                out.println("<td width='15%' class='t-align-center'> " + data.get(i).getFieldName10().trim() + ", <br />" + data.get(i).getFieldName11().trim() + "</td>");

                if(flag.equalsIgnoreCase("s")){
                    out.println("<td width='10%' class='t-align-center'><a href='ViewWinningDocuments.jsp?oldTenderId=" + data.get(i).getFieldName1().trim() + "&tenderId=" + tenderId + "&cuId=" + userId + "&docId=" + DocId + "&puId="+ data.get(i).getFieldName14() +"&flag=s'>View</a></td>");
                }else{
                    out.println("<td width='10%' class='t-align-center'><a href='ViewWinningDocuments.jsp?oldTenderId=" + data.get(i).getFieldName1().trim() + "&tenderId=" + tenderId + "&cuId=" + userId + "&docId=" + DocId + "&puId="+ data.get(i).getFieldName14() +"&pkgLotId=" + pkgLotId + "'>View</a></td>");
                }
                out.println("</tr>");
            }

            String strOffset = request.getParameter("size");
            int recordOffset =0;
            if(strOffset!=null && !"".equalsIgnoreCase(strOffset)){
                recordOffset = Integer.parseInt(strOffset);
            }

            int totalPages = 0;
            if (data.size() > 0) {
                totalPages = (int) (Math.ceil(Math.ceil(Integer.parseInt(data.get(0).getFieldName13())) / recordOffset));
                if (totalPages == 0) {
                    totalPages = 1;
                }
            }
            out.println("<tr style='display: none'>");
            out.print("<input type=\"hidden\" id=\"cntTenBrief\" value=\"" + data.size() + "\">");
            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
            out.println("</tr>");
        }else{
            out.print("<tr>");
            out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No records found</td>");
            out.print("</tr>");
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
