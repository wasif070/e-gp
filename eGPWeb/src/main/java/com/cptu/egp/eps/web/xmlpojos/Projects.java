/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Chalapathi.Bavisetti
 */
public class Projects {
    private int projectId;
    private String projectName;

    /**
     * @return the projectId
     */
    @XmlElement(nillable = true)
    public int getProjectId() {
        return projectId;
    }

    /**
     * @param projectId the projectId to set
     */
    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    /**
     * @return the projectName
     */
    @XmlElement(nillable = true)
    public String getProjectName() {
        return projectName;
    }

    /**
     * @param projectName the projectName to set
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    
}
