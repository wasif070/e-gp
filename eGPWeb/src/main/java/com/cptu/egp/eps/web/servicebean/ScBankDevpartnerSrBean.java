/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblCountryMaster;
import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster;
import com.cptu.egp.eps.model.table.TblStateMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ScBankDevpartnerSrBean {

    static final Logger logger = Logger.getLogger(ScBankDevpartnerSrBean.class);
    private String logUserId = "0";
    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
    ScBankDevpartnerService scBankDevpartnerService = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
    private List<SelectItem> countryList = new ArrayList<SelectItem>();
    private List<SelectItem> branchList = new ArrayList<SelectItem>();
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }
    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
        commonService.setUserId(logUserId);
        scBankDevpartnerService.setUserId(logUserId);
    }

    /**
     * Get list of country from TblCountryMaster
     * @return List Of <selectItem>
     * @throws Exception
     */
    public List<SelectItem> getCountryList() throws Exception {
        logger.debug("getCountryList : "+logUserId+" Starts");
        if (countryList.isEmpty()) {
            List<TblCountryMaster> countryMasters = commonService.countryMasterList();
            for (TblCountryMaster countryMaster : countryMasters) {
                countryList.add(new SelectItem(countryMaster.getCountryId(), countryMaster.getCountryName()));
            }
        }
        logger.debug("getCountryList : "+logUserId+" Ends");
        return countryList;
    }

    /**
     * setter of coyntryList
     * @param countryList
     */
    public void setCountryList(List<SelectItem> countryList) {
        this.countryList = countryList;
    }
    private List<SelectItem> stateList = new ArrayList<SelectItem>();

    /**
     * Get StateList for tbl_statemaster as per countryId
     * @param countryId
     * @return List Of <selectItem>
     */
    public List<SelectItem> getStateList(short countryId) {
        logger.debug("getStateList : "+logUserId+" Starts");
        if (stateList.isEmpty()) {
            List<TblStateMaster> stateMasters = commonService.getState(countryId);
            for (TblStateMaster stateMaster : stateMasters) {
                stateList.add(new SelectItem(stateMaster.getStateId(), stateMaster.getStateName()));
            }
        }
        logger.debug("getStateList : "+logUserId+" Ends");
        return stateList;
    }

    /**
     * settter of stateList
     * @param stateList
     */
    public void setStateList(List<SelectItem> stateList) {
        this.stateList = stateList;
    }
    private List<SelectItem> bankPartnerList = new ArrayList<SelectItem>();

    /**
     * Get DevelopmentPartner of Sceheduled bank name name and Id
     * @param type (develpment or schbank)
     * @return List<selectItem>
     */
    public List<SelectItem> getBankPartnerList(String type) {
        logger.debug("getBankPartnerList : "+logUserId+" Starts");
        if (bankPartnerList.isEmpty()) {
            List<TblScBankDevPartnerMaster> scBankList = scBankDevpartnerService.getBankPartnerList(type);

            for (TblScBankDevPartnerMaster scBank : scBankList) {
                bankPartnerList.add(new SelectItem(scBank.getSbankDevelopId(), scBank.getSbDevelopName()));
            }
        }
        logger.debug("getBankPartnerList : "+logUserId+" Ends");
        return bankPartnerList;
    }

    /**
     * setter of bankPartnerList
     * @param bankPartnerList
     */
    public void setBankPartnerList(List<SelectItem> bankPartnerList) {
        this.bankPartnerList = bankPartnerList;
    }
    List<SelectItem> designationList = new ArrayList<SelectItem>();

    /**
     * Get List of designation from TblDesignationMaster
     * @return
     */
    public List<SelectItem> getDesignationList() {
        logger.debug("getDesignationList : "+logUserId+" Starts");
        if (designationList.isEmpty()) {
            List<TblDesignationMaster> designationMasters = scBankDevpartnerService.getDesignationMasters();

            for (TblDesignationMaster designationMaster : designationMasters) {
                designationList.add(new SelectItem(designationMaster.getDesignationId(), designationMaster.getDesignationName()));
            }
        }
        logger.debug("getDesignationList : "+logUserId+" Ends");
        return designationList;
    }

    /**
     * setter of designation list
     * @param designationList
     */
    public void setDesignationList(List<SelectItem> designationList) {
        this.designationList = designationList;
    }
    private int sBankDevelopId;
    private String sbDevelopName;

    /**
     * getter of scheduled bank or development partner Name
     * @return
     */
    public String getSbDevelopName() {
        return sbDevelopName;
    }

    /**
     * setter of scheduled bank or development partner name
     * @param sbDevelopName
     */
    public void setSbDevelopName(String sbDevelopName) {
        this.sbDevelopName = sbDevelopName;
    }

    /**
     * getter of scheduled bank or development partner Id
     * @return
     */
    public int getsBankDevelopId() {
        return sBankDevelopId;
    }

    /**
     * setter of scheduled bank or development partner Id
     * @param sBankDevelopId
     */
    public void setsBankDevelopId(int sBankDevelopId) {
        this.sBankDevelopId = sBankDevelopId;
    }

    /**
     * get bankName as per user id and userTypeId
     * @param userId
     * @param userTypeId
     */
    public void getBankName(int userId, byte userTypeId) {
        logger.debug("getBankName : "+logUserId+" Starts");
        Object[] admin = scBankDevpartnerService.retriveAdminInfo(userId, userTypeId);
        setsBankDevelopId((Integer) admin[1]);
        setSbDevelopName(String.valueOf(admin[2]));
        logger.debug("getBankName : "+logUserId+" Ends");
    }

    /**
     * Get sbank develpId and name as per brancId (bank admin)
     * @param branchId
     */
    public void getBankForAdmin(int branchId) {
        logger.debug("getBankForAdmin : "+logUserId+" Starts");
        Object[] obj = scBankDevpartnerService.bankNameForAdmin(branchId);
        if (obj != null) {
            setsBankDevelopId((Integer) obj[0]);
        setSbDevelopName(String.valueOf(obj[1]));
        } else {
         setsBankDevelHeadId(0);
         setSbDevelopName("");
        }
        logger.debug("getBankForAdmin : "+logUserId+" Ends");
    }
    private int sBankDevelHeadId;

    /**
     * getter of sBankDevelHeadId
     * @return
     */
    public int getsBankDevelHeadId() {
        return sBankDevelHeadId;
    }

    /**
     * setter of sBankDevelHeadId
     * @param sBankDevelHeadId
     */
    public void setsBankDevelHeadId(int sBankDevelHeadId) {
        this.sBankDevelHeadId = sBankDevelHeadId;
    }

    /**
     * get branchList using scbank or developmet head id
     * @return List<selected Item>
     * @throws Exception
     */
    public List<SelectItem> getBranchList() throws Exception {
        logger.debug("getBranchList : "+logUserId+" Starts");
        logger.debug("dept : " + getsBankDevelHeadId());
        Object[] values = {
            "sbankDevelHeadId", Operation_enum.EQ, getsBankDevelHeadId(),
            "isBranchOffice", Operation_enum.LIKE, "Yes",
            "partnerType", Operation_enum.ORDERBY, Operation_enum.ASC};
        if (branchList.isEmpty()) {
            for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : scBankDevpartnerService.findBranchList(values)) {
                branchList.add(new SelectItem(tblScBankDevPartnerMaster.getSbankDevelopId(), tblScBankDevPartnerMaster.getSbDevelopName()));
            }

        }
        logger.debug("getBranchList : "+logUserId+" Ends");
        return branchList;
    }

    /**
     * setter of brancList
     * @param branchList
     */
    public void setBranchList(List<SelectItem> branchList) {
        this.branchList = branchList;
    }

    /**
     * Get branch Checker
     * @param bankId
     * @param role
     * @return
     */
    public String getBankChecker(String bankId, String role) {
        return scBankDevpartnerService.bankChecker(Integer.parseInt(bankId), role);
    }
    /**
     * For Branch and Bank Name
     * @param userId
     * @return List [0] branchId,[1] Branch,[2] BankId,[3] BankName
     */
    public List<Object []> getBankBranchByUserId(String userId){
        List<Object []> list = new ArrayList<Object[]>();
        try{
            list = scBankDevpartnerService.getBankBranchByUserId(Integer.parseInt(userId));
        }catch(Exception e){
            logger.error("Exception e"+e);
    }
        return list;
    }
}
