/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.service.serviceinterface.CmsVendorReqWccService;
import com.cptu.egp.eps.web.utility.AppContext;

/**
 *
 * @author rikin.p
 */
public class CmsVendorReqWccSrBean {

    private CmsVendorReqWccService cmsVendorReqWccService = (CmsVendorReqWccService) AppContext.getSpringBean("CmsVendorReqWccService"); 
    
    public boolean isRequestPending(int lotId,int cntId){
        return cmsVendorReqWccService.isRequestPending(lotId,cntId);
    }
    
    
    public int updateReqest(int wcCerId, int lotId){
        return cmsVendorReqWccService.updateReqest(wcCerId, lotId);
    }
}
