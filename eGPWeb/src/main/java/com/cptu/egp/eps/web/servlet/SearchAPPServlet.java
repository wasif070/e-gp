/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.APPSearchDtBean;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class SearchAPPServlet extends HttpServlet {

    private final AppAdvSearchService appAdvSearchService = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
    private final CommonSearchService commonService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
    private static final Logger LOGGER = Logger.getLogger(HelpGrid.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            appAdvSearchService.setLogUserId(logUserId);
            commonService.setLogUserId(logUserId);
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        List<APPSearchDtBean> appData = new ArrayList<APPSearchDtBean>();
        String styleClass = "";
        try {
            String action = request.getParameter("action");
            if (action.equalsIgnoreCase("Search")) {
                LOGGER.debug("processRequest : action : Search : "+logUserId+ LOGGERSTART);
                int stateId = 0;
                int officeId = 0;
                if (request.getParameter("stateId") != null) {
                    stateId = Integer.parseInt(request.getParameter("stateId"));
                }
                int departmentid = 0;
                if (request.getParameter("departmentid") != null && !request.getParameter("departmentid").trim().equals("")) {
                    departmentid = Integer.parseInt(request.getParameter("departmentid"));
                }
                String financialYear = "";
                if (request.getParameter("financialYear") != null && !request.getParameter("financialYear").trim().equals("")) {
                    financialYear = request.getParameter("financialYear");
                }
                if(request.getParameter("officeId")!=null && !"".equals(request.getParameter("officeId"))){
                    officeId = Integer.parseInt(request.getParameter("officeId"));
                }
                String strPageNo = request.getParameter("pageNo");
                int pageNo = Integer.parseInt(strPageNo);
                String strOffset = request.getParameter("size");
                int recordOffset = Integer.parseInt(strOffset);
                appData = appAdvSearchService.getAPPSearch(financialYear, departmentid, stateId, officeId, pageNo, recordOffset);

                if (appData != null && !appData.isEmpty()) {
                    for (int i = 0; i < appData.size(); i++) {
                        StringBuilder link = new StringBuilder();
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                        APPSearchDtBean details = appData.get(i);
                        out.print("<tr class='" + styleClass + "'>");
                        out.print("<td class=\"t-align-left\">" + details.getMinistry() + "</td>");
//                        out.print("<td class=\"t-align-left\">" + details.getDivision() + "</td>");
//                        out.print("<td class=\"t-align-left\">" + details.getOranisation() + "</td>");
                        out.print("<td class=\"t-align-left\">" + details.getOfficeName() + "</td>");
                        //if (details.getBudgetType().equalsIgnoreCase("1")) {
                        //Here change DB to CB by Proshanto Kumar Saha
                            link.append("<a href=\"AppDetails.jsp?officeId=" + details.getOfficeId() + "&bTypeId=" + 1 + "\">CB</a> | ");
                        //} else if (details.getBudgetType().equalsIgnoreCase("2")) {
                            link.append("<a href=\"AppDetails.jsp?officeId=" + details.getOfficeId() + "&bTypeId=" + 2 + "\">RB</a> | ");
                        //} else {
                            link.append("<a href=\"AppDetails.jsp?officeId=" + details.getOfficeId() + "&bTypeId=" + 3 + "\">OF</a>");
                        //}
                        out.print("<td class=\"t-align-center\">" + link.toString() + "</td>");
                        out.print("</tr>");
                    }
                    int totalPages = 1;

                    if (!appData.isEmpty()) {
                        int count = Integer.parseInt(appData.get(0).getTotalRowCount());
                        totalPages = (int) Math.ceil(Math.ceil(count) / recordOffset);
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                } else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"5\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"1\">");
                }
                LOGGER.debug("processRequest : action : Search : "+logUserId+ LOGGEREND);
            } else if (action.equalsIgnoreCase("advSearch")) {
                LOGGER.debug("processRequest : action : advSearch : "+logUserId+ LOGGERSTART);
                String officeId = request.getParameter("office");
                String keyWord = request.getParameter("keyWord");
                String bTypeId = request.getParameter("bTypeId");
                String strPageNo = request.getParameter("pageNo");
                //int pageNo = Integer.parseInt(strPageNo);
                String strOffset = request.getParameter("size");
                int recordOffset = Integer.parseInt(strOffset);
                
                //List<SPCommonSearchData> details = commonService.returndata("GetAppDetailByOfficeId", officeId, bTypeId);
                List<SPCommonSearchData> details = null;
                if(keyWord == null || "null".equals(keyWord)){
                    details = commonService.searchData("GetAppDetailByOfficeId", bTypeId, officeId, strOffset, strPageNo, "", "", "", "", "");
                }else{
                    details = commonService.searchData("GetAppDetailByPackageDtl", keyWord, strOffset, strPageNo, "", "", "", "", "", "");
                }

                if (details != null && !details.isEmpty()) {
                    for (int i = 0; i < details.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }

                        SPCommonSearchData listing = details.get(i);


                        /*GSS APP revised packages */
                        if("BeingRevised".equalsIgnoreCase(listing.getFieldName10())){
                            CommonSearchService searchService=(CommonSearchService)AppContext.getSpringBean("CommonSearchService");
                            List<SPCommonSearchData> list=searchService.searchData("getTempPkgDetails", String.valueOf(listing.getFieldName9()), "", "", "", "", "", "", "", "");
                            if(list!=null && !list.isEmpty()){
                               SPCommonSearchData tempPkgData=list.get(0);
                               listing.setFieldName4(tempPkgData.getFieldName1());
                               listing.setFieldName5(tempPkgData.getFieldName2()+", <br>"+tempPkgData.getFieldName3());
                               listing.setFieldName6(tempPkgData.getFieldName4()+", <br>"+tempPkgData.getFieldName5());


                            }
                        }

                        /* GSS APP revised packages  */
                        out.print("<tr class='" + styleClass + "'>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName1() + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName2() + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName3() + "</td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName4() + "</td>");
                        //out.print("<td class=\"t-align-center\">" + listing.getFieldName5() + "</td>");
                        String pckDescNo[] = listing.getFieldName5().split(",");
                        String pckNo = "";
                        String pckDesc = "";
                        if(pckDescNo != null && !pckDescNo.equals("")){
                            pckNo = pckDescNo[0];
                            pckDesc = pckDescNo[1];
                        }
                        String viewList = request.getContextPath() + "/resources/common/ViewAppDetails.jsp?appId=" + listing.getFieldName2() + "&pkgId=" + listing.getFieldName9() + "&pkgType=Package&from=home";
                        if("BeingRevised".equalsIgnoreCase(listing.getFieldName10())){
                        viewList = request.getContextPath() + "/resources/common/ViewAppDetails.jsp?appId=" + listing.getFieldName2() + "&pkgId=" + listing.getFieldName9() + "&pkgType=RevisePackage&from=home";
                        }
                        out.print("<td class=\"t-align-left\">" + pckNo + ",<a onclick=\"window.open('"+ viewList +"','','resizable=yes,scrollbars=1','')\"  href=\"#\">" + pckDesc + "</a></td>");
                        out.print("<td class=\"t-align-center\">" + listing.getFieldName6() + "</td>");
                        out.print("</tr>");
                    }
                    int totalPages = 1;

                    if (!details.isEmpty()) {
                        int count = Integer.parseInt(details.get(0).getFieldName7());
                        totalPages = (int) Math.ceil(Math.ceil(count) / recordOffset);
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                } else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"6\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                      out.print("<input type=\"hidden\" id=\"totalPages\" value=\"1\">");
                }
                LOGGER.debug("processRequest : action : advSearch : "+logUserId+ LOGGEREND);
            }
        } catch(Exception ex){
            LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
            ex.printStackTrace();
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGEREND);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
