/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Karan
 */
public class EvaluationConfigurationSrBean {
    String logUserId = "0";
    final Logger logger = Logger.getLogger(EvaluationConfigurationSrBean.class);
    private TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    private CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
    AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");

    public void setLogUserId(String logUserId) {
        tenderCommonService.setLogUserId(logUserId);
        commonXMLSPService.setLogUserId(logUserId);
    }
    
    public boolean Request_TSCFormation (String tenderId, String refNo, String frmUserId) throws Exception {
        logger.debug("Request_TSCFormation "+logUserId+" Starts: ");
        boolean isSuccess=false;
        try {
        CommonMsgChk commonMsgChk = null;
                commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalConfig", "isTscReq='yes'", "tenderId=" + tenderId).get(0);
       /* END : CODE TO SEND UPDATE EVALUATION CONFIGURATION TABLE */

        /* START : CODE TO SEND TSC Formation Request  MAIL */
        String mailSubject = "Formation of Technical Sub Committee (TSC)";
        String strFrmEmailId = "";
        String toPEUserId="";
        String peOffice="";

        List<SPTenderCommonData> lstPEUserId =
                tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
        toPEUserId=lstPEUserId.get(0).getFieldName1();
        peOffice=lstPEUserId.get(0).getFieldName3();

        List<SPTenderCommonData> frmEmail =
                tenderCommonService.returndata("getEmailIdfromUserId", frmUserId, null);
        strFrmEmailId = frmEmail.get(0).getFieldName1();

        List<SPTenderCommonData> emails =
                tenderCommonService.returndata("getEmailIdfromUserId", toPEUserId, null);
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

        String[] mail = {emails.get(0).getFieldName1()};
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        MailContentUtility mailContentUtility = new MailContentUtility();
        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
        String mailText = mailContentUtility.contTSCFormationRequest(tenderId, refNo);

        sendMessageUtil.setEmailTo(mail);
        sendMessageUtil.setEmailSub(mailSubject);
        sendMessageUtil.setEmailMessage(mailText);

        userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contTSCFormationRequest(tenderId, refNo, peOffice));
        sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
        sendMessageUtil.sendEmail();

        sendMessageUtil = null;
        mailContentUtility = null;
        mail = null;
        msgBoxContentUtility = null;

      /* START : CODE TO SEND TSC Formation Request  MAIL */

        // Nullify List objects
        emails = null;
        frmEmail = null;
        lstPEUserId = null;

        // Nullify String objects
        mailSubject = null;
        strFrmEmailId = null;
        toPEUserId = null;
        peOffice = null;

        isSuccess = true;
            
        } catch (Exception e) {
            logger.error("Request_TSCFormation "+logUserId+" : "+e);
        }
       /* START : CODE TO SEND UPDATE EVALUATION CONFIGURATION TABLE */
        

        logger.debug("Request_TSCFormation "+logUserId+" Ends: ");
        return isSuccess;
    }

    public int Insert_Configuration (String tenderId, String configType, String evalCommMembers, String configBy, String selectedTECUserId, String isTECReq, String tenderRefNo){
        logger.debug("Insert_Configuration "+logUserId+" Starts: ");
        int evalConfigId = 0;
        try {
        String strXML = "";
        SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
      //  CommonMsgChk commonMsgChk = null;

                String selectedTECGovUserId="0";
                 List<SPTenderCommonData> lstSelTECMemGovUserId =
                            tenderCommonService.returndata("getGovUserIdFromUserId", selectedTECUserId, null);
                 if(!lstSelTECMemGovUserId.isEmpty()){
                    selectedTECGovUserId=lstSelTECMemGovUserId.get(0).getFieldName1();
                 }
                 lstSelTECMemGovUserId = null;

       
        /* START : CODE TO INSERT INTO EVALUATION CONFIGURATION TABLE */

        CommonMsgChk commonMsgChk =addUpdate.addUpdOpeningEvaluation("InsertEvalConfig",tenderId,configType,HandleSpecialChar.handleSpecialChar(evalCommMembers),configBy,objSDF.format(new Date()),"yes",selectedTECUserId,"0",isTECReq,"Evaluation",selectedTECGovUserId,"Pending","Live","Yes","","","","","").get(0);
        evalConfigId = commonMsgChk.getId();
        
        /* END : CODE TO INSERT INTO EVALUATION CONFIGURATION TABLE */

        if (commonMsgChk.getFlag()) {

            int tscMemberCnt = 0;
            String selTECMemberNm = "";

            if ("team".equalsIgnoreCase(configType)) {
                configType = "Team";
            } else {
                configType = "Individual";
            }
            List<SPTenderCommonData> lstTECMemberNm =
                    tenderCommonService.returndata("getEmployeeNamefromUserId", tenderId, null);
            if (!lstTECMemberNm.isEmpty()) {
                selTECMemberNm = lstTECMemberNm.get(0).getFieldName1();
                    }
            /* START : CODE TO UPDATE EVALUATION NOMINATION TABLE */
            //commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalNomination", "isCurrent='No'", "tenderId="+tenderId).get(0);
            /* END : CODE TO UPDATE EVALUATION NOMINATION TABLE */

            for (SPTenderCommonData commonTenderDetails : tenderCommonService.returndata("getTenderEvaluationinfo", tenderId, null)) {
                if (!"chairperson".equalsIgnoreCase(commonTenderDetails.getFieldName3())) {
                    tscMemberCnt++;
                    // if ("team".equalsIgnoreCase(configType)) {
                    //commonMsgChk =addUpdate.addUpdOpeningEvaluation("InsertEvalNomination",Integer.toString(evalConfigId),tenderId,"0",commonTenderDetails.getFieldName7(),objSDF.format(new Date()),"Pending",configType,"Live","","Yes","","","","","","","","","").get(0);

                   // commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalNomination", strXML, "").get(0);
                    /* END : CODE TO INSERT INTO EVALUATION NOMINATION TABLE */
                 //   }
                    /* START : CODE TO SEND Configuration Notification  MAILS TO TEC MEMBERS */
                    boolean mailSent = Configuration_NotificationMail(configType, tenderId, tenderRefNo, selTECMemberNm, configBy, commonTenderDetails.getFieldName7());
                    /* END : CODE TO SEND Configuration Notification  MAILS TO TEC MEMBERS */
                }
            }

             // Nullify List objects
            lstTECMemberNm = null;
             // Nullify String objects
            strXML = null;

                }
        } catch (Exception e) {
            logger.error("Insert_Configuration "+logUserId+" : "+e);
        }
            
        logger.debug("Insert_Configuration "+logUserId+" Ends: ");
        return evalConfigId;
    }
    
    public boolean Configuration_NotificationMail (String configType, String tenderId, String refNo, String selectedMember, String frmUserId, String toUserId) throws Exception {
        logger.debug("Configuration_NotificationMail "+logUserId+" Starts: ");
        boolean isSuccess=false;
        try {
        /* START : CODE TO Configuration Notification  MAIL */
        String strFrmEmailId = "";
        String toPEUserId="";
        String peOffice="";
        String mailSubject="";

        if ("team".equalsIgnoreCase(configType)) {
            mailSubject = "eGP System: Team Evaluation - Your consent is required";
        } else {
            mailSubject = "eGP System: Individual Evaluation - Your consent is required";
        }

        List<SPTenderCommonData> lstPEUserId =
                tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
        toPEUserId=lstPEUserId.get(0).getFieldName5();
        peOffice=lstPEUserId.get(0).getFieldName3();

        List<SPTenderCommonData> frmEmail =
                tenderCommonService.returndata("getEmailIdfromUserId", frmUserId, null);
        strFrmEmailId = frmEmail.get(0).getFieldName1();

        List<SPTenderCommonData> emails =
                tenderCommonService.returndata("getEmailIdfromUserId", toUserId, null);

        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

        String[] mail = {emails.get(0).getFieldName1()};
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        MailContentUtility mailContentUtility = new MailContentUtility();

        String mailText = mailContentUtility.contConfigurationNotification(configType, tenderId, refNo, selectedMember, toPEUserId, peOffice);

        sendMessageUtil.setEmailTo(mail);
        sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
        sendMessageUtil.setEmailSub(mailSubject);
                sendMessageUtil.setEmailMessage(mailText);
        sendMessageUtil.sendEmail();

        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
        userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contConfigurationNotification(configType, tenderId, refNo, selectedMember, toPEUserId, peOffice));

        sendMessageUtil = null;
        mailContentUtility = null;
        mail = null;
        msgBoxContentUtility = null;

      /* END : CODE TO Configuration Notification  MAIL */

        // Nullify List objects
        emails = null;
        frmEmail = null;
        lstPEUserId = null;
        // Nullify String objects
        strFrmEmailId = null;
        toPEUserId = null;
        peOffice = null;
        mailSubject = null;

        isSuccess=true;
        } catch (Exception e) {
            logger.error("Configuration_NotificationMail "+logUserId+" : "+e);
        }
        
        logger.debug("Configuration_NotificationMail "+logUserId+" Ends: ");
        return isSuccess;
    }

    public boolean Modify_Configuration (String tenderId, String tenderRefNo, String configBy) throws Exception {
        logger.debug("Configuration_NotificationMail "+logUserId+" Starts: ");
        boolean isSuccess=false;
        try {
               
        String evalConfigId="0";
        String strXML = "";
        SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        CommonMsgChk commonMsgChk = null;

        /* START : CODE TO SEND UPDATE EVALUATION CONFIGURATION TABLE */
        //commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalConfig", "configType='ind', tecMemberId=0", "tenderId=" + tenderId).get(0);

        commonMsgChk =addUpdate.addUpdOpeningEvaluation("UpdateEvalConfigInsertEvalNomination",tenderId,"ind","0","No","0",objSDF.format(new Date()),"pending","ind","Live","","Yes","","","","","","","","").get(0);
       /*Parameter definition: addUpdate.addUpdOpeningEvaluation(actionName,tenderId,ConfigType,tecMemberId,isCurrent,envelopeId,nominationDate,NomineeAction,evalType,nomineeStatus,remarks,isCurrent_NOMTBL,nomineeActionDate)*/
        /* END : CODE TO SEND UPDATE EVALUATION CONFIGURATION TABLE */
        isSuccess=commonMsgChk.getFlag();


        if(isSuccess){
            for (SPTenderCommonData commonTenderDetails : tenderCommonService.returndata("getTenderEvaluationinfo", tenderId, null)) {
                        if (!"chairperson".equalsIgnoreCase(commonTenderDetails.getFieldName3())) {

                             /* START : CODE TO SEND Configuration Modification  MAILS TO TEC MEMBERS */
                            boolean mailSent = Configuration_ModificationMail("Individual", tenderId, tenderRefNo, "", configBy, commonTenderDetails.getFieldName7());
                            /* END : CODE TO SEND Configuration Modification  MAILS TO TEC MEMBERS */
                        }
                    }
        }

//        if(isSuccess){
//
//                     List<SPTenderCommonData> lstEvalConfig =
//                     tenderCommonService.returndata("getEvalConfigInfo", tenderId, null);
//
//             if(!lstEvalConfig.isEmpty()){
//                evalConfigId=lstEvalConfig.get(0).getFieldName1();
//             }
//
//             if(!"0".equalsIgnoreCase(evalConfigId)){
//                 /* START : CODE TO UPDATE EVALUATION NOMINATION TABLE */
//                    commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalNomination", "isCurrent='No'", "tenderId="+tenderId).get(0);
//                 /* END : CODE TO UPDATE EVALUATION NOMINATION TABLE */
//
//                     for (SPTenderCommonData commonTenderDetails : tenderCommonService.returndata("getTenderEvaluationinfo", tenderId, null)) {
//                        if (!"chairperson".equalsIgnoreCase(commonTenderDetails.getFieldName3())) {
//
//                            /* START : CODE TO INSERT INTO EVALUATION NOMINATION TABLE */
//                                    //strXML = "<tbl_EvalNomination evalConfigId=\"\" tenderId=\"" + tenderId + "\" envelopeId=\"0\" nomineeUserId=\"" + commonTenderDetails.getFieldName7() + "\" nominationDate=\"" + objSDF.format(new Date()) + "\" nomineeAction=\"Pending\" evaltype=\"ind\" nomineeStatus=\"Live\" remarks =\"\" isCurrent=\"Yes\" nomineeActionDt=\"\" />";
//                                    strXML = "<tbl_EvalNomination evalConfigId=\"" + evalConfigId + "\" tenderId=\"" + tenderId + "\" envelopeId=\"0\" nomineeUserId=\"" + commonTenderDetails.getFieldName7() + "\" nominationDate=\"" + objSDF.format(new Date()) + "\" nomineeAction=\"Pending\" evaltype=\"ind\" nomineeStatus=\"Live\" remarks =\"\" isCurrent=\"Yes\" nomineeActionDt=\"\" />";
//                            strXML = "<root>" + strXML + "</root>";
//
//                            commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalNomination", strXML, "").get(0);
//                            /* END : CODE TO INSERT INTO EVALUATION NOMINATION TABLE */
//
//                             /* START : CODE TO SEND Configuration Modification  MAILS TO TEC MEMBERS */
//                            boolean mailSent = Configuration_ModificationMail("Individual", tenderId, tenderRefNo, "", configBy, commonTenderDetails.getFieldName7());
//                            /* END : CODE TO SEND Configuration Modification  MAILS TO TEC MEMBERS */
//                        }
//                    }
//                     }
//             //Nullify List objects
//             lstEvalConfig = null;
//
//        }
        //Nullify String objects
        strXML = null;
        } catch (Exception e) {
            logger.error("Configuration_NotificationMail "+logUserId+" : "+e);
        }
        
        logger.debug("Configuration_NotificationMail "+logUserId+" Ends: ");
        return isSuccess;
    }

    public boolean Configuration_ModificationMail (String configType, String tenderId, String refNo, String selectedMember, String frmUserId, String toUserId) throws Exception {
        logger.debug("Configuration_ModificationMail "+logUserId+" Start: ");
        boolean isSuccess=false;
        try {
                /* START : CODE TO Configuration Notification  MAIL */
        String strFrmEmailId = "";
        String toPEUserId="";
        String peOffice="";
        String mailSubject="";

        if ("team".equalsIgnoreCase(configType)) {
            mailSubject = "Team Evaluation - Your consent is required";
        } else {
            mailSubject = "Individual Evaluation - Your consent is required";
        }

                List<SPTenderCommonData> lstPEUserId =
                tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);

        if(!lstPEUserId.isEmpty()){
            toPEUserId=lstPEUserId.get(0).getFieldName5();
            peOffice=lstPEUserId.get(0).getFieldName3();
        }

        List<SPTenderCommonData> frmEmail =
                tenderCommonService.returndata("getEmailIdfromUserId", frmUserId, null);
        if(!frmEmail.isEmpty()){
            strFrmEmailId = frmEmail.get(0).getFieldName1();
        }

        List<SPTenderCommonData> emails =
                tenderCommonService.returndata("getEmailIdfromUserId", toUserId, null);
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

        String[] mail = {emails.get(0).getFieldName1()};
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        MailContentUtility mailContentUtility = new MailContentUtility();

        String mailText = mailContentUtility.contConfigurationModification(configType, tenderId, refNo, selectedMember, toPEUserId, peOffice);

        sendMessageUtil.setEmailTo(mail);
        sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
        sendMessageUtil.setEmailSub(mailSubject);
                sendMessageUtil.setEmailMessage(mailText);
        sendMessageUtil.sendEmail();

                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
        userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("msgboxfrom"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contConfigurationModification(configType, tenderId, refNo, selectedMember, toPEUserId, peOffice));

        sendMessageUtil = null;
        mailContentUtility = null;
        mail = null;
        msgBoxContentUtility = null;
      /* END : CODE TO Configuration Notification  MAIL */
        // Nullify List objects
        emails = null;
        frmEmail = null;
        lstPEUserId = null;
        // Nullify String objects
        strFrmEmailId = null;
        toPEUserId = null;
        peOffice = null;
        mailSubject = null;

        isSuccess = true;
        } catch (Exception e) {
             logger.error("Configuration_ModificationMail "+logUserId+" : "+e);
        }
        
        logger.debug("Configuration_ModificationMail "+logUserId+" Ends: ");
        return isSuccess;
    }

}
