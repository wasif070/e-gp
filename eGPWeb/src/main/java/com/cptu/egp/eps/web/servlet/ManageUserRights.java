/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblRoleMaster;
import com.cptu.egp.eps.model.table.TblRoleMenuRights;
import com.cptu.egp.eps.model.table.TblUserMenuRights;
import com.cptu.egp.eps.service.serviceimpl.ManageUserRightsImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dipal.shah
 */
public class ManageUserRights extends HttpServlet {

    ManageUserRightsImpl objManageUserRights = (ManageUserRightsImpl)AppContext.getSpringBean("ManageUserRigths");

   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        int uId=0;
        if(request.getParameter("uId")!=null)
            uId = Integer.parseInt(request.getParameter("uId"));

        int userTypeid=0;
        if(request.getParameter("userTypeid")!=null)
            userTypeid = Integer.parseInt(request.getParameter("userTypeid"));

        String action=null;
        if(request.getParameter("action")!=null)
            action = request.getParameter("action");

        int roleId=0;
        if(request.getParameter("roleId")!=null)
            roleId = Integer.parseInt(request.getParameter("roleId"));

        String contextPath = request.getContextPath();

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        try
        {
            if(action != null && action.equalsIgnoreCase("assignRights"))
            {

                String menuId[]=null;
                 try
                 {
                    if(request.getParameter("chk")!=null)
                    {
                        menuId=request.getParameterValues("chk");
                        List<TblUserMenuRights> lstUserMenuRigths=new ArrayList<TblUserMenuRights>();
                        for(String obj : menuId)
                        {
                            TblUserMenuRights tblUserMenuRights=new TblUserMenuRights();
                            tblUserMenuRights.setMenuId(Integer.parseInt(obj));
                            tblUserMenuRights.setUserId(uId);
                            lstUserMenuRigths.add(tblUserMenuRights);
                        }
                        objManageUserRights.assignRights(lstUserMenuRigths, uId);
                        
                    }
                }
                 catch(Exception ex)
                 {
                     ex.printStackTrace();
                 }
                response.sendRedirect(contextPath+"/admin/OAndMAdminGrid.jsp?userTypeid="+userTypeid+"&userRightsmsg=success");
            }
            else if(action != null && action.equalsIgnoreCase("Create Role"))
            {

                TblRoleMaster tblRoleMaster = new TblRoleMaster();
                String successMsg="success";
                String menuId[]=null;
                 try
                 {
                    if (request.getParameter("role") != null && request.getParameter("role") != "")
                    {
                        tblRoleMaster.setRollName(request.getParameter("role"));
                        tblRoleMaster.setStatus(1); // To set default active
                        roleId=objManageUserRights.addRoles(tblRoleMaster);
                    }
                    
                    if(request.getParameter("chk")!=null && roleId != 0)
                    {
                        menuId=request.getParameterValues("chk");
                        List<TblRoleMenuRights> lstRoleMenuRigths=new ArrayList<TblRoleMenuRights>();
                        for(String obj : menuId)
                        {
                            TblRoleMenuRights tblRoleMenuRights=new TblRoleMenuRights();
                            tblRoleMenuRights.setMenuId(Integer.parseInt(obj));
                            tblRoleMenuRights.setRollId(roleId);
                            lstRoleMenuRigths.add(tblRoleMenuRights);
                        }
                        objManageUserRights.assignRoleRights(lstRoleMenuRigths, roleId);

                    }
                 }
                 catch(Exception ex)
                 {
                     successMsg="fail";
                     ex.printStackTrace();
                 }
                response.sendRedirect(contextPath+"/admin/OAndMRoleGrid.jsp?roleRightsmsg="+successMsg+"&userTypeid=20");
            }
            else if(action != null && action.equalsIgnoreCase("Update Role"))
            {

                TblRoleMaster tblRoleMaster = new TblRoleMaster();
                String successMsg="success";
                String menuId[]=null;
                 try
                 {
                    if (roleId != 0 && request.getParameter("role") != null && request.getParameter("role") != "")
                    {
                        tblRoleMaster.setRollName(request.getParameter("role"));
                        tblRoleMaster.setStatus(1); // To set default active
                        tblRoleMaster.setRollId(roleId);
                        objManageUserRights.updateRoles(tblRoleMaster);
                    }

                    if(request.getParameter("chk")!=null && roleId != 0)
                    {
                        menuId=request.getParameterValues("chk");
                        List<TblRoleMenuRights> lstRoleMenuRigths=new ArrayList<TblRoleMenuRights>();
                        for(String obj : menuId)
                        {
                            TblRoleMenuRights tblRoleMenuRights=new TblRoleMenuRights();
                            tblRoleMenuRights.setMenuId(Integer.parseInt(obj));
                            tblRoleMenuRights.setRollId(roleId);
                            lstRoleMenuRigths.add(tblRoleMenuRights);
                        }
                        objManageUserRights.assignRoleRights(lstRoleMenuRigths, roleId);
                    }
                 }
                 catch(Exception ex)
                 {
                     successMsg="fail";
                     ex.printStackTrace();
                 }
                response.sendRedirect(contextPath+"/admin/OAndMRoleGrid.jsp?roleRightsmsg="+successMsg+"&userTypeid=20");
            }
            else if (action != null && action.equalsIgnoreCase("get RoleDetails"))
            {
                int totalPages = 0;
                int totalCount = 0;
                        String rows = request.getParameter("rows");
                        String page = request.getParameter("page");
                        String sord = request.getParameter("sord");
                        String sidx = request.getParameter("sidx");
                        int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                        if(sidx.equalsIgnoreCase("") || sidx.equalsIgnoreCase(null))
                        {
                            sidx = "rollName";
                        }
                List<TblRoleMaster> roleMaster = objManageUserRights.getAllActiveRoles(offset,Integer.parseInt(rows),sidx,sord);
                List<TblRoleMaster> totvalue = objManageUserRights.getAllActiveRoles(-1,-1,sidx,sord);
                totalCount = totvalue.size();
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                    } else {
                        totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                    }

                } else {
                    totalPages = 0;
                }
                response.setContentType("text/xml;charset=UTF-8");
                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");

                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + totvalue.size() + "</records>");
                int j = 0;
                int no = Integer.parseInt(request.getParameter("page"));
                if (no == 1) {
                    j = 1;
                } else {
                    if (Integer.parseInt(rows) == 30) {
                        j = ((no - 1) * 30) + 1;
                    } else if (Integer.parseInt(rows) == 20) {
                        j = ((no - 1) * 20) + 1;
                    } else {
                        j = ((no - 1) * 10) + 1;
                    }
                }
                // be sure to put text data in CDATA
                for (int i = 0; i < roleMaster.size(); i++) {
                    out.print("<row id='" + roleMaster.get(i).getRollId() + "'>");
                    out.print("<cell>" + j + "</cell>");
                    out.print("<cell><![CDATA[" + roleMaster.get(i).getRollName() + "]]></cell>");
                    String link = "<a href=\"OAndMRoleCreation.jsp?action=Update Role&roleId="+ roleMaster.get(i).getRollId() + "\">Edit</a>";
                    out.print("<cell><![CDATA[" + link + "]]></cell>");
                    out.print("</row>");
                    j++;
                }
                out.print("</rows>");
             }
            if(action != null && action.equalsIgnoreCase("checkRoleNameExist"))
            {
                boolean nameExist=false;
                String roleName="";
                if(request.getParameter("roleName")!= null)
                    roleName=request.getParameter("roleName");
                 try
                 {
                        nameExist = objManageUserRights.checkRoleNameExist(roleName,roleId);
                 }
                 catch(Exception ex)
                 {
                     ex.printStackTrace();
                 }
                out.println(nameExist);
            }
        }
        catch(Exception ex)
         {
             ex.printStackTrace();
         }
        finally {
            out.close();
        }
        
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
