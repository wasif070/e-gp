/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCmsWcCertDoc;
import com.cptu.egp.eps.service.serviceinterface.CmsWcCertDocService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class CmsWcCertDocServiceBean {

    final Logger logger = Logger.getLogger(CmsWcCertDocServiceBean.class);
    private final static CmsWcCertDocService cmsWcCertDocService =
            (CmsWcCertDocService) AppContext.getSpringBean("CmsWcCertDocService");
    private String logUserId = "0";
    private static final String LOGGERStart = " Starts";
    private static final String LOGGEREnd = " Ends";

    public void setLogUserId(String logUserId) {
        cmsWcCertDocService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /***
     * This method inserts a TblCmsWcCertDoc object in database
     * @param tblCmsWcCertDoc
     * @return int
     */
    public int insertCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc) {
        logger.debug("insertCmsWcCertDoc : " + logUserId + LOGGERStart);
        int id = 0;
        try {
            id = cmsWcCertDocService.insertCmsWcCertDoc(tblCmsWcCertDoc);
        } catch (Exception e) {
            logger.error("insertCmsWcCertDoc : " + logUserId + " : " + e);
        }
        logger.debug("insertCmsWcCertDoc : " + logUserId + LOGGEREnd);
        return id;
    }

    /****
     * This method updates an TblCmsWcCertDoc object in DB
     * @param tblCmsWcCertDoc
     * @return boolean
     */
    public boolean updateCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc) {
        logger.debug("updateCmsWcCertDoc : " + logUserId + LOGGERStart);
        boolean isUpdate = false;
        try {
            isUpdate = cmsWcCertDocService.updateCmsWcCertDoc(tblCmsWcCertDoc);
        } catch (Exception e) {
            logger.error("updateCmsWcCertDoc : " + logUserId + " : " + e);
        }
        logger.debug("updateCmsWcCertDoc : " + logUserId + LOGGEREnd);
        return isUpdate;
    }

    /****
     * This method deletes an TblCmsWcCertDoc object in DB
     * @param tblCmsWcCertDoc
     * @return boolean
     */
    public boolean deleteCmsWcCertDoc(TblCmsWcCertDoc tblCmsWcCertDoc) {
        logger.debug("deleteCmsWcCertDoc : " + logUserId + LOGGERStart);
        boolean isDeleted = false;
        try {
            isDeleted = cmsWcCertDocService.deleteCmsWcCertDoc(tblCmsWcCertDoc);
        } catch (Exception e) {
            logger.error("deleteCmsWcCertDoc : " + logUserId + " : " + e);
        }
        logger.debug("deleteCmsWcCertDoc : " + logUserId + LOGGEREnd);
        return isDeleted;
    }

    /****
     * This method returns all TblCmsWcCertDoc objects from database
     * @return List<TblCmsWcCertDoc>
     */
    public List<TblCmsWcCertDoc> getAllCmsWcCertDoc() {
        logger.debug("getAllCmsWcCertDoc : " + logUserId + LOGGERStart);
        List<TblCmsWcCertDoc> cmsWcCertDocList = new ArrayList<TblCmsWcCertDoc>();
        try {
            cmsWcCertDocList = cmsWcCertDocService.getAllCmsWcCertDoc();
        } catch (Exception e) {
            logger.error("getAllCmsWcCertDoc : " + logUserId + " : " + e);
        }
        logger.debug("getAllCmsWcCertDoc : " + logUserId + LOGGEREnd);
        return cmsWcCertDocList;
    }

    /***
     *  This method returns no. of TblCmsWcCertDoc objects from database
     * @return long
     */
    public long getCmsWcCertDocCount() {
        logger.debug("getCmsWcCertDocCount : " + logUserId + LOGGERStart);
        long count = 0;
        try {
            count = cmsWcCertDocService.getCmsWcCertDocCount();
        } catch (Exception e) {
            logger.error("getCmsWcCertDocCount : " + logUserId + " : " + e);
        }
        logger.debug("getCmsWcCertDocCount : " + logUserId + LOGGEREnd);
        return count;
    }

    /***
     * This method returns TblCmsWcCertDoc for the given Id
     * @param int wcCertDocId
     * @return TblCmsWcCertDoc
     */
    public TblCmsWcCertDoc getCmsWcCertDoc(int wcCertDocId) {
        logger.debug("getCmsTaxConfiguration : " + logUserId + LOGGERStart);
        TblCmsWcCertDoc tblCmsWcCertDoc = new TblCmsWcCertDoc();
        try {
            tblCmsWcCertDoc = cmsWcCertDocService.getCmsWcCertDoc(wcCertDocId);
        } catch (Exception e) {
            logger.error("getCmsTaxConfiguration : " + logUserId + " : " + e);
        }
        logger.debug("getCmsTaxConfiguration : " + logUserId + LOGGEREnd);
        return tblCmsWcCertDoc;
    }

    /***
     * This method gives the list of TblCmsWcCertDoc objects for the given wcCertificateId
     * @param int wcCertificateId
     * @return List<TblCmsWcCertDoc>
     */
    public List<TblCmsWcCertDoc> getAllCmsWcCertDocForWccId(int wcCertificateId) {
        logger.debug("getAllCmsWcCertDocForWccId : " + logUserId + LOGGERStart);
        List<TblCmsWcCertDoc> cmsWcCertDocList = new ArrayList<TblCmsWcCertDoc>();
        try {
            cmsWcCertDocList = cmsWcCertDocService.getAllCmsWcCertDocForWccId(wcCertificateId);
        } catch (Exception ex) {
            logger.error("getAllCmsWcCertDocForWccId : " + logUserId + " : " + ex);
        }
        logger.debug("getAllCmsWcCertDocForWccId : " + logUserId + LOGGEREnd);
        return cmsWcCertDocList;
    }
}
