/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

/**
 *
 * @author Ramesh.Janagondakuru
 */
public class ContentVeriConfigDtBean {

    //properties
    private int contentVerificationId;
    private String isPublicForum;
    private String isProcureExpert;


    //Setters &Getters
    public int getContentVerificationId() {
        return contentVerificationId;
    }

    public void setContentVerificationId(int contentVerificationId) {
        this.contentVerificationId = contentVerificationId;
    }

    public String getIsProcureExpert() {
        return isProcureExpert;
    }

    public void setIsProcureExpert(String isProcureExpert) {
        this.isProcureExpert = isProcureExpert;
    }

    public String getIsPublicForum() {
        return isPublicForum;
    }

    public void setIsPublicForum(String isPublicForum) {
        this.isPublicForum = isPublicForum;
    }



}
