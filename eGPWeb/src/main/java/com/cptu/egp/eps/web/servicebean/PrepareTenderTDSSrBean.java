/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblCorrigendumDetail;
import com.cptu.egp.eps.model.table.TblCorrigendumMaster;
import com.cptu.egp.eps.model.table.TblTenderIttHeader;
import com.cptu.egp.eps.model.table.TblTenderTdsSubClause;
import com.cptu.egp.eps.service.serviceinterface.PrepareTenderTds;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 * <b>PrepareTenderTDSSrBean</b> <Description Goes Here> Dec 4, 2010 9:14:24 PM
 * @author yanki
 */
public class PrepareTenderTDSSrBean  extends HttpServlet {

    final Logger logger = Logger.getLogger(PrepareTenderTDSSrBean.class);
    private String logUserId ="0";

    PrepareTenderTds prepareTds = (PrepareTenderTds) AppContext.getSpringBean("prepareTenderTdsService");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        logger.debug("processRequest : "+logUserId+" Starts");
        try {
            HttpSession session = request.getSession();
            if(session.getAttribute("userId")!=null){
                logUserId = session.getAttribute("userId").toString();
                prepareTds.setUserId(logUserId);
            }
            prepareTds.setUserId(logUserId);
            if(request.getParameter("action") != null){
                if(request.getParameter("action").equalsIgnoreCase("update")) {
                    logger.debug("processRequest action : update : "+logUserId+" Starts");
                    String tdsInfo = request.getParameter("tdsInfo");
                    if(tdsInfo != null){
                        tdsInfo = handleSpecialCharHere(tdsInfo);
                    }
                    int tdsSubClauseId = Integer.parseInt(request.getParameter("tdsSubClauseId"));
                    int headerId = Integer.parseInt(request.getParameter("headerId"));
                    String insertCorrigendum=request.getParameter("insertcorrigendum");
                    if(insertCorrigendum==null){
                    
                    TblTenderTdsSubClause tblTenderTdsSubClause = new TblTenderTdsSubClause();

                    tblTenderTdsSubClause.setTenderTdsSubClauseId(tdsSubClauseId);
                    tblTenderTdsSubClause.setTblTenderIttHeader(new TblTenderIttHeader(headerId));
                    tblTenderTdsSubClause.setTenderTdsClauseName(tdsInfo);
                    
                    boolean flag = prepareTds.updateTDSSubClause(tblTenderTdsSubClause);
                    if(flag == true){
                        out.print("OK");
                    } else {
                        out.print("error");
                    }
                    }
                    else if("true".equalsIgnoreCase(insertCorrigendum)){
                        String corrigendumFieldName=request.getParameter("hd_corigendum_field");
                        String corrigendumFiledOldValue=request.getParameter("hd_corrigendum_field_oldvalue");
                        System.out.println("hd_corrigendum_field_oldvalue:"+corrigendumFiledOldValue);
                        System.out.println("field name:"+corrigendumFieldName);
                        System.out.println("corri id"+request.getParameter("corriId"));
                        System.out.println(tdsInfo.replaceAll(" ","").replaceAll("/",""));
                        System.out.println(corrigendumFiledOldValue.replaceAll(" ","").replaceAll("/",""));

                        System.out.println(tdsInfo.replaceAll(" ","").replaceAll("/","").equalsIgnoreCase(corrigendumFiledOldValue.replaceAll(" ","").replaceAll("/","")));
                        String sCorriDetailId="0";
                        if(request.getParameter("corriDetailId")!=null){
                            sCorriDetailId=request.getParameter("corriDetailId").trim();
                        }
                        int corriDetailId=Integer.parseInt(sCorriDetailId);
                      System.out.println("corri detail id"+corriDetailId);
                     if(!tdsInfo.replaceAll(" ","").replaceAll("/","").equalsIgnoreCase(corrigendumFiledOldValue.replaceAll(" ","").replaceAll("/",""))){
                        if(corrigendumFiledOldValue!=null)
                            corrigendumFiledOldValue=handleSpecialCharHere(corrigendumFiledOldValue);
                        int corriId=Integer.parseInt(request.getParameter("corriId"));
                        TblCorrigendumDetail tblCorrigendumDetail=new TblCorrigendumDetail();
                        tblCorrigendumDetail.setTblCorrigendumMaster(new TblCorrigendumMaster(corriId));
                        tblCorrigendumDetail.setFieldName(corrigendumFieldName);
                        tblCorrigendumDetail.setOldValue(corrigendumFiledOldValue);
                        tblCorrigendumDetail.setNewValue(tdsInfo);
                        tblCorrigendumDetail.setCorriDetailId(corriDetailId);

                        boolean flag=prepareTds.saveOrUpdateCorrigendumDetail(tblCorrigendumDetail);
                        if(flag)
                        {
                            out.print("OK corrigendum");
                        }else {
                            out.print("error corrigendum");
                        }

                        }
                        else
                        {
                            out.print("nothing to update");
                        }



                    }
                    logger.debug("processRequest action : update : "+logUserId+" ends");
                } else if (request.getParameter("action").equalsIgnoreCase("add")) {
                    logger.debug("processRequest action : add : "+logUserId+" starts");
                    String tdsInfo = request.getParameter("tdsInfo");
                    if(tdsInfo != null){
                        tdsInfo = handleSpecialCharHere(tdsInfo);
                    }
                    int headerId = Integer.parseInt(request.getParameter("headerId"));
                    int orderNo = Integer.parseInt(request.getParameter("orderNo"));
                    
                    TblTenderTdsSubClause tblTenderTdsSubClause = new TblTenderTdsSubClause();
                    tblTenderTdsSubClause.setOrderNumber(Short.parseShort((orderNo+1)+""));
                    tblTenderTdsSubClause.setTblTenderIttHeader(new TblTenderIttHeader(headerId));
                    tblTenderTdsSubClause.setTenderTdsClauseName(tdsInfo);
                    tblTenderTdsSubClause.setTenderIttRefrence(0);

                    boolean flag = prepareTds.save(tblTenderTdsSubClause);
                    if(flag == true) {
                        out.print("OK");
                    } else {
                        out.print("error");
                    }
                    logger.debug("processRequest action : add : "+logUserId+" ends");
                } else if (request.getParameter("action").equalsIgnoreCase("delete")) {
                    logger.debug("processRequest action : delete : "+logUserId+" starts");
                    int tdsSubClauseId = Integer.parseInt(request.getParameter("tdsSubClauseId"));
                    boolean flag = prepareTds.delete(tdsSubClauseId);
                    if(flag == true) {
                        out.print("OK");
                    } else {
                        out.print("error");
                    }
                    logger.debug("processRequest action : delete : "+logUserId+" ends");
                }
                 if(request.getParameter("action").equalsIgnoreCase("updateAll")) {
                     
                     boolean flag = updateAll(request,prepareTds);
                     if(flag){
                         response.sendRedirect("/officer/TenderTDSDashBoard.jsp?tenderId="+request.getParameter("tenderId")+"&tenderStdId="+request.getParameter("tenderStdId")+"&sectionId="+request.getParameter("sectionId"));
                       
                     }else{
                         response.sendRedirect("/officer/PrepareTenderTDS.jsp?ittHeaderId="+ request.getParameter("ittHeaderId") +"&sectionId="+ request.getParameter("sectionId") +"&tenderId="+ request.getParameter("tenderId") +"&tenderStdId="+ request.getParameter("tenderStdId") +"&edit=true&porlId="+request.getParameter("porlId")+"&msg=error");
                     }
                 }
            }
        } catch(Exception ex) {
            logger.error("setSubcontractDetails : "+logUserId+" : "+ex.toString());
        }
        logger.debug("setSubcontractDetails : "+logUserId+" Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public List<SPTenderCommonData> getTDSSubClause(int ittHeaderId){
        logger.debug("getTDSSubClause : "+logUserId+" Starts");
        List<SPTenderCommonData> tblTdsSubClause = null;
        try{
            tblTdsSubClause = prepareTds.getTenderTDSSubClause(ittHeaderId);
        }catch(Exception ex){
            logger.error("getTDSSubClause : "+logUserId+" : "+ex.toString());
        }
        logger.debug("getTDSSubClause : "+logUserId+" Ends");
        return tblTdsSubClause ;
    }
    
    public static String handleSpecialCharHere(String str){
        str = str.replaceAll("'", "&rsquo;");
        return str;
    }
    
    public boolean updateAll(HttpServletRequest request, PrepareTenderTds prepareTds){
        int i_totalCnt = 0;
        String s_tdsInfo="";
        boolean flag = false;
        String headerId = "";
        int orderNo = 0;
        String s_headId = "";
        String[] data = null;
        
        i_totalCnt = Integer.parseInt(request.getParameter("total"));
        List<TblTenderTdsSubClause> updateList = new ArrayList<TblTenderTdsSubClause>();
        List<TblTenderTdsSubClause> addList = new ArrayList<TblTenderTdsSubClause>();
        for (int i = 1; i <= i_totalCnt; i++) {
            TblTenderTdsSubClause tblTenderTdsSubClause = new TblTenderTdsSubClause();
            headerId = request.getParameter("hd_tds_"+i);
            s_tdsInfo = request.getParameter("tds_"+i);
            if(headerId.contains("_")){
            data = headerId.split("_");
            tblTenderTdsSubClause.setTenderTdsSubClauseId(Integer.parseInt(data[3]));
            tblTenderTdsSubClause.setTblTenderIttHeader(new TblTenderIttHeader(Integer.parseInt(data[3])));
            tblTenderTdsSubClause.setTenderTdsClauseName(s_tdsInfo);
            updateList.add(tblTenderTdsSubClause);
            }else{
            tblTenderTdsSubClause.setOrderNumber(Short.parseShort((i)+""));
            tblTenderTdsSubClause.setTblTenderIttHeader(new TblTenderIttHeader(Integer.parseInt(headerId.trim())));
            tblTenderTdsSubClause.setTenderTdsClauseName(s_tdsInfo);
            tblTenderTdsSubClause.setTenderIttRefrence(0);
            addList.add(tblTenderTdsSubClause);
            }
        }
        if(!updateList.isEmpty()){
            flag  =prepareTds.updateAllClause(updateList);
        }
        if(!addList.isEmpty() && flag){
            flag  =prepareTds.insertAllClause(addList);
        }
        return flag;
    }
}
