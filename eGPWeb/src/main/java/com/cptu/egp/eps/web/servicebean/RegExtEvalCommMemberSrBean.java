/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblExternalMemInfo;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
import com.cptu.egp.eps.service.serviceimpl.TblExternalMemInfoServiceImpl;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.web.databean.RegExtEvalCommMemberDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class RegExtEvalCommMemberSrBean {

    static final Logger logger = Logger.getLogger(RegExtEvalCommMemberSrBean.class);
    private String logUserId = "0";
    private TblExternalMemInfoServiceImpl tblExternalMemInfoServiceImpl = (TblExternalMemInfoServiceImpl) AppContext.getSpringBean("TblExternalMemInfoServiceImpl");
    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String getSortOrder = "";
    private String sortCol = "";
    private AuditTrail auditTrail;

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
        tblExternalMemInfoServiceImpl.setUserId(logUserId);
        commonService.setUserId(logUserId);
    }

    public String getSortOrder() {
        return getSortOrder;
    }

    public void setSortOrder(String getSortOrder) {
        this.getSortOrder = getSortOrder;
    }

    public String getSortCol() {
        return sortCol;
    }

    public void setSortCol(String sortCol) {
        this.sortCol = sortCol;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public boolean isSearch() {
        return _search;
    }

    public void setSearch(boolean _search) {
        this._search = _search;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getOp_ENUM() {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM) {
        this.op_ENUM = op_ENUM;
    }

    /**
     * Register External Member Info
     * @param regExtEvalCommMemberDtBean
     * @param password
     * @return true if member created successfully else false
     */
    public boolean insertInToTblExternalMemInfo(RegExtEvalCommMemberDtBean regExtEvalCommMemberDtBean, String password) {
        logger.debug("insertInToTblExternalMemInfo : " + logUserId + " Starts");
        boolean bSuccess = false;
        TblLoginMaster loginMaster = new TblLoginMaster();
        loginMaster.setEmailId(regExtEvalCommMemberDtBean.getEmailId());
        loginMaster.setPassword(regExtEvalCommMemberDtBean.getPassword());
        loginMaster.setHintQuestion("");
        loginMaster.setHintAnswer("");
        loginMaster.setRegistrationType("External Member");
        loginMaster.setIsJvca("No");
        loginMaster.setBusinessCountryName("bangladesh");
        loginMaster.setNextScreen("ChangePassword");
        loginMaster.setTblUserTypeMaster(new TblUserTypeMaster((byte) 14));
        loginMaster.setIsEmailVerified("yes");
        loginMaster.setFailedAttempt((byte) 0);
        loginMaster.setValidUpTo(null);
        loginMaster.setResetPasswordCode("");
        loginMaster.setIsPasswordReset("no");
        loginMaster.setRegisteredDate(new Date());
        loginMaster.setFirstLogin("yes");
        loginMaster.setStatus("Approved");
        loginMaster.setNationality("Bangladesi");
        if (tblExternalMemInfoServiceImpl.insertInToTblLoginMaster(loginMaster)) {
            TblExternalMemInfo tblExternalMemInfo = _toTblExternalMemInfo(regExtEvalCommMemberDtBean);
            setUserId(loginMaster.getUserId());
            regExtEvalCommMemberDtBean.setUserId(loginMaster.getUserId());
            tblExternalMemInfo.setTblLoginMaster(loginMaster);
            if (regExtEvalCommMemberDtBean.getNationalId() == null || regExtEvalCommMemberDtBean.getNationalId().trim().equals("")) {
                tblExternalMemInfo.setNationalId("");
            }
            if (regExtEvalCommMemberDtBean.getPhoneNo() == null || regExtEvalCommMemberDtBean.getPhoneNo().trim().equals("")) {
                tblExternalMemInfo.setPhoneNo("");
            }
            if (regExtEvalCommMemberDtBean.getMobileNo() == null || regExtEvalCommMemberDtBean.getMobileNo().trim().equals("")) {
                tblExternalMemInfo.setMobileNo("");
            }
            tblExternalMemInfo.setCreationDt(new Date());
            if (tblExternalMemInfoServiceImpl.insertInToTblExternalMemInfo(tblExternalMemInfo)) {
                if (sendMailAndSMS(regExtEvalCommMemberDtBean.getEmailId(), password)) {
                    bSuccess = true;
                } else {
                    bSuccess = false;
                }
            } else {
                bSuccess = false;
            }
        } else {
            bSuccess = false;
        }
        logger.debug("insertInToTblExternalMemInfo : " + logUserId + " Ends");
        return bSuccess;
    }
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

     /**
     * copies properties
     * @param regextEvalCommMemberDtBean
     * @return Object with copied properties
     */
    public TblExternalMemInfo _toTblExternalMemInfo(RegExtEvalCommMemberDtBean regextEvalCommMemberDtBean) {
        logger.debug("_toTblExternalMemInfo : " + logUserId + " Starts");
        TblExternalMemInfo tblExternalMemInfo = new TblExternalMemInfo();
        BeanUtils.copyProperties(regextEvalCommMemberDtBean, tblExternalMemInfo);
        logger.debug("_toTblExternalMemInfo : " + logUserId + " Ends");
        return tblExternalMemInfo;
    }

     /**
     * Send Sms and mail
     * @param mailId
     * @param password
     * @return true if mail sent successfully
     */
    private boolean sendMailAndSMS(String mailId, String password) {
        logger.debug("sendMailAndSMS : " + logUserId + " Starts");
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            MailContentUtility mailContentUtility = new MailContentUtility();

            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub("e-GP System: Your e-GP Profile");
            sendMessageUtil.setEmailMessage(mailContentUtility.getExternalUserMailContent(mailId, password));
            try {
                sendMessageUtil.sendEmail();
            } catch (Exception e) {
                logger.error("sendMailAndSMS : " + logUserId + " : " + e);

            }
            mailSent = true;
        } catch (Exception ex) {
            logger.error("sendMailAndSMS : " + logUserId + " : " + ex);
        }
        logger.debug("sendMailAndSMS : " + logUserId + " Ends");
        return mailSent;
    }

    /**
     * verify unique email id
     * @param mailId - mail id of user
     * @return messages - if email id already exist
     * e-mail ID already exist, Please enter another e-mail ID
     * else ok
     */
    public String verifyMail(String mailId) {
        logger.debug("verifyMail : " + logUserId + " Starts");
        String msg = "";
        if (!mailId.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            msg = "Please enter valid e-mail ID";
        } else {
            msg = commonService.verifyMail(mailId);
        }
        logger.debug("verifyMail : " + logUserId + " Ends");
        return msg;
    }

    /**
     * Fetching information for particular External Member from userId
     * @param uId
     * @return object with information of particular External Member
     */
    public List<Object[]> getExtCommListing(int uId) {
        return tblExternalMemInfoServiceImpl.detailsExtMem(uId);
    }
    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        tblExternalMemInfoServiceImpl.setAuditTrail(auditTrail);
    }

    /**
     * updating information for External Member
     * @param extEvalCommMemberDtBean
     * @return true if updated successfully
     * @throws ParseException
     */
    public boolean updateExtComm(RegExtEvalCommMemberDtBean extEvalCommMemberDtBean) throws ParseException {
        logger.debug("updateExtComm : " + logUserId + " Starts");
        TblExternalMemInfo tblExternalMemInfo = _toTblExternalMemInfo(extEvalCommMemberDtBean);
        tblExternalMemInfo.setTblLoginMaster(new TblLoginMaster(extEvalCommMemberDtBean.getUserId()));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date d = dateFormat.parse(extEvalCommMemberDtBean.getCreationDtString());
        tblExternalMemInfo.setCreationDt(d);
        if (extEvalCommMemberDtBean.getNationalId() == null || extEvalCommMemberDtBean.getNationalId().trim().equals("")) {
            tblExternalMemInfo.setNationalId("");
        }
        if (extEvalCommMemberDtBean.getPhoneNo() == null || extEvalCommMemberDtBean.getPhoneNo().trim().equals("")) {
            tblExternalMemInfo.setPhoneNo("");
        }
        if (extEvalCommMemberDtBean.getMobileNo() == null || extEvalCommMemberDtBean.getMobileNo().trim().equals("")) {
            tblExternalMemInfo.setMobileNo("");
        }
        logger.debug("updateExtComm : " + logUserId + " Ends");
        return tblExternalMemInfoServiceImpl.updateExternalMem(tblExternalMemInfo);
    }

    /**
     * Fetching information for External Member with Sorting
     * @param ascClause
     * @return object with information about External Member
     * @throws Exception
     */
    public List<Object[]> getExternalMemberList(String ascClause) throws Exception {
        return tblExternalMemInfoServiceImpl.findExternalMasterList(getOffset(), getLimit(), ascClause);
    }

    /**
     * Fetching information for External Member
     * @return number of records for External Members
     * @throws Exception
     */
    public long getAllCountOfExternalMember() throws Exception {
        return tblExternalMemInfoServiceImpl.getAllCountOfExternalMember();
    }

    /**
     * Fetching information for External Member with Search
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return object with information about External Member
     */
    public List<Object[]> getExternalMemberList_Search(String searchField, String searchString, String searchOper) throws Exception {
        return tblExternalMemInfoServiceImpl.findExternalMemberList_Search(getOffset(), getLimit(), searchField, searchString, searchOper);
    }

    /**
     * Fetching information for External Member with Search
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     */
    public long getAllCountOfExternalMember(String searchField, String searchString, String searchOper) throws Exception {
        return tblExternalMemInfoServiceImpl.getAllCountOfExternalMember(searchField, searchString, searchOper);
    }
}
