/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

/**
 *
 * @author Administrator
 */
public class APPDtBean {

    public APPDtBean() {
    }

    private String fieldName1;
    private String fieldName2;
    private String fieldName3;
    private String fieldName4;
    private String fieldName5;
    private String fieldName6;

    public String getFieldName1() {
        return fieldName1;
    }

    public void setFieldName1(String fieldName1) {
        this.fieldName1 = fieldName1;
    }

    public String getFieldName2() {
        return fieldName2;
    }

    public void setFieldName2(String fieldName2) {
        this.fieldName2 = fieldName2;
    }

    public String getFieldName3() {
        return fieldName3;
    }

    public void setFieldName3(String fieldName3) {
        this.fieldName3 = fieldName3;
    }

    public String getFieldName4() {
        return fieldName4;
    }

    public void setFieldName4(String fieldName4) {
        this.fieldName4 = fieldName4;
    }

    public String getFieldName5() {
        return fieldName5;
    }

    public void setFieldName5(String fieldName5) {
        this.fieldName5 = fieldName5;
    }

    public String getFieldName6() {
        return fieldName6;
    }

    public void setFieldName6(String fieldName6) {
        this.fieldName6 = fieldName6;
    }

    public APPDtBean(String fieldName1,String fieldName2,String fieldName3,String fieldName4,String fieldName5,String fieldName6) {
        this.fieldName1 = fieldName1;
        this.fieldName2 = fieldName2;
        this.fieldName3 = fieldName3;
        this.fieldName4 = fieldName4;
        this.fieldName5 = fieldName5;
        this.fieldName6 = fieldName6;
    }

}
