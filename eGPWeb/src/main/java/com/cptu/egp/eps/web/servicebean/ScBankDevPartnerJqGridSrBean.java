/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;
//Added by Proshanto Kumar Saha,Dohatec
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
//End by Proshanto Kumar Saha,Dohatec
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**ScBankDevPartnerJqGridSrBean
 *
 * @author rishita
 */
public class ScBankDevPartnerJqGridSrBean extends HttpServlet {
    static final Logger LOGGER = Logger.getLogger(ScBankCreationSrBean.class);
    private String logUSerId = "0";

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest "+logUSerId+" Starts:");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            ScBankDevpartnerService scbankdevpartner = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
            scbankdevpartner.setUserId(logUSerId);
            String sBankDevelopeid = request.getParameter("sbankdevelopId");
            String strPageNo = request.getParameter("pageNo");
            int pageNo = Integer.parseInt(strPageNo);
            String strOffset = request.getParameter("size");
            int recordOffset = Integer.parseInt(strOffset);
           // int offset = ((pageNo - 1) * recordOffset);
            String branch = "", thana = "", city ="", district = "";
            branch = request.getParameter("branch");
            thana = request.getParameter("thana");
            city = request.getParameter("city");
            district = request.getParameter("district");
            //Start By proshanto Kumar Saha,Dohatec
            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            List<SPCommonSearchData> getBankinfo = commonSearchService.searchData("getBankDetails", sBankDevelopeid,branch,thana,city,district, Integer.toString(pageNo), Integer.toString(recordOffset), null, null);
            List<SPCommonSearchData> getBankinfo1=commonSearchService.searchData("getBankDetailsSize", sBankDevelopeid,branch,thana,city,district, null, null, null, null);
            int totalCount = getBankinfo1.size();
            String styleClass = "";
            if (getBankinfo != null && !getBankinfo.isEmpty()) {
                    for (int i = 0; i < getBankinfo.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }

                        out.print("<tr class='" + styleClass + "'>");
                        String link;
                        if(getBankinfo.get(i).getFieldName6().toString().trim().equals("")){
                            link = "<b>Address: </b>" + getBankinfo.get(i).getFieldName5() + ",<br/><b>City: </b>" + getBankinfo.get(i).getFieldName7() + ",<br/><b>Dzongkhag / District: </b>" + getBankinfo.get(i).getFieldName8()+" - "+ getBankinfo.get(i).getFieldName9() +",<br/><b>Country: </b>" + getBankinfo.get(i).getFieldName1();
                        }else{
                            link = "<b>Address: </b>" + getBankinfo.get(i).getFieldName5() + ",<br/><b>City: </b>" + getBankinfo.get(i).getFieldName7() + ",<br/><b>Thana: </b>" + getBankinfo.get(i).getFieldName6() + ",<br/><b>Dzongkhag / District: </b>" + getBankinfo.get(i).getFieldName8() +" - "+ getBankinfo.get(i).getFieldName9() +",<br/><b>Country: </b>" + getBankinfo.get(i).getFieldName1();
                        }

                        out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 20 + (i + 1)) + "</td>");
                        out.print("<td class=\"t-align-left\">" + getBankinfo.get(i).getFieldName10() + "</td>");
                        out.print("<td class=\"t-align-left\">" + getBankinfo.get(i).getFieldName2() + "</td>");
                        out.print("<td class=\"t-align-left\">" + link + "</td>");
                        if(getBankinfo.get(i).getFieldName3().equals("")){
                            out.print("<td class=\"t-align-left\">-</td>");
                        }else{
                            out.print("<td class=\"t-align-center\">" + getBankinfo.get(i).getFieldName3() + "</td>");
                        }
                        if(getBankinfo.get(i).getFieldName4().equals("")){
                            out.print("<td class=\"t-align-center\">-</td>");
                        }else{
                            out.print("<td class=\"t-align-center\">" + getBankinfo.get(i).getFieldName4() + "</td>");
                        }
                        out.print("</tr>");
                    }
                    int totalPages = 1;

                    if (!getBankinfo.isEmpty()) {
                        int count = totalCount;
                        totalPages = (int) Math.ceil(Math.ceil(count) / recordOffset);
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

                } else {
                    out.print("<tr>");
                    out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"6\" style=\"color: red;font-weight: bold\">No Records Found</td>");
                    out.print("</tr>");
                     //End By proshanto Kumar Saha,Dohatec
                }
            LOGGER.debug("processRequest "+logUSerId+" Ends:");
        } catch (Exception e) {
            LOGGER.error("processRequest "+logUSerId+" :"+e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
