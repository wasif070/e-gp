/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsTaxConfig;
import com.cptu.egp.eps.web.servicebean.CmsTaxConfigBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class TaxConfigServlet extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(TaxConfigServlet.class);
    String logUserId = "0";
    static final String STARTS = " Starts";
    static final String ENDS = " Ends";
    static final String EMPTY_STRING = "";
    static final String YES = "yes";
    static final String NO = "no";
    private static CmsTaxConfigBean cmsTaxConfigBean = new CmsTaxConfigBean();
    private String isAdvanceApplicable;
    private double advancePercent = 0;
    private double advanceAdjustment = 0;
    private String isRetentionApplicable;
    private double retentionPercent = 0;
    private double retentionAdjustment = 0;
    private String isPgapplicable;
    private double pgPercent = 0;
    private double pgAdjustment = 0;
    private String isVatapplicable;
    private double vatPercent = 0;
    private String isBonusApplicable;
    private double bonusPercent = 0;
    private double bonusAfter = 0;
    private String isLdapplicable;
    private double ldPercent = 0;
    private double ldAfter = 0;
    private String isPenaltyApplicable;
    private double penaltyPercent = 0;
    private double penaltyAfter = 0;
    private int createdBy;

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest : " + STARTS);
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        if (session.getAttribute("userId") != null) {
            cmsTaxConfigBean.setLogUserId(session.getAttribute("userId").toString());
        }
        PrintWriter out = response.getWriter();
        boolean flag = false;//creation = true, updation = false;
        TblCmsTaxConfig oldCmsTaxConfig = null;
        TblCmsTaxConfig currentCmsTaxConfig = new TblCmsTaxConfig();
        try {
            int procurementNatureType = 0;
            int taxConfigId = 0;
            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                createdBy = Integer.parseInt(session.getAttribute("userId").toString());
            }
            if (request.getParameter("selectAdvanceApplicable") != null) {
                isAdvanceApplicable = request.getParameter("selectAdvanceApplicable");
            }
            if (request.getParameter("txtAdvanceApplicablePerFirst") != null) {
                advancePercent = Double.parseDouble(request.getParameter("txtAdvanceApplicablePerFirst"));
            }
            if (request.getParameter("txtAdvanceApplicablePerSecond") != null) {
                advanceAdjustment = Double.parseDouble(request.getParameter("txtAdvanceApplicablePerSecond"));
            }
            if (request.getParameter("selectRetentionMoney") != null) {
                isRetentionApplicable = request.getParameter("selectRetentionMoney");
            }
            if (request.getParameter("txtRetentionMoneyPerFirst") != null) {
                retentionPercent = Double.parseDouble(request.getParameter("txtRetentionMoneyPerFirst"));
            }
            if (request.getParameter("txtRetentionMoneyPerSecond") != null) {
                retentionAdjustment = Double.parseDouble(request.getParameter("txtRetentionMoneyPerSecond"));
            }
            if (request.getParameter("selectPgApplicable") != null) {
                isPgapplicable = request.getParameter("selectPgApplicable");
            }
            if (request.getParameter("txtPgApplicablePerFirst") != null) {
                pgPercent = Double.parseDouble(request.getParameter("txtPgApplicablePerFirst"));
            }
            if (request.getParameter("txtPgApplicablePerSecond") != null) {
                pgAdjustment = Double.parseDouble(request.getParameter("txtPgApplicablePerSecond"));
            }
            if (request.getParameter("selectVatApplicable") != null) {
                isVatapplicable = request.getParameter("selectVatApplicable");
            }
            if (request.getParameter("txtVatApplicablePerFirst") != null) {
                vatPercent = Double.parseDouble(request.getParameter("txtVatApplicablePerFirst"));
            }
            if (request.getParameter("selectBonusApplicable") != null) {
                isBonusApplicable = request.getParameter("selectBonusApplicable");
            }
            if (request.getParameter("txtBonusApplicablePerFirst") != null) {
                bonusPercent = Double.parseDouble(request.getParameter("txtBonusApplicablePerFirst"));
            }
            if (request.getParameter("txtBonusApplicablePerSecond") != null) {
                bonusAfter = Double.parseDouble(request.getParameter("txtBonusApplicablePerSecond"));
            }
            if (request.getParameter("selectLdApplicable") != null) {
                isLdapplicable = request.getParameter("selectLdApplicable");
            }
            if (request.getParameter("txtLdApplicablePerFirst") != null) {
                ldPercent = Double.parseDouble(request.getParameter("txtLdApplicablePerFirst"));
            }
            if (request.getParameter("txtLdApplicablePerSecond") != null) {
                ldAfter = Double.parseDouble(request.getParameter("txtLdApplicablePerSecond"));
            }
            if (request.getParameter("selectPenaltyApplicable") != null) {
                isPenaltyApplicable = request.getParameter("selectPenaltyApplicable");
            }
            if (request.getParameter("txtPenaltyApplicablePerFirst") != null) {
                penaltyPercent = Double.parseDouble(request.getParameter("txtPenaltyApplicablePerFirst"));
            }
            if (request.getParameter("txtPenaltyApplicablePerSecond") != null) {
                penaltyAfter = Double.parseDouble(request.getParameter("txtPenaltyApplicablePerSecond"));
            }
            if (request.getParameter("natureType") != null) {
                procurementNatureType = Byte.valueOf(request.getParameter("natureType"));
            }
            if (request.getParameter("taxConfigId") != null) {
                taxConfigId = Integer.parseInt(request.getParameter("taxConfigId"));
                if (taxConfigId != 0) {
                    oldCmsTaxConfig = cmsTaxConfigBean.getCmsTaxConfiguration(taxConfigId);
                    oldCmsTaxConfig.setIsCurrent(NO);
                    cmsTaxConfigBean.updateCmsTaxConfiguration(oldCmsTaxConfig);
                    flag = false;
                }
            } else {
                flag = true;
            }
            currentCmsTaxConfig.setAdvanceAdjustment(BigDecimal.valueOf(advanceAdjustment));
            currentCmsTaxConfig.setAdvancePercent(BigDecimal.valueOf(advancePercent));
            currentCmsTaxConfig.setBonusAfter(BigDecimal.valueOf(bonusAfter));
            currentCmsTaxConfig.setBonusPercent(BigDecimal.valueOf(bonusPercent));
            currentCmsTaxConfig.setCreatedBy(createdBy);
            currentCmsTaxConfig.setCreatedDate(getCurrentDate());
            currentCmsTaxConfig.setIsAdvanceApplicable(isAdvanceApplicable);
            currentCmsTaxConfig.setIsBonusApplicable(isBonusApplicable);
            currentCmsTaxConfig.setIsCurrent(YES);
            currentCmsTaxConfig.setIsLdapplicable(isLdapplicable);
            currentCmsTaxConfig.setIsPenaltyApplicable(isPenaltyApplicable);
            currentCmsTaxConfig.setIsPgapplicable(isPgapplicable);
            currentCmsTaxConfig.setIsRetentionApplicable(isRetentionApplicable);
            currentCmsTaxConfig.setIsVatapplicable(isVatapplicable);
            currentCmsTaxConfig.setLdAfter(BigDecimal.valueOf(ldAfter));
            currentCmsTaxConfig.setLdPercent(BigDecimal.valueOf(ldPercent));
            currentCmsTaxConfig.setPenaltyAfter(BigDecimal.valueOf(penaltyAfter));
            currentCmsTaxConfig.setPenaltyPercent(BigDecimal.valueOf(penaltyPercent));
            currentCmsTaxConfig.setPgAdjustment(BigDecimal.valueOf(pgAdjustment));
            currentCmsTaxConfig.setPgPercent(BigDecimal.valueOf(pgPercent));
            currentCmsTaxConfig.setProcurementNatureId(procurementNatureType);
            currentCmsTaxConfig.setRetentionAdjustment(BigDecimal.valueOf(retentionAdjustment));
            currentCmsTaxConfig.setRetentionPercent(BigDecimal.valueOf(retentionPercent));
            currentCmsTaxConfig.setVatPercent(BigDecimal.valueOf(vatPercent));
            cmsTaxConfigBean.insertCmsTaxConfiguration(currentCmsTaxConfig);
            currentCmsTaxConfig = cmsTaxConfigBean.getLatestCmsTaxConfiguration(procurementNatureType);
            //response.sendRedirect(request.getContextPath() + "/admin/ViewTaxConfiguration.jsp?flag=" + flag + "&taxConfigId=" + currentCmsTaxConfig.getTaxConfigId());
            response.sendRedirect("/admin/ViewTaxConfiguration.jsp?flag=" + flag);

        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : " + ENDS);
    }

    /***
     * This method returns the current date
     * @return Date
     */
    public Date getCurrentDate() {
        LOGGER.debug("getCurrentDate : " + STARTS);
        Calendar currentDate = Calendar.getInstance();
        Date dateNow = (currentDate.getTime());
        LOGGER.debug("getCurrentDate : " + ENDS);
        return dateNow;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
