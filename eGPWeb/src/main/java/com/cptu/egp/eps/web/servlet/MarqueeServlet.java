/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rishita
 */
public class MarqueeServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * Used for get Marquee list or get specific Marquee details.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String funName = request.getParameter("funName");
            String sessionId ="0";
                if(request.getSession().getAttribute("userId")!=null){
                    sessionId= request.getSession().getAttribute("userTypeId").toString();
                }
            if (funName != null && funName.equalsIgnoreCase("viewMarquee")) {
                String styleClass = "";
                String strPageNo = request.getParameter("pageNo");
                //System.out.println("Page No: " + strPageNo);
                int pageNo = Integer.parseInt(strPageNo);
                String strOffset = request.getParameter("size");
                int recordOffset = Integer.parseInt(strOffset);

                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                List<SPCommonSearchData> list = commonSearchService.searchData("getmarqueelisting", strPageNo, null, sessionId, null, null, null, null,null,null);
                /*if(sessionId!=1){
                        for(int j=0;j<list.size();j++){
                            if(sessionId==0){
                                if("2".equalsIgnoreCase(list.get(j).getFieldName10().trim())){
                                    list.remove(j);
                                }
                            }else{
                                if(sessionId!=Integer.parseInt(list.get(j).getFieldName9().trim())){
                                    list.remove(j);
                                }
                            }
                        }
                    }*/
                if (list != null && !list.isEmpty()) {
                    //System.out.println("Record Size: " + tenderDetailes.size());
                    for (int i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                       // for (SPTenderCommonData sptcd : list) {
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                            out.print("<td class=\"t-align-left\">" + URLDecoder.decode(list.get(i).getFieldName3(),"UTF-8") + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getFieldName4() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getFieldName7() + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getFieldName8() + "</td>");
                            out.print("</tr>");
                        //}
                        
                    }
                    int totalPages = 0;
                    if (list.size() > 0) {
                        int totalNo = Integer.parseInt(list.get(0).getFieldName6());
                        //totalPages = (int) (Math.ceil((totalNo)) / recordOffset);
                        totalPages = (int) Math.ceil(Math.ceil(totalNo)/ recordOffset);
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                 
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                } else {
                    out.print("<tr>");

                    out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
            }
            else if(funName.equalsIgnoreCase("viewMarqueelist"))
            {
              
                    String styleClass = "";
                    String strPageNo = request.getParameter("pageNo");
                    int pageNo = Integer.parseInt(strPageNo);
                    String strOffset = request.getParameter("size");
                    int recordOffset = Integer.parseInt(strOffset);
                    
                    //TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");                                       
                    //int srNo = 0;
                    //List<SPTenderCommonData> list = tenderCommonService.returndata("getmarqueelistWithPaging", strPageNo, strOffset);
                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                    List<SPCommonSearchData> list = commonSearchService.searchData("getmarqueelistWithPaging", strPageNo, strOffset, sessionId, null, null, null, null,null,null);
                    /*if(sessionId!=1){
                        for(int j=0;j<list.size();j++){
                            if(sessionId==0){
                                if("2".equalsIgnoreCase(list.get(j).getFieldName8().trim())){
                                    list.remove(j);
                                }
                            }else{
                                if(sessionId!=Integer.parseInt(list.get(j).getFieldName7().trim())){
                                    list.remove(j);
                                }
                            }
                        }
                    }*/
                    if (list != null && !list.isEmpty()) {
                    for (int i = 0; i < list.size(); i++) {
                        if (i % 2 == 0) {
                            styleClass = "bgColor-white";
                        } else {
                            styleClass = "bgColor-Green";
                        }
                       
                            //srNo = ((Integer.parseInt(strPageNo)-1)*Integer.parseInt(strOffset)) + (i+1);
                            String streditdelete = "<a href = Marquee.jsp?mid="+list.get(i).getFieldName1()+"&op=edit>Edit</a> | <a href=javascript:void(); id = \"deleteMarquee\" onclick = \"return Delete(" + list.get(i).getFieldName1() + ");\">Delete</a>";
                            out.print("<tr class='" + styleClass + "'>");
                            out.print("<td class=\"t-align-center\">" + ((Integer.parseInt(strPageNo) - 1) * 10 + (i + 1)) + "</td>");
                            out.print("<td class=\"t-align-left\">" + URLDecoder.decode(list.get(i).getFieldName2(),"UTF-8") + "</td>");
                            out.print("<td class=\"t-align-center\">" + list.get(i).getFieldName3() + "</td>");
                            out.print("<td class=\"t-align-center\">" + streditdelete + "</td>");
                            out.print("</tr>");
                    }
                    int totalPages = 0;                    
                    if (list.size() > 0) {                        
                        if (totalPages == 0) {
                            totalPages = Integer.parseInt(list.get(0).getFieldName4());
                        }
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                } else {
                    out.print("<tr>");

                    out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }              
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
