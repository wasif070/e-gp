/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblEvalServiceForms;
import com.cptu.egp.eps.model.table.TblEvalServiceWeightage;
import com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *This service is used for update delete and view of Evaluation Passing Mark Module
 * @author Nishith
 */
public class EvalSerCertiSrBean {
        EvalServiceCriteriaService evalServiceCriteriaService = (EvalServiceCriteriaService)AppContext.getSpringBean("EvalServiceCriteriaService");

        String logUserId = "0";

        public void setLogUserId(String logUserId) {
            evalServiceCriteriaService.setLogUserId(logUserId);
        }
        final Logger logger = Logger.getLogger(EvalSerCertiSrBean.class);

        /**
         *This method is give List of Sub Criteria except qualification Criteria
        * @return in object[0] give sub criteria id and in object[2] give sub criteria name
        */
        public List<Object []> getList(){
            logger.debug("getList "+logUserId+" Starts");
            List<Object []> list = null;
            try {
                list = evalServiceCriteriaService.getCritetria();
            } catch (Exception e) {
                logger.error("getList "+logUserId+" :"+e);
        }
            logger.debug("getList "+logUserId+" Ends");
            return list;
        }
         /**
         *This method gives sub criteria of qualification criteria
         * @param criId=Criteria Id
         * @return {subCriteriaId,subCriteria}
         */
        public List<Object []> getSubCriteria(int crtiId){
            logger.debug("getSubCriteria "+logUserId+" Starts");
            List<Object []> list = null;
            try {
                list =  evalServiceCriteriaService.getSubCriteria(crtiId);
            } catch (Exception e) {
                logger.debug("getSubCriteria "+logUserId+" :"+e);
        }
            logger.debug("getSubCriteria "+logUserId+" Ends");
            return list;
        }
         /**
         *This method is used when inserting a Table
         * @param list objectList of TblEvalServiceForms
         * @return true if success else false
         */
        public boolean insertTable(List<TblEvalServiceForms> list){
            logger.debug("insertTable "+logUserId+" Starts");
            boolean flag = false;
            try {
                flag =  evalServiceCriteriaService.insertTbl(list);
            } catch (Exception e) {
                logger.error("insertTable "+logUserId+" :"+e);
        }
            logger.debug("insertTable "+logUserId+" Ends");
            return flag;
        }
        /**
         *Gives Data of TblEvalServiceForms when updating a values
         * @param tenderId
         * @return List of TblEvalServiceForms object
         */
        public List<TblEvalServiceForms> listData(int tenderId){
            logger.debug("listData "+logUserId+" Starts");
            List<TblEvalServiceForms> list = null;
            try {
                list =  evalServiceCriteriaService.listData(tenderId);
            } catch (Exception e) {
                logger.error("listData "+logUserId+" :"+e);
        }
            logger.debug("listData "+logUserId+" Ends");
            return list;
        }
        /**
         *When update 1st it will delete and at this time this method is used
         * @param tenderId = tenderId
         * @return true and false;
         */
        public boolean deleteRecord(int tenderId,String isCorri){
            logger.debug("deleteRecord "+logUserId+" Starts");
            boolean flag = false;
            try {
                flag = evalServiceCriteriaService.deleteRecord(tenderId,isCorri);
            } catch (Exception e) {
                logger.error("deleteRecord "+logUserId+" :"+e);
        }
            logger.debug("deleteRecord "+logUserId+" Ends");
            return flag;
            
        }

         /**
         *List of Sub Criteria
         * @param tenderId = tenderId
         * @return obj[] sub Criteria Id and obj[[1] Sub criteria name
         */
        public List<Object[]> getSubCriteria(){
            logger.debug("getSubCriteria "+logUserId+" Starts");
            List<Object []> list = null;
            try {
                list = evalServiceCriteriaService.getSubCriteria();
            } catch (Exception e) {
                logger.error("getSubCriteria "+logUserId+" :"+e);
        }
            logger.debug("getSubCriteria "+logUserId+" Ends");
            return list;
        }
        /**
         *Gives data of TblEvalServiceForms and TblEvalServiceCriteria for display
         * @param tenderFormId = tenderFormId
         * @return List of objects {subCriteria,maxMarks,subCriteriaId}
         */

        public List<Object[]> getEvalServiceMarks(int tenderFormId){
            logger.debug("getEvalServiceMarks "+logUserId+" Starts");
            List<Object []> list = null;
            try {
                list =  evalServiceCriteriaService.getEvalServiceMarks(tenderFormId);
            } catch (Exception e) {
                    logger.error("getEvalServiceMarks "+logUserId+" :"+e);
        }
            logger.debug("getEvalServiceMarks "+logUserId+" Ends");
            return list;
        }
        /**
         * For View the marks
         * @param tenderId tenderId
         * @return List of object obj[0] subCriteria,[1] maxMarks, [2] formId, [3] subCriteriaId, [4]MainCriteria
         */
        public List<Object[]> viewEvalSerMark(int tenderId,String isCorrigen){
            logger.debug("viewEvalSerMark "+logUserId+" Starts");
                List<Object[]> list= null;
                try {
                list =  evalServiceCriteriaService.viewEvalSerMark(tenderId,isCorrigen);
            } catch (Exception e) {
                logger.error("viewEvalSerMark "+logUserId+" :"+e);
        }
            logger.debug("viewEvalSerMark "+logUserId+" Ends");
                return list;
        }

        /**
         * Get details from tbl_EvalSerFormDetail
         * @param tenderId tenderId , userId userId, tenderFormId tenderFormId,evalBy evalBy,bId bId
         * @return List of object
         */
        public List<Object[]> getDetailsEvalSerMark(int tenderId, int userId, int tenderFormId,int evalBy,int bId){
            logger.debug("getDetailsEvalSerMark "+logUserId+" Starts");
            List<Object []> list = null;
            try {
                list = evalServiceCriteriaService.getDetailsEvalSerMark(tenderId, userId, tenderFormId,evalBy,bId);
            } catch (Exception e) {
                logger.error("getDetailsEvalSerMark "+logUserId+" :"+e);
        }
            logger.debug("getDetailsEvalSerMark "+logUserId+" Ends");
            return list;
        }

        /**
         * Get details from tbl_EvalSerFormDetail
         * @param tenderId tenderId , userId userId, tenderFormId tenderFormId,evalBy evalBy
         * @return List of object
         */
        public List<Object[]> getDetailsEvalSerMark(int tenderId, int userId, int tenderFormId,int evalBy){
            List<Object[]> list = null;
            logger.debug("getDetailsEvalSerMark "+logUserId+" Starts");
            try {
                list  = evalServiceCriteriaService.getDetailsEvalSerMark(tenderId, userId, tenderFormId,evalBy);
            } catch (Exception e) {
                logger.debug("getDetailsEvalSerMark "+logUserId+" :"+e);
        }
            logger.debug("getDetailsEvalSerMark "+logUserId+" Ends");
            return list;
        }
        /**
         * If evaluation method is technical then and then link hase to view in tenderPrep
         * @param tenderId tenderId 
         * @return long count
         */
        public long evalCertiLink(int tenderId){
            long  cnt =0;
            logger.debug("evalCertiLink "+logUserId+" Starts");
            try {
                    cnt = evalServiceCriteriaService.evalCertiLink(tenderId);
            } catch (Exception e) {
                logger.debug("evalCertiLink "+logUserId+" :"+e);
                cnt = -1;
            }
            logger.debug("evalCertiLink "+logUserId+" Ends");
            return cnt;
        }
        
        public TblEvalServiceWeightage getTblEvalServiceByTenderId(int tenderId){
            logger.debug("getTblEvalServiceByTenderId "+logUserId+" Starts");
            TblEvalServiceWeightage tblEvalServiceWeightage = new TblEvalServiceWeightage();
            try {
               tblEvalServiceWeightage = evalServiceCriteriaService.getTblEvalServiceByTenderId(tenderId); 
            } catch (Exception e) {
                logger.debug("getTblEvalServiceByTenderId "+logUserId+" :"+e);
            }
            logger.debug("getTblEvalServiceByTenderId "+logUserId+" Ends");
            return tblEvalServiceWeightage;
        }
        
        public List<Object[]> getMainCirteriaList(){
            List<Object[]> list= null;
            logger.debug("getMainCirteriaList "+logUserId+" Starts");
            try {
                list = evalServiceCriteriaService.getMainCirteriaList();
            } catch (Exception e) {
                logger.debug("getMainCirteriaList "+logUserId+" :"+e);
                e.printStackTrace();
            }
            logger.debug("getMainCirteriaList "+logUserId+" Ends");
            return list;
        }
              
}
