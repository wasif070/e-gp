/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblBidRankDetail;
import com.cptu.egp.eps.model.table.TblBidderRank;
import com.cptu.egp.eps.model.table.TblEvalRoundMaster;
import com.cptu.egp.eps.model.table.TblNegNotifyTenderer;
import com.cptu.egp.eps.model.table.TblNegQuery;
import com.cptu.egp.eps.model.table.TblNegReply;
import com.cptu.egp.eps.model.table.TblNegotiation;
import com.cptu.egp.eps.model.table.TblReportColumnMaster;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceimpl.CommonServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.EvaluationService;
import com.cptu.egp.eps.service.serviceimpl.NegotiationProcessImpl;
import com.cptu.egp.eps.service.serviceimpl.ReportCreationService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.databean.ReportGenerateDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.cptu.egp.eps.web.utility.XMLReader;

import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;

/**
 *
 * @author Administrator
 */
public class NegotiationSrBean extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
    NegotiationProcessImpl negotiationProcessImpl = (NegotiationProcessImpl) AppContext.getSpringBean("NegotiationProcessImpl");
    TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService"); //Change by dohatec for re-evaluation
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    final Logger logger = Logger.getLogger(NegotiationSrBean.class);
    private String logUserId ="0";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        
         // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer"));
            String idType="tenderId";
            int auditId=0;
            String auditAction=null;
            String moduleName=EgpModule.Negotiation.getName();
            String auditremarks= "";
            
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            negotiationProcessImpl.setLogUserId(logUserId);
            negotiationProcessImpl.setAuditTrail(objAuditTrail);
        }
        logger.debug("processRequest : "+logUserId+" Starts");

        try {
            String action = "";
            String startDate = "";
            String endDate = "";
            String details = "";
            int company = 0;
            int roundId = 0;
            String mode= "";
            int tenderId = 0;
            int userId = 0;
            String queryText = "";
            String tenRefNo = "";
            String evalCount="";

            userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");

            Date sdate = new Date();
            Date edate = new Date();

            if(request.getParameter("action") != null){
                action = request.getParameter("action");
            }

            if("create".equals(action))
            {
                logger.debug("processRequest action : create : "+logUserId+" Starts");
                if(request.getParameter("dtStartDate") != null){
                    startDate = request.getParameter("dtStartDate").toString();
                }
                if(request.getParameter("dtEndDate") != null){
                    endDate = request.getParameter("dtEndDate").toString();
                }
                try{
                    sdate = format.parse(startDate);
                    edate = format.parse(endDate);
                }catch(Exception e){
                    logger.error("Error in Date Parsing :"+e);
                }

                if(request.getParameter("txtDetails") != null){
                    details = request.getParameter("txtDetails");
                }
                if(request.getParameter("roundId_cmbNegCompName")!=null){
                    String[] arrValue = request.getParameter("roundId_cmbNegCompName").split("_");
                    if(arrValue.length==2){
                        company = Integer.parseInt(arrValue[1]);
                        roundId = Integer.parseInt(arrValue[0]);
                    }
                }
                /*if(request.getParameter("roundId")!=null){
                    roundId = Integer.parseInt(request.getParameter("roundId").toString());
                }*/

                if(request.getParameter("negModeName")!=null){
                    mode= request.getParameter("negModeName");
                }

                if(request.getParameter("tenderIdName")!=null){
                    tenderId= Integer.parseInt(request.getParameter("tenderIdName").toString());
                }
                if(request.getParameter("tenRefNo")!=null){
                    tenRefNo = request.getParameter("tenRefNo");
                }
				//Change by dohatec for re-evaluation
                 if(request.getParameter("evalCount")!=null){
                    evalCount = request.getParameter("evalCount");
                }


                TblNegotiation tblNegotiation = new TblNegotiation();
                TblNegNotifyTenderer tblNegNotifyTenderer = new TblNegNotifyTenderer();
                String pageName = "officer/NegotiationProcess.jsp?tenderId="+tenderId+"&action=create&flag=true";
                try{
                    if(roundId!=0 && company!=0){
                    // INSERT IN TO TBLNEGOTIATION
                    tblNegotiation.setTblTenderMaster(new TblTenderMaster(tenderId));
                    tblNegotiation.setNegStartDt(sdate);
                    tblNegotiation.setNegEndDt(edate);
                    tblNegotiation.setNegStatus("Pending");
                    tblNegotiation.setNegCreatedDt(new java.util.Date());
                    tblNegotiation.setNegCreatedBy(userId);
                    tblNegotiation.setNegLocDetails(details);
                    tblNegotiation.setNegRemarks("");
                    tblNegotiation.setNegMode(mode);
                    tblNegotiation.setNegFinalSub("");
                    tblNegotiation.setNegFinalSubRemarks("");
                    tblNegotiation.setNegOfficeAggree("");
                    tblNegotiation.setNegOfficerAggreeDt(new java.util.Date());
                    tblNegotiation.setNegOfficerAggreeRemarks("");
                    tblNegotiation.setRoundId(roundId);
                    tblNegotiation.setEvalCount(Integer.parseInt(evalCount)); //Change by dohatec for re-evaluation
                    
                    int negId = negotiationProcessImpl.addNegotiation(tblNegotiation);

                    // INSERT IN TO TBLNEGNOTIFYTENDERER
                    tblNegNotifyTenderer.setTblNegotiation(new TblNegotiation(negId));
                    tblNegNotifyTenderer.setUserId(company);
                    tblNegNotifyTenderer.setCreatedDt(new java.util.Date());
                    tblNegNotifyTenderer.setRemarks("");
                    tblNegNotifyTenderer.setAcceptStatus("Pending");
                    tblNegNotifyTenderer.setAcceptDt(new java.util.Date());
                    tblNegNotifyTenderer.setReportAppDt(new java.util.Date());
                    tblNegNotifyTenderer.setReportAppStatus("");
                    tblNegNotifyTenderer.setBidAggree("");
                    tblNegNotifyTenderer.setBidAggreeDt(new java.util.Date());
                    negotiationProcessImpl.addNegotiationNotify(tblNegNotifyTenderer);
                    
                    Object[] objects =negotiationProcessImpl.getTendererMailAndMobileNo(company);

                    String mail = (String) objects[0];
                    String cntryCode = (String) objects[1];
                    String mobile = (String) objects[2];
                    

                    NegotiationProcessSrBean npsr = new NegotiationProcessSrBean();
                    npsr.sendMailAndSMS(mail, cntryCode, mobile,String.valueOf(tenderId),tenRefNo,startDate);
                    logger.debug("processRequest action : create : "+logUserId+" ends");
                    }else{
                        pageName = "officer/NegotiationProcess.jsp?tenderId="+tenderId+"&action=create&flag=false";
                    }
                    response.sendRedirect(pageName);
                    
                    //response.sendRedirectFilter("NegotiationProcess.jsp?tenderId="+tenderId+"&action=create&flag=true");

                }catch(Exception ex){
                    logger.error("processRequest action : create : "+logUserId+" : "+ex.toString());
                }
            }

            if("closeNegotiation".equals(request.getParameter("action"))){
                logger.debug("processRequest action : closeNegotiation : "+logUserId+" Starts");
                int negId = 0;
                String remarks="";
                String status = "";

                if(request.getParameter("tenderIdName")!=null){
                    tenderId = Integer.parseInt(request.getParameter("tenderIdName").toString());
                }
                if (request.getParameter("negIdName") != null){
                    negId = Integer.parseInt(request.getParameter("negIdName").toString());
                }
                if(request.getParameter("nameStatus")!=null){
                    status= request.getParameter("nameStatus");
                }
                if (request.getParameter("txtRemarks") != null){
                    remarks= request.getParameter("txtRemarks");
                }
                if (request.getParameter("tenRefNo") != null){
                    tenRefNo= request.getParameter("tenRefNo");
                }
                String setData = "negRemarks = '"+remarks+"', negStatus = '"+status+"'";
                String whereCond = "";
                whereCond = "negId ="+negId;
                auditAction = "TEC-CP Close Negotiation "+status;
                String pageName = "officer/NegotiationProcess.jsp?tenderId="+tenderId+"&action=close&flag=true";
                try{
                    //fdsafdsaf
                    List<CommonMsgChk> listspTendorCommon = negotiationProcessImpl.InsUpdateNegOperationDetailsByXML("updatetable", "tbl_Negotiation", setData, whereCond);
                    //listspTendorCommon!=null && (!listspTendorCommon.isEmpty()) && listspTendorCommon.get(0).getFlag() &&
                    TblNegNotifyTenderer tblNegNotifyTenderer =  negotiationProcessImpl.getUserIdFromNegId(negId);
                    Object[] objects =negotiationProcessImpl.getTendererMailAndMobileNo(tblNegNotifyTenderer.getUserId());
                    if(listspTendorCommon.get(0).getFlag()){
                        if(status.equalsIgnoreCase("Successful")){                            
                            if(tblNegNotifyTenderer!=null){
                                negotiationProcessImpl.dumpNegForms(tenderId+"", String.valueOf(tblNegNotifyTenderer.getUserId()), negId+"");
                                if(!getReportData(String.valueOf(tenderId),String.valueOf(negId),logUserId)){
                                    logger.error("Error in saving Negotiation Report");
                                }
                            }
                        }else{
                            Object[] obj = negotiationProcessImpl.getRoundIdandBidderId(negId);
                            //ReportCreationService reportCreationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");
                            if(saveReportRoundWise(Integer.parseInt(obj[0].toString()),tenderId)){
                                pageName = "officer/Evalclarify.jsp?tenderId="+tenderId+"&st=rp&comType=TEC";
                            }
                        }
                    }
                    sendMessageNeg(String.valueOf(tenderId),tenRefNo,(String)objects[0],status);
                } catch (Exception ex) {
                   logger.error("processRequest action : closeNegotiation : "+logUserId+" : "+ex.toString());
                   auditAction = "Error in "+auditAction+" : "+ex.getMessage();
                }finally{
                    makeAuditTrailService.generateAudit(objAuditTrail, tenderId, idType, moduleName, auditAction, remarks);
                }
                logger.debug("processRequest action : closeNegotiation : "+logUserId+" ends");
                response.sendRedirect(pageName);
                //response.sendRedirectFilter("NegotiationProcess.jsp?tenderId="+tenderId+"&action=close&flag=true");
            }

            if("agree".equals(request.getParameter("action")))
            {
                logger.debug("processRequest action : agree : "+logUserId+" starts");
                String remarks= "";
                String status = "";
                String negId = "";

                if(request.getParameter("remarksName")!=null){
                    remarks = request.getParameter("remarksName");
                }
                if(request.getParameter("statusName")!=null){
                    status = request.getParameter("statusName");
                }
                if(request.getParameter("negIdName")!=null){
                    negId = request.getParameter("negIdName");
                }
                if(request.getParameter("tenderIdName")!=null){
                    tenderId= Integer.parseInt(request.getParameter("tenderIdName").toString());
                }

                String setData = "remarks = '"+remarks+"', acceptStatus = '"+status+"'";
                String whereCond = "";
                whereCond = "negId ="+negId;
                
                // Setting audit trail variables
                auditId=tenderId;
                auditAction="Tenderer "+status+" Negotiation";
                auditremarks=remarks;
                

                try{
                    List<CommonMsgChk> listspTendorCommon = negotiationProcessImpl.InsUpdateNegOperationDetailsByXML("updatetable", "tbl_NegNotifyTenderer", setData, whereCond);
                    ///--------------------- code for send mail and message -------------------------
                      sendMailTendererAcceptRejectNegoRequest(tenderId+"", request.getParameter("tenderRefNo"), status);
                    /// -------------------- code end for send mail and message ----------------------
                    
                } catch (Exception ex) 
                {
                   logger.error("processRequest action : agree : "+logUserId+" : "+ex.toString());
                   auditAction="Error in "+auditAction+" "+ex.getMessage();
                }
                finally
                {
                    makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, auditremarks);
                }
                logger.debug("processRequest action : agree : "+logUserId+" ends");
                if("Accept".equalsIgnoreCase(status)){
                    response.sendRedirect("tenderer/NegoTendererProcess.jsp?tenderId="+tenderId+"&action=accpet&flag=true");
                    //response.sendRedirectFilter("NegoTendererProcess.jsp?tenderId="+tenderId+"&action=accpet&flag=true");
                } else{
                    response.sendRedirect("tenderer/NegoTendererProcess.jsp?tenderId="+tenderId+"&action=reject&flag=true");
                    //response.sendRedirectFilter("NegoTendererProcess.jsp?tenderId="+tenderId+"&action=reject&flag=true");
                }
            }

            if("createQuestion".equals(request.getParameter("action"))){
                 logger.debug("processRequest action : createQuestion : "+logUserId+" starts");
                 int id = 0;
                 tenderId = Integer.parseInt(request.getParameter("tenderIdName").toString());
                 queryText = request.getParameter("txtQuestions");
                 TblNegQuery tblNegQuery = new TblNegQuery();
                 int negId = Integer.parseInt(request.getParameter("negIdName").toString());
                 auditAction = "Create Query for Negotitation";
                 //INSERT TBLNEGQUERY
                 tblNegQuery.setTblTenderMaster(new TblTenderMaster(tenderId));
                 tblNegQuery.setQueryText(queryText);
                 tblNegQuery.setUserId(userId);
                 tblNegQuery.setQueryDt(new java.util.Date());
                 tblNegQuery.setQuerySign("-");
                 tblNegQuery.setQueryStatus("-");
                 tblNegQuery.setQueryType("Negotiation");
                 tblNegQuery.setNegId(negId);

                 try{
                    id = negotiationProcessImpl.addNegQuery(tblNegQuery);
                 }catch(Exception ex){
                    logger.error("processRequest action : createQuestion : "+logUserId+" : "+ex.toString());
                 }

                 //UPDATE TBLNEGQUERYDOCS
                 String setData = "negQueryId ="+id;
                 String whereCond = "tenderId = "+tenderId+" and negId ="+negId+" and negQueryId = 0";

                 try{
                    List<CommonMsgChk> negQueryUpdate = negotiationProcessImpl.InsUpdateNegOperationDetailsByXML("updatetable", "tbl_NegQryDocs", setData, whereCond);
                 } catch (Exception ex) {
                    logger.error("processRequest action : createQuestion : "+logUserId+" : "+ex.toString());
                    auditAction = "Error in "+auditAction+" : "+ex.getMessage();
                 }finally{
                    makeAuditTrailService.generateAudit(objAuditTrail, tenderId, idType, moduleName, auditAction, "");
                 }
                 logger.debug("processRequest action : createQuestion : "+logUserId+" ends");
                 response.sendRedirect("officer/ViewNegQuestions.jsp?tenderId="+tenderId+"&negId="+negId+"&action=createQuestion&flag=true");
                 //response.sendRedirectFilter("ViewNegQuestions.jsp?tenderId="+tenderId+"&negId="+negId+"&action=createQuestion&flag=true");
            }

            if("replyQuestion".equalsIgnoreCase(request.getParameter("action"))){
                logger.debug("processRequest action : replyQuestion : "+logUserId+" ends");
                int negId = 0;
                int negQueryId = 0;
                TblNegReply tblNegReply = new TblNegReply();
                String txtReply="";
                int replyId = 0;

                if(request.getParameter("tenderIdName")!=null){
                    tenderId = Integer.parseInt(request.getParameter("tenderIdName"));
                }
                if (request.getParameter("negIdName") != null){
                    negId = Integer.parseInt(request.getParameter("negIdName"));
                }
                if (request.getParameter("negQueryId") != null){
                    negQueryId = Integer.parseInt(request.getParameter("negQueryId"));
                }
                if (request.getParameter("txtReply") != null){
                    txtReply = request.getParameter("txtReply");
                }
                //Insert into TblNegReply
                tblNegReply.setTblNegQuery(new TblNegQuery(negQueryId));
                tblNegReply.setReplyText(txtReply);
                tblNegReply.setUserId(userId);
                tblNegReply.setRepliedDate(new java.util.Date());
                tblNegReply.setEsignature("-");
                tblNegReply.setReplyAction("Negotiation");

                // Setting audit trail variables
                auditId=tenderId;
                auditAction="Tenderer has Reply for Query of Negotiation";
                auditremarks=txtReply;
                
                try{
                    negotiationProcessImpl.addQueryReply(tblNegReply);
                }catch(Exception ex){
                    logger.error("processRequest action : replyQuestion : "+logUserId+" : "+ex.toString());
                    auditAction="Error in "+auditAction+" "+ex.getMessage();
                }
                finally{
                    makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, auditremarks); 
                }
                logger.debug("processRequest action : replyQuestion : "+logUserId+" ends");
                response.sendRedirect("tenderer/viewNegQuestions.jsp?tenderId="+tenderId+"&negId="+negId+"&action=replyQuestion&flag=true");
                //response.sendRedirectFilter("viewNegQuestions.jsp?tenderId="+tenderId+"&negId="+negId+"&action=replyQuestion&flag=true");
            }

            if("approveNegotiation".equalsIgnoreCase(request.getParameter("action"))){
                logger.debug("processRequest action : approveNegotiation : "+logUserId+" Starts");
                String remarks= "";
                String status = "";
                String negId = "";
                String nameNegDocs = "";

                if(request.getParameter("nameRemarks")!=null){
                    remarks = request.getParameter("nameRemarks");
                }
                if(request.getParameter("negIdName")!=null){
                    negId = request.getParameter("negIdName");
                }
                if(request.getParameter("tenderIdName")!=null){
                    tenderId= Integer.parseInt(request.getParameter("tenderIdName").toString());
                }
                if(request.getParameter("nameNegDocs")!=null){
                    nameNegDocs= request.getParameter("nameNegDocs");
                }

                String  reportAppDate= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) ;

                String setData = "remarks = '"+remarks+"', reportAppStatus = '"+nameNegDocs+"', reportAppDt = '"+reportAppDate+"'";
                String whereCond = "";
                whereCond = "negId ="+negId;

                try{
                    List<CommonMsgChk> listspTendorCommon = negotiationProcessImpl.InsUpdateNegOperationDetailsByXML("updatetable", "tbl_NegNotifyTenderer", setData, whereCond);
                } catch (Exception ex) {
                    logger.error("processRequest action : replyQuestion : "+logUserId+" : "+ex.toString());
                }
                 logger.debug("processRequest action : replyQuestion : "+logUserId+" ends");
                if("Accept".equalsIgnoreCase(nameNegDocs)){
                    response.sendRedirect("tenderer/NegoTendererProcess.jsp?tenderId="+tenderId+"&action=docsaccept&flag=true");
                    //response.sendRedirectFilter("NegoTendererProcess.jsp?tenderId="+tenderId+"&action=docsaccept&flag=true");
                } else{
                    response.sendRedirect("tenderer/NegoTendererProcess.jsp?tenderId="+tenderId+"&action=docsreject&flag=true");
                    //response.sendRedirectFilter("NegoTendererProcess.jsp?tenderId="+tenderId+"&action=docsreject&flag=true");
                }
            }

            if("negDocs".equalsIgnoreCase(request.getParameter("action"))){
                logger.debug("processRequest action : negDocs : "+logUserId+" starts");
                int negQueryId = 0;
                int negId = 0;
                String docAction = "";

                if(request.getParameter("negQueryId")!=null){
                    negQueryId = Integer.parseInt(request.getParameter("negQueryId"));
                }
                if(request.getParameter("negIdName")!=null){
                    negId = Integer.parseInt(request.getParameter("negIdName"));
                }
                if(request.getParameter("tenderId")!=null){
                    tenderId= Integer.parseInt(request.getParameter("tenderId"));
                }
                if(request.getParameter("docAction")!=null){
                    docAction= request.getParameter("docAction");
                }

                StringBuffer strDocData = new StringBuffer();
                strDocData.append("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                strDocData.append("<tr>");
                strDocData.append("<th width='4%' class='t-align-left'>Sl.  No.</th>");
                strDocData.append("<th class='t-align-left' width='23%'>File Name</th>");
                strDocData.append("<th class='t-align-left' width='28%'>File Description</th>");
                strDocData.append("<th class='t-align-left' width='7%'>File Size <br />(in KB)</th>");
                strDocData.append("<th class='t-align-left' width='18%'>Action</th>");
                strDocData.append("</tr>");

                int docCnt = 0;
                List<SPTenderCommonData> listnegDocs = null;

                if("NegReplyDocs".equals(docAction)){
                    listnegDocs = tenderCS.returndata(docAction, ""+negQueryId, "");
                } else{
                    listnegDocs = tenderCS.returndata(docAction, ""+negId, ""+negQueryId);
                }
                if (listnegDocs.size() > 0) {
                        for (SPTenderCommonData negDocData : listnegDocs) {
                            docCnt++;
                            strDocData.append("<tr>");
                            strDocData.append("<td class='t-align-left'>" + docCnt + "</td>");

                            strDocData.append("<td class='t-align-left'>" + negDocData.getFieldName1() + "</td>");
                            strDocData.append("<td class='t-align-left'>" + negDocData.getFieldName2() + "</td>");

                            strDocData.append("<td class='t-align-left'>" + Long.parseLong(negDocData.getFieldName3())/1024  + "</td>");
                            String linkDownloadDoc = "";

                            String linkDeleteDoc = "";
                            linkDeleteDoc = "<a href='javascript:void(0);' onclick=\"deleteFile('" + negDocData.getFieldName1() + "','" + negDocData.getFieldName4() + "','" + tenderId + "','"+negId+"','"+negQueryId+"')\" ><img src='"+request.getContextPath()+"/resources/images/Dashboard/Delete.png' alt='Remove' /></a>";
                            linkDownloadDoc = "<a href='javascript:void(0);' onclick=\"downloadFile('" + negDocData.getFieldName1() + "','" + negDocData.getFieldName3() + "','" + tenderId + "','"+negId+"','"+negQueryId+"')\" ><img src='"+request.getContextPath()+"/resources/images/Dashboard/Download.png' alt='Download' /></a>";
                            strDocData.append("<td class='t-align-left' width='18%'>" + linkDownloadDoc + "&nbsp;" + linkDeleteDoc + "</td>");

                            strDocData.append("</tr>");
                        }
                    } else {
                        strDocData.append("<tr>");
                        strDocData.append("<td colspan='5' class='t-align-center'>No records found.</td>");
                        strDocData.append("</tr>");
                    }
                    strDocData.append("</table>");
                logger.debug("processRequest action : negDocs : "+logUserId+" ends");
                    out.print(strDocData.toString());
            }

            if("NegotiateBid".equalsIgnoreCase(request.getParameter("action"))){
                logger.debug("processRequest action : NegotiateBid : "+logUserId+" starts");
                tenderId = Integer.parseInt(request.getParameter("hidtenderId"));
                int negId = Integer.parseInt(request.getParameter("hidnegId"));

                String setData = "negFinalSub = 'Yes'";
                String whereCond = "";
                whereCond = "negId ="+negId;

                try{
                    List<CommonMsgChk> listspTendorCommon = negotiationProcessImpl.InsUpdateNegOperationDetailsByXML("updatetable", "tbl_Negotiation", setData, whereCond);
                } catch (Exception ex) {
                   logger.error("processRequest action : NegotiateBid : "+logUserId+" : "+ex.toString());
                }
                logger.debug("processRequest action : NegotiateBid : "+logUserId+" ends");
                response.sendRedirect("officer/NegotiationProcess.jsp?tenderId="+tenderId);
                //response.sendRedirectFilter("NegotiationProcess.jsp?tenderId="+tenderId);
            }

            if("approveBid".equals(request.getParameter("action")))
            {
                logger.debug("processRequest action : approveBid : "+logUserId+" starts");
                String status = "";
                String negId = "";

                if(request.getParameter("negId")!=null){
                    negId = request.getParameter("negId");
                }
                if(request.getParameter("FinalStatus")!=null){
                    status = request.getParameter("FinalStatus");
                }
                if(request.getParameter("hidtenderId")!=null){
                    tenderId = Integer.parseInt(request.getParameter("hidtenderId"));
                }

                String  bidAggreeDt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) ;

                String setData = "bidAggree = '"+status+"', bidAggreeDt = '"+bidAggreeDt+"'";
                String whereCond = "";
                whereCond = "negId = "+negId;

                try{
                    List<CommonMsgChk> listspTendorCommon = negotiationProcessImpl.InsUpdateNegOperationDetailsByXML("updatetable", "tbl_NegNotifyTenderer", setData, whereCond);
                } catch (Exception ex) {
                   logger.error("processRequest action : approveBid : "+logUserId+" : "+ex.toString());
                }
                logger.debug("processRequest action : approveBid : "+logUserId+" ends");
                if("Accept".equalsIgnoreCase(status)){
                    response.sendRedirect("tenderer/NegoTendererProcess.jsp?tenderId="+tenderId+"&action=accpet&flag=true");
                    //response.sendRedirectFilter("NegoTendererProcess.jsp?tenderId="+tenderId+"&action=accpet&flag=true");
                } else{
                    response.sendRedirect("tenderer/NegoTendererProcess.jsp?tenderId="+tenderId+"&action=reject&flag=true");
                    //response.sendRedirectFilter("NegoTendererProcess.jsp?tenderId="+tenderId+"&action=reject&flag=true");
                }
            }
            logger.debug("processRequest  : "+logUserId+" ends");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Fetch Report Data from tenderId, neg Id and userId
     * @param tenderId
     * @param negId
     * @param userId
     * @return
     * @throws Exception
     */
    public boolean getReportData(String tenderId, String negId,String userId) throws Exception {
        logger.debug("getReportData : " + logUserId + " Starts");
        boolean flag = false;
        try {
            ReportCreationService reportCreationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");
            String reportId = reportCreationService.getRepIdByNegId(negId);
            List<SPTenderCommonData> list = null;
            List<ReportGenerateDtBean> data = new ArrayList<ReportGenerateDtBean>();
            int reportTabId = Integer.parseInt(reportCreationService.getReportTableId(reportId));
            List<TblReportColumnMaster> columnMasters = reportCreationService.getReportColumns(reportTabId);

            List<Integer> autoColumns = new ArrayList<Integer>();
            List<Integer> cmpColumns = new ArrayList<Integer>();
            List<Integer> autoNumberCols = new ArrayList<Integer>();
            List<Integer> estCosts = new ArrayList<Integer>();
            int govColId = 0;

            for (TblReportColumnMaster tRCM : columnMasters) {
                if (tRCM.getFilledBy() == 2 && tRCM.getGovCol().equals("no")) {
                    autoColumns.add(tRCM.getColumnId());
                }
                if (tRCM.getFilledBy() == 2 && tRCM.getGovCol().equals("yes")) {
                    govColId = tRCM.getColumnId();
                }
                if (tRCM.getFilledBy() == 1) {
                    cmpColumns.add(tRCM.getColumnId());
                }
                if (tRCM.getFilledBy() == 3) {
                    autoNumberCols.add(tRCM.getColumnId());
                }
                if (tRCM.getFilledBy() == 4) {
                    estCosts.add(tRCM.getColumnId());
                }
            }      
                list = reportCreationService.getBiddersForReport(tenderId);

            String getCREstCost = reportCreationService.getCREstCost(tenderId, reportId);

            for (SPTenderCommonData sPTenderCommonData : list) {
                String[] cmpName = new String[cmpColumns.size()];
                for (int j = 0; j < cmpName.length; j++) {
                    cmpName[j] = sPTenderCommonData.getFieldName2();
                }
                String[] cmpColumnsTmp = new String[cmpColumns.size()];
                for (int j = 0; j < cmpColumnsTmp.length; j++) {
                    cmpColumnsTmp[j] = cmpColumns.get(j).toString();
                }
                String[] autoNumberColsTmp = new String[autoNumberCols.size()];
                for (int j = 0; j < autoNumberColsTmp.length; j++) {
                    autoNumberColsTmp[j] = autoNumberCols.get(j).toString();
                }
                String[] autoColumnsTmp = new String[autoColumns.size()];
                for (int j = 0; j < autoColumnsTmp.length; j++) {
                    autoColumnsTmp[j] = autoColumns.get(j).toString();
                }
                String[] estCost = new String[estCosts.size()];
                for (int j = 0; j < estCost.length; j++) {
                    estCost[j] = getCREstCost;
                }
                String[] estCostsTmp = new String[estCosts.size()];
                for (int j = 0; j < estCostsTmp.length; j++) {
                    estCostsTmp[j] = estCosts.get(j).toString();
                }
                data.add(new ReportGenerateDtBean(sPTenderCommonData.getFieldName1(), cmpName, cmpColumnsTmp, autoNumberColsTmp, reportCreationService.getBidderDataForReportNeg(reportTabId, govColId, Integer.parseInt(sPTenderCommonData.getFieldName1())), String.valueOf(govColId), getAutoColData(reportTabId, autoColumns, Integer.parseInt(sPTenderCommonData.getFieldName1())), autoColumnsTmp,estCost,estCostsTmp));
                cmpName = null;
                cmpColumnsTmp = null;
                autoNumberColsTmp = null;
                autoColumnsTmp = null;
                estCost = null;
                estCostsTmp = null;
            }
            if (reportCreationService.getReportType(reportId).equals("l1")) {
                Collections.sort(data, new CompareLotBidderL1());
            } else {
                Collections.sort(data, new CompareLotBidderH1());
            }
            saveReportData(data, tenderId, reportId, columnMasters, userId);
            columnMasters = null;
            autoColumns = null;
            cmpColumns = null;
            autoNumberCols = null;
            list = null;
            flag = true;
        } catch (Exception e) {
            logger.error("save N1 report : ",e);
        }
        logger.debug("getReportData : " + logUserId + " Ends");
        return flag;
    }

    /**
     * Save Report data
     * @param rgdbs
     * @param tenderId
     * @param reportId
     * @param columnMasters
     * @param userId
     */
    public void saveReportData(List<ReportGenerateDtBean> rgdbs, String tenderId, String reportId, List<TblReportColumnMaster> columnMasters, String userId) {
        logger.debug("saveReportData : " + logUserId + " Starts");
        ReportCreationService reportCreationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");
        
        int pkgLotId = Integer.parseInt(reportCreationService.getPkgLotId(tenderId, reportId));
        int counter=1;
        int rank=1;
        int fRank=0;
        List<TblBidderRank> list1 = new ArrayList<TblBidderRank>();
        List<TblBidRankDetail> list2 = new ArrayList<TblBidRankDetail>();
        double L1amount = 0;
        double amount = 0;
        boolean isLottery = false;
        boolean isFirstDisq = true;
        CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
        for (ReportGenerateDtBean rgdb : rgdbs) {
            if (rgdb.getGovColumn() == null) {
                rgdb.setGovColumn("0");
            }
            amount = new BigDecimal(rgdb.getGovColumn()).setScale(3, 0).doubleValue();
            /*CommonSearchDataMoreService moreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
            List<SPCommonSearchDataMore> pqBidder = moreService.geteGPData("PostDisqualifyUsers",tenderId,reportId);
            int tempFlag=0;
            for(SPCommonSearchDataMore pqUser : pqBidder){
                if(pqUser.getFieldName1().equals(rgdb.getBidderId())){
                    fRank=-1;
                    tempFlag++;
                }
            }*/
            if(counter==1){
                if(fRank!=-1){
                    fRank = rank;
                }
                L1amount = amount;
            }else{
                if(amount!=L1amount){
                    L1amount = amount;
                  if(fRank==-1){
                        fRank = rank;
                        rank++;
                    }else{
                        rank++;
                        fRank = rank;
                    }
                }else{
                    if(fRank==-1){
                       L1amount = amount;
                    }else if(fRank!=-1){
                        if(fRank==rank && counter==2 && isFirstDisq){
                            isLottery = true;
                        }
                    }
                }
           }
            if(fRank==-1){
                fRank = rank;
                if(counter==1){
                    isFirstDisq = false;
                 }
            }
            /*if(tempFlag!=0){
                counter++;
                continue;
            }*/

            List<SPCommonSearchDataMore> rankDataMores = dataMore.geteGPDataMore("getRankToSaveN1", tenderId);
            if(rankDataMores!=null && (!rankDataMores.isEmpty())){
                rank = Integer.parseInt(rankDataMores.get(0).getFieldName1());
            }
            list1.add(new TblBidderRank(0, new TblTenderMaster(Integer.parseInt(tenderId)), pkgLotId, Integer.parseInt(rgdb.getBidderId()), new Date(), Integer.parseInt(userId), rank, Integer.parseInt(reportId), new BigDecimal(rgdb.getGovColumn()), 0, 0, ""));            
            for (TblReportColumnMaster master : columnMasters) {
                if (master.getFilledBy() == 1) {
                    for (int r = 0; r < rgdb.getCompanyNameColId().length; r++) {
                        if (Integer.parseInt(rgdb.getCompanyNameColId()[r]) == master.getColumnId()) {
                            list2.add(new TblBidRankDetail(0, null, master.getColumnId(), counter - 1, rgdb.getCompanyName()[r]));
                        }
                    }
                }
                if (master.getFilledBy() == 2 && master.getGovCol().equals("no")) {
                    for (int p = 0; p < rgdb.getAutoColumnColId().length; p++) {
                        if (Integer.parseInt(rgdb.getAutoColumnColId()[p]) == master.getColumnId()) {
                            String govno=null;
                            if(rgdb.getAutoColumn()[p]==null || "".equals(rgdb.getAutoColumn()[p])){
                                govno =  "0.00";
                            }else{
                                govno =  rgdb.getAutoColumn()[p];
                            }
                            list2.add(new TblBidRankDetail(0, null, master.getColumnId(), counter - 1, govno));
                        }
                    }
                }
                if (master.getFilledBy() == 2 && master.getGovCol().equals("yes")) {
                    if (Integer.parseInt(rgdb.getGovColumnColId()) == master.getColumnId()) {
                        String govyes=null;
                        if(rgdb.getGovColumn()==null || "".equals(rgdb.getGovColumn())){
                            govyes =  "0.00";
                        }else{
                            govyes =  rgdb.getGovColumn();
                        }
                        list2.add(new TblBidRankDetail(0, null, master.getColumnId(), counter - 1, govyes));
                    }
                }
                if (master.getFilledBy() == 3) {
                    for (int q = 0; q < rgdb.getAutoNumberColId().length; q++) {
                        if (Integer.parseInt(rgdb.getAutoNumberColId()[q]) == master.getColumnId()) {                            
                           list2.add(new TblBidRankDetail(0, null, master.getColumnId(), counter - 1, String.valueOf(rank)));
                        }
                    }
                }
                if (master.getFilledBy() == 4) {
                    for (int r = 0; r < rgdb.getEstCostId().length; r++) {
                        if (Integer.parseInt(rgdb.getEstCostId()[r]) == master.getColumnId()) {
                            list2.add(new TblBidRankDetail(0, null, master.getColumnId(), counter - 1, rgdb.getEstCost()[r]));
                        }
                    }
                }                
            }
            counter++;
        }
        TblEvalRoundMaster roundMaster = null;
        int evalCount = evalService.getEvaluationNo(Integer.parseInt(tenderId)); //Change by dohatec for re-evaluation
        if(!list1.isEmpty()){
            int winnerBidder = 0;
            if(isLottery){
                List<Integer> bidders = new ArrayList<Integer>();
                int finalOne = list1.get(0).getRank();
                for (TblBidderRank bidderRank : list1) {
                    if(bidderRank.getRank()==finalOne){
                        bidders.add(bidderRank.getUserId());
                    }
                }
                winnerBidder = getLotteryWinner(bidders);
            }else{
                winnerBidder = list1.get(0).getUserId();
            }
            roundMaster =  new TblEvalRoundMaster(0, Integer.parseInt(tenderId), pkgLotId, "N1", Integer.parseInt(reportId),winnerBidder, new Date(), Integer.parseInt(userId),evalCount); //Change by dohatec for re-evaluation
        }
        reportCreationService.saveReportData(roundMaster,list1, list2, columnMasters.size());
        list1 = null;
        list2 = null;
        logger.debug("saveReportData : " + logUserId + " Ends");
    }

    /**
     * Get Auto Column Data
     * @param repTabId
     * @param autoCols
     * @param userId
     * @return
     */
    public String[] getAutoColData(int repTabId, List<Integer> autoCols, int userId) {
        logger.debug("getAutoColData : " + logUserId + " Starts");
        ReportCreationService reportCreationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");
        String[] data = new String[autoCols.size()];
        int i = 0;
        for (Integer aCols : autoCols) {
            data[i] = reportCreationService.getBidderDataForReportNeg(repTabId, aCols, userId);
            i++;
        }
        logger.debug("getAutoColData : " + logUserId + " Ends");
        return data;
    }
    
    /**
     * Method for get Lottery winner.
     * @param bidders
     * @return
     */
    private int getLotteryWinner(List<Integer> bidders) {
        Collections.shuffle(bidders);
        int index = new Random().nextInt(bidders.size());
        return bidders.get(index);
    }

    /**
     * Method for save Report Round Wise.
     * @param roundId
     * @param tenderId
     * @return
     */
    public boolean saveReportRoundWise(int roundId,int tenderId){
        TblEvalRoundMaster objRound = null;
        List<TblBidderRank> listRank = null;
        //List<TblBidderRank> cloneListRank = null;
        List<TblBidRankDetail> listRankDtl = null;
        //List<TblBidRankDetail> cloneListRankDtl = null;
        int evalCount = evalService.getEvaluationNo(tenderId); //Change by dohatec for re-evaluation
        boolean isT1 = false;
        boolean isL1 = false;
        boolean isT1L1 = false;
        CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
        List<SPCommonSearchDataMore> envDataMores = dataMore.geteGPData("GetTenderEnvCount", String.valueOf(tenderId));
         if(envDataMores!=null && (!envDataMores.isEmpty())){            
            if(envDataMores.get(0).getFieldName2().equals("1")){
                //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                isT1 = true;
            }
            if(envDataMores.get(0).getFieldName2().equals("2")){
                //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                isL1 = true;
            }
            if(envDataMores.get(0).getFieldName2().equals("3")){
                //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                isT1L1 = true;
            }
        }
        boolean flag = true;
        
        ReportCreationService rcs = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");
        objRound = rcs.getTblEvalRoundMasters(roundId);
        listRank = rcs.getTblBidderRanks(roundId);
        listRankDtl = rcs.getTblBidRankDetails(listRank);
        if(isL1){
            try {
                TblEvalRoundMaster tblEvalRoundMaster = new TblEvalRoundMaster();
                if(listRank!=null && listRank.size()>1 && objRound!=null){
                    tblEvalRoundMaster.setCreatedBy(objRound.getCreatedBy());
                    tblEvalRoundMaster.setCreatedDt(new Date());
                    tblEvalRoundMaster.setPkgLotId(objRound.getPkgLotId());
                    tblEvalRoundMaster.setReportId(objRound.getReportId());
                    tblEvalRoundMaster.setReportType(objRound.getReportType());
                    tblEvalRoundMaster.setRomId(null);
                    tblEvalRoundMaster.setTenderId(objRound.getTenderId());
                    tblEvalRoundMaster.setUserId(listRank.get(1).getUserId());
                    tblEvalRoundMaster.setEvalCount(evalCount); //Change by dohatec for re-evaluation
                }

                    int noofCols = 0;
                    List<TblBidderRank> list1 = new ArrayList<TblBidderRank>();
                    List<TblBidRankDetail> list2 = new ArrayList<TblBidRankDetail>();
                    int counter=0;
                    for(TblBidderRank tblBidderRank : listRank){

                        noofCols=0;
                        for(TblBidRankDetail tblBidRankDetail : listRankDtl){
                            if(counter!=0 && tblBidderRank.getBidderRankId() == tblBidRankDetail.getTblBidderRank().getBidderRankId()){
                                tblBidRankDetail.setBidRankDetailId(0);
                                list2.add(tblBidRankDetail);
                                noofCols++;
                            }
                        }
                        if(counter!=0){
                            tblBidderRank.setBidderRankId(0);
                            list1.add(tblBidderRank);
                        }
                        counter++;
                    }
                    rcs.saveReportData(tblEvalRoundMaster, list1, list2, noofCols);
                    list1.clear();
                    list2.clear();
                    listRank.clear();
                    listRankDtl.clear();
                    list1 = null;
                    list2 = null;
                    listRankDtl = null;

            } catch (Exception e) {
                e.printStackTrace();
                flag= false;
            }
        }else{             
             List<SPCommonSearchDataMore> getbidderWinnerNeg = dataMore.geteGPDataMore("getEvalBidderForNegotiation",String.valueOf(tenderId),String.valueOf(roundId),(isT1?"T1":"T1L1"));
             List<String> userIds = new ArrayList<String>();
             for (SPCommonSearchDataMore sPCommonSearchDataMore : getbidderWinnerNeg) {
                userIds.add(sPCommonSearchDataMore.getFieldName1());
             }
             try {
                TblEvalRoundMaster tblEvalRoundMaster = new TblEvalRoundMaster();
                if(listRank!=null && listRank.size()>1 && objRound!=null && !userIds.isEmpty()){
                    tblEvalRoundMaster.setCreatedBy(objRound.getCreatedBy());
                    tblEvalRoundMaster.setCreatedDt(new Date());
                    tblEvalRoundMaster.setPkgLotId(objRound.getPkgLotId());
                    tblEvalRoundMaster.setReportId(objRound.getReportId());
                    tblEvalRoundMaster.setReportType(objRound.getReportType());
                    tblEvalRoundMaster.setRomId(null);
                    tblEvalRoundMaster.setTenderId(objRound.getTenderId()); 
                    tblEvalRoundMaster.setUserId(Integer.parseInt(userIds.get(0)));
                    tblEvalRoundMaster.setEvalCount(evalCount); //Change by dohatec for re-evaluation
                }

                    int noofCols = 0;
                    List<TblBidderRank> list1 = new ArrayList<TblBidderRank>();
                    List<TblBidRankDetail> list2 = new ArrayList<TblBidRankDetail>();
                    int counter=0;
                    for(TblBidderRank tblBidderRank : listRank){
                        //noofCols=0;
                        for(TblBidRankDetail tblBidRankDetail : listRankDtl){
                            if(userIds.contains(String.valueOf(tblBidderRank.getUserId())) && tblBidderRank.getBidderRankId() == tblBidRankDetail.getTblBidderRank().getBidderRankId()){
                                tblBidRankDetail.setBidRankDetailId(0);
                                list2.add(tblBidRankDetail);
                                //if(counter==0){
                                    noofCols = listRankDtl.size()/listRank.size();
                                //}
                            }
                        }
                        if(userIds.contains(String.valueOf(tblBidderRank.getUserId()))){
                            tblBidderRank.setBidderRankId(0);
                            list1.add(tblBidderRank);
                        }
                        counter++;
                    }                    
                    rcs.saveReportData(tblEvalRoundMaster, list1, list2, noofCols);
                    list1.clear();
                    list2.clear();
                    listRank.clear();
                    listRankDtl.clear();
                    list1 = null;
                    list2 = null;
                    listRankDtl = null;

            } catch (Exception e) {
                e.printStackTrace();
                flag= false;
            }
        }
        return flag;
    }

    /**
     * Method for send Mail to CP if Tenderer Accept/Reject Negotiation Request.
     * @param tenderId
     * @param refNo
     * @param accrejStatus
     */
    public void sendMailTendererAcceptRejectNegoRequest(String tenderId,String refNo,String accrejStatus)
    {
        try
        {
            List<SPTenderCommonData> commonData = tenderCS.returndata("GetTecCpEmail", tenderId, null);
            
            if(commonData != null && !commonData.isEmpty())
            {
                //System.out.println("------------------------ sednding mail----------");
                String mails[]  = {commonData.get(0).getFieldName1()};

                String lblStatus="Accepted";

                if(accrejStatus!= null && accrejStatus.equalsIgnoreCase("Reject"))
                    lblStatus="Rejected";
                else
                    lblStatus="Accepted";

                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                sendMessageUtil.setEmailTo(mails);
                MailContentUtility mailContentUtility = new MailContentUtility();
                String mailText = mailContentUtility.mailSendToCPForTendererAcceptRejectNegoRequest(tenderId, refNo,lblStatus);
                sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
               //System.out.println(" from="+XMLReader.getMessage("emailIdNoReply"));
                sendMessageUtil.setEmailSub("e-GP: Negotiation "+lblStatus+" by Bidder / Officer");
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                registerService.contentAdmMsgBox(mails[0], XMLReader.getMessage("emailIdNoReply"), "e-GP: Negotiation "+lblStatus+" by Bidder / Officer",msgBoxContentUtility.messageSendToCPForTendererAcceptRejectNegoRequest(tenderId, refNo,lblStatus));

                //System.out.println("mail send successfully.");
            }
            else
            {
                System.out.println("email id not found");
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
    }

    /**
     * Method for send Message to tenderer while PE close Negotiation.
     * @param tenderId
     * @param tendRefNo
     * @param mailId
     * @param finalStatus
     * @return
     */
    public boolean sendMessageNeg(String tenderId,String tendRefNo,String mailId,String finalStatus){
        boolean mailSent = false;
        String status = "This is to inform you that the negotiation with you has been failed for below mentioned Tender: <br />";
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            MailContentUtility mailContentUtility = new MailContentUtility();
                if("Successful".equalsIgnoreCase(finalStatus)){
                    status = "This is to inform you that the negotiation with you has been Successfully closed for below mentioned Tender : <br />";
                }
            List list = mailContentUtility.getNegCloseContent(tenderId,tendRefNo,status);
            String mailSub = list.get(0).toString();
            String mailText = list.get(1).toString()+list.get(2).toString()+list.get(3).toString()+list.get(4).toString()+list.get(5).toString();
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
            //MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            registerService.contentAdmMsgBox(mailId, XMLReader.getMessage("emailIdNoReply"), mailSub, list.get(1).toString()+list.get(3).toString()+list.get(5).toString());
            try{
                sendMessageUtil.sendEmail();
                CommonServiceImpl csImpl = new CommonServiceImpl();
                csImpl.sendMsg(mailId, mailSub, mailText);
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

            /*try{
               sendMessageUtil.setSmsNo(""+mobNo);
               sendMessageUtil.setSmsBody(list.get(2).toString());
               sendMessageUtil.sendSMS();
            }catch(Exception ex){
               logger.error("sendMailAndSMS : "+logUserId+" : "+ex.toString());
            }*/
            mailSent = true;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return mailSent;
    }
}
