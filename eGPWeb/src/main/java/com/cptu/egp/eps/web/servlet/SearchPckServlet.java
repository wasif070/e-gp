/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonAppSearchData;
import com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class SearchPckServlet extends HttpServlet {
    
    
    private final AppAdvSearchService appAdvSearchService = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
    private static final Logger LOGGER = Logger.getLogger(SearchServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            appAdvSearchService.setLogUserId(logUserId);
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String pckNo = request.getParameter("pkgNo");
            int appId = Integer.parseInt(request.getParameter("appId"));
//            NumberFormat fmt = new DecimalFormat("$0.00");
//            String testVal = request.getParameter("estimatedCost");
//            Number nbr = fmt.parse(testVal);
//            BigDecimal bd = new BigDecimal(nbr.doubleValue());
//            System.out.println("val: " + bd);
            String value = request.getParameter("estimatedCost");
//            Double estCost = Double.parseDouble(request.getParameter("estimatedCost"));
//            BigDecimal EstimatedCost = BigDecimal.valueOf(estCost);
            Integer EstimatedCost = 0;
            if (value != null && !value.equals("")) {
                EstimatedCost = Integer.parseInt(value);
            }

            String status = request.getParameter("status");
            String procurementNature = request.getParameter("procurementNature");
            String procurementType = request.getParameter("procurementType");

            
            List<CommonAppSearchData> commonData = appAdvSearchService.getSearchResults(" ", " ", procurementNature, 0, 0, " ", " ", procurementType, appId, " ", pckNo, new BigDecimal(EstimatedCost), 0, " ", " ", new BigDecimal(0), new BigDecimal(0), 1, 1000, status,0);
            StringBuilder strProc = new StringBuilder();
            int srno = 1;
            for (int i = 0; i < commonData.size(); i++) {
                strProc.delete(0, strProc.length());
                CommonAppSearchData commonAppSearchData = commonData.get(i);
                out.print("<tr>");
                out.print("<td align='left'>" + srno + "</td>");
                out.print("<td align='left'>" + commonAppSearchData.getPackageNo() + "<br/>" + commonAppSearchData.getPackageDesc() + "</td>");
                strProc.append(commonAppSearchData.getProcurementNature());
                if (strProc.toString().equals("null")) {
                    strProc.delete(0, strProc.length());
                }

                
                out.print("<td class=\"t-align-center\">" + strProc + "<br/>" + commonAppSearchData.getProcurementType() + "</td>");
                out.print("<td class=\"t-align-center\">" + commonAppSearchData.getPkgEstCost() + "</td>");
                String linkUpload = "<a href='" + request.getContextPath()+ "/officer/APPDashboardDocs.jsp?appId=" + commonAppSearchData.getAppId() + "&pckId=" + commonAppSearchData.getPackageId() + "&pckno=" + commonAppSearchData.getPackageNo() + "&desc=" + commonAppSearchData.getPackageDesc() +  " '>upload</a>";
                //out.print("<td class=\"t-align-center\"><input name='uploadDocFile' id='uploadDocFile' type='file' class='formTxtBox_1' style='width:250px; background:none;'/>" +
                 //       "<br/><input type='submit' id='submit' value='update'/></td>");
                out.print("<td align='left'>" + linkUpload + "</td>");
                //out.print("<td align='left'>" + strProc + "<br/>" + commonAppSearchData.getProcurementType() + "</td>");
                //out.print("<td align='left'>" + commonAppSearchData.getPkgEstCost() + "</td>");
                //String linkUpload = "<a onclick = window.open('" + request.getContextPath()+ "/officer/APPDashboardDocs.jsp?appId=" + commonAppSearchData.getAppId() + "&pckId=" + commonAppSearchData.getPackageId() + "','window','scrollbars=1','width=600,height=300')>upload</a>";
                
                StringBuilder link = new StringBuilder();
                if("Approved".equals(status)){
                    link.append("<a href='"+ request.getContextPath() + "/resources/common/ViewPackageDetail.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=Package'>View</a>&nbsp");
                }else{
                    link.append("<a href='"+ request.getContextPath() + "/resources/common/ViewPackageDetail.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=Package'>View</a>");
                    link.append("&nbsp;|&nbsp;<a href='"+ request.getContextPath() + "/officer/AddPackageDetail.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&action=Edit'>Edit Package Detail</a>");
                    //System.out.println("chek : "+(appAdvSearchService.countForQuery("TblAppPqTenderDates", "appid = "+commonAppSearchData.getAppId()+" and packageId = "+commonAppSearchData.getPackageId())));
                    if(!((appAdvSearchService.countForQuery("TblAppPqTenderDates", "appid = "+commonAppSearchData.getAppId()+" and packageId = "+commonAppSearchData.getPackageId()))>0)){
                      link.append("&nbsp;|&nbsp;<a href='"+ request.getContextPath() + "/officer/AddPackageDates.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "'>Add Dates</a>");
                    }else{
                        link.append("&nbsp;|&nbsp;<a href='"+ request.getContextPath() + "/officer/EditPackageDates.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "'>Edit Dates</a>");
                    }
                    link.append("&nbsp;|&nbsp;<a href='#'>Remove</a>");
                    link.append("&nbsp;|&nbsp;<a href='#'>Create Tender/Proposal</a>");
                    link.append("&nbsp;|&nbsp;<a href='#'>Report</a>");
                    //link = "<a href='"+ request.getContextPath() + "/resources/common/ViewPackageDetail.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=Package'>View</a>&nbsp;|&nbsp;<a href='"+ request.getContextPath() + "/officer/AddPackageDetail.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&action=Edit'>Edit Package Detail</a>&nbsp;|&nbsp;<a href='"+ request.getContextPath() + "/officer/EditPackageDates.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=Package'><br/>Edit Dates</a>&nbsp;|&nbsp;<a href='#'>Remove</a>&nbsp;|&nbsp;<a href='#'><br/>Create Tender</a>";
                }
                //out.print("<cell><![CDATA[" + link + "]]></cell>");
                out.print("<td align='left'>" + link.toString() + "</td>");
                out.print("</tr>");
                srno++;
            }
        } catch (Exception ex) {
            LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGEREND);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
