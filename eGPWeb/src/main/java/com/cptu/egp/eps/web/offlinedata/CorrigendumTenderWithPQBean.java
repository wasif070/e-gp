/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.offlinedata;

/**
 *
 * @author salahuddin
 */

import com.cptu.egp.eps.model.table.TblTenderDetailsOffline;
import com.cptu.egp.eps.model.table.TblCorrigendumDetailOffline;
import com.cptu.egp.eps.model.table.TblTenderLotPhasingOffline;
import com.cptu.egp.eps.service.serviceimpl.CorrigendumDetailsOfflineService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CommonUtils;
import java.util.ArrayList;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import java.lang.String;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.lang.reflect.Method;

public class CorrigendumTenderWithPQBean {
    
    private static final Logger LOGGER = Logger.getLogger(CorrigendumTenderWithPQBean.class);
    PrequalificationExcellBean preqExcellBean = null;
    List<TblCorrigendumDetailOffline> corrigendumDetailOffline = null;
    private final CorrigendumDetailsOfflineService corrigendumDetailsOfflineService  = (CorrigendumDetailsOfflineService) AppContext.getSpringBean("CorrigendumDetailsOfflineService");
    private String corriStatus = "Pending";
    private int CorNo=0;
    //DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd, HH:mm:ss");
    Date date = new Date();

    private String oldDate = "";
    private String newDate = "";

    public void createCorrigendum(TblTenderDetailsOffline tenderDetailsOffline,int tenderOLId, int userID, int corrigendumNo, boolean isEdit) throws Exception {


        LOGGER.debug("createCorri : " + userID + "starts");
        //TblTenderDetails tblTenderDetails = tenderCorriService.getTenderDetails("tblTenderMaster", Operation_enum.EQ, new TblTenderMaster(Integer.parseInt(tenderid)));

        CorNo = corrigendumNo;

        OfflineDataSrBean offlineDataSrBean = new OfflineDataSrBean();
        preqExcellBean = (PrequalificationExcellBean) offlineDataSrBean.editTenderWithPQForm(tenderOLId);

        List<TblCorrigendumDetailOffline> list = new ArrayList<TblCorrigendumDetailOffline>();

        
        if(!preqExcellBean.getProcurementNature().equals(tenderDetailsOffline.getProcurementNature()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "procurementNature",preqExcellBean.getProcurementNature(), tenderDetailsOffline.getProcurementNature(), corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getProcurementType().equals(tenderDetailsOffline.getProcurementType()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "procurementType",preqExcellBean.getProcurementType(), tenderDetailsOffline.getProcurementType(), corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getInvitationFor().equals(tenderDetailsOffline.getInvitationFor()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "invitationFor", preqExcellBean.getInvitationFor(), tenderDetailsOffline.getInvitationFor(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getInvitationRefNo().equals(tenderDetailsOffline.getReoiRfpRefNo()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "reoiRfpRefNo", preqExcellBean.getInvitationRefNo(), tenderDetailsOffline.getReoiRfpRefNo(),  corriStatus, CorNo, userID, date));
        }

        oldDate = preqExcellBean.getTenderissuingdate();
        
        DateFormat dtNewDate = new SimpleDateFormat("dd/MM/yyyy");
        newDate = dtNewDate.format(tenderDetailsOffline.getIssueDate());

        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "IssueDate", oldDate, newDate,  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getProcurementMethod().equals(tenderDetailsOffline.getProcurementMethod()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "procurementMethod", preqExcellBean.getProcurementMethod(), tenderDetailsOffline.getProcurementMethod(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getCbobudget().equals(tenderDetailsOffline.getBudgetType()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "budgetType", preqExcellBean.getCbobudget(), tenderDetailsOffline.getBudgetType(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getCbosource().equals(tenderDetailsOffline.getSourceOfFund()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "sourceOfFund", preqExcellBean.getCbosource(), tenderDetailsOffline.getSourceOfFund(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getDevelopmentPartner().equals(tenderDetailsOffline.getDevPartners()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "devPartners", preqExcellBean.getDevelopmentPartner(), tenderDetailsOffline.getDevPartners(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getProjectOrProgrammeCode().equals(tenderDetailsOffline.getProjectCode()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "projectCode", preqExcellBean.getProjectOrProgrammeCode(), tenderDetailsOffline.getProjectCode(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getProjectOrProgrammeName().equals(tenderDetailsOffline.getProjectName()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "projectName", preqExcellBean.getProjectOrProgrammeName(), tenderDetailsOffline.getProjectName(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getTenderPackageNo().equals(tenderDetailsOffline.getPackageNo()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "packageNo", preqExcellBean.getTenderPackageNo(), tenderDetailsOffline.getPackageNo(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getTenderPackageName().equals(tenderDetailsOffline.getPackageName()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "PackageName", preqExcellBean.getTenderPackageName(), tenderDetailsOffline.getPackageName(),  corriStatus, CorNo, userID, date));
        }

        oldDate = preqExcellBean.getPrequalificationPublicationDate();
        newDate = dtNewDate.format(tenderDetailsOffline.getTenderPubDate());

        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "tenderPubDate", oldDate, newDate,  corriStatus, CorNo, userID, date));
        }

        oldDate = preqExcellBean.getPrequalificationClosingDateandTime();
        DateFormat dtNewDate1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        newDate = dtNewDate1.format(tenderDetailsOffline.getClosingDate());

        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "ClosingDate", oldDate, newDate,  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getPlace_Date_TimeofPrequalificationMeeting().equals(tenderDetailsOffline.getPreTenderReoiplace()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "PreTenderREOIPlace", preqExcellBean.getPlace_Date_TimeofPrequalificationMeeting(), tenderDetailsOffline.getPreTenderReoiplace(),  corriStatus, CorNo, userID, date));
        }

        oldDate = preqExcellBean.getDateTimeofPrequalificationMeeting();
        newDate = dtNewDate1.format(tenderDetailsOffline.getPreTenderReoidate());

        if(!oldDate.equals(newDate))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "preTenderREOIDate", oldDate, newDate,  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getSellingPrequalificationDocumentPrincipal().equals(tenderDetailsOffline.getSellingAddPrinciple()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "SellingAddPrinciple", preqExcellBean.getSellingPrequalificationDocumentPrincipal(), tenderDetailsOffline.getSellingAddPrinciple(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getSellingPrequalificationDocumentOthers().equals(tenderDetailsOffline.getSellingAddOthers()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "SellingAddOthers", preqExcellBean.getSellingPrequalificationDocumentOthers(), tenderDetailsOffline.getSellingAddOthers(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getReceivingPrequalification().equals(tenderDetailsOffline.getReceivingAdd()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "ReceivingAdd", preqExcellBean.getReceivingPrequalification(), tenderDetailsOffline.getReceivingAdd(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getEligibilityofApplicant().equals(tenderDetailsOffline.getEligibilityCriteria()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "EligibilityCriteria", preqExcellBean.getEligibilityofApplicant(), tenderDetailsOffline.getEligibilityCriteria(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getBriefDescriptionofGoodsorWorks().equals(tenderDetailsOffline.getBriefDescription()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "BriefDescription", preqExcellBean.getBriefDescriptionofGoodsorWorks(), tenderDetailsOffline.getBriefDescription(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getBriefDescriptionofRelatedServices().equals(tenderDetailsOffline.getRelServicesOrDeliverables()))
        {            
            String text[] = new String[2];
            text[0] = preqExcellBean.getBriefDescriptionofRelatedServices().trim();
            text[1] = tenderDetailsOffline.getRelServicesOrDeliverables().trim();

            for(int i=0;i<2;i++)
            {
                //IF there is any <p> at the strat of the string                
                if (text[i].indexOf("<p>")==0) {
                    text[i] = text[i].substring(3, text[i].length()).trim();                    
                    text[i] = text[i].substring(0, text[i].length()-4).trim();                    
                }                
            }

            if(!text[0].equals(text[1]))
            {
                //list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "RelServicesOrDeliverables", preqExcellBean.getBriefDescriptionofRelatedServices(), tenderDetailsOffline.getRelServicesOrDeliverables(),  corriStatus, userID, date));
                list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "RelServicesOrDeliverables", preqExcellBean.getBriefDescriptionofRelatedServices(), text[1],  corriStatus, CorNo, userID, date));
            }
        }

        if(!preqExcellBean.getPrequalificationDocumentPrice().equals(tenderDetailsOffline.getDocumentPrice().toString()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "DocumentPrice", preqExcellBean.getPrequalificationDocumentPrice(), tenderDetailsOffline.getDocumentPrice().toString(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getNameofOfficialInvitingPrequalification().equals(tenderDetailsOffline.getPeOfficeName()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "peName", preqExcellBean.getNameofOfficialInvitingPrequalification(), tenderDetailsOffline.getPeOfficeName(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getDesignationofOfficialInvitingPrequalification().equals(tenderDetailsOffline.getPeDesignation()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "peDesignation", preqExcellBean.getDesignationofOfficialInvitingPrequalification(), tenderDetailsOffline.getPeDesignation(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getAddressofOfficialInvitingPrequalification().equals(tenderDetailsOffline.getPeAddress()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "peAddress", preqExcellBean.getAddressofOfficialInvitingPrequalification(), tenderDetailsOffline.getPeAddress(),  corriStatus, CorNo, userID, date));
        }

        if(!preqExcellBean.getContactdetailsofOfficialInvitingPrequalification().equals(tenderDetailsOffline.getPeContactDetails()))
        {
            list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), "N/A", "peContactDetails", preqExcellBean.getContactdetailsofOfficialInvitingPrequalification(), tenderDetailsOffline.getPeContactDetails(),  corriStatus, CorNo, userID, date));
        }
                                
            //try
            //{
              //Class noparams[] = {};
              //Class cls = Class.forName("com.cptu.egp.eps.web.offlinedata.PrequalificationExcellBean");
              //  Object obj = cls.newInstance();

                //Method method = cls.getDeclaredMethod("getLotNo_1", noparams);
                //Object testLot = method.invoke(obj, null);
                //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>"+testLot.toString());
            //}
            //catch(Exception ex){
              //  ex.printStackTrace();
            //}

        String[][] lotDetails = {
                                        {"1", "2", "3", "4"},
                                        {"Lot Number", "Lot Identification", "Location", "Completion Time"}
                                    };
        List<TblTenderLotPhasingOffline> tenderLotsAndPhases = tenderDetailsOffline.getTenderLotsAndPhases();

                //FIRST LOT
                if(!preqExcellBean.getLotNo_1().equals(tenderLotsAndPhases.get(0).getLotOrRefNo()))
                {
                   list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][0], lotDetails[1][0] , preqExcellBean.getLotNo_1(), tenderLotsAndPhases.get(0).getLotOrRefNo(),  corriStatus, CorNo, userID, date));
                }

                if(!preqExcellBean.getIdentificationOfLot_1().equals(tenderLotsAndPhases.get(0).getLotIdentOrPhasingServ()))
                {
                   list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][0], lotDetails[1][1] , preqExcellBean.getIdentificationOfLot_1(), tenderLotsAndPhases.get(0).getLotIdentOrPhasingServ(),  corriStatus, CorNo, userID, date));
                }

                if(!preqExcellBean.getLocation_1().equals(tenderLotsAndPhases.get(0).getLocation()))
                {
                   list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][0], lotDetails[1][2] , preqExcellBean.getLocation_1(), tenderLotsAndPhases.get(0).getLocation(),  corriStatus, CorNo, userID, date));
                }

                if(!preqExcellBean.getCompletionTimeInWeeksORmonths_1().equals(tenderLotsAndPhases.get(0).getCompletionDateTime()))
                {
                   list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][0], lotDetails[1][3] , preqExcellBean.getCompletionTimeInWeeksORmonths_1(), tenderLotsAndPhases.get(0).getCompletionDateTime(),  corriStatus, CorNo, userID, date));
                }
                
                if(tenderLotsAndPhases.size()>1)
                {
                //FOR SECOND LOT
                if(!tenderLotsAndPhases.get(1).getLotOrRefNo().equals("") || !tenderLotsAndPhases.get(1).getLotIdentOrPhasingServ().equals("") || !tenderLotsAndPhases.get(1).getLocation().equals("") ||  !tenderLotsAndPhases.get(1).getCompletionDateTime().equals(""))
                {
                    String test= CommonUtils.checkNull(preqExcellBean.getLotNo_2());
                    String test1= CommonUtils.checkNull(tenderLotsAndPhases.get(1).getLotOrRefNo());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][1], lotDetails[1][0] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(preqExcellBean.getIdentificationOfLot_2());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(1).getLotIdentOrPhasingServ());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][1], lotDetails[1][1] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(preqExcellBean.getLocation_2());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(1).getLocation());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][1], lotDetails[1][2] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(preqExcellBean.getCompletionTimeInWeeksORmonths_2());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(1).getCompletionDateTime());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][1], lotDetails[1][3] , test, test1,  corriStatus, CorNo, userID, date));
                    }
                                        
                }
        }
        if(tenderLotsAndPhases.size()>2)
        {
        //FOR THIRD LOT
                if(!tenderLotsAndPhases.get(2).getLotOrRefNo().equals("") || !tenderLotsAndPhases.get(2).getLotIdentOrPhasingServ().equals("") || !tenderLotsAndPhases.get(2).getLocation().equals("") ||  !tenderLotsAndPhases.get(2).getCompletionDateTime().equals(""))
                {
                    String test= CommonUtils.checkNull(preqExcellBean.getLotNo_3());
                    String test1= CommonUtils.checkNull(tenderLotsAndPhases.get(2).getLotOrRefNo());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][2], lotDetails[1][0] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(preqExcellBean.getIdentificationOfLot_3());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(2).getLotIdentOrPhasingServ());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][2], lotDetails[1][1] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(preqExcellBean.getLocation_3());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(2).getLocation());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][2], lotDetails[1][2] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(preqExcellBean.getCompletionTimeInWeeksORmonths_3());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(2).getCompletionDateTime());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][2], lotDetails[1][3] , test, test1,  corriStatus, CorNo, userID, date));
                    }
                                        
                }
        }
        if(tenderLotsAndPhases.size()>3)
        {
        //FOR FORTH LOT
                if(!tenderLotsAndPhases.get(3).getLotOrRefNo().equals("") || !tenderLotsAndPhases.get(3).getLotIdentOrPhasingServ().equals("") || !tenderLotsAndPhases.get(3).getLocation().equals("") ||  !tenderLotsAndPhases.get(3).getCompletionDateTime().equals(""))
                {
                    String test= CommonUtils.checkNull(preqExcellBean.getLotNo_4());
                    String test1= CommonUtils.checkNull(tenderLotsAndPhases.get(3).getLotOrRefNo());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][3], lotDetails[1][0] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(preqExcellBean.getIdentificationOfLot_4());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(3).getLotIdentOrPhasingServ());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][3], lotDetails[1][1] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(preqExcellBean.getLocation_4());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(3).getLocation());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][3], lotDetails[1][2] , test, test1,  corriStatus, CorNo, userID, date));
                    }

                    test= CommonUtils.checkNull(preqExcellBean.getCompletionTimeInWeeksORmonths_4());
                    test1= CommonUtils.checkNull(tenderLotsAndPhases.get(3).getCompletionDateTime());
                    if(!test.equals(test1))
                    {
                        list.add(new TblCorrigendumDetailOffline(0, new TblTenderDetailsOffline(tenderOLId), lotDetails[0][3], lotDetails[1][3] , test, test1,  corriStatus, CorNo, userID, date));
                    }
                                        
                }
        }
                                    
      if (!list.isEmpty())
      {
          boolean success;
          if(isEdit)
          {
              corrigendumDetailOffline = offlineDataSrBean.corrigendumDetails(tenderOLId);
              /*for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline)
              {
                success= corrigendumDetailsOfflineService.deleteCorriDetailsOffline(corrigendumDetailOffline, userID, detail.getCorrigendumOlid());
              }*/

              success= corrigendumDetailsOfflineService.deleteCorriDetailsOffline(corrigendumDetailOffline, userID, tenderOLId);
              corrigendumDetailsOfflineService.insertCorriDetailsOffline(list, userID);
          }
          else
          {
            corrigendumDetailsOfflineService.insertCorriDetailsOffline(list, userID);
          }        
      
        LOGGER.debug("createCorri : " + userID + "ends");
    }




  }

}

