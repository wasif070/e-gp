/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsInvAccHistory;
import com.cptu.egp.eps.model.table.TblCmsInvRemarks;
import com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails;
import com.cptu.egp.eps.model.table.TblCmsInvoiceDetails;
import com.cptu.egp.eps.model.table.TblCmsInvoiceMaster;
import com.cptu.egp.eps.model.table.TblCmsWpMaster;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.servicebean.TenderTablesSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class AccPaymentServlet extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
    private static final Logger LOGGER = Logger.getLogger(ConfigureDateServlet.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String logUserId = "0";
        String userTypeId = "";
        String serviceType = "";
        try {
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            int conId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                accPaymentService.setLogUserId(logUserId);
                registerService.setUserId(logUserId);
                commonservice.setUserId(logUserId);
                cmsConfigDateService.setLogUserId(logUserId);
            }
             TenderTablesSrBean beanCommon = new TenderTablesSrBean();
            String tenderType =  beanCommon.getTenderType(Integer.parseInt(request.getParameter("tenderId")));
            String procnature = commonservice.getProcNature(request.getParameter("tenderId")).toString();
            serviceType= commonservice.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
            if (request.getSession().getAttribute("userTypeId") != null) {
                userTypeId = request.getSession().getAttribute("userTypeId").toString();
            }
            LOGGER.debug("processRequest : " + logUserId + " Starts");
            String action = request.getParameter("action");

            String tenderId = "";
            if (request.getParameter("tenderId") != null) {
                tenderId = request.getParameter("tenderId");
            }
            String lotId = "";
            if (request.getParameter("lotId") != null) {
                lotId = request.getParameter("lotId");
            }
            if ("saveInvoiceAccDetais".equalsIgnoreCase(action)) {
                try {
                    boolean flag = false;
                    String InvoiceId = "";
                    String wpId = "";
                    String totalCurrency = "";
                    String invoiceNo = "";
                    if (request.getParameter("totalCurrency") != null) {
                        totalCurrency = request.getParameter("totalCurrency");
                       //  System.out.println("totalCurrency"+totalCurrency);
                    }
                  for(int incr = 0;incr< Integer.parseInt(totalCurrency);incr++ )
                  {
                    if (request.getParameter("invoiceNo") != null) {
                        invoiceNo = request.getParameter("invoiceNo");
                        
                    }

                    InvoiceId = "";
                    if (request.getParameter("InvoiceId"+incr) != null) {
                        InvoiceId = request.getParameter("InvoiceId"+incr);
                      //  System.out.println("InvoiceId"+InvoiceId);
                    }
                    String InvoiceAccDtId = "";
                    if (request.getParameter("InvoiceAccDtId"+incr) != null) {
                        InvoiceAccDtId = request.getParameter("InvoiceAccDtId"+incr);
                    }
                    wpId = "";
                    if (request.getParameter("wpId") != null) {
                        wpId = request.getParameter("wpId");
                    }
                    String strInvoiceAmt = "0";
                    if (request.getParameter("invoiceAmttxt"+incr) != null && !"".equals(request.getParameter("invoiceAmttxt"+incr))) {
                        strInvoiceAmt = request.getParameter("invoiceAmttxt"+incr);
                    }
                    String strAdvAmt = "0";
                    if (request.getParameter("AdvAmttxt") != null && !"".equals(request.getParameter("AdvAmttxt"))) {
                        strAdvAmt = request.getParameter("AdvAmttxt");
                    }
                    String strAdvAdjAmt = "0";
                    if (request.getParameter("AdvAdjuAmttxt") != null && !"".equals(request.getParameter("AdvAdjuAmttxt"))) {
                        strAdvAdjAmt = request.getParameter("AdvAdjuAmttxt");
                    }
                     String strSalvageAdjAmt = "0";
                    if (request.getParameter("SalvageAdjuAmttxt") != null && !"".equals(request.getParameter("SalvageAdjuAmttxt"))) {
                        strSalvageAdjAmt = request.getParameter("SalvageAdjuAmttxt");
                    }
                    String strRetention = "0";
                    if (request.getParameter("Retensiotxt") != null && !"".equals(request.getParameter("Retensiotxt"))) {
                        strRetention = request.getParameter("Retensiotxt");
                    }
                    String strVatAmt = "0";
                    if (request.getParameter("Vattxt") != null && !"".equals(request.getParameter("Vattxt"))) {
                        strVatAmt = request.getParameter("Vattxt");
                    }
                    String strAitAmt = "0";
                    if (request.getParameter("Aittxt") != null && !"".equals(request.getParameter("Aittxt"))) {
                        strAitAmt = request.getParameter("Aittxt");
                    }
                    String strAdvVatAmt = "0";
                    if (request.getParameter("AdvVatAdjAmttxt") != null && !"".equals(request.getParameter("AdvVatAdjAmttxt"))) {
                        strAdvVatAmt = request.getParameter("AdvVatAdjAmttxt");
                    }
                    String strAdvAitAmt = "0";
                    if (request.getParameter("AdvAitAdjAmttxt") != null && !"".equals(request.getParameter("AdvAitAdjAmttxt"))) {
                        strAdvAitAmt = request.getParameter("AdvAitAdjAmttxt");
                    }
                    String strBonusAmt = "0";
                    if (request.getParameter("Bonustxt") != null && !"".equals(request.getParameter("Bonustxt"))) {
                        strBonusAmt = request.getParameter("Bonustxt");
                    }
                    String InterestOnDP = "0";
                    if (request.getParameter("InterestOnDP") != null && !"".equals(request.getParameter("InterestOnDP"))) {
                        InterestOnDP = request.getParameter("InterestOnDP");
                    }
                    String strPenaltyAmt = "0";
                    if (request.getParameter("PenaltyAmttxt") != null && !"".equals(request.getParameter("PenaltyAmttxt"))) {
                        strPenaltyAmt = request.getParameter("PenaltyAmttxt");
                    }
                    String strLdAmt = "0";
                    if (request.getParameter("LiqDamagetxt") != null && !"".equals(request.getParameter("LiqDamagetxt"))) {
                        strLdAmt = request.getParameter("LiqDamagetxt");
                    }
                    String ModeofPayment = "";
                    if (request.getParameter("modeofPayment") != null) {
                        ModeofPayment = request.getParameter("modeofPayment");
                    }
                    String DateofPayment = "";
                    if (request.getParameter("Paymentdate") != null) {
                        DateofPayment = request.getParameter("Paymentdate");
                    }
                    String BankName = "";
                    if (request.getParameter("BankNametxt") != null) {
                        BankName = request.getParameter("BankNametxt");
                    }
                    String BranchName = "";
                    if (request.getParameter("BranchNametxt") != null) {
                        BranchName = request.getParameter("BranchNametxt");
                    }
                    String InstrumentNumber = "";
                    if (request.getParameter("InstNumbertxt") != null) {
                        InstrumentNumber = request.getParameter("InstNumbertxt");
                    }
                    String vatModeofPayment = "";
                    if (request.getParameter("vatmodeofPayment") != null) {
                        vatModeofPayment = request.getParameter("vatmodeofPayment");
                    }
                    String vatDateofPayment = "";
                    if (request.getParameter("vatPaymentdate") != null) {
                        vatDateofPayment = request.getParameter("vatPaymentdate");
                    }
                    String vatBankName = "";
                    if (request.getParameter("vatBankNametxt") != null) {
                        vatBankName = request.getParameter("vatBankNametxt");
                    }
                    String vatBranchName = "";
                    if (request.getParameter("vatBranchNametxt") != null) {
                        vatBranchName = request.getParameter("vatBranchNametxt");
                    }
                    String vatInstrumentNumber = "";
                    if (request.getParameter("vatInstNumbertxt") != null) {
                        vatInstrumentNumber = request.getParameter("vatInstNumbertxt");
                    }
                    String aitModeofPayment = "";
                    if (request.getParameter("aitmodeofPayment") != null) {
                        aitModeofPayment = request.getParameter("aitmodeofPayment");
                    }
                    String aitDateofPayment = "";
                    if (request.getParameter("aitPaymentdate") != null) {
                        aitDateofPayment = request.getParameter("aitPaymentdate");
                    }
                    String aitBankName = "";
                    if (request.getParameter("aitBankNametxt") != null) {
                        aitBankName = request.getParameter("aitBankNametxt");
                    }
                    String aitBranchName = "";
                    if (request.getParameter("aitBranchNametxt") != null) {
                        aitBranchName = request.getParameter("aitBranchNametxt");
                    }
                    String aitInstrumentNumber = "";
                    if (request.getParameter("aitInstNumbertxt") != null) {
                        aitInstrumentNumber = request.getParameter("aitInstNumbertxt");
                    }
                    String strGrossAmt = "0";
                    if (request.getParameter("GrossAmttxt"+incr) != null && !"".equals(request.getParameter("GrossAmttxt"+incr))) {
                        strGrossAmt = request.getParameter("GrossAmttxt"+incr);
                       //  System.out.println("strGrossAmt"+strGrossAmt);
                    }
                    TblCmsInvoiceAccDetails tblCmsInvoiceAccDetails = new TblCmsInvoiceAccDetails();
                    tblCmsInvoiceAccDetails.setTblCmsWpMaster(new TblCmsWpMaster(Integer.parseInt(wpId)));
                    tblCmsInvoiceAccDetails.setInvoiceAmt(new BigDecimal(strInvoiceAmt));
                    tblCmsInvoiceAccDetails.setAdvAmt(new BigDecimal(strAdvAmt));
                    tblCmsInvoiceAccDetails.setAdvAdjAmt(new BigDecimal(strAdvAdjAmt));
                     tblCmsInvoiceAccDetails.setSalvageAdjAmt(new BigDecimal(strSalvageAdjAmt));
                    if ("+".equalsIgnoreCase(request.getParameter("hiddenRetensionaction"))) {
                        tblCmsInvoiceAccDetails.setRetentionMadd(new BigDecimal(strRetention));
                    } else if ("-".equalsIgnoreCase(request.getParameter("hiddenRetensionaction"))) {
                        tblCmsInvoiceAccDetails.setRetentionMdeduct(new BigDecimal(strRetention));
                    }
                    tblCmsInvoiceAccDetails.setPgDeduct(new BigDecimal(0));
                    tblCmsInvoiceAccDetails.setPgAdd(new BigDecimal(0));
                    tblCmsInvoiceAccDetails.setVatAmt(new BigDecimal(strVatAmt));
                    tblCmsInvoiceAccDetails.setAitAmt(new BigDecimal(strAitAmt));
                    tblCmsInvoiceAccDetails.setAdvVatAmt(new BigDecimal(strAdvVatAmt));
                    tblCmsInvoiceAccDetails.setAdvAitAmt(new BigDecimal(strAdvAitAmt));
                    tblCmsInvoiceAccDetails.setBonusAmt(new BigDecimal(strBonusAmt));
                    tblCmsInvoiceAccDetails.setInterestonDP(new BigDecimal(InterestOnDP));
                    tblCmsInvoiceAccDetails.setPenaltyAmt(new BigDecimal(strPenaltyAmt));
                    tblCmsInvoiceAccDetails.setldAmt(new BigDecimal(strLdAmt));
                    tblCmsInvoiceAccDetails.setGrossAmt(new BigDecimal(strGrossAmt));
                    tblCmsInvoiceAccDetails.setGeneratedDate(new java.sql.Date(new java.util.Date().getTime()));
                    tblCmsInvoiceAccDetails.setGeneratedBy(Integer.parseInt(logUserId));
                    tblCmsInvoiceAccDetails.setModeOfPayment(ModeofPayment);
                    Date DateofPaymentdt = new Date();
                    DateofPaymentdt = DateUtils.formatStdString(DateofPayment);
                    tblCmsInvoiceAccDetails.setDateOfPayment(new java.sql.Date(DateofPaymentdt.getTime()));
                    tblCmsInvoiceAccDetails.setBankName(BankName);
                    tblCmsInvoiceAccDetails.setBranchName(BranchName);
                    tblCmsInvoiceAccDetails.setInstrumentNo(InstrumentNumber);
                    tblCmsInvoiceAccDetails.setVatmodeOfPayment(vatModeofPayment);
                    Date vatDateofPaymentdt = new Date();
                    vatDateofPaymentdt = DateUtils.formatStdString(vatDateofPayment);
                    tblCmsInvoiceAccDetails.setVatdateOfPayment(vatDateofPaymentdt);
                    tblCmsInvoiceAccDetails.setVatbankName(vatBankName);
                    tblCmsInvoiceAccDetails.setVatbranchName(vatBranchName);
                    tblCmsInvoiceAccDetails.setVatinstrumentNo(vatInstrumentNumber);
                    tblCmsInvoiceAccDetails.setAitmodeOfPayment(aitModeofPayment);
                    Date aitDateofPaymentdt = new Date();
                    aitDateofPaymentdt = DateUtils.formatStdString(aitDateofPayment);
                    tblCmsInvoiceAccDetails.setAitdateOfPayment(aitDateofPaymentdt);
                    tblCmsInvoiceAccDetails.setAitbankName(aitBankName);
                    tblCmsInvoiceAccDetails.setAitbranchName(aitBranchName);
                    tblCmsInvoiceAccDetails.setAitinstrumentNo(aitInstrumentNumber);
                    tblCmsInvoiceAccDetails.setTblCmsInvoiceMaster(new TblCmsInvoiceMaster(Integer.parseInt(InvoiceId)));
                  
                    if ("Save As Draft".equalsIgnoreCase(request.getParameter("SavePaymentname"))) {
                        flag = accPaymentService.addInvoiceAccDetails(tblCmsInvoiceAccDetails);
                        if (flag) {
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Process Invoice", "Draft Invoice");
                            TblCmsInvAccHistory tblCmsInvAccHistory = new TblCmsInvAccHistory();
                            tblCmsInvAccHistory.setTblCmsInvoiceAccDetails(new TblCmsInvoiceAccDetails(tblCmsInvoiceAccDetails.getInvoiceAccDtlId()));
                            tblCmsInvAccHistory.setInvoiceId(tblCmsInvoiceAccDetails.getTblCmsInvoiceMaster().getInvoiceId());
                            tblCmsInvAccHistory.setAdvAmt(tblCmsInvoiceAccDetails.getAdvAmt());
                            tblCmsInvAccHistory.setAdvAdjAmt(tblCmsInvoiceAccDetails.getAdvAdjAmt());
                            tblCmsInvAccHistory.setRetentionMadd(tblCmsInvoiceAccDetails.getRetentionMadd());
                            tblCmsInvAccHistory.setRetentionMdeduct(tblCmsInvoiceAccDetails.getRetentionMdeduct());
                            tblCmsInvAccHistory.setVatAmt(tblCmsInvoiceAccDetails.getVatAmt());
                            tblCmsInvAccHistory.setAitAmt(tblCmsInvoiceAccDetails.getAitAmt());
                            tblCmsInvAccHistory.setAdvVatAmt(tblCmsInvoiceAccDetails.getAdvVatAmt());
                            tblCmsInvAccHistory.setAdvAitAmt(tblCmsInvoiceAccDetails.getAdvAitAmt());
                            tblCmsInvAccHistory.setBonusAmt(tblCmsInvoiceAccDetails.getBonusAmt());
                            tblCmsInvAccHistory.setInterestonDP(tblCmsInvoiceAccDetails.getInterestonDP());
                            tblCmsInvAccHistory.setPenaltyAmt(tblCmsInvoiceAccDetails.getPenaltyAmt());
                            tblCmsInvAccHistory.setLdAmt(tblCmsInvoiceAccDetails.getldAmt());
                            tblCmsInvAccHistory.setGrossAmt(tblCmsInvoiceAccDetails.getGrossAmt());
                            tblCmsInvAccHistory.setGeneratedDt(tblCmsInvoiceAccDetails.getGeneratedDate());
                            tblCmsInvAccHistory.setModeOfPayment(tblCmsInvoiceAccDetails.getModeOfPayment());
                            tblCmsInvAccHistory.setDateOfPayment(tblCmsInvoiceAccDetails.getDateOfPayment());
                            tblCmsInvAccHistory.setBankName(tblCmsInvoiceAccDetails.getBankName());
                            tblCmsInvAccHistory.setBranchName(tblCmsInvoiceAccDetails.getBranchName());
                            tblCmsInvAccHistory.setInstrumentNo(tblCmsInvoiceAccDetails.getInstrumentNo());
                            tblCmsInvAccHistory.setVatmodeOfPayment(tblCmsInvoiceAccDetails.getVatmodeOfPayment());
                            tblCmsInvAccHistory.setVatdateOfPayment(tblCmsInvoiceAccDetails.getVatdateOfPayment());
                            tblCmsInvAccHistory.setVatbankName(tblCmsInvoiceAccDetails.getVatbankName());
                            tblCmsInvAccHistory.setVatbranchName(tblCmsInvoiceAccDetails.getVatbranchName());
                            tblCmsInvAccHistory.setVatinstrumentNo(tblCmsInvoiceAccDetails.getVatinstrumentNo());
                            tblCmsInvAccHistory.setAitmodeOfPayment(tblCmsInvoiceAccDetails.getAitmodeOfPayment());
                            tblCmsInvAccHistory.setAitdateOfPayment(tblCmsInvoiceAccDetails.getAitdateOfPayment());
                            tblCmsInvAccHistory.setAitbankName(tblCmsInvoiceAccDetails.getAitbankName());
                            tblCmsInvAccHistory.setAitbranchName(tblCmsInvoiceAccDetails.getAitbranchName());
                            tblCmsInvAccHistory.setAitinstrumentNo(tblCmsInvoiceAccDetails.getAitinstrumentNo());
                            tblCmsInvAccHistory.setEditedWhen("Calculated by Ac");
                            flag = accPaymentService.addInvAccHistoryDetails(tblCmsInvAccHistory);
                        }
                       // if (flag) {
                            //response.sendRedirect("officer/AccPaymentDocUpload.jsp?tenderId="+tenderId+"&InvoiceId="+InvoiceId+"&msg=succ");
                       //     response.sendRedirect("officer/AccPaymentView.jsp?tenderId=" + tenderId + "&InvoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&msg=succ");
                      //  } else {
                     //       response.sendRedirect("officer/AccPayment.jsp?tenderId=" + tenderId + "&InvoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&msg=failed");
                     //   }
                    } else
                    {
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Edit Invoice details (Process Invoice)", "Draft Invoice");
                        tblCmsInvoiceAccDetails.setInvoiceAccDtlId(Integer.parseInt(InvoiceAccDtId));
                        flag = accPaymentService.updateInvoiceAccDetails(tblCmsInvoiceAccDetails);
                        if (flag) {
                            TblCmsInvAccHistory tblCmsInvAccHistory = new TblCmsInvAccHistory();
                            tblCmsInvAccHistory.setTblCmsInvoiceAccDetails(new TblCmsInvoiceAccDetails(tblCmsInvoiceAccDetails.getInvoiceAccDtlId()));
                            tblCmsInvAccHistory.setInvoiceId(tblCmsInvoiceAccDetails.getTblCmsInvoiceMaster().getInvoiceId());
                            tblCmsInvAccHistory.setAdvAmt(tblCmsInvoiceAccDetails.getAdvAmt());
                            tblCmsInvAccHistory.setAdvAdjAmt(tblCmsInvoiceAccDetails.getAdvAdjAmt());
                            tblCmsInvAccHistory.setRetentionMadd(tblCmsInvoiceAccDetails.getRetentionMadd());
                            tblCmsInvAccHistory.setRetentionMdeduct(tblCmsInvoiceAccDetails.getRetentionMdeduct());
                            tblCmsInvAccHistory.setVatAmt(tblCmsInvoiceAccDetails.getVatAmt());
                            tblCmsInvAccHistory.setAitAmt(tblCmsInvoiceAccDetails.getAitAmt());
                            tblCmsInvAccHistory.setAdvVatAmt(tblCmsInvoiceAccDetails.getAdvVatAmt());
                            tblCmsInvAccHistory.setAdvAitAmt(tblCmsInvoiceAccDetails.getAdvAitAmt());
                            tblCmsInvAccHistory.setBonusAmt(tblCmsInvoiceAccDetails.getBonusAmt());
                            tblCmsInvAccHistory.setInterestonDP(tblCmsInvoiceAccDetails.getInterestonDP());
                            tblCmsInvAccHistory.setPenaltyAmt(tblCmsInvoiceAccDetails.getPenaltyAmt());
                            tblCmsInvAccHistory.setLdAmt(tblCmsInvoiceAccDetails.getldAmt());
                            tblCmsInvAccHistory.setGrossAmt(tblCmsInvoiceAccDetails.getGrossAmt());
                            tblCmsInvAccHistory.setGeneratedDt(tblCmsInvoiceAccDetails.getGeneratedDate());
                            tblCmsInvAccHistory.setModeOfPayment(tblCmsInvoiceAccDetails.getModeOfPayment());
                            tblCmsInvAccHistory.setDateOfPayment(tblCmsInvoiceAccDetails.getDateOfPayment());
                            tblCmsInvAccHistory.setBankName(tblCmsInvoiceAccDetails.getBankName());
                            tblCmsInvAccHistory.setBranchName(tblCmsInvoiceAccDetails.getBranchName());
                            tblCmsInvAccHistory.setInstrumentNo(tblCmsInvoiceAccDetails.getInstrumentNo());
                            tblCmsInvAccHistory.setVatmodeOfPayment(tblCmsInvoiceAccDetails.getVatmodeOfPayment());
                            tblCmsInvAccHistory.setVatdateOfPayment(tblCmsInvoiceAccDetails.getVatdateOfPayment());
                            tblCmsInvAccHistory.setVatbankName(tblCmsInvoiceAccDetails.getVatbankName());
                            tblCmsInvAccHistory.setVatbranchName(tblCmsInvoiceAccDetails.getVatbranchName());
                            tblCmsInvAccHistory.setVatinstrumentNo(tblCmsInvoiceAccDetails.getVatinstrumentNo());
                            tblCmsInvAccHistory.setAitmodeOfPayment(tblCmsInvoiceAccDetails.getAitmodeOfPayment());
                            tblCmsInvAccHistory.setAitdateOfPayment(tblCmsInvoiceAccDetails.getAitdateOfPayment());
                            tblCmsInvAccHistory.setAitbankName(tblCmsInvoiceAccDetails.getAitbankName());
                            tblCmsInvAccHistory.setAitbranchName(tblCmsInvoiceAccDetails.getAitbranchName());
                            tblCmsInvAccHistory.setAitinstrumentNo(tblCmsInvoiceAccDetails.getAitinstrumentNo());
                            tblCmsInvAccHistory.setEditedWhen("Calculated by Ac");
                            tblCmsInvAccHistory.setEditedWhen("Edited by Ac");
                            flag = accPaymentService.addInvAccHistoryDetails(tblCmsInvAccHistory);
                        }
                      }
                    }
                     if ("Save As Draft".equalsIgnoreCase(request.getParameter("SavePaymentname")))
                     {
                     if (flag) {
                            //response.sendRedirect("officer/AccPaymentDocUpload.jsp?tenderId="+tenderId+"&InvoiceId="+InvoiceId+"&msg=succ");
                           if(!invoiceNo.equals(""))
                                response.sendRedirect("officer/AccPaymentView.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&invoiceNo=" + invoiceNo + "&msg=succ");
                           else
                                response.sendRedirect("officer/AccPaymentView.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&msg=succ");
                        } else {
                             if(!invoiceNo.equals(""))
                                 response.sendRedirect("officer/AccPayment.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&invoiceNo=" + invoiceNo + "&msg=failed");

                             else
                                 response.sendRedirect("officer/AccPayment.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&msg=failed");
                        }
                     }
                    else{
                        if (flag) {
                              if(!invoiceNo.equals(""))
                                response.sendRedirect("officer/AccPaymentView.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&invoiceNo=" + invoiceNo + "&msg=updated");
                           else
                                response.sendRedirect("officer/AccPaymentView.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&msg=updated");



                        } else {
                             if(!invoiceNo.equals(""))
                                 response.sendRedirect("officer/AccPayment.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&invoiceNo=" + invoiceNo + "&msg=updationfailed");
                              else
                                 response.sendRedirect("officer/AccPayment.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&msg=updationfailed");

                        }
                      }
                  //  }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    //makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Process Invoice", "Save Invoice");
                }
            } else if ("sendtope".equalsIgnoreCase(action)) {
                try {
                    String InvoiceId = "";
                    boolean flag = false;
                    List<Object> list = null;

                    if (request.getParameter("InvoiceId") != null) {
                        InvoiceId = request.getParameter("InvoiceId");
                    }
                    String wpId = "";
                    if (request.getParameter("wpId") != null) {
                        wpId = request.getParameter("wpId");
                    }

                    String invoiceNo = "";
                    if (request.getParameter("invoiceNo") != null) {
                        invoiceNo = request.getParameter("invoiceNo");
                    }
                   if(!invoiceNo.equals(""))
                    {
                    list = service.getInvoiceIdForICT(invoiceNo, Integer.parseInt(wpId));
                    if(list.size()>0){
                            for (Object oob : list) {
                                flag = accPaymentService.updateStatusInInvMaster("sendtope", Integer.parseInt(oob.toString()));
                                }
                            }
                    }
                    else
                        flag = accPaymentService.updateStatusInInvMaster("sendtope", Integer.parseInt(InvoiceId));

                    sendMailForSendToPE(tenderId, lotId, logUserId.toString(), "has configured", "e-GP: Invoice details has been configured by Accounts Officer",InvoiceId);

                    if (flag) {
                        response.sendRedirect("officer/MakePayment.jsp?btnMsag=sendtope");
                    } else {
                          if(!invoiceNo.equals(""))
                                response.sendRedirect("officer/AccPaymentView.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&invoiceNo=" + invoiceNo + "&msg=sendtope");
                          else
                                response.sendRedirect("officer/AccPaymentView.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&msg=sendtope");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Send to PE (Process Invoice)", "send to PE");
                }
            } else if ("sendtotenderer".equalsIgnoreCase(action)) {
                try {
                    String InvoiceId = "";
                    List<Object> list = null;
                    boolean flag = false;
                    String wpId = "";
                    if (request.getParameter("wpId") != null) {
                        wpId = request.getParameter("wpId");
                    }
                    if (request.getParameter("InvoiceId") != null) {
                        InvoiceId = request.getParameter("InvoiceId");
                    }
                    String invoiceNo = "";
                    if (request.getParameter("invoiceNo") != null) {
                        invoiceNo = request.getParameter("invoiceNo");
                    }
                   if(!invoiceNo.equals(""))
                    {
                    list = service.getInvoiceIdForICT(invoiceNo, Integer.parseInt(wpId));
                    if(list.size()>0){
                            for (Object oob : list) {
                                flag = accPaymentService.updateStatusInInvMaster("sendtotenderer", Integer.parseInt(oob.toString()));
                                }
                            }
                    }
                    else
                        flag = accPaymentService.updateStatusInInvMaster("sendtotenderer", Integer.parseInt(InvoiceId));

                   
                    sendMailForSendToTenderer(tenderId, lotId, InvoiceId, logUserId.toString(), "has configured", "e-GP: Payment details has been configured by Accounts Officer");
                    if (flag) {
                        response.sendRedirect("officer/MakePayment.jsp?btnMsag=sendtotenderer");
                    } else {
                        if(!invoiceNo.equals(""))
                                response.sendRedirect("officer/AccPaymentView.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&invoiceNo=" + invoiceNo + "&msg=sendtotenderer");
                        else
                                response.sendRedirect("officer/AccPaymentView.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&wpId=" + wpId + "&lotId=" + lotId + "&msg=sendtotenderer");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Send to Tenderer(Process Invoice)", "send to tenderer");
                }
            } else if ("acceptInvoice".equalsIgnoreCase(action)) {
                String InvoiceId = "";
                
                String status = request.getParameter("status");
                String remarks = request.getParameter("remarks");
                if (request.getParameter("InvoiceId") != null) {
                    InvoiceId = request.getParameter("InvoiceId");

                }
                
                boolean upd = false;
                if ("accept".equalsIgnoreCase(status)) {
                    try {
                        if(InvoiceId.contains("Invoice")){
                            List<Object> list = null;
                            list = service.getInvoiceIdForICT(InvoiceId, Integer.parseInt(request.getParameter("wpId")));
                            if(list.size()>0){
                            for (Object oob : list) {
                                upd = service.updateStatusInInvMaster(Integer.parseInt(oob.toString()), remarks);
                                }
                            }
                        }
                        else
                        {
                             upd = service.updateStatusInInvMaster(Integer.parseInt(InvoiceId), remarks);
                        }

                        //sendMailForInvoiceGeneratedbySupplier(request.getParameter("tenderId"),request.getParameter("lotId"),user.toString(),"has generated","e-GP: Supplier has Generated Invoice");
                        if(!InvoiceId.contains("Invoice")){
                        sendMailForInvoiceAcceptRejectByPE(request.getParameter("tenderId"), request.getParameter("lotId"), logUserId, "has Accepted", "e-GP: Procuring entity has Accepted Invoice",InvoiceId);
                        }
                        if (upd) {
                            if("services".equalsIgnoreCase(procnature))
                            {    
                                response.sendRedirect("officer/InvoiceServiceCase.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=accept");
                            }else{
                                response.sendRedirect("officer/Invoice.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=accept");
                            }    
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Process Invoice", "Accept Invoice");
                    }
                } else {
                    try {

                        /* dohatec start*/
                         if(InvoiceId.contains("Invoice")){
                            List<Object> listICT = null;
                            listICT = service.getInvoiceIdForICT(InvoiceId, Integer.parseInt(request.getParameter("wpId")));
                            if(listICT.size()>0){
                            for (Object oob : listICT) {
                               InvoiceId = oob.toString();
                               List<TblCmsInvoiceMaster> InvMasdata = c_ConsSrv.getTblCmsInvoiceMaster(Integer.parseInt(InvoiceId));
                                if (!InvMasdata.isEmpty()) {
                                    if ("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv())) {
                                        service.updateInvmaster(Integer.parseInt(InvoiceId), remarks);
                                    } else {
                                        List<TblCmsInvoiceDetails> list = service.getAllDetailByInvoiceId(Integer.parseInt(InvoiceId));
                                        if("services".equalsIgnoreCase(procnature)){
                                            service.updateInvmaster(Integer.parseInt(InvoiceId), remarks);
                                        }
                                        if (!list.isEmpty()) {
                                            for (TblCmsInvoiceDetails tblCmsInvoiceDetails : list) {
                                                service.updateWpDetailIfInvReject(tenderId, lotId, Integer.parseInt(InvoiceId), tblCmsInvoiceDetails.getWpDetailId(), tblCmsInvoiceDetails.getItemInvQty(), remarks);
                                            }
                                        }
                                        else{
                                             service.updateInvoiceMasterIfInvRejectICT(Integer.parseInt(InvoiceId), remarks);
                                        }
                                    }
                                  }
                                }
                            }
                        }
                        /* dohaetc end*/

                        else{
                        List<TblCmsInvoiceMaster> InvMasdata = c_ConsSrv.getTblCmsInvoiceMaster(Integer.parseInt(request.getParameter("InvoiceId")));
                        if (!InvMasdata.isEmpty()) {
                            if ("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv())) {
                                service.updateInvmaster(Integer.parseInt(InvoiceId), remarks);
                            } else {
                                List<TblCmsInvoiceDetails> list = service.getAllDetailByInvoiceId(Integer.parseInt(InvoiceId));
                                if("services".equalsIgnoreCase(procnature)){
                                    service.updateInvmaster(Integer.parseInt(InvoiceId), remarks);
//                                    if(!"Time based".equalsIgnoreCase(serviceType.toString()))
//                                    {
//                                        service.updateLumpSumPr(InvMasdata.get(0).getTillPrid(),Integer.parseInt(tenderId));
//                                    }
                                }
                                if (!list.isEmpty()) {
                                    for (TblCmsInvoiceDetails tblCmsInvoiceDetails : list) {
                                        service.updateWpDetailIfInvReject(tenderId, lotId, Integer.parseInt(InvoiceId), tblCmsInvoiceDetails.getWpDetailId(), tblCmsInvoiceDetails.getItemInvQty(), remarks);
                                    }
                                }
                            }
                          }
                        }
                        sendMailForInvoiceAcceptRejectByPE(request.getParameter("tenderId"), request.getParameter("lotId"), logUserId, "has Rejected", "e-GP: Procuring entity has Rejected Invoice",InvoiceId);
                        if("services".equalsIgnoreCase(procnature))
                            {    
                                response.sendRedirect("officer/InvoiceServiceCase.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=reject");
                            }else{        
                                response.sendRedirect("officer/Invoice.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=reject");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Process Invoice", "reject Invoice");
                    }
                }
            } else if ("saveRemarks".equalsIgnoreCase(action)) {
                try {
                    String InvoiceId = "";
                    if (request.getParameter("InvoiceId") != null) {
                        InvoiceId = request.getParameter("InvoiceId");
                    }
                    String wpId = "";
                    if (request.getParameter("wpId") != null) {
                        wpId = request.getParameter("wpId");
                    }
                    String strRemarks = "";
                    if (request.getParameter("txtRemarks") != null) {
                        strRemarks = request.getParameter("txtRemarks");
                    }

                    if(InvoiceId.contains("Invoice")){  // This condition is added by dohatec
                        List<Object> list = null;
                        list = service.getInvoiceIdForICT(InvoiceId, Integer.parseInt(request.getParameter("wpId")));
                        if(list.size()>0){
                            String invoiceIds[] = new String[4];
                            int i = 0;
                            boolean flag = false;
                            int count = 0;
                            
                            for (Object oob : list) {
                                
                                invoiceIds[i] = oob.toString();
                                TblCmsInvRemarks tblCmsInvRemarks = new TblCmsInvRemarks();
                                tblCmsInvRemarks.setTblCmsInvoiceMaster(new TblCmsInvoiceMaster(Integer.parseInt(invoiceIds[i])));
                                tblCmsInvRemarks.setRemarks(strRemarks);
                                tblCmsInvRemarks.setCreatedBy(Integer.parseInt(logUserId));
                                tblCmsInvRemarks.setCreatedDt(new java.sql.Date(new java.util.Date().getTime()));

                                flag = accPaymentService.saveRemarks(tblCmsInvRemarks);

                                if (flag) {
                                    flag = accPaymentService.updateStatusInInvMaster("remarksbype", Integer.parseInt(invoiceIds[0]));
                                    if(flag) {
                                        count++;
                                    } else {
                                        count = 0;
                                        break;
                                    }
                                }
                            }

                            if (count == list.size()) {
                                response.sendRedirect("officer/ViewInvoice.jsp?tenderId=" + tenderId + "&invoiceNo=" + InvoiceId + "&lotId=" + lotId + "&wpId=" + wpId + "&msg=Remarkssucc");
                            } else {
                                response.sendRedirect("officer/ViewInvoice.jsp?tenderId=" + tenderId + "&invoiceNo=" + InvoiceId + "&lotId=" + lotId + "&wpId=" + wpId + "&msg=Remarksfail");
                            }
                        }
                    } else {    // Old Version
                        TblCmsInvRemarks tblCmsInvRemarks = new TblCmsInvRemarks();
                        tblCmsInvRemarks.setTblCmsInvoiceMaster(new TblCmsInvoiceMaster(Integer.parseInt(InvoiceId)));
                        tblCmsInvRemarks.setRemarks(strRemarks);
                        tblCmsInvRemarks.setCreatedBy(Integer.parseInt(logUserId));
                        tblCmsInvRemarks.setCreatedDt(new java.sql.Date(new java.util.Date().getTime()));

                        boolean flag = false;
                        flag = accPaymentService.saveRemarks(tblCmsInvRemarks);
                        if (flag) {
                            flag = accPaymentService.updateStatusInInvMaster("remarksbype", Integer.parseInt(InvoiceId));
                            if (flag) {
                                response.sendRedirect("officer/ViewInvoice.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&lotId=" + lotId + "&wpId=" + wpId + "&msg=Remarkssucc");
                            } else {
                                response.sendRedirect("officer/ViewInvoice.jsp?tenderId=" + tenderId + "&invoiceId=" + InvoiceId + "&lotId=" + lotId + "&wpId=" + wpId + "&msg=Remarksfail");
                            }
                        }
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** send mail notification to PE on Sending calculated invoice to PE by Accountant */
    private boolean sendMailForSendToPE(String tenderId, String lotId, String logUserId, String Operation, String mailSubject,String InvoiceId) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        Object[] invmstobj = null;
        String InvoiceAmt = "";
        String MileStoneName = "";
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            String peEmailId = "";
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if (!peEmailObj.isEmpty()) {
                Object[] objj = peEmailObj.get(0);
                peEmailId = objj[0].toString();
            }
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            String strTo[] = {peEmailId};
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            List<Object[]> getInvAmtMstObj = c_ConsSrv.getInvAmtAndMilestoneName(Integer.parseInt(InvoiceId));
            List<Object[]> getInvAmt = c_ConsSrv.getInvoiceTotalAmt(InvoiceId);
            /*if(!getInvAmt.isEmpty())
            {
                InvoiceAmt = getInvAmt.get(0)[0].toString();
            }*/
            String pnature = commonservice.getProcNature(tenderId).toString();
            String serviceType = commonservice.getServiceTypeForTender(Integer.parseInt(tenderId));
            if("services".equalsIgnoreCase(pnature))
            {
                if(!"Time based".equalsIgnoreCase(serviceType.toString()))
                {
                    if(!getInvAmtMstObj.isEmpty())
                    {
                        invmstobj = getInvAmtMstObj.get(0);
                        InvoiceAmt = invmstobj[0].toString();
                        MileStoneName = invmstobj[1].toString();
                    }
                }
            }
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            registerService.contentAdmMsgBox(strTo[0], strFrom, mailSubject, msgBoxContentUtility.invoiceNotificationToPEfromAc(Operation, c_isPhysicalPrComplete, peName, obj,InvoiceAmt,MileStoneName));
            String mailText = mailContentUtility.invoiceNotificationToPEfromAc(Operation, c_isPhysicalPrComplete, peName, obj,InvoiceAmt,MileStoneName);
            sendMessageUtil.setEmailTo(strTo);
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            sendMessageUtil.setEmailMessage(mailText);
            sendMessageUtil.sendEmail();
            sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
            StringBuilder sb = new StringBuilder(); 
            sb.append("Dear User,%0AThis is to inform you that Accounts Officer has send Invoice ");
            sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
            sb.append("%0AThanks,%0AeGP System");
            sendMessageUtil.setSmsBody(sb.toString());
            sendMessageUtil.sendSMS();
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to tenderer on generating final invoice by Accountant */
    private boolean sendMailForSendToTenderer(String tenderId, String lotId, String InvoiceId, String logUserId, String Operation, String mailSubject) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        Object[] invmstobj = null;
        String InvoiceAmt = "";
        String MileStoneName = "";
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            String peEmailId = "";
            String PeOfficeName = "";
            List<TblCmsInvoiceAccDetails> IAccDetails = null;
            String strFrom = commonservice.getEmailId(logUserId);
            IAccDetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(InvoiceId));
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if (!peEmailObj.isEmpty()) {
                Object[] objj = peEmailObj.get(0);
                peEmailId = objj[0].toString();
                PeOfficeName = objj[1].toString();
            }
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (!peNameObj.isEmpty()) {
                peName = peNameObj.get(0).toString();
            }
            String strTo[] = {peEmailId, obj[9].toString()};
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            List<Object[]> getInvAmtMstObj = c_ConsSrv.getInvAmtAndMilestoneName(Integer.parseInt(InvoiceId));
            List<Object[]> getInvAmt = c_ConsSrv.getInvoiceTotalAmt(InvoiceId);
            if(!getInvAmt.isEmpty())
            {
                InvoiceAmt = getInvAmt.get(0)[0].toString();
            }
            String pnature = commonservice.getProcNature(tenderId).toString();
            String serviceType = commonservice.getServiceTypeForTender(Integer.parseInt(tenderId));
            if("services".equalsIgnoreCase(pnature))
            {
                if(!"Time based".equalsIgnoreCase(serviceType.toString()))
                {
                    if(!getInvAmtMstObj.isEmpty())
                    {
                        invmstobj = getInvAmtMstObj.get(0);
                        InvoiceAmt = invmstobj[0].toString();
                        MileStoneName = invmstobj[1].toString();
                    }
                }
            }
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String emailId[] = new String[1];
            String mobileno = "";
            for (int i = 0; i < strTo.length; i++) {
                emailId[0] = strTo[i];
                registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.invoiceNotificationToSupplierfromAc(Operation, c_isPhysicalPrComplete, peName, PeOfficeName, obj, IAccDetails,InvoiceAmt,MileStoneName));
                String mailText = mailContentUtility.invoiceNotificationToSupplierfromAc(Operation, c_isPhysicalPrComplete, peName, PeOfficeName, obj, IAccDetails,InvoiceAmt,MileStoneName);
                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if ("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                sendMessageUtil.setSmsNo(mobileno);
                StringBuilder sb = new StringBuilder(); 
                sb.append("Dear User,%0AThis is to inform you that Accounts Officer has send Invoice ");
                sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                sb.append("%0AThanks,%0AeGP System");
                sendMessageUtil.setSmsBody(sb.toString());
                sendMessageUtil.sendSMS();
            }
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }

    /** send mail notification to tenderer on accepting/rejecting invoice by PE */
    private boolean sendMailForInvoiceAcceptRejectByPE(String tenderId, String lotId, String logUserId, String Operation, String mailSubject,String InvoiceId) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        Object[] invmstobj = null;
        String InvoiceAmt = "";
        String MileStoneName = "";
        String InvoiceNo = "";
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if (!peEmailObj.isEmpty()) {
                Object[] objj = peEmailObj.get(0);
            }
            List<Object[]> AcEmailId = accPaymentService.getAccountantEmailId(Integer.parseInt(tenderId));
            String[] strTo = new String[AcEmailId.size() + 1];
            int emailIndex = 0;
            if ("has Accepted".equalsIgnoreCase(Operation)) {
                for (Object[] emailId : AcEmailId) {
                    strTo[emailIndex] = emailId[4].toString();
                    emailIndex++;
                }
            } else {
                strTo = new String[1];
            }
            strTo[emailIndex] = obj[9].toString();

            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (!peNameObj.isEmpty()) {
                peName = peNameObj.get(0).toString();
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            List<Object[]> getInvAmtMstObj = c_ConsSrv.getInvAmtAndMilestoneName(Integer.parseInt(InvoiceId));
            List<Object[]> getInvAmt = c_ConsSrv.getInvoiceTotalAmt(InvoiceId);
        //    if(!getInvAmt.isEmpty())

         //   if(!getInvAmt.equals(Collections.<Object[]>emptyList()))
         //   {
        //        InvoiceAmt =  getInvAmt.get(0)[0].toString();
        //    }
            List<Object> getInvNo = c_ConsSrv.getInvoiceNo(InvoiceId);
            if(!getInvNo.isEmpty())
            {
                InvoiceNo = getInvNo.get(0).toString();
            }
            String pnature = commonservice.getProcNature(tenderId).toString();
            String serviceType = commonservice.getServiceTypeForTender(Integer.parseInt(tenderId));
            if("services".equalsIgnoreCase(pnature))
            {
                if(!"Time based".equalsIgnoreCase(serviceType.toString()))
                {
                    if(!getInvAmtMstObj.isEmpty())
                    {
                        invmstobj = getInvAmtMstObj.get(0);
                        InvoiceAmt = invmstobj[0].toString();
                        MileStoneName = invmstobj[1].toString();
                        InvoiceNo = invmstobj[2].toString();
                    }
                }
            }
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailFrom(strFrom);
            sendMessageUtil.setEmailSub(mailSubject);

            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String emailId[] = new String[1];
            String mobileno = "";
            for (int i = 0; i < strTo.length; i++) {
                emailId[0] = strTo[i];
                registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.invoiceNotificationToSuppliandAcfromSupplier(Operation, c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]),InvoiceAmt,MileStoneName,InvoiceNo));
                String mailText = mailContentUtility.invoiceNotificationToSuppliandAcfromSupplier(Operation, c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]),InvoiceAmt,MileStoneName,InvoiceNo);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.sendEmail();
                if ("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                sendMessageUtil.setSmsNo(mobileno);
                StringBuilder sb = new StringBuilder();
                if ("has Accepted".equalsIgnoreCase(Operation)) {
                    sb.append("Dear User,%0AThis is to inform you that PE has Accepted Invoice ");
                    sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+", ");
                    sb.append("%0AThanks,%0AeGP System");
                } else {
                    sb.append("Dear User,%0AThis is to inform you that PE has Rejected Invoice ");
                    sb.append("for Contract No."+obj[1].toString()+" ("+ obj[15].toString() +")"+" ");
                    sb.append("%0AThanks,%0AeGP System");
                }
                sendMessageUtil.setSmsBody(sb.toString());
                sendMessageUtil.sendSMS();
            }
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }
}
