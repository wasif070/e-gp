/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.service.serviceimpl.ContentAdminService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import javax.servlet.http.HttpSession;
/**
 *
 * @author TaherT
 */
public class CompanyAdminServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        HttpSession session=request.getSession();
        
        // Coad added by Dipal for Audit Trail Log.
        AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer"));
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        String idType="userId";
        int auditId=Integer.parseInt(session.getAttribute("userId").toString());
        String auditAction=null;
        String moduleName=EgpModule.My_Account.getName();
        String remarks=null;
        

        String action = request.getParameter("action");
        if ("getData".equals(action)) {
            PrintWriter out = response.getWriter();
            response.setContentType("text/xml;charset=UTF-8");
            try {
                String status = request.getParameter("status");
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String cId = request.getParameter("cId");
                String sidx = request.getParameter("sidx");
                String sord = request.getParameter("sord");
                if(sidx == null || sidx.trim().equals("")){
                    sidx = "tlm.emailId";
                }
                out.print(getData(cId, status, Integer.parseInt(page), Integer.parseInt(rows),sidx,sord));
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                out.close();
            }
        }
        else if("assignRole".equals(action))
        {
            remarks="Bidder[User id]: "+auditId+" has assigned Role to Bidder Id:"+request.getParameter("tId")+" of Company Id:"+request.getParameter("cId");
            if(assignRole(request.getParameter("cId"),request.getParameter("tId")))
            {
                auditAction="Assign Company User";
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                response.sendRedirect(request.getContextPath()+"/Logout.jsp");
            }
            else
            {
                auditAction="Error in Assign Company User";
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                response.sendRedirect(request.getHeader("referer")+"&msg=fail");
            }
        }
    }

    public String getData(String cId, String status, int page, int rows,String strSortCol,String strSortOrd) throws Exception {
        ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
        StringBuilder sb = new StringBuilder();
        int offset = ((page - 1) * rows);
        int totalPages = 0;
        int totalCount = (int) contentAdminService.getCountCompanyAdminData(cId, status);
        List<Object[]> data = contentAdminService.getCompanyAdminData(offset, rows, cId, status, strSortCol, strSortOrd);
        if (totalCount > 0) {
            if ((totalCount % rows) == 0) {
                totalPages = (totalCount / rows);
            } else {
                totalPages = (totalCount / rows) + 1;
            }

        } else {
            totalPages = 0;
        }
        sb.append("<?xml version='1.0' encoding='utf-8'?>\n");
        sb.append("<rows>");
        sb.append("<page>" + page + "</page>");
        sb.append("<total>" + totalPages + "</total>");
        sb.append("<records>" + totalCount + "</records>");
        int srNo = 1;
        int rowId = 0;
        for (Object[] obj : data) {
            sb.append("<row id='" + rowId + "'>");
            sb.append("<cell><![CDATA[" + srNo + "]]></cell>");
            sb.append("<cell><![CDATA[" + obj[2] + "]]></cell>");
            sb.append("<cell><![CDATA[" + obj[3] + " "+obj[4]+"]]></cell>");
            sb.append("<cell><![CDATA[" + obj[5] + "]]></cell>");
            sb.append("<cell><![CDATA[" + obj[6] + "]]></cell>");
            sb.append("<cell><![CDATA[" + obj[7] + "]]></cell>");
            String stat = null;
            if(obj[8].toString().equals("approved")){
                stat = "Approved";
            }else{
                stat = "Suspended";
            }
            sb.append("<cell><![CDATA[" + stat + "]]></cell>");
            sb.append("<cell><![CDATA["
                    + "<a href='CompanyUser.jsp?action=edit&uId="+obj[0]+"&cId="+cId+"&tId="+obj[1]+"' title='Edit'>Edit</a>"
                    + "&nbsp;|&nbsp;"
                    + "<a href='CompanyUser.jsp?action=view&uId="+obj[0]+"&cId="+cId+"&tId="+obj[1]+"' title='View'>View</a>");
           if(stat.equalsIgnoreCase("approved")){
               sb.append("&nbsp;|&nbsp;<a href='CompanyUser.jsp?stat=s&action=view&uId="+obj[0]+"&cId="+cId+"&tId="+obj[1]+"' title='Suspend'>Suspend</a>");
//               sb.append("&nbsp;|&nbsp;<a href='CompanyUser.jsp?action=arole&uId="+obj[0]+"&cId="+cId+"&tId="+obj[1]+"' title='Assign Role'>Assign Role</a>");
           }else{
                sb.append("&nbsp;|&nbsp;<a href='CompanyUser.jsp?stat=a&action=view&uId="+obj[0]+"&cId="+cId+"&tId="+obj[1]+"' title='Resume'>Resume</a>");
           }
            sb.append("]]></cell>");
            sb.append("</row>");
            srNo++;
            rowId++;
        }
//        if (data.isEmpty()) {
//            sb.append("<row id='" + rowId + "'>");
//            sb.append("<cell><![CDATA[No data found]]></cell>");
//            sb.append("<cell><![CDATA[No data found]]></cell>");
//            sb.append("<cell><![CDATA[No data found]]></cell>");
//            sb.append("<cell><![CDATA[No data found]]></cell>");
//            sb.append("<cell><![CDATA[No data found]]></cell>");
//            sb.append("<cell><![CDATA[No data found]]></cell>");
//            sb.append("<cell><![CDATA[No data found]]></cell>");
//            sb.append("<cell><![CDATA[No data found]]></cell>");
//            sb.append("</row>");
//        }
        sb.append("</rows>");
        return sb.toString();
    }

    public boolean assignRole(String cId,String tId){
        UserRegisterService registerService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
        return registerService.assignCompanyAdminRole(cId, tId);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
