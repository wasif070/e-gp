/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblWsOrgMaster;
import com.cptu.egp.eps.service.serviceinterface.WsOrgMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * This class is used to add a web-service consumer  and update a web-service Consumer.
 * @author Sreenu
 */
public class WebServiceConsumerControlServlet extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(WebServiceConsumerControlServlet.class);
    String logUserId = "0";
    static final String STARTS = " Starts";
    static final String ENDS = " Ends";
    static final String EMPTY_STRING = "";
    private WsOrgMasterService wsOrgMasterService = null;
    static final String ADD = "add";
    static final String EDIT = "edit";
    static final String STATUS_CHANGE = "statusChange";
    static final String ACTIVE = "active";
    static final String DEACTIVATE = "deactivate";
    static final String CHECK_MAIL = "checkMailId";
    private String message = EMPTY_STRING;

    /***
     * This web method process the request from adding a web-service consumer or
     * editing the details of a web-service consumer or change status of a web-service consumer
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest " + logUserId + STARTS);
        wsOrgMasterService = (WsOrgMasterService) AppContext.getSpringBean("WsOrgMasterService");
        String action = request.getParameter("action");
        if (STATUS_CHANGE.equalsIgnoreCase(action)) {
            changeStatus(request, response);
        } else if (CHECK_MAIL.equalsIgnoreCase(action)) {
            checkUniqueMail(request, response);
        } else {
            editOrCreationWs(request, response);
        }
        LOGGER.debug("processRequest " + logUserId + ENDS);
    }

    /***
     * This method changes the status of web-service consumer
     * @param request
     * @param response
     * @throws IOException
     */
    private void changeStatus(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOGGER.debug("changeStatus " + logUserId + STARTS);
        String status = request.getParameter("status");
        int webServiceOrgId = Integer.parseInt(request.getParameter("wsOrgId"));
        TblWsOrgMaster tblWsOrgMaster = null;
        message = STATUS_CHANGE;
        if (webServiceOrgId != 0) {
            tblWsOrgMaster = wsOrgMasterService.getTblWsOrgMaster(webServiceOrgId);
        }
        if (tblWsOrgMaster != null) {
            if (ACTIVE.equalsIgnoreCase(status)) {
                tblWsOrgMaster.setIsDeleted("N");
            } else if (DEACTIVATE.equalsIgnoreCase(status)) {
                tblWsOrgMaster.setIsDeleted("Y");
            }
            wsOrgMasterService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
            wsOrgMasterService.updateWsOrgMaster(tblWsOrgMaster,ACTIVE.equalsIgnoreCase(status)?"Activate":"Deactivate");
            response.sendRedirect("/admin/WSConsumersList.jsp?flag=true&message=" + message);
        }//null checking
        LOGGER.debug("changeStatus " + logUserId + ENDS);
    }//

    /***
     * This method calls when a request changes the web-service consumer details or
     * adding a web-service consumer
     * @param request
     * @param response
     * @throws IOException
     */
    private void editOrCreationWs(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOGGER.debug("editOrCreationWs " + logUserId + STARTS);
        String action = request.getParameter("action");
        TblWsOrgMaster tblWsOrgMaster = null;
        HttpSession session = request.getSession();
        String mailId = request.getParameter("mailId");
        String password = request.getParameter("password");
        String orgNameEng = request.getParameter("fullName");
        String orgNameBng = request.getParameter("bngName");
        String orgDetails = request.getParameter("txtAreaOrgAdd");
        String contactPerson = request.getParameter("contactPerson");
        String mobileNumber = request.getParameter("mobileNo");
        String isDeleted = request.getParameter("status");
        Date currentDate = getCurrentDate();
        String wsRights[] = request.getParameterValues("cmbWebServiceList");
        StringBuffer wsRightsBuffer = new StringBuffer();
        for (int i = 0; i < wsRights.length; i++) {
            wsRightsBuffer.append(wsRights[i]);
            wsRightsBuffer.append("^");
        }
        int userId = 0;
        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
            userId = Integer.parseInt(session.getAttribute("userId").toString());
        }
        if (ADD.equalsIgnoreCase(action)) {
            tblWsOrgMaster = new TblWsOrgMaster();
            tblWsOrgMaster.setEmailId(mailId);
            tblWsOrgMaster.setLoginPass(SHA1HashEncryption.encodeStringSHA1(password));

        } else {
            tblWsOrgMaster = wsOrgMasterService.getTblWsOrgMaster(mailId);
        }
        tblWsOrgMaster.setIsDeleted(isDeleted);
        tblWsOrgMaster.setContactPerson(contactPerson);
        tblWsOrgMaster.setCreatedBy(userId);
        tblWsOrgMaster.setCreatedDate(currentDate);
        tblWsOrgMaster.setMobileNo(mobileNumber);
        tblWsOrgMaster.setOrgDetail(orgDetails);
        tblWsOrgMaster.setOrgNameBg(orgNameBng);
        tblWsOrgMaster.setOrgNameEn(orgNameEng);
        tblWsOrgMaster.setWsRights(wsRightsBuffer.toString());
        if (ADD.equalsIgnoreCase(action)) {
            message = action;
            wsOrgMasterService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
            wsOrgMasterService.insertWsOrgMaster(tblWsOrgMaster);
        } else if (EDIT.equalsIgnoreCase(action)) {
            message = action;
            wsOrgMasterService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
            wsOrgMasterService.updateWsOrgMaster(tblWsOrgMaster,"Edit");
        }
        response.sendRedirect("/admin/WSConsumersList.jsp?flag=true&message=" + message);
        LOGGER.debug("editOrCreationWs " + logUserId + ENDS);
    }

    /***
     * This method process the request which check mail-id is unique or not
     * @param request
     * @param response
     * @throws IOException
     */
    private void checkUniqueMail(HttpServletRequest request, HttpServletResponse response) throws IOException {
        LOGGER.debug("checkUniqueMail " + logUserId + STARTS);
        String mailId = request.getParameter("mailId");
        List<TblWsOrgMaster> wsOrgList = wsOrgMasterService.getAllWsOrgMaster();
        boolean isUnique = true;
        String message = "";
        PrintWriter out = response.getWriter();
        if (!mailId.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9\\-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            message = "Please enter valid e-mail ID";
        } else {
            for (TblWsOrgMaster wsOrgMaster : wsOrgList) {
                if (wsOrgMaster.getEmailId().equalsIgnoreCase(mailId)) {
                    isUnique = false;
                    break;
                }
            }
            if (isUnique) {
                message = "OK";
            } else {
                message = "e-mail ID already exist, Please enter another e-mail ID";
            }
        }
        out.print(message);
        out.flush();
        LOGGER.debug("checkUniqueMail " + logUserId + ENDS);
    }

    /***
     * This method returns the current date
     * @return Date
     */
    private Date getCurrentDate() {
        LOGGER.debug("getCurrentDate : " + STARTS);
        Calendar currentDate = Calendar.getInstance();
        Date dateNow = (currentDate.getTime());
        LOGGER.debug("getCurrentDate : " + ENDS);
        return dateNow;
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
