/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.service.serviceinterface.DesignationMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * <b>ManageDesigSrBean</b> <Description Goes Here> Nov 27, 2010 3:28:54 PM
 * @author Administrator
 */
public class ManageDesigSrBean
{

    final Logger logger = Logger.getLogger(ManageDesigSrBean.class);

    DesignationMasterService designationMasterService = (DesignationMasterService) AppContext.getSpringBean("DesignationMasterService");
    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String getSortOrder = "";
    private String sortCol = "";
    private String logUserId="0";

     public void setLogUserId(String logUserId) {
        designationMasterService.setUserId(logUserId);
        this.logUserId = logUserId;
     }

    public String getSortOrder()
    {
        return getSortOrder;
    }

    public void setSortOrder(String getSortOrder)
    {
        this.getSortOrder = getSortOrder;
    }

    public String getSortCol()
    {
        return sortCol;
    }

    public void setSortCol(String sortCol)
    {
        this.sortCol = sortCol;
    }

    public String getColName()
    {
        return colName;
    }

    public void setColName(String colName)
    {
        this.colName = colName;
    }

    public boolean isSearch()
    {
        return _search;
    }

    public void setSearch(boolean _search)
    {
        this._search = _search;
    }

    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }

    public int getLimit()
    {
        return limit;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public int getOffset()
    {
        return offset;
    }

    public void setOffset(int offset)
    {
        this.offset = offset;
    }

    public String getOp_ENUM()
    {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM)
    {
        this.op_ENUM = op_ENUM;
    }

    public long getAllCountOfDesignation(int userId) 
    {
        logger.debug("getAllCountOfDesignation : "+logUserId+" Starts ");
        long lng = 0;
        try {
            lng = designationMasterService.getDesigCnt(userId);
        } catch (Exception e) {
            logger.error("getAllCountOfDesignation : "+logUserId+" : "+e);
    }
        logger.debug("getAllCountOfDesignation : "+logUserId+" Ends ");
        return lng;
    }
    List<TblDesignationMaster> desigList = new ArrayList<TblDesignationMaster>();

    /*public List<TblDesignationMaster> getDesignationList(int userId,String userTypeId)//malhar's code
    {
        if (desigList.isEmpty()) {
            if("5".equalsIgnoreCase(userTypeId)){
                desigList = designationMasterService.getAllDesignations(getOffset(), getLimit(), userId);
            }else{
                String orderClause = null;
                String order = null;
                if(getSortCol().equalsIgnoreCase("")){
                    orderClause = "createdDate";
                    order = "desc";
                }else if(getSortCol().equalsIgnoreCase("designation")){
                    orderClause = "designationName";
                    order = getSortOrder;
                }//else if(getSortCol().equalsIgnoreCase("department")){
//                    orderClause = "tblDepartmentMaster.departmentName";
//                    order = getSortOrder;
//                }else if(getSortCol().equalsIgnoreCase("grade")){
//
//                    order = getSortOrder;
//                }
                desigList = designationMasterService.getAllDesignationsForPE(getOffset(), getLimit(), userId,orderClause,order);
            }

        }        
        return desigList;
    }*/
    public List<Object[]> getDesignationList(int userId,String userTypeId)
    {   
        logger.debug("getDesignationList : "+logUserId+" Starts ");
        List<Object[]> list = null;
        try{
        if (desigList.isEmpty()) {
            //if("5".equalsIgnoreCase(userTypeId)){
            //    desigList = designationMasterService.getAllDesignations(getOffset(), getLimit(), userId);
           // }else{
                String orderClause = null;
                String order = null;
                if(getSortCol().equalsIgnoreCase("")){
                    orderClause = "tdm.createdDate";
                    order = "desc";
                }else if(getSortCol().equalsIgnoreCase("designation")){
                    orderClause = "tdm.designationName";
                    order = getSortOrder;
                }else if(getSortCol().equalsIgnoreCase("department")){
                    orderClause = "tm.departmentName";
                    order = getSortOrder;
                }else if(getSortCol().equalsIgnoreCase("grade")){
                    orderClause = "tgm.grade";
                    order = getSortOrder;
                }
                list = designationMasterService.getAllDesignationsForPE(getOffset(), getLimit(), userId,orderClause,order,Integer.parseInt(userTypeId));
           // }

        }
        }catch(Exception e)
        {
            logger.error("getDesignationList : "+logUserId+" : "+e);
        }
         logger.debug("getDesignationList : "+logUserId+" Ends ");
        return list;
    }
    
    public void setGradeList(List<TblDesignationMaster> desigList)
    {
        this.desigList = desigList;
    }

    public String getGetSortOrder()
    {
        return getSortOrder;
    }

    public void setGetSortOrder(String getSortOrder)
    {
        this.getSortOrder = getSortOrder;
    }
}
