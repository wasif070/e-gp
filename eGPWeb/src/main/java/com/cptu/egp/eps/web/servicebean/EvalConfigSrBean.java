/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;


import java.util.Date;

public class EvalConfigSrBean {

 
    private int evalConfigId=0;
    private int tenderId=0;
    private String configType="";
    private String evalCommittee="";
    private int configBy=0;
    private Date configDate=null;
    private String isPostQualReq="";

  // public EvalConfigser
    public EvalConfigSrBean() {
    }

    public int getConfigBy() {
        return configBy;
    }

    public void setConfigBy(int configBy) {
        this.configBy = configBy;
    }

    public Date getConfigDate() {
        return configDate;
    }

    public void setConfigDate(Date configDate) {
        this.configDate = configDate;
    }

    public String getConfigType() {
        return configType;
    }

    public void setConfigType(String configType) {
        this.configType = configType;
    }

    public String getEvalCommittee() {
        return evalCommittee;
    }

    public void setEvalCommittee(String evalCommittee) {
        this.evalCommittee = evalCommittee;
    }

    public int getEvalConfigId() {
        return evalConfigId;
    }

    public void setEvalConfigId(int evalConfigId) {
        this.evalConfigId = evalConfigId;
    }

    public int getTenderId() {
        return tenderId;
    }

    public void setTenderId(int tenderId) {
        this.tenderId = tenderId;
    }

    public String getIsPostQualReq() {
        return isPostQualReq;
    }

    public void setIsPostQualReq(String isPostQualReq) {
        this.isPostQualReq = isPostQualReq;
    }
    
}
