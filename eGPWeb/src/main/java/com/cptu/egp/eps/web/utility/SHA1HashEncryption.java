/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import java.security.MessageDigest;
import org.apache.log4j.Logger;

/**
 *
 * @author TaherT
 */
public class SHA1HashEncryption {
    private static final Logger LOGGER = Logger.getLogger(SHA1HashEncryption.class);
    public static String encodeStringSHA1(String text) {
        try {
            MessageDigest sh;
            sh = MessageDigest.getInstance("SHA1");
            byte[] sha1hash = new byte[32];
            sh.update(text.getBytes("iso-8859-1"), 0, text.length());
            sha1hash = sh.digest();
            return convertToHex(sha1hash);
        } catch (Exception e) {
            LOGGER.error("SHA1 Encoding" + e.getMessage());
        }
        return null;
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }
}
