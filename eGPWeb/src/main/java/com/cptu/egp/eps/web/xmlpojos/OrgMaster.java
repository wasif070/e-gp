/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Sudhir
 */
public class OrgMaster {
    private int Org_Id;
    private String OrgName_En;
    private String WS_Rights;
    private String Access;
    /**
     * @return the org_id
     */
    @XmlElement(nillable = true)
    public int getOrg_Id() {
        return this.Org_Id;
    }

    /**
     * @param officeName the officeName to set
     */
    public void setOrg_Id(int Org_Id) {
        this.Org_Id = Org_Id;
    }

        /**
     * @return the OrgName_En
     */
    @XmlElement(nillable = true)
    public String getOrgName_En() {
        return this.OrgName_En;
    }

    /**
     * @param officeName the officeName to set
     */
    public void setOrgName_En(String OrgName_En) {
        this.OrgName_En = OrgName_En;
    }

        /**
     * @return the officeName
     */
    @XmlElement(nillable = true)
    public String getWS_Rights() {
        return this.WS_Rights;
    }

    /**
     * @param officeName the officeName to set
     */
    public void setWS_Rights(String OWS_Rights) {
        this.WS_Rights = WS_Rights;
    }
    /**
     * @return the Access
     */
     @XmlElement(nillable = true)
    public String getAccess() {
        return Access;
    }

    /**
     * @param officeId the officeId to set
     */
    public void setAccess(String Access) {
        this.Access = Access;
    }
}
