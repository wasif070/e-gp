/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblCurrencyMaster;
import com.cptu.egp.eps.model.table.TblTenderCurrency;

import com.cptu.egp.eps.service.serviceinterface.CurrencyMasterService;
import com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.SelectItem;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import javax.script.*;

/**
 *
 * @author MD. khaled Ben Islam from Dohatec
 */
public class ConfigureCurrencySrBean {

    private final CurrencyMasterService currencyMasterService = (CurrencyMasterService) AppContext.getSpringBean("CurrencyMasterService");
    private final TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");

    private static final Logger LOGGER = Logger.getLogger(ConfigureCurrencySrBean.class);
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String logUserId = "0";

    private List<SelectItem> currencyList = new ArrayList<SelectItem>();

    /**
     * Get List of all Currencies
     * @return List of Currencies.
     */
    public List<SelectItem> getCurrencyList() {
        LOGGER.debug("getCurrencyList : " + logUserId + loggerStart);
        try {
        //List<TblCurrencyMaster> currencies = currencyMasterService.getAllCurrency();
        List<TblCurrencyMaster> currencies = currencyMasterService.getAllCurrencySortByShortName();
        if (currencies != null && !currencies.isEmpty()) {
            for (TblCurrencyMaster currency : currencies) {
                if(!("BTN".equalsIgnoreCase(currency.getCurrencyShortName()))){ //BTNBTN is the default currency and wil be added when add currency page first load, not from combo box. So discard from ComboBox
                    currencyList.add(new SelectItem(currency.getCurrencyId(), currency.getCurrencyShortName() + " - " + currency.getCurrencyName()));
                }
            }
        }
        } catch (Exception e) {
            LOGGER.error("getCurrencyList : " + logUserId,e);
        }
        LOGGER.debug("getCurrencyList : " + logUserId + loggerEnd);
        return currencyList;
    }

    /**
     * Get Currency Short Name of Particular Currency
     * @param iCurrencyId
     * @return Currency Short Name
     */
    public String getCurrencyShortName(int iCurrencyId){
        LOGGER.debug("getCurrencyShortName : " + logUserId + loggerStart);
        String strCurrencyShortName = "";
        List<TblCurrencyMaster> listCurrencyDetails = null;

       try {
            listCurrencyDetails = currencyMasterService.getCurrencyDetails(iCurrencyId);
            for (TblCurrencyMaster currencyDetails : listCurrencyDetails) {
                strCurrencyShortName = currencyDetails.getCurrencyShortName();
            }
        } catch (Exception ex) {
            LOGGER.error("getCurrencyShortName "+logUserId+" :"+ex);
        }
        return strCurrencyShortName;
    }

    /**
     * Get Currency ID of Particular Currency (Short name)
     * @param strCurrencyShortName
     * @return Currency Id
     */
    public int getCurrencyId(String strCurrencyShortName){
        LOGGER.debug("getCurrencyId using CurrencyShortName : " + logUserId + loggerStart);
        int iCurrencyID = 0;
        List<TblCurrencyMaster> listCurrencyDetails = null;

        try {
            listCurrencyDetails = currencyMasterService.getCurrencyDetails(strCurrencyShortName);
            for (TblCurrencyMaster currencyDetails : listCurrencyDetails) {
                iCurrencyID = currencyDetails.getCurrencyId();
            }
        } catch (Exception ex) {
            LOGGER.error("getCurrencyId using CurrencyShortName "+logUserId+" :"+ex);
        }
        return iCurrencyID;
    }

    /**
     * Update Table tbl_TenderCurrency with Currency Exchange Rate, UserId and date of update
     * @param tenderId
     * @param currencyId
     * @param exchangeRate
     * @return if success then true, otherwise false
     */
    public boolean updateTenderCurrencyExchangeRate(int tenderId, int currencyId, BigDecimal exchangeRate, int iUserid){
        LOGGER.debug("updateCurrencyExchangeRate "+logUserId+" Starts");
        boolean bSuccess = false;

        try {
             TblTenderCurrency tblTenderCurrency=new TblTenderCurrency();
             tblTenderCurrency.setTenderId(tenderId);
             tblTenderCurrency.setCurrencyId(currencyId);
             tblTenderCurrency.setExchangeRate(exchangeRate);
             tblTenderCurrency.setCreatedBy(iUserid);
             tblTenderCurrency.setCreatedDate(new Date());

             bSuccess = tenderCurrencyService.updateCurrencyExchangeRate(tblTenderCurrency);
        } catch (Exception e) {
            LOGGER.error("updateCurrencyExchangeRate "+logUserId+" :"+e);
        }
        LOGGER.debug("updateCurrencyExchangeRate "+logUserId+" Ends");

        return bSuccess;
    }

    public List<Object[]> getTenderFormId(int tenderId)
    {
     LOGGER.debug("getTenderFormId "+logUserId+" Starts");

        List<Object[]> listTenderForm = new ArrayList<Object[]>();

        try {

            listTenderForm = tenderCurrencyService.getTenderFormId(tenderId);
           // System.out.println("listTenderForm >>>>>>>>>>" + listTenderForm);
            //for(int i = 0;i<listTenderForm.size();i++)
             //   updateColField(listTenderForm.get(0)[0].toString(),tenderId);
        } catch (Exception e) {
            LOGGER.error("getTenderFormId "+logUserId+" :"+e);
        }
        LOGGER.debug("getTenderFormId "+logUserId+" Ends");

        return listTenderForm;
    }


    
    public String updateColField(String listTenderFormId,int tenderId)
    {
    List<Object[]> listTenderBiddingId = new ArrayList<Object[]>();
   // System.out.print(listTenderFormId);
    List<Object[]> listFormula = new ArrayList<Object[]>();
    List<Object[]> listRowId = new ArrayList<Object[]>();
    Object[] formula=null;
    listFormula = tenderCurrencyService.getFormula(Integer.parseInt(listTenderFormId));
    listTenderBiddingId = tenderCurrencyService.getTenderBiddingInfo(Integer.parseInt(listTenderFormId));
     //System.out.println(formula.toString());

    StringBuilder strCellvalue = new StringBuilder("");
    StringBuilder strColumnId = new StringBuilder("");
    StringBuilder strBidTableId = new StringBuilder("");
    StringBuilder strRowId = new StringBuilder("");


    for(int tenderLoop=0;tenderLoop<listTenderBiddingId.size();tenderLoop++)
    {
    List<Object[]> listUpdateValue = new ArrayList<Object[]>();
    Object[] updateValue = new Object[3];
    Object[] bidTenderBiddingId = null;
    String replaceValue[] = new String[10];
    bidTenderBiddingId = listTenderBiddingId.get(tenderLoop);
    listRowId = tenderCurrencyService.getRowId(Integer.parseInt(listTenderFormId),  Integer.parseInt(bidTenderBiddingId[0].toString()), Integer.parseInt( bidTenderBiddingId[1].toString()));

    String cellValueUsingFormula[] = new String[listRowId.size()];
    String cellValueWithFormula[][] = new String[listRowId.size()][listFormula.size()];


    for(int i=0;i<listFormula.size();i++)
        {

        formula = listFormula.get(i);
     //   System.out.println(formula[1].toString());
        String formulaSt = formula[1].toString();
        String[] splitFormula = formula[1].toString().split("[\\*+-/()]+");
        String formulaWithComma ="";
        for(int j=0;j<splitFormula.length;j++){
            formulaWithComma = formulaWithComma + splitFormula[j] + ",";
          //  System.out.print(formulaWithComma);
            }
        formulaWithComma  = formulaWithComma.substring(0, formulaWithComma.length() -1);
      //  System.out.print("formula >>>>"+formulaWithComma);

          for(int rowLoop = 0;rowLoop<listRowId.size()-1;rowLoop++)
          {
            formulaSt = formula[1].toString();
         //   System.out.println("splitFormula >>>>>>>>>>" + formulaSt);
          //  System.out.println("rowLoop >>>>>>>>>>" + listRowId);
      //     System.out.println("rowLoop >>>>>>>>>>" + listRowId);
          //   System.out.println("rowLoop >>>>>>>>>>" + listRowId.get(rowLoop));


             String rowId = String.valueOf(listRowId.get(rowLoop));
          //  System.out.print("rowId >>>>"+rowId);

            String cellValue = tenderCurrencyService.getTenderCellValue(formulaWithComma, Integer.parseInt(listTenderFormId),  Integer.parseInt(bidTenderBiddingId[0].toString()),Integer.parseInt( bidTenderBiddingId[1].toString()), tenderId, Integer.parseInt(rowId));
          //  System.out.println("cellValue>>>>"+cellValue);
            String[] cellValueSplit = cellValue.split(",");
    ///
            for(int k = 0;k<cellValueSplit.length;k++)
            {
                boolean flag = false;
               // if(cellValueUsingFormula.length>0)
                {
                    for(int listS = 0;listS<i;listS++)
                    {
                     //   System.out.println("listFormulaInDb>>>>"+listFormula.get(listS)[0].toString());
                     //   System.out.println("listFormulaSplit>>>>"+splitFormula[k]);
                     //   System.out.println("cellValueUsingFormula[listS]>>>>"+cellValueUsingFormula[rowLoop]);
                     //   System.out.println("formulaSt[listS]>>>>"+formulaSt);
                     //   System.out.println("lists and k >>>>"+listS+"sss"+k);

                    if(listFormula.get(listS)[0].toString().equals(splitFormula[k]) && flag == false)
                        {


                         int index =  formulaSt.lastIndexOf(splitFormula[k]);
                      //   System.out.println("output before"+ formulaSt);
                         String fullForm1 = formulaSt.substring(0,index)+cellValueWithFormula[rowLoop][k];
                      //   System.out.println("output after"+ fullForm1);
                         formulaSt = fullForm1 + formulaSt.substring(index+splitFormula[k].length(),formulaSt.length());
                     //    System.out.println("fullForm2 "+ formulaSt);


                      /*  char[] charArray = formulaSt.toCharArray();
                        String[] current = new String[formulaSt.length()];
                        int incr = 0;
                        for (char c : charArray)
                        {
                         if(Character.isDigit(c))
                         {
                            if(current[incr]!=null)
                                current[incr] =  current[incr]+  String.valueOf(c);
                            else
                                current[incr] =  String.valueOf(c);
                          //   System.out.println(current[k]);
                         }
                        else
                         {
                             incr++;
                             current[incr] = String.valueOf(c);
                            // System.out.println(current[k]);
                             incr++;
                         }
                        }
                           // for(int j = 0;j<replace.length;j++)
                          //  {
                                for(int x =0 ;x<incr;x++)
                                {
                                     System.out.println( current[x]);
                                    System.out.println( splitFormula[k]);
                                    if(splitFormula[k].equals(current[x]))
                                    {
                                         current[x] = cellValueWithFormula[rowLoop][k];
                                        //current[x] = cellValueUsingFormula[rowLoop];
                                        System.out.println( current[x]);

                                    }
                                }

                         //   }
                         formulaSt = current[0];
                          System.out.println("formulaSt[]>>>>"+formulaSt);
                        for(int x=1;x<=incr;x++)
                        {
                        formulaSt = formulaSt + current[x];
                        System.out.println("formulaSt[list]>>>>"+formulaSt);
                        }
                      //  String[] rep = formulaSt.split("[\\*+-/()]+");
                       // rep[k].replace(splitFormula[k], cellValueUsingFormula[listS]);
                       // formulaSt = formulaSt.replace(splitFormula[k], cellValueUsingFormula[listS]);
                       System.out.println("formulaSt[]>>>>"+formulaSt);*/

                         flag = true;
                        }
                    }
                }
                if(flag == false)
                {
             //    System.out.println("splitFormula[k]>>>>"+splitFormula[k]);
             //    System.out.println("cellValueSplit[k]>>>>"+cellValueSplit[k]);
                 int index =  formulaSt.lastIndexOf(splitFormula[k]);
            //     System.out.println("output before"+ formulaSt);
                 String fullForm1 = formulaSt.substring(0,index)+cellValueSplit[k];
           //      System.out.println("output after"+ fullForm1);
                 formulaSt = fullForm1 + formulaSt.substring(index+splitFormula[k].length(),formulaSt.length());
           //      System.out.println("fullForm2 "+ formulaSt);



                }
            }
     /////
                  //  System.out.println((formulaSt));
                    ScriptEngineManager mgr = new ScriptEngineManager();
                    ScriptEngine engine = mgr.getEngineByName("JavaScript");

                try {
                              //  System.out.println(engine.eval(formulaSt));
                                Double asd = Double.valueOf(engine.eval(formulaSt).toString()) ;
                                cellValueUsingFormula[rowLoop] = String.format("%.3f",asd);
                                cellValueWithFormula[rowLoop][i] = String.format("%.3f",asd);
                             //   System.out.println(" cellValueWithFormula[rowLoop][i]"+cellValueWithFormula[rowLoop][i]+"row index"+rowLoop+ "formula index"+i);
                                strCellvalue.append(String.format("%.3f",asd));

                                strCellvalue.append(",");
                                strColumnId.append(formula[0].toString());
                                strColumnId.append(",");
                                strBidTableId.append(bidTenderBiddingId[0].toString()) ;
                                strBidTableId.append(",");
                                strRowId.append(rowId) ;
                                strRowId.append(",");
                             //   System.out.println("CellValueList>>>>>>>>"+strCellvalue);
                             //   System.out.println("ColumnIdList>>>>>>>>>"+strColumnId);
                            //    System.out.println("BidTableIdList>>>>>>"+strBidTableId);
                            //    System.out.println("RowId>>>>>>"+strRowId);

                              //  updateValue[1] = formula[0]; // columnId
                              //  updateValue[2] = cellValueUsingFormula; //cellValue
                              //  listUpdateValue.add(updateValue);

                } catch (ScriptException e) {
                    // TODO Auto-generated catch block
                }
            }//end rowLoop
        }//end inner loop

      }//end outer loop
        strCellvalue = strCellvalue.delete(strCellvalue.length()-1, strCellvalue.length());
        strColumnId = strColumnId.delete(strColumnId.length()-1, strColumnId.length());
        strBidTableId = strBidTableId.delete(strBidTableId.length()-1, strBidTableId.length());
        strRowId = strRowId.delete(strRowId.length()-1, strRowId.length());
       // System.out.println("CellValueList>>>>>>>>"+strCellvalue);
       // System.out.println("ColumnIdList>>>>>>>>>"+strColumnId);
       // System.out.println("BidTableIdList>>>>>>"+strBidTableId);
       // System.out.println("strRowId>>>>>>"+strRowId);
        String status = "";
        status = tenderCurrencyService.updateTenderBidPlainData(strCellvalue.toString(), strColumnId.toString(), strBidTableId.toString(),strRowId.toString(),Integer.parseInt(listTenderFormId));
       // System.out.println("status>>>>>>"+status);
        return status;
    }

  }