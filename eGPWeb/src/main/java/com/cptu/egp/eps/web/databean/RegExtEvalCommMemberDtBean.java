/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.web.utility.BanglaNameUtils;
import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import java.util.Date;

/**
 *
 * @author rishita
 */
public class RegExtEvalCommMemberDtBean {

    private int memberId;
    private String emailId;
    private String password;
    private String fullName;
    private String nameBanglaString;
    private byte[] nameBangla;
    private String nationalId;
    private String phoneNo;
    private String mobileNo;
    private int createdBy;
    private int userId;
    private String creationDtString;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCreationDtString() {
        return creationDtString;
    }

    public void setCreationDtString(String creationDtString) {
        this.creationDtString = creationDtString;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getNameBanglaString() {
        return nameBanglaString;
    }

    public void setNameBanglaString(String nameBanglaString) {
        if (nameBanglaString != null) {
            if (!nameBanglaString.equals("")) {
                this.nameBangla = BanglaNameUtils.getBytes(nameBanglaString);
            }
        }
        this.nameBanglaString = nameBanglaString;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public byte[] getNameBangla() {
        return nameBangla;
    }

    public void setNameBangla(byte[] nameBangla) {
        this.nameBangla = nameBangla;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = SHA1HashEncryption.encodeStringSHA1(password);
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
}
