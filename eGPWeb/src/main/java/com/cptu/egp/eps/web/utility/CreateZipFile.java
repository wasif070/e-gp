/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author nishit
 */
public class CreateZipFile {

    /**
     * this method is used for zip a folder.
     * @param directory (directory Location which u want to zip)
     * @param zip (Desitination  where u want to palce ur zip)
     * @throws IOException
     */
    public void zipDirectory(File directory, File zip) throws IOException {
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zip));
        zip(directory, directory, zos);
        zos.close();
    }
    /**
     * this method is used for internal operation of zip the folder.
     */
    private void zip(File directory, File base,
            ZipOutputStream zos) throws IOException {
        File[] files = directory.listFiles();

        byte[] buffer = new byte[8192];
        int read = 0;

        for (int i = 0, n = files.length; i < n; i++) {
            if (files[i].isDirectory()) {
                zip(files[i], base, zos);
            } else {
                FileInputStream in = new FileInputStream(files[i]);
                ZipEntry entry = new ZipEntry(files[i].getPath().substring(
                        base.getPath().length() + 1));
                zos.putNextEntry(entry);
                while (-1 != (read = in.read(buffer))) {
                    zos.write(buffer, 0, read);
                }
                in.close();
            }
        }
    }
    
    
    
    
}
