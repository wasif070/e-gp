/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Karan
 */
public class SearchRegisterationUserServlet extends HttpServlet {
   
    /**this servlet handles all the request for SearchRegUser.jsp page
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   private CommonSearchService commonSearchService =(CommonSearchService) AppContext.getSpringBean("CommonSearchService");
   private static final Logger LOGGER = Logger.getLogger(SearchRegisterationUserServlet.class);
   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String logUserId = "0";

        try {
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                commonSearchService.setLogUserId(logUserId);
            }
            LOGGER.debug("processRequest : "+logUserId+" Starts");
            // START: GETTING DATA USING RISHITA'S LOGIC

           /* getting  data from database and displaying in SearchRegUser.jsp page jqgrid*/

            String strPageNo = request.getParameter("pageNo");
            int pageNo = Integer.parseInt(strPageNo);
            String strOffset = request.getParameter("size");
            int recordOffset = Integer.parseInt(strOffset);
            String styleClass = "";

            
            List<SPCommonSearchData> listing = commonSearchService.searchData("getSearchRegUserListing", strPageNo , strOffset,"","", "", "", "", "", "");

            if (!listing.isEmpty()) {
                for (int i = 0; i < listing.size(); i++) {
                    if (i % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    SPCommonSearchData commonSearchListing = listing.get(i);
                    out.print("<tr class='" + styleClass + "'>");
                    out.print("<td class=\"t-align-center\">" + ((pageNo - 1) * 10 + (i + 1)) + "</td>");
                    out.print("<td class=\"t-align-left\">" + commonSearchListing.getFieldName3() + "</td>");
                    out.print("<td class=\"t-align-left\">" + commonSearchListing.getFieldName4() + "</td>");
                    out.print("<td class=\"t-align-center\">" + commonSearchListing.getFieldName6() + "</td>");
                    out.print("<td class=\"t-align-center\">" + commonSearchListing.getFieldName5() + "</td>");
                    out.print("<td class=\"t-align-center\">");
                        out.print("<a href=\"RegistrationFeePaymentDetails.jsp?payId=" + commonSearchListing.getFieldName1() + "&uId=" + commonSearchListing.getFieldName2() + "\" title=\"\">View</a>");
                    out.print("</td>");
                    out.print("</tr>");
                }
            } else {
                out.print("<tr>");
                out.print("<td id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-left\" style=\"color: red;font-weight: bold\" colspan=\"8\">No Record Found!</td>");
                out.print("</tr>");
            }
            int totalPages = 1;
            if (!listing.isEmpty()) {
                int rowCount = Integer.parseInt(listing.get(0).getFieldName8());
                totalPages = (int) Math.ceil(Math.ceil(rowCount)/ recordOffset);
                if (totalPages == 0) {
                    totalPages = 1;
                }
            }
            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");

            // END: GETTING DATA USING RISHITA'S LOGIC


        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
