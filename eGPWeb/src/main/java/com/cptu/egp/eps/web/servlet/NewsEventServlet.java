/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author dixit
 */
public class NewsEventServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    private static final Logger LOGGER = Logger.getLogger(NewsEventServlet.class);
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();        
        try {
            String funName = request.getParameter("funName");
            String newsType = request.getParameter("newstype");
            int Size = Integer.parseInt(request.getParameter("size"));
            String logUserId = "0";
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
                tenderCommonService.setLogUserId(logUserId);
            }
            LOGGER.debug("processRequest : "+logUserId+" Starts");
            if("viewNewsEventsList".equalsIgnoreCase(funName))
            {
                LOGGER.debug("viewNewsEventsList : "+logUserId+" Starts");
                String styleClass = "";
                String strPageNo = "1";
                
                int pageNo = Integer.parseInt(request.getParameter("pageNo"));
                String strOffset = "10000";
                
                int recordOffset = Integer.parseInt(strOffset);

                
                List<SPTenderCommonData> list =null;
                List<SPTenderCommonData> list2 = new ArrayList<SPTenderCommonData>();
                list = tenderCommonService.returndata("getLiveNewslisting",strPageNo , strOffset);
                
                
                if (list != null && !list.isEmpty()) 
                {
                    for (int i = 0; i < list.size(); i++) 
                    {
                        if(list.get(i).getFieldName7().equals(newsType))
                        {
                            list2.add(list.get(i));
                        }
                    }
                }
                
                if (list2 != null && !list2.isEmpty()) 
                {
                    for (int i = (pageNo-1)*10; i < ((pageNo-1)*10)+Size && i<list2.size(); i++) 
                    {
                            if (i % 2 == 0) {
                                styleClass = "bgColor-white";
                            } else {
                                styleClass = "bgColor-Green";
                            }                            
                                String brief = "<a href=ViewNewsDetails.jsp?newsType="+newsType+"&nId="+list2.get(i).getFieldName1()+">"+list2.get(i).getFieldName2()+"</a>";
                                String action = "<a href=News.jsp?newsType="+newsType+"&nId="+list2.get(i).getFieldName1()+">Edit</a>"+
                                "&nbsp;|&nbsp;<a onclick=\"javascript: return ConfirmArchive("+list2.get(i).getFieldName1()+");\" href=#>Archive</a>"+
                                "&nbsp;|&nbsp;<a onClick=\"javascript: return ConfirmDelete("+list2.get(i).getFieldName1()+");\" name=ancDelete id =ancDelete href=#>Delete</a>";
                                out.print("<tr class='" + styleClass + "'>");
                                out.print("<td class=\"t-align-center\">" + (i+1) + "</td>");
                                out.print("<td class=\"t-align-left\">" + brief + "</td>");
                                out.print("<td class=\"t-align-center\">" + action + "</td>");
                                out.print("</tr>");
                    }
                    int totalPages = 0;
                    if (list2.size() > 0) {                        
                        totalPages = (list2.size() + Size - 1) / Size;
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                 
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                } else {
                    out.print("<tr>");

                    out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
                LOGGER.debug("viewNewsEventsList : "+logUserId+" Ends");
            }
            else if("viewArchiveNewsEventList".equalsIgnoreCase(funName))
            {
                LOGGER.debug("viewArchiveNewsEventList : "+logUserId+" Starts");
                String styleClass = "";
                String strPageNo = "1";
                int pageNo = Integer.parseInt(request.getParameter("pageNo"));
                String strOffset = "10000";
                int recordOffset = Integer.parseInt(strOffset);
                
                
                List<SPTenderCommonData> list =null;
                List<SPTenderCommonData> list2 = new ArrayList<SPTenderCommonData>();
                list = tenderCommonService.returndata("getArchiveNewslisting",strPageNo , strOffset);
                if (list != null && !list.isEmpty()) 
                {
                    for (int i = 0; i < list.size(); i++) 
                    {
                        if(list.get(i).getFieldName7().equals(newsType))
                        {
                            list2.add(list.get(i));
                        }
                    }
                }
                if (list2 != null && !list2.isEmpty()) 
                {
                    for (int i = (pageNo-1)*10; i < ((pageNo-1)*10)+Size && i<list2.size(); i++) 
                    {
//                        if (i % 2 == 0) {
//                            styleClass = "bgColor-white";
//                        } else {
//                            styleClass = "bgColor-Green";
//                        }
                        String brief = "<a href=ViewNewsDetails.jsp?newsType="+newsType+"&nId="+list2.get(i).getFieldName1()+">"+list2.get(i).getFieldName2()+"</a>";                            
                        out.print("<tr>");
                        out.print("<td class=\"t-align-center\">" + (i+1) + "</td>");
                        out.print("<td class=\"t-align-left\">" + brief + "</td>");
                        out.print("<td class=\"t-align-center\">" + list2.get(i).getFieldName3() + "</td>");
                        out.print("</tr>");
                        
                    }
                    int totalPages = 0;
                    if (list2.size() > 0) {                        
                        totalPages = (list2.size() + Size - 1) / Size;
                        if (totalPages == 0) {
                            totalPages = 1;
                        }
                 
                    }
                    out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalPages + "\">");
                } else {
                    out.print("<tr>");

                    out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                    out.print("</tr>");
                }
                LOGGER.debug("viewArchiveNewsEventList : "+logUserId+" Ends");
            }
           LOGGER.debug("processRequest : "+logUserId+" Starts");

        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
