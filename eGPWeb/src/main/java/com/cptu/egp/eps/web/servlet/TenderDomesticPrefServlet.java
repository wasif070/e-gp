/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblTenderDomesticPref;
import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.service.serviceimpl.CommitteMemberService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class TenderDomesticPrefServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(APPServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND = "Ends";
    private String logUserId = "0";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");
            HttpSession session = request.getSession();
            if (session.getAttribute("userId") != null) {
                logUserId = session.getAttribute("userId").toString();
                committeMemberService.setLogUserId(logUserId);
            }
            String action = request.getParameter("action");
            if ("insert".equalsIgnoreCase(action) || "edit".equalsIgnoreCase(action)) {
                if (saveTblTenderDomesticPref(request, committeMemberService, action)) {
                    response.sendRedirect("officer/EvalComm.jsp?tenderid=" + request.getParameter("tenderid") + "&domPreMsg=sucess");
                    //response.sendRedirectFilter("EvalComm.jsp?tenderid=" + request.getParameter("tenderid") + "&domPreMsg=sucess");
                } else {
                    response.sendRedirect("officer/EvalComm.jsp?tenderid=" + request.getParameter("tenderid") + "&domPreMsg=fail");
                    //response.sendRedirectFilter(request.getContextPath() + "/officer/EvalComm.jsp?tenderid=" + request.getParameter("tenderid") + "&domPreMsg=fail");
                }
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public boolean saveTblTenderDomesticPref(HttpServletRequest request, CommitteMemberService committeMemberService, String action) {
        boolean flag = false;
        LOGGER.debug("saveTblTenderDomesticPref : " + logUserId + LOGGERSTART);
        try {
            int tenderid = 0;
            if (request.getParameter("tenderid") != null && !"".equalsIgnoreCase(request.getParameter("tenderid"))) {
                tenderid = Integer.parseInt(request.getParameter("tenderid"));
            }
            List<TblTenderDomesticPref> list = new ArrayList<TblTenderDomesticPref>();
            for (int i = 0; i < request.getParameterValues("userId").length; i++) {

                String domesticPref = "No";
                if(request.getParameterValues("domesticPref") != null){
                    for (int j = 0; j < request.getParameterValues("domesticPref").length; j++) {
                        if (request.getParameterValues("userId")[i].equals(request.getParameterValues("domesticPref")[j])) {
                            domesticPref = "Yes";
                        }
                    }
                }

                TblTenderDomesticPref pref = new TblTenderDomesticPref();
                pref.setCreatedBy(Integer.parseInt(logUserId));
                if ("edit".equalsIgnoreCase(action)) {
                    pref.setDomPriceId(Integer.parseInt(request.getParameterValues("domPriceId")[i]));
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    Date d = dateFormat.parse(request.getParameterValues("createdDt")[i]);
                    pref.setCreatedDt(d);
                } else {
                    pref.setCreatedDt(new Date());
                }
                pref.setDomesticPref(domesticPref);
                pref.setEvalStatus("evaluation");
                pref.setTblTenderMaster(new TblTenderMaster(tenderid));
                pref.setUserId(Integer.parseInt(request.getParameterValues("userId")[i]));
                list.add(pref);
            }
            if (committeMemberService.saveTblTenderDomesticPref(list)) {
                flag = true;
            }
        } catch (Exception e) {
            LOGGER.error("saveTblTenderDomesticPref : " + logUserId + e);
        }
        LOGGER.debug("saveTblTenderDomesticPref : " + logUserId + LOGGEREND);
        return flag;
    }
}
