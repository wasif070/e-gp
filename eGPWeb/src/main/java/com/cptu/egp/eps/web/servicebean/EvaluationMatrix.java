/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

/**
 *
 * @author TaherT
 */
public class EvaluationMatrix {

    public boolean[] getEvaluationCase(String procNature,String eventType,String pMethod,boolean is2Env) {
        boolean[] cases = new boolean[3];
        boolean isCase1 = false;//PQ, 1st stage of TSTM and OSTETM
        boolean isCase2 = false;//2nd stage of OSTETM
        boolean isCase3 = false;//OTM / LTM / 2nd stage of TSTM
        if (procNature.equals("Services")) { // For Services
//            if ((eventType.equals("RFA") || eventType.equals("RFP")) && (pMethod.equals("SSS") || pMethod.equals("IC") || pMethod.equals("DC"))) {
//                isCase1 = true;
//            }
//            if ((eventType.equals("REOI") || eventType.equals("RFP")) && (pMethod.equals("QCBS") || pMethod.equals("LCS") || pMethod.equals("SFB") || pMethod.equals("DC") || pMethod.equals("CSO") || pMethod.equals("SBCQ"))) {
//                isCase2 = true;
//            }
            if ((eventType.equals("REOI")) && (pMethod.equals("QCBS") || pMethod.equals("LCS") || pMethod.equals("SFB") || pMethod.equals("DC") || pMethod.equals("SBCQ") || pMethod.equals("CSO"))) {
                isCase1 = true;
            }
//            if (eventType.equals("RFP") && (pMethod.equals("DC") || pMethod.equals("SBCQ") || pMethod.equals("SSS") || pMethod.equals("IC") || pMethod.equals("CSO")) && (is2Env)) {
//                isCase2 = true;
//            }
//            if ((eventType.equals("RFA") || eventType.equals("RFP")) && (pMethod.equals("DC") || pMethod.equals("SBCQ") || pMethod.equals("SSS") || pMethod.equals("IC") || pMethod.equals("CSO")) && (!is2Env)) {
//                isCase3 = true;
//            }
            if ((eventType.equals("RFA") && (pMethod.equals("DC") || pMethod.equals("SBCQ") || pMethod.equals("SSS") || pMethod.equals("IC") || pMethod.equals("CSO"))) || (eventType.equals("RFP") && (pMethod.equals("QCBS") || pMethod.equals("LCS") || pMethod.equals("SFB") || pMethod.equals("DC") || pMethod.equals("SBCQ") || pMethod.equals("CSO") || (pMethod.equals("DC") || pMethod.equals("SBCQ") || pMethod.equals("SSS") || pMethod.equals("IC") || pMethod.equals("CSO"))))) {
                 if(is2Env){
                    isCase2 = true;
                 }else{
                    isCase3 = true;
                 }
            }

        } else {
            if (eventType.equals("1 stage-TSTM") || eventType.equals("PQ") || (pMethod.equals("OSTETM") && (!is2Env))) {
                isCase1 = true;
            }

            if ((pMethod.equals("OSTETM") && (is2Env))) {
                isCase2 = true;
            }

            if (eventType.equals("2 stage-TSTM") || (pMethod.equals("OTM") && !eventType.equals("PQ")) || pMethod.equals("LTM") || pMethod.equals("RFQ") || pMethod.equals("RFQU") || pMethod.equals("RFQL") || pMethod.equals("DPM")) {
                isCase3 = true;
            }
        }
        cases[0] = isCase1;
        cases[1] = isCase2;
        cases[2] = isCase3;
        return cases;
    }
}
