/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import java.io.PrintWriter;
import java.io.StringWriter;

/*
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
*/
/**
 *
 * @author darshan
 */
public class AppExceptionHandler {
/*
    public static void main(String s[])
    {
    try {
            throw new Exception("for no reason!");
      }
      catch (Exception e) {
          try{
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            boolean sessionDebug = false;
            InternetAddress[] addressForMail = InternetAddress.parse("");
            Properties props = System.getProperties();
            props.put("mail.host", "");
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.port", "");
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);

            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(""));
            msg.setRecipients( Message.RecipientType.TO, addressForMail);
            msg.setSentDate(new java.util.Date());
            msg.setSubject(e.getMessage());
            msg.setContent(sw.toString(), "text/html");

            Transport.send(msg);
            
          System.out.println(sw.toString().toUpperCase());
          }catch(Exception ex){
            ex.printStackTrace();
          }
      }
    }
*/
    public static void stack2string(Exception e) {
        try {
            if(XMLReader.getMessage("exceptionFrom") != null && XMLReader.getMessage("exceptionMailTo")!= null)
            {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);

                SendMessageUtil smu = new SendMessageUtil();
                smu.setEmailFrom(XMLReader.getMessage("exceptionFrom"));
                smu.setEmailTo(XMLReader.getMessage("exceptionMailTo").split(","));
                smu.setEmailSub("eGP Error : " +e.getMessage());
                smu.setEmailMessage(sw.toString());
                smu.sendEmail();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}