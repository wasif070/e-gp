/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblAdminMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblOfficeAdmin;
import com.cptu.egp.eps.model.table.TblStateMaster;
import com.cptu.egp.eps.model.table.TblUserTypeMaster;
// Code Added by Palash, Dohatec.
import com.cptu.egp.eps.model.table.TblAdminTransfer;
 // End
import com.cptu.egp.eps.service.serviceimpl.AdminRegistrationServiceImpl;
import com.cptu.egp.eps.service.serviceinterface.GovtUserCreationService;
import com.cptu.egp.eps.web.databean.AdminMasterDtBean;
 // Code Added by Palash, Dohatec
import com.cptu.egp.eps.web.databean.AdminTransferDtBean;
// End
import com.cptu.egp.eps.web.databean.LoginMasterDtBean;
import com.cptu.egp.eps.web.databean.OfficeAdminDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.SelectItem;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author Administrator
 */
public class PEAdminSrBean {

    private AdminRegistrationServiceImpl adminRegService = (AdminRegistrationServiceImpl) AppContext.getSpringBean("AdminRegistrationService");
    GovtUserCreationService govtUserCreationService = (GovtUserCreationService) AppContext.getSpringBean("GovtUserCreationService");
    GovtUserCreationService govRegService = (GovtUserCreationService) AppContext.getSpringBean("GovtUserCreationService");
    final static Logger LOOGER = Logger.getLogger(PEAdminSrBean.class);
    private String logUserId = "0";
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        adminRegService.setAuditTrail(auditTrail);
        govtUserCreationService.setAuditTrail(auditTrail);
    }

    /**
     * For logging purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        adminRegService.setLogUserId(logUserId);
        govtUserCreationService.setLogUserId(logUserId);
        govRegService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }
    List<SelectItem> organizationList = new ArrayList<SelectItem>();
    List<OfficeAdminDtBean> officeList = new ArrayList<OfficeAdminDtBean>();

    /**
     * copies properties
     * @param adminMasterDtBean
     * @return Object with copied properties
     */
    public TblAdminMaster _toAdminMaster(AdminMasterDtBean adminMasterDtBean) {
        TblAdminMaster adminMaster = new TblAdminMaster();
        BeanUtils.copyProperties(adminMasterDtBean, adminMaster);
        return adminMaster;
    }
    // Code Added By palash, Dohatec
   public TblAdminTransfer _toAdminTransfer(AdminTransferDtBean adminTransferDtBean_1) {
       TblAdminTransfer adminTransfer = new TblAdminTransfer();
     BeanUtils.copyProperties(adminTransferDtBean_1, adminTransfer);
      return adminTransfer;
   }
    //End
    /**
     * copies properties
     * @param loginMasterDtBean
     * @return Object with copied properties
     */
    public TblLoginMaster _toLoginMaster(LoginMasterDtBean loginMasterDtBean) {
        TblLoginMaster loginMaster = new TblLoginMaster();
        BeanUtils.copyProperties(loginMasterDtBean, loginMaster);
        //loginMaster.setPassword(""+loginMasterDtBean.getPassword().hashCode());
        return loginMaster;
    }

    /**
     * copies properties
     * @param officeAdminDtBean
     * @return Object with copied properties
     */
    public TblOfficeAdmin _toOfficeAdmin(OfficeAdminDtBean officeAdminDtBean) {
        TblOfficeAdmin officeAdmin = new TblOfficeAdmin();
        BeanUtils.copyProperties(officeAdminDtBean, officeAdmin);
        return officeAdmin;
    }
    Integer deptId = 0;

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    /**
     * Get admin office using department Id
     * @param deptId
     * @return List of Object [0]officeId, [1]officeName
     */
    public List<OfficeAdminDtBean> getOfficeList() {
        LOOGER.debug("getOfficeList : " + logUserId + " Starts");
        if (officeList.isEmpty()) {
            try {
                for (Object[] obj : adminRegService.getAdminOfficeList(this.getDeptId())) {
                    officeList.add(new OfficeAdminDtBean((Integer) obj[0], (String) obj[1]));
                }
            } catch (Exception ex) {
                LOOGER.error("getOfficeList : " + logUserId + ex);
            }
        }
        LOOGER.debug("getOfficeList : " + logUserId + " Ends");
        return officeList;
    }

    public void setOfficeList(List<OfficeAdminDtBean> officeList) {
        this.officeList = officeList;
    }

    /**
     * insert values for PE admin,
     * code modify by palash, change the parameter in peAdminReg
     * @param adminMasterDtBean
     * @param loginMasterDtBean
     * @param str
     * @param pwd
     * @return true if inserted else false
     */
    public Integer peAdminReg(AdminMasterDtBean adminMasterDtBean,AdminTransferDtBean adminTransferDtBean, LoginMasterDtBean loginMasterDtBean, String[] str, String pwd) {

        LOOGER.debug("peAdminReg : " + logUserId + " Starts");
        TblLoginMaster loginMaster = _toLoginMaster(loginMasterDtBean);
        loginMaster.setBusinessCountryName("");
        loginMaster.setRegistrationType("officer");
        loginMaster.setIsJvca("no");
        //loginMaster.setBusinessCountryName("bangladesh");
        loginMaster.setBusinessCountryName("bhutan");
        loginMaster.setNextScreen("ChangePassword");
        loginMaster.setIsEmailVerified("yes");
        loginMaster.setFailedAttempt((byte) 0);
        loginMaster.setValidUpTo(null);
        loginMaster.setFirstLogin("yes");
        loginMaster.setIsPasswordReset("no");
        loginMaster.setResetPasswordCode("0");
        loginMaster.setStatus("approved");
        loginMaster.setRegisteredDate(new Date());
        //loginMaster.setNationality("bangladeshi");
        loginMaster.setNationality("Bhutanese");
        loginMaster.setHintQuestion("");
        loginMaster.setHintAnswer("");
        loginMaster.setTblUserTypeMaster(new TblUserTypeMaster((byte) 4));
        TblAdminMaster adminMaster = _toAdminMaster(adminMasterDtBean);
        // added by palash, dohatec
        TblAdminTransfer adminTransfer = _toAdminTransfer(adminTransferDtBean);
        adminTransfer.setIsCurrent("yes");
         // End
        int userId = 0;     
      
        try {
            // code added by palash, dohatec
            userId = adminRegService.registerPEAdmin(adminMaster,adminTransfer,loginMaster, str);
            // End
            if (userId > 0) {
                Object[] otherDetails = null;
                otherDetails = govtUserCreationService.getPEOfficeDetailsforMail(userId);
                sendMailAndSMS(loginMasterDtBean.getEmailId(), pwd, adminMasterDtBean.getMobileNo(), otherDetails);
                }
        } catch (Exception ex) {
            LOOGER.error("peAdminReg : " + logUserId + ex);
            }
        LOOGER.debug("peAdminReg : " + logUserId + " Ends");
            return userId;
        }

    /**
      * get department id of user
      * @param userId
      * @param userTypeId
      * @return departmentId
      * @throws Exception
      */
    public void getUserTypeWiseDepartmentId(int govUserId, int userTypeId) {
        LOOGER.debug("getUserTypeWiseDepartmentId : " + logUserId + " Starts");
        try {
            this.setDeptId(govRegService.getUserTypeWiseDepartmentId(govUserId, userTypeId));
        } catch (Exception ex) {
            LOOGER.error("getUserTypeWiseDepartmentId : " + logUserId + ex);
    }
        LOOGER.debug("getUserTypeWiseDepartmentId : " + logUserId + " Ends");
        }

    /**
      * send mail and sms for register user
      * @param mailId
      * @param password
      * @param mobNo
      * @param otherDetails
      * @return true if mail and sms sent else false
      */
    private boolean sendMailAndSMS(String mailId, String password, String mobNo, Object[] otherDetails) //private boolean sendMailAndSMS(String mailId,String password,String mobNo)
    {
        LOOGER.debug("sendMailAndSMS : " + logUserId + " Starts");
        boolean mailSent = false;
        try {
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            String[] mailTo = {mailId};
            MailContentUtility mailContentUtility = new MailContentUtility();

            List list = mailContentUtility.getAdminandGovtUserMailContent(mailId, password, otherDetails);
            //List list = mailContentUtility.getAdminandGovtUserMailContent(mailId,password);

            String mailSub = list.get(0).toString();
            String mailText = list.get(1).toString();
            sendMessageUtil.setEmailTo(mailTo);
            sendMessageUtil.setEmailSub(mailSub);
            sendMessageUtil.setEmailMessage(mailText);

            try {
                sendMessageUtil.sendEmail();
            } catch (Exception ex) {
                LOOGER.error("sendMailAndSMS : " + logUserId + ex);
            }

            try {
                SendMessageUtil sendSMS = new SendMessageUtil();
                sendSMS.setSmsNo("+975" + mobNo);
                sendSMS.setSmsBody("Dear User,%0Aprofile has been created Login information is sent to your registered Email ID.%0Ae-GP User GPPMD Office");
                sendSMS.sendSMS();
            } catch (Exception ex) {
                LOOGER.error("sendMailAndSMS : " + logUserId + ex);
            }
            mailSent = true;
        } catch (Exception ex) {
            LOOGER.error("sendMailAndSMS : " + logUserId + ex);
        }
        LOOGER.debug("sendMailAndSMS : " + logUserId + " Ends");
        return mailSent;
    }

    /**
     * fetching states from country
     * @param countryId
     * @return list of states
     */
    public List<TblStateMaster> getBanglaStates(short countryId) {
        LOOGER.debug("getBanglaStates : " + logUserId + " Starts");
        List<TblStateMaster> tblStateMasters = null;
        try {
       tblStateMasters =  adminRegService.getBanglaStates(countryId);
        } catch (Exception ex) {
            LOOGER.error("getBanglaStates : " + logUserId + ex);
        }
        LOOGER.debug("getBanglaStates : " + logUserId + " Ends");
       return tblStateMasters;
    }

    /**
     * Fetching info from TblDepartmentMaster for Approving Authority
     * @param userId
     * @return department id
     * @throws Exception
     */
    public short getDepartmentIdByUserId(int userId) throws Exception {
        LOOGER.debug("getDepartmentIdByUserId : " + logUserId + " Starts");
        short i = 0;
        try {
            i = adminRegService.getDepartmentIdByUserId(userId);
        } catch (Exception ex) {
            LOOGER.error("getDepartmentIdByUserId : " + logUserId + ex);
    }
        LOOGER.debug("getDepartmentIdByUserId : " + logUserId + " Ends");
        return i;
    }

    /**
     * Fetching department name
     * @param userId (approvingAuthorityId of TblDepartmentMaster)
     * @return department Name of particular userid
     * @throws Exception
     */
    public String getDepartmentNameByUserId(int userId) throws Exception {
        LOOGER.debug("getDepartmentNameByUserId : " + logUserId + " Starts");
        String str = "";
        try {
            str = adminRegService.getDepartmentNameByUserId(userId);
        } catch (Exception ex) {
            LOOGER.error("getDepartmentNameByUserId : " + logUserId + ex);
    }
        LOOGER.debug("getDepartmentNameByUserId : " + logUserId + " Ends");
        return str;
    }
}
