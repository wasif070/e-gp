/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.model.table.TblEmailLog;
import com.cptu.egp.eps.model.table.TblEmailPrefMaster;
import com.cptu.egp.eps.model.table.TblSmsPrefMaster;
import com.cptu.egp.eps.model.table.TblSmslog;
import com.cptu.egp.eps.service.serviceimpl.CommonServiceImpl;
import com.cptu.egp.eps.service.serviceinterface.EmailLogService;
import com.cptu.egp.eps.service.serviceinterface.SmslogService;
import com.cptu.egp.eps.service.serviceinterface.UserPrefService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
//import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
//import java.util.Date;
//import java.util.List;
//import java.util.Properties;
//import java.util.Random;
//import javax.mail.Message;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMailMessage;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *
 * @author Administrator
 */
public class SendMessageUtil implements Serializable, Runnable {

    /*private static final String SMTP_HOST_NAME = "192.168.100.131";
    private static final int SMTP_HOST_PORT = 25;
    private static final String SMTP_AUTH_USER = "mtnleprocurement@gmail.com";
    private static final String SMTP_AUTH_PWD = "mtnl1234";*/
    private String curUserId;
    private String smsEventName;
    private String sendBy;
    String emailMessage;
    String emailTo[];
    String emailSub;
    String smsBody;
    String smsNo;
    String emailId;
    boolean isEmail = true;
    boolean isSMS = true;
    String emailFrom = XMLReader.getMessage("emailId");
    static final Logger LOGGER = Logger.getLogger(SendMessageUtil.class);
    private boolean isForgotPassword = false;
    CommonServiceImpl commonServiceImpl = (CommonServiceImpl) AppContext.getSpringBean("CommonService");

    public boolean isIsForgotPassword() {
        return isForgotPassword;
    }

    public void setIsForgotPassword(boolean isForgotPassword) {
        if (isForgotPassword) {
            isEmail = true;
        }
        this.isForgotPassword = isForgotPassword;

    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getSmsBody() {
        return smsBody;
    }

    public void setSmsBody(String smsBody) {
        if (smsBody != null) {
            if (!("".equals(smsBody))) {
                smsBody = smsBody.replace(" ", "%20");
            }
        }
        this.smsBody = smsBody;
    }

    public String getSmsNo() {
        return smsNo;
    }

    public void setSmsNo(String smsNo) {
        this.smsNo = smsNo;
    }

    public String getSendBy() {
        return sendBy;
    }

    public void setSendBy(String sendBy) {
        this.sendBy = sendBy;
    }

    public String getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(String emailMessage) {
        this.emailMessage = emailMessage;
    }

    public String getEmailSub() {
        return emailSub;
    }

    public void setEmailSub(String emailSub) {
        this.emailSub = emailSub;
    }

    public String[] getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String[] emailTo) {
//        if (emailTo != null) {
//            UserPrefService prefService = (UserPrefService) AppContext.getSpringBean("UserPrefService");
//            List<Object[]> list = prefService.getEmailSmsStatus(emailTo[0]);
//            for (Object[] stat : list) {
//                if (stat[0].toString().equalsIgnoreCase("No")) {
//                    isEmail = false;
//                }
//                if (stat[1].toString().trim().equalsIgnoreCase("No")) {
//                    isSMS = false;
//                }
//            }
//        }
        this.emailTo = emailTo;
    }

    /**
     * Send Mail
     */
    public void sendEmail() {

        LOGGER.debug("sendEmail Starts:");
        //if(isEmail){
        TblEmailLog tblEmailLog = new TblEmailLog();
        String excep = "";
        EmailLogService emailLogService = (EmailLogService) AppContext.getSpringBean("EmailLogService");

        try {
//            Properties props = new Properties();
//
//            props.put("mail.transport.protocol", "smtps");
//            props.put("mail.smtps.host", SMTP_HOST_NAME);
//            props.put("mail.smtps.auth", "true");
//
//            Session mailSession = Session.getDefaultInstance(props);
////            mailSession.setDebug(true);
//
//            Transport transport = mailSession.getTransport();
//            transport.connect(SMTP_HOST_NAME, SMTP_HOST_PORT, SMTP_AUTH_USER, SMTP_AUTH_PWD);
//
//            MimeMessage message = new MimeMessage(mailSession);
//            if (!(emailMessage.length == emailSub.length && emailSub.length == emailTo.length)) {
//                throw new Exception("Emails are not Configured Properly.");
//            }
//            for (int i = 0; i < emailTo.length; i++) {
//                message.setSubject(emailSub[i]);
//                message.setText(emailMessage[i]);
//                message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailTo[i]));
//                transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
//            }
//            transport.close();]
            /* Mail sending by Simple Process - Taher
            String smtpPort = XMLReader.getMessage("smtpPort");
            boolean sessionDebug = false;
            Properties props = System.getProperties();
            props.put("mail.host", XMLReader.getMessage("smtp"));
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.port", smtpPort);
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(getEmailFrom()));
            StringBuffer to =new StringBuffer();
             */
            /*  Mail sending through Spring - Starts */
            UserPrefService prefService = (UserPrefService) AppContext.getSpringBean("UserPrefService");
            JavaMailSenderImpl mailSender = (JavaMailSenderImpl) AppContext.getSpringBean("JavaMailSender");
            mailSender.setHost(XMLReader.getMessage("smtp"));
            mailSender.setPort(Integer.parseInt(XMLReader.getMessage("smtpPort")));
            mailSender.setProtocol("smtp");
           // mailSender.setUsername("info.bhutan");
           // mailSender.setPassword("bhutan@123");
            //Authenticator auth = new SMTPAuthenticator("info.bhutan@dohatec.com.bd", "bhutan@123");
            MimeMessage msg = mailSender.createMimeMessage();
            /*  Mail sending through Spring - Ends */

//            List emailList = new ArrayList();
//            emailList.add("mal2388@gmail.com");

            /*int counter = 0;
            for(String mail: emailTo){
            if(counter == 0){
            to.append(mail);
            counter++;
            }else{
            to.append(","+mail);
            }
            }*/
//System.out.println("...................." + checkEmailPreferences(emailTo));
            // String toEmail[] = emailTo;
            String toEmail[] = checkEmailPreferences(emailTo);
//System.out.println("toAddrtoAddr>>>>"+toEmail);
            if (toEmail != null && toEmail.length > 0) {
                for (String toAddr : toEmail) {

                    /*  Mail sending through Spring - Starts */
                    msg.setContent(getEmailMessage(), "text/html");
                    MimeMailMessage message = new MimeMailMessage(msg);
                    String toAddress = toAddr;
                    message.setTo(toAddress);
                    message.setFrom(getEmailFrom());
                    message.setSubject(getEmailSub());
                    //message.setText(getEmailMessage());
                    //System.out.println("Sending email ....");
                /*  Mail sending through Spring - Ends */
                    excep = "";
                    /* Mail sending by Simple Process - Taher
                    InternetAddress[] addressForMail = InternetAddress.parse(toAddr.toString());
                    msg.setRecipients(Message.RecipientType.TO, addressForMail);
                     */
                    //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Mail To:"+toAddr);
                    tblEmailLog.setEmailId(toAddr);
                    if (toAddr != null) {
                        List<Object[]> list = prefService.getEmailSmsStatus(toAddr.toString());                        
                        for (Object[] stat : list) {
                            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>::"+stat[0].toString());
                            if (stat[0].toString().equalsIgnoreCase("No") && !isForgotPassword) {
                                isEmail = false;
                            }
                        }
                    }

                    tblEmailLog.setEmailSubject(getEmailSub());
                    tblEmailLog.setEmailSentDt(new java.util.Date());

                    /* Mail sending by Simple Process - Taher
                    msg.setSubject(getEmailSub());
                    msg.setSentDate(new java.util.Date());
                    msg.setContent(getEmailMessage(), "text/html");
                     */
                    LOGGER.debug("\n<----------------11------------------------>\n");
                    try {
                        if (isEmail) {
                            //Transport.send(msg); //Simple Process
                            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Mail Message:"+message.getMimeMessage());
                            mailSender.send(message.getMimeMessage()); //By Spring
                        } else {
                            isEmail = true;
                            excep = "User Preference set as No";
                        }
                    } catch (MailSendException mse) {
                        LOGGER.error("sendEmail :" + mse);
                        excep = mse.toString();
                        // Incase of Fail from spring schedule then send mail using transport
                        Properties props = System.getProperties();
                        props.put("mail.smtp.host", XMLReader.getMessage("smtp"));
                        props.put("mail.smtp.port", Integer.parseInt(XMLReader.getMessage("smtpPort")));
                        props.setProperty("mail.smtp.auth", "false");
                        Authenticator auth = new SMTPAuthenticator("info.bhutan", "bhutan@123");
                        Session mailSession = Session.getInstance(props, auth);
                        //Session mailSession = Session.getDefaultInstance(props, null);
                        MimeMessage message1 = new MimeMessage(mailSession);
                        message1.setFrom(new InternetAddress(getEmailFrom()));
                        message1.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
                        message1.setSubject(getEmailSub());
                        message1.setText(getEmailMessage());
                        Transport.send(message1); // In case of Spring fail becasue of SSL/TSL then go with this one.
                    } catch (Exception e) {
                        LOGGER.error("sendEmail :" + e);
                        excep = e.toString();
                    }
                    tblEmailLog.setExceptionLog(excep);

                    try {
                        emailLogService.addEmailLog(tblEmailLog);
                    } catch (Exception ex) {
                        LOGGER.error("sendEmail :" + ex);
                    }
                }
            }

        } catch (Exception e) {
            LOGGER.error("sendEmail :" + e);
        }
        //}
        LOGGER.debug("sendEmail Ends:");

    }

    private String[] checkEmailPreferences(String[] emailTo) {
        boolean check = false;
        String array[] = null;
        if(emailTo!=null && emailTo.length>0){
        List<String> emails = Arrays.asList(emailTo);
        List<String> tmpemails = new ArrayList<String>();
        
        try {

            String emailMssge = getEmailMessage();
            int start = emailMssge.indexOf("~");
            int end = emailMssge.indexOf("~~");
                if(start != -1 && end != -1){
                String first = emailMssge.substring(0, start);
                String eventname = emailMssge.substring(start + 1, end);
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Event:"+eventname);
                setSmsEventName(eventname);
                String endval = emailMssge.substring(end + 2, emailMssge.length());
                setEmailMessage(first + endval);
                int j = 0;
                for (String emailid : emails) {
                    String userId = commonServiceImpl.getUserId(emailid);
                    if(userId != null){
                        String userTypeId = commonServiceImpl.getUserTypeId(userId);
                        if(userTypeId != null){
                            int usertypeid = Integer.parseInt(userTypeId);
                            List<Object> obj = commonServiceImpl.getEmailPreferencesByUseTypeId(usertypeid);
                            boolean check2 = false;
                            if(obj != null){
                                for (int i = 0; i < obj.size(); i++) {
                                    String emailprefname = (String) obj.get(i);
                                    if (eventname.equals(emailprefname)) {
                                        check2 = true;
                                    }
                                }
                            }
                            if (check2) {
                                String emailAlertPreference = commonServiceImpl.getEmailAlertPreference(userId);
                                if(emailAlertPreference !=null){
                                    List<TblEmailPrefMaster> emailprefmasters = commonServiceImpl.getEmailPrefMaster(emailAlertPreference);
                                     if(emailprefmasters != null){
                                        for (TblEmailPrefMaster emailprefmaster : emailprefmasters) {
                                            String prefrancetype = emailprefmaster.getPrefranceType();
                                            if (eventname.trim().equals(prefrancetype.trim())) {
                                                check = true;
                                            }
                                        }
                                    }
                                    } else {
                                        check = true;
                                    }

                                } else {
                                    check = true;
                                }
                                if (check) {
                                    tmpemails.add(emailid);
                                }
                            }

                        }
                        j++;

                    }
                    if (tmpemails != null && tmpemails.size() > 0) {
                        array = (String[]) tmpemails.toArray(new String[tmpemails.size()]);
                    }
                } else {
                    return emailTo;
                }
            } catch (Exception ex) {
                //ex.printStackTrace();
                LOGGER.error("sendEMAIL : " + ex);
            }
        }
        return array;
    }

    private boolean checkSmsPreferences(String smsno) {
        //System.out.println("smsnosmsnosmsno>>>>>"+smsno);
        boolean check = false;
        try {
            if (smsno != null && !"".equals(smsno)) {
                String eventname = "";
                // System.out.println("getSmsEventName()>>>"+getSmsEventName());
                if (getSmsEventName() != null && !"".equals(getSmsEventName())) {
                    eventname = getSmsEventName();
                    String userId = commonServiceImpl.getUserTypeIdByMobileNo(smsno);
                    if (userId != null && !"".equals(userId)) {
                        String userTypeId = commonServiceImpl.getUserTypeId(userId);
                        if (userTypeId != null) {
                            int usertypeid = Integer.parseInt(userTypeId);
                            List<Object> obj = commonServiceImpl.getSmsPreferencesByUseTypeId(usertypeid);
                            boolean check2 = false;
                            if (obj != null) {
                                for (int i = 0; i < obj.size(); i++) {
                                    String emailprefname = (String) obj.get(i);
                                    if (eventname.equals(emailprefname)) {
                                        check2 = true;
                                    }
                                }
                            }
                            if (check2) {
                                String smsAlertPreference = commonServiceImpl.getSmsAlertPreference(userId);
                                if (smsAlertPreference != null) {
                                    List<TblSmsPrefMaster> smsprefmasters = commonServiceImpl.getSmsPrefMaster(smsAlertPreference);
                                    if (smsprefmasters != null) {
                                        for (TblSmsPrefMaster smsprefmaster : smsprefmasters) {
                                            String prefrancetype = smsprefmaster.getPrefranceType();
                                            if (eventname.trim().equals(prefrancetype.trim())) {
                                                check = true;
                                            }
                                        }
                                    }
                                }
                            } else {
                                check = true;
                            }
                        }
                    }
                } else {
                    check = true;
                }
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            LOGGER.error("sendSMS : " + ex);
        }

        return check;
    }
    private static final int CONNECT_TIMEOUT = 15000;

    /**
     * Send SMS.
     */
    public void sendSMS() {
        LOGGER.debug("sendSMS Starts:");
        if (checkSmsPreferences(getSmsNo())) {
            TblSmslog tblSmslog = new TblSmslog();
            String excep = "";
            SmslogService smslogService = (SmslogService) AppContext.getSpringBean("SmslogService");
            try {
                StringBuffer sb = new StringBuffer();
                String newline = null;
                try {
                    newline = System.getProperty("line.separator");
                } catch (Exception e) {
                    LOGGER.error("sendSMS : " + e);
                    newline = "\n";
                }
                String body = "123";
                URL url = null;
                try {
                    url = new URL(XMLReader.getMessage("smsUrl") + "msg=" + getSmsBody() + "&to=" + getSmsNo());
                } catch (MalformedURLException ex) {
                    LOGGER.error("sendSMS : " + ex);
                    ex.printStackTrace();
                }
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setAllowUserInteraction(false); // you may not ask the user
                conn.setDoOutput(true); // we want to send things
                // the Content-type should be default, but we set it anyway
                conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
                // the content-length should not be necessary, but we're cautious
                conn.setRequestProperty("Content-length", getSmsBody()); /// about 160
                conn.setConnectTimeout(CONNECT_TIMEOUT);
                // get the output stream to POST our form data
                OutputStream rawOutStream = conn.getOutputStream();
                PrintWriter pw = new PrintWriter(rawOutStream);
                pw.print(getSmsBody()); // here we "send" our body!
                pw.flush();
                pw.close();
                // get the input stream for reading the reply
                // IMPORTANT! Your body will not get transmitted if you get the
                // InputStream before completely writing out your output first!
                InputStream rawInStream = conn.getInputStream();

                // get response
                BufferedReader rdr = new BufferedReader(new InputStreamReader(rawInStream));
                String line;
                while ((line = rdr.readLine()) != null) {
                    sb.append(line);
                    sb.append(newline);
                }
            } catch (IOException ex) {
                LOGGER.error("sendSMS :" + ex);
            }

            tblSmslog.setMobileNo(getSmsNo());
            tblSmslog.setSmstext(getSmsBody());
            tblSmslog.setSentDate(new java.util.Date());
            tblSmslog.setExceptionLog(excep);

            try {
                smslogService.addSmslog(tblSmslog);
            } catch (Exception ex) {
                LOGGER.error("sendSMS :" + ex);
            }
        }
        LOGGER.debug("sendSMS Ends:");
    }

    @Override
    public void run() {
        LOGGER.debug("run Starts:");
        LOGGER.debug("Thread ID = " + Thread.currentThread().getName() + "  Starting Time : " + new Date());
        try {
            if ("SMS".equals(sendBy)) {
                sendSMS();
            } else if ("EMAIL".equals(sendBy)) {
                sendEmail();
            }
        } catch (Exception e) {
            LOGGER.error("run :" + e);
        }
        LOGGER.debug("Message Sending thread completed." + "  Ending Time : " + new Date());
        LOGGER.debug("run Ends:");
    }

    public StringBuffer getRandomName() {
        LOGGER.debug("getRandomName Starts:");
        String str = new String("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        StringBuffer sb = new StringBuffer();
        try {
            Random r = new Random();
            int te = 0;
            for (int i = 1; i <= 7; i++) {
                te = r.nextInt(26);
                sb.append(str.charAt(te));
            }
        } catch (Exception e) {
            LOGGER.error("getRandomName :" + e);
        }
        LOGGER.debug("getRandomName Ends:");
        return sb;
    }
    
    public StringBuffer getOTP() {
        LOGGER.debug("getOTP Starts:");
        String str = new String("0123456789");
        StringBuffer sb = new StringBuffer();
        try {
            Random r = new Random();
            int te = 0;
            for (int i = 1; i <= 4; i++) {
                te = r.nextInt(10);
                sb.append(str.charAt(te));
            }
        } catch (Exception e) {
            LOGGER.error("getOTP :" + e);
        }
        LOGGER.debug("getOTP Ends:");
        return sb;
    }

    /**
     * @return the curUserId
     */
    public String getCurUserId() {
        return curUserId;
    }

    /**
     * @param curUserId the curUserId to set
     */
    public void setCurUserId(String curUserId) {
        this.curUserId = curUserId;
    }

    /**
     * @return the smsEventName
     */
    public String getSmsEventName() {
        return smsEventName;
    }

    /**
     * @param smsEventName the smsEventName to set
     */
    public void setSmsEventName(String smsEventName) {
        this.smsEventName = smsEventName;
    }
        private class SMTPAuthenticator extends Authenticator {

        private PasswordAuthentication authentication;

        public SMTPAuthenticator(String login, String password) {
            authentication = new PasswordAuthentication(login, password);
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return authentication;
        }

    }
}
