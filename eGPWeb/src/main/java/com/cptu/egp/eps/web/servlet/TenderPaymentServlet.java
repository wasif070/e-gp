/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.servicebean.CommonSearchServiceSrBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class TenderPaymentServlet extends HttpServlet {
    private TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
    private UserRegisterService  registerService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    private MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
   /**this servlet handles all the request for the postpaymentprocess.jsp,NoAissuedList.jsp,
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
   private static final Logger LOGGER = Logger.getLogger(TenderPaymentServlet.class);
      protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String logUserId = "0";
        int ContractId = 0;
        try {
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();}
            String userTypeId = "";
            if (request.getSession().getAttribute("userTypeId") != null) {
                    userTypeId = request.getSession().getAttribute("userTypeId").toString();
            }
                tenderCommonService.setLogUserId(logUserId);
                cmsConfigDateService.setLogUserId(logUserId);
                accPaymentService.setLogUserId(logUserId);
                commonservice.setUserId(logUserId);
                registerService.setUserId(logUserId);
                LOGGER.debug("processRequest : "+logUserId+" Starts");

                /* getting  data from database and displaying in NOAIssuedList.jsp jqgrid*/

            if (request.getParameter("action").equals("fetchNOAData")) {
                response.setContentType("text/xml;charset=UTF-8");                
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String userId= request.getParameter("userId");                                                
                String branchMakerId = request.getParameter("branchMakerId");
                String tenderId = request.getParameter("tenderId");
                String emailId = request.getParameter("emailId");
                String tendererNm = request.getParameter("tendererNm");

                int totalPages = 0;
                int totalCount = 0;

                CommonSearchDataMoreService objCommonSearchDataMoreImpl = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                objCommonSearchDataMoreImpl.setLogUserId(logUserId);
                List<SPCommonSearchDataMore> getList =
                        objCommonSearchDataMoreImpl.getCommonSearchData("getNOAIssuedListing", page, rows, branchMakerId, tenderId, emailId, tendererNm, null, null, null, null, null, null, null, null, null, null, null, null, null);

                if(getList.isEmpty()){
                    totalCount=0;
                    totalPages=0;
                } else {
                    totalCount = Integer.parseInt(getList.get(0).getFieldName12());
                    totalPages=Integer.parseInt(getList.get(0).getFieldName11());
                }

                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");
                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getList.size() + "</records>");
               //int srNo = 1;
                int rowId = 0;
                if(!getList.isEmpty()){
                   //for (SPCommonSearchData details : getList) {
                    for (SPCommonSearchDataMore details : getList) {
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[" + details.getFieldName13() + "]]></cell>");
                    //out.print("<cell><![CDATA[" + details.getFieldName3() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName5() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName6() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName7() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName8() + "]]></cell>");
                    out.print("<cell><![CDATA[" + details.getFieldName10() + "]]></cell>");

                    /*
                    if("Pending".equalsIgnoreCase(details.getFieldName10())){
                        out.print("<cell><![CDATA[<a href=''></a>]]>Pending</cell>");
                    } else {
                        out.print("<cell><![CDATA[<a href=''></a>]]>Paid</cell>");
                    }
                     */

                    if("Pending".equalsIgnoreCase(details.getFieldName10())){
                        // Payment is Pending
                       
                         if("cancelled".equalsIgnoreCase(details.getFieldName9())){
                             out.print("<cell><![CDATA[<a href='#' onclick='return cancelTender();'>Make Payment</a>]]></cell>");
                         }else{
                            if("No".equalsIgnoreCase(details.getFieldName9())){
                                // Performance Security Submission Date Lapsed (Over)
                                out.print("<cell><![CDATA[<a href='#' onclick='return DateOver();'>Make Payment</a>]]></cell>");
                            } else {
                                out.print("<cell><![CDATA[<a href='TenderPayment.jsp?uId="+details.getFieldName4()+"&tenderId="+details.getFieldName3()+"&lotId="+details.getFieldName15()+"&payTyp=ps&extId="+details.getFieldName2()+"&parentLink=958'>Make Payment</a>]]></cell>");
                            }
                         }
                        
                    } else {
                        // Payment already done
                        if("No".equalsIgnoreCase(details.getFieldName17())){
                            // If payment not verified                                                                                   
                            if("Yes".equalsIgnoreCase(details.getFieldName18())){
                                // Branch Maker of Same Branch has done the Payment
                                // Allow Payment Editing and View
                                out.print("<cell><![CDATA["
                                    +"<a href='TenderPayment.jsp?payId="+details.getFieldName16()+"&uId="+details.getFieldName4()+"&tenderId="+details.getFieldName3()+"&lotId="+details.getFieldName15()+"&payTyp=ps&extId="+details.getFieldName2()+"&action=edit'>Edit</a>"
                                    +"&nbsp;&nbsp;<a href='TenderPaymentDetails.jsp?payId="+details.getFieldName16()+"&uId="+details.getFieldName4()+"&tenderId="+details.getFieldName3()+"&lotId="+details.getFieldName15()+"&payTyp=ps&extId="+details.getFieldName2()+"'>View</a>"
                                    +"]]></cell>");
                            } else {
                                // Branch Maker is of Other Branch so just show Message
                              out.print("<cell><![CDATA[Payment already done. Verification Pending]]></cell>");
                            }

                        } else {
                            // If payment verified

                            if ("Yes".equalsIgnoreCase(details.getFieldName18())) {
                                // Branch Maker of Same Branch has done the Payment

                                // Check for Release Or Forfeit Request
                                List<SPTenderCommonData> lstPaymentActionRequest =
                                        tenderCommonService.returndata("getRequestActionFromPaymentId", details.getFieldName16(), null);

                                if (lstPaymentActionRequest.isEmpty()) {
                                    // If No Pending Request Exists
                                    // Show only View link
                                    out.print("<cell><![CDATA["
                                            + "<a href='TenderPaymentDetails.jsp?payId=" + details.getFieldName16() + "&uId=" + details.getFieldName4() + "&tenderId=" + details.getFieldName3() + "&lotId=" + details.getFieldName15() + "&payTyp=ps&extId=" + details.getFieldName2() + "'>View</a>"
                                            + "]]></cell>");
                                } else {
                                    // If there is a Pending Request
//                                    if ("Release".equalsIgnoreCase(lstPaymentActionRequest.get(0).getFieldName2())) {
//                                        // If Request is for Release Action
//                                        if (!"0".equalsIgnoreCase(lstPaymentActionRequest.get(0).getFieldName1())) {
//                                            out.print("<cell><![CDATA["
//                                                    + "<a href='TenderPaymentDetails.jsp?payId=" + details.getFieldName16() + "&uId=" + details.getFieldName4() + "&tenderId=" + details.getFieldName3() + "&lotId=" + details.getFieldName15() + "&payTyp=ps&extId=" + details.getFieldName2() + "'>View</a>"
//                                                    + "&nbsp;&nbsp;<a href='PostPaymentProcess.jsp?reqId=" + lstPaymentActionRequest.get(0).getFieldName1() + "&payId=" + details.getFieldName16() + "&uId=" + details.getFieldName4() + "&tenderId=" + details.getFieldName3() + "&lotId=" + details.getFieldName15() + "&payTyp=ps&'>" + lstPaymentActionRequest.get(0).getFieldName2() + "</a>"
//                                                    + "]]></cell>");
//                                        }
//                                    } else {
                                         // Show only View link
                                            out.print("<cell><![CDATA["
                                            + "<a href='TenderPaymentDetails.jsp?payId=" + details.getFieldName16() + "&uId=" + details.getFieldName4() + "&tenderId=" + details.getFieldName3() + "&lotId=" + details.getFieldName15() + "&payTyp=ps&extId=" + details.getFieldName2() + "'>View</a>"
                                            + "]]></cell>");
                                        
                                    //}

                                  
                                }
                                // Nullify List object
                                lstPaymentActionRequest = null;

                            } else {
                                out.print("<cell><![CDATA[Payment already done.]]></cell>");
                            }

                        }
                        
                    }

                    out.print("</row>");
                    //srNo++;
                    rowId++;
                }
               } else {
                    String noDataMsg = "No record found";
                    out.print("<row id='" + rowId + "'>");
                    out.print("<cell class=\"t-align-center\"><![CDATA[1]]></cell>");
                    //out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("<cell><![CDATA[" + noDataMsg + "]]></cell>");
                    out.print("</row>");
               }

                out.print("</rows>");

                //Nullify List object
                getList = null;

                // Nullify String objects
                rows = null;
                page = null;
                userId = null;
                branchMakerId = null;
                tenderId = null;
                emailId = null;
                tendererNm = null;

                /* getting  data from database and displaying in PostPaymentProcess.jsp jqgrid*/

            } else if (request.getParameter("action").equals("updatePayStatus")) {
                HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                String submit=request.getParameter("btnSubmit");
                String requestId=request.getParameter("reqId");
                String paymentId=request.getParameter("payId");
                String uId=request.getParameter("uId");
                String tenderRefNo=request.getParameter("tenderRefNo");
                String tenderId=request.getParameter("tenderId");
                String lotId=request.getParameter("lotId");
                String payTyp=request.getParameter("payTyp");
                String operation= request.getParameter("operation");
                String comments=request.getParameter("txtComments");
                String userId="";
                String strPartTransId="0";
                String pageNm="";
                String querString="";
                String path="";
                String msgTxt="";
                String referer = "";
                String auditObjectType=null;
                int auditObjectId=0;
                String auditAction=null;
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");  
                }

                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") == null) {
                    response.sendRedirect("SessionTimedOut.jsp");
                } else {userId = hs.getAttribute("userId").toString();}

                if (hs.getAttribute("govUserId") != null) {
                    strPartTransId = hs.getAttribute("govUserId").toString();
                }

                if (submit != null) {
                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                    CommonMsgChk commonMsgChk;

                    String dtXml = "";
                    String updStatus = "";
                    String statusXML = "";
                boolean sendMail = false;
                boolean isError = false;

                //  Dohatec Start
                boolean isIctTender = false;
                String[] payIdArray = new String[4];
                String[] reqIdArray = new String[4];
                int totalRegPayId = 0;
                int i = 0;
                List<SPCommonSearchDataMore> lstPerSecPayIDDetail = null;
                
                // Tender is ICT or not
                List<SPTenderCommonData> listICT = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                if(!listICT.isEmpty() && listICT.get(0).getFieldName1().equalsIgnoreCase("true")){
                    isIctTender = true;
                }
                if(isIctTender && "bg".equalsIgnoreCase(payTyp))
                {
                    CommonSearchDataMoreService objTSDetails = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    lstPerSecPayIDDetail = objTSDetails.geteGPData("getNewPerSecPaymentIDDetail", tenderId.toString(), lotId, uId);
                    System.out.print("Old Paymend id list size " + lstPerSecPayIDDetail.size());
                    if(!lstPerSecPayIDDetail.isEmpty())
                    {
                        totalRegPayId = lstPerSecPayIDDetail.size();
                        for(i = 0; i< lstPerSecPayIDDetail.size(); i++)
                        {
                            payIdArray[i] = lstPerSecPayIDDetail.get(i).getFieldName1();
                            System.out.print("Old Payment Id : " + payIdArray[i]);
                            List<SPTenderCommonData> lstPaymentActionRequest = tenderCommonService.returndata("getRequestActionFromPaymentId", payIdArray[i], null);
                            if (!lstPaymentActionRequest.isEmpty()) {
                                reqIdArray[i] = lstPaymentActionRequest.get(0).getFieldName1();
                                System.out.print("Req Id : " + reqIdArray[i]);
                            }
                        }
                    }
                }
                //  Dohatec End

                ContractId = c_ConsSrv.getContractId(Integer.parseInt(tenderId));
                if (operation.equalsIgnoreCase("release")) {
                    updStatus = "released";
                    sendMail = true;
                    
                    if ("ts".equalsIgnoreCase(payTyp))
                    {
                        auditAction = "Release Bid Security";
                        auditObjectId=Integer.parseInt(tenderId);
                        auditObjectType="tenderId";
                    }
                    else if ("ps".equalsIgnoreCase(payTyp))
                    {
                        auditAction = "Release Performance Security";
                        auditObjectId=Integer.parseInt(tenderId);
                        auditObjectType="tenderId";
                    }
                    else if ("bg".equalsIgnoreCase(payTyp))
                    {
                        auditAction = "Release New Performance Security";
                        auditObjectId=ContractId;
                        auditObjectType="contractId";
                    }
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")),auditObjectId,auditObjectType, EgpModule.Payment.getName(), auditAction, "");
                    
                } else if (operation.equalsIgnoreCase("cancel")) {
                    updStatus = "cancelled";
                    sendMail = true;
                } else if (operation.equalsIgnoreCase("hold")) {
                    updStatus = "on-hold";
                } else if (operation.equalsIgnoreCase("forfeit")) {
                    updStatus = "forfeited";
                    sendMail = true;
                    if ("ts".equalsIgnoreCase(payTyp))
                    {
                        auditAction = "Compensate Bid Security";
                        auditObjectId=Integer.parseInt(tenderId);
                        auditObjectType="tenderId";
                    }
                    else if ("ps".equalsIgnoreCase(payTyp))
                    {
                        auditAction = "Compensate Performance Security";
                        auditObjectId=Integer.parseInt(tenderId);
                        auditObjectType="tenderId";
                    }
                    else if ("bg".equalsIgnoreCase(payTyp))
                    {
                        auditAction = "Compensate New Performance Security";
                        auditObjectId=ContractId;
                        auditObjectType="contractId";
                    }
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")),auditObjectId, auditObjectType, EgpModule.Payment.getName(),auditAction, "");
                    
                }

                /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                    comments = handleSpecialChar.handleSpecialChar(comments);
                     /* END CODE TO HANDLE SPECIAL CHARACTERS  */

                String whereCodition="";
                if(isIctTender && "bg".equalsIgnoreCase(payTyp)) //  Added by Dohatec for ICT Tender
                {
                    for(i = 0; i < totalRegPayId; i++)
                    {
                        dtXml="comments='"+ comments + "', status='"+ updStatus + "'";
                        whereCodition="tenderPaymentId=" +  payIdArray[i];

                        try{

                            commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderPayment", dtXml, whereCodition).get(0);

                            if (commonMsgChk.getFlag().equals(true)) {

                                /* START : CODE TO INSERT INTO PAYMENT STATUS TABLE*/
                                statusXML="<tbl_TenderPaymentStatus tenderPaymentId=\"" +  payIdArray[i] + "\" paymentStatus=\"" + updStatus + "\" paymentStatusDt=\"" + format.format(new Date()) + "\" createdBy=\"" + userId + "\" comments=\"" + comments + "\" tenderPayRefId=\"" +  payIdArray[i] + "\" refPartTransId=\"" + strPartTransId + "\" />";
                                statusXML = "<root>" + statusXML + "</root>";


                                commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TenderPaymentStatus", statusXML, "").get(0);

                                if (commonMsgChk.getFlag().equals(true)) {
                                    if(reqIdArray[i]!=null && !"0".equalsIgnoreCase(reqIdArray[i])){
                                     // Action Request Case Starts
                                    /* START : CODE TO UPDATE PAYMENT ACTION REQUEST TABLE*/
                                        dtXml="actionStatus='completed', actionComments='"+ comments + "', actionCompDt=Getdate()";
                                        whereCodition="requestId=" + reqIdArray[i];

                                        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_PaymentActionRequest", dtXml, whereCodition).get(0);
                                    /* END : CODE TO UPDATE PAYMENT ACTION REQUEST TABLE*/

                                        if (commonMsgChk.getFlag().equals(true)) {
                                            msgTxt=updStatus;
                                            pageNm= "TenderPaymentDetails.jsp";
                                            querString= "payId="+paymentId+"&uId="+uId+"&tenderId="+tenderId+"&lotId="+lotId+"&payTyp=" + payTyp + "&msgId=" + msgTxt;

                                            path = request.getContextPath() + "/partner/" + pageNm + "?" + querString;
                                        } else {
                                            isError=true;
                                        }
                                    } // Action Request Case Ends
                                    else {
                                        // Direction Action Case
                                        msgTxt=updStatus;
                                        pageNm= "TenderPaymentDetails.jsp";
                                        querString= "payId="+paymentId+"&uId="+uId+"&tenderId="+tenderId+"&lotId="+lotId+"&payTyp=" + payTyp + "&msgId=" + msgTxt;

                                        path = request.getContextPath() + "/partner/" + pageNm + "?" + querString;
                                    }
                                } else {
                                    isError=true;
                                }
                            }
                        }catch(Exception e){
                         LOGGER.error("Error in Post Payment Process: "+e.toString());
                       }
                    }

                    try{
                        if(isError){
                        sendMail=false;
                        msgTxt="error";
                        pageNm=referer;
                        path = request.getContextPath() + "/partner/" + pageNm + "&msgId=" + msgTxt;
                     }

                     if(sendMail){
                        boolean mailSent=false;
                        String paymentFor="";
                        if("ts".equalsIgnoreCase(payTyp)){
                            paymentFor="Bid Security";
                        } else if("ps".equalsIgnoreCase(payTyp)){
                            paymentFor="Performance Security";
                        }

                        //mailSent=SendMail(updStatus, paymentFor, uId, tenderId, tenderRefNo, lotId);
                        if("ts".equalsIgnoreCase(payTyp)){
                            SendMail(updStatus, paymentFor, uId, tenderId, tenderRefNo, lotId);
                        }else{
                            if (operation.equalsIgnoreCase("forfeit")) {
                                sendMailForForefietRequest(tenderId,lotId,paymentId,logUserId,Integer.parseInt(userTypeId),payTyp);
                            }else{
                                sendMailForReleaseRequest(tenderId,lotId,paymentId,logUserId,Integer.parseInt(userTypeId),payTyp);
                            }
                        }
                        paymentFor = null;
                     }

                     response.sendRedirect(path);

                    }catch(Exception ex){
                        LOGGER.error("Error in Post Payment Process (email) : "+ex.toString());
                    }
                }
                else    //  NCT
                {
                    dtXml="comments='"+ comments + "', status='"+ updStatus + "'";
                    whereCodition="tenderPaymentId=" + paymentId;

                    try{

                    commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderPayment", dtXml, whereCodition).get(0);

                    if (commonMsgChk.getFlag().equals(true)) {

                        /* START : CODE TO INSERT INTO PAYMENT STATUS TABLE*/
                        statusXML="<tbl_TenderPaymentStatus tenderPaymentId=\"" + paymentId + "\" paymentStatus=\"" + updStatus + "\" paymentStatusDt=\"" + format.format(new Date()) + "\" createdBy=\"" + userId + "\" comments=\"" + comments + "\" tenderPayRefId=\"" + paymentId + "\" refPartTransId=\"" + strPartTransId + "\" />";
                        statusXML = "<root>" + statusXML + "</root>";


                        commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TenderPaymentStatus", statusXML, "").get(0);

                        if (commonMsgChk.getFlag().equals(true)) {
                            if(requestId!=null && !"0".equalsIgnoreCase(requestId)){
                             // Action Request Case Starts
                            /* START : CODE TO UPDATE PAYMENT ACTION REQUEST TABLE*/
                                dtXml="actionStatus='completed', actionComments='"+ comments + "', actionCompDt=Getdate()";
                                whereCodition="requestId=" + requestId;

                                commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_PaymentActionRequest", dtXml, whereCodition).get(0);
                            /* END : CODE TO UPDATE PAYMENT ACTION REQUEST TABLE*/

                                if (commonMsgChk.getFlag().equals(true)) {
                                    msgTxt=updStatus;
                                    pageNm= "TenderPaymentDetails.jsp";
                                    querString= "payId="+paymentId+"&uId="+uId+"&tenderId="+tenderId+"&lotId="+lotId+"&payTyp=" + payTyp + "&msgId=" + msgTxt;

                                    path = request.getContextPath() + "/partner/" + pageNm + "?" + querString;
                                } else {
                                    isError=true;
                                }
                            } // Action Request Case Ends
                            else {
                                // Direction Action Case
                                msgTxt=updStatus;
                                pageNm= "TenderPaymentDetails.jsp";
                                querString= "payId="+paymentId+"&uId="+uId+"&tenderId="+tenderId+"&lotId="+lotId+"&payTyp=" + payTyp + "&msgId=" + msgTxt;

                                path = request.getContextPath() + "/partner/" + pageNm + "?" + querString;
                            }

                        } else {
                            isError=true;
                        }

                        if(isError){
                            sendMail=false;
                            msgTxt="error";
                            pageNm=referer;
                            path = request.getContextPath() + "/partner/" + pageNm + "&msgId=" + msgTxt;
                        }

                        if(sendMail){
                            boolean mailSent=false;
                            String paymentFor="";
                            if("ts".equalsIgnoreCase(payTyp)){
                                paymentFor="Bid Security";
                            } else if("ps".equalsIgnoreCase(payTyp)){
                                paymentFor="Performance Security";
                            }

                            //mailSent=SendMail(updStatus, paymentFor, uId, tenderId, tenderRefNo, lotId);
                            if("ts".equalsIgnoreCase(payTyp)){
                                SendMail(updStatus, paymentFor, uId, tenderId, tenderRefNo, lotId);
                            }else{
                                if (operation.equalsIgnoreCase("forfeit")) {
                                    sendMailForForefietRequest(tenderId,lotId,paymentId,logUserId,Integer.parseInt(userTypeId),payTyp);
                                }else{
                                    sendMailForReleaseRequest(tenderId,lotId,paymentId,logUserId,Integer.parseInt(userTypeId),payTyp);
                                }
                            }
                            paymentFor = null;
                        }

                        response.sendRedirect(path);
                    }

                    }catch(Exception e){
                     LOGGER.error("Error in Post Payment Process: "+e.toString());
                   }
                }
            }
                // Nullify String objects
                submit = null;
                requestId = null;
                paymentId = null;
                uId = null;
                tenderRefNo = null;
                tenderId = null;
                lotId = null;
                payTyp = null;
                operation = null;
                comments = null;
                userId = null;
                pageNm = null;
                querString = null;
                path = null;
                msgTxt = null;
                referer = null;

            }
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
    }

    /**
     * this method is for sending the mail
     * @param payStatus
     * @param paymentFor
     * @param payUserId
     * @param tenderId
     * @param TenderRefNo
     * @return boolean true-if send successfully , false - if failed
     * @throws Exception
     */

      /** send mail notification to maker/tenderer on forfeiting/releasing the ps/bg/ts by PE */
       public boolean SendMail(String payStatus, String paymentFor, String payUserId, String tenderId, String TenderRefNo, String lotId) throws Exception {

        boolean isSent=false;        
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        LOGGER.debug("SendMail : Starts");
        try{
            String mailSubject = "";
            String actionTaken = "";
            String peOfficerUserId = "";
            String tendererCompanyNm = "";
            String strLotNo="";
            String strLotDescription="";
            String strPEOffice = "";

             if("released".equalsIgnoreCase(payStatus)){
                //mailSubject="Release of "+ paymentFor +" on e-GP";
                 mailSubject="e-GP: " + paymentFor +" Released";
                actionTaken="Released";
             } else if("forfeited".equalsIgnoreCase(payStatus)){
               // mailSubject="Forfeit of "+ paymentFor +" on e-GP";
                mailSubject="e-GP: " + paymentFor +" Forfeited";
                actionTaken="Forfeited";
             }
             else if("extended".equalsIgnoreCase(payStatus)){
                mailSubject="Extension of "+ paymentFor +" on e-GP";
                actionTaken="Extended";
             }
              else if("cancelled".equalsIgnoreCase(payStatus)){
                mailSubject="Cancellation of "+ paymentFor +" on e-GP";
                actionTaken="Cancelled";
             }

            if(lotId!=null && lotId!="" && lotId!="null"){
                // Get Lot No and Lot Description
                for (SPTenderCommonData commonTenderDetails : tenderCommonService.returndata("gettenderlotbytenderidForPayment", tenderId, "ts")) {

                    if (lotId.equalsIgnoreCase(commonTenderDetails.getFieldName3())) {
                        strLotNo = commonTenderDetails.getFieldName1();
                        strLotDescription = commonTenderDetails.getFieldName2();
                    }
                }
            }


             /* START : CODE TO SEND MAIL */
             List<SPTenderCommonData> lstOfficerUserId =
                     tenderCommonService.returndata("getPEOfficerUserIdfromTenderId",tenderId,null);

             if (!lstOfficerUserId.isEmpty()) {
             peOfficerUserId=lstOfficerUserId.get(0).getFieldName1();
             strPEOffice = lstOfficerUserId.get(0).getFieldName3(); // Get PE Office Name
             }


             List<SPTenderCommonData> peOfficerEmail =
                     tenderCommonService.returndata("getEmailIdfromUserId",peOfficerUserId,null);

             List<SPTenderCommonData> lstTendererCompany =
                     tenderCommonService.returndata("getTendererCompanyName",payUserId,null);
             tendererCompanyNm = lstTendererCompany.get(0).getFieldName1();

            List<SPTenderCommonData> emails = 
                    tenderCommonService.returndata("getEmailIdfromUserId",payUserId,null);
            
            UserRegisterService userRegisterService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
            String [] mail={emails.get(0).getFieldName1(), peOfficerEmail.get(0).getFieldName1()};
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String mailText = mailContentUtility.contBankUser_ReleaseForfeit(tenderId,TenderRefNo, strLotNo, strLotDescription, paymentFor, actionTaken, tendererCompanyNm, strPEOffice);
            sendMessageUtil.setEmailTo(mail);
            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
            sendMessageUtil.setEmailSub(HandleSpecialChar.handleSpecialChar(mailSubject));
            sendMessageUtil.setEmailMessage(mailText);
            // Sent to Bidder
            userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contBankUser_ReleaseForfeit(tenderId,TenderRefNo, strLotNo, strLotDescription, paymentFor, actionTaken, tendererCompanyNm, strPEOffice));
            // Sent to PE
            userRegisterService.contentAdmMsgBox(mail[1], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contBankUser_ReleaseForfeit(tenderId,TenderRefNo, strLotNo, strLotDescription, paymentFor, actionTaken, tendererCompanyNm, strPEOffice));
            sendMessageUtil.sendEmail();

            sendMessageUtil=null;
            mailContentUtility=null;
            mail=null;
            msgBoxContentUtility=null;
            /* END : CODE TO SEND MAIL */

            //Nullify List objects
            emails = null;
            lstTendererCompany = null;
            peOfficerEmail = null;
            lstOfficerUserId = null;
            // Nullify String objects
            mailSubject = null;
            actionTaken = null;
            peOfficerUserId = null;
            tendererCompanyNm = null;

            isSent = true;
            LOGGER.debug("SendMail : Ends");
        } catch(Exception e) {
            LOGGER.error("Error in getBranchMembers : "+e.toString());
            isSent = false;
        }

        return isSent;
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** send mail notification to maker/tenderer on releasing the ps/bg/ts by PE */
    private boolean sendMailForReleaseRequest(String tenderId,String lotId,String tenderPaymentId,String logUserId,int userTypeId,String payType)
    {
        boolean flag = false;
        
        //Code Comment Start by Proshanto Kumar Saha,Dohatec
        //List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId),Integer.parseInt(lotId));
        //Code Comment End by Proshanto Kumar Saha,Dohatec

        //Code Start by Proshanto Kumar Saha,Dohatec
        //This portion is used to collect GetCinfoBar with or without contract sign by checking contract sign is not done,performance security is given and tender validity is over.
        String mailText=null;
        List<Object[]> GetCinfoBar=null;
        boolean checkStatus = false;
        CommonSearchServiceSrBean objCommonSearchServiceSrBean=new CommonSearchServiceSrBean();
        checkStatus=objCommonSearchServiceSrBean.getCSignTValidityAndPSecurityStatus(tenderId, logUserId);
        if(checkStatus)
        {
        GetCinfoBar = cmsConfigDateService.getContractInfoBarWithoutCSign(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        }
        else
        {
        GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        }
        //Code End by Proshanto Kumar Saha,Dohatec

        Object[] obj = null;
        if(!GetCinfoBar.isEmpty())
        {
            obj = GetCinfoBar.get(0);
            String BankName = "";
            BankName = accPaymentService.getBankName(tenderPaymentId);
            String peEmailId = "";
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if(!peEmailObj.isEmpty())
            {
                Object[] objj = peEmailObj.get(0);
                peEmailId = objj[0].toString();
            }
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object[]> AcEmailId = accPaymentService.getAccountantEmailId(Integer.parseInt(tenderId));
            String[] strTo = null;
            int emailIndex = 0;
            
            strTo = new String[AcEmailId.size()+2];
            for(Object[] emailId : AcEmailId){
                strTo[emailIndex] = emailId[4].toString();
                emailIndex++;
            }
            strTo[emailIndex] = obj[9].toString();
            strTo[emailIndex+1] = peEmailId;
            
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailFrom(strFrom);

            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String emailId[] = new String[1];
            String strfor = "Performance Security";
            String mailSubject = "e-GP: Performance Security Released";
            if("ts".equalsIgnoreCase(payType)){
                mailSubject = "e-GP: Request for Releasing Bid Security Released";
                strfor = "Bid Security";
            }
            
            String mobileno = "";
            for(int i=0; i<strTo.length; i++)
            {
                emailId[0] = strTo[i];

                //Code Comment Start by Proshanto Kumar Saha,Dohatec
                //registerService.contentAdmMsgBox(emailId[0],strFrom, mailSubject, msgBoxContentUtility.mailForReleaseRequest(c_isPhysicalPrComplete,BankName,obj,accPaymentService.getUserTypeId(emailId[0]),Integer.toString(userTypeId),accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor));
                // String mailText = mailContentUtility.mailForReleaseRequest(c_isPhysicalPrComplete,BankName,obj,accPaymentService.getUserTypeId(emailId[0]),Integer.toString(userTypeId),accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor);
                //Code Comment End by Proshanto Kumar Saha,Dohatec
                
                //Code Start By Proshanto Kumar Saha,Dohatec
                //This portion is used to add mailmessage and set mailtext with or without contract sign by checking contract sign is not done,performance security is given and tender validity is over.
                if(checkStatus)
                {
                registerService.contentAdmMsgBox(emailId[0],strFrom, mailSubject, msgBoxContentUtility.mailForReleaseRequestWithoutConSign(c_isPhysicalPrComplete,BankName,obj,accPaymentService.getUserTypeId(emailId[0]),Integer.toString(userTypeId),accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor,tenderId));
                mailText = mailContentUtility.mailForReleaseRequestWithoutConSign(c_isPhysicalPrComplete,BankName,obj,accPaymentService.getUserTypeId(emailId[0]),Integer.toString(userTypeId),accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor,tenderId);
                }
                else
                {
                registerService.contentAdmMsgBox(emailId[0],strFrom, mailSubject, msgBoxContentUtility.mailForReleaseRequest(c_isPhysicalPrComplete,BankName,obj,accPaymentService.getUserTypeId(emailId[0]),Integer.toString(userTypeId),accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor));
                mailText = mailContentUtility.mailForReleaseRequest(c_isPhysicalPrComplete,BankName,obj,accPaymentService.getUserTypeId(emailId[0]),Integer.toString(userTypeId),accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor);
                }
                //Code End By Proshanto Kumar Saha,Dohatec

                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.setEmailSub(mailSubject);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0])))
                {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }                
                else
                {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                sendMessageUtil.setSmsNo(mobileno);
                StringBuilder sb = new StringBuilder(); 
                sb.append("Dear User,%0AThis is to inform you that Financial Institute "+BankName+" has released "+strfor+" ");

                //Code Comment Start by Proshanto Kumar Saha,Dohatec
                //sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                //Code Comment End by Proshanto Kumar Saha,Dohatec

                //Code Start By Proshanto Kumar Saha,Dohatec
                //This portion is used to sent message in mobile with tenderId or with contract no by checking contract sign is not done,performance security is given and tender validity is over.
                if(checkStatus)
                {
                sb.append("for Tender ID :"+tenderId+" ");
                }
                else
                {
                sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                }
                //Code End by  Proshanto Kumar Saha,Dohatec
                
                sb.append("%0AThanks,%0Ae-GP System");
                sendMessageUtil.setSmsBody(sb.toString());
                sendMessageUtil.sendSMS();
            }
            strTo =null;
            sendMessageUtil=null;
            mailContentUtility=null;
            msgBoxContentUtility=null;
        }
        return flag;
    }

    /** send mail notification to maker/tenderer on forfeiting the ps/bg/ts by PE */
    private boolean sendMailForForefietRequest(String tenderId,String lotId,String tenderPaymentId,String logUserId,int userTypeId,String payType)
    {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId),Integer.parseInt(lotId));
        Object[] obj = null;
        if(!GetCinfoBar.isEmpty())
        {
            obj = GetCinfoBar.get(0);
            String BankName = "";
            BankName = accPaymentService.getBankName(tenderPaymentId);
            String peEmailId = "";
            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
            if(!peEmailObj.isEmpty())
            {
                Object[] objj = peEmailObj.get(0);
                peEmailId = objj[0].toString();
            }
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object[]> AcEmailId = accPaymentService.getAccountantEmailId(Integer.parseInt(tenderId));
            String[] strTo = null;
            int emailIndex = 0;

            strTo = new String[AcEmailId.size()+2];
            for(Object[] emailId : AcEmailId){
                strTo[emailIndex] = emailId[4].toString();
                emailIndex++;
            }
            strTo[emailIndex] = obj[9].toString();
            strTo[emailIndex+1] = peEmailId;

            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailFrom(strFrom);

            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String emailId[] = new String[1];
            String mobileno = "";
            String strfor = "Performance Security";
            String strtyp = "Compensated Performance Security";
            String mailSubject = "e-GP: Compensation of Performance Security or New Performance Security";
            if("ts".equalsIgnoreCase(payType)){
                mailSubject = "e-GP: Bid Security Forefieted";
                strfor = "Bid Security";
                strtyp = "Forfeit of Performance Security";
            }
            for(int i=0; i<strTo.length; i++)
            {
                emailId[0] = strTo[i];
                registerService.contentAdmMsgBox(emailId[0],strFrom, mailSubject, msgBoxContentUtility.mailForForfeitRequest(c_isPhysicalPrComplete,BankName,obj,accPaymentService.getUserTypeId(emailId[0]),Integer.toString(userTypeId),accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor,strtyp));
                String mailText = mailContentUtility.mailForForfeitRequest(c_isPhysicalPrComplete,BankName,obj,accPaymentService.getUserTypeId(emailId[0]),Integer.toString(userTypeId),accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])),strfor,strtyp);
                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.setEmailSub(mailSubject);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0])))
                {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                else
                {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                sendMessageUtil.setSmsNo(mobileno);
                StringBuilder sb = new StringBuilder(); 
                sb.append("Dear User,%0AThis is to inform you that Financial Institute "+BankName+" has "+strfor+" ");
                sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                sb.append("%0AThanks,%0Ae-GP System");
                sendMessageUtil.setSmsBody(sb.toString());
                sendMessageUtil.sendSMS();
            }
            strTo =null;
            sendMessageUtil=null;
            mailContentUtility=null;
            msgBoxContentUtility=null;
        }
        return flag;
    }
}
