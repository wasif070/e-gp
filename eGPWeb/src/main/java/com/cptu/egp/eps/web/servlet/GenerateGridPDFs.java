/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.web.utility.XMLReader;
import com.lowagie.text.DocumentException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jsoup.Jsoup;
import org.springframework.util.FileSystemUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.jsoup.nodes.Document;

/**
 *
 * @author taher.tinwala
 */
public class GenerateGridPDFs extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try {
            String pdfBuffer = request.getParameter("pdfBuffer");
            String strDriveLetter = XMLReader.getMessage("driveLatter");
            String path;
            path = strDriveLetter + "/eGP/PDF/";
            String fileName = request.getParameter("fileName");
            String id = request.getParameter("id");
            //String url = request.getHeader("referer");
           // url = url.substring(0, url.indexOf("/", 10));
            //http://localhost:8080/officer/DebarmentListing.jsp
            if(pdfBuffer!=null && fileName!=null && id!=null){
                reportGeneration(pdfBuffer,fileName, path, id ,response);
            }else{
                System.out.println("pdfBuffer,fileName or id is either null");
                response.sendRedirect(request.getHeader("referer"));
            }
            //reportGeneration(url + request.getContextPath() + "/GridPdfs.jsp", "pdfBuffer=" + URLEncoder.encode(pdfBuffer, "UTF-8"), fileName, path, id, response);
        } catch (Exception e) {
            System.out.println("Exception in GenerateGridPDFs : " + e);
        }
    }

    public void reportGeneration(String pdfBuffer,String fileName,String path, String id,HttpServletResponse response) throws IOException, DocumentException {
        
        pdfBuffer = pdfBuffer.replaceAll("<p>", "").replaceAll("</p>", "");
        pdfBuffer = pdfBuffer.replaceAll("<br>", "<br/>").replaceAll("&", "&#38;");
        
        
        String folderName = "Report";

        File destFolder = new File(path+"\\" + folderName);
        if (destFolder.exists() == false) {
            destFolder.mkdirs();
        }

        File destFolder2 = new File(path+"\\" + folderName + "\\" + id);
        if (destFolder2.exists() == false) {
            destFolder2.mkdirs();
        }
        File file = new File(path + "\\" + folderName + "\\" + id + "\\" + fileName + ".pdf");
        boolean bool = file.exists() ? FileSystemUtils.deleteRecursively(file) : true;
        ServletOutputStream outputStream = response.getOutputStream();
        FileOutputStream fos = new FileOutputStream(file);
        ITextRenderer renderer = new ITextRenderer();
        StringBuilder htmls = new StringBuilder();
        htmls.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        htmls.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
        htmls.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
        htmls.append("<head><style type=\"text/css\">");
        htmls.append(" @page { size: 17in 22in; margin: 1in; @bottom-right { content: \"Page \" counter(page) \" of \" counter(pages); } }");
        htmls.append(".pageHead_1 {background:url(../images/horDotLine_1.gif) left bottom repeat-x; padding-bottom: 10px; font-size:16px; font-weight:bold; color:#FF9326;}");
        htmls.append(".formStyle_1 {color:#6E6E6E;line-height:19px;font-size:14px;}");
        htmls.append("table.tableList_1 {color:#333; border-collapse:collapse; width : 100%;}");
        htmls.append("table.tableList_1 th {border:1px solid #FF9326; font-size:12px;  text-align: center; vertical-align: middle; padding:5px; overflow: hidden; border-bottom:2px solid #FF9326; background:#f0e68c;}");
        htmls.append("table.tableList_1 td {border:1px solid #FF9326; font-size:12px;  text-align: center; vertical-align: middle; padding:5px; overflow: hidden;}");
        htmls.append("</style></head>");
        htmls.append("<body>").append(pdfBuffer).append(" </body></html>");        
        //System.out.println("pdfBuffer : "+pdfBuffer);
        Document document = Jsoup.parse(htmls.toString());
        renderer.setDocumentFromString(document.html());
        renderer.layout();
        response.setContentType("application/octet-stream ");
        response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + ".pdf\"");
        renderer.createPDF(outputStream);
        renderer.createPDF(fos);
        fos.flush();
        fos.close();
        outputStream.flush();
        outputStream.close();
    }

//    private void reportGeneration(String reqURL, String reqQuery, String fileName, String path, String id, HttpServletResponse response) throws Exception {
//        boolean flag = false;
//        String folderName = "Report";
//        GenreatePdfCmd genreatePdfCmd = new GenreatePdfCmd();
//        File file = new File(path + "\\" + folderName + "\\" + id + "\\" + fileName + ".pdf");
//        boolean bool = file.exists() ? FileSystemUtils.deleteRecursively(file) : true;
//        if (!file.exists()) {
//            flag = genreatePdfCmd.genrateCmd(reqURL, reqQuery, folderName, id, fileName);
//        }
//        InputStream fis = new FileInputStream(file);
//        byte[] buf = new byte[(int) file.length()];
//        int offset = 0;
//        int numRead = 0;
//        while ((offset < buf.length)
//                && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
//
//            offset += numRead;
//
//        }
//        fis.close();
//        response.setContentType("application/octet-stream");
//        response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName() + "\"");
//        ServletOutputStream outputStream = response.getOutputStream();
//        outputStream.write(buf);
//        outputStream.flush();
//        outputStream.close();
//    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
