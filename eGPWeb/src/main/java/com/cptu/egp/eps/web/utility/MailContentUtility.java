
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails;
import com.cptu.egp.eps.model.table.TblCompanyDocuments;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblMandatoryDoc;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import com.cptu.egp.eps.service.serviceimpl.ContentAdminService;
import com.cptu.egp.eps.service.serviceinterface.TenderBidService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.databean.OnlinePaymentDtBean;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class MailContentUtility {

    static final Logger LOGGER = Logger.getLogger(MsgBoxContentUtility.class);
    public static final String URL = XMLReader.getMessage("url");
    String s_forCss = ".formStyle_1 {color:#6E6E6E; line-height:19px;} "
            + ".formStyle_1 .ff {font-weight:bold; vertical-align:top;}"
            + ".formStyle_1 td{vertical-align:top;}";

    /**
     * The user who has accessed the reset password functionality
     *
     * @param verificationCode
     * @return
     */
    public List getResetMailContent(String verificationCode) {
        LOGGER.debug("getResetMailContent Starts:");
        List resetContent = new ArrayList();
        try {
            resetContent.add("e-GP: Reset your password");
            resetContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br /><br /><span>"
                    + "Please perform below mentioned steps to reset your password:<br/><ol>"
                    + "<li>To reset your password, click on the below link URL:<br/>"
                    + "<a href=\"" + XMLReader.getMessage("url") + "VerifyCode.jsp\">" + XMLReader.getMessage("url") + "VerifyCode.jsp</a></li><br />"
                    + "<li>You will be redirected to a page, enter the secret code mentioned below and click on submit."
                    + "<br /><b>Verification Code :</b><span style=\"color : red\">  " + verificationCode + "</span><br/></li>"
                    + "<li>On successful verification of secret code, you will be redirected to the \"Change Password Screen\".</li>"
                    + "<li>Fill up the mandatory details and click on submit to proceed further.</li></ol><br/></span>"
                    + "Warm Regards,<br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)<br/>"
                    + "Department of National Properties<br/>"
                    + "Ministry of Finance .</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getResetMailContent :" + e);
        }
        LOGGER.debug("getResetMailContent Ends:");
        return resetContent;
    }

    public List getLoginMailContent(TblLoginMaster loginMaster, String hashCode, String url) {

        LOGGER.debug("getLoginMailContent Starts:");
        List loginContent = new ArrayList();
        String regType = null;
        try {
            if (loginMaster.getRegistrationType().equals("contractor")) {
                regType = "Bidder / Consultant";
            }
            if (loginMaster.getRegistrationType().equals("individualconsultant")) {
                regType = "Individual Consultant";
            }
            if (loginMaster.getRegistrationType().equals("govtundertaking")) {
                regType = "Government owned Enterprise";
            }
            if (loginMaster.getRegistrationType().equals("media")) {
                regType = "Media";
            }

            loginContent.add("e-GP System:  Bidder Registration - Email Verification");
            loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<strong>If you've received this mail in error, it's likely that another Bidder entered your email address (ID) by "
                    + "mistake, while trying to register with Electronic Government Procurement (e-GP) System. If you didn't initiate the request to register, you "
                    + "don't need to take any further action and can safely <u>disregard the following message.</u></strong><br /><br />"
                    + "Dear Bidder," + "<br/><br/><span>"
                    + "Welcome to the Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan.<br/><br/>"
                    + "This Email has been sent to you as a part of your registration process. There are three (3) stages of Registration, for your information.<br/><br/>"
                    + "i) Bidder e-mail verification,<br/><br/>"
                    /*hide for free payment
                    + "ii) Registration fee payment at Bank,<br/><br/>" */
                    + "ii) Profile and mandatory documents submission, and<br/><br/>"
                    + "iii) Post verification of your uploaded credential documents by authorized GPPMD Office and approval/rejection of your registration.<br/><br/>"
                    + "This e-mail is sent to you in response to your submission for the first stage of Registration process already initiated by you for registering with the " + XMLReader.getMessage("url") + "<br/><br/>"
                    + "Please click on the link given below within <u>" + XMLReader.getMessage("FinalSubMsg") + " to verify your e-mail account</u>, and enter your e-mail "
                    + "address (ID), Password and Verification Code provided below.  After entering these information, you will "
                    + "be redirected to the detail profile entry page of e-GP System:<br />  <a href=\"" + XMLReader.getMessage("url") + "EmailVerification.jsp\">" + XMLReader.getMessage("url") + "EmailVerification.jsp</a><br/><br/>"
                    + "If clicking the link above doesn't work, please copy and paste the URL<br/>(" + XMLReader.getMessage("url") + "EmailVerification.jsp)  in a new browser window.<br/><br/>"
                    //+ "Below are the login details:<br/>"
                    + "<br/><table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'><tr><td style='padding-left:5px;padding-right:5px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginMaster.getEmailId()
                    + "<br/>&nbsp;<b>Password:</b> (Password is not stored in the e-GP data base. It is your obligation to keep<br/>&nbsp;password secret and not to reveal to anyone)"
                    // + "<br/><b>Hint Answer:</b>&nbsp;&nbsp;" + loginMaster.getHintAnswer()
                    + "<br/>&nbsp;<b>Verification Code:</b>&nbsp;&nbsp;" + hashCode + "</td></tr></table><br/><br/>"
                    + "If you fail to confirm your registration email within " + XMLReader.getMessage("FinalSubMsg") + " from time of submission of registration request on e-GP System, your pending e-mail verification record will "
                    + "be removed from e-GP system and you will need to re-register.<br/><br/><br/>"
                    + "<strong>You have submitted the following information:</strong><br/><br/>"
                    + "<b>e-mail ID:</b>&nbsp;&nbsp;" + loginMaster.getEmailId()
                    //+ "<br/><b>Password:</b>&nbsp;&nbsp;" + loginId//tt
                    + "<br/><b>Hint Question:</b>&nbsp;&nbsp;" + loginMaster.getHintQuestion()
                    + "<br/><b>Hint Answer:</b>&nbsp;&nbsp;" + loginMaster.getHintAnswer()
                    + "<br/><b>Nationality:</b>&nbsp;&nbsp;" + loginMaster.getNationality()//tt
                    + "<br/><b>Registration Type:</b>&nbsp;&nbsp;" + regType//tt
                    + "<br/><b>Country of Business: </b>&nbsp;&nbsp;" + loginMaster.getBusinessCountryName()//tt
                    + "<br/><br/><strong>And you have accepted the Terms & Conditions:</strong> I have read, understood and accepted the Terms and Conditions.<br/><br/>"
                    + "Please <a href=\"" + XMLReader.getMessage("url") + "/TermsNConditions.jsp\" target=\"_blank\">click here</a> for the Terms and Conditions<br/><br/>"
                    /* Hide 
                    + "<strong>Important links:</strong><br/>"
                    + "<i><a href=\"" + XMLReader.getMessage("url") + "help/manuals/eGP_NewUserRegManual_English.pdf\" target=\"_blank\">User Manual for Bidders and Consultants</a><br/>"
                    + "<a href=\"" + XMLReader.getMessage("url") + "help/guidelines/eGP_Guidelines.pdf\" target=\"_blank\">e-GP Guidelines</a><br/>"
                    + "<a href=\"" + XMLReader.getMessage("url") + "MemScheduleBank.jsp\" target=\"_blank\">List of Member Scheduled Banks</a><br/>"
                    + "<a href=\"" + XMLReader.getMessage("url") + "MandRegDoc.jsp\" target=\"_blank\">List of Mandatory Documents for Registration</a><br/>"
                    + "<a href=\"" + XMLReader.getMessage("url") + "TermsNConditions.jsp\" target=\"_blank\">Terms and Conditions</a><br/>"
                    + "<a href=\"" + XMLReader.getMessage("url") + "PrivacyPolicy.jsp\" target=\"_blank\">Disclaimer and Privacy Policy</a><br/><br/></i>" */
                    + "Thank you for using Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br/>"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    /*+"<strong>Thank you for using Electronic Government Procurement (e-GP) System.</strong><br/><br/>"
                    +"Government Procurement and Property Management Division (GPPMD), Ministry of Finance, Box 116,<br/>"
                    +"Thimphu, Bhutan <strong>Tel: </strong>02 - 336962, E-mail: <a href=\"mailto:info.bhutan@dohatec.com.bd\">info.bhutan@dohatec.com.bd</a>"
                    + "<br/>Please click on below link within 24 hours to verify your account  by entering your e-mail ID, Password and above mentioned Verification Code:"
                    + "<br/><a href=\"http://pppd.dohatec.com.bd:8090 /EmailVerification.jsp\">http://pppd.dohatec.com.bd:8090 /EmailVerification.jsp</a><br/><br/>"
                    + "Important Notes:<ol>"
                    + "<li>Please also note that Profile which is not verified within 24 hours,will be removed automatically. Final submission of the profile must be done within 30 days from the date of Email verification.</li></ol><br/><br/></span>"
                    + "Warm Regards,<br/>"
                    + "<b>eGP Support Team.</b><br/><br/>"
                    + "<span style=\"color : red\"><b>To ensure you receive future emails, please add <a href=\"mailto:auto-email@pppd.gov.bt\">auto-email@pppd.gov.bt</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b>+"</span>"*/
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list\"</span>"
                    // + "address of procuring agencies to your Safe Sender list.\"</b>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getLoginMailContent :" + e);

        }
        LOGGER.debug("getLoginMailContent Ends:");
        return loginContent;
    }
    
    public List getOtpMailContent(String event, String tenderId, String otp) {

        LOGGER.debug("getOtpMailContent Starts:");
        List otpContent = new ArrayList();
        if(event.equalsIgnoreCase("withdrawal"))
        {
            event = "Withdrawal / Modification";
        }
        else
        {
            event = "Final Submission";
        }
        try {
            otpContent.add("e-GP System: One Time Password (OTP) for "+event);
            otpContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>OTP</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<strong>If you've received this mail in error,"
                    + " please ignore.</strong><br /><br />"
                    + "Dear Bidder," + "<br/><br/><span>"
                    + "Your One Time Password (OTP) for "+event+" of <strong>tender ID: "+tenderId+"</strong> is: <span style='color : red'><strong>"+otp+"</strong></span>.<br/>"
                    + "Please note that the One-Time Password remains valid for only <strong>5 minutes.</strong><br/><br/>"
                    + "For security reason, you should not share this password with anyone.<br/><br/>"
                    + "Thank you for using Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list\"</span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getOtpMailContent :" + e);

        }
        LOGGER.debug("getOtpMailContent Ends:");
        return otpContent;
    }

    /**
     * Register Organization Admin Organization Admin User whose e-mail ID is
     * registered
     *
     * @param loginId
     * @param password
     * @return List
     */
    public List getGovtMailContent(String loginId, String password) // for Organization Admin
    {
        LOGGER.debug("getGovtMailContent Starts:");
        List loginContent = new ArrayList();
        try {
            loginContent.add("e-GP: Your Account Details");
            loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "Your Organization's profile has been created in the e-GP System. "
                    + "Following are the login credentials:<br/><br/>"
                    // + "Below are the login details:<br/>"
                    + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                    + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                    + "<br/>&nbsp;<b>Password:</b> " + password
                    + "<br/>&nbsp;<b>Website:</b>&nbsp;&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                    + "On the first login, e-GP System will request you to change the password and "
                    + "you will be redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mentioned steps to change the password:<br/>"
                    + "<ol><li>Type a new Password.</li>"
                    + "<li>Retype the same Password.</li>"
                    + "<li>Select a Hint Question </li>"
                    + "<li>Type a Hint Answer.  </li></ol><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry Of Finance, Royal Govrnment Of Bhutan<br />"
                    + "Address: Box 116, Thimphu, Bhutan. <br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list\"</span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getGovtMailContent :" + e);
        }
        LOGGER.debug("getGovtMailContent Ends:");
//        loginContent.add("Dear User," + "<br/>"
//                + "Your login account has been created on e-GP System. "
//                + "Please check your registered e-mail ID for more details.<br/>"
//                + "Thanks,e-GP Support Team");
        return loginContent;
    }

    /**
     * When Lead partner invites sub contractor or sub consultant Tenderer
     * /Consultant
     *
     * @param emaiId
     * @param name
     * @param tenderId
     * @param tenderRefNo
     * @param list
     * @return
     */
    public List getSubContractInvtDetails(String emaiId, String name, String tenderId, String tenderRefNo, List<Object[]> list) {
        List loginContent = new ArrayList();
        LOGGER.debug("getSubContractInvtDetails Starts:");

        try {
            loginContent.add("e-GP: Invitation for Sub-Contracting / Sub-Consultant");
            StringBuilder sb = new StringBuilder();
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title> </head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that you have an invitation for Sub Contracting / Sub Consultant from " + name + " for the below mentioned Tender.<br/>"
                    //+ "<br/><b>Login ID:</b>&nbsp;&nbsp;" + emaiId
                    + "<br/><table width='100%' cellspacing='0' cellpading='0' style = 'font-family:arial;font-size:14px;line-height:19px;'>"
                    + "<tr><td style='font-weight:bold; vertical-align:middle;' width='150'> Tender Id:</td>"
                    + "<td valign='middle' >" + tenderId + "</td></tr>"
                    + "<tr><td style='font-weight:bold; vertical-align:middle;'>Tender Ref No:</td><td valign='middle'>" + tenderRefNo + "</td></tr>");
            if (!list.isEmpty()) {
                sb.append("<tr><td style='font-weight:bold; vertical-align:middle;'>Lot No:</td><td valign='middle'>" + list.get(0)[0] + "</td></tr>"
                        + "<tr><td style='font-weight:bold; vertical-align:middle;'>Lot Description:</td><td valign='middle'>" + list.get(0)[1] + "</td></tr>");
            }
            sb.append("</table>"
                    + "<br />You may perform the below mentioned steps to process the invitation:");
            sb.append("<br/>1.&nbsp;&nbsp;&nbsp;Login to e-GP website"
                    + "<br/>2.&nbsp;&nbsp;&nbsp;Click on Tender -> All Tenders"
                    + "<br/>3.&nbsp;&nbsp;&nbsp;Search Tender using search criteria"
                    + "<br/>4.&nbsp;&nbsp;&nbsp;Click on Tender Dashboard icon"
                    //+ "<br/>5.&nbsp;&nbsp;&nbsp;Accept the Terms and conditions of the first tab.<br/>"
                    + "<br/>6.&nbsp;&nbsp;&nbsp;Click on Sub-Contracting tab"
                    + "<br/>7.&nbsp;&nbsp;&nbsp;Click on received Invitations tab"
                    + "<br/>8.&nbsp;&nbsp;&nbsp;Click on accept / reject link to accept or reject the invitation<br/>"
                    + "<br/>Thanks,<br/>"
                    + "<b>e-GP System</b><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br/>"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\">GPPMD Office Contact Details</div>"
                    + "<div align=\"center\"><strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> </div>"
                    + "<div align=\"center\"><a href>" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Sub Contracting~~</body></html>");
            loginContent.add(sb);
        } catch (Exception e) {
            LOGGER.error("getSubContractInvtDetails :" + e);
        }
        list.clear();
        list = null;
        LOGGER.debug("getSubContractInvtDetails Ends:");
        return loginContent;
    }

    /**
     * When sub contractor or sub consultant accepts or rejects the invitation
     * Main tenderer/consultant who has invited sub contractor / consultant
     *
     * @param emaiId
     * @param tenderId
     * @param tenderRefNo
     * @param invAcceptStatus
     * @param name
     * @param list
     * @return
     */
    public List getProcessSubContractInv(String emaiId, String tenderId, String tenderRefNo, String invAcceptStatus, String name, List<Object[]> list) {
        LOGGER.debug("getProcessSubContractInv Starts:");

        List loginContent = new ArrayList();
        try {
            loginContent.add("e-GP:  Processing of Sub Contracting Request by Sub Contractor");
            StringBuilder sb = new StringBuilder();
            String s_status = "rejected";
            if ("Approved".equals(invAcceptStatus)) {
                s_status = "accepted";
            }
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title> </head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that your Sub Contracting  request has been " + s_status + " by " + name + " for a below mentioned Tender: <br/>"
                    //+ "<br/><b>Login ID:</b>&nbsp;&nbsp;" + emaiId
                    + "<br/><table width='100%' cellspacing='0' cellpading='0' style = 'font-family:arial;font-size:14px;line-height:19px;'>"
                    + "<tr><td style='font-weight:bold; vertical-align:middle;' width='150'> Tender Id:</td>"
                    + "<td valign='middle' >" + tenderId + "</td></tr>"
                    + "<tr><td style='font-weight:bold; vertical-align:middle;'>Tender Ref No:</td><td valign='middle'>" + tenderRefNo + "</td></tr>");
            if (!list.isEmpty()) {
                sb.append("<tr><td style='font-weight:bold; vertical-align:middle;'>Lot No:</td><td valign='middle'>" + list.get(0)[0] + "</td></tr>"
                        + "<tr><td style='font-weight:bold; vertical-align:middle;'>Lot Description:</td><td valign='middle'>" + list.get(0)[1] + "</td></tr>");
            }
            sb.append("</table>");
            sb.append("<br/>Thanks,<br/>e-Gp System</b><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br/>"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\">GPPMD Office Contact Details</div>"
                    + "<div align=\"center\"><strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> </div>"
                    + "<div align=\"center\"><a href>" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Sub Contracting~~</body></html>");
            loginContent.add(sb);
        } catch (Exception e) {
            LOGGER.error("getProcessSubContractInv :" + e);
        }
        list.clear();
        list = null;
        LOGGER.debug("getProcessSubContractInv Ends:");
        return loginContent;
    }

    public String contAdmMailFinalSub(String emailId) {
        LOGGER.debug("contAdmMailFinalSub Starts:");

        StringBuffer sb = new StringBuffer();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear Admin," + "<br/><br/><span>"
                    + "A new user with e-mail ID : " + emailId + " has created his profile on e-GP System."
                    + "Please verify user profile to approve or reject it.<br/><br/>"
                    + "<br/>Please note that the profile of user will remain PENDING till the time you do not approve/reject it.<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contAdmMailFinalSub :" + e);
        }
        LOGGER.debug("contAdmMailFinalSub Ends:");
        return sb.toString();
    }

    public String contAdmAppRej(String status, String comments, String username, String url) {
        LOGGER.debug("contAdmAppRej Starts:");
        StringBuffer sb = new StringBuffer();
        String temp = "";
        try {
            if (/*status.startsWith("A")*/status.charAt(0) == 'A') {
                temp = "approval";
                // sb.append(" e-GP System:  User Registration "+HandleSpecialChar.handleSpecialChar("-")+" APPROVED");
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                        + "Dear " + username + "," + "<br/><br/><span>"
                        + "Congratulations!<br /><br />"
                        + "Your profile details and mandatory credential documents have been primarily verified, and your registration has been APPROVED."
                        + "Procuring Agencies will require and verify the same documents during the post qualification process of a particular Tender you may be participating.<br/><br/>"
                        + "Now, you can log-on to e-GP System using your e-mail address(ID) and Password.<br/><br/>"
                        //+ "This is to inform you that your profile has been " + status + " by e-GP admin.<br/><br/>"
                        //+ "<br/>You can logon to e-GP System using your e-mail ID & Password.<br/><br/>"
                        //+ "Warm Regards,<br/>"
                        //+ "<b>e-GP Support Team.</b><br/><br/>"

                        /* Hide 
                        + "<strong>Important links:</strong><br/>"
                        + "<i><a href=\"" + XMLReader.getMessage("url") + "help/manuals/eGP_NewUserRegManual_English.pdf\" target=\"_blank\">User Manual for Bidders and consultants</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "help/guidelines/eGP_Guidelines.pdf\" target=\"_blank\">e-GP Guidelines</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "MemScheduleBank.jsp\" target=\"_blank\">List of Member Scheduled Banks</a><br/>"
                        // +"<a href=\""+XMLReader.getMessage("url")+"MandRegDoc.jsp\">List of Mandatory Documents for Registration</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "TermsNConditions.jsp\" target=\"_blank\">Terms and Conditions</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "PrivacyPolicy.jsp\" target=\"_blank\">Disclaimer and Privacy Policy</a></i><br/><br/>"
                         */
                        + "Thank you for using Electronic Government Procurement (e-GP) System.<br/><br/>"
                        + "<b>Government Procurement and Property Management Division (GPPMD)</b><br/>"
                        + "Ministry of Finance, Royal Government of Bhutan<br />"
                        + "Address :  Box 116, Thimphu, Bhutan. <br /><br />"
                        + "<div align=\"center\">GPPMD Office Contact Details</div>"
                        + "<div align=\"center\"><strong>Phone:</strong> +975-02-336962 </div>"//| <strong>Email:</strong> <a href>helpdesk@egp.gov.bt</a></div>"
                        + "<div align=\"center\"><a href>" + XMLReader.getMessage("url") + "</a></div>"
                        + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                        + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                        + "</td></tr></table></body></html>");
            } else if (/*status.startsWith("N")*/status.charAt(0) == 'N') {
                temp = "New Procurement Category Approval";
                // sb.append(" e-GP System:  User Registration "+HandleSpecialChar.handleSpecialChar("-")+" APPROVED");
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                        + "Dear " + username + "," + "<br/><br/><span>"
                        + "Congratulations!<br /><br />"
                        + "Your new requested procurement category and mandatory credential documents have been primarily verified, and your new procurement category has been APPROVED."
                        + "Procuring Agencies will require and verify the same documents during the post qualification process of a particular Tender you may be participating.<br/><br/>"
                        + "Now, you can log-on to e-GP System using your e-mail address(ID) and Password.<br/><br/>"
                        //+ "This is to inform you that your profile has been " + status + " by e-GP admin.<br/><br/>"
                        //+ "<br/>You can logon to e-GP System using your e-mail ID & Password.<br/><br/>"
                        //+ "Warm Regards,<br/>"
                        //+ "<b>e-GP Support Team.</b><br/><br/>"

                        /* Hide 
                        + "<strong>Important links:</strong><br/>"
                        + "<i><a href=\"" + XMLReader.getMessage("url") + "help/manuals/eGP_NewUserRegManual_English.pdf\" target=\"_blank\">User Manual for Bidders and consultants</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "help/guidelines/eGP_Guidelines.pdf\" target=\"_blank\">e-GP Guidelines</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "MemScheduleBank.jsp\" target=\"_blank\">List of Member Scheduled Banks</a><br/>"
                        // +"<a href=\""+XMLReader.getMessage("url")+"MandRegDoc.jsp\">List of Mandatory Documents for Registration</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "TermsNConditions.jsp\" target=\"_blank\">Terms and Conditions</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "PrivacyPolicy.jsp\" target=\"_blank\">Disclaimer and Privacy Policy</a></i><br/><br/>"
                         */
                        + "Thank you for using Electronic Government Procurement (e-GP) System.<br/><br/>"
                        + "<b>Government Procurement and Property Management Division (GPPMD)</b><br/>"
                        + "Ministry of Finance, Royal Government of Bhutan<br />"
                        + "Address :  Box 116, Thimphu, Bhutan. <br /><br />"
                        + "<div align=\"center\">GPPMD Office Contact Details</div>"
                        + "<div align=\"center\"><strong>Phone:</strong> +975-02-336962 </div>"//| <strong>Email:</strong> <a href>helpdesk@egp.gov.bt</a></div>"
                        + "<div align=\"center\"><a href>" + XMLReader.getMessage("url") + "</a></div>"
                        + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                        + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                        + "</td></tr></table></body></html>");
            } else if (/*status.startsWith("N")*/status.charAt(0) == 'R') {
                temp = "New Procurement Category Rejection";
                // sb.append(" e-GP System:  User Registration "+HandleSpecialChar.handleSpecialChar("-")+" APPROVED");
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                        + "Dear " + username + "," + "<br/><br/><span>"
                        //                        + "Congratulations!<br /><br />"
                        + "We are sorry to say that your new requested procurement category has been REJECTED."
                        //+ "This is to inform you that your profile has been " + status + " by e-GP admin.<br/><br/>"
                        //+ "<br/>Below mentioned is the reason for " + temp + " of your profile:<br/><br/>"
                        + "<br/>" + comments + "<br/><br/>"
                        + "You can request for new procurement category again following the same process you followed before.<br />"
                        // + "Thank you for using Electronic Government Procurement (e-GP) System.<br/>"
                        //+ "<b>e-GP Support Team.</b><br/><br/>"
                        // + "<span style=\"color : red\"><b>To ensure you receive future emails, please add <a href=\"mailto:auto-email@pppd.gov.bt\">auto-email@cptu.gov.bt</a> email "
                        //+ "address of procuring agencies to your Safe Sender list.\"</b></span>"

                        /* Hide
                        + "<strong>Important links:</strong><br/><br/>"
                        + "<i><a href=\"" + XMLReader.getMessage("url") + "help/manuals/eGP_NewUserRegManual_English.pdf\" target=\"_blank\">User Manual for Bidders and consultants</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "help/guidelines/eGP_Guidelines.pdf\" target=\"_blank\">e-GP Guidelines</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "MemScheduleBank.jsp\" target=\"_blank\">List of Member Scheduled Banks</a><br/>"
                        // +"<a href=\""+XMLReader.getMessage("url")+"MandRegDoc.jsp\">List of Mandatory Documents for Registration</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "TermsNConditions.jsp\" target=\"_blank\">Terms and Conditions</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "PrivacyPolicy.jsp\" target=\"_blank\">Disclaimer and Privacy Policy</a></i><br/><br/>"
                         */
                        + "Thank you for using Electronic Government Procurement (e-GP) System.<br/><br/>"
                        + "<b>Public Procure Policy Division (GPPMD)</b><br/>"
                        + "Ministry of Finance, Royal Government of Bhutan<br />"
                        + "Address: Box 116, Thimphu, Bhutan <br /><br />"
                        + "<div align=\"center\"><strong>GPPMD Office Contact Details</strong></div>"
                        + "<div align=\"center\"><strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> </div>"
                        + "<div align=\"center\"><a href>" + XMLReader.getMessage("url") + "</a></div>"
                        + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                        + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                        + "</td></tr></table></body></html>");
            } else {
                temp = "notify for modification";
                // sb.append(" e-GP System:  User Registration "+HandleSpecialChar.handleSpecialChar("-")+" REJECTED");
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                        + "Dear " + username + "," + "<br/><br/><span>"
                        + "We are sorry to say that your Registration has been NOTIFY FOR MODIFICATION because of the following reasons:<br />"
                        //+ "This is to inform you that your profile has been " + status + " by e-GP admin.<br/><br/>"
                        //+ "<br/>Below mentioned is the reason for " + temp + " of your profile:<br/><br/>"
                        + "<br/>" + comments + "<br/><br/>"
                        + "You can update your profile depends on above reason again following the same process you followed before.<br />"
                        // + "Thank you for using Electronic Government Procurement (e-GP) System.<br/>"
                        //+ "<b>e-GP Support Team.</b><br/><br/>"
                        // + "<span style=\"color : red\"><b>To ensure you receive future emails, please add <a href=\"mailto:auto-email@pppd.gov.bt\">auto-email@cptu.gov.bt</a> email "
                        //+ "address of procuring agencies to your Safe Sender list.\"</b></span>"

                        /* Hide
                        + "<strong>Important links:</strong><br/><br/>"
                        + "<i><a href=\"" + XMLReader.getMessage("url") + "help/manuals/eGP_NewUserRegManual_English.pdf\" target=\"_blank\">User Manual for Bidders and consultants</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "help/guidelines/eGP_Guidelines.pdf\" target=\"_blank\">e-GP Guidelines</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "MemScheduleBank.jsp\" target=\"_blank\">List of Member Scheduled Banks</a><br/>"
                        // +"<a href=\""+XMLReader.getMessage("url")+"MandRegDoc.jsp\">List of Mandatory Documents for Registration</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "TermsNConditions.jsp\" target=\"_blank\">Terms and Conditions</a><br/>"
                        + "<a href=\"" + XMLReader.getMessage("url") + "PrivacyPolicy.jsp\" target=\"_blank\">Disclaimer and Privacy Policy</a></i><br/><br/>"
                         */
                        + "Thank you for using Electronic Government Procurement (e-GP) System.<br/><br/>"
                        + "<b>Public Procure Policy Division (GPPMD)</b><br/>"
                        + "Ministry of Finance, Royal Government of Bhutan<br />"
                        + "Address: Box 116, Thimphu, Bhutan <br /><br />"
                        + "<div align=\"center\"><strong>GPPMD Office Contact Details</strong></div>"
                        + "<div align=\"center\"><strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> </div>"
                        + "<div align=\"center\"><a href>" + XMLReader.getMessage("url") + "</a></div>"
                        + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                        + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                        + "</td></tr></table></body></html>");
            }
        } catch (Exception e) {
            LOGGER.error("contAdmAppRej :" + e);

        }
        LOGGER.debug("contAdmAppRej Ends:");

        return sb.toString();
    }

    public List conAPPPublish(String pkgNo, String pkgDesc, int pkgId, int appId) {
        LOGGER.debug("conAPPPublish Starts:");
        List appPublish = new ArrayList();
        try {
            appPublish.add("e-GP: APP – New Package published of your choice");
            StringBuilder sb = new StringBuilder();
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that new package of your choice has been published on e-GP System.<br/><br/>"
                    + "<b>Package Detail(s) :</b><br />");
            sb.append("<br/><b>Package No. :</b>&nbsp;&nbsp;").append(pkgNo);
            sb.append("<br/><b>Package Description :</b>&nbsp;&nbsp;<a href='" + XMLReader.getMessage("url") + "resources/common/ViewPackageDetail.jsp?appId=" + appId + "&pkgId=" + pkgId + "&from=home'>").append(pkgDesc).append("</a><br/>");

            sb.append("<br/>Please visit e-GP System to get more information in this regard.&nbsp;&nbsp;<br/><br/>"
                    + "<b>e-GP System</b><br/><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a> </b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");

            appPublish.add(sb.toString());
        } catch (Exception e) {
            LOGGER.error("conAPPPublish :" + e);
        }
        LOGGER.debug("conAPPPublish Ends:");
        return appPublish;
    }

    public List conWorkflow(String eventName, String moduleName, String appCode, String officialName, String desig, String fromEmailid, String tenderID) {
        LOGGER.debug("conWorkflow Starts:");
        List conworkflow = new ArrayList();
        try {
            if (moduleName.equalsIgnoreCase("Tender")) {
                conworkflow.add("Tender : " + tenderID + " File to be processed in Workflow");
            } else {
                conworkflow.add("APP Code : " + appCode + " File to be processed in Workflow");
            }
            StringBuilder sb = new StringBuilder();
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br /><span>"
                    + "This is to inform you that you have been included in the following workflow process:<br/><br />"
                    + "<b>Module Name: </b>" + moduleName + "<br/>"
                    + "<b>Process: </b> " + eventName + "<br/>"
                    + "<b>ID / Ref No: </b>" + appCode + "<br/>"
                    + "<b>Workflow Created By: </b>" + fromEmailid + "<br/>"
                    + "<b>Official Name - Designation: </b>" + officialName + "-" + desig + "<br/><br />"
                    + "You will be notified once the file is being sent to you for processing.<br/><br />"
                    + "<b>Warm Regards,</b><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)<br/>"
                    + "Department of National Properties<br/>"
                    + "Ministry of Finance .</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Workflow~~</body></html>");
            conworkflow.add(sb.toString());
        } catch (Exception e) {
            LOGGER.error("conWorkflow :" + e);
        }
        LOGGER.debug("conWorkflow Ends:");
        return conworkflow;
    }

    public List conWorkflowFileProcess(String eventName, String moduleName, String appCode, String officialName, String desig, String fromEmailid) {
        LOGGER.debug("conWorkflow Starts:");
        List conworkflow = new ArrayList();
        try {
            conworkflow.add("APP Code : " + appCode + " File to be processed in Workflow");
            StringBuilder sb = new StringBuilder();
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br /><span>"
                    + "A file has come to you for processing. Details of the file is as mentioned below:<br/><br />"
                    + "<b>Module Name: </b>" + moduleName + "<br/>"
                    + "<b>Process: </b> " + eventName + "<br/>"
                    + "<b>ID / Ref No: </b>" + appCode + "<br/>"
                    + "<b>File Sent From: </b>" + fromEmailid + "<br/>"
                    + "<b>Official Name - Designation: </b>" + officialName + "-" + desig + "<br/><br />"
                    + "Perform below mentioned steps to process the same:<br/>"
                    + "1. Login to e-GP website <br/>"
                    + "2. Click on workflow menu<br/>"
                    + "3. Select Pending task <br/>"
                    + "4. Then at last under action tab click on \"Process\" link<br/><br />"
                    + "<b>Thanks</b><br/>"
                    + "<b>e-GP System</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Workflow~~</body></html>");
            conworkflow.add(sb.toString());
        } catch (Exception e) {
            LOGGER.error("conWorkflow :" + e);
        }
        LOGGER.debug("conWorkflow Ends:");
        return conworkflow;
    }

    public List conWorkflowforRemoveUsers(String eventName, String moduleName, int id) {
        LOGGER.debug("conWorkflowforRemoveUsers Starts:");
        List conworkflow = new ArrayList();
        try {
            conworkflow.add("e-GP System: Removal from Workflow");
            StringBuilder sb = new StringBuilder();
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><span>"
                    + "This is to inform you that you have been removed from the workflow for below mentioned module and <br/>"
                    + "process by PA: <br/>"
                    + "Module Name:" + moduleName + "<br/>"
                    + "Process: " + eventName + "<br/>"
                    + "ID / Ref No:" + id + "<br/>"
                    + "Thanks.<br/>"
                    + "<b>e-GP System.</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Workflow~~</body></html>");
            conworkflow.add(sb.toString());
        } catch (Exception e) {
            LOGGER.error("conWorkflowforRemoveUsers :" + e);
        }
        LOGGER.debug("conWorkflowforRemoveUsers Ends:");
        return conworkflow;
    }

    /**
     * Publish Corrigendum Tenderer/Consultant
     *
     * @param tenderid
     * @param refNo
     * @return String
     */
    public String tenderCorriPub(String tenderid, String refNo) {
        LOGGER.debug("conWorkflowforRemoveUsers Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that an Amendment has been published in a below mentioned Tender:<br/><br/>"
                    //+ "This is to inform you that an Amendment /Corrigendum is being published on <br/>"
                    //+ "e-GP System for below given Tender. You are requested to login to the website<br/>"
                    //+ "to view the details of the same.<br/><br/><br/>"
                    + "Tender Id: " + tenderid + "<br/>"
                    + "Ref No: " + refNo + "<br/><br/>"
                    //+ "<br/>Warm Regards,<br/>"
                    + "<b>e-GP System</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span><br/><br/>"
                    + "<b>Electronic Government Procurement (e-GP) System of the Royal Government of "
                    + "Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("url") + "</a> "//| <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a>"
                    + "</td></tr></table>~Corrigendum / Amendment~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("tenderCorriPub :" + e);
        }
        LOGGER.debug("conWorkflowforRemoveUsers Ends:");
        return sb.toString();
    }

    /**
     * Publish Tenders Tenderer /Consultant’s business category and Tender’s
     * category matches
     *
     * @param tenderid
     * @param refNo
     * @param peName
     * @param closingDate
     * @param tendBrief
     * @return String
     */
    public String tenderPub(String tenderid, String refNo, String peName, String closingDate, String tendBrief, String tenderCap) {
        LOGGER.debug("conWorkflowforRemoveUsers Starts:");
        StringBuilder sb = new StringBuilder();

        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that a " + tenderCap + " of your choice is published on e-GP system. Brief detail of the " + tenderCap + " "
                    + "is as mentioned below: <br/><br/><br/>"
                    + "<b>" + tenderCap + " ID:</b> " + tenderid + "<br/>"
                    + "<b>" + tenderCap + " Reference No:</b> " + refNo + "<br/>"
                    + "<b>" + tenderCap + " Closing Date:</b> " + closingDate + "<br/><br/>"
                    //+ "<b>Tender Brief / Title:</b> <a onclick=\"javascript:window.open('" + XMLReader.getMessage("url") + "/resources/common/ViewTender.jsp?id=" + tenderid + "', '', 'width=1200px,height=600px,scrollbars=1','');\" href=\"javascript:void(0);\">" + tendBrief + "</a><br/>"
                    + "<b>" + tenderCap + " Brief / Title:</b> <a href=\"" + XMLReader.getMessage("url") + "/resources/common/ViewTender.jsp?id=" + tenderid + "\" target=\"_blank\">" + tendBrief + "</a><br/>"
                    + "<b>Procuring Agency:</b> " + peName + "<br/><br/>"
                    //+ "Please go to Tender Dashboard, to prepare and Lodge e-Tender.<br/><br/>"
                    + "<br/>e-GP System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span><br/><br/>"
                    + "<b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan "
                    + " | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("url") + "</a>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a>"
                    + "</td></tr></table>~Tender notice~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("tenderCorriPub :" + e);
        }
        LOGGER.debug("conWorkflowforRemoveUsers Ends:");
        return sb.toString();
    }

    /**
     * Publish Tenders RFQ RFQL and RFQU Tenderer /Consultant’s business
     * category and Tender’s category matches
     *
     * @param tenderid
     * @param refNo
     * @param peName
     * @param closingDate
     * @param tendBrief
     * @return String
     */
    public String tenderPubRFQ(String tenderid, String refNo, String peName, String closingDate, String tendBrief) {
        LOGGER.debug("conWorkflowforRemoveUsers Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that a e-Limited Enquiry Method (e-LEM) of your interest is published on e-GP system. Detail notice with terms and conditions is available with the e-LEM document. Brief detail of the e-LEM is as mentioned below : <br/><br/><br/>"
                    + "<b>e-LEM ID:</b> " + tenderid + "<br/>"
                    + "<b>Reference No:</b> " + refNo + "<br/>"
                    + "<b>e-LEM Closing Date:</b> " + closingDate + "<br/><br/>"
                    //+ "<b>Tender Brief / Title:</b> <a onclick=\"javascript:window.open('" + XMLReader.getMessage("url") + "/resources/common/ViewTender.jsp?id=" + tenderid + "', '', 'width=1200px,height=600px,scrollbars=1','');\" href=\"javascript:void(0);\">" + tendBrief + "</a><br/>"
                    + "<b>e-LEM Brief / Title:</b> <a href=\"" + XMLReader.getMessage("url") + "/resources/common/ViewTender.jsp?id=" + tenderid + "\" target=\"_blank\">" + tendBrief + "</a><br/>"
                    + "<b>Procuring Agency:</b> " + peName + "<br/><br/>"
                    //+ "Please go to Tender Dashboard, to prepare and Lodge e-Tender.<br/><br/>"
                    + "<br/>e-GP System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span><br/><br/>"
                    + "<b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan "
                    + "| <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("url") + "</a>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a>"
                    + "</td></tr></table>~Tender notice~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("tenderCorriPub :" + e);
        }
        LOGGER.debug("conWorkflowforRemoveUsers Ends:");
        return sb.toString();
    }

    /**
     * Publish Tenders RFP Tenderer /Consultant’s business category and Tender’s
     * category matches
     *
     * @param tenderid
     * @param refNo
     * @param peName
     * @param closingDate
     * @param tendBrief
     * @return String
     */
    public String tenderPubRFP(String tenderid, String refNo, String peName, String closingDate, String tendBrief, List<SPCommonSearchData> list) {
        LOGGER.debug("conWorkflowforRemoveUsers Starts:");
        StringBuilder sb = new StringBuilder();
        int i = 1;
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "The Royal Government of Bhutan has allocated public funds and intends to apply a portion of the funds to eligible payments under the Contract for which this e-RFP Document is issued through e-GP system.<br/>"
                    + "The " + peName + " now invites proposals to provide the following consulting services: " + tendBrief + " <br />More details on the services are provided in the Terms of Reference (TOR).<br/>"
                    + "This is to inform you that a e-Request For Proposal (e-RFP) of your interest is published on e-GP system. <br />Detailed notice with terms and conditions is available with the e-RFP document.<br/><br />"
                    + "<b>e-RFP ID:</b> " + tenderid + "<br/>"
                    + "<b>Reference No:</b> " + refNo + "<br/>"
                    + "<b>e-RFP Closing Date:</b> " + closingDate + "<br/><br/>"
                    + "<b>Name of Consultants :</b>");

            if (list != null && !list.isEmpty()) {

                sb.append("<br /><br />");
                sb.append("<table width='100%' border='1' cellspacing='0' cellpadding='5' align='center'>");
                for (SPCommonSearchData spcd : list) {
                    if (tenderid.equals(spcd.getFieldName2()) && "rfp".equalsIgnoreCase(spcd.getFieldName9())) {
                        sb.append("<tr><td width='10%'>" + i + "</td><td>" + spcd.getFieldName8() + "</td></tr>");
                        ++i;
                    }
                }
                sb.append("</table><br />");
            }
            sb.append("<br/>e-GP System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span><br/><br/>"
                    + "<b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan "
                    + "| <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("url") + "</a>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a>"
                    + "</td></tr></table>~Tender notice~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("tenderCorriPub :" + e);
        }
        LOGGER.debug("conWorkflowforRemoveUsers Ends:");
        return sb.toString();
    }

    /**
     * Final submission of a tender/proposal Tender /Bid/Proposal withdrawal
     * Tender/Bid//Proposal modification/substitution Tenderer / Consultant
     *
     * @param tenderid
     * @param userId
     * @param flag
     * @return
     */
    public String bidTenderSubMail(String tenderid, String userId, String flag) {
        LOGGER.debug("conWorkflowforRemoveUsers Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that you have ");
            if ("final".equals(flag)) {
                sb.append("successfully completed final submission "
                        + "(Dropping of Tender into Time Stamped Secured Electronic Box) "
                        + "in a below mentioned tender:<br/><br/>");
                sb.append("~Tender submission~~");
            } else if ("withdraw".equals(flag)) {
                sb.append("successfully withdrawn below mentioned tender:<br/><br/>");
                sb.append("~Tender withdrawl~~");
            } else if ("modify".equals(flag)) {
                sb.append("successfully modified below "
                        + "mentioned Tender:<br/><br/>");
                sb.append("~Tender modification~~");
            }
            sb.append("<br/>");
            TenderBidService service = (TenderBidService) AppContext.getSpringBean("TenderBidService");
            //0. tom.officeName,1. ttd.packageNo,2. ttd.packageDescription,3. tfs.ipAddress,
            //4. tfs.tenderHash,5. tfs.finalSubmissionDt
            Object data1[] = service.finalSubTenderDetails(tenderid, userId);
            if (data1 != null) {
                sb.append("<table border='1'>");
                sb.append("<tr>");
                sb.append("<td width='40%'><b>PA Name :</b></td>");
                sb.append("<td width='60%'>" + data1[0] + "</td>");
                sb.append("</tr>");
                sb.append("<tr>");
                sb.append("<td><b>Tender Notice ID :</b></td>");
                sb.append("<td>" + tenderid + "</td>");
                sb.append("</tr>");
                sb.append("<tr>");
                sb.append("<td><b>Package No.<br/>and Description :</b></td>");
                sb.append("<td>" + data1[1] + ",<br/>" + data1[2] + "</td>");
                sb.append("</tr>");
                sb.append("<tr>");
                sb.append("<td><b>Ref. No.</b></td>");
                sb.append("<td>" + data1[6] + "</td>");
                sb.append("</tr>");
                List<Object[]> data2 = service.finalSubLotDetails(tenderid, userId);
                for (Object[] objects : data2) {
                    sb.append("<tr>");
                    sb.append("<td><b>Lot No. and<br/>Description :</b></td>");
                    sb.append("<td>" + objects[0] + ",<br/>" + objects[1] + "</td>");
                    sb.append("</tr>");
                }
                sb.append("<tr>");
                sb.append("<td><b>Submitted from which IP :</b></td>");
                sb.append("<td>" + data1[3] + "</td>");
                sb.append("</tr>");
                sb.append("<tr>");
                sb.append("<td><b>Date & Time of Submission :</b></td>");
                sb.append("<td>" + DateUtils.gridDateToStr((Date) data1[5]) + "</td>");
                sb.append("</tr>");
                if (("final".equals(flag)) && data1[4] != null && (!data1[4].toString().equals("-1"))) {
                    sb.append("<tr>");
                    sb.append("<td><b>Bidder's/Consultant's Mega Hash :</b></td>");
                    sb.append("<td>" + data1[4] + "</td>");
                    sb.append("</tr>");
                }
                sb.append("</table>");
            }
            sb.append("<br/>");
            if ("modify".equals(flag)) {
                sb.append("Please note that you need to do final submission (Dropping of a Tender "
                        + "into Secured Time stamped Electronic Tender box) once you modify your tender.<br/><br/>");
            }
            sb.append("<br/>"
                    + "<b>e-GP System.</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("tenderCorriPub :" + e);
        }
        LOGGER.debug("conWorkflowforRemoveUsers Ends:");

        return sb.toString();
    }

    /**
     * PE requests the tender/proposal validity and security validity extension
     * to the HOPE/Secretary HOPE or Secretary as the case may be
     *
     * @param tenderid
     * @param refNo
     * @param peOffice
     * @return String
     */
    public String contTOExtReq(String tenderid, String refNo, String peOffice) {
        LOGGER.debug("contTOExtReq Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that you have received an opening date and time extension request for the below mentioned Tender:<br/><br/><br/>"
                    + "Tender Id: " + tenderid + "<br/>"
                    + "Ref No: " + refNo + "<br/>"
                    + "Procuring Agency: " + peOffice + "<br/><br/>"
                    + "<br/>Click on Evaluation Tab -> Opening Date Ext. Req. to process the request.<br/><br/>"
                    + "<br/>Thanks.<br/>"
                    + "<b>e-GP System.</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contTOExtReq :" + e);
        }
        LOGGER.debug("contTOExtReq Ends:");
        return sb.toString();
    }

    /**
     * Tender/Proposal and Security Validity Extension When a
     * Tenderer/Consultant accepts or rejects the validity or security extension
     * request
     *
     * @param tenderid
     * @param refNo
     * @return String
     */
    public String contentTenProSec(String tenderid, String refNo) {
        LOGGER.debug("contentTenProSec Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that below mentioned Bidder/Consultant has <Accepted / Rejected > Tender Validity and Security (If applicable) extension request for below Tender:<br/><br/><br/>"
                    + "Tender ID: " + tenderid + "<br/>"
                    + "Reference No.: " + refNo + "<br/>"
                    + "<br/>Thanks.<br/>"
                    + "<b>e-GP System.</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span><br/><br/>"
                    + "<b>Electronic Government Procurement (e-GP) System of the Government of the People's Republic of "
                    + "Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("url") + "</a>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a>"
                    + "</td></tr></table>~Tender and Security Validity Extension~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contentTenProSec :" + e);
        }
        LOGGER.debug("contentTenProSec Ends:");
        return sb.toString();
    }

    /**
     * When Opening Committee Chairperson requests the HOPE for extension of
     * opening date and time
     *
     * @param tenderid
     * @param refNo
     * @param PEName
     * @param newDate
     * @return String
     */
    public String contOEReqApproval(String tenderid, String refNo, String PEName, String newDate) {
        LOGGER.debug("contOEReqApproval Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you the new Opening Date and Time is as mentioned below:<br/><br/>"
                    + "<b>Tender Id:</b> " + tenderid + "<br/>"
                    + "<b>Ref No:</b> " + refNo + "<br/>"
                    + "<b>PA:</b> " + PEName + "<br/>"
                    + "<b>New Date and Time:</b> " + newDate + "<br/><br/>"
                    + "<br/>Thanks.<br/>"
                    + "e-GP System.<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Opening committee~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contOEReqApproval :" + e);
        }
        LOGGER.debug("contOEReqApproval Ends:");
        return sb.toString();
    }

    /**
     * Evaluation committee When PA issues NOA to the concern Tenderer Tenderer
     * to whom the NOA is issued
     *
     * @param tenderId
     * @param refNo
     * @param contno
     * @param contid
     * @param status
     * @param name
     * @param lotNo
     * @param lotDesc
     * @return String
     */
    public String contApproval(String tenderid, String refNo, String contno, String contid, String status, String name, String lotNo, String lotDesc) {
        StringBuilder sb = new StringBuilder();
        LOGGER.debug("contApproval Starts:");
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td><br/>"
                    + "Dear User,<br/><br/><span>"
                    + name + " has " + status + " Letter of Acceptance (LOA) for a below mentioned Tender:"
                    + "<br/><br/>Tender ID: " + tenderid + ""
                    + "<br/>Reference No.: " + refNo + ""
                    + "<br/>Lot No.: " + lotNo + ""
                    + "<br/>Lot Description: " + lotDesc + ""
                    + "<br/><br/>Thanks<br/>"
                    + "<br/>e-GP System<br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<br/><br/>"
                    + "</td></tr></table>~LOA~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contApproval :" + e);
        }
        LOGGER.debug("contApproval Ends:");
        return sb.toString();
    }

    public String contTenderPayment(String tenderid, String refNo, String paymentFor, String action) {
        LOGGER.debug("contTenderPayment Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that your " + paymentFor + " in a below <br/>"
                    + "mentioned tender has been " + action + ":<br/><br/><br/>"
                    + "Id: " + tenderid + "<br/>"
                    + "Ref No: " + refNo + "<br/>"
                    + "<br/>"
                    + "<br/>Thanks.<br/>"
                    + "<b>e-GP System</b>"
                    + "<br/>"
                    + "<br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<br/>"
                    + "<br/>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a> </b></div>"//| <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td>"
                    + "</tr>"
                    + "</table>"
                    + "</body>"
                    + "</html>");
        } catch (Exception e) {
            LOGGER.error("contTenderPayment :" + e);
        }
        LOGGER.debug("contTenderPayment Ends:");
        return sb.toString();
    }

    /**
     * When a branch checker releases or forfeits the tender security payment
     * detail OR When a branch checker releases or forfeits the tender security
     * or performance security
     *
     * @param tenderid
     * @param refNo
     * @param lotNo
     * @param lotDescription
     * @param paymentFor
     * @param action
     * @param companyName
     * @param peOffice
     * @return
     */
    public String contBankUser_ReleaseForfeit(String tenderid, String refNo, String lotNo, String lotDescription, String paymentFor, String action, String companyName, String peOffice) {
        LOGGER.debug("contBankUser_ReleaseForfeit Ends:");
        StringBuilder sb = new StringBuilder();
        try {
            if (!"".equalsIgnoreCase(lotNo)) {
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                        + "<br/>Dear User," + "<br/><br/><span>"
                        + "This is to inform you that " + paymentFor + " of " + companyName + " <br/>"
                        + "has been " + action + " for below mentioned Tender:<br/><br/><br/>"
                        + "Tender ID: " + tenderid + "<br/>"
                        + "Reference No.: " + refNo + "<br/>"
                        + "Lot No.: " + lotNo + "<br/>"
                        + "PA: " + peOffice + "<br/><br/>"
                        + "<br/>Thanks.<br/>"
                        + "<b>e-GP System</b><br/><br/>"
                        + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                        + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                        + "<br/><br/>"
                        + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                        + "</td></tr></table></body></html>");
            } else {
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                        + "<br/>Dear User," + "<br/><br/><span>"
                        + "This is to inform you that " + paymentFor + " of " + companyName + " <br/>"
                        + "has been " + action + " for below mentioned Tender:<br/><br/><br/>"
                        + "Tender ID: " + tenderid + "<br/>"
                        + "Reference No.: " + refNo + "<br/>"
                        + "PA: " + peOffice + "<br/><br/>"
                        + "<br/>Thanks.<br/>"
                        + "<b>e-GP System</b><br/><br/>"
                        + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                        + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                        + "<br/><br/>"
                        + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a> </b></div>"//| <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                        + "</td></tr></table></body></html>");
            }

        } catch (Exception e) {
            LOGGER.error("contBankUser_ReleaseForfeit :" + e);
        }
        LOGGER.debug("contBankUser_ReleaseForfeit Ends:");
        return sb.toString();
    }

    /**
     * When Individual Evaluation Committee Member notifies Chairperson after
     * posting queries to be sought to the tenderers/consultants as a part of
     * tender clarification Evaluation Committee Chairperson
     *
     * @param tenderId
     * @param refNo
     * @return List Content of mail
     */
    public List evalClari(String tenderid, String refNo) {
        LOGGER.debug("evalClari Starts:");
        List listEval = new ArrayList();
        try {
            listEval.add("e-GP: Queries posted by Evaluation Committee Member");
            listEval.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that evaluation committee member has posted queries<br/> with respect to tender received for below mentioned <br/> tender id as a part of evaluation process:<br/><br/><br/>"
                    + "Id: " + tenderid + "<br/>"
                    + "Ref No: " + refNo + "<br/><br/>"
                    + "<br/>Warm Regards,<br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)<br/>"
                    + "Department of National Properties<br/>"
                    + "Ministry of Finance .</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Evaluation committee~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("evalClari :" + e);
        }
        LOGGER.debug("evalClari Ends:");
        return listEval;
    }

    /**
     * Mail content for send mail to tenderer for invitation of Negotiation
     *
     * @param tenderId
     * @param tenRefNo
     * @param sDate
     * @return
     */
    public List getNegMailContent(String tenderId, String tenRefNo, String sDate) {
        LOGGER.debug("getNegMailContent Starts:");
        List NegContent = new ArrayList();
        try {

            NegContent.add("e-GP: Invitation for negotiation");
            NegContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that you have been invited for negotiation by procuring agency for below mentioned tender:<br/>"
                    + "<br/><b>Id:</b> " + tenderId
                    + "<br/><b>Reference:</b> " + tenRefNo
                    + "<br/><b>Negotiation Start Date and Time :</b> " + sDate + " <br/><br/>"
                    + "Please logon to <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("url") + "</a> for more details.<br/><br/>"
                    + "Thanks,<br/>"
                    + "<b>e-GP System.</b>");
            NegContent.add("<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Negotiation~~</body></html>");

            /*NegContent.add("Dear User," + "<br/>"
                    + "Your login account has been created on e-GP System. "
                    + "Please check your registered e-mail ID for more details.<br/>"
                    + "Thanks,e-GP Support Team");*/
        } catch (Exception e) {
            LOGGER.error("evalClari :" + e);
            e.printStackTrace();
        }
        LOGGER.debug("getNegMailContent Ends:");
        return NegContent;
    }

    /**
     * Mail content for Negotiation Accept/Reject by tecpe to Tenderer
     *
     * @param tenderId
     * @param tenRefNo
     * @param status
     * @return
     */
    public List getNegAppRejContent(String tenderId, String tenRefNo, String msg, String status) {
        LOGGER.debug("getNegMailContent Starts:");
        List NegContent = new ArrayList();
        //System.out.println("status="+status);
        try {
            if (status != null && status.equalsIgnoreCase("Resend")) {
                NegContent.add("e-GP: Negotiation - Forms are Re-sent for Revision");
            } else if (status != null && status.equalsIgnoreCase("Rejected")) {
                NegContent.add("e-GP: Negotiation - Rejection of Form's Revision");
            } else {
                NegContent.add("e-GP: Negotiation - Acceptance of Form's Revision");
            }

            NegContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + msg
                    + "<br/><b>Id:</b> " + tenderId
                    + "<br/><b>Reference:</b> " + tenRefNo);
            NegContent.add("<br/><br/>For more detail, please visit : <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("url") + "</a>");
            NegContent.add("<br/><br/>Thanks,<br/>"
                    + "<b>e-GP System.</b>");
            NegContent.add("<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>");
            NegContent.add("</td></tr></table>~Negotiation~~</body></html>");

        } catch (Exception e) {
            LOGGER.error("evalClari :" + e);
        }
        LOGGER.debug("getNegMailContent Ends:");
        return NegContent;
    }

    /**
     * Mail content for Negotiation Close Message and Mail
     *
     * @param tenderId
     * @param tenRefNo
     * @param status
     * @return
     */
    public List getNegCloseContent(String tenderId, String tenRefNo, String status) {
        LOGGER.debug("getNegMailContent Starts:");
        List NegContent = new ArrayList();
        try {

            NegContent.add("e-GP: Negotiation Status");
            NegContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + status
                    + "<br/><b>Id:</b> " + tenderId
                    + "<br/><b>Reference:</b> " + tenRefNo);
            NegContent.add("<br/><br/>For details, please visit the following link:"
                    + "<br/> <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("url") + "</a>");
            NegContent.add("<br/><br/>Thanks,<br/>"
                    + "<b>e-GP System.</b>");
            NegContent.add("<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>");
            NegContent.add("</td></tr></table>~Negotiation~~</body></html>");

//            NegContent.add("Dear User," + "<br/>"
//                    + "Your login account has been created on e-GP System. "
//                    + "Please check your registered e-mail ID for more details.<br/>"
//                    + "Thanks,e-GP Support Team");
        } catch (Exception e) {
            LOGGER.error("evalClari :" + e);
        }
        LOGGER.debug("getNegMailContent Ends:");
        return NegContent;
    }

    /**
     * When PE request for relese or forfit TenderSecurity or Perfomance
     * Sercurity Branch Checker who has verified the payment detail earlier
     *
     * @param tenderid
     * @param refNo
     * @param lotNo
     * @param lotDescription
     * @param paymentFor
     * @param action
     * @param companyName
     * @param remarks
     * @param peOffice
     * @return mail content
     */
    public String contRequest_ReleaseForfeit(String tenderid, String refNo, String lotNo, String lotDescription, String paymentFor, String action, String companyName, String remarks, String peOffice, String userTypeId, String peName, String BankName, String BranchName, String Amount) {
        LOGGER.debug("contRequest_ReleaseForfeit Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            if ("forfeited".equalsIgnoreCase(action)) {
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                        + "<br/>Dear User," + "<br/><br/><span>"
                        + "This is to inform you that PA has requested to forfeit " + paymentFor + " for below mentioned Tender:<br/><br/><br/>"
                        + "Tender Id: " + tenderid + "<br/>"
                        + "Reference No: " + refNo + "<br/>"
                        + "Lot No: " + lotNo + "<br/>"
                        + "Lot Description: " + lotDescription + "<br/>"
                        + "Bidder / Consultant Name: " + companyName + "<br/>"
                        + "PA Office Name: " + peOffice + "<br/><br/>"
                        + "<br/>Thanks<br/>"
                        + "<b>e-GP System</b><br/><br/>"
                        + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                        + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                        + "<br/><br/>"
                        + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                        + "</td></tr></table></body></html>");
            } else if ("7".equalsIgnoreCase(userTypeId)) {
                //if("Tender Security".equalsIgnoreCase(paymentFor)){
                if ("released".equalsIgnoreCase(paymentFor)) {
                    sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                            + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                            + "<br/>Dear User," + "<br/><br/><span>"
                            + "This is to inform you that PA has requested to Release the " + paymentFor + " for the below mentioned Tender:<br/><br/><br/>"
                            + "Tender Id: " + tenderid + "<br/>"
                            + "Reference No: " + refNo + "<br/>"
                            + "Lot No: " + lotNo + "<br/>"
                            + "Lot Description: " + lotDescription + "<br/>"
                            + "Bidder / Consultant Name: " + companyName + "<br/>"
                            + "PA Name: " + peOffice + "<br/><br/>"
                            + "Reason for Return :  " + remarks + "<br/><br/>"
                            + "<br/>Thanks<br/>"
                            + "<b>Electronic Government Procurement (e-GP) System</b><br/><br/>"
                            + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                            + "Ministry of Finace, Royal Government of Bhutan<br />"
                            + "Address: Box 116, Thimphu, Bhutan. <br /><br />"
                            + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                            + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                            + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                            + "<br/><br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list </span>"
                            + "</td></tr></table></body></html>");
                } else {
                    sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                            + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                            + "<br/>Dear User," + "<br/><br/><span>"
                            + "This is to inform you that " + paymentFor + " of " + companyName + " <br/>"
                            + "for below mentioned tender has to be " + action + ":<br/><br/><br/>"
                            + "Tender Id: " + tenderid + "<br/>"
                            + "Reference No: " + refNo + "<br/><br/>"
                            + "<br/>Thanks<br/>"
                            + "<b>Electronic Government Procurement (e-GP) System</b><br/><br/>"
                            + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                            + "Ministry of Finance, Royal Government of Bhutan<br />"
                            + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                            + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                            + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                            + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                            + "<br/><br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list </span>"
                            + "</td></tr></table></body></html>");

                }
            } else {
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                        + "<br/>Dear User," + "<br/><br/><span>"
                        + "Be informed that PA has requested Financial Institute to release your " + paymentFor + " for below mentioned Tender:<br/><br/><br/>"
                        + "Tender Id: " + tenderid + "<br/>"
                        + "Reference No: " + refNo + "<br/>"
                        + "PA Name: " + peName + "<br/>"
                        + "Financial Institute and Branch Detail:: " + BranchName + "<br/>"
                        + "Tender Security Amount: " + Amount + "<br/>"
                        + "<br/><br/>"
                        + "You are requested to visit the Financial Institute for collection of " + paymentFor + ".<br/><br/>"
                        + "<br/>Thanks<br/>"
                        + "<b>Electronic Government Procurement (e-GP) System</b><br/><br/>"
                        + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                        + "Ministry of Finance, Royal Government of Bhutan<br />"
                        + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                        + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                        + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                        + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                        + "<br/><br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list </span>"
                        + "</td></tr></table></body></html>");
            }

        } catch (Exception e) {
            LOGGER.error("contRequest_ReleaseForfeit :" + e);
        }
        LOGGER.debug("contRequest_ReleaseForfeit Ends:");
        return sb.toString();
    }

    /**
     * PE requests the Tenderers for tender / proposal and security validity
     * extension Tenderer / consultant who has completed the final submission in
     * a tender
     *
     * @param tenderid
     * @param refNo
     * @param Pename
     * @return mail content
     */
    public String ValidationRequestToHope(int tenderId, String refNo, String Pename) {
        LOGGER.debug("ValidationRequestToHope Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "This is to inform you that you have received a tender validity/security extension request for the below  <br/>"
                    + "mentioned Tender :<br/><br/><br/>"
                    + "Tender Id: " + tenderId + "<br/>"
                    + "Ref No: " + refNo + "<br/>"
                    + "Procuring Agency: " + Pename + "<br/><br />"
                    + "<br/>Click on Evaluation Tab ->Tender Validity Date Ext. Req. to process the request.<br/><br />"
                    + "Thanks<br/>"
                    + "<b>Electronic Government Procurement (e-GP) System.</b><br/><br/>"
                    + "</td></tr></table>~Tender and Security Validity Extension~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("ValidationRequestToHope :" + e);
        }
        LOGGER.debug("ValidationRequestToHope Ends:");
        return sb.toString();
    }

    /**
     * PE requests the Tenderers for tender / proposal and security validity
     * extension Tenderer / consultant who has completed the final submission in
     * a tender
     *
     * @param tenderid
     * @param refNo
     * @param Pename
     * @param PEoffice
     * @return mail content
     */
    public String ValidationAppRequestToBidder(int tenderId, String refNo, String Pename, String PEoffice) {
        LOGGER.debug("ValidationAppRequestToBidder Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "This is to inform you that PA has sent you a request for Validity/Security Extension for the below <br/>"
                    + "mentioned Tender :<br/><br/><br/>"
                    + "Id: " + tenderId + "<br/>"
                    + "Ref No: " + refNo + "<br/>"
                    + "PA : " + PEoffice + "<br/><br />"
                    + "<br/>Kindly Accept / Reject the validity request.<br/>"
                    + "<br/>Thank you<br/><br />"
                    + "<b>Electronic Government Procurement (e-GP) System.</b>"
                    + "</td></tr></table>~Tender and Security Validity Extension~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("ValidationAppRequestToBidder :" + e);
        }
        LOGGER.debug("ValidationAppRequestToBidder Ends:");
        return sb.toString();
    }

    /**
     * PE requests the Tenderers for tender / proposal and security validity
     * extension Tenderer / consultant who has completed the final submission in
     * a tender
     *
     * @param tenderid
     * @param refNo
     * @param Pename
     * @param PEoffice
     * @return mail content
     */
    public String ValidationAppRequestToBiddertoEmail(int tenderId, String refNo, String Pename, String PEoffice) {
        LOGGER.debug("ValidationAppRequestToBidder Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "This is to inform you that PA has sent you a request for Validity/Security Extension for the below <br/>"
                    + "mentioned Tender :<br/><br/><br/>"
                    + "Id: " + tenderId + "<br/>"
                    + "Ref No: " + refNo + "<br/>"
                    + "PA : " + PEoffice + "<br/><br />"
                    + "<br/>Kindly logon to Electronic Government Procurement (e-GP) system to Accept / Reject the validity request.<br/>"
                    + "<br/>Thank you<br/><br />"
                    + "<b>Electronic Government Procurement (e-GP) System.</b><br />"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list\"</span>"
                    + "</td></tr></table>~Tender and Security Validity Extension~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("ValidationAppRequestToBidder :" + e);
        }
        LOGGER.debug("ValidationAppRequestToBidder Ends:");
        return sb.toString();
    }

    public String EmailToEvalTSCNotification(int tenderId, String refNo, String Pename) {
        LOGGER.debug("ValidationAppRequestToBidder Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "This is to inform you that Technical Sub Committee (TSC) has given recommendations in a below  <br/>"
                    + "mentioned Tender :<br/><br/><br/>"
                    + "Tender Id: " + tenderId + "<br/>"
                    + "Reference No: " + refNo + "<br/>"
                    + "PA : " + Pename + "<br/><br />"
                    + "<br/>Thank you<br/><br />"
                    + "<b>Electronic Government Procurement (e-GP) System.</b><br />"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a> </b></div>"//| <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list\"</span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("ValidationAppRequestToBidder :" + e);
        }
        LOGGER.debug("ValidationAppRequestToBidder Ends:");
        return sb.toString();
    }

    public String MessageToEvalTSCNotification(int tenderId, String refNo, String Pename) {
        LOGGER.debug("ValidationAppRequestToBidder Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "This is to inform you that Technical Sub Committee (TSC) has given recommendations in a below  <br/>"
                    + "mentioned Tender :<br/><br/><br/>"
                    + "Tender Id: " + tenderId + "<br/>"
                    + "Reference No: " + refNo + "<br/>"
                    + "PA : " + Pename + "<br/><br />"
                    + "<br/>Thank you<br/><br />"
                    + "<b>Electronic Government Procurement (e-GP) System.</b><br />"
                    + "</body></html>");
        } catch (Exception e) {
            LOGGER.error("ValidationAppRequestToBidder :" + e);
        }
        LOGGER.debug("ValidationAppRequestToBidder Ends:");
        return sb.toString();
    }

    /**
     * When TEC Chairperson initiates the Post Qualification process and if site
     * visit is required The concern tenderer who is to be post qualified
     *
     * @param tenderId
     * @param refNo
     * @param PEoffice
     * @param lotNo
     * @param lotDesc
     * @param tenderer
     * @param Sitevisitdate
     * @param siteVisitVal
     * @return
     */
    public String sendMessageTobidderForPQ(int tenderId, String refNo, String PEoffice, String lotNo, String lotDesc, String tenderer, Date Sitevisitdate, String siteVisitVal) {
        LOGGER.debug("ValidationAppRequestToBidder Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            String siteVisit = "";
            if ("yes".equalsIgnoreCase(siteVisitVal)) {
                siteVisit = "Site Visit Date and Time: " + Sitevisitdate + "<br /><br/>";
            }
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "This is to inform you that Site of the Bidder/Consultant will be visited by Tender Evaluation Committee as a part of Post Qualification process for below mentioned tender :<br/><br/>"
                    + "Id: " + tenderId + "<br/>"
                    + "Ref No: " + refNo + "<br/>"
                    + "Lot No: " + lotNo + "<br/>"
                    + "Lot Description: " + lotDesc + "<br/>"
                    + "PA Office : " + PEoffice + "<br/>"
                    + "Bidder/Consultant: " + tenderer + "<br/>"
                    + siteVisit
                    + "<br/>Thank you<br/><br />"
                    + "<b>Electronic Government Procurement (e-GP) System.</b><br />"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("ValidationAppRequestToBidder :" + e);
        }
        LOGGER.debug("ValidationAppRequestToBidder Ends:");
        return sb.toString();
    }

    public String ValidationRejRequestToBidder(int tenderId, String refNo, String Pename, String PEoffice) {
        LOGGER.debug("ValidationRejRequestToBidder Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "This is to inform you that PA has sent you a request for Validity/Security Extension for the below <br/>"
                    + "mentioned Tender :<br/><br/><br/>"
                    + "Id: " + tenderId + "<br/>"
                    + "Ref No: " + refNo + "<br/>"
                    + "PA Name: " + Pename + "<br/>"
                    + "PA Office: " + PEoffice + "<br/><br/>"
                    + "<br/>Kindly logon to Electronic Government Procurement (e-GP) system to Accept / Reject the same.<br/>"
                    + "<br/>Thank you<br/><br />"
                    + "<b>Electronic Government Procurement (e-GP) System.</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("ValidationRejRequestToBidder :" + e);
        }
        LOGGER.debug("ValidationRejRequestToBidder Ends:");
        return sb.toString();
    }

    /**
     * When Individual member completes the evaluation process and notifies TEC
     * chairperson Evaluation Committee Chairperson
     *
     * @param tenderId
     * @param refNo
     * @param peName
     * @return List Content of mail
     */
    public List evalClarification(String tenderid, String refNo, String peName) {
        LOGGER.debug("evalClarification Starts:");
        List listEval = new ArrayList();
        try {
            listEval.add("e-GP: Completion of Evaluation Process");
            listEval.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "One of the Evaluation Committee members has completed the Evaluation Process in a below mentioned Tender:<br/><br/>"
                    + "Tender ID: " + tenderid + "<br/>"
                    + "Ref No: " + refNo + "<br/>"
                    + "PA: " + peName + "<br/><br/>"
                    + "You are requested to proceed further.<br/>"
                    + "<br/>Thanks,<br/>"
                    + "<b>Electronic Government Procurement (e-GP) System</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Evaluation committee~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("evalClarification :" + e);
        }
        LOGGER.debug("evalClarification Ends:");
        return listEval;
    }

    /**
     * The user who has accessed the forget password functionality
     *
     * @param link
     * @return
     */
    public String getForgotPassMailContent(String link) {
        LOGGER.debug("getForgotPassMailContent Starts:");
        StringBuilder resetContent = new StringBuilder();
        try {
            // resetContent.append("e-GP: Forgot password");
            resetContent.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br /><br /><span>"
                    + "This Email has been sent to you as a part of your Forgot Password request. To change your password, click on the below given URL:<br/>"
                    + "<a href=\"" + link + "\">" + link + " </a><br/><br/>"
                    + "Thank you for using Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list\"</span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getForgotPassMailContent :" + e);
        }
        LOGGER.debug("getForgotPassMailContent Ends:");
        return resetContent.toString();
    }

    public String emailVerificationMail(String url) {
        LOGGER.debug("emailVerificationMail Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            //sb.append("e-GP System:  User Registration &#45; e-mail Verification and Profile Submission Information");
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'><table><tr><td>"
                    + "<br/>Dear Bidder,<br/><br/><span>"
                    + "Your email has been successfully verified.<br/><br/>"
                    + "Be informed that second stage of the registration process is <u>your Profile and mandatory documents "
                    + "submission for registration.</u> You must complete your Profile and upload "
                    + "mandatory documents within <strong> " + XMLReader.getMessage("FinalSubComplete") + " from the date of email verification on Electronic Government Procurement (e-GP) System.</strong> If you fail to complete your Profile and upload documents within <strong>" + XMLReader.getMessage("FinalSubComplete") + ",</strong>&nbsp;"
                    + "your complete records will be removed from Electronic Government Procurement (e-GP) System and you will need to re-register.<br/>"
                    //Changes Start
                    /*payment Info hide 
                    + "If you miss the first " + XMLReader.getMessage("profileSubDays") + " days but payment is made, you will be given additional  <font color='red'>" + XMLReader.getMessage("profileSubDays") + " "
                    + "calendar days</font>  for completing your profile, uploading required documents, make necessary corrections "
                    + "and submit in the Electronic Government Procurement (e-GP) system <a href='" + XMLReader.getMessage("url") + "' >" + XMLReader.getMessage("url") + "</a> using same email ID and Password.<br/><br/>"
                     */
                    + "If the profile is not submitted within additional " + XMLReader.getMessage("FinalSubComplete") + ", your profile will be <font color='red' style='font-weight: bold;'>DISABLED</font>, and <b>Bidder Registration process has to be done as a New Bidder Registration.  For this Bidder registration you will have to complete the registration form and upload required documents. In the bidder registration, you may use your same email ID or a new email ID.</b> <br/><br/>"
                    //+ "<br/>For completing your Profile and document submission, go to <a href='" + XMLReader.getMessage("url") + "' >"+XMLReader.getMessage("url")+"</a> and log into the "
                    //+ "Electronic Government Procurement (e-GP) System using your login information [i.e. email address (ID) and password you provided during registration request].<br/><br/>"
                    //Changes End
                    + "Before starting your Profile and documents submission, please read the following information and make "
                    + "sure the required authentic MANDATORY documents are ready:<br/><br/>"
                    + "1.&nbsp;&nbsp; Scan those mandatory documents you have been asked for and convert into <b>PDF</b> or <b>Zip file format</b>. Size of a single file must not exceed <strong>2 MB</strong>."
                    + "Scan the files in such a way that it can be easily readable and also ensure that the documents are virus free."
                    + "Government Procurement and Property Management Division (GPPMD) does not take any responsibility of any consequences to your future transactions in case ineligible scanned document, "
                    + "file containing virus or misrepresentation of the information in the uploaded documents.<br/><br/>"
                    + "2.&nbsp;&nbsp; Upload the mandatory credential documents for the registration. Failure to upload any one of the documents, Electronic Government Procurement (e-GP) system does not allow you to further proceed.<br/><br/>"
                    /* Edited
                    + "3.&nbsp;&nbsp;If the uploaded documents are found forged or false, your case will be treated under section 64 (Professional misconduct) of the Public Procurement Rules and Regulations 2009.<br/><br/>"
                     */
                    + "3.&nbsp;&nbsp;If the uploaded documents are found forged or false, your case will be treated under Professional Misconduct of the Procurement Rules and Regulations 2009 (Revised July 2015),  Ministry of Finance, Royal Government of Bhutan.<br/><br/>"
                    /*Physical Doucment Submission Hide
                    + "4.&nbsp;&nbsp;In case of physical submission of mandatory document, advice for physical submission of attested (by the government class-1 official) copies of scanned and "
                    + "already uploaded mandatory documents through Courier or Registered mail.<br/><br/>" */
                    + "Please <a href=\"" + XMLReader.getMessage("url") + "/MandRegDoc.jsp\" target=\"_blank\">click here</a> for list of mandatory credential documents<br/><br/>"
                    /*Payment hide 
                    + "<strong>How to make Payment for registration?</strong><br/><br/>"
                    + "You should make payment through one of the <a href=\"" + XMLReader.getMessage("url") + "/MemScheduleBank.jsp\" target=\"_blank\">the scheduled member banks of Electronic Government Procurement (e-GP) e-Payment Network</a> and make payment through Cash, Pay Order or Account to Account Transfer.In case of Account to Account Transfer, the bank will require to debit from your bank account on that bank for the User Registration in Electronic Government Procurement (e-GP)System.<br/><br/>"
                    + "For Bhutanese Bidders and Consultants, registration fee is <b>Nu. " + XMLReader.getMessage("localTenderRegFee") + "&nbsp;(" + XMLReader.getMessage("localTenderRegFeeWrd") + ")</b> and annual renewal fee is <b>Nu. " + XMLReader.getMessage("localTenderRenewFee") + " (" + XMLReader.getMessage("localTenderRenewFeeWrd") + ").</b>"
                    + "You must renew your user account in Electronic Government Procurement (e-GP) System each year before the expiry date.<br/><br/>"
                    + "For international Bidders and Consultants, registration fee is <b> USD $" + XMLReader.getMessage("intTednerRegFee") + " (" + XMLReader.getMessage("intTednerRegFeeWrd") + ")</b> "
                    + "and annual renewal fee is <b> USD $" + XMLReader.getMessage("intTednerRenewFee") + " (" + XMLReader.getMessage("intTednerRenewFeeWrd") + ").</b><br /><br />"
                     */
                    /* Hide 
                    + "<strong>Important links:</strong><br/>"
                    + "<i><a href=\"" + XMLReader.getMessage("url") + "help/manuals/eGP_NewUserRegManual_English.pdf\" target=\"_blank\">User Manual for Bidders and consultants</a><br/>"
                    + "<a href=\"" + XMLReader.getMessage("url") + "help/guidelines/eGP_Guidelines.pdf\" target=\"_blank\">e-GP Guidelines</a><br/>"
                    + "<a href=\"" + XMLReader.getMessage("url") + "MemScheduleBank.jsp\" target=\"_blank\">List of Member Scheduled Banks</a><br/>"
                    + "<a href=\"" + XMLReader.getMessage("url") + "MandRegDoc.jsp\" target=\"_blank\">List of Mandatory Documents for Registration</a><br/>"
                    + "<a href=\"" + XMLReader.getMessage("url") + "TermsNConditions.jsp\" target=\"_blank\">Terms and Conditions</a><br/>"
                    + "<a href=\"" + XMLReader.getMessage("url") + "PrivacyPolicy.jsp\" target=\"_blank\">Disclaimer and Privacy Policy</a></i><br/><br/>"
                     */
                    + "Thank you for using Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962  | <strong>Email:</strong> <a href='" + XMLReader.getMessage("emailIdHelp") + "'>" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href>" + XMLReader.getMessage("url") + "</a></div>"
                    // +"<strong>Help Desk Tel: </strong>88-02-9144252-53, Help Desk e-mail: <a href=\"mailto:helpdesk@egp.gov.bt\">helpdesk@egp.gov.bt</a>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href='\"mailto:'" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~New user registration~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("emailVerificationMail :" + e);
        }
        LOGGER.debug("emailVerificationMail Ends:");
        return sb.toString();
    }

    public String contRegistrationPayment(String amount, String currencySymbol, String ValidityYrPeriod, String ValidityYrPeriodInWords) {
        LOGGER.debug("contRegistrationPayment Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "As a part of the registration process, your Registration Fee of " + currencySymbol + amount + " has been received.<br/><br/>"
                    + "However, ONLY after satisfactory post verification (Please read <a href=\"" + URL + "FAQ.jsp\">FAQ</a> to know about post verification) of "
                    + "your uploaded mandatory credential documents by the \"GPPMD Office\", your "
                    + "\"User Registration\" will be approved. Validity of the registration is for " + ValidityYrPeriodInWords + " (" + ValidityYrPeriod + ") year from the date of approval of user registration. You must renew your registration annually, for uninterrupted service and getting access to Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "<b>What next?</b><br/><br/>"
                    + "Financial Institute Payment/Slip is one of the mandatory documents for completing your Profile details for the Registration.<br/><br/>"
                    + "To complete your Registration, you need to scan the Financial Institute Payment/Slip, and upload the scanned file "
                    + "along with other <a href=\"" + URL + "MandRegDoc.jsp\"> mandatory  documents</a> through Profile details page.<br/><br/>"
                    /* Hide 
                    + "<strong>Important links:</strong><br/>"
                    + "<i><a href=\"" + URL + "help/manuals/eGP_NewUserRegManual_English.pdf\" target=\"_blank\">User Manual for Bidders and Consultants</a><br/>"
                    + "<a href=\"" + URL + "help/guidelines/eGP_Guidelines.pdf\" target=\"_blank\">e-GP Guidelines</a><br/>"
                    + "<a href=\"" + URL + "MemScheduleBank.jsp\" target=\"_blank\">List of Member Scheduled Banks</a><br/>"
                    + "<a href=\"" + URL + "MandRegDoc.jsp\" target=\"_blank\">List of Mandatory Documents for Registration</a><br/>"
                    + "<a href=\"" + URL + "TermsNConditions.jsp\" target=\"_blank\">Terms and Conditions</a><br/>"
                    + "<a href=\"" + URL + "PrivacyPolicy.jsp\" target=\"_blank\">Disclaimer and Privacy Policy</a></i><br/><br/>"
                     */
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + " target=\"_blank\">" + XMLReader.getMessage("url") + "</a></div>"
                    // +"<strong>Help Desk Tel: </strong>88-02-9144252-53, Help Desk e-mail: <a href=\"mailto:helpdesk@egp.gov.bt\">helpdesk@egp.gov.bt</a>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list.\""
                    + "</td></tr></table>~Financial Institute payment~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contRegistrationPayment :" + e);
        }
        LOGGER.debug("contRegistrationPayment Ends:");

        return sb.toString();
    }

    public String finalSubDocMailToTenderer(String userId, String title, String username) throws Exception {
        LOGGER.debug("finalSubDocMailToTenderer Starts:");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        java.util.Date d = new Date();
        d.setDate(d.getDate() + 14);

        StringBuffer sb = new StringBuffer();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear " + title + " " + username + "," + "<br/><span><br/>"
                    + "You have successfully submitted the following documents:<br/><br/>");
            sb.append("<table width='100%' cellspacing='0' cellpadding='0' border='0' style='color:#333; border-collapse:collapse; '>"
                    + "<tr>"
                    + "<th style='width:4%;border:1px solid #DAA520; border-bottom:1px solid #DAA520; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Sl. No.</th>"
                    + "<th style='width:37%;border:1px solid #DAA520; border-bottom:1px solid #DAA520; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>File Name</th>"
                    + "<th style='width:44%;border:1px solid #DAA520; border-bottom:1px solid #DAA520; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Description</th>"
                    + "<th style='width:15%;border:1px solid #DAA520; border-bottom:1px solid #DAA520; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>File Size (In KB)</th>"
                    + "</tr>");
            int count = 1;
            UserRegisterService service = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
            java.util.List<TblCompanyDocuments> tblCompanyDocuments = contentAdminService.findCompanyDocumentses("tblTendererMaster", Operation_enum.EQ, new TblTendererMaster(Integer.parseInt(service.getMaterTendererId(userId))), "documentTypeId", Operation_enum.NE, "briefcase");
            List<TblMandatoryDoc> list = service.getMandatoryDocs(userId, false, false);
            for (TblCompanyDocuments ttcd : tblCompanyDocuments) {
                sb.append("<tr>"
                        + "<td align='center' style='border:1px solid #FF9326; text-align: center; vertical-align: top; padding:5px;'>" + count + "</td>"
                        + "<td align='center' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + ttcd.getDocumentName() + "</td>");
                for (TblMandatoryDoc doc : list) {
                    if (doc.getDocTypeValue().equals(ttcd.getDocumentTypeId())) {
                        sb.append("<td align='center' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + doc.getDocType() + "</td>");
                    }
                }
                DecimalFormat twoDForm = new DecimalFormat("#.##");
                sb.append("<td align='center' style='border:1px solid #FF9326; text-align: center; vertical-align: top; padding:5px;'>" + twoDForm.format(Double.parseDouble(ttcd.getDocumentSize()) / 1024) + "</td>"
                        + "</tr>");
                count++;
            }
            sb.append("</table><br/>"
                    /*hide
                    + "<br/>Be informed that the physical submission of <span style='color:red;'>attested (by the government class-1 official) copies of scanned and already uploaded documents</span> "
                    + "must reach to following address through <span style='color:red;'>post mail or courier</span> by " + dateFormat.format(d) + "<br/><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                     */
                    + "The GPPMD will notify you about the Approval or Rejection of Bidder Registration "
                    /*Hide 
                    + "within 21 days of receipt of your physical attested documents.<br/><br/>" */
                    + "Thank you for using Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + " >" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    // +"<strong>Help Desk Tel: </strong>88-02-9144252-53, Help Desk e-mail: <a href=\"mailto:helpdesk@egp.gov.bt\">helpdesk@egp.gov.bt</a>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("finalSubDocMailToTenderer :" + e);
        }
        LOGGER.debug("finalSubDocMailToTenderer Ends:");

        return sb.toString();
    }

    /**
     * Register PE Admin PE Admin User
     *
     * @param loginId
     * @param password
     * @param officeDetails
     * @return List
     */
    public List getAdminandGovtUserMailContent(String loginId, String password, Object[] officeDetails) {
        LOGGER.debug("getAdminandGovtUserMailContent Starts:");
        List loginContent = new ArrayList();
        try {
            loginContent.add("e-GP – Your account details");
            loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "Your Procuring Agency's profile has been created in the Electronic Government Procurement (e-GP) System. "
                    + "Following are the login credentials:<br/><br />"
                    + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                    + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                    + "<br/>&nbsp;<b>Password:</b> " + password
                    + "<br/>&nbsp;<b>Website:</b>&nbsp;&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mentioned steps to change the password:"
                    + "<ol><li>Type a new Password.</li>"
                    + "<li>Retype the same Password.</li>"
                    + "<li>Select a Hint Question </li>"
                    + "<li>Type a Hint Answer.  </li></ol><br/>"
                    + "<b>" + officeDetails[1] + "</b><br /><br />"
                    + "<b>" + officeDetails[2] + "</b><br />"
                    + officeDetails[3] + ",<br />"
                    + officeDetails[4] + ",<br />"
                    + officeDetails[6] + ",<br />"
                    + "Bhutan - " + officeDetails[5] + "<br /><br />"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getAdminandGovtUserMailContent :" + e);
        }
        LOGGER.debug("getAdminandGovtUserMailContent Ends:");
        return loginContent;
    }

    /**
     * Register Development Partner Admin Development Partner Admin
     *
     * @param loginId
     * @param password
     * @return List
     */
    public List getscBankDevPartnerAdminRegContent(String loginId, String password) {
        LOGGER.debug("getscBankDevPartnerAdminRegContent Starts:");
        List loginContent = new ArrayList();
        try {
            loginContent.add("e-GP – Your account details");
            loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "Your Development Partner Admin profile has been created in the Electronic Government Procurement (e-GP) System. "
                    + "Following are the login credentials:<br/><br />"
                    + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                    + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                    + "<br/>&nbsp;<b>Password:</b> " + password
                    + "<br/>&nbsp;<b>Website:</b>&nbsp;&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be "
                    + "redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mentioned steps to change the password:"
                    + "<ol><li>Type a new Password.</li>"
                    + "<li>Retype the same Password.</li>"
                    + "<li>Select a Hint Question </li>"
                    + "<li>Type a Hint Answer.  </li></ol><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getscBankDevPartnerAdminRegContent :" + e);
        }
        LOGGER.debug("getscBankDevPartnerAdminRegContent Ends:");
        return loginContent;
    }

    /**
     * Register Register Scheduled Bank Admin Scheduled BankAdmin User
     *
     * @param loginId
     * @param password
     * @return List
     */
    public List getscBankAdminRegContent(String loginId, String password) {
        LOGGER.debug("getscBankAdminRegContent Starts:");
        List loginContent = new ArrayList();
        try {
            loginContent.add("e-GP – Your account details");
            loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "Your e-Payment Service Provider Admin profile has been created in the Electronic Government Procurement (e-GP) System."
                    + "Following are the login credentials:<br/><br />"
                    + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                    + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                    + "<br/>&nbsp;<b>Password:</b> " + password
                    + "<br/>&nbsp;<b>Website:</b>&nbsp;&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be "
                    + "redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mentioned steps to change the password:"
                    + "<ol><li>Type a new Password.</li>"
                    + "<li>Retype the same Password.</li>"
                    + "<li>Select a Hint Question </li>"
                    + "<li>Type a Hint Answer.  </li></ol><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Financial Institute payment~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("getscBankAdminRegContent :" + e);
        }
        LOGGER.debug("getscBankAdminRegContent Ends:");

        return loginContent;
    }

    /**
     * Register Content Admin Content Admin
     *
     * @param loginId
     * @param password
     * @return List
     */
    public List getconAdminRegContent(String loginId, String password, Byte userTypeId) {
        LOGGER.debug("getconAdminRegContent Starts:");
        List loginContent = new ArrayList();
        try {
            String strProfile = "Your Content Admin profile has been created in the Electronic Government Procurement (e-GP) System.";
            if (userTypeId == 19) {
                strProfile = "Your O & M Admin profile has been created in the Electronic Government Procurement (e-GP) System.";
            } else if (userTypeId == 20) {
                strProfile = "Your O & M User profile has been created in the Electronic Government Procurement (e-GP) System.";
            }

            loginContent.add("e-GP – Your account details");
            loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + strProfile
                    + "Following are the login credentials:<br/><br />"
                    + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                    + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                    + "<br/>&nbsp;<b>Password:</b> " + password
                    + "<br/>&nbsp;<b>Website:</b>&nbsp;&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be "
                    + "redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mentioned steps to change the password:"
                    + "<ol><li>Type a new password.</li>"
                    + "<li>Retype the same password.</li>"
                    + "<li>Select a Hint Question </li>"
                    + "<li>Type a Hint answer.  </li></ol><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getconAdminRegContent :" + e);
        }
        LOGGER.debug("getconAdminRegContent Ends:");
        return loginContent;
    }

    /**
     * Register Development Partner User Development Partner User who is
     * registered
     *
     * @param loginId
     * @param password
     * @return List
     */
    public List getGovtDevelopmentContent(String loginId, String password) {
        LOGGER.debug("getGovtDevelopmentContent Starts:");
        List loginContent = new ArrayList();
        try {
            loginContent.add("e-GP – Your account details");
            loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "Your profile has been created in the Electronic Government Procurement (e-GP) System.Following are the login credentials:<br/><br />"
                    + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                    + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                    + "<br/>&nbsp;<b>Password:</b> " + password
                    + "<br/>&nbsp;<b>Website:</b>&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be "
                    + "redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mentioned steps to change the password:"
                    + "<ol><li>Type a new Password.</li>"
                    + "<li>Retype the same Password.</li>"
                    + "<li>Select a Hint Question </li>"
                    + "<li>Type a Hint Answer.  </li></ol><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getGovtDevelopmentContent :" + e);
        }
        LOGGER.debug("getGovtDevelopmentContent Ends:");
        return loginContent;
    }

    /**
     * Register Scheduled Bank User / Transfer of a user Scheduled Bank User who
     * is registered / The user, who is transferred
     *
     * @param loginId
     * @param password
     * @return List
     */
    public List getGovtScheduleBankContent(String loginId, String password) {
        LOGGER.debug("getGovtScheduleBankContent Starts:");
        List loginContent = new ArrayList();
        try {
            loginContent.add("e-GP – Your account details");
            loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "Your Schedule bank user profile has been created in the Electronic Government Procurement (e-GP) System."
                    + "Following are the login credentials:<br/><br />"
                    + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                    + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                    + "<br/>&nbsp;<b>Password:</b> " + password
                    + "<br/>&nbsp;<b>Website:</b>&nbsp;&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be "
                    + "redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mentioned steps to change the password:"
                    + "<ol><li>Type a new Password.</li>"
                    + "<li>Retype the same Password.</li>"
                    + "<li>Select a Hint Question </li>"
                    + "<li>Type a Hint Answer.  </li></ol>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getGovtScheduleBankContent :" + e);
        }
        LOGGER.debug("getGovtScheduleBankContent Ends:");
        return loginContent;
    }

    /**
     * When PE Is Created
     *
     * @param loginId
     * @param password
     * @param officeDetails
     * @return List of mailContent
     */
    public List getGovtPEContent(String loginId, String password, List<Object[]> officeDetails) {
        LOGGER.debug("getGovtPEContent Starts:");
        List loginContent = new ArrayList();
        StringBuilder officeContent = new StringBuilder();
        try {
            officeContent.append("<b>" + officeDetails.get(0)[5] + "</b><br /><br />");

            for (int i = 0; i < officeDetails.size(); i++) {
                officeContent.append("<b>" + officeDetails.get(i)[1] + "</b><br />"
                        + officeDetails.get(i)[2] + ",<br />"
                        + officeDetails.get(i)[3] + ",<br />"
                        + officeDetails.get(i)[6] + ",<br />"
                        + "Bhutan -" + officeDetails.get(i)[4] + "<br />");
            }

            loginContent.add("e-GP – Your account details");
            loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "Your Procuring Agency's profile has been created in the Electronic Government Procurement (e-GP) System."
                    + "Following are the login credentials:<br/><br />"
                    + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                    + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                    + "<br/>&nbsp;<b>Password:</b> " + password
                    + "<br/>&nbsp;<b>Website:</b>&nbsp;&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be "
                    + "redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mentioned steps to change the password:"
                    + "<ol><li>Type a new Password.</li>"
                    + "<li>Retype the same Password.</li>"
                    + "<li>Select a Hint Question </li>"
                    + "<li>Type a Hint Answer.  </li></ol><br/><br/>"
                    + officeContent.toString() + "<br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=\"mailto:" + XMLReader.getMessage("emailIdHelp") + "\">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring angencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getGovtPEContent :" + e);
        }
        LOGGER.debug("getGovtPEContent Ends:");
        return loginContent;
    }

    /**
     * When TEC/TOC User created
     *
     * @param loginId
     * @param password
     * @param procRole
     * @param officeDetails
     * @return List mailContent
     */
    public List getGovtTECTOCMailContent(String loginId, String password, String procRole, List<Object[]> officeDetails) {
        LOGGER.debug("getGovtTECTOCMailContent Ends:");
        List loginContent = new ArrayList();
        StringBuilder officeContent = new StringBuilder();
        try {
            for (int i = 0; i < officeDetails.size(); i++) {
                officeContent.append("<b>" + officeDetails.get(i)[1] + "</b><br />"
                        + officeDetails.get(i)[2] + ",<br />"
                        + officeDetails.get(i)[3] + ",<br />"
                        + officeDetails.get(i)[6] + ",<br />"
                        + "Bhutan -" + officeDetails.get(i)[4] + "<br />");
            }

            loginContent.add("e-GP – Your account details");
            loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "Your profile has been created in the Electronic Government Procurement (e-GP) System as a member of " + procRole
                    + "Following are the login credentials:<br/><br />"
                    + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                    + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                    + "<br/>&nbsp;<b>Password:</b> " + password
                    + "<br/>&nbsp;<b>Website:</b>&nbsp;&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be "
                    + "redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mentioned steps to change the password:"
                    + "<ol><li>Type a new Password.</li>"
                    + "<li>Retype the same Password.</li>"
                    + "<li>Select a Hint Question </li>"
                    + "<li>Type a Hint Answer.  </li></ol><br/><br/>"
                    + officeContent.toString() + "<br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getGovtTECTOCMailContent :" + e);
        }
        LOGGER.debug("getGovtTECTOCMailContent Ends:");
        officeContent.append("<b>" + officeDetails.get(0)[5] + "</b><br /><br />");

        for (int i = 0; i < officeDetails.size(); i++) {
            officeContent.append("<b>" + officeDetails.get(i)[1] + "</b><br />"
                    + officeDetails.get(i)[2] + ",<br />"
                    + officeDetails.get(i)[3] + ",<br />"
                    + officeDetails.get(i)[6] + ",<br />"
                    + "Bhutan -" + officeDetails.get(i)[4] + "<br />");
        }

        loginContent.add("e-GP – Your account details");
        loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                + "Dear User," + "<br/><br/><span>"
                + "Your profile has been created in the Electronic Government Procurement (e-GP) System as a member of " + procRole
                + "Following are the login credentials:<br/><br />"
                + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                + "<br/>&nbsp;<b>Password:</b> " + password
                + "<br/>&nbsp;<b>Website:</b>&nbsp;&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be "
                + "redirected to the 'Change Password Screen'.<br/><br/>"
                + "Please follow the below mentioned steps to change the password:"
                + "<ol><li>Type a new Password.</li>"
                + "<li>Retype the same Password.</li>"
                + "<li>Select a Hint Question </li>"
                + "<li>Type a Hint Answer.  </li></ol><br/><br/>"
                + officeContent.toString() + "<br />"
                + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                + "</td></tr></table></body></html>");

        return loginContent;

    }

    /**
     * Government User Created not pe and tec/toc
     *
     * @param loginId
     * @param password
     * @param officeDetails
     * @return List mailContent
     */
    public List getGovtOtherMailContent(String loginId, String password, List<Object[]> officeDetails) {
        LOGGER.debug("getGovtOtherMailContent Starts:");
        List loginContent = new ArrayList();
        StringBuilder officeContent = new StringBuilder();

        try {
            loginContent.add("e-GP – Your account details");
            officeContent.append("<b>" + officeDetails.get(0)[5] + "</b><br /><br />");
            for (int i = 0; i < officeDetails.size(); i++) {
                officeContent.append("<b>" + officeDetails.get(i)[1] + "</b><br />"
                        + officeDetails.get(i)[2] + ",<br />"
                        + officeDetails.get(i)[3] + ",<br />"
                        + officeDetails.get(i)[6] + ",<br />"
                        + "Bhutan -" + officeDetails.get(i)[4] + "<br />");
            }
            loginContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "Your profile has been created in the Electronic Government Procurement (e-GP) System. Following are the login credentials:<br /><br />"
                    + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                    + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                    + "<br/>&nbsp;<b>Password:</b> " + password
                    + "<br/>&nbsp;<b>Website:</b>&nbsp;&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be "
                    + "redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mentioned steps to change the password:"
                    + "<ol><li>Type a new Password.</li>"
                    + "<li>Retype the same Password.</li>"
                    + "<li>Select a Hint Question </li>"
                    + "<li>Type a Hint Answer.  </li></ol><br/><br/>"
                    + officeContent.toString() + "<br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getGovtOtherMailContent :" + e);
        }
        LOGGER.debug("getGovtOtherMailContent Ends:");

        return loginContent;
    }

    public String docReceiptMailContent(String userId) {
        LOGGER.debug("docReceiptMailContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/>"
                    + "This is to inform you that we have received attested copies of supporting documents required as part of user registration.<br/><br/>"
                    + "We will verify the same and on the basis of supporting documents received, we will accept or reject your profile and will inform you accordingly.<br/><br/>");
            sb.append("Thank you for using Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list\"</span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("docReceiptMailContent :" + e);
        }
        LOGGER.debug("docReceiptMailContent Ends:");
        return sb.toString();
    }

    public List evalAppToAa(String tenderid) {
        List listEval = new ArrayList();
        try {
            listEval.add("e-GP: Queries posted by Tender Evaluation Committee Member");
            listEval.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that TEC has sought Queries with<br/>respect to tender received for below mentioned <br/> tender id as a part of evaluation process:<br/><br/><br/>"
                    + "Id: " + tenderid + "<br/>"
                    + "<br/>Warm Regards,<br/>"
                    + "<b>Electronic Government Procurement (e-GP) Support Team.</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Evaluation committee~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("regRenewalContent :" + e);
        }
        LOGGER.debug("regRenewalContent Ends:");

        return listEval;
    }

    public String regRenewalContent(String expiryDate) {
        LOGGER.debug("regRenewalContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'>"
                    + "<br/>Dear User," + "<br/><br/>"
                    + "This is to inform you that your Electronic Government Procurement (e-GP) account will expire on " + expiryDate + ". Kindly renew "
                    + "your registration before the expiry date mentioned above for uninterupted Electronic Government Procurement (e-GP) System access.<br/><br/>"
                    + "To renew your registration, you can pay renewal fee through any of the <b><i><a target='_blank' href='" + URL + "MemScheduleBank.jsp'>member scheduled banks.</a></i></b><br /><br />");

            sb.append("<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list\"</span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("regRenewalContent :" + e);
        }
        LOGGER.debug("regRenewalContent Ends:");
        return sb.toString();
    }

    public String regRenewalLapsContent(String expiryDate) {
        LOGGER.debug("regRenewalContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'>"
                    + "<br/>Dear User," + "<br/><br/>"
                    + "This is to inform you that your Electronic Government Procurement (e-GP) account was expired on " + expiryDate + ". Your renewal period has been lapsed  and can’t be renewed again. Please register as a new user with new email id<br/><br/>");

            sb.append("<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list\"</span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("regRenewalContent :" + e);
        }
        LOGGER.debug("regRenewalContent Ends:");
        return sb.toString();
    }

    public String contTOS_POS_SentToTEC_CP(String tenderId, String refNo, String peEmail, String peOffice) {
        LOGGER.debug("contTOS_POS_SentToTEC_CP Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "PA has sent TOS/POS report for the below mentioned tender:<br/><br/>"
                    + "Tender ID : " + tenderId + "<br/>"
                    + "Reference No : " + refNo + "<br/>"
                    + "PA : " + peOffice + "<br/>"
                    + "e-mail ID of PA : " + peEmail + "<br/><br/>"
                    + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "<br/>Thanks,<br/>"
                    + "<b>Electronic Government Procurement (e-GP) System</b><br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contTOS_POS_SentToTEC_CP :" + e);
        }
        LOGGER.debug("contTOS_POS_SentToTEC_CP Ends:");
        return sb.toString();
    }

    public String contTOS_POS_SentToPE(String tenderId, String refNo, String email_TOC_CP, String officePE, String Organization) {
        LOGGER.debug("getExternalUserMailContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Opening Committee chairperson has sent the TOS / POS for the below mentioned tender::<br/><br/>"
                    + "Tender ID : " + tenderId + "<br/>"
                    + "Reference No : " + refNo + "<br/>"
                    + "PA : " + officePE + "<br/>"
                    + "Organization : " + Organization + "<br/><br/>"
                    + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "<br/>Thanks,<br/>"
                    + "<b>Electronic Government Procurement (e-GP) System</b><br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getExternalUserMailContent :" + e);
        }
        LOGGER.debug("getExternalUserMailContent Ends:");
        return sb.toString();
    }

    public String getExternalUserMailContent(String loginId, String password) {
        LOGGER.debug("getExternalUserMailContent Starts:");
        StringBuilder loginContent = new StringBuilder();
        try {
            loginContent.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "Your profile has been created in the Electronic Government Procurement (e-GP) System. Following are the login credentials.<br/><br />"
                    + "<table width='100%' cellpadding='0' cellspacing='0' style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'>"
                    + "<tr><td style='padding-left:5px;padding-right:5px;padding-top:8px;padding-bottom:8px;'>&nbsp;<b>Login ID:</b>&nbsp;&nbsp;" + loginId
                    + "<br/>&nbsp;<b>Password:</b> " + password
                    + "<br/>&nbsp;<b>Website:</b>&nbsp;&nbsp;<a href=\"" + URL + "\">" + URL + "</a></td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mention steps to change the password:<br/>"
                    + "<ol><li> Type a new Password.</li>"
                    + "<li> Retype the same Password.</li>"
                    + "<li> Select a Hint Question</li>"
                    + "<li> Type a Hint Answer.  </li></ol><br/>"
                    + "Thank you.<br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a>"
                    + "email address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getExternalUserMailContent :" + e);
        }
        LOGGER.debug("getExternalUserMailContent Ends:");
        return loginContent.toString();
    }

    /**
     * When TEC Chairperson requests to the PE for formation of Technical Sub
     * Committee (TSC) PE user of the tender
     *
     * @param tenderId
     * @param refNo
     * @return List Content of mail
     */
    public String contTSCFormationRequest(String tenderId, String refNo) {
        LOGGER.debug("contTSCFormationRequest starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that the TEC Chair person has requested TSC formation"
                    + "for below mentioned tender:<br/><br/><br/>"
                    + "Id: " + tenderId + "<br/>"
                    + "Ref No: " + refNo + "<br/><br/>"
                    + "<br/>Warm Regards,<br/>"
                    + "<b>Electronic Government Procurement (e-GP) Support Team.</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contTSCFormationRequest :" + e);
        }
        LOGGER.debug("contTSCFormationRequest Ends:");
        return sb.toString();
    }

    public String contRegistrationRenewal(String amount, String currencySymbol, String ValidityYrPeriod, String ValidityYrPeriodInWords, String renewalDt, String expiryDt) {
        LOGGER.debug("contRegistrationRenewal Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that we have received your Electronic Government Procurement (e-GP) account renewal fee " + currencySymbol + amount + " for " + ValidityYrPeriod + "Year(s) validity on " + renewalDt + ". You need to renew your account again before " + expiryDt
                    + "<br/><br/>"
                    + "Thank you for using Electronic Government Procurement (e-GP) System."
                    + "<br/><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    // +"<strong>Help Desk Tel: </strong>88-02-9144252-53, Help Desk e-mail: <a href=\"mailto:helpdesk@egp.gov.bt\">helpdesk@egp.gov.bt</a>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list.\""
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contRegistrationRenewal :" + e);
        }
        LOGGER.debug("contRegistrationRenewal Ends:");
        return sb.toString();
    }

    /**
     * Evaluation type configuration by Evaluation Committee Chair Person (CP)
     * All Evaluation Committee members except CP
     *
     * @param configType
     * @param tenderId
     * @param refNo
     * @param selectedMember
     * @param peEmail
     * @param peOffice
     * @return List Content of mail
     */
    public String contConfigurationNotification(String configType, String tenderId, String refNo, String selectedMember, String peEmail, String peOffice) {
        LOGGER.debug("contConfigurationNotification Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            if ("Team".equalsIgnoreCase(configType)) {
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                        + "<br/>Dear User," + "<br/><br/><span>"
                        + "This is to inform you that Evaluation Committee Chairperson has configured Team Evaluation in evaluation type for below mentioned tender and hence your consent is required to allow nominated member to start evaluation process.<br/><br/><br/>"
                        + "Tender ID : " + tenderId + "<br/>"
                        + "Reference No : " + refNo + "<br/>"
                        + "PA : " + peOffice + "<br/>"
                        + "e-mail ID of PA : " + peEmail + "<br/><br/>"
                        + "For more details, Kindly logon to e-GP System and follow below mentioned steps to go to Evaluation dashboard:<br/><br/>"
                        + "-Logon to Electronic Government Procurement (e-GP) System<br/>"
                        + "-Click on Evaluation menu<br/>"
                        + "-Click on Evaluation committee sub menu<br/>"
                        + "-Click on Dashboard of the tender<br/>"
                        + "<br/>Thanks,<br/>"
                        + "<b>Electronic Government Procurement (e-GP) System</b><br/><br/>"
                        + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                        + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br /><br />"
                        + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                        + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                        + "</td></tr></table>~Evaluation committee~~</body></html>");
            } else {
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                        + "<br/>Dear User," + "<br/><br/><span>"
                        + "This is to inform you that Evaluation Committee Chairperson has configured evaluation methodology in below mentioned tender and you are requested to start evaluation process.<br/><br/><br/>"
                        + "Tender ID : " + tenderId + "<br/>"
                        + "Reference No : " + refNo + "<br/>"
                        + "PA : " + peOffice + "<br/>"
                        + "e-mail ID of PA : " + peEmail + "<br/><br/>"
                        + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System and follow below mentioned steps to go to Evaluation dashboard:<br/><br/>"
                        + "-Logon to Electronic Government Procurement (e-GP) System<br/>"
                        + "-Click on Evaluation menu<br/>"
                        + "-Click on Evaluation committee sub menu<br/>"
                        + "-Click on Dashboard of the tender<br/>"
                        + "<br/>Thanks,<br/>"
                        + "<b>Electronic Government Procurement (e-GP) System</b><br/><br/>"
                        + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                        + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br /><br />"
                        + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                        + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                        + "</td></tr></table>~Evaluation committee~~</body></html>");
            }
        } catch (Exception e) {
            LOGGER.error("contConfigurationNotification :" + e);
        }
        LOGGER.debug("contConfigurationNotification Ends:");
        return sb.toString();
    }

    public String contConfigurationModification(String configType, String tenderId, String refNo, String selectedMember, String peEmail, String peOffice) {
        LOGGER.debug("contConfigurationModification Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Evaluation Committee Chairperson has configured evaluation methodology in below mentioned tender and you are requested to start evaluation process.<br/><br/><br/>"
                    + "Tender ID : " + tenderId + "<br/>"
                    + "Reference No : " + refNo + "<br/>"
                    + "PA : " + peOffice + "<br/>"
                    + "e-mail ID of PA : " + peEmail + "<br/><br/>"
                    + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System and follow below mentioned steps to go to Evaluation dashboard:<br/><br/>"
                    + "-Logon to Electronic Government Procurement (e-GP) System<br/>"
                    + "-Click on Evaluation menu<br/>"
                    + "-Click on Evaluation committee sub menu<br/>"
                    + "-Click on Dashboard of the tender<br/>"
                    + "<br/>Thanks,<br/>"
                    + "<b>Electronic Government Procurement (e-GP) System</b><br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br /><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Evaluation committee~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contConfigurationModification :" + e);
        }
        LOGGER.debug("contConfigurationModification Ends:");
        return sb.toString();
    }

    public String getLoginMailContentMultipleUser(TblLoginMaster loginMaster, String password, Object[] obj) {
        LOGGER.debug("getLoginMailContentMultipleUser Starts:");
        StringBuilder loginContent = new StringBuilder();
        try {
            loginContent.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "Welcome to the Electronic  Government Procurement (e-GP) System of the Royal Government of Bhutan.<br/><br/>"
                    + "Your e-mail ID has been registered on Electronic Government Procurement (e-GP) System by your Company Admin. Following is your Login Account information:<br/><br/>"
                    + "<b>Email / Login ID:</b>&nbsp;&nbsp;" + loginMaster.getEmailId() + "<br/>"
                    + "<b>Password:</b>&nbsp;&nbsp;" + password + "<br/>"
                    + "<b>Website:</b>&nbsp;&nbsp;" + XMLReader.getMessage("url") + "<br/><br/>"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be redirected to the 'Change Password Screen'.<br/><br/>"
                    + "Please follow the below mention steps to change the password:<br/>"
                    + "<ol><li> Type a new password.</li>"
                    + "<li> Retype the same password.</li>"
                    + "<li> Select a Hint Question</li>"
                    + "<li> Type a Hint answer. </li></ol><br/>"
                    + "In case of any query in this regard, you may contact your Company’s Admin on below given contact details:<br/><br/>"
                    + "<b>Name of the Organization:</b>&nbsp;&nbsp;" + obj[0] + "<br/>"
                    + "<b>Contact Person:</b>&nbsp;&nbsp;" + obj[2] + " " + obj[3] + "<br/><br/>"
                    + "<b>e-mail ID:</b>&nbsp;&nbsp;" + obj[1] + "<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list\"</span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getLoginMailContentMultipleUser :" + e);
        }
        LOGGER.debug("getLoginMailContentMultipleUser Ends:");
        return loginContent.toString();
    }

    /**
     * When secondary partner accepts or rejects the JVCA invitation To
     * Nominated and other partners
     *
     * @param partnerNamed
     * @param Status
     * @return
     */
    public List JvcaStatusUpdate(String partnerNamed, String Status) {
        LOGGER.debug("JvcaStatusUpdate Starts:");
        List listJvca = new ArrayList();
        try {
            listJvca.add("JVCA status update");
            listJvca.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear Sir," + "<br/><br/><span>"
                    + "This is to inform you that " + partnerNamed + "  has " + Status + "<br/> for the proposed JVCA.<br/><br/><br/>"
                    + "<br/>Thank you for using Electronic Government Procurement (e-GP) System.<br/><br />"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email address of GPPMD Office to your Safe Sender list </span>"
                    + "</td></tr></table>~Joint Venture~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("JvcaStatusUpdate :" + e);
        }
        LOGGER.debug("JvcaStatusUpdate Ends:");

        return listJvca;
    }

    public String peClariToTenderer(String peOffice, String respDate) {
        LOGGER.debug("peClariToTenderer starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that PA has sought clarification for debarment. You are requested to respond to the PE before last date of response mentioned below:<br/><br/>"
                    + "<b>PE&nbsp;&nbsp;:</b>&nbsp;" + peOffice + "<br/>"
                    + "<b>Last Date of Response&nbsp;&nbsp;:</b>&nbsp;" + respDate + "<br/><br/>"
                    + "For more details, Please logon to Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "Thanks.<br/><br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");

        } catch (Exception e) {
            LOGGER.error("peClariToTenderer :" + e);
        }
        LOGGER.debug("peClariToTenderer Ends:");
        return sb.toString();
    }

    public String tendererResponseToPE(String tendererName) {
        LOGGER.debug("tendererResponseToPE Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that below mentioned Bidder/consultant has responded to your debarment related queries:<br/><br/>"
                    + "<b>Name of Bidder / Consultant &nbsp;&nbsp;:</b>&nbsp;" + tendererName + "<br/><br/>"
                    + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "Thanks.<br/><br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("tendererResponseToPE :" + e);
        }
        LOGGER.debug("tendererResponseToPE Ends:");
        return sb.toString();
    }

    public String debarmentRequestToHope(String peName, String tendererName) {
        LOGGER.debug("debarmentRequestToHope Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that below mentioned PA has sent a debarment request for below mentioned Bidder/Consultant:<br/><br/>"
                    + "<b>Name of PA&nbsp;&nbsp;:</b>&nbsp;" + peName + "<br/><br/>"
                    + "<b>Name of Bidder / Consultant&nbsp;&nbsp;:</b>&nbsp;" + tendererName + "<br/><br/>"
                    + "You are requested to logon to Electronic Government Procurement (e-GP) system and process this request as early as possible.<br/><br/>"
                    + "Thanks.<br/><br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("debarmentRequestToHope :" + e);
        }
        LOGGER.debug("debarmentRequestToHope Ends:");
        return sb.toString();
    }

    public String hopeFormsDC(String hope, String org, String tendererName, String comName) {
        LOGGER.debug("hopeFormsDC Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that you have been selected as a member of below mentioned Debarment Committee:<br/><br/>"
                    + "<b>Debarment Committee&nbsp;&nbsp;:</b>&nbsp;" + comName + "<br/><br/>"
                    + "<b>HOPA&nbsp;&nbsp;:</b>&nbsp;" + hope + "<br/><br/>"
                    + "<b>Organization&nbsp;&nbsp;:</b>&nbsp;" + org + "<br/><br/>"
                    + "<b>Name of Bidder / Consultant&nbsp;&nbsp;:</b>&nbsp;" + tendererName + "<br/><br/>"
                    + "You are requested to process this debarment request as early as possible. Kindly logon to Electronic Government Procurement (e-GP) System for more details.<br/><br/>"
                    + "Thanks.<br/><br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("hopeFormsDC :" + e);
        }
        LOGGER.debug("hopeFormsDC Ends:");
        return sb.toString();
    }

    public String hopeNotifiesForDebarment(String tendererName) {
        LOGGER.debug("hopeNotifiesForDebarment Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that hopa has requested you to debar a below mentioned Bidder / consultant:<br/><br/>"
                    + "<b>Name of Bidder / Consultant&nbsp;&nbsp;:</b>&nbsp;" + tendererName + "<br/><br/>"
                    + "For more details, Please logon to Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "Thanks.<br/><br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");

        } catch (Exception e) {
            LOGGER.error("hopeNotifiesForDebarment :" + e);
        }
        LOGGER.debug("hopeNotifiesForDebarment Ends:");
        return sb.toString();
    }

    public String debaresTCJ(String debaredFor, String details) {
        StringBuilder sb = new StringBuilder();
        LOGGER.debug("debaresTCJ Starts:");
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that you have been debarred by the e-GP Admin on Electronic Government Procurement (e-GP) System. Debarment detail is as mentioned below:<br/><br/>"
                    + "<b>Debared for &nbsp;&nbsp;:</b>&nbsp;" + debaredFor + "<br/><br/>"
                    + "<b>Detail &nbsp;&nbsp;:<br/><br/>"
                    + details + "<br/><br/>"
                    + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "Thanks.<br/><br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("debaresTCJ :" + e);
        }
        LOGGER.debug("debaresTCJ Ends:");
        return sb.toString();
    }

    /**
     * When a branch checker verifies the document fees payment detail or When a
     * branch checker verifies the tender security payment detail or When a
     * branch checker verifies the performance security payment detail Tenderer
     * who has paid performance security
     *
     * @param tenderid
     * @param refNo
     * @param paymentFor
     * @param status
     * @param action
     * @param companyName
     * @param bankName
     * @param branchName
     * @param amount
     * @return
     */
    public String contTenderPaymentVerification(String tenderid, String refNo, String paymentFor, String status, String action, String companyName, String bankName, String branchName, String amount) {
        LOGGER.debug("contTenderPaymentVerification Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that payment for " + paymentFor + " has been received for below mentioned tender:<br/><br/>"
                    + "Tender Id : " + tenderid + "<br/>"
                    + "Reference No : " + refNo + "<br/>"
                    + "Amount : " + amount + "<br/>"
                    + "Bank Name : " + bankName + "<br/>"
                    + "Branch Name: " + branchName + "<br/><br/>"
                    + "<br/>Thanks."
                    + "<br/>Electronic Government Procurement (e-GP) System<br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a> </b></div>"//| <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table>~Bank payment~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contTenderPaymentVerification :" + e);
        }
        LOGGER.debug("contTenderPaymentVerification Ends:");
        return sb.toString();
    }
    //mail content for noa - When PE issues NOA to the tenderer

    /**
     * When PE issues NOA to the concern Tenderer Tenderer to whom the NOA is
     * issued
     *
     * @param tenderId
     * @param refNo
     * @param PEName
     * @param lastDate
     * @param lotNo
     * @param lotDesc
     * @return String
     */
    public String getNOAMailContent(String procurementNature, BigDecimal amountInNumber, String amountInWords, String bidDatedTo, String hopaName, String bidderName, String bidderAddress,String tenderId, String refNo, String PEName, String lastDate, String lotNo, String lotDesc) {
        LOGGER.debug("getNOAMailContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            //Nitish Start
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>To," + "<br/><br/><span>"
                    + "Name: "+bidderName+"<br/><br/><span>"
                    + "Address: "+bidderAddress+"<br/><br/><span>"
                    + "This is to notify you that your Bid dated on "+bidDatedTo+" for the "+procurementNature+" for "+lotDesc+" for the Contract Price of the equivalent of<br/><br/> Nu. "+amountInNumber+" ("+amountInWords+") "
                    + "in BTN, as corrected and modified in accordance with the Instructions to Bidders"+"<br/><br/><span>"+"is hereby accepted by our Agency."+"<br/><br/><br/><span>" 
                    + "The Contract in duplicate is attached hereto. You are hereby instructed to:"+"<br/><br/><br/><span>" 
                    + "(a) confirm your acceptance of this Letter of Acceptance by signing and dating both copies of it, and returning one copy to us no later than (15) days from the date hereof; and"+"<br/><br/><span>"
                    + "(b) forward the Performance Security pursuant to ITB Sub-Clause 47.1, i.e., within (15) days after receipt of this Letter of Acceptance, and pursuant to GCC Sub-Clause 19.1"+"<br/><br/><br/><br/><span>"
                    + "<br/>Authorized Signature.<br/>"
                    + "<br/>Name["+hopaName+"]<br/>"
                    + "<br/>Title of Signatory: HOPA<br/>"
                    + "Name of Agency:[ "+PEName+"]<br/>" 
                    //Nitish END
                    //+ "<div align=\"center\">"
                    //+ "<br/><strong>GPPMD Office Contact Details</strong><br />"
                    //+ "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    //+ "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of Electronic Government Procurement (e-GP) User"
                    + "<br/>Registration Desk to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~LOA~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("getNOAMailContent :" + e);
        }
        LOGGER.debug("getNOAMailContent Ends:");
        return sb.toString();
    }

    /**
     * Contract Signing Contract is signed successfully
     *
     * @param tenderId
     * @param refNo
     * @param PEName
     * @param contractSignDate
     * @return String
     */
    public String getContractMailContent(String tenderId, String refNo, String PEName, String contractSignDate) {
        LOGGER.debug("getContractMailContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Contract has been signed successfully for below mentioned tender:<br/><br/>"
                    + "Tender ID : " + tenderId + "<br/>"
                    + "Ref No : " + refNo + "<br/>"
                    + "Procuring Agency : " + PEName + "<br/>"
                    + "Date of Contract Signing : " + contractSignDate + "<br/>"
                    + "<br/>Thanks.<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<br />"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table>~Contract signing~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("getContractMailContent :" + e);
        }
        LOGGER.debug("getContractMailContent Ends:");
        return sb.toString();
    }

    /**
     * Publish pre-tender meeting document Tenderer / Consultant who have
     * purchased tender documents or have posted
     *
     * @param tenderId
     * @param refNo
     * @param responseTime
     * @return
     */
    public String getPreBidMeetingContent(String tenderId, String refNo, String responseTime) {
        LOGGER.debug("getPreBidMeetingContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Pre-Tender Meeting is scheduled tomorrow in below mentioned tender:"
                    + "<br/><br/>"
                    + "<b>Tender ID :</b>" + tenderId + "<br />"
                    + "<b>Reference No. :</b>" + refNo + "<br />"
                    + "<b>Pre-Tender Meeting Start Date and Time :</b>" + responseTime + "<br /><br />"
                    + "For more details, Kindly logon to e-GP System<br/><br />"
                    + "Thanks.<br />"
                    + "Electronic Government Procurement (e-GP) System<br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getPreBidMeetingContent :" + e);
        }
        LOGGER.debug("getPreBidMeetingContent Ends:");
        return sb.toString();
    }

    /**
     * A day prior to Pre-tender meeting start date PE of the tender
     *
     * @param tenderid
     * @param refNo
     * @param peName
     * @param closingDate
     * @param tendBrief
     * @return
     */
    public String getPreTenderMeetingPublishMailContent(String tenderid, String refNo, String peName, String closingDate, String tendBrief) {
        LOGGER.debug("conWorkflowforRemoveUsers Starts:");
        StringBuilder sb = new StringBuilder();
        try {

            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that MOM of Pre-Tender Meeting is published on Electronic Government Procurement (e-GP) system for the below mentioned tender:<br/>"
                    + "<br><br><b>Tender ID:</b> " + tenderid + "<br/>"
                    + "<b>Reference No:</b> " + refNo + "<br/>"
                    + "<b>Tender Closing Date:</b> " + closingDate + "<br/>"
                    + "<b>Tender Description:</b> <a onclick=\"javascript:window.open('" + XMLReader.getMessage("url") + "/resources/common/ViewTender.jsp?id=" + tenderid + "', '', 'width=1200px,height=600px,scrollbars=1','');\" href=\"javascript:void(0);\">" + tendBrief + "</u></em></strong></p></a><br/>"
                    + "<b>Procuring Agency:</b> " + peName + "<br/>"
                    + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System<br/><br />"
                    + "Thanks.<br />"
                    + "Electronic Government Procurement (e-GP) System<br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962  | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Pre tender meeting~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("tenderCorriPub :" + e);
        }
        LOGGER.debug("conWorkflowforRemoveUsers Ends:");
        return sb.toString();
    }

    /**
     * 1 day prior to the tender/proposal opening
     *
     * @param tenderId
     * @param refNo
     * @param responseTime
     * @return
     */
    public String getIntTenderOpenContent(String tenderId, String refNo, String responseTime) {
        LOGGER.debug("getIntTenderOpenContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Tender Opening is scheduled Today in below mentioned tender:"
                    + "<br/><br/>"
                    + "<b>Tender ID :</b>" + tenderId + "<br />"
                    + "<b>Reference No. :</b>" + refNo + "<br />"
                    + "<b>Tender Opening Start Date and Time :</b>" + responseTime + "<br /><br />"
                    + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System<br/><br />"
                    + "Thanks.<br />"
                    + "Electronic Government Procurement (e-GP) System<br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getIntTenderOpenContent :" + e);
        }
        LOGGER.debug("getIntTenderOpenContent Ends:");

        return sb.toString();
    }

    /**
     *
     * @param tenderId
     * @param refNo
     * @param responseTime
     * @return
     */
    public String getIntTenderEvalContent(String tenderId, String refNo, String responseTime) {
        LOGGER.debug("getIntTenderEvalContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Tender Evaluation is scheduled tomorrow in below mentioned tender:"
                    + "<br/><br/>"
                    + "<b>Tender ID :</b>" + tenderId + "<br />"
                    + "<b>Reference No. :</b>" + refNo + "<br />"
                    + "<b>Tender Evaluation Start Date and Time :</b>" + responseTime + "<br /><br />"
                    + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System<br/><br />"
                    + "Thanks.<br />"
                    + "Electronic Government Procurement (e-GP) System<br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");

        } catch (Exception e) {
            LOGGER.error("getIntTenderEvalContent :" + e);
        }
        LOGGER.debug("getIntTenderEvalContent Ends:");
        return sb.toString();
    }

    /**
     * Transfer of a user The user, who is transferred
     *
     * @param loginId
     * @param password
     * @param deptName
     * @param PENameAndAddress
     * @return String
     */
    public String transferGovUserContent(String loginId, String password, String deptName, StringBuilder PENameAndAddress) {
        LOGGER.debug("transferGovUserContent Starts");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "Your Procuring Agency's profile has been created in the Electronic Government Procurement (e-GP) System. Following are the login credentials:"
                    + "<br/><br/>"
                    + "<table  style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'><tr><td><b>Login ID :</b>" + loginId + "</td></tr>"
                    + "<tr><td><b>Password :</b>" + password + "</td></tr>"
                    + "<tr><td><b>Website :</b>" + XMLReader.getMessage("url") + "</td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be redirected to the 'Change Password Screen'.<br /><br />"
                    + "Please follow the below mention steps to change the password:<br />"
                    + "<ol><li>Type a new password.</li>"
                    + "<li>Retype the same password.</li>"
                    + "<li>Select a Hint Question.</li>"
                    + "<li>Type a Hint answer.</li></ol><br /><br />"
                    + "<b>" + deptName + "<br /><br />"
                    + "<b>" + PENameAndAddress + "<br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("transferGovUserContent :" + e);
        }
        LOGGER.debug("transferGovUserContent Ends");
        return sb.toString();
    }

    /**
     * Transfer of a PE Admin user The user, who is transferred
     *
     * @param loginId
     * @param password
     * @param Mobile No
     * @param PEANameAndAddress (PE Admin Name and Address)
     * @return String Added By Palash, Dohatec
     */
    public String transferPEAdminContent(String loginId, String password, String mobileNo, StringBuilder PEANameAndAddress) {
        LOGGER.debug("transferPEAdminContent Starts");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "Your PA Admin profile has been created in the Electronic Government Procurement (e-GP) System. Following are the login credentials:"
                    + "<br/><br/>"
                    + "<table  style='background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px;'><tr><td><b>Login ID :</b>" + loginId + "</td></tr>"
                    + "<tr><td><b>Password :</b>" + password + "</td></tr>"
                    + "<tr><td><b>Website :</b>" + XMLReader.getMessage("url") + "</td></tr></table><br />"
                    + "On the first login, Electronic Government Procurement (e-GP) System will request you to change the password and you will be redirected to the 'Change Password Screen'.<br /><br />"
                    + "Please follow the below mention steps to change the password:<br />"
                    + "<ol><li>Type a new password.</li>"
                    + "<li>Retype the same password.</li>"
                    + "<li>Select a Hint Question.</li>"
                    + "<li>Type a Hint answer.</li></ol><br /><br />"
                    + "<b>" + mobileNo + "<br /><br />"
                    + "<b>" + PEANameAndAddress + "<br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("transferPEAdminContent :" + e);
        }
        LOGGER.debug("transferPEAdminContent Ends");
        return sb.toString();
    }
// End, by Palash

    /**
     * Activate or Deactivate User The user, who login account is activated or
     * deactivated
     *
     * @param strReason
     * @return
     */
    public List getUserActivatationContent() {
        LOGGER.debug("getUserActivatationContent Starts:");
        List list = new ArrayList();
        try {
            list.add("e-GP System: Your Login account is Activated");
            list.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that your Electronic Government Procurement (e-GP) Login account has been activated by admin."
                    + "Now you may login to Electronic Government Procurement (e-GP) System using your registered e-mail ID and password."
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getUserActivatationContent :" + e);
        }
        LOGGER.debug("getUserActivatationContent Ends:");
        return list;
    }

    /**
     * Activate or Deactivate User The user, who login account is activated or
     * deactivated
     *
     * @param strReason
     * @return
     */
    public List getUserDeactivationContent(String strReason) {
        LOGGER.debug("getUserDeactivationContent Starts:");
        List list = new ArrayList();
        try {
            list.add("e-GP System: Your Login account is Deactivated");
            list.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that your Electronic Government Procurement (e-GP) Login account has been deactivated  by admin"
                    + " because of following reason:<br /><br />"
                    + "Reason for deactivation : " + strReason
                    + "<br /><br />You may contact your Admin for more clarification in this regard."
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getUserDeactivationContent :" + e);
        }
        LOGGER.debug("getUserDeactivationContent Ends:");
        return list;
    }

    public List getTenderSubRightsMailContent(String strTenderId, String strRefNo, String strProcEntity) {
        LOGGER.debug("getTenderSubRightsMailContent Starts:");
        List list = new ArrayList();
        try {
            list.add("e-GP: Tender Submission Right assigned");
            list.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Company Admin has assigned Tender Submission Right"
                    + " to you for a below mentioned Tender:<br /><br />"
                    + "Tender ID : " + strTenderId
                    + "<br />Reference No. : " + strRefNo
                    + "<br />Procuring Agency : " + strProcEntity
                    + "<br /><br />You can go to Tender Dashboard of above mentioned tender can submit a tender."
                    + "<br /><br />Thanks."
                    + "<br /><br />Electronic Government Procurement (e-GP) System"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("getTenderSubRightsMailContent :" + e);
        }
        LOGGER.debug("getUserDeactivationContent Ends:");
        return list;
    }

    /**
     * AA ReTendering Evaluation Report
     *
     * @param tenderId
     * @param refNo
     * @param lotPckNo
     * @param lotPckDesc
     * @param lable
     * @param role
     * @return
     */
    public String aAReTenderRpt(String tenderId, String refNo, String lotPckNo, String lotPckDesc, String lable, String role) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + role + " has recommended for Re-Tendering for below mentioned Tender:<br/><br/>"
                    + "Tender ID: " + tenderId + "<br/>"
                    + "Reference No.: " + refNo + "<br/>"
                    + lable + " No.: " + lotPckNo + "<br/>"
                    + lable + " Desc. : " + lotPckDesc + "<br/><br/>"
                    + "Thanking you.<br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");

        } catch (Exception e) {
            LOGGER.error("contentSeekClari :" + e);
        }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }

    /**
     * AA Reject Evaluation Report
     *
     * @param tenderId
     * @param refNo
     * @param lotPckNo
     * @param lotPckDesc
     * @param lable
     * @param role
     * @return
     */
    public String aARejectRpt(String tenderId, String refNo, String lotPckNo, String lotPckDesc, String lable, String role) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that " + role + " has Rejected an Evaluation Report in a below mentioned tender:<br/><br/>"
                    + "Tender ID: " + tenderId + "<br/>"
                    + "Reference No.: " + refNo + "<br/>"
                    + lable + " No.: " + lotPckNo + "<br/>"
                    + lable + " Desc. : " + lotPckDesc + "<br/><br/>"
                    + "Thanking you.<br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");

        } catch (Exception e) {
            LOGGER.error("contentSeekClari :" + e);
        }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }

    public String cancelTenderContent(String tenderId, String refNo, String peName) {
        LOGGER.debug("cancelTenderContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that below mentioned tender is cancelled by Procuring Agency:"
                    + "<br/><br/>"
                    + "<b>Tender ID :</b>" + tenderId + "<br />"
                    + "<b>Reference No. :</b>" + refNo + "<br />"
                    + "<b>Procuring Agency :</b>" + peName + "<br /><br />"
                    + "For more details, please log on to Electronic Government Procurement (e-GP) system.<br/><br />"
                    + "Thanking you.<br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("cancelTenderContent :" + e);
        }
        LOGGER.debug("cancelTenderContent Ends:");

        return sb.toString();
    }

    public String contEvalApprovalNeeded(String strTenderId, String strRefNo, String strProcEntity) {
        LOGGER.debug("contRegistrationRenewal Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Evaluation Committee Chairperson has sent Evaluation Report for approval for below mentioned tender:"
                    + "<br/><br/>"
                    + "Tender ID : " + strTenderId
                    + "<br />Reference No. : " + strRefNo
                    + "<br />Procuring Agency : " + strProcEntity
                    + "<br /><br />You are requested to review the evaluation report."
                    + "<br /><br />Thanks."
                    + "<br /><br />Electronic Government Procurement (e-GP) System"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Evaluation committee~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contRegistrationRenewal :" + e);
        }
        LOGGER.debug("contRegistrationRenewal Ends:");
        return sb.toString();
    }

    public String committeePublishContent(String tenderId, String offName, String orgName, String commType) {
        LOGGER.debug("committeePublishContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that you have been selected as a ");
            if (commType.equals("eval")) {
                sb.append("Evaluation");
                sb.append("~Evaluation committee~~");

            } else if (commType.equals("open")) {
                sb.append("Opening");
                sb.append("~Tender Opening Process~~");
            } else if (commType.equals("tsc")) {
                sb.append("Technical Sub");
            }
            else if (commType.equals("TenderCommittee")) {
                sb.append("Tender");
            }
            sb.append(" Committee Member in below mentioned Tender : "
                    + "<br/><br/>"
                    + "Tender ID : " + tenderId
                    + "<br />PA : " + offName
                    + "<br />Organization : " + orgName
                    + "<br /><br />For more details, Kindly logon to Electronic Government Procurement (e-GP) System."
                    + "<br /><br />Thanks."
                    + "<br /><br />Electronic Government Procurement (e-GP) System"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("committeePublishContent :" + e);
        }
        LOGGER.debug("committeePublishContent Ends:");
        return sb.toString();
    }
    
    
    public String cancelTenderMail(String tenderId, String offName, String orgName) {
        LOGGER.debug("cancelTenderMail Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that");
            sb.append(" below mentioned Tender has been cancelled: "
                    + "<br/><br/>"
                    + "Tender ID : " + tenderId
                    + "<br />PA : " + offName
                    + "<br />Organization : " + orgName
                    + "<br /><br />For more details, Kindly logon to Electronic Government Procurement (e-GP) System."
                    + "<br /><br />Thanks."
                    + "<br /><br />Electronic Government Procurement (e-GP) System"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("committeePublishContent :" + e);
        }
        LOGGER.debug("committeePublishContent Ends:");
        return sb.toString();
    }
    
    

    /**
     * Evaluation Committee CP sends the Evaluation report to Approving
     * Authority (AA)
     *
     * @param tenderId
     * @param refNo
     * @param lotPkgNo
     * @param lotPkgDesc
     * @param lable
     * @return
     */
    public String SendReportAA(String tenderId, String refNo, String lotPkgNo, String lotPkgDesc, String lable) {
        LOGGER.debug("committeePublishContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Evaluation Committee Chairperson has sent Evaluation Report for approval/consideration for below mentioned tender : ");
            sb.append("<br/><br/>"
                    + "Tender ID : " + tenderId
                    + "<br />Reference No. : " + refNo
                    + "<br />" + lable + " No. : " + lotPkgNo
                    + "<br />" + lable + " Description. : " + lotPkgNo
                    + "<br />You are requested to process the evaluation report."
                    + "<br /><br />Thanks."
                    + "<br /><br />Electronic Government Procurement (e-GP) System"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Evaluation committee~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("committeePublishContent :" + e);
        }
        LOGGER.debug("committeePublishContent Ends:");
        return sb.toString();
    }

    /**
     * When TEC Chairperson post queries to the tenderers/consultant as part of
     * clarification round
     *
     * @param tenderId
     * @param refNo
     * @param peOfficeName
     * @param date
     * @return
     */
    public String cpClariBidder(String tenderId, String refNo, String peOfficeName, String date) {
        LOGGER.debug("committeePublishContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "TEC Chairperson has raised queries while evaluating your below mentioned tender : <br /><br />"
                    + "<strong>Tender ID :</strong>  " + tenderId + "<br/>"
                    + "<strong>Reference No. :</strong>  " + refNo + ""
                    + "<br /><strong>PA :</strong>  " + peOfficeName
                    + "<br /><strong>Last Date of Response :</strong>  " + date
                    + "<br /><br />You are requested to respond back to the queries before above mentioned Last Date and Time to enable Evaluation Committee to complete evaluation process.<br />"
                    + "<br />Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Evaluation committee~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("committeePublishContent :" + e);
        }
        LOGGER.debug("committeePublishContent Ends:");
        return sb.toString();
    }

    /**
     * AA seeks clarification from Evaluation Comm. CP Evaluation Committee
     * Chair Person
     *
     * @param tenderId
     * @param refNo
     * @param lotPkgNo
     * @param lotPckNoTxt
     * @param lotPkgDesc
     * @param lotPckDescTxt
     * @param lastDateNTime
     * @param role
     * @return String
     */
    public String contentSeekClari(String tenderId, String refNo, String lotPkgNo, String lotPckNoTxt, String lotPkgDesc, String lotPckDescTxt, String lastDateNTime, String role) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + role + " has sought clarification in an Evaluation Report for a below mentioned Tender:<br/><br/>"
                    + "Tender ID: " + tenderId + "<br/>"
                    + "Reference No.: " + refNo + "<br/>"
                    + lotPkgNo + ": " + lotPckNoTxt + "<br/>"
                    + lotPkgDesc + ": " + lotPckDescTxt + "<br/>"
                    + "Last date for Response: " + lastDateNTime + "<br/><br/>"
                    + "You are requested to give clarification till last date for response."
                    + "<br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Evaluation committee~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contentSeekClari :" + e);
        }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }

    /**
     * Evaluation Committee CP gives clarification to the AA Approving Authority
     *
     * @param tenderId
     * @param refNo
     * @param lotPkgNo
     * @param lotPckNoTxt
     * @param lotPkgDesc
     * @param lotPckDescTxt
     * @param lastDateNTime
     * @return Content
     */
    public String contentGiveClari(String tenderId, String refNo, String lotPkgNo, String lotPckNoTxt, String lotPkgDesc, String lotPckDescTxt, String lastDateNTime) {
        LOGGER.debug("contentGiveClari Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "Evaluation Committee Chairperson has given clarification a below mentioned Tender:<br/><br/>"
                    + "Tender ID: " + tenderId + "<br/>"
                    + "Reference No.: " + refNo + "<br/>"
                    + lotPkgNo + ": " + lotPckNoTxt + "<br/>"
                    + lotPkgDesc + ": " + lotPckDescTxt + "<br/>"
                    + "Last date for Response: " + lastDateNTime + "<br/><br/>"
                    + "Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Evaluation committee~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contentGiveClari :" + e);
        }
        LOGGER.debug("contentGiveClari Ends:");
        return sb.toString();
    }

    /*public String jvcaFinalSubMail(String jvLoginID, String nominee){
    LOGGER.debug("jvcaFinalSubMail Starts:");
    StringBuilder sb = new StringBuilder();
    try {
    sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
    + "<br/>Dear User," + "<br/><br/><span>"
    + "The Login ID for JVCA you has proposed "+jvLoginID+" has been created successfully by<br/>"
    + nominee+" who was nominated by you for carrying out the Tendering Activities.<br/>"
    + "Please take note of the same and if required, please get in touch with the nominated member.<br/>"
    + "<br /><br />Thanks."
    + "<br /><br />e-GP System"
    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
    + "</td></tr></table></body></html>");
    } catch (Exception e) {
    LOGGER.error("jvcaFinalSubMail :" + e);
    }
    LOGGER.debug("jvcaFinalSubMail Ends:");
    return sb.toString();
    }*/
    /**
     * AA Approves Evaluation Report
     *
     * @param tenderId
     * @param refNo
     * @param lotPckNo
     * @param lotPckDesc
     * @param lable
     * @param role
     * @return
     */
    public String aAApproveRpt(String tenderId, String refNo, String lotPckNo, String lotPckDesc, String lable, String role) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "Evaluation Report has been approved for below mentioned Tender:<br/><br/>"
                    + "Tender ID: " + tenderId + "<br/>"
                    + "Reference No.: " + refNo + "<br/>"
                    + lable + " No.: " + lotPckNo + "<br/>"
                    + lable + " Desc. : " + lotPckDesc + "<br/><br/>"
                    + "<br />Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("aAApproveRpt :" + e);
        }
        LOGGER.debug("aAApproveRpt Ends:");
        return sb.toString();
    }

    /**
     * AA Approves Evaluation Report and send it to tenderer when case is reoi
     *
     * @param tenderId
     * @param refNo
     * @param tenderBrief
     * @param peofficeName
     * @return
     */
    public String aAApproveRptSucessTen(String tenderId, String refNo, String tenderBrief, String peOfficeName) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that you have been shortlisted in the following REOI:<br/><br/>"
                    + "REOI ID : <strong>" + tenderId + "</strong><br/>"
                    + "REOI Reference No. : <strong>" + refNo + "</strong><br/>"
                    + "REOI Brief : <strong>" + refNo + "</strong><br/>"
                    + "PA : <strong>" + refNo + "</strong><br/><br/>"
                    + "<br />Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("aAApproveRpt :" + e);
        }
        LOGGER.debug("aAApproveRpt Ends:");
        return sb.toString();
    }

    /**
     * When HOPE/AO/AA approves the final Evaluation report then system should
     * notify rejected tenderers
     *
     * @param tenderId
     * @param refNo
     * @param lotPckNo
     * @param lotPckDesc
     * @param lable
     * @param role
     * @return
     */
    public String aAApproveRptTend(String tenderId, String refNo, String lotPckNo, String lotPckDesc, String lable, String role) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that your tender have been rejected in a below tender::<br/><br/>"
                    + "Tender ID: " + tenderId + "<br/>"
                    + "Reference No.: " + refNo + "<br/>"
                    + lable + " No.: " + lotPckNo + "<br/>"
                    + lable + " Desc. : " + lotPckDesc + "<br/><br/>"
                    + "<br />Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Tender Opening Process~~</body></html>");

        } catch (Exception e) {
            LOGGER.error("contentSeekClari :" + e);
        }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }

    /**
     * Evaluation committee CP notifies other evaluation committee members to
     * sign the TERs report
     *
     * @param tenderId
     * @param refNo
     * @param lotPckNo
     * @param lotPckDesc
     * @param lable
     * @param rptType
     * @return
     */
    public String notifyTECMember(String tenderId, String refNo, String lotPckNo, String lotPckDesc, String lable, String rptType) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
        if ("ter1".equalsIgnoreCase(rptType)) {
            rptType = "Tender Evaluation Report 1";
        }
        if ("ter2".equalsIgnoreCase(rptType)) {
            rptType = "Tender Evaluation Report 2";
        }
        if ("ter3".equalsIgnoreCase(rptType)) {
            rptType = "Tender Evaluation Report 3";
        }
        if ("ter4".equalsIgnoreCase(rptType)) {
            rptType = "Tender Evaluation Report 4";
        }
        if ("per1".equalsIgnoreCase(rptType)) {
            rptType = "Proposal Evaluation Report 1";
        }
        if ("per2".equalsIgnoreCase(rptType)) {
            rptType = "Proposal Evaluation Report 2";
        }
        if ("per3".equalsIgnoreCase(rptType)) {
            rptType = "Proposal Evaluation Report 3";
        }
        if ("per4".equalsIgnoreCase(rptType)) {
            rptType = "Proposal Evaluation Report 4";
        }
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "Evaluation Committee Chairperson has requested you to sign the Evaluation report <b> " + rptType + " </b> for below mentioned Tender:<br/><br/>"
                    + "Tender ID: " + tenderId + "<br/>"
                    + "Reference No.: " + refNo + "<br/>"
                    + lable + " No.: " + lotPckNo + "<br/>"
                    + lable + " Desc. : " + lotPckDesc + "<br/><br/>"
                    + "<br />Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Evaluation committee~~</body></html>");

        } catch (Exception e) {
            LOGGER.error("contentSeekClari :" + e);
        }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();

    }

    /*Dohatec Start*/
    /**
     * Evaluation committee CP notifies other evaluation committee members to
     * sign the TERs report
     *
     * @param tenderId
     * @param refNo
     * @param lotPckNo
     * @param lotPckDesc
     * @param lable
     * @return
     */
    public String notifyPE(String tenderId, String refNo, String lotPckNo, String lotPckDesc, String lable, String remarks) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "Evaluation Committee Chairperson has requested you to Create Contract Approval Workflow </b> for below mentioned Tender:<br/><br/>"
                    + "Tender ID: " + tenderId + "<br/>"
                    + "Reference No.: " + refNo + "<br/>"
                    + lable + " No.: " + lotPckNo + "<br/>"
                    + lable + " Desc. : " + lotPckDesc + "<br/><br/>"
                    + " Comments : " + remarks + "<br/><br/>"
                    + "<br />Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Evaluation committee~~</body></html>");

        } catch (Exception e) {
            LOGGER.error("contentSeekClari :" + e);
        }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();

    }

    /*Dohatec End*/
    /**
     * When tenderer/consultant gives response to all required queries and
     * notifies TEC chairperson Evaluation Committee Chairperson
     *
     * @param tenderId
     * @param refNo
     * @param peOfficeName
     * @param tenderName
     * @return List Content of mail
     */
    public String bidderAnsCp(String tenderId, String refNo, String peOfficeName, String tenderName) {
        LOGGER.debug("contentSeekClari Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "Below mentioned Bidder/Consultant has provided clarifications for the queries being raised by the Evaluation Committee for below mentioned Tender: <br /><br />"
                    + "<strong>Tender ID :</strong>  " + tenderId + "<br/>"
                    + "<strong>Reference No. :</strong>  " + refNo
                    + "<br /><strong>PA :</strong>  " + peOfficeName
                    + "<br /><strong>Bidder / Consultant&#39;s Name :</strong>  " + tenderName
                    + "<br />Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div><br />"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<br /><br />"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");

        } catch (Exception e) {
            LOGGER.error("contentSeekClari :" + e);
        }
        LOGGER.debug("contentSeekClari Ends:");
        return sb.toString();
    }

    public String deBriefPostQuestionContent(String tenderId, String refNo, String peOfficeName, String tendererConsultatName) {
        LOGGER.debug("deBriefPostQuestionContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + tendererConsultatName + " has sought clarification in a below mentioned Tender:<br/><br/>"
                    + "Tender ID: " + tenderId + "<br/>"
                    + "Reference No.: " + refNo + "<br/><br/>"
                    + "You are requested to reply to his/her query at earliest.<br/>"
                    + "<br />Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table>~Evaluation process(Debriefing on Tender)~~</body></html>");

        } catch (Exception e) {
            LOGGER.error("deBriefPostQuestionContent :" + e);
        }
        LOGGER.debug("deBriefPostQuestionContent Ends:");
        return sb.toString();

    }

    public String deBriefPostReplyContent(String tenderId, String refNo, String peOfficeName) {
        LOGGER.debug("deBriefPostReplyContent Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "PA has given clarification in a below mentioned Tender:<br/><br/>"
                    + "Tender ID: " + tenderId + "<br/>"
                    + "Reference No.: " + refNo + "<br/><br/>"
                    + "<br />Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table>~Evaluation process(Debriefing on Tender)~~</body></html>");

        } catch (Exception e) {
            LOGGER.error("deBriefPostReplyContent :" + e);
        }
        LOGGER.debug("deBriefPostReplyContent Ends:");
        return sb.toString();
    }

    public String bidderAccSuspendResume(String status) {
        LOGGER.debug("contAdmMailFinalSub Starts:");

        StringBuffer sb = new StringBuffer();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that your account has been " + status + " by your Organization's Admin.<br/>"
                    + "For more detail, please get in touch with your Organization's Admin.<br/><br/>"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href=" + XMLReader.getMessage("emailIdHelp") + ">" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href=" + XMLReader.getMessage("url") + ">" + XMLReader.getMessage("url") + "</a></div>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("contAdmMailFinalSub :" + e);
        }
        LOGGER.debug("contAdmMailFinalSub Ends:");
        return sb.toString();
    }
    ResourceBundle resbdl = ResourceBundle.getBundle("properties.cmsproperty");

    /**
     * it sends mail notification to tenderer on configuring the commencement
     * date by PE
     *
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @return string
     */
    public String ConfiguredCommencementDt(String Operation, boolean flag, String peName, Object[] obj) {
        LOGGER.debug("ConfiguredCommencementDt Starts:");
        String str_consName = "";
        String str_dcaps = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
            str_dcaps = "Delivery";
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
            str_dcaps = "Contract End";
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
            str_dcaps = "Completion";
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Procuring Agency " + peName + " " + Operation + " the "
                    + "Commencement Start Date and " + str_dcaps + " Date of below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("Company Name :");
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view Commencement Date and " + str_dcaps + " Date, you are required to follow the"
                    + " mentioned steps:<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab >> Commencement Date Tab</li>"
                    + "<li style='padding-top:5px;'>System will display Contract Information bar in any view page</li></ol><br/>"
                    + "Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Commencement Date~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("ConfiguredCommencementDt :" + e);
        }
        LOGGER.debug("ConfiguredCommencementDt Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on editing the delivery schedule
     * date by PE
     *
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @return string
     */
    public String EditDeliveryScheduleDt(String Operation, boolean flag, String peName, Object[] obj) {
        LOGGER.debug("EditDeliveryScheduleDt Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Procuring Agency " + peName + " " + Operation + " "
                    + "Delivery Schedule Dates of below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
            String strPayTerms = "";
            if (obj[5] == null) {
                strPayTerms = "-";
            } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                strPayTerms = "Any Item Any Percent";
            } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                strPayTerms = "All Item 100 Percent";
            } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                strPayTerms = "Item Wise 100 Percent";
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view Revised Delivery Schedule Date, you are required to follow the mentioned steps:"
                    + "<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Delivery Schedule Tab >> Click on View link</li></ol><br/>"
                    + "Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Delivery Schedule~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("EditDeliveryScheduleDt :" + e);
        }
        LOGGER.debug("EditDeliveryScheduleDt Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on finalizing the progress report
     * by PE
     *
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @return string
     */
    public String finalizingProgressReport(String Operation, boolean flag, String peName, Object[] obj) {
        LOGGER.debug("finalizingProgressReport Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Procuring Agency " + peName + " " + Operation + " "
                    + "the Progress Report of below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view the Finalized Progress Report, you are required to follow the mentioned steps:"
                    + "<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Progress Report Tab >> Click on View link</li></ol><br/>"
                    + "Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Progress report~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("finalizingProgressReport :" + e);
        }
        LOGGER.debug("finalizingProgressReport Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on issuance of work completion
     * certificate by PE
     *
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @param UsertypeId
     * @return string
     */
    public String issueneOfWorkCompletionCertificate(String Operation, boolean flag, String peName, Object[] obj, String UsertypeId) {
        LOGGER.debug("issueneOfWorkCompletionCertificate Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Procuring Agency " + peName + " " + Operation + " "
                    + "Work Completion Certificate of below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();

                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("Company Name :");
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            if ("2".equalsIgnoreCase(UsertypeId)) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view and Download Work Completion Certificate, you are required to follow the mentioned steps:"
                        + "<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                        + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                        + "<li style='padding-top:5px;'>Click on Progress Report Tab >> Click on View link >> Download Work Completion Certificate</li></ol><br/>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view Work Completion Certificate Status, you are required to follow the mentioned steps:"
                        + "<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                        + "<li style='padding-top:5px;'>Click on Payment tab</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                        + "<li style='padding-top:5px;'>System will display Work Completion Certificate Status (Issued) in Contract Table</li></ol><br/>");
            }
            sb.append("Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("issueneOfWorkCompletionCertificate :" + e);
        }
        LOGGER.debug("issueneOfWorkCompletionCertificate Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to pe on requesting the issuance of work
     * completion certificate by tenderer
     *
     * @param flag
     * @param obj
     * @return string
     */
    public String RequestforissueneOfWorkCompletionCertificate(boolean flag, Object[] obj) {
        LOGGER.debug("RequestforissueneOfWorkCompletionCertificate Starts:");
        String str_consName = "";
        String strTenderer = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that " + str_consName + " ");
            if ("1".equals(obj[12].toString())) {
                strTenderer = obj[10].toString() + " " + obj[11].toString();
            } else {
                strTenderer = obj[8].toString();
            }
            sb.append(strTenderer + " has Requested "
                    + "for Issuing Work Completion Certificate of below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();

                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("Company Name :");
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To Issue Work Completion Certificate, you are required to follow the mentioned steps:"
                    + "<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Progress Report Tab >> Click on Issue Work Completion Certificate link</li></ol><br/>"
                    + "Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Work completion certificate~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("RequestforissueneOfWorkCompletionCertificate :" + e);
        }
        LOGGER.debug("RequestforissueneOfWorkCompletionCertificate Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to pe on generating invoice by tenderer
     *
     * @param Operation
     * @param flag
     * @param obj
     * @return string
     */
    public String invoiceGeneratedbySupplie(String Operation, boolean flag, Object[] obj, String InvoiceAmt, String MileStoneName, String InvoiceNo) {
        LOGGER.debug("invoiceGeneratedbySupplie Starts:");
        String str_consName = "";
        String SupplierName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        if ("1".equals(obj[12].toString())) {
            SupplierName = obj[10].toString() + " " + obj[11].toString();
        } else {
            SupplierName = obj[8].toString();
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that " + str_consName + " " + SupplierName + " " + Operation + " "
                    + "Invoice of below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Invoice Amount :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + InvoiceAmt + "</td></tr>");
            } else {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Invoice Amount :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + InvoiceAmt + "</td>");
                if (MileStoneName != null && !"".equalsIgnoreCase(MileStoneName)) {
                    sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>MileStone Name :</td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'><td>" + MileStoneName + "</td></tr>");
                } else {
                    sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
                }
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;' class='ff'>Invoice No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + InvoiceNo + "</td></tr></table><br/><br/>To Process Invoice, you are required to follow the mentioned steps:"
                    + "<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Payment Tab >> Click on View Invoice link (System will display status as Invoice Generated by " + str_consName + ")</li>"
                    + "<li style='padding-top:5px;'>Click on Accept / Reject button</li>"
                    + "</ol><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Invoice generation~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("invoiceGeneratedbySupplie :" + e);
        }
        LOGGER.debug("invoiceGeneratedbySupplie Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to PE on generating invoice calculation by
     * accountant
     *
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @return string
     */
    public String invoiceNotificationToPEfromAc(String Operation, boolean flag, String peName, Object[] obj, String InvoiceAmt, String MileStoneName) {
        LOGGER.debug("invoiceNotificationToPEfromAc Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Procuring Agency " + peName + " " + Operation + " "
                    + "the Payment Details of below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>MileStone Name :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + MileStoneName + "</td></tr>");
            } else {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Invoice Amount :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + InvoiceAmt + "</td>");
                if (MileStoneName != null && !"".equalsIgnoreCase(MileStoneName)) {
                    sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>MileStone Name :</td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'><td>" + MileStoneName + "</td></tr>");
                } else {
                    sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
                }
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view the Invoice Details (Configuration), you are required to follow the mentioned steps:"
                    + "<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Payment Tab >> Click on Invoice link (System will display status as In Process)</li>"
                    + "<li style='padding-top:5px;'>Add Remarks</li>"
                    + "<li style='padding-top:5px;'>Click on Send to Accounts Officer Button</li>"
                    + "</ol><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Invoice generation~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("invoiceNotificationToPEfromAc :" + e);
        }
        LOGGER.debug("invoiceNotificationToPEfromAc Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on sending generated invoice by
     * accountant
     *
     * @param Operation
     * @param flag
     * @param peName
     * @param PeOfficeName
     * @param obj
     * @param IAccDetails
     * @return
     */
    public String invoiceNotificationToSupplierfromAc(String Operation, boolean flag, String peName, String PeOfficeName, Object[] obj, List<TblCmsInvoiceAccDetails> IAccDetails, String InvoiceAmt, String MileStoneName) {
        LOGGER.debug("invoiceNotificationToSupplierfromAc Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Accounts Officer " + peName + " from PA Office " + PeOfficeName + " " + Operation + " "
                    + "the Payment Details of Cheque / DD / Cash / Account to Account Transfer of below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder'>"
                    + "<tr><th colspan='4' style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Invoice Amount :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + InvoiceAmt + "</td></tr>");
            } else {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Invoice Amount :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + InvoiceAmt + "</td>");
                if (MileStoneName != null && !"".equalsIgnoreCase(MileStoneName)) {
                    sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>MileStone Name :</td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'><td>" + MileStoneName + "</td></tr>");
                } else {
                    sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
                }
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>"
                    + "<table width='35%' class='tableList_1 infoBarBorder'>"
                    + "<tr><th colspan='2'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Details of Payment as mention below:</th></tr>"
                    + "<tr><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Invoice No:</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + IAccDetails.get(0).getTblCmsInvoiceMaster().getInvoiceId() + "</td></tr>"
                    + "<tr><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Mode of Payment:</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + IAccDetails.get(0).getModeOfPayment() + "</td></tr>"
                    + "<tr><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Date of Payment:</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + IAccDetails.get(0).getDateOfPayment() + "</td></tr>"
                    + "<tr><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Bank Name:</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + IAccDetails.get(0).getBankName() + "</td></tr>"
                    + "<tr><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Branch Name:</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + IAccDetails.get(0).getBranchName() + "</td></tr>"
                    + "<tr><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Instrument Number:</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + IAccDetails.get(0).getInstrumentNo() + "</td></tr></table><br/><br/>"
                    + "<table width='35%' class='tableList_1'>"
                    + "<tr><th colspan='2' style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Gross Amount (In Nu.)</th>"
                    + "<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + IAccDetails.get(0).getGrossAmt() + "</td></tr>"
                    + "</table>"
                    + "<br/><br/>"
                    + "To view the Payment Details (Configuration), you are required to follow the mentioned steps:"
                    + "<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Payment Tab >> Click on \"Invoice\" link (System will display status as Processed by Accounts Officer)</li>"
                    + "<li style='padding-top:5px;'>Click on Print Icon (if require)</li>"
                    + "</ol><br/><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Invoice generation~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("invoiceNotificationToSupplierfromAc :" + e);
        }
        LOGGER.debug("invoiceNotificationToSupplierfromAc Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to PE/accountant on generating invoice by
     * accountant
     *
     * @param Operation
     * @param flag
     * @param peName
     * @param obj
     * @param UserTypeId
     * @return string
     */
    public String invoiceNotificationToSuppliandAcfromSupplier(String Operation, boolean flag, String peName, Object[] obj, String UserTypeId, String InvoiceAmt, String MileStoneName, String InvoiceNo) {
        LOGGER.debug("invoiceNotificationToSuppliandAcfromSupplier Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that PE " + peName + " " + Operation + " "
                    + "the Invoice of below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Invoice Amount :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + InvoiceAmt + "</td></tr>");
            } else {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Invoice Amount :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + InvoiceAmt + "</td>");
                if (MileStoneName != null && !"".equalsIgnoreCase(MileStoneName)) {
                    sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>MileStone Name :</td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'><td>" + MileStoneName + "</td></tr>");
                } else {
                    sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
                }
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;' class='ff'>Invoice No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + InvoiceNo + "</td></tr></table><br/><br/>To view the Invoice Details (Configuration), you are required to follow the mentioned steps:"
                    + "<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>");
            if ("2".equalsIgnoreCase(UserTypeId)) {
                sb.append("<li style='padding-top:5px;'>Select Tender/Contract</li>"
                        + "<li style='padding-top:5px;'>Click on CMS Tab</li>");
                String invStr = "";
                if ("has Rejected".equalsIgnoreCase(Operation)) {
                    invStr = "Invoice Rejected by PE";
                } else {
                    invStr = "Invoice Generated";
                }
                sb.append("<li style='padding-top:5px;'>Click on Payment Tab >> Click on View Invoice link (System will display status as " + invStr + ")</li>");
            } else {
                sb.append("<li style='padding-top:5px;'>Click on Payment Tab >> Click on Process link (System will display status Processed by PE)</li>");
            }
            sb.append("</ol><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Invoice generation~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("invoiceNotificationToSuppliandAcfromSupplier :" + e);
        }
        LOGGER.debug("invoiceNotificationToSuppliandAcfromSupplier Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer/PE on terminating the contarct by
     * PE/tenderer
     *
     * @param flag
     * @param peName
     * @param obj
     * @param ProcurementroleId
     * @param userTypeId
     * @param AcceptReject
     * @return string
     */
    public String contractTermination(boolean flag, String peName, Object[] obj, String ProcurementroleId, String userTypeId, String AcceptReject) {
        LOGGER.debug("contractTermination Starts:");
        String str_consName = "";
        String supplierName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that ");
            if ("3".equalsIgnoreCase(userTypeId)) {
                if ("Terminate".equalsIgnoreCase(AcceptReject)) {
                    sb.append("PE " + peName + "  has Terminated ");
                } else if ("Accept".equalsIgnoreCase(AcceptReject)) {
                    sb.append("PE " + peName + "  has Accepted Contract Terminatiion Request for ");
                } else if ("Reject".equalsIgnoreCase(AcceptReject)) {
                    sb.append("PE " + peName + "  has Rejected Contract Terminatiion Request for ");
                }
            } else {
                if ("1".equals(obj[12].toString())) {
                    supplierName = obj[10].toString() + " " + obj[11].toString();
                } else {
                    supplierName = obj[8].toString();
                }
                sb.append(str_consName + "" + supplierName + "has Initiated for Contract Terminatiion for");
            }
            sb.append("below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>");
            if ("Reject".equalsIgnoreCase(AcceptReject)) {
                sb.append("To view the Remarks / Comments, you are required to follow the mentioned steps:");
            } else {
                sb.append("To view the Contract Termination details, you are required to follow the mentioned steps:");
            }
            sb.append("<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>");
            if ("25".equalsIgnoreCase(ProcurementroleId)) {
                sb.append("<li style='padding-top:5px;'>Contract Termination Status(Contract Terminated) will be displayed in Contract Information bar</li>");
            } else {
                sb.append("<li style='padding-top:5px;'>Click on Contract Termination Tab >> Click on View link </li>");
            }
            sb.append("</ol><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Contract termination~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contractTermination :" + e);
        }
        LOGGER.debug("contractTermination Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on placing the variation oreder by
     * PE
     *
     * @param flag
     * @param peName
     * @param obj
     * @return string
     */
    public String variationOrderFromPEToContractor(boolean flag, String peName, Object[] obj) {
        LOGGER.debug("variationOrderFromPEToContractor Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that PE " + peName + " has placed "
                    + "Variation Order for below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view the Variation Order Details, you are required to follow the mentioned steps:"
                    + "<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
            if ("3".equals(obj[6].toString())) {
                sb.append("<li style='padding-top:5px;'>Click on CMS Tab >> Click on Work Schedule Tab</li>"
                        + "<li style='padding-top:5px;'>Click on Variation Order link</li>");
            } else {
                sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                        + "<li style='padding-top:5px;'>Click on Work Program Tab >> Click on Variation Order link</li>"
                        + "<li style='padding-top:5px;'>System will display the changes which has been made by PE (legend has been defined on Work Program page)</li>");
            }
            sb.append("<li style='padding-top:5px;'>Click on Accept button</li>"
                    + "</ol><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Variation order~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("variationOrderFromPEToContractor :" + e);
        }
        LOGGER.debug("variationOrderFromPEToContractor Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to PE on accepting/rejecting by contractor
     *
     * @param flag
     * @param peName
     * @param obj
     * @return string
     */
    public String variationOrderFromContractorToPE(boolean flag, String peName, Object[] obj) {
        LOGGER.debug("variationOrderFromPEToContractor Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that " + str_consName + " " + peName + " has accepted "
                    + "Variation Order request for below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view the Variation Order Details, you are required to follow the mentioned steps:"
                    + "<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>");
            if ("3".equals(obj[6].toString())) {
                sb.append("<li style='padding-top:5px;'>Click on Work Schedule Tab >> Click on View link</li>");
            } else {
                sb.append("<li style='padding-top:5px;'>Click on Work Program Tab >> Click on View link</li>");
            }
            sb.append("</ol><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Variation order~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("variationOrderFromPEToContractor :" + e);
        }
        LOGGER.debug("variationOrderFromPEToContractor Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on publishing the approved
     * variation order by PE
     *
     * @param flag
     * @param peName
     * @param obj
     * @return string
     */
    public String variationOrderPublished(boolean flag, String peName, Object[] obj) {
        LOGGER.debug("variationOrderPublished Starts:");
        String str_consName = "";
        String str_dcaps = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
            str_dcaps = "Delivery";
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
            str_dcaps = "Contract End";
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Procuring Agency " + peName + " has Issued Variation Order for the "
                    + "below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("Company Name :");
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view The Variation Order, you are required to follow the"
                    + " mentioned steps:<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Work Program >> click on Variation Order History</li></ol><br/>"
                    + "Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~</body></html>");
        } catch (Exception e) {
            LOGGER.error("variationOrderPublished :" + e);
        }
        LOGGER.debug("variationOrderPublished Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on editing the delivery schedule
     * date by PE
     *
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @param UserTypeId
     * @param ProcRoleId
     * @param strfor
     * @return
     */
    public String mailForReleaseRequest(boolean flag, String peName, Object[] obj, String userTypeId, String UserTypeId, String ProcRoleId, String strfor) {
        LOGGER.debug("mailForReleaseForefeit Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>");
            if ("7".equalsIgnoreCase(UserTypeId)) {
                sb.append("This is to inform you that Financial Institute " + peName + " has "
                        + "Released " + strfor + " for below mention Contract:<br/><br/>");
            } else {
                sb.append("This is to inform you that PA " + peName + " has Requested To "
                        + "Release " + strfor + " for below mention Contract:<br/><br/>");
            }
            sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>");
            if ("3".equalsIgnoreCase(UserTypeId)) {
                if ("2".equalsIgnoreCase(userTypeId)) {
                    sb.append("To view the " + strfor + " Details, you are required to follow the mentioned steps:");
                } else if ("7".equalsIgnoreCase(userTypeId)) {
                    sb.append("To Release " + strfor + ", you are required to follow the mentioned steps:");
                }
                if ("2".equalsIgnoreCase(userTypeId)) {
                    sb.append("<br/><ol style='padding-left:35px;'>"
                            + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                            + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                    if ("Bid Security".equalsIgnoreCase(strfor)) {
                        sb.append("<li style='padding-top:5px;'>Click on Payment Tab</li>");
                    } else {
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                                + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");
                    }
                } else if ("7".equalsIgnoreCase(userTypeId)) {
                    sb.append("<br/><ol style='padding-left:35px;'>"
                            + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                            + "<li style='padding-top:5px;'>Click on Payment Tab >> Select Tender Payment option</li>"
                            + "<li style='padding-top:5px;'>Select Tender/Contract for which " + strfor + " need to be Released</li>"
                            + "<li style='padding-top:5px;'>Click on Verify Link</li>"
                            + "<li style='padding-top:5px;'>Add Remarks</li>"
                            + "<li style='padding-top:5px;'>Click On Verify Button</li>");
                }
            } else if (!"25".equalsIgnoreCase(ProcRoleId)) {
                sb.append("To view the " + strfor + " Details, you are required to follow the mentioned steps:");
                sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                if ("Bid Security".equalsIgnoreCase(strfor)) {
                } else {
                    sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                            + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");
                }
            }
            sb.append("</ol><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForReleaseForefeit :" + e);
        }
        LOGGER.debug("mailForReleaseForefeit Ends:");
        return sb.toString();
    }

    //Code Start by Proshanto Kumar Saha,Dohatec
    /**
     * it sends mail notification to Tenderer and BranchChecker without Contract
     * Signing by PE
     *
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @param UserTypeId
     * @param ProcRoleId
     * @param strfor
     * @param tenderId
     * @return
     */
    public String mailForReleaseRequestWithoutConSign(boolean flag, String peName, Object[] obj, String userTypeId, String UserTypeId, String ProcRoleId, String strfor, String tenderId) {
        LOGGER.debug("mailForReleaseRequestWithoutConSign Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>");
            if ("7".equalsIgnoreCase(UserTypeId)) {
                sb.append("This is to inform you that Financial Institute " + peName + " has "
                        + "Released " + strfor + " for below mention Contract:<br/><br/>");
            } else {
                sb.append("This is to inform you that PA " + peName + " has Requested To "
                        + "Release " + strfor + " for below mention Contract:<br/><br/>");
            }
            sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Tender ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + tenderId + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Performance Security Amount :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[4].toString() + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            sb.append("</table><br/><br/>");
            if ("3".equalsIgnoreCase(UserTypeId)) {
                if ("2".equalsIgnoreCase(userTypeId)) {
                    sb.append("To view the " + strfor + " Details, you are required to follow the mentioned steps:");
                } else if ("7".equalsIgnoreCase(userTypeId)) {
                    sb.append("To Release " + strfor + ", you are required to follow the mentioned steps:");
                }
                if ("2".equalsIgnoreCase(userTypeId)) {
                    sb.append("<br/><ol style='padding-left:35px;'>"
                            + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                            + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                    if ("Bid Security".equalsIgnoreCase(strfor)) {
                        sb.append("<li style='padding-top:5px;'>Click on Payment Tab</li>");
                    } else {
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                                + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");
                    }
                } else if ("7".equalsIgnoreCase(userTypeId)) {
                    sb.append("<br/><ol style='padding-left:35px;'>"
                            + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                            + "<li style='padding-top:5px;'>Click on Payment Tab >> Select Tender Payment option</li>"
                            + "<li style='padding-top:5px;'>Select Tender/Contract for which " + strfor + " need to be Released</li>"
                            + "<li style='padding-top:5px;'>Click on Verify Link</li>"
                            + "<li style='padding-top:5px;'>Add Remarks</li>"
                            + "<li style='padding-top:5px;'>Click On Verify Button</li>");
                }
            } else if (!"25".equalsIgnoreCase(ProcRoleId)) {
                sb.append("To view the " + strfor + " Details, you are required to follow the mentioned steps:");
                sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                if ("Bid Security".equalsIgnoreCase(strfor)) {
                } else {
                    sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                            + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");
                }
            }
            sb.append("</ol><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForReleaseRequestWithoutConSign :" + e);
        }
        LOGGER.debug("mailForReleaseRequestWithoutConSign Ends:");
        return sb.toString();
    }
    //Code End by Proshanto Kumar Saha,Dohatec

    /**
     * it sends mail notification to maker/tenderer on forfeiting/releasing the
     * ps/bg/ts by PE
     *
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @param UserTypeId
     * @param ProcRoleId
     * @param strfor
     * @param strtyp
     * @return string
     */
    public String mailForForfeitRequest(boolean flag, String peName, Object[] obj, String userTypeId, String UserTypeId, String ProcRoleId, String strfor, String strtyp) {
        LOGGER.debug("mailForForfeitRequest Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>");
            if ("7".equalsIgnoreCase(UserTypeId)) {
                sb.append("This is to inform you that Financial Institute " + peName + " has "
                        + strtyp + " for below mention Contract:<br/><br/>");

            } else {
                sb.append("This is to inform you that PA " + peName + " has Requested to "
                        + strtyp + " for below mention Contract:<br/><br/>");
            }
            sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>");
            if ("3".equalsIgnoreCase(UserTypeId)) {
                if ("2".equalsIgnoreCase(userTypeId)) {
                    sb.append("To view the " + strfor + " Details, you are required to follow the mentioned steps:");
                } else if ("7".equalsIgnoreCase(userTypeId)) {
                    sb.append("To " + strtyp + ", you are required to follow the mentioned steps:");
                }
                if ("2".equalsIgnoreCase(userTypeId)) {
                    sb.append("<br/><ol style='padding-left:35px;'>"
                            + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                            + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                    if ("Bid Security".equalsIgnoreCase(strfor)) {
                        sb.append("<li style='padding-top:5px;'>Click on Payment Tab</li>");
                    } else {
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                                + "<li style='padding-top:5px;'>Click on Payment Tab</li>"
                                + "<li style='padding-top:5px;'>Click on Payment Details link</li>");
                    }
                } else if ("7".equalsIgnoreCase(userTypeId)) {
                    sb.append("<br/><ol style='padding-left:35px;'>"
                            + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                            + "<li style='padding-top:5px;'>Click on Payment Tab >> Select Tender Payment option</li>"
                            + "<li style='padding-top:5px;'>Select Tender/Contract for which " + strfor + "  needs to be Compensate</li>"
                            + "<li style='padding-top:5px;'>Click on Verify Link</li>"
                            + "<li style='padding-top:5px;'>Add Remarks</li>"
                            + "<li style='padding-top:5px;'>Click On Verify Button</li>");
                }
            } else if (!"25".equalsIgnoreCase(ProcRoleId)) {
                sb.append("To view the " + strfor + " Details, you are required to follow the mentioned steps:");
                sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                if ("Bid Security".equalsIgnoreCase(strfor)) {
                } else {
                    sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                            + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");
                }
            }
            sb.append("</ol><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForForfeitRequest :" + e);
        }
        LOGGER.debug("mailForForfeitRequest Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to PE/tenderer on forfeiting/releasing the
     * ps/bg/ts by bank
     *
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @param UserTypeId
     * @param ProcRoleId
     * @return
     */
    public String mailForBankGuaranteeRequest(boolean flag, String peName, Object[] obj, String userTypeId, String UserTypeId, String ProcRoleId) {
        LOGGER.debug("mailForBankGuaranteeRequest Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>");
            if ("7".equalsIgnoreCase(UserTypeId)) {
                sb.append("This is to inform you that Financial Institute " + peName + " has "
                        + "Issued New Performance Security for below mention Contract:<br/><br/>");
            } else {
                sb.append("This is to inform you that PA " + peName + " has Requested for "
                        + " New Performance Security for below mention Contract:<br/><br/>");
            }
            sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>");
            if ("3".equalsIgnoreCase(UserTypeId)) {
                if ("2".equalsIgnoreCase(userTypeId)) {
                    sb.append("To view the Performance Security Details, you are required to follow the mentioned steps:");
                }
                if ("2".equalsIgnoreCase(userTypeId)) {
                    sb.append("<br/><ol style='padding-left:35px;'>"
                            + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                            + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                            + "<li style='padding-top:5px;'>Click on CMS Tab >> Click on Payment Tab</li>"
                            + "<li style='padding-top:5px;'>Click on New Performance Security Details link</li>");
                }
            } else if (!"25".equalsIgnoreCase(ProcRoleId)) {
                sb.append("To view the New Performance Security Details, you are required to follow the mentioned steps:");
                sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                        + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                        + "<li style='padding-top:5px;'>Click on New Performance Security Details link</li>");
            } else {
                sb.append("To view the New Performance Security Details, you are required to follow the mentioned steps:");
                sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                        + "<li style='padding-top:5px;'>Click on Make Payment Tab</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                        + "<li style='padding-top:5px;'>New Performance Security Status (Issued / Pending) will be Displayed in Contract Information Bar</li>");
            }
            sb.append("</ol><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Compensation of performance security or New Performance Security~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForBankGuaranteeRequest :" + e);
        }
        LOGGER.debug("mailForBankGuaranteeRequest Ends:");
        return sb.toString();
    }

    public String registerComplaint(int tenderId, int compId) {
        LOGGER.debug("TenderInfoBar Starts:");

        return "";
    }

    /**
     * it sends mail notification to PE on forfeiting/releasing the ps/bg/ts by
     * supplier
     *
     * @param flag
     * @param obj
     * @param strps
     * @return string
     */
    public String ReleasePerfomanceSecurityToPE(boolean flag, Object[] obj, String strps) {
        LOGGER.debug("ReleasePerfomanceSecurityToPE Starts:");
        String str_consName = "";
        String strTenderer = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }

        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that " + str_consName + " ");
            if ("1".equals(obj[12].toString())) {
                strTenderer = obj[10].toString() + " " + obj[11].toString();
            } else {
                strTenderer = obj[8].toString();
            }
            sb.append("" + strTenderer + " has Requested for Release " + strps + " for below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>");
            sb.append("To view the " + strps + " Details, you are required to follow the mentioned steps:");
            sb.append("<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Payment Tab</li>");
            sb.append("Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Performance security release~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("ReleasePerfomanceSecurityToPE :" + e);
        }
        LOGGER.debug("ReleasePerfomanceSecurityToPE Ends:");
        return sb.toString();
    }

    //Code Start by Proshanto Kumar Saha,Dohatec
    /**
     * it sends mail notification to PE on forfeiting/releasing the ps/bg/ts
     * without Contract Signing by supplier
     *
     * @param flag
     * @param obj
     * @param strps
     * @param tenderId
     * @return string
     */
    public String ReleasePerfomanceSecurityToPEWithoutConSign(boolean flag, Object[] obj, String strps, String tenderId) {
        LOGGER.debug("ReleasePerfomanceSecurityToPEWithoutConSign Starts:");
        String str_consName = "";
        String strTenderer = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }

        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that " + str_consName + " ");
            if ("1".equals(obj[12].toString())) {
                strTenderer = obj[10].toString() + " " + obj[11].toString();
            } else {
                strTenderer = obj[8].toString();
            }
            sb.append("" + strTenderer + " has Requested for Release " + strps + " for below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Tender ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + tenderId + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Performance Security Amount :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[4].toString() + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            sb.append("</table><br/><br/>");
            sb.append("To view the " + strps + " Details, you are required to follow the mentioned steps:");
            sb.append("<br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Payment Tab</li>");
            sb.append("Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Performance security release~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("ReleasePerfomanceSecurityToPEWithoutConSign :" + e);
        }
        LOGGER.debug("ReleasePerfomanceSecurityToPEWithoutConSign Ends:");
        return sb.toString();
    }
    //Code End by Proshanto Kumar Saha,Dohatec

    public String MailAfter7DaysIfPaid() {
        LOGGER.debug("MailAfter7DaysIfPaid Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            //sb.append("Electronic Government Procurement (e-GP) System:  User Registration &#45; e-mail Verification and Profile Submission Information");
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "You did not complete profile submission within " + XMLReader.getMessage("profileSubDays") + " days as required by the Electronic Government Procurement (e-GP) system. As you have already made the payment, e-GP system is providing you additional <font color='red'>" + XMLReader.getMessage("profileSubDays") + " calendar days </font>"
                    + "for completing your profile, uploading required documents, make necessary corrections and submit in the Electronic Government Procurement (e-GP) system.<br/>"
                    + "Please follow the following steps for completing your profile:<br/><br/>"
                    + "a) Login again with same email ID and password<br/>"
                    + "b) Complete your profile entry and documents uploading<br/>"
                    + "c) Submit the completed profile.<br/><br/>"
                    + "If the profile is not submitted online within the date mentioned above, your profile will be <font color='red'  style='font-weight: bold;'>DISABLED</font>. For profile submission, you have below mentioned options:<br/><br/>"
                    + "a) Re-activate your profile (use of registered e-mail ID): you can Re-activate your profile by paying registration fee to any of the registered member scheduled banks. Once your profile is activated, you can fill required details or can upload required documents which are required for online profile submission.<br/>"
                    + "b) Re-registration: you can register new e-mail ID. Follow all the steps of New User Registration process.<br/><br/>"
                    + "GPPMD Office.<br/><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href='" + XMLReader.getMessage("emailIdHelp") + "'>" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href>" + XMLReader.getMessage("url") + "</a></div>"
                    // +"<strong>Help Desk Tel: </strong>88-02-9144252-53, Help Desk e-mail: <a href=\"mailto:helpdesk@egp.gov.bt\">helpdesk@egp.gov.bt</a>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href='\"mailto:'" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~New user registration~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("MailAfter7DaysIfPaid :" + e);
        }
        LOGGER.debug("MailAfter7DaysIfPaid Ends:");
        return sb.toString();
    }

    public String MailAfter14DaysToBlock() {
        LOGGER.debug("MailAfter14DaysToBlock Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            //sb.append("e-GP System:  User Registration &#45; e-mail Verification and Profile Submission Information");
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "Your profile is <font color='red'  style='font-weight: bold;'>DISABLED</font> by the Electronic Government Procurement (e-GP) System as you did not complete profile submission within additional " + XMLReader.getMessage("profileSubDays") + " days as required by the Electronic Government Procurement (e-GP) system.<br/><br/>"
                    + "For profile submission, now you have below mentioned options:<br/><br/>"
                    + "a) Re-activate your profile (use of registered e-mail ID): you can Re-activate your profile by paying registration fee to any of the registered member scheduled banks. Once your profile is activated, you can fill required details or can upload required documents which are required for online profile submission.<br/>"
                    + "b) Re-registration: you can register new e-mail ID. Follow all the steps of New User Registration process.<br/><br/>"
                    + "GPPMD Office.<br/><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href='" + XMLReader.getMessage("emailIdHelp") + "'>" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href>" + XMLReader.getMessage("url") + "</a></div>"
                    // +"<strong>Help Desk Tel: </strong>88-02-9144252-53, Help Desk e-mail: <a href=\"mailto:helpdesk@egp.gov.bt\">helpdesk@egp.gov.bt</a>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href='\"mailto:'" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~New user registration~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("MailAfter14DaysToBlock :" + e);
        }
        LOGGER.debug("MailAfter14DaysToBlock Ends:");
        return sb.toString();
    }

    public String MailAfter7DaysIfUnPaid() {
        LOGGER.debug("MailAfter7DaysIfUnPaid Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            //sb.append("e-GP System:  User Registration &#45; e-mail Verification and Profile Submission Information");
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "You did not complete profile submission within " + XMLReader.getMessage("profileSubDays") + " calendar days. So your profile is deleted automatically."
                    + "<br/><br/><b>We encourage you to follow the all the New Bidder Registration processes. For this new registration you will have to submit your completed profile and get approval.</b>"
                    + "<br/><br/><b>In the new registration, you may use your same email ID or a new email ID.</b>"
                    + "<br/><br/>GPPMD Office.<br/><br/>"
                    + "<b>Government Procurement and Property Management Division (GPPMD)</b><br />"
                    + "Ministry of Finance, Royal Government of Bhutan<br />"
                    + "Address : Box 116, Thimphu, Bhutan. <br /><br />"
                    + "<div align=\"center\"><br/><strong>GPPMD Office Contact Details</strong><br />"
                    + "<strong>Phone:</strong> +975-02-336962 | <strong>Email:</strong> <a href='" + XMLReader.getMessage("emailIdHelp") + "'>" + XMLReader.getMessage("emailIdHelp") + "</a><br />"
                    + "<a href>" + XMLReader.getMessage("url") + "</a></div>"
                    // +"<strong>Help Desk Tel: </strong>88-02-9144252-53, Help Desk e-mail: <a href=\"mailto:helpdesk@egp.gov.bt\">helpdesk@egp.gov.bt</a>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href='\"mailto:'" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~New user registration~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("MailAfter7DaysIfUnPaid :" + e);
        }
        LOGGER.debug("MailAfter7DaysIfUnPaid Ends:");
        return sb.toString();
    }

    /**
     * Get Mail Content for Tenderer COmplite his negotiation and send mail to
     * CP.
     *
     * @param tenderId
     * @param refNo
     * @return String having Mail Content.
     */
    public String mailSendTOCPForTendererCompliteNegFillForm(String tenderId, String refNo) {
        LOGGER.debug("mailForTendererCompliteNegFillForm Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            //sb.append("e-GP System:  User Registration &#45; e-mail Verification and Profile Submission Information");
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "This is to inform you that Bidder/ Consultant has revised the tender forms as a part of Negotiation for below mentioned Tender:<br/><br/>"
                    + "<b>Tender ID: </b>" + tenderId + " <br/>"
                    + "<b>Reference No.: </b>" + refNo + " <br/>"
                    + "<br/><br/>To view the revised tender forms, please logon to website."
                    + "<br/><br/> Thanks."
                    + "<br/><br/> <u>Electronic Government Procurement (e-GP) System</u>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href='\"mailto:'" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "<br/><br/>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | http://www.egp.gov.bt | Help Desk"
                    + "</td></tr></table>~Negotiation~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForTendererCompliteNegFillForm :" + e);
        }
        LOGGER.debug("mailForTendererCompliteNegFillForm Ends:");
        return sb.toString();
    }

    /**
     * Get Mail Content for tenderer accept or reject neogiation Request.
     *
     * @param tenderId
     * @param refNo
     * @param accrejStatus
     * @return String having mail content.
     */
    public String mailSendToCPForTendererAcceptRejectNegoRequest(String tenderId, String refNo, String accrejStatus) {
        LOGGER.debug("mailForTendererCompliteNegFillForm Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            //sb.append("e-GP System:  User Registration &#45; e-mail Verification and Profile Submission Information");
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "This is to inform you that Bidder/ Consultant has " + accrejStatus + " Negotiation for below mentioned tender:<br/><br/>"
                    + "<b>Tender ID: </b>" + tenderId + " <br/>"
                    + "<b>Reference No.: </b>" + refNo + " <br/>"
                    + "<br/><br/>Please logon to http://www.egp.gov.bt for more details"
                    + "<br/><br/> Thanks."
                    + "<br/><br/> <u>Electronic Government Procurement (e-GP) System</u>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href='\"mailto:'" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "<br/><br/>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | http://www.egp.gov.bt | Help Desk"
                    + "</td></tr></table>~Negotiation~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForTendererCompliteNegFillForm :" + e);
        }
        LOGGER.debug("mailForTendererCompliteNegFillForm Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on editing work schedule main
     * (forms) by PE
     *
     * @param srvBoqType
     * @param flag
     * @param peName
     * @param obj
     * @return string
     */
    public String MailforEditWorkSch(String srvBoqType, boolean flag, String peName, Object[] obj) {
        LOGGER.debug("MailforEditWorkSch Starts:");
        String str_consName = "";
        String str_dcaps = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
            str_dcaps = "Delivery";
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
            str_dcaps = "Contract End";
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
            str_dcaps = "Completion";
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>"
                    + "This is to inform you that Procuring Agency " + peName + " has edited "
                    + "Work Schedule(" + srvBoqType + ") of below mention Contract:<br/><br/>"
                    + "<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("Company Name :");
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view Revised Work Schedule, you are required to follow the mentioned steps:"
                    + " <br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Work Schedule Tab >> Click on View History link</li></ol><br/>"
                    + "Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~work schedule~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("MailforEditWorkSch :" + e);
        }
        LOGGER.debug("MailforEditWorkSch Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to PE on creating the AttanSheet by supplier
     *
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @return string
     */
    public String CreatedAttanSheetbySupplier(boolean flag, String peName, Object[] obj, String userTypeId) {
        LOGGER.debug("CreatedAttanSheetbySupplier Starts:");
        String str_consName = "";
        String str_dcaps = "";
        String SupplierName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
            str_dcaps = "Delivery";
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
            str_dcaps = "Contract End";
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
            str_dcaps = "Completion";
        }
        if ("1".equals(obj[12].toString())) {
            SupplierName = obj[10].toString() + " " + obj[11].toString();
        } else {
            SupplierName = obj[8].toString();
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>");
            if ("2".equalsIgnoreCase(userTypeId)) {
                sb.append("This is to inform you that " + str_consName + " " + SupplierName + " has created Attendance Sheet "
                        + "for below mentioned Contract:<br/><br/>");
            } else {
                sb.append("This is to inform you that PE " + peName + " has Accepted / Rejected Attendance Sheet "
                        + "for below mentioned Contract:<br/><br/>");
            }
            sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("Company Name :");
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view the Attandance Sheet details, you are required to follow the mentioned steps:"
                    + " <br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Progress Report Tab >> Click on View Attendance Sheet link</li></ol><br/>"
                    + "Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Attendance sheet~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("CreatedAttanSheetbySupplier :" + e);
        }
        LOGGER.debug("CreatedAttanSheetbySupplier Ends:");
        return sb.toString();
    }

    public String cpCloseNeg(String tenderId, String refNo, String peOfficeName, String accrejStatus) {
        LOGGER.debug("mailForTendererCompliteNegFillForm Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            //sb.append("e-GP System:  User Registration &#45; e-mail Verification and Profile Submission Information");
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User,<br/><br/><span>"
                    + "This is to inform you that PA has " + accrejStatus + " Negotiation for below mentioned tender :<br/><br/>"
                    + "<b>Tender ID: </b>" + tenderId + " <br/>"
                    + "<b>Reference No.: </b>" + refNo + " <br/>"
                    + "<b>PA Name: </b>" + peOfficeName + "<br/>"
                    + "<br/><br/>Please logon to http://www.egp.gov.bt for more details"
                    + "<br/><br/> Thanks."
                    + "<br/><br/> <u>Electronic Government Procurement (e-GP) System</u>"
                    + "<br/><br/><span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href='\"mailto:'" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "<br/><br/>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | http://www.egp.gov.bt | Help Desk"
                    + "</td></tr></table>~Negotiation~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForTendererCompliteNegFillForm :" + e);
        }
        LOGGER.debug("mailForTendererCompliteNegFillForm Ends:");
        return sb.toString();
    }

    /**
     * it sends mail notification to tenderer on finalizing repeat order by PE
     *
     * @param flag
     * @param peName
     * @param obj
     * @return string
     */
    public String MailForFinaliseRo(boolean flag, String peName, Object[] obj) {
        LOGGER.debug("MailForFinaliseRo Starts:");
        String str_consName = "";
        String str_dcaps = "";
        String SupplierName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
            str_dcaps = "Delivery";
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
            str_dcaps = "Contract End";
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
            str_dcaps = "Completion";
        }
        if ("1".equals(obj[12].toString())) {
            SupplierName = obj[10].toString() + " " + obj[11].toString();
        } else {
            SupplierName = obj[8].toString();
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>");
            sb.append("This is to inform you that PE " + peName + " has finalized Repeat Order "
                    + "for below mention Contract:<br/><br/>");
            sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("Company Name :");
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>To view the Repeat Order details, you are required to follow the mentioned steps:"
                    + " <br/><ol style='padding-left:35px;'>"
                    + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                    + "<li style='padding-top:5px;'>Select Tender/Contract</li>"
                    + "<li style='padding-top:5px;'>Click on CMS Tab</li>"
                    + "<li style='padding-top:5px;'>Click on Repeat Order Tab</li></ol><br/>"
                    + "Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Repeat order~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("MailForFinaliseRo :" + e);
        }
        LOGGER.debug("MailForFinaliseRo Ends:");
        return sb.toString();
    }

    public String sendMailToPEForRegisterComp(Integer id, List<SPTenderCommonData> thisTenderInfoLst, int complaintId, String firstName, String Cmptype, String complSubject) {
        LOGGER.debug("sendMailToPEForRegisterComp Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            String msgTender = "Reference No.";
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/><span>");
            sb.append("This is to inform you that Bidder/Consultant ").append(firstName).append(" has registered a Complaint " + "for ").append(Cmptype).append("<br/><br/>");
            sb.append("<b>Complaint ID : " + complaintId + "</b><br /><br /><b>Complaint Subject :" + complSubject + "<br /><br /><table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >" + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Tender Details</th></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Tender ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(id).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(msgTender).append(" :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName2()).append("</td></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Closing Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName4()).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Opening Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName3()).append("</td></tr>"
                    + "</table>");
            sb.append("<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Complaint management~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("sendMailToPEForRegisterComp :" + e);
        }
        LOGGER.debug("sendMailToPEForRegisterComp Ends:");
        return sb.toString();
    }

    public String sendMailToAuthForClarifyComp(Integer id, List<SPTenderCommonData> thisTenderInfoLst, int complaintId, String firstName, String Cmptype) {
        LOGGER.debug("sendMailToAuthForClarifyComp Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            String msgTender = "Reference No.";
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/><span>");
            sb.append("This is to inform you that Bidder/Consultant ").append(firstName).append(" has responded clarification " + "for ").append(Cmptype).append("<br/><br/>");
            sb.append("<b>Complaint ID : " + complaintId + "</b><br /><br /><table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >" + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Tender Details</th></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Tender ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(id).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(msgTender).append(" :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName2()).append("</td></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Closing Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName4()).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Opening Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName3()).append("</td></tr>"
                    + "</table>");
            sb.append("<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Complaint management~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("sendMailToAuthForClarifyComp :" + e);
        }
        LOGGER.debug("sendMailToAuthForClarifyComp Ends:");
        return sb.toString();
    }

    public String sendMailToHighAutForEscalateComp(Integer id, List<SPTenderCommonData> thisTenderInfoLst, int complaintId, String firstName, String Cmptype) {
        LOGGER.debug("sendMailToHighAutForEscalateComp Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            String msgTender = "Reference No.";
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/><span>");
            sb.append("This is to inform you that Bidder/Consultant ").append(firstName).append(" has escalate  a Complaint " + "for ").append(Cmptype).append("<br/><br/>");
            sb.append("<b>Complaint ID : " + complaintId + "</b><br /><br /><table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >" + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Tender Details</th></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Tender ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(id).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(msgTender).append(" :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName2()).append("</td></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Closing Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName4()).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Opening Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName3()).append("</td></tr>"
                    + "</table>");
            sb.append("<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Complaint management~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("sendMailToHighAutForEscalateComp :" + e);
        }
        LOGGER.debug("sendMailToHighAutForEscalateComp Ends:");
        return sb.toString();
    }

    public String sendMailToHighAutForEscalateCompforRP(Integer id, List<SPTenderCommonData> thisTenderInfoLst, int complaintId, String firstName, String Cmptype) {
        LOGGER.debug("sendMailToHighAutForEscalateComp Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            String msgTender = "Reference No.";
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/><span>");
            sb.append("This is to inform you that Bidder/Consultant ").append(firstName).append(" has requested to assign a Complaint to Review Panel " + "for ").append(Cmptype).append("<br/><br/>");
            sb.append("<b>Complaint ID : " + complaintId + "</b><br /><br /><table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >" + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Tender Details</th></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Tender ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(id).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(msgTender).append(" :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName2()).append("</td></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Closing Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName4()).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Opening Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName3()).append("</td></tr>"
                    + "</table>");
            sb.append("<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("sendMailToHighAutForEscalateComp :" + e);
        }
        LOGGER.debug("sendMailToHighAutForEscalateComp Ends:");
        return sb.toString();
    }

    public String sendMailToTenForProcessComp(String role, Integer id, List<SPTenderCommonData> thisTenderInfoLst, int complaintId, String firstName, String complaintType, String status, String userName, String complaintSubject) {
        LOGGER.debug("sendMailToTenForProcessComp Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            String msgTender = "Reference No.";
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/><span>");
            if (status.equalsIgnoreCase("accept")) {
                sb.append("Dear User," + "<br /><br />This is to inform you that ").append(role + " " + userName).append(" has ").append(status + "ed Complaint for ").append(complaintType).append("<br/><br/>");
            } else if (status.equalsIgnoreCase("reject")) {
                sb.append("Dear User," + "<br /><br />This is to inform you that ").append(role + " " + userName).append(" has ").append(status + "ed Complaint for ").append(complaintType).append("<br/><br/>");
            } else {
                sb.append("Dear User," + "<br /><br />This is to inform you that ").append(role + " " + userName).append(" has ").append("requested clarification for ").append(complaintType).append("<br/><br/>");
            }

            sb.append("<b>Complaint ID : " + complaintId + "</b><br /><br /><b>Complaint Subject :" + complaintSubject + "<br /><br /><table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >" + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Tender Details</th></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Tender ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(id).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(msgTender).append(" :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName2()).append("</td></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Closing Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName4()).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Opening Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName3()).append("</td></tr>"
                    + "</table>");
            sb.append("<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Complaint management~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("sendMailToTenForProcessComp :" + e);
        }
        LOGGER.debug("sendMailToTenForProcessComp Ends:");
        return sb.toString();
    }

    public String sendMailToTenderernakerChecker(String complId, String bankName, String state) {
        LOGGER.debug("sendMailToTenderernakerChecker Starts:");
        StringBuilder sb = new StringBuilder();
        String msg = "";
        String msgTender = "Reference No.";
        try {
            if ("maker".equalsIgnoreCase(state)) {
                msg = "received";
            } else {
                msg = "verified";
            }
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/><span>");
            sb.append("Dear User," + "<br /><br />This is to inform you that Financial Institute ").append(bankName).append(" has ").append(msg).append(" payment for Complaint ID : ").append(complId).append("<br/><br/>");
            sb.append("<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Complaint management~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("sendMailToTenderernakerChecker :" + e);
        }
        LOGGER.debug("sendMailToTenderernakerChecker Ends:");
        return sb.toString();
    }

    public String sendMailToReviewPanel(int tenderId, List<SPTenderCommonData> thisTenderInfoLst, int complaintId, String firstName, String complaintType, String userName) {
        LOGGER.debug("sendMailToReviewPanel Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            String msgTender = "Reference No.";
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/><span>");
            sb.append("This is to inform you that DG(GPPMD) ").append(userName).append(" has assigned Complaint to review panel for ").append(complaintType).append("<br/><br/>");
            sb.append("<b>Complaint ID : " + complaintId + "</b><br /><br /><table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >" + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Tender Details</th></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Tender ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(tenderId).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(msgTender).append(" :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName2()).append("</td></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Closing Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName4()).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Opening Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName3()).append("</td></tr>"
                    + "</table>");
            sb.append("<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Complaint management~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("sendMailToReviewPanel :" + e);
        }
        LOGGER.debug("sendMailToReviewPanel Ends:");
        return sb.toString();
    }

    public String sendMailToPEForNoti(int tenderId, List<SPTenderCommonData> thisTenderInfoLst, int complaintId, String firstName, String complaintType, String userName, String comments) {
        LOGGER.debug("sendMailToPEForNoti Starts:");
        StringBuilder sb = new StringBuilder();
        try {
            String msgTender = "Reference No.";
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/><span>");
            sb.append("Dear User," + "<br /><br />This is to inform you that DG(GPPMD) ").append(userName).append(" has send notification for Complaint ID: ").append(complaintId).append("<br/><br/>");
            sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >" + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Tender Details</th></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Tender ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(tenderId).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(msgTender).append(" :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName2()).append("</td></tr>" + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Closing Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName4()).append("</td>" + "<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Opening Date and Time :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>").append(thisTenderInfoLst.get(0).getFieldName3()).append("</td></tr>"
                    + "</table><br/>");
            sb.append("<b>Description :-</b> " + comments);
            sb.append("<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Complaint management~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("sendMailToPEForNoti :" + e);
        }
        LOGGER.debug("sendMailToPEForNoti Ends:");
        return sb.toString();
    }

    public List onlinePaymentSuccess(OnlinePaymentDtBean opdb) {
        LOGGER.debug("sendMailToPEForNoti Starts:");
        StringBuilder sb = new StringBuilder();
        String paymentMode = "New User Registration";
        String sub = "Payment acknowledgement  for New User Registration from Electronic Government Procurement (e-GP) system";
        if ("renewal".equalsIgnoreCase(opdb.getFromWhere())) {
            paymentMode = "User Registration Renewal Fee";
            sub = "Payment acknowledgement  for Renewal  fees from Electronic Government Procurement (e-GP) system";
        } else if ("docFees".equalsIgnoreCase(opdb.getFromWhere())) {
            paymentMode = "Document Fees of Tender ID " + opdb.getTenderId();
            sub = "Payment acknowledgement  for Document fees payment from Electronic Government Procurement (e-GP) system";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = new Date();
        String dateString = dateFormat.format(date);
        List resetContent = new ArrayList();
        resetContent.add(sub);
        try {
            String msgTender = "Reference No.";
            resetContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/><span>"
                    + "Thank you! We have received an online payment of Nu. " + opdb.getTotal() + " against " + paymentMode + "  on " + dateString + " and your transaction number is " + URLDecoder.decode(opdb.getOnlineTraId(), "UTF-8") + "<br/><br/>"
                    + "<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>");
            resetContent.add("<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of of GPPMD Office to your Safe Sender list.\"</b></span>");
            resetContent.add("</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("sendMailToPEForNoti :" + e);
        }
        LOGGER.debug("sendMailToPEForNoti Ends:");
        resetContent.add(sb.toString());
        return resetContent;
    }

    public List onlineVASPaymentSuccess(OnlinePaymentDtBean opdb) {
        LOGGER.debug("send Mail For Misc Online Payment Starts:");
        StringBuilder sb = new StringBuilder();
        String sub = "Payment acknowledgement in Electronic Government Procurement (e-GP) system";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = new Date();
        String dateString = dateFormat.format(date);
        List resetContent = new ArrayList();
        resetContent.add(sub);
        try {
            resetContent.add("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/><span>"
                    + "Thank you! We have received an online payment of Nu. " + opdb.getTotal() + " against " + opdb.getPaymentDesc() + "  on " + dateString + " and your transaction number is " + URLDecoder.decode(opdb.getOnlineTraId(), "UTF-8") + "<br/><br/>"
                    + "<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>");
            resetContent.add("<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of of GPPMD Office to your Safe Sender list.\"</b></span>");
            resetContent.add("</td></tr></table>~New user registration~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("send Mail For Misc Online Payment :" + e);
        }
        LOGGER.debug("send Mail For Misc Online Payment Ends:");
        resetContent.add(sb.toString());
        return resetContent;
    }

    public String contAdmAdMail(int adId) {
        LOGGER.debug("contAdmAdMail Starts:");

        StringBuffer sb = new StringBuffer();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Advertisement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear Admin," + "<br/><br/><span>"
                    + "A new Advertisement with Ad ID : " + adId + " has been created on Electronic Government Procurement (e-GP) System."
                    + "Please verify Advertisement to approve or reject it.<br/><br/>"
                    + "<br/>Please note that the Advertisement of user will remain PENDING till the time you do not approve/reject it.");

            sb.append("<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Advertisement~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contAdmAdMail :" + e);
        }
        LOGGER.debug("contAdmAdMail Ends:");
        return sb.toString();
    }

    public String userMail(int adId, String status, Date publish, Date expiry, String comments, String path) {
        LOGGER.debug("userMail Starts:");

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

        StringBuffer sb = new StringBuffer();
        try {
            if (status.equalsIgnoreCase("published")) {
                String publishdate = dateFormat.format(publish);
                String expirydate = dateFormat.format(expiry);
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Advertisement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                        + "Dear User," + "<br/><br/><span>"
                        + "The Advertisement with Ad ID : " + adId + " has been published on Electronic Government Procurement (e-GP) System."
                        + "Please check Advertisement details.<br/><br/>"
                        + "<br/>Please note that the Advertisement published date is " + publishdate + "and expiry date is" + expirydate + "<br/> The content Admin Commets are as followed :" + comments + ".");
            } else if (status.equalsIgnoreCase("accepted") || status.equalsIgnoreCase("rejected")) {
                sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                        + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Advertisement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                        + "Dear User," + "<br/><br/><span>"
                        + "The Advertisement with Ad ID : " + adId + " has been processed on Electronic Government Procurement (e-GP) System."
                        + "<br/>Please note that the Advertisement status is " + status + "."
                        + "<br/> The content Admin Commets are as followed :" + comments + ".");
                if (status.equalsIgnoreCase("accepted")) {
                    sb.append("After receiving the payment only content admin will publish the Ad. Please make the payment through the following link " + path + "/RegVasFeePayment.jsp?adId=" + adId);
                }
            }
            sb.append("<br/><br/>"
                    + "Warm Regards,<br/>"
                    + "<b>Electronic Government Procurement (e-GP) Support Team.</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of of GPPMD Office to your Safe Sender list.\"</b></span>"
                    + "</td></tr></table>~Advertisement~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("userMail :" + e);
        }
        LOGGER.debug("userMail Ends:");
        return sb.toString();
    }

    public String adminMail() {
        LOGGER.debug("contAdmAdMail Starts:");

        StringBuffer sb = new StringBuffer();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Advertisement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear Admin," + "<br/><br/><span>"
                    + "A new configuration  for advertisement has been created on Electronic Government Procurement (e-GP) System."
                    + "Please configure price for the configuration created by content admin.<br/><br/>"
                    + "<br/>Please note that the price remains 0.00 till you configure price.");

            sb.append("<br/><br/>"
                    + "Thanks,<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table>~Advertisement~~</body></html>");
        } catch (Exception e) {
            LOGGER.error("contAdmAdMail :" + e);
        }
        LOGGER.debug("contAdmAdMail Ends:");
        return sb.toString();
    }

    public String FinalSubToBidderContent(String tenderId, String subDt, String peOfficeName, String refNo, String pkgNo, String pkgDes) {
        LOGGER.debug("FinalSubToBidderContent Starts:");

        StringBuffer sb = new StringBuffer();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledge</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that deadline for final submission in a below mentioned tender:<br/><br/>");

            sb.append("<table border='1'>");
            sb.append("<tr>");
            sb.append("<td width='40%'><b>PA Office Name :</b></td>");
            sb.append("<td width='60%'>" + peOfficeName + "</td>");
            sb.append("</tr>");
            sb.append("<tr>");
            sb.append("<td><b>Tender Notice ID :</b></td>");
            sb.append("<td>" + tenderId + "</td>");
            sb.append("</tr>");
            sb.append("<tr>");
            sb.append("<td><b>Package No.<br/>and Description :</b></td>");
            sb.append("<td>" + pkgNo + ",<br/>" + pkgDes + "</td>");
            sb.append("</tr>");
            sb.append("<tr>");
            sb.append("<td><b>Ref. No.</b></td>");
            sb.append("<td>" + refNo + "</td>");
            sb.append("</tr>");
            sb.append("<tr>");
            sb.append("<td><b>Date & Time of Submission :</b></td>");
            sb.append("<td>" + subDt + "</td>");
            sb.append("</tr>");
            sb.append("<br/>"
                    + "<b>Electronic Government Procurement (e-GP) System.</b><br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("emailIdNoReply") + "\">" + XMLReader.getMessage("emailIdNoReply") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("FinalSubToBidderContent :" + e);
        }
        LOGGER.debug("FinalSubToBidderContent Ends:");
        return sb.toString();
    }

    /*Dohatec Start For extending validity of performance security*/
    /**
     * it sends mail notification to tenderer on extending validity of
     * performance security by PE
     *
     * @param flag
     * @param peName
     * @param obj
     * @param userTypeId
     * @param UserTypeId
     * @param ProcRoleId
     * @param strfor
     * @return
     */
    public String mailForExtendingValidity(boolean flag, String peName, Object[] obj, String userTypeId, String UserTypeId, String ProcRoleId, String strfor, String TenderID) {
        LOGGER.debug("mailForExtendValidity Starts:");
        String str_consName = "";
        if ("1".equals(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.goodsName");
        } else if ("2".equalsIgnoreCase(obj[6].toString())) {
            str_consName = resbdl.getString("CMS.works.mailContent.worksName");
        } else {
            str_consName = resbdl.getString("CMS.works.mailContent.serviceName");
        }
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title><style type='text/css'>table.tableList_1 {color:#333; border-collapse:collapse;} table.tableList_1 th {border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;} table.tableList_1 td {border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;}.ff{font-weight: bold;}</style></head><body style='font-family: Arial; font-size: 12px; text-align:justify;'><table><tr><td>"
                    + "<br/>Dear User," + "<br/><br/><span>");
            if ("7".equalsIgnoreCase(UserTypeId)) {
                sb.append("This is to inform you that Financial Institute " + peName + " has "
                        + "Released " + strfor + " for below mention Contract:<br/><br/>");
            } else if ("2".equalsIgnoreCase(UserTypeId)) {
                sb.append("This is to inform you that bidder has accepted validity extension request of " + strfor + " from PE for below mention Contract:<br/><br/>");
            } else {
                sb.append("This is to inform you that PA " + peName + " has Requested To "
                        + "Extended validity of " + strfor + " for below mention Contract:<br/><br/>");
            }
            sb.append("<table width='100%' cellspacing='10' class='tableList_1 infoBarBorder' >"
                    + "<tr><th colspan='4'style='border:1px solid #FF9326; border-bottom:2px solid #FF9326; text-align:center; vertical-align: middle; padding:5px; background:#e4fad0;'>Contract Details</th></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract No :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[1].toString() + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Value :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[2].toString() + "</td></tr>"
                    + "<tr><td class='ff'style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract Start Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[3].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Contract End Date :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(), "yyyy-MM-dd HH:mm:ss")) + "</td></tr>"
                    + "<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>");
            sb.append(str_consName);
            if ("1".equals(obj[12].toString())) {
                String str = obj[10].toString() + " " + obj[11].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            } else {
                String str = obj[8].toString();
                sb.append("</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + str + "</td>");
            }
            sb.append("<td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>email ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + obj[9].toString() + "</td></tr>");
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Tender ID :</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + TenderID + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            if (!"3".equals(obj[6].toString())) {
                sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Payment Terms :</td>");
                String strPayTerms = "";
                if (obj[5] == null) {
                    strPayTerms = "-";
                } else if ("anyitemanyp".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Any Item Any Percent";
                } else if ("allitem100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "All Item 100 Percent";
                } else if ("itemwise100p".equalsIgnoreCase(obj[5].toString())) {
                    strPayTerms = "Item Wise 100 Percent";
                }
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>" + strPayTerms + "</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr>");
            }
            sb.append("<tr><td class='ff' style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Work Status :</td>");
            if (flag) {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Completed</td>");
            } else {
                sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>Pending</td>");
            }
            sb.append("<td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td><td style='border:1px solid #FF9326; text-align: left; vertical-align: top; padding:5px;'>&nbsp;</td></tr></table><br/><br/>");
            if ("3".equalsIgnoreCase(UserTypeId)) {
                if ("2".equalsIgnoreCase(userTypeId)) {
                    sb.append("To view the " + strfor + " Details, you are required to follow the mentioned steps:");
                } else if ("7".equalsIgnoreCase(userTypeId)) {
                    sb.append("To Extend Validity of " + strfor + ", you are required to follow the mentioned steps:");
                }
                if ("2".equalsIgnoreCase(userTypeId)) {
                    sb.append("<br/><ol style='padding-left:35px;'>"
                            + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                            + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                    if ("Bid Security".equalsIgnoreCase(strfor)) {
                        sb.append("<li style='padding-top:5px;'>Click on Payment Tab</li>");
                    } else {
                        sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                                + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");
                    }
                } else if ("7".equalsIgnoreCase(userTypeId)) {
                    sb.append("<br/><ol style='padding-left:35px;'>"
                            + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                            + "<li style='padding-top:5px;'>Click on Payment Tab >> Select Tender Payment option</li>"
                            + "<li style='padding-top:5px;'>Select Tender/Contract for which " + strfor + " validity need to be extend</li>"
                            + "<li style='padding-top:5px;'>Click on Extend Link</li>"
                            + "<li style='padding-top:5px;'>Change Date</li>"
                            + "<li style='padding-top:5px;'>Add Remarks</li>"
                            + "<li style='padding-top:5px;'>Click On Submit Button</li>");
                }
            } else if (!"25".equalsIgnoreCase(ProcRoleId)) {
                sb.append("To view the " + strfor + " Details, you are required to follow the mentioned steps:");
                sb.append("<br/><ol style='padding-left:35px;'>"
                        + "<li style='padding-top:5px;'>Log in to Electronic Government Procurement (e-GP) system</li>"
                        + "<li style='padding-top:5px;'>Select Tender/Contract</li>");
                if ("Bid Security".equalsIgnoreCase(strfor)) {
                } else {
                    sb.append("<li style='padding-top:5px;'>Click on CMS Tab</li>"
                            + "<li style='padding-top:5px;'>Click on View Payment Details link</li>");
                }
            }
            sb.append("</ol><br/>Thanks<br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForExtendValidity :" + e);
        }
        LOGGER.debug("mailForExtendValidity Ends:");
        return sb.toString();
    }

    public String mailForNotifyDebarredBidder(String details) {
        StringBuilder sb = new StringBuilder();
        LOGGER.debug("mailForNotifyDebarredBidder Starts:");
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that you have been debarred by the e-GP Admin on Electronic Government Procurement (e-GP) System. Debarment detail is as mentioned below: <br/>&nbsp;&nbsp;<br/>"
                    //                    + "<b>Debared for &nbsp;&nbsp;:</b>&nbsp;" + debaredFor + "<br/><br/>"
                    //                    + "<b>Detail &nbsp;&nbsp;:<br/><br/>"
                    + details + "<br/>&nbsp;&nbsp;<br/>"
                    //                    + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "Thanks.<br/><br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a> </b></div>"//| <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForNotifyDebarredBidder :" + e);
        }
        LOGGER.debug("mailForNotifyDebarredBidder Ends:");
        return sb.toString();
    }

    public String mailForNotifyDebarmentReinstate(String details) {
        StringBuilder sb = new StringBuilder();
        LOGGER.debug("mailForNotifyDebarmentReinstate Starts:");
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that your debarment status has been reinstated by the e-GP Admin on Electronic Government Procurement (e-GP) System for the procurement category mentioned below: <br/>&nbsp;&nbsp;<br/>"
                    //                    + "<b>Debared for &nbsp;&nbsp;:</b>&nbsp;" + debaredFor + "<br/><br/>"
                    //                    + "<b>Detail &nbsp;&nbsp;:<br/><br/>"
                    + details + "<br/>&nbsp;&nbsp;<br/>"
                    //                    + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "Thanks.<br/><br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a></b></div>"// | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForNotifyDebarmentReinstate :" + e);
        }
        LOGGER.debug("mailForNotifyDebarmentReinstate Ends:");
        return sb.toString();
    }

    public String mailForNotifyDebarmentUpdate(String details) {
        StringBuilder sb = new StringBuilder();
        LOGGER.debug("mailForNotifyDebarmentUpdate Starts:");
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "Dear User," + "<br/><br/><span>"
                    + "This is to inform you that your debarment status has been Updated by the e-GP Admin on Electronic Government Procurement (e-GP) System. Update detail is as mentioned below: <br/>&nbsp;&nbsp;<br/>"
                    //                    + "<b>Debared for &nbsp;&nbsp;:</b>&nbsp;" + debaredFor + "<br/><br/>"
                    //                    + "<b>Detail &nbsp;&nbsp;:<br/><br/>"
                    + details + "<br/>&nbsp;&nbsp;<br/>"
                    //                    + "For more details, Kindly logon to Electronic Government Procurement (e-GP) System.<br/><br/>"
                    + "Thanks.<br/><br/>"
                    + "Electronic Government Procurement (e-GP) System<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a> </b></div>" // | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("mailForNotifyDebarmentUpdate :" + e);
        }
        LOGGER.debug("mailForNotifyDebarmentUpdate Ends:");
        return sb.toString();
    }

    public String mailAfterTCApproval(String bidderName, String bidderAddress, String packageDes, String IdenRef, String bidAmount, String hopaName, String paAgency) {
        StringBuilder sb = new StringBuilder();
        LOGGER.debug("mailAfterTCApproval Starts:");
        try {
            sb.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
                    + "<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' /><title>Acknowledgement</title></head><body style='font-family: Arial; font-size: 12px;'><table><tr><td>"
                    + "To" + "<br/>"
                    +bidderName + "<br/>"
                    +bidderAddress + "<br/><br/><span>"
                    + "This is to notify you that, it is our intention to award the contract for your bid dated "+ DateUtils.formatStdDate(new Date()) +" for execution of the \""
                    + packageDes +"\", Identification Reference No. \""
                    + IdenRef +"\" for the Contract Price of "
                    + bidAmount +" as corrected and modified in accordance with the Instructions to Bidders.  <br/>&nbsp;&nbsp;<br/>"
                    //                    + "<b>Debared for &nbsp;&nbsp;:</b>&nbsp;" + debaredFor + "<br/><br/>"
                    //                    + "<b>Detail &nbsp;&nbsp;:<br/><br/>"
                    + "Thanks.<br/><br/>"
                    + hopaName + "<br/>"
                    + "Head of Procuring Agency"  + "<br/>"
                    + paAgency + "<br/><br/>"
                    + "<span style=\"color : red\">\"<b>To ensure you receive future emails, please add <a href=\"mailto:" + XMLReader.getMessage("spammsgemailid") + "\">" + XMLReader.getMessage("spammsgemailid") + "</a> email "
                    + "address of procuring agencies to your Safe Sender list.\"</b></span>"
                    + "<div align=\"center\"><br/><b>Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan | <a href='" + XMLReader.getMessage("url") + "'>" + XMLReader.getMessage("displayUrl") + "</a> </b></div>" // | <a href='" + XMLReader.getMessage("url") + "/HelpDesk.jsp'>Help Desk</a> </b></div>"
                    + "</td></tr></table></body></html>");
        } catch (Exception e) {
            LOGGER.error("mailAfterTCApproval :" + e);
        }
        LOGGER.debug("mailAfterTCApproval Ends:");
        return sb.toString();
    }
    
    /*DOhatec End for extending validity of performacne security*/
}
