/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.EvalClariPostBidderService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Karan
 */
public class BidderClarificationSrBean {

    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
    private TenderCommonService tenderCommonServiceFrm = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    private CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
    static final Logger logger = Logger.getLogger(BidderClarificationSrBean.class);
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String logUserId = "0";
    private AuditTrail auditTrail;

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        tenderCommonServiceFrm.setLogUserId(logUserId);
        commonXMLSPService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
    }

    
    /**
     * Retrieve Tender Lot Package Detail
     * @param tenderId from tbl_TenderMaster
     * @param strCondition
     * @return List of SPTenderCommonData
     */
    public List<SPTenderCommonData> getLotOrPackageData(String tenderId, String strCondition) {
        logger.debug("getLotOrPackageData : " + logUserId + loggerStart);
        List<SPTenderCommonData> stcd = null;
        try {
            stcd = tenderCommonServiceFrm.returndata("getlotorpackagebytenderid", tenderId, strCondition);
        } catch (Exception e) {
            logger.error("getLotOrPackageData : " + logUserId,e);
    }
        logger.debug("getLotOrPackageData : " + logUserId + loggerEnd);
        return stcd;
    }

    /**
     *List of Questions for Tender Forms Lot wise
     * @param tenderId from tbl_TenderMaster
     * @param pkgLotId from tbl_TenderLotSecurity
     * @param userId bidder userId
     * @return List of SPTenderCommonData
     */
    public List<SPTenderCommonData> getLotOrPackageFormsForQuestions(String tenderId, String pkgLotId, String userId, int evalCount) {
        logger.debug("getLotOrPackageFormsForQuestions : " + logUserId + loggerStart);
        List<SPTenderCommonData> stcd = null;
        try {
            String param1 = "", param2 = "";
            if (!"0".equalsIgnoreCase(pkgLotId)) {
                param1 = "td.tenderId = '" + tenderId + "' and packageLotId = " + pkgLotId;
                param2 = "tenderId = " + tenderId + " and pkgLotId = " + pkgLotId + " and userId = " + userId + " and queSentByTec = 1 and evalCount = " + evalCount;
            } else {
                param1 = "td.tenderId = '" + tenderId + "'";
                param2 = "tenderId = " + tenderId + " and userId = " + userId + " and queSentByTec = 1 and evalCount = " + evalCount;
         }
            stcd = tenderCommonServiceFrm.returndata("getFormsForEvlauationQuestions", param1, param2);
        } catch (Exception e) {
            logger.error("getLotOrPackageFormsForQuestions : " + logUserId,e);
    }
        logger.debug("getLotOrPackageFormsForQuestions : " + logUserId + loggerEnd);
        return stcd;
    }

     /**
      *Tender Lots selected by Bidder
      * @param tenderId from tbl_TenderMaster
      * @param userId bidder userId
      * @return List of SPTenderCommonData
      */
     public List<SPTenderCommonData> getTenderLots(String tenderId, String userId) {
        logger.debug("getTenderLots : " + logUserId + loggerStart);
        List<SPTenderCommonData> list = null;
        try {
            list = tenderCommonServiceFrm.returndata("getTenderLotsForBidderClarification", tenderId, userId);
        } catch (Exception e) {
            logger.error("getTenderLots : " + logUserId,e);
        }
        logger.debug("getTenderLots : " + logUserId + loggerEnd);
        //return tenderCommonServiceFrm.returndata("gettenderlotbytenderidForPayment", tenderId, "ts");
        return list;
    }

     /**
      *List of Tender Form Evaluation Questions
      * @param whereCondition search criteria
      * @return List of SPTenderCommonData
      */
     public List<SPTenderCommonData> getFormQuestions(String whereCondition) {
        logger.debug("getFormQuestions : " + logUserId + loggerStart);
        List<SPTenderCommonData> list = null;
        try {
            list = tenderCommonServiceFrm.returndata("getEvaluationFormQuestions", whereCondition, null);
        } catch (Exception e) {
            logger.error("getFormQuestions : " + logUserId,e);
    }
        logger.debug("getFormQuestions : " + logUserId + loggerEnd);
        return list;
    }

     /**
      *Updates Answers
      * @param strAnswer answer
      * @param questionId question Id
      * @param tenderId from tbl_TenderMaster
      * @param tenRefNo from tbl_TenderDetail
      * @param tenderName from tbl_TenderMaster
      * @return true or false for success or fail
      * @throws Exception
      */
     public boolean updAnswers(String strAnswer, String questionId, String tenderId, String tenRefNo,String tenderName) throws Exception {
        logger.debug("updAnswers : " + logUserId + loggerStart);
        boolean isUpdate = false;
        String action = "Bidder Give Clarification to TEC CP";
        try {
        CommonMsgChk commonMsgChk = new CommonMsgChk();
            if (!"".equalsIgnoreCase(strAnswer)) {
                commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalFormQues", "answer='" + strAnswer + "', ansDt=GetDate()", "evalQueId=" + questionId).get(0);
                    isUpdate = commonMsgChk.getFlag();
                     EvalClariPostBidderService evalClariPostBidderService = (EvalClariPostBidderService) AppContext.getSpringBean("EvalClariPostBidderService");
                    String s_officeName = evalClariPostBidderService.getPeOffice(Integer.parseInt(tenderId));
                    //sendMsgSeekClari(tenderId, tenRefNo, s_officeName, tenderName);
            }
        } catch (Exception e) {
            logger.error("updAnswers : " + logUserId,e);
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(tenderId), "TenderId", EgpModule.Evaluation.getName(), action, "");
            action = null;
        }
        logger.debug("updAnswers : " + logUserId + loggerEnd);
        return isUpdate;
    }

     /**
      *List of Bidder Clarification
      * @param tenderId from tbl_TenderMaster
      * @param userId bidder userId
      * @return List of SPTenderCommonData
      */
     public List<SPTenderCommonData> getBidderClarificationInfo(String tenderId, String userId) {
        logger.debug("getBidderClarificationInfo : " + logUserId + loggerStart);
        List<SPTenderCommonData> sptcd = null;
        try {
            sptcd = tenderCommonServiceFrm.returndata("getBidderClarificationCondition", tenderId, userId);
        } catch (Exception e) {
            logger.error("getBidderClarificationInfo : " + logUserId,e);
    }
        logger.debug("getBidderClarificationInfo : " + logUserId + loggerEnd);
        return sptcd;
    }

      /**
       *Updates Bidder Response
       * @param tenderId from tbl_TenderMaster
       * @param userId bidder userId
       * @param tenderRefNo from tbl_TenderMaster
       * @return true or false for success or false
       * @throws Exception
       */
      public boolean updConfirmation(String tenderId, String userId, String tenderRefNo) throws Exception {
        logger.debug("updConfirmation : " + logUserId + loggerStart);
        boolean isUpdated = false;
        String action = "Notify TEC CP by tenderer";
        try {
        CommonMsgChk commonMsgChk = new CommonMsgChk();
        if (!"".equalsIgnoreCase(tenderId)) {
            commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalBidderResp", "isClarificationComp='yes'", "tenderId=" + tenderId + " And " + "userId=" + userId).get(0);
        }
            isUpdated = commonMsgChk.getFlag();

            if (isUpdated) {
                String strPEOffice = "";
                String tendererCompanyNm = "";


                List<SPTenderCommonData> lstOfficerUserId =
                        tenderCommonServiceFrm.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);

                if (!lstOfficerUserId.isEmpty()) {
                    strPEOffice = lstOfficerUserId.get(0).getFieldName3(); // Get PE Office Name
                }

                List<SPTenderCommonData> lstTendererCompany =
                        tenderCommonServiceFrm.returndata("getTendererCompanyName", userId, null);
                tendererCompanyNm = lstTendererCompany.get(0).getFieldName1();

                sendMsgSeekClari(tenderId, tenderRefNo, strPEOffice, tendererCompanyNm);
            }

        } catch (Exception e) {
            logger.error("updConfirmation : " + logUserId,e);
            action = "Error in "+action+" "+e.getMessage();
        }finally{
            makeAuditTrailService.generateAudit(auditTrail, Integer.parseInt(tenderId), "TenderId", EgpModule.Evaluation.getName(), action, "");
            action = null;
        }
        logger.debug("updConfirmation : " + logUserId + loggerEnd);
        return isUpdated;
    }

      /**
       *Get FormName By formId
       * @param formId from tbl_TenderForms
       * @return Name of the Form
       */
      public String getFormName(String formId) {
        logger.debug("getFormName : " + logUserId + loggerStart);
        String frmNm = "";
        try {
            List<SPTenderCommonData> lstFrmNm = tenderCommonServiceFrm.returndata("getFormNameFromFormId", formId, null);
            if (!lstFrmNm.isEmpty()) {
                frmNm = lstFrmNm.get(0).getFieldName1();
            }
            lstFrmNm = null;
        } catch (Exception e) {
            logger.error("getFormName : " + logUserId,e);
        }
        logger.debug("getFormName : " + logUserId + loggerEnd);
        return frmNm;
    }
        
      /**
       *Sending mail and message box for Seek Clarification
       * @param tenderId from tbl_TenderMaster
       * @param refNo from tbl_TenderMaster
       * @param peOfficeName from tbl_OfficeMaster
       * @param tenderName from tbl_TenderMaster
       * @return true or false for success or false
       */
      public boolean sendMsgSeekClari(String tenderId, String refNo, String peOfficeName, String tenderName) {
        logger.debug("sendMsgSeekClari : " + logUserId + " Starts :");
        boolean flag = false;
        try {
            List<SPCommonSearchDataMore> list_Mail = new ArrayList<SPCommonSearchDataMore>();
            CommonSearchDataMoreService commonService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            list_Mail = commonService.geteGPData("getTecMember", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            for(SPCommonSearchDataMore  mailId: list_Mail){
                String[] s_mailId = {mailId.getFieldName1()};
                registerService.contentAdmMsgBox(mailId.getFieldName1(), XMLReader.getMessage("emailIdNoReply"), "e-GP: Bidder has provided the Clarification in a Tender", msgBoxContentUtility.bidderAnsCp(tenderId, refNo, peOfficeName, tenderName));
                sendMessageUtil.setEmailTo(s_mailId);
                            sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                            sendMessageUtil.setEmailSub("e-GP: Bidder has provided the Clarification in a Tender");
                            String mailConte = mailContentUtility.bidderAnsCp(tenderId, refNo, peOfficeName, tenderName);
                            sendMessageUtil.setEmailMessage(mailConte);
                            sendMessageUtil.sendEmail();
            }
            
            flag = true;
        } catch (Exception e) {
            logger.error("sendMsgSeekClari : " + logUserId + e);
        }
        logger.debug("sendMsgSeekClari : " + logUserId + " Ends:");
        return flag;
    }
}
