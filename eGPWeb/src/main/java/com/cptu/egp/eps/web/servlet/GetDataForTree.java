/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblDepartmentMaster;
import com.cptu.egp.eps.service.serviceimpl.DepartmentCreationServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author dipti
 */
public class GetDataForTree extends HttpServlet {

    private final DepartmentCreationServiceImpl deptTree = (DepartmentCreationServiceImpl) AppContext.getSpringBean("DepartmentCreationService");
    private static final Logger LOGGER = Logger.getLogger(GetDataForTree.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND = "Ends";
    private String logUserId = "0";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        if (session.getAttribute("userId") != null) {
            logUserId = session.getAttribute("userId").toString();
            deptTree.setUserId(logUserId);
        }

        LOGGER.debug("processRequest : " + logUserId + LOGGERSTART);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            short id = 0;
            boolean show = true;
//            String deptType = "Ministry";
            if (request.getParameter("id") != null) {
                id = Short.parseShort(request.getParameter("id"));
            }
            if (request.getParameter("showPrNd") != null) {
                show = Boolean.parseBoolean(request.getParameter("showPrNd"));
            }

//            if(request.getParameter("deptType") != null){
//                deptType = request.getParameter("deptType");
//            }
            List<TblDepartmentMaster> deptLst = deptTree.getChildDepartments(id, show);

            JSONArray jSONArray = new JSONArray();
            String para[] = {"attr", "data", "state", "pdeptid"};

            List l = new ArrayList();
            for (TblDepartmentMaster tblDepartmentMaster : deptLst) {
                l.add(tblDepartmentMaster.getDepartmentId());
                l.add(tblDepartmentMaster.getDepartmentName());
                l.add(tblDepartmentMaster.getDepartmentType());
                l.add(tblDepartmentMaster.getParentDepartmentId());
            }

            for (int j = 0; j < (l.size() / para.length); j++) {
                JSONObject jSONObject = new JSONObject();
                for (int i = 0; i < para.length; i++) {
                    int c = (para.length * j) + i;
                    switch (i) {
                        case 3:
                            jSONObject.put(para[i], "pdeptid_" + l.get(c));
                            break;
                        case 2:
                            jSONObject.put(para[i], "closed");
                            break;
                        case 1:
                            if (l.get(c + 1).toString().equals("Division")) {
                                jSONObject.put(para[i], l.get(c).toString() + "(Dep.)");
                            } else if (l.get(c + 1).toString().equals("District")) {
                                jSONObject.put(para[i], l.get(c).toString() + "(Dzon.)");
                            } else if (l.get(c + 1).toString().equals("SubDistrict")) {
                                jSONObject.put(para[i], l.get(c).toString() + "(Dung.)");
                            } else if (l.get(c + 1).toString().equals("Gewog")) {
                                jSONObject.put(para[i], l.get(c).toString() + "(Gewog)");
                            } else if (l.get(c + 1).toString().equals("Organization")) {
                                jSONObject.put(para[i], l.get(c).toString() + "(Div.)");
                            } else {
                                jSONObject.put(para[i], l.get(c).toString());
                            }
                            break;
                        case 0:
                            JSONObject jobj = new JSONObject();
                            jobj.put("id", "deptid_" + l.get(c).toString());
                            jobj.put("dtype", l.get(c + 2).toString());
                            jobj.put("dname", l.get(c + 1).toString());
                            jSONObject.put(para[i], jobj);
                            jobj = null;
                            break;
                        default:
                            break;
                    }
                }
                jSONArray.put(jSONObject);
                jSONObject = null;
            }
            out.print(jSONArray);
            //out.print("<br>");
            /*out.print("[{ \"attr\" : {\"id\":\"1\"}, \"data\" : \"Minister A\", \"state\" : \"closed\" }," +
                           "{ \"attr\" : {\"id\":\"2\"}, \"data\" : \"Minister B\", \"state\" : \"closed\" }," +
                           "{ \"attr\" : {\"id\":\"3\", \"rel\":\"folder\"}, \"data\" : \"Minister C\", \"state\" : \"closed\" }," +
                           "{ \"attr\" : {\"id\":\"4\", \"rel\":\"folder\"}, \"data\" : \"Minister D\", \"state\" : \"closed\", \"pDeptId\" : \"-1\" }]");*/
            jSONArray = null;
            if (l != null) {
                l = null;
            }
        } catch (Exception ex) {
            LOGGER.error("processRequest " + logUserId + " : " + ex.toString());
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : " + logUserId + LOGGEREND);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
