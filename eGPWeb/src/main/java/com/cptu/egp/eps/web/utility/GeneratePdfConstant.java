/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.utility;

/**
 *
 * @author Swati Patel
 * Created On 21st Dec. 2010
 */
public class GeneratePdfConstant {

    public final String TENDERNOTICE = "TenderNotice";
    public final String APPVIEW = "AppView";
    public final String STDPDF = "STDPdf";
    public final String TENDERDOC = "TenderDoc";
    public final String LOGINREPORT = "LoginReport";
    public final String PRETENDERMEETING = "PreTenderMeeting";
    public final String REGISTRATIONRENEWAL = "RegistrationRenewal";
    public final String SUBMISSIONRECEIPT = "SubmissionReceipt";
    public final String NOA = "NOA";
    public final String PAYMENT = "PaymentReport";
    public final String GRANDSUMMARY = "GrandSummary";
    public final String EvalPdf = "EvalPdf";
    public final String TenderPayment = "DocFeesPaymentReport";
    public final String TENDERDETAIL = "TenderDetail";
    public final String REGISTRATIONPAYMENT = "RegistrationPayment";
    public final String NOAFORMS = "NoaForms";
    public final String PROJECT = "Projectpdf";
    public final String AUDITTRAIL = "AuditTrail";
    public final String MISCPMTRPT = "MiscPaymentReport";
    public final String PROMISREPORT = "PromiseReport";
    public final String PROMISOVERALLREPORT = "PromiseOverallReport";
    public final String LOI = "LOI";
}
