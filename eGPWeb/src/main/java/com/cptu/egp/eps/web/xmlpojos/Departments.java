/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.xmlpojos;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Chalapathi.Bavisetti
 */
public class Departments {
     private short departmentId;
     private String departmentName;
     private String departmentType;

    /**
     * @return the departmentId
     */
      @XmlElement(nillable = true)
    public short getDepartmentId() {
        return departmentId;
    }

    /**
     * @param departmentId the departmentId to set
     */
    public void setDepartmentId(short departmentId) {
        this.departmentId = departmentId;
    }

    /**
     * @return the departmentName
     */
    @XmlElement(nillable = true)
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * @param departmentName the departmentName to set
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
     /**
     * @return the departmentType
     */
    // Added by sudhir 09072011 To identify for which types of department.
    @XmlElement(nillable = true)
    public String getDepartmentType() {
        return departmentType;
    }

    /**
     * @param departmentType the departmentType to set
     */
    public void setDepartmentType(String departmentType) {
        this.departmentType = departmentType;
    }

}
