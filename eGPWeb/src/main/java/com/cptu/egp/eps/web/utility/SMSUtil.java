/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

/**
 *
 * @author Administrator
 */
public class SMSUtil {

    public static void sendSMS(String mobileNo,String message) {
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        sendMessageUtil.setSendBy("SMS");
        sendMessageUtil.setSmsNo(mobileNo.replace("+8800", "+880"));
        sendMessageUtil.setSmsBody(message);
        Thread sender = new Thread(sendMessageUtil);
        sender.start();
    }    
}
