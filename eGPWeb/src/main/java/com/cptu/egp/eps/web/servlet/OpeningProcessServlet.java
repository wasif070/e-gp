/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceimpl.TenderOpeningServiceImpl;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.XMLReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class OpeningProcessServlet extends HttpServlet {

    private TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    TenderOpeningServiceImpl openingServiceImpl = (TenderOpeningServiceImpl) AppContext.getSpringBean("TenderOpeningService");
    private static final Logger LOGGER = Logger.getLogger(TenderPaymentServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        boolean isError = false;
        String logUserId = "0";
        String strPath = null;
        String tenderId = null;
        String tenderRefNo = null;
        String pageNm = "";
        String querString = "";
        String path = "";
        String msgTxt = "";
        String referer = "";

        try {
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
            } 
            tenderCommonService.setLogUserId(logUserId);
            LOGGER.debug("processRequest : " + logUserId + " Starts");

            if (request.getParameter("action").equals("notifype")) {
                if (request.getParameter("btnNotifyPE") != null) {
                    boolean mailSent = false;
                    tenderId = request.getParameter("tenderId");
                    tenderRefNo = request.getParameter("refNo");

                    try {
                        mailSent = SendNoBidMail(tenderId, tenderRefNo);

                        if (mailSent) {
                            msgTxt = "penotified";
                        } else {
                            msgTxt = "penotificationfailed";
                        }

                        pageNm = request.getContextPath() + "/officer/OpenComm.jsp";
                        querString = "tenderid=" + tenderId + "&msgId=" + msgTxt;
                        path = pageNm + "?" + querString;

                        response.sendRedirect(path);
                    } catch (Exception e) {
                        LOGGER.error("Error in Sending Notification mail to PE: " + e.toString());
                    }

                }

            } else if (request.getParameter("action").equals("autoReportGeneration")) {
                String tenId = request.getParameter("rptTenderId");
                String reportType = request.getParameter("rptType");
                boolean isGenerated = false;
                String rptMessage = "";
                try {
                    isGenerated = generateAutoReport(tenId, reportType);
                    pageNm = request.getContextPath() + "officer/OpenComm.jsp";
                    querString = "tenderid=" + tenId;
                    if(isGenerated)
                    {
                        List<Object> rtype = openingServiceImpl.getAutoGenerateReportType(Integer.parseInt(tenId));
                        if (rtype == null && rtype.isEmpty()) {
                                rptMessage = "TORTERFail";
                        } else if (rtype.size() == 1 && rtype.contains("TOR")) {
                                rptMessage = "TOR";
                        } else if (rtype.size() == 1 && rtype.contains("TER")) {
                                rptMessage = "TER";
                        } else if (rtype.size() == 2) {
                                rptMessage = "TORTER";
                        }
                            path = pageNm + "?" + querString+"&rptMessage="+rptMessage;
                    } else {
                            rptMessage = "TORTERFail";
                            path = pageNm + "?" + querString+"&rptMessage="+rptMessage;
                    }
                    
                } catch (Exception e) {
                            LOGGER.error("Error in AutoGenerationReport :"+e.toString());
                            rptMessage = "TORTERFail";
                            path = pageNm + "?" + querString+"&rptMessage="+rptMessage;
                }
                response.sendRedirect(path);
            }
        } finally {
           out.close();
        }
    }

    /* this method is for sending the mail     
     * @param tenderId
     * @param TenderRefNo
     * @return boolean true-if send successfully , false - if failed
     * @throws Exception
     */
    public boolean SendNoBidMail(String tenderId, String TenderRefNo) throws Exception {

        boolean isSent = false;
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        LOGGER.debug("SendNoBidMail : Starts");
        try {
            String mailSubject = "";
            String peOfficerUserId = "";

            mailSubject = "e-GP: Notification for No Tenders received";

            /* START : CODE TO SEND MAIL */
            List<SPTenderCommonData> lstOfficerUserId =
                    tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
            peOfficerUserId = lstOfficerUserId.get(0).getFieldName1();

            List<SPTenderCommonData> peOfficerEmail =
                    tenderCommonService.returndata("getEmailIdfromUserId", peOfficerUserId, null);


            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            String[] mail = {peOfficerEmail.get(0).getFieldName1()};
            //SendMessageUtil sendMessageUtil = new SendMessageUtil();
            //MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            //String mailText = mailContentUtility.contBankUser_ReleaseForfeit(tenderId,TenderRefNo, paymentFor, actionTaken, tendererCompanyNm);
            //sendMessageUtil.setEmailTo(mail);
            //sendMessageUtil.setEmailSub(HandleSpecialChar.handleSpecialChar(mailSubject));
            //sendMessageUtil.setEmailMessage(mailText);
            userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contOpening_NoBidMsg(tenderId, TenderRefNo));
            //sendMessageUtil.sendEmail();

            //sendMessageUtil=null;
            //mailContentUtility=null;
            mail = null;
            msgBoxContentUtility = null;
            /* END : CODE TO SEND MAIL */

            //Nullify List objects
            peOfficerEmail = null;
            lstOfficerUserId = null;
            // Nullify String objects
            mailSubject = null;
            peOfficerUserId = null;


            isSent = true;
            LOGGER.debug("SendNoBidMail : Ends");
        } catch (Exception e) {
            LOGGER.error("Error in SendNoBidMail : " + e.toString());
            isSent = false;
        }

        return isSent;
    }

    public boolean generateAutoReport(String tenderId, String reportType) {
        try {
            LOGGER.debug("generateAutoReport : Starts");
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            List<SPTenderCommonData> generateReportId = tenderCommonService.returndata("getAutoTORandTER", tenderId, reportType);
            LOGGER.debug("generateAutoReport : Ends");
            return true;
        } catch(Exception e) {
            LOGGER.debug("generateAutoReport : " + e.toString());
            return false;
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
