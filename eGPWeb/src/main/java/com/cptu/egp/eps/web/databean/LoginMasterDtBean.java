/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.web.utility.SHA1HashEncryption;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class LoginMasterDtBean implements java.io.Serializable{

    /** Creates a new instance of UserMasterDataBean */
    public LoginMasterDtBean() {
    }
    private String emailId;
    private String password;
    private String hintQuestion;
    private String hintAnswer;
    private String registrationType;
    private String isJvca="no";
    private String businessCountryName;
    private String nextScreen;
    private String isEmailVerified;
    private byte failedAttempt;
    private Date validUpTo;
    private String firstLogin;
    private String isPasswordReset;
    private String resetPasswordCode;
    private String status;
    private Date registeredDate;
    private String nationality;
    private String confirmPassWord;
    private String hintQuestionOwn;

    public String getHintQuestionOwn() {
        return hintQuestionOwn;
    }

    public void setHintQuestionOwn(String hintQuestionOwn) {
        this.hintQuestionOwn = hintQuestionOwn;
    }


    public String getConfirmPassWord() {
        return confirmPassWord;
    }

    public void setConfirmPassWord(String confirmPassWord) {
        this.confirmPassWord = confirmPassWord;
    }

    public String getBusinessCountryName() {
        return businessCountryName;
    }

    public void setBusinessCountryName(String businessCountryName) {
        this.businessCountryName = businessCountryName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public byte getFailedAttempt() {
        return failedAttempt;
    }

    public void setFailedAttempt(byte failedAttempt) {
        this.failedAttempt = failedAttempt;
    }

    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }

    public String getHintAnswer() {
        return hintAnswer;
    }

    public void setHintAnswer(String hintAnswer) {
        this.hintAnswer = hintAnswer;
    }

    public String getHintQuestion() {
        return hintQuestion;
    }

    public void setHintQuestion(String hintQuestion) {
        this.hintQuestion = hintQuestion;
    }

    public String getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(String isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public String getIsJvca() {
        return isJvca;
    }

    public void setIsJvca(String isJvca) {
        this.isJvca = isJvca;
    }

    public String getIsPasswordReset() {
        return isPasswordReset;
    }

    public void setIsPasswordReset(String isPasswordReset) {
        this.isPasswordReset = isPasswordReset;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNextScreen() {
        return nextScreen;
    }

    public void setNextScreen(String nextScreen) {
        this.nextScreen = nextScreen;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = SHA1HashEncryption.encodeStringSHA1(password);
    }

    public Date getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(Date registeredDate) {
        this.registeredDate = registeredDate;
    }

    public String getRegistrationType() {
        return registrationType;
    }

    public void setRegistrationType(String registrationType) {
        this.registrationType = registrationType;
    }

    public String getResetPasswordCode() {
        return resetPasswordCode;
    }

    public void setResetPasswordCode(String resetPasswordCode) {
        this.resetPasswordCode = resetPasswordCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getValidUpTo() {
        return validUpTo;
    }

    public void setValidUpTo(Date validUpTo) {
        this.validUpTo = validUpTo;
    }    
}
