/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.web.servicebean.RegExtEvalCommMemberSrBean;
import com.cptu.egp.eps.web.utility.DateUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Rishita
 */
public class ExtEvalCommMemberServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if (request.getParameter("action").equals("fetchData")) {
                response.setContentType("text/xml;charset=UTF-8");
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");

                RegExtEvalCommMemberSrBean regExtEvalCommMemberSrBean = new RegExtEvalCommMemberSrBean();

                regExtEvalCommMemberSrBean.setSearch(_search);
                regExtEvalCommMemberSrBean.setLimit(Integer.parseInt(rows));
                int offset = ((Integer.parseInt(page) - 1) * Integer.parseInt(rows));
                //System.out.println("offset:" + +offset);

                regExtEvalCommMemberSrBean.setOffset(offset);
                regExtEvalCommMemberSrBean.setSortOrder(sord);
                regExtEvalCommMemberSrBean.setSortCol(sidx);

                String searchField = "", searchString = "", searchOper = "";
                List<Object[]> tblExternalMemberListing = null;
                try {
                    if (_search) {
                        searchField = request.getParameter("searchField");
                        searchString = request.getParameter("searchString");
                        searchOper = request.getParameter("searchOper");
                        tblExternalMemberListing = regExtEvalCommMemberSrBean.getExternalMemberList_Search(searchField, searchString, searchOper);
                    } else {
                        String ascClause = null;
                        if (sidx.equalsIgnoreCase("")) {
                            ascClause = "temi.creationDt desc";
                        } else if (sidx.equalsIgnoreCase("emailId")) {
                            ascClause = "tlm." + sidx + " " + sord;
                        } else if (sidx.equalsIgnoreCase("fullName")) {
                            ascClause = "temi." + sidx + " " + sord;
                        } else if (sidx.equalsIgnoreCase("creationDt")) {
                            ascClause = "temi." + sidx + " " + sord;
                        }
                        tblExternalMemberListing = regExtEvalCommMemberSrBean.getExternalMemberList(ascClause);
                    }
                    int totalPages = 0;
                        int totalCount = 0;
                        if (_search) {
                            totalCount = (int) regExtEvalCommMemberSrBean.getAllCountOfExternalMember(searchField, searchString, searchOper);
                        } else {
                            totalCount = (int) regExtEvalCommMemberSrBean.getAllCountOfExternalMember();
                        }

                        if (totalCount > 0) {
                            if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                                totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                            } else {
                                totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                            }

                        } else {
                            totalPages = 0;
                        }

                        out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                        out.print("<rows>");
                        out.print("<page>" + request.getParameter("page") + "</page>");

                        out.print("<total>" + totalPages + "</total>");
                        out.print("<records>" + tblExternalMemberListing.size() + "</records>");
                        int j = 0;
                        int no = Integer.parseInt(request.getParameter("page"));
                        if (no == 1) {
                            j = 1;
                        } else {
                            if (Integer.parseInt(rows) == 30) {
                                j = ((no - 1) * 30) + 1;
                            } else if (Integer.parseInt(rows) == 20) {
                                j = ((no - 1) * 20) + 1;
                            } else {
                                j = ((no - 1) * 10) + 1;
                            }
                        }

                        // be sure to put text data in CDATA
                        for (int i = 0; i < tblExternalMemberListing.size(); i++) {
                            out.print("<row id='" + tblExternalMemberListing.get(i)[3] + "'>");
                            out.print("<cell>" + j + "</cell>");
                            out.print("<cell><![CDATA[" + tblExternalMemberListing.get(i)[0] + "]]></cell>");
                            out.print("<cell><![CDATA[" + tblExternalMemberListing.get(i)[1] + "]]></cell>");
                            out.print("<cell><![CDATA[" + DateUtils.gridDateToStrWithoutSec((Date)tblExternalMemberListing.get(i)[2]) + "]]></cell>");
                            String link = "<a href=\"EditExtEvalCommMember.jsp?uId=" + tblExternalMemberListing.get(i)[4] + "\">Edit</a> | <a href=\"ViewRegExtEvalCommMember.jsp?uId=" + tblExternalMemberListing.get(i)[4] + "&action=view\">View</a>";
                            out.print("<cell><![CDATA[" + link + "]]></cell>");
                            out.print("</row>");
                            j++;
                        }
                        out.print("</rows>");
                } catch (Exception ex) {
                    Logger.getLogger(ExtEvalCommMemberServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
