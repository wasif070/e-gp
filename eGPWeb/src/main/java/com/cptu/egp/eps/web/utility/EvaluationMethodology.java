/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nishit
 */
public class EvaluationMethodology {
    
    /**
     * This method give evaluation Methods
     * @param methodType TenderId
     * @return in obj[] give frist Status and in second it will give second status eg compleid and noncomplied
     */
    public List<String> getType(String methodType){
            CommonSearchService commonSearchService = (CommonSearchService)AppContext.getSpringBean("CommonSearchService");
         for (SPCommonSearchData evalList : commonSearchService.searchData("getProcurementMethod", methodType, null, null, null, null, null, null, null, null)) {
                 methodType = evalList.getFieldName1();
         }
        List<String> list = new ArrayList<String>();
        //String obj,obj1,obj2;
         
        if(methodType.equalsIgnoreCase("OTM") || methodType.equalsIgnoreCase("LTM")){
            list.add("Accepted");
            list.add("Rejected");
        }else if(methodType.equalsIgnoreCase("TSTM")){
            list.add("Responsive");
            list.add("Unresponsive");
        }else if(methodType.equalsIgnoreCase("REOI")){
            list.add("Excellent");
            list.add("Very Good");
            list.add("Good & Poor");
        }else if(methodType.equalsIgnoreCase("PQ")){
            list.add("Qualified");
            list.add("Conditionally Qualified");
            list.add("Disqualified");
        }else{
            list.add("Technically Qualified");
            list.add("Technically NOT Qualified");
        }
        return list;
    }
    
}
