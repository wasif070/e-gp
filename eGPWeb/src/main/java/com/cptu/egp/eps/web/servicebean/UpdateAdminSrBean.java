/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblAdminMaster;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblOfficeAdmin;
import com.cptu.egp.eps.model.table.TblOfficeMaster;
import com.cptu.egp.eps.service.serviceinterface.ManageAdminService;
import com.cptu.egp.eps.web.databean.AdminMasterDtBean;
import com.cptu.egp.eps.web.databean.ManageAdminDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

public class UpdateAdminSrBean {

    private String logUserId = "0";
    final static Logger LOGGER = Logger.getLogger(UpdateAdminSrBean.class);
    private ManageAdminService manageAdminService = (ManageAdminService) AppContext.getSpringBean("ManageAdminService");
    private static final String START = " Starts";
    private static final String END = " Ends";
    private AuditTrail auditTrail;
    int deptid;

    /**
     * For logging purpose
     *
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        manageAdminService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    public int getDeptid() {
        return deptid;
    }

    public void setDeptid(int deptid) {
        this.deptid = deptid;
    }

    /**
     * fetching data for PE admin for particular userId
     *
     * @param userId
     * @return data for userId
     */
    public List<ManageAdminDtBean> getOfficeList(int userId) {
        LOGGER.debug("getOfficeList : " + logUserId + START);
        List<ManageAdminDtBean> manageDetailList = new ArrayList<ManageAdminDtBean>();
        try {
            if (manageDetailList.isEmpty()) {
                for (Object[] obj : manageAdminService.getOfficeList(userId)) {
                    manageDetailList.add(new ManageAdminDtBean((String) obj[0], (String) obj[1], (Integer) obj[2], (Short) obj[3]));
                    setDeptid(manageDetailList.get(0).getDepartmentId());
                }
            }
        } catch (Exception e) {
            LOGGER.error("getOfficeList : " + logUserId + e);
        }
        LOGGER.debug("getOfficeList : " + logUserId + END);
        return manageDetailList;
    }

    /**
     * fetch office list for particular userId
     *
     * @param userId
     * @return list of offices
     */
    public List<ManageAdminDtBean> getOfficeAllList(int userId) {

        LOGGER.debug("getOfficeAllList : " + logUserId + START);
        List<ManageAdminDtBean> manageDetailList = new ArrayList<ManageAdminDtBean>();
        try {
            if (manageDetailList.isEmpty()) {
                for (Object[] obj : manageAdminService.getOfficeAllList(userId)) {
                    manageDetailList.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1]));
                }
            }
        } catch (Exception e) {
            LOGGER.error("getOfficeAllList : " + logUserId + e);
        }
        LOGGER.debug("getOfficeAllList : " + logUserId + END);
        return manageDetailList;
    }

    /**
     * fetching data for particular userId
     *
     * @param userTypeId
     * @param userId
     * @return Object with data
     */
    public List<ManageAdminDtBean> getManageDetail(byte userTypeid, int userId) {

        LOGGER.debug("getManageDetail : " + logUserId + START);
        List<ManageAdminDtBean> manageDetailList = new ArrayList<ManageAdminDtBean>();
        try {
            if (userTypeid == 5) {
                for (Object[] obj : manageAdminService.getManageAdminDetail(userId, userTypeid)) {
                    manageDetailList.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (String) obj[7], (Short) obj[8], (String) obj[9]));
                }
            } else if (userTypeid == 8) {
                for (Object[] obj : manageAdminService.getManageAdminContentDetail(userId, userTypeid)) {
                    manageDetailList.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8]));
                }
            } else if (userTypeid == 12) {
                for (Object[] obj : manageAdminService.getManageAdminContentDetail(userId, userTypeid)) {
                    manageDetailList.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8]));
                }
            } else if (userTypeid == 19) {
                for (Object[] obj : manageAdminService.getManageAdminContentDetail(userId, userTypeid)) {
                    manageDetailList.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8]));
                }
            } else if (userTypeid == 20) {
                for (Object[] obj : manageAdminService.getManageAdminContentDetail(userId, userTypeid)) {
                    manageDetailList.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8], "", (Integer) obj[9]));
                }
            } else if (userTypeid == 21) {
                for (Object[] obj : manageAdminService.getManageAdminContentDetail(userId, userTypeid)) {
                    manageDetailList.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8], "", (Integer) obj[9]));
                }
            }
        } catch (Exception e) {
            LOGGER.error("getManageDetail : " + logUserId + e);
        }
        LOGGER.debug("getManageDetail : " + logUserId + END);
        return manageDetailList;

    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        manageAdminService.setAuditTrail(auditTrail);
    }

    /**
     * updating data
     *
     * @param adminMasterDtBean
     * @return true if updated else false
     */
    public boolean organizationUpdate(AdminMasterDtBean adminMasterDtBean, String... actionAudit) {

        LOGGER.debug("organizationUpdate : " + logUserId + START);
        boolean flag = false;
        try {
            TblAdminMaster tblAdminMaster = new TblAdminMaster();
            tblAdminMaster.setAdminId(adminMasterDtBean.getAdminId());
            tblAdminMaster.setFullName(adminMasterDtBean.getFullName());
            tblAdminMaster.setMobileNo(adminMasterDtBean.getMobileNo());
            tblAdminMaster.setNationalId(adminMasterDtBean.getNationalId());
            tblAdminMaster.setPhoneNo(adminMasterDtBean.getPhoneNo());
            tblAdminMaster.setRollId(adminMasterDtBean.getRollId());
            tblAdminMaster.setTblLoginMaster(new TblLoginMaster(adminMasterDtBean.getUserId()));

            if (manageAdminService.updateAdminMaster(tblAdminMaster, actionAudit)) {
                flag = true;
            }
        } catch (Exception e) {
            LOGGER.error("organizationUpdate : " + logUserId + e);
        }
        LOGGER.debug("organizationUpdate : " + logUserId + END);
        return flag;
    }

    /**
     * get office list
     *
     * @param userId
     * @param userTypeid
     * @return List of offices
     */
    public List<Object> getPEManageDetailOfficeList(byte userTypeid, int userId) {
        LOGGER.debug("getPEManageDetailOfficeList : " + logUserId + START);
        List<Object> obj = null;
        try {
            obj = manageAdminService.getManagePEAdminDetailOfficeList(userId, userTypeid);
        } catch (Exception e) {
            LOGGER.error("getPEManageDetailOfficeList : " + logUserId + e);
        }
        LOGGER.debug("getPEManageDetailOfficeList : " + logUserId + END);
        return obj;
    }

    /**
     * get details for PE admin
     *
     * @param userTypeid
     * @param userId
     * @return object with data
     */
    public List<ManageAdminDtBean> getManagePEDetail(byte userTypeid, int userId) {
        LOGGER.debug("getManagePEDetail : " + logUserId + START);
        List<ManageAdminDtBean> managePEDetailList = new ArrayList<ManageAdminDtBean>();
        try {
            for (Object[] obj : manageAdminService.getManagePEAdminDetail(userId, userTypeid)) {
                managePEDetailList.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (Short) obj[3], (String) obj[4], (String) obj[5], (String) obj[6], (Integer) obj[7], (String) obj[8], (Short) obj[9], (String) obj[10]));
            }
        } catch (Exception e) {
            LOGGER.error("getManagePEDetail : " + logUserId + e);
        }
        LOGGER.debug("getManagePEDetail : " + logUserId + END);
        return managePEDetailList;
    }

    /**
     * updating PE Admin
     *
     * @param adminMasterDtBean
     * @param str
     * @return true if updated else false
     */
    public boolean peUpdate(AdminMasterDtBean adminMasterDtBean, String[] str, String... actionAudit) {

        LOGGER.debug("peUpdate : " + logUserId + START);
        boolean flag = false;
        String query = "";
        try {
//            manageAdminService.updateDeleteNewQuery("delete from TblOfficeAdmin where tblLoginMaster.userId = " + adminMasterDtBean.getUserId() + " and adminType like 'PEAdmin'");
//            TblOfficeAdmin tblOfficeAdmin = new TblOfficeAdmin();
            for (int i = 0; i < str.length; i++) {
//                tblOfficeAdmin.setAdminType("PEAdmin");
                //tblOfficeAdmin.setOfficeAdminId();
//                tblOfficeAdmin.setTblLoginMaster(new TblLoginMaster(adminMasterDtBean.getUserId()));
//                tblOfficeAdmin.setTblOfficeMaster(new TblOfficeMaster(Integer.parseInt(str[i])));
//                if (manageAdminService.updateOfficeList(tblOfficeAdmin)) {
//                    flag = true;
//                }
                query = query + " UPDATE [dbo].[tbl_OfficeAdmin]"
                        + " SET [officeId] = " + Integer.parseInt(str[i])
                        + " WHERE [userId]=" + adminMasterDtBean.getUserId();
            }

//            TblAdminMaster tblAdminMaster = new TblAdminMaster();
//            tblAdminMaster.setAdminId(adminMasterDtBean.getAdminId());
//            tblAdminMaster.setFullName(adminMasterDtBean.getFullName());
//            tblAdminMaster.setMobileNo(adminMasterDtBean.getMobileNo());
//            tblAdminMaster.setPhoneNo(adminMasterDtBean.getPhoneNo());
//            tblAdminMaster.setNationalId(adminMasterDtBean.getNationalId());
//            tblAdminMaster.setTblLoginMaster(new TblLoginMaster(adminMasterDtBean.getUserId()));
            query = query + " UPDATE [dbo].[tbl_AdminMaster] "
                    + "SET [fullName] = '" + adminMasterDtBean.getFullName() + "',[nationalId] = '" + adminMasterDtBean.getNationalId()
                    + "',[mobileNo] = '" + adminMasterDtBean.getMobileNo() + "',[phoneNo] = '" + adminMasterDtBean.getPhoneNo() + "'"
                    + " WHERE userId=" + adminMasterDtBean.getUserId();
            if (manageAdminService.updatePEAdminMaster(query, adminMasterDtBean.getUserId(), actionAudit)) {
                flag = true;
            } else {
                flag = false;
            }
        } catch (Exception ex) {
            LOGGER.error("peUpdate : " + logUserId + ex);
        }
        LOGGER.debug("peUpdate : " + logUserId + END);
        return flag;

    }
    //Grid
    private int offset = 0;
    private int limit = 0;
    private String colName = "";
    private String op_ENUM = "";
    private String condition = "";
    private boolean _search = false;
    private String getSortOrder = "";
    private String sortCol = "";

    public String getSortOrder() {
        return getSortOrder;
    }

    public void setSortOrder(String getSortOrder) {
        this.getSortOrder = getSortOrder;
    }

    public String getSortCol() {
        return sortCol;
    }

    public void setSortCol(String sortCol) {
        this.sortCol = sortCol;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public boolean isSearch() {
        return _search;
    }

    public void setSearch(boolean _search) {
        this._search = _search;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getOp_ENUM() {
        return op_ENUM;
    }

    public void setOp_ENUM(String op_ENUM) {
        this.op_ENUM = op_ENUM;
    }

    /**
     * fetching data
     *
     * @param userTypeId
     * @param userId
     * @param searchString
     * @return Object with data
     */
    public List<ManageAdminDtBean> getOrganizationGrid(byte userTypeId, int userId, String searchString) {

        LOGGER.debug("getOrganizationGrid : " + logUserId + START);
        List<ManageAdminDtBean> OrganizationGrid = new ArrayList<ManageAdminDtBean>();
        try {
            if (OrganizationGrid.isEmpty()) {
                for (Object[] obj : manageAdminService.getOrganizationAdminGrid(getOffset(), getLimit(), userTypeId, userId, searchString)) {
                    // code added by palash, dohatec
                    OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (Short) obj[4]));
                    // End
                }
            }
        } catch (Exception e) {
            LOGGER.error("getOrganizationGrid : " + logUserId + e);
        }
        LOGGER.debug("getOrganizationGrid : " + logUserId + END);
        return OrganizationGrid;
    }

    /**
     * fetching data from userid
     *
     * @param userTypeId
     * @param userId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return Object with data
     */
    public List<ManageAdminDtBean> getOrganizationGrid(byte userTypeId, int userId, String searchField, String searchString, String searchOper) {
        LOGGER.debug("getOrganizationGrid : " + logUserId + START);
        List<ManageAdminDtBean> OrganizationGrid = new ArrayList<ManageAdminDtBean>();
        try {
            if (OrganizationGrid.isEmpty()) {
                for (Object[] obj : manageAdminService.getOrganizationAdminGrid(getOffset(), getLimit(), userTypeId, userId, searchField, searchString, searchOper)) {
                    OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3]));
                }
            }
        } catch (Exception e) {
            LOGGER.error("getOrganizationGrid : " + logUserId + e);
        }
        LOGGER.debug("getOrganizationGrid : " + logUserId + END);
        return OrganizationGrid;
    }

    /**
     * for counting records for Org
     *
     * @param userTypeid
     * @return number of records
     */
    public long getAllCountOfOrganization(byte userTypeid, int userId) throws Exception {
        LOGGER.debug("getAllCountOfOrganization : " + logUserId + START);
        long l = 0;
        try {
            l = manageAdminService.getAllCountOfOrganization(userTypeid);
        } catch (Exception e) {
            LOGGER.error("getAllCountOfOrganization : " + logUserId + e);
        }
        LOGGER.debug("getAllCountOfOrganization : " + logUserId + END);
        return l;
    }

    /**
     * count records
     *
     * @param userId
     * @param userTypeId
     * @return number of data
     * @throws Exception
     */
    public long getOrganizationAdminGridCount(String userId, String userTypeid) throws Exception {
        LOGGER.debug("getOrganizationAdminGridCount : " + logUserId + START);
        long l = 0;
        try {
            l = manageAdminService.getOrganizationAdminGridCount(userId, userTypeid);
        } catch (Exception e) {
            LOGGER.error("getOrganizationAdminGridCount : " + logUserId + e);
        }
        LOGGER.debug("getOrganizationAdminGridCount : " + logUserId + END);
        return l;
    }

    /**
     * count records
     *
     * @param userId
     * @param userTypeId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of data
     * @throws Exception
     */
    public long getOrganizationAdminGridCount(String userId, String userTypeid, String searchField, String searchString, String searchOper) throws Exception {
        LOGGER.debug("getOrganizationAdminGridCount : " + logUserId + START);
        long l = 0;
        try {
            l = manageAdminService.getOrganizationAdminGridCount(userId, userTypeid);
        } catch (Exception e) {
            LOGGER.error("getOrganizationAdminGridCount : " + logUserId + e);
        }
        LOGGER.debug("getOrganizationAdminGridCount : " + logUserId + END);
        return l;
    }

    /**
     * fetching data for Organization Admin if usertypeid = 5
     *
     * @param userTypeId
     * @param orderClause
     * @return Object with data
     * @throw Exception
     */
    public List<ManageAdminDtBean> getOrganizationGrid(byte userTypeId, String orderClause) {

        LOGGER.debug("getOrganizationGrid : " + logUserId + START);
        List<ManageAdminDtBean> OrganizationGrid = new ArrayList<ManageAdminDtBean>();

        try {
            if (OrganizationGrid.isEmpty()) {
                if (userTypeId == 5) {
                    // if (getSortOrder().equalsIgnoreCase("asc")) {
                    for (Object[] obj : manageAdminService.getOrganizationAdminGrid(getOffset(), getLimit(), userTypeId, orderClause)) {
                        OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (String) obj[7], (Short) obj[8], (String) obj[9], (String) obj[10]));
                    }
                    //}
                } else if (userTypeId == 8) {
                    for (Object[] obj : manageAdminService.getContentAdminGrid(getOffset(), getLimit(), userTypeId, orderClause)) {
                        OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8], (String) obj[9]));
                    }
                } else if (userTypeId == 4) {
                    for (Object[] obj : manageAdminService.getPEAdminGrid(getOffset(), getLimit(), userTypeId, orderClause)) {
                        OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (Short) obj[3], (String) obj[4], (String) obj[5], (String) obj[6], (String) obj[7], (String) obj[8]));
                    }
                } else if (userTypeId == 12) {//needs to be chk
                    for (Object[] obj : manageAdminService.getContentAdminGrid(getOffset(), getLimit(), userTypeId, orderClause)) {
                        OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8], (String) obj[9]));
                    }
                } else if (userTypeId == 19 || userTypeId == 21) {//needs to be chk
                    for (Object[] obj : manageAdminService.getContentAdminGrid(getOffset(), getLimit(), userTypeId, orderClause)) {
                        OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8], (String) obj[9]));
                    }
                } else if (userTypeId == 20) {//needs to be chk
                    for (Object[] obj : manageAdminService.getOAndMAdminGrid(getOffset(), getLimit(), userTypeId, orderClause)) {
                        if (obj[10] != null) {
                            OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8], (String) obj[9], (Integer) obj[10]));
                        } else {
                            OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8], (String) obj[9], 0));
                        }

                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in o&m admin" + e);
            e.printStackTrace();
            LOGGER.error("getOrganizationGrid : " + logUserId + e);
        }

        LOGGER.debug("getOrganizationGrid : " + logUserId + END);
        return OrganizationGrid;
    }

    /**
     * fetching data for Organization Admin if usertypeid = 5
     *
     * @param userTypeId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return Object with data
     * @throw Exception
     */
    public List<ManageAdminDtBean> getOrganizationGrid(byte userTypeId, String searchField, String searchString, String searchOper) {

        LOGGER.debug("getOrganizationGrid : " + logUserId + START);
        List<ManageAdminDtBean> OrganizationGrid = new ArrayList<ManageAdminDtBean>();

        try {
            if (OrganizationGrid.isEmpty()) {
                if (userTypeId == 5) {
                    if (getSortOrder().equalsIgnoreCase("asc")) {
                        for (Object[] obj : manageAdminService.getOrganizationAdminGrid(getOffset(), getLimit(), userTypeId, searchField, searchString, searchOper)) {
                            OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (String) obj[7], (Short) obj[8], (String) obj[9]));
                        }
                    }
                } else if (userTypeId == 8) {
                    for (Object[] obj : manageAdminService.getContentAdminGrid(getOffset(), getLimit(), userTypeId, searchField, searchString, searchOper)) {
                        OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8]));
                    }
                } else if (userTypeId == 4) {
                    for (Object[] obj : manageAdminService.getPEAdminGrid(getOffset(), getLimit(), userTypeId, searchField, searchString, searchOper)) {
                        OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (Short) obj[3], (String) obj[4], (String) obj[5], (String) obj[6], (String) obj[7]));
                    }
                } else if (userTypeId == 12) {
                    for (Object[] obj : manageAdminService.getContentAdminGrid(getOffset(), getLimit(), userTypeId, searchField, searchString, searchOper)) {
                        OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8]));
                    }
                } else if (userTypeId == 19 || userTypeId == 21) {
                    for (Object[] obj : manageAdminService.getContentAdminGrid(getOffset(), getLimit(), userTypeId, searchField, searchString, searchOper)) {
                        OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8]));
                    }
                } else if (userTypeId == 20) {
                    for (Object[] obj : manageAdminService.getContentAdminGrid(getOffset(), getLimit(), userTypeId, searchField, searchString, searchOper)) {
                        OrganizationGrid.add(new ManageAdminDtBean((Integer) obj[0], (String) obj[1], (String) obj[2], (String) obj[3], (String) obj[4], (Byte) obj[5], (String) obj[6], (Short) obj[7], (String) obj[8]));
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("getOrganizationGrid : " + logUserId + e);
        }

        LOGGER.debug("getOrganizationGrid : " + logUserId + END);
        return OrganizationGrid;
    }

    /**
     * for counting records for Org if userTypeid = 5
     *
     * @param userTypeId
     * @return number of records
     * @throw Exception
     */
    public long getAllCountOfOrganization(byte userTypeid) throws Exception {

        LOGGER.debug("getAllCountOfOrganization : " + logUserId + START);
        long result = 0;
        try {
            if (userTypeid == 5) {
                result = manageAdminService.getAllCountOfOrganization(userTypeid);
            } else if (userTypeid == 8) {
                result = manageAdminService.getAllCountOfContent(userTypeid);
            } else if (userTypeid == 4) {
                result = manageAdminService.getAllCountOfPE(userTypeid);
            } else if (userTypeid == 12) {
                result = manageAdminService.getAllCountOfContent(userTypeid);
            } else if (userTypeid == 20) {
                result = manageAdminService.getAllCountOfContent(userTypeid);
            } else {
                result = 0;
            }
        } catch (Exception e) {
            LOGGER.error("getAllCountOfOrganization : " + logUserId + e);
        }
        LOGGER.debug("getAllCountOfOrganization : " + logUserId + END);
        return result;
    }

    /**
     * for counting records for Org if userTypeid = 5
     *
     * @param userTypeId
     * @param searchField
     * @param searchString
     * @param searchOper
     * @return number of records
     * @throw Exception
     */
    public long getSearchCountOfOrganization(byte userTypeid, String searchField, String searchString, String searchOper) throws Exception {

        LOGGER.debug("getSearchCountOfOrganization : " + logUserId + START);
        long result = 0;

        try {
            if (userTypeid == 5) {
                result = manageAdminService.getSearchCountOfOrganization(userTypeid, searchField, searchString, searchOper);
            } else if (userTypeid == 8) {
                result = manageAdminService.getSearchCountOfContent(userTypeid, searchField, searchString, searchOper);
            } else if (userTypeid == 4) {
                result = manageAdminService.getSearchCountOfPE(userTypeid, searchField, searchString, searchOper);
            } else if (userTypeid == 12) {
                result = manageAdminService.getSearchCountOfContent(userTypeid, searchField, searchString, searchOper);
            }
        } catch (Exception e) {
            LOGGER.error("getSearchCountOfOrganization : " + logUserId + e);
        }

        LOGGER.debug("getSearchCountOfOrganization : " + logUserId + END);
        return result;
    }
}
