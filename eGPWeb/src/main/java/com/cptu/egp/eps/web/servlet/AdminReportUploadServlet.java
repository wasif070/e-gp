/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.model.table.TblConfigurationMaster;
import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.CheckExtension;
import com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Administrator
 */
//@WebServlet(name = "PreTenderQueryDocServlet", urlPatterns = {"/PreTenderQueryDocServlet"})
public class AdminReportUploadServlet extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("AdminReportUploadServlet");
    private File destinationDir;

    /**
     *This class is useful to handles the request of document upload download and remove request
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int UserId = 0;       
        String docSizeMsg = "";
        boolean checkret = false;
        boolean flag = false;
        String documentBrief = "";
        String fileName = "";
        long fileSize = 0;
        String queryString = "";
        String pageName = "admin/IndicatorsDocsUpload.jsp";
        File file = null;
        String queryId = "0";
        boolean dis = false;
        String pageN = "";

int userId = Integer.parseInt(request.getSession().getAttribute("userId").toString());
 System.out.println(userId+" userId");
        response.setContentType("text/html");
        HttpSession session = request.getSession();
        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {
            try {
                try {
                    if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("remove")) {
                    
                        int docId = 0;
                        String id = "";
                        String docName = "";
                        if (request.getParameter("docId") != null) {
                            docId = Integer.parseInt(request.getParameter("docId"));

                        }
                        
                        if (request.getParameter("userId") != null) {
                            UserId = Integer.parseInt(request.getParameter("userId"));

                        }
                        if (request.getParameter("pageName") != null) {
                            pageName = request.getParameter("pageName");

                        }
                        if (request.getParameter("docName") != null) {
                            docName = request.getParameter("docName");
                        }if(request.getParameter("pageName")!=null){
                            pageN = request.getParameter("pageName");
                        }
                        String whereContition = "";
                        whereContition = "reportDocId= " + docId;
                        fileName = DESTINATION_DIR_PATH + "\\" + docName;
                        checkret = deleteFile(fileName);
                        if (checkret) {
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk;
                            commonMsgChk = commonXMLSPService.insertDataBySP("delete", "tbl_ReportDocs", "", whereContition).get(0);
                           if(request.getHeader("referer")!=null){                               
                              response.sendRedirect(request.getHeader("referer"));                            
                           }else{
                            queryString = "?&userId=" + userId+"&fq=Removed&page="+pageN;
                            response.sendRedirect(pageName+queryString);
                           }
                        } 

                    } else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("download")) {
                        file = new File(DESTINATION_DIR_PATH + "\\" + request.getParameter("docName"));//request.getSession().getAttribute("userId")
                        InputStream fis = new FileInputStream(file);
                        byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
                        int offset = 0;
                        int numRead = 0;
                        while ((offset < buf.length)
                                && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {

                            offset += numRead;

                        }
                        fis.close();
                        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                        buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
                        response.setContentType("application/octet-stream");
                        response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName") + "\"");
                        ServletOutputStream outputStream = response.getOutputStream();
                        outputStream.write(buf);
                        outputStream.flush();
                        outputStream.close();
                    }
                    else if ((request.getParameter("funName") != null) && request.getParameter("funName").equals("update")){
                     String xmldata = "";
                    int docId = 0;
                        String id = "";
                        String docName = "";

                        if (request.getParameter("docId") != null) {
                            docId = Integer.parseInt(request.getParameter("docId"));

                        }

                        if (request.getParameter("userId") != null) {
                            UserId = Integer.parseInt(request.getParameter("userId"));

                        }
                        if (request.getParameter("pageName") != null) {
                            pageName = request.getParameter("pageName");

                        }
                        if (request.getParameter("docName") != null) {
                            docName = request.getParameter("docName");
                        }if(request.getParameter("pageName")!=null){
                            pageN = request.getParameter("pageName");
                        }

                        if ((request.getParameter("status") != null) && request.getParameter("status").equals("yes")) {
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk;
                            commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_ReportDocs", "status='no'", "reportDocId="+docId).get(0);

                           //if(request.getHeader("referer")!=null){
                              // response.sendRedirect(request.getHeader("referer"));
                      //  }else{
                            queryString = "?&userId=" + userId+"&fq=Update&page="+pageN;
                            response.sendRedirect(pageName+queryString);
                          // }
                        }
                      else if((request.getParameter("status") != null) && request.getParameter("status").equals("no")) {

                           CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                           CommonMsgChk commonMsgChk;
                           commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_ReportDocs", "status='yes'", "reportDocId="+docId).get(0);
                          // if(request.getHeader("referer")!=null){
                          //     response.sendRedirect(request.getHeader("referer"));
                          //}else{

                            queryString = "?&userId=" + userId+"&fq=Update&page="+pageN;
                            response.sendRedirect(pageName+queryString);
                           //}
                        }

                    }

                    else {
                        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
                        /*
                         *Set the size threshold, above which content will be stored on disk.
                         */
                        fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB
		/*
                         * Set the temporary directory to store the uploaded files of size above threshold.
                         */
                        fileItemFactory.setRepository(tmpDir);

                        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
                        /*
                         * Parse the request
                         */
                        List items = uploadHandler.parseRequest(request);
                        Iterator itr = items.iterator();
                        //For Supporting Document
                        while (itr.hasNext()) {
                            FileItem item = (FileItem) itr.next();
                            //For Supporting Document
                    /*
                             * Handle Form Fields.
                             */
                            if (item.isFormField()) {
                                if (item.getFieldName().equals("documentBrief")) {
                                    if (item.getString() == null || item.getString().trim().length() == 0) {
                                        dis = true;
                                        break;
                                    }
                                    documentBrief = item.getString();
                                } else if (item.getFieldName().equals("userId")) {
                                    UserId = Integer.parseInt(item.getString());
                                }else if (item.getFieldName().equals("pageName")) {
                                    pageN = item.getString();
                                }
                                
                            } else {
                                

                                if (item.getName().lastIndexOf("\\") != -1) {
                                    fileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1, item.getName().length());
                                } else {
                                    fileName = item.getName();
                                }
                                //fileName  = fileName.replaceAll(" ", "");
                                String realPath = DESTINATION_DIR_PATH;//request.getSession().getAttribute("userId")
                              
                                destinationDir = new File(realPath);
                                if (!destinationDir.isDirectory()) {
                                    destinationDir.mkdir();
                                }
                                docSizeMsg = docSizeMsg(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                              //  docSizeMsg = docSizeMsg(1);
                                if (!docSizeMsg.equals("ok")) {
                                    //response.sendRedirect("SupportingDocuments.jsp?fq="+docSizeMsg);
                                } else {
                                    fileSize = item.getSize();
                                    checkret = checkExnAndSize(fileName, item.getSize(), "egpadmin");
                                    if (!checkret) {
                                   

                                        break;
                                    } else {
                                        file = new File(destinationDir, fileName);
                                        if (file.isFile()) {
                                            flag = true;
                                
                                            break;

                                        }
                                        item.write(file);
                                        FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
                                        fileSize = fileEncryptDecryptUtil.fileEncryptUtil(file, (int)fileSize);
                                    }
                                }
                            }

                        }
                    }

                } catch (FileUploadException ex) {
                    log("Error encountered while parsing the request", ex);
                } catch (Exception ex) {
                    log("Error encountered while uploading file", ex);
                }

                
                if (!docSizeMsg.equals("ok")) {
                    queryString = "?fq=" + docSizeMsg+"&UserId="+UserId+"&page="+pageN;
              //     response.sendRedirect(pageName+queryString);
                } else {
                    if (dis) {
                        docSizeMsg = "Document Discription is Mandatory";
                        queryString = "?fq=" + docSizeMsg + "&UserId=" + UserId+"&page="+pageN;
                        response.sendRedirect(pageName + queryString);
                    } else {
                        if (flag) {
                            docSizeMsg = "File already Exists";
                            queryString = "?fq=" + docSizeMsg + "&UserId=" + UserId+"&page="+pageN;
                            response.sendRedirect(pageName + queryString);
                        } else {
                            if (!checkret) {
                                CheckExtension ext = new CheckExtension();
                                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("egpadmin");
                                queryString = "?fs=" + configurationMaster.getFileSize() + "&ft=" + configurationMaster.getAllowedExtension()+"&UserId="+UserId+"&page="+pageN;
                                response.sendRedirect(pageName + queryString);
                                

                            } else {
                                /**Modified By dohatec for document upload checking start **/
                                file = new File(destinationDir, fileName);
                                if (file.isFile()) {
                                if (documentBrief != null && !documentBrief.equals("")) {
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    //String userId = "1";
                                    String status="yes";
                                    String xmldata = "";
                                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                                    documentBrief = handleSpecialChar.handleSpecialChar(documentBrief);
                                    xmldata = "<tbl_ReportDocs documentName=\"" + fileName + "\" docDescription=\"" + documentBrief + "\" docSize=\"" + fileSize + "\" uploadedDate=\"" + format.format(new Date()) + "\" userId=\"" + session.getAttribute("userId") + "\" status=\"" + status + "\" />";
                                    xmldata = "<root>" + xmldata + "</root>";
                                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    CommonMsgChk commonMsgChk;
                                    try {
                                        commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_ReportDocs", xmldata, "").get(0);
                                        queryString = "?UserId=" + userId+"&fq=Uploaded";
                                        response.sendRedirect(pageName + queryString);
                                     
                                        
                                        if(commonMsgChk!=null){
                                            commonMsgChk = null;
                                           // handleSpecialChar = null;
                                        }

                                    } catch (Exception ex) {
                                        Logger.getLogger(TenderDetailsServlet.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                  }
                                }
                                else{
                                    docSizeMsg = "Error occurred during File Upload";
                                    queryString = "?fq=" + docSizeMsg + "&UserId=" + userId+"&page="+pageN;
                                    response.sendRedirect(pageName + queryString);
                                }
                                /**Modified By dohatec for document upload checking End **/
                            }
                        }
                    }
                }
            } finally {
                //      out.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     *Check Extention
     * @param extn Extension of file
     * @param size size of file
     * @param userType session UserType
     * @return boolean
     */
    public boolean checkExnAndSize(String extn, long size, String userType) {
        boolean chextn = false;
        float fsize = 0.0f;
        float dsize = 0.0f;
        int j = extn.lastIndexOf('.');
        String lst = extn.substring(j + 1);
       
        CheckExtension ext = new CheckExtension();
        TblConfigurationMaster configurationMaster = ext.getConfigurationMaster(userType);
        String str = configurationMaster.getAllowedExtension();
        String[] str1 = str.split(",");
        for (int i = 0; i < str1.length; i++) {
            if (str1[i].trim().equalsIgnoreCase(lst)) {
                chextn = true;
            }
        }
        if (chextn) {
            fsize = size / (1024 * 1024);
       
            dsize = configurationMaster.getFileSize();
       
            if (dsize > fsize) {
                chextn = true;
            } else {
                chextn = false;
            }
        }
        return chextn;
    }

    /**
     * For DocumentSize Message
     * @param userId sessionUserId
     * @return Message
     */
    public String docSizeMsg(int userId) {
        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
        return userRegisterService.docSizeCheck(userId);
    }

    /**
     * For Deleting file
     * @param filePath
     * @return check file is deleted or not
     */
    public boolean deleteFile(String filePath) {

        File f = new File(filePath);
        if (f.delete()) {
            return true;
        } else {
            return false;
        }

    }
}
