/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.xmlpojos;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
/**
 * This class is used in web-service.
 * Every web-service returns this object only.
 * This object have mainly Three variables.
 * They are <b>responseMessage</b>, <b>responseConstant</b> and <b>responseList</b>
 * responseMessage has the web-service response message.
 * responseConstant has the web-service response constant value.
 * responseList has the web-service response List value.
 * @author Sreenu
 */
//@XmlRootElement
public class WSResponseObject {
    
    //Web Service Response codes
    public static final int SUCCESS = 0;
    public static final int NO_RECORDS_FOUND = 100;
    public static final int SEARCH_CRITRIA_EXPECT_NUMBER = 101;
    public static final int SEARCH_CRITRIA_EXPECT_STRING = 102;
    public static final int EXCEPTION = -1;
    public static final int CONNECTION_ERROR = -100;
    public static final int AUTHENTICATION_FAIL_EXCEPTION = -101;
    public static final int NO_RIGHTS_TO_ACCESS_WEB_SERVICE = -102;
    public static final int WEB_SERVICE_INACTIVE = -103;

    //Web Service Response Messages
    public static final String SUCCESS_MSG = "Success";
    public static final String NO_RECORDS_FOUND_MSG = "No Records Found";
    public static final String SEARCH_CRITERIA_EXPECT_NUMBER_MSG = "Field expect number in place of text";
    public static final String SEARCH_CRITERIA_EXPECT_STRING_MSG = "Field expect text in place of number";
    public static final String EXCEPTION_MSG = "Exception: "; // append the actual exception messsage in response message
    public static final String CONNECTION_ERROR_MSG = "Connection Error!";
    public static final String AUTHENTICATION_FAIL_ERROR_MSG = "Invalid e-mail id or password";
    public static final String ACCESS_RIGHTS_FAIL = "No rights to access  this web serivce";
    public static final String WEB_SERVICE_IS_INACTIVE ="WebService you are access is inactive";
    public static final String APPEND_CHAR = "^";
    public static final String LOGGER_START = " Starts";
    public static final String LOGGER_END = " Ends";

    //Web Service Response message
    private String responseMessage;
    //Web Service Response IntegerValue
    private int responseConstant;
    //Web Service Response List
    private List<String> responseList;
    //some times we dont need to fetch list of data while
    //authencation fails or acess rights fails etc. Then we use this flag. 
    private boolean isListRequired;

    /***
     *
     * @return String message
     */
    @XmlElement(nillable = true)
    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /***
     * it returns response constant
     * for SUCCESS it returns  0;
     * for NO_RECORDS_FOUND it returns 100
     * for SEARCH_CRITRIA_EXPECT_NUMBER it returns 101
     * for SEARCH_CRITRIA_EXPECT_STRING it returns 102
     * for EXCEPTION it returns -1
     * for CONNECTION_ERROR it returns -100
     * for AUTHENTICATION_FAIL_EXCEPTION it returns -101;
     * @return int ResponseConstant
     */
    @XmlElement(nillable = true)
    public int getResponseConstant() {
        return responseConstant;
    }

    /***
     * set the value for response constant
     * for SUCCESS = 0;
     * for NO_RECORDS_FOUND = 100
     * for SEARCH_CRITRIA_EXPECT_NUMBER = 101
     * for SEARCH_CRITRIA_EXPECT_STRING = 102
     * for EXCEPTION = -1
     * for CONNECTION_ERROR = -100
     * for AUTHENTICATION_FAIL_EXCEPTION = -101;
     */
    public void setResponseConstant(int result) {
        this.responseConstant = result;
    }

    /***
     * it returns the search values
     * @return Collection values
     */
    @XmlElement(nillable = true)
    public List<String> getResponseList() {
        return responseList;
    }

    /***
     * 
     * @param responseList
     */
    public void setResponseList(List<String> responseList) {
        this.responseList = responseList;
    }

    /***
     * 
     * @return
     */
    @XmlElement(nillable = true)
    public boolean getIsListRequired() {
        return isListRequired;
    }

    /***
     * 
     * @param isListRequired
     */
    public void setIsListRequired(boolean isListRequired) {
        this.isListRequired = isListRequired;
    }
    
}
