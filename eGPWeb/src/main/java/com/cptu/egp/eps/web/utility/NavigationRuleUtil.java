/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.utility;

import com.cptu.egp.eps.model.table.TblConfigEvalMethod;
import com.cptu.egp.eps.service.serviceimpl.EvalMethodConfigService;
import com.cptu.egp.eps.service.serviceimpl.ConfigClarificationService;
import com.cptu.egp.eps.service.serviceimpl.TenPaymentConfigService;
import com.cptu.egp.eps.service.serviceinterface.APPService;
import com.cptu.egp.eps.service.serviceinterface.ConfigPreTenderRuleService;
import com.cptu.egp.eps.service.serviceinterface.FinancialDelegationService;
import com.cptu.egp.eps.service.serviceinterface.ProcurementTimelineService;
import com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * <b>NavigationRuleUtil</b> <Description Goes Here> Dec 2, 2010 4:22:53 PM
 * @author Administrator
 */
public class NavigationRuleUtil {
    private static final Logger LOGGER = Logger.getLogger(NavigationRuleUtil.class);
    ConfigPreTenderRuleService configPreTenderRuleSr=(ConfigPreTenderRuleService)AppContext.getSpringBean("configPreTenderRuleService");


    public long getPreTenderCnt()
    {
        try
        {
            return configPreTenderRuleSr.getPreTenderCnt();
        }
        catch(Exception ex)
        {
            LOGGER.error("Exception Generated: "+ex.getMessage());
            return -1;
        }
    }

    public long getSTDCnt()
    {
        try
        {
            return configPreTenderRuleSr.getConfigSTDCnt();
        }
        catch(Exception ex)
        {
            LOGGER.error("Exception Generated: "+ex.getMessage());
            return -1;
        }
    }

    public long getAmendmentCnt()
    {

        try
        {
            return configPreTenderRuleSr.getConfigAmendmentCnt();
        }
        catch(Exception ex)
        {
            LOGGER.error("Exception Generated: "+ex.getMessage());
            return -1;
        }
    }

    public long getTECCnt(String type1,String type2)
    {
        try
        {
            return configPreTenderRuleSr.getConfigTECCnt(type1,type2);
        }
        catch(Exception ex)
        {
            LOGGER.error("Exception Generated: "+ex.getMessage());
            return -1;
        }
    }

    public long getProcCnt()
    {
        try
        {
            return configPreTenderRuleSr.getConfigProcurementCnt();
        }
        catch(Exception ex)
        {
            LOGGER.error("Exception Generated: "+ex.getMessage());
            return -1;
        }
    }

    public boolean isAdmin(int userId)
    {
        ScBankDevpartnerService scBankDevpartnerService=(ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
        try
        {
            return scBankDevpartnerService.isAdmin(userId);
        }
        catch(Exception ex)
        {
            LOGGER.error("Admin Check Exception: "+ex.getMessage());
            return true;
        }
    }

    public boolean isPEorAU(int userId)
    {
        APPService appService=(APPService) AppContext.getSpringBean("APPService");
        try
        {
            return appService.isPEorAU(userId);
        }
        catch(Exception ex)
        {
            LOGGER.error("isPEorAU Exception: "+ex.getMessage());
            return true;
        }
    }

    public long getFinPowCnt()
    {
        try
        {
            return configPreTenderRuleSr.getFinancialPowerByRoleCnt();
        }
        catch(Exception ex)
        {
            LOGGER.error("Exception Generated: "+ex.getMessage());
            return -1;
        }
    }

    public long getNoaCnt(){
        try
        {
            return configPreTenderRuleSr.getConfigNoaCount();
        }
        catch(Exception ex)
        {
            LOGGER.error("Exception Generated: "+ex.getMessage());
            return -1;
        }

    }

    public long getEvalCnt() throws Exception{
        EvalMethodConfigService configService = (EvalMethodConfigService) AppContext.getSpringBean("EvalMethodConfigService");        
        return configService.getAllEvalConfigCount();
    }
    
    public long getClarificationCnt() throws Exception{
        ConfigClarificationService configService = (ConfigClarificationService) AppContext.getSpringBean("ConfigClarificationService");        
        return configService.getAllConfigClarificationCount();
    }

    public long getPayCnt() throws Exception{
        TenPaymentConfigService configService = (TenPaymentConfigService) AppContext.getSpringBean("TenPaymentConfigService");
        return configService.getAllTenPayementConfigCount();
    }

    //  Istiak (Dohatec) - 01.Jun.15
    public int getRuleCount() throws Exception{
        ProcurementTimelineService procTimeline = (ProcurementTimelineService) AppContext.getSpringBean("ProcurementTimelineService");
        return procTimeline.getAllRuleCount();
    }

    public int getFinancialDelegationCount() throws Exception{
        FinancialDelegationService finDelegation = (FinancialDelegationService) AppContext.getSpringBean("FinancialDelegationService");
        return finDelegation.getFinancialDelegationCount();
    }

    public int getDelegationRankCnt() throws Exception{
        FinancialDelegationService delRank = (FinancialDelegationService) AppContext.getSpringBean("FinancialDelegationService");
        return delRank.getDelegationRankCnt();
    }
    //  Istiak End
}
