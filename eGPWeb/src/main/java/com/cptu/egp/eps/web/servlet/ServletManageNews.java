/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import org.apache.log4j.Logger;
/**
 *
 * @author Administrator
 */
public class ServletManageNews extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private static final Logger LOGGER = Logger.getLogger(ServletManageNews.class);
    private String logUserId = "0";
    private final CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        if (request.getSession().getAttribute("userId") != null){
                logUserId = request.getSession().getAttribute("userId").toString();
                commonXMLSPService.setLogUserId(logUserId);
         }

        LOGGER.debug("processRequest : "+logUserId+" Starts");
        String pageName = "admin/NewsManagement.jsp";
        String queryString="";
        String newsType=request.getParameter("newsType");
        String newsId="";
        String action="";

        response.setContentType("text/html;charset=UTF-8");
        //PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        if (session.getAttribute("userId") == null) {
            response.sendRedirect("SessionTimedOut.jsp");
        } else {
             try {
                /* TODO output your page here*/
                  
                  CommonMsgChk commonMsgChk = new CommonMsgChk();

                if(request.getParameter("nId") != null){
                    newsId=request.getParameter("nId");
                }

                 if(request.getParameter("action") != null){
                    action=request.getParameter("action");
                }

                if("archive".equalsIgnoreCase(action)){
                    LOGGER.debug("processRequest : archive : action : "+logUserId+" Starts");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                    String strAction = "Archive News";
                    try{
                        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_NewsMaster", "isArchive='yes'", "newsId=" + newsId).get(0);

                        if (commonMsgChk.getFlag().equals(true)) {
                            queryString = "newsType=" + newsType + "&msgId=archived";
                            response.sendRedirect(pageName + "?" + queryString);
                        } else {
                            queryString = "newsType=" + newsType + "&msgId=error";
                            response.sendRedirect(pageName + "?" + queryString);
                        }
                    }
                    catch (Exception e){
                        LOGGER.error("Exception: " + e);
                        strAction = "Error in "+strAction+" "+e.getMessage();
                    }finally{
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), strAction, "");
                        strAction = null;
                    }
                    LOGGER.debug("processRequest : archive : action : "+logUserId+" Ends");
                    
                } else if("delete".equalsIgnoreCase(action)){
                    LOGGER.debug("processRequest : delete : action : "+logUserId+" Starts");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                    String strAction = "Delete News";
                    try{
                        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_NewsMaster", "isDeleted='yes'", "newsId=" + newsId).get(0);

                        if (commonMsgChk.getFlag().equals(true)) {
                            queryString = "newsType=" + newsType + "&msgId=deleted";
                            response.sendRedirect(pageName + "?" + queryString);
                        } else {
                            queryString = "newsType=" + newsType + "&msgId=error";
                            response.sendRedirect(pageName + "?" + queryString);
                        }
                    }
                    catch (Exception e){
                        LOGGER.error("Exception: " + e);
                        strAction = "Error in "+strAction+" "+e.getMessage();
                    }finally{
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), Integer.parseInt(logUserId), "userId", EgpModule.Content.getName(), strAction, "");
                        strAction = null;
                    }
                    LOGGER.debug("processRequest : delete : action : "+logUserId+" Ends");
                }
                 /**/
            } finally {
                LOGGER.error("finally : Exception");
                //out.close();
            }
        LOGGER.debug("processRequest : "+logUserId+" Ends");
        }

      
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
