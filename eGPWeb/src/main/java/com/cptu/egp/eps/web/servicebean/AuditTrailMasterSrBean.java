/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblAuditTrailMaster;
import com.cptu.egp.eps.service.serviceimpl.AuditTrailMasterServiceImpl;
import com.cptu.egp.eps.web.utility.AppContext;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class AuditTrailMasterSrBean {

    private AuditTrailMasterServiceImpl auditTrailMasterServiceImpl = (AuditTrailMasterServiceImpl) AppContext.getSpringBean("AuditTrailMasterServiceImpl");
    private static final Logger LOGGER = Logger.getLogger(AskProcurementSrBean.class);
    private String loggerStart = " Starts";
    private String loggerEnd = " Ends";
    private String logUserId = "0";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        auditTrailMasterServiceImpl.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }

    /**
     * Add tblAuditTrailMaster Data
     * @param tblAuditTrailMaster
     */
    public void addData(TblAuditTrailMaster tblAuditTrailMaster) {
        LOGGER.debug("addData : " + logUserId + loggerStart);
        try {
            auditTrailMasterServiceImpl.addDate(tblAuditTrailMaster);
        } catch (Exception ex) {
            LOGGER.error("addData : " + logUserId,ex);
        }
        LOGGER.debug("addData : " + logUserId + loggerEnd);
    }
}
