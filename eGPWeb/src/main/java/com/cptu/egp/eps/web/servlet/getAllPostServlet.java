/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.service.serviceimpl.PublicForumPostService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

public class getAllPostServlet extends HttpServlet {

    private String logUserId = "0";
    final Logger logger = Logger.getLogger(getAllPostServlet.class);
    PublicForumPostService publicForumPostService = (PublicForumPostService) AppContext.getSpringBean("PublicForumPostService");

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        logger.debug("processRequest : " + logUserId + "starts");
        try {

            int counter = 0;
            String styleClass = "";
            int totalRecord = 0;
            List<SPCommonSearchData> colResult = null;
            colResult = searchAllPosts(request);//define the method to get all the data
            int totalpage = 0;
            //displaying the data
            if (colResult == null || (colResult != null && colResult.isEmpty())) {
                out.print("<tr>");
                out.print("<td colspan=\"6\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                out.print("</tr>");
            } else {

                for (SPCommonSearchData data : colResult) {
                    if (counter % 2 == 0) {
                        styleClass = "bgColor-white";
                    } else {
                        styleClass = "bgColor-Green";
                    }
                    out.print("<tr class='" + styleClass + "'>");
                    out.print("<td class=\"t-align-center\">" + data.getFieldName1() + "</td>");
                    out.print("<td class=\"t-align-center\">" + "<a href=viewTopic.jsp?ppfid=" + data.getFieldName2() + "&h=t&ViewType="+request.getParameter("viewType")+">" + data.getFieldName3() + "</a> </td>");
                    out.print("<td class=\"t-align-center\">" + data.getFieldName4() + "</td>");
                    out.print("<td class=\"t-align-center\">" + data.getFieldName5() + "</td>");
                    out.print("<td class=\"t-align-center\">" + data.getFieldName6() + "</td>");
                    out.print("<td class=\"t-align-center\">" + data.getFieldName9() + "</td>");
                    out.print("</tr>");
                    totalpage = Integer.parseInt(data.getFieldName7());
                    totalRecord = Integer.parseInt(data.getFieldName8());
                    counter++;
                }



            }
            if (totalpage == 0) {
                totalpage = 1;
            } else {
                if (totalRecord % 10 != 0) {
                    totalpage = totalpage + 1;
                }
            }
            out.print("<input type=\"hidden\" id=\"totalPages\" value=\"" + totalpage + "\">");
        } catch (Exception exp) {
            logger.error("processRequest : " + exp);
        }
        logger.debug("processRequest : " + logUserId + "ends");
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private List<SPCommonSearchData> searchAllPosts(HttpServletRequest request) {
        List<SPCommonSearchData> searchAllPosts = null;
        logger.debug("searchAllPosts : " + logUserId + "starts");
//getting the session to check who is currently login
        HttpSession session = request.getSession();
        Object objUsrTypeId = session.getAttribute("userTypeId");
        Object objUName = session.getAttribute("userName");
        Object user = session.getAttribute("userId");
        String userId = null;
        String uid = "";
        if (objUName == null) {
            userId = "";
        } else {

            userId = objUsrTypeId.toString();
        }
        if (user == null) {
            logUserId = "";
        } else {
            logUserId = user.toString();
            
        }
//getting the data from jsp

        String keyword = request.getParameter("keyword");
        String pageNo = request.getParameter("pageNo");
        String recordOffset = request.getParameter("size");
        String dop = request.getParameter("textfield5");
        if (dop == null) {
            dop = "";
        }
        String postedby = request.getParameter("select2");
        String viewType = request.getParameter("viewType");
        publicForumPostService.setUserId(logUserId);
//calling the procedure
        if (!"".equals(keyword) || !"".equals(dop) || !"".equals(postedby)) {
            searchAllPosts = publicForumPostService.getpost("search_Procurementforum", viewType, keyword, postedby, dop, logUserId, pageNo, recordOffset, "Accepted", "");
        } else if ("8".equals(userId)) {
            searchAllPosts = publicForumPostService.getpost("search_Procurementforum", "", "", "", "", logUserId, pageNo, recordOffset, "", "");
        } else {
            searchAllPosts = publicForumPostService.getpost("search_Procurementforum", viewType, "", "", "", logUserId, pageNo, recordOffset, "Accepted", "");

        }
        logger.debug("searchAllPosts : " + logUserId + " ends ");
        return searchAllPosts;
    }
}
