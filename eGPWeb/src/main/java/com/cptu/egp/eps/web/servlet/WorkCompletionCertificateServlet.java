/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsVendorRatingMaster;
import com.cptu.egp.eps.model.table.TblCmsWcCertiHistory;
import com.cptu.egp.eps.model.table.TblCmsWcCertificate;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CmsVendorReqWccService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.servicebean.CmsVendorRatingMasterServiceBean;
import com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Sreenu.Durga
 */
public class WorkCompletionCertificateServlet extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(WorkCompletionCertificateServlet.class);
    static final String STARTS = " Starts";
    static final String ENDS = " Ends";
    private int wcCertId;
    private TblCmsVendorRatingMaster tblCmsVendorRatingMaster;
    private String actualDateOfCompletion;
    Date actdtofcom = new Date();
    private String isWorkComplete;
    private String remarks;
    private Date createdDate;
    private int createdBy;
    private int contractId;
    private int userId;
    private String pgStatus;
    private String newPgRequire;
    private double newPgAmt;
    private String sendToTenderer;
    private static CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
    private static CmsVendorRatingMasterServiceBean vendorRatingMasterServiceBean = new CmsVendorRatingMasterServiceBean();
    private String rating;
    private int tenderId;
    private int lotId;
    private String contextPath;
    private int wccertId;
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private CmsVendorReqWccService cmsVendorReqWccService = (CmsVendorReqWccService) AppContext.getSpringBean("CmsVendorReqWccService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest : " + STARTS);
        HttpSession session = request.getSession();
        contextPath = request.getContextPath();
        String userId = "";

        if (session.getAttribute("userId") != null) {
            userId = session.getAttribute("userId").toString();
            cmsWcCertificateServiceBean.setLogUserId(session.getAttribute("userId").toString());
            vendorRatingMasterServiceBean.setLogUserId(session.getAttribute("userId").toString());
            registerService.setUserId(session.getAttribute("userId").toString());
            commonservice.setUserId(session.getAttribute("userId").toString());
            cmsConfigDateService.setLogUserId(session.getAttribute("userId").toString());
        }

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if (request.getParameter("lotId") != null) {
                lotId = Integer.parseInt(request.getParameter("lotId"));
            }
            if (request.getParameter("hidWcCertID") != null) {
                wccertId = Integer.parseInt(request.getParameter("hidWcCertID"));
            }
            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                createdBy = Integer.parseInt(session.getAttribute("userId").toString());
            }
            createdDate = Calendar.getInstance().getTime();

            if (request.getParameter("txtDateOfCompletion") != null) {
                actualDateOfCompletion = request.getParameter("txtDateOfCompletion");
            }
            actdtofcom = DateUtils.convertStringtoDate(actualDateOfCompletion, "dd/MM/yyyy");
            if (request.getParameter("radioWorkComplete") != null) {
                isWorkComplete = request.getParameter("radioWorkComplete");
            }

            if (request.getParameter("radioPerformanceGuarantee") != null) {
                pgStatus = request.getParameter("radioPerformanceGuarantee");
            }

            if (request.getParameter("radioPGRequire") != null) {
                newPgRequire = request.getParameter("radioPGRequire");
            }

            if (request.getParameter("txtPerformanceGuarantee") != null) {
                newPgAmt = Double.parseDouble(request.getParameter("txtPerformanceGuarantee"));
            }
            if (request.getParameter("radioRateVendor") != null) {
                rating = request.getParameter("radioRateVendor");
            }
            if (request.getParameter("selectSendToTenderer") != null) {
                sendToTenderer = request.getParameter("selectSendToTenderer");
            }
            if (request.getParameter("txtRemarks") != null) {
                remarks = request.getParameter("txtRemarks");
            }
            if (request.getParameter("tenderId") != null) {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }
            if (request.getParameter("contractSignId") != null) {
                contractId = Integer.parseInt(request.getParameter("contractSignId"));
            }

            TblCmsWcCertificate tblCmsWcCertificate = new TblCmsWcCertificate();

            //getDependentparameterValues(tenderId);
            tblCmsWcCertificate.setActualDateOfCompletion(actdtofcom);
            tblCmsWcCertificate.setContractId(contractId);
            tblCmsWcCertificate.setCreatedBy(createdBy);
            tblCmsWcCertificate.setCreatedDate(createdDate);
            tblCmsWcCertificate.setIsWorkComplete(isWorkComplete);
            tblCmsWcCertificate.setRemarks(remarks);
            tblCmsWcCertificate.setSendToTenderer("Y");

            tblCmsVendorRatingMaster = vendorRatingMasterServiceBean.getCmsVendorRatingMasterForRating(rating);
            tblCmsWcCertificate.setTblCmsVendorRatingMaster(tblCmsVendorRatingMaster);
            tblCmsWcCertificate.setUserId(Integer.parseInt(userId));

            TblCmsWcCertiHistory tblCmsWcCertiHistory = new TblCmsWcCertiHistory();
            tblCmsWcCertiHistory.setWcCertId(wccertId);
            tblCmsWcCertiHistory.setActualDateOfComplete(actdtofcom);
            tblCmsWcCertiHistory.setIsWorkComplete(isWorkComplete);
            tblCmsWcCertiHistory.setRemarks(remarks);
            tblCmsWcCertiHistory.setCreatedBy(createdBy);
            tblCmsWcCertiHistory.setVendorRating(tblCmsVendorRatingMaster.getVendorRatingId());
            tblCmsWcCertiHistory.setCreatedDt(createdDate);

            //TblCmsWcCertificate tblCmsWcCertificate = readInputs(request, response);

            if (wccertId != 0) {
                tblCmsWcCertificate.setWcCertId(wccertId);
                cmsWcCertificateServiceBean.updateCmsWcCertificate(tblCmsWcCertificate);
            } else {
                wccertId = cmsWcCertificateServiceBean.insertCmsWcCertificate(tblCmsWcCertificate);
            }

            tblCmsWcCertiHistory.setWcCertId(wccertId);
            int wccerthistId = cmsWcCertificateServiceBean.insertCmsWcCertihistory(tblCmsWcCertiHistory);

            cmsVendorReqWccService.updateReqest(wccerthistId, lotId);
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            int conId = c_ConsSrv.getContractId(Integer.parseInt(request.getParameter("tenderId")));
            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Work_Completion_Certificate.getName(), "Issue work completion certificate", "");
            //cmsWcCertificateServiceBean.insertCmsWcCertihistory();
            sendMailForWCIssued(request.getParameter("tenderId"), request.getParameter("lotId"), userId, "has Issued", "e-GP: PE has Issued Work Completion Certificate");
            response.sendRedirect("officer/WCCUpload.jsp?wcCertId=" + wcCertId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&&msg=wci&contractUserId=" + userId + "&contractSignId=" + contractId);
            //response.sendRedirect(contextPath + "/officer/ProgressReport.jsp?tenderId=" + tenderId+ "&lotId=" + lotId + "&msg=wci");
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest : " + ENDS);
    }

    /***
     * This function read the input values and returns TblCmsWcCertificate object.
     * @param request
     * @param response
     * @return TblCmsWcCertificate
     */
    private TblCmsWcCertificate readInputs(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.debug("readInputs : " + STARTS);
        TblCmsWcCertificate tblCmsWcCertificate = new TblCmsWcCertificate();

        HttpSession session = request.getSession();

        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
            createdBy = Integer.parseInt(session.getAttribute("userId").toString());
        }
        createdDate = Calendar.getInstance().getTime();

        if (request.getParameter("txtDateOfCompletion") != null) {
            actualDateOfCompletion = request.getParameter("txtDateOfCompletion");
        }
        actdtofcom = DateUtils.convertStringtoDate(actualDateOfCompletion, "dd/MM/yyyy");
        if (request.getParameter("radioWorkComplete") != null) {
            isWorkComplete = request.getParameter("radioWorkComplete");
        }

        if (request.getParameter("radioPerformanceGuarantee") != null) {
            pgStatus = request.getParameter("radioPerformanceGuarantee");
        }

        if (request.getParameter("radioPGRequire") != null) {
            newPgRequire = request.getParameter("radioPGRequire");
        }

        if (request.getParameter("txtPerformanceGuarantee") != null) {
            newPgAmt = Double.parseDouble(request.getParameter("txtPerformanceGuarantee"));
        }
        if (request.getParameter("radioRateVendor") != null) {
            rating = request.getParameter("radioRateVendor");
        }
        if (request.getParameter("selectSendToTenderer") != null) {
            sendToTenderer = request.getParameter("selectSendToTenderer");
        }
        if (request.getParameter("txtRemarks") != null) {
            remarks = request.getParameter("txtRemarks");
        }
        if (request.getParameter("tenderId") != null) {
            tenderId = Integer.parseInt(request.getParameter("tenderId"));
        }
        if (request.getParameter("contractUserId") != null) {
            userId = Integer.parseInt(request.getParameter("contractUserId"));
        }
        if (request.getParameter("contractSignId") != null) {
            contractId = Integer.parseInt(request.getParameter("contractSignId"));
        }
        //getDependentparameterValues(tenderId);
        tblCmsWcCertificate.setActualDateOfCompletion(actdtofcom);
        tblCmsWcCertificate.setContractId(contractId);
        tblCmsWcCertificate.setCreatedBy(createdBy);
        tblCmsWcCertificate.setCreatedDate(createdDate);
        tblCmsWcCertificate.setIsWorkComplete(isWorkComplete);
        tblCmsWcCertificate.setRemarks(remarks);
        tblCmsWcCertificate.setSendToTenderer(sendToTenderer);
        tblCmsVendorRatingMaster = vendorRatingMasterServiceBean.getCmsVendorRatingMasterForRating(rating);
        tblCmsWcCertificate.setTblCmsVendorRatingMaster(tblCmsVendorRatingMaster);
        tblCmsWcCertificate.setUserId(userId);
        LOGGER.debug("readInputs : " + ENDS);
        return tblCmsWcCertificate;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** send mail notification to tenderer on issuing the work completion certificate by PE */
    private boolean sendMailForWCIssued(String tenderId, String lotId, String logUserId, String Operation, String mailSubject) {
        boolean flag = false;
        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            List<Object[]> AcEmailId = accPaymentService.getAccountantEmailId(Integer.parseInt(tenderId));
            String[] strTo = new String[AcEmailId.size() + 1];
            int emailIndex = 0;
            for (Object[] emailId : AcEmailId) {
                strTo[emailIndex] = emailId[4].toString();
                emailIndex++;
            }
            strTo[emailIndex] = obj[9].toString();
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String emailId[] = new String[1];
            String mobileno = "";
            for (int i = 0; i < strTo.length; i++) {
                emailId[0] = strTo[i];
                registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.issueneOfWorkCompletionCertificate(Operation, c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0])));
                String mailText = mailContentUtility.issueneOfWorkCompletionCertificate(Operation, c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]));
                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.setEmailFrom(strFrom);
                sendMessageUtil.setEmailSub(mailSubject);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if ("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                sendMessageUtil.setSmsNo(mobileno);
                StringBuilder sb = new StringBuilder();
                sb.append("Dear User,%0AThis is to inform you that PE has issued Work Completion Certificate ");
                sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                sb.append("%0AThanks,%0AeGP System");
                sendMessageUtil.setSmsBody(sb.toString());
                sendMessageUtil.sendSMS();
            }
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }
}
