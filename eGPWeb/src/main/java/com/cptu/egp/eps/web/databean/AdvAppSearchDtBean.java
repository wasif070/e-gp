/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class AdvAppSearchDtBean {

    private short ministry;
    private short division;
    private short organisation;
    private String department;
    private int office;
    private String project;
    private String financialYear;
    private String budgetType;
    private String procNature;
    private String procType;
    private String appCode;
    private String appId;
    private String pkgNo;
    private String operation;
    private int value;
    private int value2;

    public String getAppCode()
    {
        return appCode;
    }

    public void setAppCode(String appCode)
    {
        this.appCode = appCode;
    }

    public String getAppId()
    {
        return appId;
    }

    public void setAppId(String appId)
    {
        this.appId = appId;
    }

    public String getBudgetType()
    {
        return budgetType;
    }

    public void setBudgetType(String budgetType)
    {
        this.budgetType = budgetType;
    }

    public short getDivision()
    {
        return division;
    }

    public void setDivision(short division)
    {
        this.division = division;
    }

    public String getFinancialYear()
    {
        return financialYear;
    }

    public void setFinancialYear(String financialYear)
    {
        this.financialYear = financialYear;
    }

    public short getMinistry()
    {
        return ministry;
    }

    public void setMinistry(short ministry)
    {
        this.ministry = ministry;
    }

    public int getOffice()
    {
        return office;
    }

    public void setOffice(int office)
    {
        this.office = office;
    }

    public String getOperation()
    {
        return operation;
    }

    public void setOperation(String operation)
    {
        this.operation = operation;
    }

    public short getOrganisation()
    {
        return organisation;
    }

    public void setOrganisation(short organisation)
    {
        this.organisation = organisation;
    }

    public String getPkgNo()
    {
        return pkgNo;
    }

    public void setPkgNo(String pkgNo)
    {
        this.pkgNo = pkgNo;
    }

    public String getProcNature()
    {
        return procNature;
    }

    public void setProcNature(String procNature)
    {
        this.procNature = procNature;
    }

    public String getProcType()
    {
        return procType;
    }

    public void setProcType(String procType)
    {
        this.procType = procType;
    }

    public int getValue()
    {
        return value;
    }

    public void setValue(int value)
    {
        this.value = value;
    }

    public String getProject()
    {
        return project;
    }

    public void setProject(String project)
    {
        this.project = project;
    }

    public int getValue2()
    {
        return value2;
    }

    public void setValue2(int value2)
    {
        this.value2 = value2;
    }

    public String getDepartment()
    {
        return department;
    }

    public void setDepartment(String department)
    {
        this.department = department;
    }

    public void printInfo()
    {
        try {
            Field dept = this.getClass().getField("department");
            Short deptId=dept.getShort(dept);
        }
        catch (IllegalArgumentException ex) {
            Logger.getLogger(AdvAppSearchDtBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex) {
            Logger.getLogger(AdvAppSearchDtBean.class.getName()).log(Level.SEVERE, null, ex);
        }        catch (NoSuchFieldException ex) {
            Logger.getLogger(AdvAppSearchDtBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (SecurityException ex) {
            Logger.getLogger(AdvAppSearchDtBean.class.getName()).log(Level.SEVERE, null, ex);
        }
             
    }

}