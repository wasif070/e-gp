/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblWorkFlowDocuments;
import com.cptu.egp.eps.web.servicebean.WorkFlowSrBean;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author test
 */
//@WebServlet(name = "workflowdocservlet",  urlPatterns = {"/workflowdocservlet"})
public class workflowdocservlet extends HttpServlet {

     private static final Logger LOGGER = Logger.getLogger(workflowdocservlet.class);
     private static final String LOGGERSTART = "Starts";
     private static final String LOGGEREND  = "Ends";
     private String logUserId ="0";
        
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         HttpSession session = request.getSession();
          if(session.getAttribute("userId")!=null){
                logUserId=session.getAttribute("userId").toString();
          }
          try {
             LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
              WorkFlowSrBean wsr = new WorkFlowSrBean();
             Map<String, String> m=  FilePathUtility.getFilePath();
            if (request.getParameter("action").equals("download")) {
                    LOGGER.debug("processRequest : action : download : "+logUserId+ LOGGERSTART);
             String fdocname =  request.getParameter("docName");
             int did = Integer.parseInt(request.getParameter("docid"));
             String userid1 =  request.getParameter("userid");
            int j =  fdocname.lastIndexOf('.');
            String lst =  fdocname.substring(j+1);
            String fst = fdocname.substring(0, j);
            File file = new File(m.get("WorkFlowFileUploadServlet")+Integer.parseInt(userid1)+"\\"+fst+"_"+did+"."+lst);
            InputStream fis = new FileInputStream(file);
            byte[] buf = new byte[Integer.valueOf(request.getParameter("docSize"))];
            int offset = 0;
            int numRead = 0;
                    while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0)) {
                offset += numRead;
            }
            fis.close();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + request.getParameter("docName")+ "\"");
            ServletOutputStream outputStream =  response.getOutputStream();
            outputStream.write(buf);
            outputStream.flush();
            outputStream.close();
                    LOGGER.debug("processRequest : action : download : "+logUserId+ LOGGEREND);
            } else if (request.getParameter("action").equals("remove")) {
                    LOGGER.debug("processRequest : action : remove : "+logUserId+ LOGGERSTART);
                String objectid = request.getParameter("objectid");
                String activityid = request.getParameter("activityid");
                String childid = request.getParameter("childid");
                String msg = "";
                boolean del = false;
               TblWorkFlowDocuments tblWorkFlowDocuments = new TblWorkFlowDocuments();
               tblWorkFlowDocuments.setWfDocumentId(Integer.parseInt(request.getParameter("docId")));
               tblWorkFlowDocuments.setDocumentName(request.getParameter("docName"));
               tblWorkFlowDocuments.setActivityId(0);
               tblWorkFlowDocuments.setChildId(0);
               tblWorkFlowDocuments.setDescription("");
               tblWorkFlowDocuments.setObjectId(0);
               tblWorkFlowDocuments.setUploadedDate(new Date());
               tblWorkFlowDocuments.setUserId(0);
               tblWorkFlowDocuments.setDocSize("");
               String fdocname = request.getParameter("docName");
               int uid = Integer.parseInt(request.getParameter("userid"));
               int j =  fdocname.lastIndexOf('.');
                String lst =  fdocname.substring(j+1);
                    String fst = fdocname.substring(0, j);
                int did = Integer.parseInt(request.getParameter("docId"));
                File f = new File(m.get("WorkFlowFileUploadServlet")+uid+"\\"+fst+"_"+did+"."+lst);
                del = f.delete();
                if(del){
                    msg = "File deleted successfully";
                }else{
                    msg = "File is not deleted";
                }
                wsr.deleteDoc(tblWorkFlowDocuments, uid);
                    LOGGER.debug("processRequest : action : remove : "+logUserId+ LOGGEREND);
                    response.sendRedirect(request.getContextPath()+"/officer/UploadFileDialog.jsp?userid="+uid+"&childid="+childid+"&objectid="+objectid+"&activityid="+activityid+"&msg="+msg);
            }else if(request.getParameter("action").equals("getdocs")){
                    LOGGER.debug("processRequest : action : getdocs : "+logUserId+ LOGGERSTART);
                    String objectid = request.getParameter("objectid");
                    String activityid = request.getParameter("activityid");
                    String childid = request.getParameter("childid");
                    String userid =  request.getParameter("userid");
              PrintWriter out = response.getWriter();
                    List<TblWorkFlowDocuments> tblwfdocs = wsr.getWorkFlowDocuments(Integer.parseInt(objectid), Integer.parseInt(activityid), Integer.parseInt(childid),Integer.parseInt(userid));
         if(tblwfdocs.size()>0){
        Iterator docs =  tblwfdocs.iterator();
        int i= 1;
         int c = 2;
         String colorchange = "style='background-color:#E4FAD0;' ";
        out.print("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
            out.print("<tr>");
            out.print("<th class='t-align-center'>S.No</th>");
            out.print("<th>File Name</th>");
            out.print("<th class='t-align-center'>Size in KB</th>");
            out.print("<th>Description</th>");
            out.print("</tr>");
            while(docs.hasNext()){
            TblWorkFlowDocuments twfd = (TblWorkFlowDocuments)docs.next();
                            if(c==i){
        out.print("<tr "+colorchange+">");
                            } else{
                                out.print("<tr>");
                            }
                            out.print("<td class='t-align-center'>"+i+"</td>");
            out.print("<td class='t-align-left'>"+twfd.getDocumentName()+"</td>");
                            out.print("<td class='t-align-center'>"+(Long.parseLong(twfd.getDocSize())/1024)+"</td>");
            out.print("<td class='t-align-left'>"+twfd.getDescription()+"</td>");
            out.print("</tr>");
                            if(c == i){
             c+=2;
                            }
            i++;
            }
            out.print("</table>");
           }
                  LOGGER.debug("processRequest : action : getdocs : "+logUserId+ LOGGEREND);
         }else if(request.getParameter("action").equals("gethistorydocs")){
            LOGGER.debug("processRequest : action : gethistorydocs : "+logUserId+ LOGGERSTART);
            String objectid = request.getParameter("objectid");
            String activityid = request.getParameter("activityid");
            String childid = request.getParameter("childid");
            String userid =  request.getParameter("userid");
            PrintWriter out = response.getWriter();
            List<TblWorkFlowDocuments> tblwfdocs = wsr.getWorkFlowDocuments(Integer.parseInt(objectid), Integer.parseInt(activityid), Integer.parseInt(childid),Integer.parseInt(userid));
            if(tblwfdocs.size()>0){
                Iterator docs =  tblwfdocs.iterator();
                int i= 1;
                int c = 2;
                String colorchange = "style='background-color:#E4FAD0;' ";
                out.print("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                out.print("<tr>");
                out.print("<th class='t-align-center'>S.No</th>");
                out.print("<th>File Name</th>");
                out.print("<th class='t-align-center'>Size in KB</th>");
                out.print("<th>Description</th>");
                out.print("<th>Action</th>");
                out.print("</tr>");
                while(docs.hasNext()){
                    TblWorkFlowDocuments twfd = (TblWorkFlowDocuments)docs.next();
                    if(c==i){
                        out.print("<tr "+colorchange+">");
                    } else{
                        out.print("<tr>");
                    }
                    out.print("<td class='t-align-center'>"+i+"</td>");
                    out.print("<td class='t-align-left'>"+twfd.getDocumentName()+"</td>");
                    out.print("<td class='t-align-center'>"+(Long.parseLong(twfd.getDocSize())/1024)+"</td>");
                    out.print("<td class='t-align-left'>"+twfd.getDescription()+"</td>");
                    out.write("<td class='t-align-center'><a href='" + request.getContextPath() + "/workflowdocservlet?docName=" + twfd.getDocumentName() + "&docSize=" + twfd.getDocSize() + "&action=download&docid=" + twfd.getWfDocumentId() + "&objectid="
                            + objectid + "&activityid=" + activityid + "&childid=" + childid + "&userid=" + userid + "'><img src='../resources/images/Dashboard/Download.png' alt='Download'></a>");
                    out.print("</tr>");
                    if(c == i){
                        c+=2;
                    }
                        i++;
                    }
                    out.print("</table>");
            }
            LOGGER.debug("processRequest : action : gethistorydocs : "+logUserId+ LOGGEREND);
         }
        } catch(Exception ex){
            LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("processRequest : "+logUserId+ LOGGEREND);
     }

    @Override
    protected void doGet(HttpServletRequest hsr, HttpServletResponse hsr1) throws ServletException, IOException {
        processRequest(hsr, hsr1);
    }

    @Override
    protected void doPost(HttpServletRequest hsr, HttpServletResponse hsr1) throws ServletException, IOException {
        processRequest(hsr, hsr1);
    }

}
