/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblTenderMaster;
import com.cptu.egp.eps.model.table.TblTenderPerfSecurity;
import com.cptu.egp.eps.service.serviceimpl.TblTenderPerformanceSec;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.AppExceptionHandler;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;

/**
 *
 * @author shreyansh
 */
public class TenderPerformanceSecurity extends HttpServlet
{

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();

        // Coad added by Dipal for Audit Trail Log.
        AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer"));
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        String idType = "tenderId";
        int auditId = Integer.parseInt(request.getParameter("tenderid"));
        String auditAction = "";
        String moduleName = EgpModule.Noa.getName();
        String remarks = "";

        int tenderId=0;int count=0;
        boolean oprSuccess=false;
        try
        {
            TblTenderPerformanceSec tenderPerformanceSec = (TblTenderPerformanceSec) AppContext.getSpringBean("TblTenderPerformanceSec");

            Object user = session.getAttribute("userId");
            String userId = null;
            if (user == null)
            {
                userId = "";
            }
            else
            {
                userId = user.toString();
            }

            TblTenderPerfSecurity tblTenderPerfSecurity = new TblTenderPerfSecurity();

            String tenderType=request.getParameter("tenderType"); String apppkgid=""; 

            //count = Integer.parseInt(request.getParameter("NoOfCurrency"));
            tenderId = Integer.parseInt(request.getParameter("tenderid"));
            int pkgid = Integer.parseInt(request.getParameter("pckid"));
            apppkgid = request.getParameter("forupdate");
            
            //Edited By Salahuddin for ICT
            if(tenderType.equals("NCT"))
            {
                tblTenderPerfSecurity.setCreatedBy(Integer.parseInt(userId));
                tblTenderPerfSecurity.setCreatedDt(new java.util.Date());
                tblTenderPerfSecurity.setPerSecurityAmt(new BigDecimal(request.getParameter("taka")));
                tblTenderPerfSecurity.setPercentage(new BigDecimal(request.getParameter("percentage")));
                tblTenderPerfSecurity.setLowestAmt(new BigDecimal(request.getParameter("lamount")));
                tblTenderPerfSecurity.setRoundId(Integer.parseInt(request.getParameter("rId")));
                tblTenderPerfSecurity.setTblTenderMaster(new TblTenderMaster(tenderId));
                tblTenderPerfSecurity.setPkgLotId(pkgid);

                if ("y".equalsIgnoreCase(request.getParameter("isEdit")))
                {
                    tblTenderPerfSecurity.setPerCostLotId(Integer.parseInt(apppkgid));
                    auditAction = "Edit Performance security by PE";
                    oprSuccess=tenderPerformanceSec.updatePerfSec(tblTenderPerfSecurity);
                }
                else
                    oprSuccess = tenderPerformanceSec.addPerfSec(tblTenderPerfSecurity);
            }
        else if (tenderType.equals("ICT"))
            {
                count = Integer.parseInt(request.getParameter("NoOfCurrency"));
                tblTenderPerfSecurity.setCreatedBy(Integer.parseInt(userId));
                tblTenderPerfSecurity.setCreatedDt(new java.util.Date());
                tblTenderPerfSecurity.setPerSecurityAmt(new BigDecimal(request.getParameter("pSecurityAmount1")));
                tblTenderPerfSecurity.setPercentage(new BigDecimal(request.getParameter("percentage1")));
                tblTenderPerfSecurity.setLowestAmt(new BigDecimal(request.getParameter("value1")));
                tblTenderPerfSecurity.setRoundId(Integer.parseInt(request.getParameter("rId")));
                tblTenderPerfSecurity.setTblTenderMaster(new TblTenderMaster(tenderId));
                tblTenderPerfSecurity.setPkgLotId(pkgid);
                tblTenderPerfSecurity.setCurrencyID(Integer.parseInt(request.getParameter("currencyID1")));

                if ("y".equalsIgnoreCase(request.getParameter("isEdit")))
                {
                    tblTenderPerfSecurity.setPerCostLotId(Integer.parseInt(request.getParameter("forupdate1")));
                    auditAction = "Edit Performance security by PE";
                    oprSuccess=tenderPerformanceSec.updatePerfSec(tblTenderPerfSecurity);
                }
                else
                    oprSuccess = tenderPerformanceSec.addPerfSec(tblTenderPerfSecurity);//for the first currency


                if(count>=2)
                    {
                        tblTenderPerfSecurity.setPerSecurityAmt(new BigDecimal(request.getParameter("pSecurityAmount2")));
                        tblTenderPerfSecurity.setPercentage(new BigDecimal(request.getParameter("percentage2")));
                        tblTenderPerfSecurity.setLowestAmt(new BigDecimal(request.getParameter("value2")));
                        tblTenderPerfSecurity.setCurrencyID(Integer.parseInt(request.getParameter("currencyID2")));

                        if ("y".equalsIgnoreCase(request.getParameter("isEdit")))
                        {
                            tblTenderPerfSecurity.setPerCostLotId(Integer.parseInt(request.getParameter("forupdate2")));
                            auditAction = "Edit Performance security by PE";
                            oprSuccess=tenderPerformanceSec.updatePerfSec(tblTenderPerfSecurity);
                        }
                        else
                            oprSuccess = tenderPerformanceSec.addPerfSec(tblTenderPerfSecurity);
                    }
                    if(count >= 3)
                    {
                        tblTenderPerfSecurity.setPerSecurityAmt(new BigDecimal(request.getParameter("pSecurityAmount3")));
                        tblTenderPerfSecurity.setPercentage(new BigDecimal(request.getParameter("percentage3")));
                        tblTenderPerfSecurity.setLowestAmt(new BigDecimal(request.getParameter("value3")));
                        tblTenderPerfSecurity.setCurrencyID(Integer.parseInt(request.getParameter("currencyID3")));
                        //oprSuccess=tenderPerformanceSec.addPerfSec(tblTenderPerfSecurity);//for the third currency
                        if ("y".equalsIgnoreCase(request.getParameter("isEdit")))
                        {
                            tblTenderPerfSecurity.setPerCostLotId(Integer.parseInt(request.getParameter("forupdate3")));
                            auditAction = "Edit Performance security by PE";
                            oprSuccess=tenderPerformanceSec.updatePerfSec(tblTenderPerfSecurity);
                        }
                        else
                            oprSuccess = tenderPerformanceSec.addPerfSec(tblTenderPerfSecurity);
                    }
                    if(count==4)
                    {
                        tblTenderPerfSecurity.setPerSecurityAmt(new BigDecimal(request.getParameter("pSecurityAmount4")));
                        tblTenderPerfSecurity.setPercentage(new BigDecimal(request.getParameter("percentage4")));
                        tblTenderPerfSecurity.setLowestAmt(new BigDecimal(request.getParameter("value4")));
                        tblTenderPerfSecurity.setCurrencyID(Integer.parseInt(request.getParameter("currencyID4")));
                        //oprSuccess=tenderPerformanceSec.addPerfSec(tblTenderPerfSecurity);//for the forth currency

                        if ("y".equalsIgnoreCase(request.getParameter("isEdit")))
                        {
                            tblTenderPerfSecurity.setPerCostLotId(Integer.parseInt(request.getParameter("forupdate4")));
                            auditAction = "Edit Performance security by PE";
                            oprSuccess=tenderPerformanceSec.updatePerfSec(tblTenderPerfSecurity);
                        }
                        else
                            oprSuccess = tenderPerformanceSec.addPerfSec(tblTenderPerfSecurity);                        
                    }
            }

            
             response.sendRedirect("officer/NOA.jsp?tenderId=" + tenderId + "&msgFlag=y");
           
        }
        catch (Exception ex)
        {
            auditAction="Error in "+auditAction+" "+ex.getMessage();
            ex.printStackTrace();
            AppExceptionHandler excHandle = new AppExceptionHandler();
            excHandle.stack2string(ex);
        }
        finally
        {
                        
            if(!oprSuccess)
            {
                auditAction="Error in "+auditAction;
            }
            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
             
            out.close();
        }
        
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
