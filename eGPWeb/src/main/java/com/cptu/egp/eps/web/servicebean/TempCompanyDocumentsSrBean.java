/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblCompanyDocuments;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblTempCompanyDocuments;
import com.cptu.egp.eps.model.table.TblTempTendererEsignature;
import com.cptu.egp.eps.model.table.TblTempTendererMaster;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.databean.TempCompanyDocumentsDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import com.cptu.egp.eps.web.utility.HandleSpecialChar;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.egp.eps.web.utility.XMLReader;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.File;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author Administrator
 */
public class TempCompanyDocumentsSrBean {

    final static Logger logger = Logger.getLogger(TempCompanyDocumentsSrBean.class);
    UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private String logUserId = "0";
    private AuditTrail auditTrail;
    private static final String START = " Starts";
    private static final String END = " Ends";

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        userRegisterService.setUserId(logUserId);
        this.logUserId = logUserId;
    }

    public boolean uploadDoc(TempCompanyDocumentsDtBean tempCompanyDocumentsDtBean, int tendererId) {
        logger.debug("uploadDoc : " + logUserId + START);
        boolean flag = false;

        try {
            TblTempCompanyDocuments tblTempCompanyDocuments = _toTblTempCompanyDocuments(tempCompanyDocumentsDtBean);
            tblTempCompanyDocuments.setUploadedDate(new Date());
            userRegisterService.uploadSupportDocs(tblTempCompanyDocuments, tendererId);
            flag = true;
        } catch (Exception e) {
            logger.error("uploadDoc : " + e);
        }
        logger.debug("uploadDoc : " + logUserId + END);
        return flag;

    }
    
    public boolean uploadDocReapply(TempCompanyDocumentsDtBean companyDocumentsDtBean, int tendererId) {
        logger.debug("uploadDocReapply : " + logUserId + START);
        boolean flag = false;

        try {
            companyDocumentsDtBean.setDocumentTypeId(companyDocumentsDtBean.getDocumentType()); 
            TblCompanyDocuments tblCompanyDocuments = _toTblCompanyDocuments(companyDocumentsDtBean);
            tblCompanyDocuments.setUploadedDate(new Date());
            userRegisterService.uploadSupportDocsReapply(tblCompanyDocuments, tendererId);
            flag = true;
        } catch (Exception e) {
            logger.error("uploadDocReapply : " + e);
        }
        logger.debug("uploadDocReapply : " + logUserId + END);
        return flag;

    }
    
    public TblCompanyDocuments _toTblCompanyDocuments(TempCompanyDocumentsDtBean companyDocumentsDtBean) {
        logger.debug("_toTblCompanyDocuments : " + logUserId + START);
        TblCompanyDocuments tblCompanyDocuments = new TblCompanyDocuments();
        BeanUtils.copyProperties(companyDocumentsDtBean, tblCompanyDocuments);
        logger.debug("_toTblCompanyDocuments : " + logUserId + END);
        return tblCompanyDocuments;
    }

    public TblTempCompanyDocuments _toTblTempCompanyDocuments(TempCompanyDocumentsDtBean tempCompanyDocumentsDtBean) {
        logger.debug("_toTblTempCompanyDocuments : " + logUserId + START);
        TblTempCompanyDocuments tblTempCompanyDocuments = new TblTempCompanyDocuments();
        BeanUtils.copyProperties(tempCompanyDocumentsDtBean, tblTempCompanyDocuments);
        logger.debug("_toTblTempCompanyDocuments : " + logUserId + END);
        return tblTempCompanyDocuments;
    }

    public String checkJVCA(int userId) throws Exception {
        logger.debug("checkJVCA : " + logUserId + START);
        String data = null;
        TblLoginMaster tblLoginMaster = userRegisterService.findUserByCriteria("userId", Operation_enum.EQ, userId).get(0);
        TblTempTendererMaster tblTempTendererMaster = userRegisterService.findTendererByCriteria("tblLoginMaster", Operation_enum.EQ, new TblLoginMaster(tblLoginMaster.getUserId())).get(0);
        int tendererId = tblTempTendererMaster.getTendererId();
        if (tblLoginMaster.getIsJvca().equals("yes")) {
            data = tendererId + "y";
        } else {
            data = tendererId + "n";
        }
        logger.debug("checkJVCA : " + logUserId + END);
        return data;
    }

    public String checkJVCA(String userId) throws Exception {
        logger.debug("checkJVCA : " + logUserId + START);
        logger.debug("checkJVCA : " + logUserId + END);
        return userRegisterService.getTendererId(userId);
    }

    public List<TblTempCompanyDocuments> getTblTempCompanyDocumentses(int tendererId) throws Exception {
        logger.debug("getTblTempCompanyDocumentses : " + logUserId + START);
        logger.debug("getTblTempCompanyDocumentses : " + logUserId + END);
        return userRegisterService.findUserDocuments("tblTempTendererMaster", Operation_enum.EQ, new TblTempTendererMaster(tendererId));
    }

    public List<TblCompanyDocuments> getTblCompanyDocumentses(int tendererId) throws Exception {
        logger.debug("getTblCompanyDocumentses : " + logUserId + START);
        logger.debug("getTblCompanyDocumentses : " + logUserId + END);
        return userRegisterService.findBidderDocuments("tblTendererMaster", Operation_enum.EQ, new TblTendererMaster(tendererId), "documentTypeId", Operation_enum.NE, "briefcase");
    }

    public boolean deleteDoc(TblTempCompanyDocuments tempCompanyDocuments, int userId) {
        logger.debug("deleteDoc : " + logUserId + START);
        File f = new File(FilePathUtility.getFilePath().get("FileUploadServlet") + userId + "\\" + tempCompanyDocuments.getDocumentName());
        boolean isFileDel = f.delete();
        logger.debug("deleteDoc : " + logUserId + " isFileDel : " + isFileDel);
        logger.debug("deleteDoc : " + logUserId + END);
        return userRegisterService.deleteUserDoc(tempCompanyDocuments);
    }
    
    public boolean deleteDocReapply(TblCompanyDocuments tblCompanyDocuments, int userId) {
        logger.debug("deleteDocReapply : " + logUserId + START);
        File f = new File(FilePathUtility.getFilePath().get("FileUploadServlet") + userId + "\\" + tblCompanyDocuments.getDocumentName());
        boolean isFileDel = f.delete();
        logger.debug("deleteDocReapply : " + logUserId + " isFileDel : " + isFileDel);
        logger.debug("deleteDocReapply : " + logUserId + END);
        return userRegisterService.deleteUserDocReapply(tblCompanyDocuments);
    }

    public boolean uploadeSign(TempCompanyDocumentsDtBean tempCompanyDocumentsDtBean, int tendererId) {
        logger.debug("uploadeSign: " + logUserId + START);
        TblTempTendererEsignature tblTempTendererEsignature = _toTblTempTendererEsignature(tempCompanyDocumentsDtBean);
        tblTempTendererEsignature.setUploadedDate(new Date());
        tblTempTendererEsignature.setTblTempTendererMaster(new TblTempTendererMaster(tendererId));
        logger.debug("uploadeSign : " + logUserId + END);
        return userRegisterService.uploadEsign(tblTempTendererEsignature);
    }

    public TblTempTendererEsignature _toTblTempTendererEsignature(TempCompanyDocumentsDtBean tempCompanyDocumentsDtBean) {
        logger.debug("_toTblTempTendererEsignature : " + logUserId + START);
        TblTempTendererEsignature tblTempTendererEsignature = new TblTempTendererEsignature();
        BeanUtils.copyProperties(tempCompanyDocumentsDtBean, tblTempTendererEsignature);
        logger.debug("_toTblTempTendererEsignature : " + logUserId + END);
        return tblTempTendererEsignature;
    }

    public List<TblTempTendererEsignature> getTblTempEsign(int tendererId) throws Exception {
        logger.debug("getTblTempEsign : " + logUserId + START);
        logger.debug("getTblTempEsign : " + logUserId + END);
        return userRegisterService.findUserEsign("tblTempTendererMaster", Operation_enum.EQ, new TblTempTendererMaster(tendererId));
    }

    public boolean deleteEsign(int docId, String docName, int userId) {
        logger.debug("deleteEsign : " + logUserId + START);
        File f = new File(FilePathUtility.getFilePath().get("FileUploadServlet") + userId + "\\" + docName);
        boolean isFileDel = f.delete();
        logger.debug("deleteEsign : " + logUserId + " isFileDel : " + isFileDel);
        TblTempTendererEsignature esignature = new TblTempTendererEsignature();
        esignature.setEsignatureId(docId);
        esignature.setDocumentBrief("");
        esignature.setDocumentName("");
        esignature.setDocumentSize("");
        esignature.setUploadedDate(new Date());
        esignature.setTblTempTendererMaster(new TblTempTendererMaster(999));
        logger.debug("deleteEsign : " + logUserId + END);
        return userRegisterService.deleteUserEsign(esignature);
    }

    public CommonMsgChk addUser(int userId, String action, String title, String name) throws Exception {
        logger.debug("addUser : " + logUserId + START);
        CommonMsgChk chk = userRegisterService.addUser(userId, action);
        if (chk != null && chk.getFlag()) {
            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            for (Object object : userRegisterService.contentAdmMailId()) {
                String[] mail = {(String) object};
                String mailText = mailContentUtility.contAdmMailFinalSub(chk.getMsg());
                sendMessageUtil.setEmailTo(mail);
                sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
                sendMessageUtil.setEmailSub("eGP: User's profile verification");
                sendMessageUtil.setEmailMessage(mailText);
                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> peBUEmail = tenderCommonService.returndata("getEmailIdfromUserId", String.valueOf(userId), null);

                userRegisterService.contentAdmMsgBox((String) object, XMLReader.getMessage("msgboxfrom"), HandleSpecialChar.handleSpecialChar("Verification of User's Profile (" + peBUEmail.get(0).getFieldName1() + ")."), msgBoxContentUtility.contAdmMsgFinalSub(chk.getMsg()));

                sendMessageUtil.sendEmail();
                mail = null;
            }
            try {
                String mobEmail = userRegisterService.getTendererMobEmail(userId);
                String mail = null;
                String mobile = null;
                if (mobEmail != null) {
                    if (!("".equals(mobEmail))) {
                        mobile = mobEmail.substring(mobEmail.lastIndexOf('$') + 1, mobEmail.length());
                        mail = mobEmail.substring(0, mobEmail.lastIndexOf('$'));
                    }
                }
                String[] mails = {mail};
                String mailText = mailContentUtility.finalSubDocMailToTenderer(String.valueOf(userId), title, name);
                sendMessageUtil.setEmailTo(mails);
                sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
                sendMessageUtil.setEmailSub("e-GP System:  User Registration - Successful Final Profile Submission");/////////
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                sendMessageUtil.setSmsBody("Dear User,%0AProfile successfully submitted. You will be notified on approval /rejection soon.%0Ae-GP User Registration Desk");
                sendMessageUtil.setSmsNo(mobile);
                sendMessageUtil.sendSMS();
                mails = null;
            } catch (Exception ex) {
                logger.error("addUser : " + logUserId + " : " + ex);
            }
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        logger.debug("addUser : " + logUserId + END);
        return chk;
    }

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        userRegisterService.setAuditTrail(auditTrail);
    }
}
