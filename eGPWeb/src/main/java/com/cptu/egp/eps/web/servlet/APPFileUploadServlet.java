/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.web.servicebean.APPSrBean;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class APPFileUploadServlet extends HttpServlet {

    private static final String TMP_DIR_PATH = "c:\\tmp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = "C:\\eGP\\Document\\AppDocuments\\";

    private static final Logger LOGGER = Logger.getLogger(APPFileUploadServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if (!tmpDir.isDirectory()) {
            tmpDir.mkdir();
        }
    }

    /**
     * This Servlet is use for upload files for APP.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        String realPath = DESTINATION_DIR_PATH + "1";
        File destinationDir = new File(realPath);
        if (!destinationDir.isDirectory()) {
            destinationDir.mkdir();
        }
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        try {
            DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
            /*
             *Set the size threshold, above which content will be stored on disk.
             */
            fileItemFactory.setSizeThreshold(1 * 1024 * 1024); //1 MB
		/*
             * Set the temporary directory to store the uploaded files of size above threshold.
             */
            fileItemFactory.setRepository(tmpDir);

            ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
            try {
                /*
                 * Parse the request
                 */
                List items = uploadHandler.parseRequest(request);
                Iterator itr = items.iterator();
                //For Supporting Document
                String pckNo = "";
                String desc = "";
                String FileName = "";
                long FileSize = 0;
                String docsBrief = "";
                int appid = 0;
                int pckid = 0;
                String fileName = "";
                while (itr.hasNext()) {
                    FileItem item = (FileItem) itr.next();
                    //For Supporting Document
                    /*
                     * Handle Form Fields.
                     */

                    if (item.isFormField()) {
                        if (item.getFieldName().equals("documentBrief")) {
                            docsBrief = item.getString();
                        } else if (item.getFieldName().equals("appid")) {
                            appid = Integer.parseInt(item.getString());
                        } else if (item.getFieldName().equals("pckid")) {
                            pckid = Integer.parseInt(item.getString());
                        }else if (item.getFieldName().equals("pckNo")) {
                            pckNo = item.getString();
                        }else if (item.getFieldName().equals("desc")) {
                            desc = item.getString();
                        }
//                            tempCompanyDocumentsDtBean.setDocumentBrief(item.getString());
//                        } else if (item.getFieldName().equals("documentType")) {
//                            tempCompanyDocumentsDtBean.setDocumentType(item.getString());
//                        } else if (item.getFieldName().equals("tendererId")) {
//                            tendererid = Integer.parseInt(item.getString());
//                        } else if (item.getFieldName().equals("funName")) {
//                            funName = item.getString();
//                        }

                    } else {
                        //Handle Uploaded files.
                        FileName = item.getName();
                        FileSize = item.getSize();
                        if (item.getName().lastIndexOf("\\") == -1) {
                            fileName = item.getName();
                        } else {
                             fileName = item.getName().substring(item.getName().lastIndexOf("\\")+1, item.getName().length());
                        }
                        //fileName  = fileName.replaceAll(" ", "");

                        File file = new File(destinationDir, fileName);
                        item.write(file);
                    }
                }
                int userid = 0;
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userid = Integer.parseInt(hs.getAttribute("userId").toString());
                }
                //For Supporting Document
                APPSrBean appSrBean = new APPSrBean();
                int docid = appSrBean.addAppEngEstDoc(docsBrief, appid, pckid, userid, fileName,FileSize);
                File oldfile = new File(destinationDir + "\\" + fileName);
                int j = fileName.lastIndexOf('.');
                String lst = fileName.substring(j + 1);
                String fst = fileName.substring(0, j);
                File newFile = new File(destinationDir + "\\" + fst + "_" + docid + "." + lst);
                boolean sus = oldfile.renameTo(newFile);
                if(sus){
                    response.sendRedirect("officer/APPDashboardDocs.jsp?appId=" + appid + "&pckId=" + pckid + "&pckno="+pckNo + "&desc=" +desc);
                    //response.sendRedirectFilter("APPDashboardDocs.jsp?appId=" + appid + "&pckId=" + pckid + "&pckno="+pckNo + "&desc=" +desc);
                }
                response.sendRedirect("officer/APPDashboardDocs.jsp?appId=" + appid + "&pckId=" + pckid + "&pckno="+pckNo + "&desc=" +desc);
                //response.sendRedirectFilter("APPDashboardDocs.jsp?appId=" + appid + "&pckId=" + pckid + "&pckno="+pckNo + "&desc=" +desc);
                
            } catch (Exception ex) {
                 LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
            }
        }finally{
            out.close();
        }
         LOGGER.debug("processRequest  : "+logUserId+ LOGGEREND);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
