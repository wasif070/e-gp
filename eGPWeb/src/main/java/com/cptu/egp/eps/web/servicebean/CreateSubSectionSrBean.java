/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.model.table.TblIttHeader;
import com.cptu.egp.eps.model.table.TblTemplateSections;
import com.cptu.egp.eps.service.serviceinterface.CreateSubSection;
import com.cptu.egp.eps.service.serviceinterface.TemplateSection;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.eps.service.audit.AuditTrail;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author yanki
 */
public class CreateSubSectionSrBean extends HttpServlet {

    CreateSubSection createSubSection = (CreateSubSection) AppContext.getSpringBean("CreateSubSectionService");
    TemplateSection templateSection = (TemplateSection) AppContext.getSpringBean("TemplateSectionService");
    final static Logger LOGGER = Logger.getLogger(CreateSubSectionSrBean.class);
    String logUserId = "0";
    
    public void setLogUserId(String logUserId) {
        createSubSection.setUserId(logUserId);
        templateSection.setUserId(logUserId);
        this.logUserId = logUserId;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if (request.getSession().getAttribute("userId") != null) {
                logUserId = request.getSession().getAttribute("userId").toString();
            }
            String action = request.getParameter("action");
            LOGGER.debug(action + " : " + logUserId + " Starts");
            LOGGER.debug(action + " : " + logUserId + " " + request.getQueryString());
            if (action != null) {
                if (action.equalsIgnoreCase("update")) {
                    int headerId = Integer.parseInt(request.getParameter("headerId"));
                    String headerName = request.getParameter("subSectionDetail");
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    TblIttHeader tblIttHeader = new TblIttHeader();
                    tblIttHeader.setIttHeaderId(headerId);
                    tblIttHeader.setIttHeaderName(headerName);
                    tblIttHeader.setTblTemplateSections(new TblTemplateSections(sectionId));
                    boolean status = createSubSection.updateSubSection(tblIttHeader);
                    out.print(status);
                } else if (action.equalsIgnoreCase("delete")) {
                    int headerId = Integer.parseInt(request.getParameter("headerId"));
                    String headerName = request.getParameter("subSectionDetail");
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    TblIttHeader tblIttHeader = new TblIttHeader();
                    tblIttHeader.setIttHeaderId(headerId);
                    tblIttHeader.setIttHeaderName(headerName);
                    tblIttHeader.setTblTemplateSections(new TblTemplateSections(sectionId));
                    boolean status = createSubSection.deleteSubSection(tblIttHeader);
                    out.print(status);
                } else if (action.equalsIgnoreCase("add")) {
                    String headerName = request.getParameter("subSectionDetail");
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    TblIttHeader tblIttHeader = new TblIttHeader();
                    tblIttHeader.setIttHeaderName(headerName);
                    tblIttHeader.setTblTemplateSections(new TblTemplateSections(sectionId));
                    boolean status = createSubSection.createSubSection(tblIttHeader);
                    int ittHeaderId = tblIttHeader.getIttHeaderId();
                    out.print(status + "," + ittHeaderId);
                } else if (action.equalsIgnoreCase("deleteSubSection")) {
                    int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    int templateId = Integer.parseInt(request.getParameter("templateId"));
                    createSubSection.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")));
                    boolean flag = createSubSection.deleteSection(sectionId);
                    response.sendRedirect("admin/DefineSTDInDtl.jsp?templateId=" + templateId + "&delete=" + flag);
                    //response.sendRedirectFilter("DefineSTDInDtl.jsp?templateId=" + templateId + "&delete=" + flag);
                    return;
                }
            } else {
                String templateId = request.getParameter("hdTemplateId");
                String sectionId = request.getParameter("hdSectionId");

                List subSectionName = new ArrayList();
                int i = 1;
                while (request.getParameter("txtSubSecName" + i) != null) {
                    subSectionName.add(request.getParameter("txtSubSecName" + i));
                    i++;
                }
                TblIttHeader tblIttHeader;
                for (i = 0; i < subSectionName.size(); i++) {
                    tblIttHeader = new TblIttHeader();
                    tblIttHeader.setTblTemplateSections(new TblTemplateSections(Integer.parseInt(sectionId)));
                    tblIttHeader.setIttHeaderName(subSectionName.get(i).toString());
                    boolean flag = createSubSection.createSubSection(tblIttHeader);
                    tblIttHeader = null;
                }
                response.sendRedirect("admin/subSectionDashBoard.jsp?sectionId=" + sectionId + "&templateId=" + templateId + "&flag=1");
                //response.sendRedirectFilter("subSectionDashBoard.jsp?sectionId=" + sectionId + "&templateId=" + templateId + "&flag=1");
            }
            LOGGER.debug(action + " : " + logUserId + " Ends");
        } catch (Exception ex) {
            LOGGER.error("CreateSubSectionSrBean : " + logUserId + " " + ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public List<TblIttHeader> getSubSectionDetail(int sectionId) {
        LOGGER.debug("getSubSectionDetail : " + logUserId + " Starts");
        List subSection = null;
        try {
            subSection = createSubSection.getSubSection(sectionId);
        } catch (Exception e) {
            LOGGER.error("getSubSectionDetail : " + logUserId + e);
        }
        LOGGER.debug("getSubSectionDetail : " + logUserId + " Ends");
        return subSection;
    }

    public List<TblIttHeader> getSubSection_TDSAppl(int sectionId) {
        LOGGER.debug("getSubSection_TDSAppl : " + logUserId + " Starts");
        List tblIttHeaderList = new ArrayList<TblIttHeader>();
        try {
        for (Object[] objects : createSubSection.getSubSectionWithTDSAppl(sectionId)) {
            tblIttHeaderList.add(new TblIttHeader((Integer) objects[0], (String) objects[1]));
        }
        } catch (Exception e) {
            LOGGER.error("getSubSection_TDSAppl : " + logUserId + e);
        }

        LOGGER.debug("getSubSection_TDSAppl : " + logUserId + " Ends");

        return tblIttHeaderList;
    }

    public String getContectType(int sectionId) {
        LOGGER.debug("getContectType : " + logUserId + " Starts");
        String str = "";
        //List templateDetail = templateSection.getSectionType(sectionId);
        try {
        List<com.cptu.egp.eps.model.table.TblTemplateSections> templateSectionsList =
                templateSection.getSectionType(sectionId);
            str = templateSectionsList.get(0).getContentType();
        } catch (Exception e) {
            LOGGER.error("getContectType : " + logUserId + e);
    }
        
        LOGGER.debug("getContectType : " + logUserId + " Ends");
        return str;
    }

     public String getSecName(short templateId,String Type) {
        LOGGER.debug("getSecName : " + logUserId + " Starts");
        String str = "";
        //List templateDetail = templateSection.getSectionType(sectionId);
        try {
            List<com.cptu.egp.eps.model.table.TblTemplateSections> templateSectionsList =
                templateSection.getSecName(templateId,Type);
            str = templateSectionsList.get(0).getSectionName();
        } catch (Exception e) {
            LOGGER.error("getSecName : " + logUserId + e);
        }

        LOGGER.debug("getSecName : " + logUserId + " Ends");
        return str;
    }


    public String getSectionName(int sectionId) {
        LOGGER.debug("getSectionName : " + logUserId + " Starts");
        String str = "";
        //List templateDetail = templateSection.getSectionType(sectionId);
        try {
            List<com.cptu.egp.eps.model.table.TblTemplateSections> templateSectionsList =
                templateSection.getSectionType(sectionId);
            str = templateSectionsList.get(0).getSectionName();
        } catch (Exception e) {
            LOGGER.error("getSectionName : " + logUserId + e);
        }

        LOGGER.debug("getSectionName : " + logUserId + " Ends");
        return str;
    }
    
    public boolean clauseCreated(int headerId) {
        LOGGER.debug("clauseCreated : " + logUserId + " Starts");
        boolean flag = false;
        SectionClauseSrBean sectionClauseSrBean = new SectionClauseSrBean();
        try {
            sectionClauseSrBean.setLogUserId(logUserId);
        long count = sectionClauseSrBean.getClauseCount(headerId);
        sectionClauseSrBean = null;
            if (count == 0) {
                flag = false;
            } else {
                flag = true;
        }
        } catch (Exception e) {
            LOGGER.error("clauseCreated : " + logUserId + e);
    }
        LOGGER.debug("clauseCreated : " + logUserId + " Ends");
        return flag;
    }
    /*public String getSubSectionContentType(int sectionId){

        List subSection = createSubSection.getSubSection(sectionId);
        return subSection;
        return "";
    }*/
}
