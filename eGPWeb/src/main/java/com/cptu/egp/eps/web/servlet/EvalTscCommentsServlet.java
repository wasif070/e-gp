/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblEvalTsccomments;
import com.cptu.egp.eps.service.serviceinterface.EvalTscCommentsService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;


/**
 *
 * @author Administrator
 */
public class EvalTscCommentsServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
       
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EvalTscCommentsServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EvalTscCommentsServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */
            boolean flag = false;
            String msg = "";
            String action = request.getParameter("action");
            String pageName = "";
            HttpSession objHS=request.getSession();
            
             // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), objHS.getAttribute("sessionId"), objHS.getAttribute("userTypeId"), request.getHeader("referer"));
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType = "tenderId";
            int auditId = 0;
            String auditAction =null;
            String moduleName = EgpModule.Evaluation.getName();
            String remarks = "";
            
            EvalTscCommentsService evalTscCommentsService = (EvalTscCommentsService)AppContext.getSpringBean("EvalTscCommentsService");
            if("delete".equalsIgnoreCase(action)){
                int commentId = Integer.parseInt(request.getParameter("commentId"));
                auditAction ="Recommendation deleted by Technical Sub Committee";
                auditId=Integer.parseInt(request.getParameter("tenderId"));
                msg = "Deleted";
                flag = evalTscCommentsService.delRec(commentId);
                if(!flag)
                {
                    auditAction="Error in "+auditAction;
                }
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                pageName = "officer/EvalTscComments.jsp?tenderId="+request.getParameter("tenderId")+"&frmId="+request.getParameter("formId")+"&msg="+msg+"&pkgLotId="+request.getParameter("pkgLotId")+"&uId="+request.getParameter("bidderId");
            }
            if("add".equalsIgnoreCase(action))
            {
                auditAction ="Recommendation Given by Technical Sub Committee";
                auditId=Integer.parseInt(request.getParameter("tenderId"));
                remarks=request.getParameter("comments");
                flag = insertData(request,evalTscCommentsService);
                if(flag)
                {
                    msg = "posted";
                }
                else
                {
                    auditAction="Error in "+auditAction;
                    msg = "error";
                }
                
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                pageName = "officer/EvalTscComments.jsp?tenderId="+request.getParameter("tenderId")+"&frmId="+request.getParameter("formId")+"&msg="+msg+"&pkgLotId="+request.getParameter("pkgLotId")+"&uId="+request.getParameter("bidderId");
            }
            if("updateLink".equalsIgnoreCase(action))
            {
                int commentId = Integer.parseInt(request.getParameter("commentId"));
                flag = true;
                pageName = "officer/EvalTscComments.jsp?tenderId="+request.getParameter("tenderId")+"&frmId="+request.getParameter("formId")+"&pkgLotId="+request.getParameter("pkgLotId")+"&uId="+request.getParameter("bidderId")+"&cmtId="+commentId+"&action=update";
                //String cmt = updateAjax(commentId,evalTscCommentsService);
                //out.print(cmt);
                //out.close();
            }
            if("update".equalsIgnoreCase(action))
            {
                remarks=request.getParameter("comments");
                auditAction ="Recommendation updated by Technical Sub Committee";
                auditId=Integer.parseInt(request.getParameter("tenderId"));
                
                int commentId = Integer.parseInt(request.getParameter("commentId"));
                flag = updateComments(request, evalTscCommentsService);
                if(flag){
                pageName = "officer/EvalTscComments.jsp?tenderId="+request.getParameter("tenderId")+"&frmId="+request.getParameter("formId")+"&pkgLotId="+request.getParameter("pkgLotId")+"&uId="+request.getParameter("bidderId")+"&cmtId="+commentId+"&msg=update";
                }else{
                    auditAction="Error in "+auditAction;
                    pageName = "officer/EvalTscComments.jsp?tenderId="+request.getParameter("tenderId")+"&frmId="+request.getParameter("formId")+"&pkgLotId="+request.getParameter("pkgLotId")+"&uId="+request.getParameter("bidderId")+"&cmtId="+commentId+"&msg=notupdate";
                }
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                //String cmt = updateAjax(commentId,evalTscCommentsService);
                //out.print(cmt);
                //out.close();
            }
            if(flag){
                response.sendRedirect(pageName);
            }
            
        } finally {
            out.close();
            out.flush();
        }
    } 

    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private boolean insertData(HttpServletRequest request,EvalTscCommentsService evalTscCommentsService){
        TblEvalTsccomments tblEvalTsccomments = new TblEvalTsccomments();
            boolean flag = false;
            int tenderId = Integer.parseInt(request.getParameter("tenderId"));
            int formId = Integer.parseInt(request.getParameter("formId"));
            int pkgLotId = Integer.parseInt(request.getParameter("pkgLotId"));
            String comments = request.getParameter("comments");
            HttpSession session = request.getSession();
            int userId = Integer.parseInt(session.getAttribute("userId").toString());
            int bidderId = Integer.parseInt(request.getParameter("bidderId"));

            tblEvalTsccomments.setComments(comments);
            tblEvalTsccomments.setCommentsDt(new Date());
            tblEvalTsccomments.setFormId(formId);
            tblEvalTsccomments.setPkgLotId(pkgLotId);
            tblEvalTsccomments.setTenderId(tenderId);
            tblEvalTsccomments.setEvalStatus("Evaluation");
            tblEvalTsccomments.setTscUserId(userId);
            tblEvalTsccomments.setUserId(bidderId);
            tblEvalTsccomments.setTscGovUserId(Integer.parseInt(request.getSession().getAttribute("govUserId").toString()));
            flag = evalTscCommentsService.insertData(tblEvalTsccomments);
        
        return flag;
    }
    /*private String updateAjax(int commentId,EvalTscCommentsService evalTscCommentsService){
        StringBuilder sb = new StringBuilder();
        TblEvalTsccomments tblEvalTsccomments = new TblEvalTsccomments();
        tblEvalTsccomments = evalTscCommentsService.getComments(commentId);
        sb.append("<textarea rows='5' id='comments' name='comments' class='formTxtBox_1' style='width: 99%;'>"+tblEvalTsccomments.getComments()+"</textarea>");
        //sb.append("<script type='text/javascript'>CKEDITOR.replace( 'comments',{toolbar : 'egpToolbar'});</script>");
        sb.append("<input type='hidden' name='commentId' value='"+tblEvalTsccomments.getCommentsId()+"' />");
        return sb.toString();
    }*/
    
    private boolean updateComments(HttpServletRequest request,EvalTscCommentsService evalTscCommentsService){
        boolean flag = false;
        int commentId = Integer.parseInt(request.getParameter("commentId"));
        String s_comments = request.getParameter("comments");
        flag = evalTscCommentsService.updateComments(commentId, s_comments);
        return flag;
    }
}
