/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.databean;

import com.cptu.egp.eps.model.table.TblDesignationMaster;
import com.cptu.egp.eps.service.serviceinterface.DesignationMasterService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.util.Date;

/**
 * <b>DesignationDtBean</b> <Description Goes Here> Nov 26, 2010 6:02:28 PM
 * @author Administrator
 */
public class DesignationDtBean {

    private int departmentId;
    private int designationId;
    private String designationName;
    private int gradeId;
    private String departmentName;
    private String gradeName;
    private int createdBy;
    private Date createdDate;
    private boolean dataExists=false;
    private String deptType;
    private byte gradeLevel;

    public byte getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(byte gradeLevel) {
        this.gradeLevel = gradeLevel;
    }

    public String getDeptType()
    {
        return deptType;
    }

    public void setDeptType(String deptType)
    {
        this.deptType = deptType;
    }

    public boolean isDataExists()
    {
        return dataExists;
    }

    public void setDataExists(boolean dataExists)
    {
        this.dataExists = dataExists;
    }

    public int getDepartmentId()
    {
        return departmentId;
    }

    public void setDepartmentId(int departmentId)
    {
        this.departmentId = departmentId;
    }

    public String getDepartmentName()
    {
        return departmentName;
    }

    public void setDepartmentName(String departmentName)
    {
        this.departmentName = departmentName;
    }

    public String getGradeName()
    {
        return gradeName;
    }

    public void setGradeName(String gradeName)
    {
        this.gradeName = gradeName;
    }

    public int getDesignationId()
    {
        return designationId;
    }

    public void setDesignationId(int designationId)
    {
        this.designationId = designationId;
    }

    public String getDesignationName()
    {
        return designationName;
    }

    public void setDesignationName(String designationName)
    {
        this.designationName = designationName;
    }

    public int getGradeId()
    {
        return gradeId;
    }

    public void setGradeId(int gradeId)
    {
        this.gradeId = gradeId;
    }

    public int getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(int createdBy)
    {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public void populateInfo(int designationId)
    {
        this.setDesignationId(designationId);
        DesignationMasterService designationMasterService=(DesignationMasterService) AppContext.getSpringBean("DesignationMasterService");
        TblDesignationMaster designationMaster=designationMasterService.getDesignationDetails(designationId);
        if(designationMaster!=null)
        {
            this.setDataExists(true);
            this.setDepartmentId(designationMaster.getTblDepartmentMaster().getDepartmentId());
            this.setDepartmentName(designationMaster.getTblDepartmentMaster().getDepartmentName());
            this.setDeptType(designationMaster.getTblDepartmentMaster().getDepartmentType());
            this.setGradeId(designationMaster.getTblGradeMaster().getGradeId());
            this.setGradeName(designationMaster.getTblGradeMaster().getGrade());
            this.setGradeLevel(designationMaster.getTblGradeMaster().getGradeLevel());
            this.setDesignationName(designationMaster.getDesignationName());
            this.setCreatedBy(designationMaster.getCreatedBy());
            this.setCreatedDate(designationMaster.getCreatedDate());
        }
    }

}
