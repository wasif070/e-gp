/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.model.table.TblCmsNewBankGuarnatee;
import com.cptu.egp.eps.service.serviceimpl.ConsolodateService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService;
import com.cptu.egp.eps.service.serviceinterface.CmsNewBankGuarnateeService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.DateUtils;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

// Dohatec Start
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
// Dohatec End

/**
 *
 * @author rikin.p
 */
public class BankGuaranteeServlet extends HttpServlet {

    static final Logger LOGGER = Logger.getLogger(BankGuaranteeServlet.class);
    static final String STARTS = " Starts";
    static final String ENDS = " Ends";
    private String contextPath = "";
    private CmsNewBankGuarnateeService cmsNewBankGuarnateeService = (CmsNewBankGuarnateeService) AppContext.getSpringBean("CmsNewBankGuarnateeService");
    private int lotId = 0;
    private int tenderId = 0;
    private BigDecimal amount = new BigDecimal(0);
    private String lastDateOfPayment = "";
    private int noOfDays = 0;
    private String lastDateOfValidity = "";
    private int uId = 0;
    private Date lastDate = null;
    private Date lastValidDate = null;
    private String txtComments = "";
    private String action = "";
    // Dohatec Start
    private String[] amountICT = new String[4];
    private String[] currencyName = new String[4];
    private boolean isICT = false;
    // Dohatec End
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
    private AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
    private UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
    private ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
    private CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        LOGGER.debug("processRequest : " + STARTS);
        HttpSession session = request.getSession();
        contextPath = request.getContextPath();
        String userId = "";
        String logUserId = "0";
        if (request.getSession().getAttribute("userId") != null) {
            logUserId = request.getSession().getAttribute("userId").toString();
            cmsConfigDateService.setLogUserId(logUserId);
            accPaymentService.setLogUserId(logUserId);
            commonservice.setUserId(logUserId);
            registerService.setUserId(logUserId);
        }
        String userTypeId = "";
        if (request.getSession().getAttribute("userTypeId") != null) {
            userTypeId = request.getSession().getAttribute("userTypeId").toString();
        }
        if (session.getAttribute("userId") != null) {
            userId = session.getAttribute("userId").toString();
            cmsNewBankGuarnateeService.setLogUserId(userId);
        }
        String procnature = commonservice.getProcNature(request.getParameter("hidTenderId")).toString();
        try {
            if (request.getParameter("hidLotId") != null) {
                lotId = Integer.parseInt(request.getParameter("hidLotId"));
            }
            if (request.getParameter("hidTenderId") != null) {
                tenderId = Integer.parseInt(request.getParameter("hidTenderId"));

                // Dohatec Start
                isICT = false;
                TenderCommonService objTCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> listDP = objTCS.returndata("chkDomesticPreference", String.valueOf(tenderId), null);
                if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                    isICT = true;
                }
                // Dohatec End
            }
            if (request.getParameter("dtEndDate") != null) {
                lastDateOfPayment = request.getParameter("dtEndDate");
                lastDate = DateUtils.StrtoDate(lastDateOfPayment);
            }
            
            // Dohatec Start
            if(isICT)
            {
                if(request.getParameterValues("txtAmount") != null)
                {
                    amountICT = request.getParameterValues("txtAmount");
                    currencyName = request.getParameterValues("currencyName");
                }
            }   // DohatecEnd
            else    // Old Version
            {
                if (request.getParameter("txtAmount") != null)
                {
                    amount = new BigDecimal(request.getParameter("txtAmount"));
                }
            }
            if (request.getParameter("dtValidity") != null) {
                lastDateOfValidity = request.getParameter("dtValidity");
                lastValidDate = DateUtils.StrtoDate(lastDateOfValidity);
            }
            if (request.getParameter("txtValidityDays") != null) {
                noOfDays = Integer.parseInt(request.getParameter("txtValidityDays"));
            }
            if (request.getParameter("txtComments") != null) {
                txtComments = request.getParameter("txtComments");
            }


            if (request.getParameter("hiduId") != null) {
                uId = Integer.parseInt(request.getParameter("hiduId"));
            }
            if (request.getParameter("action") != null) {
                action = request.getParameter("action");
            }


            if ("requestBankGuarantee".equalsIgnoreCase(action)) {

                // Dohatec Start
                if(isICT)
                {
                    int size = amountICT.length;
                    for(int i=0; i<size; i++)
                    {
                        TblCmsNewBankGuarnatee tblCmsNewBankGuarnatee = new TblCmsNewBankGuarnatee();
                        tblCmsNewBankGuarnatee.setTenderId(tenderId);
                        tblCmsNewBankGuarnatee.setPkgLotId(lotId);
                        tblCmsNewBankGuarnatee.setBgAmt(new BigDecimal(amountICT[i]));
                        tblCmsNewBankGuarnatee.setCreatedBy(Integer.parseInt(userId));
                        tblCmsNewBankGuarnatee.setCreatedDt(new java.util.Date());
                        tblCmsNewBankGuarnatee.setLastDtOfPayment(lastDate);
                        tblCmsNewBankGuarnatee.setUserId(uId);
                        tblCmsNewBankGuarnatee.setValidityDays(noOfDays);
                        tblCmsNewBankGuarnatee.setLastDtOfValidity(lastValidDate);
                        tblCmsNewBankGuarnatee.setRemarks(txtComments);
                        tblCmsNewBankGuarnatee.setCurrencyName(currencyName[i]);
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        int conId = c_ConsSrv.getContractId(tenderId);
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Request for New performance security", "");
                        cmsNewBankGuarnateeService.addBankGuarantee(tblCmsNewBankGuarnatee);
                    }
                }   // Dohatec End
                else    // Old version
                {
                    TblCmsNewBankGuarnatee tblCmsNewBankGuarnatee = new TblCmsNewBankGuarnatee();
                    tblCmsNewBankGuarnatee.setTenderId(tenderId);
                    tblCmsNewBankGuarnatee.setPkgLotId(lotId);
                    tblCmsNewBankGuarnatee.setBgAmt(amount);
                    tblCmsNewBankGuarnatee.setCreatedBy(Integer.parseInt(userId));
                    tblCmsNewBankGuarnatee.setCreatedDt(new java.util.Date());
                    tblCmsNewBankGuarnatee.setLastDtOfPayment(lastDate);
                    tblCmsNewBankGuarnatee.setUserId(uId);
                    tblCmsNewBankGuarnatee.setValidityDays(noOfDays);
                    tblCmsNewBankGuarnatee.setLastDtOfValidity(lastValidDate);
                    tblCmsNewBankGuarnatee.setRemarks(txtComments);
                    tblCmsNewBankGuarnatee.setCurrencyName(""); //  Added by Dohatec
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    int conId = c_ConsSrv.getContractId(tenderId);
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer")), conId, "contractId", EgpModule.Payment.getName(), "Request for New performance security", "");
                    cmsNewBankGuarnateeService.addBankGuarantee(tblCmsNewBankGuarnatee);
                }

                sendMailForBankGuaranteeRequest(Integer.toString(tenderId), Integer.toString(lotId), logUserId, Integer.parseInt(userTypeId));
                if("services".equalsIgnoreCase(procnature))
                {
                    response.sendRedirect(contextPath + "/officer/InvoiceServiceCase.jsp?tenderId=" + tenderId);
                }else{
                    response.sendRedirect(contextPath + "/officer/Invoice.jsp?tenderId=" + tenderId);
                }    
            }

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /** send mail notification to bank and tenderer on requesting the bank guarantee by PE */
    private boolean sendMailForBankGuaranteeRequest(String tenderId, String lotId, String logUserId, int userTypeId) {
        boolean flag = false;

        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId), Integer.parseInt(lotId));
        Object[] obj = null;
        if (!GetCinfoBar.isEmpty()) {
            obj = GetCinfoBar.get(0);
            String peName = "";
            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(logUserId));
            if (peNameObj.get(0) != null) {
                peName = peNameObj.get(0).toString();
            }
            String strFrom = commonservice.getEmailId(logUserId);
            List<Object[]> AcEmailId = accPaymentService.getAccountantEmailId(Integer.parseInt(tenderId));
            String[] strTo = null;
            int emailIndex = 0;
            if (userTypeId == 3) {
                strTo = new String[AcEmailId.size() + 1];
                for (Object[] emailId : AcEmailId) {
                    strTo[emailIndex] = emailId[4].toString();
                    emailIndex++;
                }
                strTo[emailIndex] = obj[9].toString();
            }
            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            sendMessageUtil.setEmailFrom(strFrom);

            MailContentUtility mailContentUtility = new MailContentUtility();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            String emailId[] = new String[1];
            String mailSubject = "e-GP : Request For New Performance Security";
            String mobileno = "";
            for (int i = 0; i < strTo.length; i++) {
                emailId[0] = strTo[i];
                registerService.contentAdmMsgBox(emailId[0], strFrom, mailSubject, msgBoxContentUtility.mailForBankGuaranteeRequest(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0]))));
                String mailText = mailContentUtility.mailForBankGuaranteeRequest(c_isPhysicalPrComplete, peName, obj, accPaymentService.getUserTypeId(emailId[0]), Integer.toString(userTypeId), accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailId[0])));
                sendMessageUtil.setEmailTo(emailId);
                sendMessageUtil.setEmailSub(mailSubject);
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                if ("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else if ("7".equalsIgnoreCase(accPaymentService.getUserTypeId(emailId[0]))) {
                    mobileno = accPaymentService.getBankUserMobileNowithCountryCode(accPaymentService.getUserId(emailId[0]));
                } else {
                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailId[0]));
                }
                sendMessageUtil.setSmsNo(mobileno);
                StringBuilder sb = new StringBuilder(); 
                sb.append("Dear User,%0AThis is to inform you that PE has requested for New Performance Security ");
                sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                sb.append("%0AThanks,%0AeGP System");
                sendMessageUtil.setSmsBody(sb.toString());
                if (!"".equalsIgnoreCase(mobileno) && mobileno != null) {
                    sendMessageUtil.sendSMS();
                }
            }
            strTo = null;
            sendMessageUtil = null;
            mailContentUtility = null;
            msgBoxContentUtility = null;
        }
        return flag;
    }
}
