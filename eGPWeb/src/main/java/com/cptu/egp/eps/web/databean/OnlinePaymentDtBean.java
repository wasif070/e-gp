/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.databean;

/**
 *
 * @author nishit
 */
public class OnlinePaymentDtBean {
    private String emailId;
    private String amt;
    private String fromWhere;
    private String tenderId;
    private String serviceCharge;
    private String total;
    private int onlineId;
    private String onlineTraId;
    private String cardNumber;
    private String response;
    private String dateTime;
    private boolean showRespMsg=true;
    private String payeeName;
    private String payeeAddress;
    private String paymentDesc;
    private String remarks="OnLine_Payment_For_Value_Added_Services.";
    private String paymentGateway;

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
    
    public OnlinePaymentDtBean(int onlineId) {
        this.onlineId = onlineId;
    }

    public OnlinePaymentDtBean(int onlineId, String onlineResp, String onlineMsg, String onlineTraId) {
        this.onlineId = onlineId;
        this.onlineTraId = onlineTraId;
    }

    public OnlinePaymentDtBean(String emailId, String amt, String fromWhere, String tenderId, String serviceCharge) {
        this.emailId = emailId;
        this.amt = amt;
        this.fromWhere = fromWhere;
        this.tenderId = tenderId;
        this.serviceCharge = serviceCharge;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }
    
    public String getAmt() {
        return amt;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getFromWhere() {
        return fromWhere;
    }

    public String getTenderId() {
        return tenderId;
    }

    public String getServiceCharge() {
        return serviceCharge;
    }

    public String getTotal() {
        return String.valueOf(Double.parseDouble(amt) + (Double.parseDouble(amt)*Double.parseDouble(serviceCharge))/100);
    }

    public String getOnlineTraId() {
        return onlineTraId;
    }

    

    public int getOnlineId() {
        return onlineId;
    }

    public void setOnlineId(int onlineId) {
        this.onlineId = onlineId;
    }
    public void setOnlineTraId(String onlineTraId) {
        this.onlineTraId = onlineTraId;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @return the showRespMsg
     */
    public boolean isShowRespMsg() {
        return showRespMsg;
    }

    /**
     * @param showRespMsg the showRespMsg to set
     */
    public void setShowRespMsg(boolean showRespMsg) {
        this.showRespMsg = showRespMsg;
    }
    public String getPayeeAddress() {
        return payeeAddress;
    }

    public void setPayeeAddress(String payeeAddress) {
        this.payeeAddress = payeeAddress;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getPaymentDesc() {
        return paymentDesc;
    }

    public void setPaymentDesc(String paymentDesc) {
        this.paymentDesc = paymentDesc;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}