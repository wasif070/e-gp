/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.webservices;

import com.cptu.egp.eps.dao.storedprocedure.CommonDebarredTendererDetails;
import com.cptu.egp.eps.service.serviceinterface.DebarredTenderersInfoService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.WebServiceUtil;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import org.apache.log4j.Logger;

/**
 * web service for sharing debarred tenderer's detail.
 * @author sreenu
 */
@WebService()
@XmlSeeAlso({CommonDebarredTendererDetails.class})
public class DebarredTenderers {

    private static final Logger LOGGER = Logger.getLogger(DebarredTenderers.class);
    private String logUserId = "0";
    private static final int WEB_SERVICE_ID = 3;
    private static final String SEARCH_SERVICE_NAME = "DebarredTenderersInfoService";

    /**
     * 
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    /***
     * This method returns the List of String objects of the <b>Debarred Tenderer Details</b>  and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * The order as below<br>
     * 1.Name of Tenderer/Consultant,<br>
     * 2.Debarred at (Single tender,Package,Project,PE,Organization,e-GP System),<br>
     * 3.Debarred by,<br>
     * 4.DebarredTenderers Date,<br>
     * 5.Reason,<br>
     * 6.DebarredTenderers Period,<br>
     * 7.DebarredTenderId<br>
     *
     * @param String username
     * @param String password
     * @param Integer pageNumber
     * @param Integer recordSize
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getDebarredTenderersInfo")
    public WSResponseObject getDebarredTenderersInfo(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password /*,
            @WebParam(name = "pageNumber") Integer pageNumber,   Commented by Sudhir 08072011 for no need pagination concept.
            @WebParam(name = "recordSize") Integer recordSize */) {
        LOGGER.debug("getDebarredTenderersInfo : " + logUserId + WSResponseObject.LOGGER_START);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = "";
        Integer pageNumber = 1;
        Integer recordSize = 50000;    // Added by Sudhir 08072011 for no need pagination concept.
        //insert record in log table
        WebServiceUtil.insertLogRecord(WEB_SERVICE_ID, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WEB_SERVICE_ID,
                SEARCH_SERVICE_NAME, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> debarmentDetailsSubList = null;
            try {
                debarmentDetailsSubList = getDebarredTenderersList(pageNumber, recordSize);
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getDebarredTenderersInfo : " + logUserId + e);
            }
            if (debarmentDetailsSubList != null) {
                if (debarmentDetailsSubList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(debarmentDetailsSubList);
                }
            }
        }
        LOGGER.debug("getDebarredTenderersInfo : " + logUserId + WSResponseObject.LOGGER_END);
        return wSResponseObject;
    }

    /***
     * This method returns the required list for response object.
     * @param pageNumber
     * @param recordSize
     * @return List<CommonDebarredTendererDetails>
     */
    private List<String> getDebarredTenderersList(int pageNumber, int recordSize)
            throws Exception {
        LOGGER.debug("getDebarredTenderersList : " + logUserId + WSResponseObject.LOGGER_START);
        List<String> debarmentDetailsSubList = new ArrayList<String>();
        List<String> allDebarmentDetailsList = getAllDebarredTenderersInfo();
        int totalSize = allDebarmentDetailsList.size();
        int endRecord = pageNumber * recordSize;
        int noOfPages = totalSize / recordSize;
        int startRecord = endRecord - recordSize;
        if (endRecord > totalSize) {
            endRecord = totalSize;
        }
        if (totalSize > noOfPages * recordSize) {
            noOfPages = noOfPages + 1;
        }
        if (pageNumber <= noOfPages) {
            debarmentDetailsSubList = allDebarmentDetailsList.subList(startRecord, endRecord);
        }
        LOGGER.debug("getDebarredTenderersList : " + logUserId + WSResponseObject.LOGGER_END);
        return debarmentDetailsSubList;
    }

    /***
     * This method returns all the details of Debarred tenderers. 
     * @return List<CommonDebarredTendererDetails>
     */
    private List<String> getAllDebarredTenderersInfo() throws Exception {
        LOGGER.debug("getAllDebarredTenderersInfo : " + logUserId + WSResponseObject.LOGGER_START);
        List<String> reqStringList = new ArrayList<String>();
        DebarredTenderersInfoService debarredTenderersInfoService =
                (DebarredTenderersInfoService) AppContext.getSpringBean(SEARCH_SERVICE_NAME);
        List<CommonDebarredTendererDetails> debarmentDetailsList =
                debarredTenderersInfoService.getAllDebartmentDetails();
        if (debarmentDetailsList != null) {
            for (CommonDebarredTendererDetails debarredTendererDetails : debarmentDetailsList) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(debarredTendererDetails.getCompanyName());
                stringBuilder.append(WSResponseObject.APPEND_CHAR);
                stringBuilder.append(debarredTendererDetails.getDebarredAt());
                stringBuilder.append(WSResponseObject.APPEND_CHAR);
                stringBuilder.append(debarredTendererDetails.getDebarredBy());
                stringBuilder.append(WSResponseObject.APPEND_CHAR);
                stringBuilder.append(debarredTendererDetails.getDebarmentDate());
                stringBuilder.append(WSResponseObject.APPEND_CHAR);
                stringBuilder.append(debarredTendererDetails.getReason());
                stringBuilder.append(WSResponseObject.APPEND_CHAR);
                stringBuilder.append(debarredTendererDetails.getDebarmentPeriod());
                stringBuilder.append(WSResponseObject.APPEND_CHAR);
                stringBuilder.append(debarredTendererDetails.getOfficeId());

                //stringBuilder.append(WSResponseObject.APPEND_CHAR);  Commended by Sudhir 08072011
                reqStringList.add(stringBuilder.toString());
            }
        }
        LOGGER.debug("getAllDebarredTenderersInfo : " + logUserId + WSResponseObject.LOGGER_END);
        return reqStringList;
    }
}
