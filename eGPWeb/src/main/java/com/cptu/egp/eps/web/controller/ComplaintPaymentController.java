/**
 * 
 */
package com.cptu.egp.eps.web.controller;

/**
 * @author rajanikanth.kotvali
 *
 */
import java.lang.Integer;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import java.util.*;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

import java.text.*;
import com.cptu.egp.eps.model.table.TblComplaintMaster;
import com.cptu.egp.eps.service.serviceinterface.ComplaintMgmtService;
import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.service.serviceimpl.UserTypeServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.UserRegisterServiceImpl;
import com.cptu.egp.eps.model.table.TblReviewPanel;
import com.cptu.egp.eps.model.table.TblTenderDetails;
import com.cptu.egp.eps.model.table.TblComplaintPayments;
import com.cptu.egp.eps.model.table.TblLoginMaster;
import com.cptu.egp.eps.model.table.TblPartnerAdmin;
import com.cptu.egp.eps.dao.storedprocedure.SearchComplaintPaymentBean;
import org.springframework.context.ApplicationContext;
import com.cptu.egp.eps.dao.generic.ComplaintMgmtConstants;
import com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData;
import com.cptu.egp.eps.model.table.TblComplaintLevel;
import com.cptu.egp.eps.service.serviceimpl.TenderCommonService;
import com.cptu.egp.eps.service.serviceinterface.AccPaymentService;
import com.cptu.egp.eps.service.serviceinterface.CommonService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ComplaintPaymentController {

    final static Logger log = Logger.getLogger(ComplaintPaymentController.class);
    private static final String destinationDir = ComplaintMgmtConstants.UPLOAD_PATH;
    private ComplaintMgmtService mgmtService;
    private UserTypeServiceImpl userTypeService;
    private UserRegisterServiceImpl tendererMasterService;
    private ApplicationContext context;
    private TenderCommonService tenderCommonService;
    private CommonService commonservice;
    private UserRegisterService registerService;
    private AccPaymentService accPaymentService;
    private MakeAuditTrailService AuditTrail;

    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    @Autowired
    public void setMgmtService(ComplaintMgmtService mgmtService) {
        this.mgmtService = mgmtService;

    }

    @Autowired
    public void setUserTypeService(UserTypeServiceImpl userTypeService) {
        this.userTypeService = userTypeService;

    }

    @Autowired
    public void settendererMasterService(UserRegisterServiceImpl tendererMasterService) {
        this.tendererMasterService = tendererMasterService;

    }

    @Autowired
    public void setAccPaymentService(AccPaymentService accPaymentService) {
        this.accPaymentService = accPaymentService;
    }

    @Autowired
    public void setCommonservice(CommonService commonservice) {
        this.commonservice = commonservice;
    }

    @Autowired
    public void setRegisterService(UserRegisterService registerService) {
        this.registerService = registerService;
    }

    @Autowired
    public void setTenderCommonService(TenderCommonService tenderCommonService) {
        this.tenderCommonService = tenderCommonService;
    }

    @Autowired
    public void setAuditTrail(MakeAuditTrailService AuditTrail) {
        this.AuditTrail = AuditTrail;
    }

    /*
     * branch maker flow starts here. on click of Complaint Registration Fee under Payment menu from menu bar.(searchFirstForComplaintPayments.htm)
     * the first page is search_tenderer_for_registration.jsp with no results.
     * On click of search button, page navigates to page with complaint details (searchForComplaintPayments.htm)
     * on click of make payment, page navigates to complaint_fee_payment.jsp(complaintFeePayment).
     * on click of submit button page navigates to
     */
// to view search page by branch maker on selection of Complaint Registration Payments /Complaint Security Payments
    @RequestMapping(value = "/resources/common/searchFirstForComplaintPayments.htm")
    public ModelAndView complaintPayment(HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        ModelAndView compay;
        try {

            List<TblPartnerAdmin> lstPartnerAdmin = new ArrayList<TblPartnerAdmin>();
            TblPartnerAdmin partnerAdmin = null;
            List<SearchComplaintPaymentBean> lstComplaints = new ArrayList<SearchComplaintPaymentBean>();
            String uid = session.getAttribute("userId").toString();
            String feetype = req.getParameter("feetype");
            if (req.getParameter("verify") != null && req.getParameter("verify").equals("verify")) {
                compay = new ModelAndView("../partner/search_complaints_payments_verify");
                lstPartnerAdmin = mgmtService.getTblPartnerAdmin(Integer.parseInt(uid), true);
                if (lstPartnerAdmin != null && lstPartnerAdmin.size() >= 1) {

                    compay.addObject("partnerAdmin", lstPartnerAdmin);
                }

                session.setAttribute("verify", "verify");

            } else {
                session.setAttribute("verify", null);
                compay = new ModelAndView("../partner/search_complaint_payments");
//                lstComplaints = mgmtService.searchComplaintPendingPayments("pending", feetype);
//                if (lstComplaints != null && lstComplaints.size() != 0) {
//                    log.error("GETTING COMPLAINTS LIST FOR BRANCH MAKER......COMPLAINTS_SIZE:" + lstComplaints.size());
//                    compay.addObject("cmppy", lstComplaints);
//                }

            }

            log.debug("fee type after login top.jsp " + feetype);
            session.setAttribute("feetype", feetype);

            return compay;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: complaintPayment(LINE:105) :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    @RequestMapping(value = "/resources/common/dispTenderer.htm")
    public ModelAndView getInfoOfTenderer(HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        ModelAndView compay;
        try {
            String emailId = req.getParameter("txtEmailId");
            compay = new ModelAndView("");
            return compay;
        } catch (Exception e) {
            log.error("" + e);
            return new ModelAndView("/StackTrace");
        }
    }

// 	search based on the emailid, from date, to date (bottom search)
    @RequestMapping(value = "/resources/common/searchForComplaintPayments.htm")
    public ModelAndView searchComplaintPayments(HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        ModelAndView compay;
        try {

            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            List<TblPartnerAdmin> lstPartnerAdmin = new ArrayList<TblPartnerAdmin>();
            TblPartnerAdmin partnerAdmin = null;
            String uid = session.getAttribute("userId").toString();
            boolean verifyFlag = false;
            String firstSearchByEmail = null;
            int createdBy = 0;
            int tenderId = 0;
            String tenderer = null;
            Date fromDate = null;
            Date toDate = null;
            String fromDt = null, toDt = null;
            String paymentFor = (String) session.getAttribute("feetype");
            String emailId = null;
            List<SearchComplaintPaymentBean> lstComplaints = new ArrayList<SearchComplaintPaymentBean>();
            List<SearchComplaintPaymentBean> lstComplaintsEmailSearch = new ArrayList<SearchComplaintPaymentBean>();
            List<SearchComplaintPaymentBean> lstComplaintsVerify = new ArrayList<SearchComplaintPaymentBean>();

            /// for email search
			/*int createdBy = Integer.parseInt(session.getAttribute("userId").toString());
            String paymentFor = (String) session.getAttribute("feetype");
            String emailId = req.getParameter("txtEmailId");*/
            /// for email search

            String verificationStatus = null;
            log.debug("IN SEARCH CHECKING FOR NULL VALUES STARTS..........");
            log.debug("VALUES................................");

            if (req.getParameter("txtEmailId") != null && !"".equalsIgnoreCase(req.getParameter("txtEmailId"))) {
                log.debug("FIRST EmailId :" + req.getParameter("txtEmailId"));
                compay = new ModelAndView("../partner/search_complaint_payments");
                verifyFlag = true;
                emailId = req.getParameter("txtEmailId");
                createdBy = Integer.parseInt(session.getAttribute("userId").toString());
                lstComplaintsEmailSearch = mgmtService.searchComplaintPayments(emailId, verificationStatus, fromDt, toDt, createdBy, paymentFor, verifyFlag, tenderId, tenderer);
                log.debug("total no of lstComplaintsEmailSearch in payment are" + lstComplaintsEmailSearch.size());
                if (lstComplaintsEmailSearch != null && lstComplaintsEmailSearch.size() != 0) {
                    compay.addObject("complaintsByEmail", lstComplaintsEmailSearch);
                }
            } else {

                if (req.getParameter("txtSearchEmailId") != null && !"".equalsIgnoreCase(req.getParameter("txtSearchEmailId"))) {
                    log.debug("SECOND EmailId :" + req.getParameter("txtSearchEmailId"));
                    emailId = req.getParameter("txtSearchEmailId");
                }
                if (req.getParameter("txtFromDt") != null && !"".equalsIgnoreCase(req.getParameter("txtFromDt"))) {
                    log.debug("FROM DATE :" + req.getParameter("txtFromDt"));
                    fromDate = df.parse(req.getParameter("txtFromDt"));
                    fromDt = df1.format(fromDate);
                }
                if (req.getParameter("txtToDt") != null && !"".equalsIgnoreCase(req.getParameter("txtToDt"))) {
                    log.debug("TO DATE :" + req.getParameter("txtToDt"));
                    toDate = df.parse(req.getParameter("txtToDt"));
                    toDt = df1.format(toDate);
                }
                if (req.getParameter("comboStatus") != null && !"select".equalsIgnoreCase(req.getParameter("comboStatus"))) {
                    log.debug("CPMPLAINT STATUS :" + req.getParameter("comboStatus"));
                    verificationStatus = req.getParameter("comboStatus");
                }
                if (req.getParameter("txtTenderId") != null && !req.getParameter("txtTenderId").equalsIgnoreCase("")) {
                    log.debug("TENDER ID :" + req.getParameter("txtTenderId"));
                    tenderId = Integer.parseInt(req.getParameter("txtTenderId"));
                }
                if (req.getParameter("txtTendererNm") != null && !req.getParameter("txtTendererNm").equalsIgnoreCase("")) {
                    log.debug("TENDERER NAME :" + req.getParameter("txtTendererNm"));
                    tenderer = req.getParameter("txtTendererNm");
                }
                if (req.getParameter("verify") != null && req.getParameter("verify").equals("verify")) {
                    compay = new ModelAndView("../partner/search_complaints_payments_verify");
                    log.debug("PAYMENT VERIFICATION BY BRANCH CHECKER..............");
                    verifyFlag = true;
                    if (req.getParameter("comboBranchMembers") != null && !req.getParameter("comboBranchMembers").equals("")) {
                        createdBy = Integer.parseInt(req.getParameter("comboBranchMembers"));
                        log.debug("BRANCH MAKERS ID:" + createdBy);
                    }
                    lstPartnerAdmin = mgmtService.getTblPartnerAdmin(Integer.parseInt(uid), true);
                    if (lstPartnerAdmin != null && lstPartnerAdmin.size() >= 1) {

                        compay.addObject("partnerAdmin", lstPartnerAdmin);
                    }
                    lstComplaintsVerify = mgmtService.searchComplaintPayments(emailId, verificationStatus, fromDt, toDt, createdBy, paymentFor, verifyFlag, tenderId, tenderer);
                    if (lstComplaintsVerify != null && lstComplaintsVerify.size() != 0) {
                        log.debug("GETTING COMPLAINTS LIST FOR BRANCH CHECKER......COMPLAINTS_SIZE:" + lstComplaintsVerify.size());
                        compay.addObject("cmppy", lstComplaintsVerify);
                    }
                    // for branch maker search results for payment
                } else {
                    compay = new ModelAndView("../partner/search_complaint_payments");
                    //log.error("verifyFlag in Contrroller false :"+req.getParameter("verify"));
                    verifyFlag = false;

                    lstComplaints = mgmtService.searchComplaintPayments(emailId, verificationStatus, fromDt, toDt, createdBy, paymentFor, verifyFlag, tenderId, tenderer);
                    if (lstComplaints != null && lstComplaints.size() != 0) {
                        log.debug("GETTING COMPLAINTS LIST FOR BRANCH MAKER......COMPLAINTS_SIZE:" + lstComplaints.size());
                        compay.addObject("cmppy", lstComplaints);
                    }

                }
                log.debug("VALUES................................");
                log.debug("IN SEARCH CHECKING FOR NULL VALUES ENDS..........");

            }
            return compay;

        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: searchComplaintPayments(LINE:143) :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

//	With branch maker, for new payments, click on MakePayment
    @RequestMapping(value = "/resources/common/complaintFeePayment.htm")
    public ModelAndView complaintFeePayment(HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        ModelAndView mov = null;
        try {

            List<TblPartnerAdmin> lstPartnerAdmin = new ArrayList<TblPartnerAdmin>();
            TblPartnerAdmin partnerAdmin = null;
            TblComplaintPayments payment = null;
            String paymentId = null;
            String complId = null;
            String emailId = null;
            String verify = null;

            if (req.getParameter("complaintId") != null) {
                log.debug("complaint Id " + complId);
                complId = req.getParameter("complaintId");
                session.setAttribute("complId", complId);

            }
            MailContentUtility utility = new MailContentUtility();
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            if (req.getParameter("emailId") != null) {
                log.debug("email id " + req.getParameter("emailId"));
                emailId = req.getParameter("emailId");
                session.setAttribute("emailId", emailId);
                req.setAttribute("emailId", emailId);
            }
            if (req.getParameter("complaintPaymentId") != null && Integer.parseInt(req.getParameter("complaintPaymentId")) > 0) {
                paymentId = req.getParameter("complaintPaymentId");

                List<TblComplaintPayments> payments = mgmtService.findComplaintPayments("complaintPaymentId", Operation_enum.EQ, Integer.parseInt(paymentId));
                log.debug("this is retrieving the details based on complaint" + payments.size());
                if (payments != null && payments.size() >= 1) {
                    payment = payments.get(0);

                    if (req.getParameter("verify") != null && req.getParameter("verify").equals("verify")) {
                        log.debug("In Controller for verify payment by Branch checker>>>>>>>>");
                        mov = new ModelAndView("../partner/verify_complaint_fee_payment");

                    } else {
                        log.debug("In controller for Edit Payment by Branch Maker>>>>>>>>");
                        mov = new ModelAndView("../partner/complaint_fee_payment");
                    }
                    lstPartnerAdmin = mgmtService.getTblPartnerAdmin(payment.getCreatedBy(), false);
                    if (lstPartnerAdmin != null && lstPartnerAdmin.size() >= 1) {
                        partnerAdmin = lstPartnerAdmin.get(0);
                        mov.addObject("partnerAdmin", partnerAdmin);
                    }
                    req.setAttribute("complaintFeeType", payment.getPaymentFor());
                    mov.addObject("payment", payment);
                }

            } else {
                log.debug("In controller for Make Payment by Branch Maker>>>>>>>>");
                mov = new ModelAndView("../partner/complaint_fee_payment");
                req.setAttribute("complaintFeeType", (String) session.getAttribute("feetype"));
                log.debug("complaintFeeType = " + (String) session.getAttribute("feetype"));
            }

            // session.setAttribute("paymentId",paymentId);

            log.debug("this is registration page on click of make payment link&&&&&&&&&&&&&&&&&&&&&&&");
            // mov.addObject("payment",payments);
            return mov;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: complaintFeePayment(LINE:262) :" + e);
            return new ModelAndView("/StackTrace");
        }

    }

// adding the complaint record to table TblComplaintPayment by click on submtit
    @RequestMapping(value = "/resources/common/complaintFeePaymentSave.htm")
    public ModelAndView complaintFeePaymentSave(HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        ModelAndView mov = new ModelAndView("../partner/view_complaint_payment_details");
        try {
            List<TblLoginMaster> lstLoginUser = new ArrayList<TblLoginMaster>();
            List<TblPartnerAdmin> lstPartnerAdmin = new ArrayList<TblPartnerAdmin>();
            TblLoginMaster loginUser = null;
            TblPartnerAdmin partnerAdmin = null;
            String message = "";
            String paymentFor = null;
            //retrieving the values from complaint_fee_payment.jsp.jsp
            String branchName = "";
            String bankName = "";
            String remarks = "";
            String branchMaker = "";
            String currency = "";
            String amount = null;
            String paymentMode = "";
            String instrNo = "";
            String issuingBank = "";
            String issuingBankBranch = "";
            String issuanceDate = "";
            String validDate = "";
            String payBranchName = "";
            String paymentDt = "";
            String complId = null;
            String emailId = null;
            TblComplaintPayments payment = null;

            if (req.getParameter("hdnBankNm") != null && !req.getParameter("hdnBankNm").equalsIgnoreCase("")) {
                bankName = req.getParameter("hdnBankNm");
            }
            if (req.getParameter("hdnBranch") != null && !req.getParameter("hdnBranch").equalsIgnoreCase("")) {
                branchName = req.getParameter("hdnBranch");
            }
            if (req.getParameter("hdnBranchMaker") != null && !req.getParameter("hdnBranchMaker").equalsIgnoreCase("")) {
                branchMaker = req.getParameter("hdnBranchMaker");
            }
            if (req.getParameter("cmbCurrency") != null && !req.getParameter("cmbCurrency").equalsIgnoreCase("")) {
                currency = req.getParameter("cmbCurrency");
            }
            if (req.getParameter("hdnAmount") != null && !req.getParameter("hdnAmount").equalsIgnoreCase("")) {
                amount = req.getParameter("hdnAmount");

            }
            if (req.getParameter("cmbPayment") != null && !req.getParameter("cmbPayment").equalsIgnoreCase("")) {
                paymentMode = req.getParameter("cmbPayment");
            }
            if (req.getParameter("txtInsRefNo") != null && !req.getParameter("txtInsRefNo").equalsIgnoreCase("")) {
                instrNo = req.getParameter("txtInsRefNo");
            }
            if (req.getParameter("txtIssuanceBankNm") != null && !req.getParameter("txtIssuanceBankNm").equalsIgnoreCase("")) {
                issuingBank = req.getParameter("txtIssuanceBankNm");
            }
            if (req.getParameter("txtIssuanceBranch") != null && !req.getParameter("txtIssuanceBranch").equalsIgnoreCase("")) {
                issuingBankBranch = req.getParameter("txtIssuanceBranch");
            }
            if (req.getParameter("txtIssuanceDt") != null && !req.getParameter("txtIssuanceDt").equalsIgnoreCase("")) {
                issuanceDate = req.getParameter("txtIssuanceDt");

            }
            if (req.getParameter("txtValidityDt") != null && !req.getParameter("txtValidityDt").equalsIgnoreCase("")) {
                validDate = req.getParameter("txtValidityDt");
            }
            if (req.getParameter("payBranchName") != null && !req.getParameter("payBranchName").equalsIgnoreCase("")) {
                payBranchName = req.getParameter("payBranchName");
            }
            if (req.getParameter("hdnDateOfPayment") != null && !req.getParameter("hdnDateOfPayment").equalsIgnoreCase("")) {
                paymentDt = req.getParameter("hdnDateOfPayment");
            }
            if (req.getParameter("txtComments") != null && !req.getParameter("txtComments").equalsIgnoreCase("")) {
                remarks = req.getParameter("txtComments");
            }

            if (session.getAttribute("complId") != null) {
                complId = (String) session.getAttribute("complId");
            }

            if (req.getParameter("hdnPaymentFor") != null && !req.getParameter("hdnPaymentFor").equalsIgnoreCase("")) {
                paymentFor = req.getParameter("hdnPaymentFor");
            } else {
                paymentFor = (String) session.getAttribute("feetype");
            }
            String uid = session.getAttribute("userId").toString();
            log.debug(" Financial Institute = " + bankName + " BranchName = " + branchName + " BranchMaker = " + branchMaker + " Currency = " + currency + " Amount = " + amount + " PaymentMode = " + paymentMode + " Instrument No = " + instrNo + " IssuingBank = " + issuingBank + " IssuingBankBranch = " + issuingBankBranch + " IssuanceDate = " + issuanceDate + " ValidDate = " + validDate + " PayBranchName = " + payBranchName + " PaymentDt = " + paymentDt + " PaymentFor = " + paymentFor + " Complaint id = " + complId + " Remarks = " + remarks + " Amount = " + amount + " User = " + uid);

            TblComplaintMaster master = mgmtService.getComplaintDetails(Integer.parseInt(complId));
            log.debug("this is retrieving the details based on complaint^^^^^^^^^^^^^^^^^^^^^^^" + complId);
            MailContentUtility utility = new MailContentUtility();
            SendMessageUtil sendMessageUtil = new SendMessageUtil();

            // to check for action(MakePayment or EditPayment)
            if (req.getParameter("complaintPaymentId") != null && !("").equalsIgnoreCase(req.getParameter("complaintPaymentId"))) {

                log.debug("In edit Action of Controller>>>>>>>>>");
                String paymentId = req.getParameter("complaintPaymentId");

                log.debug("payment id is : " + paymentId);

                List<TblComplaintPayments> payments = mgmtService.findComplaintPayments("complaintPaymentId", Operation_enum.EQ, Integer.parseInt(paymentId));

                if (payments != null && payments.size() >= 1) {
                    payment = payments.get(0);
                    payment.setComments(remarks);
                    //Update Complaint Payment Details for branch checker
                    if (session.getAttribute("verify") != null) {
                        payment.setIsVerified("Yes");
                        AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), payment.getComplaintMaster().getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), "Verified Payment for " + paymentFor, "");
                        String msgText = utility.sendMailToTenderernakerChecker(complId, payment.getBankName(), "");
                        String[] strTo = {commonservice.getEmailId(payment.getComplaintMaster().getTendererMaster().getTblLoginMaster().getUserId() + "")};
                        String strFrom = commonservice.getEmailId(req.getSession().getAttribute("userId").toString());
                        String cc = "";
                        String subject = "e-GP:  verified Payment for Complaint " + paymentFor;
                        registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
                        sendMessageUtil.setEmailTo(strTo);
                        sendMessageUtil.setEmailFrom(strFrom);
                        sendMessageUtil.setEmailSub(subject);
                        sendMessageUtil.setEmailMessage(msgText);
                        sendMessageUtil.sendEmail();
                        sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
                        //sendMessageUtil.setSmsNo("919427410774");
                        StringBuilder sb = new StringBuilder();
                        sb.append("Dear User,%0AThis is to inform you that Financial Institute " + payment.getBankName() + " has verified payment for Complaint No. " + payment.getComplaintMaster().getComplaintId());
                        sb.append("%0AThanks,%0Ae-GP System");
                        sendMessageUtil.setSmsBody(sb.toString());
                        sendMessageUtil.sendSMS();
                        strTo = null;

                        log.debug("Update Complaint Payment Details for branch checker verification>>>>>>>" + session.getAttribute("verify"));
                        //checkComplaintProcessFeeStatus(String[] feetypes, int complaintId);
                    } else {
                        Date dtAction = new Date();
                        payment.setDtOfAction(dtAction);
                    }
                    // adding Branch Maker details
                    lstPartnerAdmin = mgmtService.getTblPartnerAdmin(payment.getCreatedBy(), false);
                    if (lstPartnerAdmin != null && lstPartnerAdmin.size() >= 1) {
                        partnerAdmin = lstPartnerAdmin.get(0);
                        mov.addObject("partnerAdmin", partnerAdmin);
                    }
                    mgmtService.updateComplaintPayment(payment);

                } else {
                    message = "";
                    log.debug("Message  no payments = " + message);
                }
            } else {
                log.debug("In Save Action of Controller>>>>>>>>>");
                Date currentDate = new Date();

                BigDecimal dblAmount = new BigDecimal(Double.parseDouble(amount));
                // setting the values to tblComplaintPayments table
                payment = new TblComplaintPayments();
                payment.setComplaintMaster(master);
                payment.setPaymentFor(paymentFor);
                payment.setPaymentInstType(ComplaintMgmtConstants.PAYMENT_INST_TYPE);
                payment.setInstRefNumber(instrNo);
                payment.setAmount(dblAmount);
                payment.setInstDate(currentDate);
                payment.setInstValidUpto(currentDate);
                payment.setInstValidityDt(currentDate);
                payment.setBankName(bankName);
                payment.setBranchName(branchName);
                payment.setComments(remarks);
                payment.setCreatedBy(Integer.parseInt(uid));
                payment.setCreatedDate(currentDate);
                payment.setStatus(ComplaintMgmtConstants.COMP_PAY_STATUS_PAID);
                payment.setPaymentMode(paymentMode);
                payment.setCurrency(currency);
                payment.setIssuanceBank(issuingBank);
                payment.setIssuanceBranch(issuingBankBranch);
                payment.setIsVerified(ComplaintMgmtConstants.NO);
                payment.setIsLive(ComplaintMgmtConstants.YES);
                payment.setDtOfAction(currentDate);
                payment.setPartTransId(123);
                // adding Branch Maker details
                lstPartnerAdmin = mgmtService.getTblPartnerAdmin(Integer.parseInt(uid), false);
                if (lstPartnerAdmin != null && lstPartnerAdmin.size() >= 1) {
                    partnerAdmin = lstPartnerAdmin.get(0);
                    mov.addObject("partnerAdmin", partnerAdmin);
                }
                mgmtService.addComplaintPayment(payment);

                if (!"".equalsIgnoreCase(branchMaker)) {
                    AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), payment.getComplaintMaster().getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), "Payment made for " + paymentFor, "");
                    String msgText = utility.sendMailToTenderernakerChecker(complId, payment.getBankName(), "maker");

                    String[] strTo = {commonservice.getEmailId(payment.getComplaintMaster().getTendererMaster().getTblLoginMaster().getUserId() + "")};
                    String strFrom = commonservice.getEmailId(req.getSession().getAttribute("userId").toString());
                    String cc = "";
                    String subject = "e-GP:  Received Payment for Complaint " + paymentFor;
                    registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
                    sendMessageUtil.setEmailTo(strTo);
                    sendMessageUtil.setEmailFrom(strFrom);
                    sendMessageUtil.setEmailSub(subject);
                    sendMessageUtil.setEmailMessage(msgText);
                    sendMessageUtil.sendEmail();
                    sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
                    //sendMessageUtil.setSmsNo("919427410774");
                    StringBuilder sb = new StringBuilder();
                    sb.append("Dear User,%0AThis is to inform you that Financial Institute " + payment.getBankName() + " has received payment for Complaint No. " + payment.getComplaintMaster().getComplaintId());
                    sb.append("%0AThanks,%0Ae-GP System");
                    sendMessageUtil.setSmsBody(sb.toString());
                    sendMessageUtil.sendSMS();
                    strTo = null;

                }

                sendMessageUtil = null;
                utility = null;

            }
            // to retrieve last updated/saved record from tblComplaintPayments table
            log.debug("retrieve latest complaints $$$$$$$$$$$$$");
//			List<TblComplaintPayments> paydetails=mgmtService.findComplaintPayments("complaintPaymentId",Operation_enum.EQ,Integer.parseInt(complId),"isLive",Operation_enum.EQ,"Yes","paymentFor",Operation_enum.EQ,paymentFor);

            if (req.getParameter("emailId") != null) {
                req.setAttribute("emailId", req.getParameter("emailId"));
            }

            mov.addObject("paydetails", payment);
            log.debug("Make payment for complaint page calling!!!!!!!!!!!!!!!!!!!!!!!!!!");
            return mov;

        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: complaintFeePaymentSave(LINE:335) :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    @RequestMapping(value = "/resources/common/viewFeesDetails.htm")
    public ModelAndView viewFeesDetails(@RequestParam("tenderId") Integer id, @RequestParam("feetype") String feetype, @RequestParam("complId") Integer compId, HttpServletRequest req, HttpServletResponse res) {
        ModelAndView mov = new ModelAndView("viewFeesDetails");
        try {
            String toApp = "";
            if ("registration".equalsIgnoreCase(feetype)) {
                toApp = " Fee";
            } else {
                toApp = " Deposit Fee";
            }
            TblComplaintPayments payments = mgmtService.getPaymentDetails(feetype + toApp, id, compId);
            mov.addObject("paydetails", payments);

        } // to view registred  fee payment details
        catch (Exception ex) {
            System.out.println("Exception in viewfees details :" + ex);
            System.out.println(ex);
        }
        return mov;
    }
    // to view registred  fee payment details

    @RequestMapping(value = "/resources/common/viewComplaintFeeDetails.htm")
    public ModelAndView viewComplaintFeeDetails(HttpServletRequest req, HttpServletResponse res, HttpSession session) {
        List<TblPartnerAdmin> lstPartnerAdmin = new ArrayList<TblPartnerAdmin>();
        TblPartnerAdmin partnerAdmin = null;
        TblComplaintPayments payment = null;
        ModelAndView mov = new ModelAndView("../partner/view_complaint_payment_details");
        try {
            if (req.getParameter("complaintPaymentId") != null && !("").equalsIgnoreCase(req.getParameter("complaintPaymentId"))) {

                log.debug("In edit Action of Controller>>>>>>>>>");
                String paymentId = req.getParameter("complaintPaymentId");
                log.debug("payment id is : " + paymentId);

                List<TblComplaintPayments> payments = mgmtService.findComplaintPayments("complaintPaymentId", Operation_enum.EQ, Integer.parseInt(paymentId));

                if (payments != null && payments.size() >= 1) {
                    payment = payments.get(0);
                    // adding Branch Maker details
                    lstPartnerAdmin = mgmtService.getTblPartnerAdmin(payment.getCreatedBy(), false);
                    if (lstPartnerAdmin != null && lstPartnerAdmin.size() >= 1) {
                        partnerAdmin = lstPartnerAdmin.get(0);
                        mov.addObject("partnerAdmin", partnerAdmin);
                    }
                    log.debug("Payment details for complaint made page calling");

                }
            }
            mov.addObject("paydetails", payment);
            return mov;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: viewComplaintFeeDetails :" + e);
            return new ModelAndView("/StackTrace");
        }

    }

    /*
     * DG CPTU flow starts here.
     * to view all complaints
     * to assign the complaint to review panel
     * to notify to PE
     */
    // to view all complaints by DGCPTU
    @RequestMapping(value = "/resources/common/ViewComplaintsCPTU.htm")
    public ModelAndView viewComplCPTU(HttpServletRequest req, HttpServletResponse res) {
        ModelAndView mov;
        try {
            if (req.getParameter("reviewPanel") != null) {
                try {
                    String panelId = req.getParameter("reviewPanel");
                    int getReviewPanel = Integer.parseInt(panelId);
                    if (getReviewPanel != 0) {
                        int complId = Integer.parseInt(req.getParameter("complaintId"));
                        log.debug("this is review panel on second call to view the complaint by dgcptu " + getReviewPanel);
                        List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, complId);
                        TblComplaintMaster complaintmaster = complaintMasterList.get(0);
                        complaintmaster.setPanelId(getReviewPanel);
                        mgmtService.updateMaster(complaintmaster);
                        java.util.List<SPTenderCommonData> thisTenderInfoLst = tenderCommonService.returndata("tenderinfobar", complaintmaster.getTenderMaster().getTenderId() + "", null);
                        MailContentUtility utility = new MailContentUtility();
                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                        String userName = req.getSession().getAttribute("userName").toString();
                        String msgText = utility.sendMailToReviewPanel(complaintmaster.getTenderMaster().getTenderId(), thisTenderInfoLst, complaintmaster.getComplaintId(), complaintmaster.getTendererMaster().getFirstName(), complaintmaster.getComplaintType().getComplaintType(), userName);
                        List<Object> list = mgmtService.getEmailIdFromRpId(complaintmaster.getPanelId());
                        String emailId = "";
                        if (list != null && !list.isEmpty()) {
                            emailId = list.get(0).toString();
                        }
                        String[] strTo = {emailId};
                        String strFrom = commonservice.getEmailId(req.getSession().getAttribute("userId").toString());
                        String cc = "";
                        AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), complaintmaster.getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), "Assign complaint to Review Panel", "");
                        String subject = "e-GP: DG (CPTU) has assigned Complaint to Review Panel";
                        registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
                        sendMessageUtil.setEmailTo(strTo);
                        sendMessageUtil.setEmailFrom(strFrom);
                        sendMessageUtil.setEmailSub(subject);
                        sendMessageUtil.setEmailMessage(msgText);
                        sendMessageUtil.sendEmail();
                        sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
                        //sendMessageUtil.setSmsNo("919427410774");
                        StringBuilder sb = new StringBuilder();
                        sb.append("Dear User,%0AThis is to inform you that DG (CPTU) " + userName + " has assigned Complaint to Review Panel for tender No:- " + complaintmaster.getTenderMaster().getTenderId());
                        sb.append("%0AThanks,%0AeGP System");
                        sendMessageUtil.setSmsBody(sb.toString());
                        sendMessageUtil.sendSMS();
                        strTo = null;
                        sendMessageUtil = null;
                        utility = null;
                        msgBoxContentUtility = null;
                    }
                } catch (Exception e) {
                    log.error("EXCEPTION AT METHOD: complaintFeePaymentSave(LINE:335) :" + e);
                    return new ModelAndView("/StackTrace");
                }
            }

            mov = new ModelAndView("view_complaints_CPTU");
            // /complaintMgmt/

            List<TblReviewPanel> lstReviewPanels = mgmtService.getReviewPanels();
            mgmtService.updateComplaintsForPayment();
            List<Integer> lstCompaintsCount = new ArrayList<Integer>();
            List<TblComplaintMaster> lstComplaintMaster = null;
            HashMap<Integer, String> map = new HashMap<Integer, String>();
            for (TblReviewPanel panel : lstReviewPanels) {
                map.put(panel.getReviewPanelId(), panel.getReviewPanelName());
            }
            if (lstReviewPanels != null) {
                for (TblReviewPanel reviewPanel : lstReviewPanels) {
                    lstComplaintMaster = mgmtService.getPendingComplaintsByReviewPanel("panelId", Operation_enum.EQ, reviewPanel.getReviewPanelId(), "complaintStatus", Operation_enum.EQ, "pending", "complaintLevel", Operation_enum.EQ, new TblComplaintLevel(4));
                    lstCompaintsCount.add((Integer) lstComplaintMaster.size());
                }
                mov.addObject("map", map);
                mov.addObject("lstReviewPanels", lstReviewPanels);
                mov.addObject("lstCompaintsCount", lstCompaintsCount);
            }
            List<TblComplaintMaster> lstViewComplCPTU = mgmtService.searchPendingComplaintDetailsForDGCPTU(4);
            mov.addObject("lstViewComplCPTU", lstViewComplCPTU);
            log.debug("View Complaints CPTU page calling ");
            return mov;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: complaintFeePaymentSave(LINE:335) :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    //to view all the complaints by DGCPTU based on search criteria
    @RequestMapping(value = "/resources/common/searchviewcomplaintsCPTU.htm")
    public ModelAndView searchComplDGCPTU(HttpServletRequest req, HttpServletResponse res, HttpSession session) {

        ModelAndView mov;
        try {
            // Updates Complaint payments for latest status
            mgmtService.updateComplaintsForPayment();
            if (req.getParameter("reviewPanel") != null) {
                try {
                    String panelId = req.getParameter("reviewPanel");
                    int getReviewPanel = Integer.parseInt(panelId);
                    if (getReviewPanel != 0) {
                        int complId = Integer.parseInt(req.getParameter("complaintId"));
                        log.debug("this is review panel on second call to view the complaint by dgcptu " + getReviewPanel);
                        List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, complId);
                        TblComplaintMaster complaintmaster = complaintMasterList.get(0);
                        complaintmaster.setPanelId(getReviewPanel);
                        mgmtService.updateMaster(complaintmaster);
                    }
                } catch (Exception e) {
                    log.error("EXCEPTION AT METHOD: searchComplDGCPTU :" + e);
                    return new ModelAndView("/StackTrace");
                }
            }

            mov = new ModelAndView("view_complaints_CPTU");
            TblTenderDetails details = null;
            int complid = 0;
            int tenderid = 0;
            String refNo = null;
            int officeid = 0;
            log.debug("in search complaint hope");
            String role = ComplaintMgmtConstants.REVIEW_PANEL_ROLE;
            String user = session.getAttribute("userId").toString();
            int userId = Integer.parseInt(user);
            List listGovId = mgmtService.getGovUserDetails(userId);
            Integer govUserId = (Integer) listGovId.get(0);
            log.debug("gov user id is" + govUserId);
            String complId = "";
            String status = "";
            if (req.getParameter("txtCompID") != null) {
                complId = req.getParameter("txtCompID");
            }
            if (req.getParameter("status") != null) {
                status = req.getParameter("status");
            }

            log.debug("complId" + complId);
            String tenderId = req.getParameter("txtTenderID");
            log.debug("tenderId" + tenderId);
            refNo = req.getParameter("txtReferneceNo");
            log.debug("refId" + refNo);

            if ("".equalsIgnoreCase(complId)) {
                complid = 0;
                log.debug("complid is null>>>>>>>>>" + complid);
            } else {
                complid = Integer.parseInt(complId);
                log.debug("complid is not null>>>>>>>>>" + complid);
            }
            if ("".equalsIgnoreCase(req.getParameter("txtTenderID"))) {
                tenderid = 0;
                log.debug("tenderid is  null>>>>>>>>>>" + tenderid);
            } else {
                tenderid = Integer.parseInt(tenderId);
                log.debug("tenderid is not null>>>>>>>>>>" + tenderid);
            }

            TblTenderDetails refDetails = null;
            List<TblTenderDetails> tenderDetails = mgmtService.getTenderDetailsByRef("reoiRfpRefNo", Operation_enum.EQ, refNo);
            if (tenderDetails != null && tenderDetails.size() == 1) {
                log.debug("in tenderdetails size is" + tenderDetails.size());
                refDetails = tenderDetails.get(0);
                log.debug("ref ob is" + refDetails);
            }
            List<TblComplaintMaster> lstViewComplCPTU = mgmtService.searchComplaintDetails(tenderid, complid, refDetails, govUserId, role, "Clarification", true, Integer.parseInt(status));
            List<TblReviewPanel> lstReviewPanels = mgmtService.getReviewPanels();
            HashMap<Integer, String> map = new HashMap<Integer, String>();
            for (TblReviewPanel panel : lstReviewPanels) {
                map.put(panel.getReviewPanelId(), panel.getReviewPanelName());
            }
            List<Integer> lstCompaintsCount = new ArrayList<Integer>();
            List<TblComplaintMaster> lstComplaintMaster = null;
            for (TblReviewPanel panel : lstReviewPanels) {
                lstComplaintMaster = mgmtService.getPendingComplaintsByReviewPanel("panelId", Operation_enum.EQ, panel.getReviewPanelId(), "complaintStatus", Operation_enum.EQ, "pending", "complaintLevel", Operation_enum.EQ, new TblComplaintLevel(4));
                lstCompaintsCount.add((Integer) lstComplaintMaster.size());
            }

            if (lstReviewPanels != null && lstReviewPanels.size() > 0) {
                log.debug("Total no of review panels " + lstReviewPanels.size());
                mov.addObject("lstReviewPanels", lstReviewPanels);
                mov.addObject("map", map);
            }
            if (lstCompaintsCount != null && lstCompaintsCount.size() > 0) {
                mov.addObject("lstCompaintsCount", lstCompaintsCount);
            }
            if (lstViewComplCPTU != null && lstViewComplCPTU.size() > 0) {
                log.debug("all complaints displaying" + lstViewComplCPTU.size());
                mov.addObject("lstViewComplCPTU", lstViewComplCPTU);
            }


            log.debug("View all Complaints by DGCPTU by search criteria ");
            return mov;
        } catch (Exception e) {
            System.out.println(e);
            return new ModelAndView("/StackTrace");
        }
    }

    //to assign complaint to review panel
    @RequestMapping(value = "/resources/common/AssignComplaints.htm")
    public ModelAndView assignComplaint(HttpServletRequest req, HttpServletResponse res) {
        ModelAndView assignCompl = new ModelAndView("assign_complaints");
        try {
            int complaintId = 0;
            if (req.getParameter("complaintId") != null) {
                complaintId = Integer.parseInt(req.getParameter("complaintId"));
                log.debug("Complaint id is @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" + complaintId);
            }

            TblComplaintMaster master = null;
            List<Object[]> refNoCompanyName = mgmtService.getTenderDetails(complaintId);
            log.debug("Reference no and company name of the complaint id is " + refNoCompanyName.size());


            List<TblReviewPanel> lstReviewPanels = mgmtService.getReviewPanels();
            if (lstReviewPanels != null && lstReviewPanels.size() > 0) {
                log.debug("Total no of review panels " + lstReviewPanels.size());
                master = mgmtService.getComplaintDetails(complaintId);
                if (refNoCompanyName != null && refNoCompanyName.size() > 0) {
                    Object[] tenderDetails = refNoCompanyName.get(0);
                    log.debug("reference no is 1234512345123451234512345123451234512345" + tenderDetails);
                    log.debug("name of the tender / company name 33333333333333333333333333" + tenderDetails);
                }
                assignCompl.addObject("master", master);
                assignCompl.addObject("lstReviewPanels", lstReviewPanels);
            }
            return assignCompl;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: assignComplaint :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    @RequestMapping(value = "/resources/common/ComplNotificationPE.htm")
    public ModelAndView complNotificationPE(HttpServletRequest req, HttpServletResponse res) {
        ModelAndView notifyComplPE = new ModelAndView("compl_notification_PE");
        try {
            int complaintId = 0;
            if (req.getParameter("complaintId") != null) {
                complaintId = Integer.parseInt(req.getParameter("complaintId"));
            }
            log.debug("Complaint id is @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" + complaintId);
            TblComplaintMaster master = mgmtService.getComplaintDetails(complaintId);
            if (master != null) {
                notifyComplPE.addObject("master", master);
            }
            log.debug("Complaints Notification PE page calling ");

            return notifyComplPE;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: complNotificationPE(LINE:752) :" + e);
            return new ModelAndView("/StackTrace");
        }
    }

    //to send notification to PE
    @RequestMapping(value = "/resources/common/ComplNotificationCPTU.htm")
    public ModelAndView complNotificationCPTU(HttpServletRequest req, HttpServletResponse res) {
        ModelAndView notCPTU;
        try {
            String txtaCommets = null;
            TblComplaintMaster complaintmaster = null;
            int complaintId = 0;

            if (req.getParameter("complaintId") != null) {
                complaintId = Integer.parseInt(req.getParameter("complaintId"));
                log.debug("Complaint id is @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" + complaintId);
            }
            if (req.getParameter("txtaCommets") != null) {
                txtaCommets = req.getParameter("txtaCommets");
            }
            TblComplaintMaster master = mgmtService.getComplaintDetails(complaintId);
            try {

                if (txtaCommets != null) {
                    List<TblComplaintMaster> complaintMasterList = mgmtService.getComplaintById("complaintId", Operation_enum.EQ, complaintId);
                    if (complaintMasterList != null) {
                        complaintmaster = complaintMasterList.get(0);
                        complaintmaster.setNotificationRemarksToPe(txtaCommets);
                        complaintmaster.setNotificationDtToPe(new Date());
                        mgmtService.updateMaster(complaintmaster);
                        java.util.List<SPTenderCommonData> thisTenderInfoLst = tenderCommonService.returndata("tenderinfobar", complaintmaster.getTenderMaster().getTenderId() + "", null);
                        MailContentUtility utility = new MailContentUtility();
                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                        String userName = req.getSession().getAttribute("userName").toString();
                        List<SPTenderCommonData> checkPE = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", complaintmaster.getTenderMaster().getTenderId() + "", null);
                        String userId = "";
                        if (checkPE != null && !checkPE.isEmpty()) {
                            userId = checkPE.get(0).getFieldName1();
                        }
                        AuditTrail.generateAudit(new AuditTrail(req.getHeader("X-FORWARDED-FOR") != null ? req.getHeader("X-FORWARDED-FOR") : req.getRemoteAddr(), req.getSession().getAttribute("sessionId"), req.getSession().getAttribute("userTypeId"), req.getHeader("referer")), complaintmaster.getTenderMaster().getTenderId(), "tenderId", EgpModule.Complaint_Management.getName(), "Notify PE", "");
                        String msgText = utility.sendMailToPEForNoti(complaintmaster.getTenderMaster().getTenderId(), thisTenderInfoLst, complaintmaster.getComplaintId(), complaintmaster.getTendererMaster().getFirstName(), complaintmaster.getComplaintType().getComplaintType(), userName,txtaCommets);
                        String[] strTo = {commonservice.getEmailId(userId)};
                        String strFrom = commonservice.getEmailId(req.getSession().getAttribute("userId").toString());
                        String cc = "";
                        String subject = "e-GP: Notification from DG (CPTU)";
                        registerService.contentAdmMsgBox(strTo[0], strFrom, subject, msgText);
                        sendMessageUtil.setEmailTo(strTo);
                        sendMessageUtil.setEmailFrom(strFrom);
                        sendMessageUtil.setEmailSub(subject);
                        sendMessageUtil.setEmailMessage(msgText);
                        sendMessageUtil.sendEmail();
                        sendMessageUtil.setSmsNo(accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(strTo[0])));
                        //sendMessageUtil.setSmsNo("919427410774");
                        StringBuilder sb = new StringBuilder();
                        sb.append("Dear User,%0AThis is to inform you that DG CPTU has send notification for Complaint No" + complaintmaster.getComplaintId());
                        sb.append("%0AThanks,%0AeGP System");
                        sendMessageUtil.setSmsBody(sb.toString());
                        sendMessageUtil.sendSMS();
                        strTo = null;
                        sendMessageUtil = null;
                        utility = null;
                        msgBoxContentUtility = null;
                    }
                    log.debug("this comment entered by dgcptu while notifing to pe ======== " + txtaCommets);
                }

            } catch (Exception e) {
                log.error("EXCEPTION AT METHOD: complNotificationCPTU :" + e);
                return new ModelAndView("/StackTrace");
            }

            notCPTU = new ModelAndView("compl_notification_CPTU");
            notCPTU.addObject("master", master);
            log.debug("Complaints Notification CPTU page calling ");

            return notCPTU;
        } catch (Exception e) {
            log.error("EXCEPTION AT METHOD: complNotificationCPTU :" + e);
            return new ModelAndView("/StackTrace");
        }
    }
    //go back to dashboard DgCPTU

    @RequestMapping(value = "/resources/common/Dashboard.htm")
    public ModelAndView dashBoard() {
        log.debug("in the view controller ******* dashboard");
        ModelAndView dashBoard = new ModelAndView("/resources/common/InboxMessage");
        return dashBoard;
    }
}
