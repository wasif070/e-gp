/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.webservices;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.xmlpojos.WSResponseObject;
import com.cptu.egp.eps.web.utility.WebServiceUtil;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import org.apache.log4j.Logger;

/**
 * WebService for sharing report details (for RegisrationDetails.jsp Page).
 * @author sreenu
 */
@WebService()
@XmlSeeAlso({SPCommonSearchDataMore.class})
public class Reports {

    private static final Logger LOGGER = Logger.getLogger(Reports.class);
    private String logUserId = "0";
    private static final String LOGGERSTART = " Starts";
    private static final String LOGGEREND = " Ends";
    private static final String EMPTY_STRING = "";
    private static final int WS_ID_NationalRegisteredTenderersOrConsultants = 20;
    private static final String WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE =
            "CommonSearchDataMoreService";
    private static final int WS_ID_InternationalRegisteredTenderersOrConsultants = 21;
    private static final int WS_ID_TotalRegisteredTenderersOrConsultants = 22;
    private static final int WS_ID_NationalIndividualConsultants = 23;
    private static final int WS_ID_InterNationalIndividualConsultants = 24;
    private static final int WS_ID_TotalRegisteredIndividualConsultants = 25;
    private static final int WS_ID_TotalNationalTenderersOrConsultantsAndIndividualConsultants = 26;
    private static final int WS_ID_TotalInterNationalTenderersOrConsultantsAndIndividualConsultants = 27;
    private static final int WS_ID_RegisteredNumberOfMinistryDetails = 28;
    private static final int WS_ID_RegisteredNumberOfMinistryPeOffices = 29;
    private static final int WS_ID_RegisteredNumberOfDivisionDetails = 30;
    private static final int WS_ID_RegisteredNumberOfDivisionPeOffices = 31;
    private static final int WS_ID_RegisteredNumberOfOrganisationDetails = 32;
    private static final int WS_ID_RegisteredNumberOfOrganisationPeOffices = 33;
    private static final int WS_ID_RegisteredBankAndBranches = 34;
    private static final int WS_ID_RegisteredDevelopmentPartners = 35;
    private static final int WS_ID_TotalAPPsPrepared = 36;
    private static final int WS_ID_TotalAPPsApproved = 37;
    private static final int WS_ID_TotalTendersPublishedByMinistry = 38;
    private static final int WS_ID_TotalTendersProcessedByMinistry = 39;
    private static final int WS_ID_TotalContractsAwardedByMinistry = 40;
    private static final int WS_ID_TotalTendersPublishedByDivision = 41;
    private static final int WS_ID_TotalTendersProcessedByDivision= 42;
    private static final int WS_ID_TotalContractsAwardedByDivision = 43;
    private static final int WS_ID_TotalTendersPublishedByOrganization = 44;
    private static final int WS_ID_TotalTendersProcessedByOrganization = 45;
    private static final int WS_ID_TotalContractsAwardedByOrganization = 46;

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }
    
    /***
     * This method returns the no.of National Registered Tenderers/consultants
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getNationalRegisteredTenderersOrConsultants")
    public WSResponseObject getNationalRegisteredTenderersOrConsultants(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getNationalRegisteredTenderersOrConsultants : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_NationalRegisteredTenderersOrConsultants, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_NationalRegisteredTenderersOrConsultants,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> nationalRegisteredTenderersOrConsultantsValueList = new ArrayList<String>();
            String nationalRegisteredTenderersOrConsultants = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    nationalRegisteredTenderersOrConsultants = registrationDetailsList.get(0).getFieldName1();
                }
            } catch (Exception e) {
                LOGGER.error("getNationalRegisteredTenderersOrConsultants : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (nationalRegisteredTenderersOrConsultants != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                nationalRegisteredTenderersOrConsultantsValueList.add(nationalRegisteredTenderersOrConsultants);
                wSResponseObject.setResponseList(nationalRegisteredTenderersOrConsultantsValueList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getNationalRegisteredTenderersOrConsultants : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method returns the no.of International Registered Tenderers/consultants
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getInternationalRegisteredTenderersOrConsultants")
    public WSResponseObject getInternationalRegisteredTenderersOrConsultants(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getInternationalRegisteredTenderersOrConsultants : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_InternationalRegisteredTenderersOrConsultants, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_NationalRegisteredTenderersOrConsultants,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> interNationalRegisteredTenderersOrConsultantsValueList = new ArrayList<String>();
            String internationalRegisteredTenderersOrConsultants = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    internationalRegisteredTenderersOrConsultants =
                            registrationDetailsList.get(0).getFieldName2();
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.error("getInternationalRegisteredTenderersOrConsultants : " + logUserId + e);
            }
            if (internationalRegisteredTenderersOrConsultants != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                interNationalRegisteredTenderersOrConsultantsValueList.add(internationalRegisteredTenderersOrConsultants);
                wSResponseObject.setResponseList(interNationalRegisteredTenderersOrConsultantsValueList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getInternationalRegisteredTenderersOrConsultants : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method returns the total no.of i.e. National and International
     * Registered Tenderers/consultants
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalRegisteredTenderersOrConsultants")
    public WSResponseObject getTotalRegisteredTenderersOrConsultants(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalRegisteredTenderersOrConsultants : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalRegisteredTenderersOrConsultants, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalRegisteredTenderersOrConsultants,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalRegisteredTenderersOrConsultantsValueList = new ArrayList<String>();
            String totalRegisteredTenderersOrConsultants = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    totalRegisteredTenderersOrConsultants = EMPTY_STRING
                            + (WebServiceUtil.getIntegerValue(registrationDetailsList.get(0).getFieldName1())
                            + WebServiceUtil.getIntegerValue(registrationDetailsList.get(0).getFieldName2()));
                }
            } catch (Exception e) {
                LOGGER.error("getTotalRegisteredTenderersOrConsultants : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalRegisteredTenderersOrConsultants != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalRegisteredTenderersOrConsultantsValueList.add(totalRegisteredTenderersOrConsultants);
                wSResponseObject.setResponseList(totalRegisteredTenderersOrConsultantsValueList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalRegisteredTenderersOrConsultants : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method returns the no.of National Registered Individual Consultants
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getNationalIndividualConsultants")
    public WSResponseObject getNationalIndividualConsultants(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getNationalIndividualConsultants : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_NationalIndividualConsultants, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_NationalIndividualConsultants,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> nationalIndividualConsultantsValueList = new ArrayList<String>();
            String nationalIndividualConsultantsValue = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    nationalIndividualConsultantsValue =
                            registrationDetailsList.get(0).getFieldName3();
                }
            } catch (Exception e) {
                LOGGER.error("getNationalIndividualConsultants : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (nationalIndividualConsultantsValue != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                nationalIndividualConsultantsValueList.add(nationalIndividualConsultantsValue);
                wSResponseObject.setResponseList(nationalIndividualConsultantsValueList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getNationalIndividualConsultants : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method returns the no.of International Registered Individual Consultants
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getInternationalIndividualConsultants")
    public WSResponseObject getInternationalIndividualConsultants(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getInternationalIndividualConsultants : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_InterNationalIndividualConsultants, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_InterNationalIndividualConsultants,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> interNationalIndividualConsultantsValueList = new ArrayList<String>();
            String interNationalIndividualConsultantsValue = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    interNationalIndividualConsultantsValue =
                            registrationDetailsList.get(0).getFieldName4();
                }
            } catch (Exception e) {
                LOGGER.error("getInternationalIndividualConsultants : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (interNationalIndividualConsultantsValue != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                interNationalIndividualConsultantsValueList.add(interNationalIndividualConsultantsValue);
                wSResponseObject.setResponseList(interNationalIndividualConsultantsValueList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getInternationalIndividualConsultants : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }
    
    /***
     * This method returns total no.of i.e. National and International Registered
     * Individual Consultants
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalRegisteredIndividualConsultants")
    public WSResponseObject getTotalRegisteredIndividualConsultants(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalRegisteredIndividualConsultants : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalRegisteredIndividualConsultants, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalRegisteredIndividualConsultants,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalRegisteredIndividualConsultantsValueList = new ArrayList<String>();
            String totalRegisteredIndividualConsultants = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    totalRegisteredIndividualConsultants = EMPTY_STRING
                            + (WebServiceUtil.getIntegerValue(registrationDetailsList.get(0).getFieldName3())
                            + WebServiceUtil.getIntegerValue(registrationDetailsList.get(0).getFieldName4()));
                }
            } catch (Exception e) {
                LOGGER.error("getTotalRegisteredIndividualConsultants : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalRegisteredIndividualConsultants != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalRegisteredIndividualConsultantsValueList.add(totalRegisteredIndividualConsultants);
                wSResponseObject.setResponseList(totalRegisteredIndividualConsultantsValueList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalRegisteredIndividualConsultants : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method returns total no.of National Tenderers/Consultants and
     * National Individual Consultants
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalNationalTenderersOrConsultantsAndIndividualConsultants")
    public WSResponseObject getTotalNationalTenderersOrConsultantsAndIndividualConsultants(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalNationalTenderersOrConsultantsAndIndividualConsultants : "
                + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalNationalTenderersOrConsultantsAndIndividualConsultants,
                username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalNationalTenderersOrConsultantsAndIndividualConsultants,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalNationalTenderersOrConsultantsAndIndividualConsultantsValueList =
                    new ArrayList<String>();
            String totalNationalTenderersOrConsultantsAndIndividualConsultants = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    totalNationalTenderersOrConsultantsAndIndividualConsultants = EMPTY_STRING
                            + (WebServiceUtil.getIntegerValue(registrationDetailsList.get(0).getFieldName1())
                            + WebServiceUtil.getIntegerValue(registrationDetailsList.get(0).getFieldName3()));
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.error("getTotalNationalTenderersOrConsultantsAndIndividualConsultants : " + logUserId + e);
            }
            if (totalNationalTenderersOrConsultantsAndIndividualConsultants != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalNationalTenderersOrConsultantsAndIndividualConsultantsValueList.add(
                        totalNationalTenderersOrConsultantsAndIndividualConsultants);
                wSResponseObject.setResponseList(totalNationalTenderersOrConsultantsAndIndividualConsultantsValueList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalNationalTenderersOrConsultantsAndIndividualConsultants : "
                + logUserId + LOGGEREND);
        return wSResponseObject;
    }
    
    /***
     * This method returns total no.of International Tenderers/Consultants and
     * International Individual Consultants
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalInternationalTenderersOrConsultantsAndIndividualConsultants")
    public WSResponseObject getTotalInternationalTenderersOrConsultantsAndIndividualConsultants(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalInternationalTenderersOrConsultantsAndIndividualConsultants : "
                + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalInterNationalTenderersOrConsultantsAndIndividualConsultants,
                username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalInterNationalTenderersOrConsultantsAndIndividualConsultants,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalInterNationalTenderersOrConsultantsAndIndividualConsultantsValueList =
                    new ArrayList<String>();
            String totalInterNationalTenderersOrConsultantsAndIndividualConsultants = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    totalInterNationalTenderersOrConsultantsAndIndividualConsultants = EMPTY_STRING
                            + (WebServiceUtil.getIntegerValue(registrationDetailsList.get(0).getFieldName2())
                            + WebServiceUtil.getIntegerValue(registrationDetailsList.get(0).getFieldName4()));
                }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.error("getTotalInternationalTenderersOrConsultantsAndIndividualConsultants : " + logUserId + e);
            }
            if (totalInterNationalTenderersOrConsultantsAndIndividualConsultants != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalInterNationalTenderersOrConsultantsAndIndividualConsultantsValueList.add(
                        totalInterNationalTenderersOrConsultantsAndIndividualConsultants);
                wSResponseObject.setResponseList(totalInterNationalTenderersOrConsultantsAndIndividualConsultantsValueList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalInternationalTenderersOrConsultantsAndIndividualConsultants : "
                + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method gives the no.of Registered Ministry Details
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getRegisteredNumberOfMinistryDetails")
    public WSResponseObject getRegisteredNumberOfMinistryDetails(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getRegisteredNumberOfMinistryDetails : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_RegisteredNumberOfMinistryDetails, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_RegisteredNumberOfMinistryDetails,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> registeredNumberOfMinistryDetailsList = new ArrayList<String>();
            String registeredNumberOfMinistryDetailsValue = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    registeredNumberOfMinistryDetailsValue =
                            registrationDetailsList.get(0).getFieldName5();
                }
            } catch (Exception e) {
                LOGGER.error("getRegisteredNumberOfMinistryDetails : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (registeredNumberOfMinistryDetailsValue != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                registeredNumberOfMinistryDetailsList.add(registeredNumberOfMinistryDetailsValue);
                wSResponseObject.setResponseList(registeredNumberOfMinistryDetailsList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getRegisteredNumberOfMinistryDetails : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method gives the no.of Registered Ministry PE offices
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getRegisteredNumberOfMinistryPeOffices")
    public WSResponseObject getRegisteredNumberOfMinistryPeOffices(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getRegisteredNumberOfMinistryPeOffices : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_RegisteredNumberOfMinistryPeOffices, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_RegisteredNumberOfMinistryPeOffices,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> registeredNumberOfMinistryPeOfficesList = new ArrayList<String>();
            String registeredNumberOfMinistryPeOfficesValue = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    registeredNumberOfMinistryPeOfficesValue =
                            registrationDetailsList.get(0).getFieldName6();
                }
            } catch (Exception e) {
                LOGGER.error("getRegisteredNumberOfMinistryPeOffices : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (registeredNumberOfMinistryPeOfficesValue != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                registeredNumberOfMinistryPeOfficesList.add(registeredNumberOfMinistryPeOfficesValue);
                wSResponseObject.setResponseList(registeredNumberOfMinistryPeOfficesList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getRegisteredNumberOfMinistryPeOffices : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method gives the no.of Registered Division Details
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getRegisteredNumberOfDivisionDetails")
    public WSResponseObject getRegisteredNumberOfDivisionDetails(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getRegisteredNumberOfDivisionDetails : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_RegisteredNumberOfDivisionDetails, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_RegisteredNumberOfDivisionDetails,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> registeredNumberOfDivisionDetailsList = new ArrayList<String>();
            String registeredNumberOfDivisionDetailsValue = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    registeredNumberOfDivisionDetailsValue =
                            registrationDetailsList.get(0).getFieldName7();
                }
            } catch (Exception e) {
                LOGGER.error("getRegisteredNumberOfDivisionDetails : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (registeredNumberOfDivisionDetailsValue != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                registeredNumberOfDivisionDetailsList.add(registeredNumberOfDivisionDetailsValue);
                wSResponseObject.setResponseList(registeredNumberOfDivisionDetailsList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getRegisteredNumberOfDivisionDetails : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method gives the no.of Registered Division PE offices
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getRegisteredNumberOfDivisionPeOffices")
    public WSResponseObject getRegisteredNumberOfDivisionPeOffices(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getRegisteredNumberOfDivisionPeOffices : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_RegisteredNumberOfDivisionPeOffices, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_RegisteredNumberOfDivisionDetails,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> registeredNumberOfDivisionPeOfficesList = new ArrayList<String>();
            String registeredNumberOfDivisionPeOfficesValue = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    registeredNumberOfDivisionPeOfficesValue =
                            registrationDetailsList.get(0).getFieldName8();
                }
            } catch (Exception e) {
                LOGGER.error("getRegisteredNumberOfDivisionPeOffices : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (registeredNumberOfDivisionPeOfficesValue != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                registeredNumberOfDivisionPeOfficesList.add(registeredNumberOfDivisionPeOfficesValue);
                wSResponseObject.setResponseList(registeredNumberOfDivisionPeOfficesList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getRegisteredNumberOfDivisionPeOffices : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }
    /***
     * This method gives the no.of Registered Organization Details
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getRegisteredNumberOfOrganisationDetails")
    public WSResponseObject getRegisteredNumberOfOrganisationDetails(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getRegisteredNumberOfOrganisationDetails : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_RegisteredNumberOfOrganisationDetails, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_RegisteredNumberOfOrganisationDetails,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> registeredNumberOfOrganisationDetailsList = new ArrayList<String>();
            String registeredNumberOfOrganisationDetailsValue = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    registeredNumberOfOrganisationDetailsValue =
                            registrationDetailsList.get(0).getFieldName9();
                }
            } catch (Exception e) {
                LOGGER.error("getRegisteredNumberOfOrganisationDetails : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (registeredNumberOfOrganisationDetailsValue != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                registeredNumberOfOrganisationDetailsList.add(registeredNumberOfOrganisationDetailsValue);
                wSResponseObject.setResponseList(registeredNumberOfOrganisationDetailsList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getRegisteredNumberOfOrganisationDetails : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method gives the no.of Registered Organization PE offices
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getRegisteredNumberOfOrganisationPeOffices")
    public WSResponseObject getRegisteredNumberOfOrganisationPeOffices(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getRegisteredNumberOfOrganisationPeOffices : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_RegisteredNumberOfOrganisationPeOffices, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_RegisteredNumberOfOrganisationPeOffices,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> registeredNumberOfOrganisationPeOfficesList = new ArrayList<String>();
            String registeredNumberOfOrganisationPeOfficesValue = null;
            try {
                List<SPCommonSearchDataMore> registrationDetailsList = getRequiredReportData();
                if (registrationDetailsList != null && (registrationDetailsList.isEmpty() != true)) {
                    registeredNumberOfOrganisationPeOfficesValue =
                            registrationDetailsList.get(0).getFieldName10();
                }
            } catch (Exception e) {
                LOGGER.error("getRegisteredNumberOfOrganisationPeOffices : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (registeredNumberOfOrganisationPeOfficesValue != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                registeredNumberOfOrganisationPeOfficesList.add(registeredNumberOfOrganisationPeOfficesValue);
                wSResponseObject.setResponseList(registeredNumberOfOrganisationPeOfficesList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getRegisteredNumberOfOrganisationPeOffices : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }
    /***
     * This method gives the List of objects of
     * This method gives the List of String Objects of <b> Registered Bank and Branches</b> and
     * each object details are separated with a special character i.e. <b>^</b><br>
     * The order of details in each String is as below <br>
     * 1.Bank Name<br>
     * 2.No.of Branches<br>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getRegisteredBankAndBranches")
    public WSResponseObject getRegisteredBankAndBranches(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getRegisteredBankAndBranches : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_RegisteredBankAndBranches, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_RegisteredBankAndBranches,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> registeredBanksAndBranchesList = new ArrayList<String>();
            try {
                 List<SPCommonSearchDataMore> registeredBanksAndBranchesTempList =
                         getRequiredRegisteredBanksAndBranchesList();
                 if(registeredBanksAndBranchesTempList != null){
                      for (SPCommonSearchDataMore searchDataMore : registeredBanksAndBranchesTempList) {
                          StringBuilder sbCSV = new StringBuilder();
                          sbCSV.append(searchDataMore.getFieldName1());
                          sbCSV.append(WSResponseObject.APPEND_CHAR);
                          sbCSV.append(searchDataMore.getFieldName2());
                          //sbCSV.append(WSResponseObject.APPEND_CHAR);  Commented by sudhir 09072011
                          registeredBanksAndBranchesList.add(sbCSV.toString());
                     }
                 }
            } catch (Exception e) {
                LOGGER.error("getRegisteredBankAndBranches : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (registeredBanksAndBranchesList != null) {
                if (registeredBanksAndBranchesList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(registeredBanksAndBranchesList);
                }
            }
        }
        LOGGER.debug("getRegisteredBankAndBranches : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method gives the List of String Objects of <b>Registered Development Partners</b>
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getRegisteredDevelopmentPartners")
    public WSResponseObject getRegisteredDevelopmentPartners(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getRegisteredDevelopmentPartners : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_RegisteredDevelopmentPartners, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_RegisteredDevelopmentPartners,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> registeredDevelopmentPartnersList = new ArrayList<String>();
            try {
                List<SPCommonSearchDataMore> registeredDevelopmentPartnersTempList =
                        getRequiredRegisteredDevelopmentPartnersList();
                if(registeredDevelopmentPartnersTempList != null){
                      for (SPCommonSearchDataMore searchDataMore : registeredDevelopmentPartnersTempList) {
                        StringBuilder sbCSV = new StringBuilder();
                        sbCSV.append(searchDataMore.getFieldName1());
                        registeredDevelopmentPartnersList.add(sbCSV.toString());
                    }
                 }
            } catch (Exception e) {
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
                LOGGER.debug("getRegisteredDevelopmentPartners : " + logUserId + e);
            }
            if (registeredDevelopmentPartnersList != null) {
                if (registeredDevelopmentPartnersList.isEmpty()) {
                    wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                    wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
                } else {
                    wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                    wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                    wSResponseObject.setResponseList(registeredDevelopmentPartnersList);
                }
            }
        }
        LOGGER.debug("getRegisteredDevelopmentPartners : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method gives the Total No. of Annual Procurement Plans prepared
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalAPPsPrepared")
    public WSResponseObject getTotalAPPsPrepared(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalAPPsPrepared : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalAPPsPrepared, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalAPPsPrepared,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalAPPsPreparedValueList = new ArrayList<String>();
            String totalNumberOfAPPsPrepared = null;
            try {
                List<SPCommonSearchDataMore> appInfoDataList = getRequiredAPPInfoData();
                if (appInfoDataList != null && (appInfoDataList.isEmpty() != true)) {
                    totalNumberOfAPPsPrepared = appInfoDataList.get(0).getFieldName1();
                }
            } catch (Exception e) {
                LOGGER.error("getTotalAPPsPrepared : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalNumberOfAPPsPrepared != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalAPPsPreparedValueList.add(totalNumberOfAPPsPrepared);
                wSResponseObject.setResponseList(totalAPPsPreparedValueList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalAPPsPrepared : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method gives the Total No. of Annual Procurement Plans approved
     * @param username
     * @param password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalAPPsApproved")
    public WSResponseObject getTotalAPPsApproved(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalAPPsApproved : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalAPPsApproved, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalAPPsApproved,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalAPPsApprovedValueList = new ArrayList<String>();
            String totalNumberOfAPPsApproved = null;
            try {
                List<SPCommonSearchDataMore> appInfoDataList = getRequiredAPPInfoData();
                if (appInfoDataList != null && (appInfoDataList.isEmpty() != true)) {
                    totalNumberOfAPPsApproved = appInfoDataList.get(0).getFieldName2();
                }
            } catch (Exception e) {
                LOGGER.error("getTotalAPPsApproved : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalNumberOfAPPsApproved != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalAPPsApprovedValueList.add(totalNumberOfAPPsApproved);
                wSResponseObject.setResponseList(totalAPPsApprovedValueList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalAPPsApproved : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method gives the total number of tenders published by ministries.
     * @param username
     * @param password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalTendersPublishedByMinistry")
    public WSResponseObject getTotalTendersPublishedByMinistry(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalTendersPublishedByMinistry : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalTendersPublishedByMinistry, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalTendersPublishedByMinistry,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalTendersPublishedByMinistryList = new ArrayList<String>();
            String totalTendersPublishedByMinistry = null;
            try {
                List<SPCommonSearchDataMore> tendersDataList = getRequiredTendersData();
                if (tendersDataList != null && (tendersDataList.isEmpty() != true)) {
                    totalTendersPublishedByMinistry = tendersDataList.get(0).getFieldName1();
                }
            } catch (Exception e) {
                LOGGER.error("getTotalTendersPublishedByMinistry : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalTendersPublishedByMinistry != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalTendersPublishedByMinistryList.add(totalTendersPublishedByMinistry);
                wSResponseObject.setResponseList(totalTendersPublishedByMinistryList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalTendersPublishedByMinistry : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method gives the total number of tenders processed by ministries.
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalTendersProcessedByMinistry")
    public WSResponseObject getTotalTendersProcessedByMinistry(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalTendersProcessedByMinistry : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalTendersProcessedByMinistry, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalTendersProcessedByMinistry,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalTendersProcessedByMinistryList = new ArrayList<String>();
            String totalTendersProcessedByMinistry = null;
            try {
                List<SPCommonSearchDataMore> tendersDataList = getRequiredTendersData();
                if (tendersDataList != null && (tendersDataList.isEmpty() != true)) {
                    totalTendersProcessedByMinistry = tendersDataList.get(0).getFieldName2();
                }
            } catch (Exception e) {
                LOGGER.error("getTotalTendersProcessedByMinistry : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalTendersProcessedByMinistry != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalTendersProcessedByMinistryList.add(totalTendersProcessedByMinistry);
                wSResponseObject.setResponseList(totalTendersProcessedByMinistryList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalTendersProcessedByMinistry : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /***
     * This method gives the total number of tenders awarded by ministries.
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalContractsAwardedByMinistry")
    public WSResponseObject getTotalContractsAwardedByMinistry(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalContractsAwardedByMinistry : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalContractsAwardedByMinistry, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalContractsAwardedByMinistry,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalContractsAwardedByMinistryList = new ArrayList<String>();
            String totalContractsAwardedByMinistry = null;
            try {
                List<SPCommonSearchDataMore> tendersDataList = getRequiredTendersData();
                if (tendersDataList != null && (tendersDataList.isEmpty() != true)) {
                    totalContractsAwardedByMinistry = tendersDataList.get(0).getFieldName3();
                }
            } catch (Exception e) {
                LOGGER.error("getTotalContractsAwardedByMinistry : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalContractsAwardedByMinistry != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalContractsAwardedByMinistryList.add(totalContractsAwardedByMinistry);
                wSResponseObject.setResponseList(totalContractsAwardedByMinistryList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalContractsAwardedByMinistry : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }
    /***
     * This method gives the total number of tenders published by Division.
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalTendersPublishedByDivision")
    public WSResponseObject getTotalTendersPublishedByDivision(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalTendersPublishedByDivision : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalTendersPublishedByDivision, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalTendersPublishedByDivision,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalTendersPublishedByDivisionList = new ArrayList<String>();
            String totalTendersPublishedByDivision = null;
            try {
                List<SPCommonSearchDataMore> tendersDataList = getRequiredTendersData();
                if (tendersDataList != null && (tendersDataList.isEmpty() != true)) {
                    totalTendersPublishedByDivision = tendersDataList.get(0).getFieldName4();
                }
            } catch (Exception e) {
                LOGGER.error("getTotalTendersPublishedByDivision : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalTendersPublishedByDivision != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalTendersPublishedByDivisionList.add(totalTendersPublishedByDivision);
                wSResponseObject.setResponseList(totalTendersPublishedByDivisionList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalTendersPublishedByDivision : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /**
     * This method gives the total number of tenders being processed by Division.
     * @param username
     * @param password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalTendersProcessedByDivision")
    public WSResponseObject getTotalTendersProcessedByDivision(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalTendersProcessedByDivision : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalTendersProcessedByDivision, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalTendersProcessedByDivision,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalTendersProcessedByDivisionList = new ArrayList<String>();
            String totalTendersProcessedByDivision = null;
            try {
                List<SPCommonSearchDataMore> tendersDataList = getRequiredTendersData();
                if (tendersDataList != null && (tendersDataList.isEmpty() != true)) {
                    totalTendersProcessedByDivision = tendersDataList.get(0).getFieldName5();
                }
            } catch (Exception e) {
                LOGGER.error("getTotalTendersProcessedByDivision : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalTendersProcessedByDivision != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalTendersProcessedByDivisionList.add(totalTendersProcessedByDivision);
                wSResponseObject.setResponseList(totalTendersProcessedByDivisionList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalTendersProcessedByDivision : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }
    /**
     * This method gives the total number of contracts awarded by Division.
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalContractsAwardedByDivision")
    public WSResponseObject getTotalContractsAwardedByDivision(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalContractsAwardedByDivision : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalContractsAwardedByDivision, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalContractsAwardedByDivision,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalContractsAwardedByDivisionList = new ArrayList<String>();
            String totalContractsAwardedByDivision = null;
            try {
                List<SPCommonSearchDataMore> tendersDataList = getRequiredTendersData();
                if (tendersDataList != null && (tendersDataList.isEmpty() != true)) {
                    totalContractsAwardedByDivision = tendersDataList.get(0).getFieldName6();
                }
            } catch (Exception e) {
                LOGGER.error("getTotalContractsAwardedByDivision : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalContractsAwardedByDivision != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalContractsAwardedByDivisionList.add(totalContractsAwardedByDivision);
                wSResponseObject.setResponseList(totalContractsAwardedByDivisionList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalContractsAwardedByDivision : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }
    /**
     * This method gives the total number of tenders published by Organization.
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalTendersPublishedByOrganization")
    public WSResponseObject getTotalTendersPublishedByOrganization(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalTendersPublishedByOrganization : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalTendersPublishedByOrganization, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalTendersPublishedByOrganization,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalTendersPublishedByOrganizationList = new ArrayList<String>();
            String totalTendersPublishedByOrganization = null;
            try {
                List<SPCommonSearchDataMore> tendersDataList = getRequiredTendersData();
                if (tendersDataList != null && (tendersDataList.isEmpty() != true)) {
                    totalTendersPublishedByOrganization = tendersDataList.get(0).getFieldName7();
                }
            } catch (Exception e) {
                LOGGER.error("getTotalTendersPublishedByOrganization : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalTendersPublishedByOrganization != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalTendersPublishedByOrganizationList.add(totalTendersPublishedByOrganization);
                wSResponseObject.setResponseList(totalTendersPublishedByOrganizationList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalTendersPublishedByOrganization : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }
    /**
     * This method gives the total number of tenders being processed by Organization.
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalTendersProcessedByOrganization")
    public WSResponseObject getTotalTendersProcessedByOrganization(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalTendersProcessedByOrganization : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalTendersProcessedByOrganization, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalTendersProcessedByOrganization,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalTendersProcessedByOrganizationList = new ArrayList<String>();
            String totalTendersProcessedByOrganization = null;
            try {
                List<SPCommonSearchDataMore> tendersDataList = getRequiredTendersData();
                if (tendersDataList != null && (tendersDataList.isEmpty() != true)) {
                    totalTendersProcessedByOrganization = tendersDataList.get(0).getFieldName8();
                }
            } catch (Exception e) {
                LOGGER.error("getTotalTendersProcessedByOrganization : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalTendersProcessedByOrganization != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalTendersProcessedByOrganizationList.add(totalTendersProcessedByOrganization);
                wSResponseObject.setResponseList(totalTendersProcessedByOrganizationList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalTendersProcessedByOrganization : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }
    /**
     * This method gives the total number of contracts awarded by Organization.
     * @param String username
     * @param String password
     * @return WSResponseObject with list-data as List<String>
     */
    @WebMethod(operationName = "getTotalContractsAwardedByOrganization")
    public WSResponseObject getTotalContractsAwardedByOrganization(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password) {
        LOGGER.debug("getTotalContractsAwardedByOrganization : " + logUserId + LOGGERSTART);
        WSResponseObject wSResponseObject = new WSResponseObject();
        String searchKey = EMPTY_STRING;
        //insert record in log table
        WebServiceUtil.insertLogRecord(WS_ID_TotalContractsAwardedByOrganization, username, searchKey);
        //checking verification
        wSResponseObject = WebServiceUtil.initialVerification(WS_ID_TotalContractsAwardedByOrganization,
                WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE, username, password);
        boolean isListDataRequired = wSResponseObject.getIsListRequired();
        if (isListDataRequired) {
            List<String> totalContractsAwardedByOrganizationList = new ArrayList<String>();
            String totalContractsAwardedByOrganization = null;
            try {
                List<SPCommonSearchDataMore> tendersDataList = getRequiredTendersData();
                if (tendersDataList != null && (tendersDataList.isEmpty() != true)) {
                    totalContractsAwardedByOrganization = tendersDataList.get(0).getFieldName9();
                }
            } catch (Exception e) {
                LOGGER.error("getTotalContractsAwardedByOrganization : " + logUserId + e);
                wSResponseObject.setResponseConstant(WSResponseObject.EXCEPTION);
                wSResponseObject.setResponseMessage(WSResponseObject.EXCEPTION_MSG + e);
            }
            if (totalContractsAwardedByOrganization != null) {
                wSResponseObject.setResponseConstant(WSResponseObject.SUCCESS);
                wSResponseObject.setResponseMessage(WSResponseObject.SUCCESS_MSG);
                totalContractsAwardedByOrganizationList.add(totalContractsAwardedByOrganization);
                wSResponseObject.setResponseList(totalContractsAwardedByOrganizationList);
            } else {
                wSResponseObject.setResponseConstant(WSResponseObject.NO_RECORDS_FOUND);
                wSResponseObject.setResponseMessage(WSResponseObject.NO_RECORDS_FOUND_MSG);
            }
        }
        LOGGER.debug("getTotalContractsAwardedByOrganization : " + logUserId + LOGGEREND);
        return wSResponseObject;
    }

    /**
     * This method return the required data of tenders as a list of objects
     * @return List<SPCommonSearchDataMore>
     */
    private List<SPCommonSearchDataMore> getRequiredTendersData(){
        LOGGER.debug("getRequiredTendersData : " + logUserId + LOGGERSTART);
        List<SPCommonSearchDataMore> appInfoList = new ArrayList<SPCommonSearchDataMore>();
        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService)
                AppContext.getSpringBean(WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE);
        appInfoList = commonSearchDataMoreService.getCommonSearchData("GetMDOTenderCount",EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING);
        LOGGER.debug("getRequiredTendersData : " + logUserId + LOGGEREND);
        return appInfoList;
    }

    /**
     * This method return the required data of APPs as a list of objects
     * @return List<SPCommonSearchDataMore>
     */
    private List<SPCommonSearchDataMore> getRequiredAPPInfoData(){
        LOGGER.debug("getRequiredAPPInfoData : " + logUserId + LOGGERSTART);
        List<SPCommonSearchDataMore> appInfoList = new ArrayList<SPCommonSearchDataMore>();
        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean(WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE);
        appInfoList = commonSearchDataMoreService.getCommonSearchData("GetAppCount",EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,EMPTY_STRING, 
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,EMPTY_STRING);
        LOGGER.debug("getRequiredAPPInfoData : " + logUserId + LOGGEREND);
        return appInfoList;
    }

    /**
     * This method return the required development partners as a list of objects
     * @return List<SPCommonSearchDataMore>
     */
    private List<SPCommonSearchDataMore> getRequiredRegisteredDevelopmentPartnersList() {
        LOGGER.debug("getRequiredRegisteredDevelopmentPartnersList : " + logUserId + LOGGERSTART);
        List<SPCommonSearchDataMore> reportList = new ArrayList<SPCommonSearchDataMore>();
        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService)
                AppContext.getSpringBean(WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE);
        reportList = commonSearchDataMoreService.getCommonSearchData("GeteGPBankInfo", "Development",
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING);
        LOGGER.debug("getRequiredRegisteredDevelopmentPartnersList : " + logUserId + LOGGEREND);
        return reportList;
    }

    /**
     * This method return the required registered banks & branches as a list of objects
     * @return List<SPCommonSearchDataMore>
     */
    private List<SPCommonSearchDataMore> getRequiredRegisteredBanksAndBranchesList() {
        LOGGER.debug("getRequiredRegisteredBanksAndBranchesList : " + logUserId + LOGGERSTART);
        List<SPCommonSearchDataMore> reportList = new ArrayList<SPCommonSearchDataMore>();
        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService)
                AppContext.getSpringBean(WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE);
        reportList = commonSearchDataMoreService.getCommonSearchData("GeteGPBankInfo", "ScheduleBank",
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING);
        LOGGER.debug("getRequiredRegisteredBanksAndBranchesList : " + logUserId + LOGGEREND);
        return reportList;
    }

    /**
     * This method return the required reports as a list of objects
     * @return List<SPCommonSearchDataMore>
     */
    private List<SPCommonSearchDataMore> getRequiredReportData() {
        LOGGER.debug("getRequiredReportData : " + logUserId + LOGGERSTART);
        List<SPCommonSearchDataMore> reportList = new ArrayList<SPCommonSearchDataMore>();
        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean(WS_SEARCH_SERVICE_NAME_COMMON_SEARCH_DATA_MORE_SERVICE);
        reportList = commonSearchDataMoreService.getCommonSearchData("GetMISTwo", EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING,
                EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING, EMPTY_STRING);
        LOGGER.debug("getRequiredReportData : " + logUserId + LOGGEREND);
        return reportList;
    }
}
