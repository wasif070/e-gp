/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore;
import com.cptu.egp.eps.dao.storedprocedure.SpgetCommonSearchDataMore;
import com.cptu.egp.eps.model.table.TblBidDocuments;
import com.cptu.egp.eps.model.table.TblCompanyDocuments;
import com.cptu.egp.eps.model.table.TblTendererFolderMaster;
import com.cptu.egp.eps.model.table.TblTendererMaster;
import com.cptu.egp.eps.service.serviceimpl.BriefcaseDocImpl;
import com.cptu.egp.eps.web.databean.TempCompanyDocumentsDtBean;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.FileEncryptDecryptUtil;
import com.cptu.egp.eps.web.utility.FilePathUtility;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import com.cptu.eps.service.audit.AuditTrail;
import com.cptu.eps.service.audit.EgpModule;
import com.cptu.eps.service.audit.MakeAuditTrailService;
import com.cptu.egp.eps.web.utility.FilePathUtility;
/**
 *
 * @author parag
 */
//@WebServlet(name = "DocumentBriefcaseSrBean", urlPatterns = {"/DocumentBriefcaseSrBean"})
public class DocumentBriefcaseSrBean extends HttpServlet
{

    /**this servlet is used for handling request of commondoclib.jsp and DocfolderMgt.jsp
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final String DESTINATION_DIR_PATH = FilePathUtility.getFilePath().get("FileUploadServlet");
    private File destinationDir;
    private BriefcaseDocImpl briefcaseDocImpl = (BriefcaseDocImpl) AppContext.getSpringBean("BriefcaseDocImpl");
    private SpgetCommonSearchDataMore tenderCommonService = (SpgetCommonSearchDataMore) AppContext.getSpringBean("SpgetCommonSearchDataMore");
    private static final String CELLSTARTS = "<cell><![CDATA[";
    private static final String CELLENDS = "]]></cell>";
    private static final String CELL1 = "<cell>";
    private static final String CELL2 = "</cell>";
    private static final Logger LOGGER = Logger.getLogger(DocumentBriefcaseSrBean.class);
    private String logUserId = "0";
    /**
     * 
     */
    public static final String REALPATH = FilePathUtility.getFilePath().get("FileUploadServlet");
    public static final String REALPATHSBD = FilePathUtility.getFilePath().get("SBDs");
    public static final String REALPATH_MANUALS = FilePathUtility.getFilePath().get("Manuals");

    /**
     * Servlet Starting
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getHeader("referer"));
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

        String idType=null;
        int auditId=0;
        String auditAction=null;
        String moduleName=EgpModule.CDL.getName();
        String remarks="";

        response.setContentType("text/xml;charset=UTF-8");
        
        boolean _search = Boolean.parseBoolean(request.getParameter("_search"));
        String work = request.getParameter("work");
        String action = request.getParameter("action");
        String type = request.getParameter("type");
        String sord = request.getParameter("sord");
        String sidx = request.getParameter("sidx");
        String rows = request.getParameter("rows");
        String page = request.getParameter("page");

        String msg = request.getParameter("msg");

        System.out.println("userTypeId "+session.getAttribute("userTypeId"));
        int userId = 0;
        if (session.getAttribute("userId") != null)
        {
            userId = Integer.parseInt(session.getAttribute("userId").toString());
            logUserId = "" + userId;
            briefcaseDocImpl.setLogUserId(logUserId);
            //tenderCommonService.setLogUserId(logUserId);
        }
//        else{
//            userId = Integer.parseInt(request.getParameter("userId").toString());
//        }

        if (work != null)
        {
            LOGGER.debug(work + " : " + logUserId + " Starts");

        }
        if (action != null)
        {
            LOGGER.debug(action + " : " + logUserId + " Starts");

        }
        if (type != null)
        {
            LOGGER.debug(type + " : " + logUserId + " Starts");

        }
        int folderId = 0;
        if (request.getParameter("folderId") != null)
        {
            folderId = Integer.parseInt(request.getParameter("folderId"));
        }
        int formId = 0;
        if (request.getParameter("formId") != null)
        {
            formId = Integer.parseInt(request.getParameter("formId"));
        }
        int tenderId = 0;
        if (request.getParameter("tenderId") != null)
        {
            tenderId = Integer.parseInt(request.getParameter("tenderId"));
        }

        LOGGER.debug(" page is ::: " + page);
//        String sord = request.getParameter("sord");
//        String sidx = request.getParameter("sidx");
//        String rowList = request.getParameter("rowList");
        //String queryString="";
        //String pageName=request.getHeader("referer").substring(request.getHeader("referer").lastIndexOf("/")+1,request.getHeader("referer").lastIndexOf(".jsp")+4);        
        try
        {
            /*handling request for unmapped documents and mapped documents */
            if ("fetchunmapped".equalsIgnoreCase(work) || "mapDocForm".equalsIgnoreCase(work))
            {
                PrintWriter out = response.getWriter();

                // response.setContentType("text/xml;charset=UTF-8");
                try
                {
                    //LOGGER.debug(" folder Id is ::::::::::::::"+folderId);
                    List<SPCommonSearchDataMore> bidderDocDetail = null;
                    if ("".equalsIgnoreCase(sidx))
                    {
                        if ("getfoldersWithPaging".equalsIgnoreCase(action))
                        {
                            sidx = "folderName";
                            sord = "desc";
                        }
                        else
                        {
                            sidx = "uploadedDate";
                            sord = "desc";
                        }
                    }
                    if(sidx.equalsIgnoreCase("documentSize"))
                    {
                        sidx = "LEN(documentSize)";
                    }
                    sord = "Order by " + sidx + " " + sord;
                    // if ("fetchunmapped".equalsIgnoreCase(work)) {

                    /*for jqgrid search functionality*/
                    if (_search)
                    {
                        String searchField = request.getParameter("searchField");
                        String searchString = request.getParameter("searchString");
                        String searchOper = request.getParameter("searchOper");
                        if ("cn".equalsIgnoreCase(searchOper))
                        {
                            if ("GetAllDocumentsWithPaging".equalsIgnoreCase(action))
                            {
                                if ("folderName".equalsIgnoreCase(searchField))
                                {
                                    searchOper = "folderId in (select folderId from tbl_TendererFolderMaster where folderName like '%" + searchString + "%')";
                                }
                                else
                                {
                                    searchOper = searchField + " Like '%" + searchString + "%'";
                                }
                            }
                            else
                            {
                                searchOper = searchField + " Like '%" + searchString + "%'";
                            }
                            //if("maptofolder".equalsIgnoreCase(searchField)){
                            //   searchOper = folderId+" Like '%"+searchString+"%'";
                            //}

                        }
                        else
                        {
                            if ("GetAllDocumentsWithPaging".equalsIgnoreCase(action))
                            {
                                if ("folderName".equalsIgnoreCase(searchField))
                                {
                                    searchOper = "folderId in (select folderId from tbl_TendererFolderMaster where folderName = '" + searchString + "')";
                                }
                                else
                                {
                                    searchOper = searchField + " = '" + searchString + "'";
                                }
                            }
                            else
                            {
                                searchOper = searchField + " = '" + searchString + "'";
                            }
                        }
                        bidderDocDetail = tenderCommonService.executeProcedure("" + action, "" + folderId, "" + userId, page, rows, searchOper, sord, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    }
                    else
                    {
                        if ("getfoldersWithPaging".equalsIgnoreCase(action))
                        {
                            //sord = "Order by folderId desc";
                        }
                        bidderDocDetail = tenderCommonService.executeProcedure("" + action, "" + folderId, "" + userId, page, rows, "", sord, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    }
                    /*} else if ("mapDocForm".equalsIgnoreCase(work)) {
                    if ("GetBidderDocumentsWithPaging".equalsIgnoreCase(action)) {//taher
                    bidderDocDetail = tenderCommonService.searchData("" + action, "" + formId, "" + userId, page, rows, "", sord, null, null, null);
                    } else {
                    bidderDocDetail = tenderCommonService.searchData("" + action, "" + folderId, "" + userId, page, rows, "", sord, null, null, null);
                    }
                    }*/
                    /*handling request for displaying data in jq grid */
                    int totalPages = 0;
                    long totalCount = 0;
                    if (bidderDocDetail != null && !bidderDocDetail.isEmpty())
                    {
                        totalCount = Integer.parseInt(bidderDocDetail.get(0).getFieldName12());
                    }
                    //long totalCount  = bidderDocDetail.size();
                    List<SPCommonSearchDataMore> list2 = new ArrayList<SPCommonSearchDataMore>();
                    int j = 0;
                    int no = Integer.parseInt(page);
                    int recordOffset = (Integer.parseInt(rows));
                    if (no == 1)
                    {
                        j = 1;
                    }
                    else
                    {
                        if (recordOffset == 45)
                        {
                            j = ((no - 1) * 45) + 1;
                        }
                        else if (recordOffset == 30)
                        {
                            j = ((no - 1) * 30) + 1;
                        }
                        else
                        {
                            j = ((no - 1) * 15) + 1;
                        }
                    }

                    for (int i = 0; i < bidderDocDetail.size(); i++)
                    {
                        list2.add(bidderDocDetail.get(i));
                    }

                    LOGGER.debug("totalCount : " + totalCount);
                    if (totalCount > 0)
                    {
                        if (totalCount % Integer.parseInt(rows) == 0)
                        {
                            totalPages = (int) (totalCount / Integer.parseInt(rows));
                        }
                        else
                        {
                            totalPages = (int) ((totalCount / Integer.parseInt(rows)) + 1);
                        }
                    }
                    else
                    {
                        totalPages = 0;
                    }

                    out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                    out.print("<rows>");
                    out.print("<page>" + request.getParameter("page") + "</page>");

                    out.print("<total>" + totalPages + "</total>");
                    out.print("<records>" + bidderDocDetail.size() + "</records>");
                    /*if(totalCount!=0){*/
                    int int_cnt = 0;
                    if (!list2.isEmpty())
                    {
                        for (SPCommonSearchDataMore sPTenderCommonData : list2)
                        {
                            int_cnt++;
                            LOGGER.debug("  bidderDocDetail :: " + bidderDocDetail.size());
                            int mapDocId = 0;
                            String mapForm[] = null;
                            if (sPTenderCommonData.getFieldName7() != null && !"null".equalsIgnoreCase(sPTenderCommonData.getFieldName7()))
                            {
                                mapDocId = Integer.parseInt(sPTenderCommonData.getFieldName7());
                                LOGGER.debug("  mapDocID :: " + Integer.parseInt(sPTenderCommonData.getFieldName7()));
                            }

                            if (sPTenderCommonData.getFieldName10() != null && !"null".equalsIgnoreCase(sPTenderCommonData.getFieldName10()))
                            {
                                mapForm = sPTenderCommonData.getFieldName10().split(",");

                                // mappedFormId  = sPTenderCommonData.getFieldName10();
                                // LOGGER.debug("  getFieldName10 :: "+sPTenderCommonData.getFieldName10());
                            }

                            //LOGGER.debug(" map doc Id is :::: "+mapDocId);

                            String checkBox = "";
                            //sPTenderCommonData.getFieldName1()
                            //LOGGER.debug(" formid  ::: mappedFormId ::"+formId +" == "+ mappedFormId);
                            out.print("<row id='" + sPTenderCommonData.getFieldName1() + "'>");
                            if ("mapDocForm".equalsIgnoreCase(work) || "chkallowed".equalsIgnoreCase(msg))
                            {
//                                if ("GetBidderDocuments".equalsIgnoreCase(action)) {
//                                checkBox = " <input type='checkbox' name='chkComDocId' id='chkComDocId' value='" + sPTenderCommonData.getFieldName1() + "' onclick=\"checkBoxClick('" + sPTenderCommonData.getFieldName1() + "')\"   />";
//                                } else if (!"GetArchiveDocumentsWithPaging".equalsIgnoreCase(action)) {
//                                    boolean mapFlag = true;
//                                    if (mapForm == null) {
//                                        checkBox = " <input type='checkbox' name='chkComDocId' id='chkComDocId' value='" + sPTenderCommonData.getFieldName1() + "' onclick=\"checkBoxClick('" + sPTenderCommonData.getFieldName1() + "')\"   />";
//
//                                    } else {
//                                        if (mapForm.length > 0) {
//                                            for (int m = 0; m < mapForm.length; m++) {
//                                                if (mapForm[m].contains("" + formId)) {
//                                                    mapFlag = false;
//                                               break;
//                                            }
//                                         }
//                                            if (mapFlag) {
//                                                // LOGGER.debug(" mapForm in if  length is :: "+mapForm.length);
//                                              checkBox = " <input type='checkbox' name='chkComDocId' id='chkComDocId' value='" + sPTenderCommonData.getFieldName1() + "' onclick=\"checkBoxClick('" + sPTenderCommonData.getFieldName1() + "')\"   />";
//                                            } else {
//                                                LOGGER.debug(" mapForm in else length is :: " + mapForm.length);
//                                         }
//                                        } else {
//                                        checkBox = " <input type='checkbox' name='chkComDocId' id='chkComDocId' value='" + sPTenderCommonData.getFieldName1() + "' onclick=\"checkBoxClick('" + sPTenderCommonData.getFieldName1() + "')\"   />";
//                                    }
//                                }
//
//                            }
                                checkBox = " <input type='checkbox' name='chkComDocId' id='chkComDocId' value='" + sPTenderCommonData.getFieldName1() + "' onclick=\"checkBoxClick('" + sPTenderCommonData.getFieldName1() + "')\"   />";
                                out.print(CELLSTARTS + checkBox + CELLENDS);

                            }
                            out.print(CELL1 + (j++) + CELL2);
                            out.print(CELLSTARTS + sPTenderCommonData.getFieldName2() + CELLENDS);
                            if ("mapDocForm".equalsIgnoreCase(work))
                            {
                                if ("GetBidderDocuments".equalsIgnoreCase(action))
                                {
                                    out.print(CELLSTARTS + sPTenderCommonData.getFieldName12() + CELLENDS);
                                }
                            }



                            int docId = Integer.parseInt(sPTenderCommonData.getFieldName1());
                            int size = 0;
                            String fileName = sPTenderCommonData.getFieldName2();
                            String linkMap = "";
                            String linkDelete = "";
                            String linkCancel = "";
                            String linkArchive = "";
                            String linkDownloadDoc = "";
                            String viewFile = "";

                            /*handling request for unmapped documents */
                            if ("unmap".equalsIgnoreCase(type))
                            {
                                size = Integer.parseInt(sPTenderCommonData.getFieldName3());
                                out.print(CELLSTARTS + sPTenderCommonData.getFieldName4() + CELLENDS);
                                if (sPTenderCommonData.getFieldName13() != null && !"".equalsIgnoreCase(sPTenderCommonData.getFieldName13()))
                                {
                                    out.print(CELLSTARTS + sPTenderCommonData.getFieldName13() + CELLENDS);
                                }
                                else
                                {
                                    out.print(CELLSTARTS + "-" + CELLENDS);
                                }
                                out.print(CELLSTARTS + Long.parseLong(sPTenderCommonData.getFieldName3()) / 1024 + CELLENDS);

                                linkDelete = "<a href='#' onclick=\"deleteFile(" + docId + ",'" + fileName + "','" + action + "')\" >Delete</a>";
                                linkCancel = "<a href='#' onclick='cancelFile(" + docId + ")' >Cancel</a>";
                                linkArchive = "<a href='#' onclick=\"archiveFile(" + docId + ",'" + action + "')\" >Archive</a>";
                                linkDownloadDoc = "<a href='#' onclick=\"downloadFile('" + fileName + "','" + size + "')\" >Download</a>";

//                            int mapWithFolderId = 0;
//                                if (sPTenderCommonData.getFieldName8() != null && !"null".equalsIgnoreCase(sPTenderCommonData.getFieldName8())) {
//                                mapWithFolderId = Integer.parseInt(sPTenderCommonData.getFieldName8());
//                            }
//                                if (folderId == 0 && mapWithFolderId == 0) {
//                                linkMap = "<a href='#' onclick=\"window.open('MapFileToFolder.jsp?docId=" + docId + "&', 'mywindow' , 'screenX=0,screenY=0,toolbar=0,status=1,menubar=0,resizable=0,width=250,height=200,top=300,left=400')\"  >Map</a>";
//                                    out.print(CELLSTARTS + linkMap + CELLENDS);
//                                } else {
//                                linkMap = "<a href='#' onclick=\"window.open('MapFileToFolder.jsp?docId=" + docId + "&', 'mywindow' , 'screenX=0,screenY=0,toolbar=0,status=1,menubar=0,resizable=0,width=250,height=200,top=300,left=400')\"  >Move to Folder</a>";
//                                    out.print(CELLSTARTS + linkMap + CELLENDS);
//                            }
                                if ("GetAllDocumentsWithPaging".equalsIgnoreCase(action))
                                {
                                    out.print(CELLSTARTS + sPTenderCommonData.getFieldName9() + CELLENDS);
                                }
                                out.print(CELLSTARTS + sPTenderCommonData.getFieldName5() + CELLENDS);
//                            if("GetAllDocuments".equalsIgnoreCase(action)){
//                               out.print(CELL1 + sPTenderCommonData.getFieldName6() + CELL2);
//
//                            }

                                LOGGER.debug("first time data is::: " + sPTenderCommonData.getFieldName11());
                                LOGGER.debug("action is" + action);
                                out.print("<cell>");
                                if ((!("compreg".equals(sPTenderCommonData.getFieldName11().trim()) || "autletter".equals(sPTenderCommonData.getFieldName11().trim()) || "finautocert".equals(sPTenderCommonData.getFieldName11().trim()) || "icard".equals(sPTenderCommonData.getFieldName11().trim()) || "natid".equals(sPTenderCommonData.getFieldName11().trim()) || "natpassid".equals(sPTenderCommonData.getFieldName11().trim()) || "passphoto".equals(sPTenderCommonData.getFieldName11().trim()) || "regfee".equals(sPTenderCommonData.getFieldName11().trim()) || "regwirefee".equals(sPTenderCommonData.getFieldName11().trim()) || "statubody".equals(sPTenderCommonData.getFieldName11().trim()) || "tincert".equals(sPTenderCommonData.getFieldName11().trim()) || "tradlic".equals(sPTenderCommonData.getFieldName11().trim()) || "vatcert".equals(sPTenderCommonData.getFieldName11().trim()) || "authletter".equals(sPTenderCommonData.getFieldName11().trim()))) && sPTenderCommonData.getFieldName7() == null)//if(mapDocId == 0)//
                                {
                                    //out.print(<" ![CDATA[" + linkDelete + "]]> |");
                                    out.print(" <![CDATA[" + linkDelete + "]]> |");
                                }
                                //out.print(" | <![CDATA[" + linkCancel+ "]]>  ");
                                if (!"archive".equalsIgnoreCase(sPTenderCommonData.getFieldName6()))
                                {
                                    //out.print(CELLSTARTS + linkArchive + CELLENDS);
                                    out.print("  <![CDATA[" + linkArchive + "]]> | ");
                                }
                                out.print("  <![CDATA[" + "<br/>" + linkDownloadDoc + "]]> | ");
                                int mapWithFolderId = 0;
                                if (sPTenderCommonData.getFieldName8() != null && !"null".equalsIgnoreCase(sPTenderCommonData.getFieldName8()))
                                {
                                    mapWithFolderId = Integer.parseInt(sPTenderCommonData.getFieldName8());
                                }
                                if (folderId == 0 && mapWithFolderId == 0)
                                {
                                    linkMap = "<a href='#' onclick=\"window.open('MapFileToFolder.jsp?docId=" + docId + "&', 'mywindow' , 'screenX=0,screenY=0,toolbar=0,status=1,menubar=0,resizable=0,width=250,height=200,top=300,left=400')\"  >Map to Folder</a>";
                                    out.print("  <![CDATA[" + linkMap + "]]> ");
                                    //out.print(CELLSTARTS + linkMap + CELLENDS);
                                }
                                else
                                {
                                    linkMap = "<a href='#' onclick=\"window.open('MapFileToFolder.jsp?docId=" + docId + "&', 'mywindow' , 'screenX=0,screenY=0,toolbar=0,status=1,menubar=0,resizable=0,width=250,height=200,top=300,left=400')\"  >Move to Folder</a>";
                                    out.print("  <![CDATA[" + linkMap + "]]> ");
                                    //out.print(CELLSTARTS + linkMap + CELLENDS);
                                }
                                //out.print(CELLSTARTS + linkDownloadDoc + CELLENDS);
                                out.print("</cell>");
                            }
                            else if ("folderwise".equalsIgnoreCase(type))
                            {
                                LOGGER.debug(" in folder wise =====");
                                viewFile = "<a href='#' onclick=\"viewFile('" + docId + "','" + fileName + "')\" >View</a>";
                                //out.print("<cell> - </cell>");
                                //out.print(CELL1);
                                out.print(CELLSTARTS + viewFile + CELLENDS);
                                // out.print(CELL2);
                            }
                            else if ("mappedWithForm".equalsIgnoreCase(type))
                            {
                                size = Integer.parseInt(sPTenderCommonData.getFieldName3());
                                out.print(CELLSTARTS + sPTenderCommonData.getFieldName4() + CELLENDS);
                                out.print(CELLSTARTS + Long.parseLong(sPTenderCommonData.getFieldName3()) / 1024 + CELLSTARTS);

                                linkDownloadDoc = "<a href='#' onclick=\"downloadFile('" + fileName + "','" + size + "')\" >Download</a>";
                                out.print(CELLSTARTS + sPTenderCommonData.getFieldName5() + CELLSTARTS);
                                out.print(CELLSTARTS + linkDownloadDoc + CELLENDS);
                            }
                            else
                            {
                                //linkArchive = "<a href='#' onclick='archiveFile("+docId+")' >Archive</a>";
                                size = Integer.parseInt(sPTenderCommonData.getFieldName3());
                                linkDownloadDoc = "<a href='#' onclick=\"downloadFile('" + fileName + "','" + size + "')\" >Download</a>";
                                out.print(CELLSTARTS + sPTenderCommonData.getFieldName4() + CELLENDS);
                                if (!"".equalsIgnoreCase(sPTenderCommonData.getFieldName13()))
                                {
                                    out.print(CELLSTARTS + sPTenderCommonData.getFieldName13() + CELLENDS);
                                }
                                else
                                {
                                    out.print(CELLSTARTS + "-" + CELLENDS);
                                }
                                out.print(CELL1 + Long.parseLong(sPTenderCommonData.getFieldName3()) / 1024 + CELL2);
                                out.print(CELL1 + sPTenderCommonData.getFieldName5() + CELL2);
                                // out.print(CELL1 + sPTenderCommonData.getFieldName6() + CELL2);
                                //out.print("<cell> ");
                                out.print(CELLSTARTS + linkDownloadDoc + CELLENDS);
                                //out.print(" | <![CDATA[" + linkArchive + "]]> ");
                                //out.print(" </cell> ");
                            }
                            out.print("</row>");
                        }
                        LOGGER.debug("cnt" + int_cnt);
                    }
                    else
                    {
//                        out.print("<row>");
//                        out.print("<cell> 1 </cell>");
//                        out.print("<cell> No Records Found. </cell>");
//                        out.print("<cell> No Records Found. </cell>");
//                        out.print("<cell> No Records Found. </cell>");
//                        out.print("<cell> No Records Found. </cell>");
//                        out.print("<cell> No Records Found. </cell>");
//                        out.print("<cell> No Records Found. </cell>");
//                        out.print("<cell> No Records Found. </cell>");
//                        out.print("</row>");
                    }

                    /*}else
                    {
                    out.print(CELLSTARTS+"No Records Found."+CELLENDS);
                    }*/
                    out.print("</rows>");

                }
                catch (Exception ex)
                {
                    LOGGER.error("error is::" + ex);
                }
                finally
                {
                    out.flush();
                    out.close();
                }
            } /*else if("mapCompDocFromCheck".equalsIgnoreCase(work)){
            
            
            }*/
            /*handling request for displaying mapped documents data to jq grid*/

            else if ("mapDocForm1".equalsIgnoreCase(work))
            {
                PrintWriter out = response.getWriter();

                response.setContentType("text/xml;charset=UTF-8");
                try
                {
                    //LOGGER.debug(" folder Id is ::::::::::::::"+folderId);
                    List<SPCommonSearchDataMore> bidderDocDetail = tenderCommonService.executeProcedure("" + action, "" + folderId, "" + userId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                    int totalPages = 0;
                    long totalCount = bidderDocDetail.size();
                    LOGGER.debug("totalCount : " + totalCount);
                    if (totalCount > 0)
                    {
                        if (totalCount % Integer.parseInt(rows) == 0)
                        {
                            totalPages = (int) (totalCount / Integer.parseInt(rows));
                        }
                        else
                        {
                            totalPages = (int) ((totalCount / Integer.parseInt(rows)) + 1);
                        }
                    }
                    else
                    {
                        totalPages = 0;
                    }

                    out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                    out.print("<rows>");
                    out.print("<page>" + request.getParameter("page") + "</page>");

                    out.print("<total>" + totalPages + "</total>");
                    out.print("<records>" + bidderDocDetail.size() + "</records>");
                    int i = 1;
                    if (!bidderDocDetail.isEmpty())
                    {
                        for (SPCommonSearchDataMore sPTenderCommonData : bidderDocDetail)
                        {

                            int mapDocId = 0;
                            if (sPTenderCommonData.getFieldName7() != null && !"null".equalsIgnoreCase(sPTenderCommonData.getFieldName7()))
                            {
                                mapDocId = Integer.parseInt(sPTenderCommonData.getFieldName7());
                            }

                            LOGGER.debug(" map doc Id is :::: " + mapDocId);

                            out.print("<row id='" + sPTenderCommonData.getFieldName1() + "'>");
                            out.print(CELL1 + (i++) + CELL2);
                            out.print(CELL1 + sPTenderCommonData.getFieldName2() + CELL2);



                            int docId = Integer.parseInt(sPTenderCommonData.getFieldName1());
                            int size = 0;
                            String fileName = sPTenderCommonData.getFieldName2();
                            LOGGER.debug(" fileName ::: " + fileName);
                            String linkMap = "";
                            String linkDelete = "";
                            String linkCancel = "";
                            String linkArchive = "";
                            String linkDownloadDoc = "";
                            String viewFile = "";


                            if ("unmap".equalsIgnoreCase(type))
                            {
                                size = Integer.parseInt(sPTenderCommonData.getFieldName3());
                                out.print(CELL1 + sPTenderCommonData.getFieldName4() + CELL2);
                                out.print(CELL1 + Long.parseLong(sPTenderCommonData.getFieldName3()) / 1024 + CELL2);

                                linkDelete = "<a href='#' onclick=\"deleteFile(" + docId + ",'" + fileName + "','" + action + "')\" >Delete</a>";
                                linkCancel = "<a href='#' onclick='cancelFile(" + docId + ")' >Cancel</a>";
                                linkArchive = "<a href='#' onclick=\"archiveFile(" + docId + ",'" + action + "')\" >Archive</a>";
                                linkDownloadDoc = "<a href='#' onclick=\"downloadFile('" + fileName + "','" + size + "')\" >Download</a>";

                                int mapWithFolderId = 0;
                                if (sPTenderCommonData.getFieldName8() != null && !"null".equalsIgnoreCase(sPTenderCommonData.getFieldName8()))
                                {
                                    mapWithFolderId = Integer.parseInt(sPTenderCommonData.getFieldName8());
                                }
                                if (folderId == 0 && mapWithFolderId == 0)
                                {
                                    linkMap = "<a href='#' onclick=\"window.open('MapFileToFolder.jsp?docId=" + docId + "&', 'mywindow' , 'screenX=0,screenY=0,toolbar=0,status=1,menubar=0,resizable=0,width=250,height=200,top=300,left=400')\"  >Map</a>";
                                    out.print(CELLSTARTS + linkMap + CELLENDS);
                                }
                                else
                                {
                                    linkMap = "<a href='#' onclick=\"window.open('MapFileToFolder.jsp?docId=" + docId + "&', 'mywindow' , 'screenX=0,screenY=0,toolbar=0,status=1,menubar=0,resizable=0,width=250,height=200,top=300,left=400')\"  >Move to Folder</a>";
                                    out.print(CELLSTARTS + linkMap + CELLENDS);
                                }
                                if ("GetAllDocuments".equalsIgnoreCase(action))
                                {
                                    out.print(CELLSTARTS + sPTenderCommonData.getFieldName9() + CELLENDS);
                                }
                                out.print(CELL1 + sPTenderCommonData.getFieldName5() + CELL2);
//                            if("GetAllDocuments".equalsIgnoreCase(action)){
//                               out.print(CELL1 + sPTenderCommonData.getFieldName6() + CELL2);
//
//                            }

                                out.print("<cell> ");
                                if (mapDocId == 0)
                                {
                                    out.print(" <![CDATA[" + linkDelete + "]]> ");
                                }
                                //out.print(" | <![CDATA[" + linkCancel+ "]]>  ");
                                if (!"archive".equalsIgnoreCase(sPTenderCommonData.getFieldName6()))
                                {
                                    out.print(" | <![CDATA[" + linkArchive + "]]> ");
                                }
                                out.print(" | <![CDATA[" + linkDownloadDoc + "]]> ");
                                out.print(" </cell> ");
                            }
                            else if ("folderwise".equalsIgnoreCase(type))
                            {
                                LOGGER.debug(" in folder wise =====");
                                viewFile = "<a href='#' onclick=\"viewFile('" + docId + "','" + fileName + "')\" >View</a>";
                                //out.print("<cell> - </cell>");
                                out.print("<cell> ");
                                out.print(" <![CDATA[" + viewFile + "]]> ");
                                out.print(" </cell> ");
                            }
                            else
                            {
                                linkArchive = "<a href='#' onclick='archiveFile(" + docId + ")' >Archive</a>";
                                linkDownloadDoc = "<a href='#' onclick=\"downloadFile('" + fileName + "','" + size + "')\" >Download</a>";
                                size = Integer.parseInt(sPTenderCommonData.getFieldName3());
                                out.print(CELL1 + sPTenderCommonData.getFieldName4() + CELL2);
                                out.print(CELL1 + Long.parseLong(sPTenderCommonData.getFieldName3()) / 1024 + CELL2);
                                out.print(CELL1 + sPTenderCommonData.getFieldName5() + CELL2);
                                // out.print(CELL1 + sPTenderCommonData.getFieldName6() + CELL2);
                                out.print("<cell> ");
                                out.print(" <![CDATA[" + linkDownloadDoc + "]]> ");
                                //out.print(" | <![CDATA[" + linkArchive + "]]> ");
                                out.print(" </cell> ");
                            }
                            out.print("</row>");
                        }
                    }
                    else
                    {
                        out.print("<cell> No Records Found. </cell>");
                    }
                    out.print("</rows>");

                }
                catch (Exception ex)
                {
                    out.println(ex.toString());
                }
                finally
                {
                    out.flush();
                    out.close();
                }
                /*handling request for deleting documents*/
            }
            else if ("delete".equalsIgnoreCase(work))
            {
                PrintWriter out = response.getWriter();
                try
                {

                    LOGGER.debug(" in delete ::");
                    int docId = 0;
                    if (request.getParameter("docId") != null)
                    {
                        docId = Integer.parseInt(request.getParameter("docId"));
                    }

                    String fileName = request.getParameter("fileName");
                    File f = new File(REALPATH + userId + "\\" + fileName);
                    briefcaseDocImpl.deleteBriefcaseDocs(docId);
                    if (f.exists())
                    {
                        f.delete();
                        out.print(1);
                        out.flush();
                    }
                    LOGGER.debug("end in delete ::");

                }
                catch (Exception e)
                {
                    LOGGER.error("Error in file delete:::" + e);
                }
                finally
                {
                    out.flush();
                    out.close();
                }
                /*handling request for canceling documents*/
            }
            else if ("cancel".equalsIgnoreCase(work))
            {
                PrintWriter out = response.getWriter();
                try
                {
                    LOGGER.debug(" in cancel ::");
                    int docId = Integer.parseInt(request.getParameter("docId"));
                    briefcaseDocImpl.updateBriefcaseDocsStatus(docId, "Cancel");
                }
                catch (Exception e)
                {
                    LOGGER.error(" Error in file cancel:::" + e);
                }
                finally
                {
                    out.flush();
                    out.close();
                }
                /*handling request for moving documents to archive*/
            }
            else if ("archive".equalsIgnoreCase(work))
            {
                PrintWriter out = response.getWriter();
                //Seting values for Audit Trail
                 int docId = Integer.parseInt(request.getParameter("docId"));
                 
                idType="userId";
                auditId=Integer.parseInt(session.getAttribute("userId").toString());
                auditAction="Bidder has archived Document";
                moduleName=EgpModule.CDL.getName();
                remarks="Bidder(User id): "+auditId+" has archived Document Id: "+docId;
                try
                {
                    LOGGER.debug(" in Archive ::");
                   
                    briefcaseDocImpl.updateBriefcaseDocsStatus(docId, "Archive");
                }
                catch (Exception e)
                {
                     auditAction="Error in Bidder has archived Document "+e.getMessage();
                    LOGGER.error(" Error in file Archive:::" + e);
                }
                finally
                {
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);  
                    out.flush();
                    out.close();
                }
                /*handling request for downloading documents*/
            }
            else if ("download".equalsIgnoreCase(work))
            {
                boolean fromDocFolder = false;
                boolean fromMapDoc = false;
                boolean fromViewMapDoc = false;
                String fileSize = "0";
                if (request.getParameter("fileLen") != null)
                {
                    fileSize = request.getParameter("fileLen");
                }

                int uIdDoc = -1;
                if (request.getParameter("docUid") != null)
                {
                    uIdDoc = Integer.parseInt(request.getParameter("docUid"));
                }

                if (request.getParameter("fromDocFolder") != null)
                {
                    if ("true".equalsIgnoreCase(request.getParameter("fromDocFolder")))
                    {
                        fromDocFolder = true;
                    }
                }
                if (request.getParameter("fromMapDoc") != null)
                {
                    if ("true".equalsIgnoreCase(request.getParameter("fromMapDoc")))
                    {
                        fromMapDoc = true;
                    }
                }
                if (request.getParameter("fromViewMapDoc") != null)
                {
                    if ("true".equalsIgnoreCase(request.getParameter("fromViewMapDoc")))
                    {
                        fromViewMapDoc = true;
                    }
                }
                String fileName = request.getParameter("fileName");
                try
                {
                    File f;
                    if (uIdDoc == -1)
                    {
                        System.out.print(REALPATH + userId + "\\" + fileName);
                        f = new File(REALPATH + userId + "\\" + fileName);
                    }
                    else
                    {
                        f = new File(REALPATH + uIdDoc + "\\" + fileName);
                    }
                    LOGGER.debug(" file name is :: " + f.getName());
                    if (f.exists())
                    {
                        downloadFile(request, response, f, fileSize);
                    }
                    else
                    {
                        if (fromDocFolder)
                        {
                            response.sendRedirect("tenderer/DocFolderMgt.jsp?msg=0");
                            //response.sendRedirectFilter("DocFolderMgt.jsp?msg=0");
                        }
                        else if (fromMapDoc)
                        {
                            response.sendRedirect("tenderer/TendererFormDocMap.jsp?tenderId=" + tenderId + "&formId=" + formId);
                            //response.sendRedirect("TendererFormDocMap.jsp?tenderId=" + tenderId + "&formId=" + formId);
                        }
                        else if (fromViewMapDoc)
                        {
                            response.sendRedirect("tenderer/ViewMappedDoc.jsp?tenderId=" + tenderId + "&formId=" + formId);
                            //response.sendRedirect("ViewMappedDoc.jsp?tenderId=" + tenderId + "&formId=" + formId);
                        }
                        else if ("y".equals(request.getParameter("isInd")))
                        {
                            response.sendRedirect(request.getHeader("referer"));
                        }
                        else
                        {
                             String strResponse = request.getHeader("referer");
                             if(!strResponse.contains("dd=fl")){
                                strResponse  = (strResponse.indexOf("?") > 0? strResponse+"&dd=fl": strResponse+"?dd=fl");
                            }
                            response.sendRedirect(strResponse);
                            //response.sendRedirect("tenderer/CommonDocLib.jsp?msg=0");
                            //response.sendRedirect("CommonDocLib.jsp?msg=0");
                        }
                    }
                }
                catch (Exception e)
                {
                    LOGGER.error(" Error in download work =" + e);
                    e.printStackTrace();
                }
                /*handling request for creating documents */
            }
            else if ("downloadPrr".equalsIgnoreCase(work))
            {
                long fileSize;
                String fileName = request.getParameter("fileName");
                
                try
                {
                    File f;
                    f = new File(REALPATHSBD + fileName);
                    //f = new File("D:\\eGP\\Resources\\SBDs\\" + fileName);
                    fileSize = f.length();
                    LOGGER.debug(" file name is :: " + f.getName());
                    if (f.exists())
                    {
                        downloadGeneralFile(request, response, f, Long.toString(fileSize));
                    }
                    
                }
                catch (Exception e)
                {
                    LOGGER.error(" Error in download =" + e);
                    e.printStackTrace();
                }
                /*handling request for creating documents */
            }
            else if ("downloadManuals".equalsIgnoreCase(work))
            {
                long fileSize;
                String fileName = request.getParameter("fileName");
                
                try
                {
                    File f;
                    f = new File(REALPATH_MANUALS + fileName);
                    //f = new File("D:\\eGP\\Resources\\Manuals\\" + fileName);
                    fileSize = f.length();
                    LOGGER.debug(" file name is :: " + f.getName());
                    if (f.exists())
                    {
                        downloadGeneralFile(request, response, f, Long.toString(fileSize));
                    }
                    
                }
                catch (Exception e)
                {
                    LOGGER.error(" Error in download =" + e);
                    e.printStackTrace();
                }
                /*handling request for creating documents */
            }
            else if ("createFolder".equalsIgnoreCase(work))
            {

                int tendererId = 0;
                String folderName = "";
                boolean flag = false;
                if (request.getParameter("hidTendererId") != null)
                {
                    tendererId = Integer.parseInt(request.getParameter("hidTendererId"));
                }
                if (request.getParameter("txtFolderName") != null)
                {
                    folderName = request.getParameter("txtFolderName");
                }
                
                //Seting values for Audit Trail
                idType="userId";
                auditId=Integer.parseInt(session.getAttribute("userId").toString());
                auditAction="Bidder has created Folder";
                moduleName=EgpModule.CDL.getName();
                remarks="Bidder(User id): "+auditId+" has created Folder";
                
                try
                {
                    TblTendererFolderMaster folderMaster = new TblTendererFolderMaster();
                    folderMaster.setTblTendererMaster(new TblTendererMaster(tendererId));
                    folderMaster.setFolderName(folderName);
                    folderMaster.setCreationDate(new Date());
                    briefcaseDocImpl.addTendererFolderMaster(folderMaster);
                    flag = true;
                }
                catch (Exception e)
                {
                    auditAction="Error in Bidder has created Folder "+e.getMessage();
                    LOGGER.error(" briefcaseDocImpl ::: " + e);
                }
                finally
                {
                     makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);  
                }

                response.sendRedirect("tenderer/DocFolderMgt.jsp?flag=" + flag);
                //response.sendRedirectFilter("DocFolderMgt.jsp?flag=" + flag);
                /*handling request for mapped documents documents */
            }
            else if ("mapFolder".equalsIgnoreCase(work))
            {
                LOGGER.debug(" in mapFolder===============================");
                
                 int comDocId = 0;
                int mapFolderId = 0;
                boolean updated=false;
                if (request.getParameter("comDocId") != null)
                {
                    comDocId = Integer.parseInt(request.getParameter("comDocId"));
                }
                if (request.getParameter("folderId") != null)
                {
                    mapFolderId = Integer.parseInt(request.getParameter("folderId"));
                }
                
                idType="userId";
                auditId=Integer.parseInt(session.getAttribute("userId").toString());
                auditAction="Bidder has maped Document to Folder";
                moduleName=EgpModule.CDL.getName();
                remarks="Bidder(User id): "+auditId+" has maped Document Id: "+comDocId+" with Folder id: "+mapFolderId;

               
                updated=briefcaseDocImpl.updateBriefcaseDocsToMapWithFolder(comDocId, mapFolderId);
                if(!updated)
                {
                    auditAction="Error in Bidder has maped Document to Folder";
                }
                
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);  
                
                LOGGER.debug(" in mapFolder=============================== End");
                // response.sendRedirect(request.getContextPath()+"/tenderer/"+pageName);
                /*handling request for veryfying the foldername for duplication to database*/
            }
            else if ("verifyFolderName".equalsIgnoreCase(work))
            {
                response.setContentType("text/html;charset=UTF-8");
                String tenderid = request.getParameter("tenderid");
                int tenderID = Integer.parseInt(tenderid);
                PrintWriter out = response.getWriter();
                String folderName = request.getParameter("txtFolderName");
                String str = verifyFolderName(folderName, tenderID);
                out.print(str);
                out.flush();
            }
            if (work != null)
            {
                LOGGER.debug(work + " : " + logUserId + " Ends");

            }
            if (action != null)
            {
                LOGGER.debug(action + " : " + logUserId + " Ends");

            }
            if (type != null)
            {
                LOGGER.debug(type + " : " + logUserId + " Ends");

            }
        }
        catch (Exception e)
        {
            LOGGER.error(" Error in fetchunmapped ::: " + e);
        }
        finally
        {
            LOGGER.debug("finally");
        }
    }

    /**
     * this method is for the uploading the document by passing the bean object and particular tenderer id
     * @param tempCompanyDocumentsDtBean - this is the bean class object
     * @param tendererId - this is the particular tenderer id
     * @return int
     */
    public int uploadDoc(TempCompanyDocumentsDtBean tempCompanyDocumentsDtBean, int tendererId)
    {
        LOGGER.debug("uploadDoc : " + logUserId + " : Starts ");
        int companyDocId = 0;
        try
        {
            // LOGGER.debug("  in Document Brief Sr Bean --------- uploadDoc");
            TblCompanyDocuments tblCompanyDocuments = _toTblCompanyDocuments(tempCompanyDocumentsDtBean);
            tblCompanyDocuments.setUploadedDate(new Date());
            tblCompanyDocuments.setFolderId(0);
            tblCompanyDocuments.setDocumentTypeId("briefcase");
            tblCompanyDocuments.setDocStatus("Approve");
            companyDocId = briefcaseDocImpl.uploadBriefcaseDocs(tblCompanyDocuments, tendererId);
        }
        catch (Exception e)
        {
            LOGGER.error("uploadDoc :  " + logUserId + " :  " + e);
        }
        LOGGER.debug("uploadDoc :  " + logUserId + "  : Ends ");
        return companyDocId;

    }

    /**
     * this method for the mapping form documents
     * @param tenderId - this is the tenderer id for whom the documents is going to map
     * @param formId - this is the form id which is going to map
     * @param userId - this is the user id
     * @param companyDocId - this is the company document id
     * @param manDocId - this is the mandoc id
     */
    public String formMapDocData(int tenderId, int formId, int userId, int companyDocId, String manDocId)
    {
        LOGGER.debug("formMapDocData : " + logUserId + " : Starts ");
        try
        {
            List<SPCommonSearchDataMore> bidderDocDetail = tenderCommonService.executeProcedure("checkDuplicateDocument" , "" + tenderId, "" + userId, ""+formId, ""+companyDocId , null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            if(!bidderDocDetail.isEmpty()  && bidderDocDetail.get(0).getFieldName2().equalsIgnoreCase(""+companyDocId))
            {
                return bidderDocDetail.get(0).getFieldName1();
            }
            TblBidDocuments tblBidDocuments = new TblBidDocuments();
            tblBidDocuments.setTenderId(tenderId);
            tblBidDocuments.setFormId(formId);
            tblBidDocuments.setUserId(userId);
            tblBidDocuments.setMappedDate(new Date());
            tblBidDocuments.setCompanyDocId(companyDocId);
            tblBidDocuments.setManDocId(Integer.parseInt(manDocId));
            briefcaseDocImpl.mapFormDoc(tblBidDocuments);
        }
        catch (Exception e)
        {
            LOGGER.error("formMapDocData :  " + logUserId + " :  " + e);
            e.printStackTrace();
            return "Exception "+e.getMessage();
        }
        LOGGER.debug("formMapDocData :  " + logUserId + "  : Ends ");
        return "mapped";
    }

    /**
     * this method for the removing mapped form documents
     * @param formId - this is the particular form id for whom mapped documents is going to remove
     * @param userId - this is the particular user id
     * @param comDocId - this is the comdoc id
     */
    public void removeMapDocData(int formId, int userId, int comDocId)
    {
        LOGGER.debug("removeMapDocData : " + logUserId + " : Starts ");
        try
        {
            briefcaseDocImpl.deleteDataMapDocsWithFile(formId, userId, comDocId);
        }
        catch (Exception e)
        {
            LOGGER.error("removeMapDocData :  " + logUserId + " :  " + e);
        }
        LOGGER.debug("removeMapDocData :  " + logUserId + "  : Ends ");
    }

    /**
     * this method for the getting company documents data from database by passing bean class objects
     * @param tempCompanyDocumentsDtBean this is the bean class objects
     * @return the tblCompanyDocuments object, which consist the company documents data
     */
    public TblCompanyDocuments _toTblCompanyDocuments(TempCompanyDocumentsDtBean tempCompanyDocumentsDtBean)
    {
        LOGGER.debug("_toTblCompanyDocuments : " + logUserId + " : Starts ");
        TblCompanyDocuments tblCompanyDocuments = null;
        try
        {
            tblCompanyDocuments = new TblCompanyDocuments();
            BeanUtils.copyProperties(tempCompanyDocumentsDtBean, tblCompanyDocuments);
        }
        catch (Exception e)
        {
            LOGGER.error("_toTblCompanyDocuments :  " + logUserId + " :  " + e);
        }
        LOGGER.debug("_toTblCompanyDocuments :  " + logUserId + "  : Ends ");
        return tblCompanyDocuments;
    }

    /**
     * this method for the getting tenderer id from database by passing userid 
     * @param userId - this is the particular user id which is going to pass 
     * @return int, if there is id in database then return id, and if not then return 0     
     */
    public int getTendererIdFromUserId(int userId)
    {
        LOGGER.debug("getTendererIdFromUserId : " + logUserId + " : Starts ");
        int i = 0;
        try
        {
            i = briefcaseDocImpl.getTendererIdFromUserId(userId);
        }
        catch (Exception e)
        {
            LOGGER.error("getTendererIdFromUserId :  " + logUserId + " :  " + e);
        }
        LOGGER.debug("getTendererIdFromUserId :  " + logUserId + "  : Ends ");
        return i;
    }

    //HttpServletRequest req,HttpServletResponse res,int userId,String fileName,int len
    /**
     * this method for the downloading documents which is already uploaded
     * @param req - this is the HttpServletRequest object
     * @param res - this is the HttpServletResponse object
     * @param f - this is the particular file object
     * @param fileSize 
     * @return - boolean, true - if successful, false - if not
     */
    public boolean downloadFile(HttpServletRequest req, HttpServletResponse res, File f, String fileSize)
    {
        //LOGGER.debug(" in download ============ downloadFile in DocumentBriefcaseSeBean ================");
        //File f = new File("C:\\eGP\\Document\\VendorDocuments\\" + userId + "\\" + fileName);
        //byte[] buf = new byte[len];
        LOGGER.debug("downloadFile : " + logUserId + " : Starts ");
        try
        {
            java.io.InputStream fis = new FileInputStream(f);
            byte[] buf = new byte[Integer.valueOf(fileSize)];
            int offset = 0;
            int numRead = 0;
            while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0))
            {
                offset += numRead;
            }
            fis.close();
            FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
            buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
            res.setContentType("application/octet-stream");
            res.setHeader("Content-Disposition", "attachment;filename=\"" + f.getName() + "\"");
            ServletOutputStream outputStream = res.getOutputStream();
            outputStream.write(buf);
            outputStream.flush();
            outputStream.close();

            /*int readData = 0;
            res.setContentType("application/octet-stream");
            res.setHeader("Content-Disposition", "attachment;filename=" + f.getName() + "");
            while ((readData = fis.read()) != -1) {
            res.getOutputStream().write(readData);
            }
            fis.close();
            //res.getOutputStream().write(buf);           
            res.getOutputStream().flush();
            res.getOutputStream().close();*/
        }
        catch (Exception e)
        {
            LOGGER.error("downloadFile : Error in file download:::" + e);
            e.printStackTrace();
        }
        finally
        {
            LOGGER.debug("downloadFile finally");
        }
        LOGGER.debug("downloadFile :  " + logUserId + "  : Ends ");
        return true;
    }
    
    public boolean downloadGeneralFile(HttpServletRequest req, HttpServletResponse res, File f, String fileSize)
    {
        //LOGGER.debug(" in download ============ downloadFile in DocumentBriefcaseSeBean ================");
        //File f = new File("C:\\eGP\\Document\\VendorDocuments\\" + userId + "\\" + fileName);
        //byte[] buf = new byte[len];
        LOGGER.debug("downloadFile : " + logUserId + " : Starts ");
        try
        {
            java.io.InputStream fis = new FileInputStream(f);
            byte[] buf = new byte[Integer.valueOf(fileSize)];
            int offset = 0;
            int numRead = 0;
            while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0))
            {
                offset += numRead;
            }
            fis.close();
            //FileEncryptDecryptUtil fileEncryptDecryptUtil = new FileEncryptDecryptUtil();
            //buf = fileEncryptDecryptUtil.fileDecryptUtil(buf);
            res.setContentType("application/octet-stream");
            res.setHeader("Content-Disposition", "attachment;filename=\"" + f.getName() + "\"");
            ServletOutputStream outputStream = res.getOutputStream();
            outputStream.write(buf);
            outputStream.flush();
            outputStream.close();

            /*int readData = 0;
            res.setContentType("application/octet-stream");
            res.setHeader("Content-Disposition", "attachment;filename=" + f.getName() + "");
            while ((readData = fis.read()) != -1) {
            res.getOutputStream().write(readData);
            }
            fis.close();
            //res.getOutputStream().write(buf);           
            res.getOutputStream().flush();
            res.getOutputStream().close();*/
        }
        catch (Exception e)
        {
            LOGGER.error("downloadFile : Error in file download:::" + e);
            e.printStackTrace();
        }
        finally
        {
            LOGGER.debug("downloadFile finally");
        }
        LOGGER.debug("downloadFile :  " + logUserId + "  : Ends ");
        return true;
    }

    /**
     * this method is for getting the folder details data by passing the userid
     * @param userId this is the id for whom the details is getting
     * @return list of data
     */
    public List<SPCommonSearchDataMore> getFolderDetails(int userId)
    {
        LOGGER.debug("getFolderDetails : " + logUserId + " : Starts ");
        List<SPCommonSearchDataMore> bidderDocDetail1 = null;
        try
        {
            bidderDocDetail1 = tenderCommonService.executeProcedure("getfolders", "0", "" + userId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }
        catch (Exception e)
        {
            LOGGER.error("getFolderDetails : Error in getFolderDetails :: " + e);
        }
        LOGGER.debug("getFolderDetails :  " + logUserId + "  : Ends ");
        return bidderDocDetail1;
    }

    /**
     * this is the method which is going to veryfy foldername for duplication in the database
     * @param folderName - this is the folder name which is going to match in database
     * @param tenderid - this is the particular tenderer id
     * @return String, if match then return 'ok' otherwise return 'Folder name already exists'
     */
    public String verifyFolderName(String folderName, int tenderid)
    {
        LOGGER.debug("verifyFolderName : " + logUserId + " : Starts ");
        String str = null;
        try
        {
            str = briefcaseDocImpl.verifyFolderName(folderName, tenderid);
        }
        catch (Exception e)
        {
            LOGGER.error("verifyFolderName : Error in getFolderDetails :: " + e);
        }
        LOGGER.debug("verifyFolderName :  " + logUserId + "  : Ends ");
        return str;
    }

    /**
     * this method is for getting the count of remaining documents from database
     * @param tenderId - this is the tenderer id which is passed
     * @param formId - this is the form id of the remaining documents
     * @param userId - this is the user id
     * @return long, if there is remaining doc exist.then return count otherwise return 0
     */
    public long getRemainingDocCount(String tenderId, String formId, String userId)
    {
        LOGGER.debug("getRemainingDocCount : " + logUserId + " : Starts ");
        long cnt = 0;
        try
        {
            cnt = briefcaseDocImpl.getRemainingDocCount(tenderId, formId, userId);
        }
        catch (Exception ex)
        {
            LOGGER.error("getRemainingDocCount " + logUserId, ex);
        }
        LOGGER.debug("getRemainingDocCount :  " + logUserId + "  : Ends ");
        return cnt;
    }

        /**
     * this method is for checking is tenderer document exists or not
     * @param DocumentId - this is the Document id of the  documents
     * @param userId - this is the user id
     * @return boolean, if there is doc exist.then return true otherwise return false
     */
    public boolean isTendererDocumentExistsOrNot(String DocumentId,String userId)
    {
        LOGGER.debug("isTendererDocumentExistsOrNot : " + logUserId + " : Starts ");

        try
        { String realPath = DESTINATION_DIR_PATH + userId;
          destinationDir = new File(realPath);
            if (!destinationDir.isDirectory()) { destinationDir.mkdir();     }
        List<SPCommonSearchDataMore> bidderDocDetail1 = tenderCommonService.executeProcedure("getTendererDocumentNameByDocumentId" , DocumentId , userId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
         if(bidderDocDetail1 != null && !bidderDocDetail1.isEmpty())
            {    String fileName = bidderDocDetail1.get(0).getFieldName1();
                File f = new File(destinationDir + "\\" + fileName);
                if (!f.isFile()) {  return false; }
            }
            else { return false;}
        }
        catch (Exception ex)
        {
            LOGGER.error("isTendererDocumentExistsOrNot " + logUserId, ex);
            return false;
        }
        LOGGER.debug("isTendererDocumentExistsOrNot :  " + logUserId + "  : Ends ");
        return true;
    }

    /**
     * this method is for checking is tenderer registration document exists or not
     * @param fileName - this is the name of the  documents
     * @param userId - this is the user id
     * @return boolean, if there is doc exist.then return true otherwise return false
     */
    public boolean isRegisDocumentExistsOrNot(String fileName,String userId)
    {
        LOGGER.debug("isRegisDocumentExistsOrNot : " + logUserId + " : Starts ");

        try
        { String realPath = DESTINATION_DIR_PATH + userId;
          destinationDir = new File(realPath);
            if (!destinationDir.isDirectory()) { destinationDir.mkdir();     }
                File f = new File(destinationDir + "\\" + fileName);
                if (!f.isFile()) {  return false; }
        }
        catch (Exception ex)
        {
            LOGGER.error("isRegisDocumentExistsOrNot " + logUserId, ex);
            return false;
        }
        LOGGER.debug("isRegisDocumentExistsOrNot :  " + logUserId + "  : Ends ");
        return true;
    }
    /**
     * 
     * @param logUserId
     */
    public void setLogUserId(String logUserId)
    {
        this.logUserId = logUserId;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
