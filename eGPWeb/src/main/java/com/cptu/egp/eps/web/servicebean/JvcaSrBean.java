/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servicebean;

import com.cptu.egp.eps.dao.generic.Operation_enum;
import com.cptu.egp.eps.model.table.TblJointVenture;
import com.cptu.egp.eps.service.serviceinterface.JointVentureService;
import com.cptu.egp.eps.service.serviceinterface.JvcapartnersService;
import com.cptu.egp.eps.service.serviceinterface.UserRegisterService;
import com.cptu.egp.eps.web.utility.AppContext;
import com.cptu.egp.eps.web.utility.MailContentUtility;
import com.cptu.egp.eps.web.utility.MsgBoxContentUtility;
import com.cptu.egp.eps.web.utility.SendMessageUtil;
import com.cptu.eps.service.audit.AuditTrail;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
/**
 *
 * @author Administrator
 */
public class JvcaSrBean {

    private final Logger logger = Logger.getLogger(TempCompanyDocumentsSrBean.class);
    private JvcapartnersService jvcapartnersService = (JvcapartnersService)AppContext.getSpringBean("JvcapartnersService");
    private JointVentureService jointVentureService = (JointVentureService)AppContext.getSpringBean("JointVentureService");
    private String logUserId="0";
    String JVCAName;
    private AuditTrail auditTrail;

    public void setAuditTrail(AuditTrail auditTrail) {
        this.auditTrail = auditTrail;
        jvcapartnersService.setAuditTrail(auditTrail);
        jointVentureService.setAuditTrail(auditTrail);
    }

    /**
     * Logger Purpose
     * @param logUserId
     */
    public void setLogUserId(String logUserId) {
        jvcapartnersService.setLogUserId(logUserId);
        jointVentureService.setLogUserId(logUserId);
        this.logUserId = logUserId;
    }
    /**
     * Update JVCA Partner Details
     * @param JVid jvca Id
     * @param status jvca status
     * @param userId
     * @return no of rows of updated
     */
    public int updateJvcaPartner(int JVid, String status,int userId){
        logger.debug("updateJvcaPartner : "+logUserId+" Starts");
        int success = 0;
        try{
            boolean disagree = false;boolean agree = false;boolean pending = false;
            List<Object> listStatus  = new ArrayList<Object>();
            List<Object []> listEmailId  = new ArrayList<Object []>();
            logger.debug("updateJvcaPartner : "+logUserId+" : "+status+"--in sr bean--"+JVid+"----"+userId);
            success = jvcapartnersService.updateJvcaPartner(JVid,status,userId);
            //to check all member status to update jointVenture table
            listStatus = jvcapartnersService.checkAcceptRejectStatus(JVid);
            for (int i = 0; i < listStatus.size(); i++) {
               if( (listStatus.get(i).toString()).equals("Disagreed")){
                   disagree = true;
               }
               if( (listStatus.get(i).toString()).equals("Agreed")){
                   agree = true;
               }
               if( (listStatus.get(i).toString()).equals("Pending")){
                   pending = true;
               }
            }

            // to updte Joint_venture table data
            if(disagree){
                jointVentureService.updateStatus("Rejected",JVid);
            }else if(!pending){
                if(agree){
                    jointVentureService.updateStatus("Approved",JVid);
                    }
            }
            //to send email
              listEmailId = jvcapartnersService.getJvcaPartnerNameAndEmail(JVid,userId);
                  String partnerName = listEmailId.get(0)[0].toString();
                  String emailFrom = listEmailId.get(0)[1].toString();
                  
                  String emailId= "" ;
                  List<Object> listEmailTo  = new ArrayList<Object>();
                  listEmailTo = jvcapartnersService.getJvcaLeadPartnerEmail(JVid);
                  for(int i = 0; i < listEmailTo.size(); i++){
                   emailId += listEmailTo.get(i).toString()+",";
                   }
                  String emailArr[] = {emailId};
                  SendMessageUtil sendMessageUtil = new SendMessageUtil();
                  MailContentUtility mailContentUtility = new MailContentUtility();

                   List list = mailContentUtility.JvcaStatusUpdate(partnerName,status);
                   String mailSub = list.get(0).toString();
                   String mailText = list.get(1).toString();
                   sendMessageUtil.setEmailTo(emailArr);
                   sendMessageUtil.setEmailFrom(emailFrom);
                   sendMessageUtil.setEmailSub(mailSub);
                   sendMessageUtil.setEmailMessage(mailText);
                   sendMessageUtil.sendEmail();

             // to send msg
                   listEmailTo.clear();
                   emailId = "";
                   listEmailTo = jointVentureService.forMailEmail(JVid, userId);
                   for(int i = 0; i < listEmailTo.size(); i++){
                   emailId += listEmailTo.get(i).toString()+",";
                   }
                   listEmailId.clear();
                    List<Object []> listMailContent  = new ArrayList<Object []>();
                   listMailContent = jointVentureService.forMailContent(JVid,userId);
             UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
             MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
             userRegisterService.contentAdmMsgBox(emailId, emailFrom, "JVCA Invitation processed", msgBoxContentUtility.JvcaStatusUpdate(listMailContent));
             listEmailId.clear();
             listEmailTo.clear();
             listStatus.clear();
             listMailContent.clear();
             listEmailId=null;
             listEmailTo=null;
             listStatus=null;
             listMailContent = null;
            
        }catch(Exception e){
            logger.error("updateJvcaPartner : "+logUserId+" : "+e);
            success = 0;
        }
        logger.debug("updateJvcaPartner : "+logUserId+" Ends");
        return  success;
    }

    /**
     * Proposed JVCA
     * @param userId
     * @param sidx
     * @param sord
     * @return List of Object[] [0] jvid,[1] jvname,[2] jvstatus,[3] isNominatedPartner, [4] creationDate, [5]NewJvcaUserId"
     */
    public List<Object[]> preposedJvca(int userId,String sidx,String sord){
        logger.debug("preposedJvca : "+logUserId+" Starts");
        List<Object[]> list = null;
        try{
            Object e = null;
            if(sidx.equalsIgnoreCase("")){
                sidx = "jv.jvid";
                sord = "desc";
            }else if(sidx.equalsIgnoreCase("JVStatus")){
                sidx = "jv.jvstatus";
            }else{
                sidx = "jv.jvname";
            }
            if(sord.equalsIgnoreCase("asc")){
                e = Operation_enum.ASC;
            }else{
                e = Operation_enum.DESC;
            }
            list =  jointVentureService.jvcaPrposedList(userId,sidx,e);
        }catch(Exception ex){
            logger.error("preposedJvca : "+logUserId+" : "+ex);
            list = null;
        }
        logger.debug("preposedJvca : "+logUserId+" Ends");
        return list;
    }
    
    /**
     * Fetch Partner Request JVCA List
     * @param userId
     * @param sidx
     * @param sord
     * @return List of object in object[0] jvName, [1] JvId, [2] jvAcceptRejStatus,[3] creationDate,[4] jvAccpetDatetime,[5] jvstatus,[6] isNominatedPartner,[7] newJvuserId
     */
    public List<Object []> partnerRequestJvcaList(int userId,String sidx,String sord){
        logger.debug("partnerRequestJvcaList : "+logUserId+" Starts");
        List<Object []> list = null;
         if (sidx.equalsIgnoreCase("JVName")) {
            sidx = "jv.jvname";
        } else if (sidx.equalsIgnoreCase("JVStatus")) {
            sidx = "jp.jvAcceptRejStatus";
        }
        try{
        list = jvcapartnersService.getPartnerList(userId,sidx,sord);
        }catch(Exception ex){
            logger.error("partnerRequestJvcaList : "+logUserId+" : "+ex);
            list = null;
        }
        logger.debug("partnerRequestJvcaList : "+logUserId+" Ends");
        return list;
    }

    /**
     *to get JVCA user nAme from JVID
     * @param JVid
     */
    public void getJvcaNamefromId(int JVid){
     logger.debug("getJvcaNamefromId : "+logUserId+" Starts");
        try {
     Object jvca = jvcapartnersService.getJvcaNamefromId(JVid);
     setJVCAName((jvca).toString());
        } catch (Exception e) {
            logger.error("getJvcaNamefromId : "+logUserId+" : "+e);
    }
        logger.debug("getJvcaNamefromId : "+logUserId+" Ends");
    }

    /**
     * to get JVCA partner lIst from current JVCID
     * @param JVid
     * @return
     */
    public List<Object []> getJvcaPartnerDetailList(int JVid){
     logger.debug("getJvcaPartnerDetailList : "+logUserId+" Starts");
     List<Object []> list = null;
        try {
            list = jvcapartnersService.getJvcaPartnerDetailList(JVid);
        } catch (Exception e) {
            logger.error("getJvcaPartnerDetailList : "+logUserId+" : "+e);
    }
     logger.debug("getJvcaPartnerDetailList : "+logUserId+" Ends");
     return list;
    }

    /**
     * Proposed JVCA List
     * @param userId
     * @param searchField
     * @param searchOper
     * @param searchString
     * @return List of Object[] [0] jvid,[1] jvname,[2] jvstatus,[3] isNominatedPartner, [4] creationDate"
     */
    public List<Object[]> preposedJvca(int userId,String searchField,String searchOper,String searchString){
        logger.debug("preposedJvca : "+logUserId+" Starts");
        List<Object[]> list = null;
          try{
              String e = null;
              if(searchOper.equalsIgnoreCase("eq")){
                  e = "=";
              }
              else if(searchOper.equalsIgnoreCase("cn")){
                  e = "Like";
                  searchString = searchString+"%";
              }if("JVStatus".equalsIgnoreCase(searchField)){
                  searchField = "jv.jvstatus";
              }else if("JVName".equalsIgnoreCase(searchField)){
                  searchField = "jv.jvname";
              }
            list =  jointVentureService.preposedJvca(userId,searchField,e,searchString);
        }catch(Exception ex){
           logger.error("preposedJvca : "+logUserId+" : "+ex);
        }
         logger.debug("preposedJvca : "+logUserId+" Ends");
         return list;
    }
    /**
     * fetch JVCa Partner Request Details
     * @param userId
     * @param searchField
     * @param searchOper
     * @param searchString
     * @return List of object in object[0] jvName, [1] JvId, [2] jvAcceptRejStatus,[3] creationDate,[4] jvAccpetDatetime,[5] jvstatus,[6] isNominatedPartner,[7] newJvuserId
     */
    public List<Object []> partnerRequestJvcaList(int userId,String searchField,String searchOper,String searchString){
        logger.debug("partnerRequestJvcaList : "+logUserId+" Starts");
        List<Object []> list = null;
        try{
        if (searchField.equalsIgnoreCase("jvname")) {
            searchField = "jv.jvname";
        } else if (searchField.equalsIgnoreCase("JVStatus")) {
            searchField = "jp.jvAcceptRejStatus";
        }
        if (searchOper.equalsIgnoreCase("eq")) {
            searchOper = "=";
        } else {
            searchOper = "like";
            searchString = searchString+"%";
        }
       
        list = jvcapartnersService.getPartnerList(userId, searchField, searchOper, searchString);
        }catch(Exception e)
        {
            logger.error("partnerRequestJvcaList : "+logUserId+" : "+e);
        }
        logger.debug("partnerRequestJvcaList : "+logUserId+" Ends");
        return list;
    }
    /**
     * Fetch Own company Details by userId
     * @param userId
     * @return
     */
    public List<Object[]> getOwnCompanyDetails(int userId){
        logger.debug("getOwnCompanyDetails : "+logUserId+" Starts");
        List<Object[]> companyDetail = null;
        try{
            companyDetail = new ArrayList<Object[]>();
            companyDetail = jointVentureService.getOwnCompanyDetails(userId);
        }catch(Exception ex){
            logger.error("getOwnCompanyDetails : "+logUserId+" : "+ex);
        }
        logger.debug("getOwnCompanyDetails : "+logUserId+" Ends");
        return companyDetail;
    }

    /**
     * Give List of records of Tbl_JoinVenture 
     * @param jvcId
     * @return List<TblJointVenture>
     */
    public List<TblJointVenture> getJVCADetails(int jvcId){
        logger.debug("getJVCADetails : "+logUserId+" Starts");
        List<TblJointVenture> list = null;
        try {
            list = jointVentureService.getJVCADetails(jvcId);
        } catch (Exception e) {
            logger.error("getJVCADetails : "+logUserId+" : "+e);
    }
        logger.debug("getJVCADetails : "+logUserId+" Ends");
        return list;
    }
    
    /**
     * Getter of JvcaParterSerivece
     * @return
     */
    public JvcapartnersService getJvcapartnersService() {
        return jvcapartnersService;
    }

    /**
     * Setter of JvcaParterSerivece
     * @param jvcapartnersService
     */
    public void setJvcapartnersService(JvcapartnersService jvcapartnersService) {
        this.jvcapartnersService = jvcapartnersService;
    }

    /**
     * Getter of JVCAName
     * @return
     */
    public String getJVCAName() {
        return JVCAName;
    }

    /**
     * Setter of JVCAName
     * @param JVCAName
     */
    public void setJVCAName(String JVCAName) {
        this.JVCAName = JVCAName;
    }
    /**
     * This method give nextscreen when jvca is not formed or partially formed
     * @param userId
     * @return Object [0] NextScreen , [1] status
     */
    public Object[] getNextScreen(String userId){
        logger.debug("getNextScreen : "+logUserId+" Starts");
        Object[] obj = null;
        try {
            obj = jvcapartnersService.getNextScreen(userId);
        } catch (Exception e) {
            logger.error("getNextScreen : "+logUserId+" : "+e);
    }
        logger.debug("getNextScreen : "+logUserId+" Ends");
        return obj;
    }
}
