/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SearchMyAppData;
import com.cptu.egp.eps.web.servicebean.APPSrBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author rishita
 */
public class AppSearchServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(APPReportServlet.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";

    /**
     * This Servlet is use for Search APP Data for generate Grid of App list.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        LOGGER.debug("processRequest :"+logUserId+ LOGGERSTART);
        try {
            HttpSession session = request.getSession();
            if(session.getAttribute("userId")!=null){
                logUserId = session.getAttribute("userId").toString();
            }
            if (request.getParameter("action").equals("fetchData")) {
                response.setContentType("text/xml;charset=UTF-8");
                boolean _search = Boolean.parseBoolean(request.getParameter("_search"));

                String financialYear = request.getParameter("financialYear");
                String projectName = request.getParameter("projectName");
                String activityName = "";
                String budgetType = request.getParameter("budgetType");
                String status = request.getParameter("status");
                String userId = request.getParameter("userId");
                String appCode = request.getParameter("appCode");
                int createdBy=0;
                if(!request.getParameter("myChkVal").equals("")){
                    createdBy = Integer.parseInt(request.getParameter("myChkVal"));
                }
                int appId=0;
                if(!request.getParameter("appId").equals("")){
                    appId = Integer.parseInt(request.getParameter("appId"));
                }

                String rows = request.getParameter("rows");
                String page = request.getParameter("page");
                String sord = request.getParameter("sord");
                String sidx = request.getParameter("sidx");

                APPSrBean appSrBean = new APPSrBean();

                appSrBean.setSearch(_search);
                appSrBean.setLimit(Integer.parseInt(rows));
                //int offset = ((Integer.parseInt(page)-1) * Integer.parseInt(rows));
                int offset = Integer.parseInt(page);
                if(sord.equalsIgnoreCase(null) || sord.equalsIgnoreCase(""))
                {
                    sord = "";
                    sidx = "";
                }
                appSrBean.setOffset(offset);
                appSrBean.setSortOrder(sord);
                appSrBean.setSortCol(sidx);
                
                List<SearchMyAppData> getAPPDetails = appSrBean.getAppDetails(financialYear, projectName, activityName, budgetType, status,Integer.parseInt(userId),appId,appCode,createdBy,sidx,sord);

//                int totalPages=0;
//                int totalCount =0;
//                if (getAPPDetails.get(0).getTotalPage() == 0) {
//                    totalCount = 1;
//                    totalPages = 1;
//                } else {
                //int totalPages = getAPPDetails.get(0).getTotalPage();
                int totalPages = 0;
                int totalCount = 0;
                if (!getAPPDetails.isEmpty()) {
                    totalCount = getAPPDetails.get(0).getTotalRec();
                }

//                }
                if (totalCount > 0) {
                    if (totalCount % Integer.parseInt(request.getParameter("rows")) == 0) {
                        totalPages = totalCount / Integer.parseInt(request.getParameter("rows"));
                    } else {
                        totalPages = (totalCount / Integer.parseInt(request.getParameter("rows"))) + 1;
                    }

                } else {
                    totalPages = 0;
                }
                out.print("<?xml version='1.0' encoding='utf-8'?>\n");
                out.print("<rows>");
                out.print("<page>" + request.getParameter("page") + "</page>");
                out.print("<total>" + totalPages + "</total>");
                out.print("<records>" + getAPPDetails.size() + "</records>");
                // be sure to put text data in CDATA
                int j = 0;
                int no = Integer.parseInt(request.getParameter("page"));
                if(no == 1){
                    j = 1;
                }else{
                    if(Integer.parseInt(rows) == 30){
                        j = ((no-1)*30)+1;
                    }else if(Integer.parseInt(rows) == 20){
                        j = ((no-1)*20)+1;
                    }
                    else{
                        j = ((no-1)*10)+1;
                    }
                }
                for (int i = 0; i < getAPPDetails.size(); i++) {
                    //System.out.println(i + ".==>" + departmentMasterList.get(i).getDepartmentType());
                    out.print("<row id='" + i + "'>");
                    out.print("<cell>" + j + "</cell>");
                    out.print("<cell><![CDATA[" + getAPPDetails.get(i).getAppId() + "]]></cell>");
                    out.print("<cell><![CDATA[" + getAPPDetails.get(i).getAppCode() + "]]></cell>");
                    out.print("<cell><![CDATA[" + getAPPDetails.get(i).getBudgetType() + "]]></cell>");
                    if(!"".equalsIgnoreCase(getAPPDetails.get(i).getPrjName())){
                        out.print("<cell><![CDATA[" + getAPPDetails.get(i).getPrjName() + "]]></cell>");
                    }else{
                        if(!"".equalsIgnoreCase(getAPPDetails.get(i).getactivityName()))
                        {
                            out.print("<cell><![CDATA[" + getAPPDetails.get(i).getactivityName() + "]]></cell>");
                        }
                        else
                        {
                            out.print("<cell><![CDATA[-]]></cell>");
                        }
                    }
                    String link = "<a style='text-decoration: none;' href=\"APPDashboard.jsp?appID=" + getAPPDetails.get(i).getAppId() + "\"> <img src='../resources/images/Dashboard/dashBoardIcn.png' alt='Dashboard' /> </a>";
                    out.print("<cell><![CDATA[" + link + "]]></cell>");
                    //out.print("<cell>" + departmentMasterList.get(i).getDepartmentType() + "</cell>");
                    //String link="<a href=\"EditPEOffice.jsp?officeid="+ tblOfficeMasterList.get(i).getOfficeId() + "\"> Edit </a>";
                    //System.out.println("Link: "+link);
                    //out.print("<cell><![CDATA[" + link+ "]]></cell>");
                    out.print("</row>");
                    //System.out.println("iddddd::" + ");
                    j++;
                }
                out.print("</rows>");

            }
        } finally {
            out.close();
        }
        LOGGER.debug("processRequest :"+logUserId+ LOGGEREND);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
