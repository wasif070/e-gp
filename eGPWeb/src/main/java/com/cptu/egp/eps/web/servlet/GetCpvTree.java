/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cptu.egp.eps.web.servlet;

import com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData;
import com.cptu.egp.eps.model.table.TblCpvClassification;
import com.cptu.egp.eps.service.serviceimpl.CPVTreeServiceImpl;
import com.cptu.egp.eps.service.serviceimpl.CommonSearchService;
import com.cptu.egp.eps.web.utility.AppContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author dipti
 */
public class GetCpvTree extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private final CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
    private final CPVTreeServiceImpl CPVTreeService = (CPVTreeServiceImpl) AppContext.getSpringBean("CPVTreeService");
    private static final Logger LOGGER = Logger.getLogger(GetCpvTree.class);
    private static final String LOGGERSTART = "Starts";
    private static final String LOGGEREND  = "Ends";
    private String logUserId ="0";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userId")!=null){
            logUserId = session.getAttribute("userId").toString();
            commonSearchService.setLogUserId(logUserId);
            CPVTreeService.setLogUserId(logUserId);
        }
        response.setContentType("text/html;charset=UTF-8");
        LOGGER.debug("processRequest : "+logUserId+ LOGGERSTART);
        PrintWriter out = response.getWriter();

        try {                
                String action = "";
                String keyword = "";
                String searchBy = "";
                int id = 0;
                String strid = "";
                
                if(request.getParameter("id")!=null && !"".equalsIgnoreCase(request.getParameter("id"))){
                    strid = request.getParameter("id");
                    id = Integer.parseInt(request.getParameter("id"));
                }

                if(request.getParameter("action")!=null && !"".equalsIgnoreCase(request.getParameter("action"))){
                    action = request.getParameter("action");
                    if(request.getParameter("keyword")!=null && !"".equalsIgnoreCase(request.getParameter("keyword"))){
                        keyword = request.getParameter("keyword");
                    }
                    if(request.getParameter("searchBy")!=null && !"".equalsIgnoreCase(request.getParameter("searchBy"))){
                        searchBy = request.getParameter("searchBy");
                    }
                }
                List<TblCpvClassification> listCPV = new ArrayList<TblCpvClassification>();
                List<SPCommonSearchData> listcommonSearchData = null;

                if("getChildData".equalsIgnoreCase(action)){
                    String strreqCpvdesc = "";
                    String strCpvDesc = "";

                    if(request.getParameter("cpvdesc")!=null && !"".equalsIgnoreCase(request.getParameter("cpvdesc"))){
                        strreqCpvdesc = request.getParameter("cpvdesc");
                    }
                    
                    strCpvDesc = CPVTreeService.findChildDatabyCpvDesc(strreqCpvdesc);
                    out.print(strCpvDesc);
                }
                else
                {
                    if("search".equalsIgnoreCase(action)){
                       // if("keyword".equalsIgnoreCase(searchBy)){
                            listcommonSearchData = commonSearchService.searchData("SearchCPV", keyword, "", strid, "", "", "", "", "", "");
                       // }else if("code".equalsIgnoreCase(searchBy)){
                          //  listcommonSearchData = commonSearchService.searchData("SearchCPV", "", keyword, strid, "", "", "", "", "", "");
                       // }
                            if(!listcommonSearchData.isEmpty()){
                                for(SPCommonSearchData spData:listcommonSearchData){
                                    TblCpvClassification tblCpvClassification = new TblCpvClassification();
                                    tblCpvClassification.setCpvId(Integer.parseInt(spData.getFieldName1()));
                                    tblCpvClassification.setCpvDescription(spData.getFieldName2());
                                    tblCpvClassification.setCpvCode(spData.getFieldName3());

                                    Byte CpvDivision = Byte.valueOf(spData.getFieldName4());
                                    tblCpvClassification.setCpvDivision(CpvDivision);

                                    Byte CpvGroup = Byte.valueOf(spData.getFieldName5());
                                    tblCpvClassification.setCpvGroup(CpvGroup);
                                    listCPV.add(tblCpvClassification);
                                }

                                JSONArray jSONArray = new JSONArray();
                                jSONArray = getCPVListing(listCPV);
                                out.print(jSONArray);
                                jSONArray=null;
                            }
                    }
                    else
                    {
                        listCPV = CPVTreeService.getChildData(id);
                        JSONArray jSONArray = new JSONArray();
                        jSONArray = getCPVListing(listCPV);
                        out.print(jSONArray);
                        jSONArray=null;
                    }
                }
            }
            catch(Exception ex){
                LOGGER.error("processRequest "+logUserId+" : "+ex.toString());
            }finally {
                out.close();
            }
            LOGGER.debug("processRequest  : "+logUserId+ LOGGEREND);
    }

    public JSONArray getCPVListing(List<TblCpvClassification> listCPV){
        LOGGER.debug("getCPVListing  : "+logUserId+ LOGGEREND);
        int i = 0;
        JSONArray jSONArray = new JSONArray();
        try{
            while(i<listCPV.size())
            {
                JSONObject jSONObject = new JSONObject();
                if(listCPV.get(i).getCpvClass()==1){
                    jSONObject.put("state","open");
                }else{
                    jSONObject.put("state","closed");
                }
                jSONObject.put("data",listCPV.get(i).getCpvDescription());

                JSONObject jsonAttr = new JSONObject();
                jsonAttr.put("id", listCPV.get(i).getCpvId());
                jsonAttr.put("cpvcode", listCPV.get(i).getCpvCode());
                jsonAttr.put("cpvname", listCPV.get(i).getCpvDescription());
                jSONObject.put("attr", jsonAttr);
                jsonAttr = null;

                jSONArray.put(jSONObject);
                i++;
            }
        }catch(Exception ex){
            LOGGER.error("getCPVListing "+logUserId+" : "+ex.toString());
        }
        LOGGER.debug("getCPVListing  : "+logUserId+ LOGGEREND);
        return jSONArray;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
