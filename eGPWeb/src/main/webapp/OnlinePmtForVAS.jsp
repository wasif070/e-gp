<%--
    Document   : OnlinePmtForVAS
    Created on : Mar 6, 2012, 12:52:22 PM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AdvertisementService"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblAdvertisement"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Collection"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<script type="text/javascript">
    $(function() {
        $('#txtService').change(function() {
            if(<%=request.getParameter("adId")%>!=null && <%=request.getParameter("adId")%>!=""){
                if("Advertisement"==document.getElementById("txtService").value){
                    document.getElementById("tdid1").style.display='';
                    document.getElementById("tdid2").style.display='';
                    document.getElementById("tdid3").style.display='';
                }else{
                    document.getElementById("tdid1").style.display='none';
                    document.getElementById("tdid2").style.display='none';
                    document.getElementById("tdid3").style.display='none';
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        if(<%=request.getParameter("adId")%>!=null && <%=request.getParameter("adId")%>!=""){
            document.getElementById("trid1").style.display='none';
            document.getElementById("trid2").style.display='none';
            document.getElementById("trid3").style.display='none';
        }else{
            document.getElementById("trid1").style.display='';
            document.getElementById("trid2").style.display='';
            document.getElementById("trid3").style.display='';
        }
    });
</script>
<%
            String pyamentUrl = XMLReader.getBankMessage("PaymentGatewayURLDBBL");
            if (pyamentUrl != null && pyamentUrl.trim().length() > 0) {
%>
<div class="t_space">
    <div class="noticeMsg responseMsg t_space">
        Acceptable cards are DBBL Nexus Card, Master Card, VISA Card, BRAC Card
    </div>
    <%if (request.getParameter("er") != null) {
                if (request.getParameter("er").equals("1")) {
    %>
    <div class="responseMsg errorMsg t_space t_space" >
        Your transaction is fail. Please try again or go to the scheduled member banks of e-GP to make payment.
    </div>
    <%} else if (request.getParameter("er").equals("2")) {
    %>
    <div class="responseMsg errorMsg t_space t_space" >
        Unable to complete your request. Please contact GPPMD Office.
    </div>
    <%}
        }%>
</div>
<form method="POST" id="formregVasfeePay" name="formregVasfeePay" action="/OnlinePaymentServlet?adId=<%=request.getParameter("adId")%>&service=Advertisement">
    <table width="100%" border="1" cellspacing="10" cellpadding="0" class="tableList_1 t_space" >
        <tr>
            <td style="font-style: italic" class="ff" align="left" colspan="2" width="15%">Fields marked with (<span class="mandatory">*</span>) are mandatory.
                <input type="hidden" name="reqFrom" value="regVasFeePayment">
            </td>
        </tr>
        <%
                String eMailId = "";
                String username = "";
                String address = "";
                String duration = "";
                String location = "";
                String price = "";
                if (request.getParameter("adId") != null && !request.getParameter("adId").equals("")) {
                    AdvertisementService advertisementService = (AdvertisementService) AppContext.getSpringBean("AdvertisementService");
                    List<TblAdvertisement> list1 = advertisementService.findTblAdvertisement("adId", Operation_enum.EQ, Integer.parseInt(request.getParameter("adId")));
                    TblAdvertisement tblAd = list1.get(0);
                    eMailId = tblAd.geteMailId();
                    username = tblAd.getUserName();
                    address = tblAd.getAddress();
                    duration = tblAd.getDuration();
                    location = tblAd.getLocation();
                    price = tblAd.getPrice().substring(0, tblAd.getPrice().indexOf("."));
                    int adId = 0;
                    adId = Integer.parseInt(request.getParameter("adId"));
                }
        %>
        <tr>
            <td class="ff">e-mail Id : <span class="mandatory">*</span></td>
            <td class="ff">
                <input name="txtemailId" type="text" class="formTxtBox_1" id="emailId" onblur="checkValue('emailId', 'txtemailIdspan');" <%if (request.getParameter("adId") != null && request.getParameter("adId") != "") {%> value="<%= eMailId%>" <%}%>/>
                <span id="txtemailIdspan" class="reqF_1"></span>
            </td>
        </tr>
        <tr>
            <td class="ff">Payee Name : <span class="mandatory">*</span></td>
            <td class="ff">
                <input name="txtPayeeName" type="text" class="formTxtBox_1" id="txtPayeeNameid" size="74" maxlength="75" onblur="checkValue('txtPayeeNameid', 'txtPayeeNamespan');" <%if (request.getParameter("adId") != null && request.getParameter("adId") != "") {%> value="<%=username%>" <%}%>/>
                <span id="txtPayeeNamespan" class="reqF_1"></span>
            </td>
        </tr>
        <tr>
            <td class="ff">Payee Address : <span class="mandatory">*</span></td>
            <td class="ff">
                <label>
                    <textarea name="txtPayeeAddress" rows="3" class="formTxtBox_1" id="txtPayeeAddressid" style="width:400px;" onblur="checkValue('txtPayeeAddressid', 'txtPayeeAddressSpan');"><%if (request.getParameter("adId") != null && request.getParameter("adId") != "") {%><%= address%><%}%></textarea>
                </label>
                <span id="txtPayeeAddressSpan" class="reqF_1"></span>
            </td>
        </tr>
        <tr>
            <td class="ff">Description of Payment : <span class="mandatory">*</span></td>
            <td class="ff">
                <label>
                    <textarea name="txtDescofPay" rows="3" class="formTxtBox_1" id="txtDescofPayid" style="width:400px;" onblur="checkValue('txtDescofPayid', 'txtDescofPayspan');"></textarea>
                </label>
                <span id="txtDescofPayspan" class="reqF_1"></span>
            </td>
        </tr>
        <tr>
            <td class="ff">Service : <span class="mandatory"></span></td>
            <td width="15%" class="t-align-left">
                <select name="txtService" id="txtService">
                    <option value="">-- select service --</option>
                    <%if (request.getParameter("adId") != null && request.getParameter("adId") != "") {%><option selected value="Advertisement">Advertisement</option><%}%>
                </select>
            </td>
        </tr>
        <%if (request.getParameter("adId") != null && !request.getParameter("adId").equals("")) {%>
        <tr  name="tdid1" id="tdid1">
            <td class="ff">Location : </td>
            <td width="15%" class="t-align-left">
                <select name="txtLocation" id="txtLocation" readonly>
                    <option value="<%= location%>"><%= location%></option>
                </select>
            </td>
        </tr>
        <tr id="tdid2">
            <td class="ff">Duration : </td>
            <td width="15%" class="t-align-left">
                <select name="txtDuration" id="txtDuration" value="" readonly>
                    <option value="<%= duration%>"><%= duration%></option>
                </select>
            </td>
        </tr>
        <tr id="tdid3">
            <td class="ff">Total Amount (in Nu.) :</td>
            <td class="">
                <span id="totalAmount" style="font-weight: bold;"></span>
                <input type="text" name="totalAmount" readonly id="totalAmountid" value="<%=price%>" />
                <span id="txtAmountspan" style="font-weight: bold;"></span>
                <input name="txtAmount" type="hidden" class="formTxtBox_1" id="txtAmountid" value="<%=price%>"/>
            </td>
        </tr>
        <%}%>
        <tr>
            <td class="ff">Financial Institute Payment :  <span class="mandatory">*</span>
            </td>
            <td class="ff">
                <%
                        Collection<String> bankval = null;
                        Set<String> bankkey = null;
                        bankval = XMLReader.getBnkMsg().values();
                        bankkey = XMLReader.getBnkMsg().keySet();
                        Object[] valArr = bankval.toArray();
                        Object[] keyArr = bankkey.toArray();
                        for (int i = 0; i < valArr.length; i++) {
                            if (i % 2 == 1) {
                %>
                <input type="Radio" name="BankList" value=<%=keyArr[i-1]%> /> <% out.println(valArr[i]);%>&nbsp;&nbsp;<%
                            }
                        }
                %>
            </td>
        </tr>
        <tr id="trid1">
            <td class="ff">Amount (in Nu.) : <span class="mandatory">*</span></td>
            <td class="ff">
                <input name="txtAmount" type="text" class="formTxtBox_1" id="txtAmountid" onblur="generateTotal()" value=""/>
                <span id="txtAmountspan" class="reqF_1"></span>
            </td>
        </tr>
        <tr id="trid2">
            <td class="ff">Service Charge (<%out.print(XMLReader.getMessage("ServiceCharge"));%>%) : </td>
            <td>
                <span id="ServicechargeAmount" style="font-weight: bold;"></span>
            <input type="hidden" id="serviceChargeid" value="<%=XMLReader.getMessage("ServiceCharge")%>">
            </td>
        </tr>
        <tr id="trid3">
            <td class="ff">Total Amount (in Nu.) :</td>
            <td class="">
                <span id="totalAmount" style="font-weight: bold;"></span>
                <input type="hidden" name="totalAmount" id="totalAmountid">
            </td>
        </tr>
        <tr>
            <td class="ff">Remarks : </td>
            <td class="ff">
                <label>
                    <textarea name="txtremarks" rows="3" class="formTxtBox_1" id="txtremarksid" style="width:400px;"></textarea>
                </label>
            </td>
        </tr>
        <tr>
            <td class="t-align-center" colspan="2" >
                <label class="formBtn_1">
                    <input type="submit" align="middle" name="submit" id="submit" value="Make Payment" onclick="return validate();"/>
                </label>
            </td>
        </tr>
    </table>
</form>
<%} else {%>
<table width="100%" cellpadding="0" cellspacing="10" class="t_space">
    <tr>

        <td height="400" align="center" valign="middle" style="font-size:16px; color:#6e6e6e; font-weight:bold;">
            <img src="<%= request.getContextPath()%>/resources/images/Dashboard/Button_Warning.png" width="128" height="128" style="margin-bottom:10px;" /> <br />
    <legend class="heading-text">This feature will be available shortly.</legend><br/>
</td>
</tr>
</table>
<%}%>