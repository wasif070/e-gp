<%-- 
    Document   : TestApplet
    Created on : Jun 20, 2011, 2:13:30 PM
    Author     : eprocure
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test Applet</title>
    </head>
    <body>
        <script type="text/javascript" language="javascript" src="resources/js/form/deployJava.js"></script>
        <script type="text/javascript">
            var SignerAPI;
            $(function() {
                SignerAPI = document.getElementById('SignerAPI'); // get access to the signer applet.
            });
        </script>
        <script type="text/javascript">
            deployJava.runApplet(
                {
                    codebase:"/",
                    archive:"Signer.jar",
                    code:"com.cptu.egp.eps.SignerAPI.class",
                    width:"100",
                    Height:"100",
                    ID: "SignerAPI",
                    classloader_cache: "false"
                },
                null,
                "1.6"
            );
            function btnClickFun(){
                try{
                    SignerAPI = document.getElementById('SignerAPI');
                    SignerAPI.setData("abcprocure");
                    alert(SignerAPI.getSymEncrypt("yrj"));
                }catch(e){
                    alert("Exception -->" + e);
                }
            }
        </script>
        <input type="button" value="click here" onclick="btnClickFun();">
    </body>
</html>
