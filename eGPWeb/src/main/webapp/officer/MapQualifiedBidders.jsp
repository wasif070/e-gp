<%--
    Document   : MapQualifiedBidders.jsp
    Created on : december 8, 2012, 11:54:56 PM
    Author     : dixit
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <!--jalert -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <title>Map Qualified Bidders for Create RFP</title>
        
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1"><div>
            <div class="pageHead_1">Map Qualified Bidders for Create RFP
            <span style="float:right;"><a href="Notice.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back To Dashboard</a></span>
            </div>
            <%pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div class="dividerBorder t_space" >&nbsp;</div>      </div>
            <form id="mapbidderfrmid" name="mapbidder" method="post" action='<%=request.getContextPath()%>/CreateTenderPQServlet?action=mapqualifiedbidders'>
                <table cellpadding="0" width="100%"class="tableList_1" id="tblmapbidder">
                    <tr>
                        <th width="30" align="center">select</th>
                        <th width="30" align="center">Sr.No</th>
                        <th width="60" align="center">Bidder Name</th>
                    </tr>
                    <%
                        int i = 1;
                        List<SPTenderCommonData> qualifiedTenders = tenderCommonService.returndata("getREOIQualifiedTenderer",request.getParameter("tenderId"),null);
                        if(qualifiedTenders!=null && !qualifiedTenders.isEmpty()){
                            for(SPTenderCommonData SPTC : qualifiedTenders){
                    %>
                        <tr>
                            <td class="t-align-center"><input type='checkbox' name='chkName' value="<%=SPTC.getFieldName1()%>" onClick="selectall()" ></td>
                            <td class="t-align-center"><%=i%></td>
                            <td class="t-align-center"><%=SPTC.getFieldName2()%></td>

                        </tr>
                            <%i++;}%>
                    <tr >
                        <td class="t-align-center" colspan="3">
                            <span class="formBtn_1"><input type="submit" id="mapbidders"name="mapbidders" value="Map Bidder" onclick="return Validate()"/></span>&nbsp;
                        </td>
                    </tr>
                    <%}else{%>
                        <tr>
                            <td class="t-align-center" colspan="3">
                                <font color="red">No records Found</font>
                            </td>
                        </tr>
                    <%}%>
                    <input type="hidden" name="tenderId" value="<%=request.getParameter("tenderId")%>">
                    </table>
            </form>
            </div>

        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

<script type="text/javascript">
    function Validate(){
        var flag = true;
        var table = document.getElementById("tblmapbidder");
        var rowCount = table.rows.length;
        var j=0;var c=0;
        for(var k=0; k<rowCount; k++) {
            var row = table.rows[k];
            var chkbox = row.cells[0].childNodes[0];
                if(chkbox.checked==true)
                {
                    c++;
                }
            }
            if(c>7)
            {
                jAlert("You Can not Map More than 7 Bidders","Map Bidders", function(RetVal) {});
                flag= false;
            }
            if(c==0)
            {
                jAlert("Please Map Bidders","Map Bidders", function(RetVal) {});
                flag= false;
            }
        return flag;
    }
</script>