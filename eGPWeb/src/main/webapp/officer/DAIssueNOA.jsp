<%-- 
    Document   : DAIssueNOA
    Created on : Jan 2, 2011, 3:53:55 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Issue Draft Agreement</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
        <jsp:useBean id="issueNOADtBean" scope="request" class="com.cptu.egp.eps.web.databean.IssueNOADtBean"/>
        <jsp:setProperty name="issueNOADtBean" property="*"/>
    </head>
    <%
                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");
                }
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    //userId = Integer.parseInt(session.getAttribute("userId").toString());
                    userId = session.getAttribute("userId").toString();
                    issueNOASrBean.setLogUserId(userId);
                    commonSearchService.setLogUserId(userId);
                    tenderCommonService.setLogUserId(userId);
                }
    %>
    <script type="text/javascript">

        $(document).ready(function(){
            $("#frmIssueNOA").validate({

                rules:{
                    contractNo:{required:true,maxlength:100,spacevalidate:true},
                    contractAmtString:{required:true,decimal:true},
                    perfSecAmtString:{required:true,decimal:true},
                    contractName:{required:true,maxlength:100}

                },
                messages:{
                    contractNo:{required:"<div class='reqF_1'>Please enter contract no</div>",
                        maxlength:"<div class='reqF_1'>Maximum 100 characters are allowed</div>",
                        spacevalidate:"<div class='reqF_1'>Only space Is not allowed</div>"
                    },
                    contractName:{
                        required:"<div class='reqF_1'>Please enter contract name.</div>",
                        maxlength:"<div class='reqF_1'>Maximum 100 characters are allowed</div>"
                    },
                    contractAmtString:{
                        required:"<div class='reqF_1'>Please enter contract price in Figure (In Tk.)</div>",
                        decimal:"<div class='reqF_1'>Allows numbers and 2 digits after decimal.</div>"
                    },
                    perfSecAmtString:{
                        required:"<div class='reqF_1' id = 'perSecVal'>Please enter Performance security amount in Figure (In Tk.)</div>",
                        decimal:"<div class='reqF_1' id = 'perSecValDec'>Allows numbers and 2 digits after decimal.</div>"
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">
            $(function() {
                $('#frmIssueNOA').submit(function() {
                    if($('#frmIssueNOA').valid()){
                        if($('#Submit')!=null){
                            $('#Submit').attr("disabled","true");
                            $('#hdnbutton').val("Submit");
                        }
                    }
                });
            });
        </script>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <div class="contentArea_1">
                <form id="frmIssueNOA" action="" method="POST">
                    <div class="pageHead_1">Issue Draft Agreement
                        <span class="c-alignment-right">
                            <a href="#" class="action-button-goback">Go Back To Dashboard</a>
                        </span>
                    </div>
                    <%          
                                if ("Submit".equals(request.getParameter("hdnbutton"))) {
                                    if(issueNOASrBean.addSerIssueNOA(issueNOADtBean)){
                                    response.sendRedirect("DANOA.jsp?tenderId="+tenderId);
                                }
                                }

                                //List<SPCommonSearchData> packageList = commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null);
%>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                        <tr>
                            <td width="24%" class="ff">Contract No : <span class="mandatory">*</span></td>
                            <td width="76%"><input name="contractNo" type="text" class="formTxtBox_1" id="txtContractNo" style="width:200px;" />
                            </td>
                        </tr>
                        <tr>
                            <td width="24%" class="ff">Contract/Project name : <span class="mandatory">*</span></td>
                            <td width="76%"><input name="contractName" type="text" class="formTxtBox_1" id="txtprojectName" style="width:200px;" />
                            </td>
                        </tr>
                        <tr>
                            <%
                                        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            %>
                            <td class="ff">Date of Draft Agreement :</td>
                            <td class="formStyle_1"><%=dateFormat.format(new java.util.Date())%>
                                <input type="hidden" value="<%=dateFormat.format(new java.util.Date())%>" name="contractDtString"/></td>
                        </tr>
                        <tr>
                            <td class="ff">Name of Bidder/Consultant : </td>
                            <td>
                                <select id="hdUserId" style="width:205px;"class="formTxtBox_1" name="hdUserId">
                                <%for(SPTenderCommonData companyList : tenderCommonService.returndata("GetEvaluatedBidders",tenderId,"0")){ %>
                                    <option value="<%=companyList.getFieldName2() %>"><%=companyList.getFieldName1() %></option>
                                <% } %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Contract price In Figure (In Tk.) <span class="mandatory">*</span></td>
                            <td width="76%"><input name="contractAmtString" type="text" class="formTxtBox_1" id="txtContPriceFig" style="width:200px;" onblur="contractPrice(this.value);"/></td>
                        </tr>
                        <tr>
                            <td class="ff">Contract price In Words (In Tk.)</td>
                            <td><label id="conPrice"></label>
                                <input id="contractAmtWords" name="contractAmtWords" type="hidden"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">No. of days from the date of issuance of Draft Agreement</td>
                            <%
                                        List<SPCommonSearchData> issueCon = commonSearchService.searchData("ProcureNOA", tenderId, "", "", null, null, null, null, null, null);
                                        dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                                        List<SPCommonSearchData> holidayFun = commonSearchService.searchData("getBusinessDays", dateFormat.format(new java.util.Date()), issueCon.get(0).getFieldName1(), "", null, null, null, null, null, null);

                                        String NOADate = holidayFun.get(0).getFieldName1();

                                        dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                                        Calendar calendar = java.util.Calendar.getInstance();
                                        calendar.setTime(dateFormat.parse(NOADate));
                                        if (holidayFun.get(0).getFieldName2() != null) {
                                            calendar.add(Calendar.DATE, +Integer.parseInt(holidayFun.get(0).getFieldName2()));
                                        } else {
                                            calendar.add(Calendar.DATE, +0);
                                        }
                                        //calendar.add(Calendar.DATE, +10);
                                        
                                        //Date NOADateL = DateUtils.convertDateToStr(NOADate);
%>
                            <td><%=issueCon.get(0).getFieldName1()%>
                                <input type="hidden" value="<%=issueCon.get(0).getFieldName1()%>" name="noaIssueDays"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Draft Agreement acceptance last date &amp; time</td>
                            <td><%=NOADate%>
                                <input type="hidden" value="<%=NOADate%>" name="noaAcceptDtString"/></td>
                        </tr>
                        <tr>
                            <td width="24%" class="ff">Performance security Required : </td>
                            <td width="76%">
                                <input checked name="perReq" type="radio" id="perReqYes" onclick="perVisible();" value="Yes"/>Yes
                                <input name="perReq" type="radio" id="perReqNo" onclick="perInvisible();" value="No"/>No
                            </td>
                        </tr>
                        <tr id="perSecAmtInFig">
                            <td class="ff">Performance security amount in Figure (In Tk.) <span class="mandatory"></span></td>
                            <td width="76%"><input onblur="performancePrice(this.value);" name="perfSecAmtString" type="text" class="formTxtBox_1" id="txtSecurityAmtFig" style="width:200px;" /></td>
                        </tr>
                        <tr id="perSecAmtInWord">
                            <td class="ff">Performance security amount in Words (In Tk.)</td>
                            <td><label id="perSecAmt"></label>
                                <input type="hidden" name="perfSecAmtWords" id="perfSecAmtWords"/>
                            </td>
                        </tr>
                        <tr id="noOfDaysForPerSecSub">
                            <td class="ff">No. of days for performance security submission</td>
                            <td><%if (holidayFun.get(0).getFieldName2() != null) {%><%=holidayFun.get(0).getFieldName2()%><% }%>
                                <input type="hidden" name="perSecSubDays" <%if (holidayFun.get(0).getFieldName2() != null) {%>value="<%=holidayFun.get(0).getFieldName2()%>"<% }%>/>
                            </td>
                        </tr>
                        <tr id="lastDTForPerSecSub">
                            <td class="ff" >Last date &amp; time for Performance security submission </td>
                            <td><%=dateFormat.format(calendar.getTime())%>
                                <input type="hidden" value="<%=dateFormat.format(calendar.getTime())%>" name="perSecSubDtString"/>
                            </td>
                        </tr>
                        <%
                                    calendar.setTime(dateFormat.parse(NOADate));
                                    if (holidayFun.get(0).getFieldName3() != null) {
                                        calendar.add(Calendar.DATE, +Integer.parseInt(holidayFun.get(0).getFieldName3()));
                                    } else {
                                        calendar.add(Calendar.DATE, +0);
                                    }
                        %>
                        <tr>
                            <td class="ff">Signing of Contract in no. of days from the date of issuance</td>
<!--                            <td><input type="hidden" name="contractSignDays" value="<%=dateFormat.format(calendar.getTime())%>"/>
                            <%=dateFormat.format(calendar.getTime())%></td>-->
                            <td><input type="hidden" name="contractSignDays" <%if (holidayFun.get(0).getFieldName3() != null) {%> value="<%=holidayFun.get(0).getFieldName3()%>"<% }%>/>
                                <%if (holidayFun.get(0).getFieldName3() != null) {%> <%=holidayFun.get(0).getFieldName3()%><% }%></td>

                        </tr>
                        <%
                                    dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                    calendar.setTime(dateFormat.parse(dateFormat.format(new java.util.Date())));
                                    if (holidayFun.get(0).getFieldName3() != null) {
                                        //calendar.add(Calendar.DATE, +Integer.parseInt(holidayFun.get(0).getFieldName3()));
                                        calendar.add(Calendar.DATE, +Integer.parseInt(holidayFun.get(0).getFieldName3()));
                                    } else {
                                        calendar.add(Calendar.DATE, +0);
                                    }
                        %>
                        <tr id="modeOfPayment">
                            <td class="ff">Mode of payment</td>
                            <td><label><%if(issueCon.get(0).getFieldName4().equalsIgnoreCase("Offline/Bank")){%>Offline,Bank<% }else{ %><%=issueCon.get(0).getFieldName4() %><% } %></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Last Date &amp; time of contract signing</td>
                            <td><%=dateFormat.format(calendar.getTime())%>
                                <input type="hidden" value="<%=dateFormat.format(calendar.getTime())%>" name="contractSignDtString"/>
                                <input type="hidden" value="<%=userId%>" name="createdBy"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Clause Reference</td>
                            <td><textarea rows="4" cols="100" id="clauseRef" name="clauseRef"></textarea>
                            </td>
                        </tr>
                    </table>
                    <div class="t-align-center t_space">
                        <label class="formBtn_1">
                            <input name="Submit" id="Submit" type="submit" value="Submit" />
                            <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                        </label>
                    </div>
                </form>
            </div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script type="text/javascript">        
        function contractPrice(obj){
            document.getElementById('conPrice').innerHTML =WORD(obj);
            document.getElementById('contractAmtWords').value =WORD(obj);
        }
        function performancePrice(obj){
            document.getElementById('perSecAmt').innerHTML =WORD(obj);
            document.getElementById('perfSecAmtWords').value =WORD(obj);
        }
        function perInvisible(){
            document.getElementById('perSecAmtInWord').style.display = 'none';
            document.getElementById('perSecAmtInFig').style.display = 'none';
            document.getElementById('noOfDaysForPerSecSub').style.display = 'none';
            document.getElementById('lastDTForPerSecSub').style.display = 'none';
            document.getElementById('modeOfPayment').style.display = 'none';
            document.getElementById('txtSecurityAmtFig').setAttribute('name', "abc");
            if(document.getElementById('perSecVal') != null){
                if(document.getElementById('perSecVal').innerHTML != ''){
                    document.getElementById('perSecVal').innerHTML = '';
                    
                }
            }

            if(document.getElementById('perSecValDec') != null){
                if(document.getElementById('perSecValDec').innerHTML != ''){
                    document.getElementById('perSecValDec').innerHTML = '';
                    //document.getElementById('txtSecurityAmtFig').setAttribute('name', "abc");
                }
            }
        }
        function perVisible(){
            document.getElementById('perSecAmtInFig').style.display = 'table-row';
            document.getElementById('perSecAmtInWord').style.display = 'table-row';
            document.getElementById('noOfDaysForPerSecSub').style.display = 'table-row';
            document.getElementById('lastDTForPerSecSub').style.display = 'table-row';
            document.getElementById('modeOfPayment').style.display = 'table-row';
            document.getElementById('txtSecurityAmtFig').setAttribute('name', 'perfSecAmtString');
        }
    </script>
    <%
                issueNOASrBean = null;
                issueNOADtBean = null;
    %>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>

