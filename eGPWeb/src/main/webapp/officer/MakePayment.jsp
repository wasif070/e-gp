<%--
    Document   : MakePayment
    Created on : Aug 9, 2011, 11:06:17 AM
    Author     : dixit
--%>

<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Make Payment Listing</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>
    </head>
    <%
        ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
    %>
    <body>
        <%
            String straction = "";
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            String procnature = "";
            if(request.getParameter("tenderId") != null){
                procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
            }
            if(request.getParameter("btnMsag")!=null)
            {
                straction = request.getParameter("btnMsag");
            }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1"><%=bdl.getString("CMS.Inv.PaymentListing.Title")%>
                <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('6,7');">Save as PDF</a></span>
                </div>
                <%
                    if("sendtope".equalsIgnoreCase(straction))
                    {
                %>
                    <div class="t_space"><div class="responseMsg successMsg t_space"><span>

                                <%=bdl.getString("CMS.Inv.SentToPe.Msg")%>

                            </span></div></div>
                <%}if("sendtotenderer".equalsIgnoreCase(straction))
                    {
                %>
                    <div class="t_space"><div class="responseMsg successMsg t_space"><span>
                                <%if(procnature.equalsIgnoreCase("goods")){%>
                                <%=bdl.getString("CMS.Inv.SentToSupplier.Msg")%>
                                <%}else{%>
                                <%=bdl.getString("CMS.Inv.SentToSupplier.works.Msg")%>
                                <%}%>
                            </span></div></div>
                <%}%>
                <div>&nbsp;</div>
                <ul class="tabPanel_1 t_space" id="tabForApproved">
                    <li><a href="javascript:void(0);" id="pendingTab" onclick="changeTab(1);" class="sMenu"><%=bdl.getString("CMS.Inv.Tabbed.Pending")%></a></li>
                    <li><a href="javascript:void(0);" id="ProcessingTab" onclick="changeTab(2);"><%=bdl.getString("CMS.Inv.Tabbed.Processing")%></a></li>
                    <li><a href="javascript:void(0);" id="ProcessedTab" onclick="changeTab(3);"><%=bdl.getString("CMS.Inv.Tabbed.Processed")%></a></li>
                    <input type="hidden" name="status" id="status" value=""/>
                </ul>
                <%
                    if("sendtope".equalsIgnoreCase(straction))
                    {
                %>
                        <script type="text/javascript">
                        $("#status").val("Processing");
                        $("#ProcessingTab").addClass("sMenu");
                        $("#pendingTab").removeClass("sMenu");
                        $("#ProcessedTab").removeClass("sMenu");
                        </script>
                <%
                    }
                    if("sendtotenderer".equalsIgnoreCase(straction))
                    {
                %>
                        <script type="text/javascript">
                        $("#status").val("Processed");
                        $("#ProcessedTab").addClass("sMenu");
                        $("#pendingTab").removeClass("sMenu");
                        $("#ProcessingTab").removeClass("sMenu");
                        </script>
                <%}%>


                <div class="tabPanelArea_1">
                    <table width="100%" cellspacing="0" class="tableList_1" id="resultTable" cols="@0,6,7">
                        <tr>
                            <th width="5%" class="t-align-center"><%=bdl.getString("CMS.Inv.S.No")%>
                            </th>
                            <th width="20%" class="t-align-center"><%=bdl.getString("CMS.Inv.TenderCaps")%>
                            </th>
                            <th width="18%" class="t-align-center"><%=bdl.getString("CMS.Inv.ConSupplierName")%></th>
                            <th width="25%" class="t-align-center"><%=bdl.getString("CMS.Inv.PEPM")%></th>
                            <th width="10%" class="t-align-center"><%=bdl.getString("CMS.Inv.CNo")%></th>
                            <th width="10%" class="t-align-center">Invoice No</th>
                            <th width="14%" class="t-align-center"><%=bdl.getString("CMS.Inv.Remarks")%></th>
                            <th width="12%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>
                        </tr>
                    </table>
                    <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                        <tr>
                            <td align="left">Page <span id="pageNoTot">1</span> - <span id="pageTot">10</span></td>
                            <td align="center"><center><input name="textfield3" onkeypress="checkKeyGoTo(event);" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                    </label></center></td>
                            <td class="prevNext-container">
                                <ul>
                                    <li>&laquo; <a href="javascript:void(0)" id="btnFirst">First</a></li>
                                    <li>&#8249; <a href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                    <div align="center">
                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" name="size" id="size" value="10"/>
                    </div>
                </div>
                <form id="formstyle" action="" method="post" name="formstyle">

                   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                   <%
                     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                     String appenddate = dateFormat1.format(new Date());
                   %>
                   <input type="hidden" name="fileName" id="fileName" value="MakePayment_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="MakePayment" />
                </form>
                <div>&nbsp;</div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->

        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabMakePay");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
        </script>
        <script type="text/javascript">
            function chkdisble(pageNo){
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            function changeTab(tabNo){
                if(tabNo == 1){
                    $("#ProcessingTab").removeClass("sMenu");
                    $("#ProcessedTab").removeClass("sMenu");
                    $("#pendingTab").addClass("sMenu");
                    $("#statusTab").val("");
                    $("#status").val("Pending");
                    $('#pageNo').val('1');
                }
                else if(tabNo == 2){
                    $("#pendingTab").removeClass("sMenu");
                    $("#ProcessedTab").removeClass("sMenu");
                    $("#ProcessingTab").addClass("sMenu");
                    $("#statusTab").val("Live");
                    $("#status").val("Processing");
                    $('#pageNo').val('1');
                }
                else if(tabNo == 3){
                    $("#pendingTab").removeClass("sMenu");
                    $("#ProcessingTab").removeClass("sMenu");
                    $("#ProcessedTab").addClass("sMenu");
                    $("#statusTab").val("Live");
                    $("#status").val("Processed");
                    $('#pageNo').val('1');
                }
                loadTable();
            }
        </script>
        <!-- AJAX Grid Functions Start -->
        <script type="text/javascript">
           function loadTable(){
         $.post("<%=request.getContextPath()%>/PaymentListingServlet", {funName: "getGeneratedInvoiceListing",status:$("#status").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);
                        sortTable();
                        if($('#noRecordFound').attr('value') == "noRecordFound"){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }
                        chkdisble($("#pageNo").val());
                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();
                    });
        }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(totalPages>0 && $('#pageNo').val()!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);
                        $('#btnPrevious').removeAttr("disabled");
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();

                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                            if(parseInt($('#pageNo').val(), 10) == 1)
                                $('#btnPrevious').attr("disabled", "true")
                            if(parseInt($('#pageNo').val(), 10) > 1)
                                $('#btnPrevious').removeAttr("disabled");
                        }
                    }
                });
            });
        </script>
        <!-- AJAX Grid Finish-->
        <script type="text/javascript">
            loadTable();
        </script>
        <script type="text/javascript">
            function openWin(tenderid,invoiceid,wpid,cntId)
            {
                myWindow=window.open("ViewRemarks.jsp?tenderId="+tenderid+"&InvoiceId="+invoiceid+"&cntId="+cntId+"&wpId="+wpid,'','scrollbars=1','width=900,height=800');
                myWindow.focus();
            }
        </script>
</html>
