<%--
    Document   : TenExtReqView
    Created on : Jan 4, 2011, 12:55:10 PM
    Author     : rajesh
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="java.util.List" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender validity extension request</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <div class="contentArea_1">
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                            HttpSession hs = request.getSession();
                            String userId = "";
                            if (hs.getAttribute("userId") != null) {
                                userId = hs.getAttribute("userId").toString();
                            }
                            boolean flag = false;
                            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> checkPE = tenderCommonService1.returndata("CheckPE", request.getParameter("tenderId"), "");
                            for (int i = 0; i < checkPE.size(); i++) {
                                if (userId.equalsIgnoreCase(checkPE.get(i).getFieldName1())) {
                                    flag = true;
                                }
                            }
                            TenderSrBean tenderSrBeanForValSec = new TenderSrBean();
                            List<Object[]> objForChkingValSec = tenderSrBeanForValSec.getConfiForTender(Integer.parseInt(request.getParameter("tenderId")));
               
                                
                              // Coad added by Dipal for Audit Trail Log.
                            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                            String idType="tenderId";
                            int auditId=Integer.parseInt(request.getParameter("tenderId"));
                            String auditAction="View Processed Validity Extension Request";
                            String moduleName=EgpModule.Evaluation.getName();
                            String remarks="";
                            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            
            %>

                <div class="pageHead_1">Validity Extension Request
                    <%if (flag) {%>
                    <span class="c-alignment-right"><a href="TECProcess.jsp?tenderId=<%=request.getParameter("tenderId")%>&comType=TEC" class="action-button-goback">Go Back</a></span>
                        <%} else {%>
                    <span class="c-alignment-right"><a href="TenderExtReqListing.jsp" class="action-button-goback">Go Back</a></span>
                        <%}%>
                </div>
                <div class="mainDiv">
                    <%
                                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                </div>
                <div>&nbsp;</div>
                <hr />

                <form id="frmhope" action="TenExtLastDateAccep.jsp?tenderId=<%=request.getParameter("tenderId")%>&ExtId=<%=request.getParameter("ExtId")%>" method="post">
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <%
                                    if (!request.getParameter("ExtId").equals("0")) {
                                        List<SPTenderCommonData> list = tenderCommonService.returndata("GetValidityExtDetail", request.getParameter("ExtId"), null);
                                        if (!list.isEmpty()) {
                        %>
                        <tr>
                            <td width="21%" class="t-align-left ff">Tender Validity in no. of Days : </td>
                            <td width="79%" class="t-align-left"><%=list.get(0).getFieldName1()%></td>
                        </tr>

                        <tr>
                            <td class="t-align-left ff">Last Date  of Tender Validity : </td>
                            <td class="t-align-left"><%=list.get(0).getFieldName2()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">New  Date of Tender Validity : </td>
                            <td class="t-align-left"><%=list.get(0).getFieldName3()%></td>
                        </tr>
                        <%
                        if(!objForChkingValSec.isEmpty() && objForChkingValSec.get(0)[2].toString().equalsIgnoreCase("yes")){
                        %>
                        <tr>
                            <td class="t-align-left ff">Last Date of Tender Security Validity : </td>
                            <td class="t-align-left"><%=list.get(0).getFieldName4()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">New Date of Tender Security Validity : </td>
                            <td class="t-align-left"><%=list.get(0).getFieldName5()%></td>
                        </tr>
                        <% } %>
                        <%--<tr>
                            <td class="t-align-left ff">Last Date for Acceptance of Tender Validity Extension Request : </td>
                            <td class="t-align-left"><%=list.get(0).getFieldName9()%>
                            </td>
                        </tr>--%>
                        <tr>
                            <td class="t-align-left ff">Extension Reason :  </td>
                            <td class="t-align-left"><%=list.get(0).getFieldName8()%>
                            </td>
                        </tr>
                        <%}
                                                            } else {
                                                                List<SPTenderCommonData> list = tenderCommonService.returndata("GetValidityExtDetail", request.getParameter("ExtId"), request.getParameter("tenderId"));
                                                                if (!list.isEmpty()) {

                        %>
                        <tr>
                            <td width="21%" class="t-align-left ff">Tender Validity in no. of Days : </td>
                            <td width="79%" class="t-align-left"><%=list.get(0).getFieldName2()%></td>
                        </tr>

                        <tr>
                            <td class="t-align-left ff">Date  of Tender Validity : </td>
                            <td class="t-align-left"><%=list.get(0).getFieldName3()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Last Date of Tender Security Validity : </td>
                            <td class="t-align-left"><%=list.get(0).getFieldName4()%></td>
                        </tr>
                        <%}
                                    }%>
                    </table>
                    <div>&nbsp;</div>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th style="width: 4%">Sl. <br/>No</th>
                            <th>Company Name</th>
                            <th>Payment status</th>
                            <th>Extension Request / acceptance status</th>
                            <%--<th>Date of Acceptance/Rejection</th>--%>
                            <th>Action</th>
                        </tr>
                        <%
                                    int k = 1;

                                    List<SPTenderCommonData> list2 = tenderCommonService.returndata("getValidityExtensionBidder", request.getParameter("tenderId"), request.getParameter("ExtId"));
                                    if (!list2.isEmpty()) {
                                        for (SPTenderCommonData sptcd : list2) {
                        %>
                        <tr>
                            <td class="t-align-left ff"><%=k%></td>
                            <td class="t-align-left ff"><%=sptcd.getFieldName2()%></td>
                            <td class="t-align-left ff"><%=sptcd.getFieldName3()%></td>
                            <td class="t-align-left ff">
                                  <%if (sptcd.getFieldName8() != null) {%>
                                <% if(sptcd.getFieldName8().contains("Approv")){ %>
                                Accepted
                                <% } else if(sptcd.getFieldName8().contains("Reject")) {%>
                                Rejected
                                <%}else{ %>
                                <%=sptcd.getFieldName8() %>
                                <%} %>
                            <%}%>
                            </td>
                            <%--<td class="t-align-left ff"><%=sptcd.getFieldName3()%></td>--%>

                            
                            <td class="t-align-left ff">
                                <%if (sptcd.getFieldName3().trim().equals("Paid")) {%>
    <!--                            <a href="../partner/MakePaymentView.jsp?payId=<%=sptcd.getFieldName4()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts" >View</a> | <a href="../partner/MakePaymentView.jsp?action=cancel&payId=<%=sptcd.getFieldName4()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts" >Cancel</a>-->
                                <a href="../partner/TenderPaymentDetails.jsp?payId=<%=sptcd.getFieldName4()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts" >View</a>
    <!--                            | <a href="../partner/MakePaymentView.jsp?action=cancel&payId=<%=sptcd.getFieldName4()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts" >Cancel</a>-->
                                <%}%>
                            </td>
                        </tr>
                        <%k++;
                                                                }
                                                            } else {%>
                        <tr>
                            <td colspan="5">No Record Found.</td>
                        </tr>
                        <%}%>
                    </table>
                </form>
                <div>&nbsp;</div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
