<%-- 
    Document   : ViewCancelTender
    Created on : Apr 28, 2011, 6:31:23 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCancelTenderRequest"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <title>View Details</title>
        <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    </head>
    <body>
        <%
                    String tenderId = "";
                    if (request.getParameter("tenderid") != null && !"".equals(request.getParameter("tenderid"))) {
                        tenderId = request.getParameter("tenderid");
                    }
        %>
        <div class="contentArea_1">
            <div class="pageHead_1">View Cancel Tender Details</div>
            <table class="tableList_1 t_space" cellspacing="0" width="100%">
                <%
                            List<TblCancelTenderRequest> detailsCT = tenderSrBean.getDetailsCancelTender(Integer.parseInt(tenderId));
                %>
                <tr>
                    <td class="ff" width="20%">View Cancellation Reason :</td>
                    <%if(!detailsCT.isEmpty()){%>
                    <td><%=detailsCT.get(0).getRemarks() %></td>
                    <%}%>
                </tr>
<!--                <tr>
                    <td class="ff">
                        View Notice Details :
                    </td>
                    <td>
                        <span style="float:left;" id="printRem"><a href="<%=request.getContextPath() %>/resources/common/ViewTender.jsp?id=<%=tenderId %>" target="_" class="action-button-notice">View Notice</a></span>
                    </td>
                </tr>-->
            </table>
        </div>
    </body>
</html>
