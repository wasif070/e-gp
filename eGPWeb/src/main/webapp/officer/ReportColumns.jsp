<%-- 
    Document   : ReportColumns
    Created on : Dec 22, 2010, 11:14:47 AM
    Author     : TaherT
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.web.servicebean.ReportCreationSrBean" %>
<%@page import="com.cptu.egp.eps.model.table.TblReportColumnMaster" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>        
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
         <script type="text/javascript">
             $(function() {
                $('#search').submit(function() {                    
                    var cnt=0,one=0,three=0,two=0,four=0;
                    for(var i=1;i<=$('#cnt').val();i++){
                        if($.trim($('#colHeader'+i).val())==""){
                            $('#headmsg'+i).html('Please enter Column Header.');
                            cnt++;
                        }else{
                            if($('#colHeader'+i).val().length>1000){
                                $('#headmsg'+i).html('Maximum 1000 characters are allowed.');
                                cnt++;
                            }else{
                                $('#headmsg'+i).html(null);
                            }
                        }
                    }
                    for(var i=1;i<=$('#cnt').val();i++){                        
                        if($('#filledBy'+i).val()=="1"){
                            one++;
                        }
                        if($('#filledBy'+i).val()=="2"){
                            two++;
                        }
                        if($('#filledBy'+i).val()=="3"){
                            three++;
                        }
                        if($('#filledBy'+i).val()=="4"){
                            four++;
                        }
                    }
                    if(one==0 || three==0 || two==0){
                        jAlert("Filled By must be at least one 'Company', one 'Auto' and one 'Auto Number'.","Report Column Alert", function(RetVal) {
                        });
                        cnt++;
                    }

                    if(one>1){
                        jAlert("Filled By 'Company' must be only one .","Report Column Alert", function(RetVal) {
                        });
                        cnt++;
                    }
                    if(three>1){
                        jAlert("Filled By 'Auto Number' must be only one .","Report Column Alert", function(RetVal) {
                        });
                        cnt++;
                    }
                    if(four>1){
                        jAlert("Filled By 'Official Cost Estimate' must be only one .","Report Column Alert", function(RetVal) {
                        });
                        cnt++;
                    }
                    if(cnt>0){
                        return false;
                    }
                });
            });

             function validate(){
                $('.err').remove();

                var valid = true;
                if($.trim($('#reportName').val()) == ""){
                    $('#reportName').parent().append("<div class='err' style='color:red;'>Please enter Report Name.</div>");
                    valid = false;
                }
                return valid;
             }
             $(function() {
                    $( "#dialog:ui-dialog" ).dialog( "destroy" );
                    $( "#dialog-form" ).dialog({
                        autoOpen: false,
                        resizable:false,
                        draggable:false,
                        height: 500,
                        width: 700,
                        modal: true,
                        buttons: {
                         "OK": function() {
                                 $(this).dialog("close");
                         }},
                        close: function() {
                            $(this).dialog("close");
                        }
                    });
                    $("#sfblock").click(function(){
                        $("#dialog-form").dialog("open");
                    });
            });
        </script>
    </head>
    <body>
        <%
        
                    String reportType="TOR/TER";
                    if(request.getParameter("reportType")!=null)
                    {
                        reportType=request.getParameter("reportType");
                    }
                    String repId = request.getParameter("repId");
                    String tenderid = request.getParameter("tenderid");
                    ReportCreationSrBean rcsb = new ReportCreationSrBean();
                    Object data[] = rcsb.getReportTabIdNoOfCols(repId);
                    int reportTableId = Integer.parseInt(data[0].toString());
                    int noOfCols = Integer.parseInt(data[1].toString());     

                     // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String idType="tenderId";
                    int auditId=Integer.parseInt(request.getParameter("tenderid"));
                    String auditAction=null;
                    String moduleName=EgpModule.Tender_Notice.getName();
                    String remarks="";
                   
        
                    if("Next Step".equalsIgnoreCase(request.getParameter("next")))
                    {
                        boolean isPackgeWise=true;
                        auditAction="PCR for "+reportType+" created by PE";
                        if("item wise".equalsIgnoreCase(data[5].toString())){
                            isPackgeWise=false;
                        }
                        try
                        {
                            rcsb.createReportColumns(reportTableId,request.getParameterValues("colId"),request.getParameterValues("colHeader"),request.getParameterValues("filledBy"),isPackgeWise);
                           
                        }
                        catch(Exception ex)
                        {
                            auditAction="Error in "+auditAction+" "+ex.getMessage();
                        }
                        finally
                        {
                             makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                        }
                         
                        response.sendRedirect("ReportDisplay.jsp?tenderid="+tenderid+"&repId="+repId+"&reportType="+reportType);
                    }
                    else if("Update".equalsIgnoreCase(request.getParameter("next")))
                    {
                        auditAction="PCR for "+reportType+" edited by PE";
                        boolean isPackgeWise=true;
                        if("item wise".equalsIgnoreCase(data[5].toString())){
                            isPackgeWise=false;
                        }
                        String pgName="ReportDisplay.jsp?tenderid="+tenderid+"&repId="+repId+"&reportType="+reportType;
                        boolean isNot=false;
                        if("y".equals(request.getParameter("isNot"))){
                            isNot=true;
                            pgName="Notice.jsp?tenderid="+tenderid;
                        }
                        try
                        {
                            rcsb.updateReportColumns(reportTableId,request.getParameterValues("repColId"),request.getParameterValues("colId"),request.getParameterValues("colHeader"),request.getParameterValues("filledBy"),isPackgeWise,isNot);
                        }
                        catch(Exception ex)
                        {
                            auditAction="Error in "+auditAction+" "+ex.getMessage();
                        }   
                        finally
                        {
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                        }                                                                                  
                        response.sendRedirect(pgName);
                    }else if("Edit".equalsIgnoreCase(request.getParameter("repNameEdit"))){
                        rcsb.updateRepName(repId, request.getParameter("reportName"));
                        response.sendRedirect(request.getHeader("referer"));
                    }else{
                        boolean isEdit=false;
                        boolean isNot=false;
                        java.util.List<TblReportColumnMaster>  list = new java.util.ArrayList<TblReportColumnMaster>();
                        if("y".equals(request.getParameter("isedit"))){
                            list = rcsb.getReportColumns(reportTableId);
                            if(list!=null && (!list.isEmpty())){
                                isEdit=true;
                            }
                        }
                        if("y".equals(request.getParameter("isNot"))){
                            isNot=true;
                        }

        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <%
                    pageContext.setAttribute("tenderId", tenderid);
        %>
        <div class="contentArea_1">
        <div class="pageHead_1">Create Report<span style="float: right;"><a class="action-button-goback" href="Notice.jsp?tenderid=<%=tenderid%>">Go back to Dashboard</a></span></div>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <b style="color: red;"><br/>Instructions :</b>
        <ol type="a" style="list-style-type: lower-alpha;color: red;font-weight: bold;margin-left: 20px;line-height: 16px;margin-top: 12px;">
            <li>Please must ensure that Grand Total of all BSR / Price Schedule/ Price Proposal forms (Form1, Form2, Form3 ...) is selected while preparing a formula except in case of LTM Works [e-PW2(B)].</li>
            <!--<li>Once the report formula is prepared, please test it by entering numeric value and then save it.</li>-->
            <li>In case of query, please contact GPPMD Office </li>
            <li>
                <span  class="ReadMore">
                    <a id="sfblock" href="javascript:void(0);" style="color: red;">Suggested Formula</a>
                </span>
            </li>
        </ol>
        <div class="tableList_1 t-align-left t_space">
                    Field Marked (<span class="mandatory">*</span>) is Mandatory
        </div>
        <%if(isNot){%>
        <form action="ReportColumns.jsp?tenderid=<%=tenderid%>&repId=<%=repId%>&isedit=y&isNot=y" method="post" id="repFrm" onsubmit="return validate();">
        <table class="tableList_1 t_space" width="100%" cellspacing="0">
            <tr>
                <td class="t-align-left ff">Report Name : <span class="mandatory">*</span></td>
                <td class="t-align-left">
                    <input name="reportName" class="formTxtBox_1" value="<%=data[2]%>" id="reportName" style="width: 200px;" type="text">
                    <span class="formBtn_1">
                        <input type="submit" value="Edit" name="repNameEdit" id="repNameEdit" />
                    </span>
                </td>
            </tr>
        </table>
        </form>
        <%}%>
        <form action="ReportColumns.jsp?tenderid=<%=tenderid%>&repId=<%=repId%>&reportType=<%=reportType%><%if(isNot){out.print("&isNot=y");} %>" method="post" id="search">
                <input type="hidden" value="<%=noOfCols%>" id="cnt"><br/>
                <div class="tableHead_1"><%=data[2]%></div>                
                <table class="tableList_1" width="100%" cellspacing="0">
                    <tbody><tr>

                            <th colspan="<%=noOfCols+noOfCols%>" class="t-align-left ff"><%=data[3]%></th>
                        </tr>
                        <tr>
                            <%int j=0;for(int i=1;i<=noOfCols;i++){%>
                            <td class="t-align-left ff"><input type="hidden" value="<%=i%>" name="colId"/>Column Header : <span class="mandatory">*</span></td>
                            <td class="t-align-left"><input name="colHeader" class="formTxtBox_1" id="colHeader<%=i%>" style="width: 95%;" type="text" value="<%if(isEdit){out.print(list.get(j).getColHeader());}%>">
                                <div id="headmsg<%=i%>" style="color: red;"></div>
                            </td>
                            <%j++;}%>
                        </tr>

                        <tr>
                            <%int k=0;for(int i=1;i<=noOfCols;i++){%>
                            <td class="t-align-left ff"><%if(isEdit){%><input type="hidden" value="<%=list.get(k).getReportColId()%>" name="repColId"/><%}%>Filled By :  <span class="mandatory">*</span></td>
                            <td class="t-align-left">
                                <select name="filledBy" class="formTxtBox_1" id="filledBy<%=i%>">
                                    <option value="1" <%if(isEdit){if(list.get(k).getFilledBy()==1){out.print("selected");}}%>>Company</option>
                                    <%/*if("item wise".equalsIgnoreCase(data[5].toString())){*/%><%--<option value="2" <%if(isEdit){if(list.get(k).getFilledBy()==2){out.print("selected");}}%>>Government</option><%}%>--%>
                                    <option value="2" <%if(isEdit){if(list.get(k).getFilledBy()==2){out.print("selected");}}%>>Auto</option>
                                    <option value="3" <%if(isEdit){if(list.get(k).getFilledBy()==3){out.print("selected");}}%>>Auto Number</option>
                                    <option value="4" <%if(isEdit){if(list.get(k).getFilledBy()==4){out.print("selected");}}%>>Official Cost Estimate</option>
                                </select>
                            </td>
                            <%k++;}list=null;%>
                        </tr>
                        <tr>
                            <td colspan="<%=noOfCols+noOfCols%>" class="t-align-left formSubHead_1"><%=data[4]%></td>
                        </tr>
                    </tbody></table>
                <div class="t-align-center t_space">
                    <label class="formBtn_1"><input name="next" id="button3" value="<%if(isEdit){out.print("Update");}else{out.print("Next Step");}%>" type="submit"></label>

                </div>

                <table class="tableList_1 t_space" width="100%" cellspacing="0">
                    <tbody><tr>
                            <th class="t-align-left ff">Instruction for Report Preparation</th>
                        </tr>
                        <tr>
                            <td class="listBox_aboutUs t-align-left" width="12%">
                                <ul>

                                    <li>Select "AutoNumber" option from the given filled by drop downlist for the column in which system generated ranks are to be displayed.</li>
                                    <li>Select "Company Name" option from the given filled by drop downlist for the column in which company name is to be dispalyed.</li>
                                    <li>Select "Auto" option from the given filled by drop downlist for the column in which the value would be derived by creating a formula.</li>
                                    <li>In case if the value of a particular column is to be converted into words then select "Auto" option for the filled by drop downlist for the column by option.
                                        Necessary formula needs to be created using build formula functionality.
                                    </li>
                                </ul>
                            </td>
                        </tr>

                    </tbody></table>            
        </form>
 </div>
        <div id="dialog-form" title="Formula for Tender/Proposal Opening Report (TOR) & Tender/Proposal Evaluation Report (TER/PER)">
                    <fieldset>
                        <table width="100%" cellspacing="0" border="1" class="tableList_1">
                            <tr>
                                <th colspan="5">Formula for Tender Opening Report (TOR)</th>
                            </tr>
                            <tr>
                                <th width="5%">Sl. No</th>
                                <th width="25%">Name of Column to be shown in the Report</th>
                                <th width="15%">Data Type</th>
                                <th width="15%">Formula</th>
                                <th width="40%">Remarks</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Sl. No.</td>
                                <td>Auto Number</td>
                                <td></td>
                                <td>This column will be used to Display Rank as L1, L2, ...</td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Name of the Bidder</td>
                                <td>Company</td>
                                <td></td>
                                <td>Bidders/Suppliers/Contractors participated in the tender will be shown</td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Quoted Amount (in Nu.)(X)</td>
                                <td>Auto</td>
                                <td>Summation of Grand Total of Each Form</td>
                                <td>
                                    Grand Total of each Priced BSR or Schedule of Items will be used as input for Quoted Amount.<br/>
                                    For E.g. there are 3 Forms namely
                                    <ol type="1" style="list-style-type: decimal;margin-left: 20px;line-height: 16px;margin-top: 12px;margin-bottom: 5px;">
                                        <li>Price and Delivery Schedule (Form1)</li>
                                        <li>Price and Delivery Schedule_1 (Form2)</li>
                                        <li>Price and Completion Schedule for related Services (Form3)</li>
                                    </ol>
                                    The formula of Quoted Amount (in Nu.) = Form1_Grand Total + Form2_Grand Total + Form3_Grand Total
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" border="1" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5">Formula for Tender Evaluation Report (TER)</th>
                            </tr>
                            <tr>
                                <th width="5%">Sl. No</th>
                                <th width="25%">Name of Column to be shown in the Report</th>
                                <th width="15%">Data Type</th>
                                <th width="15%">Formula</th>
                                <th width="40%">Remarks</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Rank</td>
                                <td>Auto Number</td>
                                <td></td>
                                <td>This column will be used to Display Rank as L1, L2, ...</td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Name of the Bidder/Consultant</td>
                                <td>Company</td>
                                <td></td>
                                <td>Bidders/Consultants participated in the tender will be shown</td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Official Cost Estimate (E)</td>
                                <td>Official Cost Estimate</td>
                                <td></td>
                                <td>Official Cost Estimate will be punched by PE</td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Quoted Amount (in Nu.) For ICB Tender Currency should be as decided by PE (X)</td>
                                <td>Auto</td>
                                <td>Summation of Grand Total of Each Form</td>
                                <td>
                                    Grand Total of each Priced BSR or Schedule of Items will be used as input for Quoted Amount.
                                    For E.g. there are 3 Forms namely
                                    <ol type="1" style="list-style-type: decimal;margin-left: 20px;line-height: 16px;margin-top: 12px;margin-bottom: 5px;">
                                        <li>Price and Delivery Schedule (Form1)</li>
                                        <li>Price and Delivery Schedule_1 (Form2)</li>
                                        <li>Price and Completion Schedule for related Services (Form3)</li>
                                    </ol>
                                    The formula of Quoted Amount (in Nu.) = Form1_Grand Total + Form2_Grand Total + Form3_Grand Total
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Deviation Amount (Y)</td>
                                <td>Auto</td>
                                <td>E-X</td>
                                <td>
                                    Official Cost Estimate - Quoted Amount
                                    <br/>
                                    Note : If the deviation amount is positive, then it is saving
                                 </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>% of Deviation (Z)</td>
                                <td>Auto</td>
                                <td>100 - X * 100 / E</td>
                                <td>Note : If the % of deviation is positive, then it is saving</td>
                            </tr>
                        </table>
                    </fieldset>
            </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%}%>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
