<%--
    Document   : ClarificationRefDoc
    Created on : Dec 5, 2010, 8:16:44 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="java.text.SimpleDateFormat" %>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Eval Report Documents</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:100}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please Select Document.</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please Enter Description.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                    }
                });
            });
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#button').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>
        
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->


            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
                        String evalRptId = "1";
                        String tenderId = "0";

                        if (request.getParameter("evalRptId") != null) {
                            evalRptId = request.getParameter("evalRptId");
                        }
                        if (request.getParameter("tenderId") != null) {
                            tenderId = request.getParameter("tenderId");
                        }


            %>
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">
                <div class="pageHead_1">Evaluation Report Documents<span style="float:right;"><a href="EvalComm.jsp?tenderid=<%=tenderId %>" class="action-button-goback">Go Back to Dashboard</a></span></div>
                <%-- <div class="t_space" align="right"><a href="Notice.jsp?tenderid=<%=postQualId%>" class="action-button-goback">Go Back Dashboard</a></div>--%>
                <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/ClarificationRefDocServlet" enctype="multipart/form-data" name="frmUploadDoc">

                    <input type="hidden" name="evalRptId" value="<%=evalRptId%>"/>
                    <input type="hidden" name="tenderId" value="<%=tenderId%>"/>


                    <%
                                if (request.getParameter("fq") != null) {
                    %>

                    <div class="responseMsg errorMsg t_space"><%=request.getParameter("fq")%></div>
                    <%
                                }
                                if (request.getParameter("fs") != null) {
                    %>

                    <div class="responseMsg errorMsg t_space t_space">

                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                                }
                    %>

                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>

                            <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>
                        <tr>
                            <td width="10%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                            <td width="90%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Description : <span>*</span></td>
                            <td>
                                <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                <div id="dvDescpErMsg" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>

                                <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" /></label>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");%>
                            <td class="t-align-left">
                                Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
                </form>

                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="26%">File Name</th>
                        <th class="t-align-center" width="55%">File Description</th>
                        <th class="t-align-center" width="5%">File Size <br />
                            (in Kb)</th>
                        <th class="t-align-center" width="10%">Action</th>
                    </tr>
                    <%


                                if (request.getParameter("evalRptId") != null) {
                                    evalRptId = request.getParameter("evalRptId");
                                }
                                int docCnt = 0;
                                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                String status = "";
                                for (SPTenderCommonData sptcd : tenderCS.returndata("EvalRptDocInfo", evalRptId, "")) {
                                    docCnt++;
                                    for (SPTenderCommonData commonTenderDetails : tenderCS.returndata("getTscMemberStatus", tenderId, evalRptId)) {
                                        status = commonTenderDetails.getFieldName7();
                                        if (!status.equals("Pending")) {
                                            status = "Signed";
                                        }
                                    }
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <%if (status == "Signed") {%>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ClarificationRefDocServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&evalRptId=<%=evalRptId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a></td>
                                <%} else {%>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ClarificationRefDocServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&evalRptId=<%=evalRptId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                            <a href="<%=request.getContextPath()%>/ClarificationRefDocServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&evalRptId=<%=evalRptId%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                        </td>
                        <%}%>
                    </tr>

                    <%   if (sptcd != null) {
                                        sptcd = null;
                                    }
                                }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
                <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
                    <tr>
                        <th width="53%" class="ff">Committee Members </th>
                        <th width="24%">Signed Status </th>
                        <th width="18%">Signed Date &amp; Time </th>

                    </tr>
                    <%
                                tenderId = request.getParameter("tenderId");
                                evalRptId = request.getParameter("evalRptId");
                                boolean showVerify = false;
                                int memberCnt = 0;
                                int ApprovedMemCnt = 0;
                                for (SPTenderCommonData commonTenderDetails : tenderCS.returndata("getTscMemberStatus", tenderId, evalRptId)) {
                                    memberCnt++;
                                    status = commonTenderDetails.getFieldName7();
                                    if (!status.equals("Pending")) {
                                        status = "Signed";
                                    }
                                    String currUserId = "";
                                    currUserId = commonTenderDetails.getFieldName1();

                                    if (currUserId != "") {
                                        if (currUserId.equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                            
                                        }else{
                                                showVerify = true;
                                          }
                                    }
                                    String date = commonTenderDetails.getFieldName8();
                                    
                    %>
                    <tr>
                        <td class="ff">
                            <% if (showVerify) {%>
                            <a href="EvalRptApp.jsp?tenderId=<%=tenderId%>&evalRptId=<%=evalRptId%>"><%=commonTenderDetails.getFieldName2()%></a>
                            <%} else {%><%=commonTenderDetails.getFieldName2()%><%}%>
                        </td><% %>
                        <td><%=status%></td>
                        <% if (date == null) {
                                                                date = "-";
                        %><td class="t-align-center"><%=date%></td>
                        <% } else {
                        %><td class="t-align-center"><%=date%></td><%}%>
                    </tr>
                    <%}%>
                    <% if (memberCnt == 0) {%>
                    <tr>
                        <td colspan="3" class="ff">No members found!</td>
                    </tr>
                    <%}%>
                </table>
            </div>
            <div>&nbsp;</div>
            
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
