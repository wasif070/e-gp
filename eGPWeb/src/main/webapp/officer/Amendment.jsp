<%-- 
    Document   : Amendment
    Created on : Nov 17, 2010, 11:18:29 AM
    Author     : rajesh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title> Amendment / Corrigendum </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        
        <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>

        <!--jalert -->

        <script type="text/javascript" >
            function checkTender(){
                jAlert("there is no Tender Notice"," Alert ", "Alert");
            }

            function checkdefaultconf(){
                jAlert("Default workflow configuration hasn't crated"," Alert ", "Alert");
            }
            function checkDates(){
                jAlert(" please configure dates"," Alert ", "Alert");
            }
            function chekFormMark(eventType){
                //if(eventType=='REOI' || eventType=='reoi'){
                 //   jAlert("Please select Criteria, Sub Criteria and Technical for all Technical forms "," Alert ", "Alert");
               // }else{
                    jAlert("Please configure Criteria, Sub Criteria and Technical Score for all Technical forms"," Alert ", "Alert");
                //}
            }

            function checkGSExist(tenderId,corriId)
            {
             var returnFlag=true;
              $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/CreateTenderFormSrvt",
                        data:"action=checkGrandSumNeedAtCorri&tenderId="+tenderId+"&corriId="+corriId,
                        async: false,
                        success: function(j)
			{
				var alertMessage="";
				if(j.toString() != null && $.trim(j.toString()) != "0" && $.trim(j.toString()) == "Deleted")
				{
				    alertMessage="You haven't updated Grand Summary Report after Cancellation of form(s). Please update Grand Summary Report first and then try to proceed further.";
                                    alert(alertMessage);
                                    returnFlag=false;
				}
                                else if(j.toString() != null && $.trim(j.toString()) != "0" && $.trim(j.toString()) == "NotAddinGrandSummerry")
				{
                                    alertMessage="You haven't updated Grand Summary Report after addition of new form(s). Please update Grand Summary Report first and then try to proceed further.";//If you want to add new forms or Do you want to continue to publish it?";
                                    alert(alertMessage);
                                    returnFlag=false;
//                                    if(confirm(alertMessage))
//                                    {
//                                        returnFlag= false;
//                                    }
//                                    else
//                                    {
//                                        returnFlag=true;
//                                    }

                                }
                                else if(j.toString() != null && $.trim(j.toString()) != "0" && $.trim(j.toString()) == "Added")
				{
                                    alertMessage="Are you sure that you have updated Grand Summary after Cancellation of form(s)?";//If you want to add new forms or Do you want to continue to publish it?";
                                    if(confirm(alertMessage))
                                    {
                                        returnFlag= true;
                                    }
                                    else
                                    {
                                        returnFlag=false;
                                    }

                                }
			}
		});

           return returnFlag;

            }
        </script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>

        <%
                    // Variable tenderId is defined by u on ur current page.
                    pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                    pageContext.setAttribute("tab", "5");
                    String msg = "";
                    msg = request.getParameter("msg");
        %>
        <div class="contentArea_1">
            <div class="pageHead_1">Tender Dashboard

            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div class="t_space">
                <%@include  file="officerTabPanel.jsp"%>

                <div class="tabPanelArea_1">

                    <% if (msg != null && !msg.contains("not")) {%>
                    <br/><div class="responseMsg successMsg">File processed successfully</div>
                    <%  } else if (msg != null && msg.contains("not")) {%>
                    <br/> <div class="responseMsg errorMsg">File has not processed successfully</div>
                    <%  }%>
                    <%if (request.getParameter("msgId") != null) {
                                    String msgId = "", msgTxt = "";
                                    boolean isError = false;
                                    msgId = request.getParameter("msgId");
                                    if (!msgId.equalsIgnoreCase("")) {
                                        if (msgId.equalsIgnoreCase("published")) {
                                            msgTxt = "Amendment / Corrigendum published successfully";
                                        } else {
                                            msgTxt = "";
                                        }
                    %>
                    <%if (isError) {%>
                    <div class="responseMsg errorMsg"><%=msgTxt%></div>
                    <%} else {%>
                    <div class="responseMsg successMsg"><%=msgTxt%></div>
                    <%}%>
                    <%}
                            }%>


                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td width="10%" class="t-align-center ff" style="display: none;">1.</td>
                            <td width="25%" class="t-align-left ff">Float Corrigendum / Amendment / Addendum</td>
                            <td width="60%" class="t-align-left">
                                <%
                                            String i = "";
                                            if (request.getParameter("tenderid") != null) {
                                                i = request.getParameter("tenderid");
                                            }
                                            tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                            List<SPTenderCommonData> list = tenderCommonService.returndata("getCorrigenduminfo", i, null);

                                            //Regarding workflow
                                            int uid = 0;
                                            if (session.getAttribute("userId") != null) {
                                                Integer ob1 = (Integer) session.getAttribute("userId");
                                                uid = ob1.intValue();
                                            }

                                            TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                            List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights", Integer.toString(uid),i);
                                            List<SPTenderCommonData> chekTenderCreator = wfTenderCommonService.returndata("CheckTenderCreator", Integer.toString(uid),i);
                                            WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                                            short eventid = 4;
                                            int objectid = 0;
                                            short activityid = 4;
                                            int childid = 1;
                                            short wflevel = 1;
                                            int initiator = 0;
                                            int approver = 0;
                                            boolean isInitiater = false;
                                            boolean evntexist = false;
                                            String fileOnHand = "No";
                                            String checkperole = "No";
                                            String chTender = "No";
                                            boolean ispublish = false;
                                            boolean iswfLevelExist = false;
                                            boolean isconApprove = false;
                                            String amendmentstatus = "";

                                            // Taher
                                            int cnt = 0;
                                            int corrid = 0;
                                            long archiveCnt = tenderCommonService.tenderArchiveCnt(tenderid);
                                            long isPublishedCnt = tenderCommonService.tenderPublishCnt(tenderid);
                                            for (SPTenderCommonData data : list) {
                                                if (data.getFieldName4().equalsIgnoreCase("pending")) {
                                                    corrid = Integer.parseInt(data.getFieldName1());
                                                    cnt++;
                                                }
                                            }

                                            // chalapathi
                                            String objtenderId = request.getParameter("tenderid");
                                            if (objtenderId != null) {
                                                objectid = Integer.parseInt(objtenderId);
                                                childid = objectid;
                                            }
                                             String donor = "";
                                          donor = workFlowSrBean.isDonorReq(String.valueOf(eventid),
                                          Integer.valueOf(objectid));
                                         String[] norevs = donor.split("_");
                                          int norevrs = -1;

                                          String strrev = norevs[1];
                                          if(!strrev.equals("null")){

                                           norevrs = Integer.parseInt(norevs[1]);
                                           }
                                           

                                            evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
                                            List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
                                            // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);

                                            
                                            if (tblWorkFlowLevelConfig.size() > 0) {
                                                Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                                iswfLevelExist = true;
                                                while (twflc.hasNext()) {
                                                    TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                                    TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                                    if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                                                        fileOnHand = workFlowlel.getFileOnHand();
                                                    }
                                                    if (workFlowlel.getWfRoleId() == 1) {
                                                        initiator = lmaster.getUserId();
                                                    }
                                                    if (workFlowlel.getWfRoleId() == 2) {
                                                        approver = lmaster.getUserId();
                                                    }
                                                    if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                                                        isInitiater = true;
                                                    }

                                                }

                                            }
                                            String userid = "";
                                            if (session.getAttribute("userId") != null) {
                                                Object obj = session.getAttribute("userId");
                                                userid = obj.toString();
                                            }

                                            String field1 = "CheckPE";
                                            List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, userid, "");

                                            if (editdata.size() > 0) {
                                                checkperole = "Yes";
                                            }
                                            // for publish link

                                            List<SPTenderCommonData> checkAmendment = tenderCommonService.returndata("AmendmentWorkFlowStatus", Integer.toString(corrid), "'Pending'");
                                            
                                            if (checkAmendment.size() > 0) {
                                                chTender = "Yes";
                                            }
                                            List<SPTenderCommonData> publist = tenderCommonService.returndata("AmendmentWorkFlowStatus", Integer.toString(corrid), "'Approved','Conditional Approval'");
                                            
                                            String conap = "";
                                            if (publist.size() > 0) {
                                                ispublish = true;
                                                SPTenderCommonData spTenderCommondata = publist.get(0);
                                                amendmentstatus = spTenderCommondata.getFieldName1();
                                                conap = spTenderCommondata.getFieldName2();
                                                if (conap.equalsIgnoreCase("Conditional Approval")) {
                                                    isconApprove = true;
                                                }
                                            }
                                            
                                            boolean isFormConfig = false;
                                            CommonService commonService  = (CommonService) AppContext.getSpringBean("CommonService");
                                            if("Services".equalsIgnoreCase(commonService.getProcNature(request.getParameter("tenderid")).toString())){
                                                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                                List<SPCommonSearchDataMore> tenderPub = commonSearchDataMoreService.geteGPDataMore("CorriPubServiceChk",request.getParameter("tenderid"));
                                                if(tenderPub!=null && !tenderPub.isEmpty() && "0".equalsIgnoreCase(tenderPub.get(0).getFieldName1())){
                                                    isFormConfig = true;
                                                }

                                            }
                                            
                                            

                                            int count = 0;
                                            boolean flag = false;
                                            for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "lot")) {
                                                count++;
                                                commonTdetails.getLotNo();
                                            }
                                            if (count == 0) {
                                                flag = true;
                                            }
                                            if (isPublishedCnt != 0) {
                                                CommitteMemberService committeMemberService = (CommitteMemberService)AppContext.getSpringBean("CommitteMemberService");
                                                long corrCreatedCnt = committeMemberService.checkCountByHql("TblCorrigendumMaster tcm","tcm.tenderId="+tenderid);
                                                if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                if (archiveCnt != 0) {
                                                    if (cnt == 0) {                                        
                                %>

                                <a href="PrepareAmendment.jsp?id=<%=i%>&cid=">Prepare</a><%if(corrCreatedCnt!=0){%>&nbsp;|&nbsp;<%}%>
                                <%
                                                                            } else {
                                                                                if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                        || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true) || iswfLevelExist == false) {
                                %>
                                <a href="PrepareAmendment.jsp?id=<%=i%>&cid=<%=corrid%>&op=edit">Edit</a>&nbsp;|&nbsp;
                                <%
                                                                                }
                                                                                if ((ispublish == true && isInitiater == true && !amendmentstatus.equalsIgnoreCase("Approved")) || (!amendmentstatus.equalsIgnoreCase("Approved") && isInitiater == true && (initiator == approver && norevrs == 0) && initiator != 0 && approver != 0)) {
                                                                                    if (isFormConfig) {%>
                                <a href="#" onclick="chekFormMark('')" >Publish</a>&nbsp;|&nbsp;
                                <% }else{
                                %>
                                
                                <a href="ViewAmendment.jsp?tenderId=<%=i%>&ispub=y">Publish</a>&nbsp;|&nbsp;
                                <%
                                                                                }}

                                                                            }}}
                                        if(corrCreatedCnt!=0){
                                %>
                                <a href="ViewAmendment.jsp?tenderId=<%=i%>">View</a>
                                <%
                                }
                                                }
                                            
                                %>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" class="t-align-center ff" style="display: none;">2.</td>
                            <td width="25%" class="t-align-left ff">Upload Documents </td>
                            <td width="60%" class="t-align-left">
                                <%
                                         if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                            if (isPublishedCnt != 0) {
                                                if (archiveCnt != 0) {
                                                    if (cnt != 0) {
                                                        if (((amendmentstatus.equalsIgnoreCase("pending") || amendmentstatus.equals("")) && ("".equals(conap) || !conap.equalsIgnoreCase("Approved")) && isInitiater == true && fileOnHand.equalsIgnoreCase("Yes")) || iswfLevelExist == false) {
                                                            if (isTenPackageWise) {
                                %>
                                <a href="UploadAmendDoc.jsp?tenderId=<%=i%>&corrigendumId=<%=corrid%>">Upload</a>
                                <%
                                                                                    } else {
                                %>
                                <a href="LotDetail.jsp?tenderId=<%=i%>&corrigendumId=<%=corrid%>">Upload</a>
                                <%
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                         }
                                %>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" class="t-align-center ff" style="display: none;">3</td>
                            <td width="25%" class="t-align-left ff">WorkFlow</td>
                            <td width="60%" class="t-align-left">
                                <%  if(cnt != 0){
                                            if (evntexist) {
                                                if (fileOnHand.equalsIgnoreCase("yes") && checkperole.equals("Yes") && ispublish == false && isInitiater == true) {
                                %>
                                <!--<a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>'>Edit</a>-->
                                &nbsp;|&nbsp;
                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>">View</a>&nbsp;|&nbsp;
                                <%
                                                                            } else if (iswfLevelExist == false && checkperole.equals("Yes") && ispublish == false) {
                                %>
                                <!--<a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>'>Edit</a>-->
                                &nbsp;|&nbsp;
                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>">View</a>&nbsp;|&nbsp;
                                <%
                                                                            } else if (iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {
                                %>
                                <label>Workflow yet not configured</label>
                                <%                                            } else if (iswfLevelExist == true) {
                                %>
                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>">View</a>&nbsp;|&nbsp;
                                <%
                                                                            }
                                                                        } else if (ispublish == false && /*checkperole.equals("Yes")*/ !checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())) {
                                                                            List<CommonAppData> defaultconf = workFlowSrBean.editWorkFlowData("WorkFlowRuleEngine", Integer.toString(eventid), "");
                                                                            if (defaultconf.size() > 0) {
                                %>
                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&submit=Submit&tc=tc">Create</a>
                                <%
                                                                            } else {
                                %>
                                <a  href="#" onclick="checkdefaultconf()">Create</a>
                                <%                                            }
                                                                        } else if (iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {
                                %>
                                <label>Workflow yet not configured</label>
                                <%            }

                                            if (isInitiater == true && (initiator != approver || (initiator == approver && norevrs > 0)) && ispublish == false && fileOnHand.equalsIgnoreCase("Yes")) {
                                                
                                                if (chTender.equalsIgnoreCase("Yes")) {
                                %>
                                <a href="FileProcessing.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=corrid%>&eventid=<%=eventid%>&fromaction=amendment"  onclick="return checkGSExist('<%=request.getParameter("tenderid")%>','<%=corrid%>')">Process file in Workflow</a>&nbsp;|&nbsp;
                                <%
                                                }
                                            }
                                            if (iswfLevelExist) {
                                %>
                                <a href="workFlowHistory.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&userid=<%=uid%>&fraction=amendment" >View Workflow History</a>
                                <%
                                            }}


                                %>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div align="center" class="t_space"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
