<%-- 
    Document   : AddPerfSecAmount
    Created on : Jun 18, 2011, 10:28:31 AM
    Author     : shreyansh
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TblTenderPerformanceSec"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.PerformanceSecurityICT"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPerfSecurity"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!--Added by salahuddin on 11/11/2012-->
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.GetCurrencyAmount"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    <script type="text/javascript">
        function regForNumber(value)
        {
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

        }
        function validate()
        {

            $('.err').remove();

            var count = document.getElementById('taka').value;
           
            //
            //            alert(document.getElementById('taka_0').value);
            //            alert(document.getElementById('taka_1').value);
           
            var vbool=true;

            if(!regForNumber(document.getElementById('taka').value)){
                   
                $("#taka").parent().append("<div class='err' style='color:red;'>Please enter Performance Security Amount in BTN</div>");
                vbool = false;
            }else if(document.getElementById('taka').value.indexOf("-")!=-1){
                    $("#taka").parent().append("<div class='err' style='color:red;'>Only positve values are allowed</div>");
                    vbool = false;
                }
            else if(document.getElementById('taka').value.split(".")[1] != undefined){
                if(!regForNumber(document.getElementById('taka').value) || document.getElementById('taka').value.split(".")[1].length > 3 || document.getElementById('taka').value.split(".")[0].length > 12){
                    $("#taka").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }
                    
            }else{
               
                if(!regForNumber(document.getElementById('taka').value)){
                    $("#taka").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }else if(document.getElementById('taka').value.indexOf("-")!=-1){
                    $("#taka").parent().append("<div class='err' style='color:red;'>Only positve values are allowed</div>");
                    vbool = false;
                }
                else if(document.getElementById('taka').value.length > 12){
                    $("#taka").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }
            }

            if(!regForNumber(document.getElementById('percentage').value)){
                $("#percentage").parent().append("<div class='err' style='color:red;'>Please enter appropriate Percentage.</div>");
                vbool = false;
            }else if(eval(document.getElementById('percentage').value) == 0){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Performance Security Percentage cannot be 0</div>");
                    vbool = false;
            }else if(document.getElementById('percentage').value > 25){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Performance Security Percentage cannot be greater than 25</div>");
                    vbool = false;
            }else if(document.getElementById('percentage').value.indexOf("-")!=-1){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Only positve values are allowed</div>");
                    vbool = false;
                }
            else if(document.getElementById('percentage').value.split(".")[1] != undefined){
                if(!regForNumber(document.getElementById('percentage').value) || document.getElementById('percentage').value.split(".")[1].length > 3 || document.getElementById('percentage').value.split(".")[0].length > 12){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }

            }else{

                if(!regForNumber(document.getElementById('percentage').value)){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }else if(document.getElementById('percentage').value.indexOf("-")!=-1){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Only positve values are allowed</div>");
                    vbool = false;
                }
                else if(document.getElementById('percentage').value.length > 12){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }
            }
            if(!vbool){
                return false;
            } else {
                 document.getElementById("submit").style.display = 'none';
            }
        }
        function calcPer(rPrice){
            var per = $('#percentage').val();
            if(!isNaN(per)){
                $('#taka').val(Math.round(((rPrice*per)/100)*1000)/1000);
            }else{
                $('#percentage').val('');
                jAlert("Please enter digits only.","Performance Security alert", function(RetVal) {
                });                
            }
        }
    </script>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <%
                    // Variable tenderId is defined by u on ur current page.
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    pageContext.setAttribute("tab", "2");
                    TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                    List<TblTenderLotSecurity> lots = null;
                    int tendid = 0;
                    int pckgId = 0;
                    String roundId="0";
                    if(request.getParameter("rId")!=null){
                        roundId = request.getParameter("rId");
                    }
                    if (request.getParameter("tenderId") != null) {
                        tendid = Integer.parseInt(request.getParameter("tenderId"));
                        pckgId = Integer.parseInt(request.getParameter("pkgId"));
                        lots = tenderDocumentSrBean.getLotDetailsByLotId(tendid, pckgId);
                    }
                    TblTenderPerformanceSec tenderPerformanceSec = (TblTenderPerformanceSec) AppContext.getSpringBean("TblTenderPerformanceSec");
                    List<TblTenderPerfSecurity> listt = tenderPerformanceSec.getData(tendid, pckgId + "",roundId);
                    /*if("Use STD".equalsIgnoreCase(request.getParameter("submit"))){
                    tenderDocumentSrBean.dumpSTD(String.valueOf(tendid), request.getParameter("txtStdTemplate"), session.getAttribute("userId").toString());
                    response.sendRedirect("LotDetails.jsp?tenderid="+tendid);
                    }*/

                    
                    if("view".equals(request.getParameter("viewAction")))
                    {
                            // Coad added by Dipal for Audit Trail Log.
                            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            String idType="tenderId";
                            int auditId=tendid;
                            String auditAction="View Performance security by PE";
                            String moduleName=EgpModule.Noa.getName();
                            String remarks="";
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);                   
                    }
                    
                    
                    boolean action = false;
                    if("view".equals(request.getParameter("viewAction")))
                    {
                        action = true;
                    }
                    CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                    String roundAmount = "0";
                    List<SPCommonSearchDataMore> roundAmountLst =  dataMoreService.geteGPData("LowestAmtByRound",roundId);
                    if(roundAmountLst!=null && (!roundAmountLst.isEmpty())){
                        roundAmount = roundAmountLst.get(0).getFieldName1();
                    }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Performance Security</title>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>

        <div class="contentArea_1">
            <div class="pageHead_1">Performance Security
<!--                <span class="c-alignment-right"><a href="Evalclarify.jsp?tenderId=<-%=tendid%>&st=rp&comType=TEC" class="action-button-goback">Go back</a></span>-->
                <span class="c-alignment-right"><a href="NOA.jsp?tenderId=<%=tendid%>" class="action-button-goback">Go back</a></span>
            </div>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                        String procnature = commonService.getProcNature(tendid + "").toString();

            %>

            <%
                //Added by Salahuddin
                /*String  tenderID="";
                if (request.getParameter("tenderId") != null) {
                        tenderID = request.getParameter("tenderId");
                    }
                PerformanceSecurityICT perfSecurityICT  = (PerformanceSecurityICT) AppContext.getSpringBean("PerformanceSecurityICT");
                List<GetCurrencyAmount> currencyAmount = new ArrayList<GetCurrencyAmount>();
                currencyAmount= perfSecurityICT.getCurrencyAmount(Integer.parseInt(tenderID));                
                */
            %>
            <%
                //Added by Salahuddin
                /*
                List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", tenderID, null);
                boolean isIctTender = false;
                if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                    isIctTender = true;
                }
                */
            %>
            <div class="tabPanelArea_1 ">
                <form name="frmPerSec" method="post" action="<%= request.getContextPath() %>/TenderPerformanceSecurity">
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <%
                                        if (procnature.equalsIgnoreCase("services")) {
                            %>
                                <th width="10%" class="t-align-center">Package No.</th>
                                <th width="70%" class="t-align-left">Package Description</th>
                            <%} else {%>
                                <th width="10%" class="t-align-center">Lot No.</th>
                                <th width="40%" class="t-align-left">Lot Description</th>
                            <%}%>
                            <th width="15%" class="t-align-left">Lowest Amount</th>
                            <th width="15%" class="t-align-left">Percentage</th>
                            <th width="20%" class="t-align-left">Performance Security Amount in BTN</th>
                        </tr>

                        <%
                            int i = 0;
                            Iterator it = lots.iterator();
                            if (!procnature.equalsIgnoreCase("services")) { // GOODS/ WORKS CASE
                                while (it.hasNext()) {
                                    TblTenderLotSecurity tblTenderLotSecurity = (TblTenderLotSecurity) it.next();
                        %>

                        <tr>
                            <td class="t-align-center"><%= tblTenderLotSecurity.getLotNo()%></td>
                            <td><%=tblTenderLotSecurity.getLotDesc()%></td>

                            <td class="t-align-center">
                                <%=roundAmount%>
                                <input type="hidden" value="<%=roundAmount%>" name="lamount"/>
                                <input type="hidden" value="<%=roundId%>" name="rId"/>
                            </td>
                            <td class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    if(action){
                                        if (!listt.isEmpty() && listt != null) {
                                            out.println(listt.get(0).getPercentage().setScale(3, 0));
                                        }
                                    }else {%>
                                    <input type="text" class="formTxtBox_1"name="percentage" id="percentage" onblur="calcPer(<%=roundAmount%>);"
                                           value="<%if (!listt.isEmpty() && listt != null) {
                                        out.println(listt.get(0).getPercentage().setScale(3, 0));
                                    }
                                       %>"/>
                                <%}} else {%>
                                <input type="text" class="formTxtBox_1"name="percentage" id="percentage" onblur="calcPer(<%=roundAmount%>);"/>
                                <%}%>
                            </td>
                            <td class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    if(action){
                                        if (!listt.isEmpty() && listt != null) {
                                            out.println(listt.get(0).getPerSecurityAmt().setScale(3, 0));
                                        }
                                    }else {%>
                                    <input type="text" readonly class="formTxtBox_1"name="taka" id="taka" value="<%if (!listt.isEmpty() && listt != null) {
                                        out.println(listt.get(0).getPerSecurityAmt().setScale(3, 0));
                                    }
                                       %>"/>
                                <%}} else {%>
                                <input type="text" readonly class="formTxtBox_1"name="taka" id="taka" />
                                <%}%>

                            </td>
                        </tr>
                        <%
                                i++;

                            }
                        } else { //SERVICE CASE
                            int j = 0;
                            List<Object[]> list = commonService.getPkgDetailWithAppPkgId(tendid);
                            for (Object[] obj : list) {
                        %>
                        <tr>
                            <td class="t-align-center"><%= obj[0].toString()%></td>
                            <td><%=obj[1].toString()%></td>
                             <td class="t-align-center">
                                <%=roundAmount%>
                                <input type="hidden" value="<%=roundAmount%>" name="lamount"/>
                                <input type="hidden" value="<%=roundId%>" name="rId"/>
                            </td>
                            <td class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    if(action){
                                        if (!listt.isEmpty() && listt != null) {
                                            out.println(listt.get(0).getPercentage().setScale(3, 0));
                                        }
                                    }else {%>
                                    <input type="text" class="formTxtBox_1"name="percentage" id="percentage" onblur="calcPer(<%=roundAmount%>);"
                                           value="<%if (!listt.isEmpty() && listt != null) {
                                        out.println(listt.get(0).getPercentage().setScale(3, 0));
                                    }
                                       %>"/>
                                <%}} else {%>
                                <input type="text" class="formTxtBox_1" name="percentage" value="" id="percentage" onblur="calcPer(<%=roundAmount%>);"/>
                                <%}%>
                            </td>
                            <td class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                 if(action){
                                       if (!listt.isEmpty() && listt != null) {
                                        out.println(listt.get(0).getPerSecurityAmt().setScale(3, 0));
                                        }
                                    }else{%>
                                <input type="text" readonly class="formTxtBox_1"name="taka" id="taka" value="<%if (!listt.isEmpty() && listt != null) {
                                        out.println(listt.get(0).getPerSecurityAmt().setScale(3, 0));
                                    }
                                       %>"/>
                                <%}} else {%>
                                <input type="text" readonly class="formTxtBox_1"  value="" name="taka" id="taka" />
                                <%}%>
                            </td></tr>
                            <%
                                                j++;

                                            }

                                        }
                            %>
                        <input type="hidden" name="tenderid" value="<%=tendid%>" />
                        <input type="hidden" name="pckid" value="<%=request.getParameter("pkgId")%>" />
                        <input type="hidden" name="isEdit" value="<%if (request.getParameter("isedit") != null) {
                                        out.print(request.getParameter("isedit"));
                                    }
                               %>" />
                        <input type="hidden" name="forupdate" value="<%if (!listt.isEmpty() && listt != null) {
                                        out.print(listt.get(0).getPerCostLotId());
                                    }
                               %>"/>
                        <tr>
                            <td colspan="5" class="t-align-center ff">
                                <% if(!action){ %>
                                <span class="formBtn_1">
                                    <input type="submit" name="submit" id="submit" value="<%if (request.getParameter("isedit") != null) {out.print("Update");}
                                    else{out.print("Submit");}
                               %>" onclick="return validate();" /></span>
                                    <% } %>
                                    <input type="hidden" value="NCT" name="tenderType"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div>&nbsp;</div>

        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

    </body>

</html>
