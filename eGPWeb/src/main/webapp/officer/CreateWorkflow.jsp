
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblAppMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Workflow Details</title>
       
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%

                    String modulName = null;
                    String eventName = null;
                    Integer noOfReviewer = 0;
                    Integer noOfDays = 0;
                    Integer objectId = 0;
                    Integer WfRoleId = 0;
                    String Action = null;
                    Integer wfEvtConfId = 0;
                    String wpId = request.getParameter("wpId");
                    Date actiondate = new Date();
                    boolean iswfLevelExist = false;
                    String isFileProcessed ="No";
                    long wflcont = 0;
                    int uid = 0;
                    byte budgetType = 0;
                    String parentLink="";
                                    if(request.getParameter("parentLink")!=null && !request.getParameter("parentLink").trim().equals("")){
                                       parentLink= request.getParameter("parentLink");
                                       if(parentLink.equals("925"))
                                           parentLink="926";
                                       if(parentLink.equals("97"))
                                           parentLink="98";
                                       if(parentLink.equals("915"))
                                           parentLink="88";
                                       if(parentLink.equals("123"))
                                           parentLink="905";
                                       if(parentLink.equals("112"))
                                           parentLink="933";
                                       if(parentLink.equals("960"))
                                           parentLink="961";
                                    }
                    if (session.getAttribute("userId") != null) {
                        Integer ob1 = (Integer) session.getAttribute("userId");
                        uid = ob1.intValue();
                    }

                    Integer actionby = uid;
                    String isDonorConReq = "NO";
                    String eventid = request.getParameter("eventid");
                    String objectid = request.getParameter("objectid");
                    String activityid = "";
                    int intactId = 0;
                    String strobjId = "";
                    int objId = 0;
                    String tc = "";
                    String loi = "";
                    
                    if (request.getParameter("tc")!=null) {
                            tc = request.getParameter("tc");
                        }
                    
                    if (request.getParameter("loi")!=null) {
                            loi = request.getParameter("loi");
                        }
                    
                    if(request.getParameter("activityid")!=null){
                        activityid = request.getParameter("activityid");
                        intactId = Integer.parseInt(activityid);
                    }
                    if(request.getParameter("objectid")!=null){
                        strobjId = request.getParameter("objectid");
                        objId = Integer.parseInt(strobjId);
                    }
                    
                    Action = request.getParameter("action");
                    String childid = request.getParameter("childid");
                    if(request.getParameter("isFileProcessed")!=null){
                    isFileProcessed = request.getParameter("isFileProcessed");
                    }

                    String logUserId="0";
                    if(session.getAttribute("userId")!=null){
                        logUserId=session.getAttribute("userId").toString();
                    }
                    
                    WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                    workFlowSrBean.setLogUserId(logUserId);
                    
                    boolean check = false;
                    short exist = 0;
                    boolean remus = false;
                    
                    int ru = 0;
                    
                    boolean isDonorCond = false;
                    isDonorCond = workFlowSrBean.isDonorCond(intactId,objId);
                    
                    List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = null;
                     tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(Integer.parseInt(objectid), Integer.parseInt(childid), Short.parseShort(activityid));
                            
                            StringBuffer users = new StringBuffer();
                    for(TblWorkFlowLevelConfig wfl : tblWorkFlowLevelConfig)
                    {
                                iswfLevelExist = true;
                                if(wfl.getFileOnHand().equalsIgnoreCase("Yes")){
                                    exist = wfl.getWfLevel();
                                   remus = true;
                                }else if(remus && wfl.getWfRoleId() != 2){
                                     TblLoginMaster tlm = wfl.getTblLoginMaster();
                                     users.append(tlm.getUserId()+",");
                                    }
                                }
                            if(users.length()>0){
                                users.delete(users.length()-1, users.length());
                                }
                   
                    if ("Submit".equals(request.getParameter("submit")) ) {
                    //changed by nafiul for auto workflow generation
                    
                    //if(true){
//                        if (request.getParameter("textfield1") != null) {
//                            noOfReviewer = Integer.valueOf(request.getParameter("textfield1"));
//                        }
                        noOfReviewer=0;
                        if (request.getParameter("textfield2") != null && !"".equals(request.getParameter("textfield2"))) {
                            noOfDays = Integer.valueOf(request.getParameter("textfield2"));
                        }
                        
                        if (!Action.equalsIgnoreCase("Edit")) {
                            
                            if ("on".equals(request.getParameter("checkbox"))) {
                                isDonorConReq = "YES";
                            }
                        } else if (iswfLevelExist == false) {
                            if ("on".equals(request.getParameter("checkbox"))) {
                                isDonorConReq = "YES";
                            }
                        } else if (request.getParameter("checkbox") != null) {
                            isDonorConReq = request.getParameter("checkbox");
                        }

                        String alreadprocesed = request.getParameter("existrevs");
                        Integer evid = Integer.valueOf(eventid);
                        objectId = Integer.valueOf(objectid);

                        String isfileProc = request.getParameter("isFileProcessed");
                        String donor = "";
                         tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(Integer.parseInt(objectid),
                            Integer.parseInt(childid), Short.parseShort(activityid));
                          StringBuffer mailto = new StringBuffer();
                          MessageProcessSrBean messageProcessSrbean = new MessageProcessSrBean();
                        if (Action.equals("Edit")) {
                            if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid) || "13".equalsIgnoreCase(activityid)){ // Activity Id 13 is added by Dohatec
                            objectid = childid;
                            }
                            eventName = request.getParameter("eventname");
                            modulName = request.getParameter("modulename");
                            donor = workFlowSrBean.isDonorReq(eventid,
                                    Integer.valueOf(objectid));
                            String[] sarr = donor.split("_");
                            int i = 0;
                            short procuse = Short.parseShort(alreadprocesed);
                            procuse = procuse+=1;
                            if (noOfReviewer.intValue() < Integer.parseInt(sarr[1]) && isFileProcessed.equalsIgnoreCase("No")) {
                                for(TblWorkFlowLevelConfig twf : tblWorkFlowLevelConfig){
                                List l =    messageProcessSrbean.getLoginmailIdByuserId(twf.getTblLoginMaster().getUserId());
                                TblLoginMaster loginMaster = (TblLoginMaster) l.get(0);
                                String tomailid = loginMaster.getEmailId();
                                mailto.append(tomailid+",");
                                    }
                                i = workFlowSrBean.flushWorkflowLevels(Integer.parseInt(objectid), Integer.parseInt(childid), Short.parseShort(activityid));
                            }else if(noOfReviewer.intValue() < Integer.parseInt(sarr[1]) && isFileProcessed.equalsIgnoreCase("Yes")){
                                for(short nr = procuse;nr < tblWorkFlowLevelConfig.size() ; nr++){
                                    for(TblWorkFlowLevelConfig twf : tblWorkFlowLevelConfig){
                                        if(twf.getWfLevel() == nr){
                                            List l =    messageProcessSrbean.getLoginmailIdByuserId(twf.getTblLoginMaster().getUserId());
                                            TblLoginMaster loginMaster = (TblLoginMaster) l.get(0);
                                            String tomailid = loginMaster.getEmailId();
                                            mailto.append(tomailid+",");
                                          }
                                        }
                                    i = workFlowSrBean.flushWorkflowLevels(Integer.parseInt(objectid), Integer.parseInt(childid), Short.parseShort(activityid),nr);
                                }
                            }
                            if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid) || "13".equalsIgnoreCase(activityid)){ // Activity Id 13 is added by Dohatec
                            objectid = request.getParameter("objectid");
                            }
                        }
                   if(mailto.length()>0){
                    mailto.deleteCharAt(mailto.length()-1);
                    String[] multimails = mailto.toString().split(",");
                    SendMessageUtil smu = new SendMessageUtil();
                    MailContentUtility mc = new MailContentUtility();
                    List list = mc.conWorkflowforRemoveUsers(eventName, modulName, uid);
                    String sub = list.get(0).toString();
                    String mailText = list.get(1).toString();
                    smu.setEmailTo(multimails);
                    smu.setEmailSub(sub);
                    smu.setEmailMessage(mailText);
                    smu.sendEmail();
                    }
                        if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid) || "13".equalsIgnoreCase(activityid)){ // Activity Id 13 : Added by Dohatec
                            objectId = Integer.parseInt(childid);
                            objectid = childid;
                        }
                        List<CommonMsgChk> listdata = workFlowSrBean.createWorkFlowEvent(noOfReviewer, noOfDays,
                                isDonorConReq, evid, objectId, WfRoleId, Action, wfEvtConfId,
                                actiondate, actionby);                        
                        check = true;
                        RequestDispatcher rd = null;
                        wflcont = workFlowSrBean.getCountOfWorkflowlevelConfig(Integer.parseInt(objectid),
                            Integer.parseInt(childid), Short.parseShort(activityid));
                        if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid) || "13".equalsIgnoreCase(activityid)){ // Activity Id 13 : Added by Dohatec
                            objectId = Integer.parseInt(request.getParameter("objectid"));
                            objectid = request.getParameter("objectid");
                            }
                        if (wflcont > 0) {                            
                            if (Action.equals("Edit")) {System.out.println("========================================================in edit");
                                rd = request.getRequestDispatcher("WorkflowLevelEdit.jsp?eventid=" + eventid
                                        + "&objectid=" + objectId + "&activityid=" + activityid + "&action=" + Action + "&childid=" + childid+"&isFileProcessed="+isfileProc+ "&parentLink=" + parentLink);
                            } else {System.out.println("========================================================in edit else");
//                                rd = request.getRequestDispatcher("WorkflowLevelView.jsp?eventid=" + eventid
//                                        + "&objectid=" + objectId + "&activityid=" + activityid + "&action=" + Action + "&childid=" + childid);
                                rd = request.getRequestDispatcher("APPDashboard.jsp?appID="+objectId);
                            }
                        } else {System.out.println("========================================================in else wflow");
                            rd = request.getRequestDispatcher("WorkflowLevelCreate.jsp?eventid=" + eventid
                                    + "&objectid=" + objectId + "&activityid=" + activityid + "&action=" + "Create" + "&childid=" + childid+ "&parentLink=" + parentLink+"&tc="+tc+"&loi="+loi);
                        }
                        rd.forward(request, response);
                        return;
                    }else if("Next".equals(request.getParameter("next"))){
                     String isfileProc = request.getParameter("isFileProcessed");
                     RequestDispatcher rd = null;
                        wflcont = workFlowSrBean.getCountOfWorkflowlevelConfig(Integer.parseInt(objectid),
                            Integer.parseInt(childid), Short.parseShort(activityid));
                         objectId = Integer.valueOf(objectid);
                        if (wflcont > 0) {
                            if (Action.equals("Edit")) {System.out.println("========================================================in edit ac");
                                rd = request.getRequestDispatcher("WorkflowLevelEdit.jsp?eventid=" + eventid
                                        + "&objectid=" + objectId + "&activityid=" + activityid + "&action=" + Action + "&childid=" + childid+"&isFileProcessed="+isfileProc+"&hed=headTabTender");
                            } else {System.out.println("========================================================in edit ac else");
                                rd = request.getRequestDispatcher("WorkflowLevelView.jsp?eventid=" + eventid
                                        + "&objectid=" + objectId + "&activityid=" + activityid + "&action=" + Action + "&childid=" + childid);
                            }
                        } else {System.out.println("========================================================in edit ac wf else");
                            rd = request.getRequestDispatcher("WorkflowLevelCreate.jsp?eventid=" + eventid
                                    + "&objectid=" + objectId + "&activityid=" + activityid + "&action=" + "Create" + "&childid=" + childid+"&hed=headTabTender"+"&tc="+tc+"&loi="+loi);
                        }
                        rd.forward(request, response);
                        return;
                    }else {
                        if (check == false && Action.equalsIgnoreCase("Create")) {
                            List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData("workflowcreate", activityid, "");
                            Iterator it = editdata.iterator();
                            while (it.hasNext()) {
                                CommonAppData commonAppData = (CommonAppData) it.next();
                                modulName = commonAppData.getFieldName1();
                                eventName = commonAppData.getFieldName2();
                            }
                        } else if (check == false && Action.equalsIgnoreCase("Edit")) {
                            if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                            objectid = childid;
                            }
                            List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData("workflowedit", activityid, objectid);
                            Iterator it = editdata.iterator();
                            while (it.hasNext()) {
                                CommonAppData commonAppData = (CommonAppData) it.next();
                                modulName = commonAppData.getFieldName1();
                                eventName = commonAppData.getFieldName2();
                                noOfReviewer = Integer.valueOf(commonAppData.getFieldName3());
                                noOfDays = Integer.valueOf(commonAppData.getFieldName4());
                                isDonorConReq = commonAppData.getFieldName5();
                            }
                            if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                            objectid = request.getParameter("objectid");
                            }
                        } else if (check == false && Action.equalsIgnoreCase("View")) {
                            String strWF = "";
                            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            switch(Integer.parseInt(activityid))                 
                            {
                                case 1:strWF="for App";break;
                                case 2:strWF="for Tender Notice";break;
                                case 3:strWF="for PreTender Meeting";break;
                                case 4:strWF="for Amendment Approval";break;
                                case 5:strWF="for Tender Opening";break;
                                case 6:strWF="for Evaluation Committee";break;
                                case 7:strWF="for Package Approve";break;    
                                case 8:strWF="for Technical Sub Committee";break;    
                                case 9:strWF="for Cancel Tender";break;    
                                case 10:strWF="for Contract Termination";break;    
                                case 11:strWF="for Variation Order";break;
                                case 12:strWF="for Repeat Order";break;
                                case 13:strWF="for Contract Approval";break;//  Added By Dohatec 
                           }
                            int auditId = Integer.parseInt(objectid);
                             String auditIdType= "";                             
                             if(Integer.parseInt(eventid) == 1){
                                 auditIdType = "appId";
                             }else if(Integer.parseInt(eventid) == 9 || Integer.parseInt(eventid) ==10 || Integer.parseInt(eventid) ==11){
                                 auditIdType = "contractId";
                             }else{
                                 auditIdType = "tenderId";
                             }
                            List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData("workflowedit", activityid, objectid);
                            Iterator it = editdata.iterator();
                            while (it.hasNext()) {
                                CommonAppData commonAppData = (CommonAppData) it.next();
                                modulName = commonAppData.getFieldName1();
                                eventName = commonAppData.getFieldName2();
                                noOfReviewer = Integer.valueOf(commonAppData.getFieldName3());
                                noOfDays = Integer.valueOf(commonAppData.getFieldName4());
                                isDonorConReq = commonAppData.getFieldName5();
                            }
                            AuditTrail auditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                            makeAuditTrailService.generateAudit(auditTrail,auditId , auditIdType, EgpModule.WorkFlow.getName(), "View workFlow "+strWF,"");
                        }
                        List<TblAppMaster> appMaster = workFlowSrBean.checkRevinueByApporTenId(Integer.parseInt(objectid));
                        if (appMaster.size() > 0) {
                            TblAppMaster appm = appMaster.get(0);
                            budgetType = appm.getBudgetType();
                        }
        %>

        <div class="topHeader">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
        </div>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->

        <div class="contentArea_1">
            <div class="pageHead_1">Workflow Details
                <div align="right" style="width: 30%;float: right;" ><% if (Integer.parseInt(activityid) == 1) {%>
                    <a class="action-button-goback" href="APPDashboard.jsp?appID=<%=objectid%>">Go back to Dashboard</a>
                    <% } else if (Integer.parseInt(activityid) == 2 ||Integer.parseInt(activityid)== 9) {%>
                    <a class="action-button-goback" href="Notice.jsp?tenderid=<%=objectid%>">Go back to Tender Dashboard</a>
                    <% } else if (Integer.parseInt(activityid) == 5) {%>
                    <a class="action-button-goback" href="OpenComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                    <% } else if (Integer.parseInt(activityid) == 6) {%>
                    <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                    <% } else if (Integer.parseInt(activityid) == 4) {%>
                    <a class="action-button-goback" href="Amendment.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                    <% } else if (Integer.parseInt(activityid) == 3) {%>
                    <a class="action-button-goback" href="PreTenderMeeting.jsp?tenderId=<%=objectid%>">Go back to Dashboard</a>
                    <% } else if (Integer.parseInt(activityid) == 8 || Integer.parseInt(activityid) == 13) {
                        if(loi.equalsIgnoreCase("Loi")){%>
                        <a class="action-button-goback" href="LOI.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                        <%}else{
                    %> <!-- Activity Id 13 = Contract Approval Workflow : Added By Dohatec -->
                    <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                    <% }}else if (Integer.parseInt(activityid) == 10){%>
                    <a class="action-button-goback" href="TabContractTermination.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
                    <%}else if (Integer.parseInt(activityid) == 11){
                        if(wpId!=null && !"".equalsIgnoreCase(wpId) && !"null".equalsIgnoreCase(wpId))
                        {
                            if("0".equalsIgnoreCase(wpId)){    
                        %>
                            <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
                        <%    
                            }
                        }else{
                    %>
                    <a class="action-button-goback" href="DeliverySchedule.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
                    <%}}else if (Integer.parseInt(activityid) == 12){%>
                    <a class="action-button-goback" href="repeatOrderMain.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
                   <!-- Added By Dohatec Start !-->
                    <% }else if(Integer.parseInt(activityid) == 13){
                        if(loi.equalsIgnoreCase("Loi")){%>
                        <a class="action-button-goback" href="LOI.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                        <%}else{
                    %> <!-- Activity Id 13 = Contract Approval Workflow : Added By Dohatec -->
                    <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                    <% }}else if(Integer.parseInt(activityid) == 24){ %>
                    <a class="action-button-goback" href="LOI.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                    <%}%>
                    
                </div> </div>
            <div>
                <% 
                if (Integer.parseInt(activityid) == 1) { %>
                <div> <jsp:include page="InformationBar.jsp">
                        <jsp:param name="appId" value="<%=objectid%>" />
                    </jsp:include></div>
                    <%  } else if (Integer.parseInt(activityid) == 2 || Integer.parseInt(activityid) == 5
                             || Integer.parseInt(activityid) == 6 || Integer.parseInt(activityid) == 4 || Integer.parseInt(activityid) == 3
                             || Integer.parseInt(activityid) == 8 || Integer.parseInt(activityid) == 9 || Integer.parseInt(activityid) == 13) { // Activity Id 13 = Contract Approval Workflow : Added By Dohatec 
                        pageContext.setAttribute("tenderId", objectid);
                    %>

                <div> <%@include file="../resources/common/TenderInfoBar.jsp" %></div>
                <%
                    } else if(Integer.parseInt(activityid) == 10){
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>

                <div> <%@include file="../resources/common/ContractInfoBar.jsp" %></div>

                <% } %>
            </div>
            <div class="tableHead_1 t_space">Workflow :</div>
            <input name="tenderId" type="hidden" value="<%=request.getParameter("tenderId")%>" />
            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                <form name="frmworkflowevent" method="post">
                    <input type="hidden" name="alreadyProcess" id="alreadyProcess" value="no" />
                    <%
                                if (Action.equalsIgnoreCase("Create")) {

                    %>
                    <tr>
                        <td width="20%" class="ff">Module :</td>
                        <td width="80%"><%=modulName%> </td>
                    </tr>
                    <tr>
                        <td class="ff">Process :</td>
                        <td><%=eventName%></td>
                    </tr>

                    <tr>
                        <td class="ff">No. of Reviewers : <span>*</span></td>
                        <td><input name="textfield1" type="text" class="formTxtBox_1" id="textfield1" style="width:100px;" onblur="if(chkNumeric(this,'revLevel1')){clearrevLevel();}" />
                            <input name="eventid" type="hidden" value="<%=eventid%>"/>
                            <input name="objectid" type="hidden" value="<%=objectid%>"/>
                            <input name="action" type="hidden"   value="<%=Action%>" />
                            <input name="activityid" type="hidden" value="<%=activityid%>" />
                            <input name="childid" type="hidden" value="<%=childid%>" />
                            <input name="existrevs" id="existrevs" value="0" type="hidden" />
                            <input name="isFileProcessed" id="isFileProcessed" type="hidden" value="<%=isFileProcessed%>" />
                            <span id="revLevel1" class="reqF_1"></span>
                            <!-- <span class="reqF_1">
                             Please enter reviewer levels
		<br />
                             Please enter numbers only</span> -->
                        </td>
                    </tr>

                    <tr id="txtesc">
                        <td class="ff">No. of Days for File Escalation : <span>*</span></td>
                        <td><input name="textfield2" type="text" class="formTxtBox_1" id="textfield2" style="width:100px;" onblur="if(chkNumericEsc(this,'daysfileesc1')){clearfileEsc();}"/>
                            <span id="daysfileesc1" class="reqF_1"></span>
                            <!-- <span class="reqF_1">
                          Please select no. of days for file escalation
                          <br />
		Please enter numbers only</span> -->
                        </td>
                    </tr>
                    <%  if (isDonorCond) {%>
                    <tr id="chkbox">
                        <td class="ff">Donor Concurrence Requires : </td>
                        <td class="txt_2"><input type="checkbox" name="checkbox" id="checkbox" /></td>
                    </tr>
                    <%  }%>
                    <tr>
                        <td>&nbsp;</td>
                        <td><label class="formBtn_1">
                                <%  if (isDonorCond) { %>
                                <input type="submit" name="submit" id="button" value="Submit"  onclick="return Validate();"/>
                                <% } else {%>
                                <input type="submit" name="submit" id="button" value="Submit" onclick="return withoutDpValidate();" />
                                <%  }%>
                            </label>
                        </td>
                    </tr>
                    <%
                              } else if (Action.equalsIgnoreCase("Edit")) {

                    %>
                    <tr>
                        <td width="15%" class="ff">Module :</td>
                        <td width="85%" ><%=modulName%></td>
                    </tr>
                    <tr>
                        <td class="ff">Process :</td>
                        <td><%=eventName%></td>
                    </tr>

                    <tr>
                        <td class="ff">No. of Reviewers : <span>*</span></td>
                        <td><input name="textfield1" type="text" value="<%=noOfReviewer%>" class="formTxtBox_1" id="textfield1" style="width:100px;" onblur="if(chkNumeric(this,'revLevel1')){clearrevLevel();}"/>                            
                            <input name="eventid" type="hidden" value="<%=eventid%>"/>
                            <input name="objectid" type="hidden" value="<%=objectid%>"/>
                            <input name="action" type="hidden"   value="<%=Action%>" />
                            <input name="activityid" type="hidden" value="<%=activityid%>" />
                            <input name="childid" type="hidden" value="<%=childid%>" />
                            <input name="isFileProcessed" id="isFileProcessed" type="hidden" value="<%=isFileProcessed%>" />
                            <input name="existrevs" id="existrevs" value="<%=exist%>" type="hidden" />
                            <input name="remrvs" id="remrvs" value="<%=users%>" type="hidden" />
                            <input name="eventname" id="eventname" value="<%=eventName%>" type="hidden" />
                            <input name="modulename" id="modulename" value="<%=modulName%>" type="hidden" />
                            <input name="wpId" id="wpId" value="<%=wpId%>" type="hidden" />
                             <span id="revLevel1" class="reqF_1"></span>
                            <!-- <span class="reqF_1">
                          Please enter reviewer levels
		<br />
                          Please enter numbers only</span> -->
                        </td>
                    </tr>
                    <%
                                  String str = " ";
                                  if(noOfReviewer == 0) {
                                      str = "display:none;";

                  }%>

                    <tr id="txtesc" style="<%=str%>">
                        <td class="ff">No. of Days for File Escalation : <span>*</span></td>
                        <td><input name="textfield2" type="text" value="<%=noOfDays%>" class="formTxtBox_1" id="textfield2" style="width:100px;" onblur="if(chkNumericEsc(this,'daysfileesc1')){clearfileEsc();}"/>
                            <span id="daysfileesc1" class="reqF_1"></span>
                            <!-- <span class="reqF_1">
                          Please select no. of days for file escalation
                          <br />
		Please enter numbers only</span> -->
                        </td>
                    </tr>
                    <%
                        if (isDonorConReq.equalsIgnoreCase("yes") && iswfLevelExist == true && isDonorCond) {%>
                    <tr id="chkbox" style="<%=str%>">
                        <td class="ff">Donor Concurrence Requires : </td>
                        <td class="ff">
                            <input type="hidden"  name="checkbox" id="checkbox" value="YES"/>
                            <label style="">YES</label>
                        </td>
                    </tr>
                    <%  } else if (isDonorConReq.equalsIgnoreCase("No") && iswfLevelExist == true && isDonorCond) {%>
                    <tr id="chkbox" style="<%=str%>">
                        <td class="ff">Donor Concurrence Requires : </td>
                        <td class="ff">
                            <input type="hidden"  name="checkbox" id="checkbox" value="No"/>
                            <label style="">No</label>
                        </td>
                    </tr>
                    <%  } else if (iswfLevelExist == false && isDonorCond) {%>
                    <tr id="chkbox" style="<%=str%>">
                        <td class="ff">Donor Concurrence Requires : </td>
                        <% if (isDonorConReq.equalsIgnoreCase("Yes") && noOfReviewer != 0) {%>
                        <td class="txt_2"><input type="checkbox" name="checkbox" id="checkbox" checked="true" /></td>
                        <% } else {%>
                        <td class="txt_2"><input type="checkbox"  name="checkbox" id="checkbox" /></td>
                        <% }%>
                    </tr>
                    <%} else if (isDonorCond) {%>
                    <tr id="chkbox" style="<%=str%>">
                        <td class="ff">Donor Concurrence Requires : </td>
                        <input type="hidden"  name="checkbox" id="checkbox" value="No" />
                    </tr>
                    <% }%>
                    <tr>
                        <td>&nbsp;</td>
                        <td><label class="formBtn_1">
                                <% if (isDonorConReq.equalsIgnoreCase("Yes") && iswfLevelExist == true && isDonorCond) {%>
                                <input type="submit" name="submit" id="button" value="Submit"  />
                                <%  } else if (!iswfLevelExist && budgetType != 2 && budgetType != 3 && isDonorCond) {%>
                                <input type="submit" name="submit" id="button" value="Submit" onclick="return Validate();" />
                                <%  } else if (isDonorCond) {%>
                                <input type="submit" name="submit" id="button" value="Submit" onclick="return withoutDpValidate();" />
                                <% } else {%>
                                <input type="submit" name="submit" id="button" value="Submit" onclick="return validateRevandDays();"  />
                                <% }%>
                            </label>&nbsp;
                            <label class="formBtn_1">
                                <input type="submit" name="next" id="button" value="Next"
                                       <% if (!iswfLevelExist && isDonorCond) {%>
                                       onclick="return Validate();"
                                       <%  } else if (isDonorCond) {%>
                                       onclick="return withoutDpValidate();"
                                       <% } %>  />
                            </label>
                        </td>
                    </tr>
                    <%
                              } else if (Action.equalsIgnoreCase("View")) {
                    %>

                    <tr>
                        <td class="ff" width="12%">Module :</td>
                        <td width="35%">
                            <label><%=modulName%></label>

                        </td>
                            <td class="ff" width="10%">Process :</td>
                            <td width="45%"> <label><%=eventName%></label>
                        </td>
                    </tr>


                    <tr>
                        <!--<td class="ff">No. of Reviewers : </td>
                        <td><label><%=noOfReviewer%></label>-->
                            <input name="textfield1" type="hidden" value="<%=noOfReviewer%>" class="formTxtBox_1" id="textfield1" style="width:100px;" />
                            <input name="eventid" type="hidden" value="<%=eventid%>"/>
                            <input name="objectid" type="hidden" value="<%=objectid%>"/>
                            <input name="action" type="hidden"   value="<%=Action%>" />
                            <input name="activityid" type="hidden" value="<%=activityid%>" />
                            <input name="childid" type="hidden" value="<%=childid%>" />
                            <input name="wpId" type="hidden" value="<%=wpId%>" />
                        </td>
                        <%  if(noOfReviewer != 0){ %>
                        <td class="ff">No. of Days for File Escalation : </td>
                        <td><label><%=noOfDays%></label>
                            <input name="textfield2" type="hidden" value="<%=noOfDays%>" class="formTxtBox_1" id="textfield2" style="width:100px;" />
                        </td>
                        <% }else{ %>
                         <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <% } %>
                    </tr>

                    <% if (isDonorConReq.equalsIgnoreCase("yes") && isDonorCond) {%>
                    <tr>
                        <td class="ff">Donor Concurrence Requires : </td>
                        <td >
                            <label class="ff" >YES</label>
                            <input type="hidden"  name="checkbox" id="checkbox" value="YES"/>

                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <%  }else if (isDonorCond) {%>
                    <input type="hidden"  name="checkbox" id="checkbox" value="No" />
                    <% }%>

                    <% }%>

                </form>
            </table>
            <% if (Action.equalsIgnoreCase("View")) {%>
            <div class="tableHead_1 t_space">Workflow Level :</div>
            <jsp:include page="WorkFlowLevelViewGrid.jsp">
                <jsp:param name="activityid" value="<%= activityid %>" />
            </jsp:include>
            <% }%>
            <%  }%>
        </div>
        <!--Dashboard Content Part End-->
        <!--Dashboard Footer Start-->
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        <!--Dashboard Footer Ends-->
        
    </body>
        <%
            if(request.getParameter("activityid")!=null && !"1".equals(request.getParameter("activityid"))){ %>
        <script type="text/javascript" language="Javascript">
             var headSel_Obj = document.getElementById("headTabWorkFlow");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        </script>
        <% } %>
          <script type="text/javascript">
            function numeric(value)
            {
                return /^\d+$/.test(value);
            }
            function Validate()
            {
                var validatebool="";
                if(document.getElementById("textfield1").value=='')
                {
                    document.getElementById("revLevel1").innerHTML="Please enter No. of Reviewers";
                    validatebool="false";
                }
                else
                {
                    if(!numeric(document.getElementById("textfield1").value))
                    {
                        document.getElementById("revLevel1").innerHTML="Please enter Numeric Data";
                        validatebool="false";
                    }
                    if(document.getElementById("textfield1").value>15)
                    {
                        document.getElementById("revLevel1").innerHTML="Maximum reviewer should be 15";
                        validatebool="false";
                    }
                    
                }
                if(document.getElementById('txtesc').style.display!='none'){
                    if(document.getElementById("textfield2").value=='' && document.getElementById('textfield1').value!=0)
                    {
                        document.getElementById("daysfileesc1").innerHTML="Please enter No. of Days for File Escalation";
                        validatebool="false";
                    }
                    else
                    {
                        if(!numeric(document.getElementById("textfield2").value))
                        {
                            document.getElementById("daysfileesc1").innerHTML="Please enter Numeric Data";
                            validatebool="false";
                        }
                        if(document.getElementById("textfield1").value>15)
                        {
                            document.getElementById("revLevel1").innerHTML="Maximum reviewer should be 15";
                            validatebool="false";
                        }
                    }
                }
                
                if(validatebool=='false'){
                    return false;
                } else {
                    document.getElementById("submit").style.display = 'none';
                }

                if(document.getElementById("checkbox").checked == false && document.getElementById('textfield1').value!=0)
                {
                    if(validatebool==''){
                        var r=confirm("Please confirm donor concurrence option selection");
                        if (r!=true)
                        {
                            return false;
                        }
                    }else{
                        return false;
                    }
                }
                else if(document.getElementById("checkbox").checked == true)
                {
                       return true;
                }
                else if(document.getElementById("textfield1").value == '')
                {
                        return false;
                }
         }
        
        function validateRevandDays(){
            
            var validatebool=true;
            var existrevs = document.getElementById("existrevs").value;
            existrevs = existrevs-1;
            if(document.getElementById("textfield1").value=='')
            {
                document.getElementById("revLevel1").innerHTML="Please enter No. of Reviewers";
                validatebool=false;
            }
            else
            {
                if(!numeric(document.getElementById("textfield1").value))
                {
                    document.getElementById("revLevel1").innerHTML="Please enter Numeric Data";
                    validatebool=false;
                }
                if(document.getElementById("textfield1").value>15)
                        {
                            document.getElementById("revLevel1").innerHTML="Maximum reviewer should be 15";
                            validatebool="false";
                        }
            }
            if(document.getElementById("alreadyProcess").value=="yes"){
                jAlert("Already "+existrevs+" reviewer has process the file in a Workflow. You can't reduce the No. of Reviewers less than "+existrevs+"."," Alert ", "Alert");
                validatebool=false;
            }
            
            if(document.getElementById('txtesc').style.display!='none'){
                if(document.getElementById("textfield2").value=='' && document.getElementById('textfield1').value!=0)
                {
                    document.getElementById("daysfileesc1").innerHTML="Please enter No. of Days for File Escalation";
                    validatebool=false;
                }
                else
                {
                    if(!numeric(document.getElementById("textfield2").value))
                    {
                        document.getElementById("daysfileesc1").innerHTML="Please enter Numeric Data";
                        validatebool=false;
                    }
                    if(document.getElementById("textfield1").value<15)
                        {
                            
                        }else{
                            document.getElementById("revLevel1").innerHTML="Maximum reviewer should be 15";
                            validatebool="false";
                        }
                }
            }
            return validatebool;
        }
                            function withoutDpValidate()
                            {

                                var validatebool="";
                                if(document.getElementById("textfield1").value=='')
                                {
                document.getElementById("revLevel1").innerHTML="Please enter No. of Reviewers";
                                    validatebool="false";
                                }
                                else
                                {
                                    if(!numeric(document.getElementById("textfield1").value))
                                    {
                                        document.getElementById("revLevel1").innerHTML="Please enter Numeric Data";
                                        validatebool="false";
                                    }
                                    if(document.getElementById("textfield1").value>15)
                                    {
                                        document.getElementById("revLevel1").innerHTML="Maximum reviewer should be 15";
                                        validatebool="false";
                                    }
                                }

                                if(document.getElementById('textfield1').value!=0)
                                {
                                    if(document.getElementById("textfield2").value=='')
                                    {
                                                        document.getElementById("daysfileesc1").innerHTML="Please enter No. of Days for File Escalation";
                                                        validatebool="false";
                                                    }
                                                    else
                                                    {

                                        if(!numeric(document.getElementById("textfield2").value))
                                                        {
                                            document.getElementById("daysfileesc1").innerHTML="Please enter Numeric Data";
                                                            validatebool="false";
                                                        }
                                                    }
                                }            
            
                                if (validatebool=='false'){
                                    return false;
                                } else {
                                   document.getElementById("submit").style.display = 'none';
                                }

//            var r=confirm("Please make sure that the Donor Concurrence is required.");
//            if (r!=true)
//            {
//                return false;
//            }else{
//                return true;
//            }
                            }
                            function clearrevLevel(){
                                var validatebool="";
                                document.getElementById("revLevel1").innerHTML="";
                                if(document.getElementById("textfield1").value!='')
                                {

                                    if(numeric(document.getElementById("textfield1").value))
                                    {
                                        document.getElementById("revLevel1").innerHTML="";
                                        if(document.getElementById("textfield1").value>15)
                                        {
                                            document.getElementById("revLevel1").innerHTML="Maximum reviewer should be 15";
                                            validatebool="false";
                                        }
                                    }
                                    else
                                    {
                                        document.getElementById("revLevel1").innerHTML="Please enter Numeric Data";
                                        validatebool="false";
                                    }

                                }
                                if (validatebool=='false'){
                                    return false;
                                } else {
                                 //   document.getElementById("submit").style.display = 'none';
                                }
                            }

                            function clearfileEsc(){
                                var validatebool="";
                                if(document.getElementById("textfield2").value!='')
                                {
                                    if(numeric(document.getElementById("textfield2").value))
                                    {
                                        document.getElementById("daysfileesc1").innerHTML="";
                                    } else {
                                        document.getElementById("daysfileesc1").innerHTML="Please enter Numeric Data";
                                        validatebool="false";
                                    }
                                }
                                if (validatebool=='false'){
                                    return false;
                                }
                            }
                            function chkNumeric(obj,msgId){
                                var strValidChars = "0123456789";
                                var strChar;
                                var blnResult = true;
                                var checkfileproc = false;
                                var remuser = false;
                                var strString = obj.value;
                                document.getElementById("alreadyProcess").value = "no";
                                
                                if (strString.length == 0){
                                    return false;
                                }
                               var isfileproc = document.getElementById("isFileProcessed").value;
                               var existrevs = document.getElementById("existrevs").value;
                               existrevs = existrevs-1;

                                var newrev = document.getElementById("textfield1").value;
                                if(existrevs>newrev){
                                   checkfileproc = true;
                                }
               
                               //alert(newrev);
                                for (i = 0; i < strString.length && blnResult == true; i++)
                                {
                                    strChar = strString.charAt(i);
                                    if (strValidChars.indexOf(strChar) == -1)
                                    {
                                        blnResult = false;
                                        document.getElementById(msgId).innerHTML = "<br/>Please enter Numeric Data";
                                    } else if(numeric(document.getElementById("textfield1").value)) {
                                        //alert(strChar);
                                        document.getElementById(msgId).innerHTML = "";
                                        //alert(isfileproc);
                                        if(document.getElementById("textfield1").value == 0 && isfileproc == 'No'){
                                            $('#txtesc').hide();
                                            $('#chkbox').hide();
                                            $('#chkbox').attr('checked', false);
                                            document.getElementById("textfield2").value='';
                                        }else if(document.getElementById("textfield1").value == 0 && isfileproc == 'Yes' && checkfileproc == true){
                                           jAlert("Already "+existrevs+" reviewer has process the file in a Workflow. You can't reduce the No. of Reviewers less than "+existrevs+"."," Alert ", "Alert");
                                           document.getElementById("alreadyProcess").value = "yes";
                                           blnResult = false;
                                        }else if(document.getElementById("textfield1").value != 0 && document.getElementById("textfield2").value == 0){
                                            document.getElementById("textfield2").value='';
                                            $('#txtesc').show();
                                            $('#chkbox').show();
                                            if($('#txtesc').style)
                                            {
                                                $('#txtesc').style.display='inline';
                                            }
                                            if($('#chkbox').style)
                                            {
                                                $('#chkbox').style.display='inline';
                                            }
                                            if(document.getElementById("checkbox")) {
                                                document.getElementById("checkbox").checked = false
                                            }
                                        }else if(document.getElementById("textfield1").value != 0 && document.getElementById("textfield2").value != 0){
                                                $('#txtesc').show();
                                                $('#chkbox').show();
                                                $('#txtesc').style.display='inline';
                                                $('#chkbox').style.display='inline';
                                        }
                                        blnResult = true;
                                    } else {
                                        blnResult = false;
                                        document.getElementById(msgId).innerHTML = "Please enter Numeric Data.";
                                    }
                                }
                                return blnResult;
                            }

                            function chkNumericEsc(obj,msgId){
                                var strValidChars = "0123456789";
                                var strChar;
                                var blnResult = true;
                                var strString = obj.value;
                                if (strString.length == 0)
                                    return false;

                                for (i = 0; i < strString.length && blnResult == true; i++)
                                {
                                    strChar = strString.charAt(i);
                                    if (strValidChars.indexOf(strChar) == -1)
                                    {
                                        blnResult = false;
                                        document.getElementById(msgId).innerHTML = "<br/>Please enter Numeric Data";
                                    } else {
                                        blnResult = true;
                                        document.getElementById(msgId).innerHTML = "";
                                    }
                                }

                                return blnResult;
                            }
        </script>
</html>
