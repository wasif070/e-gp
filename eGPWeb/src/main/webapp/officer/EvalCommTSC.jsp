<%-- 
    Document   : EvalCommTSC
    Created on : Dec 7, 2010, 4:54:33 PM
    Author     : rajesh
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%--<jsp:useBean id="tscApptoAaSrBean" class="com.cptu.egp.eps.web.servicebean.TscApptoAaSrBean" scope="request"/>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--        <title>e-GP Officer - Evaluation</title>-->
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />        
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
<!--        <title>Committee Evaluation</title>-->
        <!--jalert -->

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" >
            function checkTender(){
                jAlert("there is no Tender Notice"," Alert ", "Alert");
            }


            function checkdefaultconf(){
                jAlert("Default workflow configuration hasn't created"," Alert ", "Alert");
            }
            function checkDates(){
                jAlert(" please configure dates"," Alert ", "Alert");
            }
        </script>
    </head>
    <body>
        <%//@include  file="../resources/common/AfterLoginTop.jsp"%>
        <%
                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                    }
        %>
<!--        <div class="tabPanelArea_1">-->
<!--        <div class="pageHead_1">Tender Dashboard
        <span style="float: right;" ><a href="<%=referer%>" class="action-button-goback">Go Back</a></span>
        </div>-->
        <%
                    // Variable tenderId is defined by u on ur current page.
                  //  pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                  //  pageContext.setAttribute("tab", "7");
                    //pageContext.setAttribute("TSCtab", "1");
                    String tid = request.getParameter("tenderid");
%>
        <%//@include file="../resources/common/TenderInfoBar.jsp" %>
        <!--<%//if(!"TEC".equalsIgnoreCase(request.getParameter("comType"))){ %>
        <%--<jsp:include page="officerTabPanel.jsp" >
             <jsp:param name="tab" value="7" />
        </jsp:include>--%>
        <%//}%>-->
         <%
                            pageContext.setAttribute("TSCtab", "2");
                            String msg ="";
                         msg = request.getParameter("msg");
                %>
                
<!--                        <div class="tabPanelArea_1">-->

                    <%--<% if (msg != null && !msg.contains("not")) {%>
                    <div class="responseMsg successMsg" >File processed successfully</div>
                    <%  } else if (msg != null && msg.contains("not")) {%>
                   <div class="responseMsg errorMsg">File has not processed successfully</div>
                    <%  }%>--%>
                <%if("y".equals(request.getParameter("ispub"))){%><div class="responseMsg successMsg" style="margin-bottom: 12px;">Committee published successfully</div><%}%>
                <%
                TenderCommonService tenderCommonService = new TenderCommonService();
                tenderCommonService = (TenderCommonService)AppContext.getSpringBean("TenderCommonService");
                    String uId = session.getAttribute("userId").toString();
                        List<SPTenderCommonData> listPe = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tid,null);
                        String peId = "";
                        if(!listPe.isEmpty()){
                                peId = listPe.get(0).getFieldName1();
                            }
                  if(uId.equalsIgnoreCase(peId)){
                %>
                <%//@include  file="../resources/common/AfterLoginTSC.jsp"%>
                <%}else{
                    //String tenderid = request.getParameter("tenderid");
                }%>
             <%
                //String tcsStatus = tscApptoAaSrBean.getStatus(Integer.parseInt(tid));
                CommitteMemberService committeMemberService = (CommitteMemberService)AppContext.getSpringBean("CommitteMemberService");
                int uid = 0;
                   if(session.getAttribute("userId") != null){
                       Integer ob1 = (Integer)session.getAttribute("userId");
                       uid = ob1.intValue();
                   }
                        WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                        short eventid = 7;
                        int objectid = 0;
                        short activityid = 8;
                        int childid = 1;
                        short wflevel = 1;
                        int initiator = 0;
                        int approver = 0;
                        boolean isInitiater = false;
                        boolean evntexist = false;
                        String fileOnHand = "No";
                        String checkperole = "No";
                        String chTender = "No";
                        boolean ispublish = false;
                        boolean iswfLevelExist = false;
                        boolean isconApprove = false;
                        String  evalTsc = "";
                        boolean isEvalCommPub = false;
                         String objtenderId = request.getParameter("tenderid");
                            if (objtenderId != null) {
                                objectid = Integer.parseInt(objtenderId);
                                childid = objectid;
                            }

                             String donor = "";
                              donor = workFlowSrBean.isDonorReq(String.valueOf(eventid),
                              Integer.valueOf(objectid));
                             String[] norevs = donor.split("_");
                              int norevrs = -1;

                              String strrev = norevs[1];
                              if(!strrev.equals("null")){

                               norevrs = Integer.parseInt(norevs[1]);
                               }
                               
                            evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
                            List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
                            // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);

                            
                            if (tblWorkFlowLevelConfig.size() > 0) {
                                Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                iswfLevelExist = true;
                                while (twflc.hasNext()) {
                                    TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                    TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                    if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                                        fileOnHand = workFlowlel.getFileOnHand();
                                    }
                                    if (workFlowlel.getWfRoleId() == 1) {
                                        initiator = lmaster.getUserId();
                                    }
                                    if (workFlowlel.getWfRoleId() == 2) {
                                        approver = lmaster.getUserId();
                                    }
                                    if(fileOnHand.equalsIgnoreCase("yes")&& uid == lmaster.getUserId()){
                                        isInitiater = true;
                                        }

                                }

                            }


                             String userid = "";
                           if(session.getAttribute("userId") != null){
                             Object obj = session.getAttribute("userId");
                             userid = obj.toString();
                             }
                              String field1 = "CheckPE";
                             List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1,userid, "");

                             if(editdata.size()>0)
                                 checkperole = "Yes";
                            
                    // for publish link

                        TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        List<SPTenderCommonData> checkTsc = tenderCommonService1.returndata("TSCWorkFlowStatus", request.getParameter("tenderid"),  "'Pending'");
                        
                        if(checkTsc.size()>0)
                            chTender = "Yes";
                        List<SPTenderCommonData> publist = tenderCommonService1.returndata("TSCWorkFlowStatus", request.getParameter("tenderid"),  "'Approved','Conditional Approval'");
                         List<SPTenderCommonData> isEvalPub = tenderCommonService1.returndata("EvalComWorkFlowStatus", request.getParameter("tenderid"),  "'Approved','Conditional Approval'");
                        
                        if(isEvalPub.size()>0){
                             
                            SPTenderCommonData  spTenderCommondata = isEvalPub.get(0);
                            String evalcomstatus = spTenderCommondata.getFieldName1();
                            
                            if(evalcomstatus.equalsIgnoreCase("Approved"))
                                isEvalCommPub = true;
                        }
                         String conap = "";
                        if(publist.size()>0){
                            ispublish = true;
                            SPTenderCommonData  spTenderCommondata = publist.get(0);
                            evalTsc = spTenderCommondata.getFieldName1();
                            conap = spTenderCommondata.getFieldName2();
                            if(conap.equalsIgnoreCase("Conditional Approval"))
                                isconApprove = true;
                                      }
                        
            %>
<!--            <div>&nbsp;</div>-->
<!--            <div class="tabPanelArea_1">-->
                <%-- Sub Panal --%>
                <%if (isInitiater || checkperole.equals("Yes")){ %>
                <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1">
                    <tr>
                        <td class="t-align-left ff" width="30%">Technical Sub Committee Formation</td>
                    <td>
                        <%
                        boolean isTscForm = true;  
                        com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean tenderCommitteSrBean = new com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean();
                        com.cptu.egp.eps.dao.storedprocedure.CommitteDtBean bean = tenderCommitteSrBean.findCommitteDetails(Integer.parseInt(request.getParameter("tenderid")), "tsccom");
                        bean = tenderCommitteSrBean.findCommitteDetails(Integer.parseInt(request.getParameter("tenderid")), "tsccom");
                        tenderCommitteSrBean=null;
                        if(bean.getpNature()!=null){
                        if((committeMemberService.checkCountByHql("TblCommittee tc","tc.tblTenderMaster.tenderId="+request.getParameter("tenderid")+" and tc.committeeType='TSC'")==0)){
                            isTscForm  = false;/*If TSC IS FORM THEN CREATE WORKFLOW LINK SHOULD BE VISIBLE AS PER BUG 4412*/
                            if(isEvalCommPub){
                           %>
                        <a href="TSCFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>">Create TSC</a>&nbsp;|&nbsp;<a href="MapCommittee.jsp?tenderid=<%=request.getParameter("tenderid")%>&type=3">Use Existing Committee</a>
                        <% } 
                        } else {
                            if((committeMemberService.checkCountByHql("TblCommittee tc","tc.tblTenderMaster.tenderId="+request.getParameter("tenderid")+" and tc.committeeType='TSC' and tc.committeStatus='pending'")==0)){%>
                            <a href="TSCFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&isview=y">View TSC</a>
                            
                            <%}else{
                                if(!"Approved".equalsIgnoreCase(conap)){
                            %>
                                <a href="TSCFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&isedit=y">Edit TSC</a>
                                &nbsp;|&nbsp;
                               <% } %> 
                                <a href="TSCFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&isview=y">View TSC</a>
                                
                                
                                <%--<%if(tcsStatus.equalsIgnoreCase("error")){%>
                                <a href="<%=request.getContextPath()%>/EvalTscAppToAa?tenderId=<%=tid%>&action=create">&nbsp;|&nbsp;Send AA for Approval</a>
                                <%}%>--%>
                                <% if((ispublish == true && isInitiater == true && !evalTsc.equalsIgnoreCase("Approved") )  || ((initiator == approver && norevrs == 0) && isInitiater == true && !evalTsc.equalsIgnoreCase("Approved") && initiator !=0 && approver !=0 ) ){ %>
                                    &nbsp;|&nbsp;<a href="TSCFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&ispub=y">Notify TSC Members</a>
                                <%}
                            }
                        }
                        }else{
                            out.print("<div class='responseMsg noticeMsg'>Business rule yet not configured</div>");
                        }%>
                    </td>
                </tr>
                <%if(isTscForm){/*If TSC IS FORM THEN CREATE WORKFLOW LINK SHOULD BE VISIBLE AS PER BUG 4412*/%>
                 <tr>
                    <td width="30%" class="t-align-left ff">Workflow </td>
                    <td width="80%" class="t-align-left">
                          <% if(evntexist){

                   if(fileOnHand.equalsIgnoreCase("yes") && checkperole.equals("Yes") && ispublish == false && isInitiater == true ){

                     %>
                    <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=915'>Edit</a>

                        &nbsp;|&nbsp;
                    <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=737">View</a>&nbsp;|&nbsp;

                   <% }else if(iswfLevelExist == false && checkperole.equals("Yes") && ispublish == false ){
                        %>

                        <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=915'>Edit</a>
                         &nbsp;|&nbsp;
                        <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=737">View</a>&nbsp;|&nbsp;
                        <%  }else if(iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")){ %>

                      <label>Workflow yet not configured</label>
                       <%
                      }else if(iswfLevelExist == true){  %>
                        <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=737">View</a>
                      &nbsp;|&nbsp;
                    <%
                     }  }else if(ispublish == false && checkperole.equals("Yes")){
                         List<CommonAppData> defaultconf =  workFlowSrBean.editWorkFlowData("WorkFlowRuleEngine",Integer.toString(eventid), "");
                        if(defaultconf.size()>0){
                   %>
                    <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&parentLink=123">Create</a>
                     
                    <% }else{ %>
                    <a  href="#" onclick="checkdefaultconf()">Create</a>
                    <%
                       }
                   }else if(iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")){ %>
                     <label>Workflow yet not configured</label>
                          <% }  %>
                       <%
                           if( isInitiater == true && (initiator != approver || (initiator == approver && norevrs > 0)) && ispublish == false && fileOnHand.equalsIgnoreCase("Yes")){
                                %>
                               <% if(chTender.equalsIgnoreCase("Yes")){ %>
                                  <a href="FileProcessing.jsp?activityid=<%=activityid%>&tenderId=<%=tid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&fromaction=tsceval" >Process file in Workflow</a>
                                   &nbsp;|&nbsp;
                    <% } } %>
                          <%
                    if(iswfLevelExist){ %>
                     <a href="workFlowHistory.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&userid=<%=uid%>&fraction=tsceval&parentLink=81" >View Workflow History</a>
                     

                    <% }%>

                    </td>
                </tr>
                <%}/*If TSC IS FORM THEN CREATE WORKFLOW LINK SHOULD BE VISIBLE AS PER BUG 4412*/%>
                <%--<tr>
                        <td width="30%" class="t-align-left ff">View TOS/POS Shared by TOC/POC</td>
                        <td width="80%" class="t-align-left">
                            <%
                                        List<SPTenderCommonData> lstTOSReport = tenderCommonService1.returndata("CheckValueIntblTosRptShare", tid, null);

                                        if (lstTOSReport.size() > 0) {
                            %>
                            <a href="TOS.jsp?tid=<%=tid%>">View TOS/POS</a>
                            <%}%>
                        </td>
                    </tr>
                    <%if(!tcsStatus.equalsIgnoreCase("Error")){%>
                                <tr>
                                    <td class="ff t-align-left" valign="top">
                                        Aa Approval Status
                                    </td>
                                    <td>
                                        <%=tcsStatus%>
                                    </td>
                                </tr>
                                <%}%>--%>
                    <%--<tr>
                        <td width="25%" class="t-align-left ff" valign="top">TSC Report :</td>
                        <td width="75%" class="t-align-left">
                            <% if(evalTsc.equalsIgnoreCase("Approved")){ %>
                            <a href="ClarificationRefReprot.jsp?tenderId=<%=request.getParameter("tenderid")%>">Create</a><br/><br/>
                            <% }else{  %>
                            <label>TSC yet not Published</label>
                            <%  } %>
                            <%
                                            List<SPTenderCommonData> checkEval0 = tenderCommonService1.returndata("SubmitReportList", request.getParameter("tenderid"), null);
                                            if (checkEval0.size() > 0) {                            
%>
                            <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1">
                                <tr>
                                    <th width="60%" class="t-align-left ff">Report Name</th>
                                    <th width="40%" class="t-align-left ff">Action</th>
                                </tr>
                                <%
                                                for (SPTenderCommonData formname : checkEval0) {
                                %>
                                <tr>
                                    <td><%=formname.getFieldName1()%></td>
                                    <td>
                                        <a href="ViewEvalRpt.jsp?tenderId=<%=request.getParameter("tenderid")%>&evalRptId=<%=formname.getFieldName2()%>">View</a>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="ClarificationRefDoc.jsp?tenderId=<%=request.getParameter("tenderid")%>&evalRptId=<%=formname.getFieldName2()%>">Upload</a>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="TSCSeekClarificaiton.jsp?tenderid=<%=request.getParameter("tenderid")%>&req=<%=formname.getFieldName2()%>&rrid=<%=formname.getFieldName3()%>&Uid=<%=formname.getFieldName4()%>">Seek Clarification</a>
                                        <%}%>
                                                                               </td>
                                </tr>
                                
                            </table>
                                <% }%>
                        </td>
                    </tr>--%>
                </table>
                <%}%>
<!--            </div>-->
<div>&nbsp;</div>
<%
              List<SPTenderCommonData> checkEval2 = tenderCommonService1.returndata("GetClarification", request.getParameter("tenderid"), null);
              if (checkEval2.size() > 0) {
%>
            <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
                <tr>
                    <td colspan="5">Clarification</td>
                </tr>
                <tr>
                    <th width="4%" class="ff">Sl. No.</th>
                    <th width="26%" class="ff">Date of clarification </th>
                    <th width="50%">clarification Details </th>
                    <th width="10%">Action </th>
                    <th width="10%">Status </th>
                </tr>
                <%--START: Evaluation MEMBERS TABLE--%>
                <%
                                int Counter = 1;
                                for (SPTenderCommonData formname : checkEval2) {
                                    String YesNo = formname.getFieldName3();
                %>
                <tr>
                    <td class="ff"><%=Counter%></td
                    <td>
                        <%=formname.getFieldName2()%>
                    </td>
                    <td>
                        <a href="TECViewClarification.jsp?tenderid=<%=request.getParameter("tenderid")%>&rcid=<%=formname.getFieldName1()%>">View</a>
                    </td>
                    <td>
                        <%if (YesNo.equals("no")) {%>
                        <a href="TECViewClarification.jsp?tenderid=<%=request.getParameter("tenderid")%>&rcid=<%=formname.getFieldName1()%>">View Reponse</a>
                        <%}%>
                    </td>
                    <td>
                        <%if (YesNo.equals("no")) {%>
                        Resolved
                        <%}%>
                    </td>
                </tr>
                <%
                    Counter++;
                }
            %>
                <%--END Evaluation MEMBERS TABLE--%>

            </table>
<%}%>

<!--        </div>-->
<!--        <br/>-->
<!--        </div>-->
<!--        <div align="center"><%//@include file="../resources/common/Bottom.jsp" %></div>-->
    </body>
     <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>

