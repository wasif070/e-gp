<%-- 
    Document   : T1L1Rpt
    Created on : Aug 1, 2011, 11:10:55 AM
    Author     : nishit
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.databean.T1L1DtBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.T1L1SrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation  T1L1 Report</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });
            });
            function printElem(options){
                $('#print').hide();
                $('#saveASPDF').hide();
                $('#goBack').hide();
                $('#repSaveBtn').hide();
                $('#print_area').printElement(options);
                $('#print').show();
                $('#saveASPDF').show();
                $('#goBack').show();
                $('#repSaveBtn').show();                
            }
        </script>
    </head>
    <body>
        <div class="mainDiv">
        <div class="fixDiv">
        <%
            boolean isNotPDF = true;
            if (request.getParameter("isPDF") != null && "true".equalsIgnoreCase(request.getParameter("isPDF"))) {
                isNotPDF = false;
            }
            if(isNotPDF){
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%}%>
        <%
                    String s_tenderId = request.getParameter("tenderid");
                    String s_reportId = request.getParameter("reportId");
                    String s_roundId = "0";
                    String s_lotId = request.getParameter("lotId");
                    String s_rId = request.getParameter("roundId");
                    if (request.getParameter("rId") != null && !"".equalsIgnoreCase(request.getParameter("rId"))) {
                        s_roundId = request.getParameter("rId");
                    }                    
                    pageContext.setAttribute("tenderId", s_tenderId);
                    pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                    pageContext.setAttribute("userId", 0);
        %>

        <div class="contentArea_1">
            <div id="print_area">                
                    <div class="pageHead_1">
                        Evaluation T1L1 Report
                        <%if(isNotPDF){%>
                        <span style="float: right;">
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                            <span id="svpdft1l1">
                                <a class="action-button-savepdf" id="saveASPDF" href="<%=request.getContextPath()%>/TorRptServlet?action=pdfT1L1&tenderId=<%=s_tenderId%>&repId=<%=s_reportId%>">Save As PDF</a>
                            </span>
                            <a id="goBack" class="action-button-goback" href="<%=request.getHeader("referer")%>">Go Back to Tender Dashboard</a>
                        </span>
                        <%}%>
                    </div>
                
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <form id="t1L1Form" name ="t1L1Form" action ="<%=request.getContextPath()%>/T1L1RptServlet" method="post">
                        <input type="hidden" name="rptId" value="<%=s_reportId%>" />
                        <input type="hidden" name="tenderId" value="<%=s_tenderId%>" />
                        <input type="hidden" name="roundId" value="<%=s_roundId%>" />
                        <input type="hidden" name="lotId" value="<%=s_lotId%>" />
                        <input type="hidden" name="action" value="save" />

                        <jsp:include page="T1L1Include.jsp"/>

                        <%
                                    boolean isLottery = false;
                                    if (request.getAttribute("isLottery") != null) {
                                        isLottery = Boolean.parseBoolean(request.getAttribute("isLottery").toString());
                                    }
                                    List<T1L1DtBean> list = new ArrayList<T1L1DtBean>();
                                    if (request.getAttribute("T1L1DtBean") != null) {
                                        list = (List<T1L1DtBean>) request.getAttribute("T1L1DtBean");
                                    }
                        %>
                        <%if (!list.isEmpty() && "0".equalsIgnoreCase(s_rId) && isNotPDF) {%>
                        <div align="center" id="repSaveBtn">
                            <br/>
                            <label class="formBtn_1">
                                <input type="submit" name="submit" value="<%if (isLottery) {
                                        out.print("Lottery");
                                    } else {
                                out.print("Save");
                            }%>"/>

                            </label>
                        </div>
                        <%}%>
                    </form>


           
            </div>
            </div>
            </div>
        </div>
        <%if(isNotPDF){%>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%}%>
    </body>
</html>
