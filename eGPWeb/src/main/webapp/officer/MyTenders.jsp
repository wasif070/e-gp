<%-- 
    Document   : MyTenders
    Created on : Nov 22, 2010, 3:56:07 PM
    Author     : Malhar,rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>My Tenders</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
    </head>
    <body onload="hide();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">My Tenders
                    <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('6');">Save as PDF</a></span>
                </div>
                <div>&nbsp;</div>
                <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                <div class="formBg_1">
                    <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
                    <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">

                        <tr>
                            <td width="17%" class="ff">Procurement Category :</td>
                            <td width="33%">
                                <select name="procNature" id="procNature" class="formTxtBox_1" id="select2" style="width:208px;">
                                    <option value="" selected="selected">-- Select Category --</option>
                                    <option value="1">Goods</option>
                                    <option value="2">Works</option>
                                    <option value="3">Service</option>
                                </select>
                            </td>
                            <td width="17%" class="ff"><input type="hidden" id="status" value="Pending"/>
                                <input type="hidden" id="statusTab" value="Under Preparation"/><!--bug id :: 1397 Live -->
                            </td>
                            <td width="33%"></td>
                        </tr>
                        <tr>
                            <td class="ff">Procurement Type : </td>
                            <td>
                                <select name="procType" class="formTxtBox_1" id="cmbType" style="width:208px;">
                                    <option value="">-- Select Type --</option>
                                    <option value="NCT">NCB</option>
                                    <option value="ICT">ICB</option>
                                </select>
                            </td>
                            <td class="ff"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="ff">Procurement Method :</td>
                            <td>
                                <select name="procMethod" class="formTxtBox_1" id="cmbProcMethod" style="width:208px;">
                                    <option value="0" selected="selected">- Select Procurement Method -</option>
                                    <c:forEach var="procMethod" items="${advAppSearchSrBean.procMethodList}">
                                        <c:choose>
                                            <c:when test = "${procMethod.objectValue=='RFQ'}">
                                                <option value="${procMethod.objectId}">LEM</option>
                                            </c:when>
                                            <c:when test = "${procMethod.objectValue=='DPM'}">
                                                <option value="${procMethod.objectId}">DCM</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${procMethod.objectId}">${procMethod.objectValue}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                            <td class="ff"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="ff">Tender ID : </td>
                            <td><input type="text" class="formTxtBox_1" id="tenderId" style="width:194px;" /></td>
                            <td class="ff">Reference No :</td>
                            <td><input type="text" class="formTxtBox_1" id="refNo" style="width:194px;" /></td>
                        </tr>
                        <tr>
                            <td class="ff">Publishing Date From :</td>
                            <td><input name="pubDtFrm" id="pubDtFrm" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('pubDtFrm','pubDtFrmImg');"/>&nbsp;
                                <a  href="javascript:void(0);" title="Calender"><img id="pubDtFrmImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('pubDtFrm','pubDtFrmImg');"/></a></td>
                            <td class="ff">Publishing Date To :</td>
                            <td><input name="pubDtTo" id="pubDtTo" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" />&nbsp;
                                <a  href="javascript:void(0);" title="Calender"><img id="pubDtToImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('pubDtTo','pubDtToImg');"/></a></td>
                        </tr>
                        <tr>
                            <td width="16%" class="ff">Status :</td>
                            <td width="32%">
                                <select name="select2" class="formTxtBox_1" id="cmbStatus" style="width:208px;" >
                                    <option value="">--Select--</option>
                                    <option value="Live">Live</option>
                                    <option value="Pending">Under Preparation</option>
                                    <option value="Archive">Archived</option>
                                    <option value="Cancelled">Canceled</option>
                                    <option value="Processing">Processing</option>
                                </select>
                            </td>
                            <td width="15%" class="ff"></td>
                            <td width="38%"></td>
                        </tr>
                        <tr>
                            <td class="ff">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="ff">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center"><label class="formBtn_1">
                                    <input type="submit" name="button" id="btnSearch" value="Search" />
                                </label>
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="reset" name="Reset" id="btnReset" value="Reset" />
                                </label></td>
                        </tr>
                        <tr>
                            <td class="t-align-center" colspan="4">
                                <div id="valAll" class="reqF_1"></div>
                            </td>
                        </tr>
                    </table>

                </div>
                <div class="tableHead_1 t_space">Tender Search Result</div>
                <!--                <ul class="tabPanel_1" style="display: none;">
                                    <li><a href="javascript:void(0);" id="pendingTab" onclick="chngTab(1)" class="sMenu">Pending</a></li>
                                    <li><a href="javascript:void(0);" id="approvedTab" onclick="chngTab(2)">Approved</a></li>
                                </ul>-->
                <ul class="tabPanel_1 t_space" id="tabForApproved">
                    <li><a href="javascript:void(0);" id="pendingTab" onclick="changeTab(1);" class="sMenu">Under Preparation</a></li>
                    <li><a href="javascript:void(0);" id="liveTab" onclick="changeTab(2);">Live</a></li>
                    <li><a href="javascript:void(0);" id="processingTab" onclick="changeTab(5);">Processing</a></li>
                    <li><a href="javascript:void(0);" id="archivedTab" onclick="changeTab(3);">Archived</a></li>
                    <li><a href="javascript:void(0);" id="cancelledTab" onclick="changeTab(4);">Canceled</a></li>
                </ul>
                <div class="tabPanelArea_1">
                    <table width="100%" cellspacing="0" class="tableList_1" id="resultTable" cols="@0,6">
                        <tr>
                            <th width="4%" class="t-align-center">Sl. <br/>
                                No.</th>
                            <th width="10%" class="t-align-center">Tender ID, <br />
                                Reference No., <br />
                                Tender Status
                            </th>
                            <th width="35%" class="t-align-center">Procurement Category, <br />
                                Title</th>
                            <th width="20%" class="t-align-center">Hierarchy Node</th>
                            <th width="10%" class="t-align-center"> Type, <br />
                                Method</th>
                            <th width="12%" class="t-align-center">Publishing Date | <br />
                                Closing Date</th>
                            <th width="8%" class="t-align-center">Dashboard</th>
                        </tr>
                        <%--<tr>
                            <td class="t-align-center"><div align="left">1</div></td>
                            <td class="t-align-center"><p align="left">1,</p>
                                <p align="left">LGED/XEN/TKG</p>
                                <p align="left">/2010/2504</p></td>
                            <td class="t-align-center"><p align="left">Goods,</p>
                                <p align="left">&nbsp;</p>
                                <p align="left"><a href=''>Improvement of Lahiri GC -Kalomegh GC via Dogachi hat road., Improvement   of Lahiri GC -Paria UP office (Paria hat) via Mesni RNGPS road.,   Improvement of Dharmogarh UP-Laxmir hat road., Improvement of Senua   hat-Katihar hat FRB(Poyenda) road. </a></p></td>
                            <td class="t-align-center"> <p align="left">Local Government Division ,</p>
                                <p align="left"> Local Government Engineering Department ,</p>
                                <p align="left"> Executive Engineer </p></td>
                            <td class="t-align-center"><p align="left">NCT,</p>
                                <p align="left">OTM</p></td>
                            <td class="t-align-center"><p align="left">10/11/2010,</p>
                                <p align="left">20/11/10</p></td>
                            <td class="t-align-center"><a href="#" title="Dashboard"><img src="../resources/images/Dashboard/dashBoardIcn.png" alt="Dashboard" /></a></td>
                        </tr>--%>
                    </table>
                    <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                        <tr>
                            <td align="left">Page <span id="pageNoTot">1</span> - <span id="pageTot">10</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="size" id="size" value="10" style="width: 100px;" class="formTxtBox_1"/></td>
                            <td align="center"><center><input name="textfield3" onkeypress="checkKeyGoTo(event);" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                    </label></center></td>
                            <td class="prevNext-container">
                                <ul>
                                    <li>&laquo; <a href="javascript:void(0)" id="btnFirst">First</a></li>
                                    <li>&#8249; <a href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                    <div align="center">
                        <input type="hidden" id="pageNo" value="1"/>                        
                    </div>
                </div>
                    <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                            String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="TenderListing_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="mytender" />
            </form>
                <div>&nbsp;</div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->

        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
        <script type="text/javascript">
            function chkdisble(pageNo){
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }
                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", true)
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            function changeTab(tabNo){
                if(tabNo == 1){
                    $("#liveTab").removeClass("sMenu");
                    $("#pendingTab").addClass("sMenu");
                    $("#archivedTab").removeClass("sMenu");
                    $("#cancelledTab").removeClass("sMenu");
                    $("#processingTab").removeClass("sMenu");
                    $("#statusTab").val("Under Preparation");
                    $("#status").val("Pending");
                    $('#pageNo').val('1');
                }
                else if(tabNo == 2){
                    $("#pendingTab").removeClass("sMenu");
                    $("#liveTab").addClass("sMenu");
                    $("#archivedTab").removeClass("sMenu");
                    $("#cancelledTab").removeClass("sMenu");
                    $("#processingTab").removeClass("sMenu");
                    $("#statusTab").val("Live");
                    $("#status").val("Approved");
                    $('#pageNo').val('1');
                }
                else if(tabNo == 3){
                    $("#liveTab").removeClass("sMenu");
                    $("#pendingTab").removeClass("sMenu");
                    $("#archivedTab").addClass("sMenu");
                    $("#cancelledTab").removeClass("sMenu");
                    $("#processingTab").removeClass("sMenu");
                    $("#statusTab").val("Archive");
                    $("#status").val("Approved");
                    $('#pageNo').val('1');
                }
                else if(tabNo ==4){
                    $("#liveTab").removeClass("sMenu");
                    $("#pendingTab").removeClass("sMenu");
                    $("#archivedTab").removeClass("sMenu");
                    $("#cancelledTab").addClass("sMenu");
                    $("#processingTab").removeClass("sMenu");
                    $("#statusTab").val("Cancelled");
                    $("#status").val("Cancelled");
                    $('#pageNo').val('1');
                }
                else if(tabNo ==5){
                    $("#liveTab").removeClass("sMenu");
                    $("#pendingTab").removeClass("sMenu");
                    $("#archivedTab").removeClass("sMenu");
                    $("#cancelledTab").removeClass("sMenu");
                    $("#processingTab").addClass("sMenu");
                    $("#statusTab").val("Processing");
                    $("#status").val("Approved");
                    $('#pageNo').val('1');
                }

                loadTenderTableone();
            }
            function chngTab(tabNo)
            {
                if(tabNo == 1)
                {
                    $("#pendingTab").addClass("sMenu");
                    $("#approvedTab").removeClass("sMenu");
                    $("#status").val("Pending");
                }
                else if(tabNo == 2)
                {
                    //$("#pendingTab").removeClass("sMenu");
                    $("#approvedTab").addClass("sMenu");
                    $("#status").val("Approved");
                }
                $('#pageNo').val("1");
                $('#dispPage').val("1");
                loadTenderTable();
            }

            //            $(function() {
            //                $('#cmbStatus').change(function() {
            //
            //                    if($('#cmbStatus').val() == "Pending")
            //                    {
            //                        chngTab(1);
            //                        $('#tabForApproved').hide();
            //                    }
            //                    else if($('#cmbStatus').val() == "Approved")
            //                    {
            //                        chngTab(2);
            //                        $('#tabForApproved').show();
            //                    }
            //                });
            //            });
            $(function() {
                $('#cmbStatus').change(function() {
                    if($('#cmbStatus').val() == "Live")
                    {
                        $("#statusTab").val("Live");
                        $("#status").val("Approved");
                    }
                    else if($('#cmbStatus').val() == "Pending")
                    {
                        //                        $("#statusTab").val("Live");
                        //                        $("#status").val("Approved");
                        $("#statusTab").val("Under Preparation");
                        $("#status").val("Pending");
                    }
                    else if($('#cmbStatus').val() == "Archive")
                    {
                        $("#statusTab").val("Archived");
                        $("#status").val("Approved");
                    }
                    else if($('#cmbStatus').val() == "Cancelled")
                    {
                        $("#statusTab").val("Cancelled");
                        $("#status").val("Cancelled");
                    }
                    else if($('#cmbStatus').val() == "Processing")
                    {
                        $("#statusTab").val("Processing");
                        $("#status").val("Approved");
                    }
                    //$("#status").val("Cancelled");
                });
            });
        </script>

        <!-- AJAX Grid Functions Start -->
        <script type="text/javascript">
            $(function() {
                var count;
                $('#btnSearch').click(function() {
                    count=0;
                    if(document.getElementById('procNature') !=null && document.getElementById('procNature').value !=''){
                        count = 1;
                    }else if(document.getElementById('cmbType') !=null && document.getElementById('cmbType').value !=''){
                        count = 1;
                    }else if(document.getElementById('cmbProcMethod') !=null && document.getElementById('cmbProcMethod').value !='0'){
                        count = 1;
                    }else if(document.getElementById('tenderId') !=null && document.getElementById('tenderId').value !=''){
                        count = 1;
                    }else if(document.getElementById('pubDtFrm') !=null && document.getElementById('pubDtFrm').value !=''){
                        count = 1;
                    }else if(document.getElementById('cmbStatus') !=null && document.getElementById('cmbStatus').value !=''){
                        count = 1;
                    }else if(document.getElementById('refNo') !=null && document.getElementById('refNo').value !=''){
                        count = 1;
                    }else if(document.getElementById('pubDtTo') !=null && document.getElementById('pubDtTo').value !=''){
                        count = 1;
                    }
                    if(count != '0'){
                        if($("#statusTab").val()=='Cancelled'){
                            changeTab(4);
                        }else if($("#statusTab").val()=='Live'){
                            changeTab(2);
                        }else if($("#statusTab").val()=='Archived'){
                            changeTab(3);
                        }else if($("#statusTab").val()=='Processing'){
                            changeTab(5);
                        }else{
                            changeTab(1);
                        }
                        $("#valAll").html('');
                    $("#pageNo").val("1");
                    $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "MyTenders",statusTab:$("#statusTab").val(),action: "get mytenders",status: $("#status").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),procNature: $("#procNature").val(),procType:$("#cmbType").val(),procMethod: $("#cmbProcMethod").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),pubDtFrm: $("#pubDtFrm").val(),pubDtTo: $("#pubDtTo").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);
                        sortTable();
                            if($('#noRecordFound').attr('value') == "noRecordFound"){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }
                        chkdisble($("#pageNo").val());
                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();

                        var counter = $('#cntTenBrief').val();
                        for(var i=0;i<counter;i++)
                        {
                            try
                            {
                                if($('#tenderBrief_'+i).html() != null){
                                //alert($('#tenderBrief_'+i).html());
                                //alert($('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, ''));
                                    var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                                    var temp1 = $('#tenderBrief_'+i).html();
                                    if(temp.length > 250){
                                        temp = temp1.substr(0, 250);
                                        $('#tenderBrief_'+i).html(temp+'...');
                                    }
                                }
                            }
                            catch(e){}
                        }
                    });

                    }else{
                        $("#valAll").html('Please enter at least One search criteria');
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnReset').click(function() {
                    $("#pageNo").val("1");
                    $("#refNo").val('');
                    $("#tenderId").val('');
                    $("#procNature").val('');
                    $("#cmbType").val('');
                    $("select#cmbProcMethod option[selected]").removeAttr("selected");
                    $("select#cmbProcMethod option[value='0']").attr("selected", "selected");
                    //$("#cmbProcMethod").val('');
                    $("#pubDtFrm").val('');
                    $("#pubDtTo").val('');
                    $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "MyTenders",action: "get mytenders",status:'Pending',tenderId: '0',refNo: '',procNature:'',procType:'',procMethod: '0',pubDtFrm: $("#pubDtFrm").val(),pubDtTo: $("#pubDtTo").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);
                        sortTable();
                        if($('#noRecordFound').attr('value') == "noRecordFound"){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }
                        chkdisble($("#pageNo").val());
                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();

                        var counter = $('#cntTenBrief').val();
                        for(var i=0;i<counter;i++){
                            try
                            {
                                if($('#tenderBrief_'+i).html() != null){
                                //alert($('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, ''));
                                    var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                                    var temp1 = $('#tenderBrief_'+i).html();
                                    if(temp.length > 250){
                                        temp = temp1.substr(0, 250);
                                        $('#tenderBrief_'+i).html(temp+'...');
                                    }
                                }
                            }
                            catch(e){}
                        }

                    });
                });
            });
        </script>

        <script type="text/javascript">
            function loadTenderTableone()
            {
                $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {
                    funName: "MyTenders",
                    action: "get mytenders",
                    statusTab:$("#statusTab").val(),
                    status: $("#status").val(),
                    tenderId: $("#tenderId").val(),
                    refNo: $("#refNo").val(),procNature: $("#procNature").val(),
                    procType:$("#cmbType").val(),
                    procMethod: $("#cmbProcMethod").val(),
                    tenderId: $("#tenderId").val(),
                    refNo: $("#refNo").val(),
                    pubDtFrm: $("#pubDtFrm").val(),
                    pubDtTo: $("#pubDtTo").val(),
                    pageNo: $("#pageNo").val(),
                    size: $("#size").val()
                },
                function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();

                    var counter = $('#cntTenBrief').val();
                    for(var i=0;i<counter;i++){
                        try
                        {
                            if($('#tenderBrief_'+i).html() != null){
                            //alert($('#tenderBrief_'+i).html());
                            //alert($('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, ''));
                                var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                                var temp1 = $('#tenderBrief_'+i).html();
                                if(temp.length > 250){
                                    temp = temp1.substr(0, 250);
                                    $('#tenderBrief_'+i).html(temp+'...');
                                }
                            }
                        }
                        catch(e){}
                    }
                });
            }
            function loadTenderTable()
            {
                $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {
                    funName: "MyTenders",
                    action: "get mytenders",
                    statusTab:$("#statusTab").val(),
                    status: $("#status").val(),
                    tenderId: $("#tenderId").val(),
                    refNo: $("#refNo").val(),procNature: $("#procNature").val(),
                    procType:$("#cmbType").val(),
                    procMethod: $("#cmbProcMethod").val(),
                    tenderId: $("#tenderId").val(),
                    refNo: $("#refNo").val(),
                    pubDtFrm: $("#pubDtFrm").val(),
                    pubDtTo: $("#pubDtTo").val(),
                    pageNo: $("#pageNo").val(),
                    size: $("#size").val()
                },
                function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();

                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();

                    var counter = $('#cntTenBrief').val();
                    for(var i=0;i<counter;i++){
                        try
                        {
                            var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                            var temp1 = $('#tenderBrief_'+i).html();
                            if(temp.length > 250){
                                temp = temp1.substr(0, 250);
                                $('#tenderBrief_'+i).html(temp+'...');
                            }
                        }
                        catch(e){}
                        //$('#tenderBrief_'+i).attr('style', 'width: 20px;');
                    }
                });
            }
        </script>
        <%--<script type="text/javascript">
    function loadListingTable()
    {
        $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "MyTenders",action: "defualt",pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
            $('#resultTable').find("tr:gt(0)").remove();
            $('#resultTable tr:last').after(j);
        });
    }

        </script>--%>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(totalPages>0 && $('#pageNo').val()!="1")
                    {
                        $('#pageNo').val("1");
                        loadTenderTable();
                        $('#dispPage').val("1");
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTenderTable();
                        $('#dispPage').val(totalPages);
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo != totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTenderTable();
                        $('#dispPage').val(Number(pageNo)+1);
                        $('#btnPrevious').removeAttr("disabled");
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();

                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTenderTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTenderTable();
                            $('#dispPage').val(Number(pageNo));
                            if(parseInt($('#pageNo').val(), 10) == 1)
                                $('#btnPrevious').attr("disabled", "true")
                            if(parseInt($('#pageNo').val(), 10) > 1)
                                $('#btnPrevious').removeAttr("disabled");
                        }
                    }
                });
            });
        </script>
        <!-- AJAX Grid Finish-->
        <script type="text/javascript">
            loadTenderTable();
        </script>
        <script type="text/javascript">
            //function showHide()
            //{
              //  if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='- Collapse'){
                //    document.getElementById('tblSearchBox').style.display = 'none';
                  //  document.getElementById('collExp').innerHTML = '+ Expand';
                //}else{
                  //  document.getElementById('tblSearchBox').style.display = 'table';
                   // document.getElementById('collExp').innerHTML = '- Collapse';
                //}
            //}
            
        function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Advanced Search';
            }else{
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }
        }
        function hide()
        {
            document.getElementById('tblSearchBox').style.display = 'none';
            document.getElementById('collExp').innerHTML = '+ Advanced Search';
        }

            function checkKeyGoTo(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                $(function() {
                //$('#btnGoto').click(function() {
                        var pageNo=parseInt($('#dispPage').val(),10);
                        var totalPages=parseInt($('#totalPages').val(),10);
                        if(pageNo > 0)
                        {
                            if(pageNo <= totalPages) {
                                $('#pageNo').val(Number(pageNo));
                                //loadTable();
                                $('#btnGoto').click();
                                $('#dispPage').val(Number(pageNo));
                            }
                        }
                    });
                //});
            }
        }
        
        var Interval;
        clearInterval(Interval);
        Interval = setInterval(function(){
            var PageNo = document.getElementById('pageNo').value;
            var Size = document.getElementById('size').value;
            CorrectSerialNumber(PageNo-1,Size); 
        }, 100);
        
        </script>
</html>
