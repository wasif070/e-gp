<%-- 
    Document   : ReportTableCreation
    Created on : Dec 22, 2010, 11:14:01 AM
    Author     : TaherT
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Officer - Evaluation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <title>Committe Evalution</title>
        <!--jalert -->

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">

        </script>
    </head>
    <body>

        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <%
                    String tenderid = request.getParameter("tenderid");
                    pageContext.setAttribute("tenderId", tenderid);
        %>
        <div class="pageHead_1">Search & Use Existing Committee<span style="float: right;"><a class="action-button-goback" href="">Go back to Dashboard</a></span></div>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <form action="ReportCreation.jsp?tenderid=<%=tenderid%>" method="post" id="search">
            <div class="tabPanelArea_1">
                <div class="tableList_1 t-align-right">

                    Field Marked (<span class="mandatory">*</span>) is Mandatory
                </div>


            </div>
            <div>&nbsp;</div>
        </form>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
