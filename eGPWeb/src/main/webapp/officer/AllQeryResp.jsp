<%-- 
    Document   : AllQeryResp
    Created on : Jun 24, 2011, 11:40:53 AM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.web.utility.GeneratePdfConstant"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluation - View All Queries and Responses</title>
    </head>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
    %>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    <%
                boolean isPDF = true;
                if (request.getParameter("isPDF") != null && request.getParameter("isPDF").equalsIgnoreCase("true")) {
                    isPDF = false;
                }

    %>
    <script type="text/javascript">
        function printPage()
        {
            $("#frmAllQeryResp").submit();
        }

    </script>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <% if (isPDF) {%>
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            </div>
            <% }%>
            <div id="print_area">
                <div class="contentArea_1">
                    <!--Dashboard Header End-->
                    <div class="pageHead_1">Evaluation – View All Queries and Responses
                        <%
                                    String referer = "";
                                    if (request.getHeader("referer") != null) {
                                        referer = request.getHeader("referer");
                                    }
                                    String tenderId = "";
                                    if (request.getParameter("tenderid") != null && !"".equals(request.getParameter("tenderid"))) {
                                        tenderId = request.getParameter("tenderid");
                                    }
                                     int evalCount = 0;
                                     if(request.getParameter("evalCount")!=null){
                                        evalCount = Integer.parseInt(request.getParameter("evalCount"));
                                     }

                                    if (isPDF) {
                                        String reqURL = request.getRequestURL().toString();
                                        String reqQuery = request.getQueryString();
                                        String folderName = pdfConstant.EvalPdf;
                        %>
                        <span class="c-alignment-right" id="printEle">
                            <a name="print" class="action-button-view" id="print">Print</a>
                            <a class="action-button-savepdf" href="<%=request.getContextPath()%>/AllQueryRespServlet?reqURL=<%=reqURL%>&reqQuery=<%=reqQuery%>&folderName=<%=folderName%>&id=<%=tenderId%>&isPDF=true" >Save As PDF</a>
                            <% }%>
                            <% if (isPDF) {%>
                        <%if(!"y".equals(request.getParameter("ter"))){%>
                        <a href="<%=referer%>" class="action-button-goback">Go Back To Dashboard</a>
                        <%}%>
                        </span>
                        <% }%>
                        </div>

                    <%

                                String userId = "";
                                HttpSession hs = request.getSession();
                                if (hs.getAttribute("userId") != null) {
                                    userId = hs.getAttribute("userId").toString();
                                }
                                pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                                pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                                pageContext.setAttribute("userId", userId);
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>

                    <form method="POST" id="frmAllQeryResp" name="frmAllQeryResp" action="AllQeryResp.jsp?tenderid=<%=tenderId%>&comType=TEC">
                        <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1">
                            <%
                                        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                        List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("gettenderlotbytenderid", tenderId, null);
                                        if (!tenderLotList.isEmpty()) {
                                            // IF PACKAGE FOUND THEN DISPLAY PACKAGE INFORMATION
                                            if (tenderLotList.get(0).getFieldName1().equalsIgnoreCase("Package")) {

                                                // FETCH PACKAGE INFORMATION
                                                List<SPTenderCommonData> packageList = tenderCommonService.returndata("getlotorpackagebytenderid",
                                                        tenderId,
                                                        "Package,1");
                                                if (!packageList.isEmpty()) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td width="15%" class="t-align-left ff">Package No :</td>
                                    <td width="85%" class="t-align-left"><%=packageList.get(0).getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Package Description :</td>
                                    <td class="t-align-left"><%=packageList.get(0).getFieldName2()%></td>
                                </tr>
                                <% List<SPCommonSearchDataMore> sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("getAllBidderForClariWithCP", tenderId, "0", String.valueOf(evalCount), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                                                    for (SPCommonSearchDataMore tendererlist : sPCommonSearchDataMores) {%>
                                <tr>
                                    <td class="t-align-left ff">Bidder/Consultant :</td>
                                    <td class="t-align-left"><%=tendererlist.getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%" cellspacing="0" class="tableList_1">
                                            <tr>
                                                <th>Sl. No.</th>
                                                <th>Form Name</th>
                                                <th>Query</th>
                                                <th>Response</th>
                                            </tr>
                                            <% int srno = 1;
                                            sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("getAllBidderClariWithCP", tenderId, "0", tendererlist.getFieldName1(), String.valueOf(evalCount), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                            System.out.print(sPCommonSearchDataMores.size());
                                            for (SPCommonSearchDataMore commonSearchDataMore : sPCommonSearchDataMores) {%>
                                            <tr>
                                                <td width="10%" class="t-align-center"><%=srno%></td>
                                                <td width="50%" ><%=commonSearchDataMore.getFieldName2()%></td>
                                                <td width="20%"><%=URLDecoder.decode(commonSearchDataMore.getFieldName3(),"UTF-8")%></td>
                                                <td width="20%"><% if (!commonSearchDataMore.getFieldName4().equals("")) {%><%=commonSearchDataMore.getFieldName4()%><% } else {%>-<%}%></td>
                                            </tr>
                                            <% srno++;
                                                                                                                                    }%>
                                        </table>
                                    </td>
                                </tr>
                                <% }
                                                                                    if (sPCommonSearchDataMores.isEmpty()) {%>
                                <td colspan="2" class="t-align-center" style="color: red;font-weight: bold">No records found</td>
                                <% }%>
                            </table>
                            <% }%>

                            <%
                                                                        } else {
                                                                            for (SPTenderCommonData tenderLot : tenderLotList) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td width="13%" class="t-align-left ff">Lot No:</td>
                                    <td width="87%" class="t-align-left"><%=tenderLot.getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Lot Description:</td>
                                    <td class="t-align-left"><%=tenderLot.getFieldName2()%></td>
                                </tr>
                                <%
                                                                                                                List<SPCommonSearchDataMore> sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("getAllBidderForClariWithCP", tenderId, tenderLot.getFieldName3(),  String.valueOf(evalCount), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                                                                                for (SPCommonSearchDataMore tendererlist : sPCommonSearchDataMores) {%>
                                <tr>
                                    <td class="t-align-left ff">Bidder/Consultant :</td>
                                    <td class="t-align-left"><%=tendererlist.getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%" cellspacing="0" class="tableList_1">
                                            <tr>
                                                <th>Sl. No.</th>
                                                <th>Form Name</th>
                                                <th>Query</th>
                                                <th>Response</th>
                                            </tr>
                                            <% int srno = 1;
                                             sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("getAllBidderClariWithCP", tenderId, tenderLot.getFieldName3(), tendererlist.getFieldName1(), String.valueOf(evalCount), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                             for (SPCommonSearchDataMore commonSearchDataMore : sPCommonSearchDataMores) {%>
                                            <tr>
                                                <td width="10%" class="t-align-center"><%=srno%></td>
                                                <td width="50%" ><%=commonSearchDataMore.getFieldName2()%></td>
                                                <td width="20%"><%=URLDecoder.decode(commonSearchDataMore.getFieldName3(),"UTF-8")%></td>
                                                <td width="20%"><% if (!commonSearchDataMore.getFieldName4().equals("")) {%><%=commonSearchDataMore.getFieldName4()%><% } else {%>-<%}%></td>
                                            </tr>
                                            <% srno++;
                                                                                                                                                                }%>
                                        </table>
                                    </td>
                                </tr>
                                <% }
                                                                                                                if (sPCommonSearchDataMores.isEmpty()) {%>
                                <tr>
                                    <td colspan="2" class="t-align-center" style="color: red;font-weight: bold">No records found</td>
                                </tr>
                                <%}%>
                            </table>

                            <%
                                                } // END LOT FOR LOOP
                                            } // END IF
                                        }
                            %>

                        </table>
                    </form>
                </div>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <% if (isPDF) {%>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <% }%>
            <!--Dashboard Footer End-->
        </div>
        <script type="text/javascript">
            $(document).ready(function() {

                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                //$('#printGoBack').hide();
                $('#printEle').hide();
                $('#print_area').printElement(options);
                //$('#printGoBack').show();
                $('#printEle').show();
            }

        </script>
    </body>
</html>
<%
            pdfConstant = null;
%>