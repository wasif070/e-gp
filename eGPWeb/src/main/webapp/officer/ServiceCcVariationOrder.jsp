<%-- 
    Document   : ServiceCcVariationOrder
    Created on : Dec 22, 2011, 4:08:39 PM
    Author     : shreyansh.shah
--%>


<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCcvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvStaffSch"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <% ResourceBundle srbd = null;
                    srbd = ResourceBundle.getBundle("properties.cmsproperty");
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
        %>
        <%
                    String referpage = request.getHeader("referer");
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String type = "";
                    int tenderId = 0;
                    int lotId = 0;
                    int formMapId = 0;
                    int varOrdId = 0;
                    int srvBoqId = 0;
                    String styleClass = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                    }
                    if (request.getParameter("formMapId") != null) {
                        formMapId = Integer.parseInt(request.getParameter("formMapId"));
                    }
                    if (request.getParameter("varOrdId") != null) {
                        varOrdId = Integer.parseInt(request.getParameter("varOrdId"));
                    }
                    if (request.getParameter("srvBoqId") != null) {
                        srvBoqId = Integer.parseInt(request.getParameter("srvBoqId"));
                    }
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                    boolean flag = false;
                    CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                    String serviceType = commService.getServiceTypeForTender(tenderId);
                    if ("Time based".equalsIgnoreCase(serviceType.toString())) {
                        flag = true;
                    }
                    List<TblCmsSrvCnsltComp> list = cmss.getSrvConsltCompoData(formMapId);
                    List<TblCmsSrvCcvari> listVarOrder = cmss.getSrvCcVariationOrder(varOrdId);
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Work Schedule</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script language="javascript">
            var variationType = "";
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function getDate(date)
            {
                var splitedDate = date.split("-");
                return Date.parse(splitedDate[1]+" "+splitedDate[0]+", "+splitedDate[2]);
            }
            function checkDate(dateofdlvry,id){
                var group = document.getElementById("group_"+id).value;
                var firstDate = getDate(document.getElementById("firstdate").value);
                var secondDate = getDate(document.getElementById("seconddate").value);
                var dod = getDate(document.getElementById(dateofdlvry).value);
                if((new Date(firstDate).getTime() <= new Date(dod).getTime()) && (new Date(dod).getTime() <=  new Date(secondDate).getTime())){
                    checkDiff(dod,id);
                }else{
                    jAlert("<%=srbd.getString("CMS.works.edidatesByTendererandPe")%> ","CMS", function(RetVal) {
                    });
                    document.getElementById(dateofdlvry).value="";
                }

            }
            function checkDiff(values,id){
                var edate = document.getElementById("edate_"+id).value;
                var sdate = document.getElementById("sdate_"+id).value;
                if(edate!=""){

                    var endDate = getDate(edate);
                    var sDate=getDate(sdate);

                    if(new Date(sDate).getTime() > new Date(endDate).getTime()){
                        jAlert("<%=srbd.getString("CMS.var.sdateenddatecheck")%>","Consultant Composition", function(RetVal) {
                        });
                        document.getElementById("edate_"+id).value="";
                    }

                }
            }
            //            function checkqty(id){
            //                $('.err').remove();
            //                if(!regForNumber(document.getElementById('Qtyenter_'+id).value) || document.getElementById('Qtyenter_'+id).value.split(".")[1].length > 3 || document.getElementById('Qtyenter_'+id).value.split(".")[0].length > 12){
            //                    $("#Qtyenter_"+id).parent().append("<div class='err' style='color:red;'><%=srbd.getString("CMS.var.ratecheck")%></div>");
            //                    document.getElementById('Qtyenter_'+id).value="";
            //
            //                }
            //                calculateGrandTotal();
            //            }
            function positive(value)
            {
                return /^\d*$/.test(value);

            }
            function checkDays (id){
                $('.err').remove();
                var values = document.getElementById('noOfDays_'+id).value;
                if(!regForNumber(document.getElementById('noOfDays_'+id).value)){
                    $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Days must be numeric value </div>");
                    document.getElementById('noOfDays_'+id).value="";
                }else if(document.getElementById('noOfDays_'+id).value.indexOf("-")!=-1){
                    $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Days must be positive value </div>");
                    document.getElementById('noOfDays_'+id).value="";
                }else if(!positive(document.getElementById('noOfDays_'+id).value)){
                    $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Decimal values are not allowed</div>");
                    document.getElementById('noOfDays_'+id).value="";
                }else if(document.getElementById('noOfDays_'+id).value.length > 5){
                    $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Maximum 5 digits are allowed </div>");
                    document.getElementById('noOfDays_'+id).value="";
                }

            }

            function f_date(id){
                var firstDate = getDate(document.getElementById("startDt_"+id).value);
                var values = document.getElementById("noOfDays_"+id).value;
                var enddate = new Date(firstDate).getTime()+(parseInt(values)*24*60*60*1000);
                var finalenddate="";
                var monthname = new Array("Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec");
                var enddate = new Date(enddate);
                finalenddate = enddate.getDate()+"-"+monthname[enddate.getMonth()]+"-"+enddate.getFullYear();
                if(values != "")
                {
                    document.getElementById("endDt_"+id).value = finalenddate;
                    return finalenddate;
                }
            }

            function GetCal(txtname,controlname,count)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d-%b-%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        // f_date(count);
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
        <script type="text/javascript">
            /* check if pageNO is disable or not */
            function chkdisble(pageNo){
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>

        <script type="text/javascript">
            /*  Handle First click event */
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    $('#first').val(0);
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Last Button click event */
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var size = (parseInt($('#size').val())*totalPages)-(parseInt($('#size').val()));
                    $('#first').val(size);
                    if(totalPages>0){
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });



        </script>
        <script type="text/javascript">
            /*  Handle Next Button click event */
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val());
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first+max);

                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo <= totalPages) {

                        loadTable();
                        $('#pageNo').val(Number(pageNo)+1);
                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            /*  Handle Previous click event */
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first-max);
                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    $('#pageNo').val(Number(pageNo)-1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)-1);
                });
            });
        </script>
        <script type="text/javascript">
            var count=0;
            var addcount = 0;
            function Search(){
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            }
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var size = (parseInt($('#size').val())*pageNo)-$('#size').val();
                    $('#first').val(size);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }

                });
            });
            function addrow(formType)
            {
                addcount = document.getElementById("listcount").value;
                count = document.getElementById("listcount").value;
                var servicetxt = "";
                if(formType == "timebase"){
                    variationType = "timebase";
                    servicetxt = '<tr><td class="t-align-left">\
                    <input class="formTxtBox_1" type="checkbox" name="chk'+count+' " id="chk'+count+'" /></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="srNo_'+count+'" style=width:40px id="srNo_'+count+'" /></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="cnlcat_'+count+'" id="cnlcat_'+count+'" /></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="inNoOfKeyProfstafftxt_'+count+'" id="inNoOfKeyProfstafftxt_'+count+'" onblur="return validate()" /><span id="inNoOfKeyProfstaffspan'+count+'" class="reqF_1"></span></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="monthstxt_'+count+'" id="monthstxt_'+count+'" onblur="return validate()"/><span id="monthsspan'+count+'" class="reqF_1"></span></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="noOffStafftxt_'+count+'" id="noOffStafftxt_'+count+'" onblur="return validate()" /><span id="noOffStaffspan'+count+'" class="reqF_1"></span></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="noOffSupportStafftxt_'+count+'" id="noOffSupportStafftxt_'+count+'" onblur="return validate()" /><span id="noOffSupportStaffspan'+count+'" class="reqF_1"></span></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="Consltnametxt_'+count+'" id="Consltnametxt_'+count+'" onblur="return validate()" /><span id="Consltnamespan'+count+'" class="reqF_1"></span></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="NoOffKeyPrStafftxt_'+count+'" id="NoOffKeyPrStafftxt_'+count+'" onblur="return validate()" /><span id="NoOffKeyPrStaffspan'+count+'" class="reqF_1"></span></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="NoOffSupportPrStafftxt_'+count+'" id="NoOffSupportPrStafftxt_'+count+'" onblur="return validate()" /><span id="NoOffSupportPrStaffspan'+count+'" class="reqF_1"></span></td>'+
                        '<input type=hidden name="srvCcVarId_'+count+ '" id="srvCcVarId_'+count+'" value="" /></td>';
                }else{
                    variationType="lumpsum";
                    servicetxt = '<tr><td class="t-align-left">\
                    <input class="formTxtBox_1" type="checkbox" name="chk'+count+' " id="chk'+count+'" /></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="srNo_'+count+'" style=width:40px id="srNo_'+count+'" /></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="cnlcat_'+count+'" id="cnlcat_'+count+'" /></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="noOffStafftxt_'+count+'" id="noOffStafftxt_'+count+'" onblur="return validate()" /><span id="noOffStaffspan'+count+'" class="reqF_1"></span></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="noOffSupportStafftxt_'+count+'" id="noOffSupportStafftxt_'+count+'" onblur="return validate()" /><span id="noOffSupportStaffspan'+count+'" class="reqF_1"></span></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="Consltnametxt_'+count+'" id="Consltnametxt_'+count+'" onblur="return validate()" /><span id="Consltnamespan'+count+'" class="reqF_1"></span></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="NoOffKeyPrStafftxt_'+count+'" id="NoOffKeyPrStafftxt_'+count+'" onblur="return validate()" /><span id="NoOffKeyPrStaffspan'+count+'" class="reqF_1"></span></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="NoOffSupportPrStafftxt_'+count+'" id="NoOffSupportPrStafftxt_'+count+'" onblur="return validate()" /><span id="NoOffSupportPrStaffspan'+count+'" class="reqF_1"></span></td>'+
                        '<input type=hidden name="srvCcVarId_'+count+ '" id="srvCcVarId_'+count+'" value="" /></td>';
                }
                // onblur=checkqtyin('+count+')
                $("#resultTable tr:last").after(servicetxt);
                count++;
                document.getElementById("listcount").value = count;
            }

            function validate()
            {
                var vbool = true;
                var totcnt = document.getElementById("listcount").value;
                //  alert("count :"+count);
                for(var id = 0; id<totcnt; id++)
                {
            <%if (flag) {%>
                    if(document.getElementById('inNoOfKeyProfstafftxt_'+id).value!="")
                    {
                        if(!regForNumber(document.getElementById('inNoOfKeyProfstafftxt_'+id).value))
                        {
                            document.getElementById('inNoOfKeyProfstaffspan'+id).innerHTML="Please enter numeric value";
                            document.getElementById('inNoOfKeyProfstafftxt_'+id).value="";
                            vbool = false;
                        }else{
                            document.getElementById('inNoOfKeyProfstaffspan'+id).innerHTML="";
                        }
                        
                    }
                    if(document.getElementById('monthstxt_'+id).value!="")
                    {
                        if(!/^[-+]?\d*\.?\d*$/.test(document.getElementById('monthstxt_'+id).value))
                        {

                            document.getElementById('monthsspan'+id).innerHTML="Please enter numeric value";
                            document.getElementById('monthstxt_'+id).value="";
                            vbool = false;
                        }else{
                            if(!validateDecimal(document.getElementById("monthstxt_"+id).value))
                            {
                                document.getElementById('monthsspan'+id).innerHTML="only 2 digits after decimal are allowed";
                                document.getElementById('monthstxt_'+id).value="";
                                vbool = false;
                            }else{
                                document.getElementById('monthsspan'+id).innerHTML="";
                            }
                        }
                        
                    }
                    
            <%}%>
                    if(document.getElementById('noOffStafftxt_'+id).value!="")
                    {
                        if(!regForNumber(document.getElementById('noOffStafftxt_'+id).value))
                        {
                            document.getElementById('noOffStaffspan'+id).innerHTML="Please enter numeric value";
                            document.getElementById('noOffStafftxt_'+id).value="";
                            vbool = false;
                        }else{
                            document.getElementById('noOffStaffspan'+id).innerHTML="";
                        }
                        
                    }
                    if(document.getElementById('noOffSupportStafftxt_'+id).value!="")
                    {
                        if(!regForNumber(document.getElementById('noOffSupportStafftxt_'+id).value))
                        {
                            document.getElementById('noOffSupportStaffspan'+id).innerHTML="Please enter numeric value";
                            document.getElementById('noOffSupportStafftxt_'+id).value="";
                            vbool = false;
                        }else{
                            document.getElementById('noOffSupportStaffspan'+id).innerHTML="";
                        }
                    }
                    if(document.getElementById('Consltnametxt_'+id).value!="")
                    {
                        document.getElementById('Consltnamespan'+id).innerHTML="";
                    }
                    if(document.getElementById('NoOffKeyPrStafftxt_'+id).value!="")
                    {
                        if(!regForNumber(document.getElementById('NoOffKeyPrStafftxt_'+id).value))
                        {
                            document.getElementById('NoOffKeyPrStaffspan'+id).innerHTML="Please enter numeric value";
                            document.getElementById('NoOffKeyPrStafftxt_'+id).value="";
                            vbool = false;
                        }else{
                            document.getElementById('NoOffKeyPrStaffspan'+id).innerHTML="";
                        }
                    }
                    
                    if(document.getElementById('NoOffSupportPrStafftxt_'+id).value!="")
                    {
                        if(!regForNumber(document.getElementById('NoOffSupportPrStafftxt_'+id).value))
                        {
                            document.getElementById('NoOffSupportPrStaffspan'+id).innerHTML="Please enter numeric value";
                            document.getElementById('NoOffSupportPrStafftxt_'+id).value="";
                            vbool = false;
                        }else{
                            document.getElementById('NoOffSupportPrStaffspan'+id).innerHTML="";
                        }
                    }
                    
                }
                return vbool;
            }

            function delRow(){
                var counter = 0;
                var listcount = document.getElementById("listcount").value;
                var trackid="";
                for(var i=0;i<listcount;i++){
                    if(document.getElementById("chk"+i)!=null){
                        if(document.getElementById("chk"+i).checked){
                            if(document.getElementById("srvCcVarId_"+i).value != "")
                            {
                                trackid=trackid+document.getElementById("srvCcVarId_"+i).value+",";
                            }
                            // document.getElementById("listcount").value = listcount-1;

                        }
                    }
                }
                $(":checkbox[checked='true']").each(function(){
                    if(document.getElementById("listcount")!= null){
                        var curRow = $(this).parent('td').parent('tr');
                        curRow.remove();
                        count--;
                        counter++;
                    }
                });

                if(counter==0){
                    jAlert("Please select at least one item to remove","Consultant Composition", function(RetVal) {
                    });
                }
                else
                {
                    $.post("<%=request.getContextPath()%>/CMSSerCaseServlet", {action:'deletesrvccvariation', val: trackid,tenderId:<%=request.getParameter("tenderId")%> },  function(j){
                        jAlert("Selected item(s) deleted successfully","Variation Order", function(RetVal) {
                        });

                    });
                }
            }
            function validateDecimal(value)
            {
                var flag =false;
                var a = value;
                var b = a.indexOf('.');
                if(b>0)
                {
                    a = a.substring(b+1, a.length);
                    if(a.length<=2)
                    {
                        flag=true;
                    }
                }
                else
                {
                    flag =true;
                }
                return flag;
            }
            function changetable(){
                var pageNo=parseInt($('#pageNo').val(),10);
                var first = parseInt($('#first').val());
                var max = parseInt($('#size').val());
                var totalPages=parseInt($('#totalPages').val(),10);
                loadTable();

            }
            function onFire(){
                var flag = true;
                var allow = false;
                var countt = document.getElementById("listcount").value;
                for(var i=0;i<countt;i++){
                    allow = true;
                    if(document.getElementById("srNo_"+i) == null)
                    {
                        continue;
                    }
                    if(variationType == 'timebase')
                    {
                        if(document.getElementById("srNo_"+i).value==""
                            || document.getElementById("cnlcat_"+i).value==""
                            || document.getElementById("inNoOfKeyProfstafftxt_"+i).value==""
                            || document.getElementById("monthstxt_"+i).value==""
                            || document.getElementById("noOffStafftxt_"+i).value==""
                            || document.getElementById("noOffSupportStafftxt_"+i).value==""
                            || document.getElementById("Consltnametxt_"+i).value==""
                            || document.getElementById("NoOffKeyPrStafftxt_"+i).value==""
                            || document.getElementById("NoOffSupportPrStafftxt_"+i).value==""){
                            jAlert("It is mandatory to specify all Groups","Consultant Composition Variation Order", function(RetVal) {
                            });
                            flag=false;
                        }
                    } else {
                    if(document.getElementById("srNo_"+i).value==""
                            || document.getElementById("cnlcat_"+i).value==""
                            || document.getElementById("noOffStafftxt_"+i).value==""
                            || document.getElementById("noOffSupportStafftxt_"+i).value==""
                            || document.getElementById("Consltnametxt_"+i).value==""
                            || document.getElementById("NoOffKeyPrStafftxt_"+i).value==""
                            || document.getElementById("NoOffSupportPrStafftxt_"+i).value==""){
                            jAlert("It is mandatory to specify all Groups","Consultant Composition Variation Order", function(RetVal) {
                            });
                            flag=false;
                        }

                    }
                
                }
                if(flag){
                    if(!allow){
                        jAlert("It is necessary to enter at least one item","Variation Order", function(RetVal) {
                        });
                        return false;
                    }else{
                        document.getElementById("addcount").value=document.getElementById("listcount").value;
                        document.getElementById("action").value="addsrvccvari";
                        document.forms["frm"].submit();
                    }
                    
                }else{
                    return false;
                }

            }

        </script>

    </head>
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">
                        <%       out.print("Consultant Composition Variation Order");
                        %>
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" title="Go Back">Go Back</a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%if (request.getParameter("lotId") != null) {%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <%}%>
                    <form name="frm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post" >
                        <input type="hidden" name="addcount" id="addcount" value="" />
                        <input type="hidden" name="action" id="action" value="" />
                        <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>" />
                        <input type="hidden" name="SrvFormMapId" id="SrvFormMapId" value="<%=request.getParameter("formMapId")%>" />
                        <input type="hidden" name="lotId" id="lotId" value="<%=lotId%>" />
                        <input type="hidden" name="varOrdId" id="varOrdId" value="<%=varOrdId%>" />
                        <input type="hidden" name="srvBoqId" id="srvBoqId" value="<%=srvBoqId%>" />
                        <%
                            if(request.getParameter("isEdit")!=null){
                        %>
                            <input type="hidden" name="isEdit" id="isEdit" value="true" />
                        <%
                            }
                        %>
                        <div id="resultDiv" style="display: block;">
                            <div  id="print_area">

                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                    <tr><td colspan="10" style="text-align: right;">
                                            <%if (flag) {%>
                                            <a class="action-button-add" id="addRow" onclick="addrow('timebase')">Add Item</a>
                                            <a class="action-button-delete" id="delRow" onclick="delRow()">Remove Item</a>
                                            <%} else {%>
                                            <a class="action-button-add" id="addRow" onclick="addrow('lumpsum')">Add Item</a>
                                            <a class="action-button-delete" id="delRow" onclick="delRow()">Remove Item</a>
                                            <%}%>
                                        </td></tr>
                                    <tr>
                                        <th width="5%">Check</th>
                                        <th>Sl. No.</th>
                                        <th>Consultant Category</th>
                                        <%if (flag) {%>
                                        <th>Indicated No. of Key Professional Staff</th>
                                        <th>Input Staff Months</th>
                                        <%}%>
                                        <th>Indicated No. of Professional Staff</th>
                                        <th>Indicated No. of Support Staff</th>
                                        <th>Consultant Name</th>
                                        <th>No. of Key Professional Staff</th>
                                        <th>No. of Support Professional Staff</th>
                                    </tr>
                                    <%

                                                if (!list.isEmpty()) {
                                                    for (int i = 0; i < list.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                    %>
                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"></td>
                                        <td class="t-align-left"><%=list.get(i).getSrNo()%></td>
                                        <td class="t-align-left"><%=list.get(i).getConsultantCtg()%></td>
                                        <%if (flag) {%>
                                        <td style="text-align :right;"><%=list.get(i).getTotalNosPe()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getMonths()%></td>
                                        <% }%>
                                        <td style="text-align :right;"><%=list.get(i).getKeyNosPe()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getSupportNosPe()%></td>
                                        <td class="t-align-left"><%=list.get(i).getConsultantName()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getKeyNos()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getSupportNos()%>
                                            <input type="hidden" name="srvCCId" value="<%=list.get(i).getSrvCcid()%>">
                                        </td>
                                    </tr>
                                    <%}
                                                }%>


                                    <%
                                                if (listVarOrder != null && !listVarOrder.isEmpty()) {
                                                    int i = 0;
                                                    for (i = 0; i < listVarOrder.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                    %>
                                    <tr>
                                        <td> <input class="formTxtBox_1" type="checkbox" name="chk<%=i%>" id="chk<%=i%>" /></td>
                                        <td class="t-align-left" style="text-align: left"><input type="text" name="srNo_<%=i%>"  id="srNo_<%=i%>" value="<%=listVarOrder.get(i).getSrNo()%>" /></td>
                                        <td class="t-align-left"><input type="text" name="cnlcat_<%=i%>" id="cnlcat_<%=i%>" value="<%=listVarOrder.get(i).getConsultantCtg()%>" /></td>
                                            <%if (flag) {%>
                                        <td class="t-align-center"><input type="text" name="inNoOfKeyProfstafftxt_<%=i%>"  id="inNoOfKeyProfstafftxt_<%=i%>" value="<%=listVarOrder.get(i).getTotalNosPe()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="inNoOfKeyProfstaffspan<%=i%>" class="reqF_1"></span></td>
                                        <td class="t-align-center"><input type="text" name="monthstxt_<%=i%>"  id="monthstxt_<%=i%>" value="<%=listVarOrder.get(i).getMonths().setScale(2)%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="monthsspan<%=i%>" class="reqF_1"></span></td>
                                            <%}%>
                                        <td class="t-align-center"><input type="text" name="noOffStafftxt_<%=i%>"  id="noOffStafftxt_<%=i%>" value="<%=listVarOrder.get(i).getKeyNosPe()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="noOffStaffspan<%=i%>" class="reqF_1"></span></td>
                                        <td class="t-align-center"><input type="text" name="noOffSupportStafftxt_<%=i%>"  id="noOffSupportStafftxt_<%=i%>" value="<%=listVarOrder.get(i).getSupportNosPe()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="noOffSupportStaffspan<%=i%>" class="reqF_1"></span></td>
                                        <td class="t-align-center"><input type="text" name="Consltnametxt_<%=i%>"  id="Consltnametxt_<%=i%>" value="<%=listVarOrder.get(i).getConsultantName()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="Consltnamespan<%=i%>" class="reqF_1"></span></td>
                                        <td class="t-align-center"><input type="text" name="NoOffKeyPrStafftxt_<%=i%>"  id="NoOffKeyPrStafftxt_<%=i%>" value="<%=listVarOrder.get(i).getKeyNos()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="NoOffKeyPrStaffspan<%=i%>" class="reqF_1"></span></td>
                                        <td class="t-align-center"><input type="text" name="NoOffSupportPrStafftxt_<%=i%>"  id="NoOffSupportPrStafftxt_<%=i%>" value="<%=listVarOrder.get(i).getSupportNos()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="NoOffSupportPrStaffspan<%=i%>" class="reqF_1"></span>
                                        </td>

                                    <input type="hidden" value="<%=listVarOrder.get(i).getSrvCcvariId()%>" name="srvCcVarId_<%=i%>" id="srvCcVarId_<%=i%>" />

                                    </tr>
                                    <%}
                                                    out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                                                } else {
                                                    out.print("<input type=hidden name=listcount id=listcount value=0 />");
                                                }
                                    %>
                                    <input type="hidden" name="forms" <%if (flag) {%>value="timebase"<%} else {%>value="lumpsum"<%}%> />
                                </table>
                                <table width="100%" cellspacing="0" class="t_space">
                                    <tr><td colspan="10" style="text-align: right;">
                                                  <%if (flag) {%>
                                            <a class="action-button-add" id="addRow" onclick="addrow('timebase')">Add Item</a>
                                            <a class="action-button-delete" id="delRow" onclick="delRow()">Remove Item</a>
                                            <%} else {%>
                                            <a class="action-button-add" id="addRow" onclick="addrow('lumpsum')">Add Item</a>
                                            <a class="action-button-delete" id="delRow" onclick="delRow()">Remove Item</a>
                                            <%}%>
                                        </td></tr>
                                </table>
                            </div>
                        </div>


                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" id="first" value="0"/>
                        <br />
                        <center>
                            <label class="formBtn_1">
                                <input type="button" name="swpbuttonadd" id="swpbuttonadd" value="Submit" onclick="onFire();" />
                            </label>
                        </center>
                    </form>
                </div>

                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>




