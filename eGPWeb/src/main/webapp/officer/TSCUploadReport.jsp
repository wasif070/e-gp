<%--
    Document   : TSCUploadReport
    Created on : Jun 25, 2011, 1:52:35 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.model.table.TblEvalTscnotification"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvalTSCNotificationService"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalTscDocs"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            <%if("on".equalsIgnoreCase(request.getParameter("download"))){%>
                Download Recommendation Report
                <%}else{%>
                Upload Report
                <%}%>
        </title>
    </head>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

    <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
    <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            $("#frmUploadDoc").validate({
                rules: {
                    uploadDocFile: {required: true},
                    documentBrief: {required: true,maxlength:100}
                },
                messages: {
                    uploadDocFile: { required: "<div class='reqF_1'>Please Select Document.</div>"},
                    documentBrief: { required: "<div class='reqF_1'>Please Enter Description.</div>",
                        maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                }
            });
        });
    </script>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
                String tenderid = "";
                if (request.getParameter("tenderid") != null && !"".equals(request.getParameter("tenderid"))) {
                    tenderid = request.getParameter("tenderid");
                }
                String st = "";
                if (request.getParameter("st") != null && !"".equals(request.getParameter("st"))) {
                    st = request.getParameter("st");
                }
    %>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            </div>
            <div class="contentArea_1">
                <!--Dashboard Header End-->
                <div class="pageHead_1">
                <%if("on".equalsIgnoreCase(request.getParameter("download"))){%>
                Download Recommendation Report
                <%}else{%>
                Upload Report
                <%}%>
                <span style="float:right;"><a href="Evalclarify.jsp?tenderId=<%=tenderid%>&download=on&st=<%=st%>&comType=TEC" class="action-button-goback">Go Back To Dashboard</a></span>
                </div>

                <% pageContext.setAttribute("tenderId", request.getParameter("tenderid"));%>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div><% if (request.getParameter("msg") != null && "success".equalsIgnoreCase(request.getParameter("msg"))) {%>
                    <div class="responseMsg successMsg" style="margin-top: 10px;">Document Deleted Successfully</div>
                    <% }%></div>
                <form method="POST" id="frmUploadDoc" enctype="multipart/form-data" name="frmUploadDoc" action="">
                    <%List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("gettenderlotbytenderid", tenderid, null);
                                EvaluationService e = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                                EvalTSCNotificationService etscns = (EvalTSCNotificationService) AppContext.getSpringBean("EvalTSCNotificationService");
                                List<TblEvalTscnotification> tets = etscns.findEvalTscNtfctn(tenderid);
                                List<TblEvalTscDocs> docss = e.getDocDetails(request.getParameter("tenderid"));
                                if (!tenderLotList.isEmpty()) {
                                    // IF PACKAGE FOUND THEN DISPLAY PACKAGE INFORMATION
                                    if (tenderLotList.get(0).getFieldName1().equalsIgnoreCase("Package")) {

                                        // FETCH PACKAGE INFORMATION
                                        List<SPTenderCommonData> packageList = tenderCommonService.returndata("getlotorpackagebytenderid",
                                                tenderid,
                                                "Package,1");


                                        if (!packageList.isEmpty()) {
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td width="15%" class="t-align-left ff">Package No :</td>
                            <td width="85%" class="t-align-left"><%=packageList.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.get(0).getFieldName2()%></td>
                        </tr>
                        <%if (docss.isEmpty() && tets.isEmpty()) {%>
                        <tr>
                            <td class="t-align-left ff">Action :</td>
                            <td class="t-align-left"><a href="UploadReport.jsp?tenderid=<%=tenderid%>&st=<%=st%>&pckLotId=0">Upload Report</a></td>
                        </tr>
                        <%}%>
                        <tr>
                            <td colspan="2">
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th class="t-align-center" width="10%">S.No</th>
                                            <th class="t-align-center" width="25%">File Name</th>
                                            <th class="t-align-center" width="45%">File Description</th>
                                            <th class="t-align-center" width="10%">File Size <br />
                                                (in Kb)</th>
                                            <th class="t-align-center" width="15%">Action</th>
                                        </tr>
                                        <%

                                                                                        if (!docss.isEmpty() || docss != null) {
                                                                                            int size = 0;
                                                                                            int i = 1;
                                                                                            for (TblEvalTscDocs ttsd : docss) {

                                                                                                size = Integer.parseInt(ttsd.getDocSize()) / 1024;
                                        %>
                                        <tr>
                                            <td class="t-align-center" style="text-align:center;"><%= i%></td>
                                            <td class="t-align-center" style="text-align:left;"><%= ttsd.getDocumentName()%></td>
                                            <td class="t-align-center" style="text-align:left;"><%= ttsd.getDocumentDesc()%></td>
                                            <td class="t-align-center" style="text-align:left;"><%= size%></td>
                                            <td class="t-align-center" style="text-align:center;">
                                                <a href="<%= request.getContextPath() %>/DocUploadForLotAndPkg?docName=<%= ttsd.getDocumentName()%>&tenderId=<%=tenderid%>&docSize=<%=size%>&action=downloadDoc&pkgId=0" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                                <%if (tets.isEmpty()) {%>|
                                                <a href="<%= request.getContextPath() %>/DocUploadForLotAndPkg?Id=<%= ttsd.getTscDocId()%>&docName=<%= ttsd.getDocumentName()%>&action=removeDoc&doc=rem&tenderId=<%=tenderid%>"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                                    <%}%>
                                            </td>


                                            <%
                                                                        i++;
                                                                    }

                                                                } else {%>
                                            <td colspan="5" style="text-align:center;">No records found</td>
                                            <%}%>
                                        </tr>
                                    </table>
                        </tr>
                    </table>
                    </td>
                    </tr>
                    </table>
                    <% }%>

                    <%
                                                        } else {
                                                            for (SPTenderCommonData tenderLot : tenderLotList) {
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td width="13%" class="t-align-left ff">Lot No:</td>
                            <td width="87%" class="t-align-left"><%=tenderLot.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Lot Description:</td>
                            <td class="t-align-left"><%=tenderLot.getFieldName2()%></td>
                        </tr>
                        <%
                                                                                List<TblEvalTscDocs> docs = e.getDocDetails(request.getParameter("tenderid"), tenderLot.getFieldName3());
                                                                                if (request.getParameter("download") == null) {
                                                                                    if (docs.isEmpty() && tets.isEmpty()) {
                        %>
                        <tr>
                            <td class="t-align-left ff">Action</td>
                            <td class="t-align-left"><a href="UploadReport.jsp?tenderid=<%=tenderid%>&st=<%=st%>&pckLotId=<%=tenderLot.getFieldName3()%>">Upload Report</a></td>
                        </tr>
                        <%}
                                                                                }%>
                        <tr>

                            <td colspan="2">
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <th class="t-align-center" width="10%">S.No</th>
                                        <th class="t-align-center" width="25%">File Name</th>
                                        <th class="t-align-center" width="45%">File Description</th>
                                        <th class="t-align-center" width="10%">File Size <br />
                                            (in Kb)</th>
                                        <th class="t-align-center" width="15%">Action</th>
                                    </tr>
                                    <%

                                                                                            if (!docs.isEmpty() && docs != null) {
                                                                                                int size = 0;
                                                                                                int i = 1;
                                                                                                for (TblEvalTscDocs ttsd : docs) {

                                                                                                    size = Integer.parseInt(ttsd.getDocSize()) / 1024;
                                    %>
                                    <tr>
                                        <td class="t-align-center" style="text-align:center;"><%= i%></td>
                                        <td class="t-align-center" style="text-align:left;"><%= ttsd.getDocumentName()%></td>
                                        <td class="t-align-center" style="text-align:left;"><%= ttsd.getDocumentDesc()%></td>
                                        <td class="t-align-center" style="text-align:left;"><%= size%></td>
                                        <td class="t-align-center" style="text-align:center;">
                                            <a href="../DocUploadForLotAndPkg?docName=<%= ttsd.getDocumentName()%>&tenderId=<%=tenderid%>&docSize=<%=size%>&action=downloadDoc&pkgId=<%=tenderLot.getFieldName3()%>" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                            <%if (tets.isEmpty()) {%>|
                                            <a href="../DocUploadForLotAndPkg?Id=<%= ttsd.getTscDocId()%>&docName=<%= ttsd.getDocumentName()%>&action=removeDoc&doc=rem&tenderId=<%=tenderid%>&pkgId=<%=tenderLot.getFieldName3()%>"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                                <%}%>
                                        </td>
                                        <%
                                                                                                                                                    i++;
                                                                                                                                                }

                                                                                                                                            } else {%>
                                        <td colspan="5" style="text-align:center;">no records found</td>
                                        <%}%>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                    </table>

                    <%
                                        } // END LOT FOR LOOP
                                    } // END IF
                                }
                    %>
                </form>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
<%
            if (checkExtension != null) {
                checkExtension = null;
            }
%>