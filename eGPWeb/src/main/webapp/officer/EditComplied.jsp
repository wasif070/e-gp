<%-- 
    Document   : EditComplied
    Created on : Jan 10, 2011, 4:44:05 PM
    Author     : Administrator
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Question and Answer</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
    </head>
    <body>
        <div class="dashboard_div">

            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <%  String tenderId;
                        tenderId = request.getParameter("tenderId");

                        String userId = "", uId = "";
                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                            userId = session.getAttribute("userId").toString();
                        }
                        if (request.getParameter("uId") != null && !"".equalsIgnoreCase(request.getParameter("uId"))) {
                            uId = request.getParameter("uId");
                        }
            %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">View Question and Answer</div>
                <div class="t_space">
                    <%
                                if (request.getParameter("err") != null) {
                                    String errorMsg = request.getParameter("err");
                                    if (errorMsg.equals("4")) {
                    %>
                    <div class="responseMsg errorMsg">Please Select Evaluation Status.</div>
                    <%}
                                                        if (errorMsg.equals("5")) {%>
                    <div class="responseMsg errorMsg">Please Enter Reason for Non Complied.</div>

                    <%                            }
                                                        if (errorMsg.equals("0")) {%>
                    <div class="responseMsg errorMsg">Your Answer is Not Updated..</div>
                    <% }
                                }
                    %>
                    <%
                                pageContext.setAttribute("tenderId", tenderId);
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <%
                                boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                    %>
                </div>
                <div class="t_space">
                    <%if (!"TEC".equalsIgnoreCase(request.getParameter("comType"))) {%>
                    <jsp:include page="officerTabPanel.jsp" >
                        <jsp:param name="tab" value="7" />
                    </jsp:include>
                    <% }%>
                </div>
                <div class="tabPanelArea_1">
                    <%
                                pageContext.setAttribute("TSCtab", "3");
                                int cnt = 0;
                    %>
                    <%@include  file="../resources/common/AfterLoginTSC.jsp"%>
                    <div class="tabPanelArea_1" style="border: none; padding: 0px;">
                        <form id="frmViewQueAns" action="<%=request.getContextPath()%>/ViewQueAnsServlet" name="frmEvalPost" method="POST" onsubmit="return validate();">
                            <input type="hidden" name="tenderId" value="<%=tenderId%>"/>
                            <input type="hidden" name="userId" value="<%=uId%>"/>
                            <%
                                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                        if (isTenPackageWis) {
                                            for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th colspan="2" class="t_align_left ff">Tender Details</th>
                                </tr>

                                <tr>
                                    <td width="22%" class="t-align-left ff">Package No. :</td>
                                    <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Package Description :</td>
                                    <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                                </tr>
                            </table>

                            <%
                                                                        }  // END packageList FOR LOOP

                                                                        // START FOR LOOP OF FORM NAME
                                                                        int formCount = 0;
                                                                        for (SPCommonSearchData formNames : commonSearchService.searchData("GetTenderFormsByLotId", tenderId, "0",
                                                                                "0", uId, null, null, null, null, null)) {
                                                                            formCount++;
                                                                            cnt++;
                            %>
                            <div class="table-section-header_title t_space b_space" style="margin-top: 25px;">
                                FormName : <%=formNames.getFieldName2()%>&nbsp;
                                <input type="hidden" name="formId_<%=cnt%>" value="<%=formNames.getFieldName1()%>"/>
                            </div>
                            <%
                                                                                                        TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                                                                        int cntQuest = 0;
                                                                                                        // START FOR LOOP OF TEC / TSC MEMBER NAME BY FORM-ID AND TENDER-ID.
                                                                                                        for (SPCommonSearchData sptcd : commonSearchService.searchData("GetEvalComMem", tenderId, formNames.getFieldName1(), null, null, null, null, null, null, null)) {

                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th colspan="3" class="t-align-left">TEC / TSC Member Name : <%=sptcd.getFieldName3()%>
                                    </th>
                                </tr>
                                <tr>
                                    <th width="4%" class="t-align-left">Sl. No.</th>
                                    <th width="48%">Questions</th>
                                    <th width="48%">Answer</th>
                                </tr>
                                <%
                                                                                                                                            // START FOR LOOP OF QUESTION POSTED BY TEC / TSC MEMBERS.
                                                                                                                                            for (SPCommonSearchData question : commonSearchService.searchData("GetEvalComMemQuestion", tenderId,
                                                                                                                                                    formNames.getFieldName1(),
                                                                                                                                                    sptcd.getFieldName2(), null, null, null, null, null, null)) {
                                                                                                                                                cntQuest++;
                                %>
                                <tr>
                                    <td class="t-align-left"><%=cntQuest%></td>
                                    <td  class="t-align-left"><%=question.getFieldName3()%></td>
                                    <%if (!question.getFieldName4().equals("")) {%>
                                    <td  class="t-align-left"><%=question.getFieldName4()%></td>
                                    <%} else {%>
                                    <td  class="t-align-left"><%=question.getFieldName4()%></td>
                                    <%}%>
                                </tr>
                                <%

                                                                                                                                            }//Member Loop Question Ends Here
%>
                                <%      }//Member Name Loop Ends Here..
                                                                                                            if (cntQuest == 0) {%>
                                <tr>
                                    <td width="85%" class="t-align-left">No Records Found</td>
                                </tr>
                            </table>
                            <%} else {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td width="10%" valign="top" class="t-align-left ff">Eval Status :</td>
                                    <%
                                                                                                                                                boolean flag = false;
                                                                                                                                                for (SPCommonSearchData question : commonSearchService.searchData("getEvalMemberStatus", tenderId, uId, formNames.getFieldName1(), userId, null, null, null, null, null)) {
                                    %>
                                    <td width="90%" class="t-align-left">
                                        <input type="hidden" name="evalStatus_<%=cnt%>" value="<%=question.getFieldName2()%>">
                                        <%if (question.getFieldName1().equalsIgnoreCase("Yes")) {%>
                                        <input type="radio" name="Complied_<%=cnt%>" id="CompliedY_<%=cnt%>" value="Yes" value="Complied" checked="cheked" onclick="HideandShow(this);"/>
                                        Complied
                                        &nbsp;&nbsp;
                                        <input type="radio" name="Complied_<%=cnt%>" id="CompliedN_<%=cnt%>" value="No" onclick="HideandShow(this);"/>
                                        Non Complied
                                        <%} else {
                                            flag = true;
                                        %>
                                        <input type="radio" name="Complied_<%=cnt%>" id="CompliedY_<%=cnt%>" value="Yes" value="Complied" onclick="HideandShow(this);"/>
                                        Complied
                                        &nbsp;&nbsp;
                                        <input type="radio" name="Complied_<%=cnt%>" id="CompliedN_<%=cnt%>" value="No" checked="cheked"  onclick="HideandShow(this);"/>
                                        Non Complied
                                        <%}%>
                                    </td>
                                </tr>
                                <%if (flag) {%>
                                <tr id="nonCompiled_<%=cnt%>" >

                                    <td valign="top" class="t-align-left ff">Reason for Non Complied :</td>
                                    <td class="t-align-left">
                                        <textarea rows="5" id="evalNonCompRemarks_<%=cnt%>" name="evalNonCompRemarks_<%=cnt%>" class="formTxtBox_1" style="width:99%"><%=question.getFieldName4()%></textarea>
                                        <span class="reqF_1" id="compRemark_<%=cnt%>"></span>
                                    </td>
                                </tr>
                                <%} else {%>
                                <tr id="nonCompiled_<%=cnt%>" style="display: none;">

                                    <td valign="top" class="t-align-left ff">Reason for Non Complied :</td>
                                    <td class="t-align-left">
                                        <textarea rows="5" id="evalNonCompRemarks_<%=cnt%>" name="evalNonCompRemarks_<%=cnt%>" class="formTxtBox_1" style="width:99%"></textarea>
                                        <span class="reqF_1" id="compRemark_<%=cnt%>"></span>
                                    </td>
                                </tr>
                                <%}%>

                            </table>
                            <% }//Loop ends finding Compled Status and remark
                                                                            }//If no record Found.Condition Ends here.
                                                                        }  // END formNames FOR LOOP
                                                                        if (formCount == 0) {
                            %>
                            <div class="tabPanelArea_1 t-align-center">
                                No Records Found
                            </div>
                            <%}//if formCounter = 0 then this condition execute.
                                                                    }//isTenPackageWis If conditin ends is package ten wise.
                                                                    else {
                                                                        for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <td colspan="2" class="t-align-center ff">Tender Details</td>
                                </tr>
                                <tr>
                                    <td width="22%" class="t-align-left ff">Package No. :</td>
                                    <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Package Description :</td>
                                    <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                                </tr>
                            </table>
                            <%  } // END PACKAGE FOR LOOP

                                                                        for (SPCommonSearchData lotList : commonSearchService.searchData("GetBidderLots", tenderId, "Lot", uId, null, null, null, null, null, null)) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">

                                <tr>
                                    <td width="22%" class="t-align-left ff">Lot No. :</td>
                                    <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Lot Description :</td>
                                    <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                                </tr>
                            </table>
                            <%
                                                                                                        // START FOR LOOP OF FORM NAME BY LotId
                                                                                                        int formcnt = 0;
                                                                                                        for (SPCommonSearchData formNames : commonSearchService.searchData("GetTenderFormsByLotId", tenderId, "1",
                                                                                                                lotList.getFieldName1(), uId, null, null, null, null, null)) {
                                                                                                            formcnt++;
                            %>
                            <div class="table-section-header_title t_space b_space">
                                FormName : <%=formNames.getFieldName2()%><%=cnt%>
                            </div>
                            <%
                                                                                                                                        TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                                                                                                        int cntQuest = 0;
                                                                                                                                        // START FOR LOOP OF TEC / TSC MEMBER NAME BY FORM-ID AND TENDER-ID.
                                                                                                                                        for (SPCommonSearchData sptcd : commonSearchService.searchData("GetEvalComMem", tenderId, formNames.getFieldName1(), null, null, null, null, null, null, null)) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th colspan="2" class="t-align-left">TEC / TSC Member Name : <%=sptcd.getFieldName3()%>
                                    </th>
                                </tr>
                                <tr>
                                    <th width="4%" class="t-align-left">Sl. No.</th>
                                    <th width="96%">Questions</th>
                                </tr>
                                <%


                                                                                                                                                                        // START FOR LOOP OF QUESTION POSTED BY TEC / TSC MEMBERS.
                                                                                                                                                                        for (SPCommonSearchData question : commonSearchService.searchData("GetEvalComMemQuestion",
                                                                                                                                                                                tenderId, formNames.getFieldName1(),
                                                                                                                                                                                sptcd.getFieldName2(), null, null, null, null, null, null)) {
                                                                                                                                                                            cntQuest++;
                                %>
                                <tr>
                                    <td class="t-align-left"><%=cntQuest%></td>
                                    <td width="85%" class="t-align-left"><%=question.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td width="10%" valign="top" class="t-align-left ff">Eval Status :</td>
                                    <%
                                                                                                                                                                                                            boolean flag = false;
                                                                                                                                                                                                            question:
                                                                                                                                                                                                            commonSearchService.searchData("getEvalMemberStatus", tenderId, uId, formNames.getFieldName1(), userId, null, null, null, null, null);
                                    %>
                                    <td width="90%" class="t-align-left">
                                        <input type="hidden" name="evalStatus_<%=cnt%>" value="<%=question.getFieldName2()%>">
                                        <%if (question.getFieldName1().equals("Yes")) {%>
                                        <input type="radio" name="Complied_<%=cnt%>" id="CompliedY_<%=cnt%>" value="Yes" value="Complied" checked="cheked" onclick="HideandShow(this);"/>
                                        Complied
                                        &nbsp;&nbsp;
                                        <input type="radio" name="Complied_<%=cnt%>" id="CompliedN_<%=cnt%>" value="No" onclick="HideandShow(this);"/>
                                        Non Complied
                                        <%} else {
                                            flag = true;
                                        %>
                                        <input type="radio" name="Complied_<%=cnt%>" id="CompliedY_<%=cnt%>" value="Yes" value="Complied" onclick="HideandShow(this);"/>
                                        Complied
                                        &nbsp;&nbsp;
                                        <input type="radio" name="Complied_<%=cnt%>" id="CompliedN_<%=cnt%>" value="No" checked="cheked"  onclick="HideandShow(this);"/>
                                        Non Complied
                                        <%}%>
                                    </td>
                                </tr>
                                <%if (flag) {%>
                                <tr id="nonCompiled_<%=cnt%>" >

                                    <td valign="top" class="t-align-left ff">Reason for Non Complied :</td>
                                    <td class="t-align-left">
                                        <textarea rows="5" id="evalNonCompRemarks_<%=cnt%>" name="evalNonCompRemarks_<%=cnt%>" class="formTxtBox_1" style="width:99%"><%=question.getFieldName4()%></textarea>
                                        <span class="reqF_1" id="compRemark_<%=cnt%>"></span>
                                    </td>
                                </tr>
                                <%} else {%>
                                <tr id="nonCompiled_<%=cnt%>" style="display: none;">

                                    <td valign="top" class="t-align-left ff">Reason for Non Complied :</td>
                                    <td class="t-align-left">
                                        <textarea rows="5" id="evalNonCompRemarks_<%=cnt%>" name="evalNonCompRemarks_<%=cnt%>" class="formTxtBox_1" style="width:99%"></textarea>
                                        <span class="reqF_1" id="compRemark_<%=cnt%>"></span>
                                    </td>
                                </tr>
                                <%}%>
                                <%

                                                                                                                                                                        }//end Loop for Question.
                                %>
                            </table>

                            <%
                                                                                                                                        }//End Loop Of MemberName
                            %>
                            <%if (cntQuest == 0) {%>
                            <tr>
                                <td width="85%" class="t-align-left">No Records Found</td>
                            </tr>
                            <%}
                                                                                                            //else Condition ends here
                                                                                                        }//End Loop Of FormName
                                                                                                        if (formcnt == 0) {%>
                            <div class="tabPanelArea_1 t-align-center">
                                No Records Found
                            </div>

                            <%}//If No Form Found.

                                                                        }//Lot Wise Loop End Here
                                }//isTenPackageWis else end here
                            %>
                            <div class="t-align-center t_space">
                                <%if(cnt != 0) {%>
                                <label class="formBtn_1">
                                    <input type="hidden" id="counter" name="counter" value="<%=cnt%>"/>
                                    <input type="hidden" id="type" name="type" value="update"/>
                                    <input name="btnPost" id="btnPost" type="submit" value="Submit" /></label>
                                    <%}//If No Recordds Found Sumbmit Button is Not Visible
                                    %>
                            </div>
                        </form>
                    </div>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        function HideandShow(idRadio){
            var temp = idRadio.id;
            var val = temp.substring(temp.indexOf("_", 0)+1, temp.length);
            if(temp==("CompliedN_"+val)){
                //document.getElementById("nonCompiled_"+val).style.display='block';
                $('#nonCompiled_'+val).show();
            }else{
                //document.getElementById("nonCompiled_"+val).style.display='none';
                $('#nonCompiled_'+val).hide();
            }

        }
        function validate(){
            var f =document.frmViewQueAns;
            var count = f.counter.value;
            var isError = false;
            var radioName ;
            for(i=1;i<=count;i++){
                radioName="CompliedN_"+i;
                var remarkName = "evalNonCompRemarks_"+i;
                var message = "compRemark_"+i;
                if(document.getElementById(radioName).checked){
                    if(document.getElementById(remarkName).value==""){
                        document.getElementById(message).innerHTML="<br/>Please Enter Reson For Non Complied";
                        return false;
                    }else{
                        if(document.getElementById(remarkName).value.length>1000){
                            document.getElementById(message).innerHTML="<br/>Please Enter Less Then 1000 Character";
                            return false;
                        }
                    }
                }else{
                    isError = true;
                }

            }
            if(isError){
                return true;
            }
        }
    </script>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
