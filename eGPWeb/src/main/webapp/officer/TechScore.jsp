<%-- 
    Document   : TechScore
    Created on : Apr 13, 2011, 3:47:57 PM
    Author     : TaherT
--%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Consultant's  Evaluation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%
                    String tendID = request.getParameter("tenderId");
                    EvalServiceCriteriaService criteriaService = (EvalServiceCriteriaService) AppContext.getSpringBean("EvalServiceCriteriaService");
                    List<Object[]> biddersName = criteriaService.getFinalSubBidders(tendID);
                    Object minScore = criteriaService.getPassingMarksofTender(tendID);
                    StringBuilder tfootData1 = new StringBuilder();
                    StringBuilder tfootData2 = new StringBuilder();
                    StringBuilder tfootData3 = new StringBuilder();
                    String cpuserId = null;
                    String cppuserId = null;
                    CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> memusersId = dataMoreService.geteGPDataMore("getMemUserIdMadeEval",tendID);
                    if(memusersId!=null && !memusersId.isEmpty()){
                        cpuserId = memusersId.get(0).getFieldName1();
                        cppuserId = memusersId.get(0).getFieldName2();
                    }else{
                        cpuserId = session.getAttribute("userId").toString();
                        cppuserId = session.getAttribute("userId").toString();
                    }
                     int evalCount=0;
                     if(request.getParameter("evalCount")!=null){
                        evalCount = Integer.parseInt(request.getParameter("evalCount"));
                    }
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    boolean isREOI = commonService.getEventType(tendID).toString().equalsIgnoreCase("REOI");
                    boolean isService = commonService.getProcNature(tendID).toString().equalsIgnoreCase("Services");
        %>
        <div class="tabPanelArea_1">
            <div class="pageHead_1">Consultant's  Evaluation<span style="float: right;"><a class="action-button-goback" href="Evalclarify.jsp?tenderId=<%=tendID%>&st=rp&comType=TEC">Go Back to Dashboard</a></span>
            </div>
            <%
                        pageContext.setAttribute("tenderId", tendID);
         %>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <table class="tableList_1 t_space" cellspacing="0" width="30%">
                <tr>
                    <td width="15%">Tender ID :</td>
                    <td width="15%"><%=tendID%></td>
                </tr>
                <tr>
                    <td>Name of PEC Member :</td>
                    <td><%=objUserName%></td>
                </tr>
            </table>
            <% if(isREOI){
                    Map<String,String> userList = new HashMap<String, String>();
            %>
                <div class="inner-tableHead t_space">Evaluation Criteria</div>
                <table class="tableList_1" cellspacing="0" width="100%">

                <tr>
                    <td class="t-align-center" colspan="3">&nbsp;</td>
                    <th class="t-align-center" colspan="<%=biddersName.size()%>">Rating</th>
                </tr>
                <tr>
                    <th class="t-align-center">Form Name</th>
                    <th class="t-align-center">Criteria</th>
                    <th class="t-align-center">Sub Criteria</th>
                    <%
                                for (Object[] bidder : biddersName) {
                                    String name = bidder[1].toString();
                                    if ("-".equals(name)) {
                                        name = bidder[2].toString() + " " + bidder[3].toString();
                                    }
                                    userList.put(bidder[0].toString(), name);
                                    out.print("<th class='t-align-center'>" + name + "</th>");
                                }
                    %>
                    <!--                    -->
                </tr>
                <%
                            //0 esd.maxMarks ,1 esd.actualMarks,2 esd.ratingWeightage,3 esd.ratedScore,
                            //4 tf.tenderFormId,5 tf.formName,6 esc.subCriteria,7 esd.bidId
                            for (Object formId : criteriaService.getAllFormId(tendID)) {
                                List<Object[]> list = criteriaService.evalServiceRepData(formId.toString(), cpuserId, tendID,evalCount); //change by dohatec for re-evaluation
                               // double secCount=0;
                                for (Object[] data : list) {
                %>
                <tr>
                    <td><%
                               if(true || !("Work plan".equals(data[2].toString())||
                                        "Organization and Staffing".equals(data[2].toString()))){
                                   out.print(data[1]);
                               } else {
                                   out.print("");
                               }
                        %></td>
                     <td><%
                               if(true || !("Work plan".equals(data[2].toString())||
                                    "Organization and Staffing".equals(data[2].toString()))){
                                   out.print(data[5]);
                               } else {
                                   out.print("");
                               }
                        %></td>
                    <td><%out.print(data[2]);%></td>
                    <%
                           for (Object[] bidder : biddersName) {
                               StringBuilder htmlString = new StringBuilder();
                               htmlString.append("<td>");
                               List<Object[]> eachData = criteriaService.evalServiceRepBidderData(tendID, formId.toString(), cpuserId, bidder[0].toString(), data[4].toString(),evalCount); //change by dohatec for re-evaluation
                               if (eachData.size() > 1) {
                                   if(isREOI){
                                       int bidCount = eachData.size();
                                       double rating = 0.0;
                                       for (Object[] thisData : eachData) {
                                           /*if (thisData[1].toString().equals("Very Poor")) {
                                           } else if (thisData[1].toString().equals("Poor")) {
                                               rating+=0.4;
                                           } else if (thisData[1].toString().equals("Good" )) {
                                               rating+=0.7;
                                           } else if (thisData[1].toString().equals("Very Good")){
                                               rating+=0.9;
                                           } else if (thisData[1].toString().equals("Excellent")){
                                               rating+=1;
                                           }*/

                                           if (thisData[1].toString().equals("Poor")) {
                                               rating+=0.4;
                                           }else if (thisData[1].toString().equals("Average")) {
                                               rating+=0.5;
                                           }else if (thisData[1].toString().equals("Satisfactory")) {
                                               rating+=0.7;
                                           }else if (thisData[1].toString().equals("Most Satisfactory")) {
                                               rating+=0.8;
                                           } else if (thisData[1].toString().equals("Good")) {
                                               rating+=0.9;
                                           } else if (thisData[1].toString().equals("Very Good")){
                                               rating+=0.95;
                                           } else if (thisData[1].toString().equals("Excellent")){
                                               rating+=1;
                                           }
                                       }
                                       //System.out.println("--> "+rating);
                                       //System.out.println("--| "+rating/bidCount);
                                       String rateValue = null;
                                       /*if (rating/bidCount == 0) {
                                           rateValue = "Very Poor";
                                       } else if (rating/bidCount <= 0.4) {
                                           rateValue = "Poor";
                                       } else if (rating/bidCount > 0.4 && rating/bidCount <= 0.7) {
                                           rateValue = "Good";
                                       } else if (rating/bidCount > 0.7 && rating/bidCount <= 0.9) {
                                           rateValue = "Very Good";
                                       } else if (rating/bidCount > 0.9 && rating/bidCount <= 1) {
                                           rateValue = "Excellent";
                                       }*/

                                       if (rating/bidCount <= 0.4) {
                                        rateValue = "Poor";
                                       } else if (rating/bidCount > 0.4 && rating/bidCount <= 0.5) {
                                           rateValue = "Average";
                                       }else if (rating/bidCount > 0.5 && rating/bidCount <= 0.7) {
                                           rateValue = "Satisfactory";
                                       }else if (rating/bidCount > 0.7 && rating/bidCount <= 0.8) {
                                           rateValue = "Most Satisfactory";
                                       }else if (rating/bidCount > 0.8 && rating/bidCount <= 0.9) {
                                           rateValue = "Good";
                                       } else if (rating/bidCount > 0.9 && rating/bidCount <= 0.95) {
                                           rateValue = "Very Good";
                                       } else if (rating/bidCount > 0.95 && rating/bidCount <= 1) {
                                           rateValue = "Excellent";
                                       }

                                       htmlString.append(rateValue);
                                   }else{
                                       double sum = 0;
                                       int bidCount = eachData.size();
                                       double rating = 0;
                                       double total = Double.parseDouble(data[3].toString());
                                       for (Object[] thisData : eachData) {
                                           sum += Double.parseDouble(thisData[0].toString());
                                       }
                                       sum = sum / bidCount;
                                       if(total!=0){
                                            rating = sum / total;
                                       }
                                       String rateValue = null;
                                       /*if (rating == 0) {
                                           rateValue = "Very Poor";
                                       } else if (rating <= 0.4) {
                                           rateValue = "Poor";
                                       } else if (rating > 0.4 && rating <= 0.7) {
                                           rateValue = "Good";
                                       } else if (rating > 0.7 && rating <= 0.9) {
                                           rateValue = "Very Good";
                                       } else if (rating > 0.9 && rating <= 1) {
                                           rateValue = "Excellent";
                                       }*/
                                       
                                       if (rating <= 0.4) {
                                        rateValue = "Poor";
                                       } else if (rating > 0.4 && rating <= 0.5) {
                                           rateValue = "Average";
                                       }else if (rating > 0.5 && rating <= 0.7) {
                                           rateValue = "Satisfactory";
                                       }else if (rating > 0.7 && rating <= 0.8) {
                                           rateValue = "Most Satisfactory";
                                       }else if (rating > 0.8 && rating <= 0.9) {
                                           rateValue = "Good";
                                       } else if (rating > 0.9 && rating <= 0.95) {
                                           rateValue = "Very Good";
                                       } else if (rating > 0.95 && rating <= 1) {
                                           rateValue = "Excellent";
                                       }
                                       htmlString.append(rateValue);
                                   }
                                  // secCount+=sum;
                               } else if (eachData.size() == 1) {
                                   htmlString.append(eachData.get(0)[1]);
                                   //secCount+=Double.parseDouble(eachData.get(0)[0].toString());
                               }
                               htmlString.append("</td>");
                               out.print(htmlString.toString());
                           }
                    %>
                </tr>
                <%}}%>
            </table>           
                <%}else{%>
            <table class="tableList_1 t_space" cellspacing="0" width="100%" id="marksTab">
                <tr>
                    <!--<th class="t-align-center">Form ID</th>-->
                    <th class="t-align-center">Form Name</th>
                    <th class="t-align-center">Criteria</th>
                    <th class="t-align-center">Sub Criteria</th>
                    <th class="t-align-center">Points</th>
                    <%
                                for (Object[] bidder : biddersName) {
                                    String name = bidder[1].toString();
                                    if ("-".equals(name)) {
                                        name = bidder[2].toString() + " " + bidder[3].toString();
                                    }
                                    out.print("<th class='t-align-center'>" + name + "</th>");
                                }
                    %>
                    <!--                    -->
                </tr>
                <%
                             double mainTotal=0;
                            //0 esd.maxMarks ,1 esd.actualMarks,2 esd.ratingWeightage,3 esd.ratedScore,
                            //4 tf.tenderFormId,5 tf.formName,6 esc.subCriteria,7 esd.bidId
                            for (Object formId : criteriaService.getAllFormId(tendID)) {
                                List<Object[]> list = criteriaService.evalServiceRepData(formId.toString(), cpuserId, tendID,evalCount); //change by dohatec for re-evaluation
                               // double secCount=0;
                                for (Object[] data : list) {
                %>
                <tr>
                    <!--td>< %
                               if (!("Adequacy For The Assignment".equals(data[2].toString())
                                       || "Time With Consultant".equals(data[2].toString())
                                       || "Experience In Region & Language".equals(data[2].toString()))) {
                                   out.print(data[0]);
                               } else {
                                   out.print("");
                               }
                        %></td-->
                    <td><%
                               if(true || !("Work plan".equals(data[2].toString())||
                                    "Organization and Staffing".equals(data[2].toString()))){
                                   out.print(data[1]);
                               } else {
                                   out.print("");
                               }
                        %></td>
                    <td><%out.print(data[5]);%></td>
                    <td><%out.print(data[2]);%></td>
                    <td opt="hide"><%out.print(data[3]);mainTotal+=Double.parseDouble(data[3].toString());%></td>
                    <%
                        for (Object[] bidder : biddersName) {
                           out.print("<td>"+new BigDecimal(criteriaService.getEvalBidderMarks(tendID, data[0].toString(), cppuserId, bidder[0].toString(),data[4].toString(),evalCount).toString()).setScale(2, 0)+"</td>"); //change by dohatec for re-evaluation
                        }
                    %>
                </tr>
                <%}
                            }
                            for (Object[] bidder : biddersName) {
                                  double marks = 0.0;
                                  for(Object data : criteriaService.getEvalBidderTotal(tendID, cppuserId, bidder[0].toString(),evalCount)){ //change by dohatec for re-evaluation
                                     marks+=Double.parseDouble(data.toString());
                                  }
                                   tfootData1.append("<td class='formSubHead_1'>"+new BigDecimal(String.valueOf(marks)).setScale(2, 0)+"</td>");
                                   tfootData2.append("<td class='formSubHead_1'>"+minScore+"</td>");
                                   if(marks>=Double.parseDouble(minScore.toString())){
                                        tfootData3.append("<td class='formSubHead_1'>Pass</td>");
                                   }else{
                                       tfootData3.append("<td class='formSubHead_1'>Fail</td>");
                                   }
                            }
                            out.print("<tr><td class='formSubHead_1'></td><td class='formSubHead_1'></td><td align='right' class='formSubHead_1'>Total Points : </td><td class='formSubHead_1'>"+mainTotal+"</td>"+tfootData1.toString()+"</tr>");
                            out.print("<tr><td class='formSubHead_1'></td><td class='formSubHead_1'></td><td align='right' class='formSubHead_1'>Minimum Points : </td><td class='formSubHead_1'></td>"+tfootData2.toString()+"</tr>");
                            out.print("<tr><td class='formSubHead_1'></td><td class='formSubHead_1'></td><td align='right' class='formSubHead_1'>Result : </td><td class='formSubHead_1'></td>"+tfootData3.toString()+"</tr>");
                %>
            </table>
            <%}%>
            <%if(isService && !isREOI){%>
            <script type="text/javascript">
                $('#marksTab td[opt="hide"]').each(function() {
                    var ctd = this.innerHTML;
                    if(ctd=='0'){
                        $(this).parent().hide();
                    }
                });
            </script>
            <%}%>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>
