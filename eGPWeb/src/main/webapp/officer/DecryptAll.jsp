<%--
    Document   : Decrypt All
    Created on : Dec 18, 2010, 3:17:10 PM
    Author     : Sanjay Prajapati,TaherT
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderOpening"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean"%>
<jsp:useBean id ="CurrencySrBean" class="com.cptu.egp.eps.web.servicebean.ConfigureCurrencySrBean"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Decrypt All</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

            String stat = request.getParameter("stat");//(stat = open or eval)
            int tenderId = 0;
            if (request.getParameter("tenderId") != null)
            {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }

            int userId = 0;
            if (session.getAttribute("userId") != null)
            {
                userId = Integer.parseInt(session.getAttribute("userId").toString());
            }

            String referer = "";
            if (request.getHeader("referer") != null)
            {
                referer = request.getHeader("referer");
            }
            //out.print("<h1>"+referer+"</h1>");

            TenderTablesSrBean beanCommon = new TenderTablesSrBean();
            String tenderType = beanCommon.getTenderType(Integer.parseInt(request.getParameter("tenderId")));

            // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType = "tenderId";
            int auditId = tenderId;
            String auditAction = "Decrypt All by Opening Committee Member";
            String moduleName = EgpModule.Tender_Opening.getName();
            String remarks = "";
        %>
        <form id="frmDecrypt" name="frmDecrypt" method="post">
            <input type="hidden" name="tenderid" value="<%= tenderId%>" />
            <input type="hidden" name="hdnFromDecryptPage" value="1" />
            <input type="hidden" name="hdnReferer" value="<%=referer%>" />
        </form>
        <%
            boolean check = false;
            TenderOpening tenOpen = new TenderOpening();

            Object obj[] = tenOpen.getBidFormIds(tenderId, stat, request.getParameter("lotId"));
            int cnt = 0;
            for (int i = 0; i < obj.length; i++)
            {
                //obj[i].toString() == formId
                check = tenOpen.getTenderEncriptionData("getTenderEncData", request.getParameter("tenderId"), obj[i].toString(), "", "", "", "", "", "", "");
                if (!check)
                {
                    cnt++;
        %>
        <!--script>
            jAlert("Form Decrypted successfully", 'Alert', function(r){
                if(r){
                     document.getElementById("frmDecrypt").action = "OpenComm.jsp";
                     document.getElementById("frmDecrypt").submit();
                }else{
                     document.getElementById("frmDecrypt").action = "OpenComm.jsp";
                     document.getElementById("frmDecrypt").submit();
                }
            });
        </script-->
        <%//}else{%>
        <!--script>
          jAlert("Form already Decrypted.", 'Alert', function(r){
              if(r){
                   document.getElementById("frmDecrypt").action = "OpenComm.jsp";
                   document.getElementById("frmDecrypt").submit();
              }else{
                   document.getElementById("frmDecrypt").action = "OpenComm.jsp";
                   document.getElementById("frmDecrypt").submit();
              }
          });
      </script-->
        <%
                }
            }
            if (cnt == 0)
            {
                 // Coad added by Dipal for Audit Trail Log.
               String status = "";
               if(tenderType.equals("ICT")){
                   List<Object[]> listTenderForm = new ArrayList<Object[]>();
                   listTenderForm = CurrencySrBean.getTenderFormId(Integer.parseInt(request.getParameter("tenderId")));
                   for(int i = 0;i<listTenderForm.size();i++)
                   status =  CurrencySrBean.updateColField(listTenderForm.get(i)[0].toString(),Integer.parseInt(request.getParameter("tenderId")));
               }
               else
               {
                    status = "true";
               }
               if(status.equals("true"))
                   {
                    auditAction="Decrypt and updated All by Opening Committee Member";
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    response.sendRedirect(referer);
                  }
              else
                   {
                    auditAction="Decrypt but not updated All by Opening Committee Member";
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    response.sendRedirect(referer);
                  }
            }
            else
            {
                // Coad added by Dipal for Audit Trail Log.
               auditAction="Error in Decrypt All by Opening Committee Member";
               makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
               
                out.print("<h1>Error in Decryption</h1>");
            }
        %>
    </body>

    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
