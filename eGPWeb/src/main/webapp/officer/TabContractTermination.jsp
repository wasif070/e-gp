<%-- 
    Document   : TabContractTermination
    Created on : Aug 20, 2011, 12:17:24 PM
    Author     : Sreenu.Durga
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.WorkFlowServiceImpl"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsContractTermination"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<jsp:useBean id="appSrBean" class="com.cptu.egp.eps.web.servicebean.APPSrBean" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Termination</title>
    </head>
    <%
                pageContext.setAttribute("TSCtab", "6");
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                    issueNOASrBean.setLogUserId(userId);
                }
                String msg = null;
                if (request.getParameter("msg") != null) {
                    msg = request.getParameter("msg");
                }
                WorkFlowServiceImpl workFlowServiceImpl = (WorkFlowServiceImpl) AppContext.getSpringBean("WorkFlowService");
    %>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1">Contract Termination
                            </div>
                            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>
                            <%@include file="../resources/common/TenderInfoBar.jsp" %>
                            <%
                            CommonService commonServiceee = (CommonService) AppContext.getSpringBean("CommonService");
                            String procnatureee = commonServiceee.getProcNature(request.getParameter("tenderId")).toString();
                            List<Object[]> lotObj = commonServiceee.getLotDetails(request.getParameter("tenderId"));
                            Object[] objj = null;
                            if(lotObj!=null && !lotObj.isEmpty())
                            {
                                objj = lotObj.get(0);
                                pageContext.setAttribute("lotId", objj[0].toString());
                            }
                            if("services".equalsIgnoreCase(procnatureee)||"works".equalsIgnoreCase(procnatureee)){
                            %>
                            <%@include file="../resources/common/ContractInfoBar.jsp" %>
                            <%}%>
                            <%
                                        pageContext.setAttribute("tab", "14");
                            %><div class="t_space">
                            <%@include  file="officerTabPanel.jsp"%></div>
                            <div class="tabPanelArea_1 t_space">
                                <%@include  file="../resources/common/CMSTab.jsp"%>                                                                
                                <div class="tabPanelArea_1">
                                    <%if (msg != null && msg.contains("Duplicate")) {%>
                                    <div id="successMsg" class="responseMsg errorMsg t_space" ><span>Bidder has already Terminated the Contract</span></div>
                                    <%}else{%>
                                    <% if (msg != null && !msg.contains("not")) {%>
                                    <div id="successMsg" class="responseMsg successMsg t_space" ><span>File processed successfully</span></div>
                                    <%  } else if (msg != null && msg.contains("not")) {%>
                                    <div id="successMsg" class="responseMsg errorMsg t_space" ><span>File has not processed successfully</span></div>
                                    <%}}%>                                    
                                    <%
                                                pageContext.setAttribute("TSCtab", "2");
                                    %>
                                    <div align="center">
                                        <%
                                                    final int ZERO = 0;
                                                    String strLotNo = "Lot No.";
                                                    String strLotDes = "Lot Description";
                                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                                    //Added for bug Id 5178
                                                    //String conType = commonService.getServiceTypeForTender(Integer.parseInt(tenderId));
                                                    List<SPCommonSearchDataMore> packageLotList = null;
                                                    if("services".equalsIgnoreCase(procnature)){//Added for bug Id 5178
                                                        strLotNo = "Package No.";
                                                        strLotDes = "Package Description";
                                                        packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                    }else{
                                                        packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                    }
                                                    
                                                    for (SPCommonSearchDataMore lotList : packageLotList) {
                                        %>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                            <tr>
                                                <td width="20%"><%=strLotNo%></td>
                                                <td width="80%"><%=lotList.getFieldName3()%></td>
                                            </tr>
                                            <tr>
                                                <td><%=strLotDes%></td>
                                                <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                            </tr>
                                            <tr>
                                                <td>Contract Termination</td>
                                                <td>
                                                    <%

                                                    Object[] object = issueNOASrBean.getDetailsNOAforInvoice(Integer.parseInt(tenderId), Integer.parseInt(lotList.getFieldName5())).get(ZERO);
                                                    if (issueNOASrBean.isAvlTCS((Integer) object[1])) {
                                                        int contractSignId = 0;
                                                        short activityid = 10;
                                                        String donor = "";
                                                        //List<Object[]> listworkflowpack = new ArrayList<Object[]>();
                                                        List<Object> getFileOnSt = new ArrayList<Object>();
                                                        contractSignId = issueNOASrBean.getContractSignId();
                                                        CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                                                        cmsContractTerminationBean.setLogUserId(userId);
                                                        TblCmsContractTermination tblCmsContractTermination =
                                                                cmsContractTerminationBean.getCmsContractTerminationForContractSignId(contractSignId);
                                                        if (tblCmsContractTermination == null) {
                                                            out.print("<a onclick = 'return checkDupli("+contractSignId+");' href ='"+request.getContextPath()+"/resources/common/ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                    + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Terminate</a> ");
                                                        } else {
                                                            if("rejected".equalsIgnoreCase(tblCmsContractTermination.getStatus()))
                                                            {
                                                                out.print("<a onclick = 'return checkDupli("+contractSignId+");' href ='"+request.getContextPath()+"/resources/common/ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                    + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Terminate</a> ");
                                                            }
                                                            else{
                                                                out.print("<a href ='ViewContractTermination.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                        + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>View Contract Termination Detail</a>");
                                                                WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                                                                List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(tblCmsContractTermination.getContractTerminationId(), tblCmsContractTermination.getContractTerminationId(), activityid);
                                                                int initiator = 0;
                                                                int approver = 0;
                                                                boolean iswfLevelExist = false;
                                                                if (tblWorkFlowLevelConfig.size() > 0) {
                                                                Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                                                iswfLevelExist = true;
                                                                while (twflc.hasNext()) {
                                                                     TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                                                     TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                                                     if (workFlowlel.getWfRoleId() == 1) {
                                                                     initiator = lmaster.getUserId();
                                                                     }
                                                                     if (workFlowlel.getWfRoleId() == 2) {
                                                                         approver = lmaster.getUserId();
                                                                     }
                                                                     }}
                                                                donor = workFlowSrBean.isDonorReq("9",tblCmsContractTermination.getContractTerminationId());
                                                                String[] norevs = donor.split("_");
                                                                String noOfReviewers = norevs[1];
                                                                getFileOnSt = appSrBean.getFileOnHandStatus(tblCmsContractTermination.getContractTerminationId(),tblCmsContractTermination.getContractTerminationId(),Integer.parseInt(userId),9);
                                                                if ("pending".equalsIgnoreCase(tblCmsContractTermination.getWorkflowStatus())) {
                                                                  // out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                    if(tblCmsContractTermination.getUserTypeId()==3)
                                                                    {
                                                                        if(!"0".equalsIgnoreCase(noOfReviewers))
                                                                        {
                                                                                out.print("&nbsp;|&nbsp;<a href ='EditCT.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                                       + "&tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8] + "&lotId=" + lotList.getFieldName5()
                                                                                       + "&contractSignId=" + contractSignId + "'>Edit Contract Termination</a>");
                                                                        }
                                                                        //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                        out.print("&nbsp;|&nbsp;<a href ='"+request.getContextPath()+"/resources/common/ContractTeminationUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                                + "&contractSignId=" + contractSignId + "&contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                                + "&action=upload" + "'>Upload/Download Files</a>");
                                                                    }
                                                                   // out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                    if("0".equalsIgnoreCase(noOfReviewers))
                                                                    {
                                                                       if(initiator!=approver)
                                                                       {
                                                                           if(tblWorkFlowLevelConfig.size()>0)
                                                                           {
                                                                                if(!"Yes".equalsIgnoreCase(getFileOnSt.get(0).toString())){
                                                                                    out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tblCmsContractTermination.getContractTerminationId() + "&action=View&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                                    out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&fraction=Termination&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View Workflow History</a>");
                                                                                }else{
                                                                                    out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tenderId + "&action=Edit&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>");
                                                                                    out.print("&nbsp|&nbsp<a href='FileProcessing.jsp?activityid="+activityid+"&objectid="+ tenderId +"&childid="+ tblCmsContractTermination.getContractTerminationId() +"&eventid=9&fromaction=Termination&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Process file in Workflow</a>");
                                                                                }
                                                                           }else{
                                                                                out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tenderId + "&action=Edit&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>");
                                                                           }     
                                                                       }else{    
                                                                              if(tblWorkFlowLevelConfig.size()>0)
                                                                              {
                                                                                if("pending".equalsIgnoreCase(tblCmsContractTermination.getStatus()))
                                                                                {
                                                                                out.print("&nbsp;|&nbsp;<a href ='PublishCT.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                                        + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>Process and Publish</a>");
                                                                                }
                                                                                else
                                                                                {
                                                                                    if(initiator!=approver)
                                                                                    {
                                                                                        out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&fraction=Termination&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View Workflow History</a>");
                                                                                    }
                                                                                    out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tblCmsContractTermination.getContractTerminationId() + "&action=View&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                                }
                                                                              }else{
                                                                                  out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tenderId + "&action=Edit&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>");
                                                                              }
                                                                       }       
                                                                    }
                                                                    else
                                                                    {
                                                                        if(tblWorkFlowLevelConfig.size()>0)
                                                                        {
                                                                            if(!"approved".equalsIgnoreCase(tblCmsContractTermination.getStatus()))
                                                                            {
                                                                                out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tenderId + "&action=Edit&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>");
                                                                            }
                                                                            //listworkflowpack = appSrBean.getWorkflowHistorylink(Integer.parseInt(tenderId));                                                                            
                                                                            //if(!listworkflowpack.isEmpty()){
                                                                            if(!"Yes".equalsIgnoreCase(getFileOnSt.get(0).toString())){
                                                                                out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"&fraction=Termination'>View Workflow History</a>");
                                                                            }else{
                                                                                out.print("&nbsp|&nbsp<a href='FileProcessing.jsp?activityid="+activityid+"&objectid="+ tenderId +"&childid="+ tblCmsContractTermination.getContractTerminationId() +"&eventid=9&fromaction=Termination&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Process file in Workflow</a>");
                                                                            }
                                                                        }else{
                                                                            if(noOfReviewers==null || noOfReviewers=="" || "".equalsIgnoreCase(noOfReviewers) || "null".equalsIgnoreCase(noOfReviewers)){
                                                                                if(workFlowServiceImpl.getWorkFlowRuleEngineData(9)){
                                                                                    out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tenderId + "&action=Create&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Create Workflow</a>");
                                                                                }else{%>&nbsp;|&nbsp;<a href="#" onclick="jAlert('Workflow Configuration is pending in BusinessRule','Repeat Order', function(RetVal) {});">Create Workflow</a><%}
                                                                            }else{
                                                                                out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tenderId + "&action=Edit&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>");
                                                                            }
                                                                        }
                                                                   }
                                                                } else if ("approved".equalsIgnoreCase(tblCmsContractTermination.getWorkflowStatus())) {
                                                                    //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                    out.print("&nbsp;|&nbsp;<a href ='"+request.getContextPath()+"/resources/common/ContractTeminationUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                            + "&contractSignId=" + contractSignId + "&contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                            + "&action=download" + "'>Download Files</a>");
                                                                    if("pending".equalsIgnoreCase(tblCmsContractTermination.getStatus())) {
                                                                        //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                        out.print("&nbsp;|&nbsp;<a href ='PublishCT.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                                + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>Process and Publish</a>");
                                                                    }
                                                                    else{
                                                                        out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"&fraction=Termination'>View Workflow History</a>");
                                                                        out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tblCmsContractTermination.getContractTerminationId() + "&action=View&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                    }
                                                                }else if ("rejected".equalsIgnoreCase(tblCmsContractTermination.getWorkflowStatus())) {
                                                                        if(!"rejected".equalsIgnoreCase(tblCmsContractTermination.getStatus()))
                                                                        {
                                                                            out.print("&nbsp;|&nbsp;<a href ='PublishCT.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                                    + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>Process and Publish</a>");
                                                                        }
                                                                        else
                                                                        {
                                                                            if("0".equalsIgnoreCase(noOfReviewers))
                                                                            {
                                                                                if(initiator!=approver)
                                                                                {
                                                                                    out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&fraction=Termination&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View Workflow History</a>");
                                                                                }
                                                                                out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tblCmsContractTermination.getContractTerminationId() + "&action=View&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                            }
                                                                            else
                                                                            {
                                                                                out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&fraction=Termination&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View Workflow History</a>");
                                                                                out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tblCmsContractTermination.getContractTerminationId() + "&action=View&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                            }
                                                                        }
                                                                }
                                                                // end else if
                                                            }//end else]
                                                          }
                                                          out.print("&nbsp|&nbsp<a href='"+request.getContextPath()+"/resources/common/CTHistory.jsp?tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Contract Termination History</a>");
                                                    }//end main if

                                                    %>
                                                </td>
                                            </tr>
                                        
                                        <br>
                                         <%
                                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                                        List<Object[]> mainForRO = ros.getMainLoopForROPESide(Integer.parseInt(lotList.getFieldName5()));
                                        if(!mainForRO.isEmpty()){
                                            int icount = 1;
                                                for(Object[] objs : mainForRO){
                                                    for (Object[] objd : issueNOASrBean.getNOAListingForRO(Integer.parseInt(tenderId),(Integer) objs[0])) {
                                                         List<Object> wpids = ros.getWpIdForAfterRODone(Integer.parseInt(objs[1].toString()));

                        %>

                              <tr>
                                    <td colspan="2">
                                <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Repeat Order-<%=icount%></li>
                                </ul>

                                    </td>
                                    </tr>
                                               <tr>
                                                <td>Contract Termination</td>
                                                <td>
                                                    <%

                                                    Object[] objects = issueNOASrBean.getDetailsForRO(Integer.parseInt(tenderId), Integer.parseInt(lotList.getFieldName5()),Integer.parseInt(objs[0].toString())).get(ZERO);
                                                    if (issueNOASrBean.isAvlTCS((Integer) objects[1])) {
                                                        int contractSignId = 0;
                                                        short activityid = 10;
                                                        String donor = "";
                                                        //List<Object[]> listworkflowpack = new ArrayList<Object[]>();
                                                        List<Object> getFileOnSt = new ArrayList<Object>();
                                                        contractSignId = issueNOASrBean.getContractSignId();
                                                        CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                                                        cmsContractTerminationBean.setLogUserId(userId);
                                                        TblCmsContractTermination tblCmsContractTermination =
                                                                cmsContractTerminationBean.getCmsContractTerminationForContractSignId(contractSignId);
                                                        if (tblCmsContractTermination == null) {
                                                            out.print("<a onclick = 'return checkDupli("+contractSignId+");' href ='"+request.getContextPath()+"/resources/common/ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) objects[8]
                                                                    + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Terminate</a> ");
                                                        } else {
                                                            if("rejected".equalsIgnoreCase(tblCmsContractTermination.getStatus()))
                                                            {
                                                                out.print("<a onclick = 'return checkDupli("+contractSignId+");' href ='"+request.getContextPath()+"/resources/common/ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) objects[8]
                                                                    + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Terminate</a> ");
                                                            }
                                                            else{
                                                                out.print("<a href ='ViewContractTermination.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                        + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>View Contract Termination Detail</a>");
                                                                WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                                                                List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(tblCmsContractTermination.getContractTerminationId(), tblCmsContractTermination.getContractTerminationId(), activityid);
                                                                int initiator = 0;
                                                                int approver = 0;
                                                                boolean iswfLevelExist = false;
                                                                if (tblWorkFlowLevelConfig.size() > 0) {
                                                                Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                                                iswfLevelExist = true;
                                                                while (twflc.hasNext()) {
                                                                     TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                                                     TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                                                     if (workFlowlel.getWfRoleId() == 1) {
                                                                     initiator = lmaster.getUserId();
                                                                     }
                                                                     if (workFlowlel.getWfRoleId() == 2) {
                                                                         approver = lmaster.getUserId();
                                                                     }
                                                                     }}
                                                                donor = workFlowSrBean.isDonorReq("9",tblCmsContractTermination.getContractTerminationId());
                                                                String[] norevs = donor.split("_");
                                                                String noOfReviewers = norevs[1];

                                                                if ("pending".equalsIgnoreCase(tblCmsContractTermination.getWorkflowStatus())) {
                                                                  // out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                    if(tblCmsContractTermination.getUserTypeId()==3)
                                                                    {
                                                                        if(!"0".equalsIgnoreCase(noOfReviewers))
                                                                        {
                                                                                out.print("&nbsp;|&nbsp;<a href ='EditCT.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                                       + "&tenderId=" + tenderId + "&contractUserId=" + (Integer) objects[8] + "&lotId=" + lotList.getFieldName5()
                                                                                       + "&contractSignId=" + contractSignId + "'>Edit Contract Termination</a>");
                                                                        }
                                                                        //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                        out.print("&nbsp;|&nbsp;<a href ='"+request.getContextPath()+"/resources/common/ContractTeminationUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                                + "&contractSignId=" + contractSignId + "&contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                                + "&action=upload" + "'>Upload/Download Files</a>");
                                                                    }
                                                                   // out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                    if("0".equalsIgnoreCase(noOfReviewers))
                                                                    {
                                                                      if(tblWorkFlowLevelConfig.size()>0)
                                                                      {
                                                                        if("pending".equalsIgnoreCase(tblCmsContractTermination.getStatus()))
                                                                        {
                                                                        out.print("&nbsp;|&nbsp;<a href ='PublishCT.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                                + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>Process and Publish</a>");
                                                                        }
                                                                        else
                                                                        {
                                                                            if(initiator!=approver)
                                                                            {
                                                                                out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&fraction=Termination&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View Workflow History</a>");
                                                                            }
                                                                            out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tblCmsContractTermination.getContractTerminationId() + "&action=View&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                        }
                                                                      }else{
                                                                          out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tenderId + "&action=Edit&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>");
                                                                      }
                                                                    }
                                                                    else
                                                                    {
                                                                        if(tblWorkFlowLevelConfig.size()>0)
                                                                        {
                                                                            if(!"approved".equalsIgnoreCase(tblCmsContractTermination.getStatus()))
                                                                            {
                                                                                out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tenderId + "&action=Edit&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>");
                                                                            }
                                                                            //listworkflowpack = appSrBean.getWorkflowHistorylink(Integer.parseInt(tenderId));
                                                                            getFileOnSt = appSrBean.getFileOnHandStatus(tblCmsContractTermination.getContractTerminationId(),tblCmsContractTermination.getContractTerminationId(),Integer.parseInt(userId),9);
                                                                            //if(!listworkflowpack.isEmpty()){
                                                                            if(!"Yes".equalsIgnoreCase(getFileOnSt.get(0).toString())){
                                                                                out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"&fraction=Termination'>View Workflow History</a>");
                                                                            }else{
                                                                                out.print("&nbsp|&nbsp<a href='FileProcessing.jsp?activityid="+activityid+"&objectid="+ tenderId +"&childid="+ tblCmsContractTermination.getContractTerminationId() +"&eventid=9&fromaction=Termination&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Process file in Workflow</a>");
                                                                            }
                                                                        }else{
                                                                            if(noOfReviewers==null || noOfReviewers=="" || "".equalsIgnoreCase(noOfReviewers) || "null".equalsIgnoreCase(noOfReviewers)){
                                                                                if(workFlowServiceImpl.getWorkFlowRuleEngineData(9)){                                                                                                                                                                                                                                       
                                                                                    out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tenderId + "&action=Create&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Create Workflow</a>");
                                                                                }else{%>&nbsp;|&nbsp;<a href="#" onclick="jAlert('Workflow Configuration is pending in BusinessRule','Repeat Order', function(RetVal) {});">Create Workflow</a><%}
                                                                            }else{
                                                                                out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tenderId + "&action=Edit&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>"); 
                                                                            }
                                                                        }
                                                                   }
                                                                } else if ("approved".equalsIgnoreCase(tblCmsContractTermination.getWorkflowStatus())) {
                                                                    out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                    out.print("&nbsp;|&nbsp;<a href ='"+request.getContextPath()+"/resources/common/ContractTeminationUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                            + "&contractSignId=" + contractSignId + "&contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                            + "&action=download" + "'>Download Files</a>");
                                                                    if("pending".equalsIgnoreCase(tblCmsContractTermination.getStatus())) {
                                                                        out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                                        out.print("<a href ='PublishCT.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                                + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>Process and Publish</a>");
                                                                    }
                                                                    else{
                                                                        out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"&fraction=Termination'>View Workflow History</a>");
                                                                        out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tblCmsContractTermination.getContractTerminationId() + "&action=View&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                    }
                                                                }else if ("rejected".equalsIgnoreCase(tblCmsContractTermination.getWorkflowStatus())) {
                                                                        if(!"rejected".equalsIgnoreCase(tblCmsContractTermination.getStatus()))
                                                                        {
                                                                            out.print("&nbsp;|&nbsp;<a href ='PublishCT.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                                    + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>Process and Publish</a>");
                                                                        }
                                                                        else
                                                                        {
                                                                            if("0".equalsIgnoreCase(noOfReviewers))
                                                                            {
                                                                                if(initiator!=approver)
                                                                                {
                                                                                    out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&fraction=Termination&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View Workflow History</a>");
                                                                                }
                                                                                out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tblCmsContractTermination.getContractTerminationId() + "&action=View&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                            }
                                                                            else
                                                                            {
                                                                                out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&fraction=Termination&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View Workflow History</a>");
                                                                                out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tblCmsContractTermination.getContractTerminationId() + "&action=View&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                            }
                                                                        }
                                                                }
                                                                // end else if
                                                            }//end else]
                                                          }
                                                          out.print("&nbsp|&nbsp<a href='"+request.getContextPath()+"/resources/common/CTHistory.jsp?tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Contract Termination History</a>");
                                                    }//end main if

                                                    %>
                                        




                                        <%}%>

                                        <%}%>

                                        <%}%>





                                        <%
                                                    }
                                        %>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
        <script type="text/javascript">
          function checkDupli(ContractId)
          {
              var flag = true;
              $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/ContractTerminationServlet",
                        data:"contractId="+ContractId+"&funName=checkDuplicateRecord",
                        async: false,
                        success: function(j){                            
                            if(j.toString()!="0"){
                                flag=false;
                                jAlert("Bidder has already Terminated the Contract","Contract Termination", function(RetVal) {
                                });
                            }
                        }
                    });
                    return flag;
          }
      </script>
    </body>
</html>

