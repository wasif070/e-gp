<%-- 
    Document   : TenderAdvList
    Created on : Nov 22, 2010, 6:53:52 PM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Advertisements</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js">

            /***********************************************
             * All Levels Navigational Menu- (c) Dynamic Drive DHTML code library (http://www.dynamicdrive.com)
             * This notice MUST stay intact for legal use
             * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
             ***********************************************/

        </script>
    </head>
    <body>
        <%
    String tenderId = "", userId = "";
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userId = hs.getAttribute("userId").toString();
                    }

                    int counterId = 0;
                    if (request.getParameter("tenderId") != "") {
                        tenderId = request.getParameter("tenderId");
                    }
                    
                // Coad added by Dipal for Audit Trail Log.
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                String idType="tenderId";
                int auditId=Integer.parseInt(tenderId);
                String auditAction="View Advertisement";
                String moduleName=EgpModule.Evaluation.getName();
                String remarks="";
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
%>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>         
            <!--Dashboard Header End-->            
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
            <div class="pageHead_1">
                Tender Advertisements
                <span style="float: right;">
                <a href="<%=request.getContextPath() %>/officer/EvalComm.jsp?tenderid=<%=tenderId %>" class="action-button-goback">Go Back</a>    
                </span>
            </div>
            <%
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", tenderId);
            %>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
             <%                
                String msgId="", msgTxt="";
                    boolean isError=false;
                if (request.getParameter("msgId")!=null){
                    
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("created")){
                            msgTxt="Tender Advertisement added successfully";
                        } else if(msgId.equalsIgnoreCase("updated")){
                            msgTxt="Tender Advertisement updated successfully";
                        }  else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error";
                        }  else {
                            msgTxt="";
                        }
                    %>                   

                <%}}%>
            

            <div class="borderDivider t_space" >&nbsp;</div>
            <%//pageContext.setAttribute("tab", "12");%>
              <% pageContext.setAttribute("tab","7"); %>

            <%@include file="officerTabPanel.jsp" %>
            <div class="tabPanelArea_1">
            
            <%if ((isError) && msgTxt!=""){%>
                       <div class="responseMsg errorMsg"><%=msgTxt%></div><br/>
                   <%} else if (msgTxt!="") {%>
                        <div class="responseMsg successMsg"><%=msgTxt%></div><br/>
                   <%}%>
                   <div class="t-align-right b_space">
                <%
                    String usertypeid = "";
                    if(request.getSession().getAttribute("userId") != null)
                    {
                        usertypeid = request.getSession().getAttribute("userTypeId").toString();
                    }
                    if("3".equalsIgnoreCase(usertypeid))
                    {
                %>
                    <a href="TenderAdv.jsp?tenderId=<%=request.getParameter("tenderId")%>"  class="action-button-add">Add Advertisement</a>
                  <%}%>
            </div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <th width="4%" class="t-align-center">Sl. No.</th>
                    <th width="20%" class="t-align-center"><span class="ff">Name of the Newspaper </span></th>
                    <th width="14%" class="t-align-center">Newspaper Advertisement Date</th>
                    <th width="27%" class="t-align-center" >Full URL of Advertisement Page</th>
                    <th width="16%" class="t-align-center">Website Advertisement Date</th>
                    <th width="12%" class="t-align-center">Date of URL Publishing </th>
                    <th width="7%" class="t-align-center">Action</th>
                </tr>

                <%
                

                    tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    for (SPTenderCommonData sptcd : tenderCommonService.returndata("getTenderAdvtList", tenderId, "")) {
                        counterId = counterId + 1;
                %>
                <tr
                    <%if(Math.IEEEremainder(counterId,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                        <%}%>
                    >

                    <td class="t-align-center"><%=counterId%></td>
                    <td class="t-align-left"><div class="break-word"><%=sptcd.getFieldName2()%></div></td>
                    <td class="t-align-center"><%=sptcd.getFieldName3()%></td>
                    <td class="t-align-center"><div class="break-word"><%if(sptcd.getFieldName4()!=null && !"".equalsIgnoreCase(sptcd.getFieldName4())){out.print(sptcd.getFieldName4());}else{%> - <%}%></div></td>
                    <td class="t-align-center">
                    <%
                        if(sptcd.getFieldName5()==null || sptcd.getFieldName5().equals("01-Jan-1900"))
                        {
                    %>    -
                        <%} else {%>
                        <%=sptcd.getFieldName5()%>
                        <%}%>
                    </td>
<!--                    <td class="t-align-center">< %=sptcd.getFieldName7()%></td>-->
                    <td class="t-align-center">
                        <%
                        if(sptcd.getFieldName5()==null || sptcd.getFieldName5().equals("01-Jan-1900")){
                        %>    -
                        <%} else {%>
                        <%=sptcd.getFieldName5()%>
                        <%}%>
                    </td>
                    <td class="t-align-center"><a href="TenderAdv.jsp?tenderId=<%=request.getParameter("tenderId")%>&action=edit&tadId=<%=sptcd.getFieldName1()%>" title="Edit">Edit</a></td>
                </tr>
                <%}%>
                <%if (counterId == 0) {%>
                <tr><td colspan="8" class="t-align-center">No records found.</td>
                </tr>
                <%}%>
            </table>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>            
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabEval");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
