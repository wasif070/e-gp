<%-- 
    Document   : SendSmsToApprovers
    Created on : Feb 19, 2011, 6:15:48 PM
    Author     : Chalapathi.Bavisetti
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Send SMS to Approvers</title>
    </head>
    <body>
       <%
        SendMessageUtil sendMessageUtil = new SendMessageUtil();
        List <SPCommonSearchData> spcommonSearchData = null;
        CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        spcommonSearchData = cmnSearchService.searchData("getMobNoforSMS", null, null, null, null, null, null, null, null, null);
        
        for(SPCommonSearchData searchData :spcommonSearchData){
        sendMessageUtil.setSmsNo(searchData.getFieldName1());
        sendMessageUtil.setSmsBody("You are requested to process below mentioned file immediately:"+
                                    "Module : "+searchData.getFieldName2()+
                                    " Process: "+searchData.getFieldName3()+
                                    " ID: "+searchData.getFieldName4());
        sendMessageUtil.sendSMS();
       
        }

           %>
    </body>
</html>
