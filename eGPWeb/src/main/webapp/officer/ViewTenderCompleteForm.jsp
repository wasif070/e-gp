<%--
    Document   : ViewTenderCompleteForm
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListBox"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderComboSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListCellDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListCellDetail"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="frmView"  class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />
<jsp:useBean id="frmDtl"  class="com.cptu.egp.eps.web.servicebean.TenderFormSrBean" />
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTables" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderColumns" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCells" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderFormula" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Form</title>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <%
                Logger LOGGER = Logger.getLogger("ViewTenderCompleteForm.jsp");
                int tenderId = 0;
                int sectionId = 0;
                int formId = 0;
                int userId = 0;

                if (session.getAttribute("userId") != null) {
                    if (!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                }


                String isPDF = "abc";
                if (request.getParameter("isPDF") != null) {
                    isPDF = request.getParameter("isPDF");
                }

                int pkgOrLotId = -1;


                if (request.getParameter("tenderId") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }
                if (request.getParameter("sectionId") != null) {
                    sectionId = Integer.parseInt(request.getParameter("sectionId"));
                }
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }
                if (request.getParameter("porlId") != null) {
                    pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                }

                String frmName = "";
                String frmHeader = "";
                String frmFooter = "";
                boolean isBOQForm = false;
                java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> frm = frmDtl.getFormDetail(formId);
                if (frm != null) {
                    if (frm.size() > 0) {
                        frmName = frm.get(0).getFormName();
                        if (frmName != null) {
                            frmName = frmName.replace("?s", "'s");
                        }
                        frmHeader = frm.get(0).getFormHeader();
                        frmFooter = frm.get(0).getFormFooter();
                        if ("yes".equals(frm.get(0).getIsPriceBid())) {
                            isBOQForm = true;
                        } else {
                            isBOQForm = false;
                        }
                    }
                    frm = null;
                }
    %>
    <body>
        <div class="dashboard_div">
            <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <% }%>
            <div class="contentArea_1">
                <div class="pageHead_1">
                    Form View

                    <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                    <!--span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%= tenderId%>" class="action-button-goback" title="Tender Document">Tender Document</a></span-->
                    <% 
                        String refererURL = request.getHeader("referer").toString(); 
                    %>
                    <% if(refererURL.contains("TenderDocView.jsp")){ %>
                        <%if (pkgOrLotId == -1) {%>
                        <span class="c-alignment-right"><a href="TenderDocView.jsp?tenderId=<%= tenderId%>" class="action-button-goback" title="Tender/Proposal Document">Tender Document</a></span>
                        <%} else {%>
                        <span class="c-alignment-right"><a href="TenderDocView.jsp?tenderId=<%= tenderId%>&porlId=<%= pkgOrLotId%>" title="Tender/Proposal Document" class="action-button-goback">Tender Document</a></span>
                        <% } %>
                     <% }else{ %>
                        <%if (pkgOrLotId == -1) {%>
                        <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%= tenderId%>" class="action-button-goback" title="Tender/Proposal Document">Tender Document</a></span>
                        <%} else {%>
                        <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%= tenderId%>&porlId=<%= pkgOrLotId%>" title="Tender/Proposal Document" class="action-button-goback">Tender Document</a></span>
                        <% }
                        } %>
                        
                     <% } %>
                </div>
                
                <%
                    pageContext.setAttribute("tenderId", tenderId);
                %>
                <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <% }%>
                <!--Middle Content Table Start-->
                <% if (!isBOQForm) {%>
                <table width="100%" cellspacing="0" class="t_space">
                    <tr>
                        <td width="100" class="ff">Is BOQ Form : </td>
                        <td>
<%
                             out.print("No");
%>
                        </td>
                    </tr>
                </table><%}%>
                <% if (frmName != null && !"".equals(frmName.trim())) {%>
                <table width="100%" class="tableHead_1 t_space">
                    <tr>
                        <td width="100" class="ff">Form Name : </td>
                        <td style="font-weight: normal;"><%= frmName%></td>
                    </tr>
                </table>
<%                  }
                    if (frmHeader != null && !"".equals(frmHeader.trim())) {
%>
                <table width="100%" class="tableHead_1 t_space">
                    <tr>
                        <td width="100" class="ff">Form Header : </td>
                        <td style="font-weight: normal; line-height: 1.75"><%= frmHeader%></td>
                    </tr>
                </table>
                    <%}%>
                
<%
                    boolean isTotFormulaCre = false;
                    List<TblTenderTables> tblTenderTables = frmView.getTenderTables(formId);
                    for (TblTenderTables tbl : tblTenderTables) {
                        java.util.List<com.cptu.egp.eps.model.table.TblTenderTables> tblInfo = frmView.getTenderTablesDetail(tbl.getTenderTableId());
                        short cols = 0;
                        short rows = 0;
                        String tableName = "";
                        String tableHeader = "";
                        String tableFooter = "";
                        if (tblInfo != null) {
                            if (tblInfo.size() >= 0) {
                                tableName = tblInfo.get(0).getTableName();
                                if (tableName != null) {
                                    tableName = tableName.replace("?s", "'s");
                                }
                                cols = tblInfo.get(0).getNoOfCols();
                                rows = tblInfo.get(0).getNoOfRows();
                                tableHeader = tblInfo.get(0).getTableHeader();
                                tableFooter = tblInfo.get(0).getTableFooter();
                            }
                            tblInfo = null;
                        }
                        //cols = frmView.getNoOfColsInTable(tbl.getTenderTableId());
                        //rows = frmView.getNoOfRowsInTable(tbl.getTenderTableId(), (short) 1);

                        java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderColumns> tblColumnsDtl = frmView.getColumnsDtls(tbl.getTenderTableId(), true).listIterator();
                        java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderCells> tblCellsDtl = frmView.getCellsDtls(tbl.getTenderTableId()).listIterator();
                        java.util.List<TblTenderColumns> tblColumnsDtlWithOutSortOrder = frmView.getColumnsDtls(tbl.getTenderTableId(), false);

                        String colHeader = "";
                        byte filledBy = 0;
                        byte dataType = 0;
                        String colType = "";
                        int colId = 0;
                        byte showOrHide = 1;

                        short fillBy[] = new short[cols];
                        int arrColId[] = new int[cols];
                        String arrColType[] = new String[cols];

                        List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
                        TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                        listCurrencyObj = tenderCurrencyService.getCurrencyTenderwise(tenderId, userId);

                        String arrColName[] = new String[cols];
                        if (tblColumnsDtlWithOutSortOrder != null) {
                            if (tblColumnsDtlWithOutSortOrder.size() > 0) {
                                byte i = 0;
                                for (TblTenderColumns ttc : tblColumnsDtlWithOutSortOrder) {
                                    arrColName[i] = ttc.getColumnHeader();
                                    i++;
                                }
                            }
                            tblColumnsDtlWithOutSortOrder = null;
                        }

                        if (tableName != null && !"".equals(tableName.trim())) {%>
                    <table width="100%" class="tableHead_1 t_space">
                        <tr>
                            <td width="100" class="ff">Table Name : </td>
                            <td style="font-weight: normal;"><%= tableName%></td>
                        </tr>
                    </table>
<%                      }
                        if (tableHeader != null && !"".equals(tableHeader.trim())) {%>
                    <table width="100%" class="tableHead_1 t_space">
                        <tr>
                            <td class="ff">Table Header : </td>
                            <td style="font-weight: normal; line-height: 1.75"><%= tableHeader%></td>
                        </tr>
                    </table>
                        <%}%>
                    <div style="overflow: auto;width: 100%">
                        <table width="100%" cellspacing="0" class="tableList_1" id="FormMatrix">
                            <tbody>
<%
                        isTotFormulaCre = frmView.isTotalFormulaCreated(tbl.getTenderTableId());
                        String strDtType = "";
                        for (short i = -1; i <= rows; i++) {
                            if (i == 0) {
%>
                                <tr id="ColumnRow">
<%
                                for (short j = 0; j < cols; j++) {
                                    if (tblColumnsDtl.hasNext()) {
                                        TblTenderColumns ttc = tblColumnsDtl.next();
                                        colHeader = ttc.getColumnHeader();
                                        if (colHeader != null) {
                                            colHeader = colHeader.replace("?s", "'s");
                                        }
                                        colType = ttc.getColumnType();
                                        filledBy = ttc.getFilledBy();
                                        dataType = ttc.getDataType();
                                        colId = ttc.getColumnId();
                                        showOrHide = Byte.parseByte(ttc.getShoworhide());
                                        ttc = null;
                                    }
                                    fillBy[j] = filledBy;
                                    arrColId[j] = colId;
                                    arrColType[j] = colType;
%>
                                    <th id="addTD<%= j + 1%>" colid="<%= j + 1%>" <%if (false && showOrHide == 0) {
                                                                                        out.print(" style='display:none' ");
                                                                                    }
%>
                                    >
                                        <%= colHeader%>
                                    </th>
<%
                                }
%>
                                </tr>
<%
                            }

                            if (i > 0) {
                                if (i == rows && isTotFormulaCre) {
                                    java.util.HashMap<Integer, Integer> hmGTCols = frmView.getGTColumns(tbl.getTenderTableId());
%>
                                <tr id="TR<%=i%>">
<%
                                    for (int j = 0; j < cols; j++) {
%>
                                    <td id="TD<%= i%>_<%= j + 1%>" align="center">
<%
                                        if (hmGTCols != null) {
                                            if (hmGTCols.containsValue(arrColId[j])) {
                                                out.print("<b>Total Formula</b>");
                                            } else {
                                                out.print("");
                                            }
                                        }
%>
                                    </td>
<%
                                    }
%>
                                </tr>
<%
                                    if (hmGTCols != null) {
                                        hmGTCols = null;
                                    }
                                } else {
%>
                                <tr id="TR<%=i%>">
<%
                                   int cnt = 0;
                                    short columnId;
                                    int cellId = 0;
                                    List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();

                                    for (int j = 0; j < cols; j++) {
                                        String cellValue = "";
                                        if (tblCellsDtl.hasNext()) {
                                            cnt++;
                                            TblTenderCells cells = tblCellsDtl.next();
%>
                                    <td id="TD<%= i%>_<%= j + 1%>" align="center" <%if (false && cells.getShowOrHide() == 0) {
                                                                                       out.print(" style='display:none' ");
                                                                                    }
        %>
                                    >
<%
                                            dataType = cells.getCellDatatype();
                                            filledBy = cells.getCellDatatype();
                                            cellValue = cells.getCellvalue();
                                            if (cellValue != null) {
                                                cellValue = cellValue.replace("?s", "'s");
                                            }

                                            columnId = cells.getColumnId();
                                            cellId = cells.getCellId();
                                            listCellDetail = frmView.getTenderListCellDetail(tbl.getTenderTableId(), columnId, cellId);

                                            if (dataType == 1) {
                                                strDtType = "Small Text";
                                            }
                                            if (dataType == 2) {
                                                strDtType = "Long Text";
                                            }
                                            if (dataType == 3) {
                                                strDtType = "Money Positive";
                                            }
                                            if (dataType == 8) {
                                                strDtType = "Money All";
                                            }
                                            if (dataType == 6) {
                                                strDtType = "";
                                            }
                                            if (dataType == 4) {
                                                strDtType = "Numeric";
                                            }
                                            if (dataType == 9) {
                                                strDtType = "Combo Box with Calculation";
                                            }
                                            if (dataType == 10) {
                                                strDtType = "Combo Box w/o Calculation";
                                            }
                                            if (dataType == 11) {
                                                strDtType = "Money All (-5 to +5)";
                                            }
                                            if (dataType == 12) {
                                                strDtType = "Date";
                                            }
                                            if (dataType == 13) {
                                                strDtType = "Money Positive(3 digits after decimal)";
                                            }

                                            if (fillBy[j] == 2) {
                                                out.print("Fill By Bidder/Consultant - " + strDtType);
                                            } else if (fillBy[j] == 1) {
                                                out.print(cellValue);
                                            } else {
                                                out.print("Auto - " + strDtType);
                                            }

                                            if (fillBy[j] == 2 && dataType == 3 && "8".equals(arrColType[j])) {
                                                if (listCurrencyObj.size() > 0) {
%>
                                        <select id="Currency<%=tbl.getTenderTableId()%>_<%=i%>_<%=columnId%>" name="Currency<%=tbl.getTenderTableId()%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeCurrencyValue('row<%=tbl.getTenderTableId()%>_<%=i%>_<%=columnId%>',this.value,'<%=tbl.getTenderTableId()%>');">
                                            <% for (Object[] obj : listCurrencyObj) {%>
                                            <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                            <% }%>
                                        </select>
<%
                                                }
                                            }

                                            if (dataType == 9 || dataType == 10) {
                                                TenderComboSrBean cmbSrBean = new TenderComboSrBean();
                                                if (listCellDetail.size() > 0) {
                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();
                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                    /* Dohatec: ICT-Goods Start */
                                                    //Get Currency List to be shown at 'Currency' Combo box
                                                    List<Object[]> currencyObjectList = null;
                                                    if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                        currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                    }

                                                    //Procurement Type (i.e. NCT/ICT) retrieval
                                                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                    List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                    boolean isIctTender = false;
                                                    if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                        isIctTender = true;
                                                    }

                                                    //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                    String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                    /* Dohatec: ICT-Goods End */
%>
                                                    <select id="idcombodetail" name="namecombodetail<%=tblListBoxMaster.getTenderListId()%>" class="formTxtBox_1">
                                                        <%  /* Dohatec: ICT-Goods Start */
                                                            if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                                for(Object[] obj :currencyObjectList){
                                                                    String selected = "";
                                                                    if(obj[3].toString().equalsIgnoreCase("BTN")){selected="selected='selected'";} //BTN is the default currency
                                                                    out.print("<option value='" + obj[2] + "' " + selected + ">" + obj[3] + "</option>");
                                                                }
                                                            }   /* Dohatec: ICT-Goods End */
                                                            else {                                                                
                                                                for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                    String selectedOption = "";
                                                                    if("Yes".equalsIgnoreCase(tblTenderListDetail.getIsDefault())){
                                                                        selectedOption = "selected='selected'";
                                                                    }
                                                                    out.print("<option value='" + tblTenderListDetail.getItemValue() + "' " + selectedOption + ">" + tblTenderListDetail.getItemText() + "</option>");
                                                                }
                                                            }
                                                        %>
                                                    </select>
<%
                                                }
                                            }
%>
                                    </td>
<%
                                            cells = null;
                                        }
                                    }
%>
                                </tr>
<%
                                }
                            }
                        }
%>
                            </tbody>
                        </table>
                    </div>
                    <% if (tableFooter != null && !"".equals(tableFooter.trim())) {%>
                    <table width="100%" class="tableHead_1 t_space">
                        <tr>
                            <td class="ff">Table Footer : </td>
                            <td style="font-weight: normal; line-height: 1.75"><%= tableFooter%></td>
                        </tr>
                    </table>
                    <%}%>
                <% if (frmFooter != null && !"".equals(frmFooter.trim())) {%>
                <table width="100%" class="tableHead_1 t_space">
                    <tr>
                        <td width="100" class="ff">Form Footer : </td>
                        <td style="font-weight: normal; line-height: 1.75"><%= frmFooter%></td>
                    </tr>
                </table>
                <%}%>

                <div style="overflow: auto;width: 100%">
                    <div class="tableHead_1 t_space">Existing Formula(s)</div>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th style="text-align:center; width:4%;">Sl. No.</th>
                            <th style="text-align:left;">Applied to Column</th>
                            <th style="text-align:left;">Formula</th>
                        </tr>
<%
                    short j = 0;
                    java.util.List<TblTenderFormula> tblFormulas = frmView.getTableFormulas(tbl.getTenderTableId());
                    if (tblFormulas != null) {
                        if (tblFormulas.size() > 0) {
                            for (TblTenderFormula ttf : tblFormulas) {
                                j++;
%>
                        <tr>
                            <td style="text-align:center;"><%= j%></td>
                            <td style="text-align:left;"><%=arrColName[ttf.getColumnId() - 1]%></td>
                            <td style="text-align:left;">
<%
                                String tempFormula = ttf.getFormula();
                                boolean wordOrTotal = false;
                                String strTemp = "";
                                char[] cArray = tempFormula.toCharArray();

                                if (cArray[0] == 'W' || cArray[0] == 'T') {
                                    wordOrTotal = true;
                                }

                                for (int c = 0; c < cArray.length; c++) {
                                    try {
                                        // out.println("cArray[" + c + "] : " + cArray[c] + " isDigit : " + Character.isDigit(cArray[c]) + "<br/>");
                                        if (cArray[c] == 'N') {
                                            int cnt = 0;
                                            for (int cc = c; cc < cArray.length; cc++) {
                                                if (cArray[cc] == '+' || cArray[cc] == '-' || cArray[cc] == '*' || cArray[cc] == '/' || cArray[cc] == '(' || cArray[cc] == ')' || cArray[cc] == 'p') {
                                                    out.println(cArray[cc]);
                                                    break;
                                                } else {
                                                    cnt++;
                                                    out.println(cArray[cc]);
                                                }
                                            }
                                            c = c + cnt;
                                        } else {
                                            if (Character.isDigit(cArray[c])) {
                                                int cnt = 0;
                                                String temp = "";
                                                for (int cc = c; cc < cArray.length; cc++) {
                                                    if (cArray[cc] == '+' || cArray[cc] == '-' || cArray[cc] == '*' || cArray[cc] == '/' || cArray[cc] == '(' || cArray[cc] == ')' || cArray[cc] == 'p') {
                                                        cnt--;
                                                        break;
                                                    } else {
                                                        cnt++;
                                                        temp = temp + cArray[cc];
                                                    }
                                                }
                                                int t = Integer.parseInt(temp);
                                                out.println(arrColName[t - 1]);
                                                c = c + cnt;
                                            } else {

                                                if ("WORD".equalsIgnoreCase(strTemp)) {
                                                    wordOrTotal = false;
                                                    out.println("Convert into Words");
                                                    strTemp = "";
                                                } else if ("TOTAL".equalsIgnoreCase(strTemp)) {
                                                    wordOrTotal = false;
                                                    out.println("Grand Total of");
                                                    strTemp = "";
                                                }


                                                if (wordOrTotal) {

                                                    strTemp = strTemp + cArray[c];

                                                } else {

                                                    out.println(cArray[c]);
                                                }
                                            }
                                        }
                                    } catch (Exception ex) {
                                        LOGGER.error(" ex " + ex.toString());
                                    }
                                }
%>
                            </td>
                        </tr>

<%
                                ttf = null;
                            }
                        } else {
%>
                        <tr>
                            <td colspan="3" style="text-align: center">
                                No Records Found
                            </td>
                        </tr>
<%
                        }
                        tblFormulas = null;
                    } else {
%>
                        <tr>
                            <td colspan="4">
                                No Records Found
                            </td>
                        </tr>
<%
                    }
%>
                    </table>
                </div>
<%
                    tbl = null;
                    if (tblColumnsDtl != null) {
                        tblColumnsDtl = null;
                    }
                    if (tblCellsDtl != null) {
                        tblCellsDtl = null;
                    }
                    fillBy = null;
                    arrColId = null;
                    arrColName = null;
                }
%>

            </div>
            <!--Middle Content Table End-->
            <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <% }%>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
<%
                if (tblTenderTables != null) {
                    tblTenderTables = null;
                }
                if (frmView != null) {
                    frmView = null;
                }
                if (frmDtl != null) {
                    frmDtl = null;
                }
%>
</html>