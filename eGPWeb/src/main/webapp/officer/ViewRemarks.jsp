<%-- 
    Document   : ViewRemarks
    Created on : Aug 16, 2011, 12:05:50 PM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsInvRemarks"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
         %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Remarks</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
       </head>
       <body>
           <%
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                String InvoiceId = "";
                if(request.getParameter("InvoiceId")!=null)
                {
                    InvoiceId = request.getParameter("InvoiceId");
                }
                String wpId = "";
                if(request.getParameter("wpId")!=null)
                {
                    wpId = request.getParameter("wpId");
                }
          %>
          <div class="dashboard_div">
            <%--<%@include  file="../resources/common/AfterLoginTop.jsp"%>--%>
            <div class="contentArea_1">
            <div class="pageHead_1">View Remarks</div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <div class="tableHead_1 t_space" align="left">View Remarks</div>
            <table class="tableList_1" cellspacing="0" width="100%">
            <%
                 AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
                 List<TblCmsInvRemarks> getRemarks = accPaymentService.getRemarksDetails(Integer.parseInt(InvoiceId));
                 if(!getRemarks.isEmpty())
                 {
                     for(int i=0; i<getRemarks.size(); i++)
                     {
            %>
                    <tr>
                    <td class="t-align-center" width="15%">
                        <%=(i+1)%>
                    </td>
                    <td>
                        <%=getRemarks.get(i).getRemarks()%>
                    </td>
                    </tr>
               <%}}%>
            </table>
          </div>
          </div>
            <div>&nbsp;</div>
            <%--<%@include file="../resources/common/Bottom.jsp" %>--%>
       </body>
</html>
