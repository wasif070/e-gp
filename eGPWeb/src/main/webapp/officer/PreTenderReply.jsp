<%-- 
    Document   : PreTenderReply
    Created on : Nov 27, 2010, 6:32:39 PM
    Author     : parag
--%>

<%@page import="java.net.URLDecoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Pre – Tender Meeting Replies</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

         <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <%--<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>--%>
        <%--<script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
       

        <script type="text/javascript">
            $(function(){
                $('#btnSubmit').click(function(){
                    var flag=true;
                    if($('#cmbPostQuery').val()=='0')
                    {
                        $('#SPErr').html('Please select Action');
                        //return false;
                        flag=false;
                    }

                    if($('#cmbPostQuery').val()=='Reply')
                    {$('#SPErr').html('');
                        if($('#txtReply').val()=='')
                        {
                            $('#SPReply').html('Please enter Response');
                            flag=false;
                        }
                        else
                        {
                            if($('#txtReply').val().length<=1000)
                            {
                                $('#SPReply').html('');
                            }
                            else
                            {
                                $('#SPReply').html('Maximum 1000 characters are allowed');
                                flag=false;
                            }
                        }
                    }
                    
                    if(flag==false)
                    {
                        return false;
                    }
                });
            });
        </script>

    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
                <%
                            int userId = 0;
                            byte suserTypeId = 0;
                            int tenderId = 1;
                            int queryId = 1;
                            int cuserId = 0;

                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            if (request.getParameter("queryId") != null) {
                                queryId = Integer.parseInt(request.getParameter("queryId"));
                            }
                            if (request.getParameter("cuserId") != null) {
                                cuserId = Integer.parseInt(request.getParameter("cuserId"));
                            }

                            SPTenderCommonData getQueryText = preTendDtBean.getDataFromSP("GetQueryText", queryId, 0).get(0);

                %>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">
                            Pre – Tender Meeting Replies<span style="float: right;"><a class="action-button-goback" href="PreTenderMeeting.jsp?tenderid=<%=tenderId%>">Go back to Dashboard</a></span>
                        </div>
                        <%
                                    // Variable tenderId is defined by u on ur current page.
                                    pageContext.setAttribute("tenderId", tenderId);
                        %>

                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div>&nbsp;</div>
                        <% pageContext.setAttribute("tab", 4);%>
                        <jsp:include page="officerTabPanel.jsp" >
                            <jsp:param name="tab" value="4" />
                        </jsp:include>

                        <div>&nbsp;</div>

                        <form method="post" id="frmPreTendQuery" action="<%=request.getContextPath()%>/PreTendQuerySrBean?action=replyQuery">
                            <div class="tabPanelArea_1">
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <td width="16%" class="t-align-left ff">Query :</td>
                                        <td width="84%" class="t-align-left"><%=URLDecoder.decode(getQueryText.getFieldName2())%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Download Reference Document : </td>
                                        <td class="t-align-left"><a href="<%=request.getContextPath()%>/tenderer/PreTenderQueryDocUpload.jsp?tenderId=<%=tenderId%>&queryId=<%=queryId%>&userId=<%=cuserId%>&actionType=download&docType=Prebid" target="_blank">Download</a></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Select Post Query Status &nbsp;<span class="mandatory">*</span> : </td>
                                        <td class="t-align-left">
                                            <select id="cmbPostQuery" name="cmbPostQuery" class="formTxtBox_1" onclick="checkStatus();" >
                                                <option value="0" > --- Select Action --- </option>
                                                <option id ="Reply" value="Reply" >Response</option>
                                                <option value="Hold" >Hold</option>
                                                <option value="Ignore" >Ignore</option>
                                            </select>
                                            <span id="SPErr" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                </table>
                                <div id="divReply" style="display:none" >
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <td width="16%" class="t-align-left ff">Rephrase Query :</td>
                                            <td width="84%" class="t-align-left">
                                                <label>
                                                    <input type="checkbox" name="checkbox" id="checkbox" onclick="checkRepQuery();" />
                                                </label>&nbsp;&nbsp;
                                                <label id="lblRepQuery" style="display:none" >
                                                    <textarea name="txtRepQuery" rows="3" class="formTxtBox_1" id="txtRepQuery" style="width:650px;"></textarea>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="16%" class="t-align-left ff">Response :</td>
                                            <td width="84%" class="t-align-left">
                                                <label>
                                                    <textarea name="txtReply" rows="3" class="formTxtBox_1" id="txtReply" style="width:675px;"></textarea>
                                                    <span id="SPReply" class="reqF_1"></span>
                                                </label>          </td>
                                        </tr>
                                        <tr class="tableList_1">
                                            <td class="t-align-left ff">Reference Document :</td>
                                            <td class="t-align-left"><a href="#" onClick="window.open('PreTenderReplyUpload.jsp?queryId=<%=queryId%>&tenderId=<%=tenderId%>&docType=Prebid','_blank','width=900,height=500,toolbar=no,menubar=no,location=no,titlebar=no',true);">Upload</a></td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="docData">
                                </div>
                                <div>&nbsp;</div>
                                <div class="t-align-center">
                                    <label class="formBtn_1">
                                        <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit" />
                                        <input type="hidden" name="hidQueryId" id="hidQueryId" value="<%=queryId%>" />
                                        <input type="hidden" name="hidTenderId" id="hidTenderId" value="<%=tenderId%>" />
                                    </label>
                                </div>
                            </div>
                            <div>&nbsp;</div>

                        </form>
                        <!--Dashboard Content Part End-->
                        <!--Dashboard Footer Start-->
                        <%-- <table width="100%" cellspacing="0" class="footerCss">
                         <tr>
                           <td align="left">e-GP &copy; All Rights Reserved
                             <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
                           <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
                         </tr>
                         </table>--%>
                        <!--Dashboard Footer End-->
                        <%@include file="../resources/common/Bottom.jsp" %>
                    </div>
                </div>
            </div>
        </div>
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<form id="form2" method="post">
</form>
<script>
    function checkStatus(){
        var responseDocs;
        var flag = false;
        if($('#cmbPostQuery').val() == "Reply"){
            $('#divReply').show();
        }else{
            $('#divReply').hide();
            var vCount = $('#tblresponseDoc').children()[0].children.length - 1;
            if(vCount > 1)
            {
                   flag = true;
            }
            if(vCount  == '1')
            {
                $('#tblresponseDoc tr').each(function() {
                    responseDocs = $(this).find("td:first").html();
                });
                if(responseDocs != 'No records found.')
                {
                    flag = true;
                }
            }
            if(flag)
            {
                document.getElementById("Reply").selected=true;
                jAlert("If any document is uploaded which is no more required then please remove the same.","Pre – Tender Meeting Replies",function(RetVal) {
                });
                $('#divReply').show();
                return false;
            }
        }
    }

    function checkRepQuery(){
        if(document.getElementById("checkbox").checked){
            $('#lblRepQuery').show();
        }else{
            $('#lblRepQuery').hide();
        }
    }

    function getDocData(){
    <%--$.ajax({
     url: "<%=request.getContextPath()%>/PreTendQuerySrBean?tenderId=<%=tenderId%>&queryId=<%=queryId%>&action=docData&docAction=PreTenderReplyDocs",
     method: 'POST',
     async: false,
     success: function(j) {
         document.getElementById("docData").innerHTML = j;
     }
 });--%>
         $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,queryId:<%=queryId%>,action:'docData',docAction:'PreTenderReplyDocs'}, function(j){
             document.getElementById("docData").innerHTML = j;
         });
     }
     getDocData();

     function downloadFile(docName,docSize,tender){
    <%--$.ajax({
             url: "<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download",
             method: 'POST',
             async: false,
             success: function(j) {
             }
     });--%>
             document.getElementById("form2").action="<%=request.getContextPath()%>/PreTenderReplyUploadServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download";
             document.getElementById("form2").submit();
         }

         function deleteFile(docName,docId,tender){
    <%--$.ajax({
           url: "<%=request.getContextPath()%>/PreTenderReplyUploadServlet?docName="+docName+"&docId="+docId+"&tender="+tender+"&funName=remove",
           method: 'POST',
           async: false,
           success: function(j) {
               jAlert("Document Deleted."," Delete Document ", function(RetVal) {
               });
           }
   });--%>

         $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
            if(r == true){
               $.post("<%=request.getContextPath()%>/PreTenderReplyUploadServlet", {docName:docName,docId:docId,tender:tender,funName:'remove'}, function(j){
                   jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                        getDocData();
                   });
               });
            }else{
                 getDocData();
            }
         });

       }
       
</script>
