<%-- 
    Document   : TER1
    Created on : Apr 14, 2011, 10:31:01 AM
    Author     : TaherT
--%>
<%--
Modified functions and add parameter for re-evaluation by dohatec
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="java.util.Collections"%>
<%@page import="org.hibernate.mapping.Collection"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String tenderid = request.getParameter("tenderid");
                    int evalCount=0;
                    String lotId = request.getParameter("lotId");
                    String roundId = "0";
                    if(request.getParameter("rId")!=null){
                        roundId = request.getParameter("rId");
                    }
                    String stat = request.getParameter("stat");//stat value is 'eval'
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    EvalServiceCriteriaService criteriaService = (EvalServiceCriteriaService) AppContext.getSpringBean("EvalServiceCriteriaService");
                    String pNature = commonService.getProcNature(request.getParameter("tenderid")).toString();
                    String repLabel = "Tender";
                    if(pNature.equalsIgnoreCase("Services")){
                        repLabel = "Proposal";
                    }
                    boolean isREOI = commonService.getEventType(request.getParameter("tenderid")).toString().equalsIgnoreCase("REOI");
                    String tenderPaidORFree = commonService.tenderPaidORFree(tenderid, lotId);
                    boolean bole_ter_flag = true;
                    if(request.getParameter("isPDF")!=null && "true".equalsIgnoreCase(request.getParameter("isPDF"))){
                        bole_ter_flag = false;
                        //System.out.println("isPDF"+bole_ter_flag);
                    }
                    if(request.getParameter("evalCount")!=null){
                        evalCount = Integer.parseInt(request.getParameter("evalCount"));
                    }
                    
                    String userId = "";
                    if(bole_ter_flag){
                        if(session.getAttribute("userId") != null){
                            userId = session.getAttribute("userId").toString();
                        }
                    }else{
                        if(request.getParameter("tendrepUserId") != null && !"".equals(request.getParameter("tendrepUserId"))){
                            userId = request.getParameter("tendrepUserId");
                        }
                    }
                    boolean showGoBack = true;
                    if("y".equals(request.getParameter("in"))){
                        showGoBack = false;
                    }
                    //System.out.println(request.getParameter("tendrepUserId"));
                    
           
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=repLabel%> Evaluation Report 1</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <%if(bole_ter_flag){%>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <% } 
                ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                //EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                //String evalCount = evalService.getEvaluationNo(Integer.parseInt(request.getParameter("tenderid").toString()));
                    List<SPCommonSearchDataMore> tendOpeningRpt = creationService.getOpeningReportData("getTenderOpeningRpt", tenderid, lotId,null);//lotid
                                      
                    List<SPCommonSearchDataMore> TECMembers = dataMoreService.getCommonSearchData("getTECMembers", tenderid, lotId,"TER1",roundId,String.valueOf(evalCount)); //lotid ,"TOR1"

                    
                    List<Object[]> ter1Criteria = creationService.getTERCriteria("TER1", pNature);

                    List<SPCommonSearchDataMore> NoteofDissent = creationService.getOpeningReportData("getNoteofDissent", tenderid, lotId,"ter1");
                    List<SPCommonSearchDataMore> pMethodEstCost = dataMoreService.getCommonSearchData("GetPMethodEstCostTender",tenderid);
                    String estCost = pMethodEstCost.get(0).getFieldName2();
                    String estCostStatus = "-";
                    if(estCost!=null)
                    {
                        estCostStatus = (Double.parseDouble(estCost)>0?"Approved":"-");
                    }

                    boolean list1 = true;                    
                    boolean list5 = true;
                    if (tendOpeningRpt.isEmpty()) {
                        list1 = false;
                    } 
                    if (TECMembers.isEmpty()) {
                        list5 = false;
                    }
                    boolean isView = false;
                    boolean isSign = false;
                    boolean isMEM = true;
                    if("y".equals(request.getParameter("isview"))){
                        isView = true;
                    }
                    if("y".equals(request.getParameter("sign"))){
                        isSign = true;
                    }
                    if("y".equals(request.getParameter("ismem"))){
                        isMEM = false;
                    }
                    
                    if(isView)
                   {
                         // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="tenderId";
                        int auditId=Integer.parseInt(request.getParameter("tenderid"));
                        String auditAction="View Tender Evaluation Report 1";
                        String moduleName=EgpModule.Evaluation.getName();
                        String remarks="";
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                   }
        %>
        <div class="tabPanelArea_1">
            <div  id="print_area">
                
            <div class="pageHead_1"><%=repLabel%> Evaluation Report 1 - Eligibility Report
                <span style="float: right;">
                    <%
                        boolean isCommCP = false;
                        List<SPCommonSearchDataMore> commCP = dataMoreService.getCommonSearchData("evaluationMemberCheck", tenderid, userId, "1", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        if (commCP != null && (!commCP.isEmpty()) && (!commCP.get(0).getFieldName1().equals("0"))) {
                            isCommCP = true;
                        }                        
                        commCP=null;
                        if(bole_ter_flag){
                            if(isView){
                    %>
                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                    <span id="svpdftor1">
                    &nbsp;&nbsp;
                    <a class="action-button-savepdf" id="saveASPDF" href="<%=request.getContextPath()%>/TorRptServlet?tenderId=<%=tenderid%>&lotId=<%=lotId%>&action=TER1">Save As PDF</a>
                    </span>
                    <% } %>
                    <%if(showGoBack){%>
                    <%if("cl".equals(request.getParameter("frm"))){%>
                        &nbsp;&nbsp;
                        <a class="action-button-goback" id="goBack" href="Evalclarify.jsp?tenderId=<%=tenderid%>&st=cl&comType=TEC">Go Back to Dashboard</a>
                    <%}else{if(isCommCP){%>
                        &nbsp;&nbsp;
                        <a class="action-button-goback" id="goBack" href="Evalclarify.jsp?tenderId=<%=tenderid%>&st=rp&comType=TEC">Go Back to Dashboard</a>
                    <%}else{%>                        
                        &nbsp;&nbsp;
                        <a id="goBack" class="action-button-goback" href="MemberTERReport.jsp?tenderId=<%=tenderid%>&comType=TEC">Go Back to Dashboard</a>
                    <%} } }}%>
                </span></div>
                
            <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                        pageContext.setAttribute("tab", "6");
                        pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                        pageContext.setAttribute("userId", 0);
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div class="tableHead_1 t_space t-align-center">Eligibility Report</div>
            <form action="<%=request.getContextPath()%>/ServletEvalCertiService?funName=evalTERReportIns&rep=<%=repLabel.charAt(0)%>er1" method="post">
            <%
            

            %>
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 b_space" width="100%">
                <tr>
                    <td width="24%" class="ff">Ministry Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName1());
                                }%></td>
                    <td width="16%" class="ff">Division Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName2());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Organization/Agency Name :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName3());
                                }%></td>
                    <td class="ff">Procuring Entity :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName4());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff"><%=repLabel%> Package No. and Description :</td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName5());
                                }%></td>
                </tr>
                 <%if(!pNature.equals("Services")){%>
                <tr>
                    <td class="ff">Lot No. and Description : </td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName12()+" & "+tendOpeningRpt.get(0).getFieldName6());
                                }%></td>
                </tr>
                <%}%>
            </table>
            <div class="inner-tableHead t_space">Procurement Data</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>

                        <th width="47%">Procurement Type</th>
                        <th width="53%">Procurement Method</th>
                    </tr>
                    <tr>
                        <td class="t-align-center"><%if (list1) {
                                   if (tendOpeningRpt.get(0).getFieldName7().equalsIgnoreCase("NCT")) //Edit by aprojit
                                       out.print("NCB");
                                   else if (tendOpeningRpt.get(0).getFieldName7().equalsIgnoreCase("ICT"))
                                       out.print("ICB");
                                }%></td>
                       <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName9());
                                }%></td>
                    </tr>
                </tbody></table>

            <div class="inner-tableHead t_space">Procurement Plan</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>
                        <th width="17%">Approval<br>
                            Status</th>
                        <th width="31%">Budget Type</th>
                        <%if(!pNature.equalsIgnoreCase("Services")){%>
                        <th width="14%">Approval Status
                            of<br>
                            Official Estimates</th>
                        <%}%>
                    </tr>
                    <tr>
                        <td class="t-align-center">Approved</td>
                        <td class="t-align-center"><%if (list1) {
                                   if(tendOpeningRpt.get(0).getFieldName8().equalsIgnoreCase("Development")) //Edit by aprojit
                                       out.print("Capital");
                                   else if(tendOpeningRpt.get(0).getFieldName8().equalsIgnoreCase("Revenue"))
                                       out.print("Recurrent");
                                   else
                                       out.print(tendOpeningRpt.get(0).getFieldName8());
                                }%></td>
                        <%if(!pNature.equalsIgnoreCase("Services")){%>
                        <td class="t-align-center"><%=estCostStatus%></td>
                        <%}%>
                    </tr>
                </tbody></table>
            <% if(pNature.equals("Services")){
                if(isREOI){
                    Map<String,String> userList = new HashMap<String, String>();
                    List<Object[]> biddersName = criteriaService.getFinalSubBidders(tenderid);
                     String cpuserId = null;
                    List<SPCommonSearchDataMore> memusersId = dataMoreService.geteGPDataMore("getMemUserIdMadeEval",tenderid);
                    if(memusersId!=null && !memusersId.isEmpty()){
                        cpuserId = memusersId.get(0).getFieldName1();
                    }else{
                        cpuserId = session.getAttribute("userId").toString();
                    }
            %>
                <div class="inner-tableHead t_space">Evaluation Criteria</div>
                <table class="tableList_1" cellspacing="0" width="100%">
                
                <tr>
                    <td class="t-align-center" colspan="3">&nbsp;</td>
                    <th class="t-align-center" colspan="<%=biddersName.size()%>">Rating</th>
                </tr>
                <tr>
                    <th class="t-align-center">Form Name</th>
                    <th class="t-align-center">Criteria</th>
                    <th class="t-align-center">Sub Criteria</th>
                    <%
                    int jk=0;
                                for (Object[] bidder : biddersName) {
                                    String name = bidder[1].toString();
                                    if ("-".equals(name)) {
                                        name = bidder[2].toString() + " " + bidder[3].toString();
                                    }
                                    userList.put(bidder[0].toString(), name);
                                    out.print("<th class='t-align-center'>" + name + "</th>");
                                }                                            
                            //0 esd.maxMarks ,1 esd.actualMarks,2 esd.ratingWeightage,3 esd.ratedScore,
                            //4 tf.tenderFormId,5 tf.formName,6 esc.subCriteria,7 esd.bidId
                            for (Object formId : criteriaService.getAllFormId(tenderid)) {
                                List<Object[]> list = criteriaService.evalServiceRepData(formId.toString(), cpuserId, tenderid,evalCount); //change by dohatec for re-evaluation
                               // double secCount=0;
                                for (Object[] data : list) {
                                    jk++;
                if(Math.IEEEremainder(jk,2)==0) {
                        %>
                        <tr class="bgColor-Green">
                         <% } else { %>
                        <tr>
                        <% } %>                                        
                    <td><%
                               if(true || !("Work plan".equals(data[2].toString())||
                                    "Organization and Staffing".equals(data[2].toString()))){
                                   out.print(data[1]);
                               } else {
                                   out.print("");
                               }
                        %></td>
                     <td><%
                               if(true || !("Work plan".equals(data[2].toString())||
                                    "Organization and Staffing".equals(data[2].toString()))){
                                   out.print(data[5]);
                               } else {
                                   out.print("");
                               }
                        %></td>
                    <td><%out.print(data[2]);%></td>                    
                    <%
                           for (Object[] bidder : biddersName) {
                               StringBuilder htmlString = new StringBuilder();
                               htmlString.append("<td>");                               
                               List<Object[]> eachData = criteriaService.evalServiceRepBidderData(tenderid, formId.toString(), cpuserId, bidder[0].toString(), data[4].toString(),evalCount); //change by dohatec for re-evaluation
                               if (eachData.size() > 1) {
                                   if(isREOI){
                                       int bidCount = eachData.size();
                                       double rating = 0.0;
                                       for (Object[] thisData : eachData) {
                                           /*if (thisData[1].toString().equals("Very Poor")) {
                                           } else if (thisData[1].toString().equals("Poor")) {
                                               rating+=0.4;
                                           } else if (thisData[1].toString().equals("Good" )) {
                                               rating+=0.7;
                                           } else if (thisData[1].toString().equals("Very Good")){
                                               rating+=0.9;
                                           } else if (thisData[1].toString().equals("Excellent")){
                                               rating+=1;
                                           }*/
                                           if (thisData[1].toString().equals("Poor")) {
                                               rating+=0.4;
                                           }else if (thisData[1].toString().equals("Average")) {
                                               rating+=0.5;
                                           }else if (thisData[1].toString().equals("Satisfactory")) {
                                               rating+=0.7;
                                           }else if (thisData[1].toString().equals("Most Satisfactory")) {
                                               rating+=0.8;
                                           } else if (thisData[1].toString().equals("Good")) {
                                               rating+=0.9;
                                           } else if (thisData[1].toString().equals("Very Good")){
                                               rating+=0.95;
                                           } else if (thisData[1].toString().equals("Excellent")){
                                               rating+=1;
                                           }
                                       }
                                       //System.out.println("--> "+rating);
                                       //System.out.println("--| "+rating/bidCount);
                                       String rateValue = null;
                                       /*if (rating/bidCount == 0) {
                                           rateValue = "Very Poor";
                                       } else if (rating/bidCount <= 0.4) {
                                           rateValue = "Poor";
                                       } else if (rating/bidCount > 0.4 && rating/bidCount <= 0.7) {
                                           rateValue = "Good";
                                       } else if (rating/bidCount > 0.7 && rating/bidCount <= 0.9) {
                                           rateValue = "Very Good";
                                       } else if (rating/bidCount > 0.9 && rating/bidCount <= 1) {
                                           rateValue = "Excellent";
                                       }*/
                                       if (rating/bidCount <= 0.4) {
                                        rateValue = "Poor";
                                       } else if (rating/bidCount > 0.4 && rating/bidCount <= 0.5) {
                                           rateValue = "Average";
                                       }else if (rating/bidCount > 0.5 && rating/bidCount <= 0.7) {
                                           rateValue = "Satisfactory";
                                       }else if (rating/bidCount > 0.7 && rating/bidCount <= 0.8) {
                                           rateValue = "Most Satisfactory";
                                       }else if (rating/bidCount > 0.8 && rating/bidCount <= 0.9) {
                                           rateValue = "Good";
                                       } else if (rating/bidCount > 0.9 && rating/bidCount <= 0.95) {
                                           rateValue = "Very Good";
                                       } else if (rating/bidCount > 0.95 && rating/bidCount <= 1) {
                                           rateValue = "Excellent";
                                       }
                                       htmlString.append(rateValue);
                                   }else{
                                       double sum = 0;
                                       int bidCount = eachData.size();
                                       double rating = 0;
                                       double total = Double.parseDouble(data[3].toString());
                                       for (Object[] thisData : eachData) {
                                           sum += Double.parseDouble(thisData[0].toString());
                                       }
                                       sum = sum / bidCount;
                                       if(total!=0){
                                            rating = sum / total;
                                       }
                                       String rateValue = null;
                                       /*if (rating == 0) {
                                           rateValue = "Very Poor";
                                       } else if (rating <= 0.4) {
                                           rateValue = "Poor";
                                       } else if (rating > 0.4 && rating <= 0.7) {
                                           rateValue = "Good";
                                       } else if (rating > 0.7 && rating <= 0.9) {
                                           rateValue = "Very Good";
                                       } else if (rating > 0.9 && rating <= 1) {
                                           rateValue = "Excellent";
                                       }*/
                                       if (rating <= 0.4) {
                                        rateValue = "Poor";
                                       } else if (rating > 0.4 && rating <= 0.5) {
                                           rateValue = "Average";
                                       }else if (rating > 0.5 && rating <= 0.7) {
                                           rateValue = "Satisfactory";
                                       }else if (rating > 0.7 && rating <= 0.8) {
                                           rateValue = "Most Satisfactory";
                                       }else if (rating > 0.8 && rating <= 0.9) {
                                           rateValue = "Good";
                                       } else if (rating > 0.9 && rating <= 0.95) {
                                           rateValue = "Very Good";
                                       } else if (rating > 0.95 && rating <= 1) {
                                           rateValue = "Excellent";
                                       }
                                       htmlString.append(rateValue);
                                  }
                                  // secCount+=sum;
                               } else if (eachData.size() == 1) {
                                   htmlString.append(eachData.get(0)[1]);
                                   //secCount+=Double.parseDouble(eachData.get(0)[0].toString());
                               }
                               htmlString.append("</td>");
                               out.print(htmlString.toString());
                           }
                    %>
                </tr>
                <%}}%>
            </table>
            <div class="inner-tableHead t_space">Evaluation Status</div>
            <table class="tableList_1" cellspacing="0" width="100%">
                <tr>
                    <th>Name of company of the Consultant/Applicant</th>
                    <th>Status</th>
                </tr>
                <%
                    List<SPCommonSearchDataMore> bidderResult = dataMoreService.geteGPDataMore("evalservicebidderResult",tenderid,(stat.equals("eval")?"evaluation":"reevaluation"),String.valueOf(evalCount)); //change by dohatec for re-evaluation
                    for(SPCommonSearchDataMore userL : bidderResult){
                %>
                <tr>
                    <td><%=userL.getFieldName3()%></td>
                    <td><%=userL.getFieldName1().equals("Pass") ? "Qualified" : "Disqualified"%></td>
                </tr>
                <%}%>
            </table>
                <%}else{
                    List<Object[]> biddersName = criteriaService.getFinalSubBidders(tenderid);
                    Object minScore = criteriaService.getPassingMarksofTender(tenderid);
                    StringBuilder tfootData1 = new StringBuilder();
                    StringBuilder tfootData2 = new StringBuilder();
                    StringBuilder tfootData3 = new StringBuilder();                    
                    String cpuserId = null;
                    String cppuserId = null;
                    List<SPCommonSearchDataMore> memusersId = dataMoreService.geteGPDataMore("getMemUserIdMadeEval",tenderid);
                    if(memusersId!=null && !memusersId.isEmpty()){
                        cpuserId = memusersId.get(0).getFieldName1();
                        cppuserId = memusersId.get(0).getFieldName2();
                    }else{
                        cpuserId = session.getAttribute("userId").toString();
                        cppuserId = session.getAttribute("userId").toString();
                    }
            %>
            <div class="inner-tableHead t_space">Criteria</div>
            <table class="tableList_1" cellspacing="0" width="100%" id="marksTab">
                <tr>
                    <th class="t-align-center">Form Name</th>
                    <th class="t-align-center">Criteria</th>
                    <th class="t-align-center">Sub Criteria</th>
                    <th class="t-align-center">Points</th>
                    <%
                                for (Object[] bidder : biddersName) {
                                    String name = bidder[1].toString();
                                    if ("-".equals(name)) {
                                        name = bidder[2].toString() + " " + bidder[3].toString();
                                    }
                                    out.print("<th class='t-align-center'>" + name + "</th>");
                                }
                    %>
                    <!--                    -->
                </tr>
                <%
                             double mainTotal=0;
                            //0 esd.maxMarks ,1 esd.actualMarks,2 esd.ratingWeightage,3 esd.ratedScore,
                            //4 tf.tenderFormId,5 tf.formName,6 esc.subCriteria,7 esd.bidId
                             int jk=0;
                            for (Object formId : criteriaService.getAllFormId(tenderid)) {
                                List<Object[]> list = criteriaService.evalServiceRepData(formId.toString(), cpuserId, tenderid,evalCount);
                               // double secCount=0;
                                for (Object[] data : list) {
                                    jk++;
                if(Math.IEEEremainder(jk,2)==0) {
                        %>
                        <tr class="bgColor-Green">
                         <% } else { %>
                        <tr>
                        <% } %>
                    <td><%
                               if(true || !("Work plan".equals(data[2].toString())||
                                    "Organization and Staffing".equals(data[2].toString()))){
                                   out.print(data[1]);
                               } else {
                                   out.print("");
                               }
                        %></td>
                    <td><%
                               if(true || !("Work plan".equals(data[2].toString())||
                                        "Organization and Staffing".equals(data[2].toString()))){
                                   out.print(data[5]);
                               } else {
                                   out.print("");
                               }
                        %></td>
                    <td><%out.print(data[2]);%></td>                    
                    <td opt="hide"><%out.print(data[3]);mainTotal+=Double.parseDouble(data[3].toString());%></td>
                    <%
                        for (Object[] bidder : biddersName) {
                           out.print("<td>"+new BigDecimal(criteriaService.getEvalBidderMarks(tenderid, data[0].toString(),cppuserId , bidder[0].toString(),data[4].toString(),evalCount).toString()).setScale(2, 0)+"</td>"); //change by dohatec for re-evaluation
                        }
                    %>
                </tr>
                <%}
                            }                            
                            for (Object[] bidder : biddersName) {
                                  double marks = 0.0;

                                  for(Object data : criteriaService.getEvalBidderTotal(tenderid, cppuserId, bidder[0].toString(), evalCount)){
                                     marks+=Double.parseDouble(data.toString());
                                  }
                                   tfootData1.append("<td class='formSubHead_1'>"+new BigDecimal(String.valueOf(marks)).setScale(2, 0)+"</td>");
                                   tfootData2.append("<td class='formSubHead_1'>"+minScore+"</td>");
                                   if(marks>=Double.parseDouble(minScore.toString())){
                                        tfootData3.append("<td class='formSubHead_1'>Pass</td>");
                                   }else{
                                       tfootData3.append("<td class='formSubHead_1 mandatory'>Fail</td>");
                                   }
                            }
                            out.print("<tr><td class='formSubHead_1'><td class='formSubHead_1'></td><td align='right' class='formSubHead_1'>Total : </td><td class='formSubHead_1'>"+mainTotal+"</td>"+tfootData1.toString()+"</tr>");
                            out.print("<tr><td class='formSubHead_1'></td><td class='formSubHead_1'></td><td align='right' class='formSubHead_1'>Minimum Passing Points : </td><td class='formSubHead_1'></td>"+tfootData2.toString()+"</tr>");
                            out.print("<tr><td class='formSubHead_1'></td><td class='formSubHead_1'></td><td align='right' class='formSubHead_1'>Status(Pass/Fail) : </td><td class='formSubHead_1'></td>"+tfootData3.toString()+"</tr>");
                %>
            </table>
            <%}}else{%>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>
                        <th width="50%">Criteria</th>
                       <%
                       List<Object[]> finalSubmissionUser = creationService.getFinalSubmissionUserForTer2(tenderid, lotId, roundId, String.valueOf(evalCount));
                       //0 tfs.userId,1 tcm.companyId,2 tcm.companyName,3 ttm.firstName,4 ttm.lastName
                           for(Object[] data : finalSubmissionUser){
                              /*
                               String userBidder = null;
                               if(!data[1].toString().equals("1")){
                                   userBidder = data[2].toString();
                               }else{
                                    userBidder = data[3].toString()+" "+data[4].toString();
                               }
 *                          */
                        %>
                        <th>
                        <%=data[2].toString()%>
                        </th>
                        <%
                           }
                       %>
                    </tr>
                    <%
                            List<Object[]> evalRepData = creationService.evalRepData(tenderid, lotId, stat, "TER1",userId,isSign,evalCount);
                            //0. ter.criteriaId,1. ter.criteria
                            for(Object[] data : ter1Criteria){
                             //if(data[1].toString().equals("Tender Validity") && (eventType.equals("PQ")||eventType.equals("1st stage of TSTM"))){
                             if(data[1].toString().equals(repLabel+" Validity") && (list1 && tendOpeningRpt.get(0).getFieldName20().equals("null"))){
                                 continue;
                             }
                             if(data[1].toString().equals(repLabel+" Security") && tenderPaidORFree.equals("free")){
                                 continue;
                             }
                    %>
                    <tr>
                        <td class="table-description" ><%=data[1]%></td>
                        <%for(Object[] data2 : finalSubmissionUser){%>
                        <td class="t-align-center">
                            <%
                                String isYes = "";
                                String isNo = "";
                                String isNA = "";
                                String evalValue=null;
                                //0. et.userId,1. et.criteriaId,2. et.value
                                for(Object[] objects : evalRepData){
                                    boolean tempbool = data2[0].toString().equals(objects[0].toString()) && data[0].toString().equals(objects[1].toString());
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("yes")){
                                        isYes = "selected";
                                        evalValue = "Yes";
                                    }
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("no")){
                                        isNo = "selected";
                                        evalValue = "No";
                                    }
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("na")){
                                        isNA = "selected";
                                        evalValue = "NA";
                                    }
                                }
                                
                                if(isView){
                                    out.print(evalValue);                                                                    
                                }else{
                            %>
                            <select name="critValue" class="formTxtBox_1">
                                <option value="Yes_<%=data2[0]%>_<%=data[0]%>" <%=isYes%>>Yes</option>
                                <option value="No_<%=data2[0]%>_<%=data[0]%>" <%=isNo%>>No</option>
                                <option value="NA_<%=data2[0]%>_<%=data[0]%>" <%=isNA%>>NA</option>
                            </select>
                            <%}%>
                        </td>
                        <%
                            isYes=null;
                            isNo=null;
                            isNA=null;
                             evalValue=null;
                            }
                        %>
                    </tr>
                    <%}%>
                </tbody></table>
                <%}%>
            <%if(!"y".equals(request.getParameter("config")) && isMEM){%>
            <%if(NoteofDissent!=null && (!NoteofDissent.isEmpty())){%>
            <div class="inner-tableHead t_space">Note of Dissent:</div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <th><%=repLabel.charAt(0)%>EC Member</th>
                    <th>Note of Dissent</th>
                </tr>
                <%for(SPCommonSearchDataMore nod : NoteofDissent){%>
                <tr>
                    <td><%=nod.getFieldName1()%></td>
                    <td><%=nod.getFieldName2()%></td>
`               </tr>
                <%}%>
            </table>
            <%}%>
            <div class="c_t_space atxt_1" style="color: red;">
<!--                I do hereby declare and confirm that I have no business or other links to any of the competing Tenderers.<br />-->
                <br />

                The Evaluation Committee certifies that the examination and evaluation has followed the requirements of the Act, the Rules made there under and the terms and conditions of the prescribed Application, Tender or Proposal Document and that all facts and information have been correctly reflected in the Evaluation Report and, that no substantial or important information has been omitted. </div>

            <div class="inner-tableHead t_space"><%=repLabel.charAt(0)%>EC Members</div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <td class="tableHead_1" colspan="5" ><%=repLabel.charAt(0)%>EC Members</td>
                </tr>
                <tr>
                    <th width="20%" >Name</th>
                    <%
                    for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                        if(dataMore.getFieldName5().equals(userId)){
                         if(dataMore.getFieldName6().equals("-")){
                             String valChk="";
                             if(!isCommCP){
                                valChk = "&nDis=y";
                             }
                   %>
                    <a href="TORSigningApp.jsp?id=<%=dataMore.getFieldName7()%>&uid=<%=dataMore.getFieldName5()%>&tid=<%=tenderid%>&lotId=<%=lotId%>&stat=<%=stat%>&rId=<%=roundId%>&rpt=ter1<%=valChk%>&parentLink=924">
                            <%=dataMore.getFieldName1()%>
                        </a>
                   <%
                        }else{
                             out.print(dataMore.getFieldName1());
                        }

                    }else{
                        out.print(dataMore.getFieldName1());
                    }
                        out.print("</td>");
                    }
                  %>
               </tr>
                <tr>
                    <th width="20%">Committee Role</th>
                    <%
                    int svpdfter1 = 0;
                    for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                            if (dataMore.getFieldName2().equals("cp")) {
                            out.print("Chairperson");
                        } else if (dataMore.getFieldName2().equals("ms")) {
                            out.print("Member Secretary");
                        } else if (dataMore.getFieldName2().equals("m")) {
                            out.print("Member");
                            }
                        out.print("</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Designation</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName3()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">PE Office</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName4()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Signed <%=repLabel%> Evaluation Report 1 On</th>
                    <%for(SPCommonSearchDataMore dataMore : TECMembers) {if(dataMore.getFieldName6().equals("-")){svpdfter1++;}out.print("<td>"+dataMore.getFieldName6()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Comments</th>
                    <%for(SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName8()+"</td>");}%>
                </tr>
            </table>
                <%if(svpdfter1!=0){%>
                    <script type="text/javascript">
                        $('#svpdftor1').hide();
                    </script>
                <%}%>
                <%}%>
                <input type="hidden" value="<%=tenderid%>" name="tId">
                <input type="hidden" value="<%=lotId%>" name="lId">
                <input type="hidden" value="<%=stat%>" name="stat">
                <input type="hidden" value="TER1" name="rType">
                <input type="hidden" value="<%=roundId%>" name="rId">
                <input type="hidden" value="<%if(isCommCP){out.print("cp");}else{out.print("m");}%>" name="role">
                <%if(!isView && bole_ter_flag){%>
                <div class="t-align-center t_space">
                    <label class="formBtn_1" accesskey="">
                        <input type="submit" value="Save"/>
                    </label>
                </div>
                <%}%>
            </form>
            </div>
        </div>
            <%if(pNature.equals("Services") && !isREOI){%>
            <script type="text/javascript">
                $('#marksTab td[opt="hide"]').each(function() {
                    var ctd = this.innerHTML;
                    if(ctd=='0'){
                        $(this).parent().hide();
                    }
                });
            </script>
            <%}%>
        <%if(bole_ter_flag){%>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <% } %>
    </body>
    <%
    %>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
    $(document).ready(function() {

        $("#print").click(function() {
            //alert('sa');
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        //alert(options);
        $('#print').hide();
        $('#saveASPDF').hide();
        $('#goBack').hide();
        $('#print_area').printElement(options);
        $('#print').show();
        $('#saveASPDF').show();
        $('#goBack').show();
        //$('#trLast').hide();
    }

    </script>
</html>
