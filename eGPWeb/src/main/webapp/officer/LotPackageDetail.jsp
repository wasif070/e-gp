<%--
    Document   : LotPackageDetail
    Created on : Nov 23, 2010, 5:26:54 PM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderOpeningServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date,java.text.DateFormat"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Opening Process</title>
        <% String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/FormulaCalculation.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/Add.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/GetHash.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script type="text/javascript" >
            function checkTender(){
                jAlert("There is no Opening Committe"," Alert ", "Alert");
            }
            function checkdefaultconf(){
                jAlert("Default workflow configuration hasn't crated"," Alert ", "Alert");
            }
            function checkDates(){
                jAlert(" Please configure dates"," Alert ", "Alert");
            }
        </script>
    </head>

    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <%
                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    TenderOpeningServiceImpl openingServiceImpl = (TenderOpeningServiceImpl) AppContext.getSpringBean("TenderOpeningService");
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String tenderId = "";
                    String strLotId = "0";
                    String strProcurementNature = "";
                    String  strReport1SingStatus = "0"; 
                    String  strReport2SingStatus = "0";
                    String strDocType = "";
                    String strPath = "";
                    String sendTo = "";
                    if (request.getParameter("tenderid") != null) {
                        tenderId = request.getParameter("tenderid");
                    }
                    String eventType = commonService.getEventType(tenderId).toString();
                    List<Object> rtype = openingServiceImpl.getAutoGenerateReportType(Integer.parseInt(tenderId));
                    long countDecrptedData = openingServiceImpl.getDecryptedBidderCount(Integer.parseInt(tenderId));
                    String rptMessage = "";
                    if (request.getParameter("rptMessage") != null) {
                                    rptMessage = request.getParameter("rptMessage");
                                    if (rptMessage.equalsIgnoreCase("TOR")) {
                                        rptMessage = "BOR Report Generated Successfully.";
                                    } else if (rptMessage.equalsIgnoreCase("TER")) {
                                        rptMessage = "TER Report Generated Successfully.";
                                    } else if (rptMessage.equalsIgnoreCase("TORTER") && countDecrptedData == 0) {
                                        rptMessage = "Report template generated.";
                                    } else if (rptMessage.equalsIgnoreCase("TORTER") && countDecrptedData > 0) {
                                        rptMessage = "Decrypted successfully and reports generated.";
                                    } else if (rptMessage.equalsIgnoreCase("TORTERFail")) {
                                        rptMessage = "Report template not generate Successfully.";
                                    }
                                }
                    
                   //out.print("count decrypted data :"+countDecrptedData);
                    // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String idType = "tenderId";
                    int auditId = 0;
                    String auditAction = "Verify Mega Mega Hash by Opening Committee Member";
                    String moduleName = EgpModule.Tender_Opening.getName();
                    String remarks = "";

              /*
              // FOR TESTING PURPOSE
              strPath= contextPath+"/OpeningProcessServlet?btnNotifyPE=btnNotifyPE&action=notifype&tenderId=1732";

              if (request.getParameter("msgId")==null){
              response.sendRedirect(strPath);
              }
              */

              /* START : CODE TO PROCESS MEGA HASH VERIFICATION */
                    if (request.getParameter("btnMegaHash") != null) {

                    // Coad added by Dipal for Audit Trail Log.
                    auditId=Integer.parseInt(tenderId);
                    remarks="User Id:"+session.getAttribute("userId")+" has done verify Mega Mega Has for Tender id:"+tenderId;

                    List<SPTenderCommonData> lstMegaHash = tenderCommonService1.returndata("getTenderMegaHashVerifiedStatus", tenderId, null);
                        if (!lstMegaHash.isEmpty() && lstMegaHash.size() > 0) {
                            if ("true".equalsIgnoreCase(lstMegaHash.get(0).getFieldName1())) {
                            // Coad added by Dipal for Audit Trail Log.
                            auditAction="Verify Mega Mega Hash by Opening Committee Member";
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

                            strPath="OpenComm.jsp?tenderid="+request.getParameter("tenderid")+"&msgId=hashsuccess";
                            //response.sendRedirect("OpenComm.jsp?tenderid="+request.getParameter("tenderid")+"&msgId=hashsuccess");
                            } else {
                            // Coad added by Dipal for Audit Trail Log.
                            auditAction="Error in Verify Mega Mega Hash by Opening Committee Member";
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

                            strPath="OpenComm.jsp?tenderid="+request.getParameter("tenderid")+"&msgId=hashfailed";
                            //response.sendRedirect("OpenComm.jsp?tenderid="+request.getParameter("tenderid")+"&msgId=hashfailed");
                        }
                        } else {
                        // Coad added by Dipal for Audit Trail Log.
                        auditAction="Error in Verify Mega Mega Hash by Opening Committee Member";
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

                        strPath="OpenComm.jsp?tenderid="+request.getParameter("tenderid")+"&msgId=hashfailed";
                    }

                    lstMegaHash=null;
                    response.sendRedirect(strPath);
                }
              List<SPTenderCommonData> lstLotsForTOR =
                    tenderCommonService1.returndata("getLoIdLotDesc_ForOpening", tenderId, null);
                /* END : CODE TO PROCESS MEGA HASH VERIFICATION */
%>
        <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
	<div class="contentArea_1">
            <div >
                <%
                if(request.getParameter("hdnFromDecryptPage")!=null){
                    if("1".equalsIgnoreCase(request.getParameter("hdnFromDecryptPage")) && request.getParameter("hdnReferer")!=null){
                        String redirectTo=request.getParameter("hdnReferer");
                        response.sendRedirect(redirectTo);
                    }
                }
                %>
            </div>
            <div class="pageHead_1">Opening Process
                <%if(request.getRequestURL().toString().toLowerCase().contains("lotpackagedetail.jsp")){%>
                <span style="float: right;" ><a href="OpenComm.jsp?tenderid=<%=tenderId%>" class="action-button-goback">Go Back</a></span>
                <%}%>
            </div>

            <%
                // Variable tenderId is defined by u on ur current page.
                pageContext.setAttribute("tenderId", tenderId);
                pageContext.setAttribute("tab", "6");
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
             <div>
            <%@include  file="officerTabPanel.jsp"%>
            </div>
            <div class="tabPanelArea_1">
            <%-- Start: CODE TO DISPLAY MESSAGES --%>

                <%
                            String msg = "";
                            msg = request.getParameter("msg");
                %>

                    <%if ("y".equals(request.getParameter("rshare"))) {
                                out.print("<div class='responseMsg successMsg'>Tender Opening Reports Shared Successfully</div>");
                            }%>
            <% if (msg != null && !msg.contains("not")) {%>
           <br/> <div class="responseMsg successMsg" style="margin-bottom: 12px;">File processed successfully</div>
            <%  } else if (msg != null && msg.contains("not")) {%>
           <br/> <div class="responseMsg errorMsg" style="margin-bottom: 12px;">File has not processed successfully</div>
            <%  }%>

            <%if("y".equals(request.getParameter("ispub"))){%><br/> <div class="responseMsg successMsg">Committee members notified successfully</div><%}%>
            <!--Committee published successfully-->

               <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("approved")){
                            msgTxt="Consent for Opening given successfully";
                        } else if(msgId.equalsIgnoreCase("hashsuccess")){
                            msgTxt="Mega Mega Hash verified successfully";
                        } else if(msgId.equalsIgnoreCase("extensionrequested")){
                            msgTxt="Opening date and time request submitted successfully";
                        } else if(msgId.equalsIgnoreCase("tenderclosed")){
                            msgTxt="Tender closed successfully";
                        } else if(msgId.equalsIgnoreCase("senttope")){
                            msgTxt="Sent to AU/PA successfully";
                        } else if(msgId.equalsIgnoreCase("sendtocp")){
                            msgTxt="Sent to TEC Chairperson successfully";
                            } else if(msgId.equalsIgnoreCase("penotified")){
                            msgTxt="AU/PA Notified successfully";
                        } else if(msgId.equalsIgnoreCase("penotificationfailed")){
                                            isError = true;
                                            msgTxt = "AU/PA Notification failed";
                        } else if(msgId.equalsIgnoreCase("hashfailed")){
                                            isError = true;
                                            msgTxt = "Mega Mega Hash verification failed";
                        } else if(msgId.equalsIgnoreCase("error")){
                                            isError = true;
                                            msgTxt = "There is some error";
                        } else {
                            msgTxt="";
                        }

                    %>
                   <%if (isError && msgTxt!=""){%>
                   <div class="responseMsg errorMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                   <%} else if (msgTxt!="") {%>
                   <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                   <%}%>
                <%}
                msgId = null;
                msgTxt = null;
                }%>

                <%-- End: CODE TO DISPLAY MESSAGES --%>
            <%
              boolean isTenderOpened = false,
                      isDeclarationTime = false,
                      isDeclarationTimeOver = false,
                      isDeclarationDone = false,
                      isMember = false,
                      isMemberApproved = false,
                      isMemberDecrypter = false,
                      isCommitteeApproved = false,
                      isExtensionApprovalPending = false,
                      isMegaHashVerified = false,
                      isTOCChairPerson = false,
                      isPEUser = false,
                      isLotId = false,
                      isPackage = false,
                      isLot = false,
                      showVerify = false,
                      showDecryptAll=false,
                      isMultiLots=false,
                      isOpeningCommitteeFormed=false,
                      isForeignCurrencyExchangeRateAdded = false, //dohatec: ICT
                      isProcurementTypeIct = false, //dohatec: ICT
                      isTenPub=false;
                boolean allowTenderClose = false,
                        isTenderClosed = false,
                        isSentToPEOfficer = false,
                        isSentToTECChairPerson = false,
                        allowTenderSharing=false,
                        isExtensionLinkSeen=false,
                        isBidExists=false,
                        grandSumLink = true;
              int committeeMemberCnt = 0,
                  committeeApprovedMemberCnt = 0,
                  minRequirement = 0,
                  memberCnt = 0,
                  ApprovedMemCnt = 0;

              String memberRole = "";

                          String userId = "";


              HttpSession hs = request.getSession();

                if (hs.getAttribute("userId") != null) {
                    userId = hs.getAttribute("userId").toString();
                }

                 /* Start: CODE TO SET CURRENT USER TYPE */
                 if (session.getAttribute("userTypeId") != null) {
                     if ("3".equalsIgnoreCase(session.getAttribute("userTypeId").toString())) {
                         //isPEUser=true; // userType is Officer";
                         String peOfficerUserId = "";
                         List<SPTenderCommonData> lstOfficerUserId = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
                         if (!lstOfficerUserId.isEmpty()) {
                              peOfficerUserId = lstOfficerUserId.get(0).getFieldName1();
                              if (peOfficerUserId.equalsIgnoreCase(userId)) {
                                  isPEUser = true;
                              }
                          }
                         lstOfficerUserId=null;
                         peOfficerUserId=null;
                     }
                 }
                 /* End: CODE TO SET CURRENT USER TYPE */


                        if (request.getParameter("lotId") != null) {
                           if (!"0".equalsIgnoreCase(request.getParameter("lotId")) && !"".equalsIgnoreCase(request.getParameter("lotId")) && !"null".equalsIgnoreCase(request.getParameter("lotId"))){
                                isLotId = true;
                                strLotId=request.getParameter("lotId");
                           } else {
                                //isPackage = true;

                                List<SPTenderCommonData> listTenderTyp = tenderCommonService1.returndata("gettenderlotbytenderid", tenderId, null);

                            if (!listTenderTyp.isEmpty()) {
                                 if ("package".equalsIgnoreCase(listTenderTyp.get(0).getFieldName1())) {
                                    isPackage = true;
                                } else {
                                    isLot = true;
                                }
                            }
                            listTenderTyp=null;
                           }
                        } else {
                            List<SPTenderCommonData> listTenderTyp = tenderCommonService1.returndata("gettenderlotbytenderid", tenderId, null);

                            if (!listTenderTyp.isEmpty()) {
                                 if ("package".equalsIgnoreCase(listTenderTyp.get(0).getFieldName1())) {
                                    isPackage = true;
                                } else {
                                    isLot = true;
                                }
                            }
                            listTenderTyp=null;
                        }
                        boolean grandsumlinkstatus = false;
                        List<SPCommonSearchDataMore> listGrandsumlinkStatus = commonSearchDataMoreService.geteGPData("getGrandSummaryLinkStatus", tenderid, strLotId , null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        if(!listGrandsumlinkStatus.isEmpty()){
                            if("1".equals(listGrandsumlinkStatus.get(0).getFieldName1())){
                                grandsumlinkstatus = true;
                            }
                        }
                        /*START : CODE TO GET TENDER INFORMATIN FOR OPENING PROCESS*/
                            List<SPTenderCommonData> OpeningProcessInfo = tenderCommonService1.returndata("getInfoForOpeningProcess", tenderId, userId);
                                if (!OpeningProcessInfo.isEmpty() && OpeningProcessInfo.size()>0){

                                    if ("yes".equalsIgnoreCase(OpeningProcessInfo.get(0).getFieldName2())){
                                        isTenderOpened = true;
                                    }

                                    if ("yes".equalsIgnoreCase(OpeningProcessInfo.get(0).getFieldName3())){
                                        isDeclarationTime = true;
                                    }

                                    if ("yes".equalsIgnoreCase(OpeningProcessInfo.get(0).getFieldName4())){
                                        isDeclarationTimeOver = true;
                                    }

                                    if ("yes".equalsIgnoreCase(OpeningProcessInfo.get(0).getFieldName5())){
                                        isDeclarationDone = true;
                                    }

                                    if ("yes".equalsIgnoreCase(OpeningProcessInfo.get(0).getFieldName6())){
                                        isMember = true;
                                    }

                                    if ("yes".equalsIgnoreCase(OpeningProcessInfo.get(0).getFieldName7())){
                                        isMemberApproved = true;
                                    }

                                     if ("yes".equalsIgnoreCase(OpeningProcessInfo.get(0).getFieldName10())){
                                        isCommitteeApproved = true;
                                    }

                                    committeeMemberCnt=Integer.parseInt(OpeningProcessInfo.get(0).getFieldName8());
                                    committeeApprovedMemberCnt=Integer.parseInt(OpeningProcessInfo.get(0).getFieldName9());
                                }
                           OpeningProcessInfo=null;
                        /*END : CODE TO GET TENDER INFORMATIN FOR OPENING PROCESS*/

                        /*START : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/
                        List<SPTenderCommonData> lstUserInfo = tenderCommonService1.returndata("getUserInfoForOpeningProcess", tenderId, userId);

                        if (!lstUserInfo.isEmpty() && lstUserInfo.size()>0){

                            memberRole = lstUserInfo.get(0).getFieldName1();

                            if ("Chair Person".equalsIgnoreCase(memberRole)){
                                   isTOCChairPerson = true;
                                }

                            if ("yes".equalsIgnoreCase(lstUserInfo.get(0).getFieldName2())){
                                        isMemberDecrypter = true;
                            }
                        }
                        lstUserInfo=null;
                        /*END : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/

                        /* Dohatec: ICT Start */
                            // 	Procurement Type (i.e. NCT/ICT) retrieving
                            List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", tenderId, null);
                            if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                isProcurementTypeIct = true;
                            }

                            //check at least one foreign currency exchange rate (except BTN) added or not
                            TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                            if(isProcurementTypeIct){
                                isForeignCurrencyExchangeRateAdded = tenderCurrencyService.isAtleastOneCurrencyExchangeRateAdded(Integer.parseInt(request.getParameter("tenderid")));
                            }
                        /* Dohatec: ICT End */

                        List<SPTenderCommonData> lstTenInfo =
                                tenderCommonService1.returndata("getTenderInfoForOpeningProcess", tenderId, null);
                        if (!lstTenInfo.isEmpty() && lstTenInfo.size()>0){
                                if ("yes".equalsIgnoreCase(lstTenInfo.get(0).getFieldName1())){
                                        isExtensionApprovalPending = true;
                                }

                                if ("yes".equalsIgnoreCase(lstTenInfo.get(0).getFieldName2())){
                                        isMegaHashVerified = true;
                                }

                                if ("yes".equalsIgnoreCase(lstTenInfo.get(0).getFieldName3())){
                                        isOpeningCommitteeFormed = true;
                        }

                                 if (!"no".equalsIgnoreCase(lstTenInfo.get(0).getFieldName4())){
                                        isTenPub = true;
                                }

                                if (!"no".equalsIgnoreCase(lstTenInfo.get(0).getFieldName5())){
                                        isBidExists = true;
                                }
                        }
                        lstTenInfo=null;

                       int intEnvelopcnt=0;
                       /* START : CODE TO GET TENDER ENVELOPE COUNT */
                        List<SPTenderCommonData> lstEnvelops =
                                tenderCommonService1.returndata("getTenderEnvelopeCount", tenderId, "0");

                        if(!lstEnvelops.isEmpty()){
                            if(Integer.parseInt(lstEnvelops.get(0).getFieldName1()) > 0){
                                intEnvelopcnt=Integer.parseInt(lstEnvelops.get(0).getFieldName1());
                            }
                            if ("Yes".equalsIgnoreCase(lstEnvelops.get(0).getFieldName2())){
                                isMultiLots=true;
                        }

                            strProcurementNature=lstEnvelops.get(0).getFieldName3();
                        }
                        lstEnvelops=null;
                        /* START : CODE TO GET TENDER ENVELOPE COUNT */

                        String torLotId="0";
                        if("Services".equalsIgnoreCase(strProcurementNature)){
                            strDocType="Proposal";
                        } else {
                            // Below added by Taher
                            torLotId = (!lstLotsForTOR.isEmpty()) ? lstLotsForTOR.get(0).getFieldName1() : "0";
                            strDocType="Tender";
                        }
                        strDocType="Tender/Proposal";

                String listingId="0",
                       tenderFrmCnt="0",
                       decryptedtenderFrmCnt="0",
                       peOfficerUserId="";

                 // Get PE User Id from Tender Id
                 List<SPTenderCommonData> lstOfficerUserId =
                         tenderCommonService.returndata("getPEOfficerUserIdfromTenderId",tenderId,null);

                 if(!lstOfficerUserId.isEmpty()){
                    peOfficerUserId=lstOfficerUserId.get(0).getFieldName1();
                 }
                 lstOfficerUserId=null;
                 //====================
                 // Get Report Sign Status 


                    if("Services".equalsIgnoreCase(strProcurementNature)){
                            List<SPCommonSearchDataMore> listReportSingStatus = commonSearchDataMoreService.geteGPData("getTenderReportSignStatus", tenderid, "TOC" , "TOR1", "0", "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            //temporarly changed committee type "POC" to "TOC" for Services by aprojit.
                           if(!listReportSingStatus.isEmpty()){
                              strReport1SingStatus = listReportSingStatus.get(0).getFieldName1();
                        }
                        } else {
                            List<SPCommonSearchDataMore> listReportSingStatus = commonSearchDataMoreService.geteGPData("getTenderReportSignStatus", tenderid, "TOC" , "TOR1", "0", "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            if(!listReportSingStatus.isEmpty()){
                                strReport1SingStatus = listReportSingStatus.get(0).getFieldName1();
                        }
                       }
               if("Services".equalsIgnoreCase(strProcurementNature)){
                            List<SPCommonSearchDataMore> listReportSingStatus = commonSearchDataMoreService.geteGPData("getTenderReportSignStatus", tenderid, "TOC" , "TOR2", "0", "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            //temporarly changed committee type "POC" to "TOC" for Services by aprojit.
                           if(!listReportSingStatus.isEmpty()){
                              strReport2SingStatus = listReportSingStatus.get(0).getFieldName1();
                        }
                        } else {
                            List<SPCommonSearchDataMore> listReportSingStatus = commonSearchDataMoreService.geteGPData("getTenderReportSignStatus", tenderid, "TOC" , "TOR2", "0", "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            if(!listReportSingStatus.isEmpty()){
                                strReport2SingStatus = listReportSingStatus.get(0).getFieldName1();
                        }
                       }
                 // End

                 String strEnvelopeCnt= String.valueOf(intEnvelopcnt);
                List<SPTenderCommonData> lstClosedInfo =
                        tenderCommonService1.returndata("getTenderClosedInfo", tenderId, strEnvelopeCnt);

                if (!lstClosedInfo.isEmpty() && lstClosedInfo.size() > 0) {

                    if(Integer.parseInt(lstClosedInfo.get(0).getFieldName1())==Integer.parseInt(lstClosedInfo.get(0).getFieldName2())){
                        allowTenderClose = true;
                    }

                    if("yes".equalsIgnoreCase(lstClosedInfo.get(0).getFieldName3())){
                        isTenderClosed = true;
                    }

                    if("yes".equalsIgnoreCase(lstClosedInfo.get(0).getFieldName4())){
                        isSentToPEOfficer = true;
                    }

                    if("yes".equalsIgnoreCase(lstClosedInfo.get(0).getFieldName5())){
                        isSentToTECChairPerson = true;
                    }

                    if(!"0".equalsIgnoreCase(lstClosedInfo.get(0).getFieldName6())){
                        listingId = lstClosedInfo.get(0).getFieldName6();
                    }

                    tenderFrmCnt=lstClosedInfo.get(0).getFieldName7();
                    decryptedtenderFrmCnt=lstClosedInfo.get(0).getFieldName8();

                    if(tenderFrmCnt.equalsIgnoreCase(decryptedtenderFrmCnt)){
                        allowTenderSharing=true;
                    }
                }

                lstClosedInfo=null;

            %>


            <% if (!isLotId) {%>

                <%
                     CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");
                     tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                     // regarding workflow
                     int uid = 0;
                     if (session.getAttribute("userId") != null) {
                         Integer ob1 = (Integer) session.getAttribute("userId");
                         uid = ob1.intValue();
                     }
                     WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                     short eventid = 5;
                     int objectid = 0;
                     short activityid = 5;
                     int childid = 1;
                     short wflevel = 1;
                     int initiator = 0;
                     int approver = 0;
                     boolean isInitiater = false;
                     boolean evntexist = false;
                     String fileOnHand = "No";
                     String checkperole = "No";
                     String chTender = "No";
                     boolean ispublish = false;
                     boolean iswfLevelExist = false;
                     boolean isconApprove = false;
                     String openCommit = "";
                     String objtenderId = request.getParameter("tenderid");
                     if (objtenderId != null) {
                         objectid = Integer.parseInt(objtenderId);
                         childid = objectid;
                     }

               String donor = "";
              donor = workFlowSrBean.isDonorReq(String.valueOf(eventid),
              Integer.valueOf(objectid));
             String[] norevs = donor.split("_");
              int norevrs = -1;

              String strrev = norevs[1];
              if(!strrev.equals("null")){

               norevrs = Integer.parseInt(norevs[1]);
               }


                     evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
                     List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
                     // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);


                     if (tblWorkFlowLevelConfig.size() > 0) {
                         Iterator twflc = tblWorkFlowLevelConfig.iterator();
                         iswfLevelExist = true;
                         while (twflc.hasNext()) {
                             TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                             TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                             if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                                 fileOnHand = workFlowlel.getFileOnHand();
                             }
                             if (workFlowlel.getWfRoleId() == 1) {
                                 initiator = lmaster.getUserId();
                             }
                             if (workFlowlel.getWfRoleId() == 2) {
                                 approver = lmaster.getUserId();
                             }
                             if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                                 isInitiater = true;
                             }

                         }

                     }


                     String userid = "";
                     if (session.getAttribute("userId") != null) {
                         Object obj = session.getAttribute("userId");
                         userid = obj.toString();
                     }
                     String field1 = "CheckPE";
                     List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, userid, "");

                     if (editdata.size() > 0) {
                         checkperole = "Yes";
                     }

                     // for publish link

                     List<SPTenderCommonData> checkTender = tenderCommonService1.returndata("OpenComWorkFlowStatus", tenderId, "'Pending'");

                     if (checkTender.size() > 0) {
                         chTender = "Yes";
                     }

                     List<SPTenderCommonData> publist = tenderCommonService1.returndata("OpenComWorkFlowStatus", tenderId, "'Approved','Conditional Approval'");

                     if (publist.size() > 0) {
                         ispublish = true;
                         SPTenderCommonData spTenderCommondata = publist.get(0);
                         openCommit = spTenderCommondata.getFieldName1();
                         String conap = spTenderCommondata.getFieldName2();
                         if (conap.equalsIgnoreCase("Conditional Approval")) {
                             isconApprove = true;
                         }
                     }
                     //publist=null;

                    String tempDataChk=null;
                    if(!checkTender.isEmpty()){
                        tempDataChk = checkTender.get(0).getFieldName4();
                        //System.out.println("tempDataChk : "+tempDataChk);
                    }
                    if(!publist.isEmpty()){
                        tempDataChk = publist.get(0).getFieldName4();
                        //System.out.println("tempDataChk : "+tempDataChk);
                    }

                    TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights", Integer.toString(uid),tenderid);
                    List<SPTenderCommonData> chekTenderCreator = wfTenderCommonService.returndata("CheckTenderCreator", Integer.toString(uid),tenderid);
                    List<SPTenderCommonData> creatorRole = wfTenderCommonService.returndata("getCreatorRole", chekTenderCreator.get(0).getFieldName1().toString(),tenderid);
                    
                    if(creatorRole.get(0).getFieldName1().equals("1"))
                    {
                        sendTo = "PA";
                    }
                    else if(creatorRole.get(0).getFieldName1().equals("5"))
                    {
                        sendTo = "AU";
                    }
                    else
                    {
                        sendTo = "AU/PA";
                    }
                %>
                <input type="hidden" id="sendToHidden" value="<%=sendTo%>">
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <td width="18%" class="t-align-left ff">Opening Committee</td>
                        <td width="82%" class="t-align-left">
                            <%/*if((committeMemberService.checkCountByHql("TblCommittee tc","tc.tblTenderMaster.tenderId="+tenderId+" and tc.committeeType in ('TOC','POC')")==0)){*/%><%--<a href="OpenCommFormation.jsp?tenderid=<%=tenderId%>">Create</a>&nbsp;|&nbsp;
                            <%}else{if((committeMemberService.checkCountByHql("TblCommittee tc","tc.tblTenderMaster.tenderId="+tenderId+" and tc.committeeType in ('TOC','POC') and tc.committeStatus='pending'")==0)){%><a href="CommFormation.jsp?tenderid=<%=tenderId%>&isview=y">View</a>&nbsp; | &nbsp;<%}else{%><a href="OpenCommFormation.jsp?tenderid=<%=tenderId%>&isedit=y">Edit</a>&nbsp;| &nbsp;<a href="OpenCommFormation.jsp?tenderid=<%=tenderId%>&isview=y">View</a>&nbsp; |&nbsp;<a href="OpenCommFormation.jsp?tenderid=<%=tenderId%>&ispub=y">Publish</a>&nbsp; | &nbsp;<%}}%>--%>
                            <%
                                 com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean tenderCommitteSrBean = new com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean();
                                 com.cptu.egp.eps.dao.storedprocedure.CommitteDtBean bean = tenderCommitteSrBean.findCommitteDetails(Integer.parseInt(tenderId), "opencom");
                                 tenderCommitteSrBean = null;
                                 long commAppCnt = committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + tenderId + " and tc.committeeType in ('TEC','PEC') and tc.committeStatus='approved'");
                                 if (bean.getpNature() != null) {
                                     if (commAppCnt != 0 || true){ //for eliminating TEC dependency -17/05/2016 Nishit vai
                                         if ((committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + tenderId + " and tc.committeeType in ('TOC','POC')") == 0)) {
                                                    if (!checkuserRights.isEmpty() && userId.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())) {
                                                        if((committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + tenderId + " and tc.committeeType in ('TC') and tc.committeStatus='approved'")==0)){
                                                            %>
                                                            <a href="#" onclick='jAlert("TC has not been configured yet"," Alert ", "");'>Create</a>
                                                           &nbsp;|&nbsp;<a href="#" onclick='jAlert("TC has not been configured yet"," Alert ", "");' >Use Existing Committee</a>
                                                            
                                                            <% } else { %>
                                                            <a href="OpenCommFormation.jsp?tenderid=<%=tenderId%>&parentLink=721">Create</a>
                                                            &nbsp;|&nbsp;<a href="MapCommittee.jsp?tenderid=<%=tenderId%>&type=2&parentLink=956" >Use Existing Committee</a>
                                                      <% } %>
                                                      <%-- &nbsp;|&nbsp;<a href="MapCommittee.jsp?tenderid=<%=tenderId%>&type=2&parentLink=956" >Use Existing Committee</a> --%>
                                                       <%}
                                                                 } else {
                                                            if ((committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + tenderId + " and tc.committeeType in ('TOC','POC') and tc.committeStatus='pending'") == 0)) {%><a href="OpenCommFormation.jsp?tenderid=<%=tenderId%>&isview=y&parentLink=932">View</a><%} else {%>
                            <%if(!checkuserRights.isEmpty() && userId.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                            if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                     || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true) || iswfLevelExist == false) {
                            if(!"dump".equalsIgnoreCase(tempDataChk)){%>
                            <a href="OpenCommFormation.jsp?tenderid=<%=tenderId%>&isedit=y&parentLink=931">Edit</a>&nbsp;| &nbsp;
                                <% } }
                                                                                            }%>
                            <%if(!checkuserRights.isEmpty() && userId.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                            if((!checkTender.isEmpty() || !publist.isEmpty())){
                                if("dump".equalsIgnoreCase(tempDataChk)){%>
                            &nbsp;<a href="OpenCommFormation.jsp?tenderid=<%=tenderId%>&ispub=y&parentLink=940">Notify Committee Members</a>&nbsp;| &nbsp;
                            <%}else if ((ispublish == true && isInitiater == true && !openCommit.equalsIgnoreCase("Approve")) || (!openCommit.equalsIgnoreCase("Approved") && isInitiater == true && (initiator == approver && norevrs == 0) && initiator != 0 && approver != 0)) {%>
                            &nbsp;<a href="OpenCommFormation.jsp?tenderid=<%=tenderId%>&ispub=y&parentLink=940">Notify Committee Members</a>&nbsp;| &nbsp;
                                <% }
                                                                                                }
                                                                                            }%>
                            <a href="OpenCommFormation.jsp?tenderid=<%=tenderId%>&isview=y&parentLink=932">View</a>
                            <%}
                                     }
                                    }else{
                                         out.print("<div class='responseMsg noticeMsg'>TEC is not created yet.</div>");
                                    }
                                 } else {
                                     out.print("<div class='responseMsg noticeMsg'>Business rule yet not configured</div>");
                                 }%>
                        </td>
                    </tr>
                        <%
        List<SPTenderCommonData> getOpenCommPubDate = tenderCommonService1.returndata("getOpenCommPubDate", tenderId, null);
        if (getOpenCommPubDate != null && !getOpenCommPubDate.isEmpty()) {
        %>
        <tr>
            <td width="30%" class="t-align-left ff">Date and time of Committee Formation</td>
            <td width="80%" class="t-align-left"><%=getOpenCommPubDate.get(0).getFieldName1()%></td>
        </tr>
        <%}%>
                    <%
                        /* Dohatec: ICT Start */
                        if(isProcurementTypeIct){
                            boolean isTenderOpeningDay = false;

                            Date openingDate = tenderCurrencyService.getTenderOpeningDate(Integer.parseInt(tenderId));
                            Date today = new Date();

                            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
                            if(dateFormatter.format(openingDate).equals(dateFormatter.format(today))){
                                isTenderOpeningDay = true;
                            }
                    %>
                    <tr>
                        <td width="18%" class="t-align-left ff">Currency Rate</td>
                        <td width="82%" class="t-align-left">
                            <%if(isTOCChairPerson && (isTenderOpeningDay || isTenderOpened) && !isMegaHashVerified){ //TOC chairperson will able to add currency exchange rate at opening date or later but before verification of mega mega hash
                                if(!isForeignCurrencyExchangeRateAdded){
                            %>
                                <a href="AddCurrencyDetails.jsp?tenderid=<%=request.getParameter("tenderid")%>">Add</a>
                                <%}else{%>
                                <a href="AddCurrencyDetails.jsp?tenderid=<%=request.getParameter("tenderid")%>">Edit</a> |
                            <%  }
                            }
                            if(isForeignCurrencyExchangeRateAdded){
                            %>
                                <a href="ViewCurrencyDetails.jsp?tenderid=<%=request.getParameter("tenderid")%>">View</a>
                            <%}%>
                        </td>
                    </tr>
                    <%} /* Dohatec: Ict End */%>
                    <%
                        if((!checkTender.isEmpty() || !publist.isEmpty())){
                    if(!"dump".equalsIgnoreCase(tempDataChk)){%>

                        <%if(isOpeningCommitteeFormed) {%>
                    <tr>
                        <td width="18%" class="t-align-left ff">Workflow</td>
                        <td width="82%" class="t-align-left">
                            <% if (evntexist) {

                                     if (fileOnHand.equalsIgnoreCase("yes") && !checkuserRights.isEmpty() && userId.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString()) && ispublish == false && isInitiater == true) {

                            %>
<!--                            <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=913'>Edit</a>

                            &nbsp;&nbsp;|&nbsp;&nbsp;-->
                            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=914">View</a>

                            <% } else if (iswfLevelExist == false && !checkuserRights.isEmpty() && userId.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString()) && ispublish == false) {
                            %>

<!--                            <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=913'>Edit</a>
                            &nbsp;&nbsp;|&nbsp;&nbsp;-->
                            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=914">View</a>
                            <%  } else if (iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {%>

                            <label>Workflow yet not configured</label>
                            <%                                          } else if (iswfLevelExist == true) {%>
                            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=914">View</a>

                            <%
                                 }
                             } else if (ispublish == false && !checkuserRights.isEmpty() && userId.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())) {
                                 List<CommonAppData> defaultconf = workFlowSrBean.editWorkFlowData("WorkFlowRuleEngine", Integer.toString(eventid), "");
                                 if (defaultconf.size() > 0) {
                            %>
                            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&parentLink=112&submit=Submit&tc=tc">Create</a>
                            <% } else {%>
                            <a  href="#" onclick="checkdefaultconf()">Create</a>
                            <%                         }
                     } else if (iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {%>
                            <label>Workflow yet not configured</label>

                            <% }
                           if (isInitiater == true && (initiator != approver || (initiator == approver && norevrs > 0)) && ispublish == false && fileOnHand.equalsIgnoreCase("Yes")) {
                            %>
                            <% if (chTender.equalsIgnoreCase("Yes")) {%>

                            &nbsp;|&nbsp; <a href="FileProcessing.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&fromaction=opening" >Process file in Workflow</a>


                            <% }
                                          }
                                          if (iswfLevelExist) {%>
                            &nbsp;|&nbsp; <a href="workFlowHistory.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&userid=<%=uid%>&fraction=opening&parentLink=113" >View Workflow History</a>

                            <% }%>

                        </td>
                    </tr>

                            <%}}}%>

<!--                    start: Row for Opening Extension Date-->
<%--START: Extension Request TABLE--%>
                     <%
                    int cntExtRequest=0;
                    for (SPTenderCommonData commonTenderDetails : tenderCommonService1.returndata("getOpeningDtExtList", tenderId, null)) {
                        cntExtRequest++;
                    %>
                     <%if (cntExtRequest==1){%>
                     <script>
                         $(document).ready(function(){
                             $('#anchHideExtension').hide();
                             $('#trExtension').hide();
                         });

                         function ShowExtension(){
                             $('#anchShowExtension').hide();
                             $('#anchHideExtension').show();
                             $('#trExtension').show();

                         }

                          function HideExtension(){
                             $('#anchShowExtension').show();
                             $('#anchHideExtension').hide();
                             $('#trExtension').hide();

                         }
                     </script>

                      <tr>
                        <td width="18%" class="t-align-left ff">Opening Date Extensions</td>
                        <td width="82%" class="t-align-left">
                            <a id="anchShowExtension" onclick="return ShowExtension();" style="cursor: pointer;" >View</a>
                                 <a id="anchHideExtension" onclick="return HideExtension();" style="cursor: pointer;" >Hide</a>
                     <table id="trExtension" width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
                         <tr>
                             <th colspan="7">
                                 Opening Date Extension List
                             </th>
                         </tr>
                         <tr>
                             <th width="4%" class="ff">Sl. No.</th>
                             <th width="20%" class="ff">Date and Time of Request</th>
                             <th width="22%" class="ff">Proposed Opening Date and Time</th>
                             <th width="20%" class="ff">Final Opening Date and Time</th>
                             <th width="20%" class="ff">Requested By</th>
                             <th width="7%" class="ff">Status</th>
                             <th width="7%">Details</th>
                         </tr>
                     <%}%>
                     <tr

                          <%if(Math.IEEEremainder(cntExtRequest,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
                         >
                         <td class="t-align-center"> <%=cntExtRequest%></td>
                         <td class="t-align-center"> <%=commonTenderDetails.getFieldName2()%></td>
                         <td class="t-align-center"> <%=commonTenderDetails.getFieldName3()%></td>
                         <td class="t-align-center"> <%=commonTenderDetails.getFieldName6()%></td>
                         <td class="t-align-center"> <%=commonTenderDetails.getFieldName4()%></td>
                         <td class="t-align-center"> <%=commonTenderDetails.getFieldName5()%></td>
                         <td class="t-align-center">
                             <a href="ViewTOExtReqDetails.jsp?tenderId=<%=tenderId%>&extId=<%=commonTenderDetails.getFieldName1()%>">View</a>
                         </td>
                     </tr>
                     <%}%>
                   <%if (cntExtRequest>0){%>
                       </table>
                   <%}%>
               <%--END: Extension Request TABLE--%>

               <%if (cntExtRequest==1){%>
                        </td>
                      </tr>
               <%}%>
<!--                    end Row for Opening Extension Date-->

                </table>

<!--/* start: code to display opening date extension request */       -->
            <% if (isTenderOpened && isDeclarationTimeOver && !isDeclarationDone) {%>
           <div >
               <%
                if (isExtensionApprovalPending){
                %>
               <div class='responseMsg noticeMsg' style="margin-top: 12px;">
                   Waiting for approval of Opening Date Extension from HOPA.
               </div>

               <%} else {
     isExtensionLinkSeen = true;
     %>
                 <% if ("Chair Person".equalsIgnoreCase(memberRole)){

                %>
                 <div class='responseMsg noticeMsg' style="margin-top: 12px;">
                 Declaration should be given within 2 hour from <%=currTenderOpeningDt%>.
                 <br/><a href="TOExtReq.jsp?tenderId=<%=tenderId%>">Make extension request to HOPA.</a>
                 </div>
                <%} else {%>
                <div class='responseMsg noticeMsg' style="margin-top: 12px;">
                    Opening Date & Time has been lapsed. TOC Chairperson needs to put Opening Date & Time extension request.
                </div>
                <%}%>
               <%}%>
                </div>
             <%}%>
<!--/* end: code to display opening date extension request */       -->
            <%
                 List<SPTenderCommonData> listMR =
                         tenderCommonService1.returndata("getMinRequirements", tenderId, null);

                 if (!listMR.isEmpty()) {
                     minRequirement = Integer.parseInt(listMR.get(0).getFieldName1());
                }
                listMR=null;
            %>
            <%}%>

            <%--START: THIS IS FIRST IF LOOP--%>
            <%  if (!isPackage) {%>

            <% if (isLotId && !isLot) {
                      List<SPTenderCommonData> lotList =
                              tenderCommonService1.returndata("getlotorpackagebytenderid",
                              tenderId,
                              "Lot," + request.getParameter("lotId"));

                          if (!lotList.isEmpty()) {%>
                          <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1">
                              <tr>
                                  <td width="18%" class="t-align-left ff">Lot No. :</td>

                                  <td width="82%" class="t-align-left"><%=lotList.get(0).getFieldName1()%></td>
                              </tr>
                              <tr>
                                  <td class="t-align-left ff">Lot Description :</td>
                                  <td class="t-align-left"><%=lotList.get(0).getFieldName2()%></td>
                              </tr>
                          </table>
                        <%}
                            lotList=null;
             }
            %>

            <%if (!isLotId) {%>

            <!--            START : WARNING MESSAGE AREA   -->
            <!--  Don't Delete this Div-->
            <div id="dvWarning">
            </div>
            <!--            END : WARNING MESSAGE AREA   -->

            <%if(isTenderOpened && !isExtensionLinkSeen && !isExtensionApprovalPending) {%>
            <%--START: COMMON COMMITEE MEMBERS TABLE--%>
            <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
                <tr>
                    <th width="20%" class="ff t-align-center">Committee Members</th>
                    <th width="20%" class="ff t-align-center">Committee Role </th>
                    <th width="20%" class="ff t-align-center">Procurement Role </th>
                    <th width="10%" class="ff t-align-center">Is Decryptor (Yes/No) </th>
                    <th width="10%" class="ff t-align-center">Opening Status (Approved / Pending)</th>
                    <th width="15%" class="ff t-align-center">Opening Date and Time</th>
                </tr>
                <%
                      for (SPTenderCommonData commonTenderDetails : tenderCommonService1.returndata("getTenderEmployeeinfo", tenderId, null)) {
                          memberCnt++;
                          ApprovedMemCnt = Integer.parseInt(commonTenderDetails.getFieldName8());

                          String currUserId = "",
                               CurrUserCommRole="",
                               CurrUserProcRole="",
                               isCurrUserDecrypterVal="No";

                         currUserId = commonTenderDetails.getFieldName7();

                            /*START : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/
                              List<SPTenderCommonData> lstUserCommInfo =
                                      tenderCommonService1.returndata("getUserCommitteeInfoForOpeningProcess", tenderId, currUserId);

                              if (!lstUserCommInfo.isEmpty() && lstUserCommInfo.size() > 0) {
                                  CurrUserCommRole = lstUserCommInfo.get(0).getFieldName1();
                                  CurrUserProcRole = lstUserCommInfo.get(0).getFieldName3();
                                  if ("yes".equalsIgnoreCase(lstUserCommInfo.get(0).getFieldName2())) {
                                      isCurrUserDecrypterVal = "Yes";
                                  }
                              }
                              /*END : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/

                           if (ApprovedMemCnt >= minRequirement) {
                              if (currUserId != "") {
                                  if (Integer.parseInt(currUserId) == Integer.parseInt(userId)) {
                                      showVerify = true;
                                  }
                              }
                          }

                %>
                <tr
                    <%if(Math.IEEEremainder(memberCnt,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
                    >
                    <td class="ff t-align-center">

                        <%
                        //if ((userId.equalsIgnoreCase(commonTenderDetails.getFieldName7())) && commonTenderDetails.getFieldName5().equalsIgnoreCase("pending") && commonTenderDetails.getFieldName9().equalsIgnoreCase("yes")) {
                        if (isTenderOpened) {%>
                            <% if(isDeclarationTime){%>
                            <%if (isMember && userId.equalsIgnoreCase(commonTenderDetails.getFieldName7())){%>
                                    <%if (isMemberApproved){%>
                                        <%=commonTenderDetails.getFieldName1()%>
                                    <%} else { %>
                                    <a id="anchMemDec" href="CommApp.jsp?tenderId=<%=tenderId%>&lotId=<%=strLotId%>&id=<%=commonTenderDetails.getFieldName6()%>&uid=<%=commonTenderDetails.getFieldName7()%>"><%=commonTenderDetails.getFieldName1()%></a>
                                    <%}%>
                               <% } else {%>
                                    <%=commonTenderDetails.getFieldName1()%>
                               <%}%>
                            <%} else if (!isDeclarationTime && isDeclarationTimeOver) {
                            // Declaration time is over
                            %>
                                <%if (isDeclarationDone){
                                // Case: Declaration has been done by atleast 1 member.
                                %>
                                <%if (isMember && userId.equalsIgnoreCase(commonTenderDetails.getFieldName7())){%>
                                          <%if (isMemberApproved){%>
                                                <%=commonTenderDetails.getFieldName1()%>
                                            <%} else { %>
                                            <a id="anchMemDec" href="CommApp.jsp?tenderId=<%=tenderId%>&lotId=<%=strLotId%>&id=<%=commonTenderDetails.getFieldName6()%>&uid=<%=commonTenderDetails.getFieldName7()%>"><%=commonTenderDetails.getFieldName1()%></a>
                                            <%}%>

                                   <%} else {%>
                                        <%=commonTenderDetails.getFieldName1()%>
                                   <%}%>
                                <%} else {
                                // Case: If declaration has not been done.
                                %>
                                    <%=commonTenderDetails.getFieldName1()%>
                                <%}%>

                            <%}%>

                        <%
                             /* END: IF TENDER IS OPENED */
                        } else {
                            /* START: IF TENDER IS NOT YET OPENED */
                        %>
                            <%=commonTenderDetails.getFieldName1()%>
                        <%}%>
                    </td>
                    <td class="t-align-center">
                        <%if ("Chair Person".equalsIgnoreCase(CurrUserCommRole)) {%>
                        Chairperson
                        <%} else {%>
                            <%=CurrUserCommRole%>
                        <%}%>
                    </td>
                    <td class="t-align-center">
                        <%  String pRole = CurrUserProcRole ;
                                if(pRole.contains("PE")){
                                   pRole = pRole.replace("PE", "PA") ;
                                }
                                if(pRole.contains("PAC")){
                                   pRole = pRole.replace("PAC", "PEC") ;
                                }
                               out.print(pRole);
                            %>
                    </td>
                    <td class="t-align-center"><%=isCurrUserDecrypterVal%></td>
                    <td class="t-align-center">
                        <%if("Approved".equalsIgnoreCase(commonTenderDetails.getFieldName5())) {%>
                        Agreed
                        <%} else {%>
                        <%=commonTenderDetails.getFieldName5().toString().substring(0,1).toUpperCase() + commonTenderDetails.getFieldName5().toString().substring(1).toLowerCase()%>
                        <%}%>

                        <% //commonTenderDetails.getFieldName5()%>
                    </td>
                        <td class="t-align-center"><%=commonTenderDetails.getFieldName4()%></td>
                </tr>
                <%
                lstUserCommInfo=null;

                currUserId = null;
               CurrUserCommRole=null;
               CurrUserProcRole=null;
               isCurrUserDecrypterVal=null;

                      } // For Loop Ends
                %>
                <% if (memberCnt == 0) {%>
                <tr>
                    <td colspan="6" class="ff">Committee not created!</td>
                </tr>
                <%}%>
            </table>
            <%--END COMMON COMMITEE MEMBERS TABLE--%>
            <%}%>
            <%
if(intEnvelopcnt==0 && isTenPub) {
%>
<div id="dvEnvelopeMsg"></div>
                    <%    }
                      %>
            <%}%>
            <% if (!isLotId && isLot) {%>
           <% if (isCommitteeApproved && isTenderOpened && (committeeApprovedMemberCnt == committeeMemberCnt) && intEnvelopcnt!=0) {%>

           <!--            START : MEGA HASH VERIFICATION    -->
            <% if (isMemberDecrypter && isMemberApproved){%>

           <%
                List<SPTenderCommonData> lstMegaHash =
                        tenderCommonService1.returndata("getTenderMegaHashStatus", tenderId, null);

                if (!lstMegaHash.isEmpty() && lstMegaHash.size()>0) {
                    if ("yes".equals(lstMegaHash.get(0).getFieldName1())){%>
                    <%} else {%>


                        <%if(isBidExists){%>
                        <form id="frmMegaHash" action="OpenComm.jsp?tenderid=<%=tenderId%>&lotId=<%=strLotId%>" method="post">
                        <%if(isProcurementTypeIct && !isForeignCurrencyExchangeRateAdded){ //Dohatec: ICT%>
                            <div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'>Currency Exchange Rate not yet added.</div>
                        <%}else { //Dohatec: ICT%>
                            <div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'>To Verify the Integrity of the Tenderer's/Consultant’s Document click on ‘Verify Mega Mega Hash’ button</div>
                            <div class="t-align-center t_space">
                                <label class="formBtn_1">
                                    <input name="btnMegaHash" type="submit" value="Verify Mega Mega Hash" />
                                </label>
                            </div>
                        <%}%>
                        </form>

                        <%} else {
                            // IF NO BID SUBMITTED FOR THIS TENDER
                        %>
                        <form id="frmNotifyPE" action="<%=contextPath%>/OpeningProcessServlet?action=notifype&tenderId=<%=tenderId%>&refNo=<%=toextTenderRefNo%>" method="post">
                            <div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'>No Submissions received for this Tender</div>

                        <div class="t-align-center t_space">

                            <label class="formBtn_1">
                                <input name="btnNotifyPE" type="submit" value="Notify <%=sendTo%>" />
                            </label>
                       </div>
                            </form>

                        <%}%>



                    <%
                    }
                }
                 lstMegaHash=null;
            }
            %>
            <!--            END : MEGA HASH VERIFICATION    -->

           <%--START: LOT DESCRIPTION TABLE;--%>
           <%if (isMegaHashVerified) {%>
           <%if (!isPEUser) {%>
           <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th width="11%" class="t-align-center">Lot No.</th>
                    <th class="t-align-center" width="79%">Lot Description</th>
                    <th class="t-align-center" width="10%">Opening</th>
                </tr>
                <%  int j = 0;
                     for (SPTenderCommonData sptcd : tenderCommonService1.returndata("gettenderlotbytenderid", tenderId, null)) {
                         j++;%>
                <tr
                    <%if(Math.IEEEremainder(j,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
                    >
                    <td class="t-align-center"><%=sptcd.getFieldName1()%></td>
                    <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                    <td class="t-align-center"><a href="LotPackageDetail.jsp?tenderid=<%=tenderId%>&lotId=<%=sptcd.getFieldName3()%>">Process</a></td>
                </tr>
                <%}%>
                <%if (j == 0) {%>
                <tr>
                    <td colspan="3" class="t-align-left">No lots found!</td>
                </tr>
                <%}%>
            </table>
            <%}%>
            <%--END: LOT DESCRIPTION TABLE;--%>
            <%}%>
            <%}%>
            <%}%>

            <%

if (isLotId && !isLot && isMember) {%>
            <%--START: LOT FORMS TABLE;--%>
            <%
             if (isMegaHashVerified && isCommitteeApproved && isTenderOpened &&  (committeeApprovedMemberCnt==committeeMemberCnt)) {
            %>

            <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
                <tr>
                    <th width="58%" class="ff t-align-center">Form Name</th>
                    <th width="24%" class="ff t-align-center">Action
                    </th>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td style="text-align: center;">
                                <a id="anchDecryptAll" style="cursor: pointer;" onClick="return decryptAll('0');">Decrypt All and generate report</a>
                    </td>
                </tr>
                <%
                     List<SPTenderCommonData> formList = tenderCommonService1.returndata("getformnamebytenderidandlotidForOpening",tenderId,strLotId);
                     if (!formList.isEmpty()) {
                         int CntForms=0;
                         grandSumLink = true;
                         for (SPTenderCommonData formname : formList) {
                             CntForms++;
                              String CurrentFormDecrypted="";
                              String strFrmStatus="";
                              String strIsFormFilled="";
                            List<SPTenderCommonData> lstFormInfo =
                                    tenderCommonService1.returndata("getFormInfoForOpeningProcess", formname.getFieldName5(), null);

                            if (!lstFormInfo.isEmpty() && lstFormInfo.size()>0){
                                CurrentFormDecrypted= lstFormInfo.get(0).getFieldName1();
                            }

                            List<SPTenderCommonData> lstFormStatusInfo =
                                    tenderCommonService1.returndata("getFormStatus_ForOpening", formname.getFieldName5(), null);

                            if(!lstFormStatusInfo.isEmpty()){
                                strFrmStatus=lstFormStatusInfo.get(0).getFieldName1();
                                strIsFormFilled=lstFormStatusInfo.get(0).getFieldName2();
                            }
                            lstFormStatusInfo=null;
             %>
                <tr
                     <%if(Math.IEEEremainder(CntForms,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
                    >
                    <td><p><strong><%=formname.getFieldName1()%></strong></p>
                    </td>
                    <td class="t-align-center">
                     <!--  start: new code-->
                            <%if (isMember && isMemberApproved) {%>

                            <%if("Canceled".equalsIgnoreCase(strFrmStatus)){%>
                                Canceled Form
                                 <%} else if ("No".equalsIgnoreCase(strIsFormFilled)) {%>
                                No Bidder has filled this form

                            <%} else {%>
                                         <% if (formname.getFieldName8().equalsIgnoreCase("yes")) {
                                // Decryption needed
                                %>
                                    <%if ("yes".equalsIgnoreCase(CurrentFormDecrypted)){%>
                                        <%
                                                boolean isMultipleFillingYes = false;
                                                List<SPTenderCommonData> mulitiList =  tenderCommonService.returndata("isCompReportMultiFilling", formname.getFieldName5(), null);
                                                    if(mulitiList!=null && (!mulitiList.isEmpty())){
                                                        isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                                                }
                                            %>
                                            <a href="<%=isMultipleFillingYes ? "SrvComReport.jsp" : "ComReport.jsp"%>?formId=<%=formname.getFieldName5()%>&tenderId=<%=tenderId%>">Comparative</a>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="IndReport.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>&lotId=<%=request.getParameter("lotId")%>">Individual</a>
                                     <%} else {%>
                                        <% if (isMemberDecrypter) {%>
                                            <%
                                        showDecryptAll=true;
                                        grandSumLink = false;
                                        %>
                                        <a href="Decrypt.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>"  onclick ="return decryptAll('<%=formname.getFieldName5()%>');">Decrypt</a>
                                        <%} else {%>
                                            Decryption pending
                                        <%}%>
                                     <%}%>
                                <%} else {
                                  // Decryption not needed
                                %>
                                  <%
                                                boolean isMultipleFillingYes = false;
                                                List<SPTenderCommonData> mulitiList =  tenderCommonService.returndata("isCompReportMultiFilling", formname.getFieldName5(), null);
                                                    if(mulitiList!=null && (!mulitiList.isEmpty())){
                                                        isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                                                }
                                            %>
                                            <a href="<%=isMultipleFillingYes ? "SrvComReport.jsp" : "ComReport.jsp"%>?formId=<%=formname.getFieldName5()%>&tenderId=<%=tenderId%>">Comparative</a>
                                   &nbsp;&nbsp;|&nbsp;&nbsp;
                                  <a href="IndReport.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>">Individual</a>
                                <%}%>
                            <%}%>


                            <%} else {%>
                                Not a member
                            <%}%>
                        <!-- end: new code-->

                    </td>
                </tr>
                <%
                         lstFormInfo=null;
                         } // End For Loop

                %>
                <%
                                                 if (grandsumlinkstatus) {
                       List<SPTenderCommonData> grandsumlink = tenderCommonService1.returndata("getGrandSummaryLink", strLotId, null);
                       if(!"0".equals(grandsumlink.get(0).getFieldName1())){
                    %>
                    <tr <%if(Math.IEEEremainder(CntForms,2)!=0) {%>
                            class="bgColor-Green"
                        <%} else {%>
                        class="bgColor-white"
                        <%   }%>>
                        <td class="ff t-align-left">Grand Summary</td>
                        <td class="t-align-center">
                            <% if(grandSumLink){ %>
                            <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=strLotId%>&report=Comparative">Comparative</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=strLotId%>&report=Individual">Individual</a>
                            <% }else{ %>
                            -
                            <% } %>
                        </td>
                    </tr>
                <%      }
                   } %>
                <% } else {%>
                <tr>
                    <td class="t-align-left" colspan="2">No forms found!</td>
                </tr>
                <%}%>
            </table>

            <%
            formList=null;
            }
            %>
            <%--END LOT FORMS TABLE;--%>
<%}%>
            <% } else if (isPackage && isMember) {
                  List<SPTenderCommonData> lotList =
                          tenderCommonService1.returndata("getlotorpackagebytenderid", tenderId, "Package,1");

                  if (!lotList.isEmpty()) {%>
                  <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1 t_space">
                      <tr>
                          <td width="18%" class="t-align-left ff">Package No.</td>

                          <td width="82%" class="t-align-left"><%=lotList.get(0).getFieldName1()%></td>
                      </tr>
                      <tr>
                          <td class="t-align-left ff">Package Description</td>
                          <td class="t-align-left"><%=lotList.get(0).getFieldName2()%></td>
                      </tr>
                  </table>


                <!--            START : WARNING MESSAGE AREA   -->
                 <div id="dvWarning">
                </div>
                <!--            END : WARNING MESSAGE AREA   -->

                <%if(isTenderOpened && !isExtensionLinkSeen && !isExtensionApprovalPending) {%>
                <!--            START : COMMITTEE MEMBERS TABLE    -->
            <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
                <tr>
                    <th width="20%" class="ff t-align-center">Committee Members</th>
                    <th width="15%" class="ff t-align-center">Committee Role </th>
                    <th width="20%" class="ff t-align-center">Procurement Role </th>
                    <th width="15%" class="ff t-align-center">Is Decryptor (Yes/No) </th>
                    <th width="10%" class="ff t-align-center">Opening Status </th>
                    <th width="15%" class="ff t-align-center">Opening Date and Time </th>
                </tr>
                <%
                    for (SPTenderCommonData commonTenderDetails : tenderCommonService1.returndata("getTenderEmployeeinfo", tenderId, null)) {
                        memberCnt++;
                        ApprovedMemCnt = Integer.parseInt(commonTenderDetails.getFieldName8());

                         String currUserId = "",
                                 CurrUserCommRole = "",
                                 CurrUserProcRole = "",
                                 isCurrUserDecrypterVal = "No";

                         currUserId = commonTenderDetails.getFieldName7();

                         /*START : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/
                         List<SPTenderCommonData> lstUserCommInfo =
                                 tenderCommonService1.returndata("getUserCommitteeInfoForOpeningProcess", tenderId, currUserId);

                        if (!lstUserCommInfo.isEmpty() && lstUserCommInfo.size()>0){

                            CurrUserCommRole = lstUserCommInfo.get(0).getFieldName1();
                            CurrUserProcRole = lstUserCommInfo.get(0).getFieldName3();
                            if ("yes".equalsIgnoreCase(lstUserCommInfo.get(0).getFieldName2())){
                                       isCurrUserDecrypterVal="Yes";
                            }
                        }
                        /*END : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/

                        if (ApprovedMemCnt >= minRequirement) {
                            if (currUserId != "") {
                                if (Integer.parseInt(currUserId) == Integer.parseInt(userId)) {
                                    showVerify = true;
                                }
                            }
                        }
                %>
                <tr
                    <%if(Math.IEEEremainder(memberCnt,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
                    >
                    <td class="ff t-align-center">

                        <%
                        //if ((userId.equalsIgnoreCase(commonTenderDetails.getFieldName7())) && commonTenderDetails.getFieldName5().equalsIgnoreCase("pending") && commonTenderDetails.getFieldName9().equalsIgnoreCase("yes")) {

                        if (isTenderOpened) {
                        %>
                            <% if(isDeclarationTime){%>
                                <%if (isMember && userId.equalsIgnoreCase(commonTenderDetails.getFieldName7())){%>
                                    <%if (isMemberApproved){%>
                                        <%=commonTenderDetails.getFieldName1()%>
                                    <%} else { %>
                                         <a id="anchMemDec"  href="CommApp.jsp?tenderId=<%=tenderId%>&id=<%=commonTenderDetails.getFieldName6()%>&uid=<%=commonTenderDetails.getFieldName7()%>"><%=commonTenderDetails.getFieldName1()%></a>
                                    <%}%>
                               <% } else {%>
                                    <%=commonTenderDetails.getFieldName1()%>
                               <%}%>
                            <%} else if (!isDeclarationTime && isDeclarationTimeOver) {
                            // Declaration time is over
                            %>
                                <%if (isDeclarationDone){
                                // Case: Declaration has been done by atleast 1 member.
                                %>
                                     <%if (isMember && userId.equalsIgnoreCase(commonTenderDetails.getFieldName7())){%>
                                          <%if (isMemberApproved){%>
                                                <%=commonTenderDetails.getFieldName1()%>
                                            <%} else { %>
                                                 <a id="anchMemDec" href="CommApp.jsp?tenderId=<%=tenderId%>&id=<%=commonTenderDetails.getFieldName6()%>&uid=<%=commonTenderDetails.getFieldName7()%>"><%=commonTenderDetails.getFieldName1()%></a>
                                            <%}%>

                                   <%} else {%>

                                   <%=commonTenderDetails.getFieldName1()%>

                                   <%}%>
                                <%} else {
                                // Case: If declaration has not been done.
                                %>
                                <%=commonTenderDetails.getFieldName1()%>
                                <%}%>

                            <%}%>

                        <%
                             /* END: IF TENDER IS OPENED */
                        } else {
                            /* START: IF TENDER IS NOT YET OPENED */
                        %>
                            <%=commonTenderDetails.getFieldName1()%>
                        <%}%>
                    </td>
                    <td class="t-align-center">
                       <%if ("Chair Person".equalsIgnoreCase(CurrUserCommRole)) {%>
                        Chairperson
                        <%} else {%>
                            <%=CurrUserCommRole%>
                        <%}%>

                     </td>
                        <td class="t-align-center">
                            <%  String pRole = CurrUserProcRole ;
                                if(pRole.contains("PE")){
                                   pRole = pRole.replace("PE", "PA") ;
                                }
                                if(pRole.contains("PAC")){
                                   pRole = pRole.replace("PAC", "PEC") ;
                                }
                               out.print(pRole);
                            %>
                        </td>
                    <td class="t-align-center"><%=isCurrUserDecrypterVal%></td>
                    <td class="t-align-center">
                        <%if("Approved".equalsIgnoreCase(commonTenderDetails.getFieldName5())) {%>
                        Agreed
                        <%} else {%>
                        <%=commonTenderDetails.getFieldName5()%>
                        <%}%>

                    </td>
                    <td class="t-align-center"><%=commonTenderDetails.getFieldName4()%></td>
                </tr>
                    <%
                        lstUserCommInfo = null;
                        currUserId = null;
                        CurrUserCommRole = null;
                        CurrUserProcRole = null;
                        isCurrUserDecrypterVal = null;

                    }

                %>
                <% if (memberCnt == 0) {%>
                <tr>
                    <td colspan="6" class="ff">Committee not created!</td>
                </tr>
                <%}%>
            </table>
<!--            END : COMMITTEE MEMBERS TABLE    -->
<%}%>



<% if (isCommitteeApproved && isTenderOpened && (committeeApprovedMemberCnt == committeeMemberCnt)) {%>

<!--            START : MEGA HASH VERIFICATION    -->
<% if (isMemberDecrypter && isMemberApproved){%>
  <% List<SPTenderCommonData> lstMegaHash =
          tenderCommonService1.returndata("getTenderMegaHashStatus", tenderId, null);

    if (!lstMegaHash.isEmpty() && lstMegaHash.size()>0) {
        if ("yes".equals(lstMegaHash.get(0).getFieldName1())){%>
        <%} else {%>


            <%if (isBidExists) {%>
            <form id="frmMegaHash" action="OpenComm.jsp?tenderid=<%=tenderId%>&lotId=<%=strLotId%>" method="post">
            <%if(isProcurementTypeIct && !isForeignCurrencyExchangeRateAdded){ //Dohatec: ICT%>
                <div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'>Currency Exchange Rate not yet added.</div>
            <%}else { //Dohatec: ICT%>
                <div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'>To Verify the Integrity of the Tenderer's/Consultant’s Document click on ‘Verify Mega Mega Hash’ button</div>
                <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input name="btnMegaHash" type="submit" value="Verify Mega Mega Hash" />
                    </label>
                </div>
            <%}%>
            </form>
            <%} else {%>

<form id="frmNotifyPE" action="<%=contextPath%>/OpeningProcessServlet?action=notifype&tenderId=<%=tenderId%>&refNo=<%=toextTenderRefNo%>" method="post">
 <div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'>No Submissions received for this Tender</div>

                        <div class="t-align-center t_space">

                            <label class="formBtn_1">
                                <input name="btnNotifyPE" type="submit" value="Notify <%=sendTo%>" />
                            </label>
                        </div>
         </form>



            <%}%>

        <%}
    }
lstMegaHash=null;


}
        %>
<!--            END : MEGA HASH VERIFICATION    -->
<%if (isMegaHashVerified){%>
<%//if (!isPEUser){ //Modified by dohatec to resolve online tracking id :6869%>

<%
//intEnvelopcnt=0;
if(intEnvelopcnt==0 && isTenPub) {%>

<div id="dvEnvelopeMsg"></div>

                    <%} else {
                            //
                        if (countDecrptedData == 0) {
                            if(!(eventType.equals("REOI") || eventType.equals("PQ") || eventType.equals("1 stage-TSTM"))){
                    %>
                    <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1 t_space">
                        <tr>
                            <td width="18%" class="t-align-left ff">Generate Report Template(BOR/BER) :</td>
                            <% if (rtype != null && rtype.isEmpty()) {%>
                            <td width="82%" class="t-align-left"><a href="javascript:void(0);" id="autorptgeneration" style="cursor: pointer;" onClick="return generateAutoReport(<%=tenderId%>,'')">Generate Report Template(BOR/BER)</a></td>
                            <% } else if (rtype.size() == 1 && rtype.contains("TOR")) {%>
                            <td width="82%" class="t-align-left"><a href="javascript:void(0);" id="autorptgeneration" style="cursor: pointer;" onClick="return generateAutoReport(<%=tenderId%>,'TER')">Generate TER Report</a></td>
                            <% } else if (rtype.size() == 1 && rtype.contains("TER")) {%>
                            <td width="82%" class="t-align-left"><a href="javascript:void(0);" id="autorptgeneration" style="cursor: pointer;" onClick="return generateAutoReport(<%=tenderId%>,'TOR')">Generate BOR Report</a></td>
                            <% } else if (rtype.size() == 2) {%>
                            <td width="82%" class="t-align-left">Report template generated</td>
                            <% }%>
                        </tr>
                    </table>
                    <% } }%>
<table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
    <tr>
        <th width="58%" class="ff t-align-center">Form Name</th>
        <th width="24%" class="ff t-align-center">Action
        </th>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td class="t-align-center" style="text-align: center;">
                                <a id="anchDecryptAll" style="cursor: pointer;" onclick="return decryptAll('0');">Decrypt All and generate report</a>
        </td>
    </tr>
    <%
    List<SPTenderCommonData> formList =
            tenderCommonService1.returndata("getTechnicalForms_ForOpening", tenderId, "0");

        //if (!formList.isEmpty()) {
             int counterRowId = 0;
             grandSumLink = true;
            for (SPTenderCommonData formname : formList) {
                boolean isMultipleFillingYes = false;
                List<SPTenderCommonData> mulitiList =  tenderCommonService.returndata("isCompReportMultiFilling", formname.getFieldName5(), null);
                if(mulitiList!=null && (!mulitiList.isEmpty())){
                    isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                }
                counterRowId = counterRowId+1;
                String CurrentFormDecrypted="";
                String strFrmStatus="";
                String strIsFormFilled="";
                List<SPTenderCommonData> lstFormInfo =
                        tenderCommonService1.returndata("getFormInfoForOpeningProcess", formname.getFieldName5(), null);

                if (!lstFormInfo.isEmpty() && lstFormInfo.size()>0){
                    CurrentFormDecrypted= lstFormInfo.get(0).getFieldName1();
                }

                 List<SPTenderCommonData> lstFormStatusInfo =
                                    tenderCommonService1.returndata("getFormStatus_ForOpening", formname.getFieldName5(), null);

                            if(!lstFormStatusInfo.isEmpty()){
                                strFrmStatus=lstFormStatusInfo.get(0).getFieldName1();
                                strIsFormFilled=lstFormStatusInfo.get(0).getFieldName2();
                            }
                            lstFormStatusInfo=null;
    %>
                <tr
                     <%if(Math.IEEEremainder(counterRowId,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
                    >
                    <td class="ff"><p><strong><%=formname.getFieldName1()%></strong></p>
                    </td>
                    <td class="t-align-center">
                        <!--  start: new code-->
                            <%if (isMember && isMemberApproved) {%>

                            <%if("Canceled".equalsIgnoreCase(strFrmStatus)) {%>
                                Canceled Form
                                <%} else if ("No".equalsIgnoreCase(strIsFormFilled)) {%>
                                No Bidder has filled this form
                            <%} else { %>
                                <% if (formname.getFieldName8().equalsIgnoreCase("yes")) {
                                // Decryption needed
                                %>
                                    <%if ("yes".equalsIgnoreCase(CurrentFormDecrypted)){
                                        if(isMultipleFillingYes){
                                %>
                                        <a href="SrvComReport.jsp?formId=<%=formname.getFieldName5()%>&tenderId=<%=tenderId%>">Comparative Report</a>
                                <%
                                        }else{
                                %>
                                        <a href="ComReport.jsp?formId=<%=formname.getFieldName5()%>&tenderId=<%=tenderId%>">Comparative Report</a>
                                <%
                                        }
                                %>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="IndReport.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>">Individual Report</a>
                                     <%} else {%>
                                        <% if (isMemberDecrypter) {%>
                                            <%
                                            showDecryptAll=true;
                                            grandSumLink = false;
                                            %>
                                            <a href="Decrypt.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>" onclick ="return decryptAll('<%=formname.getFieldName5()%>');">Decrypt</a>
                                        <%} else {%>
                                            Decryption pending
                                        <%}%>
                                     <%}%>
                                <%} else {
                                  // Decryption not needed
                                        if(isMultipleFillingYes){
                                %>
                                        <a href="SrvComReport.jsp?formId=<%=formname.getFieldName5()%>&tenderId=<%=tenderId%>">Comparative Report</a>
                                <%
                                        }else{
                                %>
                                        <a href="ComReport.jsp?formId=<%=formname.getFieldName5()%>&tenderId=<%=tenderId%>">Comparative Report</a>
                                <%
                                        }
                                %>
                                   &nbsp;&nbsp;|&nbsp;&nbsp;
                                  <a href="IndReport.jsp?tenderId=<%=tenderId%>&formId=<%=formname.getFieldName5()%>">Individual Report</a>
                                <%}%>
                            <% } %>
                            <%
                                    if (grandsumlinkstatus) {
                                   List<SPTenderCommonData> grandsumlink = tenderCommonService1.returndata("getGrandSummaryLink", strLotId, null);
                                   if(!"0".equals(grandsumlink.get(0).getFieldName1())){
                                %>
                                <tr <%if(Math.IEEEremainder(formList.size(),2)!=0) {%>
                                        class="bgColor-Green"
                                    <%} else {%>
                                    class="bgColor-white"
                                    <%   }%>>
                                    <td class="ff t-align-left">Grand Summary</td>
                                    <td class="t-align-center">
                                        <% if(grandSumLink){ %>
                                        <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=strLotId%>&report=Comparative">Comparative</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=strLotId%>&report=Individual">Individual</a>
                                        <% }else{ %>
                                        -
                                        <% } %>
                                    </td>
                                </tr>
                                <%  }
                               }%>
                            <%} else {%>
                                Not a member
                            <%}%>
                        <!-- end: new code-->
                    </td>
                </tr>
            <%
                lstFormInfo=null;
                CurrentFormDecrypted=null;
                } // For Loop Ends
            %>

            <%
                //intEnvelopcnt=1;
                // If Envelope Count is 1
                if(intEnvelopcnt==1) {

                    List<SPTenderCommonData> lstLots =
                    tenderCommonService1.returndata("getLoIdLotDesc_ForOpening", tenderId, null);

                    for (SPTenderCommonData objLot : lstLots) {%>

                    <tr>
                        <td class="t-align-center" colspan="2">
                            <%if (!"Services".equalsIgnoreCase(strProcurementNature)) {%>
                            <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1 t_space">
                                <tr>
                                    <td width="18%" class="t-align-left ff">Lot No.</td>
                                    <td width="82%" class="t-align-left"><%=objLot.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Lot Description</td>
                                    <td class="t-align-left"><%=objLot.getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <%}%>
                                        <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
                                        <%
                                        List<SPTenderCommonData> lstPriceBidFrms = null;

                                       if ("Services".equalsIgnoreCase(strProcurementNature)) {
                                            lstPriceBidFrms=tenderCommonService1.returndata("getPriceBidForms_ForOpening", tenderId, null);
                                       } else {
                                             lstPriceBidFrms=tenderCommonService1.returndata("getPriceBidForms_ForOpening", tenderId, objLot.getFieldName1());
                                       }


int lotFormRowId = 0;
grandSumLink = true;
for (SPTenderCommonData lotFormName : lstPriceBidFrms) {
    boolean isMultipleFillingYes = false;
    List<SPTenderCommonData> mulitiList =  tenderCommonService.returndata("isCompReportMultiFilling", lotFormName.getFieldName5(), null);
    if(mulitiList!=null && (!mulitiList.isEmpty())){
    isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
    }
    lotFormRowId++;
                String CurrentLotFormDecrypted="";
                String strLotFrmStatus="";
                String strLotIsFormFilled="";
                List<SPTenderCommonData> lstLotFormInfo =
                        tenderCommonService1.returndata("getFormInfoForOpeningProcess", lotFormName.getFieldName5(), null);

                 if (!lstLotFormInfo.isEmpty()){
                    CurrentLotFormDecrypted= lstLotFormInfo.get(0).getFieldName1();
                }
                lstLotFormInfo=null;

                 List<SPTenderCommonData> lstLotFormStatusInfo =
                                    tenderCommonService1.returndata("getFormStatus_ForOpening", lotFormName.getFieldName5(), null);

                            if(!lstLotFormStatusInfo.isEmpty()){
                                strLotFrmStatus=lstLotFormStatusInfo.get(0).getFieldName1();
                                strLotIsFormFilled=lstLotFormStatusInfo.get(0).getFieldName2();
                            }
                            lstLotFormStatusInfo=null;
%>

                <tr <%if(Math.IEEEremainder(lotFormRowId,2)==0) {%> class="bgColor-Green" <%} else {%>class="bgColor-white" <%}%> >
                    <td class="ff" style="width: 70%;"><p><strong><%=lotFormName.getFieldName1()%></strong></p>
                    </td>
                    <td class="t-align-center">
                        <!--  start: new code-->
                            <%if (isMember && isMemberApproved) {%>

                            <%if("Canceled".equalsIgnoreCase(strLotFrmStatus)) {%>
                                Canceled Form

                                 <%} else if ("No".equalsIgnoreCase(strLotIsFormFilled)) {%>
                                No Bidder has filled this form
                            <%} else {%>
                                                    <% if (lotFormName.getFieldName8().equalsIgnoreCase("yes")) {
                                // Decryption needed
                                %>
                                    <%if ("yes".equalsIgnoreCase(CurrentLotFormDecrypted)){
                                        if(isMultipleFillingYes){
                                %>
                                        <a href="SrvComReport.jsp?formId=<%=lotFormName.getFieldName5()%>&tenderId=<%=tenderId%>">Comparative Report</a>
                                <%
                                        }else{
                                %>
                                        <a href="ComReport.jsp?formId=<%=lotFormName.getFieldName5()%>&tenderId=<%=tenderId%>">Comparative Report</a>
                                <%
                                        }
                                %>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="IndReport.jsp?tenderId=<%=tenderId%>&formId=<%=lotFormName.getFieldName5()%>">Individual Report</a>
                                     <%} else {%>
                                        <% if (isMemberDecrypter) {
    showDecryptAll=true;
    grandSumLink = false;
    %>
                                            <a href="Decrypt.jsp?tenderId=<%=tenderId%>&formId=<%=lotFormName.getFieldName5()%>" onclick ="return decryptAll('<%=lotFormName.getFieldName5()%>');">Decrypt</a>
                                        <%} else {%>
                                            Decryption pending
                                        <%}%>
                                     <%}%>
                                <%} else {
                                  // Decryption not needed
                                        if(isMultipleFillingYes){
                                %>
                                        <a href="SrvComReport.jsp?formId=<%=lotFormName.getFieldName5()%>&tenderId=<%=tenderId%>">Comparative Report</a>
                                <%
                                        }else{
                                %>
                                        <a href="ComReport.jsp?formId=<%=lotFormName.getFieldName5()%>&tenderId=<%=tenderId%>">Comparative Report</a>
                                <%
                                        }
                                %>
                                   &nbsp;&nbsp;|&nbsp;&nbsp;
                                  <a href="IndReport.jsp?tenderId=<%=tenderId%>&formId=<%=lotFormName.getFieldName5()%>">Individual Report</a>
                                <% } %>
                            <%}%>
                            <% } else {%>
                                Not a member
                            <% } %>
                        <!-- end: new code-->
                    </td>
                </tr>

<%} // for loop ends for Price Bid Forms
%>

<%
                                                                        if (grandsumlinkstatus) {
       List<SPTenderCommonData> grandsumlink = tenderCommonService1.returndata("getGrandSummaryLink", objLot.getFieldName1(), null);
       if(!"0".equals(grandsumlink.get(0).getFieldName1())){
%>
        <tr <%if(Math.IEEEremainder(lotFormRowId,2)!=0) {%>
                class="bgColor-Green"
            <%} else {%>
            class="bgColor-white"
            <%   }%>>
            <td class="ff t-align-left">Grand Summary</td>
            <td class="t-align-center">
                <%  if(grandSumLink){ %>
                <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=objLot.getFieldName1()%>&report=Comparative">Comparative Report</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=objLot.getFieldName1()%>&report=Individual">Individual Report</a>
                <% }else{ %>
                -
                <% } %>
            </td>
        </tr>
<%    }
   } %>
<% if(lotFormRowId==0){%>
<tr>
    <td class="t-align-left" colspan="2">No Forms Found</td>
</tr>
<%}

%>
</table>
                                   <%if (!"Services".equalsIgnoreCase(strProcurementNature)) {%>
                                    </td>
                                </tr>
                            </table>
                                    <%}%>
                        </td>
                    </tr>

                      <%
} // for loop ends for Lots
}

    formList=null;
    %>
</table>


<%}%>
             <%
//}//Modified by dohatec to resolve online tracking id : 6869
             %>
<%}%>
<%}%>
            <%}%>
            <%}%>
            <%--END THIS IS PACKAGE PART--%>

            <%if (!request.getRequestURL().toString().toLowerCase().contains("lotpackagedetail.jsp")) {%>
            <%
/*
System.out.println("isMegaHashVerified :" + isMegaHashVerified);
System.out.println("isCommitteeApproved :" + isCommitteeApproved);
System.out.println("isTenderOpened :" + isTenderOpened);
System.out.println("committeeApprovedMemberCnt :" + committeeApprovedMemberCnt);
System.out.println("committeeMemberCnt :" + committeeMemberCnt);
System.out.println("intEnvelopcnt :" + intEnvelopcnt);
System.out.println("isPackage :" + isPackage);
System.out.println("isMultiLots :" + isMultiLots);
System.out.println("isSentToTECChairPerson :" + isSentToTECChairPerson);
 */

if (allowTenderSharing && isMegaHashVerified && isCommitteeApproved && isTenderOpened && (committeeApprovedMemberCnt == committeeMemberCnt) && intEnvelopcnt!=0) {%>

            <%
//if (isPackage && isMultiLots && !isSentToTECChairPerson) {
            if ( isMultiLots && !isSentToTECChairPerson) {

      if (!lstLotsForTOR.isEmpty()) {
 %>

 <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1 t_space">
     <tr>
         <th colspan="4">
             <%=strDocType%> Opening Report
         </th>
     </tr>
     <tr>
         <th>Lot No.</th>
         <th>Lot Description</th>
         <th>View</th>
     </tr>
   <%
               for (SPTenderCommonData objLot : lstLotsForTOR) {
%>

<tr>
    <td width="15%" class="t-align-center"><%=objLot.getFieldName3()%></td>
    <td width="70%" class="t-align-left"><%=objLot.getFieldName2()%></td>
    <td width="15%" class="t-align-center">
        <a href="<%=contextPath%>/officer/TORCommon.jsp?tenderid=<%=tenderId%>&lotId=<%=objLot.getFieldName1()%>">
            BOR
        </a>
        
            <%
            // Get Report Sign Status 


                    if("Services".equalsIgnoreCase(strProcurementNature)){
                            List<SPCommonSearchDataMore> listReportSingStatus = commonSearchDataMoreService.geteGPData("getTenderReportSignStatusWithLot", tenderid, "POC" , "TOR1", objLot.getFieldName1(), "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                           if(!listReportSingStatus.isEmpty()){
                              strReport1SingStatus = listReportSingStatus.get(0).getFieldName1();
                        }
                        } else {
                            List<SPCommonSearchDataMore> listReportSingStatus = commonSearchDataMoreService.geteGPData("getTenderReportSignStatusWithLot", tenderid, "TOC" , "TOR1", objLot.getFieldName1(), "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            if(!listReportSingStatus.isEmpty()){
                                strReport1SingStatus = listReportSingStatus.get(0).getFieldName1();
                        }
                       }
               if("Services".equalsIgnoreCase(strProcurementNature)){
                            List<SPCommonSearchDataMore> listReportSingStatus = commonSearchDataMoreService.geteGPData("getTenderReportSignStatusWithLot", tenderid, "POC" , "TOR2", objLot.getFieldName1(), "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                           if(!listReportSingStatus.isEmpty()){
                              strReport2SingStatus = listReportSingStatus.get(0).getFieldName1();
                        }
                        } else {
                            List<SPCommonSearchDataMore> listReportSingStatus = commonSearchDataMoreService.geteGPData("getTenderReportSignStatusWithLot", tenderid, "TOC" , "TOR2", objLot.getFieldName1(), "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            if(!listReportSingStatus.isEmpty()){
                                strReport2SingStatus = listReportSingStatus.get(0).getFieldName1();
                        }
                       }
                 // End
            %>
        <b id="SignStatus2" style="color:green">
           <%  if((!"0".equalsIgnoreCase(strReport2SingStatus)) && (!"0".equalsIgnoreCase(strReport1SingStatus))){%>
                               <%=   " ("+strReport2SingStatus+")" %>
                               <%}%>  </b>
    </td>
</tr>

<%
     } // For Loop Ends
%>
</table>
<%
                    }
%>

<%
}
%>




            <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1 t_space">

                <%/*Taher(Added false to Hide Sharing link)*/if (false && isMember && isTOCChairPerson && allowTenderSharing && !isSentToPEOfficer && allowTenderClose) {%>
                <tr>
                    <td width="25%" class="t-align-left ff"><%=strDocType%> Opening Result Sharing : </td>
                    <td width="75%" class="t-align-left">
                        <%List<SPTenderCommonData> listTOS = tenderCommonService1.returndata("checkTOSEntry", tenderId, null);
                        if (!listTOS.isEmpty() && listTOS.size() > 0) {%>
                        <a href="TenResultSharing.jsp?tenderId=<%=tenderId%>&isEdit=U">Share with Tenderers/Consultants</a>
                        <%} else {%>
                        <a href="TenResultSharing.jsp?tenderId=<%=tenderId%>&isEdit=C">Share with Tenderers/Consultants</a>
                        <%}
                        listTOS=null;
                        %>
                    </td>
                </tr>
                <%}%>
                <% if (!isMember) {%>
                    <% if (userId.equalsIgnoreCase(peOfficerUserId) && isPEUser && isTenderClosed && isSentToPEOfficer){ %>
                    <tr id="trTOS" style="display: none;">
                        <td width="25%" class="t-align-left ff"><%=strDocType%> Opening Sheet : </td>
                        <td width="75%" class="t-align-left">
                            <%if (!isSentToTECChairPerson) {%>
                            <a href="TOS.jsp?tid=<%=tenderId%>">Report</a>
                            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="TenderClosing.jsp?tenderId=<%=tenderId%>&type=CP&parentLink=917">Send to TEC Chairperson</a>
                            <%} else {%>
                             Sent to TEC Chairperson for processing
                            <%}%>
                        </td>
                    </tr>

                     <%if(!isSentToTECChairPerson && allowTenderSharing) {%>
                        <tr id="trTOR">
                            <td width="25%" class="t-align-left ff"><%=strDocType%> Opening Report : </td>
                        <td width="75%" class="t-align-left">
                                <% if (!isMultiLots) {%>
                                <a href="<%=contextPath%>/officer/TORCommon.jsp?tenderid=<%=tenderId%>&lotId=<%=torLotId%>">
                                 BOR

                                </a>
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                <%}%>
                                <a href="TenderClosing.jsp?tenderId=<%=tenderId%>&type=CP&parentLink=917">Send to TEC Chairperson</a>
                        </td>
                    </tr>
                     <%} else if (isSentToTECChairPerson && allowTenderSharing) {%>
                        <tr id="trTOR">
                            <td width="25%" class="t-align-left ff"><%=strDocType%> Opening Report : </td>
                            <td width="75%" class="t-align-left">Sent to TEC Chairperson for processing</td>
                        </tr>
                    <%}%>

                    <%}%>
                <%} else if (isMember) {%>


          <tr
              <%if (!allowTenderSharing) {%>
              style="display: none;"
              <%} else if (isMultiLots && !isPEUser && !isTOCChairPerson) {%>
              style="display: none;"
              <%} else if (isMultiLots && isTOCChairPerson && !isTenderClosed) {%>
              style="display: none;"
              <%}%>
              >
                    <td width="25%" class="t-align-left ff"><%=strDocType%> Opening Report : </td>

                    <td width="75%" class="t-align-left">
                         <%if (userId.equalsIgnoreCase(peOfficerUserId) && isPEUser && isTenderClosed && isSentToPEOfficer){%>

                            <%if (!isSentToTECChairPerson){%>
                                <% if (!isMultiLots) {%>
                                <a href="<%=contextPath%>/officer/TORCommon.jsp?tenderid=<%=tenderId%>&lotId=<%=torLotId%>">
                                  BOR
                                </a>
                                 <b id="SignStatus2" style="color:green">
           <%  if((!"0".equalsIgnoreCase(strReport2SingStatus)) && (!"0".equalsIgnoreCase(strReport1SingStatus))){%>
                               <%=   " ("+strReport2SingStatus+")" %>
                               <%}%>  </b>
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                <%}%>

                                <a href="TenderClosing.jsp?tenderId=<%=tenderId%>&type=CP&parentLink=917">Send to TEC Chairperson</a>
                            <%} else {%>
                             Sent to TEC Chairperson for processing
                            <%}%>

                        <%} else {%>

                       <%if(!isSentToPEOfficer){%>

<% if (!isMultiLots) {%>

<a href="<%=contextPath%>/officer/TORCommon.jsp?tenderid=<%=tenderId%>&lotId=<%=torLotId%>">
    BOR
</a>
     <b id="SignStatus2" style="color:green">
           <%  if((!"0".equalsIgnoreCase(strReport2SingStatus)) && (!"0".equalsIgnoreCase(strReport1SingStatus))){%>
                               <%=   " ("+strReport2SingStatus+")" %>
                               <%}%>  </b>
<%}%>

                                <%

if (isTOCChairPerson && isTenderClosed ){%>
                                    <%if (!isSentToPEOfficer) {%>

                                    <script type="text/javascript" >
                                          function ConfirmSend(){
                                            
                                            var val=confirm("Once you hand over Tender including Reports to AU/PA, access of all TOC/POC members will be frozen i.e. they cannot view the tender including Reports details.");

                                            if(val==true){
                                                return true;
                                            }else {
                                                return false;
                                            }
                                        }


                                    </script>
                                    <script type="text/javascript">


                                        $(document).ready(function(){
                                            $("#achSentToPE").click(function() {

                                                flag = ConfirmSend();
                                               // alert(flag);
                                                if(flag==false){
                                                    return false;
                                                } else{
                                                     var tenderId= <%=request.getParameter("tenderid")%>;
                                                   // window.location = "TenderClosing.jsp?tenderId="+tenderId+"&type=P";
                                                   dynamicFromSubmit("TenderClosing.jsp?tenderId="+tenderId+"&type=P&parentLink=734");


                                                }

                                            });

                                        });
                                    </script>
                                       <%if (!isMultiLots) {%>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                       <%}%>
                                       <a id="achSentToPE" style="cursor: pointer;">Send to <%=sendTo%></a>
                                    <%} else {%>
                                        Sent to <%=sendTo%> for processing
                                    <%}%>

                                <%}%>
                             <%} else {%>
                                        Sent to <%=sendTo%> for processing
                              <%}%>
                        <%}%>
                    </td>
                </tr>
                <%} else if (userId.equalsIgnoreCase(peOfficerUserId) && isPEUser && isTenderClosed && isSentToPEOfficer){%>


                        <tr
                            <%if (!allowTenderSharing) {%>
                            style="display: none;"
                            <%} else if (isMultiLots && isSentToTECChairPerson) {%>
                            style="display: none;"
                            <%}%>
                            >
                            <td width="25%" class="t-align-left ff"><%=strDocType%> Opening Report : </td>
                            <td width="75%" class="t-align-left">

                                <%if (!isMultiLots) {%>
                                
                                <a href="<%=contextPath%>/officer/TORCommon.jsp?tenderid=<%=tenderId%>&lotId=<%=torLotId%>">
                                  BOR
                                </a>
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                <%}%>

                                <%if (!isSentToTECChairPerson) {%>
                                <a href="TenderClosing.jsp?tenderId=<%=tenderId%>&type=CP&listId=">Send to TEC Chairperson</a>
                                <%}%>
                            </td>
                        </tr>
                <%}%>
                <tr>
                    <td width="25%" class="t-align-left ff">Tenderer's/Consultant's Hash : </td>
                    <td width="75%" class="t-align-left">
                        <a href="ViewTendererHash.jsp?tenderId=<%=tenderId%>">View</a>
                    </td>
                </tr>

                    <%if (isMegaHashVerified && isMember) {%>
<!--                    START: TO SHOW TENDER WITHDRAWALS AND SUBSTITUTION LINKS-->
<%
List<SPTenderCommonData> lstWithdrawnBids =
                    tenderCommonService1.returndata("getBidWithdrawalModificationList", tenderId, "withdrawal");
if(!lstWithdrawnBids.isEmpty()){
%>
 <tr>
                    <td width="25%" class="t-align-left ff">Tender Withdrawal Details: </td>
                    <td width="75%" class="t-align-left">
                        <a href="ViewModifiedBids.jsp?tenderid=<%=tenderId%>&view=withdrawal">View</a>
                    </td>
                </tr>
<%
}
lstWithdrawnBids = null;
%>

<%
List<SPTenderCommonData> lstModifiedBids =
                    tenderCommonService1.returndata("getBidWithdrawalModificationList", tenderId, "modify");
if(!lstModifiedBids.isEmpty()){
%>
 <tr>
                    <td width="25%" class="t-align-left ff">Tender Substitution / Modification Details: </td>
                    <td width="75%" class="t-align-left">
                        <a href="ViewModifiedBids.jsp?tenderid=<%=tenderId%>&view=modify">View</a>
                    </td>
                </tr>
<%
}
lstModifiedBids = null;
%>

<!--                    END: TO SHOW TENDER WITHDRAWALS AND SUBSTITUTION LINKS-->
<%}%>
                <%if (isTOCChairPerson && allowTenderSharing && allowTenderClose && !isTenderClosed){%>
                 <tr>
                     <td width="25%" class="t-align-left ff"><%=strDocType%> Opening Process : </td>
                     <td width="75%" class="t-align-left"><a href="TenderClosing.jsp?tenderId=<%=tenderId%>&type=C&parentLink=839">Close</a></td>
                 </tr>
                <%}%>
            </table>
            <%
                listingId = null;
                 tenderFrmCnt = null;
                 decryptedtenderFrmCnt = null;
                 peOfficerUserId = null;
                 memberRole = null;
                 tenderId = null;
                 userId = null;
            }
            %>
            <%}%>
        </div>
        <div>&nbsp;</div>
		    </div>
        </div>
        <div id="dialog-form" title="Enter Password">
<!--                    <form>-->
                            <label for="password">Password : </label>
                            <input type="password" name="password" id="password" value="" class="formTxtBox_1" autocomplete="off" />
                            <br/>
                            <p align="center" class="validateTips"></p>
<!--                    </form>-->
                </div>
        <input type="hidden" value="" id="decFormId"/>
        <div>
                    <input type="hidden" name="hdnPwd" id="hdnPwd"/>
                </div>
        <!--Dashboard Content Part End-->
        <!--Dashboard Footer Start-->
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <!--Dashboard Footer End-->
<script>



var password = $( "#password" ),
    allFields = $( [] ).add( password ),
    tips = $( ".validateTips" );

var check = false;
var encryptFlag = false;
function updateTips( t ) {
    tips
            .text( t )
            .addClass( "ui-state-highlight" );
    setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 1500 );
    }, 500 );
}
$("#dialog-form").dialog({
        autoOpen: false,
        height: 180,
        width: 280,
        modal: true,
        resizable: false,
        position: 'center',
        buttons: {
                "Verify Password": function() {
                        var bValid = true;

                        allFields.removeClass( "ui-state-error" );
                        bValid = bValid && checkLength( password, "password", 5, 16 );

                        if (bValid) {
                            if(document.getElementById("password").value!=""){
                                $.post("<%=request.getContextPath()%>/LoginSrBean", {param1:$('#password').val(),param2:'<%=session.getAttribute("userId")%>',funName:'verifyPWD'},  function(j){
                                    if(j.charAt(0)=="1"){
                                        document.getElementById("hdnPwd").value = j.toString().substring(2, j.toString().length);
                                        if($('#decFormId').val()=='0'){
                                            document.forms[0].action="DecryptAll.jsp?tenderId="+<%=request.getParameter("tenderid")%>+"&lotId="+<%=request.getParameter("lotId")%>+"&stat=open";
                                        }else{
                                            document.forms[0].action="Decrypt.jsp?tenderId=<%=request.getParameter("tenderid")%>&formId="+$('#decFormId').val();
                                        }
                                        document.forms[0].submit();
                                        updateTips("Done");
                                        $("#dialog-form").dialog("close");
                                    }else{
                                        updateTips("Please enter valid password");
                                        $('#password').val("");
                                    }
                                });
                            }
                        }
                }
        },
        close: function() {
                allFields.val("").removeClass("ui-state-error");
        }
});
    function generateAutoReport(tenderId,rptType)
            {
                $("#frmRptTORTER").attr("action","<%=request.getContextPath()%>/OpeningProcessServlet?action=autoReportGeneration");
                $("#action").val("autoReportGeneration");
                $("#rptTenderId").val(tenderId);
                $("#rptType").val(rptType);
                $("form#frmRptTORTER").submit();
                return false;
            }

function decryptAll(formId){
        if(<%=!(eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("1 stage-TSTM"))%>){
                if(<%=rtype.size() < 2%>){
                    alert("Generate Report Templates First");
                    return false;
                }
        }
    $("#decFormId").val(formId);
    $("#dialog-form").dialog("open");
    return false;
}
function checkLength( o, n, min, max ) {
        if(o.val()==""){
            o.addClass( "ui-state-error" );
            updateTips("Please enter password");
        }else{
            if ( o.val().length > max || o.val().length < min ) {
                    o.addClass( "ui-state-error" );
                    updateTips( "Length of " + n + " must be between " +
                            min + " and " + max + "." );
                    return false;
            } else {
                    return true;
            }
        }
}

function checkRegexp( o, regexp, n ) {
        if ( !( regexp.test( o.val() ) ) ) {
                o.addClass( "ui-state-error" );
                updateTips( n );
                return false;
        } else {
                return true;
        }
}
</script>
        <script type="text/javascript">
            if(<%=rptMessage != ""%>)
            {
                alert('<%=rptMessage%>');
            }
        </script>

 <input type="hidden" id="hdnIsTenderOpened" name="hdnIsTenderOpened" value="<%=isTenderOpened%>" />
 <input type="hidden" id="hdnEnvelopeCnt" name="hdnEnvelopeCnt" value="<%=intEnvelopcnt%>" />
 <input type="hidden" id="hdnShowDecrptAll" name="hdnShowDecryptAll" value="<%=showDecryptAll%>" />
 <input type="hidden" id="hdnIsExtensionLinkSeen" name="hdnIsExtensionLinkSeen" value="<%=isExtensionLinkSeen%>" />
 <input type="hidden" id="hdnIsExtensionApprovalPending" name="hdnIsExtensionApprovalPending" value="<%=isExtensionApprovalPending%>" />



 <script>
     $(document).ready(function(){
         //alert($('#hdnIsTenderOpened').val());
         //alert($('#hdnIsExtensionLinkSeen').val());

         if($('#hdnIsTenderOpened').val()=="true" && $('#hdnIsExtensionLinkSeen').val()!="true" && $('#hdnIsExtensionApprovalPending').val()!="true"){
             if (document.getElementById("dvWarning")!=null){
                 document.getElementById("dvWarning").innerHTML="<div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'>Don't print and share the information and documents to anybody. Please note that all the information is strictly confidential.<br /><br />Don't disclose any of the information with anybody except BOR to the tenderers who have participated in a tender. If confidentiality is breached then it will be treated as professional misconduct.</div>";
             }
         }

         if (document.getElementById("anchMemDec")!=null){
             if (document.getElementById("dvWarning")!=null){
                 //document.getElementById("dvWarning").innerHTML="<div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'>Don’t print and share the information and documents to anybody. Please note that all the information is strictly confidential. <br /><br />Don’t disclose any of the information with anybody except TOS to the tenderers who have participated in a tender. If confidentiality is breached then it will be treated as professional misconduct and it will be dealt with section 64 of the Public Procurement Act 2006.</div><div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'>Click on ‘Member’s name’ to give the consent.</div>";
                 document.getElementById("dvWarning").innerHTML="<div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'>Don't print and share the information and documents to anybody. Please note that all the information is strictly confidential.<br /><br />Don't disclose any of the information with anybody except BOR to the tenderers who have participated in a tender. If confidentiality is breached then it will be treated as professional misconduct.</div>";
             }
         }

         if($('#hdnEnvelopeCnt').val()=="0"){
             if (document.getElementById("dvEnvelopeMsg")!=null){
                 document.getElementById("dvEnvelopeMsg").innerHTML="<div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'>Evaluation Method rule not configured.</div>";
             }
         }


         if (document.getElementById("anchDecryptAll")!=null){
             document.getElementById("anchDecryptAll").style.display="none";

             if($('#hdnShowDecrptAll').val()=="true"){
                 document.getElementById("anchDecryptAll").style.display="block";
             }
         }

     });
 </script>
        <form method="post" id="frmRptTORTER" action="<%=request.getContextPath()%>/OpeningProcessServlet">
            <input type="hidden" id="rptTenderId" name="rptTenderId" value=""/>
                <input type="hidden" id="rptType" name="rptType" value=""/>
        </form>

    </body>
</html>
