<%-- 
    Document   : EvalNominationApproval
    Created on : Feb 19, 2011, 11:18:02 AM
    Author     : Swati
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.model.table.TblEvalNomination"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalNominationApprovalSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService,java.text.SimpleDateFormat" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>



<%--@page import="com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" --%>
<html>
    <head>
       <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

        String cuserId = "";
        String tenderid= "";
        
        AuditTrail auditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");


        boolean isInd = false;
        if(request.getParameter("tenderid")!= null)
            tenderid = request.getParameter("tenderid");
        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
             cuserId = session.getAttribute("userId").toString();
         }
          TenderCommonService tenderCommonServiceNominee = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
          
// to check whether it is evalution config type is individual or Team
        for (SPTenderCommonData isIndividual : tenderCommonServiceNominee.returndata("getEvalConfigType", tenderid, "")) {
                if(isIndividual.getFieldName1().equalsIgnoreCase("ind")) {
                        isInd = true;
                    }
        }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <% if(isInd){ %>
        <title>Declaration from TEC Members</title>
        <% }else{ %>
        <title>Approval for nomination of one member who will do Technical Evaluation on behalf of Team</title>
        <% } %>
        <!--jalert -->
        <!--link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script-->
        <script type="text/javascript">
            function chkConfirm()
            {
               var retVal = confirm("Do you wish to proceed, after this you will not be allowed to make changes in your selection...");
               if( retVal == true ){
                //  alert("after this you will not be allowed to make changes in your selection");
                      return true;
               }else{
                 // alert("User does not want to continue!");
                      return false;
               }

            }

            function chkforRemark(obj)
            {   
                if("I Disagree"==obj.options[obj.selectedIndex].text)
        	{
                    $('#rawId').show();
        	}else
        	{
                    $('#rawId').hide();
        	}
            }
        </script>
        
        <%! EvalNominationApprovalSrBean evalNominationApprovalSrBean = new EvalNominationApprovalSrBean(); %>
    </head>
    <body>
        <%
        String msg = "fail";
        if ("Submit".equals(request.getParameter("btnSubmit"))) {
            
             String nomineeAction = request.getParameter("nomineeActionTxt");
             String remark = "";
             if(request.getParameter("remarkTxt")!= null){
             remark = request.getParameter("remarkTxt").trim();
             }
             //SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
             SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
             Date d = new Date();
             
             String evalConfigId = "";
             for (SPTenderCommonData sptcd : tenderCommonServiceNominee.returndata("getEvalConfigInfo", tenderid, "")) {
                    evalConfigId = sptcd.getFieldName1();
             }
         /*
             String tecName = request.getParameter("tecNm");
             String peName = request.getParameter("peNm");
             
             TblEvalNomination tblEvalNomination = new TblEvalNomination();

             if(isInd){
                 tblEvalNomination.setEvaltype("ind");
             }else{
                tblEvalNomination.setEvaltype("Team");
             }
             
             tblEvalNomination.setEvalConfigId(Integer.parseInt(evalConfigId));
             tblEvalNomination.setTenderId(Integer.parseInt(tenderid));
             tblEvalNomination.setEnvelopeId(0);
             tblEvalNomination.setNominationDate(dateFormat1.parse(dateFormat1.format(d)));
             tblEvalNomination.setNomineeAction(nomineeAction);
             tblEvalNomination.setNomineeStatus("Live");
             tblEvalNomination.setRemarks(remark);
             tblEvalNomination.setNomineeUserId(Integer.parseInt(cuserId));
             
         msg = evalNominationApprovalSrBean.addNominationApproval(tblEvalNomination);*/
         AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
         CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
         CommonMsgChk commonMsgChk;
         String query = "nomineeAction='"+nomineeAction+"',nomineeActionDt=getdate(),remarks='"+remark+"'";
         String where = "evalConfigId = "+Integer.parseInt(evalConfigId)+" and nomineeUserId = "+Integer.parseInt(cuserId)+" and nomineeStatus = 'Live' and isCurrent = 'Yes'" ;
         
         // Added for Audit Trail
        commonXMLSPService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
        commonXMLSPService.setModuleName(EgpModule.Evaluation.getName());
        String auditValues[]=new String[4];
        auditValues[0]="tenderId";
        auditValues[1]= request.getParameter("tenderid");
        if(isInd)
         {
             auditValues[2]="Declaration for Evaluation Process Configuration";
         }
        else
         {
             auditValues[2]="Consent for Nomination given by TEC Member - "+nomineeAction;
         }
       
        auditValues[3]= nomineeAction;
        
        commonMsgChk =addUpdate.addUpdOpeningEvaluation("UpdateEvalNomination",nomineeAction,objSDF.format(new Date()),remark,evalConfigId,cuserId,"Live","Yes","","","","","","","","","","","","").get(0);

        
        //commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalNomination", query, where,auditValues).get(0);

        String moduleName=EgpModule.APP.getName();

        int tempID = Integer.parseInt(auditValues[1]);
        if(tempID!=0){
                //System.out.println("----------- Before call generate Audit ----------------------");
                makeAuditTrailService.generateAudit(auditTrail,tempID, auditValues[0],moduleName, auditValues[2], auditValues[3]);
               // System.out.println("----------- After call generate Audit ----------------------");
            }
        
         if (commonMsgChk.getFlag().equals(true)) {
            msg = "sucess";
            response.sendRedirect("EvalComm.jsp?tenderid="+tenderid+"&msgOfNominee="+ msg);
         }
        /* if (msg.equals("Values Added")) {
          msg = "sucess";
          response.sendRedirect("EvalComm.jsp?tenderid="+tenderid+"&msgOfNominee="+ msg);
          } 
        tblEvalNomination = null;
        evalNominationApprovalSrBean = null;*/
        }

      %>
         <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">
             <div class="pageHead_1">Tender Dashboard</div>
             <%pageContext.setAttribute("tenderId", tenderid);%>
             <%@include file="../resources/common/TenderInfoBar.jsp" %>
             <div>&nbsp;</div>
              <%@include  file="../officer/EvalCommCommon.jsp" %>
    <%    if(!isInd) { %>
      <form action="EvalNominationApproval.jsp?tenderid=<%=tenderid%>" method="post">
        <table width="100%" cellspacing="0" class="tableList_1">
          <tr>
              <th colspan="4" class="t-align-left">Approval for nomination of one member who will do Technical Evaluation on behalf of Team</th>
          </tr>
          <tr>
              <% for (SPTenderCommonData sptcd : tenderCommonService.returndata("getEmployeeNamefromUserId", tenderid, "")) { %>
              <td width="15%" class="t-align-left ff">Name of TEC Member :</td>
              <td width="35%" class="t-align-left"> <label name="tecNm" id="tecNm" ><%= sptcd.getFieldName1() %></label></td>
              <td width="16%" class="t-align-left ff">Designation :</td>
              <td width="30%" class="t-align-left"><%= sptcd.getFieldName2() %></td>

              <% } %>
          </tr>
          <tr>
              <% for (SPTenderCommonData sptcd : tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderid, "")) { %>
              <td width="16%" class="t-align-left ff">PE Office :</td>
              <td width="84%" class="t-align-left" colspan="3"><label name="peNm" id="peNm"><%= sptcd.getFieldName3() %></label></td>
              <% } %>
          </tr>
           <tr>
              <td width="16%" class="t-align-left ff">Important Message :</td>
              <td width="84%" class="t-align-left" colspan="3"><label style="color: red;">By agreeing for Team Evaluation, you hereby agree that will stand by the evaluation done by the nominated member in totality.</label> </td>
          </tr>
           <tr>
              <td width="16%" class="t-align-left ff">Do you Agree / Disagree :</td>
              <td width="84%" class="t-align-left" colspan="3">
                  <select id="nomineeActionTxt" name="nomineeActionTxt" class="formTxtBox_1">                      
                      <option value="Agreed">I Agree</option>
                      <option value="Disagreed">I Disagree</option>
                  </select>
              </td>
          </tr>
           <tr>
               <td width="16%" class="t-align-center ff" colspan="4">
                 <label class="formBtn_1">
                     <input name="btnSubmit" id="btnSubmit" type="submit" value="Submit" onclick="return chkConfirm();" />
                 </label>
               </td>
          </tr>

      </table>
 </form>

 <% }else if(isInd){
     // if IT is individual then this loop is display
    %>
 
  <form action="EvalNominationApproval.jsp?tenderid=<%=tenderid%>" method="post">
        <table width="100%" cellspacing="0" class="tableList_1">
          <tr>
              <th colspan="2" class="t-align-left">Declaration from TEC Members</th>
          </tr>
           <tr>
              <td width="16%" class="t-align-left ff">Important Message :</td>
              <td width="84%" class="t-align-left"><label style="color: red;">By agreeing for Individual Evaluation, you hereby agree that will stand by the evaluation.</label> </td>
          </tr>
           <tr>
              <td width="16%" class="t-align-left ff">Do you Agree / Disagree :</td>
              <td width="84%" class="t-align-left">
                  <select id="nomineeActionTxt" name="nomineeActionTxt" onchange="chkforRemark(this);"  class="formTxtBox_1">                      
                      <option value="Agreed">I Agree</option>
                      <option value="Disagreed">I Disagree</option>
                  </select>
              </td>
          </tr>
          <tr id="rawId" style="display: none;">
              <td class="t-align-left ff" width="16%">Remark :</td>
              <td class="t-align-left"><textarea id="remarkTxt" name="remarkTxt"> </textarea></td>
          </tr>
           <tr>
               <td width="16%" class="t-align-center ff" colspan="2">
                 <label class="formBtn_1">
                     <input name="btnSubmit" id="btnSubmit" type="submit" value="Submit" onclick="return chkConfirm();" />
                 </label>
               </td>
          </tr>
        
      </table>
 </form>
 <% } //end else if %>
        </div>
           <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>
