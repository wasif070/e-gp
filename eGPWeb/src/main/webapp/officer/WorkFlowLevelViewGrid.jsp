<%-- 
    Document   : WorkFlowLevelViewGrid
    Created on : Nov 18, 2010, 4:39:21 PM
    Author     : test
    Modified By: Dohatec
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementRole"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>WorkFlow View</title>
    </head>
    <%
        response.setHeader("Expires", "-1");
        Map<String,String> listEmpTras = new HashMap<String, String>();
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
    <body>
        <%
     String fieldName = "WorkFlowRuleEngine";
          String eventid =  request.getParameter("eventid");
           String activityid = request.getParameter("activityid");
          int objectid = 0;
            int childid = 0;
           String objappid1 = request.getParameter("appId");
           if(objappid1 != null){
               objectid = Integer.parseInt(objappid1);
               childid = objectid;
               }else{
               objectid = Integer.parseInt(request.getParameter("objectid"));
               childid = Integer.parseInt(request.getParameter("childid"));
               }
               if(Integer.parseInt(activityid) == 4 || Integer.parseInt(activityid) == 1)
                   childid = objectid;
     String moduleName = null;
     String eventName = null;
      String[] startsBy = null;
      String[] endsBy = null;


     int uid = 0;
      String userid = "";
       if(session.getAttribute("userId") != null){
           Integer ob1 = (Integer)session.getAttribute("userId");
           uid = ob1.intValue();
          userid  = String.valueOf(uid);
       }
     
      List<TblProcurementRole> procurementRoles = null;
       WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
       List<TblWorkFlowLevelConfig> tblworkFlowLevelconfig = null;
       
       
       
       
     List<CommonAppData> editdata =  workFlowSrBean.editWorkFlowData(fieldName , eventid, "");
      Iterator it = editdata.iterator();
      StringBuffer stby = new StringBuffer();
      StringBuffer endby = new StringBuffer();
      StringBuffer nstby = new StringBuffer();
      StringBuffer nendby = new StringBuffer();
        while(it.hasNext()){
           CommonAppData  commonAppData= (CommonAppData) it.next();
           moduleName = commonAppData.getFieldName1();
           eventName = commonAppData.getFieldName2();
           stby.append(commonAppData.getFieldName3()+",");
           endby.append(commonAppData.getFieldName4()+",");

            }
           if(stby.length()>0){

                    String str = stby.toString();
                    String[] str1 = str.split(",");
                    for(int j=0;j<str1.length;j++){
                        boolean check = false;
                        if(nstby.length()>0){
                         String str2= nstby.toString();
                        for(int k=0;k<str2.split(",").length;k++){
                            if(str1[j].equals(str2.split(",")[k])){
                                check = true;
                                  }
                            }
                           if(!check){
                               nstby.append(str1[j]+",");
                               }
                         continue;
                        }else nstby.append(str1[j]+",");
                    }
                     nstby.deleteCharAt(nstby.length()-1);
                    }
                  if(endby.length()>0){
                      String str = endby.toString();
                    String[] str1 = str.split(",");
                    for(int j=0;j<str1.length;j++){
                        boolean check = false;
                        if(endby.length()>0){
                         String str2= nendby.toString();
                        for(int k=0;k<str2.split(",").length;k++){
                            if(str1[j].equals(str2.split(",")[k])){
                                check = true;
                                  }
                            }
                           if(!check){
                               nendby.append(str1[j]+",");
                               }
                         continue;
                        }else nendby.append(str1[j]+",");
                    }
                       nendby.deleteCharAt(nendby.length()-1);
                       }

            String str1 = nstby.toString();
             startsBy = str1.split(",");
            String str2 = nendby.toString();
             endsBy = str2.split(",");
             procurementRoles =  workFlowSrBean.getRoles();
             int editObjectid = objectid;
             int editChildid = childid;
             if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                editObjectid = childid;
             }
             Date curdate = new Date();
             short editactivityid = Short.parseShort(activityid);
             List<CommonMsgChk> returnMsg = null;
             //if(editactivityid == 7)
                // returnMsg  =  workFlowSrBean.addUpdWffileprocessing(7, editObjectid,editChildid, uid, "Insert",
                 // "Insert", 1, "system", curdate, "");
              tblworkFlowLevelconfig =
                     workFlowSrBean.editWorkFlowLevel(editObjectid, editChildid, editactivityid);
             

    %>
         <table width="100%" cellspacing="0" class="tableList_1">
    <tr>
      <th>Level No.</th>
      <th>Workflow Role</th>
      <th>Procurement Role</th>
      <th>Name of Official and Designation</th>
      <th>File On Hand</th>

    </tr>
    <%  if(tblworkFlowLevelconfig.size()>0){ %>
    <tr>
        <td class="t-align-center" style="color: #6E6E6E;" >1</td>
      <td style="color: #6E6E6E;">Initiator</td>
      <td style="color: #6E6E6E;">

                    <%
                    String procureRole="";
                    TblWorkFlowLevelConfig wfl= tblworkFlowLevelconfig.get(0);
                    Iterator rls =  procurementRoles.iterator();
                    while(rls.hasNext()){
                      TblProcurementRole procurementRole =(TblProcurementRole)rls.next();
                     if(wfl.getProcurementRoleId() == procurementRole.getProcurementRoleId()) {
/*aprojit-Start*/       if(procurementRole.getProcurementRole().equalsIgnoreCase("HOPE"))
                             procureRole="HOPA";
                        else if(procurementRole.getProcurementRole().equalsIgnoreCase("PE"))
                             procureRole="PA"; 
                         else
                             procureRole=procurementRole.getProcurementRole();
/*aprojit-End*/     out.write("<label id='"+procurementRole.getProcurementRoleId()+"' >"+procureRole+"</label>");
                     }
                    }
               TblLoginMaster  loginMaster = wfl.getTblLoginMaster();

             String str=workFlowSrBean.getOfficialAndDesignationName(loginMaster.getUserId());
             listEmpTras.put(String.valueOf(loginMaster.getUserId()), String.valueOf(loginMaster.getTblUserTypeMaster().getUserTypeId()));


%>

       </td>
                    <td style="color: gray; text-align: center;"><label id="uservalue1"><%=str%></label>
                        <input type="hidden" name="txtuserid" value="<%=loginMaster.getUserId()%>" id="txtuserid1" /></td>
                    <td class="t-align-center" style="color: gray;">
                        <% if(wfl.getFileOnHand().equalsIgnoreCase("YES")){
                            out.write("YES");
                                   }else{
                            out.write("NO");
                                   }

                        %>

                    </td>
    </tr>
                    <%
                    if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                        objectid = childid;
                    }
                    String donor = workFlowSrBean.isDonorReq(eventid,
                  Integer.valueOf(objectid));
                  String[] sarr =  donor.split("_");
                  int l = 2;
                   int c = 2;
                  String colorchange = "style='background-color:#F0E68C;' ";
                  int val = tblworkFlowLevelconfig.size()-2;
                  
                  for(int i=1;i<=val;i++){

                   %>
    <tr <% if(c == l && val > 1) out.print(colorchange); else if(val == 1) out.print(colorchange); %>>

      <td class="t-align-center" style="color: #6E6E6E;"><%=l%></td>
      <td style="color: #6E6E6E;">

          <%
           boolean check = false;
           TblWorkFlowLevelConfig revwfl =   tblworkFlowLevelconfig.get(i);
           if(revwfl.getWfRoleId() == 4){
          out.write("<option  value='Donor'>Development Partner</option>");
          check = true;
          }else out.write("<option value='Donor'>Reviewer</option>");
           %>

      </td>
      <td style="color: #6E6E6E;">

                   <%
                   String procureRoleR="";  
                   TblWorkFlowLevelConfig revwflc =   tblworkFlowLevelconfig.get(i);
                    Iterator rev =  procurementRoles.iterator();
                    while(rev.hasNext()){
                        TblProcurementRole procurementRole =(TblProcurementRole)rev.next();
                         if(procurementRole.getProcurementRole().equalsIgnoreCase("HOPE")){procureRoleR="HOPA";}else if(procurementRole.getProcurementRole().equalsIgnoreCase("PE")){procureRoleR="PA";}else{procureRoleR=procurementRole.getProcurementRole();}
                            if(procurementRole.getProcurementRoleId() == revwflc.getProcurementRoleId())
                                 out.write("<label id='"+procurementRole.getProcurementRoleId()+"' >"+procureRoleR+"</label>");

                          }
                     %>


                     <label id="labelreviewer<%=l%>"></label>
      </td>
      <td style="color: #6E6E6E; text-align: center;">
        <%
        TblLoginMaster  lm = revwflc.getTblLoginMaster();
        String userval = "";
        
        if(!check)
        userval =workFlowSrBean.getOfficialAndDesignationName(lm.getUserId());
        else userval = workFlowSrBean.getOfficeNameAndDesignationByDP(lm.getUserId());
        out.write("<label id='uservalue"+l+"'>"+userval+"</label>");
       out.write("<input type='hidden' name='txtuserid' value='"+lm.getUserId()+"' id='txtuserid"+l+"'");
       listEmpTras.put(String.valueOf(lm.getUserId()), String.valueOf(lm.getTblUserTypeMaster().getUserTypeId()));
%>

      </td>
        <td class="t-align-center" style="color: gray;">
            <% if(revwflc.getFileOnHand().equalsIgnoreCase("YES")){
                out.write("YES");
                       }else{
                out.write("NO");
                       }
            %>

        </td>
    </tr>
    <%
     if(c == l)
       c+=2;
       l++;
       }
  %>
    <tr  <% if(c == l && val != 1) out.print(colorchange); %>>
      <td class="t-align-center" style="color: #6E6E6E;"><%=l%></td>
      <td style="color: #6E6E6E;">Approver</td>
      <td style="color: #6E6E6E;">

            <%      
                    String procureRoleA="";
                    TblWorkFlowLevelConfig wflc =   tblworkFlowLevelconfig.get(l-1);
                    Iterator endsrls =  procurementRoles.iterator();
                    while(endsrls.hasNext()){
                      TblProcurementRole procurementRole =(TblProcurementRole)endsrls.next();
                           if(procurementRole.getProcurementRoleId() == wflc.getProcurementRoleId()){
/*aprojit-Start*/              if(procurementRole.getProcurementRole().equalsIgnoreCase("HOPE"))
                                    procureRoleA="HOPA";
                               else if(procurementRole.getProcurementRole().equalsIgnoreCase("PE"))
                                    procureRoleA="PA"; 
                               else
                                   procureRoleA=procurementRole.getProcurementRole();
 /*aprojit-End*/           out.write("<label id='"+procurementRole.getProcurementRoleId()+"' >"+procureRoleA+"</label>");
                           }
                      }
                    %>

      </td>
      <td style="color: #6E6E6E; text-align: center;">
          <%
          TblLoginMaster tlm =  wflc.getTblLoginMaster();
          String str3=workFlowSrBean.getOfficialAndDesignationName(tlm.getUserId());
          out.write("<label id='uservalue"+l+"'>"+str3+"</label>");
        out.write("<input type='hidden' name='txtuserid' value='"+tlm.getUserId()+"' id='txtuserid"+l+"' ");
        listEmpTras.put(String.valueOf(tlm.getUserId()), String.valueOf(tlm.getTblUserTypeMaster().getUserTypeId()));
%>
          </td>
       <td class="t-align-center" style="color: gray;">
            <% if(wflc.getFileOnHand().equalsIgnoreCase("YES")){
                out.write("YES");
                       }else{
                out.write("NO");
                       }
        
            %>

        </td>
    </tr>
     <%  }else{ %>
     <tr>
         <td colspan="5" class="t-align-center">Workflow yet not configured</td>


     </tr>
     <% } %>
  </table>
    </body>
    <%
        String str ="";
        if(!"".equals(request.getParameter("isSelected"))){
        str = request.getParameter("isSelected");}
        if(!"no".equalsIgnoreCase(str))
        {
    %>
        <script type="text/javascript">
            var headSel_Obj = document.getElementById("headTabWorkFlow");
            if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    <%
        }
        request.setAttribute("listEmpTras", listEmpTras);
    %>
    
    <jsp:include page="../resources/common/EmpTransferHist.jsp" />

</html>
