<%-- 
    Document   : ViewL1Report
    Created on : Feb 14, 2011, 7:02:42 PM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View L1 Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
    </head>
    <body>
        <div class="dashboard_div">

            <%  String tenderId, strSt="";
                tenderId = request.getParameter("tenderId");
                strSt=request.getParameter("st");
                String sentBy = "", role = "";
                String aaName="", aaUserId="";
                boolean isSentToAA=false;
                if(session.getAttribute("userId") != null)
                  sentBy = session.getAttribute("userId").toString();

                CommonSearchService commonSearchService1 = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                String xmldata = "";
                String remark = "";


                List<SPTenderCommonData> lstgetAAName = tenderCS.returndata("getTenderConfigData",tenderId,null);
                if(!lstgetAAName.isEmpty()){
                    aaName=lstgetAAName.get(0).getFieldName2();
                    aaUserId=lstgetAAName.get(0).getFieldName7();
                }

                 List<SPTenderCommonData> lstgetSentToAAStatus = tenderCS.returndata("getSentToAAEntry",tenderId,null);
                if(!lstgetSentToAAStatus.isEmpty()){
                    if("yes".equalsIgnoreCase(lstgetSentToAAStatus.get(0).getFieldName1())){
                        isSentToAA=true;
                    }
                }

                // IF ENTRY FOUND IN "tbl_EvalSentQueToCp" TABLE IN CASE OF "tec" MEMBER.
                // GET THE TEC-MEMBER COUNT.
                List<SPTenderCommonData> isTEC = tenderCS.returndata("IsTEC", tenderId, sentBy);
                int isTECCnt = isTEC.size();

                // CHECK FOR USER-ROLL AND SET THE LINK AS PER THE ROLL.
                List<SPCommonSearchData> roleList = commonSearchService1.searchData("EvalMemberRole", tenderId, sentBy, null, null, null, null, null, null, null);

                if (!roleList.isEmpty()){
                    role = roleList.get(0).getFieldName2();
                }

            %>
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <form id="frmViewL1Report" name="frmViewL1Report"  method="post">
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            
            <div class="contentArea_1">
                  <%
                String referer = "", orgReferer="";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                }
                
               %>
                <div class="pageHead_1">View L1 Report
                <span style="float: right;" ><a href="<%=referer%>" class="action-button-goback">Go Back</a></span>
                </div>

                <%
                        pageContext.setAttribute("tenderId", tenderId);

                        String tenderid = tenderId;
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
                <%if(!"TEC".equalsIgnoreCase(request.getParameter("comType"))) { %>
                <jsp:include page="officerTabPanel.jsp" >
                     <jsp:param name="tab" value="7" />
                </jsp:include>
                <% } %>
                <div class="tabPanelArea_1">              

                <% List<SPTenderCommonData> companyList = tenderCS.returndata("getFinalSubComp",tenderId,"0"); %>

                      <%
                        if (!companyList.isEmpty()) {
                            //if ("cp".equalsIgnoreCase(role)) {
                                 if (!"cl".equalsIgnoreCase(request.getParameter("st"))) {
                                     List<SPTenderCommonData> evalMemStatusCount = tenderCS.returndata("GetEvalMemfinalStatusCount", tenderId, "0");
                            // IF THIS COUNT MATCHED WITH BIDDERS COUNT THEN SHOW BELOW BUTTON
                            if (Integer.parseInt(evalMemStatusCount.get(0).getFieldName1()) == companyList.size()) {
                      %>

                       <table width="100%" cellspacing="0" class="tableList_1  t_space">
                           
                           <tr>
                               <th>Sl. No.</th>
                               <th width="55%" class="t-align-center">Report Name</th>
                               <th width="40%" >View</th>
                           </tr>
                           
                            <%

                                    String tendId= request.getParameter("tenderId");
                                    ReportCreationService creationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");
                                    String evalType = creationService.getTenderEvalType(tendId);
                                    String pgName=null;
                                    if(evalType.equalsIgnoreCase("item wise")){
                                        pgName = "ItemWiseReport";
                                    }else{
                                        pgName = "TenderReport";
                                    }
                                    int RowCnt=0;
                                    for(Object[] obj : creationService.getRepNameId(tendId)){
                                        RowCnt++;
                             %>
                             <tr
                                 <%if(Math.IEEEremainder(RowCnt,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                        <%}%>
                                 >
                                 <td class="t-align-center"></td>
                            
                                <td>
                                    <%=obj[1]%>
                                </td>
                            
                                 <td class="t-align-center">
                                     <a href="<%=pgName%>.jsp?tenderid=<%=tendId%>&repId=<%=obj[0]%>&isEval=y">View</a>
                                 </td>
                             </tr>
                        <%} 
                           if (RowCnt==0){%>
                           <tr>
                               <td colspan="3">No records found.</td>
                           </tr>
                            
                        <%}%>
                     </table>

                      <%
                                }
                            } // END IF "EVAL. REPORT" TAB
                        //} // END IF "CP" ROLE
                       } // END IF OF "NO DATA FOUND"
                     %>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
        </form>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
