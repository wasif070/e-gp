<%-- 
    Document   : ViewConfKeyInformation
    Created on : Nov 15, 2010, 5:53:36 PM
    Author     : Kinjal Shah
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Official Cost Estimate, Approving Authority, SBD</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>

    </head>
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->

            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            </div>
            <div class="contentArea_1">
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <div class="pageHead_1">View Official Cost Estimate, Approving Authority, SBD<span style="float:right;"><a href="Notice.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back To Dashboard</a></span></div>


                <!--	<div id="action-tray" align="right">
		<ul>
        	<li><a href="Notice.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back To Dashboard</a></li>
                        </ul>
	</div>-->

                <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div style="border-top:1px solid #A8DA7F;" class="t_space"></div>
                <%
                            TenderSrBean tenderSrBean = new TenderSrBean();
                            List<Object[]> tenConfig = tenderSrBean.getConfiForTender(Integer.parseInt(request.getParameter("tenderId")));
                            tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                            for (SPTenderCommonData sptcd : tenderCommonService.returndata("getTenderConfigData", id, null)) {

                %>

                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                    <tr>
                        <td width="30%" class="ff">Estimated Cost (in million Nu.) :</td>
                        <%
                            String estCost = sptcd.getFieldName1();
                            BigDecimal estCostBigDecimal = new BigDecimal(estCost);
                            estCostBigDecimal = estCostBigDecimal.divide(new BigDecimal("1000000"),8,BigDecimal.ROUND_HALF_UP);
                            estCost = estCostBigDecimal.toString();
                            
                        %>
                        <td><%=estCost%></td>
                    </tr>
                    <tr>
                        <td class="ff">Approving Authority :</td>
                        <td><%if (sptcd.getFieldName2() != null) {%><%=sptcd.getFieldName2()%><% }%></td>
                    </tr>
                    <!--	<tr>
	  <td class="ff">Document access : </td>
                          <td><%=sptcd.getFieldName3()%></td>
	</tr>-->
                    <tr>
                        <td class="ff">Standard Bidding Document : </td>
                        <td> <% if (sptcd.getFieldName6() != null) {%><%=sptcd.getFieldName6()%><% }%></td>
                    </tr>
                    <%  if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                            %>
                        <tr>
                            <td class="ff">Domestic Preference Requires</td>
                            <td><%=listDP.get(0).getFieldName2()%></td>
                        </tr>
                        <% if(listDP.get(0).getFieldName2().equalsIgnoreCase("Yes")){%>
                        <tr>
                            <td class="ff">Domestic Preference in %</td>
                            <td><%=listDP.get(0).getFieldName3()%></td>
                        </tr>
                        <% } %>
                    <%} if (!tenConfig.isEmpty() && tenConfig.get(0)[3].toString().equalsIgnoreCase("Yes")) {%>
                    <tr>
                        <td class="ff">Tender Validity in No. of Days :</td>
                        <td><%=sptcd.getFieldName4()%></td>
                    </tr>
                    <% } if (!tenConfig.isEmpty() && tenConfig.get(0)[2].toString().equalsIgnoreCase("Yes")) {%>
                    <tr>
                        <td class="ff">Tender Security Validity in No. of Days : </td>
                        <td><%=sptcd.getFieldName5()%></td>
                    </tr>
                    <% } %>
                </table>
                <%}
                    ;%>
                <div>&nbsp;</div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

