<%--
    Document   : Configuration
    Created on : Dec 29, 2010, 4:09:23 PM
    Author     : rajesh, pulkit,
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommitteMemDtBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>



    <head>
        <%
                    HttpSession session1 = request.getSession();
                    if ((session1.getAttribute("userId") == null || session1.getAttribute("userName") == null || session1.getAttribute("userTypeId") == null)) {
                        //response.sendRedirectFilter(request.getContextPath() + "/Logout.jsp"); //Index.jsp
                    }
        %>

        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    TenderCommitteSrBean tenderCommitteSrBean = new TenderCommitteSrBean();
                    List totalTecMem = (List) tenderCommitteSrBean.committeUser(Integer.parseInt(request.getParameter("tenderid")), "TECMember");
                    List totalTscMem = (List) tenderCommitteSrBean.committeUser(Integer.parseInt(request.getParameter("tenderid")), "TSCMember");
                    CommitteMemDtBean objCommitteMemDtBeanTEC = null;
                    CommitteMemDtBean objCommitteMemDtBeanTSC = null;


                    List<SPCommonSearchData> getConfigurationDetails = null;

                    String configType = "";
                    String evalCommiittee = "";
                    String isPostQua = "";
                    String action = "";
                    String tecTEC_UserId = "";
                    String tscTEC_UserId = "";
                    String tscReq = "";
                    String TEC_ChairPerson_UserId = "";
                    boolean isConfigEntry=false, isTSCFormed=false;
                    

                    CommonSearchService objCommonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");

                    List<SPCommonSearchData> getTECChairPersonUserId = objCommonSearchService.searchData("getTECChairPersonUserId", request.getParameter("tenderid"), null, null, null, null, null, null, null, null);

                    if(!getTECChairPersonUserId.isEmpty()){
                        TEC_ChairPerson_UserId=getTECChairPersonUserId.get(0).getFieldName1();
                    }

                    if (request.getParameter("action") != null && request.getParameter("action").trim().length() != 0) {
                        if (request.getParameter("action").equalsIgnoreCase("update")) {
                            //CommonSearchService objCommonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            getConfigurationDetails = objCommonSearchService.searchData("chkConfigStatus", request.getParameter("tenderid"), null, null, null, null, null, null, null, null);
                            if (getConfigurationDetails.get(0).getFieldName2() != null) {
                                configType = getConfigurationDetails.get(0).getFieldName2();
                                evalCommiittee = getConfigurationDetails.get(0).getFieldName3();
                                isPostQua = getConfigurationDetails.get(0).getFieldName5();
                                action = request.getParameter("action");
                                tecTEC_UserId = getConfigurationDetails.get(0).getFieldName6();
                                tscTEC_UserId = getConfigurationDetails.get(0).getFieldName7();
                                tscReq = getConfigurationDetails.get(0).getFieldName8();
                                
                            }
                        } else {
                            action = "add";
                            //CommonSearchService objCommonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            getConfigurationDetails = objCommonSearchService.searchData("chkTSCCommitteeStatus", request.getParameter("tenderid"), null, null, null, null, null, null, null, null);
                            if(!getConfigurationDetails.isEmpty()){
                                if ("no".equalsIgnoreCase(getConfigurationDetails.get(0).getFieldName1())){
                                    if ("yes".equalsIgnoreCase(getConfigurationDetails.get(0).getFieldName3())){
                                        isTSCFormed=true;
                                    }
                                } else {
                                    isConfigEntry=true;
                                }
                            }
                        }

                    }


        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>



        <script type="text/javascript">
            var tscMemAvail = false;
            var tscMemReq = false;
            var isInd=false;
            var totalTecMem = <%=totalTecMem.size()%>;
            var totalTscMem = <%=totalTscMem.size()%>;
            <%if (totalTscMem.size() > 0) {%>
                tscMemAvail = true;
                tscMemReq = true;                
            <%} else if (totalTscMem.size() <= 0) {%>
                tscMemAvail = false;                
            <%}%>
                
                $(function() {
                
                    if(tscMemAvail==false){                        
                        document.getElementById('TSCTR').style.display = 'none';
                    }

                    if($('#ind').attr('checked')==true){
                        document.getElementById('TECTR').style.display = 'none';                        
                        document.getElementById('TSCTR').style.display = 'none';
                        document.getElementById('tscTEC_UserId').value=0;
                        document.getElementById('tecTEC_UserId').value=0;
                        document.getElementById('MAPFORMS').style.display='table-row';
                        document.getElementById('MAPFORMS').style.visibility='visible';
                        isInd = true;
                    }
                    if($('#TSCno').attr('checked')==true){
                        document.getElementById('TSCTR').style.display='none';
                        document.getElementById('tscTEC_UserId').value=0;
                        document.getElementById('MAPFORMS').style.display='none';
                        document.getElementById('MAPFORMS').style.visibility='hidden';

                    }

                    $("input:radio[name=TSCyes]").click(function() {
                        //alert("hello");
                        document.getElementById("spanTSCyes").innerHTML="";
                        //TSC Formation Required
                        var value = $(this).val();
                        //  alert("TSC REq :"+value);
                        if(value=='no'){
                            
                            document.getElementById('TSCTR').style.display = 'none';
                            document.getElementById('tscTEC_UserId').value=0;
                            if($('#ind').attr('checked')==true){
                               document.getElementById('MAPFORMS').style.display='table-row';
                               document.getElementById('MAPFORMS').style.visibility='visible';
                            }else{
                                //alert('hide');
                              document.getElementById('MAPFORMS').style.display='none';
                              document.getElementById('MAPFORMS').style.visibility='hidden';
                            }
                            tscMemReq = false;
                        }else
                        {
                            //alert('hello');
                            var tenderid=getUrlVars()["tenderid"];
                            var comType=getUrlVars()["comType"];

                            if(tscMemAvail==false){
                                $.alerts._show("TSC Formation", 'Do you really want to Navigate to TSC Formation page?', null, 'confirm', function(r) {
                                    if(r == true){
                                        window.location = "EvalCommTSC.jsp?tenderid="+tenderid+"&comType="+comType;
                                    }else{
                                        $("input:radio[id=TSCno]").attr("checked","true");                                        
                                        tscMemReq = false;
                                    }
                                });
                            }else{
                                //alert("TSC Already configured");                                
                                document.getElementById('MAPFORMS').style.display='table-row';
                                document.getElementById('MAPFORMS').style.visibility='visible';
                               if(document.getElementById("team").checked==true){
                                document.getElementById('TECTR').style.display = 'table-row';
                                document.getElementById('TECTR').style.visibility = 'visible';
                                document.getElementById('TSCTR').style.display = 'table-row';
                                document.getElementById('TSCTR').style.visibility='visible';
                               }
                            }
                        
                        }
                    });

                    //Evaluation Type
                    $("input:radio[name=ind]").click(function() {

                        document.getElementById("spanind").innerHTML="";

                        var value = $(this).val();
                        //alert(value+" is selected");
                        if(value=='ind'){
                            // alert(value);
                            document.getElementById('TECTR').style.display = 'none';
                            document.getElementById('TECTR').style.visibility = 'hidden';
                            document.getElementById('TSCTR').style.display = 'none';
                            document.getElementById('MAPFORMS').style.display='table-row';
                            document.getElementById('MAPFORMS').style.visibility='visible';
                            tscMemReq=false;
                            getAllTecMem();
                        }else
                        {
                            document.getElementById('TECTR').style.display = 'table-row';
                            document.getElementById('TECTR').style.visibility = 'visible';                               

                            if($('#TSCno').attr('checked')==true){
                                //alert($('#TSCno').attr('checked'));
                                isInd=false;
                                document.getElementById('TSCTR').style.display = 'hidden';
                                document.getElementById('MAPFORMS').style.display='none';
                                document.getElementById('MAPFORMS').style.visibility='hidden';
                                tscMemReq= false;
                            }else{
                                if(tscMemAvail==true  ){
                                    document.getElementById('TSCTR').style.visibility = 'visible';
                                    document.getElementById('TSCTR').style.display = 'table-row';
                                }
                            }
                        }
                    });
                   
                    //Select TEC Team Member
                    $("input:radio[name=TecTEC]").click(function() {
                        document.getElementById("spanTECTec").innerHTML="";
                    });

                    //Select TSC Team Member
                    $("input:radio[name=TscTEC]").click(function() {
                        document.getElementById("spanTSCTec").innerHTML="";
                    });

                    //IS Post Qualification Required
                    $("input:radio[name=ispost]").click(function() {
                        document.getElementById("spanispost").innerHTML="";
                    });
               
                });

                function getUrlVars() {
                    var vars = {};
                    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                        vars[key] = value;
                    });
                    return vars;
                }

                function getAllTecMem(){
                    var a;
                    var tempStr='';
                    for(a=0;a< <%=totalTecMem.size()%>;a++){
                        tempStr  = tempStr+document.getElementById('Tec_'+a).value.trim()+",";                        
                    }
                    document.getElementById('allTecMem').value= tempStr;                    
                }

                function Validation(){
                    //alert("in");
                    var check=true;

                    if($("input:radio[id=TSCyes]").attr("checked")==false && $("input:radio[id=TSCno]").attr("checked")==false){                        
                        document.getElementById("spanTSCyes").innerHTML="Please select TSC Formation";
                        check=false;
                    }
                    //alert("1");
                    if($("input:radio[id=ind]").attr("checked")==false && $("input:radio[id=team]").attr("checked")==false){
                        //alert("ind/team is not Selected");
                        document.getElementById("spanind").innerHTML="Please select Evaluation Type";
                        check=false;
                        isInd = false;
                    }else if($('#ind').attr('checked')==true){
                        //alert("isInd====>  "+isInd);
                        isInd = true;
                    }

                    if($("input:radio[id=team]").attr("checked")==true){
                        //alert("Ind not selected")
                        var i;
                        var isTecSelected = false;
                        for(i=0;i<totalTecMem;i++){
                            //       //alert(i);
                            if($('#Tec_'+i).attr('checked')==true){
                                isTecSelected = true;
                                break;
                            }else{
                                isTecSelected=false;
                            }
                        }
                        if(isTecSelected==true){
                            document.getElementById("spanTECTec").innerHTML="";
                        }else{
                            document.getElementById("spanTECTec").innerHTML="Please Select TEC Team Member";
                            check= false;
                        }

                        // Checking for TSC members
                        if(tscMemAvail==true && tscMemReq == true && $("input:radio[id=TSCyes]").attr("checked")==true){
                            //alert("inside TSC Member")
                            var i=0;
                            var isTscSelected = false;
                            for(i=0;i<totalTscMem;i++){
                                //          alert(i);
                                if($('#Tsc_'+i).attr('checked')==true){
                                    isTscSelected = true;
                                    break;
                                }else{
                                    isTscSelected=false;
                                }
                            }
                            if(isTscSelected==true){
                                document.getElementById("spanTSCTec").innerHTML="";
                            }else{
                                document.getElementById("spanTSCTec").innerHTML="Please Select TSC Team Member";
                                check= false;
                            }
                        }
                    }
                 
                    //isTecSelected = false;
                    
                    if($("input:radio[id=ispostyes]").attr("checked")==false && $("input:radio[id=ispostno]").attr("checked")==false){                        
                        document.getElementById("spanispost").innerHTML="Please select IS Post Qualification";
                        check=false;
                    }                    

                    if(check==false){                        
                        return false;
                    }else{
                        return true;
                        //formSubmit();
                    }
                }

                function  formSubmit(){
                    alert('submit');
                    var f = document.getElementById('frmEvalConfig');
                    var url = '<%=request.getContextPath()%>'+'/AddEvalConfigServlet';
                    alert(url);
                    f.action= url;
                    f.submit();
                }

                function getMemId(id,str){
                    var f = document.frmEvalConfig;

                    if(str=='TEC'){
                        f.tecTEC_UserId.value = id;                        
                    }else{
                        f.tscTEC_UserId.value = id;
                    }
                }

        </script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">

            <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
            %>
            <div class="pageHead_1">Evaluation Configuration
                <span class="c-alignment-right"><a href="EvalComm.jsp?tenderid=<%=request.getParameter("tenderid")%>" title="Tender/Proposal Document" class="action-button-goback">Go Back</a></span>
            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%
                        boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                        
            %>

            <div>&nbsp;</div>

            <form method="post"  name="frmEvalConfig" id="frmEvalConfig" action="<%=request.getContextPath()%>/AddEvalConfigServlet">
                <div class="tabPanelArea_1">
                    <%
                                pageContext.setAttribute("TSCtab", "0");
                    %>
                    <%//@include  file="../resources/common/AfterLoginTSC.jsp"%>

                    <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("tscpublished")){
                            msgTxt="TSC publised successfully.";
                        } else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                   <div class="responseMsg errorMsg" style="margin-bottom: 12px;" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                   <%}%>
                <%}}%>

                    <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1">
                        <tr id="TSCYN" style="display:table-row;">
                            <td width="18%" class="t-align-left ff">TSC Formation Required :</td>
                            <td width="82%" class="t-align-left">
                                <%if (tscReq != null && tscReq.trim().equalsIgnoreCase("yes")) {%>
                                <input type="radio" name="TSCyes" id="TSCyes" value="yes" checked="checked"/>
                                <input type="hidden" name="hdnOldTSCyes" value="yes">
                                <%} else {%>
                                <input type="radio" name="TSCyes" id="TSCyes" value="yes" 
                                       <%

if(!isConfigEntry && isTSCFormed){%>checked="checked"<%}%> />
                                <%}%>
                                <label for="radio">Yes</label>&nbsp;&nbsp;
                                <%if (tscReq != null && tscReq.trim().equalsIgnoreCase("no")) {%>
                                <input type="radio" name="TSCyes" id="TSCno" value="no" checked="checked"/>
                                <%} else {%>
                                <input type="radio" name="TSCyes" id="TSCno" value="no" />
                                <%}%>
                                <label for="radio">No</label>
                                <span id="spanTSCyes" class="reqF_1"></span>
                            </td>
                        </tr>
                        <input type ="hidden" name="allTecMem" value="" id="allTecMem" />
                        <input type="hidden" name="tecTEC_UserId" id ="tecTEC_UserId" value="<%=(tecTEC_UserId == "" ? 0 : tecTEC_UserId)%>"/>
                        <input type="hidden" name="tscTEC_UserId" id ="tscTEC_UserId" value="<%=(tscTEC_UserId == "" ? 0 : tscTEC_UserId)%>"/>

                        <tr>
                            <td width="18%" class="t-align-left ff">Evaluation Type :</td>
                            <td width="82%" class="t-align-left">
                                <%if (configType != null && configType.equalsIgnoreCase("ind")) {%>
                                <input type="radio" name="ind" id="ind" value="ind" checked="checked"/>
                                <%} else {%>
                                <input type="radio" name="ind" id="ind" value="ind" />
                                <%}%><label for="radio2">Individual</label>
                                &nbsp;&nbsp;
                                <%if (configType != null && configType.equalsIgnoreCase("team")) {%>
                                <input type="radio" name="ind" id="team" value="team" checked="checked"/>
                                <%} else {%>
                                <input type="radio" name="ind" id="team" value="team" />
                                <%}%><label for="radio2">Team</label>
                                <span id="spanind" class="reqF_1"></span>
                            </td>
                        </tr>


                        <tr id="TECTR">
                            <td valign="middle" class="t-align-left ff"><span class="ff">Select TEC Team Member :</span></td>
                            <td valign="top" class="t-align-left">

                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <%
                                                for (int i = 0; i < totalTecMem.size(); i++) {
                                                    objCommitteMemDtBeanTEC = (CommitteMemDtBean) totalTecMem.get(i);

                                                    
                                                    if(!TEC_ChairPerson_UserId.equalsIgnoreCase(objCommitteMemDtBeanTEC.getUserId())){
                                    %>

                                    <tr>
                                        <td><label>
                                                <%if (configType.equalsIgnoreCase("team") && evalCommiittee.contains(objCommitteMemDtBeanTEC.getEmpName())) {%>
                                                <input type="radio" name="TecTEC" value="<%=objCommitteMemDtBeanTEC.getEmpName()%>"
                                                       id="<%="Tec_" + i%>"
                                                       onclick="java:getMemId('<%=objCommitteMemDtBeanTEC.getUserId()%>','TEC')"

                                                       <%if(tecTEC_UserId.equalsIgnoreCase(objCommitteMemDtBeanTEC.getUserId())){%>
                                                       checked="checked"
                                                        <%}%>
                                                        />
                                                    <%=objCommitteMemDtBeanTEC.getEmpName()%>
                                                    
                                                <%
                                               } else {%>
                                                <input type="radio" name="TecTEC" value="<%=objCommitteMemDtBeanTEC.getEmpName()%>"
                                                       id="<%="Tec_" + i%>"
                                                       onclick="java:getMemId('<%=objCommitteMemDtBeanTEC.getUserId()%>','TEC')"
                                                         <%if(tecTEC_UserId.equalsIgnoreCase(objCommitteMemDtBeanTEC.getUserId())){%>
                                                       checked="checked"
                                                        <%}%>
                                                       />
                                                <%=objCommitteMemDtBeanTEC.getEmpName()%>
                                                
                                                <%}%>
                                            </label>
                                        </td>

                                    </tr>
                                    <%}
                                    }%>

                                </table><span id="spanTECTec" class="reqF_1"></span>
                            </td>
                        </tr>

                        <tr id="TSCTR">
                            <td valign="middle" class="t-align-left ff"><span class="ff">Select TSC Team Member :</span></td>
                            <td valign="top" class="t-align-left">


                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <%
                                         for (int i = 0; i < totalTscMem.size(); i++) {
                                               objCommitteMemDtBeanTSC = (CommitteMemDtBean) totalTscMem.get(i);
                                                                                                        
                                         if(!TEC_ChairPerson_UserId.equalsIgnoreCase(objCommitteMemDtBeanTSC.getUserId())){
                                    %>
                                    <tr>
                                        <td><label>
                                                
                                                <%if (configType.equalsIgnoreCase("team") && evalCommiittee.contains(objCommitteMemDtBeanTSC.getEmpName())) {%>
                                                <input type="radio" name="TscTEC" value="<%=objCommitteMemDtBeanTSC.getEmpName()%>" 
                                                       id="<%="Tsc_" + i%>"
                                                       onclick="java:getMemId('<%=objCommitteMemDtBeanTSC.getUserId()%>','TSC')"

                                                       <%if(tscTEC_UserId.equalsIgnoreCase(objCommitteMemDtBeanTSC.getUserId())){%>
                                                       checked="checked"
                                                        <%}%>
                                                        
                                                        />

                                                <%=objCommitteMemDtBeanTSC.getEmpName()%> <%=objCommitteMemDtBeanTSC.getUserId()%>
                                                <%} else {%>
                                                <input type="radio" name="TscTEC" value="<%=objCommitteMemDtBeanTSC.getEmpName()%>"
                                                       id="<%="Tsc_" + i%>"
                                                       onclick="java:getMemId('<%=objCommitteMemDtBeanTSC.getUserId()%>','TSC')"
                                                        <%if(tscTEC_UserId.equalsIgnoreCase(objCommitteMemDtBeanTSC.getUserId())){%>
                                                       checked="checked"
                                                        <%}%>

                                                       />
                                                    <%=objCommitteMemDtBeanTSC.getEmpName()%><%=objCommitteMemDtBeanTSC.getUserId()%>
                                                <%}%>
                                            </label>

                                        </td>
                                    </tr>
                                    <%}
                                                }%>


                                </table><span id="spanTSCTec" class="reqF_1"></span>
                            </td>
                        </tr>

                         <tr>
                            <td width="20%" class="t-align-left ff">Is Post Qualification Required :</td>
                            <td width="80%" class="t-align-left">
                                <%if (isPostQua.equalsIgnoreCase("yes")) {%>
                                <input type="radio" name="ispost" id="ispostyes" value="yes" checked="checked"/>
                                <%} else {%>
                                <input type="radio" name="ispost" id="ispostyes" value="yes"/>
                                <%}%>
                                <label for="radio">Yes</label>
                                &nbsp;&nbsp;
                                <%if (isPostQua.equalsIgnoreCase("no")) {%>
                                <input type="radio" name="ispost" id="ispostno" value="no" checked="checked"/>
                                <%} else {%>
                                <input type="radio" name="ispost" id="ispostno" value="no" />
                                <%}%>
                                <label for="radio">No</label>
                                <span id="spanispost" class="reqF_1"></span>
                            </td>
                        </tr>
                    </table>
                    
                    <table width="100%" cellspacing="0" class="tableList_1" style="display: none;">
                        <tr id="MAPFORMS" id="table-row">
                            <td width="18%" class="t-align-left ff">Map Forms :</td>
                            <td width="82%" class="t-align-left"><a href="../officer/MapEvalForms.jsp?action=<%=request.getParameter("action")%>&tenderid=<%=request.getParameter("tenderid")%>&lotId=0">Map</a></td>
                        </tr>
                    </table>

                    <input type="hidden" name="tenderid" value="<%=request.getParameter("tenderid")%>"/>
                    <input type="hidden" name="action" value="<%=request.getParameter("action")%>"/>
                    <div class="t-align-center t_space">
                        <label class="formBtn_1">
                            <%
                            if (request.getParameter("action").equalsIgnoreCase("add")) {%>
                            <input id="btnsubmit" name="btnsubmit" type="submit" value="Submit" onclick="return Validation();"/>
                            <%} else {%>
                            <input id="btnsubmit" name="btnsubmit" type="submit" value="Edit" onclick="return Validation();"/>
                            <%}%>
                        </label>

                    </div>

                </div>
            </form>
        </div>

        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <a href="http://localhost:8080/eGPWeb/officer/Configuration.jsp?tenderid=1412&action=add">Check TSC</a>

        <input id="hdnCurrAction" type="hidden" value="<%=request.getParameter("action")%>">
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabEval");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>

            <script type="text/javascript">
                $(document).ready(function(){
                     //alert("hi");
                     var newAct=document.getElementById('hdnCurrAction').value;
                    //alert(newAct);

                    var strTenderId= <%=request.getParameter("tenderid")%>;                   
                    //alert(strTenderId);
                     if(newAct.toString()=="add"){
                        document.getElementById("TSCno").checked="checked";
                        document.getElementById("ind").checked="checked";
                        document.getElementById("ispostno").checked="checked";
                    }


                    
                   
                });



        </script>
</html>

