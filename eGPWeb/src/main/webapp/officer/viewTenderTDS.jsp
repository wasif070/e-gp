<%--
    Document   : Add NewClause
    Created on : 24-Oct-2010, 3:13:11 PM
    Author     : yanki
--%>

<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBeanTender" />
<%--<jsp:useBean id="sectionClauseSrBean" class="com.cptu.egp.eps.web.servicebean.SectionClauseSrBean" />
<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBean" />--%>
<jsp:useBean id="prepareTDSSrBean" class="com.cptu.egp.eps.web.servicebean.PrepareTenderTDSSrBean" />

<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderIttClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderIttSubClause" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <%
        int ittHeaderId = Integer.parseInt(request.getParameter("ittHeaderId"));
        int sectionId = Integer.parseInt(request.getParameter("sectionId"));
        int tenderId = Integer.parseInt(request.getParameter("tenderId"));
        int tenderStdId = Integer.parseInt(request.getParameter("tenderStdId"));
        int pkgOrLotId = -1;
        if(request.getParameter("porlId")!=null){
            pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
        }
        
        String contentType = createSubSectionSrBean.getContectType(sectionId);
        String contentType1 = "";
        String instruction = "";
        String clauseInstr = "";
        if(contentType.equalsIgnoreCase("ITT")){
            contentType1 = "TDS/PDS";
            instruction = "Instructions for completing Tender/Proposal Data Sheet are provided in italics in parenthesis for the relevant ITB clauses";
            clauseInstr = "Amendments of, and Supplements to, Clauses in the Instructions to Tenderers/Consultants";
        }else if(contentType.equalsIgnoreCase("GCC")){
            contentType1 = "PCC";
            instruction = "Instructions for completing the Particular Conditions of Contract are provided in italics in parenthesis for the relevant GCC Clauses.";
            clauseInstr = "Amendments of, and Supplements to, Clauses in the General Conditions of Contract";
        }

         boolean corriCrtNPending=false;
            String corriId="";
            if(request.getParameter("corriCrtNPending")!=null){
                if("true".equalsIgnoreCase(request.getParameter("corriCrtNPending"))){
                    corriCrtNPending=true;
                    }
                corriId=request.getParameter("corriId");
                }

        List<SPTenderCommonData> tblTdsSubClause = null;
        tblTdsSubClause = prepareTDSSrBean.getTDSSubClause(ittHeaderId);
    %>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create <%if(contentType1.equals("PCC")){out.print("SCC");}else if(contentType1.equals("TDS/PDS")){out.print("BDS");}else{out.print(contentType1);}%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            ul li {margin-left: 20px;}
        </style>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">Prepare <%if(contentType1.equals("PCC")){out.print("SCC");}else if(contentType1.equals("TDS/PDS")){out.print("BDS");}else{out.print(contentType1);}%></div>
                            </div>
                            <%if(pkgOrLotId == -1){%>
                             <table width="100%" cellspacing="10"  class="tableView_1" >
                                <tr>
                                    <td  align="left">
                                    <a href="TenderDocPrep.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>" class="action-button-goback">Go Back to Tender Document Preparation</a>
                                    </td>
                                    <td align="right">
                                    <a href="TenderTDSDashBoard.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&sectionId=<%=sectionId%><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>" class="action-button-goback">Go Back to Tender <%if(contentType1.equals("PCC")){out.print("SCC");}else if(contentType1.equals("TDS/PDS")){out.print("BDS");}else{out.print(contentType1);}%> Dashboard</a>
                                    </td>
                                </tr>
                            </table>
                            <%}else{%>
                            <table width="100%" cellspacing="10"  class="tableView_1" >
                                <tr>
                                    <td  align="left">
                                    <a href="TenderDocPrep.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&porlId=<%= pkgOrLotId %><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>" class="action-button-goback">Go Back to Tender Document Preparation</a>
                                    </td>
                                    <td align="right">
                                    <a href="TenderTDSDashBoard.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&sectionId=<%=sectionId%>&porlId=<%= pkgOrLotId %><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>" class="action-button-goback">Go Back to Tender <%if(contentType1.equals("PCC")){out.print("SCC");}else if(contentType1.equals("TDS/PDS")){out.print("BDS");}else{out.print(contentType1);}%> Dashboard</a>
                                    </td>
                                </tr>
                            </table>
                            <%}%>
                            <%--<table width="100%" border="0" cellspacing="10" class="tableView_1">
                                <tr>
                                    <td align="left">
                                        <img src="../resources/images/Dashboard/addIcn.png" width="16" height="16" class="linkIcon_1" />
                                        <a id="addClause" href="javascript:void(0);">Add New Clause</a> &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>--%>
                            <form action="<%=request.getContextPath()%>/PrepareTDSSrBean?action=save" method="post" onsubmit="return validate();">
                                <input type="hidden" name="sectionId" id="sectionId" value="<%=request.getParameter("sectionId")%>" />
                                <input type="hidden" name="templateId" id="templateId" value="<%=request.getParameter("templateId")%>" />


                                        <table width="100%" cellspacing="0" id="tdsInfo" class="tableList_1 t_space">
                                            <tr>
                                                <td colspan="2"><%=instruction%></td>
                                            </tr>
                                            <tr>
                                                <td><b><%if(contentType.equals("PCC")){out.print("SCC");}else if(contentType.equals("TDS/PDS")){out.print("BDS");}else if(contentType.equals("ITT")){out.print("ITB");}else{out.print(contentType);}%> Clause</b></td>
                                                <td><%=clauseInstr%></td>
                                            </tr>
                                    <%
                                        //tblTdsSubClause
                                        String ittClauseId = "-1";

                                        
                                        int k=1,i;
                                        for(i=0;i<tblTdsSubClause.size(); i++){
                                            if(!ittClauseId.equals(tblTdsSubClause.get(i).getFieldName2())){
                                                ittClauseId = tblTdsSubClause.get(i).getFieldName2();
                                                %>
                                                <tr id="tr_<%=k++%>">
                                                    <td colspan="2" style="line-height: 1.75"><%=tblTdsSubClause.get(i).getFieldName3()%></td>
                                                </tr>
                                                <%
                                            }
                                            %>
                                                <tr id="tr_<%=k++%>">
                                                    <td colspan="2" style="line-height: 1.75"><%=tblTdsSubClause.get(i).getFieldName5()%></td>
                                                </tr>
                                                <tr id="tr_<%=k++%>">
                                                    <td colspan="2" style="line-height: 1.75">
                                                            <%if(tblTdsSubClause.get(i).getFieldName6() != null){out.print(tblTdsSubClause.get(i).getFieldName6());}%>
                                                        <input type="hidden" name="hd_tds_<%=i+1%>" id="hd_tds_<%=i+1%>" value="<%=ittHeaderId+"_"+tblTdsSubClause.get(i).getFieldName2()+"_"+tblTdsSubClause.get(i).getFieldName4()+"_"+tblTdsSubClause.get(i).getFieldName9()%>" />
                                                        <input type="hidden" name="hd_orderNo_<%=i+1%>" id="hd_orderNo_<%=i+1%>" value="<%=tblTdsSubClause.get(i).getFieldName10()%>" />
                                                    </td>
                                                </tr>
                                            <%
                                        }
                                        %>
                                        </table>
                                        <input type="hidden" id="total" name ="total" value="<%=i%>" />
                            </form>

                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>