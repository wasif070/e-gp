<%-- 
    Document   : SrvReExpense
    Created on : Dec 5, 2011, 11:52:47 AM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment Schedule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>


    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
         <%
                        pageContext.setAttribute("tab", "14");
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");
                        String tenderId = "";
                        int formMapId = 0;
                        if(request.getParameter("tenderId")!=null)
                        {
                            tenderId = request.getParameter("tenderId");
                        }
                        String lotId = "";
                        if(request.getParameter("lotId")!=null)
                        {
                            lotId = request.getParameter("lotId");
                        }
                        if(request.getParameter("formMapId") != null)
                        {
                            formMapId = Integer.parseInt(request.getParameter("formMapId"));
                        }
                        if (request.getParameter("srvFormMapId") != null) {
                            formMapId = Integer.parseInt(request.getParameter("srvFormMapId").toString());
                        }
                        boolean flag = false;
                        CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                        String serviceType = commService.getServiceTypeForTender(Integer.parseInt(tenderId));
                        if("Time based".equalsIgnoreCase(serviceType.toString()))
                        {
                            flag = true;
                        }
            %>
        <div class="contentArea_1">
            <div class="pageHead_1">Payment Schedule
                <span style="float: right; text-align: right;" class="noprint">
                    <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=tenderId%>" title="Go Back To WorkSchedule">Go Back</a>
                    </span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
               pageContext.setAttribute("lotId", request.getParameter("lotId")); 
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%@include file="../resources/common/ContractInfoBar.jsp"%>
            <div>&nbsp;</div>
           
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "1");

                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1">
                    <% if (request.getParameter("msg") != null) {
                         if ("succ".equalsIgnoreCase(request.getParameter("msg"))) {
                    %>
                    <div class='responseMsg successMsg'>Payment Schedule Data Submitted SuccessFully</div>
                    <%}else if("fail".equalsIgnoreCase(request.getParameter("msg"))){%>
                    <div class='responseMsg errorMsg'>Payment Schedule Data Submittion Failed</div>
                    <%}}%>
                    <div align="center">                        
                        <form id="frmReExpenses" name="frmReExpenses" method="post" action='<%=request.getContextPath()%>/CMSSerCaseServlet?srvFormMapId=<%=formMapId%>&action=SrvPaymentSchedule&tenderId=<%=tenderId%>&lotId=<%=lotId%>'>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <th>S No</th>    
                                <%if(!flag){%>
                                <th>Milestone Name </th>
                                <%}%>
                                <th>Description</th>
                                <%if(!flag){%>
                                <th>Payment as % of Contract Value</th>
                                <%}%>
                                <th>Mile Stone Date proposed by PE </th>
                                <th>Mile Stone Date proposed by Consultant</th>                                
                                <%                                    
                                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                                    List<TblCmsSrvPaymentSch> list = cmss.getPaymentScheduleData(formMapId);

                                    if(!list.isEmpty()){
                                    for(int i=0; i<list.size(); i++)
                                    {    
                                %>
                                <tr>
                                    <td class="t-align-right"style="text-align: right"><%=list.get(i).getSrNo()%></td>
                                    <%if(!flag){%>
                                    <td class="t-align-left"><%=list.get(i).getMilestone()%></td>
                                    <%}%>
                                    <td class="t-align-left"><%=list.get(i).getDescription()%></td>
                                    <%if(!flag){%>
                                    <td class="t-align-right" style="text-align: right"><%=list.get(i).getPercentOfCtrVal()%></td>
                                    <%}%>
                                    <%if(flag){%>
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(list.get(i).getPeenddate())%></td>
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(list.get(i).getEndDate())%></td>
                                    <%}else{%>                                    
                                    <td class="t-align-center">
                                        <input name="datePropByPE" type="text" class="formTxtBox_1" value="<%=DateUtils.customDateFormate(list.get(i).getPeenddate())%>" readonly="true" id="datePropByPEid<%=i%>" style="width:100px;" onfocus="GetCal('datePropByPEid<%=i%>','datePropByPEid<%=i%>');"/>
                                        <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtdatePropByPE<%=i%>" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('datePropByPEid<%=i%>','txtdatePropByPE<%=i%>');"/></a>
                                        <span id="datePropByPEspan<%=i%>" class="reqF_1"></span>
                                    </td>
                                    <td class="t-align-center">
                                        <input name="datePropByCon" type="text" class="formTxtBox_1" value="<%=DateUtils.customDateFormate(list.get(i).getEndDate())%>" readonly="true" id="datePropByConid<%=i%>" style="width:100px;" onfocus="GetCal('datePropByConid<%=i%>','datePropByConid<%=i%>');"/>
                                        <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtdatePropByCon<%=i%>" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('datePropByConid<%=i%>','txtdatePropByCon<%=i%>');"/></a>
                                        <span id="datePropByConspan<%=i%>" class="reqF_1"></span>
                                    </td>
                                    <%}%>                                    
                                    <input type="hidden" value="<%=list.get(i).getSrvPsid()%>" name="SrvPsid"/> 
                                    <input type="hidden" name="forms" <%if(flag){%>value="timebase"<%}else{%>value="lumpsum"<%}%> />
                                </tr>                                
                                <%}}%>                                
                            </table>
                            <%if(!flag){%>
                            <table class="t_space">
                                <tr>
                                    <td>
                                        <label class="formBtn_1">
                                            <input type="submit" name="submitbtn" id="submit" value="Submit" onclick="return validate();"/>
                                    </label>                                                    
                                    </td>    
                                </tr>    
                            </table>
                            <%}%>
                        </form>
                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
<script>
    function GetCal(txtname,controlname){
    new Calendar({
        inputField: txtname,
        trigger: controlname,
        showTime: false,
        dateFormat:"%d-%b-%Y",
        onSelect: function() {
            var date = Calendar.intToDate(this.selection.get());
            LEFT_CAL.args.min = date;
            LEFT_CAL.redraw();
            this.hide();            
        }
    });

    var LEFT_CAL = Calendar.setup({
        weekNumbers: false
    })
}
    function validate()
    {
        var vbool = true;
        for(var id = 0; id<<%=list.size()%>; id++)
        {
            if(document.getElementById('qtyid'+id).value!="")
            {
                document.getElementById('qtyidspan'+id).innerHTML="";
            }else{
                document.getElementById('qtyidspan'+id).innerHTML="Please enter Quantity";
                vbool = false;
            }
            if(document.getElementById('unitCostid'+id).value!="")
            {
                document.getElementById('unitCostidspan'+id).innerHTML="";
            }else{
                document.getElementById('unitCostidspan'+id).innerHTML="Please enter UnitCost";
                vbool = false;
            }
            
        }
        return vbool;
    }
    allcountCntVal();
</script>    
<script>
var headSel_Obj = document.getElementById("headTabTender");
if(headSel_Obj != null){
headSel_Obj.setAttribute("class", "selected");
}
</script>
</html>

