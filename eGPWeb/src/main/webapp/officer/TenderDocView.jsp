<%--
    Document   : TenderDocPrep
    Created on : 20-Nov-2010, 12:37:32 PM
    Author     : Yagnesh
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.TendererComboService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateGuildeLines"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderForms"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.List, com.cptu.egp.eps.model.table.TblTenderSection"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="grandSummary" class="com.cptu.egp.eps.web.servicebean.GrandSummarySrBean"  scope="page"/>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" scope="page"/>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<jsp:useBean id="procurementNatureBean" class="com.cptu.egp.eps.web.servicebean.OfficerTabSrBean" scope="page"/>
<jsp:useBean id="evalSerCertiSrBean" class="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean" scope="page" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
                    String isPDF = "abc";
                    if (request.getParameter("isPDF") != null) {
                        isPDF = request.getParameter("isPDF");
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Document Preparation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script>
            function delTenderFormConfirm(){
                if(confirm("Are You sure You want to delete this Form")){
                    return true;
                }else{
                    return false;
                }
            }

        </script>
    </head>
    <body>
        <%
                    int tenderId = 0;
                    /*This variable is taken for cheking wether the tender is service or not*/
                    boolean isSerivce = false;
            if (request.getParameter("tenderId") != null) {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }
            TendererComboService tenderercomboservice = (TendererComboService) AppContext.getSpringBean("TendererComboService");
            if (session.getAttribute("userId") != null) {
                  tenderercomboservice.setLogUserId(session.getAttribute("userId").toString());
            }

        %>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">

                <!--Middle Content Table Start-->
                <!--Page Content Start-->
                <div class="pageHead_1">
                    Tender Document View
                    <%--<span class="c-alignment-right">
                        <a class="action-button-goback" href="TenderDashboard.jsp?tenderid=<%= tenderId %>" title="Tender Dashboard">Tender Dashboard</a>
                    </span>--%>
                    <%--<span class="c-alignment-right">
                        <a class="action-button-goback" href="LotDetails.jsp?tenderid=<%= tenderId %>" title="Tender Lot Details">Lot Details</a>
                    </span>--%>
                </div>
                <%
                            pageContext.setAttribute("tenderId", tenderId);
                            pageContext.setAttribute("tab", "2");
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%
                            int packageOrLotId = -1;
            if (request.getParameter("porlId") != null) {
                packageOrLotId = Integer.parseInt(request.getParameter("porlId"));
            }

                            int tenderStdId = 0;
                            boolean isTenderPkgWise = false;
                            if (pageContext.getAttribute("isTenPackageWise") != null) {
                                isTenderPkgWise = Boolean.parseBoolean(pageContext.getAttribute("isTenPackageWise").toString());
                            }

                            if (isTenPackageWise || packageOrLotId == -1) {
                                tenderStdId = dDocSrBean.getTenderSTDId(tenderId);
                            } else {
                                tenderStdId = dDocSrBean.getTenderSTDId(tenderId, packageOrLotId);
                            }

            String stdName = "";
                            TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                            int tendid = 0;

                            //Corrigendum Created and its status is Pending
                            boolean corriCrtNPending = false;
                            List<SPTenderCommonData> lstCorri = tenderCommonService.returndata("getCorrigenduminfo", tenderId + "", null);
                            if (lstCorri != null) {
                                if (lstCorri.size() > 0) {
                                    for (SPTenderCommonData data : lstCorri) {
                                        if (data.getFieldName4().equalsIgnoreCase("pending")) {
                                            corriCrtNPending = true;
                                        }
                                    }
                                }
                            }
                            /* if("Use STD".equalsIgnoreCase(request.getParameter("submit"))){
                            tenderDocumentSrBean.dumpSTD(request.getParameter("tenderid"), request.getParameter("txtStdTemplate"), session.getAttribute("userId").toString());
                            response.sendRedirect("TenderDocPrep.jsp?tenderId="+request.getParameter("tenderid"));
                            }*/

                            //geting lot description

            //boolean isPackage = false;
            //isPackage = true;


            // workflow Integration by Chalapathi

             int uid = 0;
                            if (session.getAttribute("userId") != null) {
                                Integer ob1 = (Integer) session.getAttribute("userId");
                       uid = ob1.intValue();
                   }
                            WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                            short eventid = 2;
                            int objectid = 0;
                            short activityid = 2;
                            int childid = 1;
                            short wflevel = 1;
                            int initiator = 0;
                            int approver = 0;
                            boolean isInitiater = false;
                            boolean evntexist = false;
                            String fileOnHand = "No";
                            String checkperole = "No";
                            String chTender = "No";
                            boolean ispublish = false;
                            boolean iswfLevelExist = false;
                            boolean isconApprove = false;
                            String tenderStatus = "";
                            String tenderworkflow = "";
                            String objtenderId = request.getParameter("tenderId");
                            if (objtenderId != null) {
                                objectid = Integer.parseInt(objtenderId);
                                childid = objectid;
                            }


                            evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
                            List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
                            // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);


                            if (tblWorkFlowLevelConfig.size() > 0) {
                                Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                iswfLevelExist = true;
                                while (twflc.hasNext()) {
                                    TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                    TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                    if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                                        fileOnHand = workFlowlel.getFileOnHand();
                                    }
                                    if (workFlowlel.getWfRoleId() == 1) {
                                        initiator = lmaster.getUserId();
                                    }
                                    if (workFlowlel.getWfRoleId() == 2) {
                                        approver = lmaster.getUserId();
                                    }
                                    if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                                        isInitiater = true;
                                    }

                                }

                            }


                            Object obj = session.getAttribute("userId");
                            String userid = "";
                            if (obj != null) {
                                userid = obj.toString();
                            }
                            String field1 = "CheckPE";
                            List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, userid, "");

                            if (editdata.size() > 0) {
                                checkperole = "Yes";
                            }

                            // for publish link

                            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> checkTender = tenderCommonService1.returndata("TenderWorkFlowStatus", request.getParameter("tenderId"), "'Pending'");
                            if (checkTender.size() > 0) {
                                chTender = "Yes";
                            }
                            List<SPTenderCommonData> publist = tenderCommonService1.returndata("TenderWorkFlowStatus", tenderId + "", "'Approved','Conditional Approval'");
                            //out.println("tenderid " + tenderId);
                            //out.println("chTender ="+chTender);
                            //out.println("publist size ="+publist.size());
                            if (publist.size() > 0) {
                                ispublish = true;
                                SPTenderCommonData spTenderCommondata = publist.get(0);
                                tenderStatus = spTenderCommondata.getFieldName1();
                                tenderworkflow = spTenderCommondata.getFieldName2();

                                if (tenderworkflow.equalsIgnoreCase("Conditional Approval")) {
                                    isconApprove = true;
                                }
                            }


        %>
                <%
                            String lotDesc = "";
                            String lotNo = null;
                            List<TblTenderLotSecurity> lots = null;
                            if (isTenPackageWise == false) {
                                lots = tenderDocumentSrBean.getLotDetailsByLotId(tenderId, packageOrLotId);
                                if (lots.size() > 0) {
                                    TblTenderLotSecurity tenderLotSecurity = lots.get(0);
                                    lotDesc = tenderLotSecurity.getLotDesc();
                                    lotNo = tenderLotSecurity.getLotNo();
                                }
                            }

                            if (request.getParameter("tenderId") != null) {
                                tendid = Integer.parseInt(request.getParameter("tenderId"));

                            }
                %>

                <div class="tabPanelArea_1">
                    <%
                        stdName=tenderDocumentSrBean.checkForDump(String.valueOf(tendid));
                        String stdTemplateId = "0";
                        stdTemplateId = tenderDocumentSrBean.findStdTemplateId(String.valueOf(tendid));
                        out.print("<table width='100%' cellspacing='0' class='tableList_1'><tr><td class='t-align-left ff' width='22%'>Standard BiddingDocument :</td><td>"+stdName+"</td></tr></table>");
                    %>
                    <table width='100%' cellspacing='0' class='tableList_1 t_space'><tr><td width='15%' class='t-align-left ff'>Guidance Notes : </td><td width='85%'>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <th>Sl. No.</th>
                                        <th>File Name</th>
                                        <th>File Description</th>
                                        <th>File Size In KB</th>
                                        <th>Action</th>
                                    </tr>

            <%
                                                java.util.List<com.cptu.egp.eps.model.table.TblTemplateGuildeLines> docs = defineSTDInDtlSrBean.getGuideLinesDocs(Integer.parseInt(stdTemplateId));
                                                if (docs != null) {
                                                    int size = 0;
                                                    if (docs.size() > 0) {
                                                        short j = 0;
                                                        for (TblTemplateGuildeLines ttsd : docs) {
                                                            j++;
                                                            size = Integer.parseInt(ttsd.getDocSize()) / 1024;
            %>
                                    <tr>
                                        <td style="text-align:center;"><%= j%></td>
                                        <td style="text-align:left;"><%= ttsd.getDocName()%></td>
                                        <td style="text-align:left;"><%= ttsd.getDescription()%></td>
                                        <td style="text-align:center;"><%=size%></td>
                                        <td style="text-align:center;">
                                            <a href="../GuideLinesDocUploadServlet?docName=<%= ttsd.getDocName()%>&templateId=<%= stdTemplateId%>&docSize=<%= ttsd.getDocSize()%>&action=downloadDoc" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                        </td>
                                    </tr>

                                    <%
                                                                                                    ttsd = null;
                                                                                                }
                                                                                            } else {
                                    %>
                                    <tr>
                                        <td colspan="5">
                                            No Records Found
                                        </td>
                                    </tr>
                                    <%                                                                                                        }
                                                                                            docs = null;
                                                                                        } else {
                                    %>
                                    <tr>
                                        <td colspan="5">
                                            No Records Found
                                        </td>
                                    </tr>
                                    <%                                                            }
                                    %>
                                </table>


                            </td>
                        </tr>
                    </table>

            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <% if (!"".equals(stdName) && stdName != null) {%>
                <tr>
                            <td colspan="3" class="t-align-left ff">Standard Bidding Document : <%= stdName%> </td>
                </tr>
                        <%   }%>
                <tr>
                     <td width="15%" class="t-align-left ff">
                                <%if (isTenPackageWise) {%>
                            Package No. :
                                <%} else {%>
                            Lot No. :
                        <%}%>
                     </td>
                     <td colspan="2" class="t-align-left">
                         <%
                            if(isTenPackageWise){
                                out.print(tenderDocumentSrBean.getPkgNo(String.valueOf(tenderId)));
                            }else{
                                out.print(lotNo);
                            }
                        %>
                     </td>
                </tr>
                <tr>
                     <td width="15%" class="t-align-left ff">
                                <%if (isTenPackageWise) {%>
                            Package Description :
                                <%} else {%>
                            Lot Description :
                        <%}%>
                    </td>
                    <td colspan="2" class="t-align-left">
                         <% if(isTenPackageWise){
                                if(packageDescription == null)
                                    out.print("--");
                                else 
                                    out.print(packageDescription);
                            }else {
                                out.print(lotDesc);
                            }
                       %>
                    </td>
                </tr>
                <%
                    int tenderittSectionId = 0;
                    List<TblTenderSection> tSections = dDocSrBean.getTenderSections(tenderStdId);
                                    if (tSections != null) {
                                        if (tSections.size() > 0) {
                            short j = 0;
                                            short disp = 0;
                %>
                        <tr>
                            <th width="15%" class="t-align-left">Section No.</th>
                            <th width="75%" class="t-align-left">Section Name</th>
                            <th width="10%" class="t-align-left">Action</th>
                        </tr>
                        <%
                                                            for (TblTenderSection tts : tSections) {
                                                                disp++;
                                                                if (tts.getContentType().equals("Form")) {
                                                                    if (packageOrLotId == -1) {%>
                        <tr>
                            <td class="t-align-left"><%= disp%></td>
                                    <td colspan="2" class="t-align-left">
                                <%= tts.getSectionName()%>
                                <%

                                                                                        String folderName = pdfConstant.TENDERDOC;
                                                                                        String pdfId = tenderId + "_" + tts.getTenderSectionId() + "_" + tenderStdId;
                                                                                        String reqURL = request.getRequestURL().toString();
                                                                                        String reqQuery = "?tenderId=" + tenderId;
                                                                                        if (!(isPDF.equalsIgnoreCase("true"))) {
                                                                                            tenderSrBean.generatePdfOnLoad(Integer.parseInt(id), reqURL, reqQuery, folderName);
                                                                                        }




                                                                                        if (ispublish) {
                                %>
                                <a class="action-button-savepdf"
                                   href="<%=request.getContextPath()%>/GeneratePdf?folderName=<%=folderName%>&id=<%=pdfId%>" >Save As PDF </a>
                                <% }%>
                                        <table width="100%" cellspacing="5" cellpadding="0" border="0">
                                            <tr>

                                            </tr>
                                    <%
                                                                                            /*This method is called for cheking wether the tender is service or not*/
                                                                                            List<TblTenderDetails> tblTenderDetail = procurementNatureBean.getProcurementNature(tenderId);
                                                                                            if (tblTenderDetail.get(0).getProcurementNature().equalsIgnoreCase("Services")) {
                                                                                                isSerivce = true;
                                                                                            }
                                                                                            if (isSerivce) {
                                    %>
                                            <tr>
                                        <td width="35%" class="ff">Configure marks for technical evaluation :</td>
                                        <%
                                                                                                                        if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                        %>
                                        <td width="65%"><a href="EvalCertiService.jsp?tenderId=<%= tenderId%>">Configure</a></td>
                                        <%}/*Condtion  Insert and Edit Link*/ else {%>
                                        <td width="65%"><a href="EvalCertiServiceView.jsp?tenderId=<%= tenderId%>">View</a></td>
                                        <%}/*View criteria Condtion ends here*/%>
                                        <%/*Innner Condition Ends here*/                                                                                                }/*Is service condtion ends here*/%>
                                    </tr>
                                    <%
                                                                                            if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                    || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false || (corriCrtNPending && isInitiater && ispublish)) {%>
                                    <tr>
                                        <td width="15%" class="ff">New forms :</td>
                                        <td width="85%"><a href="CreateTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>">Prepare</a></td>
                                    </tr>
                                    <% }%>
                                </table>
                                <%
                                                                                        java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> forms = dDocSrBean.getTenderFormPackage(tts.getTenderSectionId());
                                %>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <th style="text-align:center; width:4%;">Sl. No.</th>
                                        <th width="36%">Form Name</th>
                                        <th width="60%">Actions</th>
                                    </tr>
                                    <%
                                                                                            if (forms != null) {
                                                                                                if (forms.size() != 0) {
                                                                                                    boolean isFrmCanceled = false;
                                                                                                    for (int jj = 0; jj < forms.size(); jj++) {
                                                                                                        isFrmCanceled = false;
                                    %>
                                    <tr>
                                        <td style="text-align:center; width:8%;"><%=(jj + 1)%></td>
                                        <td width="32%">
                                            <%
                                                                                                                                    out.print(forms.get(jj).getFormName().replace("?s", "'s"));
                                                                                                                                    if (forms.get(jj).getFormStatus() != null) {
                                                                                                                                        if ("cp".equals(forms.get(jj).getFormStatus().toString())) {
                                                                                                                                            isFrmCanceled = true;
                                                                                                                                            out.print("  <font color='red'>(Form Canceled)</font>");
                                                                                                                                        }
                                                                                                                                    }
                                            %>
                                        </td>
                                        <td width="60%">
                                            <%  if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {%>
                                            <a href="CreateTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Edit</a>
                                            <%if (forms.get(jj).getTemplateFormId() != 0) {
                                                  } else {%>
                                            &nbsp; | &nbsp;
                                            <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=delForm&tenderId=<%= tenderId%>&formId=<%= forms.get(jj).getTenderFormId()%>" onclick="return delTenderFormConfirm();" >Delete</a>
                                            <%}%>
                                            &nbsp; | &nbsp;
                                            <a href="TenderTableDashboard.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Form Dashboard</a> &nbsp; |
                                            <% }%>
                                            <a href="ViewTenderCompleteForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">View Form</a>
                                            <%
                                                                                                                                    if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                            %>
                                            &nbsp; | &nbsp;
                                            <a href="DefineMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= forms.get(jj).getTenderFormId()%>">Prepare Required Document List</a>
                                            &nbsp; | &nbsp;<br/>
                                            <a href="ViewMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= forms.get(jj).getTenderFormId()%>">View Required Document List</a>
                                            &nbsp; | &nbsp;
                                            <script>
                                                function confirmation(){
                                                    jConfirm('Do you really want to dump this form? ', 'Dump form confirmation', function (ans) {
                                                        if (ans)
                                                            window.location='<%=request.getContextPath()%>/CreateTenderFormSrvt?action=dumpForm&tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>';


                                                    });
                                                }
                                            </script>

                                            <a href="#" onclick="confirmation();">Dump Form</a>
                                            <% }%>
                                            &nbsp; | &nbsp;
                                            <a href="TestTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Test Form</a>
                                            <% if (corriCrtNPending && !(isFrmCanceled)) {%>
                                            &nbsp; | &nbsp;
                                            <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=cancelForm&tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Cancel Form</a>
                                            <% }%>
                                            <%
                                                                                                                                    if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                                                                                                                        if (forms.get(jj).getTemplateFormId() == 0) {
                                            %>
                                            &nbsp; | &nbsp;<a href="<%=request.getContextPath()%>/officer/CreateTenderCombo.jsp?formId=<%= forms.get(jj).getTenderFormId()%>&tenderId=<%= tenderId%>">Create Combo</a>
                                            <% }}%>
                                            <%
                                                long lng = tenderercomboservice.getComboCountFromTenderer(forms.get(jj).getTenderFormId());
                                                if(lng!=0)
                                                {
                                            %>
                                            &nbsp; | &nbsp;<a href="<%=request.getContextPath()%>/officer/ViewTenderCombo.jsp?templetformid=<%= forms.get(jj).getTenderFormId()%>&tenderId=<%= tenderId%>"TARGET = "_blank">View Combo</a>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <%
                                                                                                    }
                                                                                                }
                                                                                                forms = null;
                                                                                            }
                                    %>
                                </table>
                                <%
                                                                                        for (TblTenderLotSecurity ttls : tenderDocumentSrBean.getLotList(tenderId)) {
                                %>
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="tableList_1 t_space">
                                    <tr>
                                        <td width="15%" class="ff">Lot No.</td>
                                        <td width="85%"><%=ttls.getLotNo()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Lot Description</td>
                                        <td ><%=ttls.getLotDesc()%></td>
                                    </tr>
                                    <tr>
                                                <td class="ff">Grand summary : </td>
                                                <td>
                                                    <%
                                                    int isGSCreatedId = -1;
                                                    isGSCreatedId = grandSummary.isGrandSummaryCreated(tenderId,ttls.getAppPkgLotId());
                                                                                                                        //out.println(" isGSCreatedId " + isGSCreatedId + " ispublish " + ispublish);
                                                                                                                        if (isGSCreatedId != -1) {
                                                        %>
                                            <%  if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                                    || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {%>
<!--                                            <a href="CreateGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&gsId=<%= isGSCreatedId%>">Edit</a> &nbsp;|&nbsp;-->
                                            <% }%>
                                            <a href="ViewGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&gsId=<%= isGSCreatedId%>">View</a>
                                                        <%
                                                                                                                                        } else {
                                                                                                                                            if (!ispublish) {
                                            %>
<!--                                            <a href="CreateGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&porlId=<%= ttls.getAppPkgLotId()%>&std=package">Create</a>-->
                                            <%
                                                                                                                            } else {
                                                                                                                                out.print("Not Prepared");
                                                    }
                                                                                                                        }
                                                    %>
                                                </td>
                                            </tr>
                                </table>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <th style="text-align:center; width:4%;">Sl. No.</th>
                                        <th width="36%">Form Name</th>
                                        <th width="60%">Actions</th>
                                    </tr>
                                    <% int jj = 0;
                                                                                                                for (TblTenderForms tblTenderForms : tenderDocumentSrBean.getTenderFormLot(tts.getTenderSectionId(), ttls.getAppPkgLotId())) {%>
                                    <%
                                                                                                                                    boolean isFrmCanceled = false;
                                                                                                                                    isFrmCanceled = false;
                                    %>
                                    <tr>
                                        <td style="text-align:center; width:8%;"><%=(jj += 1)%></td>
                                        <td width="32%">
                                            <%
                                                                                                                                            out.print(tblTenderForms.getFormName().replace("?s", "'s"));
                                                                                                                                            if (tblTenderForms.getFormStatus() != null) {
                                                                                                                                                if ("cp".equals(tblTenderForms.getFormStatus().toString())) {
                                                                                                                                                    isFrmCanceled = true;
                                                                                                                                                    out.print("  <font color='red'>(Form Canceled)</font>");
                                                                                                                                                }
                                                                                                                                            }
                                            %>
                                        </td>
                                        <td width="60%">
                                            <%  if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                                    || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {%>
                                            <a href="CreateTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>">Edit</a>
                                            <% if (tblTenderForms.getTemplateFormId() != 0) {
                                                  } else {%>
                                            &nbsp; | &nbsp;
                                            <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=delForm&tenderId=<%= tenderId%>&formId=<%= tblTenderForms.getTenderFormId()%>" onclick="return delTenderFormConfirm();" >Delete</a>
                                            <%}%>
                                            &nbsp; | &nbsp;
                                            <a href="TenderTableDashboard.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>">Form Dashboard</a> &nbsp; |
                                            <% }%>
                                            <a href="ViewTenderCompleteForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%=tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>">View Form</a>
                                            <%
                                                                                                                                            if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                                    || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                            %>
                                            &nbsp; | &nbsp;
                                            <a href="DefineMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= tblTenderForms.getTenderFormId()%>">Prepare Required Document List</a>
                                            &nbsp; | &nbsp;<br/>
                                            <a href="ViewMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= tblTenderForms.getTenderFormId()%>">View Required Document List</a>
                                            &nbsp; | &nbsp;
                                            <a href="#" onclick="confirmation();">Dump Form</a>
                                            <% }%>
                                            &nbsp; | &nbsp;
                                            <a href="TestTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>">Test Form</a>
                                            <% if (corriCrtNPending && !(isFrmCanceled)) {%>
                                            &nbsp; | &nbsp;
                                            <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=cancelForm&tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>">Cancel Form</a>
                                            <% }%>
                                            <%
                                                                                                                                            if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                                    || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                                                                                                                                if (tblTenderForms.getTemplateFormId() == 0) {
                                            %>
                                            &nbsp; | &nbsp;<a href="<%=request.getContextPath()%>/officer/CreateTenderCombo.jsp?formId=<%= tblTenderForms.getTenderFormId()%>&tenderId=<%= tenderId%>">Create Combo</a>
                                            <% }}%>
                                            <%
                                                long lng = tenderercomboservice.getComboCountFromTenderer(tblTenderForms.getTenderFormId());
                                                if(lng!=0)
                                                {
                                            %>
                                            &nbsp; | &nbsp;<a href="<%=request.getContextPath()%>/officer/ViewTenderCombo.jsp?templetformid=<%= tblTenderForms.getTenderFormId()%>&tenderId=<%= tenderId%>"TARGET = "_blank">View Combo</a>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <% }%>
                                </table>
                                <% }%>
                            </td>
                        </tr>
                        <%} else {%>
                        <tr>
                            <td class="t-align-left"><%= disp%></td>
                            <td colspan="2" class="t-align-left">
                                <%= tts.getSectionName()%>
                                <%
                                                                                        if (ispublish) {
                                                                                            String folderName = pdfConstant.TENDERDOC;
                                                                                            String pdfId = tenderId + "_" + tts.getTenderSectionId() + "_" + tenderStdId;
                                %>
                                <a class="action-button-savepdf"
                                   href="<%=request.getContextPath()%>/GeneratePdf?folderName=<%=folderName%>&id=<%=pdfId%>" >Save As PDF </a>
                                <% }%>
                                <table width="100%" cellspacing="5" cellpadding="0" border="0">
                                    <tr>
                                        <td colspan="2" class="ff">
                                            Tender forms
                                        </td>
                                    </tr>
                                    <%
                                                                                            /*This method is called for cheking wether the tender is service or not*/
                                                                                            List<TblTenderDetails> tblTenderDetail = procurementNatureBean.getProcurementNature(tenderId);
                                                                                            if (tblTenderDetail.get(0).getProcurementNature().equalsIgnoreCase("service")) {
                                                                                                isSerivce = true;
                                                                                            }
                                                                                            if (isSerivce) {
                                    %>
                                    <tr>
                                        <td width="35%" class="ff">Configure marks for technical evaluation :</td>
                                        <%
                                                                                                                        if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                        %>
                                        <td width="65%"><a href="EvalCertiService.jsp?tenderId=<%= tenderId%>">Configure</a></td>
                                        <%}/*Condtion for View and Insert Link*/ else {%>
                                        <td width="65%"><a href="EvalCertiServiceView.jsp?tenderId=<%= tenderId%>">View</a></td>
                                        <%}%>
                                    </tr>
                                    <%}/*Is service codintion ends*/
                                                                                            if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                    || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false || (corriCrtNPending && isInitiater && ispublish)) {%>
                                    <tr>
                                        <td width="15%" class="ff">New forms :</td>
                                        <td width="85%"><a href="CreateTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>">Prepare</a></td>
                                    </tr>
                                    <% }%>
                                    <tr>
                                        <td class="ff">Grand summary : </td>
                                        <td>
                                            <%
                                                                                                    int isGSCreatedId = -1;
                                                                                                    isGSCreatedId = grandSummary.isGrandSummaryCreated(tenderId,packageOrLotId);
                                                                                                    //out.println(" isGSCreatedId " + isGSCreatedId + " ispublish " + ispublish);
                                                                                                    if (isGSCreatedId != -1) {
                                            %>
                                            <%  if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {%>
<!--                                            <a href="CreateGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&gsId=<%= isGSCreatedId%>&porlId=<%= packageOrLotId%>">Edit</a> &nbsp;|&nbsp;-->
                                            <% }%>
                                            <a href="ViewGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&gsId=<%= isGSCreatedId%>&porlId=<%= packageOrLotId%>">View</a>
                                            <%
                                                                                                                                } else {
                                                                                                                                    if (!ispublish) {
                                            %>
<!--                                            <a href="CreateGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&porlId=<%= packageOrLotId%>">Create</a>-->
                                            <%
                                                                                                        } else {
                                                                                                            out.print("Not Prepared");
                                                                                                        }
                                                                                                    }
                                            %>
                                        </td>
                                    </tr>
                                            <%--<tr>
                                                <td class="ff">Mandatory forms : </td>
                                                <td><a href="#">Configure</a></td>
                                            </tr>--%>
                                        </table>
                                        <%
                                            java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> forms = dDocSrBean.getTenderForm(tts.getTenderSectionId());
                                        %>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                        <th style="text-align:center; width:4%;">Sl. No.</th>
                                        <th width="36%">Form Name</th>
                                        <th width="60%">Actions</th>
                                            </tr>
                                            <%
                                                                                            if (forms != null) {
                                                                                                if (forms.size() != 0) {
                                                                                                    boolean isFrmCanceled = false;
                                                                                                    for (int jj = 0; jj < forms.size(); jj++) {
                                                                                                        isFrmCanceled = false;
                                            %>
                                                    <tr>
                                        <td style="text-align:center; width:8%;"><%=(jj + 1)%></td>
                                        <td width="32%">
                                            <%
                                                                                                                                    out.print(forms.get(jj).getFormName().replace("?s", "'s"));
                                                                                                                                    if (forms.get(jj).getFormStatus() != null) {
                                                                                                                                        if ("cp".equals(forms.get(jj).getFormStatus().toString())) {
                                                                                                                                            isFrmCanceled = true;
                                                                                                                                            out.print("  <font color='red'>(Form Canceled)</font>");
                                                                                                                                        }
                                                                                                                                    }
                                            %>
                                                        </td>
                                        <td width="60%">
                                            <%  if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {%>
                                            <a href="CreateTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Edit</a>
                                            <%if (forms.get(jj).getTemplateFormId() != 0) {
                                                  } else {%>
                                            &nbsp; | &nbsp;
                                            <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=delForm&tenderId=<%= tenderId%>&formId=<%= forms.get(jj).getTenderFormId()%>" onclick="return delTenderFormConfirm();" >Delete</a>
                                            <%}%>
                                            &nbsp; | &nbsp;
                                            <a href="TenderTableDashboard.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Form Dashboard</a> &nbsp; |
                                            <% }%>
                                            <a href="ViewTenderCompleteForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">View Form</a>
                                            <%
                                                                                                                                    if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                            %>
                                            &nbsp; | &nbsp;
                                            <a href="DefineMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= forms.get(jj).getTenderFormId()%>">Prepare Required Document List</a>
                                            &nbsp; | &nbsp;<br/>
                                            <a href="ViewMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= forms.get(jj).getTenderFormId()%>">View Required Document List</a>
                                            &nbsp; | &nbsp;
                                            <a href="#" onclick="confirmation();">Dump Form</a>
                                            <%
                                                                                                                                    }
                                            %>
                                            &nbsp; | &nbsp;
                                            <a href="TestTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Test Form</a>
                                            <%
                                                                                                                                    if (corriCrtNPending && !(isFrmCanceled)) {
                                            %>
                                            &nbsp; | &nbsp;
                                            <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=cancelForm&tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Cancel Form</a>
                                            <%
                                                                                                                                    }
                                            %>
                                            <%
                                                                                                                                    if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                                                                                                                        if (forms.get(j).getTemplateFormId() == 0) {
                                            %>
                                            &nbsp; | &nbsp;
                                            <a href="<%=request.getContextPath()%>/officer/CreateTenderCombo.jsp?formId=<%= forms.get(jj).getTenderFormId()%>&tenderId=<%= tenderId%>">Create Combo</a>
                                            <% }}%>
                                            <%
                                                long lng = tenderercomboservice.getComboCountFromTenderer(forms.get(jj).getTenderFormId());
                                                if(lng!=0)
                                                {
                                            %>
                                            &nbsp; | &nbsp;<a href="<%=request.getContextPath()%>/officer/ViewTenderCombo.jsp?templetformid=<%= forms.get(jj).getTenderFormId()%>&tenderId=<%= tenderId%>"TARGET = "_blank">View Combo</a>
                                            <%}%>
                                            <%--<a href="#">Edit</a> &nbsp;|&nbsp;
                                            <a href="#">View</a> &nbsp;|&nbsp;
                                            <a href="#">Test form</a> &nbsp;|&nbsp;
                                            <a href="#">Formula </a> &nbsp;|&nbsp;
                                            <a href="#">Cancel</a>&nbsp;|&nbsp;
                                            <a href="#">Docs. Check list</a>--%>
                                        </td>
                                                    </tr>
                                            <%
                                                    }
                                                }
                                                forms = null;
                                            }
                                            %>
                                        </table>
                                    </td>
                        </tr>
                <%
                                                                                    }
                                                                                } else if (tts.getContentType().equals("Document")) {
                %>
                                    <tr>
                            <td class="t-align-left"><%= disp%></td>
                                        <td colspan="2" class="t-align-left">

                                            <table width="100%" cellspacing="0" class="tableList_1">
                                                <tr>
                                        <td colspan="5" class="t-align-left ff"><%= tts.getSectionName()%></td>
                                                </tr>


                                    <th width="4%" class="t-align-center">Sl. No.</th>
                                    <th class="t-align-center" width="23%">File Name</th>
                                    <th class="t-align-center" width="32%">File Description</th>
                                    <th class="t-align-center" width="7%">File Size <br />
                        (in KB)</th>
                                    <th class="t-align-center" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                                                                                                String sectionId = tts.getTenderSectionId() + "";

                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                                        <td class="t-align-center">
                                            <%if (sptcd.getFieldName5().trim().equals("0")) {%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tender=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                <%} else {%>
                                            <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?templateSectionDocId=<%=sptcd.getFieldName5()%>&sectionId=<%=sptcd.getFieldName5()%>&docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}%>

                         </td>
                    </tr>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                                    <%   if (sptcd != null) {
                                                                                                        sptcd = null;
                                                                                                    }
                                                                                                }%>


                                            </table>
                                        </td>
                                    </tr>
                <%
                                                                                } else if (tts.getContentType().equals("Document By PE")) {
                %>
                                    <tr>
                            <td class="t-align-left"><%= disp%></td>
                                        <td colspan="2" class="t-align-left">


                                <div class="t-align-left">
                                    <%
                                                                                                if (((tenderStatus.equalsIgnoreCase("pending")
                                                                                                        || tenderStatus.equals("")) && ispublish == false && isInitiater == true && fileOnHand.equalsIgnoreCase("Yes"))
                                                                                                        || iswfLevelExist == false
                                                                                                        || (corriCrtNPending && isInitiater && ispublish)) {
                                                                                                    String inCorrigendum = "no";
                                                                                                    if (corriCrtNPending) {
                                                                                                        inCorrigendum = "yes";
                                                                                                    }
                                    %>
                                    <a href="TenderDocUpload.jsp?&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&porlId=<%= packageOrLotId%>&inCorri=<%= inCorrigendum%>">Upload</a></div>
                                    <% }%>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                                <tr>
                                        <td colspan="5" class="t-align-left ff"><%= tts.getSectionName()%></td>
                                                </tr>
                                                <th width="4%" class="t-align-left">Sl. No.</th>
                                    <th class="t-align-center" width="23%">File Name</th>
                                    <th class="t-align-center" width="32%">File Description</th>
                                    <th class="t-align-center" width="7%">File Size <br />
                        (in KB)</th>
                                    <th class="t-align-center" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                                                                                                String sectionId = tts.getTenderSectionId() + "";

                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                                                                boolean isDocCanceled = false;
                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                                                                                                    isDocCanceled = false;

                    %>
                    <tr>
                                        <td class="t-align-center"><%=docCnt%></td>
                                        <td class="t-align-left">
                                            <%=sptcd.getFieldName1()%>
                                            <%
                                                                                                                        if (sptcd.getFieldName6() != null) {
                                                                                                                            if ("cp".equals(sptcd.getFieldName6().trim().toString())) {
                                                                                                                                isDocCanceled = true;
                                                                                                                                out.print("  <br/><font color='red'>(Document Canceled)</font>");
                                                                                                                            }
                                                                                                                        }
                                            %>
                                        </td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                                        <td class="t-align-center">
                                            <%
                                                                                                                        if ("0".equals(sptcd.getFieldName5().trim())) {
                                            %>
                                            <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             &nbsp;
                                            <%
                                                                                                                    if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                            %>
                                            <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                                <%
                                                                                                                        }
                                                                                                                    } else {
                                                %>
                                            <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?templateSectionDocId=<%=sptcd.getFieldName5()%>&docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                <%
                                                                                                                        if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                                %>
                                            <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                                <%
                                                                                                                                }
                                                                                                                            }

                                                                                                                            if (corriCrtNPending && !(isDocCanceled)) {
                                                %>
                                            &nbsp;&nbsp;
                                            <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?funName=cancelDoc&docId=<%=sptcd.getFieldName4()%>&tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&porlId=<%= packageOrLotId%>">Cancel</a>
                                            <%
                                                                                                                        }
                                            %>
                         </td>
                    </tr>

                                    <%   if (sptcd != null) {
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                                            </table>      </td>
                                    </tr>
                <%
                                                                                } else {
                %>
                                    <tr>
                            <td class="t-align-left"><%= disp%></td>
                            <td class="t-align-left"><%= tts.getSectionName()%>
                                <div align="right">
                                    <%        if (((tts.getSectionName()).contains("ITT") || (tts.getSectionName()).contains("GCC")) && ispublish) {
                                                                                                    String tempID = tenderDocumentSrBean.findStdTemplateId(String.valueOf(tendid));
                                                                                                    String folderName = pdfConstant.STDPDF;
                                                                                                    String genId = tempID + "_" + tts.getTemplateSectionId();
                                    %>

                                    <a class="action-button-savepdf"
                                       href="<%=request.getContextPath()%>/GeneratePdf?folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>
                                    <% }%>
                                    <%        if (((tts.getSectionName()).contains("TDS") || (tts.getSectionName()).contains("PCC")) && ispublish) {
                                                                                                    String folderName = pdfConstant.TENDERDOC;
                                                                                                    String pdfId = tenderId + "_" + tenderittSectionId + "_" + tenderStdId;
                                    %>

                                    <a class="action-button-savepdf"
                                       href="<%=request.getContextPath()%>/GeneratePdf?folderName=<%=folderName%>&id=<%=pdfId%>" >Save As PDF </a>
                                    <% }%>

                                </div>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                                    <th width="4%" class="t-align-center">Sl. No.</th>
                                    <th class="t-align-center" width="23%">File Name</th>
                                    <th class="t-align-center" width="32%">File Description</th>
                                    <th class="t-align-center" width="7%">File Size <br />
                        (in KB)</th>
                                    <th class="t-align-center" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                                                                                                String sectionId = tts.getTenderSectionId() + "";


                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>

                                        <td class="t-align-center">
                                            <%if (sptcd.getFieldName5().trim().equals("0")) {%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tender=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                <%} else {%>

                                            <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?templateSectionDocId=<%=sptcd.getFieldName5()%>&sectionId=<%=sptcd.getFieldName5()%>&docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}%>
                         </td>
                    </tr>
                                    <%   if (sptcd != null) {
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                                            </table>
                                        </td>
                                        <%
                                                                                        if (tts.getContentType().equals("TDS") || tts.getContentType().equals("PCC")) {
                            %>
                            <td class="t-align-left">
                                <% if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true) || iswfLevelExist == false) {%>
                                <a href="TenderTDSDashBoard.jsp?tenderStdId=<%=tenderStdId%>&tenderId=<%=tenderId%>&sectionId=<%=tenderittSectionId%>&porlId=<%= packageOrLotId%>">Prepare</a>
                                <% } else {%>
                                <a href="TenderTDSView.jsp?tenderStdId=<%=tenderStdId%>&tenderId=<%=tenderId%>&sectionId=<%=tenderittSectionId%>&porlId=<%= packageOrLotId%>">View</a>
                                <% }%>
                            </td>
                            <% } else {
                                            tenderittSectionId = tts.getTenderSectionId();
                                            %>
                            <td class="t-align-left"><a href="TenderITTDashboard.jsp?tenderStdId=<%=tenderStdId%>&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&porlId=<%= packageOrLotId%>">View</a></td>
                            <% }%>
                                    </tr>
                <%
                                }
                                                                j++;
                                tSections = null;
                            }
                                                        } else {
                %>
                        <!--                            <tr>
                                <td colspan="3">
                                    No Records Found
                                </td>
                                                    </tr>-->
                        <%                                        }
                        tSections = null;
                                                            } else {
                %>
                        <tr>
                            <td colspan="3">
                                No Records Found
                            </td>
                        </tr>
                        <%                            }
                %>
                    </table></div></div>
            <%--<div class="t-align-center t_space"><label class="formBtn_1"><input name="DocPreparation" type="button" value="Complete Document Preparation" /></label></div>--%>
            <%--<div>&nbsp;</div>--%>

            <!--Page Content End-->
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<%
            if (grandSummary != null) {
        grandSummary = null;
    }
            if (dDocSrBean != null) {
        dDocSrBean = null;
    }
%>
