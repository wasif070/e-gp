<%-- 
    Document   : OfflineNegotiation
    Created on : Dec 23, 2010, 2:59:07 PM
    Author     : parag
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Negotiation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
        <jsp:useBean id="neg" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean" />
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
<%
    int userId = 0;
    //byte suserTypeId = 0;
    int tenderId = 1;
    int pendingNegId=0;
    boolean flag = false;
    String action = "";
    String msg = "";
    
   //if (session.getAttribute("userTypeId") != null) {
   //    suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
   //}
    if (session.getAttribute("userId") != null) {
        userId = Integer.parseInt(session.getAttribute("userId").toString());
    }
    if (request.getParameter("tenderId") != null) {
        tenderId = Integer.parseInt(request.getParameter("tenderId"));
    }
    if (request.getParameter("flag") != null) {
        if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
            flag = true;
        }
    }
    if(request.getParameter("action")!=null){
        action = request.getParameter("action");
    }
     if(flag){
       if("create".equalsIgnoreCase(action)){
            msg = " Negotiation configured successfully";
       }
       if("close".equalsIgnoreCase(action)){
            msg = " Negotiation closed successfully";
       }
    }
    
    List<SPTenderCommonData> tenderCommonDatas = neg.getNegotiationDetails(tenderId);
    
    pendingNegId = neg.isAnyNegPending(tenderId);

    pageContext.setAttribute("tenderId", tenderId);

    boolean isTECPEC = false;
    
    if(neg.isTECPECMember(tenderId, userId))
        isTECPEC = true;

    Object[] objects = neg.getTenderDetails(tenderId);
    String procurementType = "";
    String estCost = "";

    procurementType = objects[0].toString();
    int negSuccCnt = 0;
%>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->
                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        
                        <div class="pageHead_1">Negotiation</div>
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div>&nbsp;</div>
                        <% pageContext.setAttribute("tab", "7");%>
                        <%@include  file="/officer/officerTabPanel.jsp"%>
                         <div class="tabPanelArea_1">
                         <%-- Start: Common Evaluation Table --%>
                      <%@include file="/officer/EvalCommCommon.jsp" %>
                    <%-- End: Common Evaluation Table --%>

                        <%  pageContext.setAttribute("TSCtab", "7"); %>
                        <div class="t_space">
                        <%@include file="../resources/common/AfterLoginTSC.jsp" %>
                        </div>
                        <div class="tabPanelArea_1">
                            <div class="b_space">
                            <% if(flag){ %>
                                <div id="successMsg" class="responseMsg successMsg" style="display:block"><%=msg%></div>
                            <% }else{ %>
                                <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                             <% } %>
                             </div>
                             <% if(isTECPEC){ %>
                                 
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    
                                      <%if(pendingNegId == 0){%>
                                        <td width="30%" class="t-align-left ff">Negotiation :</td>
                                        <td width="70%" id="negConfig">
                                            <a href="<%=request.getContextPath()%>/officer/NegotiationConfig.jsp?tenderId=<%=tenderId%>">Configure</a>
                                        </td>
                                      <%}else{%>
                                        <td width="30%" class="t-align-left ff">Current Negotiation :</td>
                                        <td width="70%" id="negConfig">
                                            <a href="<%=request.getContextPath()%>/officer/NegotiationClose.jsp?tenderId=<%=tenderId%>&negId=<%=pendingNegId%>">Close</a>
                                        </td>
                                      <%}%>
                                </tr>
                            </table>
                                <div class='responseMsg noticeMsg t_space'>
                                     <ol type="a"  class="t_space b_space" style="margin-left: 30px; line-height: 20px;" >
                                         <li>Please prepare a document with all the required changes and negotiations to be made to the respective forms</li>
                                         <li>Click on "Process" to select the forms and upload the documents.</li>
                                         <li>Selection of the forms can be done only once</li>
                                     </ol>
                                 </div>
                            <% } %>
                        
                        <div>&nbsp;</div>
                        
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="4%">Sl. No.</th>
                                    <th width="11%">Consultant Name</th>
                                    <th width="15%">Start Date and Time</th>
                                    <th width="15%">End Date and Time</th>
                                    <th width="10%">Mode of Negotiation</th>
                                    <th width="10%">Invitation <br/>Status</th>
                                    <th width="5%">Comments of consultant for Invitation Acceptance/Rejection</th>
                                    <th>Form Revision Status</th>
                                    <th width="10%">Final Negotiation <br/>Status</th>
                                    <th width="5%">Online Negotiation</th>
                                    <!-- Dohatec Start : 19.Apr.15 : Istiak -->
                                    <th width="5%" valign="middle" class="t-align-center">Previous Winning Bidder History</th>
                                    <!-- Dohatec End -->
                                    <th width="20%">Action</th>
                                </tr>
                                <%
                                String roundId = null;
                                   if(!tenderCommonDatas.isEmpty()){
                                   int count=0;
                                   int negId = 0;
                                   for (SPTenderCommonData negDetails : tenderCommonDatas) {
                                        negId = Integer.parseInt(negDetails.getFieldName1());

                                        List<SPTenderCommonData> tenderAllCommonDatas = neg.getAllNegotiationDetails(negId);
                                %>
                                <tr>
                                    <td class="t-align-center">&nbsp;<%=++count%> </td>
                                    <td class="t-align-center">&nbsp;<% if(!"".equals(negDetails.getFieldName4())){ out.print(negDetails.getFieldName4()); }else{ out.print("-"); } %></td>
                                    <td class="t-align-center">&nbsp;<% if(!"".equals(negDetails.getFieldName2())){ out.print(negDetails.getFieldName2()); }else{ out.print("-"); } %></td>
                                    <td class="t-align-center">&nbsp;<% if(!"".equals(negDetails.getFieldName3())){ out.print(negDetails.getFieldName3()); }else{ out.print("-"); } %></td>
                                    <td class="t-align-center">&nbsp;<% if(!"".equals(negDetails.getFieldName7())){ out.print(negDetails.getFieldName7()); }else{ out.print("-"); } %></td>
                                    <td class="t-align-center">&nbsp;<% if("Accept".equalsIgnoreCase(negDetails.getFieldName8())){ %>Accepted<% }else if("Reject".equalsIgnoreCase(negDetails.getFieldName8())){ %>Rejected<% }else{ out.println(negDetails.getFieldName8()); } %></td>
                                    <%--<td class="t-align-center">&nbsp;<% if("Accept".equalsIgnoreCase(negDetails.getFieldName10())){ %>Accepted<% }else if("Reject".equalsIgnoreCase(negDetails.getFieldName10())){ %>Rejected<% }else{ out.println(negDetails.getFieldName10()); } %></td>--%>
                                    <td class="t-align-center">
                                        <a href="javascript:void(0);" onclick="commentsAll('<%=tenderAllCommonDatas.get(0).getFieldName10()%>')">View</a>  
                                        </td>
                                    <td class="t-align-center">
                                        <%
                                            if("Accept".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3()))
                                                { %>Accepted<% }
                                            else if("Reject".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3()))
                                                { %>Rejected<% }
                                            else if("yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName6()))
                                                { %>Revised<% }
                                            else if("resend".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3()))
                                                { %>Resend<% }
                                            else { %>-<%}
                                        %>
                                        
                                    </td>
                                        <td class="t-align-center">&nbsp;<% out.print(negDetails.getFieldName9()); if(negDetails.getFieldName9().equals("Successful")){negSuccCnt++;}%></td>
                                    <td class="t-align-center">&nbsp;
<%
    if("Online".equalsIgnoreCase(negDetails.getFieldName7())){
        if("Pending".equalsIgnoreCase(negDetails.getFieldName9())){
            if("Accept".equalsIgnoreCase(negDetails.getFieldName8())){
                if("".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName6())){
                    if("Services".equalsIgnoreCase(procurementType)){
                       if("".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3())  || "resend".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3())){
%>
<a href="ViewNegQuestions.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>"><span style="color: red;">*</span>Online Negotiation</a>
<%
                        }
                    }else{
%>
                          <a href="ViewNegQuestions.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>"><span style="color: red;">*</span>Online Negotiation</a>
<%
                    }
                }else{
                        if("Services".equalsIgnoreCase(procurementType) &&( "".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3())) || "resend".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3())){
%>
                                <a href="ViewNegQuestions.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>"><span style="color: red;">*</span>Online Negotiation</a>
<%

                        }else{
%>
                          <a href="ViewNegQuestions.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>&type=View">View</a>
<%
                        }
                }
            }
        }else{
%>
                          <a href="ViewNegQuestions.jsp?tenderId=<%=tenderId%>&negId=<%=negDetails.getFieldName1()%>&type=View">View</a>
<%
         }
    }else{
                                            out.print("-");
    }
%>
                                    </td>

                                    <!-- Dohatec Start : 19.Apr.15 : Istiak -->
                                    <td class="t-align-center">
                                        <a href="WinningBidderHistory.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&flag=s">View</a>
                                    </td>
                                    <!-- Dohatec End -->

                                    <td class="t-align-center">&nbsp;
<%

    if("Online".equalsIgnoreCase(negDetails.getFieldName7()))
    {
        if("Pending".equalsIgnoreCase(negDetails.getFieldName9()) || "Successful".equalsIgnoreCase(negDetails.getFieldName9()))
        {
            if("Accept".equalsIgnoreCase(negDetails.getFieldName8()))
            {
                if("Services".equalsIgnoreCase(procurementType))
                {
                    if("".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3()) || "resend".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName3()))
                    {
                        if("Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName6()))
                        {
 %>
                        <a href="NegApproveBid.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">Process</a>
 <%
                        }
                        else if("Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName2()))
                        {
%>
                        <a href="NegForms.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">Negotiation View</a> | <a href="NegApproveBid.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">Reject</a>
<%
                        }
                        else
                        {
%>
                        <a href="NegotiationForms.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">Process</a> | <a href="NegApproveBid.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">Reject</a>
<%
                        }
                    }
                    else
                    {
                        if(isTECPEC)
                        {
                            if("".equalsIgnoreCase(negDetails.getFieldName10()) && "Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName6()))
                            {
                              if(negDetails.getFieldName9()!= null && negDetails.getFieldName9().equalsIgnoreCase("pending"))
                                    {
%>
                                        <a href="NegotiationDocsUpload.jsp?negId=<%=negDetails.getFieldName1()%>&tenderId=<%=tenderId%>">Upload Negotiation Doc.</a>

                                        &nbsp;|&nbsp;
                                        <%
                                    }
                                    if("Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName8()))
                                    {
%>     
                                        <a href="NegForms.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">View Revised Form</a>
                                       &nbsp;|&nbsp;
<%
                                    }
                            }
                            else if("Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName8()))
                            {
%>
                                        <a href="NegForms.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">View Revised Form</a>
                                        &nbsp;|&nbsp;
<%
                            }
                            %>
                            <a href="<%=request.getContextPath()%>/resources/common/ViewNegTendDocs.jsp?negId=<%=negDetails.getFieldName1()%>&tenderId=<%=tenderId%>" >View Docs</a>
                            <%

                        }
                    }                    
                }
                else
                {
                    if("".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName6()) && "".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName2()))
                    {
%>
                                       <a href="NegBidPrepare.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">Process</a> | <a href="NegApproveBid.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">Reject</a>
<%
                    }
                    else
                    {
                        if(isTECPEC)
                        {
                            if("".equalsIgnoreCase(negDetails.getFieldName10()) && "Accept".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName6()))
                            {
%>
                                        <a href="NegotiationDocsUpload.jsp?negId=<%=negDetails.getFieldName1()%>&tenderId=<%=tenderId%>">Upload Negotiation Doc.</a>  | <a href="NegForms.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">View Revised Form</a>
                                        <%
                                    if("Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName8()))
                                    {
%>
                                        <a href="<%=request.getContextPath()%>/resources/common/ViewNegTendDocs.jsp?negId=<%=negDetails.getFieldName1()%>&tenderId=<%=tenderId%>" >View Docs</a>
<%
                                    }
                            }
                            else if("Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName8()))
                            {
%>
                                        <a href="<%=request.getContextPath()%>/resources/common/ViewNegTendDocs.jsp?negId=<%=negDetails.getFieldName1()%>&tenderId=<%=tenderId%>" >View Docs</a>
<%
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if("Accept".equalsIgnoreCase(negDetails.getFieldName8()))
            {
                if("Services".equalsIgnoreCase(procurementType))
                {
                    if(isTECPEC)
                        {
                            if("".equalsIgnoreCase(negDetails.getFieldName10()) && "Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName6()))
                            {
                                %>
                                        <a href="NegForms.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">View Revised Form</a>
                                <%
                            }
                           
                                %>
                                        <a href="<%=request.getContextPath()%>/resources/common/ViewNegTendDocs.jsp?negId=<%=negDetails.getFieldName1()%>&tenderId=<%=tenderId%>" >View Docs</a>
                                <%
                            
                        }
                }
                  //  out.print("-");
            }

        }
    }
    else
    {
        if (isTECPEC)
        {
            if ("Accept".equalsIgnoreCase(negDetails.getFieldName8()) && "".equalsIgnoreCase(negDetails.getFieldName10()))
            {
%>
                <a href="NegotiationDocsUpload.jsp?negId=<%=negDetails.getFieldName1()%>&tenderId=<%=tenderId%>">Upload Negotiation Doc.</a> | <a href="NegForms.jsp?tenderId=<%=tenderId%>&uId=<%=negDetails.getFieldName6()%>&negId=<%=negDetails.getFieldName1()%>">View Revised Form</a>
<%
                    if ("Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName8()))
                    {
%>
                     <br />&nbsp;&nbsp;<a href="<%=request.getContextPath()%>/resources/common/ViewNegTendDocs.jsp?negId=<%=negDetails.getFieldName1()%>&tenderId=<%=tenderId%>" >View Docs</a>
<%
                    }
            } 
            else
            {
                if ("Yes".equalsIgnoreCase(tenderAllCommonDatas.get(0).getFieldName8()))
                {
%>
                     <a href="<%=request.getContextPath()%>/resources/common/ViewNegTendDocs.jsp?negId=<%=negDetails.getFieldName1()%>&tenderId=<%=tenderId%>" >View Docs</a>
<%
                }
            }
        }
    }
%>
                                    </td>
                                </tr>
                                <%
                                if(!tenderAllCommonDatas.isEmpty()){
                                    roundId = tenderAllCommonDatas.get(0).getFieldName9();
                                }
                                    } }else{
                                %>
                                <tr>
                                    <td colspan="12">No Records Found</td>
                                </tr>
                                <% } %>
                            </table>
                            <%
                            //roundId = "756";
                            //out.print(roundId);
                                if(roundId!=null){
                                    CommonSearchDataMoreService dataMoreBid = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> getreportID = dataMoreBid.geteGPData("checkNegStartPoint", roundId,String.valueOf(tenderId),"0");
                                    if((getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1"))){
                             %>
                             <script type="text/javascript">
                                 $("#negConfig").html("<div class='responseMsg noticeMsg'>No Bidder Found</div>")
                             </script>
                             <%
                                    }else if(negSuccCnt!=0){
                                        %>
                                      <script type="text/javascript">
                                         $("#negConfig").html("<div class='responseMsg noticeMsg'>Negotiation Completed Sucessfully</div>")
                                       </script>  
                                    <%}
                                }
                            %>                            
                        </div>
                         </div>
                        
                        <%@include file="../resources/common/Bottom.jsp" %>
                    </div>
                    <div id="dialog-message" title="View Comments of consultant for Invitation Acceptance/Rejection">
                                    <span class="t-align-left" id="commentsTd"></span>
                            
                   </div>
                </div>
            </div>
                    
        </div>
                    
<script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
                    
<script type="text/javascript">
$(function() {
        // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
        $( "#dialog:ui-dialog" ).dialog( "destroy" );
        $( "#dialog-message" ).dialog({
                autoOpen: false,
                resizable:false,
                draggable:true,
                height: 200,
                width: 300,
                modal: true,
                buttons: {
                        Ok: function() {
                                $( this ).dialog( "close" );
                        }
                }
        });
});

        function commentsAll(cmt){
            $("#commentsTd").html(cmt);
            $("#dialog-message").dialog("open");
            return false;
        }
</script>
</html>
