<%-- 
    Document   : PreTenderMetDocUpload
    Created on : Nov 29, 2010, 3:51:08 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Pre Tender Meeting Document Upload</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
<script type="text/javascript">

            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:50}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please select Document</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please enter Description</div>",
                                        maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"}
                    }
                });
            });
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpld').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body onunload="doUnload()">
        <div class="mainDiv">
            <div class="fixDiv">
             <%@include file="../resources/common/AfterLoginTop.jsp"%>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->                        
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
                        int tenderId = 0;
                        if(session.getAttribute("userId")==null){
                                response.sendRedirect("SessionTimedOut.jsp");
                        }

                        if (request.getParameter("tenderId") != null){
                            tenderId = Integer.parseInt(request.getParameter("tenderId"));
                        }
%>
            <div class="pageHead_1">Pre Tender Meeting Document Upload
                <span class="c-alignment-right"><a class="action-button-goback" href="PreTenderMeeting.jsp?tenderid=<%=tenderId%>"> Go Back To Dashboard</a></span>
            </div>
  
           <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/PreTenderMetDocUploadServlet" enctype="multipart/form-data" name="frmUploadDoc">
                <input type="hidden" name="tenderId" value="<%= tenderId%>"/>
              <%   pageContext.setAttribute("tenderId", tenderId); %>                


                <%
                                if (request.getParameter("fq") != null) {
                    %>
                    <div> &nbsp;</div>
                    <div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
                    <%
                                }
                                if (request.getParameter("fs") != null) {
                    %>
                     <div> &nbsp;</div>
                    <div class="responseMsg errorMsg">

                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                                }if(request.getParameter("up")!=null){
                    %>
                    <div> &nbsp;</div>
                    <div id="successMsg" class="responseMsg successMsg">
                        File uploaded successfully.
                    </div>
                    <%}if(request.getParameter("rm")!=null){%>
                    <div> &nbsp;</div>
                    <div id="successMsg" class="responseMsg successMsg">
                        File removed successfully.
                    </div>
                    <%}%>
              
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                     <tr>

                        <td style="font-style: italic" colspan="2"  class="fff t-align-left" >Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                    </tr>
                    <tr>
                        <td width="15%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                        <td width="85%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                        </td>
                    </tr>

                    <tr>
                        <td class="ff">Description : <span>*</span></td>
                        <td>
                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="51" id="documentBrief" style="width:200px;" />
                            <div id="dvDescpErMsg" class='reqF_1'></div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>

                            <label class="formBtn_1"><input type="submit" name="btnUpld" id="btnUpld" value="Upload"/></label>
                            <!--<label class="formBtn_1"><input type="button" name="btnClose" id="btnClose" value="Close" onclick="return closeWondow()" /></label>-->
                        </td>
                    </tr>
                </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="100%"  class="t-align-left">Instructions</th>
                                </tr>
                                <tr>
                                    <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");%>
                                    <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed  <%=tblConfigurationMaster.getFileSize()%>MB.
                                        <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                                </tr>
                            </table>
            </form>

                                 <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                     <tr>
                    <th width="4%" class="t-align-center">Sl. No.</th>
                    <th class="t-align-center" width="23%">File Name</th>
                    <th class="t-align-center" width=32%">File Description</th>
                    <th class="t-align-center" width="7%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-center" width="18%">Action</th>
                </tr>
                    <%

                    String tender = request.getParameter("tenderId");
                 
                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("PreTenderMetDocs", tender, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                        <td class="t-align-left"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3())/1024)%></td>
                         <td class="t-align-center">
                             <a href="<%=request.getContextPath()%>/PreTenderMetDocUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             &nbsp;
                             <a href="<%=request.getContextPath()%>/PreTenderMetDocUploadServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tender%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                         </td>
                    </tr>

                         <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
            </table>

            <div>&nbsp;</div>
        </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->


        </div>
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<script>
function doUnload()
{
   window.opener.getDocData();

}
</script>
