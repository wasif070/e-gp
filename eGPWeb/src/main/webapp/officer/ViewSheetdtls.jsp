<%-- 
    Document   : ViewSheetdtls
    Created on : Dec 12, 2011, 5:52:46 PM
    Author     : shreyansh Jogi
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="java.lang.reflect.Array"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.math.BigDecimal"%>


<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Attendance Sheet</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">View Attendance Sheet
            <span class="c-alignment-right">
                <%
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");            
                    String serviceType = commonService.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
                    if("Time based".equalsIgnoreCase(serviceType.toString()))
                    {    
                %>
                        <a href="ProgressReportMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                <%}else{%>
                        <a href="SrvLumpSumPr.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                <%}%>
            </span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "10");

            %>
                <div class="tabPanelArea_1">
                    <div align="center">

                        <%
                                    ResourceBundle bdl = null;
                                    bdl = ResourceBundle.getBundle("properties.cmsproperty");

                                %>
                                <form name="prfrm" id="prfrm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post">
                                 <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                 <th width="3%" class="t-align-left">Sr.No.</th>
                                                <th width="20%" class="t-align-left">Name of Employees</th>
                                                <th width="15%" class="t-align-left">Position Assigned
                                                </th>
                                                <th width="9%" class="t-align-left">Home / Field
                                                </th>
                                                <th width="5%" class="t-align-left">No of Days
                                                </th>
                                                <th width="18%" class="t-align-left">Month
                                                </th>
                                                <th width="18%" class="t-align-left">Year
                                                </th>
                                              
                                            </tr>

                                                <%
                                                 CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                                                List<Object[]> list = cmss.ViewAttSheetDtls(Integer.parseInt(request.getParameter("sheetId")));
                                                 MonthName monthName = new MonthName();

                                            if(list!=null && !list.isEmpty()){

                                                for(Object obj[] : list){%>
                                                <tr>
                                                <td width="3%" class="t-align-left"><%=obj[0]%></td>
                                                <td width="20%" class="t-align-left"><%=obj[1]%></td>
                                                <td width="15%" class="t-align-left"><%=obj[2]%>
                                                </td>
                                                <td width="9%" class="t-align-left"><%=obj[7]%>
                                                </td>
                                                <td width="5%" style="text-align: right" ><%=obj[3]%>
                                                </td>
                                                <td width="5%" class="t-align-left"><%=monthName.getMonth((Integer) obj[5]) %>
                                                </td>
                                                <td width="5%" style="text-align: right"><%=obj[6]%>
                                                </tr>

                                                   <% }}%>
                                 </table>
                                </form>
                    </div>
                </div>
            </div>
        </div>
        <%
            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId",EgpModule.Progress_Report.getName(), "View Attendance Sheet", "");
        %>
        <%@include file="../resources/common/Bottom.jsp" %>
    <script>
       
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }


    </script>
</html>

