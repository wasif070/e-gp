<%-- 
    Document   : ViewPostQualificationDtl
    Created on : Dec 13, 2010, 12:33:22 PM
    Author     : Administrator
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Post Qualification Detail</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%  String tenderId, userId;

            // CHECK FOR QUERYSTRING - tenderId
            if (request.getParameter("tenderId") != null)
                tenderId = request.getParameter("tenderId");
            else
                tenderId = "0";

            // CHECK FOR QUERYSTRING - uId
            if (request.getParameter("uId") != null)
                userId = request.getParameter("uId");
            else
                userId = "0";
        
            // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType="tenderId";

            int auditId=Integer.parseInt(tenderId);
            String auditAction="View PQ by TEC CP";
            String moduleName=EgpModule.Evaluation.getName();
            String remarks="";
            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <div class="contentArea_1">
                
                <!--Dashboard Content Part Start-->
                <div class="pageHead_1">View Post Qualification Detail
                    <!--<span style="float:right;"><a href="ProcessPQ.jsp?tenderId=< %=tenderId%>&pkgLotId=< %=request.getParameter("pkgLotId")%>" class="action-button-goback">Go Back</a></span>-->
                    <span style="float:right;"><a href="<%=request.getHeader("referer")!=null ? request.getHeader("referer") : "ProcessPQ.jsp?tenderId="+tenderId+"&pkgLotId="+request.getParameter("pkgLotId")%>" class="action-button-goback">Go Back</a></span>
                </div>
                <%
                     pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
                <%
                     pageContext.setAttribute("TSCtab", "6");
                %>
<!--               <-%@include  file="../resources/common/AfterLoginTSC.jsp"%> -->
                <div>&nbsp;</div>
                <table width="100%" cellspacing="0" class="tableList_1">
                    <%
                        List<SPTenderCommonData> siteStatusList = tenderCommonService.returndata("ViewPostQualification",
                                userId,
                                request.getParameter("pkgLotId"));
                        if (!siteStatusList.isEmpty()) {%>

                    <tr>
                        <td width="20%" class="ff" >Bidder/Consultant</td>
                        <td class="t-align-left"><%=siteStatusList.get(0).getFieldName8()%></td>

                        </tr><tr>
                        <td class="ff" width="10%" >Site Visit Requires?</td>
                        <td class="t-align-left"><%=siteStatusList.get(0).getFieldName1()%></td>

                        </tr>
                        <% // Site visit require is yes
                            if( (siteStatusList.get(0).getFieldName1() != null) && (siteStatusList.get(0).getFieldName1().trim().equalsIgnoreCase("yes"))){%>
                        <tr>
                        <td class="ff" width="12%" >Planned Site Visit Date and Time</td>
                        <td class="t-align-left"><%
                        String temp[] = siteStatusList.get(0).getFieldName2().split("-");
                        String temp1[] = temp[2].split(" ");
                        if("1900".equalsIgnoreCase(temp1[0]) || siteStatusList.get(0).getFieldName1().equalsIgnoreCase("no")){
                             out.print("-");
                            }else{
                            out.print(siteStatusList.get(0).getFieldName2());
                            }
                            %></td>

                         </tr>
                         <%} // Site visit require is yes  complited%>
                         <tr>
                        <td class="ff" width="20%" >Comments</td>
                         <td class="t-align-left"><%=siteStatusList.get(0).getFieldName3()%></td>
                        </tr>
                        <% // Site visit require is yes
                            if( (siteStatusList.get(0).getFieldName1() != null) && (siteStatusList.get(0).getFieldName1().trim().equalsIgnoreCase("yes"))){%>
                        <tr>
                        <td class="ff" width="10%">Actual Site Visit Date and Time</td>
                         <td class="t-align-left"><%if(siteStatusList.get(0).getFieldName4()!=null)
                             out.print(siteStatusList.get(0).getFieldName4());
                        else
                            out.print("-");
                             %></td>

                         </tr><tr>
                        <td class="ff" width="25%" >Site Visit Comments</td>
                         <td class="t-align-left"><%=siteStatusList.get(0).getFieldName6()%></td>

                         </tr>
                         <%} // Site visit require is yes  complited%>
                         <tr>
                        <td class="ff" width="10%" >Post Qualification Status</td>
                        <td class="t-align-left"><%if(siteStatusList.get(0).getFieldName7().equalsIgnoreCase("Qualify")){out.print("Qualified");}else if("Initiated".equalsIgnoreCase(siteStatusList.get(0).getFieldName7())){out.print("Pending");}else{out.print("Disqualified");}%></td>
                    </tr>
                                       
                           
                   
                    <%
                        
                        } else {
                    %>
                    <tr>
                        <td class="t-align-left" colspan="8"><b> No post qualification detail found! </b></td>
                    </tr>
                    <% }%>
                </table>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="32%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%
                                String postQualId = "";
                                if(request.getParameter("postQualId") != null){
                                    postQualId = request.getParameter("postQualId");
                                }
                                int docCnt = 0;
                                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                                for (SPTenderCommonData sptcd : tenderCS.returndata("PostQualEstDocInfo", postQualId, "")) {
                                    docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/PostQualEstDocServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&postQualId=<%=postQualId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                                             sptcd = null;
                                         }
                                     }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>

            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
