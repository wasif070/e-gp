<%-- 
    Document   : ViewDSHistory
    Created on : Sep 15, 2011, 12:00:54 PM
    Author     : dixit
--%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetailHistory"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<!-- Dohatec Start -->
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.utility.CommonUtils"%>
<!-- DohatecEnd -->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View History</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.print.blocklink.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <%
            String tenderId = "";
            if (request.getParameter("tenderId") != null) {
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                tenderId = request.getParameter("tenderId");
            }
            String lotId = "";
            if (request.getParameter("lotId") != null) {
                pageContext.setAttribute("lotId", request.getParameter("lotId"));
                lotId = request.getParameter("lotId");
            }
            String wpId = "";
            if (request.getParameter("wpId") != null) {
                wpId = request.getParameter("wpId");
            }
            String historyCount = "";
            if (request.getParameter("historyCount") != null) {
                historyCount = request.getParameter("historyCount");
            }
            String userId = "";
            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = session.getAttribute("userId").toString();

            }
            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");                        
            CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
            List<Object[]> list = cs.getLotDetailsByPkgLotId(lotId,tenderId);
            String procnature = cs.getProcNature(tenderId).toString();
        %>

        <%
                //Added by Salahuddin
                TenderCommonService tcs1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> listDP = tcs1.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                boolean isIctTender = false;
                if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                    isIctTender = true;
                }                

            %>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div  id="print_area">
            <div class="contentArea_1">
                <div class="pageHead_1">View History
                    <span class="noprint" style="float: right; text-align: right;">
                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                    <a class="action-button-goback" href="ViewDSHistoryLinks.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=<%=wpId%>" title="Go Back">Go Back</a>
                    </span>
                </div>
                
                    <%@include file="../resources/common/TenderInfoBar.jsp"%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                <div>&nbsp;</div>
                
                <div class="tabPanelArea_1">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
               <%for(Object[] obj : list){%>
                    <tr>
                        <td width="20%">Lot No.</td>
                        <td width="80%"><%=obj[0] %></td>
                    </tr>
                    <tr>
                        <td>Lot Description</td>
                        <td class="t-align-left"><%=obj[1]%></td>
                    </tr>
                    <%}%>
                </table>

            <br />
            <%if(isIctTender){%>    <!-- Added by Dohatec for ICT Tender -->
                <table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
                    <tr>
                        <th width="3%" class="t-align-center">S.No</th>
                        <%if("works".equalsIgnoreCase(procnature)){%>
                            <th width="20%" class="t-align-center">Group</th>
                        <%}%>
                        <th width="20%" class="t-align-center">Description of Item</th>
                        <th width="15%" class="t-align-center">Measurement Unit</th>
                        <th width="10%" class="t-align-center">Quantity </th>
                        <th width="10%" class="t-align-center">Unit Rate</th>
                        <th width="10%" class="t-align-center">Currency</th>
                        <!--<th width="10%" class="t-align-center">Supplier VAT (in BTN)</th>-->
                        <th width="15%" class="t-align-center ICT">Supplier VAT + Other Local Cost (in Nu.)</th>
                        <%if("works".equalsIgnoreCase(procnature)){%>
                            <th width="18%" class="t-align-center">Start Date</th>
                        <%}else{%>
                            <th width="18%" class="t-align-center">No. of Days</th>
                        <%}%>
                        <%if("works".equalsIgnoreCase(procnature)){%>
                            <th width="18%" class="t-align-center">End Date</th>
                        <%}else{%>
                            <th width="18%" class="t-align-center">Delivery Date</th>
                        <%}%>
                        <th width="18%" class="t-align-center">Created Date</th>
                    </tr>
                    <%
                        CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        List<SPCommonSearchDataMore> getwphistory = csdms.geteGPData("getWPDetailHistory", wpId, historyCount, procnature);
                        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
                        if(!getwphistory.isEmpty())
                        {
                            for(int j=0; j<getwphistory.size(); j++)
                            {
                                    /*
                                    FieldName1 = wpSrNo
                                    FieldName2 = wpDescription
                                    FieldName3 = wpUom
                                    FieldName4 = wpQty
                                    FieldName5 = wpEndDate
                                    FieldName6 = groupId
                                    FieldName7 = wpStartDate
                                    FieldName8 = wpNoOfDays
                                    FieldName9 = wpRate
                                    FieldName10 = createDate
                                    FieldName11 = currency Name
                                    FieldName12 = VAROthers
                                    */
                    %>
                    <tr>
                        <td class="t-align-center"><%=CommonUtils.checkNull(getwphistory.get(j).getFieldName1())%></td>
                        <%if("works".equalsIgnoreCase(procnature)){%>
                            <td class="t-align-center"><%=CommonUtils.checkNull(getwphistory.get(j).getFieldName6())%></td>
                        <%}%>
                        <td class="t-align-center"><%=CommonUtils.checkNull(getwphistory.get(j).getFieldName2())%></td>
                        <td class="t-align-center"><%=CommonUtils.checkNull(getwphistory.get(j).getFieldName3())%></td>
                        <td class="t-align-right" style="text-align: right;"><%=CommonUtils.checkNull(getwphistory.get(j).getFieldName4())%></td>
                        <td class="t-align-center"><%=CommonUtils.checkNull(getwphistory.get(j).getFieldName9())%></td>
                        <td class="t-align-center"><%=CommonUtils.checkNull(getwphistory.get(j).getFieldName11())%></td>
                        <td class="t-align-center"><%=CommonUtils.checkNull(getwphistory.get(j).getFieldName12())%></td>
                        <%if("works".equalsIgnoreCase(procnature)){%>
                            <%if(getwphistory.get(j).getFieldName7()!=null){%>
                                <td class="t-align-center"><%=getwphistory.get(j).getFieldName7().trim().replace(" ", "-")%></td>
                            <%}else{%>
                                <td class="t-align-center">-</td>
                            <%}%>
                        <%}else{%>
                            <td class="t-align-center"><%=CommonUtils.checkNull(getwphistory.get(j).getFieldName8())%></td>
                        <%}%>
                   
                        <%if(getwphistory.get(j).getFieldName5()!=null){%>
                            <td class="t-align-center"><%=getwphistory.get(j).getFieldName5().trim().replace(" ", "-")%></td>
                        <%}else{%>
                            <td class="t-align-center">-</td>
                        <%}%>
                        <%if(getwphistory.get(j).getFieldName10()!=null){%>
                            <td class="t-align-center"><%=getwphistory.get(j).getFieldName10().substring(0, 20)%></td>
                        <%}else{%>
                            <td class="t-align-center">-</td>
                        <%}%>
                    </tr>
                    <%}}else{%>
                    <td class="t-align-center" colspan="5">No Records Found</td>
                    <%}%>
                </table>
            <%} else{%> <!-- old Version -->
                <table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
                    <tr>
                        <th width="3%" class="t-align-center">S.No</th>
                        <%if("works".equalsIgnoreCase(procnature)){%>
                        <th width="20%" class="t-align-center">Group</th>
                        <%}%>
                        <th width="20%" class="t-align-center">Description of Item</th>
                        <th width="15%" class="t-align-center">Measurement Unit</th>
                        <th width="10%" class="t-align-center">Quantity </th>
                        <th width="10%" class="t-align-center">Unit Rate</th>
                        <%if("works".equalsIgnoreCase(procnature)){%>
                        <th width="18%" class="t-align-center">Start Date</th>
                        <%}else{%>
                        <th width="18%" class="t-align-center">No. of Days</th>
                        <%}%>
                        <%if("works".equalsIgnoreCase(procnature)){%>
                        <th width="18%" class="t-align-center">End Date</th>
                        <%}else{%>
                        <th width="18%" class="t-align-center">Delivery Date</th>
                        <%}%>
                        <th width="18%" class="t-align-center">Created Date</th>
                    </tr>
                    <%
                        List<Object[]> getwphistory = service.getWPDetailHistory(Integer.parseInt(wpId),Integer.parseInt(historyCount),procnature);
                        if(!getwphistory.isEmpty())
                        {
                            for(int j=0; j<getwphistory.size(); j++)
                            {
                                Object[] obj = getwphistory.get(j);
                    %>
                    <tr>
                        <td class="t-align-center"><%=obj[0]%></td>
                        <%if("works".equalsIgnoreCase(procnature)){%>
                        <td class="t-align-center"><%=obj[5]%></td>
                        <%}%>
                        <td class="t-align-center"><%=obj[1]%></td>
                        <td class="t-align-center"><%=obj[2]%></td>
                        <td class="t-align-right" style="text-align: right;"><%=obj[3]%></td>
                        <td class="t-align-center"><%=obj[8]%></td>
                        <%if("works".equalsIgnoreCase(procnature)){%>
                        <td class="t-align-center"><%=DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[6].toString(),"yyyy-MM-dd HH:mm:ss"))%></td>
                        <%}else{%>
                        <td class="t-align-center"><%=obj[7].toString()%></td>
                        <%}%>
                        <td class="t-align-center"><%=DateUtils.customDateFormate(DateUtils.convertStringtoDate(obj[4].toString(),"yyyy-MM-dd HH:mm:ss"))%></td>
                        <%if(obj[9]!=null){%>
                            <td class="t-align-center"><%=DateUtils.gridDateToStr((Date)obj[9])%></td>
                        <%}else{%>
                            <td class="t-align-center">-</td>
                        <%}%>
                    </tr>
                    <%}}else{%>
                    <td class="t-align-center" colspan="5">No Records Found</td>
                    <%}%>
                </table>
            <%}%>
            `</div>
            </div>
        </div></div>

    <div>&nbsp;</div>
    <%@include file="../resources/common/Bottom.jsp" %>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

