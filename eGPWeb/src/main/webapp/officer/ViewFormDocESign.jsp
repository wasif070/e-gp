<%-- 
    Document   : ViewFormDocESign
    Created on : Jun 14, 2011, 4:14:34 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Bidder/Consultant Submitted Forms and Documents e-Signature</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <!--
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                $('#print_area').printElement(options);
            }
        </script>
    </head>
    <%
                String userId = "";

                String isPDF = "abc";
                if (request.getParameter("isPDF") != null) {
                    isPDF = request.getParameter("isPDF");
                }
                pageContext.setAttribute("isPDF", isPDF);

                
                if (request.getParameter("userId") != null && !"".equalsIgnoreCase(request.getParameter("userId").toString())) {
                    userId = request.getParameter("userId").toString();
                }

                pageContext.setAttribute("userId", userId);

                boolean isSubDt = false;
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }


                String folderName = folderName = pdfConstant.SUBMISSIONRECEIPT;
                String genId = tenderId;

                //swati--to generate pdf
                if (!(isPDF.equalsIgnoreCase("true"))) {
                    try {
                        String reqURL = request.getRequestURL().toString();
                        String reqQuery = "tenderId=" + tenderId + "&userId=" + userId;
                        pdfCmd.genrateCmd(reqURL, reqQuery, folderName, genId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                String tendererName = commonService.getConsultantName(userId);
                //end

                //List<CommonTenderDetails> commonTenderDetails = tenderSrBean.getAPPDetails(Integer.parseInt(tenderId), "tender");
                //String docAvlMethod = commonTenderDetails.get(0).getDocAvlMethod();
                //String docAvlMethod = commonTenderDetails.get(0).getTenderLotSecId();

    %>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <% }%>
                <div class="contentArea_1">
                    <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                    <div class="pageHead_1">View Bidder/Consultant Submitted Forms and Documents e-Signature</div>
                    <% }%>
                    <div  id="print_area">
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>


                        <%
                                    int disp_cnt=1;
                                    boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                        %>
                        <div class="t_space" >&nbsp;</div>                        
                        <div class="tabPanelArea_1">
                            <%
                                        //Message Added by TaherT
                                        if ("with".equals(request.getParameter("msg"))) {
                                            out.print("<br/><div class='responseMsg successMsg'>You have withdrawn your submission successfully</div>");
                                        }
                                        if ("mod".equals(request.getParameter("msg"))) {
                                            out.print("<br/><div class='responseMsg successMsg'>Reason entered successfully. Please proceed for modification</div>");
                                        }
                            %>
                            <%
                                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                        List<SPCommonSearchData> submitDateSts = commonSearchService.searchData("chkSubmissionDt", tenderId, null, null, null, null, null, null, null, null);
                                        if (submitDateSts.get(0).getFieldName1().equalsIgnoreCase("true")) {
                                            isSubDt = true;
                                        } else {
                                            isSubDt = false;
                                        }
                                        if (isTenPackageWis) {
                                            //taher
                                            //List<SPCommonSearchData> packageList = ;
                                            for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                                </tr>

                                <tr>
                                    <td width="22%" class="t-align-left ff">Package No. :</td>
                                    <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Package Description :</td>
                                    <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                                </tr>
                            </table>
                            <div class="tableHead_1 t-align-center t_space"><%=tendererName%></div>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="4%" class="t-align-left ff">Sl. No.</th>
                                    <th width="46%" class="t-align-left ff">Form Name</th>
                                    <!--<th width="25%" class="t-align-left">Mapped Documents List</th>-->
                                    <th width="10%" class="t-align-center ff">Filled (Yes/No)</th>
                                    <!--<th width="22%" class="t-align-center ff">Encrypted with Buyer Hash</th>-->
                                    <th width="40%" class="t-align-left ff">e-Signature / Hash</th>
                                </tr>
                                <%
                                                                            //Taher Starts
                                                 
                                                                            List<SPCommonSearchData> techFormList = commonSearchService.searchData("getformnamebytenderidandlotid_ForLotPrep", tenderId, "0", "Technical", userId, null, null, null, null, null);
                                                                            for (SPCommonSearchData formname : techFormList) {
                                %>
                                <tr>
                                    <td class="t-align-center"><%=disp_cnt%></td>
                                    <td class="t-align-left"><%out.print(formname.getFieldName1());
                                                            if ("c".equalsIgnoreCase(formname.getFieldName6())) {
                                                                out.print("<font color='red'>(cancelled)</font>");
                                                            }%></td>
                                    <%--<td class="t-align-center">
                                                <% List<SPCommonSearchData> mapedDLL = commonSearchService.searchData("getTenderDoc", tenderId, formname.getFieldName5(), userId, null, null, null, null, null, null);
                                                    if(!mapedDLL.isEmpty()){
                                                                                                                            for (SPCommonSearchData mapedDL : mapedDLL) {

                                    %>
                                    <%=mapedDL.getFieldName2()%><br/>
                                    <% } }else{out.print("-");}%></td>--%>
                                    <td class="t-align-center"><%=formname.getFieldName11()%></td>
                                <!--<td class="t-align-center"><%=formname.getFieldName7()%></td>-->
                                    <td class="t-align-center"><%if (formname.getFieldName10() == null || "null".equalsIgnoreCase(formname.getFieldName10())) {
                                                                out.print("-");
                                                            } else {
                                                                out.print(formname.getFieldName10());
                                                            }%></td>

                                </tr>
                                <%
                                                disp_cnt++;
                                                                            }
                                %>
                                <%
                                                                            List<SPCommonSearchData> lotIdDesc = commonSearchService.searchData("getlotid_lotdesc", tenderId, userId, null, null, null, null, null, null, null);
                                                                            for (SPCommonSearchData lotdsc : lotIdDesc) {
                                                                                out.print("<tr><td colspan='5'><table width='100%' class='tableList_1' cellspacing='0'><tr><td width='15%' class='t-align-left ff'>Lot No.</td><td width='85%'>" + lotdsc.getFieldName4() + "</td></tr><tr><td class='t-align-left ff'>Lot Description</td><td>" + lotdsc.getFieldName2() + "</td></tr></table></td></tr>");
                                                                                out.print("<tr><th colspan='5'>"+tendererName+"</th></tr>");
                                                                                out.print("<tr>");
                                                                                out.print("<th width='4%' class='t-align-left ff'>Sl. No.</th>");
                                                                                out.print("<th width='46%' class='t-align-left ff'>Form Name</th>");
                                                                                //out.print("<th width='25%' class='t-align-left'>Mapped Documents List</th>");
                                                                                out.print("<th width='10%' class='t-align-center ff'>Filled (Yes/No)</th>");
                                                                                //out.print("<th width='22%' class='t-align-center ff'>Encrypted with Buyer Hash</th>");
                                                                                out.print("<th width='40%' class='t-align-left ff'>e-Signature / Hash</th>");
                                                                                out.print("</tr>");
                                                                                List<SPCommonSearchData> priceFormList = commonSearchService.searchData("getformnamebytenderidandlotid_ForLotPrep", tenderId, "0", "Pricebid", userId, lotdsc.getFieldName1(), null, null, null, null);
                                                                                disp_cnt=1;
                                                                                for (SPCommonSearchData formdetails : priceFormList) {
                                                                                    //for (SPCommonSearchData formdetails : commonSearchService.searchData("getformDetails", tenderId, userId, "0", null, null, null, null, null, null)) {
%>
                                <tr>
                                    <td class="t-align-center"><%=disp_cnt%></td>
                                    <td class="t-align-left"><%out.print(formdetails.getFieldName1());
                                                             if ("c".equalsIgnoreCase(formdetails.getFieldName6())) {
                                                                 out.print("<font color='red'>(cancelled)</font>");
                                                             }%></td>
                                    <%--<td class="t-align-center"><% List<SPCommonSearchData> mapedDLL = commonSearchService.searchData("getTenderDoc", tenderId, formdetails.getFieldName5(), userId, null, null, null, null, null, null);
                                                   if(!mapedDLL.isEmpty()){
                                                                                                                           for (SPCommonSearchData mapedDL : mapedDLL) {

                                    %>
                                    <%=mapedDL.getFieldName2()%><br/>
                                    <% } }else{out.print("-");}%></td>--%>
                                    <td class="t-align-center"><%=formdetails.getFieldName11()%></td>
                                    <%--<td class="t-align-center"><%=formdetails.getFieldName7()%></td>--%>
                                    <td class="t-align-center"><%if (formdetails.getFieldName10() == null || "null".equalsIgnoreCase(formdetails.getFieldName10())) {
                                                                 out.print("-");
                                                             } else {
                                                                 out.print(formdetails.getFieldName10());
                                                             }%></td>
                                </tr>
                                <% disp_cnt++;}
                                                                    }//Taher Ends%>
                            </table>
                            <% }
                                                                } else {

                                                                    for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                                </tr>
                                <tr>
                                    <td width="22%" class="t-align-left ff">Package No. :</td>
                                    <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Package Description :</td>
                                    <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                                </tr>
                            </table>
                            <%  }
                                                                    for (SPCommonSearchData lotList : commonSearchService.searchData("gettenderlotbytenderid", tenderId, "", "", null, null, null, null, null, null)) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">

                                <tr>
                                    <td width="22%" class="t-align-left ff">Lot No. :</td>
                                    <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Lot Description :</td>
                                    <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                                </tr>
                            </table>
                            <div class="tableHead_1 t-align-center t_space"><%=tendererName%></div>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="4%" class="t-align-left ff">Sl. No.</th>
                                    <th width="25%" class="t-align-left ff">Form Name</th>
<!--                                    <th width="20%" class="t-align-left">Mapped Documents List</th>-->
                                    <th width="15%" class="t-align-center ff">Filled (Yes/No)</th>
<!--                                    <th width="18%" class="t-align-center ff">Encrypted with Buyer Hash</th>-->
                                    <th width="18%" class="t-align-left ff">e-Signature / Hash</th>
                                </tr>
                                <%
                                            disp_cnt=1;
                                                                                                    for (SPCommonSearchData formdetails : commonSearchService.searchData("getformDetails", tenderId, userId, lotList.getFieldName3(), null, null, null, null, null, null)) {
                                %>
                                <tr>
                                    <td class="t-align-center"><%=disp_cnt%></td>
                                    <td class="t-align-left"><%=formdetails.getFieldName2()%></td>
<!--                                    <td class="t-align-left">
                                        < %
                                            for (SPCommonSearchData mapedDL : commonSearchService.searchData("getTenderDoc", tenderId, formdetails.getFieldName1(), userId, null, null, null, null, null, null)) {
                                                if (!"".equalsIgnoreCase(mapedDL.getFieldName2())) {
                                                     out.print(mapedDL.getFieldName2()+"<br/>");
                                                } else {
                                                     out.print("-");
                                                }
                                            }
                                        %>
                                    </td>-->
                                    <td class="t-align-center"><%=formdetails.getFieldName3()%></td>
<!--                                    <td class="t-align-center">< %=formdetails.getFieldName4()%></td>-->
                                    <td class="t-align-left"><%=formdetails.getFieldName5()%></td>
                                </tr>
                                <% disp_cnt++;}%>
                            </table>
                            <%     }
                                        }
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="4%" class="t-align-center ff">Sl. No.</th>
                                    <th width="21%" class="t-align-center ff">Form Name</th>
                                    <td width="75%" class="t-align-center ff">
                                        <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1">
                                            <tr>
                                                <th width="110px" class="t-align-left ff">Mapped Document’s Name</th>
                                                <th width="110px" class="t-align-left ff">File Name </th>
                                                <th width="150px" class="t-align-left ff">e-Signature / Hash</th>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%
                                            disp_cnt=1;
                                            int f_disp_cnt=1;
                                            List<SPCommonSearchData> formList = commonSearchService.searchData("getFormNameByTenderId", tenderId, "", "", "", "", "", "", "", "");
                                            for (SPCommonSearchData formname : formList) {
                                %>
                                <tr id="tr_<%=disp_cnt%>">
                                    <td class="t-align-center"><%=f_disp_cnt%></td>
                                    <td class="t-align-left"><%=formname.getFieldName1()%></td>
                                    <td >
                                        <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1" style="table-layout: fixed;">
                                            <%
                                                                                List<SPCommonSearchData> docList = commonSearchService.searchData("getDocRelatedInfo", tenderId, formname.getFieldName2(), "" + userId, "", "", "", "", "", "");
                                                                                if (docList.isEmpty()) {

                                            %>
                                            <tr>
                                            <!--For hiding forms that don't have documents(Taher)-->
                                            <script type="text/javascript">
                                                $('#tr_<%=disp_cnt%>').hide();
                                            </script>
                                                <td colspan="3" class="t-align-center ff" style="color: red;">No Records Found</td>
                                            </tr>
                                            <%                                                                    } else {
                                                                                    f_disp_cnt++;
                                                                                    for (SPCommonSearchData docname : docList) {
                                            %>
                                            <tr>
                                                <td width="110px" class="t-align-center" style="word-wrap: break-word;"><%if(docname.getFieldName2()!=null && (!docname.getFieldName2().equals(""))){out.print(docname.getFieldName2());}else{out.print("-");}%>&nbsp;</td>
                                                <td  width="110px" class="t-align-center" style="word-wrap: break-word;"><%=docname.getFieldName1()%>&nbsp;</td>
                                                <td width="150px" class="t-align-center" style="word-wrap: break-word;"><%=docname.getFieldName3()%>&nbsp;</td>
                                            </tr>
                                            <% }
                                                                         }%>
                                        </table>
                                    </td>
                                </tr>
                                <%  disp_cnt++; }
                                %>
                            </table>                                                       
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td width="30%" class="t-align-left ff">Your Mega Hash :</td>
                                    <td width="70%" class="t-align-left">
                                        <%
                                                    for (SPCommonSearchData megaHash : commonSearchService.searchData("getTenderMegaHash", tenderId, userId, "0", null, null, null, null, null, null)) {
                                                        out.println(megaHash.getFieldName1());
                                                    }
                                        %>

                                    </td>
                                </tr>
                                <%if (!isSubDt) {%>
                                <tr>
                                    <td class="t-align-left ff">Mega Mega Hash</td>

                                    <td class="t-align-left">
                                        <%
                                            for (SPCommonSearchData megaMegaHash : commonSearchService.searchData("getTenderMegaMegaHash", tenderId, userId, "0", null, null, null, null, null, null)) {

                                                out.println(megaMegaHash.getFieldName1());
                                            }
                                        %>
                                    </td>
                                </tr>
                                <%}%>
                            </table>                           
                        </div>
                    </div>
                </div>
                <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <% }%>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>

