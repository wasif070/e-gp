<%--
    Document   : ViewTscForm
    Created on : Feb 21, 2011, 12:37:28 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Forms / Comments</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <%  String tenderId, st = "", dayExp = "0";
                tenderId = request.getParameter("tenderId");
                st = request.getParameter("st");

                String userId = "", uId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                if (request.getParameter("uId") != null && !"".equalsIgnoreCase(request.getParameter("uId"))) {
                    uId = request.getParameter("uId");
                }
                String strComType = "null";
                            if (request.getParameter("comType") != null && !"null".equalsIgnoreCase(request.getParameter("comType"))) {
                                strComType = request.getParameter("comType");
                            }
            %>
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">View Forms / Comments
                    <span style="float:right;">
                        <a href="Evalclarify.jsp?tenderId=<%=tenderId%>&st=<%=st%>&comType=<%=strComType%>" class="action-button-goback">Go back to Dashboard</a>
                    </span>
                </div>
                <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%
                       boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                %>
                <div>&nbsp;</div>
                <%if(!"TEC".equalsIgnoreCase(request.getParameter("comType"))) { %>
                <jsp:include page="officerTabPanel.jsp" >
                     <jsp:param name="tab" value="7" />
                </jsp:include>
                <% } %>
                <div class="tabPanelArea_1">
                <%
                    if ("rp".equalsIgnoreCase(request.getParameter("st")))
                        pageContext.setAttribute("TSCtab", "3");
                    else
                        pageContext.setAttribute("TSCtab", "4");
                %>
                <%@include  file="../resources/common/AfterLoginTSC.jsp"%>

                    <% for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", uId, tenderId))
                        {
                            dayExp = companyList.getFieldName2();
                    %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                 <th colspan="2" class="t_align_left ff">Company Details</th>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Company Name :</td>
                                <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                            </tr>
                        </table>
                    <%
                        } // END FOR LOOP OF COMPANY NAME

                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            // CHECK WHETHER TENDER IS "PACKAGE" OR "LOT" WISE
                            if (isTenPackageWis) {

                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                    %>
                   <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th colspan="2" class="t-align-left ff">Package Information</th>
                        </tr>

                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="55%" class="t-align-center ff">Form Name</th>
                            <th width="15%" class="t-align-center ff">Comments Status</th>
                            <th width="30%" class="t-align-center">Action</th>
                        </tr>
                        <%
                            int i = 1;
                            
                            //loop for formsLot
                            for (SPCommonSearchData formdetails : commonSearchService.searchData("GetFormsforTSC", tenderId, "0","0", uId, null, null, null, null, null)) {
                                List<SPCommonSearchData> cmtStatus = commonSearchService.searchData("getEvaluationStatus", formdetails.getFieldName1(), tenderId,"0", uId, userId, null, null, null, null);
                                String statusDisp = null;
                                if(cmtStatus!=null && !cmtStatus.isEmpty()){
                                    statusDisp = cmtStatus.get(0).getFieldName1();
                                }else{
                                    statusDisp = "Comments Pending";
                                }
                                cmtStatus=null;
                                // HIGHLIGHT THE ALTERNATE ROW
                                if(Math.IEEEremainder(i,2)==0) {
                        %>
                        <tr class="bgColor-Green">
                         <% } else { %>
                        <tr>
                        <% } %>
                            <td class="t-align-left ff"><a href="ViewEvalBidform.jsp?tenderId=<%=tenderId%>&uId=<%=uId%>&formId=<%=formdetails.getFieldName1()%>&lotId=0&bidId=<%=formdetails.getFieldName3()%>&action=Edit&isSeek=true" target="_blank"><%=formdetails.getFieldName2()%></a></td>                            
                            <td class="t-align-right"><%=statusDisp%></td>
                            <td class="t-align-left">
                                <%
                                    // VISIBLE BELOW LINK, IF RESPONSE TIME IS OVER
                                String strTSCPost="Post Comments";
                                if("y".equals(request.getParameter("notify"))){
                                    strTSCPost="View Comments";
                                }
                                if("Comments Pending".equals(statusDisp) && "y".equals(request.getParameter("notify"))){
                                    out.print("-");
                                }else{
                                %>
                                <a href="EvalTscComments.jsp?tenderId=<%=tenderId%>&st=<%=st%>&uId=<%=uId%>&frmId=<%=formdetails.getFieldName1()%>&pkgLotId=0"><%=strTSCPost%></a>
<!--                                |
                                <a href="../resources/common/evalClariDoc.jsp?tenderId=< %=tenderId%>&st=< %=st%>&formId=< %=formdetails.getFieldName1()%>&pId=office&uId=< %=uId%>">Upload Document</a>  |
                                < %  %>
                                <a href="EvalViewQue.jsp?tenderId=<-%=tenderid%>&uid=<-%=uId%>">View Questions</a>
                                <a href="ViewQueAns.jsp?tenderId=<-%=tenderid%>&st=<-%=st%>&formId=<-%=formdetails.getFieldName1()%>&uid=<-%=uId%>"> Evaluate Tenderer</a>-->
                                <%}%>
                            </td>
                        </tr>
                        <%
                            i++;
                            }  // END FOR Form LOOP LotWise

                            if (i == 1) {
                         %>
                         <tr>
                             <td colspan="3">
                                 <b>No forms found</b>
                             </td>
                         </tr>
                         <% } %>
                    </table>
                        <% }
                             } //LotWise Condition
                        else {
                                 for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th colspan="2" class="t-align-center ff">Package Information</th>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%  }
                            for (SPCommonSearchData lotList : commonSearchService.searchData("gettenderlotbytenderid", tenderId, "", "", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                            <tr>
                                <td width="22%" class="t-align-left ff">Lot No. :</td>
                                <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Lot Description :</td>
                                <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="55%" class="t-align-center ff">Form Name</th>
                                <th width="15%" class="t-align-center ff">Comments Status</th>
                                <th width="30%" class="t-align-center">Action</th>
                            </tr>
                            <%
                                int i = 1;
                                for (SPCommonSearchData formdetails : commonSearchService.searchData("GetFormsforTSC", tenderId, "1", lotList.getFieldName3(), uId, userId, null, null, null, null)) {
                                    List<SPCommonSearchData> cmtStatus = commonSearchService.searchData("getEvaluationStatus", formdetails.getFieldName1(), tenderId,lotList.getFieldName3(), uId, userId, null, null, null, null);
                                    String statusDisp = null;
                                    if(cmtStatus!=null && !cmtStatus.isEmpty()){
                                        statusDisp = cmtStatus.get(0).getFieldName1();
                                    }else{
                                        statusDisp = "Comments Pending";
                                    }
                                    cmtStatus=null;
                                    // HIGHLIGHT THE ALTERNATE ROW
                                    if(Math.IEEEremainder(i,2)==0) {
                            %>
                            <tr class="bgColor-Green">
                             <% } else { %>
                            <tr>
                            <% } %>
                                <td class="t-align-left ff">
                                    <a href="ViewEvalBidform.jsp?tenderId=<%=tenderId%>&uId=<%=uId%>&formId=<%=formdetails.getFieldName1()%>&lotId=<%=lotList.getFieldName3()%>&bidId=<%=formdetails.getFieldName3()%>&action=Edit&isSeek=true" target="_blank"><%=formdetails.getFieldName2()%></a>
                                </td>
                                <td class="t-align-right"><%=statusDisp%></td>
                                <td class="t-align-center ff">
                                    <%
                                        // VISIBLE BELOW LINK, IF RESPONSE TIME IS OVER
                                        //if (Integer.parseInt(dayExp) >= 0) {
                                    String strTSCPost="Post Comments";
                                    if("y".equals(request.getParameter("notify"))){
                                        strTSCPost="View Comments";
                                    }
                                    if("Comments Pending".equals(statusDisp) && "y".equals(request.getParameter("notify"))){
                                        out.print("-");
                                    }else{
                                    %>
                                    <a href="EvalTscComments.jsp?tenderId=<%=tenderId%>&uId=<%=uId%>&st=<%=st%>&frmId=<%=formdetails.getFieldName1()%>&pkgLotId=<%=lotList.getFieldName3()%>&bidId=<%=formdetails.getFieldName3()%>"><%=strTSCPost%></a>
<!--                                   |  <a href="../resources/common/evalClariDoc.jsp?tenderId=< %=tenderId%>&st=< %=st%>&formId=< %=formdetails.getFieldName1()%>&pId=office&uId=< %=uId%>">Upload Document</a>  |-->
                                    <% //} %>
<!--                                    | <a href="EvalViewQue.jsp?tenderId=<-%=tenderid%>&uid=<-%=uId%>">View Questions</a>-->
<!--                                    <a href="ViewQueAns.jsp?tenderId=< %=tenderId%>&st=< %=st%>&formId=< %=formdetails.getFieldName1()%>&uid=< %=uId%>"> Evaluate Tenderer</a>-->
                                    <%}%>
                                </td>
                            </tr>
                            <%
                                    i++;
                                 }  // END FOR LOOP OF FORM PacakageWise

                              if (i == 1) {
                             %>
                             <tr>
                                 <td colspan="3">
                                     <b>No forms found</b>
                                 </td>
                             </tr>
                             <% } %>
                        </table>
                        <%     }//gettenderlotbytenderid Ends here
                                    }//PackageWise Condition Ends
                        %>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

