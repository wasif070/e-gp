<%-- 
    Document   : ReportProcessing
    Created on : Jun 21, 2011, 3:42:16 PM
    Author     : nishit
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.EvaluationMatrix"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalRptSentToAa"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalReportDocs"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Report processing by Approving Authority / HOPA / AO</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <%
                    String s_tenderid = "0";
                    String s_lotId = "0";
                    String s_aaId = "0";
                    String s_rId = "0";
                    if (request.getParameter("tenderid") != null) {
                        s_tenderid = request.getParameter("tenderid");
                    }
                    if (request.getParameter("lotId") != null) {
                        s_lotId = request.getParameter("lotId");
                    }
                    if (request.getParameter("evalRptToAaid") != null) {
                        s_aaId = request.getParameter("evalRptToAaid");
                    }
                    if (request.getParameter("rId") != null) {
                        s_rId = request.getParameter("rId");
                    }
                    TblEvalRptSentToAa tblEvalRptSentToAa = new TblEvalRptSentToAa();
                    EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                    String evalType = "eval";
                    if (request.getParameter("stat") != null) {
                        evalType = request.getParameter("stat");
                    }
                    tblEvalRptSentToAa = evalService.getTblEvalRptSentToAa(Integer.parseInt(s_aaId), Integer.parseInt(s_rId));
                    CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> lstChkUserType2 = null;
                    boolean isAAMinister2 = false;
                    lstChkUserType2 = dataMore.geteGPData("getTenderAAMinister", s_tenderid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    if (lstChkUserType2 != null && !lstChkUserType2.isEmpty() && Integer.parseInt(lstChkUserType2.get(0).getFieldName1()) > 0) {
                        if(Integer.parseInt(lstChkUserType2.get(0).getFieldName1()) == tblEvalRptSentToAa.getUserId()){
                            isAAMinister2 = true;
                        }
                   }
                    /***Dohatec Start**/
                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> tenderLotList = tenderCommonService1.returndata("getProcurementNatureForEval", s_tenderid, null);
                    List<SPCommonSearchDataMore> isFinalize = null;
                    boolean isAllBidderDisqualified = false;
                    List<SPCommonSearchDataMore> getreportID = null;
                    String reportID = null;                              
                   boolean isAllBidderFail = false;
                   boolean isAllBidderFailAfterNoa = false;
                   boolean isAllBidderDisqualifiedlAfterNoa = false;
                   String strCmnProcurementNature = "";
                   String strCmnDocType = "";
                   List<SPTenderCommonData> lstCmnEnvelops = tenderCommonService1.returndata("getTenderEnvelopeCount", s_tenderid, "0");
                   if (!lstCmnEnvelops.isEmpty()) { strCmnProcurementNature = lstCmnEnvelops.get(0).getFieldName3();  }
                   if ("Services".equalsIgnoreCase(strCmnProcurementNature)) {
                       reportID="0";
                       for (SPTenderCommonData tenderLot : tenderLotList) {
                               getreportID = dataMore.geteGPData("getCRReportId", s_tenderid,"0","0");
                               if(getreportID!=null && (!getreportID.isEmpty())){
                                        reportID = getreportID.get(0).getFieldName1();
                                    }
                               }
                       strCmnDocType = "Proposal";
                       isFinalize = dataMore.geteGPDataMore("getResponsiveBidderCountService", s_tenderid);
                        if(isFinalize!=null && (!isFinalize.isEmpty()) && isFinalize.get(0).getFieldName1().equals("0")){ isAllBidderFail = true;}
                        isFinalize = null;
                        isFinalize = dataMore.geteGPDataMore("AllBidderDisqualifiedService", s_tenderid,reportID.toString());
                        if(isFinalize!=null && (!isFinalize.isEmpty()) && isFinalize.get(0).getFieldName1().equals("1")){ isAllBidderDisqualified = true;    }
                   }
                   else {   
                            for (SPTenderCommonData tenderLot : tenderLotList) {
                               getreportID = dataMore.geteGPData("getCRReportId", s_tenderid,tenderLot.getFieldName3(),"0");
                               if(getreportID!=null && (!getreportID.isEmpty())){
                                        reportID = getreportID.get(0).getFieldName1();
                                    }
                               }
                            strCmnDocType = "Tender";
                            isFinalize = dataMore.geteGPDataMore("getResponsiveBidderCount", s_tenderid);
                            if(isFinalize!=null && (!isFinalize.isEmpty()) && isFinalize.get(0).getFieldName1().equals("0")){
                                isAllBidderFail = true; }
                            isFinalize = dataMore.geteGPDataMore("getResponsiveBidderCountAfterNOA", s_tenderid);
                    if(isFinalize!=null && (!isFinalize.isEmpty()) && isFinalize.get(0).getFieldName2().equals("0")){
                        isAllBidderFailAfterNoa = true;
                    }
                if(!isAllBidderFail){
                        isFinalize = null;
                        isFinalize = dataMore.geteGPData("AllBidderDisqualified", s_tenderid,reportID.toString());
                        if(isFinalize!=null && (!isFinalize.isEmpty()) && isFinalize.get(0).getFieldName1().equals("1")){ isAllBidderDisqualified = true;    }
                    }
                 if(!isAllBidderFail){
                        isFinalize = null;
                        isFinalize = dataMore.geteGPData("AllBidderDisqualifiedAfterNOA", s_tenderid,reportID.toString());
                        if(isFinalize!=null && (!isFinalize.isEmpty()) && isFinalize.get(0).getFieldName1().equals("1")){ isAllBidderDisqualifiedlAfterNoa = true;    }
                    }
                   }                    
                        /***Dohatec End**/
        %>
        <script type="text/javascript">

            function validate(){
                $(".err").remove();
                var valid = true;
                if($("#cmbStatus").val() == "") {
                    $("#cmbStatus").parent().append("<div class='err' style='color:red;'>Please select Status</div>");
                    valid = false;
                }
                /*if($.trim($("#txtRemark").val()) == "") {
                            $("#txtRemark").parent().append("<div class='err' style='color:red;'>Please enter Comments / Query</div>");
                            valid = false;
                        }
                        if($.trim($("#txtRemark").val()).length > 2000) {
                            $("#txtRemark").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed</div>");
                            valid = false;
                        }*/
                if($("#cmbStatus").val()=='Rejected'){
                    if(!$("#reTendering").attr('checked') && !$("#reTenderingNo").attr('checked')) {
                        $("#reTendering").parent().append("<div class='err' style='color:red;'>Please select Re - Tendering</div>");
                        valid=false;
                    }
                }
                if(CKEDITOR.instances.txtRemark.getData() == 0){
                    $("#txtRemark").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                    valid=false;
                }
                if(CKEDITOR.instances.txtRemark.getData().length > 2000)
                {
                    $("#txtRemark").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed</div>");
                    valid = false;
                }
                if(!valid){
                    return false;
                } else {
                    document.getElementById("submit").style.display = 'none';
                }
            }
        function getDocData(){
                $.post("<%=request.getContextPath()%>/EvalRptDocsServlet", {tenderid:<%=s_tenderid%>,lotId:<%=s_lotId%>,rId:<%=s_rId%>,funName:'docReviewTable'}, function(j){
                    document.getElementById("queryData").innerHTML = j;
                });
            }
            function deleteFile(docId,docName,tenderId,lotId,rId){
                // alert(docId+docName+tenderId+lotId+rId);
        $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
                    if(r == true){
                        $.post("<%=request.getContextPath()%>/EvalRptDocsServlet", {docName:docName,docId:docId,tenderid:tenderId,lotId:lotId,rId:rId,funName:'docDelete'}, function(j){
                            jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                                getDocData();
                            });
                        });
                    }else{
                        getDocData();
                    }
                });
            }
        </script>
        <script type="text/javascript">
            function pageRedirect(){
                if($("#cmbStatus").val()=='seek Clarification'){
                    $("#trRemark").hide()
                    $("#trSubmit").hide();
                    $("#trHref").show();
                    $("#reTenderTr").hide()
                }else{
                    if($("#cmbStatus").val()=='Rejected'){
                        $("#reTenderTr").show()
                    }else{
                        $("#reTenderTr").hide()
                    }
                    $("#trRemark").show()
                    $("#trSubmit").show();
                    $("#trHref").hide();
                }
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
            <div class="DashboardContainer">
                <div class="contentArea_1">
                    <div class="pageHead_1">Process Evaluation Report <span class="c-alignment-right"><a href="EvalReportListing.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>" title="Go Back" class="action-button-goback">Go Back</a></span></div>
                    <%

                                pageContext.setAttribute("tenderId", s_tenderid);
                    %>
                    <div class="t_space">
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    </div>
                    <form id="frmReprotProcessing" action="<%=request.getContextPath()%>/EvaluationServlet" name="frmReprotProcessing"  method="POST">
                        <input type="hidden" value="updateAppRpt" name="action" />
                        <input type="hidden" name="tenderid" value="<%=s_tenderid%>" />
                        <input type="hidden" name="lotId" value="<%=s_lotId%>" />
                        <input type="hidden" name="role" value="<%=tblEvalRptSentToAa.getSentRole()%>" />
                        <input type="hidden" name="sUserId" value="<%=tblEvalRptSentToAa.getSentBy()%>" />
                        <input type="hidden" name="rId" value="<%=s_rId%>" />
                        <table width="100%" cellspacing="10" class="tableList_3 t_space">
                            <td class="ff" width="12%"><%=strCmnDocType%> Opening Reports :</td>
                            <td width="88%"><a href="OpeningReports.jsp?tenderId=<%=s_tenderid%>&lotId=0&comType=null" target="_blank" >View</a></td>
                            <jsp:include page="TERInclude.jsp">
                                <jsp:param name="tenderId" value="<%=s_tenderid%>"/>
                                <jsp:param name="lotId" value="<%=s_lotId%>"/>
                                <jsp:param name="roundId" value="<%=s_rId%>"/>
                                <jsp:param name="evalType" value="<%=evalType%>"/>
                            </jsp:include>
                            <tr>
                                <td class="ff">Comments of  <% if(isAAMinister2 == false) { %>Chairperson<%} else{%>Secretary<%}%> :</td>
                                <td><%=tblEvalRptSentToAa.getRemarks()%></td>
                            </tr>
                        </table>
                        <div style="font-style: italic" class="formStyle_1 t_space b_space ff">Fields marked with (<span class="mandatory">*</span>) are mandatory</div>
                        <table width="100%" cellspacing="10" class="tableView_1 t_space">
                            <tr>
                                <td class="ff" width="10%">Action :  &nbsp;<span class="mandatory">*</span></td>
                                <td width="90%"><select name="cmbStatus" class="formTxtBox_1" id="cmbStatus" onchange="pageRedirect();">
                                        <option selected="selected" value="">- Select -</option>
                                        <%if(!isAllBidderDisqualified && !isAllBidderFail && !isAllBidderFailAfterNoa && !isAllBidderDisqualifiedlAfterNoa){%>
                                        <option value="Approved">Approve</option>                                        
                                        <option value="seek Clarification">Seek Clarification</option>                                        
                                        <%} else if (!isAllBidderDisqualifiedlAfterNoa){%>
                                        <option value="Re-evaluation">Re-evaluation</option>
                                        <%} %>
                                        <option value="Rejected">Reject</option>
                                    </select></td>
                            </tr>
                            <tr id="reTenderTr" style="display: none;">
                                <td class="ff">Re - Tendering :  &nbsp;<span class="mandatory">*</span></td>
                                <td><input name="reTendering" type="radio" value="yes" id="reTendering"/> Yes &nbsp;&nbsp;&nbsp;&nbsp;
                                    <input name="reTendering" type="radio" value="No" id="reTenderingNo" /> No</td>
                            </tr>
                            <tr id="trRemark">
                                <td valign="top" class="ff">Comments : &nbsp;<span class="mandatory">*</span></td>
                                <td>
                                    <textarea cols="100" name="txtRemark" rows="5" id="txtRemark" class="formTxtBox_1"></textarea>
                                    <script type="text/javascript">
                                        CKEDITOR.replace( 'txtRemark',
                                        {
                                            toolbar : "egpToolbar"

                                        });
                                    </script>
                                </td>
                            </tr>
                            <%
                                        if (isAAMinister2 == true) {
                            %>
                            <tr>
                                <td class="ff">Reference Document :</td>
                                <td>
                                    <a href="javascript:void(0)" onclick="window.open('EvalRptDocUpload.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>&rId=<%=s_rId%>','window','resizable=yes,scrollbars=1');">Upload</a>
                                    <%--<a href="EvalRptDocUpload.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>&rId=<%=s_rId%>">Upload</a>--%>
                                </td>
                            </tr>
                            <% }%>
                            <tr id="trSubmit">
                                <td>&nbsp;</td>
                                <td class="t-align-left">
                                    <input type="hidden" name="rptId" value="<%=tblEvalRptSentToAa.getEvalRptToAaid()%>" />
                                    <label class="formBtn_1 l_space "><input type="submit" name="submit" value="Submit" onclick="return validate();"/></label>
                                </td>
                            </tr>
                            <tr id="trHref" style="display: none;">
                                <td>&nbsp;</td>
                                <td>
                                    <label class="anchorLink" ><a style="text-decoration: none;color:#ffffff;" href="SeekClarificationAA.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>&evalRptToAaid=<%=tblEvalRptSentToAa.getEvalRptToAaid()%>&rId=<%=s_rId%>">Submit</a></label>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <div id="queryData"></div>
                    </table>
                </div>
            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabEval");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        getDocData();

    </script>
</html>
<%
            if (tblEvalRptSentToAa != null) {
                tblEvalRptSentToAa = null;
            }
%>
