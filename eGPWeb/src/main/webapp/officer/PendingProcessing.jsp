<%-- 
    Document   : PendingProcessing
    Created on : Nov 11, 2010, 12:58:41 PM
    Author     : test
--%>

<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
       <%
            String viewType = request.getParameter("viewtype");
            String msg = "";
            msg = request.getParameter("msg");
            
            String colName = null;
            String colHeader = null;
            String caption = null;
            String sortColumn = null;
            String widths = "";
            String aling = "";
            
            String moduleName = "";
            String processName = "";
            String workflowId = "0";
            String processesBy = "";
            String processedStDt = "";
            String processedEndDt = "";
            
            if("Search".equalsIgnoreCase(request.getParameter("btnSearchwf")))
            {
                if(request.getParameter("module")!=null){
                    moduleName = request.getParameter("module");
                }
                if(request.getParameter("process")!=null){
                    processName = request.getParameter("process");
                }
                if(request.getParameter("workflowId")!=null){
                    workflowId = request.getParameter("workflowId");
                }
                if(request.getParameter("processesBy")!=null){
                    processesBy = request.getParameter("processesBy");
                }
                if(request.getParameter("dtStartDate")!=null){
                    processedStDt = request.getParameter("dtStartDate");
                }
                if(request.getParameter("dtEndDate")!=null){
                    processedEndDt = request.getParameter("dtEndDate");
                }
            }
            
            if(viewType.equalsIgnoreCase("pending"))
            {
                colHeader= "Sl. <br/> No.,Module Name,Process Name,ID,Processed By,Processed Date and Time,Previous Action,To be Processed By,Action";
                colName = "S.No,moduleName,eventName,objectId,fileSentFrom,processDate,action,fileSentTo,process";
                caption = " ";
                sortColumn = "false,true,true,true,true,true,true,true,false";
                widths = "5,15,20,10,20,15,10,15,10";
                aling = "center,left,left,center,left,left,center,left,center";
            }
            else if(viewType.equalsIgnoreCase("defaultworkflow"))
            {
                colHeader= "Sl. <br/> No.,ModuleName,Process Name,ID,Action";
                colName = "S.No,moduleName,eventName,objectId,View";
                caption = " ";
                sortColumn = "false,true,true,true,false";
                widths = "5,30,30,20,10";
                aling = "center,left,left,center,center";
            }
            else
            {
                colHeader= "Sl. <br/> No.,Module Name,Process Name,ID,Processed By,Processed Date and Time,Action,To be Processed By,Action";
                colName = "S.No,moduleName,eventName,objectId,fileSentFrom,processDate,action,fileSentTo,History";
                caption = " ";
                sortColumn = "false,true,true,true,true,true,true,true,false";
                widths = "5,15,10,10,20,10,10,20,5";
                aling = "center,left,left,center,left,left,center,left,center";
            }

         %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
             <% if(viewType.equalsIgnoreCase("pending")){  %>
                    Workflow: Pending tasks
             <% }else if(viewType.equalsIgnoreCase("defaultworkflow")){ %>
                    Approved Workflow
             <% }else{ %>
                    Workflow: Processed tasks
             <% } %>
        </title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script src="../resources/js/jQuery/jquery.generatepdf.js"  type="text/javascript"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            
            function validate(){
                var moduleName = document.getElementById("module").value;
                var processName = document.getElementById("process").value;
                var moduleId = document.getElementById("workflowId").value;
                var processFrom = document.getElementById("dtStartDate").value;
                var processTo = document.getElementById("dtEndDate").value;
                
                if(moduleName=="" && processName=="" && moduleId=="" && processFrom=="" && processTo==""){
                    document.getElementById("error").innerHTML = "Please enter any search criteria";
                    return false;
                }
                return true;
            }

            function resetMe(){
                document.getElementById('frmsubmit').action=window.location;
                //document.getElementById('frmsubmit').method = 'get';
                document.getElementById('frmsubmit').submit();
            }
        </script>
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
    </head>
    <body onload="hide();">
        <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
        </div>
            <div class="contentArea_1">
                <div class="pageHead_1">
                <% if(viewType.equalsIgnoreCase("pending")){  %>
                 Workflow: Pending tasks
                <% }else if(viewType.equalsIgnoreCase("defaultworkflow")){ %>
                 Approved Workflow 
                 <% }else{ %>
                 Workflow: Processed tasks
                 <div class="t_space">
                     <%  if(msg != null && msg.equalsIgnoreCase("alreadyprocess")) {%>
                     <div class="responseMsg successMsg">File already processed in a workflow</div>
                     <% }else if (msg != null && !msg.contains("not")){%>
                    <div class="responseMsg successMsg">File processed successfully</div>
                    <%  }else if (msg != null && msg.contains("not")) {%>
                    <div class="responseMsg errorMsg">File has not processed successfully</div>
                    <%  } %>
                 </div>
                 <% } %>
                 <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="
                     <% if("defaultworkflow".equalsIgnoreCase(viewType)){ %>
                      exportToPDF('4');    
                     <%}else{%>
                     exportToPDF('8');
                     <%}%>
                    ">Save as PDF</a></span>
                 </div>
        
                 <div style="width: 99%;" class="tabPanelArea_2 t_space">
                    <form id="frmworkflow" name="frmMyApp" method="post">
                    <div class="formBg_1 t_space">
                        <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                        <table cellspacing="10" class="formStyle_1" width="100%" id="tblSearchBox">
                            <tr>
                                <td width="20%" class="ff">Module Name :</td>
                                <td width="30%">
                                    <input type="text" name="module" id="module" value="<%=moduleName%>" class="formTxtBox_1" /> 
                                </td>
                                <td width="20%" class="ff">Process Name : </td>
                                <td width="30%">
                                    <input type="text" name="process" id="process" value="<%=processName%>" class="formTxtBox_1" />
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="ff" width="20%">ID : </td>
                                <td width="30%%">
                                    <input type="text" name="workflowId" id="workflowId" value="<% if(!"0".equals(workflowId)){ out.print(workflowId); } %>" class="formTxtBox_1" />
                                </td>
                                <% if(!"defaultworkflow".equalsIgnoreCase(viewType)){ %>
                                <td class="ff" width="20%" >
                                    <% if("pending".equalsIgnoreCase(viewType)){ %>
                                        Processed By : 
                                    <% }else{ %>
                                        To be Processed By :
                                    <% } %>
                                </td>
                                <td width="30%">
                                    <input type="text" name="processesBy" id="processesBy" value="<%=processesBy%>" class="formTxtBox_1" />
                                </td>
                                <% }else{ %>
                                <td class="ff" width="20%">&nbsp;</td>
                                <td width="30%">&nbsp;</td>
                                <% } %>
                            </tr>
                            
                            <tr>
                                <td width="20%" class="ff">From Processed Date and Time : </td>
                                <td width="30%">
                                    <input name="dtStartDate" type="text" class="formTxtBox_1" id="dtStartDate" readonly="true" onfocus="GetCal('dtStartDate','dtStartDate');" value="<%=processedStDt%>" />
                                    <img id="dtStartDate1" name="dtStartDate1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('dtStartDate','dtStartDate1');"/>&nbsp;&nbsp;
                                </td>
                                <td width="20%" class="ff">To Processed Date and Time</td>
                                <td width="30%">
                                    <input name="dtEndDate" type="text" class="formTxtBox_1" id="dtEndDate" readonly="true"  onfocus="GetCal('dtEndDate','dtEndDate');" value="<%=processedEndDt%>"/>
                                    <img id="dtEndDate1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('dtEndDate','dtEndDate1');"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="t-align-center"><span id="error" style="color: red;"></span></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <label class="formBtn_1">
                                        <input type="submit" name="btnSearchwf" id="btnSearchwf" value="Search" onclick="return validate();" />
                                    </label>
                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input type="button" name="Reset" id="button" value="Reset" onclick="resetMe()"/>
                                    </label>
                                </td>
                            </tr>
                        </table></div>
                </form>
                                    <form name="frmsubmit" id="frmsubmit"  action="<%=request.getContextPath()%>/officer/PendingProcessing.jsp" method="post">
                    <input type="hidden" name="viewtype" id="viewtype" value="<%=viewType%>" />
                </form>
            <div>&nbsp;</div>
        <div align="center">
            <jsp:include page="../resources/common/GridCommon.jsp" >
                <jsp:param name="caption" value="<%=caption%>" />
                <jsp:param name="url" value='<%=request.getContextPath()+"/PendingProcessedServlet?viewtype="+viewType+"&module="+moduleName+"&process="+processName+"&workflowId="+workflowId+"&processesBy="+processesBy+"&dtStartDate="+processedStDt+"&dtEndDate="+processedEndDt%>' />
                <jsp:param name="colHeader" value='<%=colHeader%>' />
                <jsp:param name="colName" value='<%=colName%>' />
                <jsp:param name="sortColumn" value="<%=sortColumn%>" />
                <jsp:param name="width" value="<%=widths%>" />
                <jsp:param name="aling" value="<%=aling%>" />
                <jsp:param name="searchOpt" value="false" />
            </jsp:include>
        </div>
                
   </div>
            </div>
            <form id="formstyle" action="" method="post" name="formstyle">

               <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
               <%
                 SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                 String appenddate = dateFormat1.format(new Date());
               %>
               <input type="hidden" name="fileName" id="fileName" value="Workflow_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="Workflow" />
            </form>
        <div>&nbsp;</div>
       <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabWorkFlow");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        
        function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Advanced Search';
            }else{
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }
        }
        function hide()
        {
            document.getElementById('tblSearchBox').style.display = 'none';
            document.getElementById('collExp').innerHTML = '+ Advanced Search';
        }

    </script>
</html>
