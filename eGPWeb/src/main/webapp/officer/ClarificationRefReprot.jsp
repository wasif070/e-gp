<%--
    Document   : ClarificationRefReprot
    Created on : Dec 1, 2010, 2:58:34 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation Reference Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <jsp:useBean id="clariRptSrBean" class="com.cptu.egp.eps.web.servicebean.ClariRptSrBean" id="clariRptSrBean" scope="request" />

<%
     String tenderId = "0";
                    if(request.getParameter("tenderId")!=null){
                            tenderId = request.getParameter("tenderId");
                        }
     int  tid = Integer.parseInt(tenderId);
                                String isUpdate  = clariRptSrBean.forUpdate(tid);

%>
<script type="text/javascript">

            $(document).ready(function() {
                $("#clariRefReprot").validate({
                    rules: {

                        reportName: {required: true,maxlength:100}
                    },
                    messages: {

                        reportName: { required: "<div class='reqF_1'>Please enter Report Name</div>",
                                        maxlength: "<div class='reqF_1'>Max 100 characters are allowed</div>"}
                    }
                });
            });
</script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->

 <%@include file="../resources/common/AfterLoginTop.jsp" %>
 <div class="contentArea_1">
<div class="pageHead_1">Evaluation Reference Report<span style="float:right;"><a href="EvalComm.jsp?tenderid=<%=tenderId %>" class="action-button-goback">Go Back to Dashboard</a></span></div>
 <%-- <div class="t_space" align="right"><a href="Notice.jsp?tenderid=<%=postQualId%>" class="action-button-goback">Go Back Dashboard</a></div>--%>
 <%pageContext.setAttribute("tenderId", tenderId);%>
 <%@include file="../resources/common/TenderInfoBar.jsp" %>
           <form  id="clariRefReprot" method="post" action="<%=request.getContextPath()%>/ClarificationRefDocServlet?&funName=report&tenderId=<%=tenderId%>"  name="frmUploadDoc">


                <div class="t_space">
               <%
                                if (request.getParameter("fq") != null) {
                    %>

                    <div class="responseMsg errorMsg t_space"><%=request.getParameter("fq")%></div>
                    <%
                                }
                    %>
                   </div>
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">

                    <tr>
                        <td width="10%" class="ff">Report Name : <span>*</span></td>
                        <td width="90%">
                            <input type="hidden" name="action" value="<%=isUpdate%>" />
                            <%if(isUpdate.equalsIgnoreCase("update")){
                                    clariRptSrBean.populateInfo(tid);
                            %>
                            <input name="reportName" type="text" class="formTxtBox_1" maxlength="101" id="reportName" style="width:200px;" value="<%=clariRptSrBean.getEvalRptName()%>"/>
                            <input type="hidden" name="rptId" value="<%=clariRptSrBean.getEvalRptId()%>" />
                            <%}else{%>
                            <input name="reportName" type="text" class="formTxtBox_1" maxlength="101" id="reportName" style="width:200px;" />
                            <%}%>
                            <div id="dvReportErMsg" class='reqF_1'></div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <%if(isUpdate.equalsIgnoreCase("create")){%>
                            <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Submit" onclick="return checkfile()" /></label>
                            <%}else{%>
                            <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Update" /></label>&nbsp;&nbsp;
                            <a href="ClarificationRefDoc.jsp?evalRptId=<%=clariRptSrBean.getEvalRptId()%>&tenderId=<%=tid%>" class="anchorLink" style="text-decoration: none; color:#FFF; height: 22px;">Next</a>
                            <%}%>
                        </td>
                    </tr>
                </table>

            </form>


 </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
           <%-- <table width="100%" cellspacing="0" class="footerCss">
                <tr>
                    <td align="left">e-GP &copy; All Rights Reserved
                        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
                    <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a>


                    </td>
                </tr>
            </table>--%>
           <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->


        </div>
    </body>
<script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>

