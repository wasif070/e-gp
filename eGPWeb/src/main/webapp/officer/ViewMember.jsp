<%-- 
    Document   : ViewMember
    Created on : Dec 19, 2010, 4:13:42 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Committee Members</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <div class="pageHead_1">View Committee Members</div>
    <body>
        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tb2">
            <tr>
                <th width="4%" class="t-align-left">Sl . No. <br /></th>
                <th width="25%" class="t-align-left">Committee role</th>
                <th width="46%" class="t-align-left">Committee member</th>
                <th width="25%" class="t-align-left">Members From</th>
            </tr>
            <%
                        CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");
                        java.util.List<Object[]> objects = committeMemberService.findCommMemberById(request.getParameter("commId"));
                        int i = 1;
                        for (Object[] os:objects) {
            %>
            <tr>
                <td class="t-align-center"><%=i%></td>
                <td class="t-align-left"><% if (os[0].equals("cp")) {
                                            out.print("Chairperson");
                                        }else if (os[0].equals("ms")) {
                                            out.print("Member Secretary");
                                        }else if (os[0].equals("m")) {
                                            out.print("Member");
                                        }%></td>
                <td class="t-align-left"><%=os[1]%></td>
                <td class="t-align-left"><%=os[2]%></td>
            </tr>
            <%i++;
           }%>
        </table>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
