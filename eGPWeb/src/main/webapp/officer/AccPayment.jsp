<%--
    Document   : AccPayment
    Created on : Aug 4, 2011, 10:44:30 AM
    Author     : dixit
--%>

<%@page import="java.util.Collections"%>
<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceMaster"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Accounts Officer Payments Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
    </head>
    <%
        ResourceBundle bdl = null;
        bdl = ResourceBundle.getBundle("properties.cmsproperty");
        ConsolodateService cs = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");
        TenderTablesSrBean beanCommon1 = new TenderTablesSrBean();
        String tenderType1 = beanCommon1.getTenderType(Integer.parseInt(request.getParameter("tenderId")));
        List<Object> objInvoiceId = null;
        String invoiceId = "";
        String invoiceNo1 = "";
        

        String wpId = "";
        if(request.getParameter("wpId")!=null)
        {
            wpId = request.getParameter("wpId");
            pageContext.setAttribute("wpId", request.getParameter("wpId"));
        }
        if(!tenderType1.equals("ICT"))
        {
            invoiceId = request.getParameter("invoiceId");
            pageContext.setAttribute("invoiceId", request.getParameter("invoiceId"));
        }
        else
        {
            pageContext.setAttribute("invoiceNo", request.getParameter("invoiceNo"));
            objInvoiceId = cs.getInvoiceIdForICT(request.getParameter("invoiceNo"), Integer.parseInt(wpId));
            invoiceId = objInvoiceId.get(0).toString();
            pageContext.setAttribute("invoiceId", objInvoiceId.get(0).toString());
            invoiceNo1 = request.getParameter("invoiceNo");
        }

       int totalCur = 0;
       int totalCurForGross = 0;
    /*   String[] curArr = new String[4];
       String[] totalArr = new String[4];
       String[] totalInvoiceId = new String[4];
       String[] cntrAmnt = new String[4];*/
    %>
    <script type="text/javascript">
        function GetCal(txtname,controlname){
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: false,
                dateFormat:"%d/%m/%Y",
                onSelect: function() {
                    var date = Calendar.intToDate(this.selection.get());
                    LEFT_CAL.args.min = date;
                    LEFT_CAL.redraw();
                    this.hide();
                }
            });

            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })
        }
        var retension = "";
        function RetensionPlusEvalute()
        {
            retension = "+";
            document.getElementById("Plusid").style.backgroundColor="#387C44";
            document.getElementById("Minusid").style.backgroundColor="#463E3F";

            document.getElementById("hiddenRetensionactionid").value = "+";
            Evalute();
        }
        function RetensionMinusEvalute()
        {
            retension = "-";
            document.getElementById("Minusid").style.backgroundColor="#387C44";
            document.getElementById("Plusid").style.backgroundColor="#463E3F";

            document.getElementById("hiddenRetensionactionid").value = "-";
            Evalute();
        }
        function Evalute()
        {
           var flag = true;
          var a =  document.getElementById("totalCurrency").value;
         //  var a =document.getElementsByName("amtWords");
           var curIndex = 999;
           for(var i = 0;i<a;i++){
               if(document.getElementById("tenderType").value == "ICT"){
                if(document.getElementById("currency"+i).value == "BTN")
                   curIndex = i;
               }
        if(document.getElementById("invoiceAmtid"+i).value!="")
           {
              if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("invoiceAmtid"+i).value))
              {
                   document.getElementById("MainAmtspan").innerHTML = "";
                   document.getElementById("GrossAmtid"+i).value = document.getElementById("invoiceAmtid"+i).value;
                   document.getElementById("totalDeductedAmt"+i).innerHTML ="0.000";
                   document.getElementById("totalAddedAmt"+i).innerHTML ="0.000";
//                   if(document.getElementById("AdvAmtid").value!="")
//                   {
//                       if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAmtid").value))
//                       {
//                            document.getElementById("AdvAmtspan").innerHTML = "";
//                            var num = ((document.getElementById("AdvAmtid").value*(document.getElementById("Contarctvalue").value))/(100));
//                            document.getElementById("AdvAmtPercentage").innerHTML = num.toFixed(3) + " <br /><font color=blue>(of Contract Value)</font>";
//                       }else
//                       {
//                           flag = false;
//                           document.getElementById("AdvAmtspan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
//                       }
//                   }
                   if(document.getElementById("AdvAdjuAmtid").value!="")
                   {
                     if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                     
                     {
                       if(/^\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                       {
                         if(validateDecimal(document.getElementById("AdvAdjuAmtid").value))
                         {
                           if(document.getElementById("AdvAmtpercentage"+i).innerHTML>=0)
                           {
                              if(document.getElementById("AdvAdjuAmtid").value<=100)
                              {
                                   var num = ((document.getElementById("AdvAdjuAmtid").value*(document.getElementById("AdvAmtpercentage"+i).innerHTML))/(100));
                                   document.getElementById("AdvAdjAmtPercentage"+i).innerHTML = num.toFixed(3);
                                  // document.getElementById("AdvAdjAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                   if(eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)<eval(document.getElementById("invoiceAmtid"+i).value))
                                   {
                                       if(eval(document.getElementById("AdvAmtpercentage"+i).innerHTML)-eval(document.getElementById("tAATDpercentage"+i).innerHTML)>=document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)
                                       {
                                           document.getElementById("AdvAdjAmtspan"+i).innerHTML = "";
                                           var MainAmtMinusAdvAdjAmt = eval(document.getElementById("GrossAmtid"+i).value)-eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML);
                                           document.getElementById("GrossAmtid"+i).value = MainAmtMinusAdvAdjAmt.toFixed(3);
                                           //document.getElementById("GrossinWords"+i).innerHTML = DoIt(document.getElementById("GrossAmtid"+i).value);
                                           //Numeric to word currency conversion by Emtaz on 20/April/2016
                                           document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                           document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("totalDeductedAmt"+i).innerHTML)+eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)).toFixed(3);
                                       }
                                       else
                                       {
                                           flag = false;
                                           document.getElementById("AdvAdjAmtspan"+i).innerHTML = "Advance Adjustment Amount should not exceeded Advance Amount";
                                       }
                                   }else{
                                       flag = false;
                                       document.getElementById("AdvAdjAmtspan"+i).innerHTML = "Advance Adjustment Amount can not be more than Invoice Amount";
                                   }
                              }
                              else
                              {
                                 flag = false;
                                 document.getElementById("AdvAdjAmtspan"+i).innerHTML = "Advance Adjustment Amount can not be more than 100%";
                              }
                            }
                            else
                            {
                               flag = false;
                               document.getElementById("AdvAdjAmtPercentage"+i).innerHTML = '0.000';
                              // document.getElementById("AdvAdjAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                               document.getElementById("AdvAdjAmtspan"+i).innerHTML = "Advance Adjustment Amount can not be done as Advance Amount is 0 %";
                            }
                         }
                         else
                         {
                               flag = false;
                               document.getElementById("AdvAdjAmtspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                         }
                       }
                       else
                       {
                           flag = false;
                            document.getElementById("AdvAdjAmtspan"+i).innerHTML = "special characters are not allowed";
                       }
                     }
                     else
                     {
                          flag = false;
                          document.getElementById("AdvAdjAmtspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                     }
                   }

                    if(document.getElementById("SalvageAdjuAmtId") !=null && document.getElementById("SalvageAdjuAmtId").value!="")
                   {
                     if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("SalvageAdjuAmtId").value))

                     {
                       if(/^\d*\.?\d*$/.test(document.getElementById("SalvageAdjuAmtId").value))
                       {
                         if(validateDecimal(document.getElementById("SalvageAdjuAmtId").value))
                         {
                           if(document.getElementById("AdvAmtpercentage"+i).innerHTML>=0)
                           {
                              if(document.getElementById("SalvageAdjuAmtId").value<=100)
                              {
                                   var num = ((document.getElementById("SalvageAdjuAmtId").value*(document.getElementById("SalvageAmount").value))/(100));
                                   document.getElementById("SalvageAdjAmtPercentage"+i).innerHTML = num.toFixed(3);
                                  // document.getElementById("AdvAdjAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                   if(eval(document.getElementById("SalvageAdjAmtPercentage"+i).innerHTML)<eval(document.getElementById("invoiceAmtid"+i).value))
                                   {
                                       if(eval(document.getElementById("SalvageAdjAmtPercentage"+i).innerHTML)+eval(document.getElementById("SalvageAdjpercentage"+i).innerHTML)<=document.getElementById("SalvageAmount").value)
                                       {
                                           document.getElementById("SalvageAdjAmtspan"+i).innerHTML = "";
                                           var MainAmtMinusSalvageAdjAmt = eval(document.getElementById("GrossAmtid"+i).value)-eval(document.getElementById("SalvageAdjAmtPercentage"+i).innerHTML);
                                           document.getElementById("GrossAmtid"+i).value = MainAmtMinusSalvageAdjAmt.toFixed(3);
                                           document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                           document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("totalDeductedAmt"+i).innerHTML)+eval(document.getElementById("SalvageAdjAmtPercentage"+i).innerHTML)).toFixed(3);
                                       }
                                       else
                                       {
                                           flag = false;
                                           document.getElementById("SalvageAdjAmtspan"+i).innerHTML = "salvage Adjustment Amount should not exceeded total salvage Amount";
                                       }
                                   }else{
                                       flag = false;
                                       document.getElementById("SalvageAdjAmtspan"+i).innerHTML = "Salvage Adjustment Amount can not be more than Invoice Amount";
                                   }
                              }
                              else
                              {
                                 flag = false;
                                 document.getElementById("SalvageAdjAmtspan"+i).innerHTML = "Salvage Adjustment Amount can not be more than 100%";
                              }
                            }
                            else
                            {
                               flag = false;
                               document.getElementById("SalvageAdjAmtPercentage"+i).innerHTML = '0.000';
                              // document.getElementById("AdvAdjAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                               document.getElementById("SalvageAdjAmtspan"+i).innerHTML = "Salvage Adjustment Amount can not be done as Salvage Amount is 0 %";
                            }
                         }
                         else
                         {
                               flag = false;
                               document.getElementById("SalvageAdjAmtspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                         }
                       }
                       else
                       {
                           flag = false;
                            document.getElementById("SalvageAdjAmtspan"+i).innerHTML = "special characters are not allowed";
                       }
                     }
                     else
                     {
                          flag = false;
                          document.getElementById("SalvageAdjAmtspan"+i).innerHTML = "please enter numeric value";
                     }
                   }


                   if((document.getElementById("Vatid").value!="") &&  ($("#Vatid").attr('readonly') != true))
                   {
                       if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("Vatid").value))
                       {
                           if(/^\d*\.?\d*$/.test(document.getElementById("Vatid").value))
                           {
                                if(validateDecimal(document.getElementById("Vatid").value))
                                {
                                  if(document.getElementById("Vatid").value<=100)
                                  {
                                    document.getElementById("vatspan").innerHTML = "";
                                    var num = ((document.getElementById("Vatid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                    document.getElementById("vatPercentage").innerHTML = num.toFixed(3);
                                    document.getElementById("vatinbdt").innerHTML = '&nbsp&nbsp'+'<%=bdl.getString("CMS.Inv.InBDT")%>';

                                    document.getElementById("vatplusaitid"+i).innerHTML = eval(document.getElementById("vatPercentage").innerHTML).toFixed(3);
                                    document.getElementById("FinalVatandAitid"+i).innerHTML = eval(document.getElementById("vatPercentage").innerHTML).toFixed(3);
                                    document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("totalDeductedAmt"+i).innerHTML)+eval(document.getElementById("vatPercentage").innerHTML)).toFixed(3);
                                    var GrossMinusvat = eval(document.getElementById("GrossAmtid"+i).value)-eval(document.getElementById("vatPercentage").innerHTML);
                                    document.getElementById("GrossAmtid"+i).value = GrossMinusvat.toFixed(3);
                                    document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                  }
                                  else
                                  {
                                     flag = false;
                                     document.getElementById("vatspan").innerHTML = "VAT can not be more than 100%";
                                  }
                                }
                                else
                                {
                                      flag = false;
                                      document.getElementById("vatspan").innerHTML = "only 3 digits after decimal are allowed";
                                }
                           }
                           else
                           {
                                flag = false;
                                document.getElementById("vatspan").innerHTML = "special characters are not allowed";
                           }
                       }
                       else
                       {
                           flag = false;
                           document.getElementById("vatspan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                       }
                   }
                 //  else
                 //  {
                     //   document.getElementById("totalDeductedAmt"+curIndex).innerHTML = (eval(document.getElementById("totalDeductedAmt"+curIndex).innerHTML)+eval(document.getElementById("vatPercentage").innerHTML)).toFixed(3);
                 //  }
                   if(document.getElementById("Aitid").value!="")
                   {
                   //for(var i = 0;i<a.length;i++)
                    //  {
                        if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("Aitid").value))
                        {
                            if(/^\d*\.?\d*$/.test(document.getElementById("Aitid").value))
                            {
                                if(validateDecimal(document.getElementById("Aitid").value))
                                {
                                  if(document.getElementById("Aitid").value<=100)
                                  {
                                    document.getElementById("aitspan"+i).innerHTML = "";
                                     var vatplusait = 0;
                                    if(document.getElementById("Vatid").value!="")
                                    {

                                        if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("Vatid").value))
                                        {
                                            if(i==curIndex)
                                                  var num = ((document.getElementById("Aitid").value*(document.getElementById("invoiceAmtid"+i).value-document.getElementById("Vatid").value))/(100));
                                            else
                                                  var num = ((document.getElementById("Aitid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                            document.getElementById("aitPercentage"+i).innerHTML = num.toFixed(3);

                                          
                                            if(i==curIndex)
                                                 vatplusait = eval(document.getElementById("vatPercentage").innerHTML)+eval(document.getElementById("aitPercentage"+i).innerHTML);
                                            else
                                                 vatplusait = eval(document.getElementById("aitPercentage"+i).innerHTML);
                                        //  }
                                       //   else
                                        //   vatplusait = eval(document.getElementById("vatPercentage").innerHTML)+eval(document.getElementById("aitPercentage"+i).innerHTML);
                                            if(document.getElementById("tenderType").value == "ICT")
                                                document.getElementById("vatplusaitid"+i).innerHTML = vatplusait.toFixed(3);
                                            else
                                                document.getElementById("vatplusaitid"+i).innerHTML = ((eval(document.getElementById("vatplusaitid"+i).innerHTML))+vatplusait).toFixed(3);
                                            document.getElementById("FinalVatandAitid"+i).innerHTML = vatplusait.toFixed(3);
                                            document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("totalDeductedAmt"+i).innerHTML)+vatplusait).toFixed(3);
                                        }
                                    }
                                    else
                                    {
                                        var num = ((document.getElementById("Aitid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                        document.getElementById("aitPercentage"+i).innerHTML = num.toFixed(3);

                                        vatplusait = eval(0)+eval(document.getElementById("aitPercentage"+i).innerHTML);
                                        document.getElementById("vatplusaitid"+i).innerHTML = vatplusait.toFixed(3);
                                        document.getElementById("FinalVatandAitid"+i).innerHTML = vatplusait.toFixed(3);
                                        document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("totalDeductedAmt"+i).innerHTML)+eval(document.getElementById("aitPercentage"+i).innerHTML)).toFixed(3);
                                    }
                                    var GrossMinusAit = eval(document.getElementById("GrossAmtid"+i).value)-vatplusait;
                                    document.getElementById("GrossAmtid"+i).value = GrossMinusAit.toFixed(3);
                                    document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                  }
                                  else
                                  {
                                     flag = false;
                                     document.getElementById("aitspan"+i).innerHTML = "AIT can not be more than 100%";
                                  }
                                }
                                else
                                {
                                      flag = false;
                                      document.getElementById("aitspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                                }
                           }
                           else
                           {
                                flag = false;
                                document.getElementById("aitspan"+i).innerHTML = "special characters are not allowed";
                           }
                        }
                        else
                        {
                            flag = false;
                            document.getElementById("aitspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                        }
                    //  }
                   }
                   ////////////////1
                   if(document.getElementById("AdvVatAdjAmtid").value!="" && document.getElementById("AdvAitAdjAmtid").value!="")
                   {
                     if(document.getElementById("tenderType").value == "ICT")
                      {
                        if(validateDecimal(document.getElementById("AdvVatAdjAmtid").value))
                              {
                                if(document.getElementById("AdvVatAdjAmtid").value<=100)
                                {
                                  document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "";
                                  if(document.getElementById("AdvAitAdjAmtid").value!="")
                                  {
                                       if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAitAdjAmtid").value))
                                       {
                                           if(/^\d*\.?\d*$/.test(document.getElementById("AdvAitAdjAmtid").value))
                                           {
                                                if(validateDecimal(document.getElementById("AdvAitAdjAmtid").value))
                                                {
                                                  if(document.getElementById("AdvAitAdjAmtid").value<=100)
                                                  {
                                                    document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "";
                                                    if(document.getElementById("vatplusaitid"+i).innerHTML!="")
                                                    {
                                                        var num = ((document.getElementById("AdvVatAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                                        document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML = num.toFixed(3);
                                                        //document.getElementById("AdvVatAdjAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                                        var num = ((document.getElementById("AdvAitAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                                        document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML = num.toFixed(3);

                                                        var ADVAITplusVAT = eval(document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML)+eval(document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML);
                                                        document.getElementById("ADVAITplusVATid"+i).innerHTML = ADVAITplusVAT.toFixed(3);
                                                        var finalVatandAit = eval(document.getElementById("vatplusaitid"+i).innerHTML)-eval(ADVAITplusVAT);
                                                        document.getElementById("FinalVatandAitid"+i).innerHTML =finalVatandAit.toFixed(3);
                                                        document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)+eval(document.getElementById("vatplusaitid"+i).innerHTML)).toFixed(3);
                                                        document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)-(finalVatandAit)).toFixed(3);
                                                        document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                                        if(document.getElementById("FinalVatandAitid"+i).innerHTML>0)
                                                        {
                                                            if(document.getElementById("AdvAdjuAmtid").value!="")
                                                            {
                                                                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                                                                {
                                                                    if(validateDecimal(document.getElementById("AdvAdjuAmtid").value))
                                                                    {
                                                                        document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)+eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                                                    }
                                                                }
                                                            }else{document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);}

                                                        }else{
                                                            document.getElementById("totalAddedAmt"+i).innerHTML = (eval(document.getElementById("totalAddedAmt"+i).innerHTML)-eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                                            if(document.getElementById("AdvAdjuAmtid").value!="")
                                                            {
                                                                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                                                                {
                                                                    if(validateDecimal(document.getElementById("AdvAdjuAmtid").value))
                                                                    {
                                                                        document.getElementById("totalDeductedAmt"+i).innerHTML =(eval(0)+eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)).toFixed(3);//alert(document.getElementById("totalDeductedAmt").innerHTML);
                                                                    }
                                                                }
                                                            }else{document.getElementById("totalDeductedAmt"+i).innerHTML=eval(0);}
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var num = ((document.getElementById("AdvVatAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                                        document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML = num.toFixed(3);

                                                        var num = ((document.getElementById("AdvAitAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                                        document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML = num.toFixed(3);

                                                        var ADVAITplusVAT = eval(document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML)+eval(document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML);
                                                        document.getElementById("ADVAITplusVATid"+i).innerHTML = ADVAITplusVAT.toFixed(3);
                                                        var finalVatandAit = eval(0)-eval(ADVAITplusVAT);
                                                        document.getElementById("FinalVatandAitid"+i).innerHTML =finalVatandAit.toFixed(3);
                                                        document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)-(finalVatandAit)).toFixed(3);
                                                        document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                                        document.getElementById("totalAddedAmt"+i).innerHTML = (eval(document.getElementById("totalAddedAmt"+i).innerHTML)-eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                                    }
                                                  }
                                                  else
                                                  {
                                                     flag = false;
                                                     document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "Advance AIT Adjustment can not be more than 100%";
                                                  }
                                                }
                                                else
                                                {
                                                      flag = false;
                                                      document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                                                }
                                           }
                                          else
                                           {
                                                flag = false;
                                                document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "special characters are not allowed";
                                           }
                                       }
                                       else
                                       {
                                            flag = false;
                                            document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                                       }
                                  }
                               }
                               else
                               {
                                  flag = false;
                                  document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "Advance VAT Adjustment can not be more than 100%";
                               }
                             }
                            else
                            {
                                  flag = false;
                                  document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                            }
                      }
                      else{//NCT
                      if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvVatAdjAmtid").value) && document.getElementById("tenderType").value != "ICT")
                      {
                          if(/^\d*\.?\d*$/.test(document.getElementById("AdvVatAdjAmtid").value) && document.getElementById("tenderType").value != "ICT")
                          {
                              if(validateDecimal(document.getElementById("AdvVatAdjAmtid").value))
                              {
                                if(document.getElementById("AdvVatAdjAmtid").value<=100)
                                {
                                  document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "";
                                  if(document.getElementById("AdvAitAdjAmtid").value!="")
                                  {
                                       if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAitAdjAmtid").value))
                                       {
                                           if(/^\d*\.?\d*$/.test(document.getElementById("AdvAitAdjAmtid").value))
                                           {
                                                if(validateDecimal(document.getElementById("AdvAitAdjAmtid").value))
                                                {
                                                  if(document.getElementById("AdvAitAdjAmtid").value<=100)
                                                  {
                                                    document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "";
                                                    if(document.getElementById("vatplusaitid"+i).innerHTML!="")
                                                    {
                                                        var num = ((document.getElementById("AdvVatAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                                        document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML = num.toFixed(3);
                                                        //document.getElementById("AdvVatAdjAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                                        var num = ((document.getElementById("AdvAitAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                                        document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML = num.toFixed(3);

                                                        var ADVAITplusVAT = eval(document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML)+eval(document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML);
                                                        document.getElementById("ADVAITplusVATid"+i).innerHTML = ADVAITplusVAT.toFixed(3);
                                                        var finalVatandAit = eval(document.getElementById("vatplusaitid"+i).innerHTML)-eval(ADVAITplusVAT);
                                                        document.getElementById("FinalVatandAitid"+i).innerHTML =finalVatandAit.toFixed(3);
                                                        document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)+eval(document.getElementById("vatplusaitid"+i).innerHTML)).toFixed(3);
                                                        document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)-(finalVatandAit)).toFixed(3);
                                                        document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                                        if(document.getElementById("FinalVatandAitid"+i).innerHTML>0)
                                                        {
                                                            if(document.getElementById("AdvAdjuAmtid").value!="")
                                                            {
                                                                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                                                                {
                                                                    if(validateDecimal(document.getElementById("AdvAdjuAmtid").value))
                                                                    {
                                                                        document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)+eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                                                    }
                                                                }
                                                            }else{document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);}

                                                        }else{
                                                            document.getElementById("totalAddedAmt"+i).innerHTML = (eval(document.getElementById("totalAddedAmt"+i).innerHTML)-eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                                            if(document.getElementById("AdvAdjuAmtid").value!="")
                                                            {
                                                                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                                                                {
                                                                    if(validateDecimal(document.getElementById("AdvAdjuAmtid").value))
                                                                    {
                                                                        document.getElementById("totalDeductedAmt"+i).innerHTML =(eval(0)+eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)).toFixed(3);//alert(document.getElementById("totalDeductedAmt").innerHTML);
                                                                    }
                                                                }
                                                            }else{document.getElementById("totalDeductedAmt"+i).innerHTML=eval(0);}
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var num = ((document.getElementById("AdvVatAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                                        document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML = num.toFixed(3);

                                                        var num = ((document.getElementById("AdvAitAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                                        document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML = num.toFixed(3);

                                                        var ADVAITplusVAT = eval(document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML)+eval(document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML);
                                                        document.getElementById("ADVAITplusVATid"+i).innerHTML = ADVAITplusVAT.toFixed(3);
                                                        var finalVatandAit = eval(0)-eval(ADVAITplusVAT);
                                                        document.getElementById("FinalVatandAitid"+i).innerHTML =finalVatandAit.toFixed(3);
                                                        document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)-(finalVatandAit)).toFixed(3);
                                                        document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                                        document.getElementById("totalAddedAmt"+i).innerHTML = (eval(document.getElementById("totalAddedAmt"+i).innerHTML)-eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                                    }
                                                  }
                                                  else
                                                  {
                                                     flag = false;
                                                     document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "Advance AIT Adjustment can not be more than 100%";
                                                  }
                                                }
                                                else
                                                {
                                                      flag = false;
                                                      document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                                                }
                                           }
                                           else
                                           {
                                                flag = false;
                                                document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "special characters are not allowed";
                                           }
                                       }
                                       else
                                       {
                                            flag = false;
                                            document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                                       }
                                  }
                               }
                               else
                               {
                                  flag = false;
                                  document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "Advance VAT Adjustment can not be more than 100%";
                               }
                             }
                            else
                            {
                                  flag = false;
                                  document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                            }
                        }
                        else
                        {
                             flag = false;
                             document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "special characters are not allowed";
                        }
                      }
                      else
                      {
                         flag = false;
                         document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                      }
                     }
                   }
                   /////////////2
                   if(document.getElementById("AdvVatAdjAmtid").value!="" && document.getElementById("AdvAitAdjAmtid").value=="")
                   {
                      if(document.getElementById("tenderType").value == "ICT")
                      {
                    //  if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvVatAdjAmtid").value) )
                      {
                        //  if(/^\d*\.?\d*$/.test(document.getElementById("AdvVatAdjAmtid").value) && document.getElementById("tenderType").value != "ICT")
                          {
                            if(validateDecimal(document.getElementById("AdvVatAdjAmtid").value))
                            {
                               if(document.getElementById("AdvVatAdjAmtid").value<=100)
                               {
                                  document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "";
                                  if(document.getElementById("vatplusaitid"+i).innerHTML!="")
                                  {
                                      var num = ((document.getElementById("AdvVatAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                      document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML = num.toFixed(3);
                                     // document.getElementById("AdvVatAdjAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                      var ADVAITplusVAT = eval(document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML)+eval(0);
                                      document.getElementById("ADVAITplusVATid"+i).innerHTML = ADVAITplusVAT.toFixed(3);
                                      var finalVatandAit = eval(document.getElementById("vatplusaitid"+i).innerHTML)-eval(ADVAITplusVAT);
                                      document.getElementById("FinalVatandAitid"+i).innerHTML =finalVatandAit.toFixed(3);
                                      document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)+eval(document.getElementById("vatplusaitid"+i).innerHTML)).toFixed(3);
                                      document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)-(finalVatandAit)).toFixed(3);
                                      document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                      if(document.getElementById("FinalVatandAitid"+i).innerHTML>0)
                                      {
                                            if(document.getElementById("AdvAdjuAmtid").value!="")
                                            {
                                                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                                                {
                                                    if(validateDecimal(document.getElementById("AdvAdjuAmtid").value))
                                                    {
                                                        document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)+eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                                    }
                                                }
                                            }else{
                                            document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(0)+eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                            }
                                        }else{
                                            document.getElementById("totalAddedAmt"+i).innerHTML = (eval(0)-eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                            if(document.getElementById("AdvAdjuAmtid").value!="")
                                            {
                                                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                                                {
                                                    if(validateDecimal(document.getElementById("AdvAdjuAmtid").value))
                                                    {
                                                        document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)).toFixed(3);
                                                    }
                                                }
                                            }else{document.getElementById("totalDeductedAmt"+i).innerHTML=eval(0);}
                                        }
                                  }
                                  else
                                  {
                                      var num = ((document.getElementById("AdvVatAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                      document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML = num.toFixed(3);
                                     // document.getElementById("AdvVatAdjAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                      var ADVAITplusVAT = eval(document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML)+eval(0);
                                      document.getElementById("ADVAITplusVATid"+i).innerHTML = ADVAITplusVAT.toFixed(3);
                                      var finalVatandAit = eval(0)-eval(ADVAITplusVAT);
                                      document.getElementById("FinalVatandAitid"+i).innerHTML = finalVatandAit.toFixed(3);
                                      document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)-(finalVatandAit)).toFixed(3);
                                      document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                      document.getElementById("totalAddedAmt"+i).innerHTML =(eval(0)-eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);

                                  }
                                }
                                else
                                {
                                   flag = false;
                                   document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "Advance VAT Adjustment can not be more than 100%";
                                }
                              }
                              else
                              {
                                   flag = false;
                                   document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                              }
                         }
                       //  else
                         {
                         //     flag = false;
                         //     document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "special characters are not allowed";
                         }
                      }
                     // else
                      {
                     //    flag = false;
                     //    document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                      }
                      }else{ //for NCT
                      if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvVatAdjAmtid").value))
                      {
                          if(/^\d*\.?\d*$/.test(document.getElementById("AdvVatAdjAmtid").value))
                          {
                            if(validateDecimal(document.getElementById("AdvVatAdjAmtid").value))
                            {
                               if(document.getElementById("AdvVatAdjAmtid").value<=100)
                               {
                                  document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "";
                                  if(document.getElementById("vatplusaitid"+i).innerHTML!="")
                                  {
                                      var num = ((document.getElementById("AdvVatAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                      document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML = num.toFixed(3);
                                     // document.getElementById("AdvVatAdjAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                      var ADVAITplusVAT = eval(document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML)+eval(0);
                                      document.getElementById("ADVAITplusVATid"+i).innerHTML = ADVAITplusVAT.toFixed(3);
                                      var finalVatandAit = eval(document.getElementById("vatplusaitid"+i).innerHTML)-eval(ADVAITplusVAT);
                                      document.getElementById("FinalVatandAitid"+i).innerHTML =finalVatandAit.toFixed(3);
                                      document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)+eval(document.getElementById("vatplusaitid"+i).innerHTML)).toFixed(3);
                                      document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)-(finalVatandAit)).toFixed(3);
                                      document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                      if(document.getElementById("FinalVatandAitid"+i).innerHTML>0)
                                      {
                                            if(document.getElementById("AdvAdjuAmtid").value!="")
                                            {
                                                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                                                {
                                                    if(validateDecimal(document.getElementById("AdvAdjuAmtid").value))
                                                    {
                                                        document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)+eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                                    }
                                                }
                                            }else{
                                            document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(0)+eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                            }
                                        }else{
                                            document.getElementById("totalAddedAmt"+i).innerHTML = (eval(0)-eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                            if(document.getElementById("AdvAdjuAmtid").value!="")
                                            {
                                                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                                                {
                                                    if(validateDecimal(document.getElementById("AdvAdjuAmtid").value))
                                                    {
                                                        document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)).toFixed(3);
                                                    }
                                                }
                                            }else{document.getElementById("totalDeductedAmt"+i).innerHTML=eval(0);}
                                        }
                                  }
                                  else
                                  {
                                      var num = ((document.getElementById("AdvVatAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                      document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML = num.toFixed(3);
                                     // document.getElementById("AdvVatAdjAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                      var ADVAITplusVAT = eval(document.getElementById("AdvVatAdjAmtPercentage"+i).innerHTML)+eval(0);
                                      document.getElementById("ADVAITplusVATid"+i).innerHTML = ADVAITplusVAT.toFixed(3);
                                      var finalVatandAit = eval(0)-eval(ADVAITplusVAT);
                                      document.getElementById("FinalVatandAitid"+i).innerHTML = finalVatandAit.toFixed(3);
                                      document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)-(finalVatandAit)).toFixed(3);
                                      document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                      document.getElementById("totalAddedAmt"+i).innerHTML =(eval(0)-eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);

                                  }
                                }
                                else
                                {
                                   flag = false;
                                   document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "Advance VAT Adjustment can not be more than 100%";
                                }
                              }
                              else
                              {
                                   flag = false;
                                   document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                              }
                         }
                         else
                         {
                              flag = false;
                              document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "special characters are not allowed";
                         }
                      }
                      else
                      {
                         flag = false;
                         document.getElementById("AdvVatAdjAmtspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                      }
                     }
                   }
                   ///////////////////////3
                   if(document.getElementById("AdvAitAdjAmtid").value!="" && document.getElementById("AdvVatAdjAmtid").value=="")
                   {
                      if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAitAdjAmtid").value))
                      {
                          if(/^\d*\.?\d*$/.test(document.getElementById("AdvAitAdjAmtid").value))
                          {
                             if(validateDecimal(document.getElementById("AdvAitAdjAmtid").value))
                             {
                               if(document.getElementById("AdvAitAdjAmtid").value<=100)
                               {
                                  document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "";
                                  if(document.getElementById("vatplusaitid"+i).innerHTML!="")
                                  {
                                      var num = ((document.getElementById("AdvAitAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                      document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML = num.toFixed(3);

                                      var ADVAITplusVAT = eval(document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML)+eval(0);
                                      document.getElementById("ADVAITplusVATid"+i).innerHTML = ADVAITplusVAT.toFixed(3);
                                      var finalVatandAit = eval(document.getElementById("vatplusaitid"+i).innerHTML)-eval(ADVAITplusVAT);
                                      document.getElementById("FinalVatandAitid"+i).innerHTML =finalVatandAit.toFixed(3);
                                      document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)+eval(document.getElementById("vatplusaitid"+i).innerHTML)).toFixed(3);
                                      document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)-(finalVatandAit)).toFixed(3);
                                      document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                      if(document.getElementById("FinalVatandAitid"+i).innerHTML>0)
                                      {
                                            if(document.getElementById("AdvAdjuAmtid").value!="")
                                            {
                                                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                                                {
                                                    if(validateDecimal(document.getElementById("AdvAdjuAmtid").value))
                                                    {
                                                        document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)+eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                                    }
                                                }
                                            }else{
                                            document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(0)+eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                            }
                                        }else{
                                            document.getElementById("totalAddedAmt"+i).innerHTML = (eval(0)-eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);
                                            if(document.getElementById("AdvAdjuAmtid").value!="")
                                            {
                                                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("AdvAdjuAmtid").value))
                                                {
                                                    if(validateDecimal(document.getElementById("AdvAdjuAmtid").value))
                                                    {
                                                        document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("AdvAdjAmtPercentage"+i).innerHTML)).toFixed(3);
                                                    }
                                                }
                                            }else{document.getElementById("totalDeductedAmt"+i).innerHTML=eval(0);}
                                        }
                                      //document.getElementById("totalDeductedAmt").innerHTML = (eval(document.getElementById("totalDeductedAmt").innerHTML)+eval(document.getElementById("AdvAitAdjAmtid").value)).toFixed(3);

                                  }
                                  else
                                  {
                                      var num = ((document.getElementById("AdvAitAdjAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                      document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML = num.toFixed(3);
                                      //document.getElementById("AdvAitAdjAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                      var ADVAITplusVAT = eval(document.getElementById("AdvAitAdjAmtPercentage"+i).innerHTML)+eval(0);
                                      document.getElementById("ADVAITplusVATid"+i).innerHTML = ADVAITplusVAT.toFixed(3);
                                      var finalVatandAit = eval(0)-eval(ADVAITplusVAT);
                                      document.getElementById("FinalVatandAitid"+i).innerHTML = finalVatandAit.toFixed(3);
                                      document.getElementById("GrossAmtid"+i).value = (eval(document.getElementById("GrossAmtid"+i).value)-(finalVatandAit)).toFixed(3);
                                      document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                      document.getElementById("totalAddedAmt"+i).innerHTML =(eval(0)-eval(document.getElementById("FinalVatandAitid"+i).innerHTML)).toFixed(3);

                                  }
                                }
                                else
                                {
                                   flag = false;
                                   document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "Advance AIT Adjustment can not be more than 100%";
                                }
                              }
                              else
                              {
                                   flag = false;
                                   document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                              }
                         }
                         else
                         {
                              flag = false;
                              document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "special characters are not allowed";
                         }
                      }
                      else
                      {
                         flag = false;
                         document.getElementById("AdvAitAdjAmtspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                      }
                   }
                   ////////////////////////////////
               //     for(var i = 0;i<a.length;i++)
               //       {
                   if(document.getElementById("LiqDamageid").value!="")
                   {

                       if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("LiqDamageid").value))
                       {
                           if(/^\d*\.?\d*$/.test(document.getElementById("LiqDamageid").value))
                           {
                              if(validateDecimal(document.getElementById("LiqDamageid").value))
                              {
                                 if(document.getElementById("LiqDamageid").value<=100)
                                 {
                                       var totalLiquiPercentage = ((document.getElementById("totalLiquidityAmt").value*10)/(100));

                                       document.getElementById("LiqDamagespan"+i).innerHTML = "";
                                       var num1 = ((document.getElementById("LiqDamageid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                       document.getElementById("LiqDamagePercentage"+i).innerHTML = num1.toFixed(3);
                                       if(document.getElementById("LiqDamagePercentage"+i).innerHTML<totalLiquiPercentage)
                                       {
                                          // document.getElementById("LiqDamageinbdt"+i).innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                           var GrossAmtMinusLiqAmt = eval(document.getElementById("GrossAmtid"+i).value)-eval(document.getElementById("LiqDamagePercentage"+i).innerHTML);
                                           document.getElementById("GrossAmtid"+i).value = GrossAmtMinusLiqAmt.toFixed(3);
                                           document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                           document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("totalDeductedAmt"+i).innerHTML)+eval(document.getElementById("LiqDamagePercentage"+i).innerHTML)).toFixed(3);


                                       }
                                       else
                                       {
                                          flag = false;
                                          document.getElementById("LiqDamagespan"+i).innerHTML = "Liquidity Damage should not be more than 10% of the balance contract value ("+document.getElementById("totalLiquidityAmt").value+")";
                                       }
                                 }
                                 else
                                 {
                                    flag = false;
                                    document.getElementById("LiqDamagespan"+i).innerHTML = "Liquidity Damages Amount can not be more than 100%";
                                 }
                              }
                              else
                              {
                                   flag = false;
                                   document.getElementById("LiqDamagespan"+i).innerHTML = "only 3 digits after decimal are allowed";
                              }
                         }
                         else
                         {
                              flag = false;
                              document.getElementById("LiqDamagespan"+i).innerHTML = "special characters are not allowed";
                         }
                       }
                       else
                       {
                           flag = false;
                           document.getElementById("LiqDamagespan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                       }
                   }else{
                   document.getElementById("LiqDamagespan"+i).innerHTML = "";
                   document.getElementById("LiqDamagePercentage"+i).innerHTML ="";
                  // document.getElementById("LiqDamageinbdt"+i).innerHTML ="";
                   }
                 //  }
                   if(document.getElementById("PenaltyAmtid").value!="")
                   {
                       if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("PenaltyAmtid").value))
                       {
                           if(/^\d*\.?\d*$/.test(document.getElementById("PenaltyAmtid").value))
                           {
                              if(validateDecimal(document.getElementById("PenaltyAmtid").value))
                              {
                                 if(document.getElementById("PenaltyAmtid").value<=100)
                                 {
                                       document.getElementById("PenaltyAmtspan"+i).innerHTML = "";
                                       var num = ((document.getElementById("PenaltyAmtid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                       document.getElementById("PenaltyAmtPercentage"+i).innerHTML = num.toFixed(3);
                                      // document.getElementById("PenaltyAmtinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                       var GrossAmtMinusPenaltyAmt = eval(document.getElementById("GrossAmtid"+i).value)-eval(document.getElementById("PenaltyAmtPercentage"+i).innerHTML);
                                       document.getElementById("GrossAmtid"+i).value = GrossAmtMinusPenaltyAmt.toFixed(3);
                                       document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                       document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("totalDeductedAmt"+i).innerHTML)+eval(document.getElementById("PenaltyAmtPercentage"+i).innerHTML)).toFixed(3);
                                 }
                                 else
                                 {
                                    flag = false;
                                    document.getElementById("PenaltyAmtspan"+i).innerHTML = "Penalty Amount can not be more than 100%";
                                 }
                              }
                              else
                              {
                                   flag = false;
                                   document.getElementById("PenaltyAmtspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                              }
                         }
                         else
                         {
                              flag = false;
                              document.getElementById("PenaltyAmtspan"+i).innerHTML = "special characters are not allowed";
                         }
                       }
                       else
                       {
                           flag = false;
                           document.getElementById("PenaltyAmtspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                       }
                   }
                   if(document.getElementById("bonusid").value!="")
                   {
                       if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("bonusid").value))
                       {
                           if(/^\d*\.?\d*$/.test(document.getElementById("bonusid").value))
                           {
                              if(validateDecimal(document.getElementById("bonusid").value))
                              {
                                  if(document.getElementById("bonusid").value<=100)
                                  {
                                       document.getElementById("bonusspan"+i).innerHTML = "";
                                       var num = ((document.getElementById("bonusid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                       document.getElementById("bonusPercentage"+i).innerHTML = num.toFixed(3);
                                     //  document.getElementById("bonusinbdt"+i).innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                       var GrossAmtPlusBonus = eval(document.getElementById("GrossAmtid"+i).value)+eval(document.getElementById("bonusPercentage"+i).innerHTML);
                                       document.getElementById("GrossAmtid"+i).value = GrossAmtPlusBonus.toFixed(3);
                                       document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                       document.getElementById("totalAddedAmt"+i).innerHTML = (eval(document.getElementById("totalAddedAmt"+i).innerHTML)+eval(document.getElementById("bonusPercentage"+i).innerHTML)).toFixed(3);
                                  }
                                  else
                                  {
                                     flag = false;
                                     document.getElementById("bonusspan"+i).innerHTML = "Bonus Amount can not be more than 100%";
                                  }
                              }
                              else
                              {
                                   flag = false;
                                   document.getElementById("bonusspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                              }
                         }
                         else
                         {
                              flag = false;
                              document.getElementById("bonusspan"+i).innerHTML = "special characters are not allowed";
                         }
                       }
                       else
                       {
                           flag = false;
                           document.getElementById("bonusspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                       }
                   }
                   if(document.getElementById("InterestOnDPid").value!="")
                   {
                       if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("InterestOnDPid").value))
                       {
                           if(/^\d*\.?\d*$/.test(document.getElementById("InterestOnDPid").value))
                           {
                              if(validateDecimal(document.getElementById("InterestOnDPid").value))
                              {
                                  if(document.getElementById("InterestOnDPid").value<=100)
                                  {
                                       document.getElementById("IntDPspan"+i).innerHTML = "";
                                       var num = ((document.getElementById("InterestOnDPid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                       document.getElementById("InterestOnDPPercentage"+i).innerHTML = num.toFixed(3);
                                   
                                       var GrossAmtPlusBonus = eval(document.getElementById("GrossAmtid"+i).value)+eval(document.getElementById("InterestOnDPPercentage"+i).innerHTML);
                                       document.getElementById("GrossAmtid"+i).value = GrossAmtPlusBonus.toFixed(3);
                                       document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                       document.getElementById("totalAddedAmt"+i).innerHTML = (eval(document.getElementById("totalAddedAmt"+i).innerHTML)+eval(document.getElementById("InterestOnDPPercentage"+i).innerHTML)).toFixed(3);
                                  }
                                  else
                                  {
                                     flag = false;
                                     document.getElementById("IntDPspan"+i).innerHTML = "Interest On Delayed Payments can not be more than 100%";
                                  }
                              }
                              else
                              {
                                   flag = false;
                                   document.getElementById("IntDPspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                              }
                         }
                         else
                         {
                              flag = false;
                              document.getElementById("IntDPspan"+i).innerHTML = "special characters are not allowed";
                         }
                       }
                       else
                       {
                           flag = false;
                           document.getElementById("IntDPspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                       }
                   }
                   if(document.getElementById("retensionid").value!="")
                   {
                       if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("retensionid").value))
                       {
                           if(/^\d*\.?\d*$/.test(document.getElementById("retensionid").value))
                           {
                              if(validateDecimal(document.getElementById("retensionid").value))
                              {
                                if(document.getElementById("retensionid").value<=100)
                                {
                                   document.getElementById("retensionmoneyspan"+i).innerHTML = "";
                                   if(retension=="+")
                                   {
                                       var num = ((document.getElementById("retensionid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                       document.getElementById("retensionPercentage"+i).innerHTML = num.toFixed(3);
                                      // document.getElementById("retensioninbdt"+i).innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                       var GrossAmtPlusBonus = eval(document.getElementById("GrossAmtid"+i).value)+eval(document.getElementById("retensionPercentage"+i).innerHTML);
                                       document.getElementById("totalAddedAmt"+i).innerHTML = (eval(document.getElementById("totalAddedAmt"+i).innerHTML)+eval(document.getElementById("retensionPercentage"+i).innerHTML)).toFixed(3);
                                       document.getElementById("retensionspan"+i).innerHTML = " <%=bdl.getString("CMS.Inv.Retension.added")%>";
                                   }
                                   if(retension=="-")
                                   {
                                       var num = ((document.getElementById("retensionid").value*(document.getElementById("invoiceAmtid"+i).value))/(100));
                                       document.getElementById("retensionPercentage"+i).innerHTML = num.toFixed(3);
                                     //  document.getElementById("retensioninbdt"+i).innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
                                       var GrossAmtPlusBonus = eval(document.getElementById("GrossAmtid"+i).value)-eval(document.getElementById("retensionPercentage"+i).innerHTML);
                                       document.getElementById("totalDeductedAmt"+i).innerHTML = (eval(document.getElementById("totalDeductedAmt"+i).innerHTML)+eval(document.getElementById("retensionPercentage"+i).innerHTML)).toFixed(3);
                                        document.getElementById("retensionspan"+i).innerHTML = " <%=bdl.getString("CMS.Inv.Retension.deducted")%>";
                                   }

                                   document.getElementById("GrossAmtid"+i).value = GrossAmtPlusBonus.toFixed(3);
                                   document.getElementById("GrossinWords"+i).innerHTML = CurrencyConverter(document.getElementById("GrossAmtid"+i).value);
                                }
                                else
                                {
                                    flag = false;
                                    document.getElementById("retensionmoneyspan"+i).innerHTML = "Retention Money can not be more than 100%";
                                }
                              }
                              else
                              {
                                   flag = false;
                                   document.getElementById("retensionmoneyspan"+i).innerHTML = "only 3 digits after decimal are allowed";
                              }
                         }
                         else
                         {
                              flag = false;
                              document.getElementById("retensionmoneyspan"+i).innerHTML = "special characters are not allowed";
                         }
                       }
                       else
                       {
                           flag = false;
                           document.getElementById("retensionmoneyspan"+i).innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
                       }
                   }

             }
             else
             {
                 flag = false;
                 document.getElementById("MainAmtspan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
             }
           }
           else
           {
              flag = false;
              document.getElementById("MainAmtspan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
           }
           }
           return flag;
        }
        function validate()
        {
           var flag = true;
           if(document.getElementById("modeofPaymentid").value == "select")
           {
               flag = false;
               document.getElementById("modeofPaymentspan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.MOP.field")%>";
           }
           else{
               document.getElementById("modeofPaymentspan").innerHTML = "";
           }
           if(document.getElementById("PstartDate").value == "")
           {
               flag = false;
               document.getElementById("DateofPaymentspan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.DOP.field")%>";
           }else{
               document.getElementById("DateofPaymentspan").innerHTML ="";
           }
//           else{
//               var value = document.getElementById("PstartDate").value;
//                if(value!=null && value!=''){
//                    flag = true;
//                var mdy = value.split('/')  //Date and month split
//                var mdyhr= mdy[2].split(' ');  //Year and time split
//
//                if(mdyhr[1] == undefined){
//                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
//                }
//                var d = new Date();
//                if(mdyhr[1] == undefined){
//                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
//                }
//                if(Date.parse(valuedate) <= Date.parse(todaydate)){
//                        flag =  true;
//                }else{
//                    document.getElementById('DateofPaymentspan').innerHTML='<%=bdl.getString("CMS.Inv.validate.Date.field")%>';
//                            flag =  false;
//                    }
//                }
//           }
           if(document.getElementById("BankNametxtid").value == "")
           {
               flag = false;
               document.getElementById("banknamespan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.Bank.field")%>";
           }
           else{
                if(/^(([a-zA-Z])(?!$)([a-zA-Z \&\.\-]+[\&\.\-\a-zA-Z ])?)+$/.test(document.getElementById("BankNametxtid").value)){
                    document.getElementById("banknamespan").innerHTML ="";
                }
                else
                {
                    document.getElementById("banknamespan").innerHTML = "Please enter a valid Bank Name";
                    flag=false;
                }
           }
           if(document.getElementById("BranchNametxtid").value == "")
           {
               flag = false;
               document.getElementById("branchnamespan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.Branch.field")%>";
           }
           else{
               document.getElementById("branchnamespan").innerHTML = "";
           }
           if(document.getElementById("InstNumbertxtid").value == "")
           {
               flag = false;
               document.getElementById("instrumentspan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.Inst.field")%>";
           }else{
               document.getElementById("instrumentspan").innerHTML = "";
           }
          /* if(document.getElementById("vatmodeofPaymentid").value == "select")
           {
               if(document.getElementById("Vatid").value!=""){
                   flag = false;
                   document.getElementById("vatmodeofPaymentspan").innerHTML = "<%//=bdl.getString("CMS.Inv.validate.MOP.field")%>";
               }
           }
           else{
               document.getElementById("vatmodeofPaymentspan").innerHTML = "";
           }
           if(document.getElementById("vatPstartDate").value == "")
           {
               if(document.getElementById("Vatid").value!=""){
                   flag = false;
                   document.getElementById("vatDateofPaymentspan").innerHTML = "<%//=bdl.getString("CMS.Inv.validate.DOP.field")%>";
               }
           }
           else{
               if(document.getElementById("Vatid").value!=""){
                    document.getElementById("vatDateofPaymentspan").innerHTML = "";
               }else{
                   jAlert("Please enter VAT Amount.","VAT", function(RetVal) {});
                   flag = false;
               }
           }
           if(document.getElementById("vatBankNametxtid").value == "")
           {
               if(document.getElementById("Vatid").value!=""){
                   flag = false;
                   document.getElementById("vatbanknamespan").innerHTML = "<%//=bdl.getString("CMS.Inv.validate.Bank.field")%>";
               }
           }
           else{
                if(document.getElementById("Vatid").value!=""){
                    if(/^(([a-zA-Z])(?!$)([a-zA-Z \&\.\-]+[\&\.\-\a-zA-Z ])?)+$/.test(document.getElementById("vatBankNametxtid").value)){
                        document.getElementById("vatbanknamespan").innerHTML ="";
                    }
                    else
                    {
                        document.getElementById("vatbanknamespan").innerHTML = "Please enter a valid Bank Name";
                        flag=false;
                    }
                }else{
                    jAlert("Please enter VAT Amount.","VAT", function(RetVal) {});
                    flag = false;
                }
           }
           if(document.getElementById("vatBranchNametxtid").value == "")
           {
               if(document.getElementById("Vatid").value!=""){
                   flag = false;
                   document.getElementById("vatbranchnamespan").innerHTML = "<%//=bdl.getString("CMS.Inv.validate.Branch.field")%>";
               }
           }
           else{
                if(document.getElementById("Vatid").value!=""){
                    document.getElementById("vatbranchnamespan").innerHTML = "";
                }
                else{
                    jAlert("Please enter VAT Amount.","VAT", function(RetVal) {});
                    flag = false;
                }
           }
           if(document.getElementById("vatInstNumbertxtid").value == "")
           {
               if(document.getElementById("Vatid").value!=""){
                   flag = false;
                   document.getElementById("vatinstrumentspan").innerHTML = "<%//=bdl.getString("CMS.Inv.validate.Inst.field")%>";
               }
           }
           else{
               if(document.getElementById("Vatid").value!=""){
                    document.getElementById("vatinstrumentspan").innerHTML = "";
               }else{
                   jAlert("Please enter VAT Amount.","VAT", function(RetVal) {});
                   flag = false;
               }
           }
           if(document.getElementById("aitmodeofPaymentid").value == "select")
           {
               if(document.getElementById("Aitid").value!=""){
                   flag = false;
                   document.getElementById("aitmodeofPaymentspan").innerHTML = "<%//=bdl.getString("CMS.Inv.validate.MOP.field")%>";
               }
           }
           else{
               document.getElementById("aitmodeofPaymentspan").innerHTML = "";
           }
           if(document.getElementById("aitPstartDate").value == "")
           {
               if(document.getElementById("Aitid").value!=""){
                   flag = false;
                   document.getElementById("aitDateofPaymentspan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.DOP.field")%>";
               }
           }
           else{
                if(document.getElementById("Aitid").value!=""){
                    document.getElementById("aitDateofPaymentspan").innerHTML = "";
                }else{
                    jAlert("Please enter AIT Amount.","AIT", function(RetVal) {});
                    flag = false;
                }
           }
           if(document.getElementById("aitBankNametxtid").value == "")
           {
               if(document.getElementById("Aitid").value!=""){
                   flag = false;
                   document.getElementById("aitbanknamespan").innerHTML = "<%//=bdl.getString("CMS.Inv.validate.Bank.field")%>";
               }
           }
           else{
                if(document.getElementById("Aitid").value!=""){
                    if(/^(([a-zA-Z])(?!$)([a-zA-Z \&\.\-]+[\&\.\-\a-zA-Z ])?)+$/.test(document.getElementById("aitBankNametxtid").value)){
                        document.getElementById("aitbanknamespan").innerHTML ="";
                    }
                    else
                    {
                        document.getElementById("aitbanknamespan").innerHTML = "Please enter a valid Bank Name";
                        flag=false;
                    }
                }else{
                    jAlert("Please enter AIT Amount.","AIT", function(RetVal) {});
                    flag = false;
                }
           }
           if(document.getElementById("aitBranchNametxtid").value == "")
           {
               if(document.getElementById("Aitid").value!=""){
                   flag = false;
                   document.getElementById("aitbranchnamespan").innerHTML = "<%//=bdl.getString("CMS.Inv.validate.Branch.field")%>";
               }
           }
           else{
               if(document.getElementById("Aitid").value!=""){
                    document.getElementById("aitbranchnamespan").innerHTML = "";
               }else{
                   jAlert("Please enter AIT Amount.","AIT", function(RetVal) {});
                   flag = false;
               }
           }
           if(document.getElementById("aitInstNumbertxtid").value == "")
           {
               if(document.getElementById("Aitid").value!=""){
                   flag = false;
                   document.getElementById("aitinstrumentspan").innerHTML = "<%//=bdl.getString("CMS.Inv.validate.Inst.field")%>";
               }
           }
           else{
               if(document.getElementById("Aitid").value!=""){
                   document.getElementById("aitinstrumentspan").innerHTML = "";
               }else{
                   jAlert("Please enter AIT Amount.","AIT", function(RetVal) {});
                   flag = false;
               }
           }*/
           if(flag)
           {
               flag = Evalute();
           }
           return flag;
        }
        function blockMessage(id,spanid)
        {
           if(document.getElementById(id).value!="")
           {
               document.getElementById(spanid).innerHTML = "";
           }
        }
//        function blockMessage()
//        {
//           if(document.getElementById("modeofPaymentid").value!="select")
//           {
//               document.getElementById("modeofPaymentspan").innerHTML = "";
//           }
//           if(document.getElementById("PstartDate").value != "")
//           {
//               document.getElementById("DateofPaymentspan").innerHTML = "";
//           }
//           if(document.getElementById("BankNametxtid").value != "")
//           {
//               document.getElementById("banknamespan").innerHTML = "";
//           }
//           if(document.getElementById("BranchNametxtid").value != "")
//           {
//               document.getElementById("branchnamespan").innerHTML = "";
//           }
//           if(document.getElementById("InstNumbertxtid").value != "")
//           {
//               document.getElementById("instrumentspan").innerHTML = "";
//           }
//           if(document.getElementById("vatmodeofPaymentid").value!="select")
//           {
//               document.getElementById("vatmodeofPaymentspan").innerHTML = "";
//           }
//           if(document.getElementById("vatPstartDate").value != "")
//           {
//               document.getElementById("vatDateofPaymentspan").innerHTML = "";
//           }
//           if(document.getElementById("vatBankNametxtid").value != "")
//           {
//               document.getElementById("vatbanknamespan").innerHTML = "";
//           }
//           if(document.getElementById("vatBranchNametxtid").value != "")
//           {
//               document.getElementById("vatbranchnamespan").innerHTML = "";
//           }
//           if(document.getElementById("vatInstNumbertxtid").value != "")
//           {
//               document.getElementById("vatinstrumentspan").innerHTML = "";
//           }
//           if(document.getElementById("aitmodeofPaymentid").value!="select")
//           {
//               document.getElementById("aitmodeofPaymentspan").innerHTML = "";
//           }
//           if(document.getElementById("aitPstartDate").value != "")
//           {
//               document.getElementById("aitDateofPaymentspan").innerHTML = "";
//           }
//           if(document.getElementById("aitBankNametxtid").value != "")
//           {
//               document.getElementById("aitbanknamespan").innerHTML = "";
//           }
//           if(document.getElementById("aitBranchNametxtid").value != "")
//           {
//               document.getElementById("aitbranchnamespan").innerHTML = "";
//           }
//           if(document.getElementById("aitInstNumbertxtid").value != "")
//           {
//               document.getElementById("aitinstrumentspan").innerHTML = "";
//           }
//        }
        function AmtInWords()
        {

       //  var a =document.getElementsByName("amtWords");
        var sDomAttr =  document.getElementById("totalCurrency").value;
            for(var i = 0;i<sDomAttr;i++)
                {
                document.getElementById("amtinWords"+i).innerHTML = '('+(CurrencyConverter(document.getElementById("invoiceAmtid"+i).value))+')';
                document.getElementById("GrossinWords"+i).innerHTML = '('+(CurrencyConverter(document.getElementById("GrossAmtid"+i).value))+')';

                }
           // document.getElementById("amtinWords").innerHTML = '('+(DoIt(document.getElementById("invoiceAmtid").value))+')';
           // document.getElementById("GrossinWords").innerHTML = '('+(DoIt(document.getElementById("GrossAmtid").value))+')';
        }
        function validateDecimal(value)
        {
            var flag =false;
            var a = value;
            var b = a.indexOf('.');
            if(b>0)
            {
                a = a.substring(b+1, a.length);
                if(a.length<=3)
                {
                    flag=true;
                }
            }
            else
            {
                flag =true;
            }
            return flag;
        }
    </script>
           <body onload="AmtInWords();">
        <%
            String tenderId = "";
            if (request.getParameter("tenderId") != null) {
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                tenderId = request.getParameter("tenderId");
            }
            String userId = "";
            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = session.getAttribute("userId").toString();
            }
            String InvoiceId = "";
            if(request.getParameter("InvoiceId")!=null)
            {
                InvoiceId = request.getParameter("InvoiceId");
            }

            String lotId = "";
            if(request.getParameter("lotId")!=null)
            {
                lotId = request.getParameter("lotId");
                pageContext.setAttribute("lotId", request.getParameter("lotId"));
            }
            pageContext.setAttribute("tab", "14");
            pageContext.setAttribute("invoiceId", InvoiceId);

            List<TblCmsInvoiceAccDetails> invoicedetails = null;
           // List<TblCmsInvoiceAccDetails> invoicedetails = null;
            ConsolodateService consolodateService = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
            invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(InvoiceId));
            boolean isEdit = false;
            CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
            String conType = commService.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
            if(!invoicedetails.isEmpty())
            {
                isEdit = true;
            }
       %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1"><%=bdl.getString("CMS.Inv.Page.Title")%>
                <span style="float: right; text-align: right;">
                <a class="action-button-goback" href="MakePayment.jsp" title="STD Dashboard">Go Back</a>
                </span>
                </div>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
  <%@include file="../resources/common/ContractInfoBar.jsp"%>
                <%--<%@include  file="officerTabPanel.jsp"%>--%>
                    <div class="tabPanelArea_1 t_space">
                    <%
                        pageContext.setAttribute("TSCtab", "3");
                    %>
                    <%--<%@include  file="../resources/common/CMSTab.jsp"%>--%>
                    <div class="tabPanelArea_1">
                    <%
                    String message="";
                    if(request.getParameter("msg")!=null)
                    {
                        message = request.getParameter("msg");
                    }
                    if("succ".equalsIgnoreCase(message))
                    {
                %>
                        <div class="responseMsg successMsg t_space"><span><%=bdl.getString("CMS.Inv.invSave")%></span></div>
                <%  }else if("failed".equalsIgnoreCase(message)){%>
                        <div class="responseMsg errorMsg t_space"><span><%=bdl.getString("CMS.Inv.invSavefail")%></span></div>
                <%}else if("updationfailed".equalsIgnoreCase(message)) {%>
                        <div class="responseMsg errorMsg t_space"><span><%=bdl.getString("CMS.Inv.invUpdatefail")%></span></div>
                <%}%>
                <%
                    boolean check = false;
                    check = consolodateService.isWorkCompleteOrNot(lotId);
                    if(check){
                %>
                <div  class='responseMsg noticeMsg t-align-left'>Current Invoice will be considered as a Final Invoice as Work Completion Certificate has been issued by Procuring Entity</div>
                <%}%>

                    <div align="center">
                      <form action='<%=request.getContextPath()%>/AccPaymentServlet?tenderId=<%=tenderId%>&action=saveInvoiceAccDetais' method="post" name="AccPaymentfrm">
                          <%
                               CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                               String procCase = commService.getProcNature(request.getParameter("tenderId")).toString();
                               String strProcNature = "";
            if("Services".equalsIgnoreCase(procCase))
            {
                    strProcNature = "Consultant";
            }else if("goods".equalsIgnoreCase(procCase)){
                    strProcNature = "Supplier";
            }else{
                    strProcNature = "Contractor";
            }
                              // ConsolodateService cs = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");
                               List<TblCmsInvoiceMaster> InvMasdata = cs.getTblCmsInvoiceMaster(Integer.parseInt(InvoiceId));
                               if(!"services".equalsIgnoreCase(procCase)){
                               if(!InvMasdata.isEmpty())
                               {
                                   if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                   {
                              List<Object[]> invIdData = null;
                            
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <%
                                if(tenderType1.equals("ICT")){
                                invIdData = cs.getInvoiceTotalAmtForICT(Integer.parseInt(InvoiceId),Integer.parseInt(request.getParameter("wpId")));
                                if (!invIdData.isEmpty() && invIdData != null) {

                                for(int k=0;k<invIdData.size(); k++)
                                {
                                
                                %>
                                <tr>
                                    <td class="ff" width="88%">Advance Amount (In <%=invIdData.get(k)[1]%>)</td>
                                    <td width="12%"><%=invIdData.get(k)[0]%></td>
                                </tr>
                                <%}}}else{%>
                                <tr>
                                    <td class="ff" width="88%">Advance Amount <%=bdl.getString("CMS.Inv.InBDT")%></td>
                                    <td width="12%"><%=InvMasdata.get(0).getTotalInvAmt()%></td>
                                </tr>
                                <% } %>
                            </table>
                            <%}else{%>
                            <%@include  file="../resources/common/CommonViewInvoice.jsp"%>
                            <%}}}else{
                            if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                               {
                            %>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <td class="ff" width="88%">Advance Amount <%=bdl.getString("CMS.Inv.InBDT")%></td>
                                        <td width="12%"><%=InvMasdata.get(0).getTotalInvAmt()%></td>
                                    </tr>
                                </table>
                             <%}else{
                                   if("time based".equalsIgnoreCase(conType)){
                             %>
                                            <%
                                                List<Object[]> AttSheetslist = cmss.ViewAttSheetDtls(InvMasdata.get(0).getTillPrid());
                                                String weeks_days[] = new String[7];
                                                MonthName monthName = new MonthName();
                                                BigDecimal TotalAmount = new BigDecimal(0);
                                                if(AttSheetslist!=null && !AttSheetslist.isEmpty()){
                                            %>
                                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                                <th width="3%" class="t-align-center">Sr.No</th>
                                                <th width="20%" class="t-align-center">Name of Employees</th>
                                                <th width="15%" class="t-align-center">Position Assigned</th>
                                                <th width="9%" class="t-align-center">Home / Field</th>
                                                <th width="5%" class="t-align-center">No of Days</th>
                                                <th width="5%" class="t-align-center">Rate</th>
                                                <th width="18%" class="t-align-center">Month</th>
                                                <th width="18%" class="t-align-center">Year</th>
                                                <th width="18%" class="t-align-center">Invoice Amount</th>
                                            </tr>
                                            <%
                                                for(Object obj[] : AttSheetslist){
                                                TotalAmount = TotalAmount.add((BigDecimal)obj[10]) ;
                                            %>
                                            <tr>
                                                <td width="3%" class="t-align-center"><%=obj[0]%></td>
                                                <td width="20%" class="t-align-center"><%=obj[1]%></td>
                                                <td width="15%" class="t-align-center"><%=obj[2]%></td>
                                                <td width="9%" class="t-align-center"><%=obj[7]%></td>
                                                <td width="5%" class="t-align-center"><%=obj[3]%></td>
                                                <td width="5%" class="t-align-center"><%=obj[4]%></td>
                                                <td width="5%" class="t-align-center"><%=monthName.getMonth((Integer) obj[5]) %>
                                                <td width="5%" class="t-align-center"><%=obj[6]%></td>
                                                <td width="5%" class="t-align-center"><%=obj[10]%></td>
                                           </tr>
                                               <% }}%>
                                           <tr><td class="t-align-center" colspan=7></td>
                                               <td class="t-align-center ff">Total Amount (In Nu.)</td>
                                               <td class="t-align-right ff" style="text-align: right;"><%=TotalAmount%></td>
                                           </tr>
                                            </table>
                              <%@include  file="../resources/common/CommonViewInvoice.jsp"%>
                              <%}else{%>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <th>S No</th>
                                <th>Milestone Name </th>
                                <th>Description</th>
                                <th>Payment as % of Contract Value</th>
                                <th>Mile Stone Date proposed by PE </th>
                                <th>Mile Stone Date proposed by Consultant</th>
                                <th>Invoice Amount <br/><%=bdl.getString("CMS.Inv.InBDT")%></th>
                                <%
                                    List<TblCmsSrvPaymentSch> listPaymentData = cmss.getPaymentScheduleDatabySrvPSId(InvMasdata.get(0).getTillPrid());
                                    if(!listPaymentData.isEmpty()){
                                %>
                                <tr>
                                    <td class="t-align-center"><%=listPaymentData.get(0).getSrNo()%></td>
                                    <td class="t-align-center"><%=listPaymentData.get(0).getMilestone()%></td>
                                    <td class="t-align-center"><%=listPaymentData.get(0).getDescription()%></td>
                                    <td class="t-align-center"><%=listPaymentData.get(0).getPercentOfCtrVal()%></td>
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(listPaymentData.get(0).getPeenddate())%></td>
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(listPaymentData.get(0).getEndDate())%></td>
                                    <td><%=new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*listPaymentData.get(0).getPercentOfCtrVal().doubleValue())/100).setScale(3,0)%>
                                </tr>
                            </table>
                            <%}}}}
                            %>
                          <table cellspacing="0" class="tableList_1 t_space" width="100%">


                                    <%

                                        List<Object[]> totalAmt = null;
                                        ConsolodateService getTotalAmt = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                      //  TenderTablesSrBean beanCommon1 = new TenderTablesSrBean();
                                       // String tenderType1 = beanCommon1.getTenderType(Integer.parseInt(request.getParameter("tenderId")));
                                        String AdvanceAmount = accPaymentService.getAdvanceContractAmount(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                                        String salvageAmountStr = accPaymentService.getSalavageContractAmount(Integer.parseInt(tenderId), Integer.parseInt(lotId));

                                        BigDecimal salvageAmount=new BigDecimal(salvageAmountStr).setScale(3,0);
                                        if(tenderType1.equals("ICT"))
                                           // totalAmt =  getTotalAmt.getInvoiceTotalAmtForICT(Integer.parseInt(InvoiceId),Integer.parseInt(request.getParameter("wpId")));
                                            totalAmt = getTotalAmt.returndata("getInvoiceTotalAmtForICT",InvoiceId, request.getParameter("wpId"));
                                        else{
                                            if("no".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                                totalAmt =  getTotalAmt.getInvoiceTotalAmt(InvoiceId);
                                            else
                                                totalAmt =  getTotalAmt. getInvoiceTotalAmtAdv(InvoiceId);
                                            }
                                           String[] curArr = new String[totalAmt.size()];
                                           String[] totalArr = new String[totalAmt.size()];
                                           String[] totalInvoiceId = new String[totalAmt.size()];
                                           String[] cntrAmnt = new String[totalAmt.size()];
                                        String totalAmount = "";
                                    //    int incr = 0;
                                        totalCur = totalAmt.size();
                                        totalCurForGross = totalCur;
                                        if(totalCur == 0)
                                            {
                                            totalCur = 1;
                                            curArr[0] = "BTN";
                                            }
                                        int incr=0;
                                        boolean bdtflag = false;
                                         List<String> listStr = new ArrayList<String>();
                                        listStr.add("BTN");
                                        for(int loop=0; loop<totalAmt.size();loop++)
                                       // for(Object[] oob:totalAmt)
                                            {

                                            totalArr[incr] = totalAmt.get(loop)[0].toString();
                                            //System.out.println("totalArr[incr]"+totalArr[incr]);
                                            if(tenderType1.equals("ICT")){
                                                if(totalAmt.get(loop)[1].toString().equals("BTN") && bdtflag == false)
                                                {
                                                    curArr[incr] = totalAmt.get(loop)[1].toString();
                                                    totalInvoiceId[incr] = totalAmt.get(loop)[2].toString();
                                                    cntrAmnt[incr] = totalAmt.get(loop)[3].toString();
                                                    incr = incr +1;
                                                    bdtflag = true;

                                                   
                                                }
                                                else if (!totalAmt.get(loop)[1].toString().equals("BTN")){
                                                  
                                                    curArr[incr] = totalAmt.get(loop)[1].toString();
                                                    totalInvoiceId[incr] = totalAmt.get(loop)[2].toString();
                                                    cntrAmnt[incr] = totalAmt.get(loop)[3].toString();
                                                    incr = incr +1;
                                                }
                                                else
                                                    {continue;}
                                            }
                                            else
                                                {
                                                curArr[incr] = "BTN";
                                                totalInvoiceId[incr] = totalAmt.get(incr)[1].toString();
                                                cntrAmnt[incr] = c_obj[2].toString();
                                                incr = incr +1;
                                                }
                                            }
                                     totalCur = incr;
                                     totalCurForGross = incr;
                                     for(int incr2=0;incr2<incr;incr2++){        %>
                                             <tr>
                                             
                                     <td class="ff" width="25%" class="t-align-left">Invoice amount (In <%=curArr[incr2].toString()%>)</td>
                                              <td class="t-align-left">

                                      <%
                                      ///  if(!totalAmt.isEmpty())
                                      //  {
                                     //       totalAmount = totalAmt.get(0).toString();
                                     //   }
                                    %>
                                    <%if(isEdit){
                                        invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(totalInvoiceId[incr2]));
                                        out.print(invoicedetails.get(0).getInvoiceAmt());
                                      }
                                      else{
                                                out.print(totalAmt.get(incr2)[0].toString());
                                        }

                                    %>

                                    <input type="hidden" class="formTxtBox_1" name="invoiceAmttxt<%=incr2%>" readonly id="invoiceAmtid<%=incr2%>" <%if(isEdit){%> value="<%=invoicedetails.get(0).getInvoiceAmt()%>"<%}else{%>value="<%=totalAmt.get(incr2)[0].toString()%>"<%}%>onblur="Evalute()">
                                    <input type="hidden" name="wpId" value="<%=wpId%>">
                                    <input type="hidden" id="Contarctvalue<%=incr2%>" value="<%=cntrAmnt[incr2]%>">
                                    <input type="hidden" name="InvoiceId<%=incr2%>" value="<%=totalInvoiceId[incr2].toString()%>">
                                    <input type="hidden" name="lotId" value="<%=lotId%>">
                                    <input type="hidden" name="currency" id="currency<%=incr2%>" value="<%=curArr[incr2]%>">
                                    <input type="hidden" name="InvoiceAccDtId<%=incr2%>" <%if(isEdit){%> value="<%=invoicedetails.get(0).getInvoiceAccDtlId()%>"<%}%>>
                                    &nbsp;&nbsp;&nbsp;<span name ="amtWords" id="amtinWords<%=incr2%>" class="ff"></span>
                                    <span id="MainAmtspan" class="reqF_1"></span>
                                </td> <% /*incr ++ ;*/}
                                     %>
                                 <input type="hidden" name="invoiceNo" value="<%=invoiceNo1%>">
                                 <input type="hidden" name="totalCurrency" id="totalCurrency" value="<%=totalCur%>">
                                <input type="hidden" name="tenderType" id="tenderType" value="<%=tenderType1%>">


                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.deduction")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%" style="table-layout: fixed;">
                            <tr><td class="ff" width="25%">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="ff" width="25%">Total Advance Adjustment till Date :</td>
                                <td>
                                    <%
                                        int ti = 0;
                                        Object[] objj = null;
                                        double objjj = 0;
                                        BigDecimal salvageAdjAmt=BigDecimal.ZERO.setScale(3,0);
                                        List<Object[]> objCurBefore = Collections.EMPTY_LIST;
                                        List<Object[]> objAdjustmentBefore = Collections.EMPTY_LIST;
                                       // String curForAdjustment[] =
                                        String[] curForAdjustment = new String[4];
                                        List<String> curNew = new ArrayList<String>();
                                         String[] amountForAdjustment = new String[4];
                                       List<Object[]> totalDeDeducted = accPaymentService.getTotalDeDuctedInvoiceAmtForWp(Integer.parseInt(wpId));
                                        if(!totalDeDeducted.isEmpty())
                                        {
                                            objj = totalDeDeducted.get(0);
                                            if(objj[1]==null)
                                            {
                                                out.print(new BigDecimal(0).setScale(3,0));

                                            }
                                            else{
                                                if(tenderType1.equals("ICT"))
                                                  { 
                                                    objCurBefore = accPaymentService.getCurrencyInAccGenerateBefore(Integer.parseInt(wpId),Integer.parseInt(tenderId));
                                                    if(!objCurBefore.isEmpty())
                                                        {
                                                       
                                                        for(int i=0;i<objCurBefore.size();i++){
                                                            for(int j=0;j<totalCur;j++)
                                                                {
                                                                if(curArr[j].equals(objCurBefore.get(i)[1].toString()))
                                                                    break;
                                                                else if (j+1 == totalCur){
                                                                    curForAdjustment[ti] = objCurBefore.get(i)[1].toString();
                                                                    amountForAdjustment[ti] = objCurBefore.get(i)[0].toString();
                                                                    ti++;
                                                                    }

                                                                }
                                                            }
                                                       
                                                     }
                                                     objjj = (Double.valueOf(objj[1].toString())/(double)(ti+totalCur));
                                                   //  out.print(new BigDecimal(objj[1].toString()).doubleValue()/(double)(ti+totalCur)).setScale(3,0);
                                                     out.print(new BigDecimal(objjj).setScale(3,0));
                                                  }
                                                else
                                                  {
                                                  objjj = Double.valueOf(objj[1].toString());
                                                  salvageAdjAmt=new BigDecimal(objj[3].toString()).setScale(3,0);
                                                  out.print(new BigDecimal(objj[1].toString()).setScale(3,0));
                                                  }
                                            }
                                        }
                                    %>
                                  
                                     <% if(!tenderType1.equals("ICT")){%>
                                          <input type="hidden" id="totaladvamttilldate" <%if(objj[1]==null){%>value="0"<%}else{%>value="<%=objjj%>"<%}%>>&nbsp;&nbsp;&nbsp;&nbsp;

                                         <span id="tAATDpercentage0"></span><span id="tAATDinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                    <% }else{ 
                                            for(int i=0;i<totalCur;i++)
                                            {
                                           %>
                                        
                                        <input type="hidden" id="totaladvamttilldate" <%if(objj[1]==null){%>value="0"<%}else{%>value="<%=objjj%>"<%}%>>&nbsp;&nbsp;&nbsp;&nbsp;

                                       <span id="tAATDpercentage<%=i%>"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="tAATDinbdt">&nbsp;&nbsp;&nbsp;&nbsp;(In  <%=curArr[i]%>)</span>&nbsp;&nbsp;<br>
                                     <%    } //}
                                    if(curForAdjustment[0]!="" && curForAdjustment[0]!=null)
                                     {
                                      for(int i=0;i<curForAdjustment.length;i++)
                                          {
                                          objAdjustmentBefore = getTotalAmt.returndata("getAdjustAmountGenerateBefore",wpId, curForAdjustment[i]);
                                          double advAdj = (Double.valueOf(amountForAdjustment[i])*Double.valueOf(AdvanceAmount))/100;
                                           %>
                                          <span id =" "><%=objAdjustmentBefore.get(0)[0].toString()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=new BigDecimal((advAdj * Double.valueOf(objAdjustmentBefore.get(0)[0].toString()))/100).setScale(3,BigDecimal.ROUND_HALF_UP)%> &nbsp;(In  <%=curForAdjustment[i]%>) </span><span></span>
                                          

                                     <% }
                                     }

                                    } %>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advAmount")%></td>
                                <td   class="t-align-left">
                                 <table width="100%" style="border:none;" >
                                  <tr><td style="text-align: left;border:none;padding:0px;">
                                <%
                                    if(!"".equalsIgnoreCase(AdvanceAmount))
                                    {
                                        out.print(new BigDecimal(AdvanceAmount).setScale(3,0));
                                    }else{out.print(new BigDecimal(0.0000).setScale(3,0));}
                                    
                                %></td>
                                 <td style="text-align: right;border:none;padding:0px;width:150px;">
                                  <% if(!tenderType1.equals("ICT")){%>
                                            <input type="hidden" name="AdvAmttxt" id="AdvAmtid0" <%if(!"".equalsIgnoreCase(AdvanceAmount)){%>value="<%= new BigDecimal(AdvanceAmount).setScale(3,0)%>"<%}else{%>value=<%=new BigDecimal(0.0000).setScale(3,0)%><%}%>>&nbsp;&nbsp;&nbsp;&nbsp;
                                <span id="AdvAmtpercentage0"></span><span id="AdvAmtinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                  <% } else {
                                        for(int i=0;i<totalCur;i++)
                                            {    %>
                                 <input type="hidden" name="AdvAmttxt" id="AdvAmtid<%=i%>" <%if(!"".equalsIgnoreCase(AdvanceAmount)){%>value="<%= new BigDecimal(AdvanceAmount).setScale(3,0)%>"<%}else{%>value=<%=new BigDecimal(0.0000).setScale(3,0)%><%}%>>&nbsp;&nbsp;&nbsp;&nbsp;
                                 <span id="AdvAmtpercentage<%=i%>"></span><span id="AdvAmtinbdt<%=i%>">&nbsp;&nbsp;(In  <%=curArr[i]%>)</span><br/>
                                <% }}if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                        totalCur = 0; %>
                                 </td>
                                  </tr></table>
                                </td>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjAmount")%></td>
                                <td   class="t-align-left">
                                    <input type="text" class="formTxtBox_1" name="AdvAdjuAmttxt" maxlength="12" id="AdvAdjuAmtid" <%if(isEdit){%><%if(invoicedetails.get(0).getAdvAdjAmt()!=null && !"0.000".equalsIgnoreCase(invoicedetails.get(0).getAdvAdjAmt().toString())){%> value="<%=invoicedetails.get(0).getAdvAdjAmt()%>"<%}}%> onblur="Evalute()">
                                    <br/>
                                     <% if(!tenderType1.equals("ICT")){%>
                                    <span id="AdvAdjAmtPercentage0"></span><span id="AdvAdjAmtinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                    <span id="AdvAdjAmtspan0" class="reqF_1"></span>
                                     <% } else {
                                        for(int i=0;i<totalCur;i++)
                                            {    %>
                                    <span id="AdvAdjAmtPercentage<%=i%>"></span><span id="AdvAdjAmtinbdt<%=i%>">&nbsp;&nbsp;(In  <%=curArr[i]%>)</span>
                                    <span class="reqF_1" id="AdvAdjAmtspan<%=i%>"></span>
                                    <% }}  %>
                                </td>
                            </tr>
                            <% if(salvageAmount.compareTo(BigDecimal.ZERO)>0 && !tenderType1.equals("ICT") ){ %>
                             <tr>
                                 <td class="ff" width="25%">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="ff" width="25%">Total Salvage Adjustment till Date :</td>
                                <td><%=salvageAdjAmt%>&nbsp;&nbsp;
                                    <input type="hidden" id="totalSalvageamttilldate" value="<%=salvageAdjAmt%>" >&nbsp;&nbsp;&nbsp;&nbsp;


                                    <span id="SalvageAdjpercentage0"><%= salvageAdjAmt.multiply(salvageAmount).divide(new BigDecimal(100).setScale(3,0)) %></span><span id="SalvageAdjinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                </td>
                            </tr>
                            <tr>
                                 <td class="ff" width="25%">Total Salvage Amount:</td>
                                <td><%= salvageAmount %>
                                <input type="hidden" id="SalvageAmount" name="SalvageAmount" value="<%=salvageAmount%>" >
                                </td>
                                <td class="ff" width="25%"> Salvage Adjustment :</td>
                                <td class="t-align-left">
                                    <input type="text" class="formTxtBox_1" name="SalvageAdjuAmttxt" maxlength="12" id="SalvageAdjuAmtId" <%if(isEdit){%><%if(invoicedetails.get(0).getSalvageAdjAmt()!=null && !"0.000".equalsIgnoreCase(invoicedetails.get(0).getSalvageAdjAmt().toString())){%> value="<%=invoicedetails.get(0).getSalvageAdjAmt()%>"<%}}%> onblur="Evalute()">
                                    <br/>
                                   <span id="SalvageAdjAmtPercentage0"></span><span id="SalvageAdjAmtinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                    <span id="SalvageAdjAmtspan0" class="reqF_1"></span>
                                    
                                   
                                </td>
                            </tr>
                            <% } %>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.VAT")%>
                                <td   class="t-align-left">
                                    <% if(!tenderType1.equals("ICT")){%>
                                    <input type="text" class="formTxtBox_1" name="Vattxt" maxlength="12" id="Vatid" <%if(isEdit){%><%if(invoicedetails.get(0).getVatAmt()!=null && !"0.000".equalsIgnoreCase(invoicedetails.get(0).getVatAmt().toString())){%>value="<%=invoicedetails.get(0).getVatAmt()%>"<%}}%> onblur="Evalute()">
                                    <br/><span id="vatPercentage"></span><span id="vatinbdt"></span>
                                    <span class="reqF_1" id="vatspan"></span>
                                    <% }
                                    else
                                        {
                                         if("no".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv())){
                                         List<SPTenderCommonData> supplierVat =
                                            tenderCommonService.returndata("getSupplierVatAccOfficeForICT", wpId, request.getParameter("invoiceNo"));

                                     %>
                                     <input type="text"  class="formTxtBox_1" style="display: none;" readonly name="Vattxt" maxlength="12" id="Vatid" <%if(isEdit){%><%if(invoicedetails.get(0).getVatAmt()!=null && !"0.000".equalsIgnoreCase(invoicedetails.get(0).getVatAmt().toString())){%>value="<%=invoicedetails.get(0).getVatAmt()%>"<%}}else{%>value="<%=supplierVat.get(0).getFieldName1()%><%}%>" >
                                     <span id="vatPercentage" ><%=supplierVat.get(0).getFieldName1()%></span><span id="vatinbdt"> (In Nu.)</span>
                                       <span class="reqF_1" id="vatspan"></span>
                                      
                                   <% } }  %>
                                </td>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjVAT")%></td>
                                <td   class="t-align-left">
                                    <input type="text" class="formTxtBox_1" name="AdvVatAdjAmttxt" maxlength="12" id="AdvVatAdjAmtid" <%if(isEdit){%><%if(invoicedetails.get(0).getAdvVatAmt()!=null && !"0.000".equalsIgnoreCase(invoicedetails.get(0).getAdvVatAmt().toString())){%> value="<%=invoicedetails.get(0).getAdvVatAmt()%>"<%}}%> onblur="Evalute()">
                                    <br/>
                                     <% if(!tenderType1.equals("ICT")){%>
                                    <span id="AdvVatAdjAmtPercentage0"></span><span id="AdvVatAdjAmtinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                    <span id="AdvVatAdjAmtspan0" class="reqF_1"></span>
                                      <% } else {
                                        for(int i=0;i<totalCur;i++)
                                            {    %>
                                    <span id="AdvVatAdjAmtPercentage<%=i%>"></span><span id="AdvVatAdjAmtinbdt<%=i%>">&nbsp;&nbsp;(In  <%=curArr[i]%>)</span>
                                    <span class="reqF_1" id="AdvVatAdjAmtspan<%=i%>"></span>
                                    <% }} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.AIT")%></td>

                                <td   class="t-align-left">
                                    <input type="text" class="formTxtBox_1" name="Aittxt" maxlength="12" id="Aitid" <%if(isEdit){%><%if(invoicedetails.get(0).getAitAmt()!=null && !"0.000".equalsIgnoreCase(invoicedetails.get(0).getAitAmt().toString())){%> value="<%=invoicedetails.get(0).getAitAmt()%>"<%}}%> onblur="Evalute()">
                                     <br/>
                                    <% if(!tenderType1.equals("ICT")){%>
                                    <span id="aitPercentage0"></span><span id="aitinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                    <span class="reqF_1" id="aitspan0"></span>
                                    <% } else {
                                        for(int i=0;i<totalCur;i++)
                                            {    %>
                                    <span id="aitPercentage<%=i%>"></span><span id="aitinbdt<%=i%>">&nbsp;&nbsp;(In  <%=curArr[i]%>)</span>
                                    <span class="reqF_1" id="aitspan<%=i%>"></span>
                                    <% }} %>
                                </td>



                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjAIT")%></td>
                                <td   class="t-align-left">

                                    <input type="text" class="formTxtBox_1" name="AdvAitAdjAmttxt" maxlength="12" id="AdvAitAdjAmtid" <%if(isEdit){%><%if(invoicedetails.get(0).getAdvAitAmt()!=null && !"0.000".equalsIgnoreCase(invoicedetails.get(0).getAdvAitAmt().toString())){%> value="<%=invoicedetails.get(0).getAdvAitAmt()%>"<%}}%> onblur="Evalute()">
                                    <br/>
                                     <% if(!tenderType1.equals("ICT")){%>
                                    <span id="AdvAitAdjAmtPercentage0"></span><span id="AdvAitAdjAmtinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                    <span id="AdvAitAdjAmtspan0" class="reqF_1"></span>
                                     <% } else {
                                        for(int i=0;i<totalCur;i++)
                                            {    %>
                                    <span id="AdvAitAdjAmtPercentage<%=i%>"></span><span id="AdvAitAdjAmtinbdt<%=i%>">&nbsp;&nbsp;(In  <%=curArr[i]%>)</span>
                                    <span class="reqF_1" id="AdvAitAdjAmtspan<%=i%>"></span>
                                    <% }} %>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.VATAIT")%></td>
                                <td   class="t-align-left">
                                 <% if(!tenderType1.equals("ICT")){%>
                                <span id="vatplusaitid0" style="padding-left:3px"></span><span><%=bdl.getString("CMS.Inv.InBDT")%></span>

                                <% } else {
                                        for(int i=0;i<totalCur;i++)
                                            {    %>
                                <span id="vatplusaitid<%=i%>" style="padding-left:3px"></span><span> &nbsp;&nbsp;(In  <%=curArr[i]%>)</span><br/>
                                      <% }} %>
                                </td>
                                <td   class="ff"><%=bdl.getString("CMS.Inv.advVATAIT")%>
                                </td>
                                <td   class="t-align-left">
                                  <% if(!tenderType1.equals("ICT")){%>
                                    <span id="ADVAITplusVATid0" style="padding-left:3px"></span><span><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                     <% } else {
                                        for(int i=0;i<totalCur;i++)
                                            {    %>
                                <span id="ADVAITplusVATid<%=i%>" style="padding-left:3px"></span><span> &nbsp;&nbsp;(In  <%=curArr[i]%>)</span><br/>
                                      <% }} %>
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td class="ff"><%=bdl.getString("CMS.Inv.netVATAIT")%></td>
                                <td class="t-align-left">
                                  <% if(!tenderType1.equals("ICT")){%>
                                    <span id="FinalVatandAitid0" style="padding-left:3px"></span><span><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                  <% } else {
                                        for(int i=0;i<totalCur;i++)
                                            {    %>
                                <span id="FinalVatandAitid<%=i%>" style="padding-left:3px;display:none"></span><br/>
                                      <% }} %>
                                </td>
                                <td class="ff"></td>
                                <td class="ff"></td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.liquditydamage")%></td>
                                <%
                                    int totalLiquidityAmt = 0;
                                    if(objj[2]==null)
                                    {
                                        totalLiquidityAmt = new BigDecimal(c_obj[2].toString()).intValue();
                                    }else{
                                        totalLiquidityAmt = new BigDecimal(c_obj[2].toString()).intValue() - new BigDecimal(objj[2].toString()).intValue();
                                    }
                                %>
                                <td  class="t-align-left">
                                    <input type="text" class="formTxtBox_1" name="LiqDamagetxt" maxlength="12" id="LiqDamageid" <%if(isEdit){%><%if(invoicedetails.get(0).getldAmt()!=null && !"0.000".equalsIgnoreCase(invoicedetails.get(0).getldAmt().toString())){%> value="<%=invoicedetails.get(0).getldAmt()%>"<%}}%> onblur="Evalute()">
                                    <input type="hidden" name="totalLiquidityAmt" id="totalLiquidityAmt" value="<%=new BigDecimal(totalLiquidityAmt).setScale(3,0)%>"/>
                                    <br/>
                                     <% if(!tenderType1.equals("ICT")){%>
                                    <span id="LiqDamagePercentage0"></span><span id="LiqDamageinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                    <span id="LiqDamagespan0" class="reqF_1"></span>
                                      <% } else {
                                        for(int i=0;i<totalCur;i++)
                                            {    %>
                                    <span id="LiqDamagePercentage<%=i%>"></span><span id="LiqDamageinbdt<%=i%>">&nbsp;&nbsp;(In  <%=curArr[i]%>)</span>
                                    <span id="LiqDamagespan<%=i%>" class="reqF_1"></span>
                                       <% }} %>
                                </td>
                                <td   class="ff"><%=bdl.getString("CMS.Inv.penalty")%>
                                </td>
                                <td   class="t-align-left">
                                    <input type="text" class="formTxtBox_1" name="PenaltyAmttxt" maxlength="12" id="PenaltyAmtid" <%if(isEdit){%><%if(invoicedetails.get(0).getPenaltyAmt()!=null && !"0.000".equalsIgnoreCase(invoicedetails.get(0).getPenaltyAmt().toString())){%> value="<%=invoicedetails.get(0).getPenaltyAmt()%>"<%}}%> onblur="Evalute()">
                                    <br/>
                                     <% if(!tenderType1.equals("ICT")){%>
                                    <span id="PenaltyAmtPercentage0"></span><span id="PenaltyAmtinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                    <span id="PenaltyAmtspan0" class="reqF_1"></span>
                                     <% } else {
                                        for(int i=0;i<totalCur;i++)
                                            {    %>
                                    <span id="PenaltyAmtPercentage<%=i%>"></span><span id="PenaltyAmtinbdt<%=i%>">&nbsp;&nbsp;(In  <%=curArr[i]%>)</span>
                                    <span id="PenaltyAmtspan<%=i%>" class="reqF_1"></span>
                                       <% }} %>
                                </td>
                            </tr>

                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.addition")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%">
                            <tr>
                                <td class="ff" width="26%"  class="t-align-left"><%=bdl.getString("CMS.Inv.Bonus")%></td>
                                <td   class="t-align-left">
                                    <input type="text" class="formTxtBox_1" name="Bonustxt" maxlength="12" id="bonusid" <%if(isEdit){%><%if(invoicedetails.get(0).getBonusAmt()!=null && !"0.000".equalsIgnoreCase(invoicedetails.get(0).getBonusAmt().toString())){%> value="<%=invoicedetails.get(0).getBonusAmt()%>"<%}}%> onblur="Evalute()">
                                    <br>
                                    <% if(!tenderType1.equals("ICT")){%>
                                    <span id="bonusPercentage0"></span><span id="bonusinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                    <span id="bonusspan0" class="reqF_1"></span>
                                    <% } else {
                                    for(int i=0;i<totalCur;i++)
                                        {    %>
                                    <span id="bonusPercentage<%=i%>"></span><span id="bonusinbdt<%=i%>">&nbsp;&nbsp;(In  <%=curArr[i]%>)</span>
                                    <span id="bonusspan<%=i%>" class="reqF_1"></span>
                                     <% }} %>
                                </td>
                                <td class="ff" width="26%"  class="t-align-left"><%=bdl.getString("CMS.Inv.InterestOnDP")%></td>
                                <td   class="t-align-left">
                                    <input type="text" class="formTxtBox_1" name="InterestOnDP" maxlength="12" id="InterestOnDPid" <%if(isEdit && invoicedetails.get(0).getInterestonDP()!=null && !"0.000".equalsIgnoreCase(invoicedetails.get(0).getInterestonDP().toString())){%> value="<%=invoicedetails.get(0).getInterestonDP()%>"<%}%> onblur="Evalute()">
                                    <br/>
                                      <% if(!tenderType1.equals("ICT")){%>
                                    <span id="InterestOnDPPercentage0"></span><span id="InterestOnDPinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span>
                                    <span id="IntDPspan0" class="reqF_1"></span>
                                     <% } else {
                                    for(int i=0;i<totalCur;i++)
                                        {    %>
                                     <span id="InterestOnDPPercentage<%=i%>"></span><span id="InterestOnDPinbdt<%=i%>">&nbsp;&nbsp;(In  <%=curArr[i]%>)</span>
                                    <span id="IntDPspan<%=i%>" class="reqF_1"></span>
                                     <% }} %>
                                </td>
                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.retention")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%">
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.retentionmoneytilldate")%></td>
                                <td   class="t-align-left">
                                <%
                                    List<Object[]> totalRDeducted = accPaymentService.getAllTotalInvoiceAmtForWp(Integer.parseInt(wpId));
                                    Object[] obj = null;
                                    if(!totalRDeducted.isEmpty())
                                    {
                                        obj = totalRDeducted.get(0);
                                        if(obj[2]==null)
                                        {
                                            out.print(new BigDecimal(0).setScale(3,0));
                                        }
                                        else{
                                        out.print(new BigDecimal(obj[2].toString()).setScale(3,0));
                                        }
                                    }
                                %>
                                <input type="hidden" id="RMTD" <%if(obj[2]==null){%>value="0"<%}else{%>value="<%=obj[2].toString()%>"<%}%>>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="RMTDpercentage" style="padding-left:40px"></span>&nbsp;&nbsp;<span id="RMTDinbdt"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.retentionmoney")%></td>
                                <td   class="t-align-left">
                                    <span class="formBtn_1"><input type="button" name="Retensionbtn" id="Plusid" value="+" onClick="RetensionPlusEvalute()" /></span>&nbsp;
                                    <span class="formBtn_1"><input type="button" name="Retensionbtn" id="Minusid" value="-" onclick="RetensionMinusEvalute()" /></span>&nbsp;
                                    <input type="text" class="formTxtBox_1" name="Retensiotxt" maxlength="12" id="retensionid" <%if(isEdit){if(invoicedetails.get(0).getRetentionMadd()!=null){%>value="<%=invoicedetails.get(0).getRetentionMadd()%>"<%}else if(invoicedetails.get(0).getRetentionMdeduct()!=null){%>value="<%=invoicedetails.get(0).getRetentionMdeduct()%>"<%}}%> >
                                    <input type="hidden" name="hiddenRetensionaction" id="hiddenRetensionactionid" value="">
                                    <br/>
                                    <% if(!tenderType1.equals("ICT")){%>
                                    <span id="retensionPercentage0" style="padding-left: 70px;"></span><span id="retensioninbdt0"></span>
                                    <span id="retensionspan0"></span>
                                    <span id="retensionmoneyspan0" class="reqF_1"></span>
                                      <% } else {
                                    for(int i=0;i<totalCur;i++)
                                        {    %>
                                    <span id="retensionPercentage<%=i%>" style="padding-left: 70px;"></span><span id="retensioninbdt<%=i%>">&nbsp;&nbsp;(In  <%=curArr[i]%>)</span>
                                    <span id="retensionspan<%=i%>"></span>
                                    <span id="retensionmoneyspan<%=i%>" class="reqF_1"></span>
                                        <% }} %>
                                </td>
                            </tr>
                            <tr> <td colspan="2"><ol style="padding-right: 5px;margin-left: 20px;"><li>Retention money at a percentage as specified in Schedule II will be
                                         deductable from each bill due to a Contractor until completion of the whole Works or delivery.</li>
                                     <li>On completion of the whole Works, half the total amount retained shall be repaid to
                                         the Contractor and the remaining amount may also be paid to the Contractor if an unconditional
                                         Bank guarantee is furnished for that remaining amount.</li>
                                     <li>The remaining amount or the Bank guarantee, under Sub-Rule (2), shall be returned,
                                         within the period specified in schedule II , after issuance of all Defects Correction Certificate
                                         under Rule 39(29) by the Project Manager or any other appropriate Authority ..</li>
                                     <li>Deduction of retention money shall not be applied to small Works Contracts if no
                                         advance payment has been made to the Contractor and in such case the provisions of Sub-Rule
                                         (7) and (8) of Rule 27 shall be applied.</li></ol>
                                 </td>
                            </tr>
                        </table>
                                    <br />
                                <table class="tableList_1" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="25%" class="ff">
                                            <%=strProcNature%> Bank Details
                                        </td>
                                        <td class="ff">
                                           <a onclick="javascript:window.open('ViewAccInfo.jsp?id=<%=id%>', '', 'width=800px,height=300px,scrollbars=1','');" href="javascript:void(0);">View</a>
                                        </td>
                                    </tr>
                                </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails")%></div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%> <span class="mandatory">*</span></td>
                            <td   class="t-align-left">
                                <select class="formTxtBox_1" name="modeofPayment" id="modeofPaymentid" onblur="blockMessage('modeofPaymentid','modeofPaymentspan')">
                                  <option value="select" selected>-Select-</option>
                                  <option value="cheque" <%if(isEdit){if("cheque".equalsIgnoreCase(invoicedetails.get(0).getModeOfPayment())){%>selected<%}}%>>Cheque</option>
                                  <option value="dd" <%if(isEdit){if("dd".equalsIgnoreCase(invoicedetails.get(0).getModeOfPayment())){%>selected<%}}%>>DD</option>
<!--                                  <option value="payorder" <%if(isEdit){if("payorder".equalsIgnoreCase(invoicedetails.get(0).getModeOfPayment())){%>selected<%}}%>>Pay Order</option>-->
                                  <option value="acctoacctsfr" <%if(isEdit){if("acctoacctsfr".equalsIgnoreCase(invoicedetails.get(0).getModeOfPayment())){%>selected<%}}%>>Account to Account Transfer</option>
                                </select>
                                <span id="modeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%> <span class="mandatory">*</span></td>
                                <td   class="t-align-left">
                                    <%
                                        Date editDate = null;
                                        if(isEdit){
                                            editDate =  DateUtils.convertStringtoDate(invoicedetails.get(0).getDateOfPayment().toString(),"yyyy-MM-dd HH:mm:ss");
                                        }
                                    %>
                                    <input name="Paymentdate" type="text" <%if(isEdit){%> value="<%=DateUtils.formatStdDate(editDate)%>"<%}%> class="formTxtBox_1" id="PstartDate" style="width:100px;" onmouseover="blockMessage('PstartDate','DateofPaymentspan');" onfocus="GetCal('PstartDate','PstartDate');"/>
                                    <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtCstartDate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('PstartDate','txtCstartDate');"/></a>
                                    <span id="DateofPaymentspan" class="reqF_1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%> <span class="mandatory">*</span></td>
                                <td   class="t-align-left">
                                <input type="text" class="formTxtBox_1" name="BankNametxt" id="BankNametxtid" <%if(isEdit){%> value="<%=invoicedetails.get(0).getBankName()%>"<%}%> onblur="blockMessage('BankNametxtid','banknamespan')">
                                <span id="banknamespan" class="reqF_1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%> <span class="mandatory">*</span></td>
                                <td   class="t-align-left">
                                <input type="text" class="formTxtBox_1" name="BranchNametxt" id="BranchNametxtid" <%if(isEdit){%> value="<%=invoicedetails.get(0).getBranchName()%>"<%}%> onblur="blockMessage('BranchNametxtid','branchnamespan')">
                                <span id="branchnamespan" class="reqF_1"></span>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%> <span class="mandatory">*</span></td>
                                <td   class="t-align-left">
                                <input type="text" class="formTxtBox_1" name="InstNumbertxt" id="InstNumbertxtid" <%if(isEdit){%> value="<%=invoicedetails.get(0).getInstrumentNo()%>"<%}%> onblur="blockMessage('InstNumbertxtid','instrumentspan')">
                                <span id="instrumentspan" class="reqF_1"></span>
                                </td>
                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails.vat")%></div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%> <span class="mandatory"></span></td>
                            <td   class="t-align-left">
                                <select class="formTxtBox_1" name="vatmodeofPayment" id="vatmodeofPaymentid" onblur="blockMessage('vatmodeofPaymentid','vatmodeofPaymentspan')">
                                  <option value="select" selected>-Select-</option>
                                  <option value="cheque" <%if(isEdit){if("cheque".equalsIgnoreCase(invoicedetails.get(0).getVatmodeOfPayment())){%>selected<%}}%>>Cheque</option>
                                  <option value="dd" <%if(isEdit){if("dd".equalsIgnoreCase(invoicedetails.get(0).getVatmodeOfPayment())){%>selected<%}}%>>DD</option>
<!--                                  <option value="payorder" <%if(isEdit){if("payorder".equalsIgnoreCase(invoicedetails.get(0).getVatmodeOfPayment())){%>selected<%}}%>>Pay Order</option>-->
                                  <option value="acctoacctsfr" <%if(isEdit){if("acctoacctsfr".equalsIgnoreCase(invoicedetails.get(0).getVatmodeOfPayment())){%>selected<%}}%>>Account to Account Transfer</option>
                                </select>
                                <span id="vatmodeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%> <span class="mandatory"></span></td>
                                <td   class="t-align-left">
                                    <%
                                        Date vateditDate = null;
                                        if(isEdit){
                                            if(invoicedetails.get(0).getVatdateOfPayment()!=null){
                                                vateditDate =  DateUtils.convertStringtoDate(invoicedetails.get(0).getVatdateOfPayment().toString(),"yyyy-MM-dd HH:mm:ss");
                                            }
                                        }
                                    %>
                                    <input name="vatPaymentdate" type="text" <%if(isEdit){if(vateditDate!=null){%> value="<%=DateUtils.formatStdDate(vateditDate)%>"<%}}%> class="formTxtBox_1" id="vatPstartDate" style="width:100px;" onmouseover="blockMessage('vatPstartDate','vatDateofPaymentspan');" onfocus="GetCal('vatPstartDate','vatPstartDate');"/>
                                    <a href="javascript:void(0);" onclick="" title="Calender"><img id="vattxtCstartDate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('vatPstartDate','vattxtCstartDate');"/></a>
                                    <span id="vatDateofPaymentspan" class="reqF_1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%> <span class="mandatory"></span></td>
                                <td   class="t-align-left">
                                <input type="text" class="formTxtBox_1" name="vatBankNametxt" id="vatBankNametxtid" <%if(isEdit){if(invoicedetails.get(0).getVatbankName()!=null){%> value="<%=invoicedetails.get(0).getVatbankName()%>"<%}}%> onblur="blockMessage('vatBankNametxtid','vatbanknamespan')">
                                <span id="vatbanknamespan" class="reqF_1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%> <span class="mandatory"></span></td>
                                <td   class="t-align-left">
                                <input type="text" class="formTxtBox_1" name="vatBranchNametxt" id="vatBranchNametxtid" <%if(isEdit){if(invoicedetails.get(0).getVatbranchName()!=null){%> value="<%=invoicedetails.get(0).getVatbranchName()%>"<%}}%> onblur="blockMessage('vatBranchNametxtid','vatbranchnamespan')">
                                <span id="vatbranchnamespan" class="reqF_1"></span>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%> <span class="mandatory"></span></td>
                                <td   class="t-align-left">
                                <input type="text" class="formTxtBox_1" name="vatInstNumbertxt" id="vatInstNumbertxtid" <%if(isEdit){if(invoicedetails.get(0).getVatinstrumentNo()!=null){%> value="<%=invoicedetails.get(0).getVatinstrumentNo()%>"<%}}%> onblur="blockMessage('vatInstNumbertxtid','vatinstrumentspan')">
                                <span id="vatinstrumentspan" class="reqF_1"></span>
                                </td>
                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails.ait")%></div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%> <span class="mandatory"></span></td>
                            <td   class="t-align-left">
                                <select class="formTxtBox_1" name="aitmodeofPayment" id="aitmodeofPaymentid" onblur="blockMessage('aitmodeofPaymentid','aitmodeofPaymentspan')">
                                  <option value="select" selected>-Select-</option>
                                  <option value="cheque" <%if(isEdit){if("cheque".equalsIgnoreCase(invoicedetails.get(0).getAitmodeOfPayment())){%>selected<%}}%>>Cheque</option>
                                  <option value="dd" <%if(isEdit){if("dd".equalsIgnoreCase(invoicedetails.get(0).getAitmodeOfPayment())){%>selected<%}}%>>DD</option>
<!--                                  <option value="payorder" <%if(isEdit){if("payorder".equalsIgnoreCase(invoicedetails.get(0).getAitmodeOfPayment())){%>selected<%}}%>>Pay Order</option>-->
                                  <option value="acctoacctsfr" <%if(isEdit){if("acctoacctsfr".equalsIgnoreCase(invoicedetails.get(0).getAitmodeOfPayment())){%>selected<%}}%>>Account to Account Transfer</option>
                                </select>
                                <span id="aitmodeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%> <span class="mandatory"></span></td>
                                <td   class="t-align-left">
                                    <%
                                        Date aiteditDate = null;
                                        if(isEdit){
                                            if(invoicedetails.get(0).getAitdateOfPayment()!=null){
                                                aiteditDate =  DateUtils.convertStringtoDate(invoicedetails.get(0).getAitdateOfPayment().toString(),"yyyy-MM-dd HH:mm:ss");
                                            }
                                        }
                                    %>
                                    <input name="aitPaymentdate" type="text" <%if(isEdit){if(aiteditDate!=null){%> value="<%=DateUtils.formatStdDate(aiteditDate)%>"<%}}%> class="formTxtBox_1" id="aitPstartDate" style="width:100px;" onmouseover="blockMessage('aitPstartDate','aitDateofPaymentspan');" onfocus="GetCal('aitPstartDate','aitPstartDate');"/>
                                    <a href="javascript:void(0);" onclick="" title="Calender"><img id="aittxtCstartDate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('aitPstartDate','aittxtCstartDate');"/></a>
                                    <span id="aitDateofPaymentspan" class="reqF_1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%> <span class="mandatory"></span></td>
                                <td   class="t-align-left">
                                <input type="text" class="formTxtBox_1" name="aitBankNametxt" id="aitBankNametxtid" <%if(isEdit){if(invoicedetails.get(0).getAitbankName()!=null){%> value="<%=invoicedetails.get(0).getAitbankName()%>"<%}}%> onblur="blockMessage('aitBankNametxtid','aitbanknamespan')">
                                <span id="aitbanknamespan" class="reqF_1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%> <span class="mandatory"></span></td>
                                <td   class="t-align-left">
                                <input type="text" class="formTxtBox_1" name="aitBranchNametxt" id="aitBranchNametxtid" <%if(isEdit){if(invoicedetails.get(0).getAitbranchName()!=null){%> value="<%=invoicedetails.get(0).getAitbranchName()%>"<%}}%> onblur="blockMessage('aitBranchNametxtid','aitbranchnamespan')">
                                <span id="aitbranchnamespan" class="reqF_1"></span>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%> <span class="mandatory"></span></td>
                                <td   class="t-align-left">
                                <input type="text" class="formTxtBox_1" name="aitInstNumbertxt" id="aitInstNumbertxtid" <%if(isEdit){if(invoicedetails.get(0).getAitinstrumentNo()!=null){%> value="<%=invoicedetails.get(0).getAitinstrumentNo()%>"<%}}%> onblur="blockMessage('aitInstNumbertxtid','aitinstrumentspan')">
                                <span id="aitinstrumentspan" class="reqF_1"></span>
                                </td>
                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.gross")%></div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                         <% System.out.println("totalCurForGross"+totalCurForGross);%>
                        <%for(int i = 0;i<totalCurForGross;i++){ %>
                        <tr>
                                <td class="ff" bgcolor="#dddddd">Total Amount Deducted (In <%=curArr[i]%>) :</td>
                                <td><span id="totalDeductedAmt<%=i%>"></span></td>
                        </tr>
                        <tr>
                                <td class="ff" bgcolor="#dddddd">Total Amount Added (In <%=curArr[i]%>) :</td>
                                <td><span id="totalAddedAmt<%=i%>"></span></td>
                        </tr>
                        <tr>
                            <td class="ff" width="25%"  class="t-align-left">Net Amount (In <%=curArr[i]%>)</td>
                            <td   class="t-align-left">
                               
                                

                                <input type="text" style="border:3px solid Green;"class="formTxtBox_1" readonly name="GrossAmttxt<%=i%>" id="GrossAmtid<%=i%>" value="<%=totalArr[i]%>"<%//}%>>
                                &nbsp;&nbsp;&nbsp;<span name="GrossWords" id="GrossinWords<%=i%>" class="ff"></span>
                            </td>
                        </tr>
                        <% } %>
                        </table>
                        <table class="t_space" cellspacing="0" width="100%">
                            <tr>
                                <td class="t-align-center">
                                    <%
                                       if(isEdit){
                                    %>
                                    <span class="formBtn_1"><input type="submit" id="SavePaymentid"name="SavePaymentname" value="Update"onclick=" return validate()"/></span>&nbsp;
                                    <%}else{%>
                                    <span class="formBtn_1"><input type="submit" id="SavePaymentid"name="SavePaymentname" value="Save As Draft"onclick=" return validate()"/></span>&nbsp;
                                    <%}%>
                                </td>
                            </tr>
                        </table>
                      </form>
                    </div>
                </div></div>
            </div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMakePay");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        //  var a =document.getElementsByName("amtWords");
            var a =  document.getElementById("totalCurrency").value;
         
         for(var j = 0;j<a;j++){
         if(document.getElementById("Contarctvalue"+j).value!="")
            {
            
          var num1 = ((document.getElementById("Contarctvalue"+j).value*(document.getElementById("AdvAmtid"+j).value))/(100));
             document.getElementById("AdvAmtpercentage"+j).innerHTML = num1.toFixed(3);
              var num2 = ((document.getElementById("AdvAmtpercentage"+j).innerHTML*(document.getElementById("totaladvamttilldate").value))/(100));
             document.getElementById("tAATDpercentage"+j).innerHTML = num2.toFixed(3);
            // document.getElementById("tAATDinbdt").innerHTML = '&nbsp&nbsp'+'<%//=bdl.getString("CMS.Inv.InBDT")%>';
             var num3 = ((document.getElementById("invoiceAmtid"+j).value*(document.getElementById("RMTD").value))/(100));
             document.getElementById("RMTDpercentage").innerHTML = num3.toFixed(3);
             document.getElementById("RMTDinbdt").innerHTML = '&nbsp&nbsp'+'<%=bdl.getString("CMS.Inv.InBDT")%>';
           }
           }
</script>
       <%
            if(isEdit)
            {
                if(invoicedetails.get(0).getRetentionMadd()!=null)
                {
       %>
                <script type="text/javascript">
                    RetensionPlusEvalute();
                </script>
       <%       }else if(invoicedetails.get(0).getRetentionMdeduct()!=null)
                {
       %>       <script type="text/javascript">
                    RetensionMinusEvalute();
                </script>
       <%       }else{
       %>
                <script type="text/javascript">
                    Evalute();
                </script>
       <%
                }
            }
       %>
</html>
