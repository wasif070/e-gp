<%-- 
    Document   : deBriefQueryView
    Created on : Jun 16, 2011, 4:01:32 PM
    Author     : dixit
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View Query and Reply</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
</head>
    <body onload="getQueryData('replyQuery')">

<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <%@include  file="../resources/common/AfterLoginTop.jsp" %>

<div class="contentArea_1">
      <div class="pageHead_1">View Query and Reply
          <span style="float:right;">
              <a href="EvalComm.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go back</a>
          </span>
      </div>
      <%

            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            //String tenderID ="";
            //if(request.getParameter("tenderId")!=null)
            //{
                //tenderID = request.getParameter("tenderId");
            //}
            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
            String message ="";
            if(request.getParameter("message")!=null)
            {
                message = request.getParameter("message");
            }
            
            // Coad added by Dipal for Audit Trail Log.
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                String idType="tenderId";
                int auditId=Integer.parseInt(request.getParameter("tenderId"));
                String auditAction="View Debriefing on Tender";
                String moduleName=EgpModule.Evaluation.getName();
                String remarks="";
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
      %>
      <%@include file="../resources/common/TenderInfoBar.jsp" %>
      <div>&nbsp;</div>
      <% pageContext.setAttribute("tab","7"); %>
      <%@include file="officerTabPanel.jsp"%>
      <div>&nbsp;</div> <div>&nbsp;</div>      
      <%
            if("success".equalsIgnoreCase(message))
            {
      %>
                <div class="responseMsg successMsg t_space"><span>Your Reply Posted Successfully</span></div>
      <%
            }
            if("fail".equalsIgnoreCase(message))
            {
      %>
                <div class="responseMsg errorMsg t_space"><span>Your Reply is not Posted Successfully</span></div>
      <%
            }
      %>
      <%--<%pageContext.setAttribute("tab", "6");%>--%>      
      <table width="100%" cellspacing="0" class="tableList_1">      
      <tr>
          <td><div id ="queryData"></div></td>
      </tr>
      </table>
      </div>
      </div>
      <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
</body>
<script type="text/javascript">
function getQueryData(val){
       $.ajax({
        url: "<%=request.getContextPath()%>/deBriefQueryServlet?tenderId=<%=request.getParameter("tenderId")%>&action=getQuestionData&queryAction="+val,
        method: 'POST',
        async: false,
        cache : false,
        success: function(j) {
            document.getElementById("queryData").innerHTML = j;
        }
     });
}
</script>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>
</html>
