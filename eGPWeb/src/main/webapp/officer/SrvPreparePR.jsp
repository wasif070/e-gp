<%-- 
    Document   : srvPreparePR
    Created on : Dec 13, 2011, 2:05:22 PM
    Author     : shreyansh Jogi
--%>

<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    String isEdit = "";
                    if ("y".equalsIgnoreCase(request.getParameter("isedit"))) {
                        isEdit = request.getParameter("isedit");
                    }
                    ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Prepare Progress Report</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script language="javascript">

            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function getDate(date)
            {
                var splitedDate = date.split("-");
                return Date.parse(splitedDate[1]+" "+splitedDate[0]+", "+splitedDate[2]);
            }
            function checkDate(dateofdlvry){


                var firstDate = getDate(document.getElementById("firstdate").value);
                var secondDate = getDate(document.getElementById("seconddate").value);
                var dod = getDate(document.getElementById(dateofdlvry).value);
                if((new Date(firstDate).getTime() <= new Date(dod).getTime()) && (new Date(dod).getTime() <=  new Date(secondDate).getTime())){

                }else{
                    jAlert("<%=bdl.getString("CMS.PR.datebetween")%> ","Progress Report", function(RetVal) {
                    });
                    document.getElementById(dateofdlvry).value="";
                }
            }

            function checkZero(id){

                        var qty = document.getElementById('Qtyenter_'+id).value;
                        if(qty.indexOf('0') == 0 && qty.indexOf('.') != 1 && eval('qty') != 0){
                        jAlert("Please remove all leading zero(s)","Progress Report", function(RetVal) {
                            });
                             document.getElementById('Qtyenter_'+id).value="";
                    }else{
                        return true;
                    }
            }

            function checkPending(id)
            {
                if(checkZero(id)){
                    var qty = eval(document.getElementById("qty_"+id).value);
                var vbool = true;
                $('.err').remove();
                if(!regForNumber(document.getElementById('Qtyenter_'+id).value)){
                    $("#Qtyenter_"+id).parent().append("<div class='err' style='color:red;'><%=bdl.getString("CMS.PR.qtynumeric")%></div>");
                    vbool = false;
                    document.getElementById('Qtyenter_'+id).value="";
                }else if(document.getElementById('Qtyenter_'+id).value.split(".")[1] != undefined){
                    if(!regForNumber(document.getElementById('Qtyenter_'+id).value) || document.getElementById('Qtyenter_'+id).value.split(".")[1].length > 3 || document.getElementById('Qtyenter_'+id).value.split(".")[0].length > 12){
                        $("#Qtyenter_"+id).parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                        document.getElementById('Qtyenter_'+id).value = "";
                        vbool = false;
                    }else{
                        if((eval(document.getElementById('Qtyenter_'+id).value) + eval(document.getElementById('Qtyaccepted_'+id).value))>qty){
                            jAlert("<%=bdl.getString("CMS.PR.qtygretertotalqty")%> ","Progress Report", function(RetVal) {
                            });
                            document.getElementById('Qtyenter_'+id).value = "";
                        }else{
                            var fqty= qty - eval(document.getElementById('Qtyenter_'+id).value) - eval(document.getElementById('Qtyaccepted_'+id).value);
                            var qtty = Math.round(fqty*Math.pow(10,3))/Math.pow(10,3)+"";
                            if(qtty.indexOf(".") < 0){
                                qtty = qtty+".000"
                            }
                            document.getElementById('Qtypending_'+id).innerHTML = qtty;
                        }
                    }
                }else if(document.getElementById('Qtyenter_'+id).value.indexOf("-")!=-1){
                    $("#Qtyenter_"+id).parent().append("<div class='err' style='color:red;'>Only positive values are allowed</div>");
                    document.getElementById('Qtyenter_'+id).value = "";
                    vbool = false;
                }else if(eval(document.getElementById('Qtyenter_'+id).value)>qty){
                    $("#Qtyenter_"+id).parent().append("<div class='err' style='color:red;'><%=bdl.getString("CMS.PR.qtygretertotalqty")%></div>");
                    document.getElementById('Qtyenter_'+id).value = "";
                    vbool = false;
                }else if (eval(document.getElementById('Qtyenter_'+id).value)==0){
                    $("#Qtyenter_"+id).parent().append("<div class='err' style='color:red;'><%=bdl.getString("CMS.PR.qtygreter0")%></div>");
                    document.getElementById('Qtyenter_'+id).value = "";
                    vbool = false;
                }else {
                    if((eval(document.getElementById('Qtyenter_'+id).value) + eval(document.getElementById('Qtyaccepted_'+id).value))>qty){
                        jAlert("<%=bdl.getString("CMS.PR.qtygretertotalqty")%> ","Progress Report", function(RetVal) {
                        });
                        document.getElementById('Qtyenter_'+id).value = "";
                    }else{
                        var fqty= qty - eval(document.getElementById('Qtyenter_'+id).value) - eval(document.getElementById('Qtyaccepted_'+id).value);
                        var qtty = Math.round(fqty*Math.pow(10,3))/Math.pow(10,3)+"";
                        if(qtty.indexOf(".") < 0){
                            qtty = qtty+".000"
                        }
                        document.getElementById('Qtypending_'+id).innerHTML = qtty;
                    }
                    }
                }
            }
            function saveAsDraft()
            {
                document.getElementById("action").value = "saveAsDreaft";
                document.forms["prfrm"].submit();

            }
           function checkEnter(e){
            var keyValue = (window.event)? e.keyCode : e.which;
                if(keyValue==13){
                    saveForm();
                }
            }

            function saveForm()
            {
                $('.err').remove();
                var count = document.getElementById("listcount").value;
                var vbool = true;
                var qty = false;
                var dd = false;
                var remarks = false;
                var flag = false;
                var totalcheck = true;
                var nodata = 0;
                var flag = true;
                for(var i=0;i<count;i++){

                    if(document.getElementById('Qtyenter_'+i).value=="" && document.getElementById('txtdate_'+i).value==""){
                    nodata++;
                    }
                    else if(document.getElementById('Qtyenter_'+i).value=="" && document.getElementById('txtdate_'+i).value!=""){
                        qty = false;
                        dd = false;
                        jAlert("It is mandatory to enter Quantity where date is selected","Progress Report", function(RetVal) {
                    });
                        break;
                    }
                    else if(document.getElementById('Qtyenter_'+i).value!="" && document.getElementById('txtdate_'+i).value==""){
                        qty = false;
                        dd = false;
                        jAlert("It is mandatory to select date where quantity has entered","Progress Report", function(RetVal) {
                    });
                        break;
                    }else{
                        qty = true;
                        dd = true;
                    }


                }

                if(qty && dd){
                     document.getElementById("action").value = "save";
                    document.getElementById("save").value = "frm";
                    document.forms["prfrm"].submit();
                }
                else if(nodata==count){
                 flag = false;
                jAlert("It is mandatory to enter data in atleast one row","Progress Report", function(RetVal) {
                    });

                }


            }


            function GetCal(txtname,controlname,id)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d-%b-%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        checkDate(txtname);
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })

            }
        </script>
        <script type="text/javascript">
            /* check if pageNO is disable or not */
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            /*  load Grid for the User Registration Report */
            function loadTable()
            {
                $('#tohide').hide();
                if($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/CMSSerCaseServlet", {tenderId: $("#tenderId").val(),size: $("#size").val(),pageNo: $("#first").val(),action:'preparepr',wpId: $("#wpId").val(),isedit:'<%=isEdit%>'},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            /*  Handle First click event */
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    $('#first').val(0);
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Last Button click event */
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var size = (parseInt($('#size').val())*totalPages)-(parseInt($('#size').val()));
                    $('#first').val(size);
                    if(totalPages>0){
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Next Button click event */
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val());
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first+max);

                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo <= totalPages) {

                        loadTable();
                        $('#pageNo').val(Number(pageNo)+1);
                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            /*  Handle Previous click event */
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first-max);
                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    $('#pageNo').val(Number(pageNo)-1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)-1);
                });
            });
        </script>
        <script type="text/javascript">
            function Search(){
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            }
            function calledonsubmit(){
               alert("called");
                return false;
            }
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var size = (parseInt($('#size').val())*pageNo)-$('#size').val();
                    $('#first').val(size);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }

                });
            });
            function changetable(){
                var pageNo=parseInt($('#pageNo').val(),10);
                var first = parseInt($('#first').val());
                var max = parseInt($('#size').val());
                var totalPages=parseInt($('#totalPages').val(),10);
                loadTable();

            }

        </script>

        <%
                    int userid = 0;
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userid = Integer.parseInt(hs.getAttribute("userId").toString());
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                    String lotId = request.getParameter("lotId");
                    CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                    List<Object[]> list = cs.getLotDetailsByPkgLotId(lotId, request.getParameter("tenderId"));
        %>
    </head>
    <body>
        <% if (request.getParameter("msg") != null) {
        %>
        <% if (request.getParameter("msg").equals("edit")) {%>

        <script>
            jAlert("<%=bdl.getString("CMS.dates.edit")%> ","Progress Report", function(RetVal) {
            });

        </script>
        <% }

                    }%>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">Prepare Progress Report
                        <span class="c-alignment-right"><a href="ProgressReportMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>

                    <%
                                pageContext.setAttribute("TSCtab", "2");
                                if (request.getParameter("lotId") != null) {
                                    pageContext.setAttribute("lotId", request.getParameter("lotId"));
                                    lotId = request.getParameter("lotId");
                                }

                    %>
                    <div>&nbsp;</div>
                    <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                    <div>&nbsp;</div>
                    <%
                                pageContext.setAttribute("tab", "14");

                    %>
                    <%@include  file="officerTabPanel.jsp"%>
                    <div class="tabPanelArea_1">


                        <%@include  file="../resources/common/CMSTab.jsp"%>
                        <div>&nbsp;</div>


                        <div class="tabPanelArea_1">
                            <form name="prfrm" id="prfrm"  method="post" action="<%=request.getContextPath()%>/ConsolidateServlet">

                                <%if (request.getParameter("isedit") != null) {%>
                                <input type="hidden" name="edit" value="edit" />
                                <input type="hidden" name="isEdit" value="<%=isEdit%>" />
                                <%}%>
                                <%if (request.getParameter("frmSrv") != null) {%>
                                <input type="hidden" name="frmSrv" value="frmSrv" />
                                <%}%>
                                <input type="hidden" name="action" id="action" value="" />
                                <input type="hidden" name="firstdate" id="firstdate" value="<%=DateUtils.customDateFormate(DateUtils.convertStringtoDate(c_obj[3].toString(), "yyyy-MM-dd HH:mm:ss"))%>" />
                                <input type="hidden" name="seconddate" id="seconddate" value="<%=DateUtils.customDateFormate(new Date())%>" />
                                <input type="hidden" name="save" id="save" value="" />
                                <input type="hidden" name="tenderId" value="<%=request.getParameter("tenderId")%>" />
                                <input type="hidden" name="lotId" value="<%=request.getParameter("lotId")%>" />
                                <%
                                            String strLotNo = "Lot No.";
                                            String strLotDes = "Lot Description";
                                            if("Services".equalsIgnoreCase(procnature))
                                            {
                                                strLotNo = "Package No.";
                                                strLotDes = "Package Description";                                                
                                            }else if("goods".equalsIgnoreCase(procnature)){
                                            }else{
                                            }
                                            String tenderId = request.getParameter("tenderId");
                                            boolean flag = true;
                                            CommonService commonService_pr = (CommonService) AppContext.getSpringBean("CommonService");
                                            List<Object[]> list1 = commonService_pr.getLotDetailsByPkgLotId(request.getParameter("lotId"), tenderId);
                                            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                            int i = 0;
                                            for (Object[] lotList : list1) {
                                %>

                                <% if (request.getParameter("msg") != null) {
        %>
       <% if ("endsave".equalsIgnoreCase(request.getParameter("msg"))) {%>
                        <div class='responseMsg successMsg'><%=bdl.getString("CMS.PR.report.save")%></div>

                        <%}

                    }%>



                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                    <tr>
                                        <td width="20%"><%=strLotNo%></td>
                                        <td width="80%"><%=lotList[0].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td><%=strLotDes%></td>
                                        <td class="t-align-left"><%=lotList[1].toString()%></td>
                                    </tr>
                                </table>
                                    <br />

                                <div  class='responseMsg noticeMsg t-align-left'>Remarks is not mandatory</div>
                                <br />
                                <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>" />
                                <input type="hidden" name="wpId" id="wpId" value="<%=request.getParameter("wpId")%>" />
                                <input type="hidden" name="action" value="preparepr" />
                                <div id="resultDiv" style="display: block;">
                                    <div  id="print_area">
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                               
                                                                                               
                                                <th width="3%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="20%" class="t-align-center"><%=bdl.getString("CMS.desc")%></th>
                                                <th width="15%" class="t-align-center"><%=bdl.getString("CMS.UOM")%>
                                                </th>
                                                <th width="9%" class="t-align-center"><%=bdl.getString("CMS.qty")%>
                                                </th>
                                                <th width="5%" class="t-align-center"><%=bdl.getString("CMS.PR.qtyacceptedtillpr")%>
                                                </th>
                                                <th width="18%" class="t-align-center"><%=bdl.getString("CMS.PR.qtyaccepted")%>
                                                </th>
                                                <th width="5%" class="t-align-center"><%=bdl.getString("CMS.PR.qtypending")%>
                                                </th>
                                                <th width="20%" class="t-align-center"><%=bdl.getString("CMS.PR.dod")%>
                                                </th>
                                                <th width="30%" class="t-align-center"><%=bdl.getString("CMS.PR.remarks")%>
                                                </th>
                                               
                                            </tr>
                                        </table>
                                        <% }%>
                                    </div>
                                </div>
                                <div id="tohide">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                        <tr>
                                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                Total Records per page : <select name="size" id="size" onchange="changetable();" style="width:40px;">
                                                    <option value="1000" selected>10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>

                                                </select>
                                            </td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" onKeydown="javascript: if (event.keyCode==13) Search();" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1">
                                                    <input type="submit" name="button"  id="btnGoto" value="Go To Page" />
                                                </label></td>
                                            <td  class="prevNext-container">
                                                <ul>
                                                    <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                    <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <input type="hidden" id="pageNo" value="1"/>
                                <input type="hidden" id="first" value="0"/>
                                <br />
                                <%//if (!c_isPhysicalPrComplete) {%>
                                <%
                                    //boolean checkitemreceived = service.checkForItemFullyReceivedOrNotByWpId(Integer.parseInt(request.getParameter("wpId")));
                                    //if (!checkitemreceived) {
                                %>
                               
                                <%if(!"services".equalsIgnoreCase(procnature)){%>
                                 <div  class='responseMsg noticeMsg t-align-left'><%=bdl.getString("CMS.PR.finalisePR")%></div>
                                <br />
                                <div>
                                    <input type="checkbox" name="chkids"onclick="msgOfQualityChk(this);" />&nbsp;&nbsp;&nbsp;<b> Quality Checked </b>
                                </div>
                                <%}%>
                                <br />
                                <div  class='responseMsg noticeMsg t-align-left'><%=bdl.getString("CMS.PR.caneditpr")%></div>
                                <br />

                                <center>
                                    <label class="formBtn_1">
                                        <input type="button" name="Boqbutton" id="Boqbutton" value="Save as Draft" onclick="saveAsDraft();" />
                                    </label>
                                    &nbsp;
                                    <%if(!"services".equalsIgnoreCase(procnature)){%>
                                    <label class="formBtn_1" style="visibility: hidden" id="lbl_finalizeBtn">
                                        <input type="button" name="finalizeBtn" id="finalizeBtn" value="Finalize Progress Report" onclick="saveForm();" />
                                    </label>
                                    <%}else{%>
                                    <label class="formBtn_1" id="lbl_finalizeBtn">
                                        <input type="button" name="finalizeBtn" id="finalizeBtn" value="Finalize Progress Report" onclick="saveForm();" />
                                    </label>
                                    <%}%>

                                </center>
                                <%//}
                                           // }%>
                            </form>
                        </div>
                    </div>
                    <!--Dashboard Content Part End-->
                </div>
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
        </div>

    </body>

    <script>
        var headSel_Obj = document.getElementById("headTabReport");
        loadTable();
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

        function msgOfQualityChk(obj){
            var finalizeObj = document.getElementById("lbl_finalizeBtn");
            if(obj.checked){
                jConfirm("Are you sure Quality Check has been done?","Quality Checked", function(RetVal){
                    if(RetVal)
                    {
                        finalizeObj.style.visibility = "visible";
                    }else{document.prfrm.chkids.checked= false;}
                });
            }else{
                finalizeObj.style.visibility = "hidden";
            }
        }
    </script>
</html>

