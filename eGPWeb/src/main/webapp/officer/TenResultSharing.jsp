<%-- 
    Document   : TenResultSharing
    Created on : Nov 23, 2010, 3:04:57 PM
    Author     : Administrator
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Opening Result Sharing</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%
            String userId = "";
            HttpSession hs = request.getSession();
            if (hs.getAttribute("userId") != null) {
                userId = hs.getAttribute("userId").toString();
            }

            String tenderId = request.getParameter("tenderId");
            
            if (request.getParameter("btnShare") != null && userId != null)
            {
                CommonXMLSPService commonXMLSPService =
                            (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");

                String shareXml = "";

                // IF isEdit = "U" THEN FIRST DELETE THE ENTRY BY TENDERID
                if (request.getParameter("isEdit").equalsIgnoreCase("U"))
                {
                    // DELETE THE RECORDS tbl_TenderResultSharing BY TENDERID
                    CommonMsgChk commonMsgChk =
                        commonXMLSPService.insertDataBySP("delete",
                                                           "tbl_TenderResultSharing",
                                                           "",
                                                           "tenderId=" + tenderId).get(0);
                }

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                // LOOP FOR Individual Chart
                if (request.getParameterValues("chkIndReport") != null)
                {
                    for (String chkIndReport : request.getParameterValues("chkIndReport"))
                    {
                        // LOOPING THRU EACH FORM LIST
                        if (shareXml == "")
                            shareXml = "<tbl_TenderResultSharing tenderId=\"" + request.getParameter("tenderId")
                                + "\" formId=\"" + chkIndReport + "\" " + "shareDate=\"" + format.format(new Date()) + "\" sharedBy=\""
                                + userId + "\" reportType=\"Individual" + "\" />";
                        else
                            shareXml += "<tbl_TenderResultSharing tenderId=\"" + request.getParameter("tenderId")
                                + "\" formId=\"" + chkIndReport + "\" " + "shareDate=\"" + format.format(new Date()) + "\" sharedBy=\""
                                + userId + "\" reportType=\"Individual" + "\" />";
                    } // END FOR LOOP -> Individual Chart
                }

                // LOOP FOR Comparative Chart
                if (request.getParameterValues("chkCompReprot") != null)
                {
                    for (String chkCompReprot : request.getParameterValues("chkCompReprot"))
                    {
                        // LOOPING THRU EACH FORM LIST
                        if (shareXml == "")
                            shareXml = "<tbl_TenderResultSharing tenderId=\"" + request.getParameter("tenderId")
                                + "\" formId=\"" + chkCompReprot + "\" " + "shareDate=\"" + format.format(new Date()) + "\" sharedBy=\""
                                + userId + "\" reportType=\"Comparative" + "\" />";
                        else
                            shareXml += "<tbl_TenderResultSharing tenderId=\"" + request.getParameter("tenderId")
                                + "\" formId=\"" + chkCompReprot + "\" " + "shareDate=\"" + format.format(new Date()) + "\" sharedBy=\""
                                + userId + "\" reportType=\"Comparative" + "\" />";
                    } // END FOR LOOP -> Comparative Chart
                }

                if (shareXml != "")
                {
                    shareXml = "<root>" + shareXml + "</root>";

                    CommonMsgChk commonMsgChk =
                            commonXMLSPService.insertDataBySP("insert",
                                                               "tbl_TenderResultSharing",
                                                               shareXml,
                                                               "").get(0);
                    //out.println(commonMsgChk.getMsg());
                      response.sendRedirect("LotOpening.jsp?tenderid=" + request.getParameter("tenderId")+"&rshare=y");
                }
                else
                {
                    out.println("<div class='responseMsg errorMsg'>Please select any one checkbox!</div>");
                }
            }
        %>
        <div class="dashboard_div">

              <!--Dashboard Header Start-->
                <div class="topHeader">
                    <%@include file="../resources/common/AfterLoginTop.jsp"%>
                </div>
                <!--Dashboard Header End-->
            <form id="frmTenResultShare" action="TenResultSharing.jsp?tenderId=<%=request.getParameter("tenderId")%>&isEdit=<%=request.getParameter("isEdit")%>" method="POST">
              
                <!--Dashboard Content Part Start-->
                <div class="contentArea_1">
                    <div class="pageHead_1">Opening  Result Sharing
                        <span style="float:right;"><a href="OpenComm.jsp?tenderid=<%=tenderId%>" class="action-button-goback">Go Back</a></span>
                    </div>
                    <%
                            pageContext.setAttribute("tenderId", tenderId);
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <%
                    //tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    
                    // CHECK docAvlMethod COLUMN WHETHER THERE IS "Lot" OR "Package"
                    List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("gettenderlotbytenderid",
                                                                                            tenderId,null);
                        if (!tenderLotList.isEmpty()) {
                            // IF PACKAGE FOUND THEN DISPLAY PACKAGE INFORMATION
                            if (tenderLotList.get(0).getFieldName1().equalsIgnoreCase("Package")) {

                                // FETCH PACKAGE INFORMATION
                                List<SPTenderCommonData> packageList = tenderCommonService.returndata("getlotorpackagebytenderid",
                                                                                    tenderId,
                                                                                    "Package,1");
                                    if (!packageList.isEmpty()) {
                %>
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <td width="15%" class="t-align-left ff">Package No :</td>
                        <td width="85%" class="t-align-left"><%=packageList.get(0).getFieldName1()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Package Description :</td>
                        <td class="t-align-left"><%=packageList.get(0).getFieldName2()%></td>
                    </tr>
                </table>
                <% }  %>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="48%" valign="middle" class="t-align-center">Form Name</th>
                        <th width="27%" valign="middle" class="t-align-center">Individual chart</th>
                        <th width="25%" valign="middle" class="t-align-center">Comparative chart</th>
                    </tr>
                    <%
                        List<SPTenderCommonData> formList = tenderCommonService.returndata("GetFormNameByTenderIdForResultShare",
                                                                                            tenderId,
                                                                                            "0");
                        if (!formList.isEmpty()) {
                            int i = 1;
                            for (SPTenderCommonData formname : formList) {
                                if(Math.IEEEremainder(i,2)==0) {
                    %>
                    <tr class="bgColor-Green">
                     <% } else { %>
                    <tr>
                    <% } %>
                        <td class="t-align-left"><%=formname.getFieldName1()%></td>
                        <% if (formname.getFieldName7().equalsIgnoreCase("Individual")) { %>
                        <td class="t-align-center"><input name="chkIndReport" type="checkbox" checked="checked" value="<%=formname.getFieldName5()%>" /></td>
                        <% } else { %>
                        <td class="t-align-center"><input name="chkIndReport" type="checkbox" value="<%=formname.getFieldName5()%>" /></td>
                        <% } if (formname.getFieldName8().equalsIgnoreCase("Comparative")) { %>
                        <td class="t-align-center"><input name="chkCompReprot" type="checkbox" checked="checked" value="<%=formname.getFieldName5()%>" /></td>
                        <% } else { %>
                        <td class="t-align-center"><input name="chkCompReprot" type="checkbox" value="<%=formname.getFieldName5()%>" /></td>
                        <% } %>
                    </tr>
                     <%
                                i++;
                             } // END FOR LOOP
                         } else {
                    %>
                    <tr>
                        <td class="t-align-left" colspan="3"><b>No form name found! </b></td>
                    </tr>
                    <% } %>
                </table>
                <%
                     } else {
                                for (SPTenderCommonData tenderLot : tenderLotList) {
                %>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td width="13%" class="t-align-left ff">Lot No:</td>
                        <td width="87%" class="t-align-left"><%=tenderLot.getFieldName1()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Lot Description:</td>
                        <td class="t-align-left"><%=tenderLot.getFieldName2()%></td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="48%" valign="middle" class="t-align-center">Form Name</th>
                        <th width="27%" valign="middle" class="t-align-center">Individual chart</th>
                        <th width="25%" valign="middle" class="t-align-center">Comparative chart</th>
                    </tr>
                    <%
                        List<SPTenderCommonData> formList = tenderCommonService.returndata("GetFormNameByTenderIdForResultShare",
                                                                                            tenderId,
                                                                                            tenderLot.getFieldName3().toString());
                        if (!formList.isEmpty()) {
                            int i = 1;
                            for (SPTenderCommonData formname : formList) {
                                if(Math.IEEEremainder(i,2)==0) {
                    %>
                    <tr class="bgColor-Green">
                     <% } else { %>
                    <tr>
                    <% } %>
                        <td class="t-align-left"><%=formname.getFieldName1()%></td>
                        <% if (formname.getFieldName7().equalsIgnoreCase("Individual")) { %>
                        <td class="t-align-center"><input name="chkIndReport" checked="checked" type="checkbox" value="<%=formname.getFieldName5()%>" /></td>
                        <% } else { %>
                        <td class="t-align-center"><input name="chkIndReport" type="checkbox" value="<%=formname.getFieldName5()%>" /></td>
                        <% } if (formname.getFieldName8().equalsIgnoreCase("Comparative")) { %>
                        <td class="t-align-center"><input name="chkCompReprot" checked="checked" type="checkbox" value="<%=formname.getFieldName5()%>" /></td>
                        <% } else { %>
                        <td class="t-align-center"><input name="chkCompReprot" type="checkbox" value="<%=formname.getFieldName5()%>" /></td>
                        <% } %>
                    </tr>
                    <%
                                i++;
                            } // END FOR LOOP
                         } else {
                    %>
                    <tr>
                        <td class="t-align-left" colspan="3"><b>No form name found! </b></td>
                    </tr>
                    <% } %>
                </table>
                <%
                                } // END LOT FOR LOOP
                           } // END IF
                      }

                    // HIDE THE BUTTON, IF USERTYPEID=2 (BIDDER)
                    if (userTypeId == 3) {
                %>
                <div class="t-align-center t_space">
                    <label class="formBtn_1"><input name="btnShare" type="submit" value="Share" /></label>
                </div>
                <% } %>
                <div>&nbsp;</div>
                </div>
                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp"></jsp:include>
                <!--Dashboard Footer End-->
            </form>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
