<%--
    Document   : SeekEvalClari
    Created on : Dec 30, 2010, 4:36:51 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Forms / Seek Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <%  String tenderId, st = "", dayExp = "0", lnkTyp="";
                tenderId = request.getParameter("tenderId");
                st = request.getParameter("st");

                String userId = "", uId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                if (request.getParameter("uId") != null && !"".equalsIgnoreCase(request.getParameter("uId"))) {
                    uId = request.getParameter("uId");
                }

                if(request.getParameter("lnk")!=null){
                    if("et".equalsIgnoreCase(request.getParameter("lnk"))){
                        lnkTyp="et";
                    }
                }
                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                  // Get Tender Procurement Nature info
                    String strProcurementNature="";
                    List<SPTenderCommonData> lstProcurementNature =
                    tenderCS.returndata("getTenderProcurementNature", tenderId, null);
                      if(!lstProcurementNature.isEmpty()){
                        strProcurementNature=lstProcurementNature.get(0).getFieldName1();
                      }
                   lstProcurementNature = null;
                     String strComType = "null";
                            if (request.getParameter("comType") != null && !"null".equalsIgnoreCase(request.getParameter("comType"))) {
                                strComType = request.getParameter("comType");
                            }


                    String strTenEnvelopcnt="0";
                       /* START : CODE TO GET TENDER ENVELOPE COUNT */
                        List<SPTenderCommonData> lstEnvelops =
                                tenderCS.returndata("getTenderEnvelopeCount", tenderId, "0");

                        if(!lstEnvelops.isEmpty()){
                            if(Integer.parseInt(lstEnvelops.get(0).getFieldName1()) > 0){
                                strTenEnvelopcnt=lstEnvelops.get(0).getFieldName1();
                            }
                        }
                        lstEnvelops=null;
                        /* START : CODE TO GET TENDER ENVELOPE COUNT */
            %>
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">View Forms / Seek Clarification
                    <span style="float:right;">
                        <a href="Evalclarify.jsp?tenderId=<%=tenderId%>&st=<%=st%>&comType=<%=strComType%>" class="action-button-goback">Go back to Dashboard</a>
                    </span>
                </div>
                <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%
                       boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                %>
                <div>&nbsp;</div>
                <%//if(!"TEC".equalsIgnoreCase(request.getParameter("comType"))) { %>
                <jsp:include page="officerTabPanel.jsp" >
                     <jsp:param name="tab" value="7" />
                </jsp:include>
                <%// } %>
                <div class="tabPanelArea_1">
                <%
                    if ("rp".equalsIgnoreCase(request.getParameter("st")))
                        pageContext.setAttribute("TSCtab", "3");
                    else
                        pageContext.setAttribute("TSCtab", "4");
                %>
                <%@include  file="../resources/common/AfterLoginTSC.jsp"%>

                    <% for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", uId, tenderId))
                        {
                            dayExp = companyList.getFieldName2();
                    %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                 <th colspan="2" class="t_align_left ff">Company Details</th>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Company Name :</td>
                                <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                            </tr>
                        </table>
                    <%
                        } // END FOR LOOP OF COMPANY NAME

                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            // CHECK WHETHER TENDER IS "PACKAGE" OR "LOT" WISE
                            if (isTenPackageWis) {

                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                    %>
                   <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>

                            <th colspan="2" class="t-align-left ff">Package Information</th>
                        </tr>

                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>
                        
                     <%
                        List<SPCommonSearchData> allForms = commonSearchService.searchData("GetTenderFormsByLotId", tenderId, "0", "0", uId, userId, "Evaluation", strProcurementNature, strTenEnvelopcnt, null);
                        int[] formColTotal = new int[allForms.size()];
                        String formIds = "";
                     %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="25%" class="t-align-center ff">Form Name</th>
                            <%if("Services".equalsIgnoreCase(afterLoginTSCTabprocNature.toString())){%>
                            <th class="t-align-center ff">Criteria</th>
                            <th class="t-align-center ff">Sub Criteria</th>
                            <th class="t-align-center ff">Rating</th>
                            <%if(!"REOI".equalsIgnoreCase(afterLoginTSCTabTenderType.toString())){%>
                            <th class="t-align-center ff">Score</th>
                            <%}}%>
                            <th class="t-align-center">Evaluation Status</th>
                            <th class="t-align-center">Action</th>
                        </tr>
                        <%
                            int i = 1;
                            for (SPCommonSearchData formdetails : allForms) {
                                List<SPCommonSearchData> listGetCriteria = null;                                
                                // HIGHLIGHT THE ALTERNATE ROW                                
                        %>
                        <tr <%=(Math.IEEEremainder(i,2)==0) ? "class=\"bgColor-Green\"" : ""%> id="fformtr_<%=i%>">
                            <td class="t-align-left ff">
                                <a href="ViewEvalBidform.jsp?tenderId=<%=tenderId%>&uId=<%=uId%>&formId=<%=formdetails.getFieldName1()%>&lotId=0&bidId=<%=formdetails.getFieldName3()%>&action=Edit&isSeek=true&comType=<%=strComType%>" target="_blank"><%=formdetails.getFieldName2()%></a>
                                <!--< %=formdetails.getFieldName2()%> -->
                            </td>
                            <%
                                if("Services".equalsIgnoreCase(afterLoginTSCTabprocNature.toString())){
                                    listGetCriteria = commonSearchService.searchData("GetCriteria4FormId",formdetails.getFieldName1(),uId,userId,tenderId,null,null,null,null,null); //change by dohatec for re-evaluation
                                    if(!listGetCriteria.isEmpty()){
                            %>
                            <td width ="20%" class="t-align-center ff"><%=listGetCriteria.get(0).getFieldName1()%></td>
                            <td width ="25%" class="t-align-center ff">
                                <table>
                                    <%for(SPCommonSearchData crits : listGetCriteria){
                                        formColTotal[i-1] = formColTotal[i-1] + Integer.parseInt(crits.getFieldName5());
                                        if(!crits.getFieldName5().equals("0")){
                                            out.print("<tr><td width='10%'>"+crits.getFieldName2()+"</td></tr>");
                                        }
                                    }%>
                                </table>                                
                            </td>
                            <td width="10%" class="t-align-center ff">
                                <table>
                                    <%for(SPCommonSearchData crits : listGetCriteria){
                                        if(!crits.getFieldName5().equals("0")){
                                            out.print("<tr><td width='10%'>"+crits.getFieldName3()+"</td></tr>");
                                        }
                                    }%>
                                </table>
                            </td>
                            <%if(!"REOI".equalsIgnoreCase(afterLoginTSCTabTenderType.toString())){%>
                            <td class="t-align-center ff">
                                <table>
                                    <%for(SPCommonSearchData crits : listGetCriteria){
                                        if(!crits.getFieldName5().equals("0")){
                                            out.print("<tr><td width='10%'>"+crits.getFieldName4()+"</td></tr>");
                                        }
                                    }%>
                                </table>
                            </td>
                            <%}}}%>
                            <td class="t-align-center  ff"><%=formdetails.getFieldName4() %></td>
  <!--                          <%out.println("aaaa >> >> >>>> >> >>>> >> >>"+formdetails.getFieldName4());%>-->
                            <td class="t-align-center">
                                <%if("et".equalsIgnoreCase(lnkTyp)) {%>
                                   <%if ("Services".equalsIgnoreCase(strProcurementNature)) {%>
                                   <%if (formdetails.getFieldName4().equals("Completed")) {%>
                                   <a href="EvalServiceMarking.jsp?uId=<%=uId%>&formId=<%=formdetails.getFieldName1()%>&tenderId=<%=tenderId%>">Form Evaluated</a>
                                   <% } else {%>
                                   <a href="EvalServiceMarking.jsp?uId=<%=uId%>&formId=<%=formdetails.getFieldName1()%>&tenderId=<%=tenderId%>">Evaluate Form</a>
                                    <%}%>
                                   <% } else {%>
<!--                                    <a href="ViewQueAns.jsp?tenderId=<%=tenderId%>&st=<%=st%>&formId=<%=formdetails.getFieldName1()%>&uid=<%=uId%>&bidId=<%=formdetails.getFieldName3()%>&pkgLotId=0&comType=<%=strComType%>">Evaluate Form</a>-->
                                    <%if (formdetails.getFieldName4().equals("Accepted")||formdetails.getFieldName4().equals("Rejected")) {%>
                                   <a href="ViewQueAns.jsp?tenderId=<%=tenderId%>&st=<%=st%>&formId=<%=formdetails.getFieldName1()%>&uid=<%=uId%>&pkgLotId=0&comType=<%=strComType%>">Form Evaluated</a>
                                   <% } else {%>
                                   <a href="ViewQueAns.jsp?tenderId=<%=tenderId%>&st=<%=st%>&formId=<%=formdetails.getFieldName1()%>&uid=<%=uId%>&pkgLotId=0&comType=<%=strComType%>">Evaluate Form</a>
                                    <%}%>
                                   <%}%>

                                <%
                                } else {
                                    // VISIBLE BELOW LINK, IF RESPONSE TIME IS OVER
                                    if (Integer.parseInt(dayExp) >= 0) {
                                %>
                                    <a href="EvalPostQue.jsp?tenderId=<%=tenderId%>&st=<%=st%>&uId=<%=uId%>&frmId=<%=formdetails.getFieldName1()%>&pkgLotId=0&comType=<%=strComType%>">Seek Clarification for a form</a>  |
                                    <a href="../resources/common/evalClariDoc.jsp?tenderId=<%=tenderId%>&st=<%=st%>&formId=<%=formdetails.getFieldName1()%>&pId=office&uId=<%=uId%>&comType=<%=strComType%>">Upload Document</a>
                                <%
                                    }
                                }
                                %>
                            </td>
                        </tr>
                        <%                            
                            i++;
                            }  // END FOR LOOP

                            if (i == 1) {
                         %>
                         <tr>
                             <td colspan="3" style="color: red; text-align: center">
                                 No forms found
                             </td>
                         </tr>
                         <% } %>
                    </table>
                     <%
                         if("Services".equals(afterLoginTSCTabprocNature.toString()) && !"REOI".equalsIgnoreCase(afterLoginTSCTabTenderType.toString())){
                             for(int x=0; x<formColTotal.length; x++){
                                 if(formColTotal[x]==0){
                                     formIds  = formIds + allForms.get(x).getFieldName1() + ",";
                     %>
                            <script type="text/javascript">                                
                                $('#fformtr_<%=x+1%>').hide();
                            </script>
                     <%}}
                            if(formIds.contains(",")){
                                commonSearchService.searchData("InsertEvalSerFormDetail4Zero", tenderId, null, uId, userId, null, null, null, null, null);
                            }
                          }
                         }
                             } else {
                                 for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th colspan="2" class="t-align-center ff">Package Information</th>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%  }
                            for (SPCommonSearchData lotList : commonSearchService.searchData("gettenderlotbytenderid", tenderId, "", "", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                            <tr>
                                <td width="22%" class="t-align-left ff">Lot No. :</td>
                                <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Lot Description :</td>
                                <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%
                            List<SPCommonSearchData> allForms = commonSearchService.searchData("GetTenderFormsByLotId", tenderId, "1", lotList.getFieldName3(), uId, userId, "Evaluation", strProcurementNature, strTenEnvelopcnt, null);
                            int[] formColTotal = new int[allForms.size()];
                            String formIds = "";
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="25%" class="t-align-center ff">Form Name</th>
                                <%if("Services".equalsIgnoreCase(afterLoginTSCTabprocNature.toString())){%>
                                <th class="t-align-center ff">Criteria</th>
                                <th class="t-align-center ff">Sub Criteria</th>
                                <th class="t-align-center ff">Rating</th>
                                <%if(!"REOI".equalsIgnoreCase(afterLoginTSCTabTenderType.toString())){%>
                                <th class="t-align-center ff">Score</th>
                                <%}}%>
                                <th class="t-align-center">Evaluation Status</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                int i = 1;
                                for (SPCommonSearchData formdetails : allForms) {
                                     List<SPCommonSearchData> listGetCriteria = null;
                                    // HIGHLIGHT THE ALTERNATE ROW                                    
                            %>
                            <tr <%=(Math.IEEEremainder(i,2)==0) ? "class=\"bgColor-Green\"" : ""%> id="fformtr_<%=i%>">
                                <td class="t-align-left ff">
                                    <%=formdetails.getFieldName2()%>
                                </td>
                                <%
                                  if("Services".equalsIgnoreCase(afterLoginTSCTabprocNature.toString())){
                                    listGetCriteria = commonSearchService.searchData("GetCriteria4FormId",formdetails.getFieldName1(),uId,userId,null,null,null,null,null,null);
                                    if(!listGetCriteria.isEmpty()){
                                %>
                                <td class="t-align-center ff"><%=listGetCriteria.get(0).getFieldName1()%></td>
                                <td class="t-align-center ff">
                                    <table>
                                         <%
                                            for(SPCommonSearchData crits : listGetCriteria){
                                                formColTotal[i-1] = formColTotal[i-1] + Integer.parseInt(crits.getFieldName5());
                                                if(!crits.getFieldName5().equals("0")){
                                                    out.print("<tr><td width='10%'>"+crits.getFieldName2()+"</td></tr>");
                                                }
                                            }
                                          %>
                                    </table>
                                </td>
                                <td class="t-align-center ff">
                                    <table>
                                        <%
                                            for(SPCommonSearchData crits : listGetCriteria){
                                                if(!crits.getFieldName5().equals("0")){
                                                    out.print("<tr><td width='10%'>"+crits.getFieldName3()+"</td></tr>");
                                                }                                                
                                        }%>
                                    </table>
                                </td>
                                <%if(!"REOI".equalsIgnoreCase(afterLoginTSCTabTenderType.toString())){%>
                                <td class="t-align-center ff">
                                    <table>
                                        <%
                                            for(SPCommonSearchData crits : listGetCriteria){
                                                if(!crits.getFieldName5().equals("0")){
                                                    out.print("<tr><td width='10%'>"+crits.getFieldName4()+"</td></tr>");
                                                } 
                                        }%>
                                    </table>
                                </td>
                                <%}}}%>
                                <td class="t-align-center ff">
                                    <%=formdetails.getFieldName4() %>
                                </td>
                                <td class="t-align-left ff">
                                    <%if("et".equalsIgnoreCase(lnkTyp)) {%>
                                        <%if ("Services".equalsIgnoreCase(strProcurementNature)) {%>
                                            <%if (formdetails.getFieldName4().equals("Completed")) {%>
                                           <a href="EvalServiceMarking.jsp?uId=<%=uId%>S&formId=<%=formdetails.getFieldName1()%>&tenderId=<%=tenderId%>&comType=<%=strComType%>"> Form Evaluated</a>
                                           <% } else {%>
                                           <a href="EvalServiceMarking.jsp?uId=<%=uId%>S&formId=<%=formdetails.getFieldName1()%>&tenderId=<%=tenderId%>&comType=<%=strComType%>"> Evaluate Form</a>
                                            <%}%>
                                    <%
                                        } else {
        %>
<!--                                        <a href="ViewQueAns.jsp?tenderId=<%=tenderId%>&st=<%=st%>&formId=<%=formdetails.getFieldName1()%>&uid=<%=uId%>&pkgLotId=<%=lotList.getFieldName3()%>&bidId=<%=formdetails.getFieldName3()%>&comType=<%=strComType%>"> Evaluate Form</a>-->
                                           <%if (formdetails.getFieldName4().equals("Accepted")||formdetails.getFieldName4().equals("Rejected")) {%>
                                           <a href="ViewQueAns.jsp?tenderId=<%=tenderId%>&st=<%=st%>&formId=<%=formdetails.getFieldName1()%>&uid=<%=uId%>&pkgLotId=<%=lotList.getFieldName3()%>&comType=<%=strComType%>"> Form Evaluated</a>
                                           <% } else {%>
                                           <a href="ViewQueAns.jsp?tenderId=<%=tenderId%>&st=<%=st%>&formId=<%=formdetails.getFieldName1()%>&uid=<%=uId%>&pkgLotId=<%=lotList.getFieldName3()%>&comType=<%=strComType%>"> Evaluate Form</a>
                                            <%}%>
                                        <%}%>

                                    <%
                                    } else {
                                        // VISIBLE BELOW LINK, IF RESPONSE TIME IS OVER
                                        if (Integer.parseInt(dayExp) >= 0) {
                                    %>
                                     <a href="EvalPostQue.jsp?tenderId=<%=tenderId%>&uId=<%=uId%>&st=<%=st%>&frmId=<%=formdetails.getFieldName1()%>&pkgLotId=<%=lotList.getFieldName3()%>&comType=<%=strComType%>">Seek Clarification for a form</a>  |
                                    <a href="../resources/common/evalClariDoc.jsp?tenderId=<%=tenderId%>&st=<%=st%>&formId=<%=formdetails.getFieldName1()%>&pId=office&uId=<%=uId%>&comType=<%=strComType%>">Upload Document</a>
                                    <%
                                        }
                                    }
%>
                                </td>
                            </tr>
                            <%
                                    i++;
                                 }  // END FOR LOOP OF FORM

                              if (i == 1) {
                             %>
                             <tr>
                                 <td colspan="3" class="ff" style="color: red; text-align: center">
                                     No forms found
                                 </td>
                             </tr>
                             <% } %>
                        </table>
                        <%
                         if(!"REOI".equalsIgnoreCase(afterLoginTSCTabTenderType.toString())){
                             for(int x=0; x<formColTotal.length; x++){
                                 if(formColTotal[x]==0){
                                     formIds  = formIds + allForms.get(x).getFieldName1() + ",";
                     %>
                            <script type="text/javascript">
                                $('#fformtr_<%=x+1%>').hide();
                            </script>
                     <%}}
                            if(formIds.contains(",")){
                                commonSearchService.searchData("InsertEvalSerFormDetail4Zero", tenderId, null, uId, userId, null, null, null, null, null);
                            }
                          }
                            }
                                    }
                        %>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
