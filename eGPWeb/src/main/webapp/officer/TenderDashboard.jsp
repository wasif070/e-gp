<%-- 
    Document   : ${name}
    Created on : ${date}, ${time}
    Author     : Malhar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Notice Preparation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>

    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
            <div class="pageHead_1">Tender Dashboard</div>
            <%
                   
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                        session.setAttribute("TenderIdForResponse", request.getParameter("tenderid"));
            %>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%@include file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">
                <div class="bigTxt_1" align="center">TENDER DASHBOARD</div>
            </div>
            </div>
            <!--Dashboard Content Part End-->
            <div>&nbsp;</div>
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
