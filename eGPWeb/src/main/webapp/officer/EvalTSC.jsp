<%-- 
    Document   : EvalTSC
    Created on : Dec 6, 2010, 2:16:54 PM
    Author     : rajesh
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation</title>
         <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        
        <!--jalert -->


        <script type="text/javascript" >
            function checkTender(){
                jAlert("there is no Tender Notice"," Alert ", "Alert");
            }


            function checkdefaultconf(){
                jAlert("Default workflow configuration hasn't crated"," Alert ", "Alert");
            }
            function checkDates(){
                jAlert(" please configure dates"," Alert ", "Alert");
            }
        </script>
    </head>
    <body>

        <div class="dashboard_div">
        <!--Dashboard Header Start-->
       <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <div class="contentArea_1">
        <div class="pageHead_1">Evaluation</div>
        <%
        String strTSCTenderId = request.getParameter("tenderId");
                    // Variable tenderId is defined by u on ur current page.
                    pageContext.setAttribute("tenderId", strTSCTenderId);
                    pageContext.setAttribute("tab", "7");
                    //pageContext.setAttribute("TSCtab", "1");
%>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
           <div>&nbsp;</div>
        <jsp:include page="officerTabPanel.jsp" >
                     <jsp:param name="tab" value="7" />
                </jsp:include>

        <div class="tabPanelArea_1">

                        <%-- Start: CODE TO DISPLAY MESSAGES --%>
               
                 <%if (request.getParameter("msgId") != null) {
                    String msgId = "", msgTxt = "";
                    boolean isError = false;
                    msgId = request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")) {
                        if (msgId.equalsIgnoreCase("error")) {
                            isError = true;
                            msgTxt = "There was some error.";
                        } else {
                            if (msgId.equalsIgnoreCase("nc")) {
                                msgTxt = "Successfully notified to Chairperson.";
                            } else {
                                msgTxt = "Successfully sent to Chairperson.";
                            }
                        }
                    %>
                    <%if (isError) {%>
                        <div class="responseMsg errorMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                    <%} else {%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                    <%}%>
                    <%}
                         }
                    %>

                      <%if (request.getParameter("flag") != null && request.getParameter("flag").equals("true")) {%>
            <div id="successMsg" class="responseMsg successMsg" style="margin-bottom: 12px;">Declaration given successfully</div>
            <%}%>
                    <%-- End: CODE TO DISPLAY MESSAGES --%>

            <%
                       TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            %>

            
            <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1">
                <%
                            List<SPTenderCommonData> checkEval0 =
                                    tenderCommonService1.returndata("GetCommiteeDetailsTSC", strTSCTenderId, null);
                            SPTenderCommonData spTenderCommondata0 = checkEval0.get(0);
                            if (checkEval0.size() > 0) {
                %>
                <tr>
                    <td width="25%" class="t-align-left ff">Committee Name :</td>
                    <td width="75%" class="t-align-left">
                        <%=spTenderCommondata0.getFieldName1()%>
                    </td>
                </tr>
                <tr>
                    <td width="25%" class="t-align-left ff">Minimum Member's Declaration Required :</td>
                    <td width="75%" class="t-align-left">
                        <%=spTenderCommondata0.getFieldName2()%>
                    </td>
                </tr>
               
                <%} else {%>
                <tr>
                    <td width="25%" class="t-align-left ff" colspan="2">No Record Found.</td>
                </tr>
                <%}%>
            </table>
            <div>&nbsp;</div>
            <%
                    pageContext.setAttribute("TSCtab", "1");
            %>

            <%@include file="../resources/common/AfterLoginTSC.jsp"%>
            <div class="tabPanelArea_1">
            <%


                       List<SPTenderCommonData> checkEval1 =
                                tenderCommonService1.returndata("CheckCommitteeStatus", strTSCTenderId, "approved");

                        if (checkEval1.size() > 0) {
            %>
            <%--START: Evaluation MEMBERS TABLE--%>
            <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1">
                <tr>
                    <th width="4%" class="ff" class="t-align-center">Sl. No.</th>
                    <th width="36%" class="ff">Committee Members </th>
                    <th width="20%" class="ff" class="t-align-center">Role </th>
                    <th width="20%" class="t-align-center">Declaration Status </th>
                    <th width="20%" class="t-align-center">Declaration Date and Time</th>
                </tr>
                <%
                                            int minRequirement = 0, memberCnt = 0, ApprovedMemCnt = 0;
                                            boolean showVerify = false;
                                            String userId = "";

                                            HttpSession hs = request.getSession();
                                            if (hs.getAttribute("userId") != null) {
                                                userId = hs.getAttribute("userId").toString();
                                            }

                                            //tenderId = request.getParameter("tenderid");

                                            //for (SPTenderCommonData commonTenderDetails : tenderCommonService.returndata("getTenderEvaluationTSCinfo", tenderId, null)) {

                                            

                                            for (SPTenderCommonData commonTenderDetails : tenderCommonService.returndata("getTenderTSCCommInfo", strTSCTenderId, null)) {
                                                memberCnt++;
                                                //ApprovedMemCnt = Integer.parseInt(commonTenderDetails.getFieldName8());

                                                /*
                                                if (ApprovedMemCnt >= minRequirement) {
                                                    String currUserId = "";
                                                    currUserId = commonTenderDetails.getFieldName7();
                                                    if (currUserId != "") {
                                                        if (Integer.parseInt(currUserId) == Integer.parseInt(userId)) {
                                                            showVerify = true;
                                                        }
                                                    }
                                                }
 */
                %>
                <tr <%if (Math.IEEEremainder(memberCnt, 2) == 0) {%>class="bgColor-Green"<%}%>>
                    <td  class="t-align-center"><%=memberCnt%></td>
                    <td>
                        <% if ( userId.equalsIgnoreCase(commonTenderDetails.getFieldName1()) && commonTenderDetails.getFieldName4().equalsIgnoreCase("pending")) {%>
                        <a href="EvalApp.jsp?tenderid=<%=strTSCTenderId%>&id=<%=commonTenderDetails.getFieldName6()%>&uid=<%=commonTenderDetails.getFieldName1()%>&Name=<%=commonTenderDetails.getFieldName2()%>&comType=<%=request.getParameter("comType")%>"><%=commonTenderDetails.getFieldName2()%></a>
                        <%} else {%><%=commonTenderDetails.getFieldName2()%><%}%>
                    </td>
                    <td class="t-align-center">
                        <%
                        if(commonTenderDetails.getFieldName3().equalsIgnoreCase("chair person")){%>
                        Chairperson
                        <%}else{%>
                        <%=commonTenderDetails.getFieldName3()%>
                        <%}%>
                    </td>
                    <td class="t-align-center">
                        <%=commonTenderDetails.getFieldName4()%>
                    </td>
                    <td class="t-align-center">
                       <% if (!commonTenderDetails.getFieldName4().equalsIgnoreCase("pending")) {%>
                        <%=commonTenderDetails.getFieldName5()%>
                        <%} else {%>
                        -
                        <%}%>
                    </td>
                </tr>
                
                <%}%>
            
                <% if (memberCnt == 0) {%>
                <tr>
                    <td colspan="5" class="ff">No members found!</td>
                </tr>
                <%}%>
              
                <%}%>
                </table>
                
          <%--END Evaluation MEMBERS TABLE--%>
        </div>
        </div>
        <br/>
        </div>

         <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->


        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
