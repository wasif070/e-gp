<%-- 
    Document   : EvalClari
    Created on : Dec 30, 2010, 2:16:46 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List of Tenderers/Consultants for seeking clarification View Queries – View Queries / Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="dashboard_div">
        
            <%  String tenderId;
                tenderId = request.getParameter("tenderId");
                String sentBy = "", role = "";

                if(session.getAttribute("userId") != null)
                  sentBy = session.getAttribute("userId").toString();

                CommonSearchService commonSearchService1 = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                String xmldata = "";
                String remark = "";

                if(request.getParameter("btnSendToTEC") != null ) {
                 try {
                      
                      String evalStatus = "Evaluation";
                      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                      HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                      remark = handleSpecialChar.handleSpecialChar(remark);
                      xmldata = "<tbl_EvalSentQueToCp tenderId=\"" + tenderId + "\" sentBy=\"" + sentBy + "\" sentDt=\"" + format.format(new Date()) + "\" evalStatus=\"" + evalStatus + "\" remarks=\"" + remark + "\" sentFor=\"" + "question" + "\" />";
                      xmldata = "<root>" + xmldata + "</root>";
                      
                      CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                      CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalSentQueToCp", xmldata, "").get(0);
                      if (commonMsgChk != null || handleSpecialChar != null) {
                             commonMsgChk = null;
                             handleSpecialChar = null;
                     }

                  String emailId= "" ;
                  String refNo = "";
                  List<SPTenderCommonData> lstTCS = tenderCS.returndata("GetTecCpEmail",tenderId,null);
                  for(SPTenderCommonData data : lstTCS){
                   emailId += data.getFieldName1()+",";
                   refNo = data.getFieldName2();
                   }
                  String emailArr[] = {emailId};
                  SendMessageUtil sendMessageUtil = new SendMessageUtil();
                  MailContentUtility mailContentUtility = new MailContentUtility();
                  
                   List list = mailContentUtility.evalClari(tenderId,refNo);
                   String mailSub = list.get(0).toString();
                   String mailText = list.get(1).toString();
                   sendMessageUtil.setEmailTo(emailArr);
                   sendMessageUtil.setEmailSub(mailSub);
                   sendMessageUtil.setEmailMessage(mailText);
                   sendMessageUtil.sendEmail();
                   response.sendRedirect("EvalClari.jsp?tenderId=" + tenderId + "&msgId=sc");

                }catch (Exception ex) {
                      ex.printStackTrace();
                      response.sendRedirect("EvalClari.jsp?tenderId=" + tenderId + "&msgId=error");
                }
            }

            if (request.getParameter("btnNotifyToTEC") != null)
                {
                    try {

                      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                      HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                      remark = handleSpecialChar.handleSpecialChar(remark);
                      xmldata = "<tbl_EvalSentQueToCp tenderId=\"" + tenderId + "\" sentBy=\"" + sentBy + "\" sentDt=\"" + format.format(new Date()) + "\" evalStatus=\"" + "Evaluation" + "\" remarks=\"" + remark + "\" sentFor=\"" + "evaluation" + "\" />";
                      xmldata = "<root>" + xmldata + "</root>";

                      CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                      CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalSentQueToCp", xmldata, "").get(0);
                      if (commonMsgChk != null || handleSpecialChar != null) {
                             commonMsgChk = null;
                             handleSpecialChar = null;
                        }


                  String emailId= "" ;
                  String refNo = "";
                  List<SPTenderCommonData> lstTCS = tenderCS.returndata("GetTecCpEmail",tenderId,null);
                  for(SPTenderCommonData data : lstTCS){
                   emailId += data.getFieldName1()+",";
                   refNo = data.getFieldName2();
                   }
                  String emailArr[] = {emailId};
                  SendMessageUtil sendMessageUtil = new SendMessageUtil();
                  MailContentUtility mailContentUtility = new MailContentUtility();

                   List list = mailContentUtility.evalClarification(tenderId,refNo);
                   String mailSub = list.get(0).toString();
                   String mailText = list.get(1).toString();
                   sendMessageUtil.setEmailTo(emailArr);
                   sendMessageUtil.setEmailSub(mailSub);
                   sendMessageUtil.setEmailMessage(mailText);
                   sendMessageUtil.sendEmail();
                   response.sendRedirect("EvalClari.jsp?tenderId=" + tenderId + "&msgId=nc");
                    }catch (Exception ex) {
                         ex.printStackTrace();
                         response.sendRedirect("EvalClari.jsp?tenderId=" + tenderId + "&msgId=error");
                    }
                    //
                }

                // IF ENTRY FOUND IN "tbl_EvalSentQueToCp" TABLE IN CASE OF "tec" MEMBER.
                // GET THE TEC-MEMBER COUNT.
                List<SPTenderCommonData> isTEC = tenderCS.returndata("IsTEC",
                                                                     tenderId,
                                                                     sentBy);
                int isTECCnt = isTEC.size();

                // CHECK FOR USER-ROLL AND SET THE LINK AS PER THE ROLL.
                List<SPCommonSearchData> roleList = commonSearchService1.searchData("EvalMemberRole", tenderId, sentBy, null, null, null, null, null, null, null);

                if (!roleList.isEmpty()){
                    role = roleList.get(0).getFieldName2();
                }

            %>
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <form id="frmEvalClari" name="frmEvalClari" action="EvalClari.jsp?tenderId=<%=tenderId%>" method="post">
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">List of Tenderers/Consultants for seeking clarification</div>
                <%
                        pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
                <%if(!"TEC".equalsIgnoreCase(request.getParameter("comType"))) { %>
                <jsp:include page="officerTabPanel.jsp" >
                     <jsp:param name="tab" value="7" />
                </jsp:include>
                <% } %>
                <div class="tabPanelArea_1">
                 <%if (request.getParameter("msgId") != null) {
                    String msgId = "", msgTxt = "";
                    boolean isError = false;
                    msgId = request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")) {
                        if (msgId.equalsIgnoreCase("error")) {
                            isError = true;
                            msgTxt = "There was some error.";
                        } else {
                             // IF ROLL IS "CHAIR PERSON" (CP)
                             if ("cp".equalsIgnoreCase(role))
                                msgTxt = "Successfully sent to Bidder/Consultant.";
                             else if(msgId.equalsIgnoreCase("nc"))
                                 msgTxt = "Successfully notify  to TEC Chairperson.";
                             else
                                msgTxt = "Successfully sent to TEC Chairperson.";
                        }
                    %>
                    <%if (isError) {%>
                    <div class="responseMsg errorMsg"><%=msgTxt%></div>
                    <%} else {%>
                    <div class="responseMsg successMsg"><%=msgTxt%></div>
                    <%}%>
                    <%}
                         }
                    %>
                    <%
                    if ("rp".equalsIgnoreCase(request.getParameter("st")))
                        pageContext.setAttribute("TSCtab", "3");
                    else
                        pageContext.setAttribute("TSCtab", "4");
                %>
                <%@include file="../resources/common/AfterLoginTSC.jsp"%>

                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                          <th width="4%" class="t-align-center" height="21">Sl. No.</th>
                          <th width="58%" class="t-align-center" height="21">List of Tenderers/Consultants</th>
                          <th class="t-align-center" width="38%" height="21"><strong>Action</strong></th>
                        </tr>
                        <%
                            List<SPTenderCommonData> companyList = tenderCS.returndata("getFinalSubComp",
                                                                                       tenderId,
                                                                                       "0");
                            if (!companyList.isEmpty()) {
                                int cnt = 1;
                            for (SPTenderCommonData companyname : companyList) {
                                // HIGHLIGHT THE ALTERNATE ROW
                                if(Math.IEEEremainder(cnt,2)==0) {
                        %>
                        <tr class="bgColor-Green">
                         <% } else { %>
                        <tr>
                        <% } %>
                          <td class="t-align-left" width="5%"><%=cnt%></td>
                          <td class="t-align-left" width="55%"><%=companyname.getFieldName3()%></td>
                          <td class="t-align-left" width="38%">
                              <%
                                // ACCORDING TO ROLL SET THE LINK
                                if (!"".equalsIgnoreCase(role)) {
                                    // IF ROLL IS "CHAIR PERSON" (CP)
                                    if ("cp".equalsIgnoreCase(role)) {
                                        // AND IF USER COMMING FROM
                                        if ("cl".equalsIgnoreCase(request.getParameter("st"))) {
                                            // SET THE LINK OF "NO QUESTION" WHEN NO QUESTION FOUND FOR ANY BIDDER
                                            List<SPTenderCommonData> evalFormQuesCount = tenderCS.returndata("GetEvalFormQuesCount", tenderId, companyname.getFieldName2());

                                            String questCount = evalFormQuesCount.get(0).getFieldName1();
                                            
                                            if (Integer.parseInt(questCount) == 0) {
                                %>
                                                <label>No Query</label>&nbsp; &nbsp;
                                <%
                                                }
                                            else
                                                {
                                            List<SPTenderCommonData> evalBidderRespExist = tenderCS.returndata("GetEvalBidderRespTenderId", tenderId, companyname.getFieldName2());
                                            // SHOW BELOW LINK IF - "EvalBidderResponse" EXISTS
                                            if (evalBidderRespExist.isEmpty()) {
                              %>
                              <a href="EvalClariPostBidder.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>">View Queries / Send Queries To Bidder/Consultant</a>&nbsp; &nbsp;
                              <% } else { %>
                              <label>Posted To Bidder/Consultant</label>&nbsp; | &nbsp;
                              <a href="EvalViewQue.jsp?tenderId=<%=tenderid%>&uid=<%=companyname.getFieldName2()%>&st=<%=request.getParameter("st")%>">View Queries / Clarification</a>
                              <%
                                    }
                                  }
                                } else { %>
                              <a href="EvalTenderer.jsp?userId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>">Finalize Responsiveness</a>&nbsp; &nbsp;
                              <%
                                    //  SHOW BELOW "Give Answer To CP" LINK, IF FOUND IN "tbl_EvalCpMemClarification"
                                    List<SPTenderCommonData> evalCPExistCount = tenderCS.returndata("GetEvalCPClarificationStatus",
                                                                                                    companyname.getFieldName2(),
                                                                                                    sentBy);
                                    if (!evalCPExistCount.isEmpty()) {
                              %>
                              &nbsp;<a href="EvalanstoCP.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>">View Clarification By Member</a>
                              <%
                                 } // END IF
                                // PUT COND. FOR HIDE BELOW LINK FROM - tbl_EvalBidderStatus - tenderid and all users
                              %>
<!--                              <a href="TenderersCompRpt.jsp?tenderid=<%=tenderId%>&st=<%=request.getParameter("st")%>">View Comparative Report</a>-->
                              <%
                                    } // END ELSE FOR "EVAL REPT." - TAB
                                } else {
                                    // HIDE THE LINK IF ENTRY FOUND IN "tbl_EvalSentQueToCp" TABLE IN CASE OF "tec" MEMBER.
                                    if (isTECCnt == 0) {
                              %>
                              <a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>">Seek Clarification from Bidder/Consultant</a>&nbsp; &nbsp;
                              <% 
                                    }
                                    else {
                              %>
                              <a href="EvalViewQue.jsp?tenderId=<%=tenderid%>&uid=<%=companyname.getFieldName2()%>&st=<%=request.getParameter("st")%>">View Queries / Clarification</a>
                              <%
                                    }
                                    // VISIBLE BELOW LINK, IF RESPONSE TIME IS OVER
                                    if (Integer.parseInt(companyname.getFieldName4()) < 0) {
                              %>
                              | <a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>">Evaluate Bidder/Consultant</a>
                              <%
                                        }
                                    //  SHOW BELOW "Give Answer To CP" LINK, IF FOUND IN "tbl_EvalCpMemClarification"
                                    List<SPTenderCommonData> evalCPExistCount = tenderCS.returndata("GetEvalCPClarificationExist",
                                                                                                    companyname.getFieldName2(),
                                                                                                    sentBy);
                                    if (!evalCPExistCount.isEmpty()) {
                              %>
                              <a href="EvalanstoCP.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>">Give Clarification To Chair Person</a>
                              <%
                                    } // END IF OF - evalCPExistCount
                                  }
                                } else {
                                    // HIDE THE LINK IF ENTRY FOUND IN "tbl_EvalSentQueToCp" TABLE IN CASE OF "tec" MEMBER.
                                    if (isTECCnt == 0) {
                              %>
                              <a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>">Seek Clarification from Bidder/Consultant</a>&nbsp; &nbsp;
                              <%
                                    } else {
                              %>
                              <a href="EvalViewQue.jsp?tenderId=<%=tenderid%>&uid=<%=companyname.getFieldName2()%>&st=<%=request.getParameter("st")%>">View Queries / Clarification</a> &nbsp;
                              <%
                                    }
                                    // VISIBLE BELOW LINK, IF RESPONSE TIME IS OVER
                                    if (Integer.parseInt(companyname.getFieldName4()) < 0) {

                                         List<SPTenderCommonData> evalNotifycnt1 = tenderCS.returndata("evalNotifycnt",
                                                                                              tenderId,sentBy);
                                if (Integer.parseInt(evalNotifycnt1.get(0).getFieldName1()) == 0) {
                              %>
                              | <a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>">Evaluate Bidder/Consultant</a>
                              <%
                                       } }
                                   }
                              %>
                          </td>
                        </tr>
                        <%
                                cnt++;
                                } //END FOR LOOP
                            } else {
                        %>
                        <tr>
                            <td class="t-align-left" colspan="3">
                                <b> No record found! </b>
                            </td>
                        </tr>
                        <% }%>
                     </table>
                      <%
                        if (!companyList.isEmpty()) {
                            if ("cp".equalsIgnoreCase(role)) {
                                 if (!"cl".equalsIgnoreCase(request.getParameter("st"))) {
                                     List<SPTenderCommonData> evalMemStatusCount = tenderCS.returndata("GetEvalMemfinalStatusCount",
                                                                                              tenderId,
                                                                                              "0");
                            // IF THIS COUNT MATCHED WITH BIDDERS COUNT THEN SHOW BELOW BUTTON
                            if (Integer.parseInt(evalMemStatusCount.get(0).getFieldName1()) == companyList.size()) {
                      %>
                      <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                          <th width="55%" class="t-align-center" height="21">Report</th>
                          <th class="t-align-center" width="45%" height="21"><strong>Action</strong></th>
                        </tr>
                        <tr>
                            <td>
                                Technical Evaluation Report
                            </td>
                            <td>
                                <a href="TenderersCompRpt.jsp?tenderid=<%=tenderId%>&st=<%=request.getParameter("st")%>">View</a>
                            </td>
                        </tr>
<!--                        <tr>
                            <td width="33%" class="t-align-left">Generate L1 of Technically Qualified Tenderers </td>
                            <td width="67%" class="t-align-left"><a href="ReportCreation.jsp?tenderid=<//tenderId %>">Create</a></td>
                        </tr>-->
                      </table>
                       <table width="100%" cellspacing="0" class="tableList_1  t_space">
                            <tr>
                                <th  width="58%" class="t-align-center">Report Name</th>
                                <th  width="24%" class="t-align-center">Action</th>
                            </tr>
                            <%
                                    String tendId= request.getParameter("tenderId");
                                    ReportCreationService creationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");
                                    String evalType = creationService.getTenderEvalType(tendId);
                                    String pgName=null;
                                    if(evalType.equalsIgnoreCase("item wise")){
                                        pgName = "ItemWiseReport";
                                    }else{
                                        pgName = "TenderReport";
                                    }
                                    for(Object[] obj : creationService.getRepNameId(tendId)){
                             %>
                            <tr>
                                <td><%=obj[1]%></td>
                                <td class="t-align-center"><a href="<%=pgName%>.jsp?tenderid=<%=tendId%>&repId=<%=obj[0]%>&isEval=y">Price Bid Comparative</a></td>
                            </tr>
                        <%}%>
                     </table>


                        <%
                            boolean isSentToAA = false;

                            List<SPTenderCommonData> lstgetSentToAAStatus = tenderCS.returndata("getSentToAAEntry", tenderId, null);
                            if (!lstgetSentToAAStatus.isEmpty()) {
                                if ("yes".equalsIgnoreCase(lstgetSentToAAStatus.get(0).getFieldName1())) {
                                    isSentToAA = true;
                                }
                            }
                        %>
                        <%if(!isSentToAA){%>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <td>
                                    <a href="TECReport.jsp?tenderId=<%=tenderId%>&st=rp">Send Report to AA</a>
                                </td>
                            </tr>
                        </table>
                    <%}%>

                      <%
                                }
                            } // END IF "EVAL. REPORT" TAB
                        } // END IF "CP" ROLE

                        // 1). HIDE THE BUTTON, IF THE USERID'S MEMBER R0LL IS "cp"
                        // 2). HIDE THE BUTTON, IF ENTRY FOUND IN "tbl_EvalSentQueToCp" TABLE IN CASE OF "tec" MEMBER.
                        if (!"cp".equalsIgnoreCase(role)) {
                            
                            if (isTECCnt == 0) {
                      %>
                    <div class="t-align-center  t_space">
                        <label class="formBtn_1">
                             <input name="btnSendToTEC" type="submit" value="Click here to notify TEC chairperson once you have posted all questions" />
                        </label>
                     </div>
                         <%
                            } // end if
                            List<SPTenderCommonData> evalMemStatusCount = tenderCS.returndata("GetEvalMemStatusCount",
                                                                                              tenderId,
                                                                                              "0");
                            // IF THIS COUNT MATCHED WITH BIDDERS COUNT THEN SHOW BELOW BUTTON
                            if (Integer.parseInt(evalMemStatusCount.get(0).getFieldName1()) == companyList.size()) {

                                List<SPTenderCommonData> evalNotifycnt = tenderCS.returndata("evalNotifycnt",
                                                                                              tenderId,sentBy);
                                if (Integer.parseInt(evalNotifycnt.get(0).getFieldName1()) == 0) {
                         %>
                    <div class="t-align-center  t_space">
                        <label class="formBtn_1">
                             <input name="btnNotifyToTEC" type="submit" value="Notify TEC ChairPerson if evaluation completed" />
                        </label>
                    </div>
                     <% 
                           } } // end if - bidders count func.
                         }
                       } // END IF OF "NO DATA FOUND"
                     %>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
        </form>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
