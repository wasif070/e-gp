<%-- 
    Document   : InitiatePQ
    Created on : Dec 5, 2010, 3:52:44 PM
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Initiate Post Qualification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%  String tenderId;
                    tenderId = request.getParameter("tenderId");
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Initiate Post Qualification</div>
                <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <div>&nbsp;</div>
                

                <%-- Start: Common Evaluation Table --%>
                <% pageContext.setAttribute("tab","7"); %>
                    <%@include file="officerTabPanel.jsp"%>
                    <div class="tabPanelArea_1">
                    <%@include file="/officer/EvalCommCommon.jsp" %>
                <%-- End: Common Evaluation Table --%>

                <%
                            pageContext.setAttribute("TSCtab", "6");
                %>
                <div class="t_space">
                    <%@include  file="../resources/common/AfterLoginTSC.jsp"%>
                </div>
                <div class="tabPanelArea_1">
                    <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <th width="10%" valign="middle" class="t-align-center">Lot No.</th>
                        <th width="37%" valign="middle" class="t-align-center">Lot Description</th>
                        <th width="15%" valign="middle" class="t-align-center">Post Qualification</th>
                    </tr>
                    <%
                               
                                CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                                List<Object[]> list1 = cs.getLotDetails(tenderId);
                                for(Object[] obj : list1){
                    %>
                     <tr>
                         <td class="t-align-center"><%=obj[1].toString()%></td>
                         <td class="t-align-center"><%=obj[2].toString()%></td>
                         <td class="t-align-center">
                        <% 
                        String s="";
                        List<Object[]> listcompany = cs.getCompanyNameAndEntryDate(tenderId,obj[0].toString(),evalCount);
                       for (Object[] obj1 : listcompany) {
                      s= obj1[4].toString();
                        }
                         if(s.equals("Qualify")) {%>
                                   <a href="ProcessPQ.jsp?tenderId=<%=tenderId%>&pkgLotId=<%=obj[0].toString()%>">Post Qualified</a>
                                   <% } else {%>
                                   <a href="ProcessPQ.jsp?tenderId=<%=tenderId%>&pkgLotId=<%=obj[0].toString()%>">Process</a>
                                    <%}%>
                         </td>
                     </tr>
                     <%}%>
                    </table>
                    <div>&nbsp;</div>
                </div>
                </div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
