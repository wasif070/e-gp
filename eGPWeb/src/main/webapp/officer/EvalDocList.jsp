<%-- 
    Document   : EvalDocList
    Created on : Jan 12, 2011, 3:32:21 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
        <%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
        <%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
        <%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
        <%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
        <title>Document Dowload</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="dashboard_div">

            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <div class="contentArea_1">
                <div class="pageHead_1">Download Evaluation Document<span style="float:right;"><a href="<%=request.getHeader("Referer")%>" class="action-button-goback">Go Back</a></span></div>
                <%
                 String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                                tenderId = request.getParameter("tenderId");
                            }
                EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                int evalCount = evalService.getEvaluationNo(Integer.parseInt(request.getParameter("tenderId").toString()));
                pageContext.setAttribute("tenderId", tenderId);
%>
                 <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%
                           
                            String uId = "";
                            String formId = "";
                            
                            if (request.getParameter("formId") != null) {
                                formId = request.getParameter("formId");
                            }
                            if (request.getParameter("uId") != null) {
                                uId = request.getParameter("uId");
                            }

                %>
                 <% for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", uId, "0")) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th colspan="2" class="t_align_left ff">Company Details</th>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Company Name :</td>
                                <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                            </tr>
                        </table>
                        <%
                                    } // END FOR LOOP OF COMPANY NAME
                        %>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="16%">Document Type</th>
                        <th class="t-align-center" width="20%">File Name</th>
                        <th class="t-align-center" width="33%">File Description</th>
                        <th class="t-align-center" width="5%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="8%">Action</th>
                    </tr>
                    <%

                                int docCnt = 0;
                                CommonSearchService CommonSD = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                String userid = session.getAttribute("userId").toString();
                                
                                boolean flag = false;
								//changes by dohatec for reevaluation
                                for (SPCommonSearchData spcsd : CommonSD.searchData("EvalClariDocsCP", tenderId, formId, uId, String.valueOf(evalCount), null, null, null, null, null)) {
                                    docCnt++;

                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                         <td class="t-align-left"><%=spcsd.getFieldName5()%></td>
                        <td class="t-align-left"><%=spcsd.getFieldName1()%></td>
                        <td class="t-align-left"><%=spcsd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(spcsd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ServletEvalClariDocs?docName=<%=spcsd.getFieldName1()%>&docSize=<%=spcsd.getFieldName3()%>&tenderId=<%=tenderId%>&formId=<%=formId%>&funName=download&fs=<%=spcsd.getFieldName7()%>&docType=<%=spcsd.getFieldName5()%>&evalCount=<%=evalCount%>" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                        </td>
                    </tr>
                    <%   if (spcsd != null) {
                                        spcsd = null;
                                    }
                                }//Officer Loop Ends here
                    %>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="6" style="text-align: center; color: red;" >No records found</td>
                    </tr>
                    <%}%>
                </table>
            </div>
        </div>
                
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>