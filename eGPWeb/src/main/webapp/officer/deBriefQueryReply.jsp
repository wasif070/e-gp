<%-- 
    Document   : deBriefQueryReply
    Created on : Jun 17, 2011, 6:39:30 PM
    Author     : dixit
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDebriefdetailDoc"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDebriefing"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderDebriefingService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Post reply</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $("#frmPreTendQuery").validate({
                    rules:{
                        txtReply:{required:true,maxlength:1000},
                        uploadDocFile:{required:function(){var val = $('#documentBrief').val().length;if(val>0){return true;}else{return false;}}},
                        documentBrief:{required:function(){var val = $('#uploadDocFile').val().length;if(val>0){return true;}else{return false;}},maxlength:100}
                    },
                    messages:{
                        txtReply:{required:"<div class='reqF_1'>Please enter query</div>",maxlength: "<div class='reqF_1'>Maximum 1000 characters are allowed.</div>"},
                        uploadDocFile: { required: "<div class='reqF_1'>Please Select Document.</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please Enter Document Description.</div>",maxlength:"<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                    }
                });
            });
            $(function() {
                $('#frmPreTendQuery').submit(function() {
                    if($('#frmPreTendQuery').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpld').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>

    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
                <%                            
                            int tenderId = 1;                            
                            if (request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId"))) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            String defriefid ="";
                            if (request.getParameter("debriefId") != null && !"null".equalsIgnoreCase(request.getParameter("debriefId"))) {
                                defriefid = request.getParameter("debriefId");
                            }
                            TenderDebriefingService tenderDebriefingService = (TenderDebriefingService) AppContext.getSpringBean("TenderDebriefingService");
                            if (session.getAttribute("userId") != null) {
                                   tenderDebriefingService.setLogUserId(session.getAttribute("userId").toString());
                            }
                            List<TblTenderDebriefing> list = tenderDebriefingService.getDebriefData(Integer.parseInt(defriefid));
                %>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">Post reply
                            <span class="c-alignment-right"><a href="deBriefQueryView.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a></span>
                        </div>
                        <%
                                    // Variable tenderId is defined by u on ur current page.
                                    pageContext.setAttribute("tenderId", tenderId);
                        %>
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div>&nbsp;</div>
                        <% pageContext.setAttribute("tab","7"); %>
                        <%@include file="officerTabPanel.jsp"%>
                        <%--<% pageContext.setAttribute("tab", 11);%>
                        <jsp:include page="officerTabPanel.jsp" >
                            <jsp:param name="tab" value="11" />
                        </jsp:include>--%>

                        <div>&nbsp;</div>

                        <form method="post" enctype="multipart/form-data" id="frmPreTendQuery" action="<%=request.getContextPath()%>/deBriefQueryServlet?tenderId=<%=tenderId%>&debriefid=<%=defriefid%>&action=addreply">
                            <input type="hidden" name="refNo" value="<%=toextTenderRefNo%>"/>
                            <div class="tabPanelArea_1">
                                <%
                                        if (request.getParameter("fq") != null)
                                        {
                                        %>

                                        <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=request.getParameter("fq")%></div>
                                        <%
                                        }
                                        if (request.getParameter("fs") != null)
                                        {
                                        %>

                                        <div class="responseMsg errorMsg"  style="margin-top: 10px;">
                                            Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                                        </div>
                                        <%
                                        }

                                        if (request.getParameter("retFlag") != null && request.getParameter("retFlag").equalsIgnoreCase("true"))
                                        {
                                        %>
                                            <div id="succMsg" class="responseMsg successMsg">Topic Posted Successfully.</div>

                                        <%
                                        }
                                        if (request.getParameter("retFlag") != null && request.getParameter("retFlag").equalsIgnoreCase("false"))
                                        {
                                        %>
                                                 <div id="errMsg" class="responseMsg errorMsg">Some Problem occur during Post Topic.</div>

                                        <%
                                        }
                                        %>
                                        <div style="font-style: italic" class="formStyle_1 ff t_space">Fields marked with (<span class="mandatory">*</span>) are mandatory</div>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <td width="16%" class="t-align-left ff">Query :</td>
                                        <td width="84%" class="t-align-left">
                                        <%
                                            if(!list.isEmpty())
                                            {
                                                out.print(list.get(0).getQueryText());
                                            }    
                                        %>
                                        </td>
                                    </tr>                                    
                                </table>
                                <%
                                    List<TblTenderDebriefdetailDoc> listDocs = tenderDebriefingService.getDebriefDetailData(Integer.parseInt(defriefid));
                                    boolean tenside = false;boolean Penside = false;int docCnt = 0;
                                    for (TblTenderDebriefdetailDoc objDoc : listDocs) {
                                    if(objDoc.getUserTypeId()==2)
                                    {
                                        tenside = true;
                                    }
                                    if(objDoc.getUserTypeId()==3)
                                    {
                                        Penside = true;
                                    }
                                }
                                %>
                                <%if(tenside){%>
                                <div class="tableHead_1 t_space" align="left">Tenderers Documents</div>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="4%" class="t-align-left">Sl. No.</th>
                                    <th class="t-align-left" width="23%">File Name</th>
                                    <th class="t-align-left" width="32%">File Description</th>
                                    <th class="t-align-left" width="7%">File Size <br />
                                        (in KB)</th>
                                    <th class="t-align-left" width="28%">File Uploaded by</th>
                                    <th class="t-align-left" width="18%">Action</th>
                                </tr>
                                <%
                                    for (TblTenderDebriefdetailDoc objDoc : listDocs) {
                                        docCnt++;
                                        if(objDoc.getUserTypeId()==2)
                                        {
                                %>
                                        <tr>
                                            <td class="t-align-center"><%=docCnt%></td>
                                            <td class="t-align-left"><%=objDoc.getDocumentName()%></td>
                                            <td class="t-align-left"><%=objDoc.getDocDescription()%></td>
                                            <td class="t-align-center"><%= new BigDecimal(Double.parseDouble(objDoc.getDocSize()) / 1024).setScale(2, 0)%></td>
                                            <td class="t-align-center">
                                                <%
                                                    if (2 == objDoc.getUserTypeId()) {
                                                        out.print("tenderer");
                                                    }else{
                                                        out.print("PE Officer");
                                                    }
                                                %>
                                            </td>
                                            <td class="t-align-center">
                                                <a href="<%=request.getContextPath()%>/deBriefQueryServlet?action=downloadDocument&tenderId=<%=tenderid%>&uploadedBy=<%=objDoc.getUploadedBy()%>&docName=<%=objDoc.getDocumentName()%>&docSize=<%=objDoc.getDocSize()%>&debriefId=<%=objDoc.getKeyId()%>"><img src=<%=request.getContextPath()%>"/resources/images/Dashboard/Download.png" alt="Download" /></a>&nbsp;
                                                <%
                                                    if("2".equalsIgnoreCase(session.getAttribute("userTypeId").toString()))
                                                    {
                                                    %>
                                                    <a href="<%=request.getContextPath()%>/deBriefQueryServlet?action=removeDocument&tenderId=<%=tenderid%>&uploadedBy=<%=objDoc.getUploadedBy()%>&docName=<%=objDoc.getDocumentName()%>&docSize=<%=objDoc.getDocSize()%>&debriefId=<%=objDoc.getKeyId()%>&DetailDocId=<%=objDoc.getDebDetailDocId()%>"><img src=<%=request.getContextPath()%>"/resources/images/Dashboard/Delete.png" alt="Remove" /></a>
                                                    <%
                                                    }
                                                %>
                                            </td>
                                        </tr>
                                <%}}}%>
                                </table>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <td width="16%" class="t-align-left ff">Reply : <span class="mandatory">*</span></td>
                                        <td width="84%" class="t-align-left">
                                            <label>
                                                <textarea name="txtReply" rows="3" class="formTxtBox_1" id="txtReply" style="width:675px;"></textarea>
                                            </label>          </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" nowrap> Upload Document : </td>
                                        <td width="85%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" nowrap>Document Description : </td>
                                        <td>
                                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                            <div id="dvDescpErMsg" class='reqF_1'></div>
                                        </td>
                                    </tr>
                                </table>
                                <div id="docData">
                                </div>
                                <div>&nbsp;</div>
                                <div class="t-align-center">
                                    <label class="formBtn_1">
                                        <input type="submit" name="button3" id="button3" value="Reply" />                                        
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div>&nbsp;</div>

                    <!--Dashboard Content Part End-->
                    <!--Dashboard Footer Start-->
                    <!--Dashboard Footer End-->
                    <%@include file="../resources/common/Bottom.jsp" %>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
