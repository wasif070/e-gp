<%-- 
    Document   : MapCommittee
    Created on : Dec 19, 2010, 2:51:07 PM
    Author     : TaherT
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="appSrBean" class="com.cptu.egp.eps.web.servicebean.APPSrBean" />

<html>
    <html>
        <head>
                    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Use Existing Committee </title>
            <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
            <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
            <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
            <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>            
            <!--jalert -->

            <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
            <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
            <script type="text/javascript">
                $(function() {
                    $('#search').submit(function() {
                        var vbool=true;
                        if($.trim($('#txtSearchVal').val())==""){
                            
                            $('#dis').css("display", "table-row");
                            if($('#cmbSearchBy').val()=="comName"){
                                $('#SearchValError').html('Please enter Committee Name');
                            }else{
                                $('#SearchValError').html('Please enter Tender/Proposal ID');
                            }
                            vbool=false;
                        }else{
                            if($('#cmbSearchBy').val()=="tendId"){                                
                                if(!digits($('#txtSearchVal').val())){
                                     $('#dis').css("display", "table-row");
                                    $('#SearchValError').html('Please enter the Numerals only');
                                    vbool=false;
                        }
                            }
                        }                       
                        return vbool;
                    });
                });
                function digits(value) {
                    return /^\d+$/.test(value);
                }
                $(function() {
                    $('#map').submit(function() {
                        var tcTocViolation=0;
                        var chkCnt=0;
                        var npCnt=0;
                        var npCntEval=0;
                        var npCntTender=0;
                        var cnt=$("#tb2").children().children().length-1;
                        if(cnt>0){
                            for(var i=1;i<=cnt;i++){
                                if($("#chkMap"+i).attr("checked")){
                                    //alert($("#chkMap"+i).attr('tenderid'));
                                    //alert('< %=request.getParameter("type")%>'=='2');
                                    if(('<%=request.getParameter("type")%>'=='2') && ($("#usealltenders2").val().indexOf($("#chkMap"+i).attr('tenderId'), 0) != -1)){
                                        tcTocViolation++;
                                    }
                                    if(('<%=request.getParameter("type")%>'=='2') && ($("#usealltenders").val().indexOf($("#chkMap"+i).attr('tenderId'), 0) != -1)){
                                        npCnt++;
                                    }
                                    else if(('<%=request.getParameter("type")%>'=='1') && ($("#usealltenders").val().indexOf($("#chkMap"+i).attr('tenderId'), 0) != -1)){
                                        npCntEval++;
                                    }
                                    else if(('<%=request.getParameter("type")%>'=='4') && ($("#usealltenders").val().indexOf($("#chkMap"+i).attr('tenderId'), 0) != -1)){
                                        npCntTender++;
                                    }
                                    chkCnt++;
                                }
                            }
                            if(chkCnt==0){
                                jAlert("Please select committee.","Committe Alert", function(RetVal) {
                                });
                                return false;
                            }
                            if(('<%=request.getParameter("type")%>'=='2') && tcTocViolation==0){
                               // jAlert("Opening Committee having Common Member from Evaluation Committee not found.","Committe Alert", function(RetVal) {
                               jAlert("This Opening Committee does not contain any member of the Tender Committee.","Committe Alert", function(RetVal) {
                                });
                                return false;
                            }
                            if(npCnt!=0){
                               // jAlert("Opening Committee having Common Member from Evaluation Committee not found.","Committe Alert", function(RetVal) {
                               jAlert(" One of the TOC member of selected tender is found in this TEC.","Committe Alert", function(RetVal) {
                                });
                                return false;
                            }
                            if(npCntEval!=0){
                               // jAlert("Opening Committee having Common Member from Evaluation Committee not found.","Committe Alert", function(RetVal) {
                               jAlert(" One of the TEC member of selected tender is found in this TOC or TC.","Committe Alert", function(RetVal) {
                                });
                                return false;
                            }
                            if(npCntTender!=0){
                               // jAlert("Opening Committee having Common Member from Evaluation Committee not found.","Committe Alert", function(RetVal) {
                               jAlert(" One of the TC member of selected tender is found in this TEC.","Committe Alert", function(RetVal) {
                                });
                                return false;
                            }
                            
                        }else{
                            jAlert("No committee found.","Committe Alert", function(RetVal) {
                            });
                            return false;
                        }
                    });
                });
                function openInfo(commId){
                    window.open('ViewMember.jsp?commId='+commId,'ViewMembers','menubar=0,scrollbars=1,width=600,height=500')
                }
            </script>
        </head>
        <body>
            <%
                        boolean estFlag = false;
                        String msg=null;
                        String tenderid = request.getParameter("tenderid");
                        String type = request.getParameter("type");
                        java.util.List<SPCommonSearchData> list = new java.util.ArrayList<SPCommonSearchData>();
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                        
                        
                        if(type.equals("4"))
                        {
                                TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                                List<TblTenderLotSecurity> lots = null;
                                int tendid = 0;
                                if (request.getParameter("tenderid") != null) {
                                    tendid = Integer.parseInt(request.getParameter("tenderid"));
                                    lots = tenderDocumentSrBean.getLotDetails(tendid);
                                }


                                String[] maxMin = new String[7];
                                Object list2 = null;
                                Object list3 = null;
                                Object list4 = null;
                                List<Object[]> list5 = new ArrayList<Object[]>();
                                if(request.getParameter("tenderid")!=null)
                                {
                                    list2 = tenderDocumentSrBean.getPkgNo(request.getParameter("tenderid").toString());
                                    list3 = tenderDocumentSrBean.getDptId(request.getParameter("tenderid").toString());
                                    list5 = tenderDocumentSrBean.getGivenEst(request.getParameter("tenderid").toString());
                                    if(list3!=null)
                                    {
                                        list4 = tenderDocumentSrBean.getDptType(list3.toString());
                                    }
                                    if(list4!=null)
                                    {
                                        maxMin = appSrBean.getMinMax(list2.toString(),list4.toString());
                                    }
                                }
                                BigDecimal tableValue = new BigDecimal(maxMin[0].toString());
                                Iterator itr=list5.iterator();
                                while(itr.hasNext()){  
                                    BigDecimal givenValue = new BigDecimal(itr.next().toString());
                                    int estResult = givenValue.compareTo(tableValue);
                                    if(estResult == 1)
                                    {
                                        estFlag = true;
                                    }
                               } 
                        }

                        
                        
                        
                        
                        if ("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {
                            String committeeType = null;
                            String tendId = "";
                            String comName = "";
                            if ("tendId".equalsIgnoreCase(request.getParameter("searchBy"))) {
                                tendId = request.getParameter("searchVal");
                            } else if ("comName".equalsIgnoreCase(request.getParameter("searchBy"))) {
                                comName = request.getParameter("searchVal");
                            }
                            if (type.equals("1")) {
                                committeeType = "'TEC','PEC'";
                            } else if (type.equals("2")) {
                                committeeType = "'TOC','POC'";
                            } else if (type.equals("3")) {
                                committeeType = "'TSC'";
                            } else if(type.equals("4")) {
                                committeeType = "'TC', 'PC'";
                                
                            }                           
                            list = commonSearchService.searchData("SearchCommittee", tenderid, committeeType, tendId, comName, session.getAttribute("userId").toString(), null, null, null, null);
                        }
                        if ("Submit".equalsIgnoreCase(request.getParameter("map"))) {
                            SPCommonSearchData data;
                            if(type.equals("1")){
                                data = commonSearchService.searchData("DumpCommitteeEval", request.getParameter("commId"), tenderid, session.getAttribute("userId").toString(), type, null, null, null, null, null).get(0);
                            }
                            else if(type.equals("4"))
                            {
                                data = commonSearchService.searchData("DumpTenderCommittee", request.getParameter("commId"), tenderid, session.getAttribute("userId").toString(), type, null, null, null, null, null).get(0);
                            }
                            else if(type.equals("2"))
                            {
                                data = commonSearchService.searchData("DumpCommitteeOpen", request.getParameter("commId"), tenderid, session.getAttribute("userId").toString(), type, null, null, null, null, null).get(0);
                            }
                            else{    
                                data = commonSearchService.searchData("DumpCommittee", request.getParameter("commId"), tenderid, session.getAttribute("userId").toString(), type, null, null, null, null, null).get(0);
                            }
                            String action = null;
                            String commType = null;
                            String pageName=null;
                            if (type.equals("1")) {
                                pageName="EvalComm";
                                commType = "Evaluation";
                            } else if (type.equals("2")) {
                                pageName="OpenComm";
                                commType = "Opening";
                            } else if (type.equals("3")) {
                                pageName="EvalComm";
                                commType = "Technical Sub";
                            }
                            else if(type.equals("4"))
                            {
                                pageName="TenderComm";
                                commType="Tender";
                            }
                            if(data.getFieldName1().equals("0")){
                                msg="Committee couldn't be mapped";
                                action = "Error in Use Existing "+commType+" Committee";
                            }else{
                                action =  "Use Existing "+commType+" Committee";
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(tenderid), "tenderId", type.equals("2") ? EgpModule.Opening.getName() : EgpModule.Evaluation.getName(), action, "");
                                response.sendRedirect(pageName+".jsp?tenderid="+tenderid);
                            }
                        }
            %>
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <%
                        pageContext.setAttribute("tenderId", tenderid);
            %>
            <div class="pageHead_1">Search and Use Existing Committee<span style="float: right;"><a class="action-button-goback" href="<%if (type.equals("1")) {out.print("EvalComm.jsp?tenderid="+tenderid);} else if (type.equals("2")) {out.print("OpenComm.jsp?tenderid="+tenderid);} else if (type.equals("3")) {out.print("EvalComm.jsp?tenderid="+tenderid);}else if (type.equals("4")) {out.print("TenderComm.jsp?tenderid="+tenderid);}%>">Go back to Dashboard</a></span></div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>            
            <br/>
            <%if(msg!=null){%>
            <div class="responseMsg errorMsg"><%=msg%></div><br/>
            <%}%>
            <%if(estFlag){%><div class="responseMsg noticeMsg" style="color:brown; font-size: 15px;"><b>As the Official Cost Estimate is greater than threshold value, you are instructed to select TC members from higher level PA office.</div><br><%}%>
                    
            <div class="formBg_1">
                <form action="MapCommittee.jsp?tenderid=<%=tenderid%>&type=<%=type%>" method="post" id="search">
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td class="ff" width="10%">Search by   : <span>*</span></td>
                            <td width="90%"><select name="searchBy" class="formTxtBox_1" id="cmbSearchBy" style="width: 200px;">
                                    <option value="comName">Committee Name</option>
                                    <option value="tendId">Tender ID</option>
                                </select>&nbsp;
                                <input name="searchVal" type="text" class="formTxtBox_1" id="txtSearchVal" style="width:200px;"/>&nbsp;
                                <label class="formBtn_1">
                                    <input type="submit" name="btnSearch" id="btnSearch" value="Search" />
                                </label>
                            </td>
                        </tr>
                        <tr id="dis" style="display: none;"><td width="10%">&nbsp;</td><td class="t-align-left"><span id="SearchValError" class="reqF_1"></span></td></tr>
                    </table>                    
                </form>
            </div>
            <form action="MapCommittee.jsp?tenderid=<%=tenderid%>&type=<%=type%>" method="post" id="map">
                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tb2">
                    <tr>
                        <th width="6%" class="t-align-left">Select</th>
                        <th width="4%" class="t-align-left">Sl. No. <br /></th>
                        <th width="20%" class="t-align-left">Tender ID</th>
                        <th width="56%" class="t-align-left">Committee Name</th>
                        <th width="14%" class="t-align-left" >View Committee Details</th>
                    </tr>
                    <%if ("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {if(list.isEmpty()){%>
                    <tr>
                        <td colspan="5" class="t-align-center"><span style="color: red;">No records found as per the business rule configured</span></td>
                    </tr>
                    <%}}%>
                    <%
                                int i = 1;
                                StringBuilder tenderIds = new StringBuilder();
                                StringBuilder usetenderIds = new StringBuilder();
                                StringBuilder usetenderIds2 = new StringBuilder();
                                //out.print("taher _ "+list.size());
                                for (SPCommonSearchData data : list) {
                                    tenderIds.append(data.getFieldName2()+",");
                    %>
                    <tr class="<%if(i%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                        <td class="t-align-center">
                            <input type="radio" name="commId" id="chkMap<%=i%>" value="<%=data.getFieldName1()%>" tenderId="<%=data.getFieldName2()%>"/>
                        </td>
                        <td class="t-align-center"><%=i%></td>
                        <td class="t-align-center"><%=data.getFieldName2()%></td>
                        <td class="t-align-center"><%=data.getFieldName3()%></td>
                        <td class="t-align-center"><a href="javascript:void(0);" onclick="openInfo(<%=data.getFieldName1()%>);">View</a></td>
                    </tr>
                    <%
                        i++;
                       }
                          
                          if(type.equals("2") && tenderIds.length()>0){
                               list = commonSearchService.searchData("TC_TOC_Violation", tenderIds.substring(0, tenderIds.length()-1), tenderid, null, null,null,null, null, null, null);
                               for (SPCommonSearchData data : list) {
                                    usetenderIds2.append(data.getFieldName1()+",");
                               }
                           }
                          if(type.equals("2") && tenderIds.length()>0){
                               list = commonSearchService.searchData("OpenCommitteeDumpCheck", tenderIds.substring(0, tenderIds.length()-1), tenderid, null, null,null,null, null, null, null);
                               for (SPCommonSearchData data : list) {
                                    usetenderIds.append(data.getFieldName1()+",");
                               }
                           }

                           if(type.equals("1") && tenderIds.length()>0){
                               list = commonSearchService.searchData("EvalCommitteeDumpCheck", tenderIds.substring(0, tenderIds.length()-1), tenderid, null, null,null,null, null, null, null);
                               for (SPCommonSearchData data : list) {
                                    usetenderIds.append(data.getFieldName1()+",");
                               }
                           }
                           
                           if(type.equals("4") && tenderIds.length()>0){
                               list = commonSearchService.searchData("TenderCommitteeDumpCheck", tenderIds.substring(0, tenderIds.length()-1), tenderid, null, null,null,null, null, null, null);
                               for (SPCommonSearchData data : list) {
                                    usetenderIds.append(data.getFieldName1()+",");
                               }






                           }
                        //out.print("taher _ "+tenderIds);
                    %>
                </table>                
                <input type="hidden" name="alltenders" id="alltenders" value="<%=(tenderIds.length()>0) ? tenderIds.substring(0, tenderIds.length()-1) : tenderIds %>"/>                
                <input type="hidden" name="usealltenders" id="usealltenders" value="<%=(usetenderIds.length()>0) ? usetenderIds.substring(0, usetenderIds.length()-1) : usetenderIds%>"/>
                <input type="hidden" name="usealltenders2" id="usealltenders2" value="<%=(usetenderIds2.length()>0) ? usetenderIds2.substring(0, usetenderIds2.length()-1) : usetenderIds2%>"/>
                <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input name="map" type="submit" value="Submit" id="btnMap"/>
                    </label>
                </div>
            </form>
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        </body>
        <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
    </html>
