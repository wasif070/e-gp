<%-- 
    Document   : TenderFormsList
    Created on : Dec 22, 2010, 10:21:10 AM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Forms</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
         <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%
        String thisTenderId="", thisLotId="" ,memberRole="", thisUserId="";
        boolean isCurTenLotWise=false, isCurTenPackageWise = false;
        boolean isMemberDecrypter = false;
        thisTenderId = request.getParameter("tid");


         if (session.getAttribute("userId") != null) {
            thisUserId = session.getAttribute("userId").toString();
        }

        TenderCommonService tenderCommonServiceFrm = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

         List<SPTenderCommonData> listTenderTyp = tenderCommonServiceFrm.returndata("gettenderlotbytenderid", thisTenderId, null);

            if (!listTenderTyp.isEmpty()) {
                String tenderType = "";
                tenderType = listTenderTyp.get(0).getFieldName1();

                if (tenderType.equalsIgnoreCase("package")) {
                    isCurTenPackageWise = true;
                } else {
                    isCurTenLotWise = true;
                }
            }

            if (request.getParameter("lotId") != null) {
               if ( !"".equalsIgnoreCase(request.getParameter("lotId")) && !"null".equalsIgnoreCase(request.getParameter("lotId"))){
                   thisLotId=  request.getParameter("lotId");
               } 
            }

          /*START : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/
            List<SPTenderCommonData> lstUserInfo = tenderCommonServiceFrm.returndata("getUserInfoForOpeningProcess", thisTenderId, thisUserId);
            if (!lstUserInfo.isEmpty() && lstUserInfo.size()>0){
                
                memberRole = lstUserInfo.get(0).getFieldName1();
                if ("yes".equalsIgnoreCase(lstUserInfo.get(0).getFieldName2())){
                            isMemberDecrypter = true;
                }
            }
            /*END : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/
%>

        <% if (isCurTenLotWise && thisLotId!="") {%>
        <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
            <tr>
                <th width="58%" class="ff">Form Name </th>
                <th width="24%">Action</th>
            </tr>
        <%
    List<SPTenderCommonData> formList = tenderCommonServiceFrm.returndata("getformnamebytenderidandlotid",thisTenderId,thisLotId);
    if (!formList.isEmpty()) {
        for (SPTenderCommonData formname : formList) {
        String CurrentFormDecrypted="";
            List<SPTenderCommonData> lstFormInfo = tenderCommonServiceFrm.returndata("getFormInfoForOpeningProcess", formname.getFieldName5(), null);

            if (!lstFormInfo.isEmpty() && lstFormInfo.size()>0){
                CurrentFormDecrypted= lstFormInfo.get(0).getFieldName1();
            }
    %>
        <tr>
            <td class="ff"><p><strong><%=formname.getFieldName1()%></strong></p>
            </td>
            <td>
                 <% if (formname.getFieldName8().equalsIgnoreCase("yes")) {
                                // Decryption needed
                                %>
                                    <%if ("yes".equalsIgnoreCase(CurrentFormDecrypted)){%>
                                        <%
                                                boolean isMultipleFillingYes = false;
                                                List<SPTenderCommonData> mulitiList =  tenderCommonServiceFrm.returndata("isCompReportMultiFilling",formname.getFieldName5(), null);
                                                    if(mulitiList!=null && (!mulitiList.isEmpty())){
                                                        isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                                                }
                                            %>
                                            <a href="<%=isMultipleFillingYes ? "SrvComReport.jsp" : "ComReport.jsp"%>?formId=<%=formname.getFieldName5()%>&tenderId=<%=thisTenderId%>">Comparative Report</a>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="IndReport.jsp?tenderId=<%=thisTenderId%>&formId=<%=formname.getFieldName5()%>">Individual Report</a>
                                     <%} else {%>
                                        <% if (isMemberDecrypter) {%>
                                        <a href="Decrypt.jsp?tenderId=<%=thisTenderId%>&formId=<%=formname.getFieldName5()%>">Decrypt Report</a>
                                        <%} else {%>
                                            Decryption pending
                                        <%}%>
                                     <%}%>
                                <%} else {
                                  // Decryption not needed
                                %>
                                  <%
                                                boolean isMultipleFillingYes = false;
                                               List<SPTenderCommonData> mulitiList =  tenderCommonServiceFrm.returndata("isCompReportMultiFilling",formname.getFieldName5(), null);
                                                    if(mulitiList!=null && (!mulitiList.isEmpty())){
                                                        isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                                                }
                                            %>
                                            <a href="<%=isMultipleFillingYes ? "SrvComReport.jsp" : "ComReport.jsp"%>?formId=<%=formname.getFieldName5()%>&tenderId=<%=thisTenderId%>">Comparative Report</a>
                                   &nbsp;&nbsp;|&nbsp;&nbsp;
                                  <a href="IndReport.jsp?tenderId=<%=thisTenderId%>&formId=<%=formname.getFieldName5()%>">Individual Report</a>
                                <%}%>
            </td>
        </tr>
        <%}
    } else {%>
     <tr>
        <td class="t-align-left" colspan="2">No forms found!</td>
    </tr>
    <%}
%>
        </table>
<%} else if (isCurTenPackageWise){%>
<table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
            <tr>
                <th width="58%" class="ff">Form Name </th>
                <th width="24%">Action</th>
            </tr>
        <%
    List<SPTenderCommonData> formList = tenderCommonServiceFrm.returndata("getformnamebytenderidandlotid",thisTenderId,"0");
    if (!formList.isEmpty()) {
        for (SPTenderCommonData formname : formList) {
            String CurrentFormDecrypted="";
            List<SPTenderCommonData> lstFormInfo = tenderCommonServiceFrm.returndata("getFormInfoForOpeningProcess", formname.getFieldName5(), null);

            if (!lstFormInfo.isEmpty() && lstFormInfo.size()>0){
                CurrentFormDecrypted= lstFormInfo.get(0).getFieldName1();
            }

    %>
        <tr>
            <td class="ff"><p><strong><%=formname.getFieldName1()%></strong></p>
            </td>
            <td style="text-align: center;">
                        <%
                            List<SPTenderCommonData> formList1 = tenderCommonServiceFrm.returndata("FindFormInTenderBidPlainData",formname.getFieldName5(),null);
                            if (!formList1.isEmpty()) {
                        %>
                                  <%
                                                boolean isMultipleFillingYes = false;
                                                List<SPTenderCommonData> mulitiList =  tenderCommonServiceFrm.returndata("isCompReportMultiFilling",formname.getFieldName5(), null);
                                                    if(mulitiList!=null && (!mulitiList.isEmpty())){
                                                        isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                                                }
                                            %>
                                            <a href="<%=isMultipleFillingYes ? "SrvComReport.jsp" : "ComReport.jsp"%>?formId=<%=formname.getFieldName5()%>&tenderId=<%=thisTenderId%>">Comparative Report</a>
                                   &nbsp;&nbsp;|&nbsp;&nbsp;
                                  <a href="IndReport.jsp?tenderId=<%=thisTenderId%>&formId=<%=formname.getFieldName5()%>">Individual Report</a>
                         <%} else{%>
                         No Bidder has filled this form
                         <%}%>
                 <%--<% if (formname.getFieldName8().equalsIgnoreCase("yes")) {
                                // Decryption needed
                                %>
                                    <%if ("yes".equalsIgnoreCase(CurrentFormDecrypted)){%>
                                        <a href="ComReport.jsp?formId=<%=formname.getFieldName5()%>&tenderId=<%=thisTenderId%>">Comparative</a>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="IndReport.jsp?tenderId=<%=thisTenderId%>&formId=<%=formname.getFieldName5()%>">Individual</a>
                                     <%} else {%>
                                        <% if (isMemberDecrypter) {%>
                                        <a href="Decrypt.jsp?tenderId=<%=thisTenderId%>&formId=<%=formname.getFieldName5()%>">Decrypt</a>
                                        <%} else {%>
                                            Decryption pending
                                        <%}%>
                                     <%}%>
                                <%} else {
                                  // Decryption not needed
                                %>
                                  <a href="ComReport.jsp?formId=<%=formname.getFieldName5()%>&tenderId=<%=thisTenderId%>">Comparative</a>
                                   &nbsp;&nbsp;|&nbsp;&nbsp;
                                  <a href="IndReport.jsp?tenderId=<%=thisTenderId%>&formId=<%=formname.getFieldName5()%>">Individual</a>
                                <%}%>--%>
            </td>
        </tr>
        <%}
    } else {%>
     <tr>
        <td class="t-align-left" colspan="2">No forms found!</td>
    </tr>
    <%}
%>
        </table>
<%}%>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
