<%--
    Document   : RepeatOrder
    Created on : Jul 30, 2011, 11:18:11 AM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsDateConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPayment"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    boolean isPerformanceSecurityPaid = false;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Repeat Order</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Repeat Order</div>
                <%
                            String tenderId = "";
                            if (request.getParameter("tenderId") != null) {
                                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                                tenderId = request.getParameter("tenderId");
                            }
                            String userId = "";
                            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                userId = session.getAttribute("userId").toString();
                                issueNOASrBean.setLogUserId(userId);
                            }
                            pageContext.setAttribute("tab", "14");

                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>

                <%@include  file="officerTabPanel.jsp"%>
                    <div class="tabPanelArea_1">
                    <%
                        pageContext.setAttribute("TSCtab", "5");

                    %>
                    <%@include  file="../resources/common/CMSTab.jsp"%>
                    <div class="tabPanelArea_1">
                    <div align="center">
                        <%for (Object[] obj : issueNOASrBean.getNOAListing(Integer.parseInt(tenderId))) {
                                        List<TblTenderPayment> detailsPayment = issueNOASrBean.getPaymentDetails(Integer.parseInt(tenderId), (Integer) obj[0], (Integer) obj[8]);
                                        SimpleDateFormat simpl = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                                        if (issueNOASrBean.isAvlTCS((Integer) obj[1])) {
                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                    CommonService commonserv = (CommonService) AppContext.getSpringBean("CommonService");
                                    Object ProcurementNature = commonserv.getProcNature(tenderId);
                                    List<TblCmsDateConfig> configdatalist = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId),Integer.parseInt(obj[0].toString()));
                                    boolean flagg = service.checkForItemFullyReceivedOrNot(Integer.parseInt(obj[0].toString()));
                                    boolean editFlag = false;
                                    if(!configdatalist.isEmpty())
                                    {
                                        editFlag = true;
                                    }
                                    boolean flag = service.checkContractSigning(tenderId, Integer.parseInt(obj[0].toString()));
                        %>

                        <table cellspacing="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td class="ff" width="12%" class="t-align-left">Lot No.</td>
                                <td   class="t-align-left"><%=obj[6]%></td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left">Lot Description</td>
                                <td  class="t-align-left"><%=obj[7]%></td>
                            </tr>
                        </table>
                        <br />
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="15%" class="t-align-left">Name of Contractor </th>
                                <th width="10%" class="t-align-left">Contract No.</th>
                                <th width="10%" class="t-align-left">Contract Name</th>
                                <th width="10%" class="t-align-left">Action</th>
                            </tr>
                            <tr>
                                <td style="text-align: left;"><%=obj[9]%></td>
                                <td class="t-align-center" > <%=obj[2]%></td>
                                <td class="t-align-center" > <%=obj[7]%></td>
                                <td class="t-align-center">
                                <%
if("goods".equalsIgnoreCase(ProcurementNature.toString()) && flagg){
%>
 <a href="RepeatOrderNext.jsp?pckLotId=<%=obj[0].toString()%>&tenderId=<%=tenderId%>">Repeat Order</a>
<%}else{%>
-
<%}%>
                                </td>
                            </tr>
                        </table>
                        <%}
                                        }%>
                    </div>
                </div></div>
            </div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
<%
            issueNOASrBean = null;
%>