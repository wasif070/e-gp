
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPEvalCommonSearchData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.EvalCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%--
    Document   : EvalComm
    Created on : Nov 16, 2010, 11:06:27 AM
    Author     : TaherT,Rajesh Singh,rishita
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderOpening"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.web.servicebean.EvaluationMatrix"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");

            String userId = "";
            HttpSession hs = request.getSession();
            if (hs.getAttribute("userId") != null) {
                userId = hs.getAttribute("userId").toString();
            }
            boolean IsFinancialReportDecrypted = false;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation Process</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <!--jalert -->

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" >

            function checkTender() {
                jAlert("there is no Tender Notice", " Alert ", "Alert");
            }


            function checkdefaultconf() {
                jAlert("Default workflow configuration hasn't crated", " Alert ", "Alert");
            }
            function checkDates() {
                jAlert(" please configure dates", " Alert ", "Alert");
            }

        </script>
    </head>
    <body>

        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">
            <div class="pageHead_1">Evaluation Process</div>

            <%                String evalTendId = request.getParameter("tenderid");
                // Variable tenderId is defined by u on ur current page.
                pageContext.setAttribute("tenderId", evalTendId);
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>

            <div>&nbsp;</div>
            <% pageContext.setAttribute("isTenPackageWise", (Boolean) isTenPackageWise); %>
            <% pageContext.setAttribute("tab", "7");%>

            <div>
                <%@include file="officerTabPanel.jsp"%>
            </div>


            <div class="tabPanelArea_1">
                <%-- Start: CODE TO DISPLAY MESSAGES --%>
                <%                String com = "";
                    boolean IsAllFormDecrypted = false;
                    boolean IsDecryptionRequiredByTECCP = true;
                    if (request.getParameter("comType") != null) {
                        com = request.getParameter("comType");
                    }
                    String msg = null;
                    String msgOFNominee = null;
                    String msgs = null;
                    int eCount = 0;
                    EvaluationService evalService1 = (EvaluationService) AppContext.getSpringBean("EvaluationService");

                    eCount = evalService1.getEvaluationNo(Integer.parseInt(evalTendId));

                    if (request.getParameter("msg") != null) {
                        msg = request.getParameter("msg");
                    }

                    if (request.getParameter("msgOfNominee") != null) {
                        msgOFNominee = request.getParameter("msgOfNominee");
                    }

                    if (request.getParameter("msgs") != null) {
                        msgs = request.getParameter("msgs");
                    }

                %>
                <%if (request.getParameter("infomsg") != null) {%>
                <div class="responseMsg successMsg">Official Cost Estimate Entered Successfully</div>
                <%                            }%>
                <%if (request.getParameter("flag") != null && request.getParameter("flag").equals("true")) {%>
                <div id="successMsg" class="responseMsg successMsg" style="margin-bottom: 12px;">Declaration given successfully</div>
                <%}%>
                <%if (request.getParameter("domPreMsg") != null && request.getParameter("domPreMsg").equals("sucess")) {%>
                <div id="successMsg" class="responseMsg successMsg" style="margin-bottom: 12px;">Domestic preference configured successfully</div>
                <%}%>

                <% if (msgs != null && msgs.contains("trues")) {%>
                <div id="successMsg" class="responseMsg successMsg" style="margin-bottom: 12px;">Max Mark submitted successfully for Evaluation Clarification</div>
                <%  }%>

                <%if (msgOFNominee != null && msgOFNominee.contains("sucess")) {%>
                <div class="responseMsg successMsg" style="margin-bottom: 12px;">Nomination done Successfully!</div>
                <%} else if (msgOFNominee != null && msgOFNominee.contains("fail")) {%>
                <div id="successMsg" class="responseMsg errorMsg" style="margin-bottom: 12px;">Nomination not done Successfully</div>
                <%}%>

                <% if (msg != null && !msg.contains("not")) {%>
                <div id="successMsg" class="responseMsg successMsg t_space" style="margin-bottom: 12px;">File processed successfully</div>
                <%  } else if (msg != null && msg.contains("not")) {%>
                <div id="successMsg" class="responseMsg errorMsg" style="margin-bottom: 12px;">File has not processed successfully</div>
                <%  }%>

                <%if ("y".equals(request.getParameter("ispub"))) {%> <div class="responseMsg successMsg" style="margin-bottom: 12px;">Committee members notified successfully</div><%}%>
                <!--Committee published successfully(msg changed)-->

                <%if (request.getParameter("msgId") != null) {
                        String msgId = "", msgTxt = "";
                        boolean isError = false;
                        msgId = request.getParameter("msgId");
                        if (!msgId.equalsIgnoreCase("")) {
                            if (msgId.equalsIgnoreCase("mapped")) {
                                msgTxt = "Mapping done successfully.";
                            } else if (msgId.equalsIgnoreCase("configdone")) {
                                msgTxt = "Configuration done successfully.";
                            } else if (msgId.equalsIgnoreCase("configfailed")) {
                                isError = true;
                                msgTxt = "Error while doing Evaluation Configuration";
                            } else if (msgId.equalsIgnoreCase("tscrequested")) {
                                msgTxt = "Technical Sub Committee formation request sent successfully.";
                            } else if (msgId.equalsIgnoreCase("tscpublished")) {
                                msgTxt = "Technical Sub Committee published successfully.";
                            } else if (msgId.equalsIgnoreCase("error")) {
                                isError = true;
                                msgTxt = "There was some error.";
                            } else if (msgId.equalsIgnoreCase("error")) {
                                isError = true;
                                msgTxt = "There was some error.";
                            } else if (msgId.equalsIgnoreCase("configchanged")) {
                                msgTxt = "Configuration Modified successfully.";
                            } else if (msgId.equalsIgnoreCase("senttopa")) {
                                msgTxt = "Sent to PA/AU Successfully.";
                            } else if (msgId.equalsIgnoreCase("senttotc")) {
                                msgTxt = "Sent to Tender Committee Successfully.";
                            } else if (msgId.equalsIgnoreCase("approved")) {
                                msgTxt = "Evaluation Report Approved.";
                            } else {
                                msgTxt = "";
                            }
                %>
                <%if (isError) {%>
                <div class="responseMsg errorMsg" id="successMsg" style="margin-bottom: 12px;" ><%=msgTxt%></div>
                <%} else {%>
                <div class="responseMsg successMsg" id="successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                <%}%>


                <%}
                        msgId = null;
                        msgTxt = null;
                    }%>

                <%-- End: CODE TO DISPLAY MESSAGES --%>
            </div>

            <%if (isCurUserPE || isCurUserAU || (isAAMinister && (isCurUserHope || isCurUserSecretory))) {%>
            <table width="100%" cellspacing="0" class="tableList_1">
                <%if (isCurUserPE || isCurUserAU) {%>
                <tr>
                    <td width="30%" class="t-align-left ff">Advertisement</td>
                    <td width="80%" class="t-align-left">
                        <a href="<%=request.getContextPath()%>/officer/TenderAdvList.jsp?tenderId=<%=tenderid%>">View</a>
                    </td>
                </tr>
                <%}%>
                <%if (isCurUserPE || (isAAMinister && (isCurUserHope || isCurUserSecretory))) {

                        List<Object[]> viewEvalRptList1 = evalService1.forReprotProcessView(Integer.parseInt(tenderid), -1);
                        if (viewEvalRptList1 != null & !viewEvalRptList1.isEmpty()) {
                %>
                <tr>
                    <td width="30%" class="t-align-left ff">Evaluation Report</td>
                    <td width="80%" class="t-align-left">
                        <a href="<%=request.getContextPath()%>/officer/ViewEvalReportListing.jsp?tenderid=<%=tenderid%>">View</a>
                    </td>
                </tr>
            </table>
            <%}
                        }
                    }%>

            <%-- Start: Common Evaluation Table --%>
            <%@include file="/officer/EvalCommCommon.jsp" %>
            <%                    List<SPTenderCommonData> listDomesticPre
                        = tenderCommonService.returndata("getTORReceivedData", tenderid, session.getAttribute("userId").toString());
                if (!listDomesticPre.isEmpty() && listDomesticPre.get(0).getFieldName1().equalsIgnoreCase("yes") && listDomesticPre.get(0).getFieldName2().equalsIgnoreCase("yes") && listDomesticPre.get(0).getFieldName3().equalsIgnoreCase("no") && listDomesticPre.get(0).getFieldName4().equalsIgnoreCase("yes")) {
                    List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreferenceAfterTER2", tenderid, null);
                    if (!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")) {
            %>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <td width="30%" class="t-align-left ff">Domestic Preference</td>
                    <td width="80%" class="t-align-left">
                        <a href="<%=request.getContextPath()%>/officer/ConfigureDomesticPreference.jsp?tenderId=<%=tenderid%>">Configure</a>
                        <%
                            committeMemberService.setLogUserId(userid);
                            if (committeMemberService.getTblTenderDomesticPref(Integer.parseInt(tenderid)) != 0) {
                        %>
                        &nbsp; | &nbsp;
                        <a  onclick="javascript:window.open('<%=request.getContextPath()%>/officer/ViewDomesticPreference.jsp?tenderId=<%=tenderid%>', '', 'resizable=yes,scrollbars=1', '');" href="javascript:void(0);">View</a>
                        <% } %>
                    </td>
                </tr>
            </table>
            <% }
                }
            %>
            <%-- End: Common Evaluation Table --%>
            <%
                //boolean  isUserEvalMember = false, isUserEvalTSCMember = false;
                String peId = "";
                String usid = session.getAttribute("userId").toString();

                List<SPTenderCommonData> listPe = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", evalTendId, null);
                //List<SPTenderCommonData> CheckProcurementNature = tenderCommonService.returndata("tenderinfobarMore", tenderid, null);
                if (!listPe.isEmpty()) {
                    peId = listPe.get(0).getFieldName1();
                }

                listPe = null;
                List<SPTenderCommonData> tenStatus
                        = tenderCommonService1.returndata("Checktenderpublishstatus", evalTendId, null);
                boolean isTenPublish = false;
                if (!tenStatus.isEmpty()) {
                    if (tenStatus.get(0).getFieldName1().equalsIgnoreCase("approved")) {
                        isTenPublish = true;
                    }
                }
                tenStatus.clear();
                tenStatus = null;
                // remove the checking of pe to enable AU/TEC user as initiator in TSC workflow. HDID 9467     
                // if(usid.equalsIgnoreCase(peId)){
                if (isTSCReq && isTenPublish) {
            %>
            <%--<jsp:include page="EvalCommTSC.jsp" >
                <jsp:param name="tenderid" value="<%=evalTendId%>" />
                <jsp:param name="tsedit" value="y" />
            </jsp:include>--%>
            <%}/*Tsc Formation is true for this tenderId*///}/*cheking for Pe*/%>
            <%
                boolean showTSCSubMenu = false;

                List<SPTenderCommonData> lstGetTenderTECInfo
                        = tenderCommonService1.returndata("getTenderTECInfo", evalTendId, null);

                if (!lstGetTenderTECInfo.isEmpty()) {
                    if (isCmnConfigTeamWise) {
                        if (!"0".equalsIgnoreCase(lstGetTenderTECInfo.get(0).getFieldName1()) && !"0".equalsIgnoreCase(lstGetTenderTECInfo.get(0).getFieldName2())) {
                            if (lstGetTenderTECInfo.get(0).getFieldName2().equalsIgnoreCase(lstGetTenderTECInfo.get(0).getFieldName1())) {
                                if (isUserEvalMember || isUserEvalTSCMember) {
                                    showTSCSubMenu = true;
                                }
                            }
                        }
                    } else if (isCmnConfigIndWise) {
                        if (isUserEvalMember || isUserEvalTSCMember) {
                            showTSCSubMenu = true;
                        }
                    }
                }

                lstGetTenderTECInfo = null;
            %>

            <%
                showTSCSubMenu = true;
                if (showTSCSubMenu) {%>
            <!-- Start: Show Sub Menu and Related Content-->
            <%-- Sub Panal --%>
            <%          //TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                pageContext.setAttribute("TSCtab", "1");

                pageContext.setAttribute("comType", com);

                List<SPTenderCommonData> getTenderStatus
                        = tenderCommonService1.returndata("Checktenderpublishstatus", evalTendId, null);

                if (!getTenderStatus.isEmpty()) {

                    if (getTenderStatus.get(0).getFieldName1().equalsIgnoreCase("approved")) {

                        isTenPublish = true;
                        //This is to check TOS is send to TEC
                        List<SPTenderCommonData> TOStoTEC
                                = tenderCommonService1.returndata("getTOSReportStatus", evalTendId, null);

                        if (!TOStoTEC.isEmpty()) {

                            if (TOStoTEC.get(0).getFieldName1().equalsIgnoreCase("yes")) {

                                List<SPTenderCommonData> getConfigStatus
                                        = tenderCommonService1.returndata("getEvalConfigType", evalTendId, null);

                                if (getConfigStatus != null) {

                                    if (getConfigStatus.size() > 0) {

                                        // if(getConfigStatus.get(0).getFieldName2().equalsIgnoreCase("yes")){

            %>
            <%--  <div class="t_space">
                  <%@include  file="../resources/common/AfterLoginTSC.jsp"%>                    
</div>--%>

            <%                                            //}
                                    }
                                }
                                getConfigStatus = null;
                            }
                        }
                        TOStoTEC = null;

                    }
                }
                getTenderStatus = null;

            %>

            <div class="tabPanelArea_1">
                <%                    //TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    List<SPCommonSearchDataMore> listReportID = null;
                    String reportID = "0";
                    String tenderLot = "0";
                    String roundID = "0";
                    boolean isRptSentToTC = false;
                    boolean isRptSentForLoi = false;
                    boolean isRptSentToTEC = false;
                    boolean isCRSaved = false;

                    lstTEC_CPInfo = tenderCommonServiceCommon.returndata("EvalTECChairPerson_Info", tenderid, "");
                    if (!lstTEC_CPInfo.isEmpty()) {
                        userId_TEC_CP = lstTEC_CPInfo.get(0).getFieldName1();
                    }
                    String userId_TC = "";
                    boolean IsUserTCMember = false;
                    List<SPTenderCommonData> lstTC_CPInfo = tenderCommonServiceCommon.returndata("TCMember", tenderid, "");
                    if (!lstTEC_CPInfo.isEmpty()) {
                        if (lstTC_CPInfo.get(0).getFieldName3().equals("cp")) {
                            userId_TC = lstTC_CPInfo.get(0).getFieldName1(); // Get TC/PC Chairperson UserId      
                        }
                    }

                    for (SPTenderCommonData TCMemberInfo : lstTC_CPInfo) {
                        if (usid.equalsIgnoreCase(TCMemberInfo.getFieldName1())) {
                            IsUserTCMember = true;
                        }
                    }

                    int intEnvelopcnt = 0;
                    List<SPTenderCommonData> lstEnvelops = tenderCommonService1.returndata("getTenderEnvelopeCount", tenderid, "0");
                    if (!lstEnvelops.isEmpty()) {
                        if (Integer.parseInt(lstEnvelops.get(0).getFieldName1()) > 0) {
                            intEnvelopcnt = Integer.parseInt(lstEnvelops.get(0).getFieldName1());
                        }
                    }
                    String strEnvelopeCnt = String.valueOf(intEnvelopcnt);
                    List<SPTenderCommonData> lstClosedInfo = tenderCommonService1.returndata("getTenderClosedInfo", tenderid, strEnvelopeCnt);
                    if (!lstClosedInfo.isEmpty() && lstClosedInfo.size() > 0) {
                        if ("yes".equalsIgnoreCase(lstClosedInfo.get(0).getFieldName5())) {
                            isRptSentToTEC = true;
                        }
                    }

                    //lstTORReport = tenderCommonService1.returndata("CheckValueIntblTosRptShare", comTenderId, null);
                    List<SPTenderCommonData> lstLots = tenderCommonService1.returndata("getLoIdLotDesc_ForOpening", tenderid, null);
                    for (SPTenderCommonData objLot : lstLots) {
                        if (!tenderProcureNature.equalsIgnoreCase("Services")) {
                            tenderLot = objLot.getFieldName1();
                        }
                        String evalRptStatus = "";
                        isCRSaved = false;
                        reportID = "0";
                        roundID = "0";
                        isRptSentToTC = false;
                        String eventType = commonService.getEventType(tenderid).toString();
                        listReportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid, tenderLot, "0");
                        if (listReportID != null && (!listReportID.isEmpty())) {
                            //if(listReportID.get(0).getFieldName2().equals("1"))
                            reportID = listReportID.get(0).getFieldName1();
                        }

                        //NOA RE-EVALUATION
                        List<SPCommonSearchDataMore> getreportID = null;
                        getreportID = dataMore.getEvalProcessInclude("FirstRoundAndNoaBiddersChk", tenderid, tenderLot, reportID, String.valueOf(eCount));

                        //END
                        List<SPCommonSearchDataMore> bidderRounds = new ArrayList<SPCommonSearchDataMore>();//Round List Initialized
                        bidderRounds = dataMore.getEvalProcessInclude("getRoundsforEvaluation", tenderid, reportID, "L1", String.valueOf(eCount), tenderLot);
                        if (bidderRounds != null && !bidderRounds.isEmpty()) {
                            roundID = bidderRounds.get(0).getFieldName1();
                        }
                        if (Integer.parseInt(roundID) > 0) {
                            listReportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid, "0", roundID);
                            if (listReportID != null && (!listReportID.isEmpty())) {
                                reportID = listReportID.get(0).getFieldName1();
                                if (listReportID.get(0).getFieldName2().equals("1")) {
                                    isCRSaved = true;
                                }
                            }
                        }

                        //added by dohatec
                        if (getreportID != null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")) {
                            getreportID = dataMore.getEvalProcessInclude("BidderRoundsforEvaluationChk", roundID, tenderid, tenderLot);
                            if (getreportID != null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")) {
                                isCRSaved = false;
                            }
                        }
                        //end

                        listReportID = dataMore.getEvalProcessInclude("getEvalRptSent", tenderid, tenderLot, roundID);
                        if (listReportID != null && (!listReportID.isEmpty())) {
                            evalRptStatus = listReportID.get(0).getFieldName1();
                        }
                        envDataMores = dataMore.getEvalProcessInclude("GetTenderEnvCount", tenderid, null, null);
                        String envelope = "";
                        if (envDataMores != null && (!envDataMores.isEmpty())) {
                            envelope = envDataMores.get(0).getFieldName1().toString();
                        }
                        List<SPCommonSearchDataMore> listSentRptNew = new ArrayList<SPCommonSearchDataMore>();
                        if (eventType.equalsIgnoreCase("REOI")) {
                            listSentRptNew = dataMore.getEvalProcessInclude("isTERSentToAA", tenderid, tenderLot, envelope, roundID, "");
                        } else {
                            listSentRptNew = dataMore.getEvalProcessInclude("isTERSentToTc", tenderid, tenderLot, roundID, "senttotc");
                        }
                        if (listSentRptNew != null && (!listSentRptNew.isEmpty()) && (listSentRptNew.get(0).getFieldName1().equals("1"))) {
                            isRptSentToTC = true;
                        }
                        if ((usid.equalsIgnoreCase(peId) && evalRptStatus.equalsIgnoreCase("senttopa")) || (usid.equalsIgnoreCase(userId_TEC_CP) && isRptSentToTEC) || (usid.equalsIgnoreCase(userId_TC) && isRptSentToTC) || (IsUserTCMember && isRptSentToTC)) {

                            List<SPTenderCommonData> lstPriceBidFrms = null;
                            String strLotIsFormFilled = "";
                            lstPriceBidFrms = tenderCommonService1.returndata("getPriceBidForms_ForOpening", tenderid, objLot.getFieldName1());
                            for (SPTenderCommonData lotFormName : lstPriceBidFrms) {
                                List<SPTenderCommonData> lstLotFormStatusInfo = tenderCommonService1.returndata("getFormStatus_ForOpening", lotFormName.getFieldName5(), null);
                                if (!lstLotFormStatusInfo.isEmpty()) {
                                    strLotIsFormFilled = lstLotFormStatusInfo.get(0).getFieldName2();
                                }
                            }
                %>
                <%
                    boolean is2Env = false;
                    boolean isT1L1 = false;
                    boolean isT1 = false;
                    String strComType = com;
                    //List<SPCommonSearchDataMore> envDataMores = dataMore.getEvalProcessInclude("GetTenderEnvCount", tenderid_eval, null, null);
                    if (envDataMores != null && (!envDataMores.isEmpty())) {
                        if (envDataMores.get(0).getFieldName1().equals("2")) {
                            is2Env = true;
                        }
                        if (envDataMores.get(0).getFieldName2().equals("3")) {
                            //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                            isT1L1 = true;
                        }
                        if (envDataMores.get(0).getFieldName2().equals("1")) {
                            //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                            isT1 = true;
                        }
                    }
                    if (!is2Env && !isT1L1) {
                        IsAllFormDecrypted = true;
                    }
                %>
                <table width='100%' cellspacing='0' class='tableList_1 t_space'>

                    <%if (eventType.equals("REOI") || (eventType.equals("RFP") && is2Env && isT1L1)) {%>
                    <tr>
                        <td class="t-align-left ff">Finalize Evaluation Status :</td>
                        <td class="t-align-center">
                            <jsp:include page="CPEvalReport.jsp">
                                <jsp:param name="tenderid" value="<%=tenderid%>"/>
                                <jsp:param name="pnature" value="services"/>
                                <jsp:param name="comtype" value="<%=com%>"/>
                                <jsp:param name="lotId" value="0"/>
                                <jsp:param  name="isCommCP" value="true"/>
                                <jsp:param name="allowFinalResponseProcess" value="Yes"/>
                                <jsp:param name="evalRptStatus" value="<%=evalRptStatus%>"/>
                                <jsp:param name="eventType" value="<%=eventType%>"/>
                            </jsp:include>
                        </td>
                        <%  %>
                    </tr>
                    <% }
                        boolean IsAllBidderEvaluated = false;
                        if (request.getAttribute("IsAllBidderEvaluated") != null) {
                            if (request.getAttribute("IsAllBidderEvaluated").toString().equalsIgnoreCase("true")) {
                                IsAllBidderEvaluated = true;
                            }
                        }
                        if (eventType.equals("RFP") && IsAllBidderEvaluated) { %>
                    <%
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");

                        EvaluationMatrix matrix = new EvaluationMatrix();
                        String procNature = commonService.getProcNature(tenderid).toString();
                        String pMethod = commonService.getProcMethod(tenderid).toString();
                        boolean[] cases = matrix.getEvaluationCase(procNature, eventType, pMethod, is2Env);
                        boolean isCase1 = cases[0];
                        boolean isCase2 = cases[1];
                        boolean isCase3 = cases[2];
                        matrix = null;
                        cases = null;
                        boolean isSentToAADecryptAll = isCase3;
                        List<SPCommonSearchDataMore> getEvalOpenCommonMember = dataMore.geteGPData("getEvalCommitteeMember", tenderid);
                        boolean IsTEC_CP_in_Committee = false;
                        for (SPCommonSearchDataMore TEC_CP : getEvalOpenCommonMember) {
                            if (TEC_CP.getFieldName1().equals(userId_TEC_CP)) {
                                IsTEC_CP_in_Committee = true;
                                break;
                            }
                        }
                        boolean isDecyptAvail = (!getEvalOpenCommonMember.isEmpty() && userId_TEC_CP.equals(session.getAttribute("userId").toString()) && IsTEC_CP_in_Committee);
                        if (is2Env) {
                            TenderOpening opening = new TenderOpening();


                    %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <%                                int lotCnt = 0;
                            List<SPCommonSearchData> lotIdDesc = commonSearchService.searchData("get2EnvLoIdLotDesc", tenderid, null, null, null, null, null, null, null, null);
                            for (SPCommonSearchData lotdsc : lotIdDesc) {
                                if (isDecyptAvail) {
                                    out.print("<tr id='decLink_" + lotCnt + "'>"
                                            + "<td colspan='3' class='t-align-center'>"
                                            + "(<a href='DecryptAll.jsp?tenderId=" + tenderid + "&stat=eval&lotId=" + lotdsc.getFieldName1() + "' onclick=\"return AllBidderFinalizedConfirmation();\" >Decrypt All</a>)"
                                            + "</td></tr>");
                                }
                                out.print("<tr><td colspan='3'><table width='100%' class='tableList_1' cellspacing='0'>"
                                        + "<tr>"
                                        + "<td width='15%' class='t-align-left ff'>" + (procNature.equals("Services") ? "Package" : "Lot") + " No.</td>"
                                        + "<td width='85%'>" + lotdsc.getFieldName3() + "</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td class='t-align-left ff'>" + (procNature.equals("Services") ? "Package" : "Lot") + " Description</td>"
                                        + "<td>" + lotdsc.getFieldName2() + "</td>"
                                        + "</tr>"
                                        + "</table></td></tr>");
                                List<SPCommonSearchData> priceFormList = commonSearchService.searchData("getPriceBidfromLotId", tenderid, lotdsc.getFieldName1(), null, null, null, null, null, null, null);
                                int cnt = 1;
                                int cntDec = 1;
                                if (priceFormList.isEmpty()) {
                                    out.print("<tr><td colspan='3' style='color: red;' class='t-align-center'>No BOQ Forms Available</td></tr>");
                                } else {%>
                        <tr>
                            <th width="10%" class="t-align-center ff">S No.</th>
                            <th width="65%" class="t-align-center ff">Form Name</th>
                            <th width="25%" class="t-align-center ff">Action</th>
                            <!--<th width="21%" valign="middle" class="t-align-center ff">Action <span id="decLink">(<a href="DecryptAll.jsp?tenderId=< %=tenderId%>&stat=eval">Decrypt All</a>)</span></th>-->
                        </tr>
                        <%
                            for (SPCommonSearchData priceList : priceFormList) {
                                boolean isMultipleFillingYes = false;
                                List<SPTenderCommonData> mulitiList = tenderCommonService.returndata("isCompReportMultiFilling", priceList.getFieldName5(), null);
                                if (mulitiList != null && (!mulitiList.isEmpty())) {
                                    isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                                }
                        %>
                        <tr>
                            <td class="t-align-center"><%=cnt%></td>
                            <td><%=priceList.getFieldName1()%></td>
                            <td class="t-align-center">
                                <%
                                    //[Already Placed above  if (opening.getCommonMemberCount(tenderid_eval, session.getAttribute("userId").toString()) != 0) {
                                    //(No need of this condition(sachin))if (opening.getBidderStatusCount(tenderid_eval) != 0) {
                                    if (opening.getBidPlainDataCount(priceList.getFieldName5()) == 0) {
                                        if (isDecyptAvail) {
                                %>
                                <a href="Decrypt.jsp?tenderId=<%=tenderid%>&formId=<%=priceList.getFieldName5()%>&comType=<%=strComType%>" onclick="return AllBidderFinalizedConfirmation()">Decrypt</a>
                                <%
                                    } else {
                                        out.print("Decryption Pending");
                                    }
                                } else {
                                    IsFinancialReportDecrypted = true;
                                    cntDec++;
                                    if (isMultipleFillingYes) {
                                %>
                                <a href="SrvComReport.jsp?formId=<%=priceList.getFieldName5()%>&tenderId=<%=tenderid%>&frm=eval">Comparative Report</a>
                                <%
                                } else {
                                %>
                                <a href="ComReport.jsp?formId=<%=priceList.getFieldName5()%>&tenderId=<%=tenderid%>&comType=<%=strComType%>&frm=eval">Comparative Report</a>
                                <%
                                    }
                                %>
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                <a href="IndReport.jsp?tenderId=<%=tenderid%>&formId=<%=priceList.getFieldName5()%>&frm=eval&comType=<%=strComType%>" >Individual Report</a>
                                <%
                                    }
                                    //}
                                    //}
                                %>
                            </td>
                        </tr>
                        <%
                                    cnt++;
                                }
                            }
                            if (cnt == 1 || cntDec - 1 == priceFormList.size()) {
                        %>
                        <script type="text/javascript">
                            $('#decLink_<%=lotCnt%>').hide();
                        </script>
                        <%
                                    IsAllFormDecrypted = true;
                                }
                                lotCnt++;
                            }
                        %>
                    </table>
                    <%
                            opening = null;
                        }

                    %>
                    <%}
                    
                    if (!evalRptStatus.equalsIgnoreCase("senttopa") && (("Yes".equalsIgnoreCase(strLotIsFormFilled) && (IsAllFormDecrypted || !tenderProcureNature.equalsIgnoreCase("Services"))) || (isCRSaved || (eventType.equals("REOI") && IsAllBidderEvaluated))))
                    {
                    %>
                        <br/><div class="responseMsg noticeMsg" style="color:red; font-size: 15px; font-style: italic;"><b>Please make sure the offline evaluation is done and you have already prepared the evaluation report. Please upload the evaluation report for HOPA approval.</div><br/>
                    
                    <%}%>
                    
                    
                    
                    <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                        <tr>
                            <td width="25%" class="t-align-left ff">Lot No</td>
                            <td width="75%" class="t-align-left ff"><%=objLot.getFieldName3()%></td>
                        <tr>
                            <%if ("Yes".equalsIgnoreCase(strLotIsFormFilled) && (IsAllFormDecrypted || !tenderProcureNature.equalsIgnoreCase("Services"))) {%>
                        <tr>
                            <td width="25%" class="t-align-left ff">Price Comparison Report</td>
                            <td width="75%">
                                <%if (!isCRSaved) {%>
                                <!--(dated: 9th march , 2017)frid (roundID) set to 0 for first phase. For re-evaluation it will be 0 as the system doesn't allow post qualification and NOA rejection-->
                                <a href="<%=request.getContextPath()%>/officer/TenderReport.jsp?tenderid=<%=tenderid%>&repId=<%=reportID%>&istos=y&from=ter&frId=0&lotId=<%=tenderLot%>">View and Save</a>
                                <%} else {%>
                                <a href="<%=request.getContextPath()%>/officer/TenderReport.jsp?tenderid=<%=tenderid%>&repId=<%=reportID%>&istos=y&from=ter&sv=y&rId=<%=roundID%>&lotId=<%=tenderLot%>">View</a>
                                <% } %>
                            </td>
                        </tr>
                        <%}%>
                    </table>
                    <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                        <%if (isCRSaved || (eventType.equals("REOI") && IsAllBidderEvaluated)) { %>
                        <tr>
                            <% if (evalRptStatus.equalsIgnoreCase("senttopa")) {%>
                            <td width="25%" class="t-align-left ff">View Evaluation Document</td>
                            <td width="75%" >
                                <a href="<%=request.getContextPath()%>/officer/EvalRptDocToTC.jsp?tenderId=<%=tenderid%>&roundId=<%=roundID%>&lotId=<%=tenderLot%>">View</a>

                                <div class="reqF_1" id="valUpload"></div>
                            </td>
                            <%} else {%>
                            <td width="25%" class="t-align-left ff">Upload Evaluation Document</td>
                            <td width="75%" >
                                <a href="<%=request.getContextPath()%>/officer/EvalRptDocToTC.jsp?tenderId=<%=tenderid%>&roundId=<%=roundID%>&lotId=<%=tenderLot%>&noDelete=1">Upload / Remove</a>

                                <div class="reqF_1" id="valUpload"></div>
                            </td>
                            <%}%>
                        </tr>
                        <%if (usid.equalsIgnoreCase(userId_TEC_CP)) { %>
                        <tr>
                            <td width="25%" class="t-align-left ff">Send Evaluation Report To PA/AU</td>
                            <% if (evalRptStatus.equalsIgnoreCase("senttopa")) { %>
                            <td>
                                Evaluation Report is sent to PA/AU by Evaluation Chairperson.
                            </td>
                            <%} else {%>
                            <td>
                                <a href="<%=request.getContextPath()%>/officer/SendReportTC.jsp?tenderid=<%=tenderid%>&roundId=<%=roundID%>&lotID=<%=tenderLot%>&sendTo=PA" <% if (eventType.equals("REOI")) { %> onclick="return AllBidderFinalizedConfirmationREOI()" <% } %> >Send</a>
                            </td>
                            <%}%>
                        </tr>
                        <%}%>
                        <%if (usid.equalsIgnoreCase(peId) && evalRptStatus.equalsIgnoreCase("senttopa") ) {
                                //listReportID = dataMore.getEvalProcessInclude("getEvalRptSent", tenderid,tenderLot,roundID,usid,"by");
                                //if(listReportID!=null && (!listReportID.isEmpty())){
                                //    evalRptStatus = listReportID.get(0).getFieldName1();
                                //}

                        %>
                        <tr>
                            <td width="25%" class="t-align-left ff">Send Evaluation Report To Tender Committee</td>
                            <% if (isRptSentToTC) { %>
                            <td>
                                Evaluation Report is sent to Tender Committee by PA/AU.
                            </td>
                            <%} else {%>
                            <td>
                                <a href="<%=request.getContextPath()%>/officer/SendReportTC.jsp?tenderid=<%=tenderid%>&roundId=<%=roundID%>&lotID=<%=tenderLot%>&sendTo=TC">Send</a>
                            </td>
                            <%}%>
                        </tr>
                        <%}%>
                        <%if (usid.equalsIgnoreCase(userId_TC)) { %>
                        <tr>
                            <td width="25%" class="t-align-left ff">Evaluation Report Approval</td>
                            <%
                                if (eventType.equalsIgnoreCase("REOI")) {
                                    listSentRptNew = dataMore.getEvalProcessInclude("isTERSentToAAApp", tenderid, tenderLot, envelope,roundID,"");
                                    if(listSentRptNew!=null && (!listSentRptNew.isEmpty()) && (listSentRptNew.get(0).getFieldName1().equals("1"))){
                                        isRptSentForLoi = true;
                                    }
                                }else{
                                    listSentRptNew = dataMore.getEvalProcessInclude("isTERSentToTc", tenderid, tenderLot, roundID, "sentforloi"); //TODO: this data should be fetched by also checking senttotc
                                    if ((listSentRptNew != null) && (!listSentRptNew.isEmpty()) && (listSentRptNew.get(0).getFieldName1().equals("1"))) {
                                        isRptSentForLoi = true;
                                    }
                                    else
                                    {
                                        isRptSentForLoi = false;
                                    }
                                }
                                if (isRptSentForLoi) {
                            %>
                            <td>
                                Approved by Tender Committee Chairperson.
                            </td>
                            <% } else {%>
                            <td>
                                <a href="<%=request.getContextPath()%>/officer/SendReportTC.jsp?tenderid=<%=tenderid%>&roundId=<%=roundID%>&lotID=<%=tenderLot%>">Approve</a>
                            </td>
                            <%}%>
                        </tr>
                        <%}%>
                        <% } %>                       
                    </table>

                    <jsp:include page="../resources/common/EmpTransferHist.jsp" />
                    <!-- End: Show Sub Menu and Related Content-->
                    <%}
                            }
                        }%>

                    <%
                        // Set To Null
                        msg = null;
                        msgOFNominee = null;
                        msgs = null;

                        peId = null;
                        usid = null;
                    %>
            </div> 


        </div>

        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>

    </body>

    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }

        function AllBidderFinalizedConfirmation()
        {
            var IsAnyDocumentDecrypted = "<%=IsFinancialReportDecrypted%>";
            if (IsAnyDocumentDecrypted == "false")
            {
                var response = confirm("Once you Decrypt any report Finalize Respnsiveness link will be disabled");
                return response;
            } else
            {
                return true;
            }
        }

        function AllBidderFinalizedConfirmationREOI()
        {
            var response = confirm("Once you Send the report to PA/AU Finalize Respnsiveness link will be disabled");
            return response;
        }

    </script>
</html>
