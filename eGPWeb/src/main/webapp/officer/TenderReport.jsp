<%-- 
    Document   : TenderReport
    Created on : Dec 29, 2010, 7:34:39 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.web.servicebean.ReportCreationSrBean" %>
<%@page import="com.cptu.egp.eps.model.table.TblReportColumnMaster" %>
<%@page import="com.cptu.egp.eps.web.databean.ReportGenerateDtBean" %>


<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id ="DomesticPreference" class="com.cptu.egp.eps.web.servicebean.DomesticPreferenceSrBean"/>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Price Comparison Report</title>
        <script type="text/javascript">
            $(document).ready(function() {

                $("#print").click(function() {
                    //alert('sa');
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                //alert(options);
                $('#print').hide();
                $('#saveASPDF').hide();
                $('#goBack').hide();                
                $('#repSaveBtn').hide();
                $('#print_area').printElement(options);                
                $('#print').show();
                $('#saveASPDF').show();
                $('#goBack').show();
                $('#repSaveBtn').show();
                //$('#trLast').hide();
            }
            function checkSubmit(){
                //var varN = 'hdn'+($('#svBtn').attr('name'));
                var varN = $('#svBtn').val();
                $('#svBtn').attr('disabled', true);
                $('#hdnsvBtn').val(varN);
                return true;
            }
        </script>
    </head>
    <body>
        <%
        //update bidder rank and others if domestic preference yes
       // tenderCommonService
        // TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
          String tenderid = request.getParameter("tenderid");
         Integer preference = 0;
              preference = Integer.valueOf(DomesticPreference.IsDomesticPreference(Integer.valueOf(tenderid)).toString());
              if(preference >= 1)
              {
                   TenderCommonService tender = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                   List<SPTenderCommonData> listDomesticPre = tender.returndata("updateAfterDomesticPref", tenderid, null);
                  //  for (SPTenderCommonData listing : listDomesticPre) {
                        if(listDomesticPre.get(0).getFieldName1().equals("false"))
                       //if(listing.getFieldName1().equals("false"))
                            {%>
                            <script type="text/javascript">
                               alert("Please click Go Back and then click View and Sign again");
                            </script>
                           <%}//Extra Added By Nazmul
                   }
                       // else{ //Hide By Nazmul

                    boolean isTos = false;
                    boolean isEval = false;
                    boolean isTER = false;
                    boolean isSave = true;
                    boolean isNotPDF = true;
                    if (request.getParameter("isPDF") != null && "true".equalsIgnoreCase(request.getParameter("isPDF"))) {
                        isNotPDF = false;
                    }
                    if ("y".equals(request.getParameter("istos"))) {
                        isTos = true;
                    }
                    if ("ter".equals(request.getParameter("from"))) {
                        isTER = true;
                    }
                    if ("y".equals(request.getParameter("sv"))) {
                        isSave = false;
                    }
                    if ("y".equals(request.getParameter("isEval"))) {
                        isEval = true;
                    }
                    String lotId = "0";
                    if(request.getParameter("lotId")!=null){
                        lotId = request.getParameter("lotId");
                    }
                    String repId = request.getParameter("repId");
                    String roundId = "0";
                    String finalRoundId = "0";
                    if (request.getParameter("rId") != null) {
                        roundId = request.getParameter("rId");
                    }
                    if (request.getParameter("frId") != null) {
                        finalRoundId = request.getParameter("frId");
                    }

                    String tend_rep_UserId = null;
                    if (session.getAttribute("userId") != null) {
                        tend_rep_UserId = session.getAttribute("userId").toString();
                    } else {
                        tend_rep_UserId = request.getParameter("tendrepUserId");
                    }
                    String pageName = null;
					//added by dohatec for re-evaluation
                    EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                    int evalCount  = evalService.getEvaluationNo(Integer.parseInt(tenderid));


                    // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail = null;
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String idType = "tenderId";
                    int auditId = Integer.parseInt(request.getParameter("tenderid"));
                    String auditAction = "View Price Comparison Report";
                    String moduleName = EgpModule.Evaluation.getName();
                    String remarks = "";
                    if (isNotPDF) {
                        objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                    }

                    if ("Save Report".equals(request.getParameter("hdnsaveRep")) || "Lottery".equals(request.getParameter("hdnsaveRep"))) {//lottery
                        auditAction = "Save Price Comparison Report";
                        String win = "1";
                        if(request.getParameter("rdbWinner")!=null)
                            win = request.getParameter("rdbWinner");
                        ReportCreationSrBean rcsb = new ReportCreationSrBean();
                        rcsb.setRoundId(roundId);
                        rcsb.getReportData(tenderid, repId, tend_rep_UserId, false,false,win,lotId);
                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                        rcsb = null;
                        response.sendRedirect(request.getParameter("pageName"));
                    } else if ("Save Report".equals(request.getParameter("hdnroundSaveRep")) || "Lottery".equals(request.getParameter("hdnroundSaveRep"))) {
                        //String win = request.getParameter("rdbWinner");
                        ReportCreationSrBean rcsb = new ReportCreationSrBean();
                        String pgRedirect = request.getParameter("pageName");
                        if (rcsb.saveReportRoundWise(request.getParameterValues("lotteryBidder"), request.getParameter("svfrId"))) {
                            pgRedirect = pgRedirect + "&sv=succ";
                        } else {
                            auditAction = "Error in " + auditAction;
                            pgRedirect = pgRedirect + "&sv=err";
                        }


                        //makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                        //response.sendRedirect(pgRedirect);
                    } else {
                        if (session.getAttribute("userId") != null && "t1".equals(request.getParameter("evalm"))) {
                            EvalServiceCriteriaService criteriaService = (EvalServiceCriteriaService) AppContext.getSpringBean("EvalServiceCriteriaService");
                            criteriaService.saveT1Data(Integer.parseInt(tenderid), Integer.parseInt(session.getAttribute("userId").toString()));
                        }
            if (isNotPDF) {
        %>        
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <%                
            }
            pageContext.setAttribute("tenderId", tenderid);
            pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
            pageContext.setAttribute("userId", 0);
        %>
        <div class="contentArea_1">
            <div  id="print_area">
                <div class="pageHead_1">Price Comparison Report
                    <%if (isNotPDF) {%>
                    <span style="float: right;">
                        <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                        <span id="svpdfl1">                            
                            <a class="action-button-savepdf" id="saveASPDF" href="<%=request.getContextPath()%>/TorRptServlet?tenderId=<%=tenderid%>&repId=<%=repId%>&action=pdfL1">Save As PDF</a>
                        </span>
                        <a id="goBack" class="action-button-goback"
                           href="<%=request.getContextPath()%>/officer/EvalComm.jsp?tenderid=<%=tenderid%>">Go back to Dashboard</a>
                    </span>
                    <%}%>
                </div>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div class="tabPanelArea_1">
                <%
                        if (isTER && isSave && isNotPDF) {
                            String qString = request.getQueryString();
                            pageName = "EvalComm.jsp?tenderid="+tenderid;
                    %>
                    <br/><div class="responseMsg noticeMsg" style="color:red; font-size: 15px; font-style: italic;"><b>Please make sure the offline evaluation is done and you have already prepared the evaluation report. Please upload the evaluation report for HOPA approval.</div><br/>
                    
                    <form action="TenderReport.jsp?<%=qString%>" method="post" id="frmSbmt" onsubmit="return checkSubmit();">
                 
                
                    <jsp:include page="TenderRepInclude.jsp">
                        <jsp:param name="tenderid"  value="<%=tenderid%>"/>
                        <jsp:param name="repId"  value="<%=repId%>"/>
                        <jsp:param name="isEval"  value="<%=isEval%>"/>
                        <jsp:param name="istos"  value="<%=isTos%>"/>
                        <jsp:param name="ister"  value="<%=isTER%>"/>
                        <jsp:param name="rId"  value="<%=roundId%>"/>
                        <jsp:param name="frId"  value="<%=finalRoundId%>"/>
                        <jsp:param name="isSave"  value="<%=isSave%>"/>
                        <jsp:param name="evalCount"  value="<%=evalCount%>"/>
                        <jsp:param name="lotId"  value="<%=lotId%>"/>
                    </jsp:include>
                        
                     
                        <input type="hidden" value="<%=pageName%>" name="pageName"/>
                        <div class="t-align-center t_space" id="repSaveBtn">
                            <label class="formBtn_1">
                                <%if (!finalRoundId.equals("0")) {%>
                                <input type="hidden" value="<%=finalRoundId%>" name="svfrId"/>
                                <%out.print(request.getAttribute("lotteryBidder"));%>
                                <input type="submit" name="roundSaveRep"  value="<%/*if (request.getAttribute("isLottery") != null && (Boolean) request.getAttribute("isLottery")) {
                                        out.print("Lottery");
                                    } else {*/
                                        out.print("Save Report");
                                    //}%>" id="svBtn"/>
                                <input type="hidden" name="hdnroundSaveRep" value="" id="hdnsvBtn"/>
                                <%} else {%>
                                <input type="submit" name="saveRep"  value="<%/*if (request.getAttribute("isLottery") != null && (Boolean) request.getAttribute("isLottery")) {
                                        out.print("Lottery");
                                    } else { */
                                        out.print("Save Report");
                                    //}%>" id="svBtn"/>
                                <input type="hidden" name="hdnsaveRep" value="" id="hdnsvBtn"/>
                                <%}%>
                            </label>
                        </div>
                        <!--<div id="repMsg" class="responseMsg noticeMsg">
                            Price Comparison Report have no data
                        </div>-->
                        <script type="text/javascript">
                            if($('#repTable').html()==null){
                                $('#repSaveBtn').hide();
                            }
                        </script>
                    </form>
                    <%}else{%>
                        <jsp:include page="TenderRepInclude.jsp">
                        <jsp:param name="tenderid"  value="<%=tenderid%>"/>
                        <jsp:param name="repId"  value="<%=repId%>"/>
                        <jsp:param name="isEval"  value="<%=isEval%>"/>
                        <jsp:param name="istos"  value="<%=isTos%>"/>
                        <jsp:param name="ister"  value="<%=isTER%>"/>
                        <jsp:param name="rId"  value="<%=roundId%>"/>
                        <jsp:param name="frId"  value="<%=finalRoundId%>"/>
                        <jsp:param name="isSave"  value="<%=isSave%>"/>
                         <jsp:param name="evalCount"  value="<%=evalCount%>"/>
                    </jsp:include>
                    <%}%>
                </div>
            </div>
        </div>
        <%if (isNotPDF) {%>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%}%>
        <%}%>
       <%//}} //Hide By Nazmul //end domestic preference and update%>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
