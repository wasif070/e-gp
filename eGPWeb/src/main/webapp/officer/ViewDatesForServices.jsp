<%-- 
    Document   : ViewDatesForServices
    Created on : Sep 13, 2011, 5:06:59 PM
    Author     : shreyansh
--%>
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%String flag="";
if(request.getParameter("flag")!=null)
    flag = request.getParameter("flag");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Dates</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script language="javascript">
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function GetCal(txtname,controlname,id)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d-%b-%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        checkDate(txtname,id);
                        $('#divid_'+id).remove();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

        </script>
        <script type="text/javascript">
            /* check if pageNO is disable or not */
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            /*  load Grid for the User Registration Report */
            function loadTable()
            {
               
                $('#tohide').hide();
                if($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/CmsServiceServlet", {tenderId: $("#tenderId").val(),size: $("#size").val(),pageNo: $("#first").val(),action:'viewDatesInService',wpId: $("#wpId").val(),flag:'<%=flag%>'},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            /*  Handle First click event */
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    $('#first').val(0);
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Last Button click event */
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var size = (parseInt($('#size').val())*totalPages)-(parseInt($('#size').val()));
                      $('#first').val(size);
                    if(totalPages>0){
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Next Button click event */
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val());
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first+max);

                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo <= totalPages) {

                        loadTable();
                         $('#pageNo').val(Number(pageNo)+1);
                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            /*  Handle Previous click event */
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first-max);
                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    $('#pageNo').val(Number(pageNo)-1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)-1);
                });
            });
        </script>
        <script type="text/javascript">
            function Search(){
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            }
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var size = (parseInt($('#size').val())*pageNo)-$('#size').val();
                    $('#first').val(size);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }

                });
            });
            function changetable(){
                var pageNo=parseInt($('#pageNo').val(),10);
                var first = parseInt($('#first').val());
                var max = parseInt($('#size').val());
                var totalPages=parseInt($('#totalPages').val(),10);
                loadTable();

            }

        </script>

        <%
                    int userid = 0;
                    ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userid = Integer.parseInt(hs.getAttribute("userId").toString());
                         pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                    String lotId = request.getParameter("lotId");
                    CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                   List<Object[]> list = cs.getLotDetailsByPkgLotId(lotId,request.getParameter("tenderId"));
        %>
    </head>
    <body onload="loadTable();">

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1"><%if("".equalsIgnoreCase(flag)){%>View Dates of Work Schedule
                        <%}else{%>
                        Edit Dates of Work Schedule
                        <%}%>
                        <span class="c-alignment-right"><a href="WorkScheduleMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span>
                    </div>
 <%@include file="../resources/common/TenderInfoBar.jsp" %>
 <div>&nbsp;</div>
 <form name="frm" action="<%=request.getContextPath()%>/CmsServiceServlet"  method="post">

     <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
         <%for(Object[] obj : list){%>
                            <tr>
                                <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                                <td width="80%"><%=obj[0] %></td>
                            </tr>
                            <tr>
                                <td><%=bdl.getString("CMS.lotdes")%></td>
                                <td class="t-align-left"><%=obj[1]%></td>
                            </tr>
                            <%}%>
     </table>

     <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>" />
        <input type="hidden" name="wpId" id="wpId" value="<%=request.getParameter("wpId")%>" />
     <input type="hidden" name="action" value="editdates" />
                    <div id="resultDiv" style="display: block;">
                        <div  id="print_area">
                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                <tr>
                                    <th width="3%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                    <th width="10%" class="t-align-center">Milestone</th>
                                    <th width="20%" class="t-align-center"><%=bdl.getString("CMS.desc")%></th>
                                    <th width="10%" class="t-align-center">% of Contract Value
                                                                          </th>
                                    <th width="10%" class="t-align-center">PE Start Date
                                    </th>
                                    <th width="10%" class="t-align-center">PE End Date
                                    </th>
                                    <th width="18%" class="t-align-center"><%=bdl.getString("CMS.sdate")%>
                                    </th>
                                    <th width="18%" class="t-align-center"><%=bdl.getString("CMS.edate")%>
                                    </th>
                                </tr>
                            </table>

                        </div>
                    </div>
     <div id="tohide">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                        <tr>
                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Total Records per page : <select name="size" id="size" onchange="changetable();" style="width:40px;">
                                    <option value="1000" selected>10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>

                                </select>
                            </td>
                            <td align="center"><input name="textfield3" type="text" id="dispPage" onKeydown="javascript: if (event.keyCode==13) Search();" value="1" class="formTxtBox_1" style="width:20px;" />
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="submit" name="button"  id="btnGoto" value="Go To Page" />
                                </label></td>
                            <td  class="prevNext-container">
                                <ul>
                                    <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                    <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                </ul>
                            </td>
                        </tr>
                    </table>
     </div>
                    <input type="hidden" id="pageNo" name="pageNo" value="1"/>
                    <input type="hidden" name="first" id="first" value="0"/>
                    <br />
                    <center>
                    <%if(!"".equalsIgnoreCase(flag)){%>
                     <label class="formBtn_1" id="lbl_finalizeBtn">
                                        <input type="submit" name="finalizeBtn" id="finalizeBtn" value="Edit Dates"  />
                                    </label>
                     <%}%>
                    </center>
 </form>
                </div>

                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>


</body>

<script>
    var headSel_Obj = document.getElementById("headTabReport");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
</html>

