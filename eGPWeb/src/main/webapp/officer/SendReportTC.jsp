<%-- 
    Document   : SendReportTC
    Created on : Apr 26, 2016, 12:43:26 PM
    Author     : HP-Ahsan
--%>

<%@page import="com.cptu.egp.eps.web.utility.NumberToWord"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <title>Send Evaluation Report To PA</title>
        <script type="text/javascript">
            function checkSubmit(){
                var vbool = true;
                if(document.getElementById('txtComments').value.length > 0)
                {
                    document.getElementById('CommentErrorMsg').innerHTML = "";
                    vbool = true;
                }
                else
                {
                    document.getElementById('CommentErrorMsg').innerHTML = "Please Enter comment";
                    vbool = false;
                }
                return vbool;
            }
            
            function ClearCommentError()
            {
                if(document.getElementById('txtComments').value.length > 0)
                {
                    document.getElementById('CommentErrorMsg').innerHTML = "";
                }
                else
                {
                    document.getElementById('CommentErrorMsg').innerHTML = "Please Enter comment";
                }
            }
        </script>
    </head>
    <body>
        <%
            String tenderId = "0";
            String roundId = "0";
            String lotId = "0";
            String sendTo = "";
            if (request.getParameter("tenderid") != null) {
                pageContext.setAttribute("tenderid", request.getParameter("tenderid"));
                tenderId = request.getParameter("tenderid");
            }
            if (request.getParameter("roundId") != null) {
                roundId = request.getParameter("roundId");
            }
            if (request.getParameter("lotID") != null) {
                lotId = request.getParameter("lotID");
            }
            if (request.getParameter("sendTo") != null) {
                sendTo = request.getParameter("sendTo");
            }
            String usid = session.getAttribute("userId").toString();

            if (request.getParameter("submit") != null && request.getParameter("submit").equalsIgnoreCase("submit")) {

                if (request.getParameter("roundId") != null) {
                    roundId = request.getParameter("roundId");
                }

                if (request.getParameter("lotId") != null) {
                    lotId = request.getParameter("lotId");
                }

                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");
                }
                AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                TenderCommonService objTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                CommonSearchService commonSearchData = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");

                CommonMsgChk commonMsgChk = null;
                String idType = "tenderId";
                int auditId = Integer.parseInt(tenderId);
                String moduleName = EgpModule.Evaluation.getName();
                String auditAction = null;
                String remarks = "";
                HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                String userId_TEC_CP = "", govUserId_TEC_CP = "";
                String userId_TC = "", govUserId_TC = "";
                String userid_PA = "", govUserId_PA = "";

                List<SPTenderCommonData> lstTEC_CPInfo = objTenderCommonService.returndata("EvalTECChairPerson", tenderId, "");
                if (!lstTEC_CPInfo.isEmpty()) {
                    userId_TEC_CP = lstTEC_CPInfo.get(0).getFieldName1(); // Get TEC/PEC Chairperson UserId
                    govUserId_TEC_CP = lstTEC_CPInfo.get(0).getFieldName5(); // Get TEC/PEC Chairperson GovUserId
                }
                List<SPTenderCommonData> lstOfficerUserId = objTenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
                if (!lstOfficerUserId.isEmpty()) {
                    userid_PA = lstOfficerUserId.get(0).getFieldName1(); // Get PE UserIi
                    govUserId_PA = lstOfficerUserId.get(0).getFieldName1(); // Get PE UserIi
                }
                List<SPTenderCommonData> lstTC_CPInfo = objTenderCommonService.returndata("TCMember", tenderId, "");
                if (!lstTEC_CPInfo.isEmpty()) {
                    if (lstTC_CPInfo.get(0).getFieldName3().equals("cp")) {
                        userId_TC = lstTC_CPInfo.get(0).getFieldName1(); // Get TC/PC Chairperson UserId
                        govUserId_TC = lstTC_CPInfo.get(0).getFieldName7(); // Get TC/PC Chairperson GovUserId
                    }

                }
                
                String evalRptStatus = "";
                List<SPCommonSearchDataMore> listReportID = null;
                CommonSearchDataMoreService dataMoree = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                listReportID = dataMoree.getEvalProcessInclude("getEvalRptSent", tenderId, lotId, roundId);
                if (listReportID != null && (!listReportID.isEmpty())) 
                {
                    evalRptStatus = listReportID.get(0).getFieldName1();
                }
                else
                {
                    evalRptStatus = "";
                }

                if (usid.equals(userId_TEC_CP) && !evalRptStatus.equalsIgnoreCase("senttopa")) {
                    commonMsgChk = addUpdate.addUpdOpeningEvaluation("SendEvalReportToPA", tenderId, lotId, roundId, userId_TEC_CP, userid_PA, handleSpecialChar.handleSpecialChar(request.getParameter("txtComments")), "senttopa").get(0);
                    if (commonMsgChk.getFlag().equals(true)) {
                        /* START : CODE TO SEND MAIL */
                        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                        String mailSubject = "e-GP System: TEC Chairperson has sent Evaluation Report to you";
                        String strFrmEmailId = "";

                        List<SPTenderCommonData> frmEmail
                                = objTenderCommonService.returndata("getEmailIdfromUserId", userId_TEC_CP, null);
                        if (!frmEmail.isEmpty()) {
                            strFrmEmailId = frmEmail.get(0).getFieldName1(); // Email Id of Sender (TEC CP)
                        }
                        List<SPTenderCommonData> emails = objTenderCommonService.returndata("getEmailIdfromUserId", userid_PA, null);

                        String[] mail = {emails.get(0).getFieldName1()}; // Email Id of PE

                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();

                        //userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.EvalReportSentToTC(tenderId, frmEmail.get(0).getFieldName1()));
                        msgBoxContentUtility = null;
                        /* END : CODE TO SEND MAIL */

                        strFrmEmailId = null;
                        frmEmail = null;

                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                        response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&msgId=senttopa");
                    } else {
                        response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&msgId=error");
                    }
                } else if (usid.equals(userid_PA)) {

                    List<SPTenderCommonData> lstOfficergovUserId = objTenderCommonService.returndata("getGovUserIdFromUserId", userid_PA, null);
                    if (!lstOfficergovUserId.isEmpty()) {
                        govUserId_PA = lstOfficergovUserId.get(0).getFieldName1(); // Get PE UserIi
                    }
                    CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> envDataMores = dataMore.getEvalProcessInclude("GetTenderEnvCount", tenderId, null, null);
                    String envelope = "";
                    if (envDataMores != null && (!envDataMores.isEmpty())) {
                        envelope = envDataMores.get(0).getFieldName1().toString();
                    }
                    String evalCount = "0";

                    String eventType = commonService.getEventType(tenderId).toString();

                    if (eventType.equalsIgnoreCase("REOI")) {
                        commonMsgChk = addUpdate.addUpdOpeningEvaluation("SendEvalReportToTC", userId_TC, userid_PA, tenderId, handleSpecialChar.handleSpecialChar(request.getParameter("txtComments")), govUserId_TC, govUserId_PA, "HOPA", lotId, "Pending", "", envelope, roundId, null, evalCount).get(0);
                    } else {
                        commonMsgChk = addUpdate.addUpdOpeningEvaluation("SendApproveEvalReport", tenderId, lotId, roundId, userid_PA, userId_TC, handleSpecialChar.handleSpecialChar(request.getParameter("txtComments")), "senttotc").get(0);
                    }
                    if (commonMsgChk.getFlag().equals(true)) {
                        /* START : CODE TO SEND MAIL */
                        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                        String mailSubject = "e-GP System: PA has sent Evaluation Report to you";
                        String strFrmEmailId = "";

                        List<SPTenderCommonData> frmEmail = objTenderCommonService.returndata("getEmailIdfromUserId", userid_PA, null);
                        if (!frmEmail.isEmpty()) {
                            strFrmEmailId = frmEmail.get(0).getFieldName1(); // Email Id of Sender (TEC CP)
                        }
                        List<SPTenderCommonData> emails = objTenderCommonService.returndata("getEmailIdfromUserId", userId_TC, null);

                        String[] mail = {emails.get(0).getFieldName1()}; // Email Id of PE

                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();

                        //userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.EvalReportSentToTC(tenderId, frmEmail.get(0).getFieldName1()));
                        msgBoxContentUtility = null;
                        /* END : CODE TO SEND MAIL */

                        strFrmEmailId = null;
                        frmEmail = null;

                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                        response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&msgId=senttotc");
                    } else {
                        response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&msgId=error");
                    }
                } else if (usid.equals(userId_TC)) {
                    String eventType = commonService.getEventType(tenderId).toString();
                    if (eventType.equalsIgnoreCase("REOI")) {
                        commonMsgChk = addUpdate.addUpdOpeningEvaluation("ApproveEvalReportByTC", tenderId, roundId, lotId, "Approved", handleSpecialChar.handleSpecialChar(request.getParameter("txtComments"))).get(0);
                        if (commonMsgChk.getFlag().equals(true)) {
                            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                            response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&msgId=approved");
                        } else {
                            response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&msgId=error");
                        }
                    } else {
                        commonMsgChk = addUpdate.addUpdOpeningEvaluation("SendApproveEvalReport", tenderId, lotId, roundId, userId_TC, userid_PA, handleSpecialChar.handleSpecialChar(request.getParameter("txtComments")), "sentforloi").get(0);
                        if (commonMsgChk.getFlag().equals(true)) {
                            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                            List<SPTenderCommonData> frmEmail = objTenderCommonService.returndata("getEmailIdAfterTCApprove", userid_PA, tenderId);
                            String[] mails = new String[frmEmail.size()];
                            for (int i = 0; i < frmEmail.size(); i++) {
                                mails[i] = frmEmail.get(i).getFieldName1();
                            }
                            String bidderName;
                            String bidderAddress;
                            String packageDes;
                            String IdenRef;
                            String bidAmount;
                            String paName;
                            String paDesignation;
                            String paAgency;
                            String hopaName;
                            List<SPTenderCommonData> bidderInfo = objTenderCommonService.returndata("getBidderInfoAfterTCApprove", tenderId, roundId);
                            if (bidderInfo.get(0).getFieldName1().equalsIgnoreCase("company")) {
                                bidderName = bidderInfo.get(0).getFieldName2();
                                bidderAddress = bidderInfo.get(0).getFieldName3()
                                        + (bidderInfo.get(0).getFieldName4().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName4())
                                        + (bidderInfo.get(0).getFieldName5().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName5())
                                        + (bidderInfo.get(0).getFieldName6().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName6())
                                        + (bidderInfo.get(0).getFieldName7().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName7());
                            } else {
                                bidderName = bidderInfo.get(0).getFieldName2()
                                        + (bidderInfo.get(0).getFieldName3().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName3())
                                        + (bidderInfo.get(0).getFieldName4().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName4())
                                        + (bidderInfo.get(0).getFieldName5().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName5());
                                bidderAddress = bidderInfo.get(0).getFieldName6()
                                        + (bidderInfo.get(0).getFieldName7().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName7())
                                        + (bidderInfo.get(0).getFieldName8().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName8())
                                        + (bidderInfo.get(0).getFieldName9().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName9())
                                        + (bidderInfo.get(0).getFieldName10().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName10())
                                        + (bidderInfo.get(0).getFieldName11().equalsIgnoreCase("") ? "" : ", " + bidderInfo.get(0).getFieldName11());
                            }
                            List<SPTenderCommonData> tenderinfo = objTenderCommonService.returndata("getTenderInfoAfterTCApprove", tenderId, null);
                            IdenRef = tenderinfo.get(0).getFieldName1();
                            packageDes = tenderinfo.get(0).getFieldName2();
                            paName = tenderinfo.get(0).getFieldName3();
                            paDesignation = tenderinfo.get(0).getFieldName4();
                            paAgency = tenderinfo.get(0).getFieldName5();
                            hopaName = tenderinfo.get(0).getFieldName6();

                            List<SPCommonSearchData> amountInfo = commonSearchData.searchData("GetBidderAmountRank1", tenderId, lotId, roundId, null, null, null, null, null, null);
                            bidAmount = amountInfo.get(0).getFieldName1();
                            String wordAmount = NumberToWord.Convert(bidAmount);
                            bidAmount = " Nu. " + bidAmount + " (" + wordAmount + " Nu.)";

                            MailContentUtility mailContentUtility = new MailContentUtility();
                            SendMessageUtil sendMessageUtil = new SendMessageUtil();
                            sendMessageUtil.setEmailTo(mails);
                            sendMessageUtil.setEmailSub("Letter of Intent");
                            String mailText = mailContentUtility.mailAfterTCApproval(bidderName, bidderAddress, packageDes, IdenRef, bidAmount, hopaName, paAgency);
                            sendMessageUtil.setEmailMessage(mailText);
                            sendMessageUtil.sendEmail();
                            mails = null;
                            sendMessageUtil = null;

                            response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&msgId=approved");
                        } else {
                            response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&msgId=error");
                        }
                    }

                }
            }

        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--div class="contentArea_1"-->
        <div class="pageHead_1">
        <%if (!sendTo.equals("")) {%>
        Send Evaluation Report to <%=sendTo%>
        <%} else {%>
        Approval of Evaluation Report
        <%}%>
        <span class="c-alignment-right">
            <a href="EvalComm.jsp?tenderid=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
        </span>
        </div>
        <!--/div-->
        <%
            pageContext.setAttribute("tenderId", tenderId);
        %>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <form method="post" action="SendReportTC.jsp?tenderId=<%=tenderId%>" id="frmSubmit" onsubmit="return checkSubmit();" >
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                <tr>
                    <td width="20%" class="ff">Comments<span class="mandatory">*</span></td>
                    <td width ="80%">
                        <textarea rows="4" cols="100" id="txtComments" name="txtComments" class="formTxtBox_1" onblur="ClearCommentError();"></textarea><br/>
                        <span id="CommentErrorMsg" name="CommentErrorMsg" style="color:red;" ></span>
                    </td>
                </tr>
                <%--
                <tr>
                    <td width="22%" align="left" valign="middle" class="ff">Upload Evaluation Document : <span class="mandatory">*</span></td>
                    <td width="78%" align="left" valign="middle">
                        <a onclick="javascript:window.open('<%=request.getContextPath()%>/officer/EvalRptDocToTC.jsp?tenderId=<%=tenderId%>&roundId=<%=roundId%>', '', 'width=800px,height=650px,scrollbars=1','');" href="javascript:void(0);">Upload / Remove</a>                        
                        <div class="reqF_1" id="valUpload"></div>
                    </td>
                </tr>--%>
                <tr>
                    <td colspan="2" class="t-align-center">
                        <label class="formBtn_1">
                            <input type="submit" name="submit" id="btnSubmit" value="Submit"/>
                            <input type="hidden" name="lotId" id="lotId" value="<%=lotId%>"/>
                            <input type="hidden" name="roundId" id="roundId" value="<%=roundId%>"/>
                            <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>"/>
                        </label>
                    </td>
                </tr>
            </table>
            <%--
            <div id="dataDoc">
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th width="8%" class="t-align-center">S. No.</th>
                    <th class="t-align-center" width="23%">File Name</th>
                    <th class="t-align-center" width="28%">File Description</th>
                    <th class="t-align-center" width="7%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-center" width="18%">Action</th>

                        </tr>
                        
                        <%

                                    int docCnt = 0;
                                    TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    /*Listing of data*/
                                    for (SPTenderCommonData sptcd : tenderCS.returndata("evaluationDoc", String.valueOf(tenderId), String.valueOf(roundId))) {
                                        docCnt++;
                        %>
                        <tr>
                            <td class="t-align-left"><%=docCnt%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                            <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                            <td class="t-align-left">
                                <a href="<%=request.getContextPath()%>/ServletEvalRptDoc?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&roundId=<%=roundId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            </td>

                        </tr>

                        <%   if (sptcd != null) {
                                            sptcd = null;
                                        }
                                    }%>
            <%--<input type="hidden" name="manUpload" id="manUpload" value="<%=docCnt%>" >
            <% if (docCnt == 0) {%>
            <tr>
                <td colspan="5" class="t-align-center">No records found.</td>
            </tr>
            <%}%>
        </table>
    </div>--%>
        </form>
    </div>
    <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
</body>
</html>
