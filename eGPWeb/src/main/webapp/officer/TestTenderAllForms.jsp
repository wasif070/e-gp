<%-- 
    Document   : TestTenderAllForms
    Created on : May 28, 2011, 3:37:08 PM
    Author     : nishit
--%>

<%@page import="com.cptu.egp.eps.model.table.TblTenderSection"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderForms"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
<!--    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>-->
    <body>
        <%
            TenderDocumentSrBean dDocSrBean = new TenderDocumentSrBean();
            int sectionId = Integer.parseInt(request.getParameter("sectionId"));
            int porlotId = -1;
            if(request.getParameter("porlId")!=null){
                    porlotId = Integer.parseInt(request.getParameter("porlId"));
            }
            List<TblTenderForms> forms = dDocSrBean.getTenderForm(sectionId);
            int int_loopCnt = 0;
            String tenderId = "0";
            if(request.getParameter("tenderId")!=null){
                tenderId = request.getParameter("tenderId");
            }
            for(TblTenderForms ttf : forms){
           
        %>
        <jsp:include page="TestTenderForm.jsp">
            <jsp:param name="tenderId" value="<%=tenderId%>"></jsp:param>
            <jsp:param name="sectionId" value="<%=sectionId%>"></jsp:param>
            <jsp:param name="formId" value="<%=ttf.getTenderFormId()%>"></jsp:param>
            <jsp:param name="isPDF" value="true"></jsp:param>
            <jsp:param name="porlotId" value="<%=porlotId%>"></jsp:param>
            <jsp:param name="lotDetail" value="<%=int_loopCnt%>"></jsp:param>
            
        </jsp:include>
        <%int_loopCnt++;}%>
    </body>
</html>
