<%-- 
    Document   : ViewTOExtReqDetails
    Created on : Dec 21, 2010, 3:20:59 PM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Opening Date and Time Extension Request Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="dashboard_div">

            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Opening Date and Time Extension Request Details
                   <span style="float: right;" >
                    <a href="OpenComm.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
               </span>
                </div>
             
                <%
                    // Variable tenderId is defined by u on ur current page.
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>            

                <%
                     String tenderId="",
                            extId="";

                      if(request.getParameter("tenderId")!=null){
                            tenderId=request.getParameter("tenderId");
                        }

                      if(request.getParameter("extId")!=null){
                            extId=request.getParameter("extId");
                        }
                %>

                <%
                    List<SPTenderCommonData> lstExtensionDetails = tenderCommonService.returndata("getOpeningDtExtDetails", tenderId, extId);

                    if (!lstExtensionDetails.isEmpty() && lstExtensionDetails.size()>0){%>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td width="25%" class="t-align-left ff">Current Opening Date and Time </td>
                            <td width="75%" class="t-align-left"><%=currTenderOpeningDt%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Newly Proposed Opening Date and Time</td>
                            <td class="t-align-left"><%=lstExtensionDetails.get(0).getFieldName3()%></td>
                        </tr>
                         <tr>
                            <td class="t-align-left ff">Requested By</td>
                            <td class="t-align-left"><%=lstExtensionDetails.get(0).getFieldName4()%></td>
                        </tr>
                         <tr>
                            <td class="t-align-left ff">Status</td>
                            <td class="t-align-left"><%=lstExtensionDetails.get(0).getFieldName5()%></td>
                        </tr>
                         <tr>
                            <td class="t-align-left ff">Date and Time of Request</td>
                            <td class="t-align-left"><%=lstExtensionDetails.get(0).getFieldName2()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Remarks</td>
                            <td class="t-align-left"><%=lstExtensionDetails.get(0).getFieldName7()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Request sent to</td>
                            <td class="t-align-left"><%=lstExtensionDetails.get(0).getFieldName6()%></td>
                        </tr>
                        <tr>
                            <!--Change HOPE to HOPA by Proshanto-->
                            <td class="t-align-left ff">Comments by HOPA</td>
                            <td class="t-align-left"><%=lstExtensionDetails.get(0).getFieldName9()%></td>
                        </tr>
                         <tr>
                            <td class="t-align-left ff">Final Opening Date and Time</td>
                            <td class="t-align-left"><%=lstExtensionDetails.get(0).getFieldName8()%></td>
                        </tr>
                         <tr>
                            <td class="t-align-left ff">Date and Time of Approval</td>
                            <td class="t-align-left"><%=lstExtensionDetails.get(0).getFieldName10()%></td>
                        </tr>

                    </table>
                    
                <%} else {%>
                     <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td width="100%" class="t-align-left">No Details found.</td>
                        </tr>
                    </table>
                <%}
                    lstExtensionDetails=null;

                    tenderId=null;
                    extId=null;

                %>
                
                
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
