<%-- 
    Document   : EvalClariEditPkg
    Created on : Jan 17, 2011, 6:16:28 PM
    Author     : Rajesh Singh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="texxt/html; charset=UTF-8">
        <title>Evaluation Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function Validate(){
                var cnt=$(":checkbox:checked").length;
                if(cnt==0){
                    jAlert("Please select atleast one checkbox.","Evaluation Clarification Alert", function(RetVal) {
                    });
                    return false;
                }
            }

            function Reset(){
                $(":checkbox:checked").attr('checked',false);
                return false;
            }
        </script>
    </head>
    <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <%
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    //userId = Integer.parseInt(session.getAttribute("userId").toString());
                    userId = session.getAttribute("userId").toString();
                }
                boolean isSubDt = false;
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }

                //List<CommonTenderDetails> commonTenderDetails = tenderSrBean.getAPPDetails(Integer.parseInt(tenderId), "tender");
                //String docAvlMethod = commonTenderDetails.get(0).getDocAvlMethod();
                //String docAvlMethod = commonTenderDetails.get(0).getTenderLotSecId();
                
    %>
    <body>
        <div class="dashboard_div">

            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <form id="frmEvalClari" name="frmEvalClari" action="" method="post">
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->

                <%
                            if (request.getParameter("btnSendLot") != null) {

                                String userid = "";
                                HttpSession hs = request.getSession();
                                if (hs.getAttribute("userId") != null) {
                                    userid = hs.getAttribute("userId").toString();
                                }


                                //Deleting all data from the table.
                                String table = "", updateString = "", whereCondition = "";
                                table = "tbl_EvalServiceForms";
                                whereCondition = "tenderId=" + request.getParameter("tenderId") + " and configBy=" + userid;

                                CommonXMLSPService commonXMLSPService1 = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                CommonMsgChk commonMsgChk1 = commonXMLSPService1.insertDataBySP("delete", "tbl_EvalServiceForms", null, whereCondition).get(0);
                                //out.println(commonMsgChk.getMsg());
                                if (commonMsgChk1.getFlag() == true) {
                                    //End Deleting Code.

                                    String dtXml = "";
                                    java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                                    int lotcount = Integer.parseInt(request.getParameter("countlot"));
                                    int errorcount = 0;
                                    //Lot wise for lop
                                    for (int z = 0; z < lotcount; z++) {
                                        int formcount = Integer.parseInt(request.getParameter("countform" + String.valueOf(z)));
                                        for (int y = 0; y < formcount; y++) {
                                            
                                            if (request.getParameter("lotfromlist" + String.valueOf(z) + String.valueOf(y)) != null) {
                                                dtXml = "<root><tbl_EvalServiceForms tenderId=\"" + request.getParameter("tenderId") + "\" "
                                                        + "pkgLotId=\"" + request.getParameter("lotNumber" + String.valueOf(z)) + "\" "
                                                        + "tenderFormId=\"" + request.getParameter("lotfromlist" + String.valueOf(z) + String.valueOf(y)) + "\"  maxMarks=\" "
                                                        + "\" configBy=\"" + userid + "\" "
                                                        + "configDt=\"" + format.format(new Date()) + "\"/></root>";

                                                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                                CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalServiceForms", dtXml, "").get(0);
                                                if (commonMsgChk.getFlag() == false) {
                                                    errorcount = 1;
                                                }
                                            }
                                        }
                                    }


                                    if (errorcount == 1) {
                                        response.sendRedirect("EvalClariPkg.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=false");
                                    } else {
                                        response.sendRedirect("EvalClariMMark.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=true");
                                    }
                                }
                            }


                            if (request.getParameter("btnSendfrom") != null) {

                                String userid = "";
                                HttpSession hs = request.getSession();
                                if (hs.getAttribute("userId") != null) {
                                    userid = hs.getAttribute("userId").toString();
                                }

                                //Deleting all data from the table.
                                String table = "", updateString = "", whereCondition = "";
                                table = "tbl_EvalServiceForms";
                                whereCondition = "tenderId=" + request.getParameter("tenderId") + " and configBy=" + userid;

                                CommonXMLSPService commonXMLSPService1 = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                CommonMsgChk commonMsgChk1 = commonXMLSPService1.insertDataBySP("delete", "tbl_EvalServiceForms", null, whereCondition).get(0);
                                //out.println(commonMsgChk.getMsg());
                                if (commonMsgChk1.getFlag() == true) {
                                    //End Deleting Code.

                                    String dtXml = "";
                                    java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                                    int errorcount = 0;
                                    //Lot wise for lop

                                    int formcount = Integer.parseInt(request.getParameter("countforms"));
                                    for (int y = 0; y < formcount; y++) {
                                        
                                        if (request.getParameter("fromlist" + String.valueOf(y)) != null) {
                                            dtXml = "<root><tbl_EvalServiceForms tenderId=\"" + request.getParameter("tenderId") + "\" "
                                                    + "pkgLotId=\"" + 0 + "\" "
                                                    + "tenderFormId=\"" + request.getParameter("fromlist" + String.valueOf(y)) + "\"  maxMarks=\" "
                                                    + "\" configBy=\"" + userid + "\" "
                                                    + "configDt=\"" + format.format(new Date()) + "\"/></root>";

                                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                            CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalServiceForms", dtXml, "").get(0);
                                            if (commonMsgChk.getFlag() == false) {
                                                errorcount = 1;
                                            }
                                        }
                                    }


                                    if (errorcount == 1) {
                                        response.sendRedirect("EvalClariPkg.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=false");
                                    } else {
                                        response.sendRedirect("EvalClariMMark.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=true");
                                    }
                                }
                            }
                %>
                <%
                            // Variable tenderId is defined by u on ur current page.
                            String msg = "";
                            msg = request.getParameter("msg");
                %>
                <div class="contentArea_1">
                    <div class="pageHead_1">Evaluation Clarification
                    <span style="float:right;"><a href="EvalComm.jsp?tenderid=<%=tenderId%>" class="action-button-goback"> Go Back To Dashboard</a></span>
                    </div>
                    <% if (msg != null && msg.contains("false")) {%>
                    <div class="responseMsg errorMsg t_space">Operation Failed, Please try again later.</div>
                    <%  }%>
                    <%
                                pageContext.setAttribute("tenderId", tenderId);
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <%
                                boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                    %>
                    <div>&nbsp;</div>
                    <%if (!"TEC".equalsIgnoreCase(request.getParameter("comType"))) {%>
                    <jsp:include page="officerTabPanel.jsp" >
                        <jsp:param name="tab" value="7" />
                    </jsp:include>
                    <% }%>
                    <div class="tabPanelArea_1">
                        <%
                                    if ("rp".equalsIgnoreCase(request.getParameter("st"))) {
                                        pageContext.setAttribute("TSCtab", "3");
                                    } else {
                                        pageContext.setAttribute("TSCtab", "4");
                                    }
                        %>
                        <%@include file="../resources/common/AfterLoginTSC.jsp"%>
                        <%
                                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                    List<SPCommonSearchData> submitDateSts = commonSearchService.searchData("chkSubmissionDt", tenderId, null, null, null, null, null, null, null, null);
                                    if (submitDateSts.get(0).getFieldName1().equalsIgnoreCase("true")) {
                                        isSubDt = true;
                                    } else {
                                        isSubDt = false;
                                    }
                                    if (isTenPackageWis) {
                                        //List<SPCommonSearchData> packageList = ;
                                        for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                            </tr>

                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="22%" class="t-align-left ff">Form Name</th>
                                <th width="23%" class="t-align-left">Action</th>
                            </tr>
                            <%
                                                                        int l = 0;
                                                                        for (SPCommonSearchData formdetails : commonSearchService.searchData("getselectedformDetails", tenderId, userId, "0", null, null, null, null, null, null)) {
                            %>
                            <tr>
                                <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                                <td class="t-align-left">
                                    <%if (formdetails.getFieldName3().equals("yes")) {%>
                                    <input type="checkbox" checked="checked" name="fromlist<%=l%>" id="fromlist<%=l%>" value="<%=formdetails.getFieldName1()%>" />
                                    <%} else {%>
                                    <input type="checkbox" name="fromlist<%=l%>" id="fromlist<%=l%>" value="<%=formdetails.getFieldName1()%>" />
                                    <%}%>
                                </td>
                            </tr>
                            <% l++;
                                                                        }%>
                            <input type="hidden" name="countforms" id="countforms" value="<%=l%>"/>
                        </table>
                        <div class="t-align-center t_space">
                            <label class="formBtn_1">
                                <input name="btnSendfrom" id="btnSendfrom" type="submit" value="Submit" onclick="return Validate();"/>
                            </label>
                            <label class="formBtn_1">
                                <input name="btnSendreset" type="submit" value="Reset" onclick="return Reset();"/>
                            </label>
                        </div>
                        <% }
                                                            } else {

                                                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%  }
                                                                int i = 0;
                                                                for (SPCommonSearchData lotList : commonSearchService.searchData("gettenderlotbytenderid", tenderId, "", "", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                            <tr>
                                <td width="22%" class="t-align-left ff">Lot No. :</td>
                                <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Lot Description :</td>
                                <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                            </tr>
                        </table><table width="100%" cellspacing="0" class="tableList_1" id="tb2">
                            <tr>
                                <th width="22%" class="t-align-left ff">Form Name</th>
                                <th width="23%" class="t-align-left">Action</th>
                            </tr>
                            <%
                                                                                                int j = 0;
                                                                                                for (SPCommonSearchData formdetails : commonSearchService.searchData("getselectedformDetails", tenderId, userId, lotList.getFieldName3(), null, null, null, null, null, null)) {
                            %>
                            <tr>
                                <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                                <td class="t-align-left">
                                    <%if (formdetails.getFieldName3().equals("yes")) {%>
                                    <input type="checkbox" checked="checked" name="lotfromlist<%=i%><%=j%>" id="lotfromlist<%=i%><%=j%>" value="<%=formdetails.getFieldName1()%>" />
                                    <%} else {%>
                                    <input type="checkbox" name="lotfromlist<%=i%><%=j%>" id="lotfromlist<%=i%><%=j%>" value="<%=formdetails.getFieldName1()%>" />
                                    <%}%>
                                </td>
                            </tr>
                            <% j++;
                                                                                                }%>
                            <input type="hidden" name="countform<%=i%>" id="countform<%=i%>" value="<%=j%>"/>
                        </table>
                        <input type="hidden" name="lotNumber<%=i%>" id="lotNumber<%=i%>" value="<%=lotList.getFieldName1()%>"/>
                        <%     i++;
                                                                }%>
                        <input type="hidden" name="countlot" id="countlot" value="<%=i%>"/>
                        <div class="t-align-center t_space">
                            <label class="formBtn_1">
                                <input name="btnSendLot" id="btnSendLot" type="submit" value="Submit" onclick="return Validate();"/>
                            </label>
                            <label class="formBtn_1">
                                <input name="btnSendreset" type="submit" value="Reset" onclick="return Reset();"/>
                            </label>
                        </div>
                        <% }%>


                    </div>
                    <div>&nbsp;</div>
                    <!--Dashboard Content Part End-->
                </div>
            </form>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
