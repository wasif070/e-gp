<%-- 
    Document   : ReportLotPkgSelection
    Created on : Dec 29, 2010, 1:15:25 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.model.table.TblReportLots"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.web.servicebean.ReportCreationSrBean" %>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String tenderid = request.getParameter("tenderid");
                    String labTORPOR="TOR";
                    String labTERPER="TER";
                    String lotpkgLabel ="Lot";
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    if(commonService.getProcNature(tenderid).toString().equalsIgnoreCase("services")){
                        labTORPOR = "POR";
                        labTERPER = "PER";
                        lotpkgLabel = "Package";
                    }
                    
                    String reportType="TOR/TER";
                    if(request.getParameter("reportType")!=null)
                    {
                        reportType=request.getParameter("reportType");
                    }
                   // out.println("report type="+reportType);
                    
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Select <%=lotpkgLabel%></title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                    $('#search').submit(function() {
                        var vbool=true;
                         var cnt=$('#cnt').val();
                         var chk=0;
                         var terCnt=0;
                         var torCnt=0;
                         var lotId=0;
                         for(var i=0;i<cnt;i++){
                            if($('#formId'+i).attr("checked")){
                                lotId = $('#formId'+i).val();
                                if($('#ter'+i).val()=="Yes"){
                                    terCnt++;
                                }
                                if($('#tor'+i).val()=="Yes"){
                                    torCnt++;
                                }
                                chk++;
                            }
                         }
                         if(chk==0){
                              jAlert("Please Select Lot.","Lot Selection Alert", function(RetVal) {
                              });
                             return false;
                         }
                         if(terCnt!=0 && torCnt!=0){
                            jAlert("BOR & BER already created for this Lot No.","Form Selection Alert", function(RetVal) {
                            });
                             return false;
                         }
                         if(terCnt!=0  && '<%=reportType%>'=='TER'){
                            $.ajax({
                                type: "POST",
                                url: "<%=request.getContextPath()%>/ReportCreationServlet",
                                data:"action=ajaxLotRptTERTORCnt&lotId="+lotId+"&stat=TER&repId=<%=request.getParameter("repId")%>",
                                async: false,
                                success: function(j){
                                    if(j.toString()!="0"){
                                        vbool = false;
                                        jAlert("BER already created for this Lot No.","Form Selection Alert", function(RetVal) {
                                        });
                                    }
                                }
                            });
                         }
                         if(torCnt!=0 && '<%=reportType%>'=='TOR'){
                            $.ajax({
                                type: "POST",
                                url: "<%=request.getContextPath()%>/ReportCreationServlet",
                                data:"action=ajaxLotRptTERTORCnt&lotId="+lotId+"&stat=TOR&repId=<%=request.getParameter("repId")%>",
                                async: false,
                                success: function(j){                                    
                                    if(j.toString()!="0"){
                                        vbool = false;
                                        jAlert("BOR already created for this Lot No.","Form Selection Alert", function(RetVal) {
                                        });                                        
                                    }
                                }
                            });
                         }
                         return vbool;
                    });
                });
        </script>
    </head>
    <body>
        <%
             String repId = request.getParameter("repId");
             ReportCreationSrBean rcsb = new ReportCreationSrBean();
             boolean isNot=false;
             Object lot = null;
             if("y".equals(request.getParameter("isNot"))){
                isNot=true;
                lot = rcsb.getReportLots(repId);
             }
             if("Select Lot".equalsIgnoreCase(request.getParameter("btnLot")) || "Select Package".equalsIgnoreCase(request.getParameter("btnLot"))){
                TblReportLots rptlots = new TblReportLots();
                rptlots.setReportId(Integer.parseInt(repId));
                rptlots.setPkgLotId(Integer.parseInt(request.getParameter("lpId")));
                if(!rcsb.insertReportLots(rptlots,isNot)){
                   out.println("Error in inserting Lots");
                }
                 response.sendRedirect("ReportFormSelection.jsp?tenderid="+tenderid+"&repId="+repId+"&plId="+request.getParameter("lpId")+"&reportType="+reportType);
             }
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <%                   
                    pageContext.setAttribute("tenderId", tenderid);                    
                    String isTORDisp = "";
                    /*String procMethod = commonService.getProcMethod(tenderid).toString();
                    String eventType = commonService.getEventType(tenderid).toString();
                    if(eventType.equals("REOI") || eventType.equals("PQ") || eventType.equals("1 stage-TSTM")){
                        isTORDisp = "style='display: none;'";
                    }*/
        %>
        <div class="pageHead_1">Select <%=lotpkgLabel%><span style="float: right;"><a class="action-button-goback" href="Notice.jsp?tenderid=<%=tenderid%>">Go back to Dashboard</a></span></div>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <form action="ReportLotPkgSelection.jsp?tenderid=<%=tenderid%>&repId=<%=repId%>&reportType=<%=reportType%><%if(isNot){out.print("&isNot=y");}%>" method="post" id="search">
            <div class="tabPanelArea_1">                
                <table class="tableList_1 t_space" width="100%" cellspacing="0">
                    <tbody>
                        <tr>                            
                            <th class="t-align-left ff">Select</th>
                            <th><%=lotpkgLabel%> No</th>
                            <th><%=lotpkgLabel%> Description</th>
                            <th <%=isTORDisp%>><%=labTORPOR%> Prepared</th>
                            <th><%=labTERPER%> Prepared</th>

                        </tr>
                        <%
                            java.util.List<SPCommonSearchDataMore> list = rcsb.getReportFormDetail(tenderid,repId);
                            int i=0;for(SPCommonSearchDataMore objects : list){
                        %>
                        <tr>
                            <td class="t-align-center" width="6%">
                                <input name="lpId" id="formId<%=i%>" type="radio" value="<%=objects.getFieldName1()%>" <%if(lot!=null && objects.getFieldName1().equals(lot.toString())){out.print("checked");}%> />
                            </td>
                            <td class="t-align-center" width="10%"><%=objects.getFieldName2()%></td>
                            <td width="68%"><%=objects.getFieldName3()%></td>
                            <td class="t-align-center" width="8%" <%=isTORDisp%>>
                                <%=objects.getFieldName4()%>
                                <input type="hidden" value="<%=objects.getFieldName4()%>" id="tor<%=i%>"/>
                            </td>
                            <td class="t-align-center" width="8%">
                                <%=objects.getFieldName5()%>
                                <input type="hidden" value="<%=objects.getFieldName5()%>" id="ter<%=i%>"/>
                            </td>
                        </tr>
                        <%i++;}%>
                    </tbody></table>
                    <input type="hidden" id="cnt" value="<%=list.size()%>">                    
                    <div class="t-align-center t_space">
                    <!--Below condtion is placed to stop editing of Lot-->
                    <!--Starts-->
                    <%if(!(isNot && lot!=null)){%>
                    <label class="formBtn_1">
                        <input name="btnLot" id="button3" value="Select <%=lotpkgLabel%>" type="submit">
                    </label>
                    <%}%>
                    <!--Ends-->
                    <%if(isNot && lot!=null){%>
                    <a href="ReportFormSelection.jsp?tenderid=<%=tenderid%>&repId=<%=repId%>&plId=<%=lot%>&isNot=y" class="anchorLink">Next</a>
                    <%}%>
                </div>                            
            </div>
        </form>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
