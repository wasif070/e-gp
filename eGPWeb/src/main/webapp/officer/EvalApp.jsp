<%--
    Document   : EvalApp
    Created on : Dec 2nd, 2010, 3:16:31 PM
    Author     : Rajesh Singh
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.SHA1HashEncryption" %>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Member’s Declaration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function(){
                $('#frmTOS').validate({
                    rules:{
                        txtpassword:{required:true},
                        txtcomments:{
                            required:true,
                            maxlength:500}
                    },
                    messages:{
                        txtpassword:{required:"<div class='reqF_1'>Please enter Password.</div>"},
                        txtcomments:{
                            required:"<div class='reqF_1'>Please enter Comments.</div>",
                            maxlength:"<div class='reqF_1'>Only 500 characters are allowed.</div>"}
                    }
                });
            })
        </script>

    </head>
    <body>
        <%
        TenderCommonService tenderCommonServiceObj = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                    String i_tenderId = "", tenderId = "";
                    String strCommTyp = "";
                    //Fetch tenderid from querysting and get corrigendumid
                    if (!request.getParameter("id").equals("")) {
                        i_tenderId = request.getParameter("id");
                    }

                     if (request.getParameter("comType") != null && !"null".equalsIgnoreCase(request.getParameter("comType"))) {
                         strCommTyp = request.getParameter("comType");
                     }

                    tenderId = request.getParameter("tenderid");

                    //This condition is used to submit data in database, this is button click event.
                    if (request.getParameter("btnsubmit") != null) {
                        strCommTyp = request.getParameter("hdnCommTyp");
                        String strPath = "";
                        
                        //Get password and check in database for existence, if found perform insert operation
                        //else show message of invalid password.
                        System.out.print(SHA1HashEncryption.encodeStringSHA1(request.getParameter("txtpassword")));
                        
                        List<SPTenderCommonData> list = tenderCommonServiceObj.returndata("checkLogin", request.getParameter("hiddenemail"), SHA1HashEncryption.encodeStringSHA1(request.getParameter("txtpassword")));

                        if (!list.isEmpty()) {
                            if (!request.getParameter("id").equals("") && !request.getParameter("uid").equals("")) {
                                String table = "", updateString = "", whereCondition = "";

                                table = "tbl_Committeemembers";
                                updateString = "appStatus='approved', appDate=getdate(), remarks='" + handleSpecialChar.handleSpecialChar(request.getParameter("txtcomments")) + "'";
                                whereCondition = "committeeId=" + request.getParameter("id") + " And userID=" + request.getParameter("uid");
                                
                                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
        
                                // Added for Audit Trail
                                commonXMLSPService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                commonXMLSPService.setModuleName(EgpModule.Evaluation.getName());
                                String auditValues[]=new String[4];
                                auditValues[0]="tenderId";
                                auditValues[1]= request.getParameter("tenderid");
                                auditValues[3]= request.getParameter("txtcomments");    
        
                                if("tsc".equalsIgnoreCase(strCommTyp))
                                {
                                     auditValues[2]="Declaration for Evaluation given by TSC/PSC Member";
                                }   
                                else
                                {
                                     auditValues[2]="Declaration for Evaluation given by TEC/PEC Member";
                                }                                                                                                  
                                CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_Committeemembers", updateString, whereCondition,auditValues).get(0);
                                out.println(commonMsgChk.getMsg());
                                
                                if("tsc".equalsIgnoreCase(strCommTyp)) {
                                    strPath = "EvalTSC.jsp?tenderId=" + tenderId + "&comType=" + request.getParameter("comType")+"&flag=true";
                                } else {
                                    strPath = "EvalComm.jsp?tenderid=" + tenderId + "&comType=" + request.getParameter("comType")+"&flag=true";
                                }
                                //response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&comType=" + request.getParameter("comType")+"&flag=true");
                                response.sendRedirect(strPath);
                            }

                        } else {
                            response.sendRedirect("EvalApp.jsp?tenderid="+tenderId+"&id=" + request.getParameter("id") + "&uid=" + request.getParameter("uid") + "&Name=" + request.getParameter("hdnName") + "&comType="+request.getParameter("comType")+"&status=false");
                        }
                    }

        %>



        <!--Dashboard Header Start-->

        <%@include file="../resources/common/AfterLoginTop.jsp" %>

        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <form id="frmTOS" action="EvalApp.jsp?tenderid=<%=tenderId%>&id=<%=request.getParameter("id")%>&uid=<%=request.getParameter("uid")%>&comType=<%=request.getParameter("comType")%>" method="post">
                <%
                            //TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                
                List<SPTenderCommonData> list = tenderCommonServiceObj.returndata("getCommitteeInfo", i_tenderId, request.getParameter("uid"));
                            if (!list.isEmpty()) {
                                strCommTyp = list.get(0).getFieldName5();
                %>
                
                <div class="pageHead_1">Committee Member’s Declaration
                    <span class="c-alignment-right">
                        <%if("tsc".equalsIgnoreCase(strCommTyp)) { %>
                        <a href="EvalTSC.jsp?tenderId=<%=list.get(0).getFieldName4()%>&f=<%=request.getParameter("f")%>&comType=<%=request.getParameter("comType")%>" class="action-button-goback">Go Back to Dashboard</a>
                        <%} else {%>
                        <a href="EvalComm.jsp?tenderid=<%=list.get(0).getFieldName4()%>&f=<%=request.getParameter("f")%>&comType=<%=request.getParameter("comType")%>" class="action-button-goback">Go Back to Dashboard</a>
                        <%}%>

                    </span>
                </div>

                 <%
                                pageContext.setAttribute("tenderId", tenderId);
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>

                <div style="font-style: italic" class="t-align-left t_space">
                    <%if (request.getParameter("status") != null && request.getParameter("status").equals("false")) {%>
                    <div id="errordiv" class="responseMsg errorMsg">Please enter Correct Password.</div>
                    <%}%>
                    <span class="t-align-left" style="float:left; font-weight:normal;">
				Fields marked with (<span class="mandatory">*</span>) are mandatory
                    </span><br/>
                </div>
                <div>&nbsp;</div>
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <td width="15%" class="ff">Committee Member's Name :</td>
                        <td width="85%"><%=list.get(0).getFieldName6()%>
                            <input type="hidden" id="hdnName" name="hdnName" value="<%=list.get(0).getFieldName6()%>"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="ff">e-mail ID :  </td>

                        <td width="85%"><%=list.get(0).getFieldName1()%><input type="hidden" name="hiddenid" id="hiddenid" value=""/>
                            <input type="hidden" name="hiddenemail" id="hiddenemail" value="<%=list.get(0).getFieldName1()%>"/>
                        </td>


                    </tr>
                    <tr>
                        <td class="ff" valign="top">Password : <span class="mandatory">*</span> </td>
                        <td>
                            <input name="txtpassword" type="password" class="formTxtBox_1" id="textfield" style="width:200px;" autocomplete="off" />
                        </td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top">Declaration Text : </td>
                        <td>
                            I do hereby declare and confirm that I have no business or other links to any of the competing tenderer or Applicant
                        </td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top">Comments : <span class="mandatory">*</span></td>
                        <td>
                            <textarea name="txtcomments" id="txtcomments" class="formTxtBox_1" cols="35" rows="5"></textarea>
                        </td>
                    </tr>
                </table>
                <%}%>
                <table width="100%" cellspacing="0" class="t_space">
                    <tr><td width="15%"></td><td width="85%" class="t-align-left">
                        <label class="formBtn_1">
                        <input name="btnsubmit" type="submit" value="Submit" /></label>
                    </td></tr></table>
                <input type="hidden" name="hdnCommTyp" id="hdnCommTyp" value="<%=strCommTyp%>">
            </form>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
        </div>
        <!--Dashboard Footer Start-->

        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <!--Dashboard Footer End-->

        
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabEval");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
