<%-- 
    Document   : EditCT
    Created on : Aug 24, 2011, 5:58:51 PM
    Author     : Sreenu.Durga
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsContractTermination"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblNoaIssueDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsCTReasonType"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsCTReasonTypeBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Contact Termination Page</title>
    </head>
    <%
        ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
    %>
    <%
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                int contractUserId = 0;
                if (request.getParameter("contractUserId") != null) {
                    contractUserId = Integer.parseInt(request.getParameter("contractUserId"));
                }
                int contractSignId = 0;
                if (request.getParameter("contractSignId") != null) {
                    contractSignId = Integer.parseInt(request.getParameter("contractSignId"));
                }
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                String lotId = "";
                if (request.getParameter("lotId") != null) {
                    pageContext.setAttribute("lotId", request.getParameter("lotId"));
                    lotId = request.getParameter("lotId");
                }
                int contractTerminationId = 0;
                if (request.getParameter("contractTerminationId") != null) {
                    contractTerminationId = Integer.parseInt(request.getParameter("contractTerminationId"));
                }
                CmsCTReasonTypeBean cmsCTReasonTypeBean = new CmsCTReasonTypeBean();
                cmsCTReasonTypeBean.setLogUserId(userId);
                List<TblCmsCTReasonType> cmsCTReasonTypeList = cmsCTReasonTypeBean.getAllCmsCTReasonType();
                String perSAmt = "";
                NOAServiceImpl noaService = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
                noaService.setLogUserId(userId);
                List<TblNoaIssueDetails> lstNoa = noaService.getPerformanceSecAmt(Integer.parseInt(tenderId), contractUserId, Integer.parseInt(lotId));
                if (!lstNoa.isEmpty() && lstNoa.size() > 0) {
                    perSAmt = lstNoa.get(0).getPerfSecAmt().toString();
                }
                CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                cmsContractTerminationBean.setLogUserId(userId);
                TblCmsContractTermination tblCmsContractTermination = cmsContractTerminationBean.getCmsContractTermination(contractTerminationId);
                if (tblCmsContractTermination == null) {
                    response.sendRedirect("SessionTimedOut.jsp");
                }
    %>
    <script language="javascript">
        /* Call Calendar function */
        function GetCalendar(txtname,controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: true,
                dateFormat:"%d-%b-%Y",
                onSelect: function() {                    
                    var date = Calendar.intToDate(this.selection.get());
                    var DateVar = document.getElementById("curDate").value;
                    var currentDate =Date.parse(DateVar);
                    //var currentDate = new Date();
                    if(date > currentDate){
                        jAlert("Date should be past or Current","Warning!");
                        document.getElementById(txtname).value = "";
                    }else{
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                    }
                    this.hide();
                }
            });
            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })
        }
    </script>
    <script type="text/javascript">
//        $(document).ready(function(){
//            $("#formContractTermination").validate({
//                rules: {
//                    txtDateOfTermination:{required: true},
//                    checkReasonType:{required: true},
//                    radioReleasePG:{required: true},
//                    txtReason:{required: true}
//                },
//                messages: {
//                    txtDateOfTermination: {
//                        required: "<div class='reqF_1'>Please select Date</div>"
//                    },
//                    checkReasonType:{
//                        required: "<div class='reqF_1'>Please check atleast one reason for Contract Termination</div>"
//                    },
//                    radioReleasePG: {
//                        required: "<div class='reqF_1'>Please select any one option</div>"
//                    },
//                    txtReason: {
//                        required: "<div class='reqF_1'>Please enter description for Contract Termination</div>"
//                    }
//                },
//                errorPlacement:function(error ,element){
//                    error.insertAfter(element);
//                }
//            });
//        });
    </script>

    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                            <br/>
                            <!--Page Content Start-->
                            <div class="pageHead_1"> Edit Contract Termination Page
                                <span style="float: right; text-align: right;">
                                    <a class="action-button-goback" href="TabContractTermination.jsp?tenderId=<%=tenderId%>">Go back</a>
                                </span>
                            </div>                          
                            <form method="POST" id="formContractTermination" name="frm" action="<%= request.getContextPath() %>/ContractTerminationServlet?action=edit">
                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1" >
                                    <tr id="tblRow_mandatoryMessage">
                                        <td style="font-style: italic" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.
                                            <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>">
                                            <input type="hidden" name="contractTerminationId" id="contractTerminationId" value="<%=contractTerminationId%>">
                                            <input type="hidden" name="lotId" id="lotId" value="<%=lotId%>">
                                            <input type="hidden" name="contractSignId" id="contractSignId" value="<%=contractSignId%>">
                                        </td>
                                    </tr>
                                    <tr id="tblRow_dateOfTermination">
                                        <td class="ff">Select Contract Termination Date : <span>*</span></td>
                                        <td class="ff">
                                            <input name="txtDateOfTermination" type="text" class="formTxtBox_1" id="txtDateOfTermination" readonly="true" onClick="GetCalendar('txtDateOfTermination','txtDateOfTermination');" value="<%=DateUtils.customDateFormate(DateUtils.convertStringtoDate(tblCmsContractTermination.getDateOfTermination().toString(),"yyyy-MM-dd HH:mm:ss"))%>" />
                                            &nbsp;
                                            <a href="javascript:void(0);" onclick ="GetCalendar('txtDateOfTermination','imgCal');" title="Calender">
                                                <img id="imgCal" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"/>
                                            </a>
                                            <span id="txtDateOfTerminationspan" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr id="tblRow_typeOfReason">
                                        <td class="ff">Select Reason for Contract Termination : <span>*</span></td>
                                        <td class="ff">
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1" >
                                                <%
                                                            for (TblCmsCTReasonType cmsCTReasonType : cmsCTReasonTypeList) {
                                                                String check = "";
                                                                String[] reasonTypeIds = tblCmsContractTermination.getReasonType().split("\\^");
                                                                for (String reasonTypeId : reasonTypeIds) {
                                                                    int reasonId = Integer.parseInt(reasonTypeId);
                                                                    if (reasonId == cmsCTReasonType.getCtReasonTypeId()) {
                                                                        check = "checked";
                                                                        break;
                                                                    }
                                                                }
                                                                out.print("<tr>"
                                                                        + "<td class='ff'>"
                                                                        + " <input type='checkbox' name='checkReasonType' id='checkReasonType' value='"
                                                                        + cmsCTReasonType.getCtReasonTypeId() + "' " + check + "'>&nbsp;&nbsp;" + cmsCTReasonType.getCtReason()
                                                                        + "&nbsp;&nbsp;&nbsp;&nbsp;"
                                                                        + "</td>"
                                                                        + "</tr>");
                                                            }// outer for
                                                %>
                                            </table>
                                            <span id="checkReasonTypespan" class="reqF_1"></span>
                                        </td>

                                    </tr>
                                    <%--<tr id="tblRow_releasePerformanceGuarantee" >
                                        <td  class="ff">Performance Security : <span>*</span></td>
                                        <td class="ff">
                                            <%
                                                        String releasePGForfeit = "";
                                                        String releasePGRelease = "";
                                                        if ("release".equalsIgnoreCase(tblCmsContractTermination.getReleasePg())) {
                                                            releasePGForfeit = "";
                                                            releasePGRelease = "checked";
                                                        } else {
                                                            releasePGForfeit = "checked";
                                                            releasePGRelease = "";
                                                        }
                                            %>
                                            <input type="radio" name="radioReleasePG" id="radioReleasePG"  value="release"  <%=releasePGRelease%> >&nbsp;<b>Release</b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="radioReleasePG" id="radioReleasePG" value="forfeit" <%=releasePGForfeit%> >&nbsp;<b>Compensiate</b>
                                        </td>
                                        <%
                                                    if (!"".equals(perSAmt)) {
                                                        out.print("<td>");
                                                        out.print("Performance Security (In BTN): " + perSAmt);
                                                        out.print(" </td>");
                                                    }
                                        %>
                                    </tr>--%>
                                    <tr>
                                        <td class="ff" width="25%"  class="t-align-left">Payment Settlement Amount (In Nu.) :</td>
                                        <td   class="t-align-left">
                                            <input type="text" class="formTxtBox_1" name="PSAtxt" id="PSAtxtid" maxlength="12" value="<%=tblCmsContractTermination.getPaySetAmt()%>"/>
                                        <span id="PSAtxtspan" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="17%" class="t-align-left ff">Remarks for Payment Settlement Amount :</td>
                                        <td width="83%" class="t-align-left">
                                            <label>
                                                <textarea name="txtRemarks"  rows="3" class="formTxtBox_1" id="txtRemarks" style="width:360px;"><%=tblCmsContractTermination.getPaySetAmtRemarks()%></textarea>
                                            </label>
                                            <span id="txtRemarksspan" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr id="tblRow_reason">
                                        <td  class="ff">Reason For Termination : <span>*</span></td>
                                        <td >
                                            <textarea name="txtReason" rows="5" cols="50" style="width:90%;" class="formTxtBox_1" id="txtReason" >
                                                <%= tblCmsContractTermination.getReason()%>
                                            </textarea>
                                            <script type="text/javascript">
                                                CKEDITOR.replace( 'txtReason',{toolbar : "egpToolbar"});
                                            </script>
                                            <span id="SPtxtNewsDetl" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr id="tblRow_formButton">
                                        <td class="t-align-center" colspan="2" >
                                            <label class="formBtn_1">
                                                <input type="submit" align="middle" name="submit" id="submit" value="Edit Terminate" onclick="return chkSubmit();"/>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
        <script type="text/javascript">
       function chkSubmit()
        {
            var j=0; var m=0;
            var vbool = true;
            if(document.getElementById("txtDateOfTermination").value == "")
            {
                document.getElementById("txtDateOfTerminationspan").innerHTML = "Please select Date";
                vbool= false;
            }
            else{document.getElementById("txtDateOfTerminationspan").innerHTML="";}

            for (i=0; i<document.frm.checkReasonType.length; i++){
            if (document.frm.checkReasonType[i].checked==true)
            {j++;}
            }
            if(j==0)
            {
               document.getElementById("checkReasonTypespan").innerHTML = "Please check atleast one reason for Contract Termination";
               vbool= false;
            }
            else{document.getElementById("checkReasonTypespan").innerHTML ="";}
            if(document.getElementById("PSAtxtid").value!="")
            {
               if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("PSAtxtid").value))
               {
                  if(/^\d*\.?\d*$/.test(document.getElementById("PSAtxtid").value))
                  {
                      if(validateDecimal(document.getElementById("PSAtxtid").value))
                      {
                        document.getElementById("PSAtxtspan").innerHTML= "";
                      }
                      else
                      {
                            vbool= false;
                            document.getElementById("PSAtxtspan").innerHTML = "only 3 digits after decimal are allowed";
                      }
                  }
                  else
                  {
                       vbool= false;
                       document.getElementById("PSAtxtspan").innerHTML = "special characters are not allowed";
                  }
               }
               else
               {
                    vbool= false;
                    document.getElementById("PSAtxtspan").innerHTML = "<%=bdl.getString("CMS.Inv.validate.field")%>";
               }
            }
            if(document.getElementById("txtRemarks").value!="")
            {
                if(document.getElementById("txtRemarks").value.length<1000)
                {
                    document.getElementById("txtRemarksspan").innerHTML = "";
                }else{
                    document.getElementById("txtRemarksspan").innerHTML = "Maximum 1000 characters are allowed.";
                }
            }
            <%--<%
            if(userTypeId==3)
            {
            %>
                for( i = 0; i < document.frm.radioReleasePG.length; i++ )
                {
                 if( document.frm.radioReleasePG[i].checked == false )
                 {
                    m++;
                 }
                }
                if(m==2)
                {
                    document.getElementById("radioReleasePGspan").innerHTML = "Please select any one option";
                    vbool= false;
                }
                else{document.getElementById("radioReleasePGspan").innerHTML = "";}
            <%}%>--%>

            if($.trim(CKEDITOR.instances.txtReason.getData()) == ""){
                document.getElementById("SPtxtNewsDetl").innerHTML = "Please enter description for Contract Termination";
                vbool= false;
            }
            return vbool;
        }
        $(document).ready(function() {
                $(document).ready(function() {
                CKEDITOR.instances.txtReason.on('blur', function()
                {
                    if(CKEDITOR.instances.txtReason.getData() != 0)
                    {
                            document.getElementById("SPtxtNewsDetl").innerHTML="";
                    }
                });
            });
            });
      </script>
    </body>
</html>

