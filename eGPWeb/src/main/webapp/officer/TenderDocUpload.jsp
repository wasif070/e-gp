<%-- 
    Document   : TenderDocUpload
    Created on : Nov 30, 2010, 5:50:07 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Upload Document </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:100}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please select Document.</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please enter Description.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                    }
                });
            });
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#button').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
        <%
            int tenderId = 0;
            int sectionId = 0;
            int pkgOrLotId = -1;
            String inCorri = "no";
            String folderName = "";
            String lotNo = "";
           
            if(session.getAttribute("userId")==null){
                response.sendRedirect("SessionTimedOut.jsp");
            }
            if (request.getParameter("tenderId") != null){
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }
            if(request.getParameter("sectionId") != null){
                sectionId = Integer.parseInt(request.getParameter("sectionId"));
            }
            System.out.println("request.getParameter(folderName)"+request.getParameter("folderName"));
            if(request.getParameter("folderName") != null){
                folderName = request.getParameter("folderName").replace("^", "&");
            }
            if(request.getParameter("porlId")!=null){
                pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
            }
            if(request.getParameter("inCorri")!=null){
                inCorri = request.getParameter("inCorri");
            }
            if(request.getParameter("lotNo")!=null){
                lotNo = request.getParameter("lotNo").replace("^", "&");
            }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                 <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <div class="contentArea_1">
            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">
                Upload Document
                <%if(pkgOrLotId == -1){%>
                    <span class="c-alignment-right"><a class="action-button-goback" href="TenderDocPrep.jsp?tenderId=<%=tenderId%>">Go back to Tender Document</a></span>
                <%}else{%>
                    <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%= tenderId %>&porlId=<%= pkgOrLotId %>" title="Tender/Proposal Document" class="action-button-goback">Go back to Tender Document</a></span>
                <%}%>
            </div>

<div class="t-align-right t_space"></div>
           <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/TenderSecUploadServlet" enctype="multipart/form-data" name="frmUploadDoc">
                <input type="hidden" name="tenderId" value="<%= tenderId%>" />
                <input type="hidden" name="sectionId" value="<%= sectionId%>" />
                <input type="hidden" name="porlId" value="<%= pkgOrLotId%>" />
                <input type="hidden" name="lotNo" value="<%=lotNo%>" />
                <input type="hidden" name="folderName" value="<%= folderName%>" />
                <%if("yes".equals(inCorri)){%>
                    <input type="hidden" name="inCorri" value="<%= inCorri %>" />
                <%}%>
            <%   pageContext.setAttribute("tenderId", tenderId); %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                 <%
                                if (request.getParameter("fq") != null) {
                    %>
                    
                    <div class="responseMsg errorMsg t_space"><%=request.getParameter("fq")%></div>
                    <%
                                }
                                if (request.getParameter("fs") != null) {
                    %>
                     
                    <div class="responseMsg errorMsg t_space">

                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                                }if(request.getParameter("up")!=null){
                    %>
                    <div> &nbsp;</div>
                    <div id="successMsg" class="responseMsg successMsg">
                        File uploaded successfully.
                    </div>
                    <%}if(request.getParameter("rm")!=null){%>
                    <div> &nbsp;</div>
                    <div id="successMsg" class="responseMsg successMsg">
                        File removed successfully.
                    </div>
                    <%}%>
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space">
                   <tr>

                       <td style="font-style: italic" colspan="2" class="" align="right">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                    </tr>
                     <tr>
                        <td width="15%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                        <td width="85%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                        </td>
                    </tr>

                    <tr>
                        <td class="ff">Description : <span>*</span></td>
                        <td>
                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                            <div id="dvDescpErMsg" class='reqF_1'></div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>

                            <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" /></label>
                        </td>
                    </tr>
                </table>
                    <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="100%"  class="t-align-left">Instructions</th>
                                </tr>
                                <tr>
                                     <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");%>
                                    <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                        <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                                </tr>
                            </table>


            </form>

              <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="30%">File Name</th>
                        <th class="t-align-center" width="41%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%
                     String sectionid = request.getParameter("sectionId");
                     //String tenderid = "0";
                     String tenderid = request.getParameter("tenderId");
                     
                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionid, "docInfo")) {

                                     docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3())/1024)%></td>
                         <td class="t-align-center">
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderid%>&sectionId=<%=sectionid%>&lotNo=<%=lotNo.replace("&", "^")%>&folderName=<%=folderName.replace("&", "^")%>&funName=download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             &nbsp;
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tenderid%>&sectionId=<%=sectionid%>&lotNo=<%=lotNo.replace("&", "^")%>&porlId=<%=pkgOrLotId%>&folderName=<%=folderName.replace("&", "^")%>&funName=remove" ><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                         </td>
                    </tr>
                         <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
            </table>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->


        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>