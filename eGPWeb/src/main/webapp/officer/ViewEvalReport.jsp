<%--
    Document   : ViewEvalReport
    Created on : Jun 21, 2011, 6:14:21 PM
    Author     : nishit
--%>

<%@page import="com.cptu.egp.eps.model.table.TblEvalRptForwardToAa"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvaluationMatrix"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalRptSentToAa"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE html>
  <%
    String s_tenderid = "0";
    String s_lotId = "0";
    String s_aaId = "0";
    String s_rptId = "0";
    String s_rId = "0";
    String evalType = "eval";
    String strOtherComments = "";
    String strStatus ="";
    String strLoginUserComments ="";
    String strSecretory = "";
    String s_aaFFId = "0";
    int iLoginUserID = Integer.parseInt(request.getSession().getAttribute("userId").toString());
    int iTECCPId = 0;
    String loi = "";
    

    List<SPCommonSearchDataMore> lstReview = null;
    TblEvalRptSentToAa tblEvalRptSentToAa = new TblEvalRptSentToAa();
    TblEvalRptForwardToAa tblEvalRptForwardToAa = new TblEvalRptForwardToAa();
    EvaluationService evalService = (EvaluationService)AppContext.getSpringBean("EvaluationService");
    CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");

    if (request.getParameter("loi")!=null) {
        loi = request.getParameter("loi");
    }
    
    if(request.getParameter("tenderid")!=null){
     s_tenderid = request.getParameter("tenderid");
    }
    if(request.getParameter("rId")!=null){
     s_rId = request.getParameter("rId");
    }
    if(request.getParameter("lotId")!=null){
     s_lotId = request.getParameter("lotId");
    }
    if(request.getParameter("rpt")!=null){
     s_rptId = request.getParameter("rpt");
    }
    if(request.getParameter("evalRptToAaid")!=null){
     s_aaId = request.getParameter("evalRptToAaid");
    }
    if(request.getParameter("stat")!=null){
        evalType = request.getParameter("stat");
    }
    if(request.getParameter("isSU")!=null){
     strSecretory = request.getParameter("isSU");
    }
    if(request.getParameter("s_aaFFId")!=null){
     s_aaFFId = request.getParameter("s_aaFFId");
    }
    /**
    * For Introducing Reviewer (Secretary) in Evaluation Approve
    */
    lstReview = dataMore.getCommonSearchData("isTERSentForReview", s_tenderid, s_lotId, s_rId);
    if(lstReview!=null && (!lstReview.isEmpty()) && (!lstReview.get(0).getFieldName1().equals("0"))){
        tblEvalRptForwardToAa = evalService.getTblEvalRptForwardToAa(Integer.parseInt(s_tenderid),Integer.parseInt(s_lotId),Integer.parseInt(s_rId));
        strStatus = tblEvalRptForwardToAa.getRptStatus();
        if(tblEvalRptForwardToAa.getSentByUserId() == iLoginUserID){
            strLoginUserComments = tblEvalRptForwardToAa.getSentUserRemarks();
            strOtherComments = tblEvalRptForwardToAa.getActionUserRemarks();
        }else{
            strOtherComments = tblEvalRptForwardToAa.getSentUserRemarks();
            strLoginUserComments = tblEvalRptForwardToAa.getActionUserRemarks();
        }
     }

   // System.out.println("strLoginUserComments "+strLoginUserComments+ " strOtherComments "+strOtherComments+ " strStatus "+strStatus);
     List<SPCommonSearchDataMore> lstCP = dataMore.geteGPData("getTECMemberRole", s_tenderid,""+iLoginUserID,"cp");
     if(lstCP!=null && (!lstCP.isEmpty()) && (!lstCP.get(0).getFieldName1().equals("0"))){
        iTECCPId = Integer.parseInt(lstCP.get(0).getFieldName1());
     }
    /*
        Reviewer Code Complied
    */
//    System.out.println("*******iTECCPId "+iTECCPId+" iLoginUserID "+iLoginUserID+" lstReview "+lstReview);
    if(lstReview == null || lstReview.isEmpty() || (iTECCPId != 1)){ //IN case of there is no reviewer or login user is minister then below block should be execute
//        System.out.println("************** here 1");
        if("0".equalsIgnoreCase(s_aaId)){
//            System.out.println("************** here 2 s_tenderid" +s_tenderid+" s_lotId "+s_lotId+" s_rptId "+s_rptId+" s_rId "+s_rId);
            tblEvalRptSentToAa = evalService.getTblEvalRptSentToAa(Integer.parseInt(s_tenderid),Integer.parseInt(s_lotId),Integer.parseInt(s_rptId),Integer.parseInt(s_rId));
            strStatus = tblEvalRptSentToAa.getRptStatus();
            if(tblEvalRptSentToAa.getSentBy() == iLoginUserID){
                strOtherComments = tblEvalRptSentToAa.getRemarks();
                strLoginUserComments = tblEvalRptSentToAa.getAaRemarks();
             } else {
                strLoginUserComments  = tblEvalRptSentToAa.getRemarks();
                strOtherComments= tblEvalRptSentToAa.getAaRemarks();
             }
         }else{
//            System.out.println("************** here 3");
            tblEvalRptSentToAa = evalService.getTblEvalRptSentToAa(Integer.parseInt(s_aaId),Integer.parseInt(s_rId));
            strStatus = tblEvalRptSentToAa.getRptStatus();
            strOtherComments = tblEvalRptSentToAa.getRemarks();
            strLoginUserComments = tblEvalRptSentToAa.getAaRemarks();
         }
    }
    // Coad added by Dipal for Audit Trail Log.
    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
    String idType="tenderId";
    int auditId=Integer.parseInt(request.getParameter("tenderid"));
    String auditAction="View Evaluation Report ";
    String moduleName=EgpModule.Evaluation.getName();
    String remarks="";
    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Evaluation Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
    </head>
    <body>
        <div class="dashboard_div">
        <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="DashboardContainer">
            <div class="contentArea_1">
                <div class="pageHead_1">View Evaluation Report
                    <span class="c-alignment-right">
                    <%if("view".equalsIgnoreCase(request.getParameter("lnk"))){%>
                    <a href="ViewEvalReportListing.jsp?tenderid=<%=s_tenderid%>&st=rp&comType=TEC&rId=<%=s_rId%>&isSU=<%=strSecretory%>&evalRptFFid=<%=s_aaFFId%>" title="Go Back" class="action-button-goback">Go Back</a>
                    <%}else if("loi".equalsIgnoreCase(request.getParameter("loi"))){%>
                        <a href="LOI.jsp?tenderid=<%=s_tenderid%>" title="Go Back" class="action-button-goback">Go Back</a>

                    <% }else{%>
                    <%if("0".equalsIgnoreCase(s_aaId)){%>
                    <a href="Evalclarify.jsp?tenderId=<%=s_tenderid%>&st=rp&comType=TEC" title="Go Back" class="action-button-goback">Go Back</a>
                    <%}else{%>
                    <a href="EvalReportListing.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>" title="Go Back" class="action-button-goback">Go Back</a>
                    <%}}%>
                    </span>
                </div>
    <%
            pageContext.setAttribute("tenderId", s_tenderid);
    %>
                <div class="t_space">
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                </div>
              <table width="100%" cellspacing="10" class="tableList_3 t_space">                
                      <%  if ("Services".equalsIgnoreCase(tenderProcureNature)) {%>
                        <td class="ff" width="12%">Proposal Opening Reports</td>
                        <%} else {%>
                        <td class="ff" width="12%">Tender Opening Reports</td>
                        <%}%>
                        <td width="88%"><a href="OpeningReports.jsp?tenderId=<%=s_tenderid%>&lotId=0&comType=null" target="_blank" >View</a></td>                  
                    <jsp:include page="TERInclude.jsp">
                        <jsp:param name="tenderId" value="<%=s_tenderid%>"/>
                        <jsp:param name="lotId" value="<%=s_lotId%>"/>
                        <jsp:param name="roundId" value="<%=s_rId%>"/>
                        <jsp:param name="evalType" value="<%=evalType%>"/>
                    </jsp:include>
                    <!-- Dohatec Start !-->
                <!--    <tr>
                      <td class="ff">Comments from <% ///if(lstReview != null && !lstReview.isEmpty() && !lstReview.get(0).getFieldName1().equals("0")){ %>Secretary<%//} else { if(iTECCPId == iLoginUserID) {%>AA <%//} else {%>TEC Chairperson<%//}}%> :</td>
                      <td valign="top"><%//=strOtherComments%></td>
                    </tr>
                    <tr>
                      <td class="ff">Status :</td>
                      <td><%//=strStatus%></td>
                    </tr>
                    <tr>
                      <td class="ff">Comments :</td>
                      <td><%//=strLoginUserComments%></td>
                    </tr> !-->
                <!-- Dohatec End !-->
                  </table>
          </div>
      </div>
      <!--Dashboard Content Part End-->

      <!--Dashboard Footer Start-->
    <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
      <!--Dashboard Footer End-->
    </div>
    </body>
    <script type="text/javascript">
                var headSel_Obj = document.getElementById("headTabEval");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }

    </script>
</html>
<%
if(tblEvalRptSentToAa!=null){
    tblEvalRptSentToAa = null;
}
%>
