<%-- 
    Document   : CommApp
    Created on : Nov 22, 2010, 3:16:31 PM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.SHA1HashEncryption" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page buffer="10kb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Members Consent/Approval for Opening</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript" language="javascript">
            $(document).ready(function(){                
                $('#CommApp').validate({                    
                    rules:{
                        txtpassword: {required: true},
                        txtcomments: {required: true, maxlength: 500}
                    },
                    messages: {
                        txtpassword: {required:"<div class='reqF_1'>Please enter Password</div>"},
                        txtcomments: {
                            required: "<div class='reqF_1'>Please enter Comments</div>",
                            maxlength: "<div class='reqF_1'>Max 500 Characters are allowed</div>"}
                    }
                });
            });
        </script>

    </head>
    <body>
        <!--Dashboard Header Start-->
        <div class="topHeader">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
        </div>
        <!--Dashboard Header End-->

        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <form id="CommApp" action="CommApp.jsp?tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=request.getParameter("lotId")%>&id=<%=request.getParameter("id")%>&uid=<%=request.getParameter("uid")%>" method="post">
                <%
                            String referer = "OpenComm.jsp?tenderid=" + request.getParameter("tenderId") + "&lotId=" + request.getParameter("lotId");
                %>
                <div class="pageHead_1">Members Consent/Approval for Opening
                    <span style="float: right;" >
                        <a href="<%=referer%>" class="action-button-goback">Go Back</a>
                    </span>
                </div>

                <%
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>


                <%
                            String i = "",
                                   userId = "";
                            //Fetch tenderid from querysting and get corrigendumid

                            HttpSession hs = request.getSession();

                            if (hs.getAttribute("userId") != null) {
                                userId = hs.getAttribute("userId").toString();
                            }

                            if (!request.getParameter("id").equals("")) {
                                i = request.getParameter("id");
                            }


                            if (request.getParameter("uid") != null) {
                                if (!userId.equalsIgnoreCase(request.getParameter("uid"))) {
                                    response.sendRedirect("../SessionTimedOut.jsp");
                                }
                            }
                            
                             // Coad added by Dipal for Audit Trail Log.
                            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            String idType="tenderId";
                            int auditId=Integer.parseInt(request.getParameter("tenderId"));
                            String auditAction="Declaration by Opening Committee Member";
                            String moduleName=EgpModule.Tender_Opening.getName();
                            String remarks="";

                            //This condition is used to submit data in database, this is button click event.
                            if (request.getParameter("btnsubmit") != null) {
                                /*
                                Get password and check in database for existence, if found perform insert operation
                                else show message of invalid password.
                                */
                                
                                //System.out.print(SHA1HashEncryption.encodeStringSHA1(request.getParameter("txtpassword")));
                                //tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                
                                List<SPTenderCommonData> list =
                                        tenderCommonService.returndata("checkLogin", request.getParameter("hiddenemail"), SHA1HashEncryption.encodeStringSHA1(request.getParameter("txtpassword")));

                                if (!list.isEmpty()) {
                                    if (!request.getParameter("id").equals("") && !request.getParameter("uid").equals("")) {
                                        String table = "", 
                                               updateString = "",
                                               whereCondition = "";

                                        table = "tbl_Committeemembers";
                                        updateString = "appStatus='Approved', appDate=getdate(), remarks='" + request.getParameter("txtcomments") + "'";
                                        whereCondition = "committeeId=" + request.getParameter("id") + " And userID=" + request.getParameter("uid");
                                        
                                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                        CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_Committeemembers", updateString, whereCondition).get(0);
                                        
                                        table = null;
                                        updateString = null;
                                        whereCondition = null;
                                        list=null;

                                        if (commonMsgChk.getFlag().equals(true)) 
                                        {
                                            // Code for audit trail
                                            remarks=request.getParameter("txtcomments");
                                            auditAction="Declaration by Opening Committee Member";
                                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

                                            response.sendRedirect("OpenComm.jsp?tenderid=" + request.getParameter("tenderId") + "&lotId=" + request.getParameter("lotId") + "&msgId=approved");
                                        } 
                                        else 
                                        {
                                            // Code for audit trail
                                            remarks=request.getParameter("txtcomments");
                                            auditAction="Error in Declaration by Opening Committee Member";
                                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                                    
                                            response.sendRedirect("CommApp.jsp?tenderId=" + request.getParameter("tenderId") + "&lotId=" + request.getParameter("lotId") + "&id=" + request.getParameter("id") + "&uid=" + request.getParameter("uid") + "&msgId=error");
                                        }
                                    }

                                }
                                else 
                                {
                                    // Code for audit trail
                                    remarks=request.getParameter("txtcomments");
                                    auditAction="Error in Declaration by Opening Committee Member";
                                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                                    
                                    //out.println("This is wrong password");
                                    list=null;
                                    response.sendRedirect("CommApp.jsp?tenderId=" + request.getParameter("tenderId") + "&lotId=" + request.getParameter("lotId") + "&id=" + request.getParameter("id") + "&uid=" + request.getParameter("uid") + "&msgId=invalid");
                                }
                            }

                %>

                <%-- Start: CODE TO DISPLAY MESSAGES --%>
                <%if (request.getParameter("msgId") != null) {
                                String msgId = "", msgTxt = "";
                                boolean isError = false;
                                msgId = request.getParameter("msgId");
                                if (!msgId.equalsIgnoreCase("")) {
                                    if (msgId.equalsIgnoreCase("invalid")) {
                                        isError = true;
                                        msgTxt = "Invalid password.";
                                    } else if (msgId.equalsIgnoreCase("error")) {
                                        isError = true;
                                        msgTxt = "There was some error.";
                                    } else {
                                        msgTxt = "";
                                    }
                %>
                <%if (isError && msgTxt != "") {%>
                <div>&nbsp;</div>
                <div class="responseMsg errorMsg"><%=msgTxt%></div>
                <%} else if (msgTxt != "") {%>
                <div>&nbsp;</div>
                <div class="responseMsg successMsg"><%=msgTxt%></div>
                <%}%>
                <%}
                  msgId = null;
                  msgTxt = null;
                }%>

                <%-- End: CODE TO DISPLAY MESSAGES --%>
                <div style="font-style: italic" class="formStyle_1 t_space ff">
                    Fields marked with (<span class="mandatory">*</span>) are mandatory.
                </div>
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <td width="10%" class="ff">e-mail ID :  </td>
                        <%                                    
                                    List<SPTenderCommonData> list = 
                                            tenderCommonService.returndata("getCommitteeInfo", i, request.getParameter("uid"));
                                    
                                    if (!list.isEmpty()) {
                        %>
                        <td width="90%"><%=list.get(0).getFieldName1()%>
                            <input type="hidden" name="hiddenid" id="hiddenid" value="<%=list.get(0).getFieldName4()%>"/>
                            <input type="hidden" name="hiddenemail" id="hiddenemail" value="<%=list.get(0).getFieldName1()%>"/>
                        </td>

                        <%}%>
                    </tr>
                    <tr>
                        <td class="ff" valign="top">Password : <span class="mandatory">*</span> </td>
                        <td>
                            <input name="txtpassword" type="password" class="formTxtBox_1" id="txtpassword" style="width:200px;" autocomplete="off" />
                        </td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top">Comments : <span class="mandatory">*</span></td>
                        <td>
                            <textarea name="txtcomments" id="txtcomments" class="formTxtBox_1" cols="35" rows="5"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top">&nbsp;</td>
                        <td>
                            <div >
                                <label class="formBtn_1">
                                    <input name="btnsubmit" type="submit" value="Submit" />
                                </label>
                            </div>
                        </td>
                    </tr>
                </table>

                    

                <%
                // Set To Null
                            referer = null;
                            i = null;
                            userId = null;
                %>

            </form>

        </div>
        <div>&nbsp;</div>
        <!--Dashboard Content Part End-->

        <!--Dashboard Footer Start-->
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <!--Dashboard Footer End-->


    </div>

</body>
<script type="text/javascript" language="Javascript">
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }

</script>
</html>
