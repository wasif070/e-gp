<%-- 
    Document   : OpeningReports
    Created on : Dec 18, 2010, 6:20:42 PM
    Author     : Rikin
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Opening Reports</title>
        <%String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />        
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>       
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%    
                    String userId = "";
                    boolean isPackage = false;
                    boolean isLotId = false;
                    String strLotId = "";
                    boolean isLot = false;

                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    
                    
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = session.getAttribute("userId").toString();
                        commonService.setUserId(userId);
                        tenderCommonService1.setLogUserId(userId);
                        commonSearchDataMoreService.setLogUserId(userId);
                    }

                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        tenderId = request.getParameter("tenderId");
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }

                    if (request.getParameter("lotId") != null) {
                       if (!"0".equalsIgnoreCase(request.getParameter("lotId")) && !"".equalsIgnoreCase(request.getParameter("lotId")) && !"null".equalsIgnoreCase(request.getParameter("lotId"))){
                            isLotId = true;
                            strLotId=request.getParameter("lotId");
                       } else {
                            isPackage = true;
                       }
                    }

                    String procureNature = "";
                    for (CommonTenderDetails commonTenderDetails : tenderSrBean.getAPPDetails(Integer.parseInt(tenderId), "tender")) {
                        procureNature = commonTenderDetails.getProcurementNature();
                    }
                    
                    List<Object[]> listPackage =  new ArrayList<Object[]>();
                    List<Object[]> lotlist =  new ArrayList<Object[]>();
                    
                    boolean is2Env = false;
                    List<SPCommonSearchDataMore> envDataMores = commonSearchDataMoreService.geteGPData("GetTenderEnvCount", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    if(envDataMores!=null && (!envDataMores.isEmpty()) && envDataMores.get(0).getFieldName1().equals("2")){
                        is2Env = true;
                    }
                %>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <div class="contentArea_1">
                    <div class="pageHead_1">
                        Opening  Report
                        <!--<span style="float:right;">
                            <a class="action-button-goback" href="OpenComm.jsp?tenderid=<%=tenderId%>">Go back to Dashboard</a>
                        </span>-->
                    </div>
                    <div  id="print_area">
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div class="t_space">&nbsp;</div>
                        <div class="tabPanelArea_1">
                        <% 
                            listPackage = commonService.getPkgDetialByTenderId(Integer.parseInt(tenderId));
                            
                            if(isPackage){
                        %>
                            <% if("Services".equalsIgnoreCase(procureNature)){ %>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <%
                                        for(Object[] pkg:listPackage){ 
                                    %>
                                        <tr>
                                            <td width="22%" class="t-align-left ff">Package No. :</td>
                                            <td width="78%" class="t-align-left"><%=pkg[0].toString()%></td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left ff">Package Description :</td>
                                            <td class="t-align-left"><%=pkg[1].toString()%></td>
                                        </tr>
                                    <%
                                        }
                                    %>
                                </table>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <th width="70%">Form Name</th>
                                        <th width="30%">Action</th>
                                    </tr>
                                    <%
                                       List<SPCommonSearchDataMore> techForms = new ArrayList<SPCommonSearchDataMore>(); 
                                       if(!is2Env){
                                            techForms =  commonSearchDataMoreService.geteGPData("GetTORReportForms", ""+tenderId, "0", "All", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                       }else{
                                            techForms =  commonSearchDataMoreService.geteGPData("GetTORReportForms", ""+tenderId, "0", "No", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                       }
                                       
                                       for(SPCommonSearchDataMore techForm:techForms){
                                    %>
                                    <tr>
                                        <td class="ff t-align-left"><%=techForm.getFieldName1()%></td>
                                        <td class="t-align-center">
                                          <%
                                                if(techForm.getFieldName3().equalsIgnoreCase("c") ) {
                                                    out.print("Canceled Form");
                                           %>                                                    
                                            <%}else{%>
                                                <a href="IndReport.jsp?tenderId=<%=tenderId%>&formId=<%=techForm.getFieldName2()%>&frm=eval">Individual Report</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                            <%
                                                boolean isMultipleFillingYes = false;
                                                List<SPTenderCommonData> mulitiList =  tenderCommonService.returndata("isCompReportMultiFilling", techForm.getFieldName2(), null);
                                                    if(mulitiList!=null && (!mulitiList.isEmpty())){
                                                        isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                                                }
                                            %>
                                            <a href="<%=isMultipleFillingYes ? "SrvComReport.jsp" : "ComReport.jsp"%>?tenderId=<%=tenderId%>&formId=<%=techForm.getFieldName2()%>&frm=eval">Comparative Report</a>
                                           <% }%>
                                        </td>
                                    </tr>
                                    <% } %>
                                </table>
                                
                            <% }else{
                                lotlist = commonService.getLotDetailsByTenderIdforEval(tenderId);
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1">
                            <%
                                for(Object[] pkg:listPackage){ 
                            %>
                                    <tr>
                                        <td width="22%" class="t-align-left ff">Package No. :</td>
                                        <td width="78%" class="t-align-left"><%=pkg[0].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Package Description :</td>
                                        <td class="t-align-left"><%=pkg[1].toString()%></td>
                                    </tr>
                            <%
                                }
                            %>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="70%">Form Name</th>
                                    <th width="30%">Action</th>
                                </tr>
                                <%
                                   List<SPCommonSearchDataMore> techForms =  commonSearchDataMoreService.geteGPData("GetTORReportForms", ""+tenderId, "0", "No", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                   for(SPCommonSearchDataMore techForm:techForms){
                                %>
                                <tr>
                                    <td class="ff t-align-left"><%=techForm.getFieldName1()%></td>
                                    <td class="t-align-center">
                                      <%
                                            if( techForm.getFieldName3().equalsIgnoreCase("c")) {
                                                out.print("Canceled Form");
                                            }else{%>
                                                <a href="IndReport.jsp?tenderId=<%=tenderId%>&formId=<%=techForm.getFieldName2()%>&frm=eval">Individual Report</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                            <%
                                                boolean isMultipleFillingYes = false;
                                                List<SPTenderCommonData> mulitiList =  tenderCommonService.returndata("isCompReportMultiFilling", techForm.getFieldName2(), null);
                                                    if(mulitiList!=null && (!mulitiList.isEmpty())){
                                                        isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                                                }
                                            %>
                                            <a href="<%=isMultipleFillingYes ? "SrvComReport.jsp" : "ComReport.jsp"%>?tenderId=<%=tenderId%>&formId=<%=techForm.getFieldName2()%>&frm=eval">Comparative Report</a>
                                        <% }%>
                                    </td>
                                </tr>
                                <% } %>
                            </table>
                             <% if(!is2Env){ %>
                             
                             <%
                                for(Object[] lot:lotlist){
                             %>
                             <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <td width="22%" class="t-align-left ff">Lot No. :</td>
                                        <td width="78%" class="t-align-left"><%=lot[0].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Lot Description :</td>
                                        <td class="t-align-left"><%=lot[1].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">BOR</td>
                                        <td class="t-align-left"><a href="<%=request.getContextPath()%>/officer/TORCommon.jsp?tenderid=<%=tenderId%>&lotId=<%=lot[2].toString()%>&frm=eval">View</a></td>
                                    </tr>
                                    
                             </table>
                             <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="70%">Form Name</th>
                                    <th width="30%">Action</th>
                                </tr>
                                <%
                                   List<SPCommonSearchDataMore> priceBidForms =  commonSearchDataMoreService.geteGPData("GetTORReportForms", ""+tenderId, lot[2].toString(), "Yes", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                   if(priceBidForms.isEmpty()){
                                %>
                                <tr>                                    
                                    <td class="t-align-center" colspan="2" style="color: red;">
                                        No Records Available
                                    </td>
                                </tr>
                                <%
                                   }else{
                                   for(SPCommonSearchDataMore priceBidForm:priceBidForms){
                                %>
                                <tr>
                                    <td class="ff t-align-left"><%=priceBidForm.getFieldName1()%></td>
                                    <td class="t-align-center">
                                        <%
                                            if( priceBidForm.getFieldName3().equalsIgnoreCase("c") ) {
                                                out.print("Canceled Form");
                                            }else{%>
                                                <a href="IndReport.jsp?tenderId=<%=tenderId%>&formId=<%=priceBidForm.getFieldName2()%>&frm=eval">Individual Report</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                <%
                                                boolean isMultipleFillingYes = false;
                                                List<SPTenderCommonData> mulitiList =  tenderCommonService.returndata("isCompReportMultiFilling", priceBidForm.getFieldName2(), null);
                                                if(mulitiList!=null && (!mulitiList.isEmpty())){
                                                    isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                                                }
                                                %>
                                                <a href="<%=isMultipleFillingYes ? "SrvComReport.jsp" : "ComReport.jsp"%>?tenderId=<%=tenderId%>&formId=<%=priceBidForm.getFieldName2()%>&frm=eval">Comparative Report</a>
                                        <%}%>
                                    </td>
                                </tr>
                                <% } } %>
                                <% List<SPTenderCommonData> grandsumlink = tenderCommonService1.returndata("getGrandSummaryLink", lot[2].toString(), null);
                                   if(!"0".equals(grandsumlink.get(0).getFieldName1())){
                                %>
                                <tr>
                                    <td class="ff t-align-left">Grand Summary</td>
                                    <td class="t-align-center">
                                        <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=lot[2].toString()%>&report=Individual">Individual Report</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=lot[2].toString()%>&report=Comparative">Comparative Report</a>
                                        
                                    </td>
                                </tr>
                              <%  } %>
                             </table>
                             <%
                                 }
                               }
                             %>
                            <% }
                           }
                           else
                           {
                        %>
                            <% if("Services".equalsIgnoreCase(procureNature)){ %>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tbody>
                                <%
                                    for(Object[] pkg:listPackage){ 
                                %>
                                    <tr>
                                        <td width="22%" class="t-align-left ff">Package No. :</td>
                                        <td width="78%" class="t-align-left"><%=pkg[0].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Package Description :</td>
                                        <td class="t-align-left"><%=pkg[1].toString()%></td>
                                    </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="70%">Form Name</th>
                                    <th width="30%">Action</th>
                                </tr>
                                <%
                                   List<SPCommonSearchDataMore> totalForms =  new ArrayList<SPCommonSearchDataMore>();
                                   if(!is2Env){
                                       totalForms =  commonSearchDataMoreService.geteGPData("GetTORReportForms", ""+tenderId, strLotId, "All", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                   }else{
                                       totalForms =  commonSearchDataMoreService.geteGPData("GetTORReportForms", ""+tenderId, strLotId, "No", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                   }
                                   if(totalForms.isEmpty()){
                                %>
                                <tr>                                    
                                    <td class="t-align-center" colspan="2" style="color: red;">
                                        No Records Available
                                    </td>
                                </tr>
                                <%
                                   }else{
                                   for(SPCommonSearchDataMore totalForm:totalForms){
                                %>
                                <tr>
                                    <td class="ff t-align-left"><%=totalForm.getFieldName1()%></td>
                                    <td class="t-align-center">
                                        <%
                                            if( totalForm.getFieldName3().equalsIgnoreCase("c")) {
                                                out.print("Canceled Form");
                                         }else{%>
                                            <a href="IndReport.jsp?tenderId=<%=tenderId%>&formId=<%=totalForm.getFieldName2()%>&frm=eval">Individual Report</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                            <%
                                                boolean isMultipleFillingYes = false;
                                                List<SPTenderCommonData> mulitiList =  tenderCommonService.returndata("isCompReportMultiFilling", totalForm.getFieldName2(), null);
                                                    if(mulitiList!=null && (!mulitiList.isEmpty())){
                                                        isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                                                }
                                            %>
                                            <a href="<%=isMultipleFillingYes ? "SrvComReport.jsp" : "ComReport.jsp"%>?tenderId=<%=tenderId%>&formId=<%=totalForm.getFieldName2()%>&frm=eval">Comparative Report</a>
                                            <%}%>
                                    </td>
                                </tr>
                                <% }  %>
                                <% List<SPTenderCommonData> grandsumlink = tenderCommonService1.returndata("getGrandSummaryLink", strLotId, null);
                                   if(!"0".equals(grandsumlink.get(0).getFieldName1())){
                                %>
                                <tr>
                                    <td class="ff t-align-left">Grand Summary</td>
                                    <td class="t-align-center">
                                        <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=strLotId%>&report=Individual">Individual Report</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=strLotId%>&report=Comparative">Comparative Report</a>
                                        
                                    </td>
                                </tr>
                              <%  } } %>
                             </table>
                            <% }else{ %>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tbody>
                                <%
                                    for(Object[] pkg:listPackage){ 
                                %>
                                    <tr>
                                        <td width="22%" class="t-align-left ff">Package No. :</td>
                                        <td width="78%" class="t-align-left"><%=pkg[0].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Package Description :</td>
                                        <td class="t-align-left"><%=pkg[1].toString()%></td>
                                    </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1">    
                            <%
                                lotlist = commonService.getLotDetailsByPkgLotId(strLotId,tenderId);
                                for(Object[] lot:lotlist){
                             %>
                                    <tr>
                                        <td width="22%" class="t-align-left ff">Lot No. :</td>
                                        <td width="78%" class="t-align-left"><%=lot[0].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Lot Description :</td>
                                        <td class="t-align-left"><%=lot[1].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">TOR1</td>
                                        <td class="t-align-left"><a href="<%=request.getContextPath()%>/officer/TOR1.jsp?tenderid=<%=tenderId%>&lotId=<%=strLotId%>&frm=eval">View</a></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">TOR2</td>
                                        <td class="t-align-left"><a href="<%=request.getContextPath()%>/officer/TOR2.jsp?tenderid=<%=tenderId%>&lotId=<%=strLotId%>&frm=eval">View</a></td>
                                    </tr>
                                </tbody>
                             </table>
                             <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="70%">Form Name</th>
                                    <th width="30%">Action</th>
                                </tr>
                                <%
                                   List<SPCommonSearchDataMore> totalForms = new  ArrayList<SPCommonSearchDataMore>();
                                   if(!is2Env){
                                       totalForms =  commonSearchDataMoreService.geteGPData("GetTORReportForms", ""+tenderId, strLotId, "All", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                   }else{
                                       totalForms =  commonSearchDataMoreService.geteGPData("GetTORReportForms", ""+tenderId, strLotId, "No", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    }
                                   
                                   if(totalForms.isEmpty()){
                                %>
                                <tr>                                    
                                    <td class="t-align-center" colspan="2" style="color: red;">
                                        No Records Available
                                    </td>
                                </tr>
                                <%
                                   }else{
                                   for(SPCommonSearchDataMore totalForm:totalForms){
                                %>
                                <tr>
                                    <td class="ff t-align-left"><%=totalForm.getFieldName1()%></td>
                                    <td class="t-align-center">
                                        <%
                                            if( totalForm.getFieldName3().equalsIgnoreCase("c"))  {
                                                out.print("Canceled Form");
                                       }else{%>
                                            <a href="IndReport.jsp?tenderId=<%=tenderId%>&formId=<%=totalForm.getFieldName2()%>&frm=eval">Individual Report</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                            <%
                                                boolean isMultipleFillingYes = false;
                                                List<SPTenderCommonData> mulitiList =  tenderCommonService.returndata("isCompReportMultiFilling", totalForm.getFieldName2(), null);
                                                    if(mulitiList!=null && (!mulitiList.isEmpty())){
                                                        isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                                                }
                                            %>
                                            <a href="<%=isMultipleFillingYes ? "SrvComReport.jsp" : "ComReport.jsp"%>?tenderId=<%=tenderId%>&formId=<%=totalForm.getFieldName2()%>&frm=eval">Comparative Report</a>
                                            <%}%>
                                    </td>
                                </tr>
                                <% }  %>
                                <% List<SPTenderCommonData> grandsumlink = tenderCommonService1.returndata("getGrandSummaryLink", strLotId, null);
                                   if(!"0".equals(grandsumlink.get(0).getFieldName1())){
                                %>
                                <tr>
                                    <td class="ff t-align-left">Grand Summary</td>
                                    <td class="t-align-center">
                                        <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=strLotId%>&report=Individual">Individual Report</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="<%=request.getContextPath()%>/officer/GrandSumm.jsp?tenderId=<%=tenderId%>&lotId=<%=strLotId%>&report=Comparative">Comparative Report</a>
                                        
                                    </td>
                                </tr>
                              <%  } } %>
                             </table>
                             <% } %>
                            <% }
                          }
                        %>
                        </div>
                    </div>         
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
