<%-- 
    Document   : SSVariOrder
    Created on : Dec 22, 2011, 12:04:57 PM
    Author     : shreyansh Jogi
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTcvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSsvari"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvStaffSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTeamComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <% ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Variation Order</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <%
                    String referpage = request.getHeader("referer");
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String type = "";
                    int tenderId = 0;
                    int lotId = 0;
                    int formMapId = 0;
                    int srvBoqId = 0;
                    int varId = 0;
                    String styleClass = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    if (request.getParameter("varId") != null) {
                        varId = Integer.parseInt(request.getParameter("varId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                    }
                    if (request.getParameter("formMapId") != null) {
                        formMapId = Integer.parseInt(request.getParameter("formMapId"));
                    }
                    if (request.getParameter("srvBoqId") != null) {
                        srvBoqId = Integer.parseInt(request.getParameter("srvBoqId"));
                    }
                    List<TblCmsSrvSsvari> tcsts = cmss.getDetailOfSSForVari(varId);
                    List<TblCmsSrvTcvari> tcvari = cmss.getDetailOfTCForVari(varId);
                    CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                    String serviceType = commService.getServiceTypeForTender(tenderId);
                    boolean serType = true;
                    if ("Time based".equalsIgnoreCase(serviceType.toString())) {
                        serType = true;
                    }
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));

        %>
        <script>
            var count = 0;
        </script>
        <%if (request.getParameter("isEdit") != null) {%>
        <script>

            count = <%=tcsts.size()%>
        </script>
        <%} else {%>
        <script>
            count = 0;
        </script>
        <%}%>
        <script>
            function GetCal(txtname,controlname,id)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d-%b-%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        if(document.getElementById("nod_"+id).value!=""){
                            f_date(id);
                        }
                        $('#divid_'+id).remove();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function positive(value)
            {
                return /^\d*$/.test(value);

            }
            function getDate(date)
            {
                var splitedDate = date.split("-");
                return Date.parse(splitedDate[1]+" "+splitedDate[0]+", "+splitedDate[2]);
            }
            function f_date(id){
                var firstDate = getDate(document.getElementById("sdate_"+id).value);
                var values = document.getElementById("nod_"+id).value;
                var enddate = new Date(firstDate).getTime()+(parseInt(values)*24*60*60*1000);
                var finalenddate="";
                var monthname = new Array("Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec");
                var enddate = new Date(enddate);
                finalenddate = enddate.getDate()+"-"+monthname[enddate.getMonth()]+"-"+enddate.getFullYear();
                if(values != "" && !isNaN(firstDate))
                {
                    document.getElementById("edate_"+id).value = finalenddate;
                    return finalenddate;
                }
                return finalenddate;
            }
            
            function checkDays(id){
                $('.err').remove();
                var values = document.getElementById('nod_'+id).value;
                if(document.getElementById('nod_'+id).value.indexOf(".")!=-1){
                    $("#nod_"+id).parent().append("<div class='err' style='color:red;'>Decimal values are not allowed</div>");
                    document.getElementById('nod_'+id).value="";
                }

                else if(!regForNumber(document.getElementById('nod_'+id).value)){
                    $("#nod_"+id).parent().append("<div class='err' style='color:red;'>Days must be positive numbers </div>");
                    document.getElementById('nod_'+id).value="";
                }else if(document.getElementById('nod_'+id).value.indexOf("-")!=-1){
                    $("#nod_"+id).parent().append("<div class='err' style='color:red;'>Days must be positive value </div>");
                    document.getElementById('nod_'+id).value="";
                }else if(!positive(document.getElementById('nod_'+id).value)){
                    $("#nod_"+id).parent().append("<div class='err' style='color:red;'>Decimal values are not allowed</div>");
                    document.getElementById('nod_'+id).value="";
                }else if(document.getElementById('nod_'+id).value.length > 5){
                    $("#nod_"+id).parent().append("<div class='err' style='color:red;'>Maximum 5 digits are allowed </div>");
                    document.getElementById('nod_'+id).value="";
                }
                else{
                    if(document.getElementById("sdate_"+id).value!=""){
                        document.getElementById("edate_"+id).value=f_date(id);
                    }
                          
                }
            }

            function addrow()
            {
                var newTxt = '<tr><td class="t-align-left">\n\
        <input class="formTxtBox_1" type="checkbox" name="chk'+count+' " id="chk'+count+'" /></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="sno_'+count+'" style=width:40px id="sno_'+count+'" /></td>'+
                    '<td class="t-align-left"><select class="formTxtBox_1" style=width:100px name="name_'+count+'" id="name_'+count+'" >'+
                    '<%if (!tcvari.isEmpty()) {
                                for (TblCmsSrvTcvari tcst : tcvari) {%><option value="<%=tcst.getSrvTcvariId()%>"><%=tcst.getEmpName()%></option><%}
                                } else {%><option value="0">Select</option><%}%></select></td>'+
                                            '<td class="t-align-left"><select class="formTxtBox_1" style=width:80px name="WF_'+count+'" id="WF_'+count+'"><option value="Home">Home</option><option value="Field">Field</option></select></td>'+
                                            '<td class="t-align-left"><input width=23% class="formTxtBox_1" type="text" readonly onclick =GetCal("sdate_'+count+'","sdate_'+count+'",'+count+') name="sdate_'+count+'" id="sdate_'+count+'" />&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_'+count+' src=../resources/images/Dashboard/calendarIcn.png alt=Select a Date border= style=vertical-align:middle onclick =GetCal("sdate_'+count+'","calc_'+count+'",'+count+') /></a></td>'+
                                            '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="nod_'+count+'" style=width:40px id="nod_'+ count+'" onchange=checkDays('+count+')  /></td>'+
                                            '<td class="t-align-left"><input width=23% class="formTxtBox_1" type="text"  readonly name="edate_'+count+ '" id="edate_'+ count+'" /></td>'+
                                            '</tr>';

                                        $("#resultTable tr:last").after(newTxt);
                                        count++;
                                    }
        </script>
        <%if (request.getParameter("isEdit") != null) {%>
        <script>
            function delRow(){
                var listcount = count;
                var trackid="";
                var counter = 0;
                for(var i=0;i<listcount;i++){
                    if(document.getElementById("chk"+i)!=null){
                        if(document.getElementById("chk"+i).checked){
                            if(document.getElementById("varDtlId"+i)!=null){
                                trackid=trackid+document.getElementById("varDtlId"+i).value+",";
                                document.getElementById("count").value = listcount-1;
                            }
                        }
                    }
                }
                $(":checkbox[checked='true']").each(function(){
                    if(document.getElementById("count")!= null){
                        var curRow = $(this).parent('td').parent('tr');
                        curRow.remove();
                        count--;
                        counter++;
                    }
                });
                if(counter==0){
                    jAlert("Please select at least one item to remove","Repeat Order", function(RetVal) {
                    });
                }else{
                    $.post("<%=request.getContextPath()%>/CMSSerCaseServlet", {action:'deleterowforvariationForSS', val: trackid,tenderId:<%=request.getParameter("tenderId")%> },  function(j){
                        jAlert("Selected item(s) deleted successfully","Variation Order", function(RetVal) {
                        });

                    });
                }


            }
        </script>
        <%} else {%>
        <script>

            function delRow(){
                var counter = 0;
                $(":checkbox[checked='true']").each(function(){

                    var curRow = $(this).parent('td').parent('tr');
                    curRow.remove();
                    count--;
                    counter++;

                });
                if(counter==0){
                    jAlert("Please select at least one item to remove","Repeat Order", function(RetVal) {
                    });
                }else{
                    calculateGrandTotal();
                }
            }
        </script>
        <%}%>
        <script>
            function onFire(){

                var flag = true;
                var allow = false;
                for(var i=0;i<count;i++){
                    allow = true;
                    if(document.getElementById("sno_"+i).value=="" || document.getElementById("name_"+i).value=="" || document.getElementById("WF_"+i).value=="" || document.getElementById("sdate_"+i).value=="" || document.getElementById("nod_"+i).value=="" || document.getElementById("edate_"+i).value==""){
                        jAlert("It is necessary to enter all data","Variation Order", function(RetVal) {
                        });
                        flag=false;
                    }
                }
                if(flag){
                    if(!allow){
                        jAlert("It is necessary to enter at least one Employee detail","Variation Order", function(RetVal) {
                        });
                        return false;
                    }else{
                        <%if(serType){%>
                        alert("Please verify the Payment Schedule to adjust the Milestone");
                        <%}%>
                        document.getElementById("count").value=count;
                        document.forms["frm"].submit();
                    }
                }else{
                    return false;
                }

            }
        </script>
    </head>
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">
                        <%
                                    if (request.getParameter("tenderId") != null) {
                                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                                    }
                        %>
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="<%=referpage%>" title="Go Back">Go Back</a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%if (request.getParameter("lotId") != null) {%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <%}%>
                    <body>
                        <form name="frm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post" >
                            <%if (request.getParameter("isEdit") != null) {%>
                            <input type="hidden" name="isEdit" value ="SSvari" />
                            <%}%>
                            <input type="hidden" name="action" value ="SSvari" />
                            <input type="hidden" name="count" id="count" value ="" />
                            <input type="hidden" name="srvFormMapId" value ="<%=formMapId%>" />
                            <input type="hidden" name="tenderId" value ="<%=request.getParameter("tenderId")%>" />
                            <input type="hidden" name="varId" value ="<%=request.getParameter("varId")%>" />
                            <%
                                if(request.getParameter("isEdit")!=null){
                            %>
                                <input type="hidden" name="isEdit" id="isEdit" value="true" />
                            <%
                                }
                            %>
                            <br />
                            <%if (tcvari.isEmpty()) {%>
                            <div  class='t_space responseMsg noticeMsg t-align-left'>It is mandatory to add Employee details in Team Composition and Task Assignments form first</div>
                            <%}%>
                            <table width="100%" cellspacing="0" class="t_space">
                                <tr><td colspan="5" style="text-align: right;">
                                        <a class="action-button-add" id="addRow" onclick="addrow()">Add Item</a>
                                        <a class="action-button-delete" id="delRow" onclick="delRow()">Remove Item</a>
                                    </td></tr>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                <tr>
                                    <th>Check</th>
                                    <th>Sl. No.</th>
                                    <th>Name of Employees </th>
                                    <th>Home / Field</th>
                                    <th>Start Date </th>
                                    <th>No. of Days</th>
                                    <th>End Date</th>
                                </tr>
                                <%
                                            List<TblCmsSrvStaffSch> list = cmss.getStaffScheduleData(formMapId);
                                            if (!list.isEmpty()) {
                                                for (int i = 0; i < list.size(); i++) {

                                %>
                                <tr>
                                    <td class="t-align-left">&nbsp;</td>
                                    <td class="t-align-left"><%=list.get(i).getSrno()%></td>
                                    <td class="t-align-left"><%=list.get(i).getEmpName()%></td>
                                    <td class="t-align-left"><%=list.get(i).getWorkFrom()%></td>
                                    <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getStartDt()).split(" ")[0]%></td>
                                    <td style="text-align :right;"><%=list.get(i).getNoOfDays()%></td>
                                    <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getEndDt()).split(" ")[0]%></td>

                                </tr>
                                <%}
                                            }%>
                                <%
                                            if (request.getParameter("isEdit") != null) {
                                                for (int i = 0; i < tcsts.size(); i++) {
                                %>
                                <tr><td class="t-align-left">
                                        <input type="hidden" name="varDtlId<%=i%>" id="varDtlId<%=i%>" value="<%=tcsts.get(i).getSrvSsvariId()%>" />
                                        <input class="formTxtBox_1" type="checkbox" id="chk<%=i%>" name="chk<%=i%>" /></td>
                                    <td class="t-align-left"><input class="formTxtBox_1" type="text" value="<%=tcsts.get(i).getSrno()%>" name="sno_<%=i%>" style=width:40px id="sno_<%=i%>" /></td>
                                    <td class="t-align-left"><select style=width:100px class="formTxtBox_1" name="name_<%=i%>" id="name_<%=i%>" >
                                            <%if (!tcvari.isEmpty()) {
                                                                                                    for (TblCmsSrvTcvari tcst : tcvari) {%>
                                            <option value="<%=tcst.getSrvTcvariId()%>"<%if (tcst.getSrvTcvariId() == tcsts.get(i).getSrvTcvariId()) {%> selected <%}%> ><%=tcst.getEmpName()%></option>
                                            <%}
                                            } else {%>
                                            <option value="0">Select</option><%}%>
                                        </select>

                                    </td>
                                    <td class="t-align-left"><select style=width:70px class="formTxtBox_1" name="WF_<%=i%>" id="WF_<%=i%>">
                                            <option value="Home" <%if ("Home".equalsIgnoreCase(tcsts.get(i).getWorkFrom())) {%> selected<%}%>>Home</option>
                                            <option value="Field" <%if ("Field".equalsIgnoreCase(tcsts.get(i).getWorkFrom())) {%> selected<%}%>>Field</option>
                                        </select></td>
                                    <td class="t-align-left"><input width=23% value="<%=DateUtils.gridDateToStr(tcsts.get(i).getStartDt()).split(" ")[0].trim()%>" class="formTxtBox_1" type="text" readonly onclick =GetCal("sdate_<%=i%>","sdate_<%=i%>",<%=i%>) name="sdate_<%=i%>" id="sdate_<%=i%>" />&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_<%=i%> src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border= style=vertical-align:middle onclick ="GetCal('sdate_<%=i%>','calc_<%=i%>',<%=i%>)" /></a></td>
                                    <td class="t-align-left"><input style=width:40px value="<%=tcsts.get(i).getNoOfDays()%>" class="formTxtBox_1" type="text" name="nod_<%=i%>" onchange="checkDays(<%=i%>);" id="nod_<%=i%>"  /></td>
                                    <td class="t-align-left"><input value="<%=DateUtils.gridDateToStr(tcsts.get(i).getEndDt()).split(" ")[0].trim()%>" width=23% class="formTxtBox_1" type="text"  readonly name="edate_<%=i%>" id="edate_<%=i%>" /></td>
                                </tr>
                                <%}
                                            }%>
                                <input type="hidden" id="count" name="count" value="<%=tcsts.size()%>" />
                            </table>
                            <table width="100%" cellspacing="0" class="t_space">
                                <tr><td colspan="5" style="text-align: right;">
                                        <a class="action-button-add" id="addRow" onclick="addrow()">Add Item</a>
                                        <a class="action-button-delete" id="delRow" onclick="delRow()">Remove Item</a>
                                    </td></tr>
                            </table>
                            <center>

                                <%if (!tcvari.isEmpty()) {%>
                                <label class="formBtn_1">
                                    <input type="button" name="Boqbutton" id="Boqbutton" value="Submit" onclick="onFire();" />
                                </label>
                                <%}%>


                            </center>
                        </form>

                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </body>
</html>
