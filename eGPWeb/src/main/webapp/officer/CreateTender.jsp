
<%--
    Document   : CreateTender.jsp
    Created on : Nov 4, 2010, 4:01:36 PM
    Author     : rishita, Rajesh Singh
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<jsp:useBean id="configDocumentFeesSrBean" class="com.cptu.egp.eps.web.servicebean.ConfigDocumentFeesSrBean"/>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ConfigDocumentFeesService" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create IFB/ PQ Tender / REOI / RFP / Advertisement</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
         <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
         <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->

        <%--End -- This is done by Rajesh--%>
        <script type="text/javascript">
            var holiArray = new Array();
        </script>
        
<!--        <script type="text/javascript">      
            $(document).ready( function ()
            {    
                $('#txttenderpublicationDate').change(function(){
                    ShowPreBidMeetingDates();
                });
            });
        </script>-->
        
        
    </head>
    <%--<jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servlet.tenderInfoServlet"/>--%>
    <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <% TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        List<SPTenderCommonData> holidayList = tenderCommonService.returndata("getHolidayDatesBD", null, null);
        String tId = "";
        if (!request.getParameter("id").equals("")) {
            tId = request.getParameter("id");
        }
        List<SPTenderCommonData> subDays = tenderCommonService.returndata("GetSubDays", tId, null);
        List<SPTenderCommonData> tenEvent = tenderCommonService.returndata("GetTenEvent", tId, null);
        out.print("<script type='text/javascript'>");
        for (SPTenderCommonData holidays : holidayList) {
            out.print("holiArray.push('" + holidays.getFieldName1() + "');");
        }
        out.print("</script>");
    %>
    <%     /* Dohatec Start - rokib*/
        ConfigDocumentFeesService configDocumentFeesService = (ConfigDocumentFeesService) AppContext.getSpringBean("ConfigDocumentFeesService");
        /* Dohatec End - rokib*/
    %>
    <%--<jsp:useBean id="tenderDtBean" class="com.cptu.egp.eps.web.databean.TenderDtBean"/>
    <jsp:setProperty name="tenderDtBean" property="*"/>--%>
    
    
    
    <script type='text/javascript'>
        var changedSubDay;
        var subDayFlag = 1;
        var tenEventJS;
        function subDayAlert(obj)
        {
            var pMethodForAlert = obj.options[obj.selectedIndex].value;
            var prevProcMethod = document.getElementById('hdnProcureMethod').value;
            
            <%
                for (SPTenderCommonData tenEventNew : tenEvent)
                {%>
                        tenEventJS = "<%=tenEventNew.getFieldName1()%>";
                <%}
                for (SPTenderCommonData subDaysNew : subDays)
                {%>
                    if(prevProcMethod != "RFQ" && pMethodForAlert == "<%=subDaysNew.getFieldName4()%>" && tenEventJS == "<%=subDaysNew.getFieldName5()%>")
                    {
                        changedSubDay = "<%=subDaysNew.getFieldName3().equals("0") ? subDaysNew.getFieldName1() : subDaysNew.getFieldName2()%>";
                        subDayFlag = 2;
                    }
                    else if(pMethodForAlert == "<%=subDaysNew.getFieldName4()%>" && "TENDER" == "<%=subDaysNew.getFieldName5()%>")
                    {
                        changedSubDay = "<%=subDaysNew.getFieldName3().equals("0") ? subDaysNew.getFieldName1() : subDaysNew.getFieldName2()%>";
                        subDayFlag = 2;
                    }
                    
                <%}
            %>
                    
             
            
        }
    </script>
    
    
    <body onload="documentAvailable();ShowPreBidMeetingDates(); " >
        <%int countI = 0;
            int lotno = 0;
            String id = "";
            if (!request.getParameter("id").equals("")) {
                id = request.getParameter("id");
            }
            String userid = "";
            HttpSession hs = request.getSession();
            if (hs.getAttribute("userId") != null) {
                userid = hs.getAttribute("userId").toString();
                tenderSrBean.setLogUserId(userid);
            }
            String msgSecAmt = "";
            //if ("Submit".equals(request.getParameter("hdnbutton"))) {
            //  response.sendRedirect("CreateTenderInsert.jsp");
            //}

            String contractTypeREOI = "";
            String foreignFirmREOI = "";
            %>
        <input type="hidden" id="boolcheck" value="true"/>
        <%--<input type="text" id="counthd" value="1"/>--%>

        <div class="mainDiv">
            <div class="fixDiv">

                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <div class="contentArea_1">
                    <!--                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="contentArea_1">-->
                    <div class="pageHead_1">Create IFB/ PQ  Tender / REOI / RFP / Advertisement </div>

                    <form id="frmCreateTender" name="frmCreateTender" method="POST" action="CreateTenderInsert.jsp">
                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                            <%                                List<CommonTenderDetails> objREOITenderDetails = tenderSrBean.getAPPDetails(Integer.parseInt(id), "REOITenderInfoForRFP");
                                for (CommonTenderDetails commonTenderDetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "tender")) {
                                    List<Object[]> obj = tenderSrBean.getConfiForTender(Integer.parseInt(id));
                            %>
                            <tr>
                                <td style="font-style: italic" colspan="4" class="ff t-align-left" align="left">
                                    <%
                                        if (objREOITenderDetails != null && objREOITenderDetails.size() > 0) {
                                            CommonTenderDetails objREOI = objREOITenderDetails.get(0);

                                            if (objREOI != null) {
                                                contractTypeREOI = objREOI.getContractType();
                                                foreignFirmREOI = objREOI.getForeignFirm();
                                            }
                                            // out.println("contractTypeREOI="+contractTypeREOI);
                                        }
                                    %>
                                    Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                        </table>
                        <div class="tableHead_22 ">Integrity Pact :</div>
                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                            <tr>
                                <td class="ff">Terms and Conditions : <span>*</span></td>
                                <td style="width: 80%;">
                                    <div style=" padding: 5px; border-top: 1px #ededed solid; border-left: 1px #ededed solid; width:100%; height: 250px; text-align: justify;overflow: scroll">
                                        <b style="color: green;">Integrity Pact</b>
                                        <br/><br/><b> 1. General:</b>
                                        <br/>Whereas the Head of the Procuring Agency of the Royal Government of Bhutan, hereinafter referred to as the <b>“Employer”</b> on one part, and <b>Business</b> registered with the authority concerned, hereinafter referred to as the <b>“Bidder”</b> on the other part hereby shall execute this pact as follows:
                                        <br/><br/>Whereas, the Employer and the Bidder agree to abide by the terms and conditions stated in this document, hereinafter referred to as ‘IP’.
                                        <br/><br/>This IP is applicable to all <b>contracts</b>  related to works, goods and services.
                                        <br/><br/><b> 2. Objectives:</b>
                                        <br/>This IP aims to prevent all forms of corruption or deceptive practice by following a system that is fair, transparent and free from any influence/unprejudiced dealings in the <b>bidding process</b>  and <b>contract administration</b> , with a view to:
                                        <br/><br/>2.1 Enabling the Employer to obtain the desired contract at a reasonable and competitive  price in conformity to the defined specifications of  the works  or goods or services; and 
                                        <br/><br/>2.2 Enabling bidders to abstain from bribing or any corrupt practice in order to secure the contract by providing assurance to them that their competitors will also refrain from bribing and other corrupt practices.
                                        <br/><br/><b>Business</b>, means any business, trade, occupation, profession, calling, industry or undertaking of any kind, or any other activity carried on for gain or profit by any person within Bhutan or elsewhere, and includes all property derived from or used in or for the purpose of carrying on such other activity, and all the rights and liabilities arising from such other activity.
                                        <br/><b>Contract</b>, means a formal agreement in writing entered into between the procuring agency and the supplier, service provider or the contractor on acceptable terms and conditions and which are in compliance with all the relevant provisions of the laws of the Kingdom. The term “contract” will also include “framework contract”.
                                        <br/><b>Bidding process</b>, for the purpose of this IP, shall mean the procedures covering tendering process starting from bid preparation, bid submission, bid processing, and bid evaluation. 
                                        <br/><b>Contract administration</b>, for the purpose of this IP, shall mean contract award, contract implementation, un-authorized sub-contracting and contract handing/taking over.
                                        <br/><br/><b> 3. Scope:</b>
                                        <br/>The validity of this IP shall cover the bidding process and contract administration period.
                                        <br/><br/><b> 4. Commitments of the Employer :</b>
                                        <br/>The Employer Commits itself to the following:
                                        <br/><br/>4.1 The  Employer  hereby  undertakes  that  no  officials of  the  Employer,  connected directly or indirectly with the contract, will demand, take a promise for or accept, directly or through intermediaries, any bribe, consideration, gift, reward, favor or any material or immaterial benefit or any other advantage from the Bidder, either for themselves or for any person, organization or third party related to the contract in exchange for an advantage in the bidding process and contract administration.
                                        <br/><br/>4.2 The Employer hereby confirms that its officials shall declare conflict of interest and if any official(s) or his or her relative or associate has a private or personal interest in a decision to be taken by the Employer, those officials shall not vote or take part in a proceeding or process of the Employer relating to such decision.
                                        <br/><br/>4.3 Officials of the Employer, who may have observed or noticed or have reasonable suspicion of person(s) who breaches or attempts to breach the conditions under clauses 4.1 and 4.2 shall report it to the Employer or the authority concerned.
                                        <br/><br/>4.4 Following report on breach of conditions under clauses 4.1 and 4.2 by official (s), through any source, necessary disciplinary proceedings or any other action as deemed fit, shall be initiated by the Employer including criminal proceedings and such a person shall be debarred from further dealings related to the bidding process and contract administration.
                                        <br/><br/><b> 5. Commitments of Bidders :</b>
                                        <br/>The Bidder commits himself/herself to take all measures necessary to prevent corrupt practices, unfair means and illegal activities during any stage of the bidding process and contract administration in order to secure the contract or in furtherance to secure it and in particular commits himself/herself to the following:
                                        <br/><br/>5.1 The Bidder shall not offer, directly or through intermediaries, any bribe, gift, consideration, reward, favor, any material or immaterial benefit or other advantage, commission, fees, brokerage or inducement to any official of the Employer, connected  directly  or  indirectly  with  the bidding  process  and contract administration, or to any person, organization or third party related to the contract in exchange for any advantage in the bidding process and contract administration.
                                        <br/><br/>5.2 The Bidder shall not collude with other parties interested in the contract to manipulate in whatsoever form or manner, the bidding process and contract administration.
                                        <br/><br/>5.3 If the bidder(s) have observed or noticed or have reasonable suspicion that the provisions of the IP have been breached by the Employer or other bidders, the bidder shall report such breach to the Employer or authority concerned.
                                        <br/><br/><b> 6. Sanctions :</b>
                                        <br/>For the breach of any of the aforementioned conditions, the bidder/employer shall also be liable for offences under the Chapter 4 of the Anti-Corruption Act 2011 and other relevant rules and laws.
                                        <br/><br/><b> 7. Monitoring and Administration :</b>
                                        <br/><br/>7.1 The respective Employer shall be responsible for administration and monitoring of the IP as per the relevant laws. 
                                        <br/><br/>7.2 The bidder shall have the right to appeal as per the arbitration mechanism contained in the relevant rules.
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><input type="checkbox" value="IntegrityPack" name="IntegrityPack" id="integrityPackcnk" />
                                    <span id="tnc">I, hereby declare that I have read and understood the clauses of this agreement and I hereby affirm that I shall stand by the above conditions. In the event that I default, I understand that I shall be dealt with as per the Anti-Corruption Act of Bhutan 2011 and/or any other Rules and Laws of the Kingdom of Bhutan.</span>
                                    
                                </td>
                                
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <span class="reqF_1" id="spanchkIntegrityPack"></span>
                                </td>
                            </tr>

                        </table>
                        <div class="tableHead_22 ">Basic Information :</div>
                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                            <tr>
                                <td width="25%" class="ff">Procuring Agency :</td>
                                <td width="25%">
                                    
                                    <%=commonTenderDetails.getPeOfficeName()%>,
                                    
                                    <label id="lblMinistry"><%=commonTenderDetails.getMinistry()%>,</label>
                                    <input type="hidden" id="pqTenderId" value="<%=commonTenderDetails.getPqTenderId()%>" name="pqTenderId" />
                                    
                                    <%=commonTenderDetails.getPeDistrict()%>
                                    
                                </td>
                                    <%--      <td width="25%" class="ff">Division :</td>
                                <td width="25%"><label id="lblDivision"><%=commonTenderDetails.getDivision()%></label></td>
                            </tr>
                            <tr>
                                <td class="ff">Organization :</td>
                                <td><%=commonTenderDetails.getAgency()%></td> --%>
                                <%--td class="ff">Procuring Agency Name :</td>
                                <td><%=commonTenderDetails.getPeOfficeName()%></td--%>
                            </tr>
                            <tr>
                                <td class="ff">Procuring Agency Code :</td>
                                <td><%=commonTenderDetails.getPeCode()%></td>
                                <%--td class="ff">Procuring Agency Dzongkhag / District :</td>
                                <td><%=commonTenderDetails.getPeDistrict()%></td--%>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Procurement Category :</td>
                                <td width="25%"><label id="lblProcurementNature"><%=commonTenderDetails.getProcurementNature()%></label>
                                <input type ="hidden" id="hdnProcurementNature" value="<%=commonTenderDetails.getProcurementNature()%>"/></td>
                                <td class="ff" width="25%">Procurement Type :</td>
                                <td width="25%"><%if (commonTenderDetails.getProcurementType().equalsIgnoreCase("NCT")) {
                                        out.print("NCB");
                                    } else if (commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")) {
                                        out.print("ICB");
                                    }%> <!--Edit by aprojit -->
                                    <input type="hidden" value="<%=configDocumentFeesService.getMaxDocumentPriceInBDT(commonTenderDetails.getProcurementType())%>" id="docPriceBDTMaxID" name="docPriceBDTMaxID"/>
                                    <input type="hidden" value="<%=configDocumentFeesService.getMaxDocumentPriceInUSD(commonTenderDetails.getProcurementType())%>" id="docPriceUSDMaxID" name="docPriceUSDMaxID"/>
                                    <input type="hidden" value="<%=commonTenderDetails.getProcurementType()%>" id="hdnprocuretype"/>
                                </td>

                            </tr>
                                
                           <%
                            TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                            Object list2 = null;
                            Object list3[] = null;
                            //Object list4 = null;
                            list2 = tenderDocumentSrBean.getPkgNo(id);
                            //list4 = tenderDocumentSrBean.getWorkCategory(id);
                            list3 = tenderDocumentSrBean.getBidderCategory(list2.toString());
                            
                            if(commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works"))
                            {%>
                            <%-- Start Nitish --%>
                            <tr>
                                <td class="ff" width="25%">Work Type : </td>
                                <td width="25%"><label id="workCategory"><%=list3[1].toString()%></label></td>
                            </tr>
                            <%-- END Nitish --%>
                            <tr>
                                <td class="ff" width="25%">Work Category : <span>*</span></td>
                                <td width="25%">
                                    <input type="checkbox" name="All" id="ckhAll" value="All" <%if ("W1, W2, W3, W4".equalsIgnoreCase(list3[0].toString())) {
                                                out.print("checked");
                                            }
                                        %> onchange="setBidderCategory(this);"/> All
                                    <input type="checkbox" name="W1" id="chkW1" value="W1" <%
                                            if (list3[0].toString().contains("W1")) {
                                                out.print("checked");
                                            }
                                        %> onchange="setBidderCategory(this);"/> W1
                                    <input type="checkbox" name="W2" id="chkW2" value="W2" <%
                                            if (list3[0].toString().contains("W2")) {
                                                out.print("checked");
                                            }
                                        %> onchange="setBidderCategory(this);"/> W2
                                    <input type="checkbox" name="W3" id="chkW3" value="W3" <%
                                            if (list3[0].toString().contains("W3")) {
                                                out.print("checked");
                                            }
                                        %> onchange="setBidderCategory(this);"/> W3
                                    <input type="checkbox" name="W4" id="chkW4" value="W4" <%
                                            if (list3[0].toString().contains("W4")) {
                                                out.print("checked");
                                            }
                                        %> onchange="setBidderCategory(this);"/> W4
                                    <span class="reqF_2" id="msgBidderCategory"></span>
                                </td>
                            </tr>
                         <%}%>
                                
                            <tr>
                                <!--td class="ff">Event Type :</td-->
                                <!--td--><% if (commonTenderDetails.getEventType() != null) {%>
                                    <label hidden id="lblEventType">
                                        <%if (commonTenderDetails.getEventType().equalsIgnoreCase("RFQ")){
                                                //out.print("LIMITED ENQUIRY");
                                           } 
                                         else {
                                               //out.print(commonTenderDetails.getEventType().toUpperCase());
                                          }%>
                                    </label><% }%>
                                    <input type="hidden" id="hdnEventType" name="hdnEventType" value="<%=commonTenderDetails.getEventType()%>"/>
                                <!--/td-->
                                <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {%> <td class="ff" width="25%">Invitation for :</td>
                                <td width="25%"><% if (commonTenderDetails.getInvitationFor() != null) {%><%=commonTenderDetails.getInvitationFor()%><% }%></td> <% }%>
                            </tr><% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {
                                    String msg = "";
                                    if (commonTenderDetails.getEventType() != null) {
                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                            msg = "Request for expression of interest for selection of :";
                                        } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {
                                            msg = "REOI/RFP for selection of :";
                                        }
                                    }
                            %>
                            <tr><% if (!msg.equals("")) {%>
                                <td class="ff"><%=msg%></td>
                                <td><label><%=commonTenderDetails.getReoiRfpFor()%></label><%--<input name="expInterestSel" readonly type="text" class="formTxtBox_1" id="txtexpInterestSel" style="width:200px;" value="<%=commonTenderDetails.getReoiRfpFor()%>" />--%></td><% }%>

                                <%
                                    if (contractTypeREOI != null && !contractTypeREOI.equalsIgnoreCase("")) {
                                %>
                                <td class="ff">Contract Type : </td>
                                <td>
                                    <input type="hidden" name="contractType" id="contractType" value="<%=contractTypeREOI%>"/>
                                    <%=contractTypeREOI%>
                                </td>
                                    <%
                                    } else {
                                    %>
                                <td class="ff">Contract Type : <span>*</span></td>
                                <td>
                                    <select name="contractType" id="contractType" class="formTxtBox_1" id="cmbcontractType" style="width:200px;">
                                        <option value="select" selected="selected">--- Please Select ---</option>
                                        <option value="Time based">Time based</option>
                                        <option value="Lump - Sum">Lump – Sum</option>
                                    </select>
                                    <%
                                        }
                                    %>
                                    <span id="spancontractType" style="color: red;"></span>
                                </td>
                            </tr><% }%>
                            <tr><%
                                String msgTender = "";
                                if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {
                                    msgTender = "Invitation Reference No.";
                                } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                    msgTender = "REOI No.";
                                } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {
                                    msgTender = "RFP No.";
                                } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {
                                    msgTender = "RFA No.";
                                } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFQ") || commonTenderDetails.getEventType().equalsIgnoreCase("RFQU") || commonTenderDetails.getEventType().equalsIgnoreCase("RFQL")) {
                                    msgTender = "LEM No.";
                                }
                                %>
                                <td class="ff" width="25%"><label id="msgTenderShow"><%=msgTender%></label><% if (!msgTender.equals("")) {%> : <span>*</span></td>
                                <td colspan="3" width="25%"><input name="invitationRefNo" type="text" class="formTxtBox_1" id="txtinvitationRefNo" style="width:200px;"/>
                                    <span class="reqF_1" id="spantxtinvitationRefNo"></span> <%--rajeshsingh--%>
                                    <input type="hidden" id="hdnmsgTender" name="hdnmsgTenderName" value="<%=commonTenderDetails.getEventType()%>" ></input>
                                </td><% }%>
                                <!--                                <td class="ff"></td>
                                                                <td></td>-->
                            </tr>
                        </table>

                        <!--div class="tableHead_22 ">Key Information and Funding Information :</div-->
                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                            <tr>
                                <td width="25%" class="ff">Procurement Method : </td>
                                <%
                                  boolean Works = false;  
                                  boolean WorkspqReq = false;        
                                  if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")){ 
                                      Works = true;
                                      if( commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {
                                           WorkspqReq = true;
                                      }
                                  }                     
                                %>
                                <td>
                                <%
                                if(commonTenderDetails.getReoiTenderId()!= null && commonTenderDetails.getReoiTenderId() != 0)
                                {
                                   out.print(commonTenderDetails.getProcurementMethod());
                                   %>
                                    <input type="hidden" id="hdnProcurementMethod" name="hdnProcurementMethod" value="<%=commonTenderDetails.getProcurementMethod()%>"/>
                                   <%
                                }
                                else{
                                %>
                                <select class="formTxtBox_1" id="hdnProcurementMethod" name="hdnProcurementMethod" style="width: 215px;" onChange="subDayAlert(this);setFieldName(this);"> 
                                        <%if (!commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")){ %>
                                    <option value="OTM" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM")) {
                                                                out.print("selected");
                                                            }%>>Open Tendering Method (OTM)</option>
                                    <% if (!WorkspqReq) { %>                      
                                    <option value="LTM" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LTM")) {
                                                                out.print("selected");
                                                            }%>>Limited Tendering Method (LTM)</option>
                                    <option value="DPM" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("DPM"))  {
                                                                out.print("selected");
                                                            }%>>Direct Contracting Method (DCM)</option>
                                    <option value="FC" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("FC"))  {
                                                                out.print("selected");
                                                            }%>>Framework Contract (FC)</option>
                                    <%  { %>
                                    <option value="RFQ" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LEQ") || commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQ"))  {
                                                                out.print("selected");
                                                            }%>>Limited Enquiry Method (LEM)</option>
                                        <% } 
                                    }
                                    }else {
                                     if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                        %>
                                        <option value="QCBS" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("QCBS")) {
                                                                out.print("selected");
                                                            }%>>Quality Cost Based Selection (QCBS)</option>
                                        <option value="LCS" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LCS")) {
                                                                out.print("selected");
                                                            }%>>Least Cost Selection (LCS)</option>
                                         <option value="SBCQ" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SBCQ")) {
                                                                out.print("selected");
                                                            }%>>Selection Based on Consultants Qualification (SBCQ)</option>
                                         <option value="SFB" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SFB")) {
                                                                out.print("selected");
                                                            }%>>Selection under a Fixed Budget (SFB)</option>
                                        <%
                                    } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP") || commonTenderDetails.getEventType().equalsIgnoreCase("RFA") ) {
                                            if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("IC")) {
                                                    %>
                                                    <option value="IC" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("IC")) {
                                                                            out.print("selected");
                                                                        }%>>Selection of Independent Individual Consultant (IC)</option>
                                                    <%
                                                 }
                                                 else{
                                                    %>
                                                    <option value="QCBS" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("QCBS")) {
                                                                            out.print("selected");
                                                                        }%>>Quality Cost Based Selection (QCBS)</option>
                                                     <option value="SFB" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SFB")) {
                                                                            out.print("selected");
                                                                        }%>>Selection under a Fixed Budget (SFB)</option>
                                                    <option value="LCS" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LCS")) {
                                                                            out.print("selected");
                                                                        }%>>Least Cost Selection (LCS)</option>
                                                    <!-- <option value="SBCQ" <%/*if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SBCQ")) {
                                                                            out.print("selected");
                                                                        }*/%>>Selection Based on Consultants Qualification (SBCQ)</option> -->
                                                    <option value="SSS" <%if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SSS")) {
                                                                            out.print("selected");
                                                                        }%>>Single Source Selection (SSS)</option>
                                                    <%
                                                 }

                                            }
                                    }
                                        %>
                                         </select>
                                        <%
                                        }
                                        %>
                                    <input type="hidden" id="hdnProcureMethod" name="hdnProcureMethod" value="<%=commonTenderDetails.getProcurementMethod()%>"/>
                                </td>
                               <%-- <td width="25%"><input type="hidden" value="<%=commonTenderDetails.getProcurementMethod()%>" id="hdnProcurementMethod" name="hdnProcurementMethod"/><label id="lblProcurementMethod">
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQ")) {%><%="Request For Quotation (RFQ)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM")) {%><%="Open Tendering Method (OTM)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LTM")) {%><%="Limited Tendering Method (LTM)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("TSTM")) {%><%="Two Stage Tendering Method (TSTM)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("QCBS")) {%><%="Quality Cost Based Selection (QCBS)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LCS")) {%><%="Least Cost Selection (LCS)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SFB")) {%><%="Selection under a Fixed Budget (SFB)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("DC")) {%><%="Design Contest (DC)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SBCQ")) {%><%="Selection Based on Consultants Qualification (SBCQ)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SSS")) {%><%="Single Source Selection (SSS)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("IC")) {%><%="Individual Consultant (IC)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("CSO")) {%><%="Community Service Organisation (CSO)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("DPM")) {%><%="Direct Procurement (DPM)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OSTETM")) {%><%="One stage Two Envelopes Tendering Method (OSTETM)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQU")) {%><%="Request For Quotation Unit Rate (RFQU)"%><%}%>
                                        <% if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQL")) {%><%="Request For Quotation Lump Sum (RFQL)"%><%}%>
                                    </label></td>--%>
                               
                                <td width="25%" class="ff">Budget Type :</td>
                                <td width="25%"><%if (commonTenderDetails.getBudgetType().equalsIgnoreCase("Revenue")) {
                                        out.print("Recurrent");
                                    } else if (commonTenderDetails.getBudgetType().equalsIgnoreCase("Development")) {
                                        out.print("Capital");
                                    } else {
                                        out.print(commonTenderDetails.getBudgetType());
                                    }%></td> <!--Edit by aprojit -->
                            </tr>
                            <tr>
                                <td width="25%" class="ff">Source of Funds :</td>
                                <td width="25%"><%=commonTenderDetails.getSourceOfFund()%></td>
                                <% if (commonTenderDetails.getDevPartners() != null) {%>
                                <td width="25%" class="ff">Development Partner :</td>
                                <td width="25%"><%=commonTenderDetails.getDevPartners()%></td>
                                <% } %>
                            </tr>
                            <%// if (commonTenderDetails.getDevPartners() == null && commonTenderDetails.getEstCost().setScale(0,0).equals(BigDecimal.ZERO)) {%>
                            <!--                            <script type="text/javascript">
                                                            $('#trDevEst').hide();
                                                        </script>-->
                            <%//}
                                List<Object> allocatedCost = tenderSrBean.getAllocatedCost(Integer.parseInt(id));
                                if (!allocatedCost.isEmpty() && !(allocatedCost.get(0).toString().startsWith("0.00"))) {
                                    if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LTM") || commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SFB")) {
                            %>
                            <tr id="trDevEst">
                                <td width="25%" class="ff">Allocated Budget Amount in Tk. :</td>
                                <td width="25%"><%=new BigDecimal(allocatedCost.get(0).toString()).setScale(2, 0)%></td>
                                <td width="25%" class="ff">&nbsp;</td>
                                <td width="25%">&nbsp;</td>
                            </tr>
                            <% }
                                }%>
                        </table>
                        <div class="tableHead_22 ">Particular Information :</div>
                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" id="tableDT">
                            <tr>
                                <td width="25%" class="ff">Project Code : </td>
                                <td width="25%"><% if (commonTenderDetails.getProjectCode() != null) {%><%=commonTenderDetails.getProjectCode()%><% } else {%>Not applicable<%}%></td>
                                <td width="25%" class="ff">Project Name : </td>
                                <td width="25%"><% if (commonTenderDetails.getProjectName() != null) {%><%=commonTenderDetails.getProjectName()%><% } else {%>Not applicable<%}%></td>
                            </tr>
                            <tr>
                                <td  width="25%" class="ff"><% if (!(commonTenderDetails.getEventType().equalsIgnoreCase("REOI") || commonTenderDetails.getEventType().equalsIgnoreCase("RFA") || commonTenderDetails.getEventType().equalsIgnoreCase("RFP"))) {%>Tender/Proposal <% }%>Package No. and Description :</td>
                                <td colspan="3" width="75%"><%=commonTenderDetails.getPackageNo()%><br/>
                                    <%=commonTenderDetails.getPackageDescription()%></td>
                            </tr>
                            <tr>
                                <td class="ff"  width=25%">Category : </td>
                                <td  width="75%" colspan="3"><%=commonTenderDetails.getCpvCode()%></td>
                            </tr>
                            <tr id="SetPreBidMeeting">
                                <td class="ff">
                                    Set <% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre - Qualification <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>Pre - REOI <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Pre - RFP <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>Pre - Application <%} else {%>Pre - bid <%}%>meeting:
                                </td>    
                                <td>
                                    <input type="checkbox" name="SetPreBidMeetingCheckbox" id="SetPreBidMeetingCheckbox" onchange="ShowPreBidMeetingDates()"/>
                                </td>     
                            </tr>
                            <tr>
                                <%
                                    String msgpublication = "";
                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {
                                        msgpublication = "Scheduled Tender/Proposal";
                                    }
                                    else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFQ")) {
                                        msgpublication = "LEM";
                                    }
                                    else if (!commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {
                                        msgpublication = commonTenderDetails.getEventType().toUpperCase();
                                    } else {
                                        msgpublication = "Scheduled Pre-Qaulification";
                                    }
                                %>
                                <td class="ff" id="publicationDT"><%//=msgpublication%> Publication<br/>Date and Time : <span>*</span></td>
                                <td class="formStyle_1"><input name="tenderpublicationDate" type="text" class="formTxtBox_1" value="<%=DateUtils.formatDate(commonTenderDetails.getTenderPubDt())%>" id="txttenderpublicationDate" style="width:100px;" readonly="true"  onfocus="GetCal('txttenderpublicationDate', 'txttenderpublicationDate');" onblur="publicationHoliday();findHoliday(this,0);clearvalidation('spantxttenderpublicationDate');"/>
                                    <img id="txttenderpublicationDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txttenderpublicationDate', 'txttenderpublicationDateimg');"/>
                                    <span id="spantxttenderpublicationDate" ></span>
                                    <p id="demoPublication" style="color:red"></p>
                                </td>
                                <% //if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().equalsIgnoreCase("1 stage-TSTM")) {%>
                                <td class="ff" id="docSellDT"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification<% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender<% }%> Document last selling /<br/>downloading Date and Time : <span>*</span></td>
                                <td class="formStyle_1"><input name="tenderLastSellDate" type="text" class="formTxtBox_1" id="txttenderLastSellDate" style="width:100px;" readonly="true"  onfocus="GetCal('txttenderLastSellDate', 'txttenderLastSellDate');" onblur="docSellHoliday();findHoliday(this,1); clearvalidation('spantxttenderLastSellDate'); "/>
                                    <img id="txttenderLastSellDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txttenderLastSellDate', 'txttenderLastSellDateimg');"/>
                                    <span id="spantxttenderLastSellDate" ></span>
                                    <p id="demoDocSell" style="color:red"></p>
                                </td>
                                <% //}
                                %>

                            </tr>
                            <%                                     //List<TblConfigPreTender> conPreTender = tenderSrBean.getConfigPreTenderDetails(commonTenderDetails.getProcurementMethodId());
                                //rishita
                                List<SPTenderCommonData> sptcd = tenderCommonService.returndata("GetConfigPrebid", id, null);

                            %>
                            
                            <tr id="PreBidMeetingDates">   
                                <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre - Qualification <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>Pre - REOI <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Pre - RFP <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>Pre - Application <%} else {%>Pre - bid <%}%>meeting Start<br/>Date and Time : <%for (SPTenderCommonData sptcd1 : sptcd) {
                                            if (sptcd1.getFieldName1().equalsIgnoreCase("Yes")) {%>
                                    <input type="hidden" id="hdncheck" value="<%=sptcd1.getFieldName1()%>" />
                                    <%}
                                        }%>
                                </td>
                                <td class="formStyle_1"><input name="preTenderMeetStartDate" type="text" class="formTxtBox_1" id="txtpreTenderMeetStartDate" style="width:100px;" readonly="true"  onfocus="GetCal('txtpreTenderMeetStartDate', 'txtpreTenderMeetStartDate');" onblur="meetStartHoliday();findHoliday(this,2); clearvalidation('spantxtpreTenderMeetStartDate'); "/>
                                    <img id="txtpreTenderMeetStartDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtpreTenderMeetStartDate', 'txtpreTenderMeetStartDateimg');"/>
                                    <!--<img src="../resources/images/Dashboard/Refresh.png" alt="Clear date" border="0" style="vertical-align:middle;"  onclick ="ClearPreBidStartDate();"/>-->
                                    <span id="spantxtpreTenderMeetStartDate" ></span>
                                    <p id="demoMeetStart" style="color:red"></p>
                                </td>
                                <td class="ff" id="PreBidEndDateTag"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre - Qualification <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>Pre - REOI <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Pre - RFP <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>Pre - Application <% } else {%>Pre - bid <%}%>meeting End<br/>Date and Time : <%for (SPTenderCommonData sptcd1 : sptcd) {
                                            if (sptcd1.getFieldName1().equalsIgnoreCase("Yes")) {%><% }
                                                }%>
                                </td>
                                <td class="formStyle_1"><input name="preTenderMeetEndDate" type="text" class="formTxtBox_1" id="txtpreTenderMeetEndDate" style="width:100px;" readonly="true"  onfocus="" onblur="meetEndHoliday();findHoliday(this,3); clearvalidation('spantxtpreTenderMeetEndDate');" onclick="EnterPreBidMeetingStartDateFirst();" />
                                    <img id="txtpreTenderMeetEndDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="EnterPreBidMeetingStartDateFirst();"/>
                                    <span id="spantxtpreTenderMeetEndDate" ></span>
                                    <p id="demoMeetEnd" style="color:red"></p>
                                    <%for (SPTenderCommonData sptcd1 : sptcd) {%>
                                    <input type="hidden" value="<%=sptcd1.getFieldName3()%>" name="pubBidDays" id="pubBidDays"/><% }
                                        for (SPTenderCommonData sptcdGetProcMethodDays : tenderCommonService.returndata("GetProcMethodDays", id, null)) {
                                            int daysCounter = 0;
                                    %><input type="hidden" value="<%=sptcdGetProcMethodDays.getFieldName3().equals("0") ? sptcdGetProcMethodDays.getFieldName1() : sptcdGetProcMethodDays.getFieldName2()%>" name="procMethodDays<%=daysCounter%>" id="procMethodDays<%=daysCounter%>"/><%
                                        }%>
                                        <span id="EnterPreBidStartDateFirstSpan" style="color: red;"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                    <td class="ff" id="closingDT"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification <% }
                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender/Proposal <% }

                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Proposal <% }
                                                                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>EOI <% }%> Closing<br/>Date and Time : <span>*</span></td>
                                <td class="formStyle_1"><input name="preQualCloseDate" type="text" class="formTxtBox_1" id="txtpreQualCloseDate" value="<%=DateUtils.formatDate(commonTenderDetails.getSubmissionDt())%>" style="width:100px;" readonly="true"  onfocus="GetCal('txtpreQualCloseDate', 'txtpreQualCloseDate');" onblur="findHoliday2(this);openCloseWeekend();UpdateOpdtAndBSecuritySubDt(this); clearvalidation('spantxtpreQualCloseDate');clearvalidation('spantxtpreQualOpenDate');"/> <!--findHoliday(this, 4);-->
                                    <img id="txtpreQualCloseDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtpreQualCloseDate', 'txtpreQualCloseDateimg');"/>
                                    <span id="spantxtpreQualCloseDate" ></span>
                                    <p id="demoClose" style="color:red"></p>
                                    <p id="holiClose" style="color:red"></p>
                                </td>
                                    <td class="ff" id="openingDT"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification <% }
                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender/Proposal <% }

                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Proposal <% }
                                                                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>EOI <% }%> Opening<br/>Date and Time : <span>*</span></td>
                                <td class="formStyle_1"><input name="preQualOpenDate" type="text" class="formTxtBox_1" id="txtpreQualOpenDate" value="<%=DateUtils.formatDate(commonTenderDetails.getOpeningDt())%>" style="width:100px;" readonly="true" onfocus="" onblur="clearvalidation('spantxtpreQualOpenDate'); "/><!--findHoliday(this, 5);GetCal('txtpreQualOpenDate', 'txtpreQualOpenDate');-->
                                    <img id="txtpreQualOpenDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick =""/> <!--GetCal('txtpreQualOpenDate', 'txtpreQualOpenDateimg');-->
                                    <span id="spantxtpreQualOpenDate" ></span>
                                    <p id="demoOpen" style="color:red"></p>
                                    <p id="holiOpen" style="color:red"></p>
                                </td>
                            </tr>
                                
                            <% if (!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")) { %>
                            <tr>
                                <td class="ff">Last Date and Time for bid Security Submission :<span>*</span></td>
                                <td><input name="lastDateTenderSub" type="text" class="formTxtBox_1" id="txtlastDateTenderSub" style="width:100px;" readonly="true"  onfocus="GetCal('txtlastDateTenderSub', 'txtlastDateTenderSub');" onblur="secSubHoliday();findHoliday(this, 6); clearvalidation('spantxtlastDateTenderSub');"/>
                                    <img id="txtlastDateTenderSubimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtlastDateTenderSub', 'txtlastDateTenderSubimg');"/>
                                    <span id="spantxtlastDateTenderSub" style="color: red;"></span>
                                    <p id="demoSecSub" style="color:red"></p>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <% }%>
                            
                        </table>

                        <div class="tableHead_22 ">Information for Bidder/Consultant :</div>

                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%" id="infoBidder">
                            <tr>
                                <td width="25%" class="ff"><input type="hidden" id="hdnevenyType" value="<%=commonTenderDetails.getProcurementNature()%>"/>Eligibility of Bidder/Consultant : <span>*</span></td>
                                <td colspan="2"><textarea cols="100" rows="5" id="txtaeligibilityofTenderer" name="eligibilityofTenderer" class="formTxtBox_1" ></textarea>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        CKEDITOR.replace('eligibilityofTenderer',
                                                {
                                                    toolbar: "egpToolbar"

                                                });
                                        //]]>
                                    </script>
                                    <span id="spantxtaeligibilityofTenderer"></span>
                                </td>

                            </tr>
                            <tr>
                                <td class="ff">Brief Description of <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods")) {%>Goods and Related Service<% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {%>Works<% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {%>assignment<% }%> : <span>*</span></td>
                                <td ><input type="hidden" value="<%=commonTenderDetails.getProcurementNature()%>" id="briefValMsg"/>
                                    <textarea cols="100" rows="5" id="txtabriefDescGoods" name="briefDescGoods" class="formTxtBox_1" ></textarea>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        CKEDITOR.replace('briefDescGoods',
                                                {
                                                    toolbar: "egpToolbar"

                                                });
                                        //]]>
                                    </script>
                                    <span id="spantxtabriefDescGoods"></span>
                                </td><td>&nbsp;</td>
                            </tr>
                            <% if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI") || commonTenderDetails.getEventType().equalsIgnoreCase("RFP") || commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>
                            <!--tr>
                                <td class="ff">Experience, Resources and<br />
                                    delivery capacity required : <span>*</span></td>
                                <td><textarea cols="100" rows="5" id="txtaexpRequired" name="expRequired" class="formTxtBox_1" ></textarea>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        CKEDITOR.replace('txtaexpRequired',
                                                {
                                                    toolbar: "egpToolbar"

                                                });
                                        //]]>
                                    </script>
                                    <span id="spantxtaexpRequired"></span>
                                </td><td>&nbsp;</td>
                            </tr-->
                            <tr>
                                <td class="ff">Other details (if  applicable) :</td>
                                <td><textarea cols="100" rows="5" id="txtaotherDetails" name="otherDetails" class="formTxtBox_1"></textarea>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        CKEDITOR.replace('txtaotherDetails',
                                                {
                                                    toolbar: "egpToolbar"

                                                });
                                        //]]>
                                    </script>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="ff"> Association with
                                    local  firm : <span>*</span></td>
                                    <%
                                        if (foreignFirmREOI != null && !foreignFirmREOI.equalsIgnoreCase("")) {
                                    %>
                                <td>
                                    <input type="hidden" name="assoForiegnFirm" id="assoForiegnFirm" value="<%=foreignFirmREOI%>"/>
                                    <%=foreignFirmREOI%>
                                    <%
                                    } else {
                                    %>
                                    <td>
                                        <select name="assoForiegnFirm" id="assoForiegnFirm" class="formTxtBox_1" id="cmbassoForiegnFirm" style="width:200px;">
                                            <option value="select" selected="selected">--- Please Select ---</option>
                                            <option value="Encouraged">Encouraged</option>
                                            <option value="Not Encouraged">Not Encouraged</option>
                                        </select>
                                        <%
                                            }
                                        %>

                                        <span id="spanassoForiegnFirm" style="color: red;"></span>
                                    </td>
                                    <td>&nbsp;</td>
                            </tr>
                            <% }
                                if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {
                            %>
                            <tr>
                                <td class="ff"> Evaluation Type :  <!-- <span>*</span>--></td>
                                <td>Package wise<input type="hidden" name="evaluationType" id="evaluationType" value="Package wise"/>
                                    <!--                                    <select name="evaluationType" class="formTxtBox_1" id="evaluationType" style="width:200px;">
                                                                            <option value="Package wise">Package wise</option>
                                                                        </select>-->
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <%                                                                    }
                                if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods")) {%>
                            <tr>
                                <td class="ff"> Evaluation Type :  <!-- <span>*</span>--></td>
                                <td>
                                    <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) { %>
                                    Package wise
                                    <% } 
                                       if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods")) { %>
                                    Lot wise
                                    <% } %>
                                    <input value="Lot wise" type="hidden" name="evaluationType" id="evaluationType"/>
                                    <!--                                    <select name="evaluationType" class="formTxtBox_1" id="evaluationType" style="width:200px;">
                                                                            <option value="Package wise">Package wise</option>
                                                                            <option value="Lot wise">Lot Wise </option>
                                                                            <option value="Item wise">Item wise</option>
                                                                        </select>-->
                                </td>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <%for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "lot")) {
                                        lotno++;
                                    }
                                %>
                                <td class="ff"> Document Available :  <!--<span>*</span>--></td>
                                <td>Package wise
                                    <input type="hidden" value="Package" name="docAvailable" id="docAvailable"/>
                                    <!--                                    <select name="docAvailable" class="formTxtBox_1" id="cmbdocAvailable" style="width:200px;" onchange="documentAvailable();">
                                                                            <option value="Package">Package wise</option>
                                    <%
                                        //if (lotno > 1) {
                                    %><option value="Lot">Lot wise</option>
                                    <% //}%>

                                </select>-->
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <% }
                                if (!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")) {
                            %>
                            <tr>
                                <td class="ff">Document Fees : <!--<span>*</span>--></td>
                                <td>Package wise
                                    <input type="hidden" value="Package wise" name="docFees" id="cmbdocFees"/>
                                    <!--                                    <select name="docFees" class="formTxtBox_1" id="cmbdocFees" style="width:200px;" onchange="changeDocumentsFees();">
                                                                            <option value="Package wise">Package wise</option>
                                                                            % if (lotno > 1) {%>
                                                                            <option value="Lot wise">Lot wise</option>
                                                                            <s% }%
                                                                        </select>-->
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <% } %>
                            <!--                            <tr id="docsprice" style="display: none;">
                                                                <td class="ff"><% //if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification Document Price (In BTN)<% //}
                                                                    //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {%>Tender Document Price (In BTN) <%// }%>
                                                             : <span>*</span></td>
                                                            <td><input name="preQualDocPrice" type="text" class="formTxtBox_1" id="txtpreQualDocPrice" style="width:200px;" />
                                                                <span id="spantxtpreQualDocPriceman"></span>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>-->
                            <%if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){%>
                            <tr id="docsprice">
                                <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification Document Price (In Nu.)<% }
                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {%>Tender/Proposal Document Price (In Nu.) <% }
                                            if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>RFP Document Price (In Nu.)<% }
                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("RFQ") || commonTenderDetails.getEventType().equalsIgnoreCase("RFQL") || commonTenderDetails.getEventType().equalsIgnoreCase("RFQU")) {%>LEM Document Price (In Nu.)<% }
                                                                                                           if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>REOI Document Price (In Nu.)<% }
                                                                                                           if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>RFA Document Price (In Nu.)<% }

                                    %>
                                    : <!--<span>*</span>--></td>
                                    <td><input name="preQualDocPrice" type="text" value="0" readonly onblur="documentPrice(this);" class="formTxtBox_1" id="txtpreQualDocPrice" style="width:200px;" />
                                    <div id="preQualDocPriceInWords"></div>
                                    <span id="spantxtpreQualDocPriceman"></span>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <!--  Start ICT Tender by Dohatec-->
                            <% if (commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")) {%>
                            <tr id="docspriceICT"> <td class="ff">Equivalent Tender Document Price (In USD)

                                    : <!--<span>*</span>--></td>


                                <td>
                                    <span style="float: right;margin-right: 50%;" id="spantxtBDRate" class="reqF_1"> <a href="http://www.bob.bt/" target="_blank;" style="color:red;">See Bank of Bhutan's Conversion Rate </a></span>
                                    <input  style="width:200px;"  name="preQualDocPriceUSD" type="text" class="formTxtBox_1" id="preQualDocPriceUSD"  onblur="documentPriceUSD(this);"/>
                                    <div  id="preQualDocPriceICTInWords"></div>
                                    <span id="spantxtpreQualDocPricemanICT" class="reqF_1"></span>


                                </td>
                                <!--<td><select name="cmbTndrCat" class="formTxtBox_1" id="cmbTndrCat">
                                      <option value="select">-- Select Currency --</option>

                                      <option value="Manufacturer">USD</option>
                                      <option value="Importer">GBP</option>

                                  </select></td>-->


                            </tr>
                            <% } %>
                            <!--  End ICT Tender by Dohatec-->
                            <% }
                                //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().contains("TSTM")) {
                                if (!obj.isEmpty()) {
                                    if (obj.get(0)[0].toString().equalsIgnoreCase("Yes") || obj.get(0)[2].toString().equalsIgnoreCase("Yes")) {
                            %>
                            <tr>
                                <td class="ff">Mode of Payment : </td>
                                <td><!--<input name="docFeesMode"  type="checkbox" class="" id="chkdocFeesModeOff" value="Offline" />
                                    Offline-->
                                    <!--                                    <input name="docFeesMode" type="checkbox" checked id="chkdocFeesModeBank" value="Bank"/>-->
                                    <!--                                    Bank<input type="hidden" id="hdnnouse"/>-->
                                    <label>Payment through Financial Institution</label>
                                    <input type="hidden" id="chkdocFeesModeBank" value="Bank" name="docFeesMode"/>
                                    <span id="spanchkdocFeesModeOffid"></span>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <% }
                                if (obj.get(0)[2].toString().equalsIgnoreCase("Yes") && commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {%>
                            <td class="ff">
                                <%

                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {
                                        msgSecAmt = "Please Enter Pre-Qualification Security Amount"; %>Pre-Qualification Security Amount (In Nu.)<% }
                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {
                                                    msgSecAmt = "Please Enter Bid Security Amount";%>Bid Security Amount (In Nu.) <% }
                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {
                                                    msgSecAmt = "Please Enter RFP Security Amount";%>RFP Security Amount (In Nu.)<% }
                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("RFQ") || commonTenderDetails.getEventType().equalsIgnoreCase("RFQL") || commonTenderDetails.getEventType().equalsIgnoreCase("RFQU")) {
                                                    msgSecAmt = "Please Enter LEM Security Amount";%>LEM Security Amount (In Nu.)<% }
                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                                    msgSecAmt = "Please Enter REOI Security Amount";%>REOI Security Amount (In Nu.)<% }
                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {
                                                    msgSecAmt = "Please Enter RFA Security Amount";%>RFA Security Amount (In Nu.)<% }

                                %>
                                <span class="mandatory">*</span></td>
                            <td ><input id="txtsecurityAmountService_0" name="securityAmountService" class="formTxtBox_1" type="text" style="width:200px;" onblur="chkAmountSecurity('<%=msgSecAmt%>');"/>
                                <span id="amountLotService" style="color: red;"></span>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>&nbsp;</td>
                            <%}
                                }

                                // if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") && commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM") && commonTenderDetails.getPqTenderId() == 0) {
                            %>
                            <!--                            <tr>
                                                            <td class="ff">
                                            Name &amp; Address
                                                    of the Office(s)
                                                                Selling <% //if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-qualification <% //}%>
                            <% //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {%>Tender <% //}%>Document : <span>*</span>
                        </td>
                        <td><textarea cols="100" rows="5" id="txtanameAddressTenderDoc" name="nameAddressTenderDoc" class="formTxtBox_1"></textarea>

                            <span id="spantxtanameAddressTenderDoc"></span>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                            <% //} else if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().equalsIgnoreCase("1 stage-TSTM")) {
                            %><tr>
                                <td class="ff">
                Name &amp; Address
                        of the Office(s)
                                    Selling <% //if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-qualification <% //}%>
                            <% //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {%>Tender <% //}%>Document : <span>*</span>
                        </td>
                        <td><textarea cols="100" rows="5" id="txtanameAddressTenderDoc" name="nameAddressTenderDoc" class="formTxtBox_1"></textarea>

                            <span id="spantxtanameAddressTenderDoc"></span>
                        </td>
                        <td>&nbsp;</td>
                    </tr>-->



                            <!--                            <tr>
                                                            <td class="ff">Name &amp; Address
                                                                of the Office(s) for tender security submission : <span>*</span>
                                                            </td>security submission
                                                            <td><textarea cols="100" rows="5" id="txtanameAddressTenderSub" name="nameAddressTenderSub" class="formTxtBox_1"></textarea>
                                                                <span id="spantxtanameAddressTenderSub" style="color: red;"></span>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>-->
                            <%
                                if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {

                                    if (!commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                        //EvalSerCertiSrBean evalSerCertiSrBean = new EvalSerCertiSrBean();
                                        //if(evalSerCertiSrBean.evalCertiLink(Integer.parseInt(id)) != 0){%>
                            <tr>
                                <td class="ff">Passing Points For Technical Evaluation : <span>*</span></td>
                                <td><input name="passingMarks" type="text" class="formTxtBox_1" id="txtPassingMarks" style="width:200px;" onblur="valPassingMarks();"/>
                                    <span id="spanPassingMarks" class="mandatory"></span>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <%}
                                boolean isT1L1 = false;
                                CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                List<SPCommonSearchDataMore> envDataMores = dataMore.geteGPData("GetTenderEnvCount", id, null, null);
                                if (envDataMores != null && (!envDataMores.isEmpty())) {
                                    if (envDataMores.get(0).getFieldName2().equals("3")) {
                                        //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                                        isT1L1 = true;
                                    }
                                }
                                if (isT1L1) {%>
                            <tr>

                                <td class="ff">Weightage For Technical Evaluation (%) : <span>*</span></td>
                                <td><input name="weightPer" type="text" class="formTxtBox_1" id="txtweightPer" style="width:200px;" onblur="valWeightPer();"/>
                                    <span id="spanPassingMarks" class="mandatory"></span>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="ff">Weightage For Financial Evaluation (%) : </td>
                                <td><label id="txtweightPerF" ></label>
                                    <input type="hidden" value="QCBS" name="QcbsHdn" />
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <%}/*Condition for QCBS*/ } //} %>
                        </table>

                        <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {%>
                        <div align="right" class="b_space">
                            <a class="action-button-add" id="addRow">Add Phasing of Services</a><%--onclick="addRow('dataTable')"--%>
                            <a class="action-button-delete" id="delRow">Remove Phasing of Service</a><%--onclick="deleteRow('dataTable')"--%>
                        </div>

                        <table id="dataTable" width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="5%" class="t-align-left">Select</th>
                                <th width="15%" class="t-align-left">Ref. No. <span class="mandatory">*</span></th>
                                <th width="30%" class="t-align-left">Phasing of Service  <span class="mandatory">*</span></th>
                                <th width="20%" class="t-align-left">Location <span class="mandatory">*</span></th>
                                <th width="15%" class="t-align-left" >Indicative Contract Start<br /> Date   <span class="mandatory">*</span></th>
                                <th width="15%" class="t-align-left">Indicative Contract End<br /> Date   <span class="mandatory">*</span> </th>
                            </tr>
                            <tr <%--id="chk0"--%>>
                                <td class="t-align-left"><input class="formTxtBox_1" type="checkbox" name="chk_0" id="chk0"/></td>
                                <td class="t-align-left">
                                    <input name="refNo_0" type="text" class="formTxtBox_1" id="txtrefNo_0" onblur="chkRefNoBlank(this);" style="width: 95%;" />
                                    <span id="refno_0" class='reqF_1'></span>
                                </td>
                                <td class="t-align-left">
                                    <textarea onblur="chkPhaseSerBlank(this);"  style="width: 98%;" rows="5" id="txtaphasingService_0" name="phasingService_0" class="formTxtBox_1"></textarea>
                                    <span id="phaseSer_0" class='reqF_1'></span>
                                </td>
                                <td class="t-align-left">
                                    <input onblur="chkLocRefBlank(this);" name="locationRefNo_0" type="text" class="formTxtBox_1" id="txtlocationRefNo_0" style="width: 95%;" />
                                    <span id="locRef_0" class='reqF_1'></span>
                                </td>
                                <td class="t-align-left">
                                    <input onblur="chkIndStartBlank(this);findHoliday(this, 9);" name="indicativeStartDate_0" type="text" class="formTxtBox_1" id="txtindicativeStartDate_0" style="width:70px;" readonly="true"  onfocus="GetCalWithouTime('txtindicativeStartDate_0', 'txtindicativeStartDate_0');"/>
                                    <img id="txtindicativeStartDateimg_0" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithouTime('txtindicativeStartDate_0', 'txtindicativeStartDateimg_0');"/>
                                    <span id="indStart_0" class='reqF_1'></span>
                                </td>
                                <td class="t-align-left">
                                    <input onblur="chkIndCompBlank(this);findHoliday(this, 10);" name="indicativeComplDate_0" type="text" class="formTxtBox_1" id="txtindicativeComplDate_0" style="width:70px;" readonly="true" onfocus="GetCalWithouTime('txtindicativeComplDate_0', 'txtindicativeComplDate_0');" />
                                    <img id="txtindicativeComplDateimg_0" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick ="GetCalWithouTime('txtindicativeComplDate_0', 'txtindicativeComplDateimg_0');" />
                                    <span id="indComp_0" class='reqF_1'></span>
                                </td>
                            </tr>
                        </table>

                        <% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="lotDescription">
                            <tr>
                                <th width="6%" class="t-align-left">Lot No.</th>
                                    <% if (!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes") && obj.get(0)[2].toString().equalsIgnoreCase("Yes")) {
                                    %>
                                <th width="34%" class="t-align-left">Identification of Lot</th>
                                    <% } else if (!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")) {%>
                                <th width="44%" class="t-align-left">Identification of Lot</th>
                                    <% } else if (!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")) {%>
                                <th width="44%" class="t-align-left">Identification of Lot</th>
                                    <%} else {%>
                                <th width="50%" class="t-align-left">Identification of Lot</th>
                                    <% } %>
                                <th width="10%" class="t-align-center">Location <span class="mandatory">*</span></th>
                                    <% 
                                        if (!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")) {
                                            /*if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM") && !commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {*/
                                    %>
                                <!-- ICT Start Dohatec-->
                                <% if (commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")) {%>
                                <th width="20%" class="t-align-center">Bid Security (Amount in Nu.) and Type <span class="mandatory">*</span></th>
                                <th width="15%" class="t-align-center">Equivalent Bid Security (Amount in USD) <span class="mandatory">*</span></th>
                                <!-- <th width="5%" class="t-align-center">Foreign Currency (FC) <span class="mandatory">*</span></th>-->
                                <%}%>

                                <% if (commonTenderDetails.getProcurementType().equalsIgnoreCase("NCT")) {%>
                                <th width="20%" class="t-align-center">Bid Security (Amount in Nu.) and Type <span class="mandatory">*</span></th>

                                <%}%>
                                <!-- ICT end Dohatec-->
                                <!--<th width="10%" class="t-align-center">Tender/Proposal Security (Amount in BTN) <span class="mandatory">*</span></th>-->


                                <% }%>
                                <% if (!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")) { %>
                                <th width="10%" class="t-align-center" style="display:none" id="thDocsFeesAmount">Document Fees<br />(Amount in Nu.) <span class="mandatory">*</span></th>
                                    <% } %>
                                <th width="10%" class="t-align-center">Contract Start Date <span class="mandatory">*</span> </th>
                                <th width="10%" class="t-align-center">Contract End Date <span class="mandatory">*</span> </th>
                            </tr>
                            <% for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "lot")) {%>
                            <tr>
                                <td class="t-align-center"><label><%=commonTdetails.getLotNo()%></label> </td>
                                <td class="t-align-center"><label><%=commonTdetails.getLotDesc()%></label></td>
                                <td class="t-align-center"><input name="locationlot_<%=countI%>" type="text" style="width: 180px;" class="formTxtBox_1" id="locationlot_<%=countI%>" onblur="chkLocLotBlank(this);"/><span id="locLot_<%=countI%>" style="color: red;">&nbsp;</span></td>                                                             
                                    <%  //if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM") && commonTenderDetails.getEventType().equalsIgnoreCase("Tender")) {
                                        if (!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")) {
                                            /*if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM") && !commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {*/
                                    %>
                                    <!--<td class="t-align-center"><input name="tenderSecurityAmount_<%=countI%>" type="text" class="formTxtBox_1" id="tenderSecurityAmount_<%=countI%>" onblur="chkAmountLotBlank(this);" /><span id="amountLot_<%=countI%>" style="color: red;"></span></td>-->

                                <!-- ICT Start by Dohatec-->
                                <% if (commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")) {%>
                                <td class="t-align-center">
                                    <div><input name="tenderSecurityAmount_<%=countI%>" type="text" class="formTxtBox_1" id="tenderSecurityAmount_<%=countI%>"  onblur="chkAmountLotBlank(this);" style="width:185px;"/><span id="amountLot_<%=countI%>" style="color: red;"></span></div>
                                    <div style="text-align: left; padding: 9px 0 0 5px;"> 
                                    <input type="checkbox" value ="1" name = "finanDeclaration_<%=countI%>" id="chkfinanDeclaration_<%=countI%>"  onchange="chkmsgDeclaration(<%=countI%>,1);"/> Financial Institution Payment <br/>
                                    <input type="checkbox" value="2" name="bidSecDeclaration_<%=countI%>"  id="chkbidSecDeclaration_<%=countI%>" onchange="chkmsgDeclaration(<%=countI%>,2);"/> Bid Securing Declaration <br/>
                                    <span id="msgDeclaration_<%=countI%>" style="color: red;"></span>
                                    </div> 
                                </td>
                                <td class="t-align-center"><input name="tenderSecurityAmountUSD_<%=countI%>" type="text" class="formTxtBox_1" id="tenderSecurityAmountUSD_<%=countI%>"  onblur="chkAmountLotBlankUSD(this);" /><span id="amountLotUSD_<%=countI%>" style="color: red;"></span></td>

                                <!--<td class="t-align-center">
                                   <select name="cmbTndrCat" class="formTxtBox_1" id="cmbTndrCat">
                                             <option value="">Select Currency</option>
     
                                             <option value="Manufacturer">USD</option>
                                             <option value="Importer">GBP</option>
     
                                         </select>
     
                                </td>-->
                                <%}%>

                                <% if (commonTenderDetails.getProcurementType().equalsIgnoreCase("NCT")) {%>
                                <td class="t-align-center">
<!--                                    <select name="bidSecurityType" class="formTxtBox_1" id="cmbBidSecurity" style="width:200px; margin-bottom: 10px;" onChange="setSecurityType(this,<%=countI%>);">
                                        <option value = "">- Select Bid Security Type-</option>
                                        <option value = "1">Bid Security Amount</option>
                                        <option value = "2">Bid Securing Declaration</option>
                                        <option value = "3">Anyone</option>
                                    </select>                                 -->
                                    <div><input name="tenderSecurityAmount_<%=countI%>" type="text" class="formTxtBox_1" id="tenderSecurityAmount_<%=countI%>"  onblur="chkAmountLotBlank(this);" style="width:185px;"/><span id="amountLot_<%=countI%>" style="color: red;"></span></div>
                                   <div style="text-align: left; padding: 9px 0 0 10px;"> 
                                    <input type="checkbox" value ="1" name = "finanDeclaration_<%=countI%>" id="chkfinanDeclaration_<%=countI%>"  onchange="chkmsgDeclaration(<%=countI%>,1);"/> Financial Institution Payment <br/>
                                    <input type="checkbox" value="2" name="bidSecDeclaration_<%=countI%>"  id="chkbidSecDeclaration_<%=countI%>" onchange="chkmsgDeclaration(<%=countI%>,2);"/> Bid Securing Declaration <br/>
                                    <span id="msgDeclaration_<%=countI%>" style="color: red;"></span>
                                   </div> 
                                </td>

                                <%}%>

                                <!-- ICT End Dohatec-->
                                <% }%>
                                 <% if (!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")) {%>
                                <td class="t-align-center" style="display:none" id="tdDocFeeslot<%=countI%>"><input name="docFeeslot_<%=countI%>" type="text" class="formTxtBox_1" id="docFeeslot_<%=countI%>" onblur="chkDocFeesLotBlank(this);"/><span id="docFees_<%=countI%>" style="color: red;"></span></td>
                                    <% } %>
                                <td class="t-align-center">
                                    <input name="startLotNo_0" readonly="true" type="text" class="formTxtBox_1" id="startTimeLotNo_<%=countI%>" style="width:100px;" onfocus="GetCalWithouTime('startTimeLotNo_<%=countI%>', 'startTimeLotNo_<%=countI%>');" onblur="chkStartTimeLotBlank(this);findHoliday(this, 7);"/><span id="startLot_<%=countI%>" style="color: red;">&nbsp;</span>
                                    <img id="txtstartTimeLotNoimg_<%=countI%>"  src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithouTime('startTimeLotNo_<%=countI%>', 'txtstartTimeLotNoimg_<%=countI%>');"/>
                                </td>
                                <td class="t-align-center">
                                    <input name="complTimeLotNo_0" readonly="true" type="text" class="formTxtBox_1" id="complTimeLotNo_<%=countI%>" style="width:100px;" onfocus="GetCalWithouTime('complTimeLotNo_<%=countI%>', 'complTimeLotNo_<%=countI%>');" onblur="chkCompTimeLotBlank(this);findHoliday(this, 8);"/><span id="compLot_<%=countI%>" style="color: red;">&nbsp;</span>
                                    <img id="txtcomplTimeLotNoimg_<%=countI%>"  src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithouTime('complTimeLotNo_<%=countI%>', 'txtcomplTimeLotNoimg_<%=countI%>');"/>
                                    <input type="hidden" id="tenderLotSecId<%=countI%>" name="tenderLotSecId<%=countI%>" value="<%=commonTdetails.getTenderLotSecId()%>" />
                                </td>
                            </tr>
                            <%countI++;

                                }%>
                        </table>
                        <% }%>

                        <div class="tableHead_22 t_space">Procuring Agency Details :</div>
                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space" width="100%">
                            <tr>
                                <td class="ff" width="25%">Name of Official Inviting <% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%> Pre-Qualification<% }
                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%> Tender<% }
                                            if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%> REOI<% }
                                                                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%> RFP<% }%> :</td>
                                <td width="25%"><%=commonTenderDetails.getPeName()%></td>
                                <td class="ff" width="26%"> Designation :
                                    <% 
                                        //if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {
                                    %>      <!-- Pre-Qualification -->
                                    <% //}
                                        //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {
                                    %> <!-- Tender -->
                                    <% //}
                                         //if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                    %> <!-- REOI -->
                                    <% //}
                                          //if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {
                                    %> <!-- RFP -->
                                    <% //}
                                    %> <!-- : -->
                                </td>
                                <td width="25%"><%=commonTenderDetails.getPeDesignation()%></td>
                            </tr>

                            <tr>
                                <td class="ff">Official Address :
                                    <% 
                                        //if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) 
                                        //{
                                    %>      <!--Pre-Qualification --><% //}
                                        //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {
                                    %> <!-- Tender/Proposal -->
                                    <% //}
                                          //if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                    %> <!-- REOI -->
                                    <% //}
                                         //if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {
                                    %> <!-- RFP -->
                                    <% //}
                                    %> <!-- : --> 
                                </td>
                                <td><%
                                    if (commonTenderDetails.getPeAddress().contains("Thana")) {
                                        out.print(commonTenderDetails.getPeAddress().replace("Thana", "Gewog"));
                                    } else {
                                        out.print(commonTenderDetails.getPeAddress());
                                    }
                                    %></td>
                                <td class="ff">Contact details :<!-- of Official Inviting -->
                                    <% 
                                        //if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {
                                    %> <!-- Pre-Qualification -->
                                    <% //}
                                        //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {
                                    %> <!-- Tender/Proposal -->
                                    <% //}
                                        //if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                    %> <!-- REOI -->
                                    <% //}
                                        //if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {
                                    %> <!-- RFP -->
                                    <% //}
                                    %> <!-- : -->
                                </td>
                                <td><%=commonTenderDetails.getPeContactDetails()%>
                                </td>
                            </tr>
                            <% }%>
                            <tr>
                                <td colspan="4" style="text-align: center;" class="ff mandatory">The procuring agency reserves the right to accept or reject all Tenders / Pre-Qualifications / EOIs</td>
                            </tr>
                        </table>

                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space" width="100%">
                            <!--                        <tr>
                                                        <td colspan="4" align="center">
                                                            <div class="errorMsg"><span id="msgVal"></span></div>
                                                        </td>
                                                    </tr>-->
                            <tr>
                                <td colspan="4" align="center">
                                    <label class="formBtn_1"><input type="submit" name="submit" id="btnsubmit" value="Submit" onClick="return Validation();"/>
                                        <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/><!--rishita - button disable-->
                                    </label>&nbsp;&nbsp;
                                    <%--<label class="formBtn_1"><input type="submit" name="update" id="btnupdate" value="Update" /></label>--%>
                                    <input type="hidden" value="1" id="txtcounter" name="txtcounter"/>
                                    <input type="hidden" id="combineRefNo" name="combineRefNo"/>
                                    <input type="hidden" id="combinePOS" name="combinePOS"/>
                                    <input type="hidden" id="combineLocation" name="combineLocation"/>
                                    <input type="hidden" id="combineIndicativeStartDate" name="combineIndicativeStartDate" />
                                    <input type="hidden" id="combineIndicativeComplDate" name="combineIndicativeComplDate" />

                                    <input type="hidden" id="combineLocationLot" name="combineLocationLot" />
                                    <input type="hidden" id="combineTenderSecurityAmount" name="combineTenderSecurityAmount" />
                                    <input type="hidden" id="combineBidSecurityType" name="combineBidSecurityType" />
                                    <input type="hidden" id="combineTenderSecurityAmountUSD" name="combineTenderSecurityAmountUSD" />
                                    <input type="hidden" id="combineDocFeesLot" name="combineDocFeesLot" />
                                    <input type="hidden" id="combineComplTimeLotNo" name="combineComplTimeLotNo" />
                                    <input type="hidden" id="combineStartTimeLotNo" name="combineStartTimeLotNo" />
                                    <input type="hidden" id="combinetenderLotSecId" name="combinetenderLotSecId" />
                                    <input type="hidden" id="hdndoc" name="hdndoc" />
                                    <input type="hidden" id="hdndocprice" name="hdndocprice" value=""/>
                                    <input type="hidden" id="tenderId" value="<%=id%>" name="tenderId" />
                                    <input type="hidden" id="chkSecAmt" name="chkSecAmt" value="<%=msgSecAmt%>"/>
                                </td>
                            </tr>
                        </table>
                        <div>&nbsp;</div>
                    </form>
                    <!--Dashboard Content Part End-->
                    
                    
                    
                    <script type="text/javascript">//rishita - button disable
                        $(function () {
                            $('#frmCreateTender').submit(function () {
                                //alert($('#frmCreateTender').valid());
                                if ($('#frmCreateTender').valid()) {
                                    if ($('#btnsubmit') != null) {
                                        $('#btnsubmit').attr("disabled", "true");
                                        $('#hdnbutton').val("Submit");
                                    }
                                }
                            });
                        });
                    </script>
                    <script type="text/javascript">
                        var vbooltemp = true;
                        var lotno =<%=lotno%>;
                        var i = 0;
                        function documentAvailable() {
                            /*if(document.getElementById('lblEventType') !=null && document.getElementById('lblProcurementMethod') != null && lotno == "1"){//rishita
                             var eventType = document.getElementById('lblEventType').innerHTML;
                             var procurementMethod = document.getElementById('lblProcurementMethod').innerHTML;
                             if(eventType == 'TENDER' || eventType == 'PQ'){
                             //alert(procurementMethod);
                             if(procurementMethod.toString().toLowerCase() == 'otm'){
                             document.getElementById("docsprice").style.display = 'table-row';
                             }
                             }else if(eventType == '1 STAGE-TSTM'){
                             document.getElementById("docsprice").style.display = 'table-row';
                             }
                             }*/
                            //                            alert("lotno:: "+lotno);
                            //                            alert("event Type:: "+ document.getElementById('lblEventType').innerHTML);
                            //                            alert("Procurement method:: "+ document.getElementById('lblProcurementMethod').innerHTML);
                            //                            alert("pqTenderId:: "+ document.getElementById('pqTenderId').value);
                            //if(document.getElementById('lblEventType') !=null && lotno == "1"){//rishita - docprice
                            if (document.getElementById('lblEventType') != null) {//rishita - docprice
                                if (document.getElementById('lblEventType').innerHTML == 'PQ' && document.getElementById('docsprice') != null) {
                                    document.getElementById("docsprice").style.display = 'table-row';
                                }
                            }
                            //if(lotno == "1" && document.getElementById('lblProcurementMethod') != null && document.getElementById('pqTenderId') !=null){
                            if (document.getElementById('hdnProcurementMethod') != null && document.getElementById('pqTenderId') != null) {
                                if (document.getElementById('lblEventType').innerHTML == 'TENDER' && document.getElementById('hdnProcurementMethod').value.toString().toUpperCase() == 'OTM' && document.getElementById('pqTenderId').value == 0 && document.getElementById('docsprice') != null) {
                                    document.getElementById("docsprice").style.display = 'table-row';
                                }

                            }
                            //if( lotno == "1" && document.getElementById('lblEventType') !=null){
                            if (document.getElementById('lblEventType') != null) {
                                if (document.getElementById('lblEventType').innerHTML.toString().toUpperCase() == '1 STAGE-TSTM' && document.getElementById('docsprice') != null) {
                                    document.getElementById("docsprice").style.display = 'table-row';
                                }
                            }
                            if (document.getElementById('cmbdocAvailable') != null && document.getElementById('cmbdocAvailable').value != null) {
                                var docAvailable = document.getElementById('cmbdocAvailable').value;

                                if (docAvailable == 'Package') {
                                    if (document.getElementById('cmbdocFees') != null && document.getElementById('cmbdocFees').value != null) {
                                        document.getElementById('cmbdocFees').value = 'Package wise';
                                        if (document.getElementById('cmbdocFees').value == 'Package wise' && lotno == "1" && document.getElementById('docsprice') != null) {
                                            document.getElementById("docsprice").style.display = 'table-row';
                                        }
                                    }
                                } else {
                                }
                            } else {
                                //alert('rishita');
                            }
                            if ($('#cmbdocFees').html() != null) {
                                if ($('#cmbdocAvailable').val() == "Lot") {
                                    $('#cmbdocFees').html($('#cmbdocFees').html() + "<option value='Lot wise'>Lot wise</option>");
                                } else if ($('#cmbdocAvailable').val() == "Package") {
                                    changeDocumentsFees();
                                    $('#cmbdocFees').html("<option value='Package wise'>Package wise</option>");
                                }
                            }
                        }
                        function changeDocumentsFees() {//rajesh

                            if (document.getElementById('cmbdocFees') != null && document.getElementById('cmbdocFees').value != null) {
                                document.getElementById('spantxtpreQualDocPriceman').innerHTML = '';
                                if (document.getElementById('cmbdocFees').value == 'Package wise') {
                                    document.getElementById('hdndoc').value = 'Yes';
                                    document.getElementById('hdndocprice').value = 'Yes';
                                    //if(document.getElementById('docsprice') != null && lotno == '1'){
                                    if (document.getElementById('docsprice') != null) {
                                        document.getElementById('docsprice').style.display = 'table-row';
                                    }
                                    i = 0;
                                    for (i; i <= lotno; i++) {
                                        if (document.getElementById('tdDocFeeslot' + i) != null) {
                                            document.getElementById('tdDocFeeslot' + i).style.display = 'none';
                                        }
                                    }
                                    if (document.getElementById('thDocsFeesAmount') != null) {
                                        document.getElementById('thDocsFeesAmount').style.display = 'none';
                                    }

                                } else if (document.getElementById('cmbdocFees').value == 'Lot wise') {
                                    document.getElementById('hdndoc').value = 'No';
                                    document.getElementById('hdndocprice').value = 'No';
                                    if (document.getElementById('docsprice') != null) {
                                        document.getElementById('docsprice').style.display = 'none';
                                    }
                                    if (document.getElementById('thDocsFeesAmount') != null) {
                                        document.getElementById('thDocsFeesAmount').style.display = 'table-cell';
                                    }
                                    i = 0;
                                    for (i; i <= lotno; i++) {
                                        if (document.getElementById('tdDocFeeslot' + i) != null) {
                                            document.getElementById('tdDocFeeslot' + i).style.display = 'table-cell';
                                        }
                                    }
                                }
                            }
                        }
                    </script>

                    <script type="text/javascript">
                        function GetCal(txtname, controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: 24,
                                onSelect: function () {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                    if(txtname=="txttenderpublicationDate"||txtname=="txtpreQualCloseDate")
                                    {
                                        ShowPreBidMeetingDates();
                                    }
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                            
                        }
                        function GetCalWithouTime(txtname, controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: false,
                                dateFormat: "%d/%m/%Y",
                                onSelect: function () {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                        }    
                        
                        function chkmsgDeclaration(id,action){
                            if (document.getElementById("chkfinanDeclaration_"+id).checked == false && document.getElementById("chkbidSecDeclaration_"+id).checked == false) {
                                document.getElementById("msgDeclaration_"+id).innerHTML='Please select Bid Declaration Type';
                            }  
                            else{
                                if (action == 1)
                                    document.getElementById("chkbidSecDeclaration_"+id).checked = false;       
                                if (action == 2)
                                    document.getElementById("chkfinanDeclaration_"+id).checked = false;
                               
                                document.getElementById("msgDeclaration_"+id).innerHTML='';
                            }
                            
                        }
//                        function setSecurityType(obj,count){                         
//                            if ("" == obj.options[obj.selectedIndex].value) {
//                                $('#tenderSecurityAmount_'+count).hide();
//                                $('#chkdeclaration').hide();
//                            }
//                            else if ("1" == obj.options[obj.selectedIndex].value){
//                                $('#chkdeclaration').hide();
//                                $('#tenderSecurityAmount_'+count).show();
//                            }
//                            else if ("2" == obj.options[obj.selectedIndex].value){
//                                $('#chkdeclaration').show();
//                                $('#tenderSecurityAmount_'+count).hide();
//                            }
//                            else if ("3" == obj.options[obj.selectedIndex].value){
//                                $('#chkdeclaration').show();
//                                $('#tenderSecurityAmount_'+count).show();
//                            }  
//                        }
                    </script>

                    <%--Start -- This is done by Rajesh--%>
                    <script type="text/javascript">

                        //Function for Required
                        function required(controlid)
                        {
                            var temp = controlid.length;
                            if (temp <= 0) {
                                return false;
                            } else {
                                return true;
                            }
                        }

                        //Function for MaxLength
                        function Maxlenght(controlid, maxlenght)
                        {
                            var temp = controlid.length;
                            if (temp >= maxlenght) {
                                return false;
                            } else
                                return true;
                        }

                        //Function for digits
                        function digits(control) {
                            return /^\d+$/.test(control);
                        }

                        function CompareToForEqual(value, params)
                        {

                            var mdy = value.split('/')  //Date and month split
                            var mdyhr = mdy[2].split(' ');  //Year and time split
                            var mdyp = params.split('/')
                            var mdyphr = mdyp[2].split(' ');
                            var date = new Date(mdyhr[0], mdy[1], mdy[0]);
                            var datep = new Date(mdyphr[0], mdyp[1], mdyp[0]);
                            return Date.parse(date) == Date.parse(datep);
                        }

                        //Function for CompareToForToday
                        function CompareToForToday(first)
                        {
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr = mdy[2].split(' ');  //Year and time split
                            var mdyhrtime = mdyhr[1].split(':');
                            if (mdyhrtime[1] == undefined) {
                                var valuedate = new Date(mdyhr[0], mdy[1] - 1, mdy[0]);
                            } else
                            {
                                var valuedate = new Date(mdyhr[0], mdy[1] - 1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }

                            var d = new Date();
                            if (mdyhrtime[1] == undefined) {
                                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                            } else
                            {
                                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes());
                            }
                            return Date.parse(valuedate) > Date.parse(todaydate);
                        }
                        function CompareHoliday(first)
                        {
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var mdyhrtime=mdyhr[1].split(':');
                            if(mdyhrtime[1] == undefined){
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            }else
                            {
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }
                            var result = valuedate.toString();
                            return result.split(" ")[0];
                        }
                        function findHoliday3(compVal) {
                            //var compVal = comp.value;
                            var cnt = 0;
                            if (compVal != null && compVal != "") {
                                for (var i = 0; i < holiArray.length; i++) {
                                    if (CompareToForEqual(holiArray[i], compVal)) {
                                        cnt++;
                                    }
                                }
                                
                            
                                
                            }
                            
                            if (cnt != 0) {
                                return true;
                            }
                            else 
                            {
                                return false;
                            }
                            
                        }
                        function publicationHoliday()
                        {
                            var first = document.getElementById('txttenderpublicationDate').value;
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var mdyhrtime=mdyhr[1].split(':');
                            if(mdyhrtime[1] == undefined){
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            }else
                            {
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }
                            var result = valuedate.toString();
                            var result2 = result.split(" ")[0];
                            if(result2=="Sat" || result2=="Sun")
                            {
                                document.getElementById("demoPublication").innerHTML = "Weekend!";
                            }
                            else
                            {
                                document.getElementById("demoPublication").innerHTML = "";
                            }
                        }
                        
                        
                        
                        function openCloseWeekend()
                        {
                            var first = document.getElementById('txtpreQualCloseDate').value;
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var mdyhrtime=mdyhr[1].split(':');
                            if(mdyhrtime[1] == undefined){
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            }else
                            {
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }
                            var result = valuedate.toString();
                            var result2 = result.split(" ")[0];
                            if(result2=="Sat" || result2=="Sun")
                            {
                                document.getElementById("demoClose").innerHTML = "Weekend!";
                                document.getElementById("demoOpen").innerHTML = "Weekend!";
                            }
                            else
                            {
                                document.getElementById("demoClose").innerHTML = "";
                                document.getElementById("demoOpen").innerHTML = "";
                            }
                        }
                        
                        
                        
                        function docSellHoliday()
                        {
                            var first = document.getElementById('txttenderLastSellDate').value;
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var mdyhrtime=mdyhr[1].split(':');
                            if(mdyhrtime[1] == undefined){
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            }else
                            {
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }
                            var result = valuedate.toString();
                            var result2 = result.split(" ")[0];
                            if(result2=="Sat" || result2=="Sun")
                            {
                                document.getElementById("demoDocSell").innerHTML = "Weekend!";
                            }
                            else
                            {
                                document.getElementById("demoDocSell").innerHTML = "";
                            }
                        }
                        function secSubHoliday()
                        {
                            var first = document.getElementById('txtlastDateTenderSub').value;
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var mdyhrtime=mdyhr[1].split(':');
                            if(mdyhrtime[1] == undefined){
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            }else
                            {
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }
                            var result = valuedate.toString();
                            var result2 = result.split(" ")[0];
                            if(result2=="Sat" || result2=="Sun")
                            {
                                document.getElementById("demoSecSub").innerHTML = "Weekend!";
                            }
                            else
                            {
                                document.getElementById("demoSecSub").innerHTML = "";
                            }
                        }

                        function meetStartHoliday()
                        {
                            var first = document.getElementById('txtpreTenderMeetStartDate').value;
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var mdyhrtime=mdyhr[1].split(':');
                            if(mdyhrtime[1] == undefined){
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            }else
                            {
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }
                            var result = valuedate.toString();
                            var result2 = result.split(" ")[0];
                            if(result2=="Sat" || result2=="Sun")
                            {
                                document.getElementById("demoMeetStart").innerHTML = "Weekend!";
                            }
                            else
                            {
                                document.getElementById("demoMeetStart").innerHTML = "";
                            }
                        }

                        function meetEndHoliday()
                        {
                            var first = document.getElementById('txtpreTenderMeetEndDate').value;
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var mdyhrtime=mdyhr[1].split(':');
                            if(mdyhrtime[1] == undefined){
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            }else
                            {
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }
                            var result = valuedate.toString();
                            var result2 = result.split(" ")[0];
                            if(result2=="Sat" || result2=="Sun")
                            {
                                document.getElementById("demoMeetEnd").innerHTML = "Weekend!";
                            }
                            else
                            {
                                document.getElementById("demoMeetEnd").innerHTML = "";
                            }
                        }

                        //Function for CompareToForGreater
                        function CompareToForGreater(value, params)
                        {
                            if (value != '' && params != '') {

                                var mdy = value.split('/')  //Date and month split
                                var mdyhr = mdy[2].split(' ');  //Year and time split
                                var mdyp = params.split('/')
                                var mdyphr = mdyp[2].split(' ');


                                if (mdyhr[1] == undefined && mdyphr[1] == undefined)
                                {
                                    //alert('Both Date');
                                    var date = new Date(mdyhr[0], mdy[1], mdy[0]);
                                    var datep = new Date(mdyphr[0], mdyp[1], mdyp[0]);
                                } else if (mdyhr[1] != undefined && mdyphr[1] != undefined)
                                {
                                    //alert('Both DateTime');
                                    var mdyhrsec = mdyhr[1].split(':');
                                    var date = new Date(mdyhr[0], parseFloat(mdy[1]) - 1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var mdyphrsec = mdyphr[1].split(':');

                                    var datep = new Date(mdyphr[0], parseFloat(mdyp[1]) - 1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                } else
                                {
                                    //alert('one Date and One DateTime');
                                    var a = mdyhr[1];  //time
                                    var b = mdyphr[1]; // time

                                    if (a == undefined && b != undefined)
                                    {
                                        //alert('First Date');
                                        var date = new Date(mdyhr[0], mdy[1], mdy[0], '00', '00');
                                        var mdyphrsec = mdyphr[1].split(':');
                                        var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                    } else
                                    {
                                        //alert('Second Date');
                                        var mdyhrsec = mdyhr[1].split(':');
                                        var date = new Date(mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                        var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], '00', '00');
                                    }
                                }
                                return Date.parse(date) > Date.parse(datep);
                            } else
                            {
                                return false;
                            }
                        }

                        //Function for CompareToForGreater
                        function CompareToForSmaller(value, params)
                        {
                            if (value != '' && params != '') {

                                var mdy = value.split('/')  //Date and month split
                                var mdyhr = mdy[2].split(' ');  //Year and time split
                                var mdyp = params.split('/')
                                var mdyphr = mdyp[2].split(' ');


                                if (mdyhr[1] == undefined && mdyphr[1] == undefined)
                                {
                                    //alert('Both Date');
                                    var date = new Date(mdyhr[0], mdy[1], mdy[0]);
                                    var datep = new Date(mdyphr[0], mdyp[1], mdyp[0]);
                                } else if (mdyhr[1] != undefined && mdyphr[1] != undefined)
                                {
                                    //alert('Both DateTime');
                                    var mdyhrsec = mdyhr[1].split(':');
                                    var date = new Date(mdyhr[0], parseFloat(mdy[1]) - 1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var mdyphrsec = mdyphr[1].split(':');
                                    var datep = new Date(mdyphr[0], parseFloat(mdyp[1]) - 1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                } else
                                {
                                    //alert('one Date and One DateTime');
                                    var a = mdyhr[1];  //time
                                    var b = mdyphr[1]; // time

                                    if (a == undefined && b != undefined)
                                    {
                                        //alert('First Date');
                                        var date = new Date(mdyhr[0], mdy[1], mdy[0], '00', '00');
                                        var mdyphrsec = mdyphr[1].split(':');
                                        var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                    } else
                                    {
                                        //alert('Second Date');
                                        var mdyhrsec = mdyhr[1].split(':');
                                        var date = new Date(mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                        var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], '00', '00');
                                    }
                                }

                                return Date.parse(date) < Date.parse(datep);
                            } else
                            {
                                return false;
                            }
                        }

                        //Function for CompareToWithoutEqual
                        function CompareToWithoutEqual(value, params)
                        {
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr = mdy[2].split(' ');  //Year and time split
                            var mdyp = params.split('/')
                            var mdyphr = mdyp[2].split(' ');


                            if (mdyhr[1] == undefined && mdyphr[1] == undefined)
                            {
                                //alert('Both Date');
                                var date = new Date(mdyhr[0], mdy[1], mdy[0]);
                                var datep = new Date(mdyphr[0], mdyp[1], mdyp[0]);
                            } else if (mdyhr[1] != undefined && mdyphr[1] != undefined)
                            {
                                //alert('Both DateTime');
                                var mdyhrsec = mdyhr[1].split(':');
                                var date = new Date(mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                var mdyphrsec = mdyphr[1].split(':');
                                var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                            } else
                            {
                                //alert('one Date and One DateTime');
                                var a = mdyhr[1];  //time
                                var b = mdyphr[1]; // time

                                if (a == undefined && b != undefined)
                                {
                                    //alert('First Date');
                                    var date = new Date(mdyhr[0], mdy[1], mdy[0], '00', '00');
                                    var mdyphrsec = mdyphr[1].split(':');
                                    var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                } else
                                {
                                    //alert('Second Date');
                                    var mdyhrsec = mdyhr[1].split(':');
                                    var date = new Date(mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], '00', '00');
                                }
                            }
                            return Date.parse(date) > Date.parse(datep);
                        }
                        //

                    </script>
                    <%--End -- This is done by Rajesh--%>



                    <%--Start -- This is done by Rajesh--%>
                    <script type="text/javascript">
                        
                        
                        
                        function setBidderCategory(obj) {
                if (document.getElementById('ckhAll').checked == true) {
                    document.getElementById('chkW1').checked = true;
                    document.getElementById('chkW2').checked = true;
                    document.getElementById('chkW3').checked = true;
                    document.getElementById('chkW4').checked = true;
                } else {
                    if ("ckhAll" == obj.id) {
                        document.getElementById('chkW1').checked = false;
                        document.getElementById('chkW2').checked = false;
                        document.getElementById('chkW3').checked = false;
                        document.getElementById('chkW4').checked = false;
                    }
                }
            }
            
                        
                        

                        function numeric(value) {
                            return /^\d+$/.test(value);
                        }

                        function chkRefNoBlank(obj) {
                            var boolcheck1 = 'true';
                            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                            if (obj.value != '' && obj.value.charAt(0) != ' ') {
                                if (obj.value.length <= 50)
                                {
                                    document.getElementById("refno_" + i).innerHTML = "";
                                    boolcheck1 = 'true';
                                } else
                                {
                                    document.getElementById("refno_" + i).innerHTML = "<br/>Maximum 50 characters are allowed";
                                    boolcheck1 = false;
                                }
                            } else {
                                document.getElementById("refno_" + i).innerHTML = "<br/>Please enter Ref. No";
                                boolcheck1 = false;
                            }

                            if (boolcheck1 == false) {
                                document.getElementById("boolcheck").value = boolcheck1;
                            } else
                            {
                                document.getElementById("boolcheck").value = 'true';
                            }
                        }
                        function chkPhaseSerBlank(obj) {
                            var boolcheck2 = 'true';
                            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                            if (obj.value != '' && obj.value.charAt(0) != ' ')
                            {
                                if (obj.value.length <= 500)
                                {
                                    document.getElementById("phaseSer_" + i).innerHTML = "";
                                    boolcheck2 = 'true';
                                } else
                                {
                                    document.getElementById("phaseSer_" + i).innerHTML = "<br/>Maximum 500 characters are allowed";
                                    boolcheck2 = false;
                                }
                            } else {
                                document.getElementById("phaseSer_" + i).innerHTML = "<br/>Please enter Phasing of service";
                                boolcheck2 = false;
                            }
                            if (boolcheck2 == false) {
                                document.getElementById("boolcheck").value = boolcheck2;
                            } else
                            {
                                document.getElementById("boolcheck").value = 'true';
                            }
                        }
                        function chkLocRefBlank(obj) {
                            var boolcheck4 = 'true';

                            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                            if (obj.value != '') {
                                if (obj.value.length <= 100)
                                {
                                    document.getElementById("locRef_" + i).innerHTML = "";
                                    boolcheck4 = 'true';
                                } else
                                {
                                    document.getElementById("locRef_" + i).innerHTML = "<br/>Maximum 100 characters are allowed";
                                    boolcheck4 = false;
                                }
                            } else {
                                document.getElementById("locRef_" + i).innerHTML = "<br/>Please enter Location";
                                boolcheck4 = false;
                            }
                            if (boolcheck4 == false) {
                                document.getElementById("boolcheck").value = boolcheck4;
                            } else
                            {
                                document.getElementById("boolcheck").value = 'true';
                            }
                        }
                        function chkIndStartBlank(obj) {
                            vbooltemp = true;
                            var boolcheck5 = 'true';
                            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                            var closeDate = document.getElementById("txtpreQualCloseDate").value;
                            var year = parseInt(closeDate.split('/')[2]);
                            var month = (closeDate.split('/')[1]);
                            var day = (closeDate.split('/')[0]);
                            var date = new Date(year, (month - 1), day);

                            var objdate = obj.value;
                            var yearobj = parseInt(objdate.split('/')[2]);
                            var monthobj = (objdate.split('/')[1]);
                            var dayobj = (objdate.split('/')[0]);
                            var dateobj = new Date(yearobj, (monthobj - 1), dayobj);

                            if (obj.value != '') {
                                if (Date.parse(dateobj) > Date.parse(date))
                                {
                                    document.getElementById("indStart_" + i).innerHTML = "";
                                    boolcheck5 = 'true';
                                } else
                                {
                                    document.getElementById("indStart_" + i).innerHTML = "<br/>Indicative Contract Start Date must be greater than EOI Closing Date and Time";
                                    boolcheck5 = false;
                                }
                            } else {
                                document.getElementById("indStart_" + i).innerHTML = "<br/>Please enter Indicative Contract Start Date";
                                boolcheck5 = false;
                            }
                            if (boolcheck5 == false) {
                                document.getElementById("boolcheck").value = boolcheck5;
                                vbooltemp = false;
                            } else
                            {
                                document.getElementById("boolcheck").value = 'true';
                                vbooltemp = true;
                            }
                        }
                        function chkIndCompBlank(obj) {
                            var boolcheck6 = 'true';
                            vbooltemp = true;
                            var i = (obj.id.substr(obj.id.indexOf("_") + 1));

                            if (obj.value != '') {
                                if (CompareToForGreater(obj.value, document.getElementById("txtindicativeStartDate_" + i).value))
                                {
                                    document.getElementById("indComp_" + i).innerHTML = "";
                                    boolcheck6 = 'true';
                                } else
                                {
                                    document.getElementById("indComp_" + i).innerHTML = "<br/>Indicative Contract End Date must be greater than Indicative Contract Start Date";
                                    boolcheck6 = false;
                                }
                            } else {
                                document.getElementById("indComp_" + i).innerHTML = "<br/>Please enter Indicative Contract End Date";
                                boolcheck6 = false;
                            }

                            if (boolcheck6 == false) {
                                document.getElementById("boolcheck").value = boolcheck6;
                                vbooltemp = false;
                            } else
                            {
                                document.getElementById("boolcheck").value = 'true';
                                vbooltemp = true;
                            }

                        }
                        function chkLocLotBlank(obj) {
                            var boolcheck7 = 'true';
                            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                            if (obj.value != '') {
                                if (obj.value.length <= 100)
                                {
                                    document.getElementById("locLot_" + i).innerHTML = "";
                                    boolcheck7 = 'true';
                                } else
                                {
                                    document.getElementById("locLot_" + i).innerHTML = "<br/>Maximum 100 characters are allowed";
                                    boolcheck7 = false;
                                }
                            } else {
                                document.getElementById("locLot_" + i).innerHTML = "<br/>Please enter Location";
                                boolcheck7 = false;
                            }
                            if (boolcheck7 == false) {
                                document.getElementById("boolcheck").value = boolcheck7;
                            } else
                            {
                                document.getElementById("boolcheck").value = 'true';
                            }
                        }
                        //Start Dohatec for ICT
                        function chkAmountLotBlank(obj) {
                            var boolcheck8 = 'true';
                            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                            if (obj.value != '')
                            {
                                if (numeric(obj.value))
                                {
                                    document.getElementById("amountLot_" + i).innerHTML = "";
                                    obj.value = ($.trim(obj.value) * 1);
                                    boolcheck8 = 'true';
                                    tenderSecAmt(obj);

                                    if (obj.value == '0')
                                    {
                                        document.getElementById("tenderSecurityAmountUSD_" + i).value = '0';
                                        document.getElementById("tenderSecurityAmountUSD_" + i).disabled = 'true';
                                        document.getElementById("amountLotUSD_" + i).innerHTML = "";
                                        $(".tenderSecAmtInWordsUSD_" + i).remove();
                                    } else
                                    {
                                        if (document.getElementById("tenderSecurityAmountUSD_" + i).disabled)
                                        {
                                            document.getElementById("tenderSecurityAmountUSD_" + i).removeAttribute("disabled");
                                            document.getElementById("tenderSecurityAmountUSD_" + i).value = '';
                                            document.getElementById("amountLotUSD_" + i).innerHTML = "";
                                            $(".tenderSecAmtInWordsUSD_" + i).remove();
                                        }
                                    }
                                } else
                                {
                                    document.getElementById("amountLot_" + i).innerHTML = "<br/>Please enter Numeric Data";
                                    var temp = obj.id.split("_");
                                    var countTSA = temp[1];
                                    $(".tenderSecAmtInWords_" + i).remove();
                                    boolcheck8 = false;
                                }

                            } else {
                                document.getElementById("amountLot_" + i).innerHTML = "<br/>Please enter Bid Security Amount";
                                var temp = obj.id.split("_");
                                var countTSA = temp[1];
                                $(".tenderSecAmtInWords_" + i).remove();
                                boolcheck8 = false;
                            }
                            if (boolcheck8 == false) {
                                document.getElementById("boolcheck").value = boolcheck8;
                            } else
                            {
                                document.getElementById("boolcheck").value = 'true';
                            }
                        }

                        function chkAmountLotBlankUSD(obj) {
                            var boolcheckUSD = 'true';
                            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                            if (obj.value != '')
                            {
                                if (numeric(obj.value))
                                {
                                    document.getElementById("amountLotUSD_" + i).innerHTML = "";
                                    /*if(obj.value!='0')
                                     {*/
                                    obj.value = ($.trim(obj.value) * 1);
                                    boolcheckUSD = 'true';
                                    tenderSecAmtUSD(obj);
                                    /*}
                                     else
                                     {
                                     boolcheck7='false';
                                     document.getElementById("amountLotUSD_"+i).innerHTML = "<br/>Only 0 value is not allowed";
                                     $(".tenderSecAmtInWordsUSD_" + i).remove();
                                     }*/
                                } else
                                {
                                    document.getElementById("amountLotUSD_" + i).innerHTML = "<br/>Please enter Numeric Data";
                                    var temp = obj.id.split("_");
                                    var countTSA = temp[1];
                                    $(".tenderSecAmtInWordsUSD_" + i).remove();
                                    boolcheckUSD = 'false';
                                }

                            } else {
                                document.getElementById("amountLotUSD_" + i).innerHTML = "<br/>Please enter Bid Security Amount";
                                var temp = obj.id.split("_");
                                var countTSA = temp[1];
                                $(".tenderSecAmtInWordsUSD_" + i).remove();
                                boolcheckUSD = 'false';
                            }

                            if (boolcheckUSD == 'false') {
                                document.getElementById("boolcheck").value = boolcheckUSD;
                            } else
                            {
                                document.getElementById("boolcheck").value = boolcheckUSD;
                            }
                        }
                        //Start Dohatec for ICT

                        function chkAmountSecurity(msg) {
                            var boolcheck8 = 'true';
                            var obj = document.getElementById("txtsecurityAmountService_0");
                            //var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if (document.getElementById("txtsecurityAmountService_0").value != '')
                            {
                                if (document.getElementById("txtsecurityAmountService_0").value > 0) {
                                    if (numeric(document.getElementById("txtsecurityAmountService_0").value))
                                    {
                                        document.getElementById("amountLotService").innerHTML = "";
                                        boolcheck8 = 'true';
                                        tenderSecAmt(document.getElementById("txtsecurityAmountService_0"));
                                    } else
                                    {
                                        document.getElementById("amountLotService").innerHTML = "<br/>Please enter Numeric Data";
                                        //var temp = obj.id.split("_");
                                        //var countTSA = temp[1];
                                        $(".tenderSecAmtInWords_0").remove();
                                        $(".tenderSecAmtInWords_0").remove();
                                        boolcheck8 = false;
                                    }
                                } else {
                                    //document.getElementById('amountLotService').innerHTML = '<div class="reqF_1">Only 0 value is not allowed</div>';
                                    //var temp = obj.id.split("_");
                                    //var countTSA = temp[1];
                                    //$(".tenderSecAmtInWords_" + countTSA).remove();
                                    //boolcheck8 = false;
                                }
                            } else {
                                document.getElementById("amountLotService").innerHTML = "<br/>" + msg;
                                var temp = obj.id.split("_");
                                var countTSA = temp[1];
                                $(".tenderSecAmtInWords_" + countTSA).remove();
                                boolcheck8 = false;
                            }
                            if (boolcheck8 == false) {
                                document.getElementById('combineTenderSecurityAmount').value = '';
                                document.getElementById("boolcheck").value = boolcheck8;
                            } else
                            {
                                document.getElementById('combineTenderSecurityAmount').value = obj.value;
                                document.getElementById("boolcheck").value = 'true';
                            }
                        }
                        function chkDocFeesLotBlank(obj) {
                            var boolcheck9 = 'true';
                            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                            if (!regForSpace(obj.value) && obj.value != '')
                            {
                                if (numeric(obj.value))
                                {
                                    document.getElementById("docFees_" + i).innerHTML = "";
                                    boolcheck9 = 'true';
                                    docFeesWord(obj);
                                } else
                                {
                                    document.getElementById("docFees_" + i).innerHTML = "<br/>Please Enter Numeric Data";
                                    var temp = obj.id.split("_");
                                    var countDF = temp[1];
                                    $(".docFeesInWords_" + countDF).remove();
                                    var temp = obj.id.split("_");
                                    var countDF = temp[1];
                                    $(".docFeesInWords_" + countDF).remove();
                                    boolcheck9 = false;
                                }

                            } else {
                                document.getElementById("docFees_" + i).innerHTML = "<br/>Please enter Document fees amount in Tk";
                                var temp = obj.id.split("_");
                                var countDF = temp[1];
                                $(".docFeesInWords_" + countDF).remove();
                                boolcheck9 = false;
                            }
                            if (boolcheck9 == false) {
                                document.getElementById("boolcheck").value = boolcheck9;
                            } else
                            {
                                document.getElementById("boolcheck").value = 'true';
                            }
                        }
                        function chkCompTimeLotBlank(obj) {
                            var boolcheck10 = 'true';
                            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                            if (obj.value != '') {
                                if (obj.value.length <= 100)
                                {
                                    document.getElementById('compLot_' + i).innerHTML = '';
                                    boolcheck10 = 'true';
                                } else
                                {
                                    document.getElementById('compLot_' + i).innerHTML = '<br/>Maximum 100 characters are allowed';
                                    boolcheck10 = false;
                                }
                            } else {
                                //document.getElementById('compLot_'+i).innerHTML = '<br/>Please enter Completion Date';
                                //boolcheck10=false;
                            }
                            if (boolcheck10 == false) {
                                document.getElementById('boolcheck').value = boolcheck10;
                            } else
                            {
                                document.getElementById("boolcheck").value = 'true';
                            }
                        }
                        function chkStartTimeLotBlank(obj) {
                            var boolcheck11 = 'true';
                            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                            if (obj.value != '') {
                                if (obj.value.length <= 100)
                                {
                                    document.getElementById('startLot_' + i).innerHTML = '';
                                    boolcheck11 = 'true';
                                } else
                                {
                                    document.getElementById('startLot_' + i).innerHTML = '<br/>Maximum 100 characters are allowed';
                                    boolcheck11 = false;
                                }
                            } else {
                                //document.getElementById('startLot_'+i).innerHTML = '<br/>Please enter Start Date';
                                //boolcheck11=false;
                            }
                            if (boolcheck11 == false) {
                                document.getElementById('boolcheck').value = boolcheck11;
                            } else
                            {
                                document.getElementById("boolcheck").value = 'true';
                            }
                            return boolcheck11;
                        }




                        //end
                    </script>

                    <script type="text/javascript">
                        $(function () {
                            var counter = document.getElementById("txtcounter").value;
                            $("#addRow").click(function () {
                                //var newTxt = '<tr><td class="t-align-center"><input class="formTxtBox_1" type="checkbox" name="chk'+ counter+ ' " id="chk'+ counter+ ' "/></td><td class="t-align-center"><input name="refNo'+ counter+ '" type="text" class="formTxtBox_1" id="txtrefNo'+ counter+ '"/></td><td class="t-align-center"><textarea cols="50" rows="5" id="txtaphasingService'+ counter+ '" name="phasingService'+ counter+ '" class="formTxtBox_1"></textarea></td><td class="t-align-center"><input name="locationRefNo'+ counter+ '" type="text" class="formTxtBox_1" id="txtlocationRefNo'+ counter+ '" /></td><td class="t-align-center"><input name="indicativeStartDate'+ counter+'" type="text" class="formTxtBox_1" id="txtindicativeStartDate'+ counter+'" style="width:70px;" readonly="true"  onfocus="GetCal(txtindicativeStartDate'+ counter+',txtindicativeStartDate'+ counter+');"/><img id="txtindicativeStartDateimg'+ counter+'" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick="GetCal(txtindicativeStartDate'+ counter+',txtindicativeStartDateimg'+ counter+');"/></td><td class="t-align-center"><input name="indicativeComplDate'+ counter+'" type="text" class="formTxtBox_1" id="txtindicativeComplDate'+ counter+'" style="width:70px;" readonly="true" onfocus="GetCal(txtindicativeComplDate'+ counter+',txtindicativeComplDate'+ counter+');" /><img id="txtindicativeComplDateimg'+ counter+'" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal(txtindicativeComplDate'+ counter+',txtindicativeComplDateimg'+ counter+');" /></td></tr>';
                                //var newTxt = '<tr><td class="t-align-center"><input class="formTxtBox_1" type="checkbox" name="chk'+ counter+ ' " id="chk'+ counter+ ' "/></td><td class="t-align-center"><input name="refNo'+ counter+ '" type="text" class="formTxtBox_1" id="txtrefNo'+ counter+ '"/></td><td class="t-align-center"><textarea cols="50" rows="5" id="txtaphasingService'+ counter+ '" name="phasingService'+ counter+ '" class="formTxtBox_1"></textarea></td><td class="t-align-center"><input name="locationRefNo'+ counter+ '" type="text" class="formTxtBox_1" id="txtlocationRefNo'+ counter+ '" /></td><td class="t-align-center"><input name="indicativeStartDate'+ counter+'" type="text" class="formTxtBox_1" id="txtindicativeStartDate'+ counter+'" style="width:70px;" readonly="true"  onfocus="GetCalWithouTime(\'txtindicativeStartDate'+ counter+'\',\'txtindicativeStartDate'+ counter+'\');"/><img id="txtindicativeStartDateimg'+ counter+'" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick="GetCalWithouTime(\'txtindicativeStartDate'+ counter+'\',\'txtindicativeStartDateimg'+ counter+'\');"/></td><td class="t-align-center"><input name="indicativeComplDate'+ counter+'" type="text" class="formTxtBox_1" id="txtindicativeComplDate'+ counter+'" style="width:70px;" readonly="true" onfocus="GetCalWithouTime(\'txtindicativeComplDate'+ counter+'\',\'txtindicativeComplDate'+ counter+'\');" /><img id="txtindicativeComplDateimg'+ counter+'" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCalWithouTime(\'txtindicativeComplDate'+ counter+'\',\'txtindicativeComplDateimg'+ counter+'\');" /></td></tr>';
                                var newTxt = '<tr><td class="t-align-left"><input class="formTxtBox_1" type="checkbox" name="chk' + counter + ' " id="chk' + counter + ' "/></td><td class="t-align-left"><input style="width: 95%;" name="refNo_' + counter + '" type="text" class="formTxtBox_1" id="txtrefNo_' + counter + '" onBlur="chkRefNoBlank(this);"/><span id="refno_' + counter + '" style="color: red;">&nbsp;</span></td><td class="t-align-left"><textarea cols="50" style="width: 98%;" rows="5" id="txtaphasingService_' + counter + '" name="phasingService_' + counter + '" class="formTxtBox_1" onBlur="chkPhaseSerBlank(this);"></textarea><span id="phaseSer_' + counter + '" style="color: red;">&nbsp;</span></td><td class="t-align-left"><input style="width: 95%;" name="locationRefNo_' + counter + '" type="text" class="formTxtBox_1" id="txtlocationRefNo_' + counter + '" onBlur="chkLocRefBlank(this);"/><span id="locRef_' + counter + '" style="color: red;">&nbsp;</span></td><td class="t-align-left"><input name="indicativeStartDate_' + counter + '" type="text" class="formTxtBox_1" id="txtindicativeStartDate_' + counter + '" style="width:70px;" readonly="true"  onfocus="GetCalWithouTime(\'txtindicativeStartDate_' + counter + '\',\'txtindicativeStartDate_' + counter + '\');" onBlur="chkIndStartBlank(this);"/><img id="txtindicativeStartDateimg_' + counter + '" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick="GetCalWithouTime(\'txtindicativeStartDate_' + counter + '\',\'txtindicativeStartDateimg_' + counter + '\');"/><span id="indStart_' + counter + '" style="color: red;">&nbsp;</span></td><td class="t-align-left"><input name="indicativeComplDate_' + counter + '" type="text" class="formTxtBox_1" id="txtindicativeComplDate_' + counter + '" style="width:70px;" readonly="true" onfocus="GetCalWithouTime(\'txtindicativeComplDate_' + counter + '\',\'txtindicativeComplDate_' + counter + '\');" onBlur="chkIndCompBlank(this);"/><img id="txtindicativeComplDateimg_' + counter + '" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCalWithouTime(\'txtindicativeComplDate_' + counter + '\',\'txtindicativeComplDateimg_' + counter + '\');" /><span id="indComp_' + counter + '" style="color: red;">&nbsp;</span></td></tr>';
                                //var newTxt = '<tr><td class="t-align-center"><input class="formTxtBox_1" type="checkbox" name="chk'+ counter+ ' " id="chk'+ counter+ ' "/></td><td class="t-align-center"><input name="refNo_'+ counter+ '" type="text" class="formTxtBox_1" id="txtrefNo_'+ counter+ '" onBlur="chkRefNoBlank(this);"/><span id="refno_'+ counter+ '" style="color: red;">&nbsp;</span></td><td class="t-align-center"><textarea cols="50" rows="5" id="txtaphasingService_'+ counter+ '" name="phasingService_'+ counter+ '" class="formTxtBox_1" onBlur="chkPhaseSerBlank(this);"></textarea><span id="phaseSer_'+ counter+ '" style="color: red;">&nbsp;</span></td><td class="t-align-center"><input name="locationRefNo_'+ counter+ '" type="text" class="formTxtBox_1" id="txtlocationRefNo_'+ counter+ '" onBlur="chkLocRefBlank(this);"/><span id="locRef_'+ counter+ '" style="color: red;">&nbsp;</span></td><td class="t-align-center"><input name="indicativeStartDate_'+ counter+'" type="text" class="formTxtBox_1" id="txtindicativeStartDate_'+ counter+'" style="width:70px;" readonly="true"  onfocus="GetCalWithouTime(\'txtindicativeStartDate_'+ counter+'\',\'txtindicativeStartDate_'+ counter+'\');" onBlur="chkIndStartBlank(this);"/><img id="txtindicativeStartDateimg_'+ counter+'" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick="GetCalWithouTime(\'txtindicativeStartDate_'+ counter+'\',\'txtindicativeStartDateimg_'+ counter+'\');"/><span id="indStart_'+ counter+ '" style="color: red;">&nbsp;</span></td><td class="t-align-center"><input name="indicativeComplDate_'+ counter+'" type="text" class="formTxtBox_1" id="txtindicativeComplDate_'+ counter+'" style="width:70px;" readonly="true" onfocus="GetCalWithouTime(\'txtindicativeComplDate_'+ counter+'\',\'txtindicativeComplDate_'+ counter+'\');" onBlur="chkIndCompBlank(this);"/><img id="txtindicativeComplDateimg_'+ counter+'" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCalWithouTime(\'txtindicativeComplDate_'+ counter+'\',\'txtindicativeComplDateimg_'+ counter+'\');" /><span id="indComp_'+ counter+ '" style="color: red;">&nbsp;</span></td></tr>';
                                $("#dataTable").append(newTxt);
                                counter++;
                                document.getElementById("txtcounter").value = counter;
                            });
                        });

                        $(function () {
                            $("#delRow").click(function () {
                                var len = $('#dataTable').children()[0].children.length;
                                var lenSelected = $(":checkbox[checked='true']").length + 1;
                                if (len != 2 && len != lenSelected) {
                                    $(":checkbox[checked='true']").each(function () {
                                        //alert($(this).parents('tr'));
                                        var curRow = $(this).parents('tr');
                                        //return false;
                                        curRow.remove();
                                    });
                                } else {
                                    jAlert("Minimum one phasing service is required", "Phasing Of Service", function (RetVal) {
                                    });
                                }
                            });
                        });



                        function daydiff(first, second) {
                            return (first - second) / (1000 * 60 * 60 * 24)
                        }
                        function parseDate(str) {
                            var mdy = str.split('/')
                            var spaceSplit = mdy[2].split(' ');

                            return new Date(spaceSplit[0], mdy[1], mdy[0]);
                        }
                        //                function violatedRule(){
                        //                }
                        //                function combineValues(){
                        //
                        //
                        //
                        //                }


                    </script>
                    <!--                        </td>
                                        </tr>
                                    </table>-->
                </div>
            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <%--Start -- This is done by Rajesh--%>
            <script type="text/javascript">
                function UpdateOpdtAndBSecuritySubDt(comp)
                {
                    document.getElementById('txtpreQualOpenDate').value=comp.value;
                    var DateTimeParts = comp.value.split(" ");
                    var DayMonthYear = DateTimeParts[0].split("/");
                    var date = new Date(DayMonthYear[2], DayMonthYear[1] - 1, DayMonthYear[0]);
                    date.setDate(date.getDate()-1);
                    var day = date.getDate();
                    var month = (date.getMonth()+1);
                    var year = date.getFullYear();
                    if(day<10)
                    {
                        var day = '0' + day;
                    }
                    if(month<10)
                    {
                        var month = '0' + month;
                    }
                    var FinalDate = day + '/' + month + '/' + year + ' ' + DateTimeParts[1];
                    document.getElementById('txtlastDateTenderSub').value=FinalDate;
                    secSubHoliday();
                    findHoliday(document.getElementById('txtlastDateTenderSub'), 6);
                }
                function findHoliday2(comp) {
                        var compVal = comp.value;
                        var cnt = 0;
                        if (compVal != null && compVal != "") {
                            for (var i = 0; i < holiArray.length; i++) {
                                if (CompareToForEqual(holiArray[i], compVal)) {
                                    cnt++;
                                }
                            }



                        }
                        if (cnt != 0) {
                            document.getElementById("holiClose").innerHTML = "Holiday!";
                            document.getElementById("holiOpen").innerHTML = "Holiday!";
                        }
                        else 
                        {
                            document.getElementById("holiClose").innerHTML = "";
                            document.getElementById("holiOpen").innerHTML = "";
                        }
                        
                    }
                
                function findHoliday(comp, compi) {
                    $(".err" + compi).remove();
                    var compVal = comp.value;
                    var cnt = 0;
                    if (compVal != null && compVal != "") {
                        for (var i = 0; i < holiArray.length; i++) {
                            if (CompareToForEqual(holiArray[i], compVal)) {
                                cnt++;
                            }
                        }
                    }
                    if (cnt != 0) {
                        $('#' + comp.id).parent().append("<div class='err" + compi + "' style='color:red;'>Holiday!</div>");
                    }
                }
               
                function chkNumbers(value)
                {
                    //Allow only characters
                    //Characters + numbers
                    //Don't allow only numbers
                    return /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\,\-\s]+$/.test(value);
                }
                function regForSpace(value) {
                    return /^\s+|\s+$/g.test(value);
                }
                function valPassingMarks() {
                    if (document.getElementById("txtPassingMarks") != null) {
                        if (!required($.trim(document.getElementById('txtPassingMarks').value)))
                        {
                            document.getElementById('spanPassingMarks').innerHTML = '<div class="reqF_1">Please enter Passing Points For Technical Evaluation</div>';
                        } else if (!numeric(document.getElementById("txtPassingMarks").value)) {
                            document.getElementById('spanPassingMarks').innerHTML = '<div class="reqF_1">Please enter Positive Numerals (0-9) only</div>';
                        } else if (parseInt($('#txtPassingMarks').val()) <= 0)
                        {
                            document.getElementById('spanPassingMarks').innerHTML = '<div class="reqF_1">Passing Points For Technical Evaluation should be greater than Zero (0)</div>';
                        } else
                        {
                            document.getElementById('spanPassingMarks').innerHTML = '';
                        }
                    }
                }
                // weightPer

                function valWeightPer() {
                    var flag = true;
                    if ($('#txtweightPer').val() != null) {
                        $('.err').remove();

                        //alert($('#txtweightPer').val());
                        if ($('#txtweightPer').val() != '') {
                            if (numeric($('#txtweightPer').val())) {
                                if (parseInt($('#txtweightPer').val()) > 0) {
                                    if (parseInt($('#txtweightPer').val()) < 100) {
                                        var s = parseInt(100 - $('#txtweightPer').val());
                                        $('#txtweightPerF').html(s);
                                    } else {
                                        $('#txtweightPer').parent().append("<div class='err reqF_1'>Please enter Weightage For Technical Evaluation less than 100</div>");
                                        $('#txtweightPerF').html('');
                                        flag = false;
                                    }
                                } else {
                                    $('#txtweightPer').parent().append("<div class='err reqF_1'>Weightage For Technical Evaluation should be greater than Zero (0)</div>");
                                    $('#txtweightPerF').html('');
                                    flag = false;
                                }
                            } else {
                                $('#txtweightPer').parent().append("<div class='err reqF_1'>Please enter Positive Numerals (0-9) only</div>");
                                $('#txtweightPerF').html('');
                                flag = false;
                            }
                        } else {
                            $('#txtweightPer').parent().append("<div class='err reqF_1'>Please enter Weightage For Technical Evaluation</div>");
                            $('#txtweightPerF').html('');
                            flag = false;
                        }
                    }
                    if (!flag) {
                        return 'false';
                    }

                }
                
                function clearvalidation(id)
                {
                   document.getElementById(id).innerHTML = '' ;  
                }
                
//txtweightPerF
                function Validation()
                {
                    vbooltemp = true;
                    var vbool = 'true';
                    //Invitation Reference No.
                    var msg;
                    if (document.getElementById('hdnmsgTender') != null) {
                        msg = document.getElementById('hdnmsgTender').value;
                        //var msg= document.getElementById('hdnmsgTender').value;
                        if(!document.getElementById('integrityPackcnk').checked)
                        {
                            document.getElementById('spanchkIntegrityPack').innerHTML = '<div class="reqF_1">Please accept Terms and Conditions of Integrity Pact</div>';
                            vbool = 'false';
                        }
                        else
                        {
                            document.getElementById('spanchkIntegrityPack').innerHTML = '';
                        }
                        if (document.getElementById('txtinvitationRefNo') != null) {
                            if (!required($.trim(document.getElementById('txtinvitationRefNo').value)))
                            {
                                switch (msg.toString().toUpperCase())
                                {
                                    case "PQ" :
                                        document.getElementById('spantxtinvitationRefNo').innerHTML = '<div class="reqF_1">Please enter Invitation Reference No.</div>';
                                        vbool = 'false';
                                        break;
                                    case "TENDER":
                                        document.getElementById('spantxtinvitationRefNo').innerHTML = '<div class="reqF_1">Please enter Invitation Reference No.</div>';
                                        vbool = 'false';
                                        break;
                                    case "1 STAGE-TSTM":
                                        document.getElementById('spantxtinvitationRefNo').innerHTML = '<div class="reqF_1">Please enter Invitation Reference No.</div>';
                                        vbool = 'false';
                                        break;
                                    case "Tender":
                                        document.getElementById('spantxtinvitationRefNo').innerHTML = '<div class="reqF_1">Please enter Invitation Reference No.</div>';
                                        vbool = 'false';
                                        break;
                                    case "REOI":
                                        document.getElementById('spantxtinvitationRefNo').innerHTML = '<div class="reqF_1">Please enter REOI No.</div>';
                                        vbool = 'false';
                                        break;
                                    case "RFP":
                                        document.getElementById('spantxtinvitationRefNo').innerHTML = '<div class="reqF_1">Please enter RFP No.</div>';
                                        vbool = 'false';
                                        break;
                                    case "RFA":
                                        document.getElementById('spantxtinvitationRefNo').innerHTML = '<div class="reqF_1">Please enter RFA No.</div>';
                                        vbool = 'false';
                                        break;
                                }
                            } else if (document.getElementById('txtinvitationRefNo').value.length > 64) {
                                document.getElementById('spantxtinvitationRefNo').innerHTML = 'Allows maximum 64 characters';
                                vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxtinvitationRefNo').innerHTML = '';
                            }
                        }
                    }


                    if (document.getElementById("chkSecAmt").value != "") {
                        chkAmountSecurity(document.getElementById("chkSecAmt").value);
                        //    vbool='false';
                        // }

                    }
                    //Contract Type
                    if (document.getElementById("contractType") != null)
                    {
                        if (document.getElementById("contractType").value == 'select')
                        {
                            document.getElementById('spancontractType').innerHTML = '<div class="reqF_1">Select Contract Type</div>';
                            vbool = 'false';
                        } else
                        {
                            document.getElementById('spancontractType').innerHTML = '';
                        }
                    }

                    //Eligibility of Tenderer.
                    /*if(document.getElementById('txtaeligibilityofTenderer')!=null){
                     if(!required($.trim(document.getElementById('txtaeligibilityofTenderer').value)))
                     {
                     document.getElementById('spantxtaeligibilityofTenderer').innerHTML='<div class="reqF_1">Please enter Eligibility of Tenderer.</div>';
                     vbool='false';
                     }
                     else
                     {
                     if(!Maxlenght(document.getElementById('txtaeligibilityofTenderer'),'2000')){
                     document.getElementById('spantxtaeligibilityofTenderer').innerHTML='<div class="reqF_1">Maximum 2000 characters are allowed.</div>';
                     vbool='false';
                     }else
                     {
                     document.getElementById('spantxtaeligibilityofTenderer').innerHTML='';
                     }
                     }
                     }*/
                    //Eligibility of Tenderer.
                    if (CKEDITOR.instances.txtaeligibilityofTenderer != null) {

                        if (!required($.trim(CKEDITOR.instances.txtaeligibilityofTenderer.getData())) || isCKEditorFieldBlank($.trim(CKEDITOR.instances.txtaeligibilityofTenderer.getData().replace(/<[^>]*>|\s/g, ''))))
                        {
                            var hdnEvenType = document.getElementById('hdnevenyType').value;
                            if (hdnEvenType.toString().toUpperCase() == 'GOODS' || hdnEvenType.toString().toUpperCase() == 'WORKS') {
                                document.getElementById('spantxtaeligibilityofTenderer').innerHTML = '<div class="reqF_1">Please enter Eligibility of Bidder</div>';
                            } else {
                                document.getElementById('spantxtaeligibilityofTenderer').innerHTML = '<div class="reqF_1">Please enter Eligibility of Consultant</div>';
                            }
                            vbool = 'false';
                        } else
                        {
                            if (!Maxlenght(CKEDITOR.instances.txtaeligibilityofTenderer.getData(), '2000')) {
                                document.getElementById('spantxtaeligibilityofTenderer').innerHTML = '<div class="reqF_1">Maximum 2000 characters are allowed</div>';
                                vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxtaeligibilityofTenderer').innerHTML = '';
                            }
                        }
                    }


                    //Brief description of Works
                    /*if(document.getElementById('txtabriefDescGoods')!=null){
                     if(!required($.trim(document.getElementById('txtabriefDescGoods').value)))
                     {
                     document.getElementById('spantxtabriefDescGoods').innerHTML='<div class="reqF_1">Please enter Brief Description</div>';
                     vbool='false';
                     }
                     else
                     {
                     if(!Maxlenght(document.getElementById('txtabriefDescGoods').value,'2000')){
                     document.getElementById('spantxtabriefDescGoods').innerHTML='<div class="reqF_1">Maximum 2000 characters are allowed.</div>';
                     vbool='false';
                     }
                     else
                     {
                     document.getElementById('spantxtabriefDescGoods').innerHTML='';
                     }
                     }
                     }*/
                    //Brief description of Works
                    if (CKEDITOR.instances.txtabriefDescGoods != null) {
                        if (!required($.trim(CKEDITOR.instances.txtabriefDescGoods.getData())) || isCKEditorFieldBlank($.trim(CKEDITOR.instances.txtabriefDescGoods.getData().replace(/<[^>]*>|\s/g, ''))))
                        {
                            var msgBreif = document.getElementById('briefValMsg').value;
                            if (msgBreif.toString().toUpperCase() == 'GOODS') {
                                document.getElementById('spantxtabriefDescGoods').innerHTML = '<div class="reqF_1">Please enter Brief Description of Goods and Related Service</div>';
                            } else if (msgBreif.toString().toUpperCase() == 'WORKS') {
                                document.getElementById('spantxtabriefDescGoods').innerHTML = '<div class="reqF_1">Please enter Brief Description of Works</div>';

                            } else if (msgBreif.toString().toUpperCase() == 'SERVICES') {
                                document.getElementById('spantxtabriefDescGoods').innerHTML = '<div class="reqF_1">Please enter Brief Description of assignment</div>';
                            }
                            vbool = 'false';
                        } else
                        {
                            if (!Maxlenght(CKEDITOR.instances.txtabriefDescGoods.getData(), '2000')) {
                                document.getElementById('spantxtabriefDescGoods').innerHTML = '<div class="reqF_1">Maximum 2000 characters are allowed</div>';
                                vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxtabriefDescGoods').innerHTML = '';
                            }
                        }
                    }
                    //Name & Address of the Office(s) Selling Tender Document
                    if (document.getElementById('txtanameAddressTenderDoc') != null) {
                        if (!required($.trim(document.getElementById('txtanameAddressTenderDoc').value)))
                        {
                            document.getElementById('spantxtanameAddressTenderDoc').innerHTML = '<div class="reqF_1">Please Enter Name and Address of the Office(s) Selling Tender Document</div>';
                            vbool = 'false';
                        } else
                        {
                            if (!Maxlenght(document.getElementById('txtanameAddressTenderDoc').value, '2000')) {
                                document.getElementById('spantxtanameAddressTenderDoc').innerHTML = '<div class="reqF_1">Maximum 2000 characters are allowed</div>';
                                vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxtanameAddressTenderDoc').innerHTML = '';
                            }
                        }
                    }

                    //Mode  of document fees
                    if (document.getElementById("chkdocFeesModeOff") != null && document.getElementById("chkdocFeesModeBank") != null) {
                        if ((document.getElementById("chkdocFeesModeOff").checked == false) && (document.getElementById("chkdocFeesModeBank").checked == false))
                        {
                            document.getElementById('spanchkdocFeesModeOffid').innerHTML = '<div class="reqF_1">Select atleast one Mode of document Fee</div>';
                            vbool = 'false';
                        } else
                        {
                            document.getElementById('spanchkdocFeesModeOffid').innerHTML = '';
                        }
                    }

                    //Association with foreign firm
                    if (document.getElementById("assoForiegnFirm") != null)
                    {
                        if (document.getElementById("assoForiegnFirm").value == 'select')
                        {
                            document.getElementById('spanassoForiegnFirm').innerHTML = '<div class="reqF_1">Select Association with foreign firm</div>';
                            vbool = 'false';
                        } else
                        {
                            document.getElementById('spanassoForiegnFirm').innerHTML = '';
                        }
                    }

                    //Passing Marks
                    if (document.getElementById("txtPassingMarks") != null) {
                        if (!required($.trim(document.getElementById('txtPassingMarks').value)))
                        {
                            document.getElementById('spanPassingMarks').innerHTML = '<div class="reqF_1">Please enter Passing Points For Technical Evaluation</div>';
                            vbool = 'false';
                        } else if (!numeric(document.getElementById("txtPassingMarks").value)) {
                            document.getElementById('spanPassingMarks').innerHTML = '<div class="reqF_1">Please enter Positive Numerals (0-9) only</div>';
                            vbool = 'false';
                        } else if (parseInt(document.getElementById("txtPassingMarks").value) >= 100)
                        {
                            document.getElementById('spanPassingMarks').innerHTML = '<div class="reqF_1">Please enter Passing Points less than 100</div>';
                            vbool = 'false';
                        } else if (parseInt($('#txtPassingMarks').val()) <= 0)
                        {
                            document.getElementById('spanPassingMarks').innerHTML = '<div class="reqF_1">Passing Points For Technical Evaluation should be greater than Zero (0)</div>';
                            vbool = 'false';
                        } 
                        //else if (document.getElementById("txtweightPer") != null && (document.getElementById("txtweightPer").value != '') && (parseInt(document.getElementById("txtPassingMarks").value) > parseInt(document.getElementById("txtweightPer").value)))
                        //{
                          //  document.getElementById('spanPassingMarks').innerHTML = '<div class="reqF_1">Passing Points For Technical Evaluation should be less than Weightage For Technical Evaluation Points</div>';
                           // vbool = 'false';
                        //} else
                        {
                            document.getElementById('spanPassingMarks').innerHTML = '';
                        }
                    }
                    //Weighted Percentage
                    //alert($('#hdnProcurementMethod').val());
                    //if($('#hdnProcurementMethod').val()=='QCBS'){
                    if ($('#txtweightPer').val() != null) {
                        vbooltemp = valWeightPer();
                        //     alert(vbool);
                        if (vbooltemp == false || vbooltemp == 'false')
                            vbool = 'false';
                    }
                    //}

                    // Validation for Tender Doc Price
                    //  alert(document.getElementById("txtpreQualDocPrice"));

                    vbooltemp = true;
                    if (document.getElementById("txtpreQualDocPrice") != null) {

                        vbooltemp = documentPrice(document.getElementById("txtpreQualDocPrice"));

                        if (vbooltemp == false || vbooltemp == 'false')
                            vbool = 'false';

                    }

                    var vboolUSD = true;
                    if (document.getElementById("preQualDocPriceUSD") != null) {

                        vboolUSD = documentPriceUSD(document.getElementById("preQualDocPriceUSD"));

                        if (vboolUSD == false || vboolUSD == 'false')
                            vbool = 'false';

                    }

                    //Experience, Resources and delivery capacity required
                    if (CKEDITOR.instances.txtaexpRequired != null) {
                        //if(document.getElementById('txtaexpRequired')!=null){
                        if (!required($.trim(CKEDITOR.instances.txtaexpRequired.getData())) || isCKEditorFieldBlank($.trim(CKEDITOR.instances.txtaexpRequired.getData().replace(/<[^>]*>|\s/g, ''))))
                        {
                            document.getElementById('spantxtaexpRequired').innerHTML = '<div class="reqF_1">Please enter Experience, Resources and delivery capacity.</div>';
                            vbool = 'false';
                        } else
                        {
                            if (!Maxlenght(CKEDITOR.instances.txtaexpRequired.getData(), '2000')) {
                                document.getElementById('spantxtaexpRequired').innerHTML = '<div class="reqF_1">Maximum 2000 characters are allowed</div>';
                                vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxtaexpRequired').innerHTML = '';
                            }
                        }
                    }

                    //Name & Address of the Office(s) for tender security submission
                    if (document.getElementById('txtanameAddressTenderSub') != null) {
                        if (!required($.trim(document.getElementById('txtanameAddressTenderSub').value)))
                        {
                            document.getElementById('spantxtanameAddressTenderSub').innerHTML = '<div class="reqF_1">Please Enter Name and Address of the Office(s)</div>';
                            vbool = 'false';
                        } else
                        {
                            if (!Maxlenght(document.getElementById('txtanameAddressTenderSub').value, '2000')) {
                                document.getElementById('spantxtanameAddressTenderSub').innerHTML = '<div class="reqF_1">Maximum 2000 characters are allowed</div>';
                                vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxtanameAddressTenderSub').innerHTML = '';
                            }
                        }
                    }

                    //Last date & time for Tender Security Submission
                    if (document.getElementById('txtlastDateTenderSub') != null) {
                        if (!required($.trim(document.getElementById('txtlastDateTenderSub').value)))
                        {
                            document.getElementById('spantxtlastDateTenderSub').innerHTML = '<div class="reqF_1">Please enter Last Date and Time for Tender Security Submission</div>';
                            vbool = 'false';
                        } else
                        {
                            if (!CompareToForToday(document.getElementById('txtlastDateTenderSub').value))
                            {
                                document.getElementById('spantxtlastDateTenderSub').innerHTML = '<div class="reqF_1">Last Date and Time for Tender Security Submission must be greater than Current Date and Time</div>';
                                vbool = 'false';
                            } else if (!CompareToForSmaller(document.getElementById('txtlastDateTenderSub').value, document.getElementById('txtpreQualCloseDate').value))
                            {
                                document.getElementById('spantxtlastDateTenderSub').innerHTML = '<div class="reqF_1">Tender Security Date and Time must not be after Tender Closing Date and Time</div>';
                                vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxtlastDateTenderSub').innerHTML = '';
                            }

                        }
                    }

                    //Pre-Qaulification Publication Date & Time
                    if (document.getElementById('txttenderpublicationDate') != null) {
                        if (!required(document.getElementById('txttenderpublicationDate').value))
                        {
                            document.getElementById('spantxttenderpublicationDate').innerHTML = '<div class="reqF_1">Please enter Publication Date and Time</div>';
                            vbool = 'false';
                        } else
                        {
                            if (!CompareToForToday(document.getElementById('txttenderpublicationDate').value))
                            {
                                document.getElementById('spantxttenderpublicationDate').innerHTML = '<div class="reqF_1">Publication Date and Time must be greater than Current Date and Time</div>';
                                vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxttenderpublicationDate').innerHTML = '';
                            }
                        }
                    }

                    //Pre-Qualification Document last selling date & time
                    if (document.getElementById('txttenderLastSellDate') != null) {
                        if (!required(document.getElementById('txttenderLastSellDate').value))
                        {
                            document.getElementById('spantxttenderLastSellDate').innerHTML = '<div class="reqF_1">Please enter last Date and Time of Document selling</div>';
                            vbool = 'false';
                        } else
                        {
                            if (!CompareToForGreater(document.getElementById('txttenderLastSellDate').value, document.getElementById('txttenderpublicationDate').value) || !CompareToForSmaller(document.getElementById('txttenderLastSellDate').value, document.getElementById('txtpreQualCloseDate').value))
                            {
                                document.getElementById('spantxttenderLastSellDate').innerHTML = '<div class="reqF_1">Last Date and Time of Document selling should be greater than Publication Date and Time and smaller than Closing Date and Time</div>';
                                vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxttenderLastSellDate').innerHTML = '';
                            }
                        }
                    }

                    //Pre - Qualification meeting Start Date & Time
                    if (document.getElementById('hdncheck') != null && document.getElementById('hdncheck').value == 'Yes') {
                        if (!required(document.getElementById('txtpreTenderMeetStartDate').value))
                        {
                            //document.getElementById('spantxtpreTenderMeetStartDate').innerHTML = '<div class="reqF_1">Please enter meeting Start Date and Time</div>';
                            //vbool = 'false';
                        } else
                        {
                            
                            if (!CompareToForGreater(document.getElementById('txtpreTenderMeetStartDate').value, document.getElementById('txttenderpublicationDate').value))
                            {
                                document.getElementById('spantxtpreTenderMeetStartDate').innerHTML = '<div class="reqF_1">Meeting start Date and Time must be greater than the Publication Date and Time</div>';
                                vbool = 'false';
                            } else
                            {
                                if (document.getElementById('txtpreTenderMeetEndDate').value!="")
                                {
                                    if (!CompareToForSmaller(document.getElementById('txtpreTenderMeetEndDate').value, document.getElementById('txtpreQualCloseDate').value))
                                    {
                                        document.getElementById('spantxtpreTenderMeetEndDate').innerHTML = '<div class="reqF_1">Pre - Tender meeting End Date and Time should be less than Closing Date and Time</div>';
                                        vbool = 'false';
                                    } else
                                    {
                                        document.getElementById('spantxtpreTenderMeetEndDate').innerHTML = '';
                                        document.getElementById('spantxtpreTenderMeetStartDate').innerHTML = '';
                                    }
                                }
                            }
                        }
                    } else
                    {

                        if (document.getElementById('txtpreTenderMeetStartDate').value != '')
                        {

                            if (!CompareToForGreater(document.getElementById('txtpreTenderMeetStartDate').value, document.getElementById('txttenderpublicationDate').value))
                            {
                                document.getElementById('spantxtpreTenderMeetStartDate').innerHTML = '<div class="reqF_1">Meeting start Date and Time must be greater than the Publication Date and Time</div>';
                                vbool = 'false';
                            } else
                            {
                                if (document.getElementById('txtpreTenderMeetEndDate').value != '')
                                {
                                    document.getElementById('spantxtpreTenderMeetStartDate').innerHTML = '';
                                } else
                                {
                                    //document.getElementById('spantxtpreTenderMeetEndDate').innerHTML = '<div class="reqF_1">Please enter Meeting End Date and Time</div>';
                                    //vbool = 'false';
                                }
                            }

                        }
                    }



                    //Pre - Qualification meeting End Date & Time
                    if (document.getElementById('hdncheck') != null && document.getElementById('hdncheck').value == 'Yes') {
                        if (!required(document.getElementById('txtpreTenderMeetEndDate').value))
                        {
                            //document.getElementById('spantxtpreTenderMeetEndDate').innerHTML = '<div class="reqF_1">Please enter meeting End Date and Time</div>';
                            //vbool = 'false';
                        } else
                        {
                            if (!CompareToForGreater(document.getElementById('txtpreTenderMeetEndDate').value, document.getElementById('txtpreTenderMeetStartDate').value))
                            {
                                document.getElementById('spantxtpreTenderMeetEndDate').innerHTML = '<div class="reqF_1">Meeting End Date and Time should be greater than Meeting Start Date and Time</div>';
                                vbool = 'false';
                            } else
                            {
                                if (!CompareToForSmaller(document.getElementById('txtpreTenderMeetEndDate').value, document.getElementById('txtpreQualCloseDate').value))
                                {
                                    //rajeshsingh
                                    document.getElementById('spantxtpreTenderMeetEndDate').innerHTML = '<div class="reqF_1">Pre - Tender meeting End Date and Time should be less than Closing Date and Time</div>';
                                    vbool = 'false';
                                } else
                                {
                                    document.getElementById('spantxtpreTenderMeetEndDate').innerHTML = '';
                                }
                            }
                        }
                    } else
                    {
                        if (document.getElementById('txtpreTenderMeetEndDate').value != '')
                        {
                            if (document.getElementById('txtpreTenderMeetStartDate').value != '')
                            {
                                if (!CompareToForGreater(document.getElementById('txtpreTenderMeetEndDate').value, document.getElementById('txtpreTenderMeetStartDate').value))
                                {
                                    document.getElementById('spantxtpreTenderMeetEndDate').innerHTML = '<div class="reqF_1">Meeting End Date and Time should be greater than Meeting Start Date and Time</div>';
                                    vbool = 'false';
                                } else
                                {
                                    if (!CompareToForSmaller(document.getElementById('txtpreTenderMeetEndDate').value, document.getElementById('txtpreQualCloseDate').value))
                                    {
                                        //rajeshsingh
                                        document.getElementById('spantxtpreTenderMeetEndDate').innerHTML = '<div class="reqF_1">Pre - Tender meeting End Date and Time should be less than Closing Date and Time</div>';
                                        vbool = 'false';
                                    } else
                                    {
                                        document.getElementById('spantxtpreTenderMeetEndDate').innerHTML = '';
                                    }
                                }
                            } else
                            {
                                //document.getElementById('spantxtpreTenderMeetStartDate').innerHTML = '<div class="reqF_1">Please enter Meeting Start Date and Time</div>';
                                //vbool = 'false';
                            }
                        }
                    }


                    //Pre-Qualification Closing Date & Time
                    if (document.getElementById('txtpreQualCloseDate') != null) {
                        if (!required(document.getElementById('txtpreQualCloseDate').value))
                        {
                            document.getElementById('spantxtpreQualCloseDate').innerHTML = '<div class="reqF_1">Please enter Closing Date and Time</div>';
                            vbool = 'false';
                        } else
                        {
                            if(findHoliday3(document.getElementById('txtpreQualCloseDate').value))
                            {
                                clearvalidation('holiOpen');
                                clearvalidation('holiClose');
                                document.getElementById('spantxtpreQualCloseDate').innerHTML='<div class="reqF_1">Closing and Opening Date can not be holiday!</div>';
                                vbool='false';
                                
                            }
                            else if(CompareHoliday(document.getElementById('txtpreQualCloseDate').value)=="Sat" || CompareHoliday(document.getElementById('txtpreQualCloseDate').value)=="Sun")
                            {
                                clearvalidation('demoOpen');
                                clearvalidation('demoClose');
                                document.getElementById('spantxtpreQualCloseDate').innerHTML='<div class="reqF_1">Closing and Opening Date can not be weekend!</div>';
                                vbool='false';
                            }
                            else if (!CompareToForGreater(document.getElementById('txtpreQualCloseDate').value, document.getElementById('txttenderpublicationDate').value))
                            {
                                document.getElementById('spantxtpreQualCloseDate').innerHTML = '<div class="reqF_1">Closing Date and Time should be greater than Publication Date and Time</div>';
                                vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxtpreQualCloseDate').innerHTML = '';
                            }
                        }
                    }

                    //Pre-Qualification Opening Date & Time
                    if (document.getElementById('txtpreQualOpenDate') != null && document.getElementById('txttenderpublicationDate') != null) {
                        if (!required(document.getElementById('txtpreQualOpenDate').value))
                        {
                            document.getElementById('spantxtpreQualOpenDate').innerHTML = '<div class="reqF_1">Please enter Opening Date and Time</div>';
                            vbool = 'false';
                        } else
                        {
                            if(findHoliday3(document.getElementById('txtpreQualOpenDate').value))
                            {
                                clearvalidation('holiOpen');
                                clearvalidation('holiClose');
                                document.getElementById('spantxtpreQualOpenDate').innerHTML='<div class="reqF_1">Opening Date can not be holiday!</div>';
                                vbool='false';
                                
                            }
                            else if(CompareHoliday(document.getElementById('txtpreQualOpenDate').value)=="Sat" || CompareHoliday(document.getElementById('txtpreQualOpenDate').value)=="Sun")
                            {
                                clearvalidation('demoOpen');
                                clearvalidation('demoClose');
                                document.getElementById('spantxtpreQualOpenDate').innerHTML='<div class="reqF_1">Opening Date can not be weekend!</div>';
                                vbool='false';
                            }
                            else if (!CompareToForGreater(document.getElementById('txtpreQualOpenDate').value, document.getElementById('txtpreQualCloseDate').value) && document.getElementById('txtpreQualOpenDate').value != document.getElementById('txtpreQualCloseDate').value)
                            {
                                document.getElementById('spantxtpreQualOpenDate').innerHTML = '<div class="reqF_1">Opening Date and Time should be equal or greater than Closing Date and Time</div>';
                                vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxtpreQualOpenDate').innerHTML = '';
                            }
                        }
                    }

                    //Pre-Qualification Opening Date & Time
                    if (document.getElementById('txtpreQualOpenDate') != null && document.getElementById('txttenderpublicationDate') != null) {
                        if (!required(document.getElementById('txtpreQualOpenDate').value))
                        {
                            //document.getElementById('spantxtpreQualOpenDate').innerHTML = '<div class="reqF_1">Please enter Opening Date and Time</div>';
                            //vbool = 'false';
                        } else
                        {
                            if (!CompareToForGreater(document.getElementById('txtpreQualOpenDate').value, document.getElementById('txtpreQualCloseDate').value))
                            {
                                //document.getElementById('spantxtpreQualOpenDate').innerHTML = '<div class="reqF_1">Opening Date and Time should be greater than Closing Date and Time</div>';
                                //vbool = 'false';
                            } else
                            {
                                document.getElementById('spantxtpreQualOpenDate').innerHTML = '';
                            }
                        }
                    }
                    //document price (TK) -rishita
                    if (document.getElementById('txtpreQualDocPrice') != null) {
                        if (document.getElementById('cmbdocFees') != null && document.getElementById('cmbdocFees').value == 'Package wise') {
                            if (!required(document.getElementById('txtpreQualDocPrice').value))
                            {   //document.getElementById('hdndoc').value;
                                //alert(document.getElementById('hdndocprice').value);
                                if (document.getElementById('hdndocprice').value == 'Yes') {
                                    document.getElementById('spantxtpreQualDocPriceman').innerHTML = '<div class="reqF_1">Please enter Tender Document Price</div>';
                                    vbool = 'false';
                                } else {
                                    var removedecimal = document.getElementById('txtpreQualDocPrice').value.split(".");
                                    if ($.trim(document.getElementById('txtpreQualDocPrice').value.charAt(0)) == '0' || removedecimal[0] == '0')
                                    {
                                        //document.getElementById('spantxtpreQualDocPriceman').innerHTML = '<div class="reqF_1">Only 0 value is not allowed</div>';
                                        //vbool = 'false';
                                    } else
                                    {
                                        //  document.getElementById('spantxtpreQualDocPriceman').innerHTML='';
                                    }
                                }

                            } else if (!numeric(document.getElementById('txtpreQualDocPrice').value)) {
                                document.getElementById('spantxtpreQualDocPriceman').innerHTML = '<div class="reqF_1">Please enter numeric data</div>';
                                vbool = 'false';
                            } else {
                                var removedecimal = document.getElementById('txtpreQualDocPrice').value.split(".");
                                if ($.trim(document.getElementById('txtpreQualDocPrice').value.charAt(0)) == '0' || removedecimal[0] == '0')
                                {
                                    //document.getElementById('spantxtpreQualDocPriceman').innerHTML = '<div class="reqF_1">Only 0 value is not allowed</div>';
                                    //vbool = 'false';
                                } else
                                {
                                    // document.getElementById('spantxtpreQualDocPriceman').innerHTML='';
                                }
                                //document.getElementById('spantxtpreQualDocPriceman').innerHTML='';
                            }
                        }
                    }



                    //Made by Dhruti -- START
                    var counter = eval(document.getElementById("txtcounter").value);
                    var count = <%=countI%>;
                    if (count != 0)
                    {
                        var closeDate;
                        var year;
                        var month;
                        var day;
                        var date;
                        if (document.getElementById("txtpreQualCloseDate") != null && document.getElementById("txtpreQualCloseDate").value != "") {
                            closeDate = document.getElementById("txtpreQualCloseDate").value;
                            year = parseInt(closeDate.split('/')[2]);
                            month = (closeDate.split('/')[1]);
                            day = (closeDate.split('/')[0]);
                            date = new Date(year, (month - 1), day);
                        }
                        for (var k = 0; k < count; k++)
                        {
                            if (document.getElementById("locationlot_" + k) != null) {
                                if (document.getElementById("locationlot_" + k).value == "") {
                                    document.getElementById("locLot_" + k).innerHTML = "<br/>Please enter Location";
                                    vbool = 'false';
                                } else if (regForSpace(document.getElementById("locationlot_" + k).value)) {
                                    document.getElementById("locLot_" + k).innerHTML = "<br/>Only Space is not allowed";
                                    vbool = 'false';
                                } else if (!chkNumbers(document.getElementById("locationlot_" + k).value)) {
                                    document.getElementById("locLot_" + k).innerHTML = "<br/>Allows Characters,Numbers and Special character (Space,-)";
                                    vbool = 'false';
                                }
                            }

                            if (document.getElementById("tenderSecurityAmount_" + k) != null) {
                                if (document.getElementById("tenderSecurityAmount_" + k).value == "") {
                                    document.getElementById("amountLot_" + k).innerHTML = "<br/>Please enter Bid Security Amount";
                                    vbool = 'false';
                                }
                            }
                            if (document.getElementById("tenderSecurityAmount_" + k) != null) {                           
                                if (document.getElementById("chkfinanDeclaration_"+ k).checked == false && document.getElementById("chkbidSecDeclaration_"+ k).checked == false) {
                                    document.getElementById("msgDeclaration_"+ k).innerHTML='Please select Bid Declaration Type';
                                    vbool = 'false';
                                }  
                            }
                            if (document.getElementById("tenderSecurityAmountUSD_" + k) != null) {
                                if (document.getElementById("tenderSecurityAmountUSD_" + k).value == "") {
                                    document.getElementById("amountLotUSD_" + k).innerHTML = "<br/>Please enter Bid Security Amount";
                                    vbool = 'false';
                                }
                            }

                            if (document.getElementById("complTimeLotNo_" + k) != null) {
                                if (document.getElementById("complTimeLotNo_" + k).value == "") {
                                    document.getElementById("compLot_" + k).innerHTML = "<br/>Please enter Contract End Date";
                                    vbool = 'false';
                                } else {
                                    var objdate1 = document.getElementById("startTimeLotNo_" + k).value;
                                    var yearobj1 = parseInt(objdate1.split('/')[2]);
                                    var monthobj1 = (objdate1.split('/')[1]);
                                    var dayobj1 = (objdate1.split('/')[0]);
                                    var dateStart1 = new Date(yearobj1, (monthobj1 - 1), dayobj1);


                                    var objdate = document.getElementById("complTimeLotNo_" + k).value;
                                    var yearobj = parseInt(objdate.split('/')[2]);
                                    var monthobj = (objdate.split('/')[1]);
                                    var dayobj = (objdate.split('/')[0]);
                                    var dateobj = new Date(yearobj, (monthobj - 1), dayobj);
                                    if (Date.parse(dateobj) > Date.parse(dateStart1))
                                    {
                                        document.getElementById("compLot_" + k).innerHTML = "";
                                        //vbool='true';
                                    } else
                                    {
                                        document.getElementById("compLot_" + k).innerHTML = "<br/>Contract End Date and Time must be greater than Contract Start Date and Time";
                                        vbool = 'false';
                                    }
                                }
                            }

                            if (document.getElementById("startTimeLotNo_" + k) != null) {
                                if (document.getElementById("startTimeLotNo_" + k).value == "") {
                                    document.getElementById("startLot_" + k).innerHTML = "<br/>Please enter Contract Start Date";
                                    vbool = 'false';
                                } else {
                                    var objdate = document.getElementById("startTimeLotNo_" + k).value;
                                    var yearobj = parseInt(objdate.split('/')[2]);
                                    var monthobj = (objdate.split('/')[1]);
                                    var dayobj = (objdate.split('/')[0]);
                                    var dateobj = new Date(yearobj, (monthobj - 1), dayobj);
                                    if (Date.parse(dateobj) > Date.parse(date))
                                    {
                                        document.getElementById("startLot_" + k).innerHTML = "";
                                        //vbool='true';
                                    } else
                                    {
                                        document.getElementById("startLot_" + k).innerHTML = "<br/>Contract Start Date must be greater than Closing Date and Time";
                                        vbool = 'false';
                                    }
                                }
                            }

                            if (document.getElementById("docFeeslot_" + k) != null && document.getElementById('hdndoc') != null && document.getElementById('hdndoc').value == 'No') {
                                if (document.getElementById("docFeeslot_" + k).value == "") {
                                    document.getElementById("docFees_" + k).innerHTML = "<br/>Please enter Document fees amount in Tk";
                                    vbool = 'false';
                                }
                            }
                        }
                    } else
                    {
                        for (var j = 0; j <= counter; j++)
                        {
                            if (document.getElementById("txtrefNo_" + j) != null) {
                                if (document.getElementById("txtrefNo_" + j).value == '' || document.getElementById("txtrefNo_" + j).value.charAt(0) == ' ') {
                                    document.getElementById("refno_" + j).innerHTML = "<br/>Please enter Ref. No.";
                                    vbool = 'false';
                                }
                            }
                            if (document.getElementById("txtaphasingService_" + j) != null) {
                                if (document.getElementById("txtaphasingService_" + j).value == '' || document.getElementById("txtaphasingService_" + j).value.charAt(0) == ' ') {
                                    document.getElementById("phaseSer_" + j).innerHTML = "<br/>Please enter Phasing of service";
                                    vbool = 'false';
                                }
                            }
                            if (document.getElementById("txtlocationRefNo_" + j) != null) {
                                if (document.getElementById("txtlocationRefNo_" + j).value == "") {
                                    document.getElementById("locRef_" + j).innerHTML = "<br/>Please enter Location";
                                    vbool = 'false';
                                } else if (regForSpace(document.getElementById("txtlocationRefNo_" + j).value)) {
                                    document.getElementById("locRef_" + j).innerHTML = "<br/>Only Space is not allowed";
                                    vbool = 'false';
                                }
                            }
                            if (document.getElementById("txtindicativeStartDate_" + j) != null) {
                                if (document.getElementById("txtindicativeStartDate_" + j).value == "") {
                                    document.getElementById("indStart_" + j).innerHTML = "<br/>Please enter Indicative Contract Start Date";
                                    vbool = 'false';
                                }
                            }
                            if (document.getElementById("txtindicativeComplDate_" + j) != null) {
                                if (document.getElementById("txtindicativeComplDate_" + j).value == "") {
                                    document.getElementById("indComp_" + j).innerHTML = "<br/>Please enter Indicative Contract End Date";
                                    vbool = 'false';
                                }
                            }
                            
                            if(document.getElementById("txtindicativeComplDate_"+j)!=null && document.getElementById("txtindicativeStartDate_"+j)!=null)
                            {
                                if(CompareToForGreater(document.getElementById("txtindicativeComplDate_"+j).value,document.getElementById("txtindicativeStartDate_"+j).value))
                                {
                                    document.getElementById("indComp_"+j).innerHTML="";
                                }
                                else
                                {
                                    document.getElementById("indComp_"+j).innerHTML="<br/>Indicative Contract End Date Must be Greater than Indicative Contract Start Date";
                                    vbool = 'false';
                                }
                            }
                            
                        }
                    }


                    vbooltemp = true;
                    if (document.getElementById("txtpreQualDocPrice") != null)
                    {

                        vbooltemp = documentPrice(document.getElementById("txtpreQualDocPrice"));

                        if (vbooltemp == false || vbooltemp == 'false')
                            vbool = 'false';

                    }

                    if (vbool == 'false' || document.getElementById("boolcheck").value == false || document.getElementById("boolcheck").value == 'false' || vbooltemp == false) {
                        return false;
                    }
                    
                    if(document.getElementById("txtpreTenderMeetStartDate").value != "")
                    {
                        if(document.getElementById("txtpreTenderMeetEndDate").value == "")
                        {
                            document.getElementById("spantxtpreTenderMeetEndDate").innerHTML = '<div class="reqF_1">Enter Pre Bid meeting end date.</div>';
                            return false;
                        }
                    }



                    if (document.getElementById('txtpreTenderMeetStartDate') != null && document.getElementById("txttenderpublicationDate") != null && document.getElementById('pubBidDays') != null) {
                        //return false;
                        if (document.getElementById('txtpreTenderMeetStartDate').value != '' && document.getElementById("txttenderpublicationDate").value != '') {
                            var tpublicationDate = document.getElementById("txttenderpublicationDate").value;
                            //var tpreQualCloseDate = document.getElementById("txtpreQualCloseDate").value;
                            //var days = daydiff(parseDate(tpreQualCloseDate), parseDate(tpublicationDate));
                            //var days = daydiff(parseDate(txtpreTenderMeetStartDate), parseDate(tpublicationDate));
                            var days = calcDays(document.getElementById('txtpreTenderMeetStartDate'), document.getElementById("txttenderpublicationDate"));
                            var txtpreTenderMeetStartDate = document.getElementById('txtpreTenderMeetStartDate').value;
                            var pubBidDays = document.getElementById('pubBidDays').value;
                            //var diffDaysPub = days * pubBidDays;
                            //var daysMeetingPub = daydiff(parseDate(txtpreTenderMeetStartDate), parseDate(tpublicationDate));
                            //if(diffDaysPub > daysMeetingPub){
                            //return false;
                            if (days <= pubBidDays) {

                            } else {
                                //document.getElementById('msgVal').innerHTML = '';
                                jAlert("Pre-Tender meeting business rule violated. Meeting should be scheduled within " + pubBidDays + " days from date of publication", "Business Rule Violated", function (RetVal) {
                                });
                                return false;
                            }
                        }
                    }
                    if (document.getElementById("txttenderpublicationDate") != null && document.getElementById("txtpreQualCloseDate") != null && document.getElementById('procMethodDays0') != null) {
                        if (document.getElementById("txttenderpublicationDate").value != '' && document.getElementById("txtpreQualCloseDate").value != '') {
                            var procMethodDays;
                            if(subDayFlag == 1)
                            {
                                procMethodDays = document.getElementById('procMethodDays0').value;
                            }
                            else
                            {
                                procMethodDays = changedSubDay;
                            }
                            var tpublicationDate = document.getElementById("txttenderpublicationDate").value;
                            var tpreQualCloseDate = document.getElementById("txtpreQualCloseDate").value;
                            //var days = daydiff(parseDate(tpreQualCloseDate), parseDate(tpublicationDate));
                            //var days = daydiff(parseDate(document.getElementById("txtpreQualCloseDate")), parseDate(document.getElementById("txttenderpublicationDate")));
                            var days = calcDays(document.getElementById("txtpreQualCloseDate"), document.getElementById("txttenderpublicationDate"));
                            if (procMethodDays >= days) {
                                //if(10 >= days){
                                jAlert("Minimum " + procMethodDays + " days for Tender Submission must be given.", "Business Rule Violated", function (RetVal) {
                                });
                                //document.getElementById('msgVal').innerHTML = 'Meeting rule violated. Meeting must be scheduled with in <<days>> from the date of publication.';
                                return false;
                            }
                        }
                    }

                    //Made By Rishita - Combines Values
                    var counter = document.getElementById("txtcounter").value;
                    var cmbrefNo = '';
                    var cmbPhasingofService = '';
                    var cmbLocation = '';
                    var cmbIndicativeStartDate = '';
                    var cmbIndicativeComplDate = '';

                    var cmbLocationLot = '';
                    var cmbTenderSecurityAmount = '';
                    var cmbBidSecurityType = '';
                    var cmbTenderSecurityAmountUSD = '';
                    var cmbComplTimeLotNo = '';
                    var cmbStartTimeLotNo = '';
                    var cmbdocFeeslot = '';
                    var cmbtenderLotSecId = '';

                    for (var i = 0; i < counter; i++) {

                        if (document.getElementById('txtrefNo_' + i) != null) {
                            cmbrefNo += document.getElementById('txtrefNo_' + i).value;
                            cmbrefNo = cmbrefNo + '@$';
                        }

                        if (document.getElementById('txtaphasingService_' + i) != null) {
                            cmbPhasingofService += document.getElementById('txtaphasingService_' + i).value;
                            cmbPhasingofService = cmbPhasingofService + '@$';
                        }

                        if (document.getElementById('txtlocationRefNo_' + i) != null) {
                            cmbLocation += document.getElementById('txtlocationRefNo_' + i).value;
                            cmbLocation = cmbLocation + '@$';
                        }
                        var splitSlashStart = new Array();
                        if (document.getElementById('txtindicativeStartDate_' + i) != null) {
                            splitSlashStart = (document.getElementById('txtindicativeStartDate_' + i).value).split("/");
                            var indicativeDate = splitSlashStart[2] + "-" + splitSlashStart[1] + "-" + splitSlashStart[0];

                            //cmbIndicativeStartDate += document.getElementById('txtindicativeStartDate'+i).value;
                            cmbIndicativeStartDate += indicativeDate;
                            cmbIndicativeStartDate = cmbIndicativeStartDate + '@$';
                        }
                        var splitSlashComDate = new Array();
                        if (document.getElementById('txtindicativeComplDate_' + i) != null) {
                            splitSlashComDate = (document.getElementById('txtindicativeComplDate_' + i).value).split("/");
                            var comdate = splitSlashComDate[2] + "-" + splitSlashComDate[1] + "-" + splitSlashComDate[0];
                            //cmbIndicativeComplDate += document.getElementById('txtindicativeComplDate'+i).value;
                            cmbIndicativeComplDate += comdate;
                            cmbIndicativeComplDate = cmbIndicativeComplDate + '@$';
                        }

                    }
                    var flag = <%=countI%>;
                    for (var i = 0; i < flag; i++) {

                        if (document.getElementById("tenderLotSecId" + i) != null) {
                            cmbtenderLotSecId += document.getElementById("tenderLotSecId" + i).value;
                            cmbtenderLotSecId = cmbtenderLotSecId + '@$';
                        }

                        if (document.getElementById('locationlot_' + i) != null) {
                            cmbLocationLot += document.getElementById('locationlot_' + i).value;
                            cmbLocationLot = cmbLocationLot + '@$';
                        }

                        if (document.getElementById('tenderSecurityAmount_' + i) != null) {
                            cmbTenderSecurityAmount += document.getElementById('tenderSecurityAmount_' + i).value;
                            cmbTenderSecurityAmount = cmbTenderSecurityAmount + '@$';
                        } else {
                            cmbTenderSecurityAmount += 0;
                            cmbTenderSecurityAmount = cmbTenderSecurityAmount + '@$';
                        }
                        
                        if (document.getElementById('tenderSecurityAmount_' + i) != null) {  
                          if ((document.getElementById("chkfinanDeclaration_"+ i).checked) || (document.getElementById("chkbidSecDeclaration_"+ i).checked)) {                       
                                if((document.getElementById("chkfinanDeclaration_" + i).checked) && (document.getElementById("chkbidSecDeclaration_" + i).checked)){
                                    cmbBidSecurityType += "3";
                                    cmbBidSecurityType = cmbBidSecurityType + '@$';                                
                                }
                                else{
                                    if(document.getElementById("chkfinanDeclaration_" + i).checked){
                                       cmbBidSecurityType += "1";
                                       cmbBidSecurityType = cmbBidSecurityType + '@$'; 
                                    }
                                    else if(document.getElementById("chkbidSecDeclaration_" + i).checked)
                                    {
                                       cmbBidSecurityType += "2";
                                       cmbBidSecurityType = cmbBidSecurityType + '@$';  
                                    }
                                }
                            } else {
                                cmbBidSecurityType += "0";
                                cmbBidSecurityType = cmbBidSecurityType + '@$';
                            }
                         }
                         else {
                                cmbBidSecurityType += '@$';
                                cmbBidSecurityType = cmbBidSecurityType + '@$';
                            }
                       
                        if (document.getElementById('tenderSecurityAmountUSD_' + i) != null) {
                            cmbTenderSecurityAmountUSD += document.getElementById('tenderSecurityAmountUSD_' + i).value;
                            cmbTenderSecurityAmountUSD = cmbTenderSecurityAmountUSD + '@$';
                        } else {
                            cmbTenderSecurityAmountUSD += 0;
                            cmbTenderSecurityAmountUSD = cmbTenderSecurityAmountUSD + '@$';
                        }

                        if (document.getElementById('complTimeLotNo_' + i) != null) {
                            cmbComplTimeLotNo += document.getElementById('complTimeLotNo_' + i).value;
                            cmbComplTimeLotNo = cmbComplTimeLotNo + '@$';
                        }
                        if (document.getElementById('startTimeLotNo_' + i) != null) {
                            cmbStartTimeLotNo += document.getElementById('startTimeLotNo_' + i).value;
                            cmbStartTimeLotNo = cmbStartTimeLotNo + '@$';
                        }
                        if (document.getElementById('docFeeslot_' + i) == null || document.getElementById('docFeeslot_' + i).value == "") {
                            cmbdocFeeslot += 0;
                            cmbdocFeeslot = cmbdocFeeslot + '@$';
                        } else {
                            cmbdocFeeslot += document.getElementById('docFeeslot_' + i).value;
                            cmbdocFeeslot = cmbdocFeeslot + '@$';
                        }
                    }
                    document.getElementById('combineRefNo').value = cmbrefNo.substring(0, cmbrefNo.length - 2);

                    document.getElementById('combinePOS').value = cmbPhasingofService.substring(0, cmbPhasingofService.length - 2);

                    document.getElementById('combineLocation').value = cmbLocation.substring(0, cmbLocation.length - 2);

                    document.getElementById('combineIndicativeStartDate').value = cmbIndicativeStartDate.substring(0, cmbIndicativeStartDate.length - 2);

                    document.getElementById('combineIndicativeComplDate').value = cmbIndicativeComplDate.substring(0, cmbIndicativeComplDate.length - 2);

                    document.getElementById('combineLocationLot').value = cmbLocationLot.substring(0, cmbLocationLot.length - 2);

                    document.getElementById('combineTenderSecurityAmount').value = cmbTenderSecurityAmount.substring(0, cmbTenderSecurityAmount.length - 2);

                    document.getElementById('combineTenderSecurityAmountUSD').value = cmbTenderSecurityAmountUSD.substring(0, cmbTenderSecurityAmountUSD.length - 2);
                    
                    document.getElementById('combineBidSecurityType').value = cmbBidSecurityType.substring(0, cmbBidSecurityType.length - 2);

                    document.getElementById('combineComplTimeLotNo').value = cmbComplTimeLotNo.substring(0, cmbComplTimeLotNo.length - 2);

                    document.getElementById('combineStartTimeLotNo').value = cmbStartTimeLotNo.substring(0, cmbStartTimeLotNo.length - 2);

                    document.getElementById('combineDocFeesLot').value = cmbdocFeeslot.substring(0, cmbdocFeeslot.length - 2);

                    document.getElementById('combinetenderLotSecId').value = cmbtenderLotSecId.substring(0, cmbtenderLotSecId.length - 2);


                    //Made by Dhruti -- END
                    //alert("false"+vbool);
                    //alert("false"+document.getElementById("boolcheck").value);
                    //  alert(" last="+vbool);


                }

                //Made By Rishita - ViolateRule
                function calcDays(date1, date2) {
                    var dat1 = (date1.value).split("/");
                    var dt1 = dat1[2].split(" ");
                    var dat2 = (date2.value).split("/");
                    var dt2 = dat2[2].split(" ");
                    var sDate = new Date(dt1[0] + "/" + dat1[1] + "/" + dat1[0]);
                    var eDate = new Date(dt2[0] + "/" + dat2[1] + "/" + dat2[0]);
                    var daysApart = Math.abs(Math.round((sDate - eDate) / 86400000));
                    return daysApart;

                }
            </script>

            <%--End -- This is done by Rajesh--%>
        </div>
    </body>
    <%
        tenderSrBean = null;
    %>

    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <script type="text/javascript">
        
        function addtableRow() {  
         var check = false;
         var i = 0;
         var ictcheck = document.getElementById('hdnprocuretype').value;  
        
         $('#tableDT').append('<tr><td class="ff">Last Date and Time for Tender Security Submission :<span>*</span></td><td><input name="lastDateTenderSub" type="text" class="formTxtBox_1" id="txtlastDateTenderSub" style="width:100px;" readonly="true"  onfocus="GetCal(\'txtlastDateTenderSub\', \'txtlastDateTenderSub\');" onblur="findHoliday(this, 6);secSubHoliday();"/>&nbsp;<img id="txtlastDateTenderSubimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal(\'txtlastDateTenderSub\', \'txtlastDateTenderSubimg\');"/><span id="spantxtlastDateTenderSub" style="color: red;"></span></td></tr>');
         $('#infoBidder').append('<tr><td class="ff">Document Fees :</td><td>Package wise<input type="hidden" value="Package wise" name="docFees" id="cmbdocFees"/></td></tr>');
         $('#infoBidder').append('<tr id="docsprice"><td class="ff">Tender Document Price (In Nu.) :</td><td><input name="preQualDocPrice" type="text" onblur="documentPrice(this);" class="formTxtBox_1" id="txtpreQualDocPrice" style="width:200px;" /><div id="preQualDocPriceInWords"></div><span id="spantxtpreQualDocPriceman"></span></td></tr>');
         if(ictcheck == 'ICT'){
            $('#infoBidder').append('<tr id="docspriceICT"> <td class="ff">Equivalent Tender Document Price (In USD):</td><td><span style="float: right;margin-right: 50%;" id="spantxtBDRate" class="reqF_1"> <a href="http://www.bob.bt/" target="_blank;" style="color:red;">See Bank of Bhutan Conversion Rate </a></span><input  style="width:200px;"  name="preQualDocPriceUSD" type="text" class="formTxtBox_1" id="preQualDocPriceUSD"  onblur="documentPriceUSD(this);"/><div  id="preQualDocPriceICTInWords"></div><span id="spantxtpreQualDocPricemanICT" class="reqF_1"></span></td></tr>');
         }
         $('#infoBidder').append('<tr><td class="ff">Mode of Payment : </td><td><label>Payment through Financial Institution</label><input type="hidden" id="chkdocFeesModeBank" value="Bank" name="docFeesMode"/><span id="spanchkdocFeesModeOffid"></span></td></tr>');
           if(ictcheck == 'ICT'){
                $('#lotDescription').find('tr').each(function(){            
                  if(!check){
                    $(this).find('th').eq(2).after('<th width="20%" class="t-align-center">Bid Security (Amount in Nu.) and Type <span class="mandatory">*</span></th>');  
                    $(this).find('th').eq(3).after('<th width="15%" class="t-align-center">Equivalent Bid Security (Amount in USD) <span class="mandatory">*</span></th>');
                    check = true;
                  }
                  else{
                    var columnhtml = '<td class="t-align-center"><div><input name="tenderSecurityAmount_'+i+'" type="text" class="formTxtBox_1" id="tenderSecurityAmount_'+i+'"  onblur="chkAmountLotBlank(this);" style="width:185px;"/><span id="amountLot_'+i+'" style="color: red;"></span></div><div style="text-align: left; padding: 9px 0 0 7px;"><input type="checkbox" value ="1" name = "finanDeclaration_'+i+'" id="chkfinanDeclaration_'+i+'"  onchange="chkmsgDeclaration('+i+',1);"/> Financial Institution Payment <br/><input type="checkbox" value="2" name="bidSecDeclaration_'+i+'"  id="chkbidSecDeclaration_'+i+'" onchange="chkmsgDeclaration('+i+',2);"/> Bid Security Declaration <br/><span id="msgDeclaration_'+i+'" style="color: red;"></span></div> </td>';
                    var columnhtml2 = '<td class="t-align-center"><input name="tenderSecurityAmountUSD_'+i+'" type="text" class="formTxtBox_1" id="tenderSecurityAmountUSD_'+i+'"  onblur="chkAmountLotBlankUSD(this);" /><span id="amountLotUSD_'+i+'" style="color: red;"></span></td>';
                    $(this).find('td').eq(2).after(columnhtml);
                    $(this).find('td').eq(3).after(columnhtml2);
                    i++;
                   }
                });   
             }
            else {
                $('#lotDescription').find('tr').each(function(){            
                    if(!check){
                      $(this).find('th').eq(2).after('<th width="20%" class="t-align-center">Bid Security (Amount in Nu.) and Type <span class="mandatory">*</span></th>');  
                      check = true;
                    }
                    else{
                      var columnhtml = '<td class="t-align-center"><div><input name="tenderSecurityAmount_'+i+'" type="text" class="formTxtBox_1" id="tenderSecurityAmount_'+i+'"  onblur="chkAmountLotBlank(this);" style="width:185px;"/><span id="amountLot_'+i+'" style="color: red;"></span></div><div style="text-align: left; padding: 9px 0 0 7px;"><input type="checkbox" value ="1" name = "finanDeclaration_'+i+'" id="chkfinanDeclaration_'+i+'"  onchange="chkmsgDeclaration('+i+',1);"/> Financial Institution Payment <br/><input type="checkbox" value="2" name="bidSecDeclaration_'+i+'"  id="chkbidSecDeclaration_'+i+'" onchange="chkmsgDeclaration('+i+',2);"/> Bid Security Declaration <br/><span id="msgDeclaration_'+i+'" style="color: red;"></span></div> </td>';
                      $(this).find('td').eq(2).after(columnhtml);
                      i++;
                     }
                  });
              }
        }
        function deltableRow(){
             var check = false;
             var ictcheck = document.getElementById('hdnprocuretype').value;  
             document.getElementById('tableDT').deleteRow(7); // To delete security submission date
             document.getElementById('infoBidder').deleteRow(4);
             document.getElementById('infoBidder').deleteRow(4);
             if(ictcheck == 'ICT'){
                 document.getElementById('infoBidder').deleteRow(4);
             }
             document.getElementById('infoBidder').deleteRow(4);
             if(ictcheck == 'ICT'){
                $('#lotDescription').find('tr').each(function(){
                    if(!check){
                       $(this).find('th').eq(3).remove();
                       $(this).find('th').eq(3).remove();
                       check = true;
                    }
                    else{
                       $(this).find('td').eq(3).remove(); 
                       $(this).find('td').eq(3).remove(); 
                    }
                });  
             }
             else{
                $('#lotDescription').find('tr').each(function(){
                    if(!check){
                       $(this).find('th').eq(3).remove();
                       check = true;
                    }
                    else{
                       $(this).find('td').eq(3).remove(); 
                    }
                });
            }
        }
        
        function setFieldName(obj){
            if(document.getElementById('hdnProcurementNature').value == 'Goods' || document.getElementById('hdnProcurementNature').value == 'Works')
            {  
                if ("RFQ" == obj.options[obj.selectedIndex].value) {
                    document.getElementById('msgTenderShow').innerHTML = 'LEM No.';
                }
                else{
                    document.getElementById('msgTenderShow').innerHTML = 'Invitation Reference No.';
                }
                
                if ("OTM" == obj.options[obj.selectedIndex].value) {
                    if(document.getElementById('hdnProcureMethod').value == 'RFQ') 
                    {    
                        document.getElementById('lblEventType').innerHTML = 'TENDER';
                        document.getElementById('hdnEventType').value = 'Tender';
                        document.getElementById('hdnProcureMethod').value = 'OTM';
                        $('#publicationDT').html('Scheduled Tender Publication <br/>Date and Time : <span>*</span>');
                        $('#docSellDT').html('Tender Document last selling /<br/>downloading Date and Time : <span>*</span>');            
                        $('#closingDT').html('Tender  Closing <br/> Date and Time : <span>*</span>');            
                        $('#openingDT').html('Tender  Opening <br/> Date and Time : <span>*</span>'); 
                        addtableRow();
                   }
                }
                if ("LTM" == obj.options[obj.selectedIndex].value) {
                    if(document.getElementById('hdnProcureMethod').value == 'RFQ') 
                    {    
                        document.getElementById('lblEventType').innerHTML = 'TENDER';
                        document.getElementById('hdnEventType').value = 'Tender';
                        document.getElementById('hdnProcureMethod').value = 'LTM';
                        $('#publicationDT').html('Scheduled Tender Publication <br/>Date and Time : <span>*</span>');
                        $('#docSellDT').html('Tender Document last selling /<br/>downloading Date and Time : <span>*</span>');            
                        $('#closingDT').html('Tender  Closing <br/> Date and Time : <span>*</span>');            
                        $('#openingDT').html('Tender  Opening <br/> Date and Time : <span>*</span>'); 
                        addtableRow();
                   }
                }
                if ("FC" == obj.options[obj.selectedIndex].value) {
                    if(document.getElementById('hdnProcureMethod').value == 'RFQ') 
                    {    
                        document.getElementById('lblEventType').innerHTML = 'TENDER';
                        document.getElementById('hdnEventType').value = 'Tender';
                        document.getElementById('hdnProcureMethod').value = 'FC';
                        $('#publicationDT').html('Scheduled Tender Publication <br/>Date and Time : <span>*</span>');
                        $('#docSellDT').html('Tender Document last selling /<br/>downloading Date and Time : <span>*</span>');            
                        $('#closingDT').html('Tender  Closing <br/> Date and Time : <span>*</span>');            
                        $('#openingDT').html('Tender  Opening <br/> Date and Time : <span>*</span>'); 
                        addtableRow();
                   }
                }
                if ("RFQ" == obj.options[obj.selectedIndex].value) {
                    deltableRow(); 
                    document.getElementById('lblEventType').innerHTML = 'LIMITED ENQUIRY';
                    document.getElementById('hdnEventType').value = 'RFQ';
                    document.getElementById('hdnProcureMethod').value = 'RFQ';
                    $('#publicationDT').html('LEM Publication <br/>Date and Time : <span>*</span>');
                    $('#docSellDT').html('Document last selling /<br/>downloading Date and Time : <span>*</span>');
                    $('#closingDT').html('Closing <br/> Date and Time : <span>*</span>');  
                    $('#openingDT').html('Opening <br/> Date and Time : <span>*</span>'); 
                }
                if ("DPM" == obj.options[obj.selectedIndex].value) {
                   if(document.getElementById('hdnProcureMethod').value == 'RFQ') 
                   { 
                        $('#publicationDT').html('Scheduled Tender Publication <br/>Date and Time : <span>*</span>');
                        $('#docSellDT').html('Tender Document last selling /<br/>downloading Date and Time : <span>*</span>');            
                        $('#closingDT').html('Tender Closing <br/> Date and Time : <span>*</span>');            
                        $('#openingDT').html('Tender Opening <br/> Date and Time : <span>*</span>'); 
                        document.getElementById('lblEventType').innerHTML = 'TENDER';
                        document.getElementById('hdnEventType').value = 'Tender';
                        document.getElementById('hdnProcureMethod').value = 'DPM'; 
                        addtableRow();
                    }
                }
            } 
        }

        function documentPrice(obj) {
            document.getElementById('preQualDocPriceInWords').innerHTML = '';
            if (!required(obj.value)) {
                document.getElementById('spantxtpreQualDocPriceman').innerHTML = '';
               // document.getElementById('spantxtpreQualDocPriceman').innerHTML = '<div class="reqF_1">Please enter Tender/Proposal Document Price</div>';
              //  return 'false';
            } else if (!numeric($.trim(obj.value))) {
                document.getElementById('spantxtpreQualDocPriceman').innerHTML = '<div class="reqF_1">Please enter only numeric values</div>';
                return 'false';
            } else {
                var docPrice = obj.value.split('.');
                if (docPrice[0] == '0')
                {
                    //document.getElementById('spantxtpreQualDocPriceman').innerHTML = '<div class="reqF_1">Only 0 value is not allowed</div>';
                    //return 'false';
                } else
                {
                    /*  Start Dohatec - rokib */
                    var docPriceBDT = obj.value;
                    if (parseFloat(docPriceBDT) > parseFloat(document.getElementById('docPriceBDTMaxID').value))
                    {
                        document.getElementById('spantxtpreQualDocPriceman').innerHTML = '<div class="reqF_1">Tender Document Price (In Nu.) can not be greater then ' + document.getElementById('docPriceBDTMaxID').value + ' </div>';
                        return 'false';
                    } else
                    {
                        /*  End Dohatec - rokib   */
                        document.getElementById('spantxtpreQualDocPriceman').innerHTML = '';
                        obj.value = ($.trim(obj.value) * 1);
                        document.getElementById('preQualDocPriceInWords').innerHTML = WORD(obj.value);
                    }
                }
            }
        }
        // Dohatec Start
        function documentPriceUSD(obj) {
            document.getElementById('preQualDocPriceICTInWords').innerHTML = '';
            if (!required(obj.value)) {
                document.getElementById('spantxtpreQualDocPricemanICT').innerHTML = '';
               // document.getElementById('spantxtpreQualDocPricemanICT').innerHTML = '<div class="reqF_1">Please enter Tender/Proposal Document Price</div>';
               // return 'false';
            } else if (!numeric($.trim(obj.value))) {
                document.getElementById('spantxtpreQualDocPricemanICT').innerHTML = '<div class="reqF_1">Please enter only numeric values</div>';
                return 'false';
            } else {
                var docPrice = obj.value.split('.');

                if (docPrice[0] == '0')
                {
                    //document.getElementById('spantxtpreQualDocPricemanICT').innerHTML = '<div class="reqF_1">Only 0 value is not allowed</div>';
                    //return 'false';
                } else
                {
                    /*  Start Dohatec  - rokib */
                    var docPriceICT = obj.value;
                    if (parseFloat(docPriceICT) > parseFloat(document.getElementById('docPriceUSDMaxID').value))
                    {
                        document.getElementById('spantxtpreQualDocPricemanICT').innerHTML = '<div class="reqF_1">Equivalent Tender Document Price (In USD) can not be greater then ' + document.getElementById('docPriceUSDMaxID').value + ' </div>';
                        return 'false';
                    } else
                    {
                        /*  End Dohatec   - rokib */
                        document.getElementById('spantxtpreQualDocPricemanICT').innerHTML = '';
                        obj.value = ($.trim(obj.value) * 1);
                        document.getElementById('preQualDocPriceICTInWords').innerHTML = WORD(obj.value);
                    }

                }
            }
        }

        function tenderSecAmtUSD(obj) {
            var temp = obj.id.split("_");
            var countTSA = temp[1];
            $(".tenderSecAmtInWordsUSD_" + countTSA).remove();
            $(obj).parent().append("<div class='tenderSecAmtInWordsUSD_" + countTSA + "'>" + WORD(obj.value) + "</div>");
        }

        // Dohatec End

        function tenderSecAmt(obj) {
            var temp = obj.id.split("_");
            var countTSA = temp[1];
            $(".tenderSecAmtInWords_" + countTSA).remove();
            $(obj).parent().append("<div class='tenderSecAmtInWords_" + countTSA + "'>" + WORD(obj.value) + "</div>");
        }

        function docFeesWord(obj) {
            var temp = obj.id.split("_");
            var countDF = temp[1];
            $(".docFeesInWords_" + countDF).remove();
            $(obj).parent().append("<div class='docFeesInWords_" + countDF + "'>" + WORD(obj.value) + "</div>");
        }
        
        /*function MakePreBidEndDateDisable()
        {
            if(document.getElementById("txtpreTenderMeetStartDate").value == "")
            {
                document.getElementById("txtpreTenderMeetEndDate").style.visibility = 'hidden';
                document.getElementById("PreBidEndDateTag").style.visibility = 'hidden';
                document.getElementById("txtpreTenderMeetEndDateimg").style.visibility = 'hidden';
            }
            else
            {
                document.getElementById("txtpreTenderMeetEndDate").style.visibility = 'visible';
                document.getElementById("PreBidEndDateTag").style.visibility = 'visible';
                document.getElementById("txtpreTenderMeetEndDateimg").style.visibility = 'visible';
            }
        }*/
        /*function ClearPreBidStartDate()
        {
            document.getElementById("txtpreTenderMeetStartDate").value = "";
            document.getElementById("txtpreTenderMeetEndDate").value = "";
            document.getElementById("spantxtpreTenderMeetEndDate").innerHTML = '';
            //MakePreBidEndDateDisable();
        }*/
        function ShowPreBidMeetingDates()
        {
            if(document.getElementById("SetPreBidMeetingCheckbox").checked)
            {
                document.getElementById("PreBidMeetingDates").style.display = 'table-row';
                if(document.getElementById("txttenderpublicationDate").value!='' /*&& document.getElementById("txtpreTenderMeetStartDate").value==''*/)
                {
                    var PubDate = document.getElementById("txttenderpublicationDate").value;
                    var dt   = parseInt(PubDate.substring(0,2));
                    var mon  = parseInt(PubDate.substring(3,5));
                    var yr   = parseInt(PubDate.substring(6,10));
                    var hour = parseInt(PubDate.substring(11,13));
                    var min = parseInt(PubDate.substring(14,16));
                    var PreBidMeetingStartDate = new Date(yr, mon-1, dt, hour, min);
                    
                    if(document.getElementById("txtpreQualCloseDate").value!='')
                    {
                        var ClosingDate = document.getElementById("txtpreQualCloseDate").value;
                        dt   = parseInt(ClosingDate.substring(0,2));
                        mon  = parseInt(ClosingDate.substring(3,5));
                        yr   = parseInt(ClosingDate.substring(6,10));
                        hour = parseInt(ClosingDate.substring(11,13));
                        min = parseInt(ClosingDate.substring(14,16));
                        var PreBidMeetingEndDate = new Date(yr, mon-1, dt, hour, min);
                        var DateDifferenceInMiliseconds = PreBidMeetingEndDate.getTime()-PreBidMeetingStartDate.getTime();
                        PreBidMeetingEndDate = new Date(PreBidMeetingStartDate.getTime()+(2/3)*DateDifferenceInMiliseconds);
                        var EndDay;
                        var EndMonth;
                        var EndYear;
                        var EndHour;
                        var EndMinute;
                        if(PreBidMeetingEndDate.getDate()<10)
                        {
                            EndDay = '0'+PreBidMeetingEndDate.getDate();
                        }
                        else
                        {
                            EndDay = PreBidMeetingEndDate.getDate();
                        }
                        if((PreBidMeetingEndDate.getMonth()+1)<10)
                        {
                            EndMonth = '0'+(PreBidMeetingEndDate.getMonth()+1);
                        }
                        else
                        {
                            EndMonth = (PreBidMeetingEndDate.getMonth()+1);
                        }
                        EndYear = PreBidMeetingEndDate.getFullYear();
                        if(PreBidMeetingEndDate.getHours()<10)
                        {
                            EndHour = '0'+PreBidMeetingEndDate.getHours();
                        }
                        else
                        {
                            EndHour = PreBidMeetingEndDate.getHours();
                        }
                        if(PreBidMeetingEndDate.getMinutes()<10)
                        {
                            EndMinute = '0'+PreBidMeetingEndDate.getMinutes();
                        }
                        else
                        {
                            EndMinute = PreBidMeetingEndDate.getMinutes();
                        }
                        var PreBidMeetingEndDateStr = EndDay+ '/' + EndMonth + '/' + EndYear + ' ' + EndHour + ':' + EndMinute;
                        document.getElementById("txtpreTenderMeetEndDate").value = PreBidMeetingEndDateStr;
                        document.getElementById("spantxtpreTenderMeetEndDate").innerHTML = '';
                    }   
                    
                    
                    PreBidMeetingStartDate.setDate(PreBidMeetingStartDate.getDate() + 7);
                    
                    var StartDay;
                    var StartMonth;
                    var StartYear;
                    var StartHour;
                    var StartMinute;
            
                    if(PreBidMeetingStartDate.getDate()<10)
                    {
                        StartDay = '0'+PreBidMeetingStartDate.getDate();
                    }
                    else
                    {
                        StartDay = PreBidMeetingStartDate.getDate();
                    }
                    if((PreBidMeetingStartDate.getMonth()+1)<10)
                    {
                        StartMonth = '0'+(PreBidMeetingStartDate.getMonth()+1);
                    }
                    else
                    {
                        StartMonth = (PreBidMeetingStartDate.getMonth()+1);
                    }
                    StartYear = PreBidMeetingStartDate.getFullYear();
                    if(PreBidMeetingStartDate.getHours()<10)
                    {
                        StartHour = '0'+PreBidMeetingStartDate.getHours();
                    }
                    else
                    {
                        StartHour = PreBidMeetingStartDate.getHours();
                    }
                    if(PreBidMeetingStartDate.getMinutes()<10)
                    {
                        StartMinute = '0'+PreBidMeetingStartDate.getMinutes();
                    }
                    else
                    {
                        StartMinute = PreBidMeetingStartDate.getMinutes();
                    }
                    var PreBidMeetingStartDateStr = StartDay+ '/' + StartMonth + '/' + StartYear + ' ' + StartHour + ':' + StartMinute;
                    document.getElementById("txtpreTenderMeetStartDate").value = PreBidMeetingStartDateStr;
                    document.getElementById("spantxtpreTenderMeetStartDate").innerHTML = '';
                }
            }
            else
            {
                document.getElementById("PreBidMeetingDates").style.display = 'none';
                document.getElementById("txtpreTenderMeetStartDate").value = "";
                document.getElementById("txtpreTenderMeetEndDate").value = "";
                document.getElementById("spantxtpreTenderMeetEndDate").innerHTML = '';
                document.getElementById("spantxtpreTenderMeetStartDate").innerHTML = '';
            }
        }
        function EnterPreBidMeetingStartDateFirst()
        {
            if(document.getElementById("txtpreTenderMeetStartDate").value == "")
            {
                document.getElementById("spantxtpreTenderMeetEndDate").innerHTML = '<div class="reqF_1">Enter Pre Bid meeting start date first.</div>';
            }
            else
            {
                document.getElementById("spantxtpreTenderMeetEndDate").innerHTML = '';
                GetCal('txtpreTenderMeetEndDate', 'txtpreTenderMeetEndDate');
            }
        }
//        function EnterPreBidStartDateFirst()
//        {
//            if(document.getElementById("txtpreTenderMeetStartDate").value == "")
//            {
//                document.getElementById("EnterPreBidStartDateFirstSpan").innerHTML = "Enter Pre Bid meeting start date first.";
//            }
//            else
//            {
//                document.getElementById("EnterPreBidStartDateFirstSpan").innerHTML = "";
//            }
//        }

    </script>


</html>
