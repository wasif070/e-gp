<%-- 
    Document   : SrVariOrder
    Created on : Dec 22, 2011, 3:13:03 PM
    Author     : shreyansh Jogi
--%>
<%@page import="java.math.RoundingMode"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSrvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTcvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSsvari"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvStaffSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTeamComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <% ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Variation Order</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <%
                    String referpage = request.getHeader("referer");
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String type = "";
                    int tenderId = 0;
                    int lotId = 0;
                    int formMapId = 0;
                    int srvBoqId = 0;
                    int varId = 0;
                    String styleClass = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    if (request.getParameter("varId") != null) {
                        varId = Integer.parseInt(request.getParameter("varId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                    }
                    if (request.getParameter("formMapId") != null) {
                        formMapId = Integer.parseInt(request.getParameter("formMapId"));
                    }
                    if (request.getParameter("srvBoqId") != null) {
                        srvBoqId = Integer.parseInt(request.getParameter("srvBoqId"));
                    }
                    List<TblCmsSrvSrvari> tcsts = cmss.getDetailOfSRForVari(varId);
                    List<TblCmsSrvTcvari> tcvari = cmss.getDetailOfTCForVari(varId);
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));

        %>
        <script>
            var count = 0;
        </script>
        <%if (request.getParameter("isEdit") != null) {%>
        <script>

            count = <%=tcsts.size()%>
        </script>
        <%} else {%>
        <script>
            count = 0;
        </script>
        <%}%>
        <script>
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function positive(value)
            {
                return /^\d*$/.test(value);

            }
            function getDate(date)
            {
                var splitedDate = date.split("-");
                return Date.parse(splitedDate[1]+" "+splitedDate[0]+", "+splitedDate[2]);
            }
           
            function checkDays(id)
            {
                $('.err').remove();
                if(!regForNumber(document.getElementById('month_'+id).value)){
                    $("#month_"+id).parent().append("<div class='err' style='color:red;'>Month must be numeric value</div>");
                    document.getElementById('month_'+id).value="";
                }else if(document.getElementById('month_'+id).value.split(".")[1] != undefined){
                    if(!regForNumber(document.getElementById('month_'+id).value) || document.getElementById('month_'+id).value.split(".")[1].length > 2 || document.getElementById('month_'+id).value.split(".")[0].length > 5){
                        $("#month_"+id).parent().append("<div class='err' style='color:red;'>Maximum 5 digits are allowed and 2 digits after decimal</div>");
                        document.getElementById('month_'+id).value = "";
                    }
                }else if(document.getElementById('month_'+id).value.indexOf("-")!=-1){
                    $("#month_"+id).parent().append("<div class='err' style='color:red;'>Only positive values are allowed</div>");
                    document.getElementById('month_'+id).value = "";
                    vbool = false;
                }
            }
            function checkRate(id){
                $('.err').remove();
                if(!regForNumber(document.getElementById('rate_'+id).value)){
                    $("#rate_"+id).parent().append("<div class='err' style='color:red;'>Only numeric values are allowed</div>");
                    document.getElementById('rate_'+id).value="";
                } else if(document.getElementById('rate_'+id).value.indexOf("-")!=-1){
                    $("#rate_"+id).parent().append("<div class='err' style='color:red;'>Only positve values are allowed</div>");
                    document.getElementById('rate_'+id).value="";
                }
                else if(document.getElementById('rate_'+id).value != undefined){
                    if(document.getElementById('rate_'+id).value.split(".")[1] == undefined || document.getElementById('rate_'+id).value.split(".")[0] == undefined)
                    {
                        if(document.getElementById('rate_'+id).value.length > 12)
                        {
                            $("#rate_"+id).parent().append("<div class='err' style='color:red;'><%=bdl.getString("CMS.var.ratecheck")%></div>");
                            document.getElementById('rate_'+id).value="";
                        }
                    }
                    else if(document.getElementById('rate_'+id).value.split(".")[1].length > 3 || document.getElementById('rate_'+id).value.split(".")[0].length > 12){
                        $("#rate_"+id).parent().append("<div class='err' style='color:red;'><%=bdl.getString("CMS.var.ratecheck")%></div>");
                        document.getElementById('rate_'+id).value="";
                    }
                }
                countnewitemTotal();
            }

            function addrow()
            {
                var newTxt="";
            <%if (!tcvari.isEmpty()) {
                            for (TblCmsSrvTcvari tcst : tcvari) {%>
                                    newTxt = '<tr><td class="t-align-left">\n\
        <input class="formTxtBox_1" type="checkbox" name="chk'+count+' " id="chk'+count+'" /></td>'+
                                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="sno_'+count+'" style=width:40px id="sno_'+count+'" /></td>'+
                                        '<td class="t-align-left"><label><%=tcst.getEmpName()%></label></td>'+
                                        '<td class="t-align-left"><label><%=tcst.getEmpName()%></label></td>'+
                                        '<td class="t-align-left"><%=tcst.getEmpName()%></td>'+
                                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" onChange=checkRate('+count+') name="rate_'+count+'"  id="rate_'+ count+'" /></td>'+
                                        '</tr>';
            <%}
                        }%>
                

                                $("#resultTable tr:last").after(newTxt);
                                count++;
                            }
        </script>
        <%if (request.getParameter("isEdit") != null) {%>
        <script>
            function delRow(){
                var listcount = count;
                var trackid="";
                var counter = 0;
                for(var i=0;i<listcount;i++){
                    if(document.getElementById("chk"+i)!=null){
                        if(document.getElementById("chk"+i).checked){
                            if(document.getElementById("varDtlId"+i)!=null){
                                trackid=trackid+document.getElementById("varDtlId"+i).value+",";
                                document.getElementById("count").value = listcount-1;
                            }
                        }
                    }
                }
                $(":checkbox[checked='true']").each(function(){
                    if(document.getElementById("count")!= null){
                        var curRow = $(this).parent('td').parent('tr');
                        curRow.remove();
                        count--;
                        counter++;
                    }
                });
                if(counter==0){
                    jAlert("Please select at least one item to remove","Variation Order", function(RetVal) {
                    });
                }else{
                    $.post("<%=request.getContextPath()%>/CMSSerCaseServlet", {action:'deleterowforvariationForSR', val: trackid,tenderId:<%=request.getParameter("tenderId")%> },  function(j){
                        jAlert("Selected item(s) deleted successfully","Variation Order", function(RetVal) {
                        });

                    });
                }


            }
        </script>
        <%} else {%>
        <script>

            function delRow(){
                var counter = 0;
                $(":checkbox[checked='true']").each(function(){

                    var curRow = $(this).parent('td').parent('tr');
                    curRow.remove();
                    count--;
                    counter++;

                });
                if(counter==0){
                    jAlert("Please select at least one item to remove","Variation Order", function(RetVal) {
                    });
                }else{
                    calculateGrandTotal();
                }
            }
        </script>
        <%}%>
        <script>
            function onFire(){
                var flag = true;
                var count = document.getElementById("count").value;
                for(var i=0;i<count;i++){
                    if(document.getElementById("sno_"+i).value=="" || document.getElementById("rate_"+i).value=="" ){
                        jAlert("It is necessary to enter all data","Variation Order", function(RetVal) {
                        });
                        flag=false;
                    }
                }
                if(flag){
                    document.forms["frm"].submit();
                }else{
                    return false;
                }

            }
            function countnewitemTotal(){
                var total = 0;
                if(document.getElementById("maincount").value > 0){
                                     for(var i = 0; i<document.getElementById("maincount").value ; i++){
                                         if(document.getElementById("sal_"+i)!=null){
                                             total = eval(total) + eval(document.getElementById("sal_"+i).value);
                                         }
                                     }
                                }
                if(document.getElementById("count").value > 0){
                                     for(var i = 0; i<document.getElementById("count").value ; i++){
                                         if(document.getElementById("rate_"+i).value!=""){
                                             total = eval(total) +eval(document.getElementById("rate_"+i).value);
                                         }
                                    }
                                }
                                document.getElementById("total").innerHTML = total;
            }


        </script>
    </head>
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">
                        <%
                                    if (request.getParameter("tenderId") != null) {
                                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                                    }
                        %>
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="<%=referpage%>" title="Go Back">Go Back</a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%if (request.getParameter("lotId") != null) {%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <%}%>
                    <body onload="countnewitemTotal();">
                        <form name="frm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post" >
                            <%if (request.getParameter("isEdit") != null) {%>
                            <input type="hidden" name="isEdit" value ="SRvari" />
                            <%}%>
                            <input type="hidden" name="action" value ="SRvari" />
                            <input type="hidden" name="srvFormMapId" value ="<%=formMapId%>" />
                            <input type="hidden" name="tenderId" value ="<%=request.getParameter("tenderId")%>" />
                            <input type="hidden" name="varId" value ="<%=request.getParameter("varId")%>" />
                            <%
                                if(request.getParameter("isEdit")!=null){
                            %>
                                <input type="hidden" name="isEdit" id="isEdit" value="true" />
                            <%
                                }
                            %>
                            <br />
                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                <tr>

                                    <th width="4%">Sl. No.</th>
                                    <th width="60%">Name of Employees </th>
                                    <th width="16%">Home / Field</th>
                                    <th width="10%">Staff Months</th>
                                    <th width="10%">Staff Month Rate</th>
                                </tr>
                                <%
                                            boolean show = true;
                                            List<TblCmsSrvSalaryRe> list = cmss.getSalaryReimbursData(formMapId);
                                            if (!list.isEmpty()) {
                                                int cc = 0;
                                                for (int i = 0; i < list.size(); i++) {

                                %>
                                <tr>

                                    <td class="t-align-left"><%=list.get(i).getSrNo()%></td>
                                    <td class="t-align-left"><%=list.get(i).getEmpName()%></td>
                                    <td class="t-align-left"><%=list.get(i).getWorkFrom()%></td>
                                    <td style="text-align :right;"><%=list.get(i).getWorkMonths()%></td>
                                    <td style="text-align :right;"><%=list.get(i).getEmpMonthSalary()%></td>
                                <input type="hidden" name="sal_<%=i%>" id="sal_<%=i%>" value="<%=list.get(i).getEmpMonthSalary()%>" />
                                </tr>
                                
                                <%cc++;}%>
                                <input type="hidden" id="maincount" name="maincount" value="<%=cc%>" />
                                            <%}%>
                                <%
                                            if (request.getParameter("isEdit") == null) {
                                                int ij = 0;
                                                if (!tcvari.isEmpty()) {
                                                    for (TblCmsSrvTcvari tcst : tcvari) {
                                                        List<Object[]> data = cmss.getSalReDataFromTCAndSS(tcst.getSrvTcvariId(), tcst.getVariOrdId());
                                                        if (data != null && !data.isEmpty()) {
                                                            show = false;
                                                            for (Object[] obsd : data) {
                                %>
                                <tr>
                                    <td class="t-align-left"><input class="formTxtBox_1" type="text" name="sno_<%=ij%>" style=width:40px id="sno_<%=ij%>" /></td>
                                    <td class="t-align-left"><label><%=tcst.getEmpName()%></label></td>
                                    <td class="t-align-left"><label><%=obsd[0]%></label></td>
                                    <td style="text-align: right"><%=new BigDecimal(obsd[1].toString()).divide(new BigDecimal(30), 2, RoundingMode.HALF_UP)%></td>
                                    <td class="t-align-left"><input class="formTxtBox_1" type="text" onChange=checkRate(<%=ij%>) name="rate_<%=ij%>"  id="rate_<%=ij%>" /></td>
                                </tr>
                                <input type="hidden" name="name_<%=ij%>" id="name_<%=ij%>" value="<%=tcst.getSrvTcvariId()%>" />
                                <input type="hidden" name="month_<%=ij%>" id="month_<%=ij%>" value="<%=new BigDecimal(obsd[1].toString()).divide(new BigDecimal(30), 2, RoundingMode.HALF_UP)%>" />
                                <input type="hidden" name="WF_<%=ij%>" id="WF_<%=ij%>" value="<%=obsd[0]%>" />
                                <input type="hidden" name="ename_<%=ij%>" id="ename_<%=ij%>" value="<%=tcst.getEmpName()%>" />
             

                                <% ij++;
                                                                                            }
                                                                                        }
                                                                                    }%>

                                <input type="hidden" id="count" name="count" value="<%=ij%>" />
                                <%                    }
                                                                            } else {

                                                                                for (int i = 0; i < tcsts.size(); i++) {
                                                                                    show = false;
                                %>
                                <tr>
                                <input type="hidden" name="varDtlId<%=i%>" id="varDtlId<%=i%>" value="<%=tcsts.get(i).getSrvSrvariId()%>" />

                                <td class="t-align-left"><input class="formTxtBox_1" type="text" value="<%=tcsts.get(i).getSrNo()%>" name="sno_<%=i%>" style=width:40px id="sno_<%=i%>" /></td>
                                <td class="t-align-left"><%=tcvari.get(i).getEmpName()%></td><!-- Doubt -->
                                <td class="t-align-left"><%=tcsts.get(i).getWorkFrom()%></td>
                                <td style="text-align: right"><%=tcsts.get(i).getWorkMonths().setScale(2, 0)%></td>
                                <td class="t-align-left"><input  value="<%=tcsts.get(i).getEmpMonthSalary().setScale(3, 0)%>" class="formTxtBox_1" type="text" name="rate_<%=i%>" onchange="checkRate(<%=i%>);" id="rate_<%=i%>"  /></td>
                                </tr>
                                <input type="hidden" name="name_<%=i%>" id="name_<%=i%>" value="<%=tcsts.get(i).getSrvTcvariId()%>" />
                                <input type="hidden" name="month_<%=i%>" id="month_<%=i%>" value="<%=tcsts.get(i).getWorkMonths()%>" />
                                <input type="hidden" name="WF_<%=i%>" id="WF_<%=i%>" value="<%=tcsts.get(i).getWorkFrom()%>" />
                                <input type="hidden" name="ename_<%=i%>" id="ename_<%=i%>" value="<%=tcsts.get(i).getEmpName()%>" />
                                <%}%>
                                <%
                                                                                List<Object[]> remList = cmss.getSalRemDataFrmSSandSR(varId);
                                                                                int k = tcsts.size();
                                                                                if (remList != null && !remList.isEmpty()) {
                                                                                    show = false;
                                                                                    for (Object[] os : remList) {
                                                                                        List<Object[]> data = cmss.getSalReDataFromTCAndSS((Integer) os[3], (Integer) os[2]);
                                                                                        if (data != null && !data.isEmpty()) {
                                                                                            for (Object[] obsd : data) {
                                %>
                                <tr>
                                    <td class="t-align-left"><input class="formTxtBox_1" type="text" value="" name="sno_<%=k%>" style=width:40px id="sno_<%=k%>" /></td>
                                    <td class="t-align-left"><%=os[0]%></td>
                                    <td class="t-align-left"><%=obsd[0]%></td>
                                    <td style="text-align: right"><%=new BigDecimal(obsd[1].toString()).divide(new BigDecimal(30), 2, RoundingMode.HALF_UP)%></td>
                                    <td class="t-align-left"><input  value="" class="formTxtBox_1" type="text" name="rate_<%=k%>" onchange="checkRate(<%=k%>);" id="rate_<%=k%>"  /></td>
                                </tr>

                                <input type="hidden" name="name_<%=k%>" id="name_<%=k%>" value="<%=os[3]%>" />
                                <input type="hidden" name="month_<%=k%>" id="month_<%=k%>" value="<%=new BigDecimal(obsd[1].toString()).divide(new BigDecimal(30), 2, RoundingMode.HALF_UP)%>" />
                                <input type="hidden" name="WF_<%=k%>" id="WF_<%=k%>" value="<%=obsd[0]%>" />
                                <input type="hidden" name="ename_<%=k%>" id="ename_<%=k%>" value="<%=os[0]%>" />
                                <%k++;
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }%>
                                <input type="hidden" id="count" name="count" value="<%=k%>" />
                                <%   }%>
                                 <tr>
                                        <td colspan="3"></td>
                                        <td class="ff">Grand Total :</td>
                                        <td class="ff" style="text-align:  right"><label id="total"></lable></td>
                                    </tr>

                            </table>
                            <br />
                            <center>

                                <%if (show) {%>

                                <div  class='t_space responseMsg noticeMsg t-align-left'>It is mandatory to add Employee details in Team Composition and Task Assignments form and also do the necessary changes in Staffing Schedule form</div>
                                <%} else {%>
                                <label class="formBtn_1">
                                    <input type="button" name="Boqbutton" id="Boqbutton" value="Submit" onclick="onFire();" />
                                </label>
                                <%}%>


                            </center>
                        </form>

                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </body>
</html>

