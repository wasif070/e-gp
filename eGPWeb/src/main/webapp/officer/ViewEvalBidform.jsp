<%--
    Document   : BidForm
    Created on : Nov 16, 2010, 5:18:59 PM
    Author     : Sanjay
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateCells"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateColumns"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateTables"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Preparation</title>
        <%String contextPath = request.getContextPath();%>

        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />

        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/FormulaCalculation.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/Add.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/GetHash.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

<%
String isPostQue = "";
                if(!"".equalsIgnoreCase(request.getParameter("isPostQue"))) {
                    isPostQue = (request.getParameter("isPostQue"));
                }
String isSeek = "";
                if(!"".equalsIgnoreCase(request.getParameter("isSeek"))) {
                    isSeek = (request.getParameter("isSeek"));
                }

                %>

        <jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />
        <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <% if(isPostQue == null) { %>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <% }  %>
                <div class="fixDiv">
                
                    <!--Middle Content Table Start-->
<%
                int userId = 0;
                if(session.getAttribute("userId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                }

                int uId = 0;
                if(!"".equalsIgnoreCase(request.getParameter("uId"))) {
                    uId = Integer.parseInt(request.getParameter("uId"));
                }
                /*int userTypeId = 0;
                if(session.getAttribute("userTypeId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())) {
                        userTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
                    }
                }*/
                int formId = 0;
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }

                int tenderId = 0;
                if (request.getParameter("tenderId") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }

                int bidId = 0;
                if (request.getParameter("bidId") != null) {
                    bidId = Integer.parseInt(request.getParameter("bidId"));
                }

                int lotId = 0;
                if (request.getParameter("lotId") != null) {
                    lotId = Integer.parseInt(request.getParameter("lotId"));
                }

                String action = "";
                if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action"))){
                     action = request.getParameter("action");
                }

                boolean isEdit = false;
                if("Edit".equalsIgnoreCase(action)){
                    isEdit = true;
                }

                boolean isView = false;
                if("View".equalsIgnoreCase(action)){
                    isView = true;
                }

                boolean isEncrypt = false;
                if("Encrypt".equalsIgnoreCase(action)){
                    isEncrypt = true;
                }

                int tableCount = 0;
                tableCount = tenderBidSrBean.getNoOfTable(formId);

                if("".equalsIgnoreCase(action)){
%>
<script>
        var gblCnt =0;
        var isMultiTable = false;
        var arrCompType = new Array(<%=tableCount%>); //Stores the Array of the ComponentTypes of the Table Fields
        var arrCellData = new Array(<%=tableCount%>);//Stores the Array of the CellData of the Table Fields
        var arrRow = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using normal index
        var arrCol = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using normal index
        var arrTableAdded = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables
        var arrTableFormula = new Array(<%=tableCount%>); //Stores the Array of the table formula
        var arrFormulaFor = new Array(<%=tableCount%>); //Stores the Array of the fields where formula is applicable
        var arrIds = new Array(<%=tableCount%>); //Stores the Array of the ids of the cols of the formula
        var brokenFormulaIds = new Array();
        var arrColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table
        var arrStaticColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table which r only txt
        var arrRowsKey = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using directly ths tableid as a key
        var arrColsKey = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using directly ths tableid as a key
        var arrTableAddedKey = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables using directly ths tableid as a key
        var arrDataTypesforCell = new Array(<%=tableCount%>);
        var arrColTotalIds = new Array(<%=tableCount%>);
        var arrColTotalWordsIds = new Array(<%=tableCount%>);	// added for total in words.
        var arrColOriValIds = new Array(<%=tableCount%>);	// added for original value to keep.
        var isColTotalforTable = new Array(<%=tableCount%>);
        for(var i=0;i<isColTotalforTable.length;i++)
                isColTotalforTable[i]=0;
        var arrForLabelDisp = new Array(<%=tableCount%>); //Stores the Array of the col ids of the which Fillby=3,Datatype=2 table
</script>
<%
                    }
                    if(isEdit || isView || isEncrypt){

                        tableCount = tenderBidSrBean.getNoOfTable(formId);
%>
<script>
		verified = false;
                var gblCnt =0;
                chkEdit = true;
                var isMultiTable = false;
                var arrCompType = new Array(<%=tableCount%>); //Stores the Array of the ComponentTypes of the Table Fields
                var arrCellData = new Array(<%=tableCount%>);//Stores the Array of the CellData of the Table Fields
                var arrRow = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using normal index
                var arrCol = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using normal index
                var arrTableAdded = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables
                var arrTableFormula = new Array(<%=tableCount%>); //Stores the Array of the table formula
                var arrFormulaFor = new Array(<%=tableCount%>); //Stores the Array of the fields where formula is applicable
                var arrIds = new Array(<%=tableCount%>); //Stores the Array of the ids of the cols of the formula
                var brokenFormulaIds = new Array();
                var arrColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table
                var arrStaticColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table which r only txt
                var arrRowsKey = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using directly ths tableid as a key
                var arrColsKey = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using directly ths tableid as a key
                var arrTableAddedKey = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables using directly ths tableid as a key
                var arrDataTypesforCell = new Array(<%=tableCount%>);
                var arrColTotalIds = new Array(<%=tableCount%>);
                var arrColTotalWordsIds = new Array(<%=tableCount%>);	// added for total in words.
                var arrColOriValIds = new Array(<%=tableCount%>);	// added for original value to keep.
                var isColTotalforTable = new Array(<%=tableCount%>);
                var totalBidTable = new Array(<%=tableCount%>);
                for(var i=0;i<isColTotalforTable.length;i++)
                        isColTotalforTable[i]=0;
                var arrForLabelDisp = new Array(<%=tableCount%>); //Stores the Array of the col ids of the which Fillby=3,Datatype=2 table

</script>
<%
                    }
%>
<form id="frmBidSubmit" name="frmBidSubmit" method="post" action="NegGetBidData.jsp">
                       
                    <%if(isEdit || isView || isEncrypt){%>
                    <input type="hidden" name="hdnBidId" id="hdnBidId" value="<%=bidId%>">
                    <%}%>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                            <td class="contentArea_1">
<%
        String formName = "";
        String formHeader = "";
        String formFooter = "";
        String isMultipleFormFeeling = "";
        String isEncryption = "";
        String isPriceBid = "";
        String isMandatory = "";

        
        List<CommonFormData> formDetails = tenderBidSrBean.getFormData(formId);
        for(CommonFormData formData : formDetails){
            formName = formData.getFormName();
            formHeader = formData.getFormHeader();
            formFooter = formData.getFormFooter();
            isMultipleFormFeeling = formData.getIsMultipleFormFeeling();
            isEncryption = formData.getIsEncryption();
            isPriceBid = formData.getIsPriceBid();
            isMandatory = formData.getIsMandatory();
        }
%>
                                <div class="t_space">
                                <div class="pageHead_1" id="divFormName">
                                    <%=formName%>
                                    <%--<span style="float: right; text-align: right;">
<% if(isPostQue == null) { %>
<%
        if(lotId==0){
%>
                                        <a class="action-button-goback" href="SeekEvalClari.jsp?tenderId=<%=tenderId%>&uId=<%=uId%>" title="Bid Dashboard">Go Back To Dashboard</a>
<%
        }else{
%>
                                        <a class="action-button-goback" href="NegBidPrepare.jsp?tenderId=<%=tenderId%>&uId=<%=uId%>" title="Bid Dashboard">Go Back To Dashboard</a>
<%
        }
%>
<%  }  %>                                    </span>--%>
                                </div>
                                <div class="t_space t-align-center formStyle_1 ff">
                                                    <%
                                                        ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                                                        CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                                        List<SPCommonSearchDataMore> tendererData = dataMore.geteGPData("getUNameTendererIdCmpId", String.valueOf(uId), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                        String[] jvSubData = creationService.jvSubContractChk(String.valueOf(tenderId), String.valueOf(uId));
                                                        StringBuilder name_link = new StringBuilder();
                                                        name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewRegistrationDetail.jsp?uId="+uId+"&tId="+tendererData.get(0).getFieldName3()+"&cId="+tendererData.get(0).getFieldName2()+"&top=no' target='_blank'>"+tendererData.get(0).getFieldName1()+"</a>");
                                                        if(jvSubData[0]!=null && jvSubData[0].equals("jvyes")){
                                                            name_link.delete(0, name_link.length());
                                                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+uId+"' target='_blank'>"+tendererData.get(0).getFieldName1()+"</a>");
                                                            name_link.append("<br/>");
                                                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+uId+"' target='_blank' style='color:red'>(JVCA - View Details)</a>");
                                                        }
                                                        if(jvSubData[1]!=null && jvSubData[1].equals("subyes")){
                                                            name_link.append("<br/>");
                                                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewTenderSubContractor.jsp?uId="+uId+"&tenderId="+tenderId+"&tId="+tendererData.get(0).getFieldName3()+"&cId="+tendererData.get(0).getFieldName2()+"' target='_blank' style='color:red'>(Sub Contractor/Consultant - View Details)</a>");
                                                        }
                                                        out.print(name_link);
                                                    %>
                                                 </div>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td>
<%
        if(isEncrypt)
        {
%>
                                            <input type="hidden" name="hdnBuyerPwd" id="hdnBuyerPwd" value="<%=tenderBidSrBean.getBuyerPwdHash(tenderId)%>">
<%
        }
%>
                                            <input type="hidden" name="hdnTenderId" id="hdnTenderId" value="<%=tenderId%>">
                                            <input type="hidden" name="hdnFormId" id="hdnFormId" value="<%=formId%>">
                                            <input type="hidden" name="hdnLotId" id="hdnLotId" value="<%=lotId%>">
<%
        int tableId = 0;
        int tblCnt1 = 0;
        short cols = 0;
        short rows = 0;
        String tableName = "";
        String isMultiTable = "";

        List<CommonFormData> formTables = tenderBidSrBean.getFormTables(formId);
        for(CommonFormData formData : formTables){
            tableId = formData.getTableId();

            List<CommonFormData> tableInfo = tenderBidSrBean.getFormTablesDetails(tableId);
            if (tableInfo != null) {
                if (tableInfo.size() >= 0) {
                    tableName = tableInfo.get(0).getTableName();
                    cols = tableInfo.get(0).getNoOfCols();
                    rows = tableInfo.get(0).getNoOfRows();
                    isMultiTable = tableInfo.get(0).getIsMultipleFilling();
                }
                tableInfo = null;
            }

            cols = (tenderBidSrBean.getNoOfColsInTable(tableId)).shortValue();
            rows = (tenderBidSrBean.getNoOfRowsInTable(tableId, (short) 1)).shortValue();

%>
<script>
        chkBidTableId.push(<%=tableId%>);
        arrBidCount.push(1);
        // Setting TableId in Array
        arr[<%=tblCnt1%>]=<%=tableId%>;
        // Setting TableIdwise No of Rows in Array
        arrRow[<%=tblCnt1%>]=<%=rows%>;
        //alert('tablecnt : <%=tblCnt1%>')
        //alert('<%=rows%>');
        arrRowsKey[<%=tableId%>]=<%=rows%>;
        // Setting TableIdwise No of Cols in Array
        arrCol[<%=tblCnt1%>]=<%=cols%>;
        arrColsKey[<%=tableId%>]=<%=cols%>;
        // Setting TableIdwise No of Tables in Added
        arrTableAdded[<%=tblCnt1%>]=<%=1%>;
        arrTableAddedKey[<%=tableId%>]=<%=1%>;
        arrColTotalIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        arrColTotalWordsIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        arrColOriValIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        for(var i=0;i<arrColTotalIds[<%=tblCnt1%>].length;i++){
                arrColTotalIds[<%=tblCnt1%>][i] = 0;
                arrColTotalWordsIds[<%=tblCnt1%>][i] = 0;
                arrColOriValIds[<%=tblCnt1%>][i] = 0;
        }
</script>
                                                <div id="divMsg" class="responseMsg successMsg" style="display:none">&nbsp;</div>                                                
                                                <table width="100%" cellspacing="10" class="tableList_1 t_space">
                                                    <tr>
                                                        <td width="100" class="ff">Table Name : </td>
                                                        <td><%=tableName%></td>
                                                        <td class="t-align-right">
<%
        if("yes".equalsIgnoreCase(isMultiTable)){
%>
                                                            <script>
                                                                isMultiTable = true;
                                                            </script>
                                                            <!--label <%if(isEdit){%>class="formBtn_disabled"<%}else if(!(isEdit || isView || isEncrypt)){%>class="bidFormBtn_1"<%}%> style="float: right" id="lblAddTable<%=tableId%>"
                                                            <%if(isView){%>style="display:none" <%}%>>
                                                                <input type="button" name="btn<%=tableId%>" id="bttn<%=tableId%>" value="Add Table" onClick="AddTable(this.form,<%=tableId%>,this)"
                                                                       <%if(isEdit){%>
                                                                       disabled="true"
                                                                       <%}%>
                                                                       <%if(isView || isEncrypt){%>style="display:none" <%}%>
                                                                       />
                                                            </label-->
<%
        }
%>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <jsp:include page="ViewEvalBidformTable.jsp" flush="true" >
                                                    <jsp:param name="tableId" value="<%=tableId%>" />
                                                    <jsp:param name="cols" value="<%=cols%>" />
                                                    <jsp:param name="rows" value="<%=rows%>" />
                                                    <jsp:param name="TableIndex" value="<%=tblCnt1%>" />
                                                    <jsp:param name="isMultiTable" value="<%=isMultiTable%>" />
                                                    <jsp:param name="uId" value="<%=uId%>" />
                                                </jsp:include>
<script>
        var totalInWordAt = -1;
        try{
            totalInWordAt = TotalWordColId
            if(totalInWordAt < 0){
                totalInWordAt = -1;
            }
        }catch(err){
            totalInWordAt = -1;
        }
</script>
<%if(tblCnt1 >= 0){%>
<script>
        if(arrStaticColIds[<%=tblCnt1%>].length > 0)
	{
            for(var j=0;j<arr.length;j++)
            {
                LoopCounter++;
            }
	}
        breakFormulas();
        checkForFunctions();
</script>
<%}%>
<%
            tblCnt1++;
            formData = null;
        }
%>
<script>
    breakFormulas();
    checkForFunctions();
</script>
<%
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                        List<SPCommonSearchData> DocList = commonSearchService.searchData("getTenderDocForIndi", String.valueOf(tenderId), String.valueOf(formId),String.valueOf(uId), null, null, null, null, null, null) ;
                        if (!DocList.isEmpty()) {
                            int cnt =1;
                    %>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="ff">Sl. No.</th>
                        <th width="36%" class="ff">Mapped Document Name</th>
                        <th width="35%" class="ff">File Name</th>
                        <th width="15%" class="ff">File Size(In Kb)</th>
            <!--                                <th width="45%" class="t-align-left ff">Description</th>-->
                        <th width="10%" class="ff">Action</th>
                    </tr>
               <%for (SPCommonSearchData mapedDL : DocList){  %>
               <tr>
                   <td class="t-align-center"> <%=cnt%></td>
                   <td class="t-align-center"><%=mapedDL.getFieldName1()%></td>
                   <td class="t-align-center"> <%=mapedDL.getFieldName2()%></td>
                   <td class="t-align-center"> <%=new BigDecimal(Double.parseDouble((mapedDL.getFieldName3()))/1024).setScale(2, 0)%></td>
            <!--       <td class="t-align-left ff"> < %=mapedDL.getFieldName4()%></td>-->
                   <td class="t-align-center"><a class="action-button-download" href="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=download&fileName=<%=mapedDL.getFieldName2()%>&fileLen=<%=mapedDL.getFieldName3()%>&docUid=<%=mapedDL.getFieldName5()%>">Download</a></td>
              </tr>
              <%cnt++;}}%>
               </table>
                                        </td>
                                    </tr>
                                    <% if (isPostQue == null && isSeek == null) {%>
                                    <tr>
                                        <td align="center">
                                            <label class="formBtn_1 t_space">
                                                <input type="submit" name="save" id="save" value="Update"/>
                                            </label>
                                        </td>
                                    </tr>
                                    <% } %>
                                </table>
                                </div>
                            </td>
                        </tr>
                    </table>                    
                </form>
                </div>
                <div id="dialog-form" title="Enter Password">
<!--                    <form>-->
                            <label for="password">Password : </label>
                            <input type="password" name="password" id="password" value="" class="formTxtBox_1" autocomplete="off" />
                            <br/>
                            <p align="center" class="validateTips"></p>
<!--                    </form>-->
                </div>
                <div>
                    <input type="hidden" name="hdnPwd" id="hdnPwd"/>
                </div>
                <% if(isPostQue == null) { %>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <% } %>
            </div>
        </div>
    </head>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

<script>
$( "#dialog:ui-dialog" ).dialog( "destroy" );

var password = $( "#password" ),
    allFields = $( [] ).add( password ),
    tips = $( ".validateTips" );

var check = false;
var encryptFlag = false;
function updateTips( t ) {
    tips
            .text( t )
            .addClass( "ui-state-highlight" );
    setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 1500 );
    }, 500 );
}

function checkLength( o, n, min, max ) {
        if(o.val()==""){
            o.addClass( "ui-state-error" );
            updateTips("Please enter password");
        }else{
            if ( o.val().length > max || o.val().length < min ) {
                    o.addClass( "ui-state-error" );
                    updateTips( "Length of " + n + " must be between " +
                            min + " and " + max + "." );
                    return false;
            } else {
                    return true;
            }
        }
}

function checkRegexp( o, regexp, n ) {
        if ( !( regexp.test( o.val() ) ) ) {
                o.addClass( "ui-state-error" );
                updateTips( n );
                return false;
        } else {
                return true;
        }
}

$("#dialog-form").dialog({
        autoOpen: false,
        height: 180,
        width: 280,
        modal: true,
        resizable: false,
        position: 'center',
        buttons: {
                "Verify Password": function() {
                        var bValid = true;
                        allFields.removeClass( "ui-state-error" );
                        bValid = bValid && checkLength( password, "password", 5, 16 );

                        if (bValid) {
                            if(document.getElementById("password").value!=""){
                                $.post("<%=request.getContextPath()%>/LoginSrBean", {param1:$('#password').val(),param2:'<%=userId%>',funName:'verifyPWD'},  function(j){
                                    if(j.charAt(0)=="1"){
                                        document.getElementById("hdnPwd").value = j.toString().substring(2, j.toString().length);
                                        if(check){
                                            generateString();
                                            if(document.getElementById("signedData")!=null)
                                                document.getElementById("signedData").value = sha1Hash(document.getElementById("signedData").value);
                                            if(document.getElementById("encrypt")!=null)
                                                document.getElementById("encrypt").disabled = false;
                                            if(document.getElementById("lblEncrypt")!=null)
                                                document.getElementById("lblEncrypt").className = "bidFormBtn_1";
                                            if(document.getElementById("sign")!=null)
                                                document.getElementById("sign").disabled = true;
                                            if(document.getElementById("lblSign")!=null)
                                                document.getElementById("lblSign").className = "formBtn_disabled";
                                            verified = false;
                                            if(document.getElementById("divMsg")!=null){
                                                document.getElementById("divMsg").innerHTML = "Form signed successfully, please encrypt and update the form";
                                            }
                                        }else{
                                            decryptData();
                                            if(!encryptFlag)
                                            {
                                                if(document.getElementById("decrypt")!=null)
                                                    document.getElementById("decrypt").style.display ="none";
                                                if(document.getElementById("lblDecrypt")!=null)
                                                    document.getElementById("lblDecrypt").style.display ="none";
                                                if(document.getElementById("encrypt")!=null)
                                                    document.getElementById("encrypt").style.display ="block";
                                                if(document.getElementById("lblEncrypt")!=null)
                                                    document.getElementById("lblEncrypt").style.display ="block";
                                                if(document.getElementById("verify")!=null)
                                                    document.getElementById("verify").disabled = false;
                                                if(document.getElementById("lblVerify")!=null)
                                                    document.getElementById("lblVerify").className = "bidFormBtn_1";
                                                if(document.getElementById("sign")!=null)
                                                    document.getElementById("sign").disabled = true;
                                                if(document.getElementById("lblSign")!=null)
                                                    document.getElementById("lblSign").className = "formBtn_disabled";
                                                if(document.getElementById("divMsg")!=null){
                                                    document.getElementById("divMsg").innerHTML = "Form Decrypted successfully, click verify to proceed";
                                                    document.getElementById("divMsg").style.display = "block";
                                                }
                                            }
                                            else
                                            {
                                                if(document.getElementById("decrypt")!=null)
                                                    document.getElementById("decrypt").style.display ="none";
                                                if(document.getElementById("lblDecrypt")!=null)
                                                    document.getElementById("lblDecrypt").style.display ="none";
                                                if(document.getElementById("encrypt")!=null)
                                                    document.getElementById("encrypt").style.display ="block";
                                                if(document.getElementById("lblEncrypt")!=null)
                                                    document.getElementById("lblEncrypt").style.display ="block";
                                                if(document.getElementById("lblEncrypt")!=null)
                                                    document.getElementById("lblEncrypt").className = "bidFormBtn_1";

                                            }
                                        }
                                        updateTips("Done");
                                        $("#dialog-form").dialog("close");
                                    }else{
                                        updateTips("Please enter valid password");
                                        $('#password').val() = "";
                                    }
                                });
                            }
                        }
                }
        },
        close: function() {
                allFields.val("").removeClass("ui-state-error");
        }
});

function validate()
{
    var finalRows;
    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(chkEdit == true)
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
        else
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);

        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                {
                    if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value=="")
                    {
                        jAlert("Please enter data first!","Title", function(retVal) {});
                        return false;
                    }
                }
            }
        }
    }
    return true;
}
function SignNVerify(){

    if(validate()){
        check = true;
        $("#dialog-form").dialog("open");
    }
}

function SignVerify(){

    if(validate()){
        check = true;
        if(check){
            generateString();
            document.getElementById("signedData").value = sha1Hash(document.getElementById("signedData").value);
            document.getElementById("sign").disabled = true;
            document.getElementById("lblSign").className = "formBtn_disabled";
            document.getElementById("save").disabled = false;
            document.getElementById("lblSave").className = "bidFormBtn_1";
        }else{
        }
    }
}

function decryptNVerify(){
    $("#dialog-form").dialog("open");
}

function decryptNEncrypt(){
    encryptFlag = true;
    $("#dialog-form").dialog("open");
}

function decryptNView(){
    $("#dialog-form").dialog("open");
}

function verifyFormData(){

    var finalRows;
    var signData = "";

    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(chkEdit == true){
            //alert('arrBidCount : '+parseInt(arrBidCount[totalTables]));
            //alert('arrRow : '+parseInt(arrRow[totalTables]));
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
            //alert('finalRows : '+finalRows);
        }else
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);

        signData +=  " value for New Table ";

        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                {
                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = false;
                    signData = trim(signData) + "_" + document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value;
                }
            }
        }
    }
    //alert(signData);
    signData = sha1Hash(signData);
    //alert(signData);

    if(document.getElementById("signedData").value == signData){
        //alert('sanjay');
        document.getElementById("lblVerify").className = "formBtn_disabled";
        document.getElementById("verify").disabled = true;
        document.getElementById("lblSign").className = "bidFormBtn_1";
        document.getElementById("sign").disabled = false;
        <%if(isEdit){%>
            for(var totalTables=0;totalTables<arr.length;totalTables++)
            {
                    if(isMultiTable){
                        if(document.getElementById("bttn"+arr[totalTables])!=null && document.getElementById("lblAddTable"+arr[totalTables])!=null){
                            document.getElementById("bttn"+arr[totalTables]).disabled = false;
                            document.getElementById("lblAddTable"+arr[totalTables]).className = "bidFormBtn_1";
                        }
                    }
            }
        <%}%>
        jAlert("eSignature Verified Successfully","Success", function(retVal) {});
        return true;
    } else {
        jAlert("eSignature Not Verified","Failure", function(retVal) {});
        return false;
    }
}

function verifyData(){
    var finalRows;
    var signData = "";

    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(chkEdit == true)
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
        else
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);

        signData +=  " value for New Table ";

        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                {
                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = false;
                    signData = trim(signData) + "_" + document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value;
                }
            }
        }
    }

    signData = sha1Hash(signData);

    if(document.getElementById("signedData").value == signData){
        document.getElementById("verify").style.display ="none";
        document.getElementById("lblVerify").style.display ="none";
        document.getElementById("sign").style.display = "block";
        document.getElementById("lblSign").style.display = "block";
        document.getElementById("sign").disabled = false;
        document.getElementById("lblSign").className = "bidFormBtn_1";
        updateTips("");
        verified = true;
        if(document.getElementById("divMsg")!=null){
            document.getElementById("divMsg").innerHTML = "eSignature Verified Successfully,edit the details and click Sign";
            //document.getElementById("divMsg").style.display = "block";
        }
        jAlert("eSignature Verified Successfully","Success", function(retVal) {});
        return true;
    } else {
        jAlert("eSignature Not Verified","Failure", function(retVal) {});
        return false;
    }
}

function generateString()
{
    document.getElementById("signedData").value = "";
    var finalRows;
    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(parseInt(arrRow[totalTables])!=0)
        {
            if(chkEdit == true)
                finalRows = eval(parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]));
            else
                finalRows = eval(parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]));

            document.getElementById("signedData").value +=  " value for New Table ";

            for(var rowCount=0;rowCount<=finalRows;rowCount++)
            {
                for(var colCount=0;colCount<arrCol[totalTables];colCount++)
                {
                    if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                    {
                        document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = true;
                        document.getElementById("signedData").value = trim(document.getElementById("signedData").value) + "_" + document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value;
                    }
                }
            }
        }
        //alert(document.getElementById("signedData").value);
    }
    //alert(document.getElementById("signedData").value);
    return true;
}

function encryptData(){
    var finalRows;
    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(chkEdit == true)
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
        else
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);

        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                {
                    SignerAPI.setData(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value);
                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = SignerAPI.getSymEncrypt($('#hdnPwd').val());
                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = true;
                }
            }
        }
    }
    document.getElementById("encrypt").disabled = true;
    document.getElementById("lblEncrypt").className = "formBtn_disabled";
    document.getElementById("save").disabled = false;
    document.getElementById("lblSave").className = "bidFormBtn_1";
}

function encryptBData(objEncBtn){
    objEncBtn.style.display = "none";
    var finalRows;
    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(chkEdit == true)
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
        else
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);

        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                {
                    SignerAPI.setData(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value);
                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = SignerAPI.getSymEncrypt($('#hdnBuyerPwd').val());
                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = true;
                }
            }
        }
    }
    return true;
}

function decryptData(){
    var finalRows;
    for(var totalTables=0;totalTables<arr.length;totalTables++)
    {
        if(chkEdit == true)
            finalRows = parseInt(arrBidCount[totalTables]) * parseInt(arrRow[totalTables]);
        else
            finalRows = parseInt(arrTableAdded[totalTables]) * parseInt(arrRow[totalTables]);

        for(var rowCount=0;rowCount<=finalRows;rowCount++)
        {
            for(var colCount=0;colCount<arrCol[totalTables];colCount++)
            {
                if(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)) != null)
                {
                    SignerAPI.setEncrypt(document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value);
                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).value = SignerAPI.getSymDecrypt($('#hdnPwd').val());
                    document.getElementById("row"+arr[totalTables]+"_"+rowCount+"_"+(parseInt(colCount)+1)).readOnly = true;
                }
            }
        }
    }
}
</script>