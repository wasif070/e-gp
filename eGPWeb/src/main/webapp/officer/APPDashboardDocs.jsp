<%-- 
    Document   : APPDashboardDocs
    Created on : Nov 4, 2010, 6:34:28 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.model.table.TblAppEngEstDoc" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<jsp:useBean id="appServlet" class="com.cptu.egp.eps.web.servlet.APPServlet" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Estimated Cost</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
       
        <script type="text/javascript">

            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:100}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please select Document</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please enter Description</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed</div>"}
                    }
                });
            });
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpload').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>
    </head>
    <jsp:useBean id="appSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.APPSrBean"/>
    <body>
        <%  int appId = 0;
                    if (!request.getParameter("appId").equals("") || request.getParameter("appId") != null) {
                        appId = Integer.parseInt(request.getParameter("appId"));
                    }
                    int pckId = 0;
                    if (!request.getParameter("pckId").equals("")) {
                        pckId = Integer.parseInt(request.getParameter("pckId"));
                    }
                    String pckno = "";
                    if (!request.getParameter("pckno").equals("")) {
                        pckno = request.getParameter("pckno");
                    }
                    String desc = "";
                    if (!request.getParameter("desc").equals("")) {
                        desc = request.getParameter("desc");
                    }
                    String uId="0";
                    if(session.getAttribute("userId")!=null){
                        uId=session.getAttribute("userId").toString();
                    }
                    appSrBean.setLogUserId(uId);
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1">Estimated Cost<span style="float:right;"><a href="APPDashboard.jsp?appID=<%=request.getParameter("appId")%>" class="action-button-goback">Go Back to Dashboard</a></span></div>

                            <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/AppDownloadDocs"  enctype="multipart/form-data" name="frmUploadDoc">
                                <input type="hidden" id="txtappid" name="appId" value="<%=appId%>"/>
                                <input type="hidden" id="txtpckid" name="pckId" value="<%=pckId%>"/>
                                <input type="hidden" id="pckNo" name="pckNo" value="<%=pckno%>"/>
                                <input type="hidden" id="desc" name="desc" value="<%=desc%>"/>
                                <%
                                            if (request.getParameter("fq") != null) {
                                                if (request.getParameter("fq").equals("Removed") || request.getParameter("fq").equals("Uploaded")) {
                                %>
                                <div class="responseMsg successMsg" style="margin-top: 10px;">File <%=request.getParameter("fq")%> successfully</div>
                                <%} else {%>
                                <div> &nbsp;</div>
                                <div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
                                <%
                                                }
                                            }
                                            if (request.getParameter("fs") != null) {
                                %>
                                <div> &nbsp;</div>
                                <div class="responseMsg errorMsg">
                                    Max. file size of a single file must not exceed <%=request.getParameter("fs")%>MB, Acceptable file types are : <%=request.getParameter("ft")%>.
                                </div>
                                <%
                                            }
                     
                                        java.util.List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
                                        appListDtBean = appServlet.getAPPDetails("App", appId+"", "");
                            
                                %>
                                <div class="tableHead_1 t_space">APP Information Bar :</div>
                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <%--<table border="0" cellspacing="10" cellpadding="0" class="tableList_1">--%>
                                    <tr><% for (CommonAppData details : appListDtBean) {%>
                                                    <td width="10%" class="ff">APP ID	: </td>
                                                    <td width="20%"><%=details.getFieldName1()%></td>
                                                    <td width="10%" class="ff">Letter Ref. No. : </td>
                                                    <td width="20%" ><%=details.getFieldName3()%></td>
                                                    <td width="20%" class="ff">Project Name (If Applicable): </td>
                                                    <td width="20%"><%=details.getFieldName6()%></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Financial Year : </td>
                                                    <td><%=details.getFieldName4()%></td>
                                                    <td class="ff">Budget Type : </td>
                                                    <td><%=details.getFieldName5()%></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <%if(details!=null){
                                                    details = null;
                                                }
                                                }%>
<!--                                    <tr>
                                        <td width="10%" class="ff">Package Number   :</td>
                                        <td width="90%"><label><%=pckno%></label></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" >Package Description   :</td>
                                        <td><label><%=desc%></label></td>
                                    </tr>-->
                                </table>
                                    <%
                                            if(appListDtBean!=null){
                                                    appListDtBean.clear();
                                                    appListDtBean= null;
                                                }
                                            
                                    %>
                                <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1 t_space">
                                    <tr>
                                        <td width="10%" class="ff">Document   : <span>*</span></td>
                                        <td width="90%"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:250px; background:none;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Description   : <span>*</span></td>
                                        <td>
                                            <input name="documentBrief" id="documentBrief" type="text" class="formTxtBox_1" style="width:245px; background:none;"/>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td>
                                        <td align="left"><label class="formBtn_1">
                                                <input type="submit" name="upload" id="btnUpload" value="Upload"/></label>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="100%"  class="t-align-left">Instructions</th>
                                </tr>
                                <tr>
                                    <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");%>
                                    <td class="t-align-left">
                                        Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                        <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">Acceptable File Types <span class="mandatory"><%=tblConfigurationMaster.getAllowedExtension().replace(",", ",  ") %></span></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                                </tr>
                            </table>
                            <table border="0" <%--cellspacing="10"--%> width="100%" cellpadding="0" class="tableList_1 t_space">
                                <tr>

                                    <th>File Name</th>
                                    <th>Description</th>
                                    <th>Uploaded Date and Time</th>
                                    <th class="t-align-left" width="18%">Action</th>
                                </tr>
                                <%  int i = 1;
                                            for (TblAppEngEstDoc listings : appSrBean.getAllListing(pckId)) {
                                %>
                                <tr>
                                    <td align="left"><%= listings.getDocumentName()%></td>
                                    <td align="left"><%= listings.getDocumentDesc()%></td>
                                    <td align="center" class="t-align-center">
                                        <%= new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(new java.util.Date(listings.getUploadedDate().getTime()))%>
                                        <%//= new SimpleDateFormat("dd-MON-yyyy HH:mm:ss").format(new java.util.Date(listings.getUploadedDate().getTime()))%>
                                    </td>
                                    <td align="center" class="t-align-center"><a href="<%=request.getContextPath()%>/AppDownloadDocs?docName=<%=listings.getDocumentName()%>&appId=<%=appId%>&docId=<%=listings.getAppEngEstId()%>&docSize=<%= listings.getDocSize()%>&funName=download" ><img src="../resources/images/Dashboard/Download.png" /></a>
                                        &nbsp;
                                        <a href="<%=request.getContextPath()%>/AppDownloadDocs?docName=<%=listings.getDocumentName()%>&appId=<%= appId%>&docId=<%=listings.getAppEngEstId()%>&pckId=<%=pckId%>&pckNo=<%=pckno%>&desc=<%=desc%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a></td>
                                </tr>
                                <%
                                                i++;
                                            }
                                %>
                            </table>
                        </td>
                    </tr>
                </table>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
     <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabApp");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
</html>
