<%-- 
    Document   : SrvReExpense
    Created on : Dec 5, 2011, 11:52:47 AM
    Author     : dixit
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reimbursable Expenses</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>


    </head>
     <%
                        pageContext.setAttribute("tab", "14");
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");
                        String tenderId = "";
                        int formMapId = 0;
                        if (request.getParameter("tenderId") != null) {
                            tenderId = request.getParameter("tenderId");
                        }
                        String lotId = "";
                        if (request.getParameter("lotId") != null) {
                            lotId = request.getParameter("lotId");
                        }
                        if (request.getParameter("formMapId") != null) {
                            formMapId = Integer.parseInt(request.getParameter("formMapId").toString());
                        }
                        if (request.getParameter("srvFormMapId") != null) {
                            formMapId = Integer.parseInt(request.getParameter("srvFormMapId").toString());
                        }

            %>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Reimbursable Expenses
            <span style="float: right; text-align: right;" class="noprint">
                    <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=tenderId%>" title="Go Back To WorkSchedule">Go Back</a>
                    </span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%@include file="../resources/common/ContractInfoBar.jsp"%>
            <div>&nbsp;</div>
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "1");

                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1">
                    <% if (request.getParameter("msg") != null) {
                                    if ("succ".equalsIgnoreCase(request.getParameter("msg"))) {
                    %>
                    <div class='responseMsg successMsg'>Reimbursable Expenses details updated Successfully</div>
                    <%} else if ("fail".equalsIgnoreCase(request.getParameter("msg"))) {%>
                    <div class='responseMsg errorMsg'>REIMBURSABLE EXPENSES Data Submittion Failed</div>
                    <%}
                                }%>
                    <div align="center">                        
                        <form id="frmReExpenses" name="frmReExpenses" method="post" action='<%=request.getContextPath()%>/CMSSerCaseServlet?srvFormMapId=<%=formMapId%>&action=SrvReExpenses&tenderId=<%=tenderId%>&lotId=<%=lotId%>'>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <th>Sl. No.</th>
                                <th>Description Category</th>
                                <th>Description</th>
                                <th>Unit</th>
                                <th>Quantity</th>
                                <th>Unit Cost</th>
                                <th>Total Amount (In Nu.)</th>
                                <%
                                            CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                                            List<TblCmsSrvReExpense> list = cmss.getSrvReExpensesData(formMapId);
                                            if (!list.isEmpty()) {
                                                for (int i = 0; i < list.size(); i++) {
                                %>
                                <tr>
                                    <td class="t-align-right" style="text-align: right"><%=list.get(i).getSrNo()%></td>
                                    <td class="t-align-left"><%=list.get(i).getCatagoryDesc()%></td>
                                    <td class="t-align-left"><%=list.get(i).getDescription()%></td>
                                    <td class="t-align-left"><%=list.get(i).getUnit()%></td>
                                    <td class="t-align-center"><input type="text" name="qtytxt"  id="qtyid<%=i%>" value="<%=list.get(i).getQty()%>" class="formTxtBox_1" onchange="checkQty(<%=i%>)"/><span id="qtyidspan<%=i%>" class="reqF_1"></span></td>
                                    <td class="t-align-center"><input type="text" name="unitCosttxt"  id="unitCostid<%=i%>" value="<%=list.get(i).getUnitCost()%>" class="formTxtBox_1" onchange="checkUnitCost(<%=i%>)" /><span id="unitCostidspan<%=i%>" class="reqF_1"></span></td>
                                    <td class="t-align-right" style="text-align: right;"><span id="muliply<%=i%>"></span>
                                        <input type="hidden" name="muliplytxt" id="muliplytxt<%=i%>"/>
                                        <input type="hidden" value="<%=list.get(i).getSrvReid()%>" name="srvReId"/>
                                    </td>
                                </tr>                                
                                <%}
                                            }%>
                                <tr>
                                    <td colspan="5"></td>
                                    <td class="t-align-right ff" style="text-align: right;">Grand Total</td>
                                    <td class="t-align-right" style="text-align: right;"><span id="total"></span></td>
                                </tr>
                            </table>
                            <table class="t_space">
                                <tr>
                                    <td>
                                        <label class="formBtn_1">
                                            <input type="submit" name="submitbtn" id="submit" value="Submit" onclick="return validate();"/>
                                        </label>
                                    </td>    
                                </tr>    
                            </table>    
                        </form>
                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
    <script>
        function regForNumber(value)
        {
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

        }
        function positive(value)
        {
            return /^\d*$/.test(value);

        }
        function checkZero(id){
            var qty = document.getElementById('qtyid'+id).value;
            if(qty.indexOf('0') == 0 && qty.indexOf('.') != 1){
                jAlert("Please remove all leading zero(s)","REIMBURSABLE EXPENSES", function(RetVal) {
                });
                document.getElementById('qtyid'+id).value="";
            }else{
                return true;
            }
        }
        function checkQty(id){
            $('.err').remove();
            if(checkZero(id)){
                if(!regForNumber(document.getElementById('qtyid'+id).value)){
                    $("#qtyid"+id).parent().append("<div class='err' style='color:red;'><%=bdl.getString("CMS.PR.qtynumeric")%></div>");
                    vbool = false;
                    document.getElementById('qtyid'+id).value="";
                }else if(document.getElementById('qtyid'+id).value.indexOf("-")!=-1){
                    $("#qtyid"+id).parent().append("<div class='err' style='color:red;'>Quantity must not be negative value</div>");
                    document.getElementById('qtyid'+id).value = "";
                } else if(document.getElementById('qtyid'+id).value.split(".")[1] != undefined){
                    if(!regForNumber(document.getElementById('qtyid'+id).value) || document.getElementById('qtyid'+id).value.split(".")[1].length > 3 || document.getElementById('qtyid'+id).value.split(".")[0].length > 12){
                        $("#qtyid"+id).parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                        document.getElementById('qtyid'+id).value = "";
                        vbool = false;
                    }else{
                        var qty =  eval(document.getElementById('qtyid'+id).value);
                        var rate = eval(document.getElementById('unitCostid'+id).value);
                        var amt = qty*rate;
                        var f_value = Math.round(amt*Math.pow(10,3))/Math.pow(10,3)+"";
                        if(f_value.indexOf(".") < 0){
                            f_value = f_value+".000"
                        }
                        $('#muliply'+id).html(f_value);
                        $('#muliplytxt'+id).val(f_value);
                        countCntVal();

                    }
                }else{

                    var qty =  eval(document.getElementById('qtyid'+id).value);
                    var rate = eval(document.getElementById('unitCostid'+id).value);
                    var amt = qty*rate;
                    var f_value = Math.round(amt*Math.pow(10,3))/Math.pow(10,3)+"";
                    if(f_value.indexOf(".") < 0){
                        f_value = f_value+".000"
                    }
                    $('#muliply'+id).html(f_value);
                    $('#muliplytxt'+id).val(f_value);
                    countCntVal();
                }
            }
        }
        function checkZero1(id){
            var qty = document.getElementById('unitCostid'+id).value;
            if(qty.indexOf('0') == 0 && qty.indexOf('.') != 1){
                jAlert("Please remove all leading zero(s)","REIMBURSABLE EXPENSES", function(RetVal) {
                });
                document.getElementById('qtyid'+id).value="";
            }else{
                return true;
            }
        }
        function checkUnitCost(id){
            $('.err').remove();
            if(checkZero1(id)){
                if(!regForNumber(document.getElementById('unitCostid'+id).value)){
                    $("#unitCostid"+id).parent().append("<div class='err' style='color:red;'><%=bdl.getString("CMS.PR.qtynumeric")%></div>");
                    vbool = false;
                    document.getElementById('unitCostid'+id).value="";
                }else if(document.getElementById('unitCostid'+id).value.indexOf("-")!=-1){
                    $("#unitCostid"+id).parent().append("<div class='err' style='color:red;'>Quantity must not be negative value</div>");
                    document.getElementById('unitCostid'+id).value = "";
                } else if(document.getElementById('unitCostid'+id).value.split(".")[1] != undefined){
                    if(!regForNumber(document.getElementById('unitCostid'+id).value) || document.getElementById('unitCostid'+id).value.split(".")[1].length > 3 || document.getElementById('unitCostid'+id).value.split(".")[0].length > 12){
                        $("#unitCostid"+id).parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                        document.getElementById('unitCostid'+id).value = "";
                        vbool = false;
                    }else{
                        var qty =  eval(document.getElementById('qtyid'+id).value);
                        var rate = eval(document.getElementById('unitCostid'+id).value);
                        var amt = qty*rate;
                        var f_value = Math.round(amt*Math.pow(10,3))/Math.pow(10,3)+"";
                        if(f_value.indexOf(".") < 0){
                            f_value = f_value+".000"
                        }
                        $('#muliply'+id).html(f_value);
                        $('#muliplytxt'+id).val(f_value);
                        countCntVal();
                    }
                }else{

                    var qty =  eval(document.getElementById('qtyid'+id).value);
                    var rate = eval(document.getElementById('unitCostid'+id).value);
                    var amt = qty*rate;
                    var f_value = Math.round(amt*Math.pow(10,3))/Math.pow(10,3)+"";
                    if(f_value.indexOf(".") < 0){
                        f_value = f_value+".000"
                    }
                    $('#muliply'+id).html(f_value);
                    $('#muliplytxt'+id).val(f_value);
                    countCntVal();
                }
            }
        }
        function countCntVal(){
            var count = <%=list.size()%>
            var actualamount = 0;
            for(var i=0;i<count;i++){
                actualamount = actualamount+eval(document.getElementById('muliply'+i).innerHTML);
            }
            var f_value = Math.round(actualamount*Math.pow(10,3))/Math.pow(10,3)+"";
            if(f_value.indexOf(".") < 0){
                f_value = f_value+".000"
            }
            $('#total').html(f_value);
        }
        function allcountCntVal(){
            for(var id = 0; id<<%=list.size()%>; id++)
            {
                var qty =  eval(document.getElementById('qtyid'+id).value);
                var rate = eval(document.getElementById('unitCostid'+id).value);
                var amt = qty*rate;
                var f_value = Math.round(amt*Math.pow(10,3))/Math.pow(10,3)+"";
                if(f_value.indexOf(".") < 0){
                    f_value = f_value+".000"
                }
                $('#muliply'+id).html(f_value);
                $('#muliplytxt'+id).val(f_value);
            }
            countCntVal();
        }
        function validate()
        {
            var vbool = true;
            for(var id = 0; id<<%=list.size()%>; id++)
            {
                if(document.getElementById('qtyid'+id).value!="")
                {
                    document.getElementById('qtyidspan'+id).innerHTML="";
                }else{
                    document.getElementById('qtyidspan'+id).innerHTML="Please enter Quantity";
                    vbool = false;
                }
                if(document.getElementById('unitCostid'+id).value!="")
                {
                    document.getElementById('unitCostidspan'+id).innerHTML="";
                }else{
                    document.getElementById('unitCostidspan'+id).innerHTML="Please enter UnitCost";
                    vbool = false;
                }
            
            }
            return vbool;
        }
        allcountCntVal();
    </script>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

