<%--
    Document   : EditPackageDates
    Created on : Nov 11, 2010, 3:26:35 PM
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <jsp:useBean id="appViewPkgDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AppViewPkgDtBean"/>
    <%
                String strAppId = request.getParameter("appId");
                String strPkgId = request.getParameter("pkgId");

                short methodId = 0;
                String methodName = "";
                int appId = 0;
                int pkgId = 0;
                if (strAppId != null && !strAppId.equals("")) {
                    appId = Integer.parseInt(strAppId);
                }
                if (strPkgId != null && !strPkgId.equals("")) {
                    pkgId = Integer.parseInt(strPkgId);
                }
                if (appId != 0 && pkgId != 0) {
                    appViewPkgDtBean.populateAPPDates(appId, pkgId, "Package");
                    if(appViewPkgDtBean.isDataExists()) {
                    methodId = appViewPkgDtBean.getProcurementMethodId();
                    methodName = appViewPkgDtBean.getProcMethodName(methodId);
                    }
                }
                String logUserId="0";
                if(session.getAttribute("userId")!=null){
                    logUserId=session.getAttribute("userId").toString();
                }
                boolean isRevision=false;
                if("Revise".equalsIgnoreCase(request.getParameter("action"))){
                  isRevision=true;
                }
                 if("r".equalsIgnoreCase(request.getParameter("msg"))){
                  isRevision=true;
                }
                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                tenderCS.setLogUserId(logUserId);
    %>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <% if(isRevision) { %>
              <title>Revise APP : Package Dates</title>

        <%}else { %>
        <title>Edit APP : Package Dates</title>
        <% } %>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>


        <script type="text/javascript">
            function numeric(value)
            {
                if(value!="")
                    return /^\d+$/.test(value);
                return true;
            }
            function maxDay(value)
            {
                //alert(value);
                if(parseInt(value)>999)
                    return true;
                return false;
            }
            function submitfunction()
            {
                var validatebool = "true";

                if(document.getElementById("hdnPQReq").value=="Yes"){
                    if(document.getElementById("txtpqdtadvtinvt") ==null || document.getElementById("txtpqdtadvtinvt").value==''){
                        document.getElementById("error").innerHTML="<br/>Please select the date";
                        validatebool="false";
                    }

                    if(document.getElementById("txtpqdtadvtinvtNo")!=null)
                    if(document.getElementById("txtpqdtadvtinvtNo").value==''){
                        document.getElementById("errorNo").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtpqdtadvtinvtNo").value)){
                            document.getElementById("errorNo").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtpqdtadvtinvtNo").value))
                            {
                                document.getElementById("errorNo").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtpqdtadvtinvtNo").value="";
                                document.getElementById("txtpqdtadvtinvtNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtpqdtappsub")!=null)
                    if(document.getElementById("txtpqdtappsub").value==''){
                        document.getElementById("e2").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtPqdtappsubNo")!=null)
                    if(document.getElementById("txtPqdtappsubNo").value==''){
                        document.getElementById("e2No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtPqdtappsubNo").value)){
                            document.getElementById("e2No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtPqdtappsubNo").value))
                            {
                                document.getElementById("e2No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtPqdtappsubNo").value="";
                                document.getElementById("txtPqdtappsubNo").focus();
                                validatebool="false";
                            }
                        }
                    }


                    if(document.getElementById("txtPqdtsubevarpt")!=null)
                    if(document.getElementById("txtPqdtsubevarpt").value==''){
                        document.getElementById("e3").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtPqdtsubevarptNo")!=null)
                    if(document.getElementById("txtPqdtsubevarptNo").value==''){
                        document.getElementById("e3No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtPqdtsubevarptNo").value)){
                            document.getElementById("e3No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtPqdtsubevarptNo").value))
                            {
                                document.getElementById("e3No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtPqdtsubevarptNo").value="";
                                document.getElementById("txtPqdtsubevarptNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtPqdtapplst")!=null)
                    if(document.getElementById("txtPqdtapplst").value==''){
                        document.getElementById("e4").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("e5")!=null)
                    if(!CompareToForGreaterWithPqdtapplst(document.getElementById("txtPqdtapplst"),document.getElementById("txtRfqdtadvtift"))){
                        document.getElementById("e5").innerHTML = "<br/>Expected date of advertisement must be greater than the Approval date";
                        validatebool="false";
                    }
                }

                //alert('PQReq : '+document.getElementById("hdnPQReq").value);
                //alert('hdnPMName : '+document.getElementById("hdnPMName").value);
                //alert('hdnREO : '+document.getElementById("hdnREO").value);

                if(((document.getElementById("hdnPQReq").value!="") && (document.getElementById("hdnPMName").value!="TSTM"))
                    || ((document.getElementById("hdnPQReq").value=="") && ((document.getElementById("hdnPMName").value=="OTM")
                    || (document.getElementById("hdnPMName").value=="RFQ") || ((document.getElementById("hdnPMName").value=="LTM")
                    || (document.getElementById("hdnPMName").value=="DPM") || (document.getElementById("hdnPMName").value=="OSTETM")
                    || (document.getElementById("hdnPMName").value=="RFQU") || (document.getElementById("hdnPMName").value=="RFQL")
                    && (document.getElementById("hdnREO").value==""))))){

                    if(document.getElementById("txtRfqdtadvtift")==null || document.getElementById("txtRfqdtadvtift").value==''){
                        document.getElementById("e5").innerHTML="<br/>Please select date";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRfqdtadvtiftNo")!=null)
                    if(document.getElementById("txtRfqdtadvtiftNo").value==''){
                        document.getElementById("e5No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRfqdtadvtiftNo").value)){
                            document.getElementById("e5No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRfqdtadvtiftNo").value))
                            {
                                document.getElementById("e5No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqdtadvtiftNo").value="";
                                document.getElementById("txtRfqdtadvtiftNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRfqdtsub")!=null)
                    if(document.getElementById("txtRfqdtsub").value==''){
                        document.getElementById("e6").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRfqdtsubNo")!=null)
                    if(document.getElementById("txtRfqdtsubNo").value==''){
                        document.getElementById("e6No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRfqdtsubNo").value)){
                            document.getElementById("e6No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRfqdtsubNo").value))
                            {
                                document.getElementById("e6No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqdtsubNo").value="";
                                document.getElementById("txtRfqdtsubNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRfqexpdtopen")!=null)
                    if(document.getElementById("txtRfqexpdtopen").value==''){
                        document.getElementById("e7").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRfqexpdtopenNo")!=null)
                    if(document.getElementById("txtRfqexpdtopenNo").value==''){
                        document.getElementById("e7No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRfqexpdtopenNo").value)){
                            document.getElementById("e7No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRfqexpdtopenNo").value))
                            {
                                document.getElementById("e7No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqexpdtopenNo").value="";
                                document.getElementById("txtRfqexpdtopenNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRfqdtsubevaRpt")!=null)
                    if(document.getElementById("txtRfqdtsubevaRpt").value==''){
                        document.getElementById("e8").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRfqdtsubevaRptNo")!=null)
                    if(document.getElementById("txtRfqdtsubevaRptNo").value==''){
                        document.getElementById("e8No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRfqdtsubevaRptNo").value)){
                            document.getElementById("e8No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRfqdtsubevaRptNo").value))
                            {
                                document.getElementById("e8No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqdtsubevaRptNo").value="";
                                document.getElementById("txtRfqdtsubevaRptNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRfqexpdtAppawd")!=null)
                    if(document.getElementById("txtRfqexpdtAppawd").value==''){
                        document.getElementById("e9").innerHTML="";
                        validatebool="false";
                    }    
                
                 //Code by Proshanto Kumar Saha
                    if(document.getElementById("txtRfqexpdtAppawdNo")!=null)
                    if(document.getElementById("txtRfqexpdtAppawdNo").value==''){
                        document.getElementById("e81No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRfqexpdtAppawdNo").value)){
                            document.getElementById("e81No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRfqexpdtAppawdNo").value))
                            {
                                document.getElementById("e81No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqexpdtAppawdNo").value="";
                                document.getElementById("txtRfqexpdtAppawdNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRfqexpdtLtrIntAwd")!=null)
                    if(document.getElementById("txtRfqexpdtLtrIntAwd").value==''){
                        document.getElementById("e91").innerHTML="";
                        validatebool="false";
                    }
                    //Code end by Proshanto Kumar Saha

                    //Id change by proshanto kumar saha
                    if(document.getElementById("txtRfqexpdtLtrIntAwdNo")!=null)
                    if(document.getElementById("txtRfqexpdtLtrIntAwdNo").value==''){
                        document.getElementById("e9No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRfqexpdtLtrIntAwdNo").value)){
                            document.getElementById("e9No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRfqexpdtLtrIntAwdNo").value))
                            {
                                document.getElementById("e9No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqexpdtLtrIntAwdNo").value="";
                                document.getElementById("txtRfqexpdtLtrIntAwdNo").focus();
                                validatebool="false";
                            }
                        }
                    }
                    //END

                    if(document.getElementById("txtRfqdtIssNOA")!=null)
                    if(document.getElementById("txtRfqdtIssNOA").value==''){
                        document.getElementById("e10").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRfqdtIssNOANo")!=null)
                    if(document.getElementById("txtRfqdtIssNOANo").value==''){
                        document.getElementById("e10No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRfqdtIssNOANo").value)){
                            document.getElementById("e10No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRfqdtIssNOANo").value))
                            {
                                document.getElementById("e10No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqdtIssNOANo").value="";
                                document.getElementById("txtRfqdtIssNOANo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRfqexpdtSign")!=null)
                    if(document.getElementById("txtRfqexpdtSign").value==''){
                        document.getElementById("e11").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRfqexpdtSignNo")!=null)
                    if(document.getElementById("txtRfqexpdtSignNo").value==''){
                        document.getElementById("e11No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRfqexpdtSignNo").value)){
                            document.getElementById("e11No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRfqexpdtSignNo").value))
                            {
                                document.getElementById("e11No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqexpdtSignNo").value="";
                                document.getElementById("txtRfqexpdtSignNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRfqexpdtCompContract")!=null)
                    if(document.getElementById("txtRfqexpdtCompContract").value==''){
                        document.getElementById("e12").innerHTML="";
                        validatebool="false";
                    }


                }

            //////////// TSTM validation   /////////////////////////////
                if(document.getElementById("hdnPMName").value=="TSTM"){
                    if(document.getElementById("txtTSTMexpdtadvtIFT")!=null)
                    if(document.getElementById("txtTSTMexpdtadvtIFT").value==''){
                        document.getElementById("t1").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtTSTMexpdtadvtIFTNo")!=null)
                    if(document.getElementById("txtTSTMexpdtadvtIFTNo").value==''){
                        document.getElementById("t1No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTMexpdtadvtIFTNo").value)){
                            document.getElementById("t1No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTMexpdtadvtIFTNo").value))
                            {
                                document.getElementById("t1No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTMexpdtadvtIFTNo").value="";
                                document.getElementById("txtTSTMexpdtadvtIFTNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtTSTMexpdtSub")!=null)
                    if(document.getElementById("txtTSTMexpdtSub").value==''){
                        document.getElementById("t2").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtTSTMexpdtSubNo")!=null)
                    if(document.getElementById("txtTSTMexpdtSubNo").value==''){
                        document.getElementById("t2No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTMexpdtSubNo").value)){
                            document.getElementById("t2No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTMexpdtSubNo").value))
                            {
                                document.getElementById("t2No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTMexpdtSubNo").value="";
                                document.getElementById("txtTSTMexpdtSubNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtTSTMexpdtopen")!=null)
                    if(document.getElementById("txtTSTMexpdtopen").value==''){
                        document.getElementById("t3").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtTSTMexpdtopenNo")!=null)
                    if(document.getElementById("txtTSTMexpdtopenNo").value==''){
                        document.getElementById("t3No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTMexpdtopenNo").value)){
                            document.getElementById("t3No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTMexpdtopenNo").value))
                            {
                                document.getElementById("t3No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTMexpdtopenNo").value="";
                                document.getElementById("txtTSTMexpdtopenNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtTSTMexpdtsubEvaRpt")!=null)
                    if(document.getElementById("txtTSTMexpdtsubEvaRpt").value==''){
                        document.getElementById("t4").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtTSTMexpdtsubEvaRptNo")!=null)
                    if(document.getElementById("txtTSTMexpdtsubEvaRptNo").value==''){
                        document.getElementById("t4No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTMexpdtsubEvaRptNo").value)){
                            document.getElementById("t4No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTMexpdtsubEvaRptNo").value))
                            {
                                document.getElementById("t4No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTMexpdtsubEvaRptNo").value="";
                                document.getElementById("txtTSTMexpdtsubEvaRptNo").focus();                         validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtTSTMexpdtappEvaRpt")!=null)
                    if(document.getElementById("txtTSTMexpdtappEvaRpt").value==''){
                        document.getElementById("t5").innerHTML="";
                        validatebool="false";
                    }

                    if(!CompareToForGreaterWithTSTMexpdtappEvaRpt(document.getElementById("txtTSTMexpdtappEvaRpt"),document.getElementById("txtTSTM2expdtIssuefinalDoc"))){
                        document.getElementById("t6").innerHTML = "<br/>Expected date of TSTM must be greater than the Approval date";
                        validatebool="false";
                    }

                    if(document.getElementById("txtTSTM2expdtIssuefinalDoc")!=null)
                    if(document.getElementById("txtTSTM2expdtIssuefinalDoc").value==''){
                        document.getElementById("t6").innerHTML=".";
                        validatebool="false";
                    }

                    if(document.getElementById("txtTSTM2expdtIssuefinalDocNo")!=null)
                    if(document.getElementById("txtTSTM2expdtIssuefinalDocNo").value==''){
                        document.getElementById("t6No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTM2expdtIssuefinalDocNo").value)){
                            document.getElementById("t6No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTM2expdtIssuefinalDocNo").value))
                            {
                                document.getElementById("t6No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtIssuefinalDocNo").value="";
                                document.getElementById("txtTSTM2expdtIssuefinalDocNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtTSTM2expdtSub")!=null)
                    if(document.getElementById("txtTSTM2expdtSub").value==''){
                        document.getElementById("t7").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtTSTM2expdtSubNo")!=null)
                    if(document.getElementById("txtTSTM2expdtSubNo").value==''){
                        document.getElementById("t7No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTM2expdtSubNo").value)){
                            document.getElementById("t7No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTM2expdtSubNo").value))
                            {
                                document.getElementById("t7No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtSubNo").value="";
                                document.getElementById("txtTSTM2expdtSubNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtTSTM2expdtOpen")!=null)
                    if(document.getElementById("txtTSTM2expdtOpen").value==''){
                        document.getElementById("t8").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtTSTM2expdtOpenNo")!=null)
                    if(document.getElementById("txtTSTM2expdtOpenNo").value==''){
                        document.getElementById("t8No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTM2expdtOpenNo").value)){
                            document.getElementById("t8No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTM2expdtOpenNo").value))
                            {
                                document.getElementById("t8No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtOpenNo").value="";
                                document.getElementById("txtTSTM2expdtOpenNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtTSTM2expdtsubevaRpt")!=null)
                    if(document.getElementById("txtTSTM2expdtsubevaRpt").value==''){
                        document.getElementById("t9").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtTSTM2expdtsubevaRptNo")!=null)
                    if(document.getElementById("txtTSTM2expdtsubevaRptNo").value==''){
                        document.getElementById("t9No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTM2expdtsubevaRptNo").value)){
                            document.getElementById("t9No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTM2expdtsubevaRptNo").value))
                            {
                                document.getElementById("t9No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtsubevaRptNo").value="";
                                document.getElementById("txtTSTM2expdtsubevaRptNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtTSTM2expdtAppEvaRpt")!=null)
                    if(document.getElementById("txtTSTM2expdtAppEvaRpt").value==''){
                        document.getElementById("t10").innerHTML="";
                        validatebool="false";
                    }

                    //Id change by proshanto
                    if(document.getElementById("txtTSTM2expdtAppEvaRptNo")!=null)
                    if(document.getElementById("txtTSTM2expdtAppEvaRptNo").value==''){
                        document.getElementById("t91No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTM2expdtAppEvaRptNo").value)){
                            document.getElementById("t91No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTM2expdtAppEvaRptNo").value))
                            {
                                document.getElementById("t91No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtAppEvaRptNo").value="";
                                document.getElementById("txtTSTM2expdtAppEvaRptNo").focus();
                                validatebool="false";
                            }
                        }
                    }
                    //Id end

                    //code by proshanto
                    if(document.getElementById("txtTSTM2expdtLetterOfIntent")!=null)
                    if(document.getElementById("txtTSTM2expdtLetterOfIntent").value==''){
                        document.getElementById("t101").innerHTML="";
                        validatebool="false";
                    }
                    //code end
                    //Code by proshanto
                    if(document.getElementById("txtTSTM2expdtLetterOfIntentNo")!=null)
                    if(document.getElementById("txtTSTM2expdtLetterOfIntentNo").value==''){
                        document.getElementById("t10No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTM2expdtLetterOfIntentNo").value)){
                            document.getElementById("t10No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTM2expdtLetterOfIntentNo").value))
                            {
                                document.getElementById("t10No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtLetterOfIntentNo").value="";
                                document.getElementById("txtTSTM2expdtLetterOfIntentNo").focus();
                                validatebool="false";
                            }
                        }
                    }
                    //Code end

                    if(document.getElementById("txtTSTM2expdtIssueNOA")!=null)
                    if(document.getElementById("txtTSTM2expdtIssueNOA").value==''){
                        document.getElementById("t11").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtTSTM2expdtIssueNOANo")!=null)
                    if(document.getElementById("txtTSTM2expdtIssueNOANo").value==''){
                        document.getElementById("t11No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTM2expdtIssueNOANo").value)){
                            document.getElementById("t11No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTM2expdtIssueNOANo").value))
                            {
                                document.getElementById("t11No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtIssueNOANo").value="";
                                document.getElementById("txtTSTM2expdtIssueNOANo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtTSTM2expdtSignContract")!=null)
                    if(document.getElementById("txtTSTM2expdtSignContract").value==''){
                        document.getElementById("t12").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtTSTM2expdtSignContractNo")!=null)
                    if(document.getElementById("txtTSTM2expdtSignContractNo").value==''){
                        document.getElementById("t12No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtTSTM2expdtSignContractNo").value)){
                            document.getElementById("t12No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtTSTM2expdtSignContractNo").value))
                            {
                                document.getElementById("t12No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtSignContractNo").value="";
                                document.getElementById("txtTSTM2expdtSignContractNo").focus();
                                validatebool="false";
                            }
                        }
                    }


                }

            //////////// REOI validation   /////////////////////////////////////////////////////////////////////////////
                 if(document.getElementById("hdnREO").value=="REOI"){
                    if(document.getElementById("txtREOQexpdtadvtREOI")!=null)
                    if(document.getElementById("txtREOQexpdtadvtREOI").value==''){
                        document.getElementById("re1").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtREOQexpdtadvtREOINo")!=null)
                    if(document.getElementById("txtREOQexpdtadvtREOINo").value==''){
                        document.getElementById("re1No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtREOQexpdtadvtREOINo").value)){
                            document.getElementById("re1No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtREOQexpdtadvtREOINo").value))
                            {
                                document.getElementById("re1No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtREOQexpdtadvtREOINo").value="";
                                document.getElementById("txtREOQexpdtadvtREOINo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtREOQexpdtlstdtRcptEOI")!=null)
                    if(document.getElementById("txtREOQexpdtlstdtRcptEOI").value==''){
                        document.getElementById("re2").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtREOQexpdtlstdtRcptEOINo")!=null)
                    if(document.getElementById("txtREOQexpdtlstdtRcptEOINo").value==''){
                        document.getElementById("re2No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtREOQexpdtlstdtRcptEOINo").value)){
                            document.getElementById("re2No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtREOQexpdtlstdtRcptEOINo").value))
                            {
                                document.getElementById("re2No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtREOQexpdtlstdtRcptEOINo").value="";
                                document.getElementById("txtREOQexpdtlstdtRcptEOINo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtREOQexpdtsubsrtlstFrm")!=null)
                    if(document.getElementById("txtREOQexpdtsubsrtlstFrm").value==''){
                        document.getElementById("re3").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtREOQexpdtsubsrtlstFrmNo")!=null)
                    if(document.getElementById("txtREOQexpdtsubsrtlstFrmNo").value==''){
                        document.getElementById("re3No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtREOQexpdtsubsrtlstFrmNo").value)){
                            document.getElementById("re3No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtREOQexpdtsubsrtlstFrmNo").value))
                            {
                                document.getElementById("re3No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtREOQexpdtsubsrtlstFrmNo").value="";
                                document.getElementById("txtREOQexpdtsubsrtlstFrmNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtREOQextdtAppsrtlstFrm")!=null)
                    if(document.getElementById("txtREOQextdtAppsrtlstFrm").value==''){
                        document.getElementById("re4").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtREQexpdtcompContract")!=null)
                    if(document.getElementById("txtREQexpdtcompContract").value==''){
                        document.getElementById("re5").innerHTML="";
                        validatebool="false";
                    }

                     //alert(document.getElementById("txtREOQextdtAppsrtlstFrm").value);
                     //alert(document.getElementById("txtRFPexpdtissueRFP").value);
                    if(!CompareToForGreaterWithRFPexpdtissueRFP(document.getElementById("txtREOQextdtAppsrtlstFrm"),document.getElementById("txtRFPexpdtissueRFP"))){
                            document.getElementById("rp1").innerHTML = "<br/>Expected date of RFP must be greater than the Approval date";
                            validatebool="false";
                     }

                    if(document.getElementById("txtRFPexpdtissueRFP")!=null)
                    if(document.getElementById("txtRFPexpdtissueRFP").value==''){
                        document.getElementById("rp1").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFPexpdtissueRFPNo")!=null)
                    if(document.getElementById("txtRFPexpdtissueRFPNo").value==''){
                        document.getElementById("rp1No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFPexpdtissueRFPNo").value)){
                            document.getElementById("rp1No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFPexpdtissueRFPNo").value))
                            {
                                document.getElementById("rp1No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFPexpdtissueRFPNo").value="";
                                document.getElementById("txtRFPexpdtissueRFPNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFPexpdtSubProposal")!=null)
                    if(document.getElementById("txtRFPexpdtSubProposal").value==''){
                        document.getElementById("rp2").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFPexpdtSubProposalNo")!=null)
                    if(document.getElementById("txtRFPexpdtSubProposalNo").value==''){
                        document.getElementById("rp2No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFPexpdtSubProposalNo").value)){
                            document.getElementById("rp2No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFPexpdtissueRFPNo").value))
                            {
                                document.getElementById("rp1No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFPexpdtissueRFPNo").value="";
                                document.getElementById("txtRFPexpdtissueRFPNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFPexpdttechOpen")!=null)
                    if(document.getElementById("txtRFPexpdttechOpen").value==''){
                        document.getElementById("rp3").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFPexpdttechOpenNo")!=null)
                    if(document.getElementById("txtRFPexpdttechOpenNo").value==''){
                        document.getElementById("rp3No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFPexpdttechOpenNo").value)){
                            document.getElementById("rp3No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFPexpdttechOpenNo").value))
                            {
                                document.getElementById("rp3No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFPexpdttechOpenNo").value="";
                                document.getElementById("txtRFPexpdttechOpenNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFPexpdttechEva")!=null)
                    if(document.getElementById("txtRFPexpdttechEva").value==''){
                        document.getElementById("rp4").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFPexpdttechEvaNo")!=null)
                    if(document.getElementById("txtRFPexpdttechEvaNo").value==''){
                        document.getElementById("rp4No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFPexpdttechEvaNo").value)){
                            document.getElementById("rp4No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFPexpdttechEvaNo").value))
                            {
                                document.getElementById("rp4No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFPexpdttechEvaNo").value="";
                                document.getElementById("txtRFPexpdttechEvaNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFPextdtFinOpen")!=null)
                    if(document.getElementById("txtRFPextdtFinOpen").value==''){
                        document.getElementById("rp5").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFPextdtFinOpenNo")!=null)
                    if(document.getElementById("txtRFPextdtFinOpenNo").value==''){
                        document.getElementById("rp5No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFPextdtFinOpenNo").value)){
                            document.getElementById("rp5No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFPextdtFinOpenNo").value))
                            {
                                document.getElementById("rp5No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFPextdtFinOpenNo").value="";
                                document.getElementById("txtRFPextdtFinOpenNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFPexpdtsubCOmEvaRpt")!=null)
                    if(document.getElementById("txtRFPexpdtsubCOmEvaRpt").value==''){
                        document.getElementById("rp6").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFPexpdtsubCOmEvaRptNo")!=null)
                    if(document.getElementById("txtRFPexpdtsubCOmEvaRptNo").value==''){
                        document.getElementById("rp6No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFPexpdtsubCOmEvaRptNo").value)){
                            document.getElementById("rp6No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFPexpdtsubCOmEvaRptNo").value))
                            {
                                document.getElementById("rp6No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFPexpdtsubCOmEvaRptNo").value="";
                                document.getElementById("txtRFPexpdtsubCOmEvaRptNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFPexpdtappcomEvaRpt")!=null)
                    if(document.getElementById("txtRFPexpdtappcomEvaRpt").value==''){
                        document.getElementById("rp7").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFPexpdtappcomEvaRptNo")!=null)
                    if(document.getElementById("txtRFPexpdtappcomEvaRptNo").value==''){
                        document.getElementById("rp7No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFPexpdtappcomEvaRptNo").value)){
                            document.getElementById("rp7No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFPexpdtappcomEvaRptNo").value))
                            {
                                document.getElementById("rp7No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFPexpdtappcomEvaRptNo").value="";
                                document.getElementById("txtRFPexpdtappcomEvaRptNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFPexpdtcompNego")!=null)
                    if(document.getElementById("txtRFPexpdtcompNego").value==''){
                        document.getElementById("rp8").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFPexpdtcompNegoNo")!=null)
                    if(document.getElementById("txtRFPexpdtcompNegoNo").value==''){
                        document.getElementById("rp8No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFPexpdtcompNegoNo").value)){
                            document.getElementById("rp8No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFPexpdtcompNegoNo").value))
                            {
                                document.getElementById("rp8No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFPexpdtcompNegoNo").value="";
                                document.getElementById("txtRFPexpdtcompNegoNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFPexpdtappawdContract")!=null)
                    if(document.getElementById("txtRFPexpdtappawdContract").value==''){
                        document.getElementById("rp9").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFPexpdtappawdContractNo")!=null)
                    if(document.getElementById("txtRFPexpdtappawdContractNo").value==''){
                        document.getElementById("rp9No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFPexpdtappawdContractNo").value)){
                            document.getElementById("rp9No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFPexpdtappawdContractNo").value))
                            {
                                document.getElementById("rp9No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFPexpdtappawdContractNo").value="";
                                document.getElementById("txtRFPexpdtappawdContractNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFPexpdtsigncontract")!=null)
                    if(document.getElementById("txtRFPexpdtsigncontract").value==''){
                        document.getElementById("rp10").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFPexpdtsigncontractNo")!=null)
                    if(document.getElementById("txtRFPexpdtsigncontractNo").value==''){
                        document.getElementById("rp10No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFPexpdtsigncontractNo").value)){
                            document.getElementById("rp10No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFPexpdtsigncontractNo").value))
                            {
                                document.getElementById("rp10No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFPexpdtsigncontractNo").value="";
                                document.getElementById("txtRFPexpdtsigncontractNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFPexpdtcomplcontract")!=null)
                    if(document.getElementById("txtRFPexpdtcomplcontract").value==''){
                        document.getElementById("rp11").innerHTML="";
                        validatebool="false";
                    }


                }

                //--------------------------------RFA validation-----------------------
                if(document.getElementById("hdnREO").value=="RFA" || document.getElementById("hdnREO").value=="RFP"){
                    if(document.getElementById("txtRFAexpdtadvtRFA")!=null)
                    if(document.getElementById("txtRFAexpdtadvtRFA").value==''){
                       document.getElementById("ra1").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFAexpdtadvtRFANo")!=null)
                    if(document.getElementById("txtRFAexpdtadvtRFANo").value==''){
                        document.getElementById("ra1No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFAexpdtadvtRFANo").value)){
                            document.getElementById("ra1No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFAexpdtadvtRFANo").value))
                            {
                                document.getElementById("ra1No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFAexpdtadvtRFANo").value="";
                                document.getElementById("txtRFAexpdtadvtRFANo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFAdtrcptApp")!=null)
                    if(document.getElementById("txtRFAdtrcptApp").value==''){
                        document.getElementById("ra2").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFAdtrcptAppNo")!=null)
                    if(document.getElementById("txtRFAdtrcptAppNo").value==''){
                        document.getElementById("ra2No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFAdtrcptAppNo").value)){
                            document.getElementById("ra2No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFAdtrcptAppNo").value))
                            {
                                document.getElementById("ra2No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFAdtrcptAppNo").value="";
                                document.getElementById("txtRFAdtrcptAppNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFAdteveapp")!=null)
                    if(document.getElementById("txtRFAdteveapp").value==''){
                        document.getElementById("ra3").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFAdteveappNo")!=null)
                    if(document.getElementById("txtRFAdteveappNo").value==''){
                        document.getElementById("ra3No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFAdteveappNo").value)){
                            document.getElementById("ra3No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFAdteveappNo").value))
                            {
                                document.getElementById("ra3No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFAdteveappNo").value="";
                                document.getElementById("txtRFAdteveappNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFAdtintrvwselInd")!=null)
                    if(document.getElementById("txtRFAdtintrvwselInd").value==''){
                        document.getElementById("ra4").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFAdtintrvwselIndNo")!=null)
                    if(document.getElementById("txtRFAdtintrvwselIndNo").value==''){
                        document.getElementById("ra4No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFAdtintrvwselIndNo").value)){
                            document.getElementById("ra4No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFAdtintrvwselIndNo").value))
                            {
                                document.getElementById("ra4No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFAdtintrvwselIndNo").value="";
                                document.getElementById("txtRFAdtintrvwselIndNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFAdtevaFnlselLst")!=null)
                    if(document.getElementById("txtRFAdtevaFnlselLst").value==''){
                        document.getElementById("ra5").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFAdtevaFnlselLstNo")!=null)
                    if(document.getElementById("txtRFAdtevaFnlselLstNo").value==''){
                        document.getElementById("ra5No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFAdtevaFnlselLstNo").value)){
                            document.getElementById("ra5No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFAdtevaFnlselLstNo").value))
                            {
                                document.getElementById("ra5No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFAdtevaFnlselLstNo").value="";
                                document.getElementById("txtRFAdtevaFnlselLstNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFAdtsubevaRpt")!=null)
                    if(document.getElementById("txtRFAdtsubevaRpt").value==''){
                        document.getElementById("ra6").innerHTML="";
                        validatebool="false";
                    }

                    if(document.getElementById("txtRFAdtsubevaRptNo")!=null)
                    if(document.getElementById("txtRFAdtsubevaRptNo").value==''){
                        document.getElementById("ra6No").innerHTML="<br/>Please enter No. of days";
                        validatebool="false";
                    }else{
                        if(!numeric(document.getElementById("txtRFAdtsubevaRptNo").value)){
                            document.getElementById("ra6No").innerHTML="<br/>Please enter numbers only";
                            validatebool="false";
                        }
                        else
                        {
                            if(maxDay(document.getElementById("txtRFAdtsubevaRptNo").value))
                            {
                                document.getElementById("ra6No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFAdtsubevaRptNo").value="";
                                document.getElementById("txtRFAdtsubevaRptNo").focus();
                                validatebool="false";
                            }
                        }
                    }

                    if(document.getElementById("txtRFAdtappCons")!=null)
                    if(document.getElementById("txtRFAdtappCons").value==''){
                        document.getElementById("ra7").innerHTML="";
                        validatebool="false";
                    }
                }

               //alert(validatebool);
                if (validatebool=='true'){
                    return true;
                }else{
                    return false;
                }
            }

            //to hide error message
           function cleartxtpqdtadvtinvt()
            {
                        if(document.getElementById("txtpqdtadvtinvt")!=null)
                        if(document.getElementById("txtpqdtadvtinvt").value!=''){
                            document.getElementById("error").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtpqdtadvtinvtNo")!=null)
                        if(document.getElementById("txtpqdtadvtinvtNo").value!=''){
                            document.getElementById("errorNo").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtpqdtappsub")!=null)
                        if(document.getElementById("txtpqdtappsub").value!=''){
                            document.getElementById("e2").innerHTML="";
                             validatebool="false";
                        }

                        if(document.getElementById("txtPqdtappsubNo")!=null)
                        if(document.getElementById("txtPqdtappsubNo").value!=''){
                            document.getElementById("e2No").innerHTML="";
                             validatebool="false";
                        }

                        if(document.getElementById("txtPqdtsubevarpt")!=null)
                        if(document.getElementById("txtPqdtsubevarpt").value!=''){
                            document.getElementById("e3").innerHTML="";
                             validatebool="false";
                        }

                        if(document.getElementById("txtPqdtsubevarptNo")!=null)
                        if(document.getElementById("txtPqdtsubevarptNo").value!=''){
                            document.getElementById("e3No").innerHTML="";
                             validatebool="false";
                        }

                        if(document.getElementById("txtPqdtapplst")!=null)
                        if(document.getElementById("txtPqdtapplst").value!=''){
                            document.getElementById("e4").innerHTML="";
                             validatebool="false";
                        }

                        if(document.getElementById("txtRfqdtadvtift")!=null)
                        if(document.getElementById("txtRfqdtadvtift").value!=''){
                            document.getElementById("e5").innerHTML="";
                             validatebool="false";
                        }

                        if(document.getElementById("txtRfqdtadvtiftNo")!=null)
                        if(document.getElementById("txtRfqdtadvtiftNo").value!=''){
                            document.getElementById("e5No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqdtsub")!=null)
                        if(document.getElementById("txtRfqdtsub").value!=''){
                            document.getElementById("e6").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqdtsubNo")!=null)
                        if(document.getElementById("txtRfqdtsubNo").value!=''){
                            document.getElementById("e6No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqexpdtopen")!=null)
                        if(document.getElementById("txtRfqexpdtopen").value!=''){
                            document.getElementById("e7").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqexpdtopenNo")!=null)
                        if(document.getElementById("txtRfqexpdtopenNo").value!=''){
                            document.getElementById("e7No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqdtsubevaRpt")!=null)
                        if(document.getElementById("txtRfqdtsubevaRpt").value!=''){
                            document.getElementById("e8").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqdtsubevaRptNo")!=null)
                        if(document.getElementById("txtRfqdtsubevaRptNo").value!=''){
                            document.getElementById("e8No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqexpdtAppawd")!=null)
                        if(document.getElementById("txtRfqexpdtAppawd").value!=''){
                            document.getElementById("e9").innerHTML="";
                            validatebool="false";
                        }

                        //Code by Proshanto Kumar Saha
                        if(document.getElementById("txtRfqexpdtAppawdNo")!=null)
                        if(document.getElementById("txtRfqexpdtAppawdNo").value!=''){
                            document.getElementById("e81No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqexpdtLtrIntAwd")!=null)
                        if(document.getElementById("txtRfqexpdtLtrIntAwd").value!=''){
                            document.getElementById("e91").innerHTML="";
                            validatebool="false";
                        }
                        //Code end by Proshanto Kumar Saha
                       
                       //Id change by proshanto
                        if(document.getElementById("txtRfqexpdtLtrIntAwdNo")!=null)
                        if(document.getElementById("txtRfqexpdtLtrIntAwdNo").value!=''){
                            document.getElementById("e9No").innerHTML="";
                            validatebool="false";
                        }
                         //Id change end by proshanto

                        if(document.getElementById("txtRfqdtIssNOA")!=null)
                        if(document.getElementById("txtRfqdtIssNOA").value!=''){
                            document.getElementById("e10").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqdtIssNOANo")!=null)
                        if(document.getElementById("txtRfqdtIssNOANo").value!=''){
                            document.getElementById("e10No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqexpdtSign")!=null)
                        if(document.getElementById("txtRfqexpdtSign").value!=''){
                            document.getElementById("e11").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqexpdtSignNo")!=null)
                        if(document.getElementById("txtRfqexpdtSignNo").value!=''){
                            document.getElementById("e11No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRfqexpdtCompContract")!=null)
                        if(document.getElementById("txtRfqexpdtCompContract").value!=''){
                            document.getElementById("e12").innerHTML="";
                            validatebool="false";
                        }
           /////////////////  TSTM validation   //////////////////////////////
                        if(document.getElementById("txtTSTMexpdtadvtIFT")!=null)
                        if(document.getElementById("txtTSTMexpdtadvtIFT").value!=''){
                            document.getElementById("t1").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTMexpdtadvtIFTNo")!=null)
                        if(document.getElementById("txtTSTMexpdtadvtIFTNo").value!=''){
                            document.getElementById("t1No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTMexpdtSub")!=null)
                        if(document.getElementById("txtTSTMexpdtSub").value!=''){
                            document.getElementById("t2").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTMexpdtSubNo")!=null)
                        if(document.getElementById("txtTSTMexpdtSubNo").value!=''){
                            document.getElementById("t2No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTMexpdtopen")!=null)
                        if(document.getElementById("txtTSTMexpdtopen").value!=''){
                            document.getElementById("t3").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTMexpdtopenNo")!=null)
                        if(document.getElementById("txtTSTMexpdtopenNo").value!=''){
                            document.getElementById("t3No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTMexpdtsubEvaRpt")!=null)
                        if(document.getElementById("txtTSTMexpdtsubEvaRpt").value!=''){
                            document.getElementById("t4").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTMexpdtsubEvaRptNo")!=null)
                        if(document.getElementById("txtTSTMexpdtsubEvaRptNo").value!=''){
                            document.getElementById("t4No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTMexpdtappEvaRpt")!=null)
                        if(document.getElementById("txtTSTMexpdtappEvaRpt").value!=''){
                            document.getElementById("t5").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTM2expdtIssuefinalDoc")!=null)
                        if(document.getElementById("txtTSTM2expdtIssuefinalDoc").value!=''){
                            document.getElementById("t6").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTM2expdtIssuefinalDocNo")!=null)
                        if(document.getElementById("txtTSTM2expdtIssuefinalDocNo").value!=''){
                            document.getElementById("t6No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTM2expdtSub")!=null)
                        if(document.getElementById("txtTSTM2expdtSub").value!=''){
                            document.getElementById("t7").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTM2expdtSubNo")!=null)
                        if(document.getElementById("txtTSTM2expdtSubNo").value!=''){
                            document.getElementById("t7No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTM2expdtOpen")!=null)
                        if(document.getElementById("txtTSTM2expdtOpen").value!=''){
                            document.getElementById("t8").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTM2expdtOpenNo")!=null)
                        if(document.getElementById("txtTSTM2expdtOpenNo").value!=''){
                            document.getElementById("t8No").innerHTML="";
                            validatebool="false";
                        }


                        if(document.getElementById("txtTSTM2expdtsubevaRpt")!=null)
                        if(document.getElementById("txtTSTM2expdtsubevaRpt").value!=''){
                            document.getElementById("t9").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTM2expdtsubevaRptNo")!=null)
                        if(document.getElementById("txtTSTM2expdtsubevaRptNo").value!=''){
                            document.getElementById("t9No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTM2expdtAppEvaRpt")!=null)
                        if(document.getElementById("txtTSTM2expdtAppEvaRpt").value!=''){
                            document.getElementById("t10").innerHTML="";
                            validatebool="false";
                        }

                        //Id change by Proshanto
                        if(document.getElementById("txtTSTM2expdtAppEvaRptNo")!=null)
                        if(document.getElementById("txtTSTM2expdtAppEvaRptNo").value!=''){
                            document.getElementById("t91No").innerHTML="";
                            validatebool="false";
                        }
                        //Id end by Proshanto
                      
                        //Code by Proshanto
                        if(document.getElementById("txtTSTM2expdtLetterOfIntentNo")!=null)
                        if(document.getElementById("txtTSTM2expdtLetterOfIntentNo").value!=''){
                            document.getElementById("t10No").innerHTML="";
                            validatebool="false";
                        }
                        //Code end by Proshanto

                        //Code by Proshanto
                       if(document.getElementById("txtTSTM2expdtLetterOfIntent")!=null)
                        if(document.getElementById("txtTSTM2expdtLetterOfIntent").value!=''){
                            document.getElementById("t101").innerHTML="";
                            validatebool="false";
                        }
                        //End by Proshano
                        

                        if(document.getElementById("txtTSTM2expdtIssueNOA")!=null)
                        if(document.getElementById("txtTSTM2expdtIssueNOA").value!=''){
                            document.getElementById("t11").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTM2expdtIssueNOANo")!=null)
                        if(document.getElementById("txtTSTM2expdtIssueNOANo").value!=''){
                            document.getElementById("t11No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTM2expdtSignContract")!=null)
                        if(document.getElementById("txtTSTM2expdtSignContract").value!=''){
                            document.getElementById("t12").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtTSTM2expdtSignContractNo")!=null)
                        if(document.getElementById("txtTSTM2expdtSignContractNo").value!=''){
                            document.getElementById("t12No").innerHTML="";
                            validatebool="false";
                        }
         //---------------------
         //--------------------- REOI validation   -----------------
                        if(document.getElementById("txtREOQexpdtadvtREOI")!=null)
                        if(document.getElementById("txtREOQexpdtadvtREOI").value!=''){
                            document.getElementById("re1").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtREOQexpdtadvtREOINo")!=null)
                        if(document.getElementById("txtREOQexpdtadvtREOINo").value!=''){
                            document.getElementById("re1No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtREOQexpdtlstdtRcptEOI")!=null)
                        if(document.getElementById("txtREOQexpdtlstdtRcptEOI").value!=''){
                            document.getElementById("re2").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtREOQexpdtlstdtRcptEOINo")!=null)
                        if(document.getElementById("txtREOQexpdtlstdtRcptEOINo").value!=''){
                            document.getElementById("re2No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtREOQexpdtsubsrtlstFrm")!=null)
                        if(document.getElementById("txtREOQexpdtsubsrtlstFrm").value!=''){
                            document.getElementById("re3").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtREOQexpdtsubsrtlstFrmNo")!=null)
                        if(document.getElementById("txtREOQexpdtsubsrtlstFrmNo").value!=''){
                            document.getElementById("re3No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtREOQextdtAppsrtlstFrm")!=null)
                        if(document.getElementById("txtREOQextdtAppsrtlstFrm").value!=''){
                            document.getElementById("re4").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtREQexpdtcompContract")!=null)
                        if(document.getElementById("txtREQexpdtcompContract").value!=''){
                            document.getElementById("re5").innerHTML="";
                            validatebool="false";
                        }
         //----------------------------------/////////////////////////////////////////
         //----------------------------------RFP validation
                        if(document.getElementById("txtRFPexpdtissueRFP")!=null)
                        if(document.getElementById("txtRFPexpdtissueRFP").value!=''){
                            document.getElementById("rp1").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtissueRFPNo")!=null)
                        if(document.getElementById("txtRFPexpdtissueRFPNo").value!=''){
                            document.getElementById("rp1No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtSubProposal")!=null)
                        if(document.getElementById("txtRFPexpdtSubProposal").value!=''){
                            document.getElementById("rp2").innerHTML="";
                        }

                        if(document.getElementById("txtRFPexpdtSubProposalNo")!=null)
                        if(document.getElementById("txtRFPexpdtSubProposalNo").value!=''){
                            document.getElementById("rp2No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdttechOpen")!=null)
                        if(document.getElementById("txtRFPexpdttechOpen").value!=''){
                            document.getElementById("rp3").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdttechOpenNo")!=null)
                        if(document.getElementById("txtRFPexpdttechOpenNo").value!=''){
                            document.getElementById("rp3No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdttechEva")!=null)
                        if(document.getElementById("txtRFPexpdttechEva").value!=''){
                            document.getElementById("rp4").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdttechEvaNo")!=null)
                        if(document.getElementById("txtRFPexpdttechEvaNo").value!=''){
                            document.getElementById("rp4No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPextdtFinOpen")!=null)
                        if(document.getElementById("txtRFPextdtFinOpen").value!=''){
                            document.getElementById("rp5").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPextdtFinOpenNo")!=null)
                        if(document.getElementById("txtRFPextdtFinOpenNo").value!=''){
                            document.getElementById("rp5No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtsubCOmEvaRpt")!=null)
                        if(document.getElementById("txtRFPexpdtsubCOmEvaRpt").value!=''){
                            document.getElementById("rp6").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtsubCOmEvaRptNo")!=null)
                        if(document.getElementById("txtRFPexpdtsubCOmEvaRptNo").value!=''){
                            document.getElementById("rp6No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtappcomEvaRpt")!=null)
                        if(document.getElementById("txtRFPexpdtappcomEvaRpt").value!=''){
                            document.getElementById("rp7").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtappcomEvaRptNo")!=null)
                        if(document.getElementById("txtRFPexpdtappcomEvaRptNo").value!=''){
                            document.getElementById("rp7No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtcompNego")!=null)
                        if(document.getElementById("txtRFPexpdtcompNego").value!=''){
                            document.getElementById("rp8").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtcompNegoNo")!=null)
                        if(document.getElementById("txtRFPexpdtcompNegoNo").value!=''){
                            document.getElementById("rp8No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtappawdContract")!=null)
                        if(document.getElementById("txtRFPexpdtappawdContract").value!=''){
                            document.getElementById("rp9").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtappawdContractNo")!=null)
                        if(document.getElementById("txtRFPexpdtappawdContractNo").value!=''){
                            document.getElementById("rp9No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtsigncontract")!=null)
                        if(document.getElementById("txtRFPexpdtsigncontract").value!=''){
                            document.getElementById("rp10").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtsigncontractNo")!=null)
                        if(document.getElementById("txtRFPexpdtsigncontractNo").value!=''){
                            document.getElementById("rp10No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFPexpdtcomplcontract")!=null)
                        if(document.getElementById("txtRFPexpdtcomplcontract").value!=''){
                            document.getElementById("rp11").innerHTML="";
                            validatebool="false";
                        }


                        //-------------------------------------------------
                        if(document.getElementById("txtRFAexpdtadvtRFA")!=null)
                        if(document.getElementById("txtRFAexpdtadvtRFA").value!=''){
                            document.getElementById("ra1").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAexpdtadvtRFANo")!=null)
                        if(document.getElementById("txtRFAexpdtadvtRFANo").value!=''){
                            document.getElementById("ra1No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAdtrcptApp")!=null)
                        if(document.getElementById("txtRFAdtrcptApp").value!=''){
                            document.getElementById("ra2").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAdtrcptAppNo")!=null)
                        if(document.getElementById("txtRFAdtrcptAppNo").value!=''){
                            document.getElementById("ra2No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAdteveapp")!=null)
                        if(document.getElementById("txtRFAdteveapp").value!=''){
                            document.getElementById("ra3").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAdteveappNo")!=null)
                        if(document.getElementById("txtRFAdteveappNo").value!=''){
                            document.getElementById("ra3No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAdtintrvwselInd")!=null)
                        if(document.getElementById("txtRFAdtintrvwselInd").value!=''){
                            document.getElementById("ra4").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAdtintrvwselIndNo")!=null)
                        if(document.getElementById("txtRFAdtintrvwselIndNo").value!=''){
                            document.getElementById("ra4No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAdtevaFnlselLst")!=null)
                        if(document.getElementById("txtRFAdtevaFnlselLst").value!=''){
                            document.getElementById("ra5").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAdtevaFnlselLstNo")!=null)
                        if(document.getElementById("txtRFAdtevaFnlselLstNo").value!=''){
                            document.getElementById("ra5No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAdtsubevaRpt")!=null)
                        if(document.getElementById("txtRFAdtsubevaRpt").value!=''){
                            document.getElementById("ra6").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAdtsubevaRptNo")!=null)
                        if(document.getElementById("txtRFAdtsubevaRptNo").value!=''){
                            document.getElementById("ra6No").innerHTML="";
                            validatebool="false";
                        }

                        if(document.getElementById("txtRFAdtappCons")!=null)
                        if(document.getElementById("txtRFAdtappCons").value!=''){
                            document.getElementById("ra7").innerHTML="";
                            validatebool="false";
                        }
                        //-------------------------------------------------------------
                        if(document.getElementById("txtpqdtadvtinvtNo")!=null)
                        if(numeric(document.getElementById("txtpqdtadvtinvtNo").value)){
                            if(!maxDay(document.getElementById("txtpqdtadvtinvtNo").value)){
                                document.getElementById("errorNo").innerHTML="";
                            }else{
                                document.getElementById("errorNo").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtpqdtadvtinvtNo").value="";
                                document.getElementById("txtpqdtadvtinvtNo").focus();
                            }
                        }else{
                            document.getElementById("errorNo").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtPqdtappsubNo")!=null)
                        if(numeric(document.getElementById("txtPqdtappsubNo").value)){
                            if(!maxDay(document.getElementById("txtPqdtappsubNo").value)){
                                document.getElementById("e2No").innerHTML="";
                            }else{
                                document.getElementById("e2No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtPqdtappsubNo").value="";
                                document.getElementById("txtPqdtappsubNo").focus();
                            }
                        }else{
                            document.getElementById("e2No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtPqdtsubevarptNo")!=null)
                        if(numeric(document.getElementById("txtPqdtsubevarptNo").value)){
                            if(!maxDay(document.getElementById("txtPqdtsubevarptNo").value)){
                                document.getElementById("e3No").innerHTML="";
                            }else{
                                document.getElementById("e3No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtPqdtsubevarptNo").value="";
                                document.getElementById("txtPqdtsubevarptNo").focus();
                            }
                        }else{
                            document.getElementById("e3No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRfqdtadvtiftNo")!=null)
                        if(numeric(document.getElementById("txtRfqdtadvtiftNo").value)){
                            if(!maxDay(document.getElementById("txtRfqdtadvtiftNo").value)){
                                document.getElementById("e5No").innerHTML="";
                            }else{
                                document.getElementById("e5No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqdtadvtiftNo").value="";
                                document.getElementById("txtRfqdtadvtiftNo").focus();
                            }
                        }else{
                            document.getElementById("e5No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRfqdtsubNo")!=null)
                        if(numeric(document.getElementById("txtRfqdtsubNo").value)){
                            if(!maxDay(document.getElementById("txtRfqdtsubNo").value)){
                                document.getElementById("e6No").innerHTML="";
                            }else{
                                document.getElementById("e6No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqdtsubNo").value="";
                                document.getElementById("txtRfqdtsubNo").focus();
                            }
                        }else{
                            document.getElementById("e6No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRfqexpdtopenNo")!=null)
                        if(numeric(document.getElementById("txtRfqexpdtopenNo").value)){
                            if(!maxDay(document.getElementById("txtRfqexpdtopenNo").value)){
                                document.getElementById("e7No").innerHTML="";
                            }else{
                                document.getElementById("e7No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqexpdtopenNo").value="";
                                document.getElementById("txtRfqexpdtopenNo").focus();
                            }
                        }else{
                            document.getElementById("e7No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRfqdtsubevaRptNo")!=null)
                        if(numeric(document.getElementById("txtRfqdtsubevaRptNo").value)){
                            if(!maxDay(document.getElementById("txtRfqdtsubevaRptNo").value)){
                                document.getElementById("e8No").innerHTML="";
                            }else{
                                document.getElementById("e8No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqdtsubevaRptNo").value="";
                                document.getElementById("txtRfqdtsubevaRptNo").focus();
                            }
                        }else{
                            document.getElementById("e8No").innerHTML="<br/>Please enter numbers without decimal .";
                        }
                         //Code by Proshanto Kumar Saha
                           if(document.getElementById("txtRfqexpdtAppawdNo")!=null)
                        if(numeric(document.getElementById("txtRfqexpdtAppawdNo").value)){
                            if(!maxDay(document.getElementById("txtRfqexpdtAppawdNo").value)){
                                document.getElementById("e81No").innerHTML="";
                            }else{
                                document.getElementById("e81No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqexpdtAppawdNo").value="";
                                document.getElementById("txtRfqexpdtAppawdNo").focus();
                            }
                        }else{
                            document.getElementById("e81No").innerHTML="<br/>Please enter Positive Numerals without Decimal.";
                        }
                        //Code end by Proshanto Kumar Saha

                        //Id change by Proshanto Kumar Saha
                        if(document.getElementById("txtRfqexpdtLtrIntAwdNo")!=null)
                        if(numeric(document.getElementById("txtRfqexpdtLtrIntAwdNo").value)){
                            if(!maxDay(document.getElementById("txtRfqexpdtLtrIntAwdNo").value)){
                                document.getElementById("e9No").innerHTML="";
                            }else{
                                document.getElementById("e9No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqexpdtLtrIntAwdNo").value="";
                                document.getElementById("txtRfqexpdtLtrIntAwdNo").focus();
                            }
                        }else{
                            document.getElementById("e9No").innerHTML="<br/>Please enter numbers without decimal .";
                        }
                        // Id change end by proshanto
                        if(document.getElementById("txtRfqdtIssNOANo")!=null)

                        if(numeric(document.getElementById("txtRfqdtIssNOANo").value)){
                            if(!maxDay(document.getElementById("txtRfqdtIssNOANo").value)){
                                document.getElementById("e10No").innerHTML="";
                            }else{
                                document.getElementById("e10No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqdtIssNOANo").value="";
                                document.getElementById("txtRfqdtIssNOANo").focus();
                            }
                        }else{
                            document.getElementById("e10No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRfqexpdtSignNo")!=null)
                        if(numeric(document.getElementById("txtRfqexpdtSignNo").value)){
                            if(!maxDay(document.getElementById("txtRfqexpdtSignNo").value)){
                                document.getElementById("e11No").innerHTML="";
                            }else{
                                document.getElementById("e11No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRfqexpdtSignNo").value="";
                                document.getElementById("txtRfqexpdtSignNo").focus();
                            }
                        }else{
                            document.getElementById("e11No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtTSTMexpdtadvtIFTNo")!=null)
                        if(numeric(document.getElementById("txtTSTMexpdtadvtIFTNo").value)){
                            if(!maxDay(document.getElementById("txtTSTMexpdtadvtIFTNo").value)){
                                document.getElementById("t1No").innerHTML="";
                            }else{
                                document.getElementById("t1No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTMexpdtadvtIFTNo").value="";
                                document.getElementById("txtTSTMexpdtadvtIFTNo").focus();
                            }
                        }else{
                            document.getElementById("t1No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtTSTMexpdtSubNo")!=null)
                        if(numeric(document.getElementById("txtTSTMexpdtSubNo").value)){
                            if(!maxDay(document.getElementById("txtTSTMexpdtSubNo").value)){
                                document.getElementById("t2No").innerHTML="";
                            }else{
                                document.getElementById("t2No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTMexpdtSubNo").value="";
                                document.getElementById("txtTSTMexpdtSubNo").focus();
                            }
                        }else{
                            document.getElementById("t2No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtTSTMexpdtopenNo")!=null)
                        if(numeric(document.getElementById("txtTSTMexpdtopenNo").value)){
                            if(!maxDay(document.getElementById("txtTSTMexpdtopenNo").value)){
                                document.getElementById("t3No").innerHTML="";
                            }else{
                                document.getElementById("t3No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTMexpdtopenNo").value="";
                                document.getElementById("txtTSTMexpdtopenNo").focus();
                            }
                        }else
                        {
                            document.getElementById("t3No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtTSTMexpdtsubEvaRptNo")!=null)
                        if(numeric(document.getElementById("txtTSTMexpdtsubEvaRptNo").value)){
                            if(!maxDay(document.getElementById("txtTSTMexpdtsubEvaRptNo").value)){
                                document.getElementById("t4No").innerHTML="";
                            }else{
                                document.getElementById("t4No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTMexpdtsubEvaRptNo").value="";
                                document.getElementById("txtTSTMexpdtsubEvaRptNo").focus();
                            }
                        }else
                        {
                            document.getElementById("t4No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtTSTM2expdtIssuefinalDocNo")!=null)
                        if(numeric(document.getElementById("txtTSTM2expdtIssuefinalDocNo").value)){
                            if(!maxDay(document.getElementById("txtTSTM2expdtIssuefinalDocNo").value)){
                                document.getElementById("t6No").innerHTML="";
                            }else{
                                document.getElementById("t6No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtIssuefinalDocNo").value="";
                                document.getElementById("txtTSTM2expdtIssuefinalDocNo").focus();
                            }
                        }else
                        {
                            document.getElementById("t6No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtTSTM2expdtSubNo")!=null)
                        if(numeric(document.getElementById("txtTSTM2expdtSubNo").value)){
                            if(!maxDay(document.getElementById("txtTSTM2expdtSubNo").value)){
                                document.getElementById("t7No").innerHTML="";
                            }else{
                                document.getElementById("t7No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtSubNo").value="";
                                document.getElementById("txtTSTM2expdtSubNo").focus();
                            }
                        }else
                        {
                            document.getElementById("t7No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtTSTM2expdtOpenNo")!=null)
                        if(numeric(document.getElementById("txtTSTM2expdtOpenNo").value)){
                            if(!maxDay(document.getElementById("txtTSTM2expdtOpenNo").value)){
                                document.getElementById("t8No").innerHTML="";
                            }else{
                                document.getElementById("t8No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtOpenNo").value="";
                                document.getElementById("txtTSTM2expdtOpenNo").focus();
                            }
                        }else
                        {
                            document.getElementById("t8No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtTSTM2expdtsubevaRptNo")!=null)
                        if(numeric(document.getElementById("txtTSTM2expdtsubevaRptNo").value)){
                           if(!maxDay(document.getElementById("txtTSTM2expdtsubevaRptNo").value)){
                                document.getElementById("t9No").innerHTML="";
                            }else{
                                document.getElementById("t9No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtsubevaRptNo").value="";
                                document.getElementById("txtTSTM2expdtsubevaRptNo").focus();
                            }
                        }else
                        {
                            document.getElementById("t9No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        //Id change by proshanto
                        if(document.getElementById("txtTSTM2expdtAppEvaRptNo")!=null)
                        if(numeric(document.getElementById("txtTSTM2expdtAppEvaRptNo").value)){
                            if(!maxDay(document.getElementById("txtTSTM2expdtAppEvaRptNo").value)){
                                document.getElementById("t91No").innerHTML="";
                            }else{
                                document.getElementById("t91No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtAppEvaRptNo").value="";
                                document.getElementById("txtTSTM2expdtAppEvaRptNo").focus();
                            }
                        }else
                        {
                            document.getElementById("t91No").innerHTML="<br/>Please enter numbers without decimal .";
                        }
                        //Id end

                        //Code by proshanto
                        if(document.getElementById("txtTSTM2expdtLetterOfIntentNo")!=null)
                        if(numeric(document.getElementById("txtTSTM2expdtLetterOfIntentNo").value)){
                            if(!maxDay(document.getElementById("txtTSTM2expdtLetterOfIntentNo").value)){
                                document.getElementById("t10No").innerHTML="";
                            }else{
                                document.getElementById("t10No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtLetterOfIntentNo").value="";
                                document.getElementById("txtTSTM2expdtLetterOfIntentNo").focus();
                            }
                        }else
                        {
                            document.getElementById("t10No").innerHTML="<br/>Please enter numbers without decimal .";
                        }
                        //Code end

                        if(document.getElementById("txtTSTM2expdtIssueNOANo")!=null)
                        if(numeric(document.getElementById("txtTSTM2expdtIssueNOANo").value)){
                            if(!maxDay(document.getElementById("txtTSTM2expdtIssueNOANo").value)){
                                document.getElementById("t11No").innerHTML="";
                            }else{
                                document.getElementById("t11No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtIssueNOANo").value="";
                                document.getElementById("txtTSTM2expdtIssueNOANo").focus();
                            }
                        }else
                        {
                            document.getElementById("t11No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtTSTM2expdtSignContractNo")!=null)
                        if(numeric(document.getElementById("txtTSTM2expdtSignContractNo").value)){
                            if(!maxDay(document.getElementById("txtTSTM2expdtSignContractNo").value)){
                                document.getElementById("t12No").innerHTML="";
                            }else{
                                document.getElementById("t12No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtTSTM2expdtSignContractNo").value="";
                                document.getElementById("txtTSTM2expdtSignContractNo").focus();
                            }
                        }else
                        {
                            document.getElementById("t12No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtREOQexpdtadvtREOINo")!=null)
                        if(numeric(document.getElementById("txtREOQexpdtadvtREOINo").value)){
                            if(!maxDay(document.getElementById("txtREOQexpdtadvtREOINo").value)){
                                document.getElementById("re1No").innerHTML="";
                            }else{
                                document.getElementById("re1No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtREOQexpdtadvtREOINo").value="";
                                document.getElementById("txtREOQexpdtadvtREOINo").focus();
                            }
                        }else
                        {
                            document.getElementById("re1No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtREOQexpdtlstdtRcptEOINo")!=null)
                        if(numeric(document.getElementById("txtREOQexpdtlstdtRcptEOINo").value)){
                           if(!maxDay(document.getElementById("txtREOQexpdtlstdtRcptEOINo").value)){
                                document.getElementById("re2No").innerHTML="";
                            }else{
                                document.getElementById("re2No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtREOQexpdtlstdtRcptEOINo").value="";
                                document.getElementById("txtREOQexpdtlstdtRcptEOINo").focus();
                            }
                        }else
                        {
                            document.getElementById("re2No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtREOQexpdtsubsrtlstFrmNo")!=null)
                        if(numeric(document.getElementById("txtREOQexpdtsubsrtlstFrmNo").value)){
                            if(!maxDay(document.getElementById("txtREOQexpdtsubsrtlstFrmNo").value)){
                                document.getElementById("re3No").innerHTML="";
                            }else{
                                document.getElementById("re3No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtREOQexpdtsubsrtlstFrmNo").value="";
                                document.getElementById("txtREOQexpdtsubsrtlstFrmNo").focus();
                            }
                        }else
                        {
                            document.getElementById("re3No").innerHTML="<br/>Please enter numbers without decimal .";
                        }
                        //-----------------------------RFP numeric validation
                        if(document.getElementById("txtRFPexpdtissueRFPNo")!=null)
                        if(numeric(document.getElementById("txtRFPexpdtissueRFPNo").value)){
                           if(!maxDay(document.getElementById("txtRFPexpdtissueRFPNo").value)){
                                document.getElementById("rp1No").innerHTML="";
                            }else{
                                document.getElementById("rp1No").innerHTML="<br/>Max 999 days are allowed";
                                document.getElementById("txtRFPexpdtissueRFPNo").value="";
                                document.getElementById("txtRFPexpdtissueRFPNo").focus();
                            }
                        }else
                        {
                            document.getElementById("rp1No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFPexpdtSubProposalNo")!=null)
                        if(numeric(document.getElementById("txtRFPexpdtSubProposalNo").value)){
                            if(!maxDay(document.getElementById("txtRFPexpdtSubProposalNo").value)){
                                document.getElementById("rp2No").innerHTML="";
                            }else{
                                document.getElementById("rp2No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFPexpdtSubProposalNo").value="";
                                document.getElementById("txtRFPexpdtSubProposalNo").focus();
                            }
                        }else
                        {
                            document.getElementById("rp2No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFPexpdttechOpenNo")!=null)
                        if(numeric(document.getElementById("txtRFPexpdttechOpenNo").value)){
                            if(!maxDay(document.getElementById("txtRFPexpdttechOpenNo").value)){
                                document.getElementById("rp3No").innerHTML="";
                            }else{
                                document.getElementById("rp3No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFPexpdttechOpenNo").value="";
                                document.getElementById("txtRFPexpdttechOpenNo").focus();
                            }
                        }else
                        {
                            document.getElementById("rp3No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFPexpdttechEvaNo")!=null)
                        if(numeric(document.getElementById("txtRFPexpdttechEvaNo").value)){
                            if(!maxDay(document.getElementById("txtRFPexpdttechOpenNo").value)){
                                document.getElementById("rp4No").innerHTML="";
                            }else{
                                document.getElementById("rp4No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFPexpdttechOpenNo").value="";
                                document.getElementById("txtRFPexpdttechOpenNo").focus();
                            }
                        }else
                        {
                            document.getElementById("rp4No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFPextdtFinOpenNo")!=null)
                        if(numeric(document.getElementById("txtRFPextdtFinOpenNo").value)){
                            if(!maxDay(document.getElementById("txtRFPextdtFinOpenNo").value)){
                                document.getElementById("rp5No").innerHTML="";
                            }else{
                                document.getElementById("rp5No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFPextdtFinOpenNo").value="";
                                document.getElementById("txtRFPextdtFinOpenNo").focus();
                            }
                        }else
                        {
                            document.getElementById("rp5No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFPexpdtsubCOmEvaRptNo")!=null)
                        if(numeric(document.getElementById("txtRFPexpdtsubCOmEvaRptNo").value)){
                            if(!maxDay(document.getElementById("txtRFPexpdtsubCOmEvaRptNo").value)){
                                document.getElementById("rp6No").innerHTML="";
                            }else{
                                document.getElementById("rp6No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFPexpdtsubCOmEvaRptNo").value="";
                                document.getElementById("txtRFPexpdtsubCOmEvaRptNo").focus();
                            }
                        }else
                        {
                            document.getElementById("rp6No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFPexpdtappcomEvaRptNo")!=null)
                        if(numeric(document.getElementById("txtRFPexpdtappcomEvaRptNo").value)){
                            if(!maxDay(document.getElementById("txtRFPexpdtappcomEvaRptNo").value)){
                                document.getElementById("rp7No").innerHTML="";
                            }else{
                                document.getElementById("rp7No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFPexpdtappcomEvaRptNo").value="";
                                document.getElementById("txtRFPexpdtappcomEvaRptNo").focus();
                            }
                        }else
                        {
                            document.getElementById("rp7No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFPexpdtcompNegoNo")!=null)
                        if(numeric(document.getElementById("txtRFPexpdtcompNegoNo").value)){
                            if(!maxDay(document.getElementById("txtRFPexpdtcompNegoNo").value)){
                                document.getElementById("rp8No").innerHTML="";
                            }else{
                                document.getElementById("rp8No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFPexpdtcompNegoNo").value="";
                                document.getElementById("txtRFPexpdtcompNegoNo").focus();
                            }
                        }else
                        {
                            document.getElementById("rp8No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFPexpdtappawdContractNo")!=null)
                        if(numeric(document.getElementById("txtRFPexpdtappawdContractNo").value)){
                            if(!maxDay(document.getElementById("txtRFPexpdtappawdContractNo").value)){
                                document.getElementById("rp9No").innerHTML="";
                            }else{
                                document.getElementById("rp9No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFPexpdtappawdContractNo").value="";
                                document.getElementById("txtRFPexpdtappawdContractNo").focus();
                            }
                        }else
                        {
                            document.getElementById("rp9No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFPexpdtsigncontractNo")!=null)
                        if(numeric(document.getElementById("txtRFPexpdtsigncontractNo").value)){
                            if(!maxDay(document.getElementById("txtRFPexpdtsigncontractNo").value)){
                                document.getElementById("rp10No").innerHTML="";
                            }else{
                                document.getElementById("rp10No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFPexpdtsigncontractNo").value="";
                                document.getElementById("txtRFPexpdtsigncontractNo").focus();
                            }
                        }else
                        {
                            document.getElementById("rp10No").innerHTML="<br/>Please enter numbers without decimal .";
                        }
                        //---------------------------------------------------
                        if(document.getElementById("txtRFAexpdtadvtRFANo")!=null)
                        if(numeric(document.getElementById("txtRFAexpdtadvtRFANo").value)){
                            if(!maxDay(document.getElementById("txtRFAexpdtadvtRFANo").value)){
                                document.getElementById("ra1No").innerHTML="";
                            }else{
                                document.getElementById("ra1No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFAexpdtadvtRFANo").value="";
                                document.getElementById("txtRFAexpdtadvtRFANo").focus();
                            }
                        }else
                        {
                            document.getElementById("ra1No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFAdtrcptAppNo")!=null)
                        if(numeric(document.getElementById("txtRFAdtrcptAppNo").value)){
                            if(!maxDay(document.getElementById("txtRFAdtrcptAppNo").value)){
                                document.getElementById("ra2No").innerHTML="";
                            }else{
                                document.getElementById("ra2No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFAdtrcptAppNo").value="";
                                document.getElementById("txtRFAdtrcptAppNo").focus();
                            }
                        }else
                        {
                            document.getElementById("ra2No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFAdteveappNo")!=null)
                        if(numeric(document.getElementById("txtRFAdteveappNo").value)){
                            if(!maxDay(document.getElementById("txtRFAdteveappNo").value)){
                                document.getElementById("ra3No").innerHTML="";
                            }else{
                                document.getElementById("ra3No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFAdteveappNo").value="";
                                document.getElementById("txtRFAdteveappNo").focus();
                            }
                        }else
                        {
                            document.getElementById("ra3No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFAdtintrvwselIndNo")!=null)
                        if(numeric(document.getElementById("txtRFAdtintrvwselIndNo").value)){
                            if(!maxDay(document.getElementById("txtRFAdtintrvwselIndNo").value)){
                                document.getElementById("ra4No").innerHTML="";
                            }else{
                                document.getElementById("ra4No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFAdtintrvwselIndNo").value="";
                                document.getElementById("txtRFAdtintrvwselIndNo").focus();
                            }
                        }else
                        {
                            document.getElementById("ra4No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFAdtevaFnlselLstNo")!=null)
                        if(numeric(document.getElementById("txtRFAdtevaFnlselLstNo").value)){
                            if(!maxDay(document.getElementById("txtRFAdtevaFnlselLstNo").value)){
                                document.getElementById("ra5No").innerHTML="";
                            }else{
                                document.getElementById("ra5No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFAdtevaFnlselLstNo").value="";
                                document.getElementById("txtRFAdtevaFnlselLstNo").focus();
                            }
                        }else
                        {
                            document.getElementById("ra5No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

                        if(document.getElementById("txtRFAdtsubevaRptNo")!=null)
                        if(numeric(document.getElementById("txtRFAdtsubevaRptNo").value)){
                            if(!maxDay(document.getElementById("txtRFAdtsubevaRptNo").value)){
                                document.getElementById("ra6No").innerHTML="";
                            }else{
                                document.getElementById("ra6No").innerHTML="<br/>Max 999 days are allowed";
																document.getElementById("txtRFAdtsubevaRptNo").value="";
                                document.getElementById("txtRFAdtsubevaRptNo").focus();
                            }
                        }else
                        {
                            document.getElementById("ra6No").innerHTML="<br/>Please enter numbers without decimal .";
                        }

               }

            
    //Function for CompareToForGreaterEqual
    function CompareToForGreaterEqual(textbox1,textbox2)
    {
	var StartDate = textbox1;
	var EndDate = textbox2;

	var StrStartArray=StartDate.split("/");
	var StrEndArray=EndDate.split("/");


        //alert('FDate:');
        //alert('Date : '+StrStartArray[0]);
        //alert('Month : '+StrStartArray[1]);
        //alert('Year : '+StrStartArray[2]);
        //alert('SDate:');
        //alert('Date : '+StrEndArray[0]);
        //alert('Month : '+StrEndArray[1]);
        //alert('Year : '+StrEndArray[2]);
        if(eval(StrStartArray[2])==eval(StrEndArray[2]) && eval(StrStartArray[0])==eval(StrEndArray[0]) && eval(StrStartArray[1])==eval(StrEndArray[1])){
            return false;
        }

	if(eval(StrStartArray[2])<eval(StrEndArray[2]))
        {
            return true;
        }
        else if(eval(StrStartArray[2])>eval(StrEndArray[2]))
	{
            return false;
	}
	else if(eval(StrStartArray[2])==eval(StrEndArray[2]))
	{
            if(eval(StrStartArray[1])<eval(StrEndArray[1]))
            {
                return true;
            }
            else if(eval(StrStartArray[1])>eval(StrEndArray[1]))
            {
                return false;
            }
            else if(eval(StrStartArray[1])==eval(StrEndArray[1]))
            {
                if(eval(StrStartArray[0])<eval(StrEndArray[0])){
                    return true;
                }
                else if(eval(StrStartArray[0])>eval(StrEndArray[0]))
                {
                    return false;
                }
                else if(eval(StrStartArray[0])==eval(StrEndArray[0]))
                {
                    return true;
                }
            }
	}
	return true;
    }

    function GetCalWithCond(txtname,controlname,msgId)
    {
        new Calendar({
            inputField: txtname,
            trigger: controlname,
            showTime: false,
            dateFormat:"%d/%m/%Y",
            onSelect: function() {
                    var date = Calendar.intToDate(this.selection.get());
                    var cDate = new Date();
                    var selDate = date.getDate()+"/"+date.getMonth()+"/"+date.getFullYear();
                    var curDate = cDate.getDate()+"/"+cDate.getMonth()+"/"+cDate.getFullYear();

                    if(CompareToForGreaterEqual(curDate,selDate)){
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        document.getElementById(msgId).innerHTML = "";
                        document.getElementById(txtname).focus();
                        this.hide();
                    }else{
                        document.getElementById(txtname).value="";
                        document.getElementById(msgId).innerHTML = "<br/>Date must be greater than the current date";
                    }
                }
            });


        var LEFT_CAL = Calendar.setup({
        weekNumbers: false
        });
    }

    function GetCal(txtname,controlname)
    {
        new Calendar({
            inputField: txtname,
            trigger: controlname,
            showTime: false,
            dateFormat:"%d/%m/%Y",
            onSelect: function() {
                var date = Calendar.intToDate(this.selection.get());
                LEFT_CAL.args.min = date;
                LEFT_CAL.redraw();
                this.hide();
                document.getElementById(txtname).focus();
                }
            });


             var LEFT_CAL = Calendar.setup({
        weekNumbers: false
        });
    }

//////////////////////////////////  PQ Tenders Dates - start  //////////////
    function setPqdtappsubDate(){
        try{
            if(document.getElementById("txtpqdtadvtinvt").value!="" && document.getElementById("txtpqdtadvtinvtNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtpqdtadvtinvt').val()+"&param2="+$('#txtpqdtadvtinvtNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtpqdtappsub").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err2").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err2").innerHTML = "";
                        if(setPqdtsubevarptDate()){
                            if(setPqdtapplstDate()){
                                return true;
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
            //alert(e);
        }
    }
    function setPqdtsubevarptDate(){
        try{
            if(document.getElementById("txtpqdtappsub").value!="" && document.getElementById("txtPqdtappsubNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtpqdtappsub').val()+"&param2="+$('#txtPqdtappsubNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtPqdtsubevarpt").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err3").innerHTML = ""+j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                         else
                            document.getElementById("err3").innerHTML = "";
                        if(setPqdtapplstDate()){
                            return true;
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
            //alert(e);
        }
    }
    function setPqdtapplstDate(){
        try{
            if(document.getElementById("txtPqdtsubevarpt").value!="" && document.getElementById("txtPqdtsubevarptNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtPqdtsubevarpt').val()+"&param2="+$('#txtPqdtsubevarptNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtPqdtapplst").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err4").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err4").innerHTML = "";
                        return true;
                     }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
            //alert(e);
        }
    }

    //////////////////////////////////  Tender/RFQ Dates - start  //////////////
    function setRfqdtsubDate(){
        try{
            if(document.getElementById("txtRfqdtadvtift").value!="" && document.getElementById("txtRfqdtadvtiftNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRfqdtadvtift').val()+"&param2="+$('#txtRfqdtadvtiftNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRfqdtsub").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err6").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err6").innerHTML = "";
                        if(setRfqexpdtopenDate()){
                            if(setRfqdtsubevaRptDate()){
                                if(setRfqexpdtAppawdDate()){
                                    //Code by Proshanto
                                    if(setRfqLtrIntAwdDate()){
                                    if(setRfqdtIssNOADate()){
                                        if(setRfqexpdtSignDate()){
                                            if(setRfqexpdtCompContractDate()){
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }


                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
           // alert('e1 : '+e);
        }
    }
    function setRfqexpdtopenDate(){
        try{
            if(document.getElementById("txtRfqdtsub").value!="" && document.getElementById("txtRfqdtsubNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRfqdtsub').val()+"&param2="+$('#txtRfqdtsubNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRfqexpdtopen").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err7").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err7").innerHTML = "";
                        if(setRfqdtsubevaRptDate()){
                            if(setRfqexpdtAppawdDate()){
                                //Code by proshanto
                                if(setRfqLtrIntAwdDate()){
                                if(setRfqdtIssNOADate()){
                                    if(setRfqexpdtSignDate()){
                                        if(setRfqexpdtCompContractDate()){
                                            return true;
                                        }
                                    }
                                }
                            }
                            }
                        }
                    }
                });
                return false;
            } else   {
                return false;
            }
        }catch(e){
            //alert('e2 : '+e);
        }
    }
    function setRfqdtsubevaRptDate(){
        try{
            if(document.getElementById("txtRfqexpdtopen").value!="" && document.getElementById("txtRfqexpdtopenNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRfqexpdtopen').val()+"&param2="+$('#txtRfqexpdtopenNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRfqdtsubevaRpt").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err8").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err8").innerHTML = "";
                        if(setRfqexpdtAppawdDate()){
                            //Code by proshanto
                            if(setRfqLtrIntAwdDate()){
                            if(setRfqdtIssNOADate()){
                                if(setRfqexpdtSignDate()){
                                    if(setRfqexpdtCompContractDate()){
                                        return true;
                                    }
                                }
                            }
                        }
                       }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
            //alert('e3 : '+e);
        }
    }
    function setRfqexpdtAppawdDate(){
        try{
            if(document.getElementById("txtRfqdtsubevaRpt").value!="" && document.getElementById("txtRfqdtsubevaRptNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRfqdtsubevaRpt').val()+"&param2="+$('#txtRfqdtsubevaRptNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRfqexpdtAppawd").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err9").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err9").innerHTML = "";
                         //Code added by Proshanto Kumar Saha,Dohatec
                        if(setRfqLtrIntAwdDate()){
                        if(setRfqdtIssNOADate()){
                            if(setRfqexpdtSignDate()){
                                if(setRfqexpdtCompContractDate()){
                                    return true;
                                }
                            }
                        }
                    }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
            //alert('e4 : '+e);
        }
      }


    //Code  by Proshanto
     function setRfqLtrIntAwdDate(){
        try{
            if(document.getElementById("txtRfqexpdtAppawd").value!="" && document.getElementById("txtRfqexpdtAppawdNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRfqexpdtAppawd').val()+"&param2="+$('#txtRfqexpdtAppawdNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRfqexpdtLtrIntAwd").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err91").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err91").innerHTML = "";
                        if(setRfqdtIssNOADate()){
                            if(setRfqexpdtSignDate()){
                                if(setRfqexpdtCompContractDate()){
                                    return true;
                                }
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
            //alert('e4 : '+e);
        }
    }
    //end by proshanto

    function setRfqdtIssNOADate(){
        try{
            if(document.getElementById("txtRfqexpdtLtrIntAwd").value!="" && document.getElementById("txtRfqexpdtLtrIntAwdNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRfqexpdtLtrIntAwd').val()+"&param2="+$('#txtRfqexpdtLtrIntAwdNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRfqdtIssNOA").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err10").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err10").innerHTML = "";
                        if(setRfqexpdtSignDate()){
                            if(setRfqexpdtCompContractDate()){
                                return true;
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
            //alert('e5 : '+e);
        }
    }
    function setRfqexpdtSignDate(){
        try{
            if(document.getElementById("txtRfqdtIssNOA").value!="" && document.getElementById("txtRfqdtIssNOANo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRfqdtIssNOA').val()+"&param2="+$('#txtRfqdtIssNOANo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRfqexpdtSign").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err11").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err11").innerHTML = "";
                        if(setRfqexpdtCompContractDate()){
                            return true;
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
            //alert('e6 : '+e);
        }
    }
    function setRfqexpdtCompContractDate(){
        try{
            if(document.getElementById("txtRfqexpdtSign").value!="" && document.getElementById("txtRfqexpdtSignNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRfqexpdtSign').val()+"&param2="+$('#txtRfqexpdtSignNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRfqexpdtCompContract").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err12").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err12").innerHTML = "";
                        return true;
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
           // alert('e7 : '+e);
        }
    }
    
        function settxtRfqexpdtSignNo(){
        try{
            if(document.getElementById("txtRfqexpdtSign").value!="" && document.getElementById("txtRfqexpdtCompContract").value.indexOf("/")>=0 ){
              // alert ('Works');
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRfqexpdtSign').val()+"&param2="+$('#txtRfqexpdtCompContract').val()+"&funName=getCalcDays",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                       // alert(j);  
                        if(j<=0){
                           document.getElementById("err12").innerHTML = "Expected Date of Completion of Contract must be greater than Expected Date of Signing of Contract";                      
                           document.getElementById("txtRfqexpdtSignNo").value = '';
                        }
                        else{
                         document.getElementById("txtRfqexpdtSignNo").value = j;
                         document.getElementById("err12").innerHTML = "";
                         return true;
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
           // alert('e7 : '+e);
        }
    }
    
    function settxtRFPexpdtsigncontractNo(){  
        try{
            if(document.getElementById("txtRFPexpdtsigncontract").value!="" && document.getElementById("txtRFPexpdtcomplcontract").value.indexOf("/")>=0 ){
              // alert ('Works');
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFPexpdtsigncontract').val()+"&param2="+$('#txtRFPexpdtcomplcontract').val()+"&funName=getCalcDays",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                       // alert(j);  
                        if(j<=0){
                           document.getElementById("txtRFPexpdtsigncontractNo").value = '';
                           document.getElementById("err36").innerHTML = "Expected Date of Completion of Contract must be greater than Expected Date of Signing of Contract";                      
                        }
                        else{
                         document.getElementById("txtRFPexpdtsigncontractNo").value = j;
                         document.getElementById("err36").innerHTML = "";
                         return true;
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){
           // alert('e7 : '+e);
        }
    } 
    //////////////////////////////////  TSTM Stage 1 Dates - start  //////////////
    function setTSTMexpdtSubDate(){
        try{
            if(document.getElementById("txtTSTMexpdtadvtIFT").value!="" && document.getElementById("txtTSTMexpdtadvtIFTNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTMexpdtadvtIFT').val()+"&param2="+$('#txtTSTMexpdtadvtIFTNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTMexpdtSub").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err13").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err13").innerHTML = "";
                        if(setTSTMexpdtopenDate()){
                            if(setTSTMexpdtsubEvaRptDate()){
                                if(setTSTMexpdtappEvaRptDate()){
                                    return true;
                                }
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    
    function setTSTMexpdtopenDate(){
        try{
            if(document.getElementById("txtTSTMexpdtSub").value!="" && document.getElementById("txtTSTMexpdtSubNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTMexpdtSub').val()+"&param2="+$('#txtTSTMexpdtSubNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTMexpdtopen").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err14").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err14").innerHTML = "";

                        if(setTSTMexpdtsubEvaRptDate()){
                            if(setTSTMexpdtappEvaRptDate()){
                                return true;
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setTSTMexpdtsubEvaRptDate(){
        try{
            if(document.getElementById("txtTSTMexpdtopen").value!="" && document.getElementById("txtTSTMexpdtopenNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTMexpdtopen').val()+"&param2="+$('#txtTSTMexpdtopenNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTMexpdtsubEvaRpt").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err15").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err15").innerHTML = "";

                        if(setTSTMexpdtappEvaRptDate()){
                            return true;
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setTSTMexpdtappEvaRptDate(){
        try{
            if(document.getElementById("txtTSTMexpdtsubEvaRpt").value!="" && document.getElementById("txtTSTMexpdtsubEvaRptNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTMexpdtsubEvaRpt').val()+"&param2="+$('#txtTSTMexpdtsubEvaRptNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTMexpdtappEvaRpt").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err16").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err16").innerHTML = "";
                        return true;
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }

    //////////////////////////////////  TSTM Stage 2 Dates - start  //////////////
    function setTSTM2expdtSubDate(){
        try{
            if(document.getElementById("txtTSTM2expdtIssuefinalDoc").value!="" && document.getElementById("txtTSTM2expdtIssuefinalDocNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTM2expdtIssuefinalDoc').val()+"&param2="+$('#txtTSTM2expdtIssuefinalDocNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTM2expdtSub").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err17").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err17").innerHTML = "";
                        if(setTSTM2expdtOpenDate()){
                            if(setTSTM2expdtsubevaRptDate()){
                                if(setTSTM2expdtAppEvaRptDate()){
                                     //code by proshanto
                              if(setTSTM2expdtLetterOfIntentDate()){
                                    if(setTSTM2expdtIssueNOADate()){
                                        if(setTSTM2expdtSignContractDate()){
                                            if(setTSTM2expdtcomplcontractDate()){
                                                return true;
                                            }
                                        }
                                    }
                                }
                                }
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }

    function setTSTM2expdtOpenDate(){
        try{
            if(document.getElementById("txtTSTM2expdtSub").value!="" && document.getElementById("txtTSTM2expdtSubNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTM2expdtSub').val()+"&param2="+$('#txtTSTM2expdtSubNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTM2expdtOpen").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err18").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err18").innerHTML = "";

                        if(setTSTM2expdtsubevaRptDate()){
                            if(setTSTM2expdtAppEvaRptDate()){
                                 //code by proshanto
                          if(setTSTM2expdtLetterOfIntentDate()){
                                if(setTSTM2expdtIssueNOADate()){
                                    if(setTSTM2expdtSignContractDate()){
                                        if(setTSTM2expdtcomplcontractDate()){
                                            return true;
                                        }
                                    }
                                }
                            }
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setTSTM2expdtsubevaRptDate(){
        try{
            if(document.getElementById("txtTSTM2expdtOpen").value!="" && document.getElementById("txtTSTM2expdtOpenNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTM2expdtOpen').val()+"&param2="+$('#txtTSTM2expdtOpenNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTM2expdtsubevaRpt").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err19").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err19").innerHTML = "";

                        if(setTSTM2expdtAppEvaRptDate()){
                             //code by proshanto
                          if(setTSTM2expdtLetterOfIntentDate()){
                            if(setTSTM2expdtIssueNOADate()){
                                if(setTSTM2expdtSignContractDate()){
                                    if(setTSTM2expdtcomplcontractDate()){
                                        return true;
                                    }
                                }
                            }
                        }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setTSTM2expdtAppEvaRptDate(){
        try{
            if(document.getElementById("txtTSTM2expdtsubevaRpt").value!="" && document.getElementById("txtTSTM2expdtsubevaRptNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTM2expdtsubevaRpt').val()+"&param2="+$('#txtTSTM2expdtsubevaRptNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTM2expdtAppEvaRpt").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err20").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err20").innerHTML = "";
                        //code by proshanto
                          if(setTSTM2expdtLetterOfIntentDate()){
                        if(setTSTM2expdtIssueNOADate()){
                            if(setTSTM2expdtSignContractDate()){
                                if(setTSTM2expdtcomplcontractDate()){
                                    return true;
                                }
                            }
                        }
                    }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    //Code by proshanto
    function setTSTM2expdtLetterOfIntentDate(){
        try{
            if(document.getElementById("txtTSTM2expdtAppEvaRpt ").value!="" && document.getElementById("txtTSTM2expdtAppEvaRptNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTM2expdtAppEvaRpt').val()+"&param2="+$('#txtTSTM2expdtAppEvaRptNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTM2expdtLetterOfIntent").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err201").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err201").innerHTML = "";

                        if(setTSTM2expdtIssueNOADate()){
                            if(setTSTM2expdtSignContractDate()){
                                if(setTSTM2expdtcomplcontractDate()){
                                    return true;
                                }
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    //end

    //id change by proshanto
    function setTSTM2expdtIssueNOADate(){
        try{
            if(document.getElementById("txtTSTM2expdtLetterOfIntent").value!="" && document.getElementById("txtTSTM2expdtLetterOfIntentNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTM2expdtLetterOfIntent').val()+"&param2="+$('#txtTSTM2expdtLetterOfIntentNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTM2expdtIssueNOA").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err21").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err21").innerHTML = "";

                        if(setTSTM2expdtSignContractDate()){
                            if(setTSTM2expdtcomplcontractDate()){
                                return true;
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setTSTM2expdtSignContractDate(){
        try{
            if(document.getElementById("txtTSTM2expdtIssueNOA").value!="" && document.getElementById("txtTSTM2expdtIssueNOANo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTM2expdtIssueNOA').val()+"&param2="+$('#txtTSTM2expdtIssueNOANo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTM2expdtSignContract").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err22").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err22").innerHTML = "";

                        if(setTSTM2expdtcomplcontractDate()){
                            return true;
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setTSTM2expdtcomplcontractDate(){
        try{
            if(document.getElementById("txtTSTM2expdtSignContract").value!="" && document.getElementById("txtTSTM2expdtSignContractNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtTSTM2expdtSignContract').val()+"&param2="+$('#txtTSTM2expdtSignContractNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtTSTM2expdtcomplcontract").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err23").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err23").innerHTML = "";
                        return true;
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    //////////////////////////////////  REOI Dates - start  //////////////
    function setREOQexpdtlstdtRcptEOIDate(){
        try{
            if(document.getElementById("txtREOQexpdtadvtREOI").value!="" && document.getElementById("txtREOQexpdtadvtREOINo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtREOQexpdtadvtREOI').val()+"&param2="+$('#txtREOQexpdtadvtREOINo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtREOQexpdtlstdtRcptEOI").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err24").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err24").innerHTML = "";
                            if(setREOQexpdtsubsrtlstFrmDate()){
                                if(setREOQextdtAppsrtlstFrmDate()){
                                    return true;
                                }
                            }
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setREOQexpdtsubsrtlstFrmDate(){
        try{
            if(document.getElementById("txtREOQexpdtlstdtRcptEOI").value!="" && document.getElementById("txtREOQexpdtlstdtRcptEOINo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtREOQexpdtlstdtRcptEOI').val()+"&param2="+$('#txtREOQexpdtlstdtRcptEOINo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtREOQexpdtsubsrtlstFrm").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err25").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err25").innerHTML = "";

                            if(setREOQextdtAppsrtlstFrmDate()){
                                return true;
                            }
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setREOQextdtAppsrtlstFrmDate(){
        try{
            if(document.getElementById("txtREOQexpdtsubsrtlstFrm").value!="" && document.getElementById("txtREOQexpdtsubsrtlstFrmNo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtREOQexpdtsubsrtlstFrm').val()+"&param2="+$('#txtREOQexpdtsubsrtlstFrmNo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtREOQextdtAppsrtlstFrm").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err26").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err26").innerHTML = "";
                            return true;
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }

    //////////////////////////////////  RFP Dates - start  //////////////
    function setRFPexpdtSubProposalDate(){
        try{
            if(document.getElementById("txtRFPexpdtissueRFP").value!="" && document.getElementById("txtRFPexpdtissueRFPNo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFPexpdtissueRFP').val()+"&param2="+$('#txtRFPexpdtissueRFPNo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtRFPexpdtSubProposal").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err27").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err27").innerHTML = "";
                            if(setRFPexpdttechOpenDate()){
                               if(setRFPexpdttechEvaDate()){
                                    if(setRFPextdtFinOpenDate()){
                                        if(setRFPexpdtsubCOmEvaRptDate()){
                                            if(setRFPexpdtappcomEvaRptDate()){
                                               if(setRFPexpdtcompNegoDate()){
                                                    if(setRFPexpdtappawdContractDate()){
                                                        if(setRFPexpdtsigncontractDate()){
                                                            if(setRFPexpdtcomplcontractDate()){
                                                                return true;
                                                            }
                                                        }
                                                    }
                                               }
                                            }
                                        }
                                    }
                               }
                            }
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFPexpdttechOpenDate(){
        try{
            if(document.getElementById("txtRFPexpdtSubProposal").value!="" && document.getElementById("txtRFPexpdtSubProposalNo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFPexpdtSubProposal').val()+"&param2="+$('#txtRFPexpdtSubProposalNo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtRFPexpdttechOpen").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err28").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err28").innerHTML = "";

                           if(setRFPexpdttechEvaDate()){
                                if(setRFPextdtFinOpenDate()){
                                    if(setRFPexpdtsubCOmEvaRptDate()){
                                        if(setRFPexpdtappcomEvaRptDate()){
                                           if(setRFPexpdtcompNegoDate()){
                                                if(setRFPexpdtappawdContractDate()){
                                                    if(setRFPexpdtsigncontractDate()){
                                                        if(setRFPexpdtcomplcontractDate()){
                                                            return true;
                                                        }
                                                    }
                                                }
                                           }
                                        }
                                    }
                                }
                           }
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFPexpdttechEvaDate(){
        try{
            if(document.getElementById("txtRFPexpdttechOpen").value!="" && document.getElementById("txtRFPexpdttechOpenNo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFPexpdttechOpen').val()+"&param2="+$('#txtRFPexpdttechOpenNo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtRFPexpdttechEva").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err29").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err29").innerHTML = "";
                            if(setRFPextdtFinOpenDate()){
                                if(setRFPexpdtsubCOmEvaRptDate()){
                                    if(setRFPexpdtappcomEvaRptDate()){
                                       if(setRFPexpdtcompNegoDate()){
                                            if(setRFPexpdtappawdContractDate()){
                                                if(setRFPexpdtsigncontractDate()){
                                                    if(setRFPexpdtcomplcontractDate()){
                                                        return true;
                                                    }
                                                }
                                            }
                                       }
                                    }
                                }
                            }
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFPextdtFinOpenDate(){
        try{
            if(document.getElementById("txtRFPexpdttechEva").value!="" && document.getElementById("txtRFPexpdttechEvaNo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFPexpdttechEva').val()+"&param2="+$('#txtRFPexpdttechEvaNo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtRFPextdtFinOpen").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err30").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err30").innerHTML = "";

                            if(setRFPexpdtsubCOmEvaRptDate()){
                                if(setRFPexpdtappcomEvaRptDate()){
                                   if(setRFPexpdtcompNegoDate()){
                                        if(setRFPexpdtappawdContractDate()){
                                            if(setRFPexpdtsigncontractDate()){
                                                if(setRFPexpdtcomplcontractDate()){
                                                    return true;
                                                }
                                            }
                                        }
                                   }
                                }
                            }
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFPexpdtsubCOmEvaRptDate(){
        try{
            if(document.getElementById("txtRFPextdtFinOpen").value!="" && document.getElementById("txtRFPextdtFinOpenNo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFPextdtFinOpen').val()+"&param2="+$('#txtRFPextdtFinOpenNo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtRFPexpdtsubCOmEvaRpt").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err31").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err31").innerHTML = "";

                            if(setRFPexpdtappcomEvaRptDate()){
                               if(setRFPexpdtcompNegoDate()){
                                    if(setRFPexpdtappawdContractDate()){
                                        if(setRFPexpdtsigncontractDate()){
                                            if(setRFPexpdtcomplcontractDate()){
                                                return true;
                                            }
                                        }
                                    }
                               }
                            }
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFPexpdtappcomEvaRptDate(){
        try{
            if(document.getElementById("txtRFPexpdtsubCOmEvaRpt").value!="" && document.getElementById("txtRFPexpdtsubCOmEvaRptNo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFPexpdtsubCOmEvaRpt').val()+"&param2="+$('#txtRFPexpdtsubCOmEvaRptNo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtRFPexpdtappcomEvaRpt").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err32").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err32").innerHTML = "";

                           if(setRFPexpdtcompNegoDate()){
                                if(setRFPexpdtappawdContractDate()){
                                    if(setRFPexpdtsigncontractDate()){
                                        if(setRFPexpdtcomplcontractDate()){
                                            return true;
                                        }
                                    }
                                }
                           }
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }

    function setRFPexpdtcompNegoDate(){
        try{
            if(document.getElementById("txtRFPexpdtappcomEvaRpt").value!="" && document.getElementById("txtRFPexpdtappcomEvaRptNo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFPexpdtappcomEvaRpt').val()+"&param2="+$('#txtRFPexpdtappcomEvaRptNo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtRFPexpdtcompNego").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err33").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err33").innerHTML = "";

                            if(setRFPexpdtappawdContractDate()){
                                if(setRFPexpdtsigncontractDate()){
                                    if(setRFPexpdtcomplcontractDate()){
                                        return true;
                                    }
                                }
                            }
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFPexpdtappawdContractDate(){
        try{
            if(document.getElementById("txtRFPexpdtcompNego").value!="" && document.getElementById("txtRFPexpdtcompNegoNo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFPexpdtcompNego').val()+"&param2="+$('#txtRFPexpdtcompNegoNo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtRFPexpdtappawdContract").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err34").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err34").innerHTML = "";

                            if(setRFPexpdtsigncontractDate()){
                                if(setRFPexpdtcomplcontractDate()){
                                    return true;
                                }
                            }
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFPexpdtsigncontractDate(){
        try{
            if(document.getElementById("txtRFPexpdtappawdContract").value!="" && document.getElementById("txtRFPexpdtappawdContractNo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFPexpdtappawdContract').val()+"&param2="+$('#txtRFPexpdtappawdContractNo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtRFPexpdtsigncontract").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err35").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err35").innerHTML = "";

                            if(setRFPexpdtcomplcontractDate()){
                                return true;
                            }
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFPexpdtcomplcontractDate(){
        try{
            if(document.getElementById("txtRFPexpdtsigncontract").value!="" && document.getElementById("txtRFPexpdtsigncontractNo").value!=""){
                $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFPexpdtsigncontract').val()+"&param2="+$('#txtRFPexpdtsigncontractNo').val()+"&funName=getCalcDate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("txtRFPexpdtcomplcontract").value = j.substr(eval(j.lastIndexOf("_")+1));
                            if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                                document.getElementById("err36").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                            else
                                document.getElementById("err36").innerHTML = "";
                            return true;
                        }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }

    //////////////////////////////////  RFA Dates - start  //////////////
    function setRFAdtrcptAppDate(){
        try{
            if(document.getElementById("txtRFAexpdtadvtRFA").value!="" && document.getElementById("txtRFAexpdtadvtRFANo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFAexpdtadvtRFA').val()+"&param2="+$('#txtRFAexpdtadvtRFANo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRFAdtrcptApp").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err37").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err37").innerHTML = "";
                        if(setRFAdteveappDate()){
                            if(setRFAdtintrvwselIndDate()){
                                if(setRFAdtevaFnlselLstDate()){
                                    if(setRFAdtsubevaRptDate()){
                                        if(setRFAdtappConsDate()){
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFAdteveappDate(){
        try{
            if(document.getElementById("txtRFAdtrcptApp").value!="" && document.getElementById("txtRFAdtrcptAppNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFAdtrcptApp').val()+"&param2="+$('#txtRFAdtrcptAppNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRFAdteveapp").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err38").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err38").innerHTML = "";
                        if(setRFAdtintrvwselIndDate()){
                            if(setRFAdtevaFnlselLstDate()){
                                if(setRFAdtsubevaRptDate()){
                                    if(setRFAdtappConsDate()){
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFAdtintrvwselIndDate(){
        try{
            if(document.getElementById("txtRFAdteveapp").value!="" && document.getElementById("txtRFAdteveappNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFAdteveapp').val()+"&param2="+$('#txtRFAdteveappNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRFAdtintrvwselInd").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err39").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err39").innerHTML = "";

                        if(setRFAdtevaFnlselLstDate()){
                            if(setRFAdtsubevaRptDate()){
                                if(setRFAdtappConsDate()){
                                    return true;
                                }
                            }
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFAdtevaFnlselLstDate(){
        try{
            if(document.getElementById("txtRFAdtintrvwselInd").value!="" && document.getElementById("txtRFAdtintrvwselIndNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFAdtintrvwselInd').val()+"&param2="+$('#txtRFAdtintrvwselIndNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRFAdtevaFnlselLst").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err40").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err40").innerHTML = "";
                        if(setRFAdtsubevaRptDate()){
                            if(setRFAdtappConsDate()){
                                return true;
                            }
                        }
                    }
                });
                 return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFAdtsubevaRptDate(){
        try{
            if(document.getElementById("txtRFAdtevaFnlselLst").value!="" && document.getElementById("txtRFAdtevaFnlselLstNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFAdtevaFnlselLst').val()+"&param2="+$('#txtRFAdtevaFnlselLstNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRFAdtsubevaRpt").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err41").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err41").innerHTML = "";
                        if(setRFAdtappConsDate()){
                            return true;
                        }
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function setRFAdtappConsDate(){
        try{
            if(document.getElementById("txtRFAdtsubevaRpt").value!="" && document.getElementById("txtRFAdtsubevaRptNo").value!=""){
                $.ajax({
                    url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#txtRFAdtsubevaRpt').val()+"&param2="+$('#txtRFAdtsubevaRptNo').val()+"&funName=getCalcDate",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        document.getElementById("txtRFAdtappCons").value = j.substr(eval(j.lastIndexOf("_")+1));
                        if("Working Day"!=j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1)))))
                            document.getElementById("err42").innerHTML = j.substr(eval(j.indexOf("_")+1),(eval(j.lastIndexOf("_")-eval(j.indexOf("_")+1))));
                        else
                            document.getElementById("err42").innerHTML = "";
                        return true;
                    }
                });
                return false;
            }else{
                return false;
            }
        }catch(e){

        }
    }
    function chkNumeric(obj,msgId){
        var strValidChars = "0123456789";
        var strChar;
        var blnResult = true;
        var strString = obj.value;

        if (strString.length == 0)
            return false;

        for (i = 0; i < strString.length && blnResult == true; i++)
        {
            strChar = strString.charAt(i);
            if (strValidChars.indexOf(strChar) == -1)
            {
                blnResult = false;
                document.getElementById(msgId).innerHTML = "<br/>Please enter numerics only";
            }
            else{
                blnResult = true;
                document.getElementById(msgId).innerHTML = "";
            }
        }
        /*if(blnResult){
            if(parseInt(strString)!=0){
                blnResult = true;
                document.getElementById(msgId).innerHTML = "";
            }else{
                blnResult = false;
                document.getElementById(msgId).innerHTML = "<br/>Only zero not allowed";
            }
        }*/
        return blnResult;
    }

    function CompareToForGreaterWithPqdtapplst(firstDate, secondDate){
        if(!CompareToForGreater(firstDate,secondDate)){
                document.getElementById("e5").innerHTML = "<br/>Expected Date of Advertisement of IFB on e-GP website must be greater than the Expected Date of Approval of List";
            return false;
        }
        else{
            document.getElementById("e5").innerHTML = "";
            return true;
        }
    }

    function CompareToForGreaterWithTSTMexpdtappEvaRpt(firstDate, secondDate){
        try{
            //if(Date.parse(firstDate.value) >= Date.parse(secondDate.value)){
        if(!CompareToForGreater(firstDate,secondDate)){
                document.getElementById("t6").innerHTML = "<br/>Expected date of TSTM must be greater than the Approval date";
            return false;
        }
        else{
            document.getElementById("t6").innerHTML = "";

            return true;
        }
        }catch(e){
            //alert(e);
    }
    }

    function CompareToForGreaterWithRFPexpdtissueRFP(firstDate, secondDate){

        if(!CompareToForGreater(firstDate,secondDate)){
            document.getElementById("rp1").innerHTML = "<br/>Expected date of RFP must be greater than the Approval date";
            return false;
        }
        else{
            document.getElementById("rp1").innerHTML = "";
            return true;
        }
    }

    //Function for CompareToForGreater
    function CompareToForGreater(textbox1,textbox2)
    {
	var StartDate = textbox1.value;
        var splitvalue = StartDate.split("-");
	var EndDate = textbox2.value;

        var newdateformat = splitvalue[1]+" "+splitvalue[0]+", "+splitvalue[2];
        var myDate = new Date(newdateformat);

        var date= myDate.getDate();
        var month = parseInt(myDate.getMonth())+1;
        var year = myDate.getFullYear();

        StartDate = date+"/"+month+"/"+year;
	var StrStartArray=StartDate.split("/");
	var StrEndArray=EndDate.split("/");


	if(eval(StrStartArray[2])<eval(StrEndArray[2]))
        {
            return true;
        }
        else if(eval(StrStartArray[2])>eval(StrEndArray[2]))
	{
            return false;
	}
	else if(eval(StrStartArray[2])==eval(StrEndArray[2]))
	{
            if(eval(StrStartArray[1])<eval(StrEndArray[1]))
            {
                return true;
            }
            else if(eval(StrStartArray[1])>eval(StrEndArray[1]))
            {
                return false;
            }
            else if(eval(StrStartArray[1])==eval(StrEndArray[1]))
            {
                if(eval(StrStartArray[0])<eval(StrEndArray[0])){
                    return true;
                }
                else if(eval(StrStartArray[0])>eval(StrEndArray[0]))
                {
                    return false;
                }
                else if(eval(StrStartArray[0])==eval(StrEndArray[0]))
                {
                    return false;
                }
            }
	}
	return true;
    }
    function calculateTotalNoofDays(){
        var txtboxlist = document.getElementsByTagName('input');
        var sum = 0;
        for(var i=0;i<txtboxlist.length;i++)
        {
           var inputname =  txtboxlist[i].name;
           if(inputname!="TSTM2expdtSignContractNo" && inputname!="RfqexpdtSignNo" && inputname!="RFPexpdtsigncontractNo")
           {
               var lasttwochar = inputname.substring(inputname.length-2);
               if(lasttwochar=="No"){
                    if(txtboxlist[i].value!="")
                        sum = sum + parseInt(txtboxlist[i].value);
                    else
                        continue;
               }
           }
        }
        if(sum!="0"){
        document.getElementById("idtxttotal").value = sum;
        }else{
            document.getElementById("idtxttotal").value = "";
        }
    }

    function recommendDay(spanId, hdnCntrolVal, txtVal)
    {
        if (eval(document.getElementById(hdnCntrolVal).value) < eval(document.getElementById(txtVal).value))
        {
            document.getElementById(spanId).innerHTML="<br/>Recommended days are maximum " + document.getElementById(hdnCntrolVal).value + " days";
        }
    }

function GetCalWithouTime(txtname,controlname)
                {
                    new Calendar({
                        inputField: txtname,
                        trigger: controlname,
                        showTime: false,
                        dateFormat:"%d/%m/%Y",
                        onSelect: function() {
                            var date = Calendar.intToDate(this.selection.get());
                            LEFT_CAL.args.min = date;
                            LEFT_CAL.redraw();
                            this.hide();
                            document.getElementById(txtname).focus();
                        }
                    });

                    var LEFT_CAL = Calendar.setup({
                        weekNumbers: false
                    })
                }
        </script>
    </head>
    <body onload="calculateTotalNoofDays();">

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
             <div class="contentArea_1">
            <div class="pageHead_1">
                Package Dates
                <span style="float:right;"><a href="APPDashboard.jsp?appID=<%=appId%>" class="action-button-goback"> Go Back To Dashboard</a></span>
            </div>
            <%if("success".equalsIgnoreCase(request.getParameter("msg"))) {%>
            <div>&nbsp;</div>
            <div class="responseMsg successMsg">Package details updated successfully</div>
            <%}%>
            <div class="stepWiz_1 t_space">
                <ul>
                    <li>APP</li>
                    <li>&gt;&gt;&nbsp;&nbsp; <% long chkpkgs = 0;
                        AppAdvSearchService appAdvSearchService = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
                        appAdvSearchService.setLogUserId(logUserId);
                        chkpkgs = appAdvSearchService.countForQuery("TblAppPackages", "appid="+appId+"and packageId="+pkgId);
                            if(chkpkgs != 0){
                                if(isRevision){
                               %>
                                <a style="text-decoration: underline;" style="" href="AddPackageDetail.jsp?appId=<%=appId%>&pkgId=<%=pkgId%>&action=Revise"  >Revise Package Details</a>
                               <% } else { %>
                             <a style="text-decoration: underline;" style="" href="AddPackageDetail.jsp?appId=<%=appId%>&pkgId=<%=pkgId%>&action=Edit"  >Edit Package Details</a>
                          <% } }else{ %>
                          Add Package Detail
                          <% } %></li>
                   <% if(isRevision){
                               %>
                    <li class="sMenu">&gt;&gt;&nbsp;&nbsp;Revise Package Dates</li>
                    <% } else { %>
                    <li class="sMenu">&gt;&gt;&nbsp;&nbsp;Edit Package Dates</li>
                    <% } %>
                </ul>
            </div>
            <%if("updateSuccess".equalsIgnoreCase(request.getParameter("msg"))) {%>
            <br/>
            <div class="responseMsg successMsg">Package details updated successfully</div>
            <%}%>
            <div>&nbsp;</div>
            <%
                    if(appId != 0 && pkgId != 0 && appViewPkgDtBean.isDataExists()) {
                               int prjId = 0;
                               if(appViewPkgDtBean.getProjectId()!=null && appViewPkgDtBean.getProjectId()!=0)
                                    prjId = appViewPkgDtBean.getProjectId();
            %>
            <form id="frmAddPackageDate" action="getdate.jsp" method="POST">
                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                    <tr>
                        <td style="font-style: italic" class="ff t-align-left" colspan="4" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory
                            <input type="hidden" value="${appViewPkgDtBean.pqDtId}" name="pqDtId"/>
                            <input type="hidden" name="hdnAppId" id="hdnAppId" value="${appViewPkgDtBean.appId}">
                            <input type="hidden" name="hdnPkgId" id="hdnPkgId" value="${appViewPkgDtBean.packageId}">
                        </td>
                    </tr>
                     <%
                        tenderCS.setLogUserId(logUserId);
                        List<SPTenderCommonData> appAuthList = tenderCS.returndata("GetAppAuth",
                                                                                    String.valueOf(pkgId),
                                                                                    "0");
                    %>
                    <tr>
                        <td width="157" class="ff">Approving Authority :</td>
                        <td width="474">
                            <!--%=appAuthList.get(0).getFieldName2() %-->
                            <!--Check condition to show HOPA instead of HOPE and PA instead of PE by Proshanto Kumar Saha-->
                            <%-- Edited By Emtaz on 16/April/2016. Back to PE --%>
                            <%if(appAuthList.get(0).getFieldName2().equalsIgnoreCase("Hope")){out.print("HOPA");}
                                    else if(appAuthList.get(0).getFieldName2().equalsIgnoreCase("PE")){
                                              out.print("PA");
                                                  }
                                       else{out.print(appAuthList.get(0).getFieldName2());}%>
                            <input type="hidden" name="hdnAuthId" id="hdnAuthId" value="<%=appAuthList.get(0).getFieldName1()%>">
                        </td>
                        <td width="121">&nbsp;</td>
                        <td width="177">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="157" class="ff">APP ID :</td>
                        <td width="474">${appViewPkgDtBean.appId}</td>
                        <td width="121">&nbsp;</td>
                        <td width="177">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="ff">Financial Year :</td>
                        <td>${appViewPkgDtBean.financialYear}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="ff">Budget Type :</td>
                        <td>${appViewPkgDtBean.budgetType}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                     <%if (prjId != 0) {%>
                    <tr>
                        <td class="ff">Project Name :</td>
                        <td>${appViewPkgDtBean.projectName}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <%}%>
                    <tr>
                        <td class="ff">Letter Ref. No. :</td>
                        <td>${appViewPkgDtBean.appCode}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="ff">Package No. :</td>
                        <td>${appViewPkgDtBean.packageNo}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <input type="hidden" name="hdnPQReq" id="hdnPQReq" value="<%=appViewPkgDtBean.getIsPQRequired()%>"/>
                    <input type="hidden" name="hdnREO" id="hdnREO" value="<%=appViewPkgDtBean.getReoiRfaRequired()%>"/>
                    <input type="hidden" name="hdnPMId" id="hdnPMId" value="<%=appViewPkgDtBean.getProcurementMethodId()%>"/>
                    <input type="hidden" name="hdnPMName" id="hdnPMName" value="<%=appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())%>"/>
                    <input type="hidden" name="hdnDateType" id="hdnDateType" value=""/>
                </table>
                <%
                    List<SPTenderCommonData> plannedDayList = tenderCS.returndata("GetAppPlannedDays",
                                                                                  appAuthList.get(0).getFieldName1(),"0");
                %>
                <div id="PQ" style="display:block;" >
                    <%if ("Yes".equalsIgnoreCase(appViewPkgDtBean.getIsPQRequired())) {%>
                    <script>
                        document.getElementById("hdnDateType").value = "PQ";
                    </script>
                    <div class="tableHead_1 t_space">PQ Tender Dates :</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>
                            <td width="450" class="ff" colspan="2">Expected Date of Advertisement of Invitation on e-GP website :  <span>*</span></td>
                            <td width="100" colspan="2"><input name="Pqdtadvtinvt" value="${appViewPkgDtBean.advtDt}" type="text" class="formTxtBox_1" id="txtpqdtadvtinvt" style="width:100px;" readonly="readonly" onfocus="GetCalWithCond('txtpqdtadvtinvt','txtpqdtadvtinvt','error');" onblur="setPqdtappsubDate();cleartxtpqdtadvtinvt()"/>
                                <a  href="javascript:void(0);" title="Calender"><img id="txtpqdtadvtinvtimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCalWithCond('txtpqdtadvtinvt','txtpqdtadvtinvtimg','error');"/></a>
                                <span id="error" style="color: red;"></span>
                            </td>

                        </tr>
                        <tr>
                            <td width="150" class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td width="50"><input name="PqdtadvtinvtNo" type="text" value="${appViewPkgDtBean.advtDays}" class="formTxtBox_1" id="txtpqdtadvtinvtNo" style="width:100px;" onblur="if(chkNumeric(this,'errorNo')){setPqdtappsubDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();" />
                                <span id="errorNo" style="color: red;"></span>
                            </td>
                            <td class="ff" width="400">Expected Date of Applications Submission  :</td>
                            <td width="300"><input name="Pqdtappsub" type="text" value="${appViewPkgDtBean.subDt}" class="formTxtBox_1" id="txtpqdtappsub" style="width:100px;" readonly="readonly"  onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="e2" style="color: red;"></span>
                                <span id="err2" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="PqdtappsubNo" type="text" value="${appViewPkgDtBean.subDays}" class="formTxtBox_1" id="txtPqdtappsubNo" style="width:100px;" onblur="if(chkNumeric(this,'e2No')){setPqdtsubevarptDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays()" />
                                <span id="e2No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date Of Submission Of Evaluation Report With Recommended List : </td>
                            <td><input name="Pqdtsubevarpt" type="text" value="${appViewPkgDtBean.evalRptDt}" class="formTxtBox_1" id="txtPqdtsubevarpt" style="width:100px;" readonly="readonly"  onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="e3" style="color: red;"></span><span id="err3" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="PqdtsubevarptNo" type="text" value="${appViewPkgDtBean.evalRptDays}" class="formTxtBox_1" id="txtPqdtsubevarptNo" style="width:100px;" onblur="if(chkNumeric(this,'e3No')){setPqdtapplstDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays()" />
                                <span id="e3No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Approval Of List :  </td>
                            <td><input name="Pqdtapplst" value="${appViewPkgDtBean.appLstDt}" type="text" class="formTxtBox_1" id="txtPqdtapplst" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()" />
                                <span id="e4" style="color: red;"></span><span id="err4" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                    </table>
                </div>TSTM 2nd Stage:
                 <%}%>
                 <%if((!"".equalsIgnoreCase(appViewPkgDtBean.getIsPQRequired()) && !"TSTM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) ) || (("".equalsIgnoreCase(appViewPkgDtBean.getIsPQRequired())) &&  ( ("OTM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))) || ("LTM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) && "".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())) ||  ("DPM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))) || ("OSTETM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))) ||  ("RFQ".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) || "RFQL".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) || "RFQU".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())))))) {%>
                <div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>
                            <div class="tableHead_1 t_space">
                                <%if("RFQU".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) || "RFQL".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId())) || "RFQ".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))){out.print(" RFQ ");}else{out.print(" Tender ");}%>Dates :</div>
                        </tr>
                        <tr>
                            <td width="450" class="ff" colspan="2">Expected Date of Advertisement of IFB on e-GP website : <span>*</span></td>
                            <td width="100" colspan="2">
                                <input name="Rfqdtadvtift" value="${appViewPkgDtBean.tenderAdvertDt}" type="text" class="formTxtBox_1" id="txtRfqdtadvtift" style="width:100px;" readonly="readonly"
                                                   onfocus="GetCalWithCond('txtRfqdtadvtift','txtRfqdtadvtift','e5');" onblur="if(CompareToForGreaterWithPqdtapplst(document.getElementById('txtPqdtapplst'),this)){setRfqdtsubDate();cleartxtpqdtadvtinvt();}"/>
                                 <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtRfqdtadvtiftimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCalWithCond('txtRfqdtadvtift','txtRfqdtadvtiftimg','e5');" /></a>
                                <span id="e5" style="color: red;"></span>

                        </tr>
                        <tr>
                            <td width="150" class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td width="50"><input name="RfqdtadvtiftNo" type="text" value="${appViewPkgDtBean.tenderAdvertDays}" class="formTxtBox_1" id="txtRfqdtadvtiftNo" style="width:100px;"  onblur="if(chkNumeric(this,'e5No')){setRfqdtsubDate();cleartxtpqdtadvtinvt();}; calculateTotalNoofDays();"/>
                                <span id="e5No" style="color: red;"></span>
                            </td>
                            <td class="ff" width="400">Expected Date of Submission of Tenders :  </td>
                            <td width="300"><input name="Rfqdtsub" type="text" value="${appViewPkgDtBean.tenderSubDt}" class="formTxtBox_1" id="txtRfqdtsub" style="width:100px;" readonly="readonly"  onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="e6" style="color: red;"></span><span id="err6" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RfqdtsubNo" type="text" value="${appViewPkgDtBean.tenderSubDays}" class="formTxtBox_1" id="txtRfqdtsubNo" style="width:100px;"  onblur="if(chkNumeric(this,'e6No')){setRfqexpdtopenDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="e6No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Opening of Tenders :  </td>
                            <td><input name="Rfqexpdtopen" type="text" value="${appViewPkgDtBean.tenderOpenDt}" class="formTxtBox_1" id="txtRfqexpdtopen" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()" />
                                <span id="e7" style="color: red;"></span><span id="err7" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="RfqexpdtopenNo" type="text" value="${appViewPkgDtBean.tenderOpenDays}" class="formTxtBox_1" id="txtRfqexpdtopenNo" style="width:100px;"  onblur="if(chkNumeric(this,'e7No')){setRfqdtsubevaRptDate();cleartxtpqdtadvtinvt(); recommendDay('e7No', 'hdnRfqexpdtopen', 'txtRfqexpdtopenNo');} calculateTotalNoofDays();" />
                                <input type="hidden" name="hdnRfqexpdtopen" id="hdnRfqexpdtopen" value="<% if(plannedDayList.size() > 0){ plannedDayList.get(0).getFieldName1(); } %>">
                                <span id="e7No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Submission of Evaluation Report :  </td>
                            <td><input name="RfqdtsubevaRpt" type="text" value="${appViewPkgDtBean.tenderEvalRptDt}" class="formTxtBox_1" id="txtRfqdtsubevaRpt" style="width:100px;" readonly="readonly"/>
                                <span id="e8" style="color: red;"></span>
                                <span id="err8" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="RfqdtsubevaRptNo" type="text" value="${appViewPkgDtBean.tenderEvalRptdays}" class="formTxtBox_1" id="txtRfqdtsubevaRptNo" style="width:100px;"
                                onblur="if(chkNumeric(this,'e8No')){setRfqexpdtAppawdDate();cleartxtpqdtadvtinvt(); recommendDay('e8No', 'hdnRfqdtsubevaRpt', 'txtRfqdtsubevaRptNo');} calculateTotalNoofDays();" />
                                <input type="hidden" name="hdnRfqdtsubevaRpt" id="hdnRfqdtsubevaRpt" value="<% if(plannedDayList.size() > 0){ plannedDayList.get(0).getFieldName2(); } %>">
                                <span id="e8No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Approval for Award of Contract : </td>
                            <td><input name="RfqexpdtAppawd" type="text" value="${appViewPkgDtBean.tenderContractAppDt}" class="formTxtBox_1" id="txtRfqexpdtAppawd" style="width:100px;" readonly="readonly" />
                                <span id="e9" style="color: red;"></span><span id="err9" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <% if(appViewPkgDtBean.getProcurementMethodId()!=14) {%>
                        <tr>
                             <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="RfqexpdtAppawdNo" type="text" value="${appViewPkgDtBean.tenderContractAppDays}" class="formTxtBox_1" id="txtRfqexpdtAppawdNo" style="width:100px;"
                                onblur="if(chkNumeric(this,'e81No')){setRfqLtrIntAwdDate();cleartxtpqdtadvtinvt(); } calculateTotalNoofDays();" />
                                <span id="e81No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Letter of Intent to Award :  </td>
                            <td><input name="RfqexpdtLtrIntAwd" type="text" value="${appViewPkgDtBean.tenderLetterIntentDt}" class="formTxtBox_1" id="txtRfqexpdtLtrIntAwd" style="width:100px;" readonly="readonly" />
                                <span id="e91" style="color: red;"></span>
                                <span id="err91" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <% }else {%>
                            <input name="RfqexpdtAppawdNo" type="hidden" value="0" class="formTxtBox_1" id="txtRfqexpdtAppawdNo" style="width:100px;"
                            onblur="if(chkNumeric(this,'e81No')){setRfqLtrIntAwdDate();cleartxtpqdtadvtinvt(); } calculateTotalNoofDays();" />
                            <span id="e81No" style="color: red; display: none;"></span>
                            
                            <input name="RfqexpdtLtrIntAwd" type="hidden" value="${appViewPkgDtBean.tenderLetterIntentDt}" class="formTxtBox_1" id="txtRfqexpdtLtrIntAwd" style="width:100px;" readonly="readonly" />
                            <span id="e91" style="color: red; display: none;"></span>
                            <span id="err91" style="color: red; font-weight: bold; display: none; "></span>
                          
                        <% } %>
                       
                        <tr>
                             <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="RfqexpdtLtrIntAwdNo" type="text" value="${appViewPkgDtBean.tnderLetterIntentDays}" class="formTxtBox_1" id="txtRfqexpdtLtrIntAwdNo" style="width:100px;"
                                onblur="if(chkNumeric(this,'e9No')){setRfqdtIssNOADate();cleartxtpqdtadvtinvt(); recommendDay('e9No', 'hdnRfqexpdtLtrIntAwd', 'txtRfqexpdtLtrIntAwdNo');} calculateTotalNoofDays();" />
                                <input type="hidden" name="hdnRfqexpdtLtrIntAwd" id="hdnRfqexpdtLtrIntAwd" value="<% if(plannedDayList.size() > 0){ plannedDayList.get(0).getFieldName3(); } %>">
                                <span id="e9No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Issuance of the Letter of Acceptance (LOA) :  </td>
                            <td><input name="RfqdtIssNOA" type="text" value="${appViewPkgDtBean.tenderNoaIssueDt}" class="formTxtBox_1" id="txtRfqdtIssNOA" style="width:100px;" readonly="readonly" />
                                <span id="e10" style="color: red;"></span><span id="err10" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="RfqdtIssNOANo" type="text" value="${appViewPkgDtBean.tenderNoaIssueDays}" class="formTxtBox_1" id="txtRfqdtIssNOANo" style="width:100px;"
                                onblur="if(chkNumeric(this,'e10No')){setRfqexpdtSignDate();cleartxtpqdtadvtinvt(); } cleartxtpqdtadvtinvt(); calculateTotalNoofDays();"/>
                                <span id="e10No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Signing of Contract :  </td>
                            <td><input name="RfqexpdtSign" type="text" value="${appViewPkgDtBean.tenderContractSignDt}" class="formTxtBox_1" id="txtRfqexpdtSign" style="width:100px;" readonly="readonly"  onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="e11" style="color: red;"></span><span id="err11" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RfqexpdtSignNo" type="text" value="${appViewPkgDtBean.tenderContractSignDays}" class="formTxtBox_1" id="txtRfqexpdtSignNo" style="width:100px;"  onblur="if(chkNumeric(this,'e11No')){setRfqexpdtCompContractDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="e11No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Completion of Contract : </td>
                            <td><input name="RfqexpdtCompContract" type="text" value="${appViewPkgDtBean.tenderContractCompDt}" class="formTxtBox_1" id="txtRfqexpdtCompContract" style="width:100px;" readonly="readonly" onblur="cleartxtpqdtadvtinvt()" onfocus ="GetCalWithouTime('txtRfqexpdtCompContract','txtRfqexpdtCompContract'); settxtRfqexpdtSignNo();"/>
                                <a  href="javascript:void(0);" title="Calender"> <img id="txtRfqexpdtCompContractimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithouTime('txtRfqexpdtCompContract','txtRfqexpdtCompContractimg');"/> </a>
                                <span id="e12" style="color: red;"></span><span id="err12" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                    </table>
                </div>
                <%} else if ("TSTM".equalsIgnoreCase(appViewPkgDtBean.getProcMethodName(appViewPkgDtBean.getProcurementMethodId()))) {%>
                <script>
                    document.getElementById("hdnDateType").value = "TSTM";
                </script>
                <div id="TSTM">
                    <div class="tableHead_1 t_space">TSTM 1st Stage:</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>
                            <td width="450" class="ff" colspan="2">Expected Date of Advertisement of IFB on e-GP website : <span>*</span></td>
                            <td width="100" colspan="2"><input name="TSTMexpdtadvtIFT" type="text" value="${appViewPkgDtBean.advtDt}" class="formTxtBox_1" id="txtTSTMexpdtadvtIFT" style="width:100px;" readonly="readonly" onfocus="GetCalWithCond('txtTSTMexpdtadvtIFT','txtTSTMexpdtadvtIFT','t1');" onblur="setTSTMexpdtSubDate();cleartxtpqdtadvtinvt()"/>
                                <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtTSTMexpdtadvtIFTimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick ="GetCalWithCond('txtTSTMexpdtadvtIFT','txtTSTMexpdtadvtIFTimg','t1');" /></a>
                                <span id="t1"></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="150" class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td width="50"><input name="TSTMexpdtadvtIFTNo" type="text" value="${appViewPkgDtBean.advtDays}" class="formTxtBox_1" id="txtTSTMexpdtadvtIFTNo" style="width:100px;" onblur="if(chkNumeric(this,'t1No')){setTSTMexpdtSubDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();" />
                                <span id="t1No" style="color: red;"></span>
                            </td>
                            <td class="ff" width="400">Expected Date of Submission of Tenders : </td>
                            <td width="300"><input name="TSTMexpdtSub" type="text" value="${appViewPkgDtBean.subDt}" class="formTxtBox_1" id="txtTSTMexpdtSub" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="t2" style="color: red;"></span><span id="err13" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="TSTMexpdtSubNo" type="text" value="${appViewPkgDtBean.subDays}" class="formTxtBox_1" id="txtTSTMexpdtSubNo" style="width:100px;"  onblur="if(chkNumeric(this,'t2No')){setTSTMexpdtopenDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="t2No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Opening of Tenders :  </td>
                            <td><input name="TSTMexpdtopen" type="text" value="${appViewPkgDtBean.openDt}" class="formTxtBox_1" id="txtTSTMexpdtopen" style="width:100px;" readonly="readonly"  onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="t3" style="color: red;"></span><span id="err14" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="TSTMexpdtopenNo" type="text" value="${appViewPkgDtBean.openDays}" class="formTxtBox_1" id="txtTSTMexpdtopenNo" style="width:100px;" onblur="if(chkNumeric(this,'t3No')){setTSTMexpdtsubEvaRptDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();" />
                                <span id="t3No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Submission of Evaluation Report :  </td>
                            <td><input name="TSTMexpdtsubEvaRpt" type="text" value="${appViewPkgDtBean.evalRptDt}" class="formTxtBox_1" id="txtTSTMexpdtsubEvaRpt" style="width:100px;"  readonly="readonly" />
                                <span id="t4" style="color: red;"></span><span id="err15" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="TSTMexpdtsubEvaRptNo" type="text" value="${appViewPkgDtBean.evalRptDays}" class="formTxtBox_1" id="txtTSTMexpdtsubEvaRptNo" style="width:100px;"
                                onblur="if(chkNumeric(this,'t4No')){setTSTMexpdtappEvaRptDate();cleartxtpqdtadvtinvt(); recommendDay('t4No', 'hdnTSTMexpdtsubEvaRpt', 'txtTSTMexpdtsubEvaRptNo');} calculateTotalNoofDays();"  />
                                <input type="hidden" name="hdnTSTMexpdtsubEvaRpt" id="hdnTSTMexpdtsubEvaRpt" value="<% if(plannedDayList.size() > 0){ plannedDayList.get(0).getFieldName1(); } %>">
                                <span id="t4No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Approval of the Evaluation Report :  </td>
                            <td><input name="TSTMexpdtappEvaRpt" type="text" value="${appViewPkgDtBean.appLstDt}" class="formTxtBox_1" id="txtTSTMexpdtappEvaRpt" style="width:100px;"  readonly="readonly" onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="t5" style="color: red;"></span><span id="err16" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                    </table>
                    <div class="tableHead_1 t_space">TSTM 2nd Stage:</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>
                            <td width="450" class="ff" colspan="2">Expected Date of issue of Final Tender Document to qualified bidders :  <span class="mandatory">*</span></td>
                            <td width="100" colspan="2"><input name="TSTM2expdtIssuefinalDoc" type="text" value="${appViewPkgDtBean.tenderAdvertDt}" class="formTxtBox_1" id="txtTSTM2expdtIssuefinalDoc" style="width:100px;" readonly="readonly" onfocus="GetCal('txtTSTM2expdtIssuefinalDoc','txtTSTM2expdtIssuefinalDoc');"  onblur="if(CompareToForGreaterWithTSTMexpdtappEvaRpt(document.getElementById('txtTSTMexpdtappEvaRpt'),this)){setTSTM2expdtSubDate();cleartxtpqdtadvtinvt();}"/>
                                 <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtTSTM2expdtIssuefinalDocimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick ="GetCal('txtTSTM2expdtIssuefinalDoc','txtTSTM2expdtIssuefinalDocimg');" /></a>
                                <span id="t6" style="color: red;"></span>
                            </td>


                        </tr>
                        <tr>
                            <td width="150" class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td width="50"><input name="TSTM2expdtIssuefinalDocNo" type="text" value="${appViewPkgDtBean.tenderAdvertDays}" class="formTxtBox_1" id="txtTSTM2expdtIssuefinalDocNo" style="width:100px;" onblur="if(chkNumeric(this,'t6No')){setTSTM2expdtSubDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();" />
                                <span id="t6No" style="color: red;"></span>
                            </td>
                            <td class="ff" width="400">Expected Date of Submission of Tenders :  </td>
                            <td width="300"><input name="TSTM2expdtSub" type="text" value="${appViewPkgDtBean.tenderSubDt}" class="formTxtBox_1" id="txtTSTM2expdtSub" style="width:100px;" readonly="readonly"  onfocus="GetCal('txtTSTM2expdtSub','txtTSTM2expdtSub');" onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="t7" style="color: red;"></span><span id="err17" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="TSTM2expdtSubNo" type="text" value="${appViewPkgDtBean.tenderSubDays}" class="formTxtBox_1" id="txtTSTM2expdtSubNo" style="width:100px;" onblur="if(chkNumeric(this,'t7No')){setTSTM2expdtOpenDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();" />
                                <span id="t7No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Opening of Tenders :  </td>
                            <td><input name="TSTM2expdtOpen" type="text" value="${appViewPkgDtBean.tenderOpenDt}" class="formTxtBox_1" id="txtTSTM2expdtOpen" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="t8" style="color: red;"></span><span id="err18" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="TSTM2expdtOpenNo" type="text" value="${appViewPkgDtBean.tenderOpenDays}" class="formTxtBox_1" id="txtTSTM2expdtOpenNo" style="width:100px;"  onblur="if(chkNumeric(this,'t8No')){setTSTM2expdtsubevaRptDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();" />
                                <span id="t8No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Submission of Evaluation Report : </td>
                            <td><input name="TSTM2expdtsubevaRpt" type="text" value="${appViewPkgDtBean.tenderEvalRptDt}" class="formTxtBox_1" id="txtTSTM2expdtsubevaRpt" style="width:100px;" readonly="readonly" />
                                <span id="t9" style="color: red;"></span><span id="err19" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                             <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="TSTM2expdtsubevaRptNo" type="text" value="${appViewPkgDtBean.tenderEvalRptdays}" class="formTxtBox_1" id="txtTSTM2expdtsubevaRptNo" style="width:100px;"
                                 onblur="if(chkNumeric(this,'t9No')){setTSTM2expdtAppEvaRptDate();cleartxtpqdtadvtinvt(); recommendDay('t9No', 'hdnTSTM2expdtsubevaRpt', 'txtTSTM2expdtsubevaRptNo');} calculateTotalNoofDays();" />
                                <input type="hidden" name="hdnTSTM2expdtsubevaRpt" id="hdnTSTM2expdtsubevaRpt" value="<% if(plannedDayList.size() > 0){ plannedDayList.get(0).getFieldName1(); } %>">
                                <span id="t9No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Approval of the Evaluation Report :  </td>
                            <td><input name="TSTM2expdtAppEvaRpt" type="text" value="${appViewPkgDtBean.tenderEvalRptAppDt}" class="formTxtBox_1" id="txtTSTM2expdtAppEvaRpt" style="width:100px;" readonly="readonly" />
                                <span id="t10" style="color: red;"></span><span id="err20" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <% if(appViewPkgDtBean.getProcurementMethodId()!=14) {%>
                       <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="TSTM2expdtAppEvaRptNo" type="text" value="${appViewPkgDtBean.tenderEvalRptAppDays}" class="formTxtBox_1" id="txtTSTM2expdtAppEvaRptNo"
                                style="width:100px;" value="0" onblur="if(chkNumeric(this,'t91No')){setTSTM2expdtLetterOfIntentDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();" />
                                <span id="t91No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Letter of Intent to Award : </td>
                            <td><input name="TSTM2expdtLetterOfIntent" type="text" value="${appViewPkgDtBean.tenderLetterIntentDt}"  class="formTxtBox_1" id="txtTSTM2expdtLetterOfIntent" style="width:100px;" readonly="readonly" />
                                <span id="t101" style="color: red;"></span>
                                <span id="err201"  style="color: red;"></span>
                            </td>

                        </tr>
                        <% } else { %>
                       
                            <input name="TSTM2expdtAppEvaRptNo" type="hidden" value="0" class="formTxtBox_1" id="txtTSTM2expdtAppEvaRptNo"
                            style="width:100px;" onblur="if(chkNumeric(this,'t91No')){setTSTM2expdtLetterOfIntentDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();" />
                            <span id="t91No" style="color: red; display: none;"></span>
                            
                            <input name="TSTM2expdtLetterOfIntent" type="hidden" value="0"  class="formTxtBox_1" id="txtTSTM2expdtLetterOfIntent" style="width:100px;" readonly="readonly" />
                            <span id="t101" style="color: red; display: none;"></span>
                            <span id="err201"  style="color: red; display: none;"></span>
                         
                        <% } %>
                       
                        <tr>
                             <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="TSTM2expdtLetterOfIntentNo" type="text" value="${appViewPkgDtBean.tnderLetterIntentDays}" class="formTxtBox_1" id="txtTSTM2expdtLetterOfIntentNo" style="width:100px;"
                                 onblur="if(chkNumeric(this,'t10No')){setTSTM2expdtIssueNOADate();cleartxtpqdtadvtinvt(); recommendDay('t10No', 'hdnTSTM2expdtLetterOfIntentNo', 'txtTSTM2expdtLetterOfIntentNo');} calculateTotalNoofDays();"/>
                                <input type="hidden" name="hdnTSTM2expdtLetterOfIntentNo" id="hdnTSTM2expdtLetterOfIntentNo" value="<% if(plannedDayList.size() > 0){ plannedDayList.get(0).getFieldName2(); } %>">
                                <span id="t10No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Issue of Letter of Acceptance (LOA) :  </td>
                            <td><input name="TSTM2expdtIssueNOA" type="text" value="${appViewPkgDtBean.tenderNoaIssueDt}" class="formTxtBox_1" id="txtTSTM2expdtIssueNOA" style="width:100px;" readonly="readonly"  onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="t11" style="color: red;"></span><span id="err21" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        

                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="TSTM2expdtIssueNOANo" type="text" value="${appViewPkgDtBean.tenderNoaIssueDays}" class="formTxtBox_1" id="txtTSTM2expdtIssueNOANo" style="width:100px;"  onblur="if(chkNumeric(this,'t11No')){setTSTM2expdtSignContractDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="t11No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Signing of Contract :  </td>
                            <td><input name="TSTM2expdtSignContract" type="text" value="${appViewPkgDtBean.tenderContractSignDt}" class="formTxtBox_1" id="txtTSTM2expdtSignContract" style="width:100px;" readonly="readonly"  onblur="cleartxtpqdtadvtinvt()" />
                                <span id="t12" style="color: red;"></span><span id="err22" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="TSTM2expdtSignContractNo" type="text" value="${appViewPkgDtBean.tenderContractSignDays}" class="formTxtBox_1" id="txtTSTM2expdtSignContractNo" style="width:100px;"  onblur="if(chkNumeric(this,'t12No')){setTSTM2expdtcomplcontractDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="t12No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Completion of Contract :</td>
                            <td><input name="TSTM2expdtcomplcontract" type="text" value="${appViewPkgDtBean.tenderContractCompDt}" class="formTxtBox_1" id="txtTSTM2expdtcomplcontract" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="rp11" style="color: red;"></span>
                                <span id="err23" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                    </table>
                </div>
                <%} else if ("REOI".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())) {%>
                <script>
                    document.getElementById("hdnDateType").value = "REOI";
                </script>
                <div id="REOI" >
                    <div class="tableHead_1 t_space">REOI Dates :</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>
                            <td width="450" class="ff" colspan="2">Expected Date of Advertisement of REOI on e-GP website : <span>*</span></td>
                            <td width="100" colspan="2"><input name="REOQexpdtadvtREOI" type="text" value="${appViewPkgDtBean.advtDt}" class="formTxtBox_1" id="txtREOQexpdtadvtREOI" style="width:100px;" readonly="readonly"  onfocus="GetCalWithCond('txtREOQexpdtadvtREOI','txtREOQexpdtadvtREOI','re1');" onblur="setREOQexpdtlstdtRcptEOIDate();cleartxtpqdtadvtinvt()" />
                                 <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtREOQexpdtadvtREOIimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithCond('txtREOQexpdtadvtREOI','txtREOQexpdtadvtREOIimg','re1');" /></a>
                                <span id="re1" style="color: red;"></span>
                            </td>

                        </tr>
                        <tr>
                             <td width="150" class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td width="50"><input name="REOQexpdtadvtREOINo" type="text" value="${appViewPkgDtBean.advtDays}" class="formTxtBox_1" id="txtREOQexpdtadvtREOINo" style="width:100px;"  onblur="if(chkNumeric(this,'re1No')){setREOQexpdtlstdtRcptEOIDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="re1No" style="color: red;"></span>
                            </td>
                            <td class="ff" width="400">Expected Date of Last Date of Receipt of EOI :  </td>
                            <td width="300"><input name="REOQexpdtlstdtRcptEOI" type="text" value="${appViewPkgDtBean.reoiReceiptDt}" class="formTxtBox_1" id="txtREOQexpdtlstdtRcptEOI" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="re2" style="color: red;"></span><span id="err24" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="REOQexpdtlstdtRcptEOINo" type="text" value="${appViewPkgDtBean.reoiReceiptDays}" class="formTxtBox_1" id="txtREOQexpdtlstdtRcptEOINo" style="width:100px;" onblur="if(chkNumeric(this,'re2No')){setREOQexpdtsubsrtlstFrmDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();" />
                                <span id="re2No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Submission of Recommended Short-listed Firm :  </td>
                            <td><input name="REOQexpdtsubsrtlstFrm" type="text" value="${appViewPkgDtBean.subDt}" class="formTxtBox_1" id="txtREOQexpdtsubsrtlstFrm" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="re3" style="color: red;"></span><span id="err25" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="REOQexpdtsubsrtlstFrmNo" type="text" value="${appViewPkgDtBean.subDays}" class="formTxtBox_1" id="txtREOQexpdtsubsrtlstFrmNo" style="width:100px;" onblur="if(chkNumeric(this,'re3No')){setREOQextdtAppsrtlstFrmDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="re3No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Approval of Recommended Shortlisted Firm :  </td>
                            <td><input name="REOQextdtAppsrtlstFrm" type="text" value="${appViewPkgDtBean.appLstDt}" class="formTxtBox_1" id="txtREOQextdtAppsrtlstFrm" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="re4" style="color: red;"></span><span id="err26" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                    </table>
                </div>

                <div id="RFP" >
                    <div class="tableHead_1 t_space">RFP Dates :</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>
                            <td width="450" class="ff" colspan="2">Expected date of Issue of RFP : <span>*</span></td>
                            <td width="100" colspan="2"><input name="RFPexpdtissueRFP" type="text" value="${appViewPkgDtBean.tenderAdvertDt}" class="formTxtBox_1" id="txtRFPexpdtissueRFP" style="width:100px;" readonly="readonly"  onfocus="GetCal('txtRFPexpdtissueRFP','txtRFPexpdtissueRFP');" onblur="if(CompareToForGreaterWithRFPexpdtissueRFP(document.getElementById('txtREOQextdtAppsrtlstFrm'),this)){setRFPexpdtSubProposalDate();}"/>
                               <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtRFPexpdtissueRFPimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtRFPexpdtissueRFP','txtRFPexpdtissueRFPimg');" /></a>
                               <span id="rp1" style="color: red;"></span>
                            </td>

                        </tr>
                        <tr>
                            <td width="150" class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td width="50"><input name="RFPexpdtissueRFPNo" type="text" value="${appViewPkgDtBean.tenderAdvertDays}" class="formTxtBox_1" id="txtRFPexpdtissueRFPNo" style="width:100px;"  onblur="if(chkNumeric(this,'rp1No')){setRFPexpdtSubProposalDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="rp1No"></span>
                            </td>
                            <td class="ff" width="400">Expected Date of Submission of Tender :  </td>
                            <td width="300"><input name="RFPexpdtSubProposal" type="text" value="${appViewPkgDtBean.tenderSubDt}" class="formTxtBox_1" id="txtRFPexpdtSubProposal" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="rp2" style="color: red;"></span><span id="err27" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RFPexpdtSubProposalNo" type="text" value="${appViewPkgDtBean.tenderSubDays}" class="formTxtBox_1" id="txtRFPexpdtSubProposalNo" style="width:100px;"  onblur="if(chkNumeric(this,'rp2No')){setRFPexpdttechOpenDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="rp2No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Technical Tender Opening :  </td>
                            <td><input name="RFPexpdttechOpen" type="text" value="${appViewPkgDtBean.tenderOpenDt}" class="formTxtBox_1" id="txtRFPexpdttechOpen" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="rp3" style="color: red;"></span><span id="err28" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RFPexpdttechOpenNo" type="text" value="${appViewPkgDtBean.tenderOpenDays}" class="formTxtBox_1" id="txtRFPexpdttechOpenNo" style="width:100px;" onblur="if(chkNumeric(this,'rp3No')){setRFPexpdttechEvaDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();" />
                                <span id="rp3No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Technical Tender Evaluation :  </td>
                            <td><input name="RFPexpdttechEva" type="text" value="${appViewPkgDtBean.rfpTechEvalDt}" class="formTxtBox_1" id="txtRFPexpdttechEva" style="width:100px;" readonly="readonly"  onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="rp4" style="color: red;"></span><span id="err29" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RFPexpdttechEvaNo" type="text" value="${appViewPkgDtBean.rfpTechEvalDays}" class="formTxtBox_1" id="txtRFPexpdttechEvaNo" style="width:100px;"  onblur="if(chkNumeric(this,'rp4No')){setRFPextdtFinOpenDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="rp4No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Financial Tender Opening :  </td>
                            <td><input name="RFPextdtFinOpen" type="text" value="${appViewPkgDtBean.rfpFinancialOpenDt}" class="formTxtBox_1" id="txtRFPextdtFinOpen" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="rp5" style="color: red;"></span><span id="err30" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RFPextdtFinOpenNo" type="text" value="${appViewPkgDtBean.rfpFinancialOpenDays}" class="formTxtBox_1" id="txtRFPextdtFinOpenNo" style="width:100px;" onblur="if(chkNumeric(this,'rp5No')){setRFPexpdtsubCOmEvaRptDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="rp5No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Submission of Combined Evaluation Report :  </td>
                            <td><input name="RFPexpdtsubCOmEvaRpt" type="text" value="${appViewPkgDtBean.tenderEvalRptDt}" class="formTxtBox_1" id="txtRFPexpdtsubCOmEvaRpt" style="width:100px;" readonly="readonly" />
                                <span id="rp6" style="color: red;"></span><span id="err31" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                             <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="RFPexpdtsubCOmEvaRptNo" type="text" value="${appViewPkgDtBean.tenderEvalRptdays}" class="formTxtBox_1" id="txtRFPexpdtsubCOmEvaRptNo" style="width:100px;"
                                 onblur="if(chkNumeric(this,'rp6No')){setRFPexpdtappcomEvaRptDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();recommendDay('rp6No', 'hdnRFPexpdtsubCOmEvaRptNo', 'txtRFPexpdtsubCOmEvaRptNo');" />
                                <input type="hidden" name="hdnRFPexpdtsubCOmEvaRptNo" id="hdnRFPexpdtsubCOmEvaRptNo" value="<% if(plannedDayList.size() > 0){ plannedDayList.get(0).getFieldName1(); } %>">
                                <span id="rp6No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Approval of Combined Evaluation Report :  </td>
                            <td><input name="RFPexpdtappcomEvaRpt" type="text" value="${appViewPkgDtBean.tenderEvalRptAppDt}" class="formTxtBox_1" id="txtRFPexpdtappcomEvaRpt" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="rp7" style="color: red;"></span><span id="err32" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="RFPexpdtappcomEvaRptNo" type="text" value="${appViewPkgDtBean.tenderEvalRptAppDays}" class="formTxtBox_1" id="txtRFPexpdtappcomEvaRptNo"
                                style="width:100px;" onblur="if(chkNumeric(this,'rp7No')){setRFPexpdtcompNegoDate();cleartxtpqdtadvtinvt();recommendDay('rp7No', 'hdnRFPexpdtappcomEvaRpt', 'txtRFPexpdtappcomEvaRptNo');} calculateTotalNoofDays();" />
                                <input type="hidden" name="hdnRFPexpdtappcomEvaRpt" id="hdnRFPexpdtappcomEvaRpt" value="<% if(plannedDayList.size() > 0){ plannedDayList.get(0).getFieldName2(); } %>">
                                <span id="rp7No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Completion of Negotiation :</td>
                            <td><input name="RFPexpdtcompNego" type="text" value="${appViewPkgDtBean.rfpNegCompDt}" class="formTxtBox_1" id="txtRFPexpdtcompNego" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="rp8" style="color: red;"></span><span id="err33" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                             <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RFPexpdtcompNegoNo" type="text" value="${appViewPkgDtBean.rfpNegCompDays}" class="formTxtBox_1" id="txtRFPexpdtcompNegoNo" style="width:100px;"  onblur="if(chkNumeric(this,'rp8No')){setRFPexpdtappawdContractDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="rp8No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Approval for Award of Contract :</td>
                            <td><input name="RFPexpdtappawdContract" type="text" value="${appViewPkgDtBean.rfpContractAppDt}" class="formTxtBox_1" id="txtRFPexpdtappawdContract" style="width:100px;" readonly="readonly" />
                                <span id="rp9" style="color: red;"></span><span id="err34" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td>
                                <input name="RFPexpdtappawdContractNo" type="text" value="${appViewPkgDtBean.rfpContractAppDays}" class="formTxtBox_1" id="txtRFPexpdtappawdContractNo" style="width:100px;"
                                onblur="if(chkNumeric(this,'rp9No')){setRFPexpdtsigncontractDate();cleartxtpqdtadvtinvt(); recommendDay('rp9No', 'hdnRFPexpdtappawdContract', 'txtRFPexpdtappawdContractNo');} calculateTotalNoofDays();" />
                                <input type="hidden" name="hdnRFPexpdtappawdContract" id="hdnRFPexpdtappawdContract" value="<% if(plannedDayList.size() > 0){ plannedDayList.get(0).getFieldName2(); } %>">
                                <span id="rp9No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Signing of Contract :</td>
                            <td><input name="RFPexpdtsigncontract" type="text" value="${appViewPkgDtBean.tenderContractSignDt}" class="formTxtBox_1" id="txtRFPexpdtsigncontract" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="rp10" style="color: red;"></span><span id="err35" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RFPexpdtsigncontractNo" type="text" value="${appViewPkgDtBean.tenderContractSignDays}" class="formTxtBox_1" id="txtRFPexpdtsigncontractNo" style="width:100px;" onblur="if(chkNumeric(this,'rp10No')){setRFPexpdtcomplcontractDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="rp10No" style="color: red;"></span>
                            </td>
                            <td class="ff">Expected Date of Completion of Contract :</td>
                            <td><input name="RFPexpdtcomplcontract" value="${appViewPkgDtBean.tenderContractCompDt}" type="text" class="formTxtBox_1" id="txtRFPexpdtcomplcontract" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()" onfocus ="GetCalWithouTime('txtRFPexpdtcomplcontract','txtRFPexpdtcomplcontract'); settxtRFPexpdtsigncontractNo();"/>
                                    <a  href="javascript:void(0);" title="Calender"> <img id="txtRFPexpdtcomplcontractimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithouTime('txtRFPexpdtcomplcontract','txtRFPexpdtcomplcontractimg');"/> </a>
                                <span id="rp11" style="color: red;"></span>
                                <span id="err36" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                    </table>
                </div>
                <%} else if ("RFA".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired()) || "RFP".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())) {%>
                <%
                   String strprocLabel = "";
                   if("RFA".equalsIgnoreCase(appViewPkgDtBean.getReoiRfaRequired())){
                        strprocLabel = "RFA";
                   }else{
                        strprocLabel = "RFP";
                   }
                %>
                <script>
                    document.getElementById("hdnDateType").value = "<%=strprocLabel%>";
                </script>
                <div id="RFA" >
                    <div class="tableHead_1 t_space"><%=strprocLabel%> Dates :</div>
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>
                            <td width="450" class="ff" colspan="2"> Expected Date of Advertisement of <%=strprocLabel%> on e-GP Website : <span>*</span></td>
                            <td width="100" colspan="2"><input name="RFAexpdtadvtRFA" type="text" value="${appViewPkgDtBean.rfaAdvertDt}" class="formTxtBox_1" id="txtRFAexpdtadvtRFA" style="width:100px;" readonly="readonly"  onfocus="GetCalWithCond('txtRFAexpdtadvtRFA','txtRFAexpdtadvtRFA','ra1');" onblur="setRFAdtrcptAppDate();cleartxtpqdtadvtinvt()"/>
                                <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtRFAexpdtadvtRFAimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithCond('txtRFAexpdtadvtRFA','txtRFAexpdtadvtRFAimg','ra1');" /></a>
                                <span id="ra1" style="color: red;"></span>
                            </td>

                        </tr>
                        <tr>
                            <td width="150" class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td width="50"><input name="RFAexpdtadvtRFANo" type="text" value="${appViewPkgDtBean.rfaAdvertDays}" class="formTxtBox_1" id="txtRFAexpdtadvtRFANo" style="width:100px;"  onblur="if(chkNumeric(this,'ra1No')){setRFAdtrcptAppDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="ra1No" style="color: red;"></span>
                            </td>
                            <td class="ff" width="400"> Date of Receipt of Application :  </td>
                            <td width="300"><input name="RFAdtrcptApp" type="text" value="${appViewPkgDtBean.rfaReceiptDt}" class="formTxtBox_1" id="txtRFAdtrcptApp" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="ra2" style="color: red;"></span><span id="err37" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RFAdtrcptAppNo" type="text" value="${appViewPkgDtBean.rfaReceiptDays}" class="formTxtBox_1" id="txtRFAdtrcptAppNo" style="width:100px;"  onblur="if(chkNumeric(this,'ra2No')){setRFAdteveappDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"  />
                                <span id="ra2No"></span>
                            </td>
                            <td class="ff"> Date of Evaluation of Application :  </td>
                            <td><input name="RFAdteveapp" type="text" value="${appViewPkgDtBean.rfaEvalDt}" class="formTxtBox_1" id="txtRFAdteveapp" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="ra3" style="color: red;"></span><span id="err38" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RFAdteveappNo" type="text" value="${appViewPkgDtBean.rfaEvalDays}" class="formTxtBox_1" id="txtRFAdteveappNo" style="width:100px;"  onblur="if(chkNumeric(this,'ra3No')){setRFAdtintrvwselIndDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="ra3No"></span>
                            </td>
                            <td class="ff"> Date of Interview of Selected Individuals :  </td>
                            <td><input name="RFAdtintrvwselInd" type="text" value="${appViewPkgDtBean.rfaInterviewDt}" class="formTxtBox_1" id="txtRFAdtintrvwselInd" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="ra4"></span><span id="err39" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RFAdtintrvwselIndNo" type="text" value="${appViewPkgDtBean.rfaInterviewDays}" class="formTxtBox_1" id="txtRFAdtintrvwselIndNo" style="width:100px;" onblur="if(chkNumeric(this,'ra4No')){setRFAdtevaFnlselLstDate();cleartxtpqdtadvtinvt();}" />
                                <span id="ra4No" style="color: red;"></span>
                            </td>
                            <td class="ff"> Date of Evaluation of Final selection list :  </td>
                            <td><input name="RFAdtevaFnlselLst" type="text" value="${appViewPkgDtBean.rfaFinalSelDt}" class="formTxtBox_1" id="txtRFAdtevaFnlselLst" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="ra5" style="color: red;"></span><span id="err40" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RFAdtevaFnlselLstNo" type="text" value="${appViewPkgDtBean.rfaFinalSelDays}" class="formTxtBox_1" id="txtRFAdtevaFnlselLstNo" style="width:100px;" onblur="if(chkNumeric(this,'ra5No')){setRFAdtsubevaRptDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="ra5No" style="color: red;"></span>
                            </td>
                            <td class="ff"> Date of Submission of Evaluation Report :  </td>
                            <td><input name="RFAdtsubevaRpt" type="text" value="${appViewPkgDtBean.rfaEvalRptSubDt}" class="formTxtBox_1" id="txtRFAdtsubevaRpt" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="ra6" style="color: red;"></span><span id="err41" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">+ No. Of Days : <span class="mandatory">*</span></td>
                            <td><input name="RFAdtsubevaRptNo" type="text" value="${appViewPkgDtBean.rfaEvalRptSubDays}" class="formTxtBox_1" id="txtRFAdtsubevaRptNo" style="width:100px;"  onblur="if(chkNumeric(this,'ra6No')){setRFAdtappConsDate();cleartxtpqdtadvtinvt();} calculateTotalNoofDays();"/>
                                <span id="ra6No" style="color: red;"></span>
                            </td>
                            <td class="ff"> Date of Approval of Consultants :  </td>
                            <td><input name="RFAdtappCons" type="text" value="${appViewPkgDtBean.rfaAppConsultantDt}" class="formTxtBox_1" id="txtRFAdtappCons" style="width:100px;" readonly="readonly"   onblur="cleartxtpqdtadvtinvt()"/>
                                <span id="ra7" style="color: red;"></span><span id="err42" style="color: red; font-weight: bold"></span>
                            </td>

                        </tr>
                    </table>
                </div>
                <%}%>
                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                   <tr>

                        <td width="165" class="ff">Total Time to Contract Signing :</td>
                        <td width="50"><input type="text" class="formTxtBox_1" name="txttotal" id="idtxttotal" style="width:100px;" readonly="readonly" /></td>
                        <td width="400">&nbsp;</td>
                        <td width="100">&nbsp;</td>
                   </tr>
                    <% if(isRevision) { %>
                    <input type="hidden" name="isRevision" id="isRevision" value="true" />

                    <% } %>
                    <tr>
                        <td colspan="4" align="center">
                            <label class="formBtn_1">
                                 <% if(isRevision) { %>
                                              <input type="submit" name="Revise" id="btnSave" value="Revise" onclick="return submitfunction();"/>
                                              <% }else { %>
                                             <input type="submit" name="update" id="btnSave" value="Update" onclick="return submitfunction();"/>
                                              <% } %>
                               
                            </label>
                            &nbsp;&nbsp;
                            <label class="formBtn_1">
                                <input type="button" name="cancel" id="btnCancel" value="Cancel" onclick="location.href='APPDashboard.jsp?appID=<%=appId%>';"/>
                            </label>
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>
            </form>
             <%}
              else {%>
              <table width="100%" border="0" cellspacing="10" cellpadding="0">
                    <tr>
                        <td>
                            <div class="responseMsg errorMsg">Provide Valid App-Id, Package-Id</div>
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>
                <%}%>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
             </div>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabApp");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
