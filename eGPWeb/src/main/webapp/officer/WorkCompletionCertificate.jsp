<%-- 
    Document   : WorkCompletionCertificate
    Created on : Jul 28, 2011, 10:54:19 AM
    Author     : Sreenu.Durga
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.model.table.TblContractSign"%>
<%@page import="com.cptu.egp.eps.model.table.TblNoaIssueDetails"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />       
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Work Completion Certificate</title>
    </head>
    <%
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                int contractUserId = 0;
                if (request.getParameter("contractUserId") != null) {
                    contractUserId = Integer.parseInt(request.getParameter("contractUserId"));
                }
                int contractSignId = 0;
                if (request.getParameter("contractSignId") != null) {
                    contractSignId = Integer.parseInt(request.getParameter("contractSignId"));
                }
                String strUserId = "";
                if (session.getAttribute("userId") != null) {
                    strUserId = session.getAttribute("userId").toString();
                }
                int intWcCertiId = 0;
                if(request.getParameter("wcCertId")!=null){
                    intWcCertiId  = Integer.parseInt(request.getParameter("wcCertId"));
                }
    %>
    <script language="javascript">
        /* Call Calendar function */
        function GetCalendar(txtname,controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: 24,
                //dateFormat:"%d/%m/%Y",
                onSelect: function() {
                    var date = Calendar.intToDate(this.selection.get());
                    //var DateVar = document.getElementById("curDate").value;
                    //var currentDate =Date.parse(DateVar);
                    if(compareWithCurrDate()){
                        jAlert("Please select current date or past date","Warning!");
                        document.getElementById(txtname).value = "";
                    }else{
                        if(compareWithContractSigningDate()){
                            jAlert("issuing Work Completion certificate date can not be less than contract Signing date","Warning!");
                        document.getElementById(txtname).value = "";
                        }else{
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();                        
                        }
                    }
                    this.hide();
                }
            });
            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })
        }

        function compareWithCurrDate(){
            var first = document.getElementById("txtDateOfCompletion").value;
            var mdy = first.split('/')  //Date and month split
            var mdyhr= mdy[2].split(' ');  //Year and time split
            var mdyhrtime=mdyhr[1].split(':');
            if(mdyhrtime[1] == undefined){
                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
            }else
            {
                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0],0 ,0);
            }

            var d = new Date();
            if(mdyhrtime[1] == undefined){
                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
            }
            else
            {
                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),0,0);
            }            
            return Date.parse(valuedate) > Date.parse(todaydate);
        }
        function compareWithContractSigningDate(){
            var first = document.getElementById("txtDateOfCompletion").value;
            var mdy = first.split('/')  //Date and month split
            var mdyhr= mdy[2].split(' ');  //Year and time split
            var mdyhrtime=mdyhr[1].split(':');
            if(mdyhrtime[1] == undefined){
                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
            }else
            {
                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
            }

            var second = document.getElementById("contractSignDate").value;
            var mdyy = second.split('/')  //Date and month split
            var mdyhrr= mdyy[2].split(' ');  //Year and time split
            var mdyhrtimee=mdyhrr[1].split(':');
            if(mdyhrtimee[1] == undefined){
                var valuedatee= new Date(mdyhrr[0], mdyy[1]-1, mdyy[0]);
            }else
            {
                var valuedatee= new Date(mdyhrr[0], mdyy[1]-1, mdyy[0], mdyhrtimee[0], mdyhrtimee[1]);
            }
            return Date.parse(valuedate) < Date.parse(valuedatee);
        }
    </script>
    <script type="text/javascript">
//        $(document).ready(function(){
//            $("tr[id='tblRow_PGRequire']").hide();
//            $("tr[id='tblRow_txtPerformanceGuarantee']").hide();
//            $("forfeitRadioButton").change(function(){
//                $("tr[id='tblRow_PGRequire']").hide();
//                document.getElementById('txtPerformanceGuarantee').value = 0;
//                $("tr[id='tblRow_txtPerformanceGuarantee']").hide();
//            });
//            $("relaseRadioButton").change(function(){
//                $("tr[id='tblRow_PGRequire']").show();
//                $("tr[id='tblRow_txtPerformanceGuarantee']").hide();
//            });
//            $("#frmWCCertificate").validate({
//                rules: {
//                    txtDateOfCompletion:{required: true},
//                    radioWorkComplete:{required: true},
//                    radioPerformanceGuarantee:{required: true},
//                    radioPGRequire:{required: true},
//                    txtPerformanceGuarantee:{required: true,requiredWithoutSpace: true , number:true},
//                    radioRateVendor:{required: true}
//                    //selectSendToTenderer:{required: true,alphanumeric:true}
//                    //txtRemarks:{required: true}
//                },
//                messages: {
//                    txtDateOfCompletion: {
//                        required: "<div class='reqF_1'>Please Select a Date</div>"
//                    },
//                    radioWorkComplete: {
//                        required: "<div class='reqF_1'>Please Select a Value</div>"
//                    },
//                    radioPerformanceGuarantee: {
//                        required: "<div class='reqF_1'>Please Select a Value</div>"
//                    },
//                    radioPGRequire: {
//                        required: "<div class='reqF_1'>Please Select a Value</div>"
//                    },
//                    txtPerformanceGuarantee:{
//                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
//                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
//                    },
//                    radioRateVendor: {
//                        required: "<div class='reqF_1'>Please Select a Value</div>"
//                    }
//                    //selectSendToTenderer: {
//                    //    required: "<div class='reqF_1'>Please Select a Value</div>",
//                    //    alphanumeric:"<div class='reqF_1'>Please Select a value</div>"
//                    //}
//                    //txtRemarks: {
//                    //    required: "<div class='reqF_1'>Please Enter Remarks</div>"
//                    //}
//                },
//                errorPlacement:function(error ,element){
//                    //error.insertBefore(element);
//                    error.insertAfter(element);
//                }
//            });
//        });

        $(function() {
            $( "#dialog:ui-dialog" ).dialog( "destroy" );
            $( "#dialog-form" ).dialog({
                autoOpen: false,
                resizable:false,
                draggable:true,
                height: 250,
                width: 250,
                modal: true,
                buttons: {
                    "Ok": function() {
                         $(":input[type='radio']").each(function(){
                             if($( this ).attr('checked')){
                                 setHdnStar(this);
                             }
                         });
                        $( this ).dialog( "close" );
                    }
                },
                close: function() {
                }
            });

            $("#addmem" ).click(function(){
                $("#dialog-form").dialog("open");
            });
        });

        function setHdnStar(obj){
            var hdnStarObj = document.getElementById("hdnStar");
            var hdnObj = document.getElementById("radioRateVendor");
            var lblObj = document.getElementById("lblVR");
            var star;
            if(obj.value == "Poor"){
                hdnStarObj.innerHTML = "<img src=../resources/images/star.jpeg />";
                star="*";
            }else if(obj.value == "Ok"){
                hdnStarObj.innerHTML = "<img src=../resources/images/star.jpeg /><img src=../resources/images/star.jpeg />";
                star="**";
            }else if(obj.value == "Good"){
                hdnStarObj.innerHTML = "<img src=../resources/images/star.jpeg /><img src=../resources/images/star.jpeg /><img src=../resources/images/star.jpeg />";
                star="***";
            }else if(obj.value == "Very Good"){
                hdnStarObj.innerHTML = "<img src=../resources/images/star.jpeg /><img src=../resources/images/star.jpeg /><img src=../resources/images/star.jpeg /><img src=../resources/images/star.jpeg />";
                star="****";
            }else{
                hdnStarObj.innerHTML = "<img src=../resources/images/star.jpeg /><img src=../resources/images/star.jpeg /><img src=../resources/images/star.jpeg /><img src=../resources/images/star.jpeg /><img src=../resources/images/star.jpeg />";
                star="*****";
            }
            hdnObj.value = obj.value;
            lblObj.innerHTML = obj.value;
        }

        function showHideNewPG(obj){
            if(obj.value =="no"){
                $("tr[id='tblRow_txtPerformanceGuarantee']").hide();
            }else{
                $("tr[id='tblRow_txtPerformanceGuarantee']").show();
            }
        }
    </script>
    <%
        String lotId = "";
        if (request.getParameter("lotId") != null) {
            pageContext.setAttribute("lotId", request.getParameter("lotId"));
            lotId = request.getParameter("lotId");
        }

        ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
        boolean isPhysicalPrComplete = service.checkForItemFullyReceivedOrNot(Integer.parseInt(lotId));

        String perSAmt = "";
        NOAServiceImpl noaService = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
        noaService.setLogUserId(strUserId);
        List<TblNoaIssueDetails> lstNoa = noaService.getPerformanceSecAmt(Integer.parseInt(tenderId), contractUserId, Integer.parseInt(lotId));
        if(!lstNoa.isEmpty() && lstNoa.size() > 0){
            perSAmt = lstNoa.get(0).getPerfSecAmt().toString();
        }
        CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
        String ContractSigningdate = "";
        List<TblContractSign> ContractsignList = cmsConfigDateService.getContractSigningDate(contractSignId);
        {
            if(!ContractsignList.isEmpty())
            {
                ContractSigningdate = DateUtils.convertStrToDate(ContractsignList.get(0).getContractSignDt());
            }    
        }
        boolean isServ = false;
        boolean isTimeBased = false;
        boolean isAllMileSComp = false;
        CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
        String procnature = commonservice.getProcNature(request.getParameter("tenderId")).toString();
        if("services".equalsIgnoreCase(procnature))
        {
            isServ = true;
        }    
        String serviceType = commonservice.getServiceTypeForTender(Integer.parseInt(tenderId));
        if("Time based".equalsIgnoreCase(serviceType))
        {
            isTimeBased = true;
        }
        isAllMileSComp = service.checkForItemFullyReceivedOrNotForServices(Integer.parseInt(tenderId));
    %>
    <body>
        <div id="dialog-form" title="Vendor Rating">
            <fieldset>
                <table border="0" cellspacing="5" cellpadding="5" class="formStyle_1" >
                    <tr>
                        <td><input type="radio" name="rdo" id="radioRateVendor1"  value="Poor" ></td>
                        <td><img src="../resources/images/star.jpeg"/></td>
                        <td><b>Poor</b></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="rdo" id="radioRateVendor2"  value="Ok" ></td>
                        <td><img src="../resources/images/star.jpeg"/><img src="../resources/images/star.jpeg"/></td>
                        <td><b>OK</b></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="rdo" id="radioRateVendor3"  value="Good" ></td>
                        <td><img src="../resources/images/star.jpeg"/><img src="../resources/images/star.jpeg"/><img src="../resources/images/star.jpeg"/></td>
                        <td><b>Good</b></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="rdo" id="radioRateVendor4"  value="Very Good" ></td>
                        <td><img src="../resources/images/star.jpeg"/><img src="../resources/images/star.jpeg"/><img src="../resources/images/star.jpeg"/><img src="../resources/images/star.jpeg"/></td>
                        <td><b>Very Good</b></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="rdo" id="radioRateVendor5"  value="Excellent" ></td>
                        <td><img src="../resources/images/star.jpeg"/><img src="../resources/images/star.jpeg"/><img src="../resources/images/star.jpeg"/><img src="../resources/images/star.jpeg"/><img src="../resources/images/star.jpeg"/></td>
                        <td><b>Excellent</b></td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                            <br/>
                            <!--Page Content Start-->
                            <div class="pageHead_1"> Work Completion Certificate
                                <span style="float: right; text-align: right;">
                                    <%
                                     if("services".equalsIgnoreCase(procnature))
                                     {
                                        if("Time based".equalsIgnoreCase(serviceType.toString()))
                                        {    
                                    %>
                                        <a class="action-button-goback" href="ProgressReportMain.jsp?tenderId=<%=tenderId%>">Go back</a>
                                        <%}else{%>
                                        <a class="action-button-goback" href="SrvLumpSumPr.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}}else{%>
                                        <a class="action-button-goback" href="ProgressReport.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}%>
                                </span>
                            </div>
                            <form method="POST" id="frmWCCertificate" action="<%= request.getContextPath() %>/WorkCompletionCertificateServlet">
                                <input type="hidden" name="hidWcCertID" id="hidWcCertID" value="<%=intWcCertiId%>" />
                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1" >
                                    <tr id="tblRow_mandatoryMessage">
                                        <td style="font-style: italic" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.
                                            <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>">
                                            <input type="hidden" name="contractUserId" id="contractUserId" value="<%=contractUserId%>">
                                            <input type="hidden" name="contractSignId" id="contractSignId" value="<%=contractSignId%>">
                                            <input type="hidden" name="contractSignDate" id="contractSignDate" value="<%=ContractSigningdate%>">
                                            <input type="hidden" name="lotId" id="lotId" value="<%=lotId%>">
                                        </td>
                                    </tr>
                                    <tr id="tblRow_dateOfCompletion">
                                        <td class="ff" width="25%">
                                            <% if(isPhysicalPrComplete){ %>
                                            Actual Contract Completion Date : 
                                            <% }else{ %>
                                            Date of issuing Work Completion Certificate :
                                            <% } %>
                                            <span>*</span>
                                        </td>
                                        <td class="ff" width="75%">
                                            <input name="txtDateOfCompletion" type="text" class="formTxtBox_1" id="txtDateOfCompletion" readonly="true" onClick="GetCalendar('txtDateOfCompletion','txtDateOfCompletion');" />
                                            &nbsp;
                                            <a href="javascript:void(0);" onclick ="GetCalendar('txtDateOfCompletion','imgCal');" title="Calender">
                                                <img id="imgCal" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"/>
                                            </a>
                                            <span id="txtDateOfCompletionspan" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr id="tblRow_isWorkCompletion">
                                        <td class="ff">Is Work Completed : </td>
                                        <td>
                                            <%                                            
                                            if(isServ){
                                                if(isTimeBased){
                                                %>
                                                    Yes <input type='radio' name='radioWorkComplete' id='radioWorkCompleteY' value='Yes' />
                                                    No <input type='radio' name='radioWorkComplete' id='radioWorkCompleteN' value='No' checked />
                                                <%
                                                }else{
                                                    if(isAllMileSComp){
                                                        out.print("Yes");
                                                        out.print("<input type='hidden' name='radioWorkComplete' id='radioWorkComplete' value='Yes' />");
                                                    }else{
                                                        out.print("No");
                                                        out.print("<input type='hidden' name='radioWorkComplete' id='radioWorkComplete' value='No' />");
                                                    }
                                                }
                                            }else{
                                                if(isPhysicalPrComplete){
                                                    out.print("Yes");
                                                    out.print("<input type='hidden' name='radioWorkComplete' id='radioWorkComplete' value='Yes' />");
                                                }else{
                                                    out.print("No");
                                                    out.print("<input type='hidden' name='radioWorkComplete' id='radioWorkComplete' value='No' />");
                                                }
                                            }
                                            %>
                                        </td>
                                    </tr>
                                    <%--<tr id="tblRow_PerformanceGuarantee">
                                        <td class="ff">Performance Guarantee : <span>*</span></td>
                                        <td class="ff">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="100">
                                                    <relaseRadioButton>
                                                        <input type="radio" name="radioPerformanceGuarantee" id="radioPerformanceGuarantee"  value="release" >&nbsp;<b>Release</b>
                                                    </relaseRadioButton>
                                                    </td>
                                                    <td width="100">
                                                        <forfeitRadioButton>
                                                            <input type="radio" name="radioPerformanceGuarantee" id="radioPerformanceGuarantee" value="forfeit" >&nbsp;<b>Forfeit</b>
                                                        </forfeitRadioButton>
                                                    </td>
                                                    <% if(!"".equals(perSAmt)){%>
                                                    <td>
                                                        Performance Security (In BTN): <%= perSAmt %>
                                                    </td>
                                                    <% } %>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                    <tr id="tblRow_PGRequire" >
                        <td  class="ff">New Performance Security Require : <span>*</span></td>
                        <td class="ff">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="100"><input type="radio" name="radioPGRequire" id="radioPGRequire"  value="yes" onclick="showHideNewPG(this);" >&nbsp;<b>Yes</b></td>
                                    <td width="100"><input type="radio" name="radioPGRequire" id="radioPGRequire" value="no" onclick="showHideNewPG(this);" checked >&nbsp;<b>No</b></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="tblRow_txtPerformanceGuarantee" style="display: none">
                        <td  class="ff">Amount of New Performance Security:<br />(In BTN): <span>*</span></td>
                        <td class="ff">
                            <input type="text" class="formTxtBox_1" name="txtPerformanceGuarantee" id="txtPerformanceGuarantee"  size="20">
                        </td>
                    </tr>--%>
                    <tr id="tblRow_rateVendor">
                        <td  class="ff">Vendor Rating : </td>
                        <td class="ff">
                            <table>
                                <tr>
                                    
                                    <td><a id="addmem" class="action-button-add">Rating</a></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><label id="hdnStar"></label></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><label id="lblVR"></label></td>
                                    <input type="hidden" class="formTxtBox_1" id="radioRateVendor" name="radioRateVendor" value="" />
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td class="ff">Send To Bidder : <span>*</span></td>
                        <td class="ff">
                            <select name="selectSendToTenderer" class="formTxtBox_1" id="selectSendToTenderer" style="width:100px;">
                                <option value='^'>- Select -</option>
                                <option value="Y" selected='selected'>Yes</option>
                                <option value="N" >No</option>
                            </select>
                        </td>
                    </tr>
                    <tr id="tblRow_remarks">
                        <td  class="ff">Remarks  : <span>*</span></td>
                        <td >
                            <textarea name="txtRemarks" rows="5" cols="50" style="width:90%;" class="formTxtBox_1" id="txtRemarks" ></textarea>
                            <script type="text/javascript">
                                CKEDITOR.replace( 'txtRemarks',{toolbar : "egpToolbar"});
                            </script>
                            <span id="remarksspan" class="reqF_1"></span>
                        </td>
                    </tr>
                    <%if(isPhysicalPrComplete){%>
                    <tr id="tblRow_formButton">
                        <td class="t-align-center" colspan="2" >
                            <div  class='responseMsg noticeMsg t-align-left'>
                                On issuance of Work Completion Certificate, system will not allow to prepare any further Progress Report
                            </div>
                        </td>
                    </tr>
                    <%}%>
                    <tr id="tblRow_formButton">
                        <td class="t-align-center" colspan="2" >
                            <label class="formBtn_1">
                                <input type="submit" align="middle" name="submit" id="submit" value="Save" onclick="return chkSubmit();"/>
                            </label>
                        </td>
                    </tr>
                </table>
                </form>
                </td><!--Page Content End-->
                </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
    <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
    <script type="text/javascript">
       function chkSubmit()
        {
            var j=0; var m=0;
            var vbool = true;
            if(document.getElementById("txtDateOfCompletion").value == "")
            {
                document.getElementById("txtDateOfCompletionspan").innerHTML = "Please select Date";
                vbool= false;
            }
            else{document.getElementById("txtDateOfCompletionspan").innerHTML="";}            
            if($.trim(CKEDITOR.instances.txtRemarks.getData()) == ""){
                document.getElementById("remarksspan").innerHTML = "Please Enter Remarks";
                vbool= false;
            }
            return vbool;
        }
        $(document).ready(function() {
                $(document).ready(function() {
                CKEDITOR.instances.txtRemarks.on('blur', function()
                {
                    if(CKEDITOR.instances.txtRemarks.getData() != 0)
                    {
                            document.getElementById("remarksspan").innerHTML="";
                    }
                });
            });
            });
      </script>
</html>
