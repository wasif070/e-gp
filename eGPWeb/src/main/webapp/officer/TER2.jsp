<%--
    Document   : TER2
    Created on : Apr 14, 2011, 10:31:01 AM
    Author     : TaherT
--%>
<%--
Modified functions and add parameter for re-evaluation by dohatec
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CompareReportT1"%>
<%@page import="java.util.Collections"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
       <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String tenderid = request.getParameter("tenderid");
                    String lotId = request.getParameter("lotId");
                    String roundId = "0";
                    String rId = "0";
                    if(request.getParameter("rId")!=null){
                        roundId = request.getParameter("rId");
                    }
                    if(request.getParameter("roundId")!=null){
                        rId = request.getParameter("roundId");
                    }
                    String stat = request.getParameter("stat");//stat value is 'eval'
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String pNature = commonService.getProcNature(request.getParameter("tenderid")).toString();
                    //String eventType = commonService.getEventType(tenderid).toString();
                    //String tenderPaidORFree = commonService.tenderPaidORFree(tenderid, lotId);
                    boolean bole_ter_flag = true;
                    if(request.getParameter("isPDF")!=null && "true".equalsIgnoreCase(request.getParameter("isPDF"))){
                        bole_ter_flag = false;
                        //System.out.println("isPDF"+bole_ter_flag);
                    }
                    String cnty = request.getParameter("cnt");
                    String grId = request.getParameter("grId");
                    String userId = "";
                    if(bole_ter_flag){
                        if(session.getAttribute("userId") != null){
                            userId = session.getAttribute("userId").toString();
                        }
                    }else{
                        if(request.getParameter("tendrepUserId") != null && !"".equals(request.getParameter("tendrepUserId"))){
                            userId = request.getParameter("tendrepUserId");
                        }
                    }
                     int evalCount = 0;
                     if(request.getParameter("evalCount")!=null){
                        evalCount = Integer.parseInt(request.getParameter("evalCount"));
                        //System.out.println("isPDF"+bole_ter_flag);
                    }
                    String repLabel = "Tender";
                    if(pNature.equalsIgnoreCase("Services")){
                        repLabel = "Proposal";
                    }
                    boolean showGoBack = true;
                    if("y".equals(request.getParameter("in"))){
                        showGoBack = false;
                    }
                    // EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                    //int evalCount = evalService.getEvaluationNo(Integer.parseInt(tenderid));
                   
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=repLabel%> Evaluation Report 2</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <%if(bole_ter_flag){%>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <% } 
                ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");

                    List<SPCommonSearchDataMore> tendOpeningRpt = creationService.getOpeningReportData("getTenderOpeningRpt", tenderid, lotId,null);//lotid

                    CommonSearchDataMoreService searchDataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> TECMembers = dataMoreService.getCommonSearchData("getTECMembers", tenderid, lotId,"TER2",roundId,String.valueOf(evalCount)); //lotid ,"TOR1"


                    List<Object[]> ter2Criteria = creationService.getTERCriteria("TER2", pNature);
                    List<SPCommonSearchDataMore> NoteofDissent = creationService.getOpeningReportData("getNoteofDissent", tenderid, lotId,"ter2");
                    List<SPCommonSearchDataMore> pMethodEstCost = searchDataMoreService.getCommonSearchData("GetPMethodEstCostTender",tenderid);
                    String estCost = pMethodEstCost.get(0).getFieldName2();
                    String estCostStatus = "-";
                    if(estCost!=null)
                    {
                        estCostStatus = (Double.parseDouble(estCost) > 0?"Approved":"-");
                    }

                    boolean list1 = true;
                    boolean list5 = true;
                    if (tendOpeningRpt.isEmpty()) {
                        list1 = false;
                    }
                    if (TECMembers.isEmpty()) {
                        list5 = false;
                    }
                    boolean isView = false;
                    boolean isSign = false;
                    boolean isMEM = true;
                    if("y".equals(request.getParameter("isview"))){
                        isView = true;
                    }
                    if("y".equals(request.getParameter("sign"))){
                        isSign = true;
                    }
                    if("y".equals(request.getParameter("ismem"))){
                        isMEM = false;
                    }
                    
                    if(isView)
                    {
                         // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    String idType="tenderId";
                    int auditId=Integer.parseInt(request.getParameter("tenderid"));
                    String auditAction="View Tender Evaluation Report 2";
                    String moduleName=EgpModule.Evaluation.getName();
                    String remarks="";
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                        
                    }
                    
                    
        %>
        <div class="tabPanelArea_1">
            <div  id="print_area">
            <div class="pageHead_1"><%=repLabel%> Evaluation Report 2 - Technical Responsiveness Report<span style="float: right;">
                    <%
                        boolean isCommCP = false;                        
                        List<SPCommonSearchDataMore> commCP = dataMoreService.getCommonSearchData("evaluationMemberCheck", tenderid, userId, "1", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        if (commCP != null && (!commCP.isEmpty()) && (!commCP.get(0).getFieldName1().equals("0"))) {
                            isCommCP = true;
                        }                        
                        commCP=null;
                        if(bole_ter_flag){
                            if(isView){
                    %>
                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                        <span id="svpdftor1">
                        &nbsp;&nbsp;
                        <a class="action-button-savepdf" id="saveASPDF" href="<%=request.getContextPath()%>/TorRptServlet?tenderId=<%=tenderid%>&lotId=<%=lotId%>&action=TER2">Save As PDF</a>
                        </span>
                        <% } %>
                    <%if(showGoBack){%>
                    <%if("cl".equals(request.getParameter("frm"))){%>
                            &nbsp;&nbsp;
                        <a class="action-button-goback" id="goBack" href="Evalclarify.jsp?tenderId=<%=tenderid%>&st=cl&comType=TEC">Go Back to Dashboard</a>
                    <%}else{if(isCommCP){
                    %>                        
                        &nbsp;&nbsp;
                        <a class="action-button-goback" id="goBack" href="Evalclarify.jsp?tenderId=<%=tenderid%>&st=rp&comType=TEC">Go Back to Dashboard</a>
                    <%}else{%>
                        &nbsp;&nbsp;
                        <a class="action-button-goback" id="goBack" href="MemberTERReport.jsp?tenderId=<%=tenderid%>&comType=TEC">Go Back to Dashboard</a>
                    <%}}}}%>
                </span></div>
            <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                        pageContext.setAttribute("tab", "6");
                        pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                        pageContext.setAttribute("userId", 0);
                        /*if(!bole_ter_flag){
                            pageContext.setAttribute("isPDF","true");
                        }*/
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div class="tableHead_1 t_space t-align-center">Technical Responsiveness Report</div>
            <form action="<%=request.getContextPath()%>/ServletEvalCertiService?funName=evalTERReportIns&rep=<%=repLabel.charAt(0)%>er2" method="post">
            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 b_space" width="100%">
                <tr>
                    <td width="24%" class="ff">Ministry Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName1());
                                }%></td>
                    <td width="16%" class="ff">Division Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName2());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Organization/Agency Name :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName3());
                                }%></td>
                    <td class="ff">Procuring Entity :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName4());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff"><%=repLabel%> Package No. and Description :</td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName5()+" & "+tendOpeningRpt.get(0).getFieldName6());
                                }%></td>
                </tr>
                 <%if(!pNature.equals("Services")){%>
                <tr>
                    <td class="ff">Lot No. and Description : </td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName12());
                                    if(!tendOpeningRpt.get(0).getFieldName12().equals("")){
                                        out.print(" & ");
                                    }
                                    out.print(tendOpeningRpt.get(0).getFieldName13());
                                }%></td>
                </tr>
                <%}%>
            </table>
            <div class="inner-tableHead t_space">Procurement Data</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>

                        <th width="47%">Procurement Type</th>
                        <th width="53%">Procurement Method</th>
                    </tr>
                    <tr>
                        <td class="t-align-center"><%if (list1) {
                                    if (tendOpeningRpt.get(0).getFieldName7().equalsIgnoreCase("NCT")) //Edit by aprojit
                                       out.print("NCB");
                                   else if (tendOpeningRpt.get(0).getFieldName7().equalsIgnoreCase("ICT"))
                                       out.print("ICB");
                                }%></td>
                       <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName9());
                                }%></td>
                    </tr>
                </tbody></table>

            <div class="inner-tableHead t_space">Procurement Plan</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>
                        <th width="17%">Approval<br>
                            Status</th>
                        <th width="31%">Budget Type</th>
                        <%  if(!pNature.equals("Services")){%>
                       <th width="14%">Approval Status
                            of<br>
                            Official Estimates</th>
                       <%}%>
                    </tr>
                    <tr>
                        <td class="t-align-center">Approved</td>
                        <td class="t-align-center"><%if (list1) {                                    
                                   if(tendOpeningRpt.get(0).getFieldName8().equalsIgnoreCase("Development")) //Edit by aprojit
                                       out.print("Capital");
                                   else if(tendOpeningRpt.get(0).getFieldName8().equalsIgnoreCase("Revenue"))
                                       out.print("Recurrent");
                                   else
                                       out.print(tendOpeningRpt.get(0).getFieldName8());
                                }%></td>
                        <%  if(!pNature.equals("Services")){%>
                        <td class="t-align-center"><%=estCostStatus%></td>
                        <%}%>
                    </tr>
                </tbody></table>
            
            <%  if(pNature.equals("Services")){
                boolean is2Env = false;
                boolean isT1 = false;
                boolean isT1L1 = false;
                boolean isL1 = false;

                List<SPCommonSearchDataMore> envDataMores = dataMoreService.geteGPData("GetTenderEnvCount", tenderid);
                if(envDataMores!=null && (!envDataMores.isEmpty())){
                    if(envDataMores.get(0).getFieldName1().equals("2")){
                        is2Env = true;
                    }
                    if(envDataMores.get(0).getFieldName2().equals("1")){
                        //Evaluation Method 1. T1 2. L1 3. T1L1
                        isT1 = true;
                    }
                    if(envDataMores.get(0).getFieldName2().equals("2")){
                        //Evaluation Method 1. T1 2. L1 3. T1L1
                        isL1 = true;
                    }
                    if(envDataMores.get(0).getFieldName2().equals("3")){
                        //Evaluation Method 1. T1 2. L1 3. T1L1
                        isT1L1 = true;
                    }
                }
            %>
            <%
                        Object repId = creationService.findRepIdofTOR(tenderid, lotId,"TER");
                        if(repId!=null){
                        out.print("<div class='inner-tableHead t_space'>Price Evaluation</div>");
                    %>
                    <jsp:include page="../officer/TenderRepInclude.jsp">
                        <jsp:param name="tenderid"  value="<%=tenderid%>"/>
                        <jsp:param name="repId"  value="<%=repId%>"/>
                        <jsp:param name="isEval"  value="true"/>
                        <jsp:param name="istos"  value="true"/>
                        <jsp:param name="rId"  value="<%=isL1?roundId:grId%>"/>
                    </jsp:include>
                <%}%>
                <% 
                    if(isT1){
                        String evalStat = null;
                        EvalServiceCriteriaService criteriaService = (EvalServiceCriteriaService) AppContext.getSpringBean("EvalServiceCriteriaService");
                        if ("eval".equals(stat)) {
                            evalStat = "evaluation";
                        } else {
                            evalStat = "reevaluation";
                        }
                        List<Object[]> list = criteriaService.getBidderEvalStatus(tenderid, evalStat, evalCount);
                        Collections.sort(list, new CompareReportT1());
                        int counter  = 1;
                        if(request.getParameter("cnt")!=null && !"null".equals(request.getParameter("cnt")) && !"".equals(request.getParameter("cnt"))){
                            counter = Integer.parseInt(request.getParameter("cnt"));
                        }
                %>
                <table class="tableList_1 t_space" cellspacing="0" width="100%">
                            <tr>
                                <th class="t-align-center" width="15%">Rank</th>
                                <th class="t-align-center" width="50%">Consultant Name</th>
                                <th class="t-align-center" width="35%">Points</th>
                            </tr>
                            <%
                                            int cnt = 1;
                                            for (Object[] obj : list) {
                            %>
                            <tr>
                                <td class="t-align-center">T<%=cnt%><%if(cnt==counter){out.print("<span style='color:green;'> <b>(Current Highest Scorer)</b></span>");}%></td>
                                <td>
                                    <%String data = null;
                                                            if (obj[3].toString().equals("-")) {
                                                                data = obj[1].toString() + " " + obj[2].toString();
                                                                out.print(data);
                                                            } else {
                                                                data = obj[3].toString();
                                                                out.print(data);
                                                            }%>
                                    <input type="hidden" value="<%=data%>" name="cmpName">
                                </td>
                                <td>
                                    <input type="hidden" value="<%=obj[4]%>" name="userIds"/>
                                    <input type="hidden" value="<%=obj[0]%>" name="amount"/>
                                    <%=obj[0]%>
                                </td>
                            </tr>
                            <%
                                    cnt++;
                                }
                                if(list.isEmpty()){
                                   out.print("<tr><td colspan='3' class='ff t-align-center' style='color:red;'>No Records Found</td></tr>");
                                }
                            %>
                        </table>
                <%}%>                    
                <%
                    if(isT1L1){
                        repId = creationService.findRepIdofTOR(tenderid, lotId,"TER");
                        if(repId!=null){
                %>
                <jsp:include page="T1L1Include.jsp">
                    <jsp:param name="reportId" value="<%=repId%>"/>                    
                    <jsp:param name="cnt" value="<%=cnty%>"/>
                </jsp:include>
                <%}}%>
            <%}else{%>
            <div class="inner-tableHead t_space">Criteria</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>
                        <th width="35%">Name of Bidder/Consultant</th>
                         <%for(Object[] data : ter2Criteria){%>
                         <th><%=data[1]%></th>
                         <%}%>
                    </tr>
                    <%
                        List<Object[]> finalSubmissionUser = creationService.getFinalSubmissionUserForTer2(tenderid,lotId,roundId,String.valueOf(evalCount));
                        List<Object[]> evalRepData = creationService.evalRepData(tenderid, lotId, stat, "TER2",userId,isSign,evalCount);
                       //0 tfs.userId,1 tcm.companyId,2 tcm.companyName,3 ttm.firstName,4 ttm.lastName
                           for(Object[] data : finalSubmissionUser){
                           String userBidder = null;
                           if(!data[1].toString().equals("1")){
                               userBidder = data[2].toString();
                           }else{
                                userBidder = data[3].toString()+" "+data[4].toString();
                           }
                    %>
                    <tr>
                        <td><%=userBidder%></td>
                        <%
                            for(Object[] data2 : ter2Criteria){
                              if(!data2[1].toString().equalsIgnoreCase("Evaluation Status")){
                        %>
                        <td class="t-align-center">
                            <%
                                String isYes = "";
                                String isNo = "";
                                String isNA = "";
                                String evalValue=null;
                                //0. et.userId,1. et.criteriaId,2. et.value
                                for(Object[] objects : evalRepData){
                                    boolean tempbool = data[0].toString().equals(objects[0].toString()) && data2[0].toString().equals(objects[1].toString());
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("yes")){
                                        isYes = "selected";
                                        evalValue = "Yes";
                                    }
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("no")){
                                        isNo = "selected";
                                        evalValue = "No";
                                    }
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("na")){
                                        isNA = "selected";
                                        evalValue = "NA";
                                    }
                                }
                                if(isView){
                                    out.print(evalValue);
                                }else{
                            %>
                             <select name="critValue" class="formTxtBox_1">
                                <option value="Yes_<%=data[0]%>_<%=data2[0]%>" <%=isYes%>>Yes</option>
                                <option value="No_<%=data[0]%>_<%=data2[0]%>" <%=isNo%>>No</option>
                                <option value="NA_<%=data[0]%>_<%=data2[0]%>" <%=isNA%>>NA</option>
                            </select>
                            <%}%>
                         </td>
                         <%
                            isYes=null;
                            isNo=null;
                            isNA=null;
                             evalValue=null;
                              }}
                          %>
                          <td class="t-align-center">
                          <%
                                String evalStatus = creationService.getBidderStatus(data[0].toString(), tenderid, lotId,evalCount);
                                if("Technically Unresponsive".equalsIgnoreCase(evalStatus)){
                                    out.print("Technically Non-responsive");
                                }else{
                                    out.print(evalStatus);
                                }
                          %>
                          </td>
                    </tr>
                    <%
                           }
                    %>
                </tbody></table>
                <%}%>
                <%if(!"y".equals(request.getParameter("config")) && isMEM){%>
                <%if(NoteofDissent!=null && (!NoteofDissent.isEmpty())){%>
            <div class="inner-tableHead t_space">Note of Dissent:</div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <th><%=repLabel.charAt(0)%>EC Member</th>
                    <th>Note of Dissent</th>
                </tr>
                <%for(SPCommonSearchDataMore nod : NoteofDissent){%>
                <tr>
                    <td><%=nod.getFieldName1()%></td>
                    <td><%=nod.getFieldName2()%></td>
                </tr>
                <%}%>
            </table>
            <%}%>
            <div class="c_t_space atxt_1" style="color: red;">
<!--                I do hereby declare and confirm that I have no business or other links to any of the competing Tenderers.<br />-->
                <br />
                The Evaluation Committee certifies that the examination and evaluation has followed the requirements of the Act, the Rules made there under and the terms and conditions of the prescribed Application, Tender or Proposal Document and that all facts and information have been correctly reflected in the Evaluation Report and, that no substantial or important information has been omitted. </div>

            <div class="inner-tableHead t_space"><%=repLabel.charAt(0)%>EC Members</div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <td class="tableHead_1" colspan="5" ><%=repLabel.charAt(0)%>EC Members</td>
                </tr>
                <tr>
                    <th width="20%" >Name</th>
                    <%
                    for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                        if(dataMore.getFieldName5().equals(userId)){
                         if(dataMore.getFieldName6().equals("-")){
                             String valChk="";
                             if(!isCommCP){
                                valChk = "&nDis=y";
                             }
                   %>
                    <a href="TORSigningApp.jsp?id=<%=dataMore.getFieldName7()%>&uid=<%=dataMore.getFieldName5()%>&tid=<%=tenderid%>&lotId=<%=lotId%>&stat=<%=stat%>&rId=<%=roundId%>&roundId=<%=rId%>&rpt=ter2<%=valChk%>">
                            <%=dataMore.getFieldName1()%>
                        </a>
                   <%
                        }else{
                             out.print(dataMore.getFieldName1());
                        }

                    }else{
                        out.print(dataMore.getFieldName1());
                    }
                        out.print("</td>");
                    }
                  %>
               </tr>
                <tr>
                    <th width="20%">Committee Role</th>
                    <%
                    int svpdfter2 = 0;
                    for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                            if (dataMore.getFieldName2().equals("cp")) {
                            out.print("Chairperson");
                        } else if (dataMore.getFieldName2().equals("ms")) {
                            out.print("Member Secretary");
                        } else if (dataMore.getFieldName2().equals("m")) {
                            out.print("Member");
                            }
                        out.print("</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Designation</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName3()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">PE Office</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName4()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Signed <%=repLabel%> Evaluation Report 2 On</th>
                    <%for(SPCommonSearchDataMore dataMore : TECMembers) {if(dataMore.getFieldName6().equals("-")){svpdfter2++;}out.print("<td>"+dataMore.getFieldName6()+"</td>");}%>
                </tr>
                 <tr>
                    <th width="20%">Comments</th>
                    <%for(SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName8()+"</td>");}%>
                </tr>
            </table>
                <%if(svpdfter2!=0){%>
                    <script type="text/javascript">
                        $('#svpdftor1').hide();
                    </script>
                <%}%>
                <%}%>
                <input type="hidden" value="<%=tenderid%>" name="tId">
                <input type="hidden" value="<%=lotId%>" name="lId">
                <input type="hidden" value="<%=stat%>" name="stat">
                <input type="hidden" value="TER2" name="rType">
                <input type="hidden" value="<%=roundId%>" name="rId">
                <input type="hidden" value="<%if(isCommCP){out.print("cp");}else{out.print("m");}%>" name="role">
                <%if(!isView && bole_ter_flag){%>
                <div class="t-align-center t_space">
                    <label class="formBtn_1" accesskey="">
                        <input type="submit" value="Save"/>
                    </label>
                </div>
                <%}%>
            </form>
        </div>
        </div>
            <%if(bole_ter_flag){%>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <% } %>
    </body>
    <%
    %>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
    $(document).ready(function() {

        $("#print").click(function() {
            //alert('sa');
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        //alert(options);
        $('#print').hide();
        $('#saveASPDF').hide();
        $('#goBack').hide();
        $('#print_area').printElement(options);
        $('#print').show();
        $('#saveASPDF').show();
        $('#goBack').show();
        //$('#trLast').hide();
    }

    </script>
</html>
