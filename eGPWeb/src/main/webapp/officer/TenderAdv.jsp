<%--
    Document   : TenderAdv
    Created on : Jan 1, 2011, 3:06:11 PM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import ="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Advertisement Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js">

            /***********************************************
             * All Levels Navigational Menu- (c) Dynamic Drive DHTML code library (http://www.dynamicdrive.com)
             * This notice MUST stay intact for legal use
             * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code//Please enter Newspaper Advertisement Date.
             ***********************************************/

        </script>

        <script type="text/javascript">

            $(document).ready(function(){
                $.validator.addMethod("webRegx", function(value, element) {
                        return this.optional(element) || /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\-\s]+$/.test(value);
                    }, "");
                $('#frmTenderAdv').validate({
                    rules:{
                        txtNewsPaper:{required:true,maxlength: 500},
                        txtNAdvDt:{required:true, TodayOrLessThenToday:"#tmp"},
                        txtWeb:{maxlength: 500},//webRegx: true,url:true
                        txtWAdvDt:{TodayOrLessThenToday:"#tmp"}
                    },

                    messages:{
                        txtNewsPaper:{required:"<div class='reqF_1'>Please enter the Name of the Newspaper</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 500 characters only</div>"
                        },
                        txtNAdvDt:{required:"<div class='reqF_1'>Please select the Newspaper Advertisement Date</div>",
                            TodayOrLessThenToday:"<div class='reqF_1'>Date must be smaller than or equal to the current date</div>"},
                        txtWeb:{//url:"<div class='reqF_1'>Please enter Website in www.xyz.com.</div>"},
                            maxlength:"<div class='reqF_1'>Allows maximum 500 characters only</div>"
                            //url:"<div class='reqF_1'>Please enter valid URL</div>"
                            //webRegx:"<div class='reqF_1'>Allows Characters,Numbers and Special character (Space,-)</div>"
                        },
                        txtWAdvDt:{TodayOrLessThenToday:"<div class='reqF_1'>Date must be smaller than or equal to the current date</div>"}
                    },

                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "txtNAdvDt")
                            error.insertAfter("#cal2");
                        else if (element.attr("name") == "txtWAdvDt")
                            error.insertAfter("#cal1");
                        else
                            error.insertAfter(element);
                    }

                });
            });

            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })              
            }

        </script>

    </head>
    <body>


        <%
            HandleSpecialChar handleSpecialChar = new HandleSpecialChar();

            if (request.getParameter("btnSubmit") != null && request.getParameter("tenderId") != null && request.getParameter("tenderId") != "")
            {
                String dtXml = "", creationDate = "", tenderId = "", tadId = "", cmdNm = "";
                boolean isUpdate = false;

                if (request.getParameter("action") != null)
                {
                    if (request.getParameter("action").equals("edit") && request.getParameter("tenderId") != "" && request.getParameter("tadId") != "")
                    {
                        isUpdate = true;
                    }
                }

                String dt = request.getParameter("txtNAdvDt");
                String webDt = request.getParameter("txtWAdvDt");

                String[] dtArr = dt.split("/");
                dt = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];

                if (!webDt.equalsIgnoreCase(""))
                {
                    String[] webDtArr = webDt.split("/");
                    webDt = webDtArr[2] + "-" + webDtArr[1] + "-" + webDtArr[0];
                }

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                if (isUpdate)
                {
                    cmdNm = "update";
                    tadId = request.getParameter("hdntadId");
                    tenderId = request.getParameter("hdntenderdId");
                    creationDate = request.getParameter("hdncDate");
                    String strtxtweb = request.getParameter("txtWeb");
                    dtXml = "<tbl_TenderAdvertisement tenderAdvtId=\"" + tadId + "\" tenderId=\"" + tenderId + "\" newsPaper=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtNewsPaper")) + "\" newsPaperPubDt=\"" + dt + "\" webSite=\"" + handleSpecialChar.handleSpecialChar(strtxtweb) + "\" webSiteAdvtDt=\"" + webDt + "\" creationDate=\"" + creationDate + "\" />";
                }
                else
                {
                    cmdNm = "insert";
                    tenderId = request.getParameter("tenderId");

                    creationDate = format.format(new Date());
                    dtXml = "<tbl_TenderAdvertisement tenderId=\"" + tenderId + "\" newsPaper=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtNewsPaper")) + "\" newsPaperPubDt=\"" + dt + "\" webSite=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtWeb")) + "\" webSiteAdvtDt=\"" + webDt + "\" creationDate=\"" + creationDate + "\" />";
                }

                //dtXml = "<tbl_TenderAdvertisement tenderId=\"" + tenderId + "\" newsPaper=\"" + request.getParameter("txtNewsPaper") + "\" newsPaperPubDt=\"" + dt + "\" webSite=\"" + request.getParameter("txtWeb") + "\" webSiteAdvtDt=\"" + webDt + "\" creationDate=\"" + creationDate + "\" />";
                dtXml = "<root>" + dtXml + "</root>";


                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP(cmdNm, "tbl_TenderAdvertisement", dtXml, "").get(0);
                //out.println(commonMsgChk.getMsg());

                // Coad added by Dipal for Audit Trail Log.
                AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                String idType = "tenderId";
                int auditId = Integer.parseInt(request.getParameter("tenderId"));
                String auditAction = null;
                String moduleName = EgpModule.Evaluation.getName();
                String remarks = "";
                if(isUpdate)
                {
                     auditAction = "Edit Advertisement";
                }
                else
                {
                     auditAction = "Add New Advertisement";
                }

                if (commonMsgChk.getFlag() == true)
                {
                    if (isUpdate)
                    {
                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                        response.sendRedirect("TenderAdvList.jsp?tenderId=" + tenderId + "&msgId=updated");
                    }
                    else
                    {
                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                        response.sendRedirect("TenderAdvList.jsp?tenderId=" + tenderId + "&msgId=created");
                    }
                    
                }
                else
                {
                    auditAction = "Error in "+auditAction;
                    makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                    response.sendRedirect("TenderAdvList.jsp?tenderId=" + tenderId + "&msgId=error");
                }
                

            }

        %>


        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->

            <form  id="frmTenderAdv" action="TenderAdv.jsp?tenderId=<%=request.getParameter("tenderId")%>&action=<%=request.getParameter("action")%>&tadId=<%=request.getParameter("tadId")%>" method="POST">

                <!--Dashboard Content Part Start-->
               
                    <div class="pageHead_1">Tender Advertisement Details<span style="float: right;"><a class="action-button-goback" href="TenderAdvList.jsp?tenderId=<%=request.getParameter("tenderId")%>" >Go Back to Dashboard</a></span></div>
                    <div>&nbsp;</div>              
                    <%
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <%pageContext.setAttribute("tab", "12");%>
                    <div class="borderDivider t_space" >&nbsp;</div>
                    <%@include file="officerTabPanel.jsp" %>

                    <div class="tabPanelArea_1">
                        <div style="font-style: italic" class="t-align-left">
                        <b>Fields marked with (<span class="mandatory">*</span>) are mandatory</b>
                        </div>
                         <div class="tabPanelArea_1">



                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                            <%  List<SPTenderCommonData> list = null;
                                boolean isEdit = false;
                                if (request.getParameter("action") != null)
                                {
                                    if (request.getParameter("action").equals("edit") && request.getParameter("tenderId") != "" && request.getParameter("tadId") != "")
                                    {
                                        isEdit = true;


                                        String tenderAdvtId = "";
                                        tenderAdvtId = request.getParameter("tadId");

                                        tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                        list = tenderCommonService.returndata("getTenderAdDetail_ForEdit", tenderAdvtId, null);

                                    }
                                }
                            %>
                            <%if (isEdit)
                        {%>
                            <input type="hidden" name="hdntadId" id="hdntadId" value="<%=list.get(0).getFieldName1()%>" />
                            <input type="hidden" name="hdntenderdId" id="hdntenderdId" value="<%=list.get(0).getFieldName6()%>" />
                            <input type="hidden" name="hdncDate" id="hdncDate" value="<%=list.get(0).getFieldName7()%>" />
                            <%}%>
                            <tr>
                                <td class="ff" width="22%">Name of the Newspaper : <span class="mandatory">*</span></td>
                                <td width="28%">
                                    <input <%if (isEdit)
                                {%>value="<%=list.get(0).getFieldName2()%>"<%}%> name="txtNewsPaper" type="text" maxlength="501" class="formTxtBox_1" id="txtNewsPaper" style="width:250px;" /></td>

                                <td width="22%" class="ff">Newspaper Advertisement Date : <span>*</span></td>
                                <td width="35%">
                                    <input <%if (isEdit)
                                {%>value="<%=list.get(0).getFieldName3()%>"<%}%> name="txtNAdvDt" type="text" class="formTxtBox_1" id="txtNAdvDt" style="width:100px;" readonly="true" onclick ="GetCal('txtNAdvDt','txtNAdvDt');"/>
                                    <img id="cal2" src="../resources/images/Dashboard/calendarIcn.png" alt="Select A Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtNAdvDt','cal2');"/>
                                </td>
                            </tr>
                            <tr>
                                <td  class="ff">Full URL of Advertisement Page : </td>
                                <td >
                                    <input <%if (isEdit)
                                {
                                    if ("".equals(list.get(0).getFieldName4()))
                                    {%>value =""<%}
                            else
                            {%>value="<%=list.get(0).getFieldName4()%>"<%}
                                }%>  name="txtWeb" type="text" class="formTxtBox_1" id="txtWeb" style="width:250px;" maxlength="501" onblur="blockvalidate()"/><span id="webspan" class="reqF_1" ></span></td>
                                <td class="ff">Website Advertisement Date : </td>
                                <td><input <%if (isEdit)
                                    {
                                        if (list.get(0).getFieldName5().equals("01/01/1900"))
                                        {
                                        %>value =""<%                                            }
                                            else
                                            {%>value="<%=list.get(0).getFieldName5()%>"<%}
                                            }%> name="txtWAdvDt" type="text" class="formTxtBox_1" id="txtWAdvDt" style="width:100px;" readonly="true" onclick ="GetCal('txtWAdvDt','txtWAdvDt');"/>
                                    <img id="cal1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select A Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtWAdvDt','cal1');"/> <span id="webAdvspan" class="reqF_1" ></span></td>
                            </tr>
                            <tr>
                                <td colspan="4" class="t-align-center">
                                    <label class="formBtn_1 t_space">
                                        <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit" onclick="return validate()"/>
                                    </label>
                                </td>
                            </tr>
                            <%if (isEdit)
                    {%>
                            <input type="hidden" name="hdntadId" id="hdntadId" value="<%=list.get(0).getFieldName1()%>" />
                            <input type="hidden" name="hdntenderdId" id="hdntenderdId" value="<%=list.get(0).getFieldName6()%>" />
                            <input type="hidden" name="hdncDate" id="hdncDate" value="<%=list.get(0).getFieldName7()%>" />
                            <%}%>
                        </table>
                        </div>
                    </div>
            </form>
        </div>
        <!--Dashboard Content Part End-->
        <!--Dashboard Footer Start-->
        <div align="center">
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
        <!--Dashboard Footer End-->
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <script type="text/javascript">
        function validate()
        {
            var flag = true;
            if(document.getElementById("txtWAdvDt").value!="")
            {
                if(document.getElementById("txtWeb").value=="")
                {
                    document.getElementById("webspan").innerHTML= "Please enter Website name";
                    flag = false;
                }else{
                    // var tomatch= /^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)/
                    var tomatch = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/
                     if (!tomatch.test(document.getElementById("txtWeb").value))
                     {
                        flag = false;
                        document.getElementById("webspan").innerHTML = "Please enter valid URL";
                     }
                }
            }
            if(document.getElementById("txtWeb").value!="")
            {
                if(document.getElementById("txtWAdvDt").value=="")
                {
                    document.getElementById("webAdvspan").innerHTML= "Please enter Website Advertisement date";
                    flag = false;
                }
            }
            if(document.getElementById("txtWAdvDt").value!="")
            {
                document.getElementById("webAdvspan").innerHTML="";
            }
            return flag;
        }
    </script>
    <script type="text/javascript">
        function blockvalidate()
        {
            if(document.getElementById("txtWeb").value!="")
            {
                document.getElementById("webspan").innerHTML="";
            }
        }
    </script>
</html>
