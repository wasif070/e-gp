<%-- 
    Document   : ViewCTerminationWFFile
    Created on : Aug 31, 2011, 6:28:44 PM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.CmsCTReasonTypeBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsCTReasonType"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsContractTermination"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsContractTerminationService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Accounts Officer Payments Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
    </head>
    <body>
        <%
            String userId = "";
            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = session.getAttribute("userId").toString();
            }
            String tenderId = "";
            if(request.getParameter("tenderId")!=null)
            {

                tenderId = request.getParameter("tenderId");
                pageContext.setAttribute("tenderId", tenderId);
            }
            String lotId = "";                      
            String childid = "";
            if(request.getParameter("childid")!=null)
            {
                childid = request.getParameter("childid");
            }
            CmsContractTerminationService cmsContractTerminationService =
            (CmsContractTerminationService) AppContext.getSpringBean("CmsContractTerminationService");
            List<Object>  list = cmsContractTerminationService.getLotID(Integer.parseInt(childid));
            if(!list.isEmpty())
            {
                lotId = list.get(0).toString();
                pageContext.setAttribute("lotId", lotId);
            }
            
            String ContractTerminationId="";
            CmsCTReasonTypeBean cmsCTReasonTypeBean = new CmsCTReasonTypeBean();
            cmsCTReasonTypeBean.setLogUserId(userId);
            List<TblCmsCTReasonType> cmsCTReasonTypeList = cmsCTReasonTypeBean.getAllCmsCTReasonType();
            List<Object> getTerminationDetails = cmsContractTerminationService.getContractTerminationDetails(Integer.parseInt(lotId));
            if(getTerminationDetails.get(0)!=null)
            {
                ContractTerminationId = getTerminationDetails.get(0).toString();
            }
            TblCmsContractTermination tblCmsContractTermination = cmsContractTerminationService.getCmsContractTermination(Integer.parseInt(ContractTerminationId));

        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%--<%@include  file="../resources/common/AfterLoginTop.jsp"%>--%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1 t_space">
                <div class="pageHead_1">Contract Details</div>

                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <div class="pageHead_1 t_space">Contract Termination Details</div>
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1" >
                        <tr id="tblRow_dateOfTermination">
                            <td class="ff" width="15%">Date of Termination : </td>
                            <td><%=DateUtils.customDateFormate(DateUtils.convertStringtoDate(tblCmsContractTermination.getDateOfTermination().toString(),"yyyy-MM-dd HH:mm:ss"))%></td>
                        </tr>
                        <tr id="tblRow_typeOfReason">
                            <td class="ff">Type of Reason : </td>
                            <td>
                                <%
                                    String[] reasonTypes = tblCmsContractTermination.getReasonType().split("\\^");
                                    for (String reasonTypeId : reasonTypes) {
                                        int reasonId = Integer.parseInt(reasonTypeId);
                                        for (TblCmsCTReasonType cmsCTReasonType : cmsCTReasonTypeList) {
                                            if (cmsCTReasonType.getCtReasonTypeId() == reasonId) {
                                                out.print(cmsCTReasonType.getCtReason() + "<br>");
                                                break;
                                            }
                                        }
                                    }
                                %>
                            </td>
                        </tr>
                        <tr id="tblRow_workFlowStatus">
                            <td class="ff">Status : </td>
                            <td>
                                <%= tblCmsContractTermination.getStatus()%>
                            </td>
                        </tr>                        
<!--                        <tr id="tblRow_releasePerformanceGuarantee" >
                            <td class="ff">Release Performance Security : </td>
                            <td>
                                <a%if(tblCmsContractTermination.getReleasePg()!=null && !"".equalsIgnoreCase(tblCmsContractTermination.getReleasePg())){%>
                                <a%= tblCmsContractTermination.getReleasePg()%>
                                <a%}else{%>
                                -
                                <a%}%>
                            </td>
                        </tr>-->
                        <tr id="tblRow_reason">
                            <td  class="ff">Reason For Termination : </td>
                            <td>
                                <%= tblCmsContractTermination.getReason()%>
                            </td>
                        </tr>
                    </table>
            </div>
        </div>

    </body>
</html>
