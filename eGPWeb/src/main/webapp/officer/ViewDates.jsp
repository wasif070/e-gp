<%--
    Document   : ViewDates
    Created on : Jul 22, 2011, 2:03:46 PM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Dates</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
        <script language="javascript">
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }

        </script>
        <%
                    ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");
        %>
        <script type="text/javascript">
            /* check if pageNO is disable or not */
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            /*  load Grid for the User Registration Report */
            function loadTable()
            {
                $('#tohide').hide();
                if($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/ConsolidateServlet", {tenderId: $("#tenderId").val(),size: $("#size").val(),pageNo: $("#first").val(),action:'view',wpId: $("#wpId").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            /*  Handle First click event */
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    $('#first').val(0);
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Last Button click event */
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var size = (parseInt($('#size').val())*totalPages)-(parseInt($('#size').val()));
                    $('#first').val(size);
                    if(totalPages>0){
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Next Button click event */
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val());
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first+max);

                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo <= totalPages) {

                        loadTable();
                        $('#pageNo').val(Number(pageNo)+1);
                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            /*  Handle Previous click event */
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first-max);
                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    $('#pageNo').val(Number(pageNo)-1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)-1);
                });
            });
        </script>
        <script type="text/javascript">
            function Search(){
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            }
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var size = (parseInt($('#size').val())*pageNo)-$('#size').val();
                    $('#first').val(size);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }

                });
            });
            function changetable(){
                var pageNo=parseInt($('#pageNo').val(),10);
                var first = parseInt($('#first').val());
                var max = parseInt($('#size').val());
                var totalPages=parseInt($('#totalPages').val(),10);
                loadTable();

            }

        </script>

        <%

                    int userid = 0;
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userid = Integer.parseInt(hs.getAttribute("userId").toString());
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                    String lotId = request.getParameter("lotId");
                    CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                    List<Object[]> list = cs.getLotDetailsByPkgLotId(lotId, request.getParameter("tenderId"));
        %>
    </head>
    <body onload="loadTable();">

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->

            <div  id="print_area">
                <div class="contentArea_1">
                    <div class="DashboardContainer">
                        <div class="pageHead_1">View Dates of Delivery Schedules
                            <span class="c-alignment-right noprint">
                                <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                                <%if (request.getParameter("noa") != null) {%>
                                <a href="NOA.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                                <%} else if (request.getParameter("RO") != null) {%>
                                <a href="repeatOrderMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                                <%} else {%>
                                <a href="DeliverySchedule.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                                <%}%>
                            </span>

                        </div>
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div>&nbsp;</div>
                        <form name="frm" action="<%=request.getContextPath()%>/ConsolidateServlet" method="post">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <%for (Object[] obj : list) {%>
                                <tr>
                                    <td width="20%">Lot No.</td>
                                    <td width="80%"><%=obj[0]%></td>
                                </tr>
                                <tr>
                                    <td>Lot Description</td>
                                    <td class="t-align-left"><%=obj[1]%></td>
                                </tr>
                                <%}%>
                            </table>

                            <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>" />
                            <input type="hidden" name="wpId" id="wpId" value="<%=request.getParameter("wpId")%>" />
                            <input type="hidden" name="action" value="view" />
                            <div id="resultDiv" style="display: block;">
                                <div  id="print_area">
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                        <tr>
                                            <%if (procnature.equalsIgnoreCase("goods")) {%>
                                            <%
                                             TenderTablesSrBean beanCommon = new TenderTablesSrBean();
                                             String tenderType = beanCommon.getTenderType(Integer.parseInt(request.getParameter("tenderId")));

                                            %>
                                            <th width="3%" class="t-align-center">S.No</th>
                                            <th width="20%" class="t-align-center"><%=bdl.getString("CMS.desc")%></th>
                                            <th width="10%" class="t-align-center"><%=bdl.getString("CMS.UOM")%>
                                            </th>
                                            <th width="9%" class="t-align-center"><%=bdl.getString("CMS.qty")%>
                                           <%
                                              if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                             {
                                            %>
                                            <th width="9%" class="t-align-center"><%=bdl.getString("CMS.rateICT")%>
                                            <th width="8%" class="t-align-center ICT">Currency
                                            </th>
                                            <!--
                                            <th width="10%" class="t-align-center">Supplier VAT (in BTN)
                                            </th> -->
                                             <th width="15%" class="t-align-center ICT">Supplier VAT + Other Local Cost (in Nu.)
                                             </th>
                                            <% } else {%>
                                             <th width="9%" class="t-align-center"><%=bdl.getString("CMS.rate")%>
                                            <% } %>
                                            <th width="8%" class="t-align-center">No. of Days
                                            <th width="10%" class="t-align-center">Delivery Date
                                            </th>
                                            <%} else {%>
                                            <th width="3%" class="t-align-center">S.No</th>
                                            <th width="8%" class="t-align-center">Group</th>
                                            <th width="20%" class="t-align-center">Description</th>
                                            <th width="15%" class="t-align-center">Unit<br />
                                                of Measurement
                                                <br />
                                            </th>
                                            <th width="10%" class="t-align-center">Qty
                                            </th>
                                            <th width="18%" class="t-align-center">Start Date
                                            </th>
                                            <th width="18%" class="t-align-center">End Date
                                            </th>
                                            <%}%>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                            <div id="tohide">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                    <tr>
                                        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            Total Records per page : <select name="size" id="size" onchange="changetable();" style="width:40px;">
                                                <option value="1000" selected>10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>

                                            </select>
                                        </td>
                                        <td align="center"><input name="textfield3" type="text" id="dispPage" onKeydown="javascript: if (event.keyCode==13) Search();" value="1" class="formTxtBox_1" style="width:20px;" />
                                            &nbsp;
                                            <label class="formBtn_1">
                                                <input type="submit" name="button"  id="btnGoto" value="Go To Page" />
                                            </label></td>
                                        <td  class="prevNext-container">
                                            <ul>
                                                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <input type="hidden" id="pageNo" value="1"/>
                            <input type="hidden" id="first" value="0"/>
                            <br />

                        </form>
                    </div>

                    <!--Dashboard Content Part End-->
                </div></div>
            <!--Dashboard Footer Start-->
            <%                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Delivery_Schedule.getName(), "View Dates", ""); %>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->

        </div>

    </body>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#print").click(function() {
                printElem({ leaveOpen: true, printMode: 'popup' });
            });

        });
        function printElem(options){
            $('#print_area').printElement(options);
        }
    </script>
    <script>
        var headSel_Obj = document.getElementById("headTabReport");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

