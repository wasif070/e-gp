<%-- 
    Document   : TECReport
    Created on : Feb 14, 2011, 5:33:26 PM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Evaluation Committee Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
    </head>
    <body>
        <div class="dashboard_div">

            <%  String tenderId, strSt = "";
                tenderId = request.getParameter("tenderId");
                        strSt = request.getParameter("st");
                        String sentBy = "", role = "", strGovUserId="0";
                        String aaName = "", aaUserId = "", aaGovUserId="0";
                        boolean isSentToAA = false;
                        if (session.getAttribute("userId") != null) {
                  sentBy = session.getAttribute("userId").toString();
                        }

                        if (session.getAttribute("govUserId") != null) {
                            strGovUserId = session.getAttribute("govUserId").toString();
                        }


                CommonSearchService commonSearchService1 = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                        List<SPTenderCommonData> lstgetAAName = tenderCS.returndata("getTenderConfigData", tenderId, null);
                        if (!lstgetAAName.isEmpty()) {
                            aaName = lstgetAAName.get(0).getFieldName2();
                            aaUserId = lstgetAAName.get(0).getFieldName7();                            
                        }
                        lstgetAAName = null;

                        List<SPTenderCommonData> lstgetAAGovUserId = tenderCS.returndata("getGovUserIdFromUserId", aaUserId, null);
                        if(!lstgetAAGovUserId.isEmpty()){
                            aaGovUserId = lstgetAAGovUserId.get(0).getFieldName1();
                }

                        List<SPTenderCommonData> lstgetSentToAAStatus = tenderCS.returndata("getSentToAAEntry", tenderId, null);
                        if (!lstgetSentToAAStatus.isEmpty()) {
                            if ("yes".equalsIgnoreCase(lstgetSentToAAStatus.get(0).getFieldName1())) {
                                isSentToAA = true;
                    }
                }
                        lstgetSentToAAStatus = null;

         if (request.getParameter("btnSentToAA") != null) {
             String redirectURL = "";
                            String xmldata = "";
                            String remark = "";
                            String strTenRefNo="";
             HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
             SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                            if(request.getParameter("hdnTenderRefNo")!=null){
                                strTenRefNo = request.getParameter("hdnTenderRefNo");
                            }

             remark = request.getParameter("txtComments");
             remark = handleSpecialChar.handleSpecialChar(remark);

                            xmldata = "<tbl_EvalRptSentToAA evalRptId=\"0\" userId=\"" + aaUserId + "\" sentBy=\"" + sentBy + "\" tenderId=\"" + tenderId + "\" remarks=\"" + remark + "\" sentDate=\"" + format.format(new Date()) + "\" aaGovUserId=\"" + aaGovUserId + "\" sentByGovUserId=\"" + strGovUserId + "\" />";
             xmldata = "<root>" + xmldata + "</root>";

             CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
             CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalRptSentToAA", xmldata, "").get(0);

             if (commonMsgChk.getFlag().equals(true)) {
                                /* START : CODE TO SEND MAIL TO AAPROVING AUTHORITY */
                                String mailSubject = "e-GP: Approval of Evaluation Report";
                                String strFrmEmailId = "", strPEOffice = "";

                                List<SPTenderCommonData> frmEmail = tenderCS.returndata("getEmailIdfromUserId", sentBy, null);
                                strFrmEmailId = frmEmail.get(0).getFieldName1();


                                List<SPTenderCommonData> emails = tenderCS.returndata("getEmailIdfromUserId", aaUserId, null);

                                List<SPTenderCommonData> lstPEInfo = tenderCS.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
                                if (!lstPEInfo.isEmpty()) {
                                    strPEOffice = lstPEInfo.get(0).getFieldName3();
                                }

                                UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

                                String[] mail = {emails.get(0).getFieldName1()};
                                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                MailContentUtility mailContentUtility = new MailContentUtility();
                                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                String mailText = mailContentUtility.contEvalApprovalNeeded(tenderId, strTenRefNo, strPEOffice);

                                sendMessageUtil.setEmailTo(mail);
                                sendMessageUtil.setEmailSub(mailSubject);
                                sendMessageUtil.setEmailMessage(mailText);

                                userRegisterService.contentAdmMsgBox(mail[0], strFrmEmailId, HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contEvalApprovalNeeded(tenderId, strTenRefNo, strPEOffice));
                                //sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailId"));
                                sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                                sendMessageUtil.sendEmail();

                                sendMessageUtil = null;
                                mailContentUtility = null;
                                mail = null;
                                msgBoxContentUtility = null;
                                /* END : CODE TO SEND MAIL TO AAPROVING AUTHORITY */

                                //frmEmail = null;
                                emails = null;
                                lstPEInfo = null;

                                mailSubject = null;
                                strFrmEmailId = null;
                                strPEOffice = null;
                                xmldata = null;
                                remark = null;

                 redirectURL = "TECReport.jsp?tenderId=" + tenderId + "&st=" + strSt + "&msgId=senttoaa";
             } else {
                 redirectURL = "TECReport.jsp?tenderId=" + tenderId + "&st=" + strSt + "&msgId=error";
             }

             response.sendRedirect(redirectURL);
         }

                // IF ENTRY FOUND IN "tbl_EvalSentQueToCp" TABLE IN CASE OF "tec" MEMBER.
                // GET THE TEC-MEMBER COUNT.
                List<SPTenderCommonData> isTEC = tenderCS.returndata("IsTEC", tenderId, sentBy);
                int isTECCnt = isTEC.size();
                        isTEC = null;

                // CHECK FOR USER-ROLL AND SET THE LINK AS PER THE ROLL.
                List<SPCommonSearchData> roleList = commonSearchService1.searchData("EvalMemberRole", tenderId, sentBy, null, null, null, null, null, null, null);

                        if (!roleList.isEmpty()) {
                    role = roleList.get(0).getFieldName2();
                }
                        roleList = null;

            %>
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <form id="frmEvalClari" name="frmTECRepoort" action="TECReport.jsp?tenderId=<%=tenderId%>&st=<%=strSt%>" method="post">
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Tender Evaluation Committee Report</div>
                <%
                        pageContext.setAttribute("tenderId", tenderId);

                        String tenderid = tenderId;
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
                    <%if (!"TEC".equalsIgnoreCase(request.getParameter("comType"))) {%>
                <jsp:include page="officerTabPanel.jsp" >
                     <jsp:param name="tab" value="7" />
                </jsp:include>
                    <% }%>
                <div class="tabPanelArea_1">
                        <%-- Start: CODE TO DISPLAY MESSAGES --%>
                 <%if (request.getParameter("msgId") != null) {
                    String msgId = "", msgTxt = "";
                    boolean isError = false;
                    msgId = request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")) {
                        if (msgId.equalsIgnoreCase("senttoaa")) {
                            msgTxt = "Sent to AA successfully.";
                                            } else if (msgId.equalsIgnoreCase("error")) {
                            isError = true;
                            msgTxt = "There was some error.";
                        } else {
                             msgTxt = "";
                        }
                    %>
                    <%if (isError) {%>
                        <div class="responseMsg errorMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                    <%} else if (!"".equalsIgnoreCase(msgTxt)) {%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                    <%}%>
                        <%
                         }
                                    }
                    %>
                        <%-- End: CODE TO DISPLAY MESSAGES --%>

                        <% List<SPTenderCommonData> companyList = tenderCS.returndata("getFinalSubComp", tenderId, "0");%>

                      <%
                        if (!companyList.isEmpty()) {
                            if ("cp".equalsIgnoreCase(role)) {
                                 if (!"cl".equalsIgnoreCase(request.getParameter("st"))) {
                                     List<SPTenderCommonData> evalMemStatusCount = tenderCS.returndata("GetEvalMemfinalStatusCount", tenderId, "0");
                            // IF THIS COUNT MATCHED WITH BIDDERS COUNT THEN SHOW BELOW BUTTON
                            if (Integer.parseInt(evalMemStatusCount.get(0).getFieldName1()) == companyList.size()) {
                        %>

                        <table width="100%" cellspacing="0" class="tableList_1">
                          <tr>
                              <th  width="4%" class="t-align-center">Sl. No.</th>
                              <th  width="96%" class="t-align-center">Report Name</th>
                          </tr>
                          <tr>
                              <td class="t-align-center">1</td>
                              <td>
                                  <a href="TenderersCompRpt.jsp?tenderid=<%=tenderId%>&st=<%=request.getParameter("st")%>">Technical Evaluation Report</a>
                              </td>
                          </tr>
                          <tr>
                              <td class="t-align-center">2</td>
                              <td>
                                  <a href="ViewL1Report.jsp?tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>">View L1 Report </a>
                              </td>
                          </tr>
                      </table>


                     <%
                                                        if (!isSentToAA) {
                    %>
                     <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                          <tr>
                                <td style="font-style: italic" class="ff t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>

                             <tr>
                                <td class="ff">Approving Authority : </td>
                                <td>
                                    <%=aaName%>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff">Remarks : <span class="mandatory">*</span></td>
                                <td>
                                    <textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea>
                                    <span id="SpError" class="reqF_1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <div class="t-align-left">
                                        <label class="formBtn_1"><input name="btnSentToAA" id="btnSentToAA" type="submit" value="Send Report to AA" /></label>
                                    </div>

                                </td>
                            </tr>

                     </table>
                    <script type="text/javascript" >
            $(document).ready(function(){
                $('#btnSentToAA').click(function(){
                    var flag=true;

                     if($.trim($('#txtComments').val())==''){
                            $('#SpError').html('Please enter Remarks.');
                            flag=false;
                        }
                        else {
                            if($('#txtComments').length<=1000) {
                                $('#SpError').html('');
                            }
                            else{
                                $('#SpError').html('Maximum 1000 characters are allowed.');
                                flag=false;
                            }
                        }

                         if(flag==false){
                            return false;
                        }

                });

                $('#txtComments').blur(function(){
                    var flag=true;

                     if($.trim($('#txtComments').val())==''){
                            $('#SpError').html('Please enter Remarks.');
                            flag=false;
                        }
                        else {
                            if($('#txtComments').length<=1000) {
                                $('#SpError').html('');
                            }
                            else{
                                $('#SpError').html('Maximum 1000 characters are allowed.');
                                flag=false;
                            }
                        }

                         if(flag==false){
                            return false;
                        }
                });

            });
        </script>

                    <%}%>
                      <%
                                }
                            } // END IF "EVAL. REPORT" TAB
                                        } // END IF "CP" ROLE
                       } // END IF OF "NO DATA FOUND"
%>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
                    <input type="hidden" id="hdnTenderRefNo" name="hdnTenderRefNo" value="<%=toextTenderRefNo%>">
        </form>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->

            <%
// Nullify String variables and List objects
                        tenderId = null;
                        strSt = null;
                        sentBy = null;
                        role = null;
            %>

        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
