<%-- 
    Document   : ContractAgreement
    Created on : Jan 8, 2011, 3:23:12 PM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.model.table.TblNoaIssueDetails"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd"> -->

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
        <title>Contract Agreement</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
        <jsp:useBean id="contractSignDtBean" scope="request" class="com.cptu.egp.eps.web.databean.ContractSignDtBean"/>
        <jsp:setProperty name="contractSignDtBean" property="*"/>

 <script type="text/javascript">
        function setPubAgg(obj){
            //isPubAggOnWeb
            if(document.getElementById('checkbox').checked){
                document.getElementById('isPubAggOnWeb').value = 'Yes';
            }else{
                document.getElementById('isPubAggOnWeb').value = 'No';
            }
        }
    </script>
    <script type="text/javascript">
        function setPaymentTerms(obj){
            document.getElementById("paymentTerms").value = obj.value;
        }
    </script>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
        <script type="text/javascript">
            function CompareToForToday(first)
            {
                var mdy = first.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split
                var mdyhrtime=mdyhr[1].split(':');
                if(mdyhrtime[1] == undefined){
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                }else
                {
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                }

                var d = new Date();
                if(mdyhrtime[1] == undefined){
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                }
                else
                {
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
                }
                return Date.parse(valuedate) >= Date.parse(todaydate);
            }
            function required(){
                jAlert("Atleast one witness required.","Contract Agreement", function(RetVal) {
                });
                return false;
            }
            function msgForAddress(){
                jAlert("Please select Witness to be removed.","Contract Agreement", function(RetVal) {
                });
                return false;
            }
            function clrMsgLoc(){
                if(document.getElementById('txtPlaceSigAgr').value != ''){
                    document.getElementById('valPlace').innerHTML ='';
                }
            }
            /*function clrMsgDate(){
                if(document.getElementById('txtDateSigAgr').value != ''){
                    if(CompareToForToday(document.getElementById('txtDateSigAgr').value))
                    {
                        document.getElementById('valDateSigAgr').innerHTML='Date of Signing of Agreement should not be greater than the Current Date and Time';
                    }else{
                        document.getElementById('valDateSigAgr').innerHTML ='';
                    }
                }
            }*/
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
        <script type="text/javascript">
            function required(controlid)
            {
                var temp=controlid.length;
                if(temp <= 0 ){
                    return false;
                }else{
                    return true;
                }
            }

            $(function() {
                $("#addRow").click(function(){
                    var counter = document.getElementById("txtcounter").value;
                    //var newTxt = '<tr><td class="t-align-left"><input class="formTxtBox_1" type="checkbox" name="chk'+ counter+ ' " id="chk'+ counter+ ' "/></td><td class="t-align-left"><input style="width: 95%;" name="refNo_'+ counter+ '" type="text" class="formTxtBox_1" id="txtrefNo_'+ counter+ '" onBlur="chkRefNoBlank(this);"/><span id="refno_'+ counter+ '" style="color: red;">&nbsp;</span></td><td class="t-align-left"><textarea cols="50" style="width: 98%;" rows="5" id="txtaphasingService_'+ counter+ '" name="phasingService_'+ counter+ '" class="formTxtBox_1" onBlur="chkPhaseSerBlank(this);"></textarea><span id="phaseSer_'+ counter+ '" style="color: red;">&nbsp;</span></td><td class="t-align-left"><input style="width: 95%;" name="locationRefNo_'+ counter+ '" type="text" class="formTxtBox_1" id="txtlocationRefNo_'+ counter+ '" onBlur="chkLocRefBlank(this);"/><span id="locRef_'+ counter+ '" style="color: red;">&nbsp;</span></td><td class="t-align-left"><input name="indicativeStartDate_'+ counter+'" type="text" class="formTxtBox_1" id="txtindicativeStartDate_'+ counter+'" style="width:70px;" readonly="true"  onfocus="GetCalWithouTime(\'txtindicativeStartDate_'+ counter+'\',\'txtindicativeStartDate_'+ counter+'\');" onBlur="chkIndStartBlank(this);"/><img id="txtindicativeStartDateimg_'+ counter+'" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick="GetCalWithouTime(\'txtindicativeStartDate_'+ counter+'\',\'txtindicativeStartDateimg_'+ counter+'\');"/><span id="indStart_'+ counter+ '" style="color: red;">&nbsp;</span></td><td class="t-align-left"><input name="indicativeComplDate_'+ counter+'" type="text" class="formTxtBox_1" id="txtindicativeComplDate_'+ counter+'" style="width:70px;" readonly="true" onfocus="GetCalWithouTime(\'txtindicativeComplDate_'+ counter+'\',\'txtindicativeComplDate_'+ counter+'\');" onBlur="chkIndCompBlank(this);"/><img id="txtindicativeComplDateimg_'+ counter+'" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCalWithouTime(\'txtindicativeComplDate_'+ counter+'\',\'txtindicativeComplDateimg_'+ counter+'\');" /><span id="indComp_'+ counter+ '" style="color: red;">&nbsp;</span></td></tr>';
                    var newTxt = '<tr><td width="5%" class="t-align-left"><input class="formTxtBox_1" type="checkbox" name="chk'+ counter+ ' " id="chk'+ counter+ ' "/><textarea onBlur="chkValAdd(this);" style="width: 500px;" cols="50" rows="5" id="txtNameAddressWit_'+ counter+ '" name="nameAddressWit_'+ counter+ '" class="formTxtBox_1" ></textarea><span id="witVal_'+ counter+ '" style="color: red;">&nbsp;</span></td></tr>';
                    $("#dataTable").append(newTxt);
                    document.getElementById("txtcounter").value = ++counter;
                });
            });

            $(function() {
                $("#delRow").click(function(){
                    var count = 0;
                    $(":checkbox[checked='true']").each(function(){
                        if(document.getElementById("txtcounter")!= null && document.getElementById("txtcounter").value != 1){
                            var curRow = $(this).parent('td').parent('tr');
                            curRow.remove();
                            var tmp = document.getElementById("txtcounter").value;
                            document.getElementById("txtcounter").value = --tmp;
                        }else{
                            required();
                        }
                        count++;
                    });
                    if(count == 0){
                        msgForAddress();
                    }
                });
            });

            function chkVal(){
                document.getElementById("boolcheck").value = 'true'
                if(document.getElementById('txtDateSigAgr') != null){
                    if(document.getElementById('txtDateSigAgr').value == ''){
                        document.getElementById('valDateSigAgr').innerHTML = 'Please select Date of Signing of Agreement.';
                        document.getElementById("boolcheck").value = 'false';
                    }else if(CompareToForToday(document.getElementById('txtDateSigAgr').value))
                    {
                        document.getElementById('valDateSigAgr').innerHTML='Date of Signing of Agreement should not be greater than the Current Date and Time';
                        document.getElementById("boolcheck").value = 'false';
                    }else{
                        document.getElementById('valDateSigAgr').innerHTML = '';
                    }
                }

                if(document.getElementById('txtPlaceSigAgr') != null){
                    if(document.getElementById('txtPlaceSigAgr').value == ''){
                        document.getElementById('valPlace').innerHTML = '<br/>Please enter Place of Signing Agreement.';
                        document.getElementById("boolcheck").value = 'false';
                    }else{
                        document.getElementById('valPlace').innerHTML = '';
                    }
                }
                if(document.getElementById('manUpload').value==0){
                    document.getElementById('valUpload').innerHTML = 'Please upload at least one document';
                    document.getElementById("boolcheck").value = 'false';
                }else{
                    document.getElementById('valUpload').innerHTML = '';
                }
                var flag = document.getElementById('txtcounter').value;
                $(".dynamicError").remove();
                $('textArea[id^=txtNameAddressWit_]').each(function(){
                    if($.trim(CKEDITOR.instances[$(this).attr("id")].getData()) == "" || isCKEditorFieldBlank($.trim(CKEDITOR.instances[$(this).attr("id")].getData().replace(/<[^>]*>|\s/g, '')))){
                        $(this).parent().append("<div class='dynamicError' style='color:red'>Please enter the Name and Address of Witness.</div>");
                        $(this).focus();
                        document.getElementById("boolcheck").value = 'false';
                    }
                });
                /*for(var i=0;i<4;i++){
                    alert(CKEDITOR.instances.txtNameAddressWit_+i);
                    if(!required($.trim(CKEDITOR.instances.txtNameAddressWit_+i.getData())) || isCKEditorFieldBlank($.trim(CKEDITOR.instances.txtNameAddressWit_+i.getData().replace(/<[^>]*>|\s/g, '')))){
                    //if(document.getElementById('txtNameAddressWit_'+i).value == ''){
                        document.getElementById("witVal_"+i).innerHTML = "<br/>Please enter the Name and Address of Witness.";
                        //return false;
                        document.getElementById("boolcheck").value = 'false';
                    }else{
                        document.getElementById("witVal_"+i).innerHTML = "";
                    }
                }*/

                //var cmbNameAdd = document.getElementById('cmbNameAdd').value;
                /*var cmbNameAdd = '';
                for(var i=0;i<flag;i++){
                    if(document.getElementById('txtNameAddressWit_'+i) != null){
                        //alert(document.getElementById('txtNameAddressWit_'+i).value);
                        cmbNameAdd += document.getElementById('txtNameAddressWit_'+i).value;
                        cmbNameAdd = cmbNameAdd + '@';
                    }
                }
                document.getElementById('witnessInfo').value = cmbNameAdd.substring(0,cmbNameAdd.length - 1);*/
                if(document.getElementById("boolcheck").value == 'false'){
                    return false;
                }else{
                    if(document.getElementById("checkbox").checked){
                        jConfirm('Contract information will be published in e-GP System and GPPMD website, so please make sure the contract has been already signed and information is correct before you click Submit button', 'Contract Agreement', function (ans){
                        if(!ans){
                            return false;
                        }else{
                            var formObj = document.getElementById('frmContractAgreement');
                            var obj = document.getElementById('conSubmit');
                            $('#conSubmit').removeAttr("disabled");
                            obj.click();
                            //formObj.submit();
                        }
                    });
                    }else{
                        alert("Please select Publish Agreement on website");
                        return false;
//                        jConfirm('Please make sure the contract has been already signed and information is correct before you click Submit button', 'Contract Agreement', function (ans){
//                        if(!ans){
//                            return false;
//                        }else{
//                            var formObj = document.getElementById('frmContractAgreement');
//                            var obj = document.getElementById('conSubmit');
//                            $('#conSubmit').removeAttr("disabled");
//                            obj.click();
//                            //formObj.submit();
//                        }
//                    });
                    }
                    
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#frmContractAgreement').submit(function() {
                    if($('#frmContractAgreement').valid()){
                        if($('#Submit')!=null){
                            $('#Submit').attr("disabled","true");
                            $('#hdnbutton').val("Submit");
                        }
                    }
                });
            });


        </script>
    </head>
    <%
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }

                int noaIssueId = 0;
                if (request.getParameter("noaIssueId") != null) {
                    noaIssueId = Integer.parseInt(request.getParameter("noaIssueId"));
                }

                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    //userId = Integer.parseInt(session.getAttribute("userId").toString());
                    userId = session.getAttribute("userId").toString();
                    issueNOASrBean.setLogUserId(userId);
                }
                if ("Submit".equals(request.getParameter("hdnbutton"))) 
                {
                     MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                     ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                     int conId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                         
                    if (issueNOASrBean.addTblContractSign(contractSignDtBean, request.getParameterValues("nameAddressWit"),tenderId)) 
                    {
                            
                        if(request.getParameter("isRO")!=null) // For repeat Order contract signing
                        {
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), conId, "contractId", EgpModule.Repeat_Order.getName(), "Contract Sign For Repeat Order", "");
                            response.sendRedirect("repeatOrderMain.jsp?tenderId=" + tenderId);
                        }
                        else
                        {
                            // Code added by Dipal For Make audit trail entry at first time contract agreemetn
                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(tenderId), "tenderId", EgpModule.Contract_Signing.getName(), "Contract Sign by PE", "User Id:"+session.getAttribute("userId")+" has sign contract for Tender Id: "+tenderId);
                            response.sendRedirect("NOAListing.jsp?tenderId=" + tenderId);
                        }

                    }
                    else
                    {
                        if(request.getParameter("isRO")!=null) // For repeat Order contract signing
                        {
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), conId, "contractId", EgpModule.Repeat_Order.getName(), "Error in Contract Sign For Repeat Order", "");
                        }
                        else
                        {
                            // Code added by Dipal For Make audit trail entry at first time contract agreemetn
                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()),  Integer.parseInt(tenderId), "tenderId", EgpModule.Contract_Signing.getName(), "Error in Contract Sign by PE", "User Id:"+session.getAttribute("userId")+" has sign contract for Tender Id: "+tenderId);
                        } 
                    }
                }
                //dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat simpl = new SimpleDateFormat("dd-MMM-yyyy");
    %>
    <body>
        <!--        <div class="mainDiv">-->
        <!--            <div class="fixDiv">-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">
            <div class="pageHead_1">Contract  Agreement
                <span class="c-alignment-right">
                    <%if(request.getParameter("isRO")!=null){%>
                    <a href="repeatOrderMain.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back </a>
                    <%}else{%>
                    <a href="NOAListing.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
                    <%}%>
                </span>
            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div style="font-style: italic" class="formStyle_1 t-align-left ff t_space" >
                  	Fields marked with (<span class="mandatory">*</span>) are mandatory
            </div>
            <form id="frmContractAgreement" name="frmContractAgreement" action="ContractAgreement.jsp?noaIssueId=<%=noaIssueId %>&tenderId=<%=tenderId %>" method="post">
                <%if(request.getParameter("isRO")!=null){%>
                <input type="hidden" name="isRO" value="<%=request.getParameter("isRO")%>" />
                <%}%>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                    <tr>
                        <td align="left" valign="middle" class="ff">Last Date of Signing of Agreement :</td>
                        <% List<TblNoaIssueDetails> details = issueNOASrBean.getDetailsCS(noaIssueId);%>
                        <td align="left" valign="middle"><%=simpl.format(details.get(0).getContractSignDt())%>
                            <input type="hidden" value="<%=dateFormat1.format(details.get(0).getContractSignDt())%>" name="lastDtContractDtString" id="lastDtContractDtString"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Date of Signing of Agreement : <span class="mandatory">*</span></td>
                        <td align="left" valign="middle">
                            <input name="contractSignDtString" type="text" onfocus="GetCal('txtDateSigAgr','txtDateSigAgr');" class="formTxtBox_1" id="txtDateSigAgr" style="width:100px;" readonly="true" />
                            <img name="dateSigAgrImg" id="dateSigAgrImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" onclick ="GetCal('txtDateSigAgr','dateSigAgrImg');" border="0" style="vertical-align:middle;" />
                            <div id="valDateSigAgr" class="mandatory"></div>
                        </td>
                    </tr>
                    <!--                    <tr>
                                            <td colspan="2" style="text-align: right;">
                                                <a class="action-button-add" id="addRow">Add More Witnesses</a>
                                                <a class="action-button-delete" id="delRow">Remove Witnesses</a>
                                            </td>
                                        </tr>-->
                    <tr>
                        <td align="left" valign="middle" class="ff">Witnesses  Name and Address From PE: <span class="mandatory">*</span></td>
                        <td align="left" valign="middle">
                            <table width="100%" id="dataTable" cellpadding="0" cellspacing="0" class="formStyle_1">
                                <tr>

                                    <td class="t-align-left" width="95%">
                                        <textarea style="width: 500px;"  cols="50" rows="5" id="txtNameAddressWit_0" name="nameAddressWit" class="formTxtBox_1" style="width:40%;"></textarea>
                                        <script type="text/javascript">
                                          //  <![CDATA[
                                            CKEDITOR.replace( 'txtNameAddressWit_0',
                                            {
                                                toolbar : "egpToolbar"

                                            });
                                          //  ]]>
                                        </script>
                                        <span id="witVal_0" class='reqF_1'></span>
                                    </td>
                                </tr>
                                <tr>

                                    <td class="t-align-left" width="95%">
                                        <textarea style="width: 500px;"  cols="50" rows="5" id="txtNameAddressWit_1" name="nameAddressWit" class="formTxtBox_1" style="width:40%;"></textarea>
                                        <script type="text/javascript">
                                            //<![CDATA[
                                            CKEDITOR.replace( 'txtNameAddressWit_1',
                                            {
                                                toolbar : "egpToolbar"

                                            });
                                            //]]>
                                        </script>
                                        <span id="witVal_1" class='reqF_1'></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Witnesses  Name and Address From Bidder/Consultant: <span class="mandatory">*</span></td>
                        <td align="left" valign="middle">
                            <table width="100%" id="dataTable" cellpadding="0" cellspacing="0" class="formStyle_1">
                                <tr>

                                    <td class="t-align-left" width="95%">
                                        <textarea style="width: 500px;"  cols="50" rows="5" id="txtNameAddressWit_2" name="nameAddressWit" class="formTxtBox_1" style="width:40%;"></textarea>
                                        <script type="text/javascript">
                                            //<![CDATA[
                                            CKEDITOR.replace( 'txtNameAddressWit_2',
                                            {
                                                toolbar : "egpToolbar"

                                            });
                                            //]]>
                                        </script>
                                        <span id="witVal_2" class='reqF_1'></span>
                                    </td>
                                </tr>
                                <tr>

                                    <td class="t-align-left" width="95%">
                                        <textarea style="width: 500px;"  cols="50" rows="5" id="txtNameAddressWit_3" name="nameAddressWit" class="formTxtBox_1" style="width:40%;"></textarea>
                                        <script type="text/javascript">
                                            //<![CDATA[
                                            CKEDITOR.replace( 'txtNameAddressWit_3',
                                            {
                                                toolbar : "egpToolbar"

                                            });
                                            //]]>
                                        </script>
                                        <span id="witVal_3" class='reqF_1'></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Place of Signing Agreement : <span class="mandatory">*</span></td>
                        <td align="left" valign="middle">
                            <input name="contractSignLocation" type="text" class="formTxtBox_1" id="txtPlaceSigAgr" style="width:200px;" onblur="clrMsgLoc();" />
                            <span class="mandatory" id="valPlace"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Publish Agreement on website : <span class="mandatory">*</span></td>
                        <td align="left" valign="middle"><input type="checkbox" onclick="setPubAgg(this);" name="checkbox" id="checkbox" />
                            <label for="checkbox"></label>
                            <input type="hidden" value="2" id="txtcounter" name="txtcounter"/>
                            <input type="hidden" value="<%=noaIssueId%>" id="noaId" name="noaId"/>
                            <input type="hidden" value="<%=userId%>" id="createdBy" name="createdBy"/>
                            <input type="hidden" id="boolcheck" value="true"/>
                            <input type="hidden" id="isPubAggOnWeb" name="isPubAggOnWeb" value="No"/>
                            <input type="hidden" name="witnessInfo" id="witnessInfo" value=""/>
                             <%if(request.getParameter("isRO")!=null){%>
                            <input type="hidden" name="isRepeatOrder" id="isRepeatOrder" value="yes"/>
                            <%}else{%>
                            <input type="hidden" name="isRepeatOrder" id="isRepeatOrder" value="no"/>
                            <%}%>
                            <input type="hidden" name="paymentTerms" id="paymentTerms" value="allitem100p"/>
                        </td>
                    </tr>
                    <%
                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                        String procnature = commonService.getProcNature(tenderId).toString();
                        boolean isServices = false;
                        if(procnature.equalsIgnoreCase("services")){
                            isServices = true;
                        }
                    %>
                    <tr <% if(isServices){ out.print(" style=\"display:none\" "); } %> >
                        <td width="22%" align="left" valign="middle" class="ff">Payment Terms for Contract : <span class="mandatory">*</span></td>
                        <td width="78%" align="left" valign="middle">
                            <input type="radio" name="rdoPaymentTerms" id="rdoPaymentTermsA" value="allitem100p" checked onclick="setPaymentTerms(this);" />&nbsp;All Quantities 100 Percent<br/>
                            <input type="radio" name="rdoPaymentTerms" id="rdoPaymentTermsB" value="itemwise100p" onclick="setPaymentTerms(this);" />&nbsp;Item wise 100 Percent<br/>
                            <input type="radio" name="rdoPaymentTerms" id="rdoPaymentTermsC" value="anyitemanyp" onclick="setPaymentTerms(this);" />&nbsp;Any Item any Percent
                        </td>
                    </tr>

                    <tr>
                        <td width="22%" align="left" valign="middle" class="ff">Upload Contract Agreement : <span class="mandatory">*</span></td>
                        <td width="78%" align="left" valign="middle"><a onclick="javascript:window.open('<%=request.getContextPath()%>/officer/ContractSignDoc.jsp?noaIssueId=<%=noaIssueId%>&tenderId=<%=tenderId%>', '', 'width=800px,height=650px,scrollbars=1','');" href="javascript:void(0);">Upload / Remove</a>
                            <div class="reqF_1" id="valUpload"></div>
                        </td>
                    </tr>
                </table>
                <div id="dataDoc">
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="4%" class="t-align-center">Sl. No.</th>
                            <th class="t-align-center" width="23%">File Name</th>
                            <th class="t-align-center" width="32%">File Description</th>
                            <th class="t-align-center" width="7%">File Size <br />
                                (in KB)</th>
                            <th class="t-align-center" width="18%">Action</th>

                        </tr>
                        
                        <%

                                    int docCnt = 0;
                                    TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    /*Listing of data*/
                                    for (SPTenderCommonData sptcd : tenderCS.returndata("contractSignDoc", String.valueOf(noaIssueId), null)) {
                                        docCnt++;
                        %>
                        <tr>
                            <td class="t-align-left"><%=docCnt%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                            <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                            <td class="t-align-left">
                                <a href="<%=request.getContextPath()%>/ServletContractSignDoc?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&noaIssueId=<%=noaIssueId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            </td>

                        </tr>

                        <%   if (sptcd != null) {
                                            sptcd = null;
                                        }
                                    }%>
                        <input type="hidden" name="manUpload" id="manUpload" value="<%=docCnt%>" >
                        <% if (docCnt == 0) {%>
                        <tr>
                            <td colspan="5" class="t-align-center">No records found.</td>
                        </tr>
                        <%}%>
                    </table>
                </div>
                <div class="t_space t-align-center">
                    <label class="formBtn_1 t-align-center">
                        <input name="conSubmit" type="submit" id="conSubmit" style="display: none;" disabled onclick="return true;"/>
                        <input name="Submit" type="button" onclick="return chkVal();" id="Submit" value="Submit" />
                    </label>
                    <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                </div>
            </form>
        </div>
        <!--            </div>-->
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        <!--        </div>-->
           
    </body>

</html>
<%
            issueNOASrBean = null;
            contractSignDtBean = null;
%>
