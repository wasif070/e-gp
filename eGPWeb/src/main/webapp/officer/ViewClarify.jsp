<%--
Document   : ViewClarify
Created on : Apr 22, 2011, 12:55:40 PM
Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderOpening"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<%
response.setHeader("Expires", "-1");
response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
response.addHeader("Cache-Control", "post-check=0, pre-check=0");
response.setHeader("Pragma", "no-cache");
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List of Tenderers/Consultants for seeking clarification View Query / Clarification</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="dashboard_div">

<%
boolean isQueSentToCP = false, isEvalNotificationGiven = false;
String tenderId;
tenderId = request.getParameter("tenderId");
String sentBy = "", role = "", committeetype = "", strEvalStatus = "evaluation";
String xmldata = "";
String remark = "";
String strTeamSelUserId = "0";
CommonSearchService commonSearchService1 = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

if (session.getAttribute("userId") != null) {
sentBy = session.getAttribute("userId").toString();
}

// IF ENTRY FOUND IN "tbl_EvalSentQueToCp" TABLE IN CASE OF "tec/pec" MEMBER.
// GET THE TEC-MEMBER COUNT.
List<SPTenderCommonData> lstIsTEC =
tenderCS.returndata("IsTEC", tenderId, sentBy);

int isTECCnt = lstIsTEC.size();

lstIsTEC = null;

// CHECK FOR USER-ROLL AND SET THE LINK AS PER THE ROLL.
List<SPCommonSearchData> lstRole =
commonSearchService1.searchData("EvalMemberRole", tenderId, sentBy, null, null, null, null, null, null, null);

if (!lstRole.isEmpty()) {
role = lstRole.get(0).getFieldName2();
committeetype = lstRole.get(0).getFieldName3();
}

lstRole = null;

// Get the UserId of the Evaluation Member selected for doing Evaluation
List<SPTenderCommonData> lstTeamSelUserId =
tenderCS.returndata("getEvalTeamSelectedMember", tenderId, null);
if (!lstTeamSelUserId.isEmpty()) {
strTeamSelUserId = lstTeamSelUserId.get(0).getFieldName1();
}
lstTeamSelUserId = null;

// Get info whether Questions sent to Chaieperson or not
List<SPTenderCommonData> getQueSenttoCPStatus =
tenderCS.returndata("getQueSenttoCPStatus", tenderId, strTeamSelUserId);
if (!getQueSenttoCPStatus.isEmpty()) {
if ("Yes".equalsIgnoreCase(getQueSenttoCPStatus.get(0).getFieldName1())) {
isQueSentToCP = true;
}

if ("Yes".equalsIgnoreCase(getQueSenttoCPStatus.get(0).getFieldName2())) {
isEvalNotificationGiven = true;
}
}
getQueSenttoCPStatus = null;

// Get Tender Procurement Nature info
String strProcurementNature = "";
List<SPTenderCommonData> lstProcurementNature =
tenderCS.returndata("getTenderProcurementNature", tenderId, null);
if (!lstProcurementNature.isEmpty()) {
strProcurementNature = lstProcurementNature.get(0).getFieldName1();
}
lstProcurementNature = null;
 String strComType = "null";
                            if (request.getParameter("comType") != null && !"null".equalsIgnoreCase(request.getParameter("comType"))) {
                                strComType = request.getParameter("comType");
                            }

 String strST = "";
                        if(request.getParameter("st") != null)
                        {
                            strST = request.getParameter("st");
                        }

%>
<!--Dashboard Header Start-->
<div class="topHeader">
<%@include file="../resources/common/AfterLoginTop.jsp" %>
</div>
<form id="frmEvalClari" name="frmEvalClari" action="Evalclarify.jsp?tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>&comType=<%=strComType%>" method="post">
<!--Dashboard Header End-->
<!--Dashboard Content Part Start-->
<div class="contentArea_1">
<div class="pageHead_1">List of Tenderers/Consultants for seeking clarification</div>
<%
pageContext.setAttribute("tenderId", tenderId);
%>
<%@include file="../resources/common/TenderInfoBar.jsp" %>
<div>&nbsp;</div>

<%//if (showOfficerTab) {%>
<jsp:include page="officerTabPanel.jsp" >
<jsp:param name="tab" value="7" />
</jsp:include>
<%// }%>
<div class="tabPanelArea_1">
<%-- Start: CODE TO DISPLAY MESSAGES --%>
<%if (request.getParameter("msgId") != null) {
String msgId = "", msgTxt = "";
boolean isError = false;
msgId = request.getParameter("msgId");
if (!msgId.equalsIgnoreCase("")) {
if (msgId.equalsIgnoreCase("error")) {
isError = true;
msgTxt = "There was some error.";
} else {
// IF ROLL IS "CHAIR PERSON" (CP)
if ("cp".equalsIgnoreCase(role)) {
msgTxt = "Successfully sent to Bidder/Consultant.";
} else if (msgId.equalsIgnoreCase("nc")) {
msgTxt = "Successfully notified to Chairperson.";
} else {
msgTxt = "Successfully sent to Chairperson.";
}
}
%>
<%if (isError) {%>
<div class="responseMsg errorMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
<%} else {%>
<div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
<%}%>
<%}
}
%>
<%-- End: CODE TO DISPLAY MESSAGES --%>

<%
if ("rp".equalsIgnoreCase(request.getParameter("st"))) {
pageContext.setAttribute("TSCtab", "3");
} else {
pageContext.setAttribute("TSCtab", "4");
}
%>

<%@include file="../resources/common/AfterLoginTSC.jsp"%>
<div class="tabPanelArea_1">
<%
// CHECK COMMITTEE
if (!"".equalsIgnoreCase(committeetype)) {
// IF TEC Committe
if ("tec".equalsIgnoreCase(committeetype) || "pec".equalsIgnoreCase(committeetype)) {
// IF TEC CHAIRPERSON THEN SHOW BELOW LINKS
if ("cp".equalsIgnoreCase(role)) {
%>

<table width="100%" cellspacing="0" class="tableList_1 t_space">
<tr>
<th width="4%" class="t-align-center" height="21">Sl. No.</th>
<th width="36%" class="t-align-center" height="21">List of Tenderers/Consultants</th>
<th class="t-align-center" width="60%" height="21"><strong>Action</strong></th>
</tr>
<%
            List<SPTenderCommonData> companyList =
                    tenderCS.returndata("getFinalSubComp", tenderId, "0");
            
            if (!companyList.isEmpty()) {
                int cnt = 1;
                for (SPTenderCommonData companyname : companyList) {
                    // HIGHLIGHT THE ALTERNATE ROW
                    if (Math.IEEEremainder(cnt, 2) == 0) {
%>
<tr class="bgColor-Green">
<%                                                     } else {
%>
<tr>
<%                                                    }
%>
<td class="t-align-center" width="5%"><%=cnt%></td>
<td class="t-align-left" width="35%"><%=companyname.getFieldName3()%></td>
<td class="t-align-left" width="60%">
<%
            boolean isCPBidderEvalDone = false;
            boolean isBidderClariAsked = false;
            boolean isBidderClariGiven = false;

            List<SPCommonSearchData> lstCPBidderEvalChk =
                    commonSearchService1.searchData("getClarificationToCPLink", tenderId, companyname.getFieldName2(), strEvalStatus, null, null, null, null, null, null);
            if (!lstCPBidderEvalChk.isEmpty()) {
                isCPBidderEvalDone = true; // shows that evaluation CP has given finalize responsiveness process for this bidder
            }
            lstCPBidderEvalChk = null;


            List<SPCommonSearchData> lstBidderClariStatus =
                    commonSearchService1.searchData("GetEvalBidderClarificationStatus", tenderId, companyname.getFieldName2(), strEvalStatus, null, null, null, null, null, null);

            if (!lstBidderClariStatus.isEmpty()) {
                isBidderClariAsked = true;
                if ("Yes".equalsIgnoreCase(lstBidderClariStatus.get(0).getFieldName1())) {
                    isBidderClariGiven = true; // shows that biider has given his clarification
                } else {
                    isBidderClariGiven = false;
                }
            } else {
                isBidderClariAsked = false;
            }
            lstBidderClariStatus = null;


            if ("cl".equalsIgnoreCase(request.getParameter("st"))) {
                // Clarification Case
                List<SPTenderCommonData> evalBidderRespExist =
                        tenderCS.returndata("GetEvalBidderRespTenderId", tenderId, companyname.getFieldName2());

                // SET THE LINK OF "NO QUESTION" WHEN NO QUESTION FOUND FOR ANY BIDDER
                List<SPTenderCommonData> evalFormQuesCount =
                        tenderCS.returndata("GetEvalFormQuesCount", tenderId, companyname.getFieldName2());
                
                String questCount = evalFormQuesCount.get(0).getFieldName1();
                
                List<SPCommonSearchData> evalFormFinalQuesCount =
                    commonSearchService1.searchData("GetEvalFormFinalQuesCount", tenderId, companyname.getFieldName2(), strEvalStatus, sentBy, null, null, null, null, null);
                String strFnlQuestCount = evalFormFinalQuesCount.get(0).getFieldName1();
                
                
                if (!isEvalNotificationGiven && !isCPBidderEvalDone && !isBidderClariAsked && !isQueSentToCP && Integer.parseInt(questCount) == 0) {
                    // if (!isQueSentToCP) {
%>
&nbsp;&nbsp;<label>No Question Posted yet</label><br>
<%                                                                                        // }
            } else {
                // SHOW BELOW LINK IF - "EvalBidderResponse" EXISTS
                // If Questions Count > 0
                //if (evalBidderRespExist.isEmpty()) {
                if (!isCPBidderEvalDone) {
                    if (!isBidderClariAsked) {
%>

<%if (Integer.parseInt(strFnlQuestCount) == 0) {%>
&nbsp;&nbsp;<label>No Question Posted yet</label><br>
<%} else {%>
&nbsp;&nbsp;<a href="EvalClariPostBidder.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>&comType=<%=strComType%>&lnk=view">View Questions / Send Questions To Bidder/Consultant</a><br>
<%}%>
<%
                    } else {
%>
&nbsp;&nbsp;<label>Posted To Bidder/Consultant</label><br>
&nbsp;&nbsp;<a href="EvalViewQue.jsp?tenderId=<%=tenderId%>&uid=<%=companyname.getFieldName2()%>&st=<%=request.getParameter("st")%>&comType=<%=strComType%>&lnk=view">View Query / Clarification</a><br>
<%          }
                } else {
                    if (isBidderClariAsked) {
%>
&nbsp;&nbsp;<a href="EvalViewQue.jsp?tenderId=<%=tenderId%>&uid=<%=companyname.getFieldName2()%>&st=<%=request.getParameter("st")%>&comType=<%=strComType%>&lnk=view">View Query / Clarification</a><br>
<%
                        }
                    }
                }
%>

<%
                // VISIBLE BELOW LINK, IF RESPONSE TIME IS OVER
                if (Integer.parseInt(companyname.getFieldName4()) < 0) {
%>
<%
                if ("Services".equalsIgnoreCase(strProcurementNature)) {
                    // For Services Case
%>

<%
                List<SPTenderCommonData> lstEvalBidderStatus =
                        tenderCS.returndata("GetEvalBidderStatus", "tenderId=" + tenderId + " And userId=" + companyname.getFieldName2() + " And evalBy=" + strTeamSelUserId + " And evalCount=" + evalCount, null); //change by dohatec for re-evaluation
                //tenderCS.returndata("GetEvalBidderStatus", "tenderId=" + tenderId + " And userId=" + companyname.getFieldName2() + " And evalBy=" + session.getAttribute("userId").toString(), null);

                if (!lstEvalBidderStatus.isEmpty()) {
                    if ("Yes".equalsIgnoreCase(lstEvalBidderStatus.get(0).getFieldName1())) {                     
%>

&nbsp;&nbsp;<a href="EvalBidderMarks.jsp?tenderId=<%=tenderId%>&uId=<%=companyname.getFieldName2()%>&memUid=<%=strTeamSelUserId%>&comType=<%=strComType%>">View Member’s Evaluation</a><br>
<%
                }
                }

                lstEvalBidderStatus = null;
%>

<%} else {
                    // Good and Works Case

                                                 //                   List<SPCommonSearchData> lstClarificationToCP =
                                                //commonSearchService1.searchData("getClarificationToCPLink", tenderId, companyname.getFieldName2(), strEvalStatus, null, null, null, null, null, null);
                     boolean  isEvalMemBidderEvalDone = false;
                                                 List<SPTenderCommonData> lstMemBidderEvalStatus =
                                                    tenderCS.returndata("getEvaluateTenderer_linkStatusForTECMember", tenderId, strTeamSelUserId);

                                                    if(!lstMemBidderEvalStatus.isEmpty()){
                                                        if(Integer.parseInt(lstMemBidderEvalStatus.get(0).getFieldName1()) > 0){
                                                            isEvalMemBidderEvalDone = true;
                                                        }
                                                    }
                                                //



                    %>

                    <%if(isEvalMemBidderEvalDone){%>
                        &nbsp;&nbsp<a href="EvalViewStatus.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>">View Member’s Evaluation</a><br>
                    <%}%>

<%}%>

<%
                }
            } else {
                // REPORT CASE
%>
<%if ("Services".equalsIgnoreCase(strProcurementNature)) {%>
<%
List<SPCommonSearchData> lstClarificationToCP =
commonSearchService1.searchData("getClarificationToCPLink", tenderId, companyname.getFieldName2(), strEvalStatus, null, null, null, null, null, null);
if (lstClarificationToCP.isEmpty()) {
%>
&nbsp;&nbsp;<a href="EvalComMemberReport.jsp?tenderId=<%=tenderId%>&uId=<%=companyname.getFieldName2()%>&stat=eval&comType=<%=strComType%>">Finalize Responsiveness</a>
<%
                } else {
%>
&nbsp;&nbsp;Evaluated
<% }
lstClarificationToCP = null;
%>
<%} else {
List<SPCommonSearchData> lstClarificationToCP =
commonSearchService1.searchData("getClarificationToCPLink", tenderId, companyname.getFieldName2(), strEvalStatus, null, null, null, null, null, null);
if (lstClarificationToCP.isEmpty()) {
%>
&nbsp;&nbsp;<a href="EvalTenderer.jsp?userId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>&lnk=et&comType=<%=strComType%>">Finalize Responsiveness</a>
<%} else {%>
&nbsp;&nbsp;Evaluated
<%}%>
<%}%>


<%
                //  SHOW BELOW "Give Answer To CP" LINK, IF FOUND IN "tbl_EvalCpMemClarification"

                List<SPTenderCommonData> evalCPExistCount =
                        tenderCS.returndata("GetEvalCPClarificationLinkStatus", companyname.getFieldName2(), tenderId);
                if (!evalCPExistCount.isEmpty()) {
%>
<br>&nbsp;&nbsp;<a href="EvalanstoCP.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>&comType=<%=strComType%>">View Clarification By Member</a>
<%
                } // END IF

                // PUT COND. FOR HIDE BELOW LINK FROM - tbl_EvalBidderStatus - tenderid and all users

            } //   REPORT CASE ENDS
%>
</td>
</tr>
<%
    cnt++;
} //END FOR LOOP
} else {
%>
<tr>
<td class="t-align-left" colspan="3">
<b> No record found! </b>
</td>
</tr>
<% }%>
</table>

<%
        if ("Services".equalsIgnoreCase(strProcurementNature)) {
            if ("cl".equalsIgnoreCase(request.getParameter("st"))) {
%>
<%
List<SPTenderCommonData> lstAllConsultantEvalLinkStatus =
    tenderCS.returndata("getAllConsultantEvaluationLinkStatus", tenderId, sentBy);

if (!lstAllConsultantEvalLinkStatus.isEmpty()) {
if ("Yes".equalsIgnoreCase(lstAllConsultantEvalLinkStatus.get(0).getFieldName1())) {
%>
<table width="100%" cellspacing="0" class="tableList_1 t_space">
<tr>
<td style="width: 40%" class="t-align-left ff">
All Consultant Evaluation
</td>
<td class="t-align-left">
&nbsp;&nbsp;<a href="EvalServiceReport.jsp?tenderId=<%=tenderId%>&comType=<%=strComType%>">View All Consultant Evaluation Report</a>
</td>
</tr>
</table>
<%        }
}

lstAllConsultantEvalLinkStatus = null;
%>

<% } else if ("rp".equalsIgnoreCase(request.getParameter("st"))) {%>
<%
List<SPTenderCommonData> lstOverallTechScoreLinkStatus =
    tenderCS.returndata("getOverallTechScore_ForEvaluation", tenderId, null);

if (!lstOverallTechScoreLinkStatus.isEmpty()) {
if (!"0".equalsIgnoreCase(lstOverallTechScoreLinkStatus.get(0).getFieldName1())) {
    // Show this link only when all TEC members (except chairperson) have notified to the chairperson
%>
<table width="100%" cellspacing="0" class="tableList_1 t_space">
<tr>
<td style="width: 40%" class="t-align-left ff">
Overall Technical Evaluation Report
</td>
<td class="t-align-left">
&nbsp;&nbsp;<a href="TechScore.jsp?tenderId=<%=tenderId%>&comType=<%=strComType%>">View Overall Technical Evaluation Report</a>
</td>
</tr>
</table>
<%
                    }
                }

                lstOverallTechScoreLinkStatus = null;
            }
        }
%>

<%
        if (!"cl".equalsIgnoreCase(request.getParameter("st"))) {
            List<SPTenderCommonData> evalMemStatusCount =
                    tenderCS.returndata("GetEvalMemfinalStatusCount", tenderId, "0");
            // IF THIS COUNT MATCHED WITH BIDDERS COUNT THEN SHOW BELOW BUTTON
            if (Integer.parseInt(evalMemStatusCount.get(0).getFieldName1()) == companyList.size()) {
%>
<table width="100%" cellspacing="0" class="tableList_1 t_space">
<tr>
<th width="55%" class="t-align-center" height="21">Report</th>
<th class="t-align-center" width="45%" height="21"><strong>Action</strong></th>
</tr>
<tr>
<td>
Technical Evaluation Report
</td>
<td>
<a href="TenderersCompRpt.jsp?tenderid=<%=tenderId%>&st=<%=request.getParameter("st")%>&comType=<%=strComType%>">View</a>
</td>
</tr>

</table>

<table width="100%" cellspacing="0" class="tableList_1  t_space">
<tr>
<th  width="58%" class="t-align-center">Report Name</th>
<th  width="24%" class="t-align-center">Action</th>
</tr>
<%
String tendId = request.getParameter("tenderId");
ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
String evalType = creationService.getTenderEvalType(tendId);
String pgName = null;
if (evalType.equalsIgnoreCase("item wise")) {
    pgName = "ItemWiseReport";
} else {
    pgName = "TenderReport";
}
for (Object[] obj : creationService.getRepNameId(tendId)) {
%>
<tr>
<td><%=obj[1]%></td>
<td class="t-align-center"><a href="<%=pgName%>.jsp?tenderid=<%=tendId%>&repId=<%=obj[0]%>&isEval=y&comType=<%=strComType%>">Price Bid Comparative</a></td>
</tr>
<%}%>
</table>
    <jsp:include page="EvalProcessInclude.jsp">
        <jsp:param name="tenderid" value="<%=tenderId%>"/>
        <jsp:param name="stat" value="eval"/>
        <jsp:param name="pnature" value="<%=strProcurementNature%>"/>
        <jsp:param name="comtype" value="<%=strComType%>"/>
    </jsp:include>

<%
            }
        }
    } else {
        //IF TEC MEMBERS THEN SHOW BELOW LINKS
%>
<table width="100%" cellspacing="0" class="tableList_1">
<tr>
<th width="4%" class="t-align-center" height="21">Sl. No.</th>
<th width="36%" class="t-align-center" height="21">List of Tenderers/Consultants</th>
<th class="t-align-center" width="60%" height="21"><strong>Action</strong></th>
</tr>
<%
            List<SPTenderCommonData> companyList =
                    tenderCS.returndata("getFinalSubComp", tenderId, "0");
            if (!companyList.isEmpty()) {
                int cnt = 1;
                for (SPTenderCommonData companyname : companyList) {
                    // HIGHLIGHT THE ALTERNATE ROW
                    if (Math.IEEEremainder(cnt, 2) == 0) {
%>
<tr class="bgColor-Green">

<%                                                                                } else {
%>
<tr>
<%                                                                                }
%>
<td class="t-align-center" width="5%"><%=cnt%></td>
<td class="t-align-left" width="35%"><%=companyname.getFieldName3()%></td>
<td class="t-align-left" width="60%">
<%
            List<SPTenderCommonData> evalBidderRespExist =
                    tenderCS.returndata("GetEvalBidderRespTenderId", tenderId, companyname.getFieldName2());

            // SET THE LINK OF "NO QUESTION" WHEN NO QUESTION FOUND FOR ANY BIDDER
            List<SPTenderCommonData> evalFormQuesCount =
                    tenderCS.returndata("GetEvalFormQuesCount", tenderId, companyname.getFieldName2());
            String questCount = evalFormQuesCount.get(0).getFieldName1();

            if (Integer.parseInt(questCount) == 0) {
                if (!isQueSentToCP) {
%>
&nbsp;&nbsp;<label>No Question Posted yet</label><br>
<%                                                    }
            } else {
                // SHOW BELOW LINK IF - "EvalBidderResponse" EXISTS
                if (evalBidderRespExist.isEmpty()) {%>
&nbsp;&nbsp<a href="EvalViewQue.jsp?tenderId=<%=tenderId%>&uid=<%=companyname.getFieldName2()%>&st=<%=request.getParameter("st")%>&comType=<%=strComType%>&lnk=view">View Query / Clarification</a><br>
<%
            } else {
%>
&nbsp;&nbsp<label>Posted To Bidder/Consultant</label><br>
&nbsp;&nbsp<a href="EvalViewQue.jsp?tenderId=<%=tenderId%>&uid=<%=companyname.getFieldName2()%>&st=<%=request.getParameter("st")%>&comType=<%=strComType%>&lnk=view">View Query / Clarification</a><br>
<%
                }
            }
%>

<%
            // VISIBLE BELOW LINK, IF RESPONSE TIME IS OVER
            if (Integer.parseInt(companyname.getFieldName4()) < 0) {
%>
<%
                if ("Services".equalsIgnoreCase(strProcurementNature)) {
                List<SPTenderCommonData> lstEvalBidderStatus =
                        tenderCS.returndata("GetEvalBidderStatus", "tenderId=" + tenderId + " And userId=" + companyname.getFieldName2() + " And evalBy=" + strTeamSelUserId, null);

                if (!lstEvalBidderStatus.isEmpty()) {
                    if ("Yes".equalsIgnoreCase(lstEvalBidderStatus.get(0).getFieldName1())) {
%>
&nbsp;&nbsp<a href="EvalBidderMarks.jsp?tenderId=<%=tenderId%>&uId=<%=companyname.getFieldName2()%>&memUid=<%=strTeamSelUserId%>&comType=<%=strComType%>">View Member’s Evaluation</a><br>
<%
                    }
                }
                lstEvalBidderStatus = null;
%>
<%}%>


<%
            }
%>

</td>
</tr>
<%
    cnt++;
} //END FOR LOOP
} else {
%>
<tr>
<td class="t-align-left" colspan="3">
<b> No record found! </b>
</td>
</tr>
<% }%>
</table>

<%
        // All Consultant Evaluation
        if ("Services".equalsIgnoreCase(strProcurementNature)) {
            if ("cl".equalsIgnoreCase(request.getParameter("st"))) {
%>
<%
List<SPTenderCommonData> lstAllConsultantEvalLinkStatus =
    tenderCS.returndata("getAllConsultantEvaluationLinkStatus", tenderId, strTeamSelUserId);

if (!lstAllConsultantEvalLinkStatus.isEmpty()) {
if ("Yes".equalsIgnoreCase(lstAllConsultantEvalLinkStatus.get(0).getFieldName1())) {
%>

<table width="100%" cellspacing="0" class="tableList_1 t_space">
<tr>
<td width="40%" class="t-align-left ff">All Consultant Evaluation</td>
<td class="t-align-left">
&nbsp;&nbsp;<a href="EvalServiceReport.jsp?tenderId=<%=tenderId%>&comType=<%=strComType%>">View All Consultant Evaluation Report</a>
</td>
</tr>
</table>
<%
                    }
                }
                lstAllConsultantEvalLinkStatus = null;
            }
        }
%>

<%
    }//END TEC MEM

    // End :IF Code for TEC Committe
}

%>
<%} else {%>
<table width="100%" cellspacing="0" class="tableList_1">
<tr>
<td colspan="3">Not Authorised Member</td>
</tr>
</table>
<%}%>
</div>


</div>
<div>&nbsp;</div>
<!--Dashboard Content Part End-->
</div>
</form>
<!--Dashboard Footer Start-->
<jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
<!--Dashboard Footer End-->
</div>
</body>
<script type="text/javascript" language="Javascript">
var headSel_Obj = document.getElementById("headTabEval");
if(headSel_Obj != null){
headSel_Obj.setAttribute("class", "selected");
}

</script>
</html>
