<%-- 
    Document   : ReportCreation
    Created on : Dec 22, 2010, 10:59:10 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.web.servicebean.ReportCreationSrBean" %>
<jsp:useBean id="tblReportMaster" class="com.cptu.egp.eps.model.table.TblReportMaster"/>
<jsp:setProperty property="*" name="tblReportMaster"/>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
             $(document).ready(function() {
                $("#search").validate({
                    rules: {
                        reportName:{required:true,maxlength: 1000},
                        reportHeader:{/*required:true,*/maxlength: 1000},
                        reportFooter:{/*required:true,*/maxlength: 1000},
                        repCols:{required:true,digits:true,threetenchk:true}
                    },
                    messages: {
                         reportName:{required:"<div class='reqF_1'>Please enter Report Name.</div>",
                                     maxlength:"<div class='reqF_1'>Maximum 1000 Characters are allowed.</div>"},
                         reportHeader:{/*required:"<div class='reqF_1'>Please enter Report Header.</div>",*/
                                     maxlength:"<div class='reqF_1'>Maximum 1000 Characters are allowed.</div>"},
                         reportFooter:{/*required:"<div class='reqF_1'>Please enter Report Footer.</div>",*/
                                     maxlength:"<div class='reqF_1'>Maximum 1000 Characters are allowed.</div>"},
                         repCols:{required:"<div class='reqF_1'>Please enter No. of Columns.</div>",
                                     digits:"<div class='reqF_1'>Please enter numbers only.</div>",
                                     threetenchk:"<div class='reqF_1'>Minimum 3 and maximum 10 columns are allowed.</div>"}
                    }
                });
            });
        </script>
        <script type="text/javascript">
             $(function() {
                $('#search').submit(function() {
                    var vbool=false;
                    if($('#search').valid()){
                        vbool=true;
                        //below validation are placed in jquery.validate threetenchk
                        /*if($('#repCols').val()<3){
                            $('#colmsg').html('Minimum 3 columns are required.');
                            vbool=false;
                        }else if($('#repCols').val()>10){
                            $('#colmsg').html('Maximum 10 columns are allowed.');
                            vbool=false;
                        }*/
                        if(vbool){
                            //vbool = confirm("Report For and No. of Columns cannot be edited once clicked on Ok. Please confirm the details before proceeding next");
                            vbool = confirm("You can't modify 'No. of Columns' and 'Report For' once the report is prepared");
                        }
                    }else{
                        $('#colmsg').html(null);
                    }
                    return vbool;
                });
            });
            $(function() {
                    $( "#dialog:ui-dialog" ).dialog( "destroy" );
                    $( "#dialog-form" ).dialog({
                        autoOpen: false,
                        resizable:false,
                        draggable:false,
                        height: 500,
                        width: 700,
                        modal: true,
                        buttons: {
                         "OK": function() {
                                 $(this).dialog("close");
                         }},
                        close: function() {
                            $(this).dialog("close");
                        }
                    });
                    $("#sfblock").click(function(){
                        $("#dialog-form").dialog("open");
                    });
            });
        </script>
    </head>
    <body>
        <%
            String tenderid = request.getParameter("tenderid");
            ReportCreationSrBean rcsb = new ReportCreationSrBean();
            String labTORPOR="TOR";
            String labTERPER="TER";
            CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
            if(commonService.getProcNature(tenderid).toString().equalsIgnoreCase("services")){
                labTERPER = "PER";
                labTORPOR = "POR";
            }
            if("Next Step".equalsIgnoreCase(request.getParameter("next")))
            {                
                int repId = rcsb.reportCreation(tblReportMaster,request.getParameter("repCols"),request.getParameter("tenderid"));
                tblReportMaster=null;                
                response.sendRedirect("ReportColumns.jsp?tenderid="+tenderid+"&repId="+repId+"&reportType="+request.getParameter("isTorter"));
            }
            boolean isPackageWise=true;
            if(rcsb.getTenderEvalType(tenderid).equalsIgnoreCase("item wise")){
                isPackageWise=false;
            }
            boolean isTORDisp = true;
            CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
            List<SPCommonSearchDataMore> envDataMores = dataMore.geteGPData("GetTenderEnvCount", tenderid);
            if(envDataMores!=null && (!envDataMores.isEmpty())){
                if(envDataMores.get(0).getFieldName1().equals("2")){
                    isTORDisp = false;
                }
            }
            /*String procMethod = commonService.getProcMethod(tenderid).toString();
            String eventType = commonService.getEventType(tenderid).toString();
            if(eventType.equals("REOI") || eventType.equals("PQ") || eventType.equals("1 stage-TSTM")){
                isTORDisp = false;
            }*/
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <%                    
                    pageContext.setAttribute("tenderId", tenderid);
        %>
        <div class="pageHead_1">Create Report<span style="float: right;"><a class="action-button-goback" href="Notice.jsp?tenderid=<%=tenderid%>">Go back to Dashboard</a></span></div>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <form action="ReportCreation.jsp?tenderid=<%=tenderid%>" method="post" id="search" name="search">
            <input name="reportType" value="l1" type="hidden"/>
            <div class="tabPanelArea_1">
                <b style="color: red;"><br/>Instructions :</b>
                <ol type="a" style="list-style-type: lower-alpha;color: red;font-weight: bold;margin-left: 20px;line-height: 16px;margin-top: 12px;">
                    <li>Please must ensure that Grand Total of all BSR / Price Schedule/ Price Proposal forms (Form1, Form2, Form3 ...) is selected while preparing a formula except in case of LTM Works [e-PW2(B)].</li>
                    <!--<li>Once the report formula is prepared, please test it by entering numeric value and then save it.</li>-->
                    <li>In case of query, please contact GPPMD Office </li>
                    <li>
                        <span  class="ReadMore">
                            <a id="sfblock" href="javascript:void(0);" style="color: red;">Suggested Formula</a>
                        </span>
                    </li>
                </ol>
                <div class="t_space">
                    <b>Field Marked (<span class="mandatory">*</span>) is Mandatory</b>
                </div>                
                <table class="tableList_1 t_space" width="100%" cellspacing="0">
                    <tbody><tr>

                            <th colspan="2" class="t-align-left ff">Enter Report Details</th>
                        </tr>
                        <tr>
                            <td class="t-align-left ff" width="15%">Report Name : <span class="mandatory">*</span></td>
                            <td class="t-align-left" width="85%">
                                <input name="reportName" class="formTxtBox_1" id="textfield" style="width: 50%;" type="text">      </td>
                        </tr>

                        <tr>
                            <td class="t-align-left ff">Report Header : <!--span class="mandatory">*</span--></td>
                            <td class="t-align-left">
                                <textarea rows="5" id="textarea4" name="reportHeader" class="formTxtBox_1" style="width: 50%;"></textarea>
                            </td>
                        </tr>
                        <tr>

                            <td class="t-align-left ff">Report Footer : <!--span class="mandatory">*</span--></td>
                            <td class="t-align-left"><textarea rows="5" id="textarea5" name="reportFooter" class="formTxtBox_1" style="width: 50%;"></textarea></td>
                        </tr>
                        <!--<tr>
                            <td class="t-align-left ff">Report Type :</td>
                            <td class="t-align-left"><input name="reportType" id="radio" value="h1" type="radio">
                                <label for="radio">H1 </label>
                                <input name="reportType" id="radio" value="l1" class="l_space" type="radio" checked>
                                <label for="radio">L1 </label>

                            </td>
                        </tr>-->
                        <%
                            boolean isTOR=false;
                            boolean isTER=false;
                            List<SPCommonSearchDataMore> rptTORChk = dataMore.geteGPData("reportTORTERChk", tenderid,"tor2");
                            if(rptTORChk!=null && (!rptTORChk.isEmpty()) && rptTORChk.get(0).getFieldName1().equals("1")){
                                isTOR = true;
                            }
                            rptTORChk = dataMore.geteGPData("reportTORTERChk", tenderid,"ter3");
                            if(rptTORChk!=null && (!rptTORChk.isEmpty()) && rptTORChk.get(0).getFieldName1().equals("1")){
                                isTER = true;
                            }
                        %>
                        <tr>
                            <td class="t-align-left ff">Report For :</td>
                            <td class="t-align-left">
                                <input name="isTorter" id="radio" value="TER" type="radio" checked <%if(isTER){out.print("disabled");}%>><label for="radio"><%=labTERPER%> </label>
                                <%if(isTORDisp){%>
                                <input name="isTorter" id="radio" value="TOR" class="l_space" type="radio" <%if(isTOR){out.print("disabled");}%>><label for="radio"><%=labTORPOR%> </label>
                                <%}%>
                                <span style="font-weight: bold;float: right;"><%=labTORPOR%>: <%if(labTORPOR.equalsIgnoreCase("POR")){out.print("Proposal");}else{out.print("Tender");}%> Opening Report, <%=labTERPER%>: <%if(labTERPER.equalsIgnoreCase("PER")){out.print("Proposal");}else{out.print("Tender");}%> Evaluation Report</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">No. of Columns :<span class="mandatory">*</span></td>
                            <td class="t-align-left"><%if(isPackageWise){%><input name="repCols" class="formTxtBox_1" id="repCols" style="width: 10%;" type="text">
                                <%}if(!isPackageWise){%>3<input type="hidden" value="3" name="repCols" id="repCols"/><%}%>
                                <div id="colmsg" style="color: red;"></div>
                            </td>
                        </tr>
                        <%if(isTER || isTOR)%>
                        <tr>
                            <td>&nbsp;</td>
                            <td>                                
                                <%                                    
                                    if(isTER){out.print("<div class='responseMsg errorMsg'>Evaluation Report is already Signed. You can't prepare Price Comparison Report for Evaluation now.</div>");}
                                    else{if(isTOR && isTORDisp){out.print("<div class='responseMsg errorMsg'>Opening Report is already Signed. You can't prepare Price Comparison Report for Opening now.</div>");}}
                                %>
                            </td>
                        </tr>
                    </tbody></table>
                <div class="t-align-center t_space">
                    <label class="formBtn_1"><input name="next" id="button3" value="Next Step" type="submit" <%if(isTER){out.print("disabled");}%>></label>
                </div>

            </div>
            <div>&nbsp;</div>
        </form>
        <div id="dialog-form" title="Formula for Tender/Proposal Opening Report (TOR) & Tender/Proposal Evaluation Report (TER/PER)">
                    <fieldset>
                        <table width="100%" cellspacing="0" border="1" class="tableList_1">
                            <tr>
                                <th colspan="5">Formula for Tender Opening Report (TOR)</th>
                            </tr>
                            <tr>
                                <th width="5%">Sl. No</th>
                                <th width="25%">Name of Column to be shown in the Report</th>
                                <th width="15%">Data Type</th>
                                <th width="15%">Formula</th>
                                <th width="40%">Remarks</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Sl. No.</td>
                                <td>Auto Number</td>
                                <td></td>
                                <td>This column will be used to Display Rank as L1, L2, ...</td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Name of the Bidder</td>
                                <td>Company</td>
                                <td></td>
                                <td>Bidders/Suppliers/Contractors participated in the tender will be shown</td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Quoted Amount (in Nu.)(X)</td>
                                <td>Auto</td>
                                <td>Summation of Grand Total of Each Form</td>
                                <td>
                                    Grand Total of each Priced BSR or Schedule of Items will be used as input for Quoted Amount.<br/>
                                    For E.g. there are 3 Forms namely
                                    <ol type="1" style="list-style-type: decimal;margin-left: 20px;line-height: 16px;margin-top: 12px;margin-bottom: 5px;">
                                        <li>Price and Delivery Schedule (Form1)</li>
                                        <li>Price and Delivery Schedule_1 (Form2)</li>
                                        <li>Price and Completion Schedule for related Services (Form3)</li>
                                    </ol>
                                    The formula of Quoted Amount (in Nu.) = Form1_Grand Total + Form2_Grand Total + Form3_Grand Total
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" border="1" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5">Formula for Tender Evaluation Report (TER/PER)</th>
                            </tr>
                            <tr>
                                <th width="5%">Sl. No</th>
                                <th width="25%">Name of Column to be shown in the Report</th>
                                <th width="15%">Data Type</th>
                                <th width="15%">Formula</th>
                                <th width="40%">Remarks</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Rank</td>
                                <td>Auto Number</td>
                                <td></td>
                                <td>This column will be used to Display Rank as L1, L2, ...</td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Name of the Bidder/Consultant</td>
                                <td>Company</td>
                                <td></td>
                                <td>Bidders/Consultants participated in the tender will be shown</td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Official Cost Estimate (E)</td>
                                <td>Official Cost Estimate</td>
                                <td></td>
                                <td>Official Cost Estimate will be punched by PE</td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Quoted Amount (in Nu.) For ICB Tender Currency should be as decided by PE (X)</td>
                                <td>Auto</td>
                                <td>Summation of Grand Total of Each Form</td>
                                <td>
                                    Grand Total of each Priced BSR or Schedule of Items will be used as input for Quoted Amount.
                                    For E.g. there are 3 Forms namely
                                    <ol type="1" style="list-style-type: decimal;margin-left: 20px;line-height: 16px;margin-top: 12px;margin-bottom: 5px;">
                                        <li>Price and Delivery Schedule (Form1)</li>
                                        <li>Price and Delivery Schedule_1 (Form2)</li>
                                        <li>Price and Completion Schedule for related Services (Form3)</li>
                                    </ol>
                                    The formula of Quoted Amount (in Nu.) = Form1_Grand Total + Form2_Grand Total + Form3_Grand Total
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Deviation Amount (Y)</td>
                                <td>Auto</td>
                                <td>E-X</td>
                                <td>
                                    Official Cost Estimate - Quoted Amount
                                    <br/>
                                    Note : If the deviation amount is positive, then it is saving
                                 </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>% of Deviation (Z)</td>
                                <td>Auto</td>
                                <td>100 - X * 100 / E</td>
                                <td>Note : If the % of deviation is positive, then it is saving</td>
                            </tr>
                        </table>
                    </fieldset>
            </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
