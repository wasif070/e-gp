<%-- 
    Document   : DebarmentListing
    Created on : Jan 9, 2011, 3:29:04 PM
    Author     : TaherT
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Process Debarment Requests</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.generatepdf.js"  type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />               
        <script type="text/javascript">
                function fillGridOnEvent(type,email,cmpName,isSearch){
                    var queryString = '<%=request.getContextPath()%>/InitDebarment?p=1&q=1&action=fetchData&status='+type;
                    if(isSearch){
                        queryString = queryString + '&mailId='+email+'&cmpName='+cmpName;
                    }
                    $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                    jQuery("#list").jqGrid({
                        url : queryString,
                        datatype: "xml",
                        height: 250,
                        colNames:['Sl. No.','Company Name','Procuring Agency','Date and Time of Request',"Status","Action"],
                        colModel:[
                            {name:'srNo',index:'srNo', width:50,sortable:false,align:'center'},
                        {name:'companyName',index:'(CASE WHEN tcm.companyId = 1 THEN concat(tm.firstName,\' \',tm.lastName)  ELSE tcm.companyName END)', width:200},
                        {name:'procureEntity',index:'procureEntity', width:320,sortable:true},
                            {name:'dateRequest',index:'tdr.clarificationReqDt', width:160,align:'center'},
                        {name:'status',index:'tdr.debarmentStatus', width:170,sortable:true,align:'center'},
                            {name:'userid',index:'userid', width:50,sortable:false,align:'center'}
                        ],
                        multiselect: false,
                        paging: true,
                        rowNum:10,
                        rowList:[10,20,30],
                        pager: $("#page"),
                        sortable:false,
                        caption: " ",
                        gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                    }).navGrid('#page',{edit:false,add:false,del:false,search:false});
                }
                jQuery().ready(function (){
                    //fillGrid();
                });
       
                function test(id){
                    var isSearch = false;
                    var isValid = true;
                    if(id==0){
                        id = $("#tabvalue").val();
                        isSearch = true;
                    }else{
                        $('#emailId').val('');
                        $('#companyName').val('');
                    }
                    var email = $('#emailId').val();
                    var cmpName = $('#companyName').val();
                    if(isSearch){
                         $(".err").remove();
                         if($.trim($("#companyName").val()) == "") {
                             if($.trim($("#emailId").val()) == "") {
                                $("#emailId").parent().append("<div class='err' style='color:red;'>Please enter e-mail ID</div>");
                                isValid = false;
                             }
                             if($.trim($("#emailId").val()) != "") {
                                var mailTest = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
                                if(!mailTest.test($("#emailId").val())) {
                                    $("#emailId").parent().append("<div class='err' style='color:red;'>Please enter valid e-mail ID</div>");
                                    isValid = false;
                                }
                             }
                         }else{
                              if($.trim($("#companyName").val()) == "") {
                                $("#companyName").parent().append("<div class='err' style='color:red;'>Please enter Company Name</div>");
                                isValid = false;
                             }
                         }
                    }
                    if(isValid){
                        if(id==1){
                            document.getElementById("linkPending").className = "sMenu";
                            document.getElementById("linkProccessed").className = "";
                            $("#tabvalue").val('1');
                            fillGridOnEvent('in (\'pending\',\'withpe\') and tdr.debarmentBy=<%=session.getAttribute("userId")%>',email,cmpName,isSearch);
                        }else if(id==2){
                            document.getElementById("linkPending").className = "";
                            document.getElementById("linkProccessed").className = "sMenu";
                            $("#tabvalue").val('2');
                            fillGridOnEvent('not in (\'pending\',\'withpe\') and tdr.debarmentBy=<%=session.getAttribute("userId")%>',email,cmpName,isSearch);                            
                        }
                    }
                }                
      </script>
    </head>
    <body onload="test(<%if ("y".equals(request.getParameter("isprocess"))) {
                    out.print("2");
                } else {
                    out.print("1");
                }%>);">
        <div class="dashboard_div">
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <div class="contentArea_1">
            <div class="pageHead_1">Process Debarment Requests</div><br/>
                <%if ("msg".equals(request.getParameter("tend"))) {
                                out.print("<div class=\"responseMsg successMsg\">Clarification posted to Bidder/Consultant successfully</div><br/>");
                            }
                            if ("hope".equals(request.getParameter("msg"))) {
                                out.print("<div class=\"responseMsg successMsg\">Debarment request sent to Hopa successfully</div><br/>");
                            }%>
                <span style="float: right;"><a href="InitiateDebarmentProcess.jsp" class="action-button-add">Initiate Debarment</a>
                    <a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('5');">Save as PDF</a>
                </span><br/><br/>
            <div class="formBg_1 t_space">
                <table width="100%" cellspacing="8" class="formStyle_1">
                    <tr>
                        <td class="ff t-align-left" width="20%">Email ID :</td>
                        <td class="t-align-left" width="80%">
                            <input type="text" name="emailId" id="emailId" style="width: 200px;" class="formTxtBox_1"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff t-align-left" width="20%">Company Name :</td>
                        <td class="t-align-left" width="80%">
                            <input type="text" name="companyName" id="companyName" style="width: 200px;" class="formTxtBox_1"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left" width="20%"></td>
                        <td class="t-align-left" width="80%">
                            <label class="formBtn_1">
                                <input type="button" value="Search" id="search" onclick="test(0);"/>
                            </label>
                            <label class="formBtn_1">
                                <input type="reset" value="Reset" onclick="window.location.reload();"/>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>            
            <input type="hidden" value="1" id="tabvalue"/>
            <ul class="tabPanel_1 t_space">
                <li><a href="javascript:void(0);" id="linkPending" onclick="test(1);">Pending</a></li>
                <li><a href="javascript:void(0);" id="linkProccessed" onclick="test(2);">Processed</a></li>
            </ul>            
            <div class="tabPanelArea_1">
                    <div id="jQGrid" align="center">
                        <table id="list"></table>
                        <div id="page"></div>
                    </div>
                </div>
           </div>
            <!--For Generate PDF  Starts-->
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="DebarmentListing_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="debarment" />
            </form>
            <!--For Generate PDF  Ends-->
            <%@include file="../resources/common/Bottom.jsp" %>            
        </div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabDebar");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
