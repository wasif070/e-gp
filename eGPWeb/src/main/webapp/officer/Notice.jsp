<%--
    Document   : Notice
    Created on : Nov 16, 2010, 11:06:27 AM
    Author     : TaherT,Rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.GrandSummaryService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalServiceForms"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPostQueConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderPostQueConfigService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCancelTenderRequest"%>
<%@page import="com.cptu.egp.eps.model.table.TblCurrencyMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CurrencyMasterService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCurrency"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>

<%@page import= "com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Dashboard</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <!--jalert -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" >

            function AlertNot(){
                jAlert("Configure Document Available Method first"," Alert ", "Alert");
            }

            function checkTender(){
                jAlert("there is no Tender Notice"," Alert ", "Alert");
            }

            function checkdefaultconf(){
                jAlert("Default workflow configuration hasn't crated"," Alert ", "Alert");
            }

            function checkTocMem(){
                jAlert("TOC members for Encryption/Decryption not selected yet"," Alert ", "Alert");
            }
            function configureEstCost() {
                jAlert("It is necessary to configure estimate cost before work flow"," Alert ","Alert");
            }
            function chekFormMark(eventType){
                //if(eventType=='REOI' || eventType=='reoi'){
                  //  jAlert("Please select Criteria, Sub Criteria and Technical for all Technical forms "," Alert ", "Alert");
                //}else{
                    jAlert("Please configure Criteria, Sub Criteria and Technical Score for all Technical forms"," Alert ", "Alert");
              //  }
            }
            function checkEstCost(){
                jAlert("Please enter Official Cost Estimate"," Alert ", "Alert");
            }
            function checkKeyInformation(){
                jAlert("Please configure Key Information"," Alert ", "Alert");
            }
            function checksubmisDate(){
                jAlert("Please prepare notice first"," Alert ", "Alert");
            }
            function showauthMsg(){
                jAlert("Authorized Tenderers/Consultants not yet mapped"," Alert ", "Alert");
            }
            function checkClarification()
            {
                jAlert("Configuration of Clarification on Tender is pending"," Alert ", "Alert");
            }
            function evalSerprocMsg(){
                jAlert("Configure Marks for Technical Evaluation"," Alert ", "Alert");
            }
            function checkTecTc(id){
                if(id==3){
                    jAlert("TEC and TC has not configured yet"," Alert ", "Alert");
                } 
               else if(id==2){
                    jAlert("TC has not configured yet"," Alert ", "Alert");
                }
                else if(id==1){
                    jAlert("TEC has not configured yet"," Alert ", "Alert");
                }
            }
            
            function showFormMsg(id, fName, tName){
                var msgToShow = "";
                switch(parseInt(id)){
                    case 0:
                        //msgToShow = "Please Create Forms";
                        msgToShow = "Please complete Tender Forms in Tender Document";
                        break;
                    case 1:
                        //msgToShow = "Please Create Tables in <b>Form</b> ";
                        msgToShow = "Please complete Tender Forms in Tender Document";
                        break;
                    case 2:
                        //msgToShow = "Please Create Columns in <b>Table</b> of <b>Form</b>";
                        msgToShow = "Please complete Tender Forms in Tender Document";
                        break;
                    case 3:
                        //msgToShow = "Please Create Rows in <b>Table</b> of <b>Form</b>";
                        msgToShow = "Please complete Tender Forms in Tender Document";
                        break;
                    case 4:
                        //msgToShow = "Please Complete Formulas in <b>Table</b> of <b>Form</b>";
                        msgToShow = "Please complete Tender Forms in Tender Document";
                        break;
                }
                jAlert(msgToShow, "Complete Form", function(r){
                    if(r){
                        return true;
                    }else{
                        return false;
                    }
                });
            }
        </script>
        <title>Committe Evalution</title>
    </head>
    <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <%
                    if ("y".equals(request.getParameter("repDel")))  // Code for delete report formate
                    {

                        // Coad added by Dipal for Audit Trail Log.
                        String reportType="TOR/TER";
                        if(request.getParameter("reportType")!=null)
                        {
                            reportType=request.getParameter("reportType");
                        }
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        String idType="tenderId";
                        int auditId=Integer.parseInt(request.getParameter("tenderid"));
                        String auditAction="PCR for "+reportType+" deleted by PE";
                        String moduleName=EgpModule.Tender_Notice.getName();
                        String remarks="";

                        ReportCreationService rcs = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                        if (rcs.deleteWholeReport(request.getParameter("repId")))
                        {
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            response.sendRedirect("Notice.jsp?tenderid=" + request.getParameter("tenderid"));
                        }
                        else
                        {
                            auditAction="Error in "+auditAction;
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            response.sendRedirect("Notice.jsp?tenderid=" + request.getParameter("tenderid") + "repDel=n");
                        }
                    }

                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    EvalServiceCriteriaService evalServiceCriteriaService = (EvalServiceCriteriaService) AppContext.getSpringBean("EvalServiceCriteriaService");
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    GrandSummaryService grandSummaryService = (GrandSummaryService)  AppContext.getSpringBean("GrandSummaryService");
                    boolean chkReTenderLink = false;

        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>

        <%
                    // Variable tenderId is defined by u on ur current page.
                    pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                    pageContext.setAttribute("tab", "1");
                    String msgPublish = "";
                    if (request.getParameter("msgPublish") != null) {
                        msgPublish = request.getParameter("msgPublish");
                    }


                    String msg = "";
                    msg = request.getParameter("msg");
        %>
         <%
                                    TenderEstCost estcost = (TenderEstCost) AppContext.getSpringBean("TenderEstCost");
                                    boolean flag = false;
                                    flag = estcost.checkForLink(Integer.parseInt(request.getParameter("tenderid")));

                        %>
        <div class="contentArea_1">

            <div class="pageHead_1">Tender Dashboard</div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div class="dividerBorder t_space" >&nbsp;</div>
            <%@include  file="officerTabPanel.jsp"%>

            <div class="tabPanelArea_1">
                <% //if (request.getParameter("TenderCancel") != null && "true".equals(request.getParameter("TenderCancel"))) {%>
                <!--                <div class="responseMsg successMsg">Tender canceled successfully</div>-->
                <% //}else
                            if (request.getParameter("TenderCancel") != null && "false".equals(request.getParameter("TenderCancel"))) {%>
                <div class="responseMsg errorMsg">Tender cancellation failed</div>
                <%}%>
                <% if (msg != null && !msg.contains("not")) {%>
                <div class="responseMsg successMsg">File processed successfully</div>
                <%  } else if (msg != null && msg.contains("not")) {%>
                <div class="responseMsg errorMsg">File has not processed successfully</div>
                <%  }%>
                <%if (request.getParameter("infomsg") != null) {%>
                <div class="responseMsg successMsg">Official Cost Estimate Entered Successfully</div>
                <%                            }
                            String message = "";
                            if (request.getParameter("message") != null) {
                                message = request.getParameter("message");
                            }
                            if ("fail".equalsIgnoreCase(message)) {
                %>
                <div class="responseMsg errorMsg t_space"><span>Configuration of Clarification is failed</span></div>
                <%      } else if ("success".equalsIgnoreCase(message)) {
                %>
                <div class="responseMsg successMsg t_space"><span>Clarification on Tender Configured successfully</span></div>
                <%      } else if ("update".equalsIgnoreCase(message)) {
                %>
                <div class="responseMsg successMsg t_space"><span>Clarification on Tender option Updated Successfully</span></div>
                <%      } else if ("updatefail".equalsIgnoreCase(message)) {
                %>           <div class="responseMsg errorMsg t_space"><span>Updation of Clarification  is failed</span></div>
                <%}%>&nbsp;
                <%
                    boolean editRules = false; //Nitish :: check for if edit option is disable or not in committee member encription or decryption and oficial cost estimate selecteion of bidder in LTM, DCM method
                    
                List<SPCommonSearchDataMore> reTenderingLink2 = commonSearchDataMoreService.geteGPData("checkForReTenderOrReject", tenderid);
                            CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");
                            int uid = 0;
                            if (session.getAttribute("userId") != null) {
                                Integer ob1 = (Integer) session.getAttribute("userId");
                                uid = ob1.intValue();
                            }
                            WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                            short eventid = 2;
                            int objectid = 0;
                            short activityid = 2;
                            int childid = 1;
                            short wflevel = 1;
                            int initiator = 0;
                            int approver = 0;
                            boolean isInitiater = false;
                            boolean evntexist = false;
                            String fileOnHand = "No";
                            String checkperole = "No";
                            String chTender = "No";
                            boolean ispublish = false;
                            boolean iswfLevelExist = false;
                            boolean isconApprove = false;
                            String tenderStatus = "";
                            String tenderworkflow = "";
                            boolean istocmem = false;
                            boolean isPE = false;
                            boolean isCreator = false;
                            boolean checksubmission = false;
                            boolean isOfcEstCostAdded = false;
                            boolean isForeignCurrencyExchangeRateAdded = false;
                            boolean isICTorGoodsLarge = false;
                            boolean isGoodsLarge = false;
                            long totalCurrencyAdded = 0;
                            String objtenderId = request.getParameter("tenderid");
                            if (objtenderId != null) {
                                objectid = Integer.parseInt(objtenderId);
                                childid = objectid;
                            }
                            String donor = "";
                            donor = workFlowSrBean.isDonorReq(String.valueOf(eventid),
                                    Integer.valueOf(objectid));
                            String[] norevs = donor.split("_");
                            int norevrs = -1;

                            String strrev = norevs[1];
                            if (!strrev.equals("null")) {

                                norevrs = Integer.parseInt(norevs[1]);
                            }


                            evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
                            List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
                            // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);


                            if (tblWorkFlowLevelConfig.size() > 0) {
                                Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                iswfLevelExist = true;
                                while (twflc.hasNext()) {
                                    TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                    TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                    if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                                        fileOnHand = workFlowlel.getFileOnHand();
                                    }
                                    if (workFlowlel.getWfRoleId() == 1) {
                                        initiator = lmaster.getUserId();
                                    }
                                    if (workFlowlel.getWfRoleId() == 2) {
                                        approver = lmaster.getUserId();
                                    }
                                    if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                                        isInitiater = true;
                                    }

                                }

                            }


                            String userid = "";
                            if (session.getAttribute("userId") != null) {
                                Object obj = session.getAttribute("userId");
                                userid = obj.toString();
                            }
                            String field1 = "CheckCreator";
                            List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, request.getParameter("tenderid"), userid);
                            String IsAu = "CheckAu";
                            List<CommonAppData> editdata2 = workFlowSrBean.editWorkFlowData(IsAu, userid, "");

                            if (editdata.size() > 0 || editdata2.size() > 0) 
                            {
                                checkperole = "Yes";
                            }

                            // for publish link
                            boolean b_isPublish = false;
                            TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> tocmem = wfTenderCommonService.returndata("TocMemEncDec", request.getParameter("tenderid"), "");
                            if (tocmem.size() > 0) {
                                istocmem = true;
                            }

                            List<SPTenderCommonData> checkTender = wfTenderCommonService.returndata("TenderWorkFlowStatus", request.getParameter("tenderid"), "'Pending'");
                            if (checkTender.size() > 0) {
                                chTender = "Yes";
                            }
                            List<SPTenderCommonData> publist = wfTenderCommonService.returndata("TenderWorkFlowStatus", request.getParameter("tenderid"), "'Approved','Conditional Approval'");

                            List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights", userid,objtenderId);
                            List<SPTenderCommonData> chekTenderCreator = wfTenderCommonService.returndata("CheckTenderCreator", Integer.toString(uid),tenderid);
                            if (publist.size() > 0) {
                                ispublish = true;
                                SPTenderCommonData spTenderCommondata = publist.get(0);
                                tenderStatus = spTenderCommondata.getFieldName1();
                                tenderworkflow = spTenderCommondata.getFieldName2();
                                if ("Approved".equalsIgnoreCase(tenderStatus)) {
                                    b_isPublish = true;
                                }
                                if (tenderworkflow.equalsIgnoreCase("Conditional Approval")) {
                                    isconApprove = true;
                                }
                            }

                            SPTenderCommonData isFormOk = tenderSrBean.isTenderFormOk(objectid);
                            List<TblTenderDetails> tenderDetails = workFlowSrBean.checkSubmissionDate(objectid);
                            List<TblTenderDetails> tenderDetails1 = tenderSrBean.getTenderDetails(objectid);
                            

                            if (tenderDetails.size() > 0) {
                                TblTenderDetails wfTenderDetails = tenderDetails.get(0);
                                if (!"".equals(wfTenderDetails.getSubmissionDt()) && wfTenderDetails.getSubmissionDt() != null) {
                                    checksubmission = true;
                                }
                            }


                            List<SPTenderCommonData> checkconfstatus = wfTenderCommonService.returndata("GetConfigStatus", request.getParameter("tenderid"), null);

                            List<SPTenderCommonData> gtTednerDocAvlMethodLst = wfTenderCommonService.returndata("getTenderDocAvlMethod", request.getParameter("tenderid"), null);
                            String curDocAvlMethod = "";

                            if (!gtTednerDocAvlMethodLst.isEmpty()) {
                                curDocAvlMethod = gtTednerDocAvlMethodLst.get(0).getFieldName2();
                            }
                            int pqTenderId = 0;
                            boolean isCancelled = true;
                            List<TblTenderDetails> tblTenderDetailTS = tenderSrBean.getTenderStatus(request.getParameter("tenderid"));
                            if (!tblTenderDetailTS.isEmpty()) {
                                pqTenderId = tblTenderDetailTS.get(0).getPqTenderId();
                                if ("Cancelled".equalsIgnoreCase(tblTenderDetailTS.get(0).getTenderStatus())) {
                                    isCancelled = false;
                                }
                            }

                            long tenderLTMCnt = tenderCommonService.chkTenderLimited(tenderid);
                            long tenderAutTendCnt = tenderCommonService.getauthTendCnt(tenderid);

                            String procureNature = "";
                            for (CommonTenderDetails commonTenderDetails : tenderSrBean.getAPPDetails(Integer.parseInt(objtenderId), "tender")) {
                                procureNature = commonTenderDetails.getProcurementNature();
                            }
                            boolean isFormConfig = false;
                            if("Services".equalsIgnoreCase(procureNature)){
                                List<SPCommonSearchDataMore> tenderPub = commonSearchDataMoreService.geteGPDataMore("TenderPubServiceChk",objtenderId);
                                if(tenderPub!=null && !tenderPub.isEmpty() && "0".equalsIgnoreCase(tenderPub.get(0).getFieldName1())){
                                    isFormConfig = true;
                                }

                            }
                            List<TblEvalServiceForms> listEvalSerForm = new ArrayList<TblEvalServiceForms>();
                            if ("Services".equalsIgnoreCase(procureNature)) {
                                listEvalSerForm = evalServiceCriteriaService.listData(Integer.parseInt(objtenderId));
                            }

                            String eventType = commonService.getEventType(objtenderId).toString();
                            if (msgPublish != null && msgPublish.contains("msgPublish")) {%>
                <div class="responseMsg successMsg">File has processed and Tender Published successfully</div>
                <%  }%>
                <table class="tableList_1" cellspacing="0" width="100%">
                    <tbody><tr>
                            <td class="t-align-left ff" width="30%">Notice</td>
                            <td class="t-align-left" width="70%">
                                <%
                                            boolean flag1 = evalServiceCriteriaService.reTendering(request.getParameter("tenderid"));
                                            boolean flag2 = evalServiceCriteriaService.reTenderingHide(request.getParameter("tenderid"));
                                            if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString()))
                                            {
                                            if (isCancelled) {
                                                if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && b_isPublish == false && publist.isEmpty())
                                                        || (!tenderStatus.equalsIgnoreCase("Approved") && isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true) // remove  && initiator != approver
                                                        || iswfLevelExist == false) {
                                                    boolean isCreate = false;
                                                    for (CommonTenderDetails commonTenderDetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "tender")) {
                                                        if (commonTenderDetails.getTenderPubDt() != null) {
                                                            isCreate = true;
                                                        }
                                                    }
                                                    if (isCreate) {%>
                                <a href="UpdateTender.jsp?id=<%=request.getParameter("tenderid")%>">Edit</a>&nbsp; |&nbsp;
                                <%} else {%>
                                <a href="CreateTender.jsp?id=<%=request.getParameter("tenderid")%>">Edit</a>&nbsp; |&nbsp;
                                <% }



                                                }
                                            }}%>

<!--                            <a href="<%=request.getContextPath()%>/resources/common/ViewTender.jsp?id=<%=request.getParameter("tenderid")%>" >View</a> -->
                                <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/ViewTender.jsp?id=<%=request.getParameter("tenderid")%>', '', 'resizable=yes,scrollbars=1','');">View</a>
                                <% System.out.println("commonService.getServerDateTime():"+tenderDetails1.get(0).getTenderPubDt());
                                            String userId = "";
                                            HttpSession hs = request.getSession();
                                            if (hs.getAttribute("userId") != null) {
                                                userId = hs.getAttribute("userId").toString();
                                            }
                                            List<SPTenderCommonData> checkPErole = tenderCommonService.returndata("checkPERole", request.getParameter("tenderid"), userId);
                                            if(!checkPErole.isEmpty() && checkPErole!=null){


                                            List<SPCommonSearchDataMore> reTenderingLink = commonSearchDataMoreService.geteGPData("getReTenderingLink", tenderid);

                                            if (flag1 && !flag2) {
                                                chkReTenderLink = true;
                                            %>
                                &nbsp; |&nbsp;<a href="<%=request.getContextPath()%>/CreateTenderPQServlet?tId=<%=tenderid%>&eType=4">Re-Tendering</a>
                                <% }else if(!reTenderingLink.isEmpty() && reTenderingLink.get(0).getFieldName1().equals("yes") && !flag2){chkReTenderLink = true;%>
                                &nbsp; |&nbsp;<a href="<%=request.getContextPath()%>/CreateTenderPQServlet?tId=<%=tenderid%>&eType=4">Re-Tendering</a>
                                <%}else if(!reTenderingLink2.isEmpty() && reTenderingLink2.get(0).getFieldName1().equals("1") && !flag2){chkReTenderLink = true;%>
                                &nbsp; |&nbsp;<a href="<%=request.getContextPath()%>/CreateTenderPQServlet?tId=<%=tenderid%>&eType=4">Re-Tendering</a>
                                <%}}%>
   <!--                            <a href="<-%=request.getContextPath()%>/resources/common/ViewTender.jsp?id=<-%=request.getParameter("tenderid")%>">View</a> &nbsp;|&nbsp;-->
                                <%
                                            if (ispublish && isCancelled) {
                                            if (eventType != null) {
                                            List<SPCommonSearchDataMore> lotSelectionSub = commonSearchDataMoreService.geteGPData("chkLotSelectionSubmission", tenderid,null , null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                            if(!lotSelectionSub.isEmpty()){
                                                if(lotSelectionSub.get(0).getFieldName1().equalsIgnoreCase("yes")){
                                                    boolean isPEFor2ndStage = false;
                                                    boolean isAUFor2ndStage = false;
                                                    String roleThis = "";
                                                    roleThis = commonService_media.getUserRoleByUserId(Integer.parseInt(session.getAttribute("userId").toString()));
                                                    if(roleThis.contains("AU"))
                                                    {
                                                        isAUFor2ndStage = true;
                                                    }
                                                    List<SPTenderCommonData> checkPEFor2ndStage = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", request.getParameter("tenderid"), userId);
                                                    if (!checkPEFor2ndStage.isEmpty() && userId.equals(checkPEFor2ndStage.get(0).getFieldName1())) {
                                                    isPEFor2ndStage = true;
                                                }
                                                    if((!chkReTenderLink) && (isPEFor2ndStage || isAUFor2ndStage)){
                                %>
                                <%if (eventType.equalsIgnoreCase("PQ")) {%>&nbsp;|&nbsp;<a href="<%=request.getContextPath()%>/CreateTenderPQServlet?tId=<%=tenderid%>&eType=1">Create 2nd Stage PQ</a><%}%>
                                <%if (eventType.equalsIgnoreCase("REOI")) {%>&nbsp;|&nbsp;<a href="<%=request.getContextPath()%>/CreateTenderPQServlet?tId=<%=tenderid%>&eType=2">Create RFP</a><%}%>
                                <%if (eventType.equalsIgnoreCase("1 stage-TSTM")) {%>&nbsp;|&nbsp;<a href="<%=request.getContextPath()%>/CreateTenderPQServlet?tId=<%=tenderid%>&eType=3">Create 2nd Stage of TSTM</a><%}%>
                                <% }} }}
                                            }%>
                                <%
                                            if (isCancelled) {
                                                if ((ispublish == true && isInitiater == true && !tenderStatus.equalsIgnoreCase("Approved")) || ((initiator == approver && norevrs == 0) && isInitiater == true && !tenderStatus.equalsIgnoreCase("Approved") && checksubmission == true && chTender.equalsIgnoreCase("Yes") && istocmem == true && !checkconfstatus.isEmpty() && initiator != 0 && approver != 0)) {
                                                    boolean docMaped = tenderSrBean.chkDocMaped(Integer.parseInt(tenderid));
                                                    if (docMaped) {
                                                                    List<SPCommonSearchDataMore> pMethodEstCost = commonSearchDataMoreService.getCommonSearchData("GetPMethodEstCostTender",tenderid);
                                                                    String pMethod = pMethodEstCost.get(0).getFieldName1();
                                                                    boolean isSFB = "SFB".equals(pMethod);
                                                                    String estCost = pMethodEstCost.get(0).getFieldName2();
                                                        if (listt.isEmpty()) {
                                %>
                                &nbsp;|&nbsp;<a href="#" onclick="checkClarification();">Publish</a>
                                <%}else if (!eventType.equalsIgnoreCase("REOI") && isSFB && estCost==null) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="checkEstCost()" >Publish</a>
                                <!--aprojit --/>(05-July-2017) hide "Please configure Criteria, Sub Criteria and Technical Score for all Technical forms" point --!>
                                <%--  } else if (isFormConfig) { 
                                &nbsp;|&nbsp;<a href="#" onclick="chekFormMark('<%=eventType%>')" >Publish</a> --%>
                                <% }
                                                        else {
                                                                                        if (Integer.parseInt(isFormOk.getFieldName1().trim()) == 5) {
                                                                                            if(grandSummaryService.isGrandSummaryCreated(objectid) == -1 && (!(eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("1 stage-TSTM")))) {
                                %>
                                &nbsp;|&nbsp;<a href="#" onclick="checkGrandSummaryCreated('<%grandSummaryService.isGrandSummaryCreated(objectid);%>')">Publish</a>
                                <% } else if(commonService.getServerDateTime().compareTo(tenderDetails1.get(0).getTenderPubDt())>0) {%>
                                    &nbsp;|&nbsp;<a href="#" onclick="javascript:alert('publish date should be greater than current date.')">Publish</a>
                               <% } else {%>
                                &nbsp;|&nbsp;<a href="PublishViewTender.jsp?id=<%=request.getParameter("tenderid")%>" onclick="return isPublished(<%=request.getParameter("tenderid")%>)">Publish</a>
                                <%
                                                                                       } } else {
                                %>
                                &nbsp;|&nbsp;<a href="#" onclick="showFormMsg('<%= isFormOk.getFieldName1().trim()%>', '', '');">Publish</a>
                                <%
                                                                                        }
                                %>

                                <%}%>
                                <%  } else {
                                %>
                                &nbsp;|&nbsp;<a href="#" onclick="return docMaped();">Publish</a>
                                <%                                                           }
                                                                                } else if (!checksubmission && iswfLevelExist == true && isInitiater == true && (initiator == approver && norevrs == 0)) {%>
                                <a href="#" onclick="checksubmisDate()">publish</a> &nbsp;|&nbsp;
                                <% } else if (checkconfstatus.isEmpty() && iswfLevelExist == true && isInitiater == true && (initiator == approver && norevrs == 0)) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="checkKeyInformation()" >Publish</a>
                                <% } else if (!istocmem && iswfLevelExist == true && isInitiater == true && (initiator == approver && norevrs == 0)) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="checkTocMem()" >Publish</a>
                                <% }
                                            }
                                            if (tenderStatus.equalsIgnoreCase("Approve")) {%>
                                <a href="#">Re Tender</a>&nbsp;| &nbsp;
                                <% }
                                            //if (isInitiater == true && initiator != approver && ispublish == false && fileOnHand.equalsIgnoreCase("Yes")) {%>
                                <!--                                &nbsp;| &nbsp;<a href="#">Cancel</a>-->
                                <% //}
                                            if (isCancelled) {%>

<!--                                <span style="padding-right:5px;float: right;"><a href="<%=request.getContextPath()%>/help/tendernoticedocsprep/TenderNotice_Doc_PreparationSteps_25April2011.pdf" class="action-button-download" target="_blank">Steps for Tender/Proposal Preparation</a></span> Comment by Aprojit-->
                                <% }%>
                            </td>
                        </tr>
                        <tr><td class="t-align-left ff">Configure Key Information </td>
                            <td>
                                <%
                                            if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString()))
                                            {
                                                if (isCancelled) {
                                                if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false)
                                                        || (!tenderStatus.equalsIgnoreCase("Approved") && isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true)
                                                        || iswfLevelExist == false) {
                                                    if (!checkconfstatus.isEmpty()) {
                                                        editRules = true;
                                %>
                                <a href="ConfigureKeyInfo.jsp?tenderId=<%=request.getParameter("tenderid")%>&isEdit=yes">Edit</a>&nbsp; |&nbsp;
                                <%} else {%>
                                <%if ("".equalsIgnoreCase(curDocAvlMethod.trim())) {%>
                                <a onclick="AlertNot()" style="cursor: pointer;" >Create</a>
                                <%} else {%>
                                <a href="ConfigureKeyInfo.jsp?tenderId=<%=request.getParameter("tenderid")%>">Create</a>
                                <%}%>

                                <% }
                                                }
                                            }
                                            }
                                            if (!checkconfstatus.isEmpty()) {%>

                                <a href="ViewConfKeyInformation.jsp?tenderId=<%=request.getParameter("tenderid")%>">View</a>
                                <%}%>
                            </td>
                        </tr>
                        <tr><td class="t-align-left ff">Clarification on Tender</td>
                            <td>
                                <%if (listt.isEmpty()  )
                                  {
//                                    if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString()) && tenderProcureNature.equalsIgnoreCase("Services"))
//                                    {
                                %>
                                <% /* if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && b_isPublish == false)/*Remove isInstation != approver dixit*/
                                        /*    || (!tenderStatus.equalsIgnoreCase("Approved") && isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true) // remove  && initiator != approver
                                            || iswfLevelExist == false) { */%>
                                <!--<a href="ConfigTenderClari.jsp?tenderId=<%//=request.getParameter("tenderid")%>">Configure</a>-->
                                <%// }}%>
                                <%} else {%>
                                <% /* if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && b_isPublish == false)
                                           || (!tenderStatus.equalsIgnoreCase("Approved") && isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true) // remove  && initiator != approver
                                           || iswfLevelExist == false) {
                                    if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString()) && tenderProcureNature.equalsIgnoreCase("Services"))
                                    { */
                                %>
                                <!--<a href="ConfigTenderClari.jsp?tenderId=<%//=request.getParameter("tenderid")%>&queryId=<%=listt.get(0).getPostQueConfigId()%>&msg=edit">Edit</a> &nbsp;|&nbsp;-->
                                <% //}}%>
                                <a href="ViewConfigTenderClari.jsp?tenderId=<%=request.getParameter("tenderid")%>&msg=view">View</a>
                                <%}%>
                            </td>
                        </tr>

                        <%  /* Dohatec: ICT Start */
                            //Procurement Type (i.e. NCT/ICT) retrieving
                            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", request.getParameter("tenderid"), null);
                          //
                            if((!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")) || tenderCommonService1.getSBDCurrency(Integer.parseInt(request.getParameter("tenderid"))).equalsIgnoreCase("Yes")){ // If ICT
                                isICTorGoodsLarge = true ;
                        %>
                        <tr><td class="t-align-left ff">Currency</td>
                            <td>
                                <%
                                    boolean showAddCurRate = false;
                                    List<SPTenderCommonData> checkPE = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", request.getParameter("tenderid"), userId);

                                    if (!checkPE.isEmpty() && userId.equals(checkPE.get(0).getFieldName1())) {
                                                isPE = true;
                                    }
                                    if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                     isCreator = true;
                                    }
                                    TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                                    totalCurrencyAdded = tenderCurrencyService.getCurrencyCountForTender(Integer.parseInt(request.getParameter("tenderid")));
                                    isForeignCurrencyExchangeRateAdded = tenderCurrencyService.isCurrencyExchangeRateAdded(Integer.parseInt(request.getParameter("tenderid")));
                                    if(isCreator && !ispublish){ //if No other currencies added except BTN, so <=1%>
                                   <!-- if(isCreator && !ispublish && (totalCurrencyAdded <= 1)){ //if No other currencies added except BTN, so <=1%> -->
                                    BTN - Bhutanese Ngultrum  <a href="AddCurrency.jsp?tenderid=<%=request.getParameter("tenderid")%>">Add More Currency</a>
                                <%}else if(isCreator && !ispublish && (fileOnHand.equalsIgnoreCase("Yes") || !iswfLevelExist )){%>
                                    <a href="AddCurrency.jsp?tenderid=<%=request.getParameter("tenderid")%>">Edit Currency</a>
                                    <% showAddCurRate = true;   %> 
                                <span>|</span>
                                <%} if(totalCurrencyAdded > 1){%>
                                    <a href="ViewCurrency.jsp?tenderid=<%=request.getParameter("tenderid")%>">View Currency</a>        
                                <%}%>
                                <% if(tenderCommonService1.getSBDCurrency(Integer.parseInt(request.getParameter("tenderid"))).equalsIgnoreCase("Yes")) {
                                    isGoodsLarge = true ;
                                    if(showAddCurRate) {
                                        if(!isForeignCurrencyExchangeRateAdded) { %>
                                            <span>|</span>
                                            <a href="AddCurrencyDetails.jsp?tenderid=<%=request.getParameter("tenderid")%>">Add Currency Rate</a>
                                        <% } else { %>                                           
                                                <span>|</span>
                                                <a href="AddCurrencyDetails.jsp?tenderid=<%=request.getParameter("tenderid")%>">Edit Currency Rate</a>                                              
                                                <span>|</span>
                                                <a href="ViewCurrencyDetails.jsp?tenderid=<%=request.getParameter("tenderid")%>">View Currency Rate</a>  
                                             <% }%>
                                    <% }else{ 
                                                if(isForeignCurrencyExchangeRateAdded && (totalCurrencyAdded > 1)) { %>
                                                <span>|</span>
                                                  <a href="ViewCurrencyDetails.jsp?tenderid=<%=request.getParameter("tenderid")%>">View Currency Rate</a>  
                                            <%  } 
                                            } 
                                }%>
                                
                            </td>                              
                        </tr>
                        <%} /* Dohatec: ICT End */%>

                        <tr>   <td class="t-align-left ff">Workflow </td>
                            <td class="t-align-left">

                                <%
                                            if (evntexist) {

                                                if (fileOnHand.equalsIgnoreCase("yes") && !checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString()) && b_isPublish == false && isInitiater == true) {
                                                    if (isCancelled && publist.isEmpty() && false) {
                                                        if (tenderLTMCnt != 0) {
                                                            if (tenderAutTendCnt != 0) {
                                %>
                                <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=916'>Edit</a>

                                <%
                                                                                            } else {
                                %>
                                <a  href='javascript:void(0);' onclick="showauthMsg();">Edit</a>
                                <%                                                                                                                                }
                                                                                        } else {
                                %>
                                <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=916'>Edit</a>
                                <%
                                                                                        }

                                %>
                                <% }%>
                                <% if(publist.isEmpty()){ %>
                                    &nbsp;|&nbsp;
                                <% } %>
                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=906">View</a>

                                <% } else if (iswfLevelExist == false && checkperole.equals("Yes") && b_isPublish == false) {
                                                                                      if (isCancelled && false) {
                                                                                        if (tenderLTMCnt != 0) {
                                                                                            if (tenderAutTendCnt != 0) {
                                %>
                                <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=916'>Edit</a>
                                <%
                                                                                                                            } else {
                                %>

                                <a  href='javascript:void(0);' onclick="showauthMsg();">Edit</a>
                                <%                                                                                                }
                                                                                                                        } else {
                                %>

                                <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=916'>Edit</a>
                                <%
                                                                                                                        }
                                %>
                                <% }%>
                                <% if(publist.isEmpty()){ %>
                                &nbsp;|&nbsp;
                                <% } %>
                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=906">View</a>
                                <%  } else if (isCancelled && iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {%>

                                <label>Workflow yet not configured</label>
                                <%                                                              } else if (iswfLevelExist == true) {%>
                                <% if(publist.isEmpty()){ %>
                                    &nbsp;|&nbsp;
                                <% } %>
                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=906">View</a>


                                <%
                                                                                }
                                                                            } else if (b_isPublish == false && /*checkperole.equals("Yes")*/ !checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())) {
                                                                                if (isCancelled) {
                                                                                    List<CommonAppData> defaultconf = workFlowSrBean.editWorkFlowData("WorkFlowRuleEngine", Integer.toString(eventid), "");
                                                                                    if (defaultconf.size() > 0) {
                                                                                        //if (Integer.parseInt(isFormOk.getFieldName1().trim()) == 5) {
                                                                                        if (tenderLTMCnt != 0) {
                                                                                            if (tenderAutTendCnt != 0) {
                                                                                                if ("Services".equalsIgnoreCase(procureNature)) {
                                                                                                    if ( true || !listEvalSerForm.isEmpty()) { // aprojit-->(05-July-2017) condition always ture to avoid "Configure Marks for Technical Evaluation" alert
                                                                                                        if(grandSummaryService.isGrandSummaryCreated(objectid) == -1 && (!(eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("1 stage-TSTM")))) {
                                %>
                                &nbsp;|&nbsp;<a href="#" onclick="checkGrandSummaryCreated('<%grandSummaryService.isGrandSummaryCreated(objectid);%>')">Create</a>
                                <%} else if (!flag && (!(eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("1 stage-TSTM")))) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="configureEstCost()" >Create</a>
                                 <% } else {%>
                                <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&parentLink=925&submit=Submit&tc=tc">Create</a>
                                <%
                                                                                             }   } else {
                                %>
                                <a href="#" onclick="evalSerprocMsg('','','');">Create</a>
                                <%                                                           }
                                                                                            } else {
                                                                                                    if(grandSummaryService.isGrandSummaryCreated(objectid) == -1 && (!(eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("1 stage-TSTM")))) {
                                %>
                                                                &nbsp;|&nbsp;<a href="#" onclick="checkGrandSummaryCreated('<%grandSummaryService.isGrandSummaryCreated(objectid);%>')">Create</a>
                                <%} else if (!flag) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="configureEstCost()" >Create</a>
                                 <% } else {%>
                                <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&parentLink=925&submit=Submit&tc=tc">Create</a>
                                <%
                                                                                          }  }
                                                                                        } else {
                                %>
                                <a href="javascript:void(0);" onclick='showauthMsg();'>Create</a>
                                <%                                                                                                                                                        }
                                                                                    } else {
                                                                                        if ("Services".equalsIgnoreCase(procureNature)) {
                                                                                            if ( true || !listEvalSerForm.isEmpty()) { // aprojit-->(05-July-2017) condition always ture to avoid "Configure Marks for Technical Evaluation" alert
                                                                                                if(grandSummaryService.isGrandSummaryCreated(objectid) == -1 && (!(eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("1 stage-TSTM")))) {
                                %>
                                                                                                &nbsp;|&nbsp;<a href="#" onclick="checkGrandSummaryCreated('<%grandSummaryService.isGrandSummaryCreated(objectid);%>')">Create</a>
                                <%} else if (!flag && (!(eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("1 stage-TSTM")))) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="configureEstCost()" >Create</a>
                                 <% } else {%>
                                <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&parentLink=925&submit=Submit&tc=tc">Create</a>
                                <%
                                                                                            } } else {
                                %>
                                <a href="#" onclick="evalSerprocMsg('','','');">Create</a>
                                <%                                                       }
                                                                                        } else {
                                                                                            if(grandSummaryService.isGrandSummaryCreated(objectid) == -1 && (!(eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("1 stage-TSTM")))) {
                                %>
                                                                                                &nbsp;|&nbsp;<a href="#" onclick="checkGrandSummaryCreated('<%grandSummaryService.isGrandSummaryCreated(objectid);%>')">Create</a>
                                <%} else if (!flag) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="configureEstCost()" >Create</a>
                                 <% } else {%>
                                <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&parentLink=925&submit=Submit&tc=tc">Create</a>
                                <%
                                                                                   } }

                                                                                }
                                %>

                                <%
                                                                                // } else {
%>
                                <%--<a href="#" onclick="showFormMsg('<%= isFormOk.getFieldName1().trim()%>', '', '');">Create</a>--%>
                                <%
                                                                                //  }
                                                                            } else {
                                %>
                                <a  href="#" onclick="checkdefaultconf()">Create</a>
                                <%                                                            }
                                                                                }
                                                                            } else if (isCancelled && iswfLevelExist == false /*&& !checkperole.equalsIgnoreCase("Yes")*/) {%>
                                <label>Workflow yet not configured</label>

                                <% }        //TEC & TC Check query start
                                            String tenderId = request.getParameter("tenderid");
                                            long tecCommCheck = committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + tenderId + " and tc.committeeType in ('TEC','PEC') and tc.committeStatus='approved'");
                                            long tcCommCheck = committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + tenderId + " and tc.committeeType in ('TC') and tc.committeStatus='approved'");
                                             //TEC & TC Check query end
                                            if (isCancelled && publist.isEmpty()) {
                                                if (isInitiater == true && (initiator != approver || (initiator == approver && norevrs > 0)) && b_isPublish == false && fileOnHand.equalsIgnoreCase("Yes")) {

                                %>
                                <%
                                                                                    if (Integer.parseInt(isFormOk.getFieldName1().trim()) == 5) {
                                                                                        if (!checksubmission) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="checksubmisDate()">Process file in Workflow</a>
                                <%} else if (checkconfstatus.isEmpty()) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="checkKeyInformation()" >Process file in Workflow</a>
                                <% } else if (!istocmem) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="checkTocMem()">Process file in Workflow</a>
                                <% } else if (listt.isEmpty()) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="checkClarification()">Process file in Workflow</a>
                                <!--aprojit --/>(05-July-2017) hide "Please configure Criteria, Sub Criteria and Technical Score for all Technical forms" point --!>
                                <%--} else if (isFormConfig) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="chekFormMark('<%=eventType%>')" >Process file in Workflow</a> --%>
                                  <% }
                                else if (isGoodsLarge && (!isForeignCurrencyExchangeRateAdded)) {%>
                                &nbsp;|&nbsp;<a href="#" onclick="jAlert('Please Add Currency Conversion Rate', 'Alert' ,'Alert');" >Process file in Workflow</a>
                                <% }else {
                                     if((tecCommCheck != 0) && (tcCommCheck!= 0) ){
                                %>                                   
                                    &nbsp;|&nbsp;<a href="FileProcessing.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&fromaction=notice" >Process file in Workflow</a>
                                <%   }
                                   else{
                                    if((tecCommCheck == 0) && (tcCommCheck == 0) ){ %>
                                      &nbsp;|&nbsp;  <a href="#" onclick="checkTecTc(3)">Process file in Workflow</a>
                                    <% }
                                       else if(tecCommCheck == 0){ %>
                                      &nbsp;|&nbsp;  <a href="#" onclick="checkTecTc(1)">Process file in Workflow</a>
                                    <% }
                                     else if(tcCommCheck == 0 ){ %>
                                      &nbsp;|&nbsp;  <a href="#" onclick="checkTecTc(2)">Process file in Workflow</a>
                                    <% }
                                     }
                                    }
                                                                    } else {

                                %>
                                &nbsp;|&nbsp;<a href="#" onclick="showFormMsg('<%= isFormOk.getFieldName1().trim()%>', '', '');">Process file in Workflow</a>
                                <%
                                                     }
                                                   }
                                                 }
                                             if (iswfLevelExist) {%>
                                &nbsp;| &nbsp;
                                <a href="workFlowHistory.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&userid=<%=uid%>&fraction=notice&parentLink=75" >View Workflow History</a>

                                <% }%>
                            </td>
                        </tr>

                        <!--  <tr>
                               <td class="t-align-left">Authorize Bidders for Limited Tender</td>
                               <td class="t-align-left"><a href="#">Map</a> &nbsp;| &nbsp;<a href="#">View</a></td>

                           </tr>-->
                        <tr>
                            <td class="t-align-left ff">Committee Member for Encryption/Decryption</td>
                            <td class="t-align-left">

                                <%if ((committeMemberService.checkCountByHql("TblTenderHash th", "th.tenderId=" + request.getParameter("tenderid")) == 0)) {
                                        if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                            if (isCancelled) {
                                                    if (/*checkperole.equalsIgnoreCase("Yes") && */ !tenderStatus.equalsIgnoreCase("Approved")) {%>
                                <a href="PEEncHash.jsp?tenderid=<%=request.getParameter("tenderid")%>&parentLink=804">Create</a>
                                <% }
                                    }}
                                } else {
                                    if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                %>
                                <% if (/*checkperole.equalsIgnoreCase("Yes") && */ !tenderStatus.equalsIgnoreCase("Approved")) {
                                        if (isCancelled && publist.isEmpty() && editRules ) { //Nitish Start%> 
                                <a href="PEEncHash.jsp?tenderid=<%=request.getParameter("tenderid")%>&isedit=y&parentLink=698">Edit</a>&nbsp;|&nbsp;
                                <% }
                                    }}%>
                                <a href="PEEncHash.jsp?tenderid=<%=request.getParameter("tenderid")%>&isview=y&parentLink=887">View</a><%}%>
                            </td>
                        </tr>


                    <%
                        if (!eventType.equalsIgnoreCase("REOI"))
                        {
                   %>
                        <tr>
                            <td class="t-align-left ff">Official Cost Estimate</td>
                            <td class="t-align-left">

                                <%

                                            List<SPTenderCommonData> checkPE = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", request.getParameter("tenderid"), userId);
//                                            if (!checkPE.isEmpty() && userId.equals(checkPE.get(0).getFieldName1())) {
//                                                isPE = true; }
                                            if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                isCreator = true;
                                                boolean isTER = true;
                                                List<SPCommonSearchDataMore> rptTERChk = dataMore.geteGPData("reportTORTERChk", tenderid,"ter3");
                                                if(rptTERChk!=null && (!rptTERChk.isEmpty()) && rptTERChk.get(0).getFieldName1().equals("1")){
                                                    isTER = false;
                                                }

                                %>

                                <% if (!flag) {
                                     if(!checkuserRights.isEmpty() && isTER && b_isPublish == false){
                                %>

                                <a href="AddPckLotEstCost.jsp?tenderid=<%=request.getParameter("tenderid")%>">Add</a>
                                <%} else {%>
                                Not Configured
                                <%
                                }} else {
                                 /**
                                       *  As per CR Suggestd by client that after publish tender Official estimate cost should not be edited
                                       *  23- Jan-2013 (CR Suggestd to Raghu from CPTU)
                                       *  Developed by Shreyansh Shah on 28-Jan-2013
                                       */
                                       if(!checkuserRights.isEmpty()  && isTER && b_isPublish == false && editRules){   //Nitish Start
                                      // if(!checkuserRights.isEmpty()  && isTER){
                                %>
                                <a href="ViewPckLotEstCost.jsp?tenderId=<%=request.getParameter("tenderid")%>&isedit=y">Edit</a>&nbsp;|&nbsp;
                                <%}%>
                                <a href="ViewPckLotEstCost.jsp?tenderId=<%=request.getParameter("tenderid")%>&isedit=n">View</a>
                                <%}
                                  } 
                        // Only creator of tender will be able to see tender estimated cost. Done by Emtaz on 20/March/2018
//                                else {%>
                                <!--<a href="ViewPckLotEstCost.jsp?tenderId=<%//=request.getParameter("tenderid")%>&isedit=n">View</a>-->
                             <%//}
                        //Emtaz end
                             %>
                        
                            </td>
                        </tr>
                    <a href="EvalNominationApproval.jsp"></a>
                        <!--
                        framework contract todo
                        -->
                        <!-- <%
                        //if(tenderDetails1.get(0).getProcurementMethodId()==18){%>
                        <tr>
                            <td class="t-align-left ff">Framework Contract Office Wise Item</td>
                            <td class="t-align-left">
                                <a href="FCItemWise.jsp?tenderid=<%//=request.getParameter("tenderid")%>">Add</a>
                            </td>
                        </tr>
                        <%
                            //}
                        %> -->

                    <%
                        }

                           if(!(eventType.equals("REOI") || eventType.equals("PQ") || eventType.equals("1 stage-TSTM"))){
                                    String procRole = null;
                                    if (session.getAttribute("procurementRole") != null) {
                                        procRole = session.getAttribute("procurementRole").toString();
                                        if (procRole.contains("PE") || procRole.contains("AU")) {
                        %>
                        <!--Comment out the following TR for the issue "EGPBDONE-477" -->
<!--                        <tr>
                            <td class="t-align-left ff">Creation of format for Price Comparison Report</td>
                            <td class="t-align-left">
                                <%

                                            String docAvlMethod = commonService.getDocAvlMethod(tenderid).toString();
                                            ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                                            String labTORPOR="TOR";
                                            String labTERPER="TER";
                                            if(procureNature.equalsIgnoreCase("services")){
                                                labTERPER = "PER";
                                                labTORPOR = "POR";
                                            }
                                            List<Object[]> reportList = creationService.getRepNameId(tenderid, docAvlMethod);
                                if(!checkuserRights.isEmpty() && reportList.isEmpty()){
                                if (isCancelled && isCreator) {%>
                                <a href="ReportCreation.jsp?tenderid=<%//=tenderid%>">Prepare</a>
                                <div class='responseMsg noticeMsg'>BOR/BER Reports will be available after Tender Opening Process.</div>
                                <% }}
                                %>
                                <div class="t_space" style="font-weight: bold;float: right;"><%=labTORPOR%>: <%if(procureNature.equalsIgnoreCase("Services")){out.print("Proposal");}else{out.print("Tender");}%> Opening Report, <%=labTERPER%>: <%if(procureNature.equalsIgnoreCase("Services")){out.print("Proposal");}else{out.print("Tender");}%> Evaluation Report</div>
                                <%
                                            if (!reportList.isEmpty()) {
                                %>
                                <table width='100%' cellspacing='0' class='tableList_1'>
                                    <tr>
                                        <th class="t-align-center">Sl. No.</th>
                                        <th class="t-align-center">Report Title</th>
                                        <th class="t-align-center">For <%=labTORPOR%> / <%=labTERPER%></th>
                                        <th class="t-align-center">Action</th>
                                    </tr>
                                    <%
                                        int cnt_report = 1;
                                        for (Object[] obj : reportList) {
                                            boolean isReportSaved = (creationService.isReportSaved(tenderid, obj[0].toString()) == 0);
                                            String pg_Name = null;
                                            //boolean editStatOpen = true;
                                            //boolean editStatEval = true;
                                            boolean editStat = true;
                                            if (obj[3].toString().equalsIgnoreCase("TOR")) {
                                                List<SPCommonSearchDataMore> rptTORChk = commonSearchDataMoreService.geteGPData("reportTORChk", tenderid);
                                                if(rptTORChk!=null && (!rptTORChk.isEmpty()) && rptTORChk.get(0).getFieldName1().equals("1")){
                                                    editStat = false;
                                                }
                                                rptTORChk=null;
                                                /*long repCnt = creationService.getCommitteeAppCount(tenderid, "'TEC','PEC'");
                                                if (repCnt != 0) {
                                                    editStatEval = false;
                                                }*/
                                            }
                                            if (obj[3].toString().equalsIgnoreCase("TER") && !isReportSaved) {
                                                editStat = false;
                                                /*long repCnt = creationService.getCommitteeAppCount(tenderid, "'TOC','POC'");
                                                if (repCnt != 0) {
                                                    editStatOpen = false;
                                                }*/
                                            }
                                            if (obj[2].toString().equalsIgnoreCase("item wise")) {
                                                pg_Name = "ReportFormSelection";
                                            } else {
                                                pg_Name = "ReportLotPkgSelection";
                                            }
                                    %>
                                    <tr>
                                        <td class="t-align-center"><%=cnt_report%></td>
                                        <td><%=obj[1]%></td>
                                        <td class="t-align-center"><%
                                                if(obj[3].toString().equals("TOR")){
                                                        out.print(labTORPOR);
                                                }else if(obj[3].toString().equals("TER")){
                                                        out.print(labTERPER);
                                                }
                                            %></td>
                                        <td class="t-align-center">
                                            <%if ((editStat) && isCancelled && isCreator && isReportSaved) {%>
                                            <a href="ReportColumns.jsp?tenderid=< %=tenderid%>&repId=< %=obj[0]%>&isedit=y&isNot=y&reportType=< %=(obj[3].toString().equals("TOR")) ? labTORPOR : labTERPER%>">Edit Report Format</a>
                                            &nbsp;|&nbsp;
                                            <a href="<%=pg_Name%>.jsp?tenderid=<%=tenderid%>&repId=<%=obj[0]%>&isNot=y&reportType=<%=(obj[3].toString().equals("TOR")) ? labTORPOR : labTERPER%>">Edit Report Formula</a>
                                            &nbsp;|&nbsp; 
                                            <%}%>
                                            <a href="ReportDisplay.jsp?tenderid=<%=tenderid%>&repId=<%=obj[0]%>&isdisp=y&reportType=<%=(obj[3].toString().equals("TOR")) ? labTORPOR : labTERPER%>">View</a>
                                            <%if ((editStat) && isCancelled && isCreator && isReportSaved) {%>
                                            &nbsp;|&nbsp;
                                            <a href="Notice.jsp?tenderid=<%=tenderid%>&repId=<%=obj[0]%>&repDel=y&reportType=<%=(obj[3].toString().equals("TOR")) ? labTORPOR : labTERPER%>" onclick="return confirm('Do you want to delete the Price Bid Comparison report?');">Delete Report Format</a>
											
                                            <%}%>
                                        </td>
                                    </tr>
                                    <%
                                            cnt_report++;
                                            }
                                    %>
                                </table>
                                <%
                                                                            }
                                                                            if (docAvlMethod.equalsIgnoreCase("Lot")) {
                                                                                reportList = creationService.getRepNameLotWise(tenderid);
                                                                                if (!reportList.isEmpty()) {
                                %>
                                <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                                    <tr>
                                        <th class="t-align-center">Lot No.</th>
                                        <th class="t-align-center">Lot Description</th>
                                        <th class="t-align-center">Report Title</th>
                                        <th class="t-align-center">For <%=labTORPOR%> / <%=labTERPER%></th>
                                        <th class="t-align-center">Action</th>
                                    </tr>
                                    <%
                                        for (Object[] obj : reportList) {
                                            boolean isReportSaved = (creationService.isReportSaved(tenderid, obj[0].toString()) == 0);
                                            String pg_Name = null;
                                            //boolean editStatEval = true;
                                            //boolean editStatOpen = true;
                                            boolean editStat= true;
                                            if (obj[3].toString().equalsIgnoreCase("TOR")) {
                                                List<SPCommonSearchDataMore> rptTORChk = commonSearchDataMoreService.geteGPData("reportTORChk", tenderid,obj[0].toString());
                                                if(rptTORChk!=null && (!rptTORChk.isEmpty()) && rptTORChk.get(0).getFieldName1().equals("1")){
                                                    editStat = false;
                                                }
                                                rptTORChk=null;
                                                /*long repCnt = creationService.getCommitteeAppCount(tenderid, "'TEC','PEC'");
                                                if (repCnt != 0) {
                                                    editStatEval = false;
                                                }*/
                                            }
                                            if (obj[3].toString().equalsIgnoreCase("TER") && !isReportSaved) {
                                                editStat = false;
                                                /*long repCnt = creationService.getCommitteeAppCount(tenderid, "'TOC','POC'");
                                                if (repCnt != 0) {
                                                    editStatOpen = false;
                                                }*/
                                            }
                                            if (obj[2].toString().equalsIgnoreCase("item wise")) {
                                                pg_Name = "ReportFormSelection";
                                            } else {
                                                pg_Name = "ReportLotPkgSelection";
                                            }
                                    %>
                                    <tr>
                                        <td class="t-align-center"><%=obj[4]%></td>
                                        <td><%=obj[5]%></td>
                                        <td><%=obj[1]%></td>
                                        <td class="t-align-center"><%
                                                if(obj[3].toString().equals("TOR")){
                                                        out.print(labTORPOR);
                                                }else if(obj[3].toString().equals("TER")){
                                                        out.print(labTERPER);
                                                }
                                            %></td>
                                        <td class="t-align-center">
                                            <%if ((editStat) && isCancelled && isCreator && isReportSaved) {%>
                                            <a href="ReportColumns.jsp?tenderid=<%=tenderid%>&repId=<%=obj[0]%>&isedit=y&isNot=y">Edit Report Format</a>
                                            &nbsp;|&nbsp;
                                            <a href="<%=pg_Name%>.jsp?tenderid=<%=tenderid%>&repId=<%=obj[0]%>&isNot=y">Edit Report Formula</a>
                                            &nbsp;|&nbsp;
                                            <%}%>
                                            <a href="ReportDisplay.jsp?tenderid=<%=tenderid%>&repId=<%=obj[0]%>&isdisp=y">View</a>
                                            <%if ((editStat) && isCancelled && isCreator && isReportSaved) {%>
                                            &nbsp;|&nbsp;
                                            <a href="Notice.jsp?tenderid=<%=tenderid%>&repId=<%=obj[0]%>&repDel=y" onclick="return confirm('Do you want to delete thePrice Bid Comparison report?');">Delete Report Format</a>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <%}%>
                                </table>
                                <%}
                                                                            }%>
                            </td>
                        </tr>-->
                        <%}
                                    }
                                    procRole = null;
                             }
                        %>



                        <%






                                    if (procureNature.equalsIgnoreCase("Works")) {
                        %>
                        <!--                    <tr>
                                                <td class="t-align-left ff">Engineering Estimation</td>

                                                <td class="t-align-left"><a href="UploadEngEstDocs.jsp?tenderid=<-%=request.getParameter("tenderid")%>">Upload</a></td>
                                                <td class="t-align-left">
                        <% //if(((tenderStatus.equalsIgnoreCase("pending") || tenderStatus.equals("")) && ispublish == false && isInitiater == true && fileOnHand.equalsIgnoreCase("Yes"))|| iswfLevelExist == false){ %>
                        <a href="UploadEngEstDocs.jsp?tenderid=<-%=request.getParameter("tenderid")%>">Upload</a>
                        <%  //}  %>
                    </td>

                </tr>-->
                        <% }


                                    if (tenderLTMCnt != 0) {
                        %>
                        <tr>
                            <td class="t-align-left ff">Selected Bidders/Consultants for Tender</td>
                            <td class="t-align-left">
                                <%
                                                                        List<SPCommonSearchDataMore> spcsdm = commonSearchDataMoreService.geteGPData("chkMapForAT", tenderid,null , null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                                        if (commonService.getProcNature(tenderid).toString().equalsIgnoreCase("Services")) {
                                                                            long archiveCnt = tenderCommonService.tenderArchiveCnt(tenderid);
                                                                            if ((isCancelled && archiveCnt != 0) || !ispublish) {
                                                                                if(!spcsdm.isEmpty()){
                                                                                   // if(spcsdm.get(0).getFieldName1().equalsIgnoreCase("yes")){

                                %>
                                <% if (checkperole.equalsIgnoreCase("Yes") && !tenderStatus.equalsIgnoreCase("Approved")) {
                                    int size = tenderSrBean.chkTblLimitedTenders(tenderid,0,false);
                                    if(size != 0){
                                    if(eventType.equalsIgnoreCase("2 Stage-PQ") || eventType.equalsIgnoreCase("2 stage-TSTM") || (eventType.equalsIgnoreCase("RFP") && pqTenderId != 0)){}else{%>
                                    <a href="MapTenderers.jsp?tenderid=<%=tenderid%>">Edit</a>&nbsp;|&nbsp;
                                    <% } %>
                                    <a href="MapTenderers.jsp?tenderid=<%=tenderid%>&isview=y">View</a>
                                    <%}else{
                                        if(eventType.equalsIgnoreCase("2 Stage-PQ") || eventType.equalsIgnoreCase("2 stage-TSTM") || (eventType.equalsIgnoreCase("RFP") && pqTenderId != 0)){
                                            %><a href="MapTenderers.jsp?tenderid=<%=tenderid%>&isview=y">View</a>
                                            <%
                                        }else {
                                    %>
                                    <a href="MapTenderers.jsp?tenderid=<%=tenderid%>">Create a Bidder's/Consultant's List</a>
                                    <% } } %>
                                <% }  %>
<!--                                    <a href="MapTenderers.jsp?tenderid=<-%=tenderid%>&isview=y">View</a>-->
                                <%  }else{%>
                                    <a href="MapTenderers.jsp?tenderid=<%=tenderid%>&isview=y">View</a>
                                <% } }
                                  if(tenderStatus.equalsIgnoreCase("Approved")){ %>
                                  <a href="MapTenderers.jsp?tenderid=<%=tenderid%>&isview=y">View</a>
                                    <% }%>
                                <% } else {
                                    if(tenderStatus.equalsIgnoreCase("Approved")){ %>
                                    <a href="MapLotWise.jsp?tenderid=<%=tenderid%>&cpe=<%=checkperole.charAt(0)%>&ispub=<%=ispublish%>&action=view">View</a>
                                    <%}else if(!spcsdm.isEmpty()){
                                        //if(spcsdm.get(0).getFieldName1().equalsIgnoreCase("yes")){
                                            int size = tenderSrBean.chkTblLimitedTenders(tenderid,0,false);
                                            if(size != 0){
                                                if(eventType.equalsIgnoreCase("2 Stage-PQ") || eventType.equalsIgnoreCase("2 stage-TSTM") || (eventType.equalsIgnoreCase("RFP") && pqTenderId != 0))
                                                {
                                                
                                                }
                                                else
                                                {
                                                    if(editRules) //Nitish Start
                                                    {
                                                %>
                                <a href="MapLotWise.jsp?tenderid=<%=tenderid%>&cpe=<%=checkperole.charAt(0)%>&ispub=<%=ispublish%>">Edit</a>&nbsp;|&nbsp;
                                <% }} %>
                                <a href="MapLotWise.jsp?tenderid=<%=tenderid%>&cpe=<%=checkperole.charAt(0)%>&ispub=<%=ispublish%>&action=view">View</a>
                                <% }else{
                                                if(eventType.equalsIgnoreCase("2 Stage-PQ") || eventType.equalsIgnoreCase("2 stage-TSTM") || (eventType.equalsIgnoreCase("RFP") && pqTenderId != 0)){%>
                                                <a href="MapLotWise.jsp?tenderid=<%=tenderid%>&cpe=<%=checkperole.charAt(0)%>&ispub=<%=ispublish%>&action=view">View</a>
                                <%}else{%>
                                <a href="MapLotWise.jsp?tenderid=<%=tenderid%>&cpe=<%=checkperole.charAt(0)%>&ispub=<%=ispublish%>">Create a Bidder's/Consultant's List</a>
                                <% } } } } //}%>
                            </td>
                        </tr>



                        <% }%>

                        <!-- Added by Ketan Prajapati for tender multiple  configuration -->
                        <!--                        <tr>
                                                    <td class="t-align-left ff">Configure Currency</td>
                                                    <td class="t-align-left">
                        <%
                                    /*CurrencyMasterService currencyMasterService = (CurrencyMasterService) AppContext.getSpringBean("CurrencyMasterService");
                                    TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                                    List<TblTenderCurrency> listConfiguredCurrency = tenderCurrencyService.getConfiguredCurrency(tenderid);
                                    List<TblCurrencyMaster> listCurrencyDetails = null;
                                    String strCurrencyName = "";
                                    if (listConfiguredCurrency != null && listConfiguredCurrency.size() > 0) {
                                    HashMap<String, BigDecimal> hashConfiguredCurrency = new HashMap<String, BigDecimal>();
                                    for (TblTenderCurrency tenderCurrency : listConfiguredCurrency) {
                                    int iCurrencyId = tenderCurrency.getCurrencyId();
                                    listCurrencyDetails = currencyMasterService.getCurrencyDetails(iCurrencyId);

                                    for (TblCurrencyMaster currencyDetails : listCurrencyDetails) {
                                    strCurrencyName = currencyDetails.getCurrencyName();
                                    }
                                    BigDecimal bigExchangeRate = tenderCurrency.getExchangeRate();
                                    hashConfiguredCurrency.put(strCurrencyName, bigExchangeRate);
                                    }
                                    request.getSession().setAttribute("hashConfiguredCurrency", hashConfiguredCurrency);*/

                        %>
                        <a href="ConfigureTenderCurrency.jsp?tenderid=<-%=tenderid%>">Edit</a>
                        <%//} else {
                                    // request.getSession().removeAttribute("hashConfiguredCurrency");
%>
                                                <a href="ConfigureTenderCurrency.jsp?tenderid=<-%=tenderid%>">Configure</a>
                        <%//}%>
                    </td>
                </tr>-->
                        <%          /*int allowCancel = 0;
                                    if (session.getAttribute("userTypeId") != null) {
                                    allowCancel = Integer.parseInt(session.getAttribute("userTypeId").toString());
                                    }*/
                                    boolean allowCancel = false;
                                    TenderCommonService commonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    for (SPTenderCommonData commonData : commonService1.returndata("CheckPE", tenderid, null)) {
                                        if (commonData.getFieldName1().equals(session.getAttribute("userId").toString())) {
                                            allowCancel = true;
                                        }
                                    }
                                    List<TblCancelTenderRequest> detailsCT = tenderSrBean.getDetailsCancelTender(Integer.parseInt(tenderid));
                                    if(!reTenderingLink2.isEmpty() && reTenderingLink2.get(0).getFieldName1().equals("1")){
                                        allowCancel = true;
                                    }
                                    if (allowCancel) {
                                        for (TblTenderDetails tblTenderDetails : tenderSrBean.getCancelTenDet(Integer.parseInt(tenderid))) {
                                            if (("Approved".equalsIgnoreCase(tblTenderDetails.getTenderStatus()) || "Cancelled".equalsIgnoreCase(tblTenderDetails.getTenderStatus())) && tblTenderDetails.getSubmissionDt() != null) {
                                                if (tblTenderDetails.getSubmissionDt().after(new Date())) {
                        %>
                        <tr>
                            <td class="t-align-left ff">Cancellation of Tender</td>
                            <td class="t-align-left">
                                <%if (detailsCT.isEmpty() && isCancelled) {
                                if(!checkuserRights.isEmpty())
                                {
                                %>
                                <a href="<%=request.getContextPath()%>/resources/common/ViewTender.jsp?id=<%=request.getParameter("tenderid")%>&action=cancel">Cancellation Process</a>
                                <%}} else {%>
                                <a href="javascript:void(0);" onclick="javascript:window.open('<%=request.getContextPath()%>/officer/ViewCancelTender.jsp?tenderid=<%=request.getParameter("tenderid")%>', '', 'width=350px,height=400px,scrollbars=1','');">View</a>
                                <%}%>
                            </td>
                        </tr>
                        <% }
                                            }
                                        }
                                    }
                                    if (!detailsCT.isEmpty()) {
                        %>
                        <tr>
                            <td class="t-align-left ff">Cancel Tender Workflow</td>
                            <%
                                                                    eventid = 8;
                                                                    activityid = 9;
                                                                    objectid = 0;
                                                                    childid = 1;
                                                                    wflevel = 1;
                                                                    initiator = 0;
                                                                    approver = 0;
                                                                    isInitiater = false;
                                                                    evntexist = false;
                                                                    fileOnHand = "No";
                                                                    checkperole = "No";
                                                                    chTender = "No";
                                                                    ispublish = false;
                                                                    iswfLevelExist = false;
                                                                    isconApprove = false;
                                                                    tenderStatus = "";
                                                                    tenderworkflow = "";
                                                                    istocmem = false;
                                                                    checksubmission = false;
                                                                    objtenderId = request.getParameter("tenderid");
                                                                    if (objtenderId != null) {
                                                                        objectid = Integer.parseInt(objtenderId);
                                                                        childid = objectid;
                                                                    }
                                                                    donor = "";
                                                                    donor = workFlowSrBean.isDonorReq(String.valueOf(eventid),
                                                                            Integer.valueOf(objectid));
                                                                    norevs = donor.split("_");
                                                                    norevrs = -1;

                                                                    strrev = norevs[1];
                                                                    if (!strrev.equals("null")) {

                                                                        norevrs = Integer.parseInt(norevs[1]);
                                                                    }

                                                                    evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
                                                                    tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
                                                                    // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);


                                                                    if (tblWorkFlowLevelConfig.size() > 0) {
                                                                        Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                                                        iswfLevelExist = true;
                                                                        while (twflc.hasNext()) {
                                                                            TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                                                            TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                                                            if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                                                                                fileOnHand = workFlowlel.getFileOnHand();
                                                                            }
                                                                            if (workFlowlel.getWfRoleId() == 1) {
                                                                                initiator = lmaster.getUserId();
                                                                            }
                                                                            if (workFlowlel.getWfRoleId() == 2) {
                                                                                approver = lmaster.getUserId();
                                                                            }
                                                                            if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                                                                                isInitiater = true;
                                                                            }

                                                                        }

                                                                    }

                                                                    field1 = "CheckCreator";
                                                                    editdata = workFlowSrBean.editWorkFlowData(field1, request.getParameter("tenderid"), userid);

                                                                    if (editdata.size() > 0) {
                                                                        checkperole = "Yes";
                                                                    }

                                                                    // for publish link

                                                                    tocmem = wfTenderCommonService.returndata("TocMemEncDec", request.getParameter("tenderid"), "");
                                                                    if (tocmem.size() > 0) {
                                                                        istocmem = true;
                                                                    }

                                                                    checkTender = wfTenderCommonService.returndata("CancelTenderWorkFlowStatus", request.getParameter("tenderid"), "'Pending'");
                                                                    if (checkTender.size() > 0) {
                                                                        chTender = "Yes";
                                                                    }
                                                                    publist = wfTenderCommonService.returndata("CancelTenderWorkFlowStatus", request.getParameter("tenderid"), "'Approved','Conditional Approval'");

                                                                    if (publist.size() > 0) {
                                                                        ispublish = true;
                                                                        SPTenderCommonData spTenderCommondata = publist.get(0);
                                                                        tenderStatus = spTenderCommondata.getFieldName1();
                                                                        tenderworkflow = spTenderCommondata.getFieldName2();

                                                                        if (tenderworkflow.equalsIgnoreCase("Conditional Approval")) {
                                                                            isconApprove = true;
                                                                        }
                                                                    }

                                                                    tenderDetails = workFlowSrBean.checkSubmissionDate(objectid);

                                                                    if (tenderDetails.size() > 0) {
                                                                        TblTenderDetails wfTenderDetails = tenderDetails.get(0);
                                                                        if (!"".equals(wfTenderDetails.getSubmissionDt()) && wfTenderDetails.getSubmissionDt() != null) {
                                                                            checksubmission = true;
                                                                        }
                                                                    }


                                                                    checkconfstatus = wfTenderCommonService.returndata("GetConfigStatus", request.getParameter("tenderid"), null);

                                                                    gtTednerDocAvlMethodLst = wfTenderCommonService.returndata("getTenderDocAvlMethod", request.getParameter("tenderid"), null);
                                                                    curDocAvlMethod = "";

                                                                    if (!gtTednerDocAvlMethodLst.isEmpty()) {
                                                                        curDocAvlMethod = gtTednerDocAvlMethodLst.get(0).getFieldName2();
                                                                    }

                            %>
                            <td class="t-align-left">

                                <%
                                                                        if (evntexist) {

                                                                            if (fileOnHand.equalsIgnoreCase("yes") && checkperole.equals("Yes") && ispublish == false && isInitiater == true) {
                                                                                if (isCancelled) {
                                %>
                               <!-- <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=916'>Edit</a>

                                &nbsp;|&nbsp;-->
                                <% }%>
                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=906">View</a>&nbsp;|&nbsp;

                                <% } else if (iswfLevelExist == false && checkperole.equals("Yes") && ispublish == false) {
                                                                                                                if (isCancelled) {
                                %>

                               <!-- <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=916'>Edit</a>
                                &nbsp;|&nbsp;-->
                                <% }%>
                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=906">View</a>&nbsp;|&nbsp;
                                <%  } else if (isCancelled && iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {%>

                                <label>Workflow yet not configured</label>
                                <%                                                              } else if (iswfLevelExist == true) {%>
                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=906">View</a>
                                &nbsp;|&nbsp;

                                <%
                                                                                                            }
                                                                                                        } else if (ispublish == false && checkperole.equals("Yes")) {
                                                                                                            List<CommonAppData> defaultconf = workFlowSrBean.editWorkFlowData("WorkFlowRuleEngine", Integer.toString(eventid), "");
                                                                                                            if (isCancelled && defaultconf.size() > 0) {
                                %>
                                <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&parentLink=925&submit=Submit&tc=tc">Create</a>
                                <%

                                                                                                                                    //if (Integer.parseInt(isFormOk.getFieldName1().trim()) == 5) {
%>

                                <%
                                                                                                                                    //  } else {
%>
                                <%--<a href="#" onclick="showFormMsg('<%= isFormOk.getFieldName1().trim()%>', '', '');">Create</a>--%>
                                <%
                                                                                                                                    //   }
                                                                                                                                } else {
                                                                                                                                    if (isCancelled) {
                                %>
                                <a  href="#" onclick="checkdefaultconf()">Create</a>
                                <%                                                          }
                                                                                                            }
                                                                                                        } else if (isCancelled && iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {%>
                                <label>Workflow yet not configured</label>

                                <% }
                                                                        //if (b_isPublish == false) {
                                                                            List<SPCommonSearchDataMore> statusWorkFlow = commonSearchDataMoreService.geteGPData("chkWorkFlowStatusForCancel", tenderid);
                                                                            if (isInitiater == true && (initiator != approver || (initiator == approver && norevrs > 0)) && fileOnHand.equalsIgnoreCase("Yes")) {
                                                                                if(!statusWorkFlow.isEmpty() && statusWorkFlow.get(0).getFieldName1().equalsIgnoreCase("No")){
                                %>
                                <%
                                    if (Integer.parseInt(isFormOk.getFieldName1().trim()) == 5) {
                                        if (!checksubmission) {%>
                                <a href="#" onclick="checksubmisDate()">Process file in Workflow</a> &nbsp;|&nbsp;
                                <% } else {
                                if(tecCommCheck != 0 && tcCommCheck!= 0 ){
                                %>
                                <a href="FileProcessing.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&fromaction=notice" >Process file in Workflow</a>&nbsp;| &nbsp;
                               <%
                                   }
                                  else{
                                    if(tecCommCheck == 0 && tcCommCheck == 0 ){ %>
                                      &nbsp;|&nbsp;  <a href="#" onclick="checkTecTc(3)">Process file in Workflow</a>
                                    <% }
                                       else if(tecCommCheck == 0){ %>
                                      &nbsp;|&nbsp;  <a href="#" onclick="checkTecTc(1)">Process file in Workflow</a>
                                    <% }
                                     else if(tcCommCheck == 0 ){ %>
                                      &nbsp;|&nbsp;  <a href="#" onclick="checkTecTc(2)">Process file in Workflow</a>
                                    <% }
                                     } 
                                 }
                                                                       } else {%>
                                <a href="#" onclick="showFormMsg('<%= isFormOk.getFieldName1().trim()%>', '', '');">Process file in Workflow</a>
                                <% }%>
                                <%                                         } }
                                                                           //}
                                                                           if (iswfLevelExist) {%>

                                <a href="workFlowHistory.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&userid=<%=uid%>&fraction=notice&parentLink=75" >View Workflow History</a>

                                <% }%>
                            </td>
                        </tr>
                        <% }%>
                    </tbody></table>
                    <%-- comment out for eGP Bhutan
                    <%  //rishita -download - remove - upload functionality
                                //start of code
                                int Srno = 1;
                                List<SPTenderCommonData> downloadDocs = wfTenderCommonService.returndata("TenderEngEstDocs", request.getParameter("tenderid"), null);

                                if (procureNature.equalsIgnoreCase("Works") && false ) { //condition false for hide the following section
                    %>
                <table class="tableList_1 t_space" cellspacing="0" width="100%">

                    <tbody>
                        <tr>
                            <th class="t-align-left" colspan="6">APP Official Cost Estimate</th>
                        </tr>
                        <tr>
                            <th class="t-align-left" width="4%">Sl. No.</th>
                            <th class="t-align-left" width="26%">File Name</th>
                            <th class="t-align-left" width="32%">File Description</th>
                            <th class="t-align-left" width="9%">File Size<br>
                                (in KB)</th>
                            <th class="t-align-left" width="9%">Status</th>

                            <th class="t-align-left" width="16%">Action</th>
                        </tr>
                        <%
                                                            if (downloadDocs.isEmpty()) {%>
                        <tr>
                            <td colspan="6" class="mandatory t-align-center">No records found</td>
                        </tr>
                        <% }
                                                            for (SPTenderCommonData detailsDoc : downloadDocs) {%>
                        <tr>
                            <td class="t-align-center"><%=Srno%></td>
                            <td class="t-align-left"><%=detailsDoc.getFieldName1()%></td>
                            <td class="t-align-left"><%=detailsDoc.getFieldName2()%></td>
                            <td class="t-align-center"><%=detailsDoc.getFieldName3()%></td>

                            <td class="t-align-center">Approved</td>
                            <td class="t-align-center">
                                <a href="<%=request.getContextPath()%>/NoticeDocFunc?docName=<%=detailsDoc.getFieldName1()%>&appId=<%=detailsDoc.getFieldName5()%>&docId=<%=detailsDoc.getFieldName4()%>&docSize=<%=detailsDoc.getFieldName3()%>&funName=download&tenderid=<%=request.getParameter("tenderid")%>" ><img src="../resources/images/Dashboard/Download.png" /></a> &nbsp; &nbsp;
    <!--bugid ::0004723                            <a href="<%=request.getContextPath()%>/NoticeDocFunc?docName=<%=detailsDoc.getFieldName1()%>&appId=<%=detailsDoc.getFieldName5()%>&docId=<%=detailsDoc.getFieldName4()%>&desc=<%=detailsDoc.getFieldName2()%>&funName=remove&tenderid=<%=request.getParameter("tenderid")%>"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>-->
                            </td>
                        </tr>
                        <% Srno++;
                                                            }
                                                            //rishita -download - remove - upload functionality
                                                            //end of code
                        %>
                    </tbody></table>
                    <% }%>  --%>
            </div>
            <script>
                function docMaped(){
                    jAlert("Please configure key information."," Alert ", "Alert");
                    return false;
                }
                function checkGrandSummaryCreated(grandSumId)
            {
                if(<%=!(eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("1 stage-TSTM"))%>){
                alert("Grand Summary is not yet created. It is mandatory for BOR/BER Report Generation.","Alert","Alert");
                return false;
                }
            }
            </script>
        </div>
        <br/>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%
                    tenderSrBean = null;
        %>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
<script type="text/javascript">
function isPublished(tenderId)
{
 var flag = false;
 $.ajax({
    type: "POST",
    url: "<%=request.getContextPath()%>/TenderDetailsServlet",
    data:"funName=checkTenderStatus&tenderId="+tenderId,
    async: false,
    success: function(j){
        if($.trim(j.toString())=="Approved"){
            flag = false;
            alert("Tender is already published!!!");
        }else{
            flag = true;
        }
    }
});
return flag;
}
</script>
</html>