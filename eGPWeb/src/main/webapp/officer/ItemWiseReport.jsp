<%-- 
    Document   : ItemWiseReport
    Created on : Jan 6, 2011, 8:06:00 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.model.table.TblBidderRank"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.web.databean.ReportGenerateDtBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblReportColumnMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ReportCreationSrBean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Item wise Report</title>
    </head>
    <body>
        <%
            boolean isTos=false;
            if("y".equals(request.getParameter("istos"))){
                isTos=true;
            }
            String tenderid = request.getParameter("tenderid");
            String repId = request.getParameter("repId");
            ReportCreationSrBean rcsb = new ReportCreationSrBean();
            Object data[] = rcsb.getReportTabIdNoOfCols(repId);
            int reportTableId = Integer.parseInt(data[0].toString());
            int noOfCols = Integer.parseInt(data[1].toString());
            java.util.List<TblReportColumnMaster> list = rcsb.getReportColumns(reportTableId);
            long mainCnt = rcsb.getCountForItemDisplay(repId);
            List<Object[]> objects = rcsb.getItemWiseReportHeader(repId);
            ListIterator<Object[]> mainObj = objects.listIterator();
            int calCnt = objects.size()/(int)mainCnt;
            List<TblBidderRank> ranks = rcsb.getItemWiseReport(tenderid, repId, session.getAttribute("userId").toString());
            pageContext.setAttribute("tenderId", tenderid);
        %>    
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="pageHead_1">Tender Item wise Report<span style="float: right;"><a class="action-button-goback" href="">Go back to Dashboard</a></span></div>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <div class="tabPanelArea_1">
            <table class="tableList_1" width="100%" cellspacing="0">
                <%
                    for(int i=0;i<calCnt;i++){
                %>
                <tr>
                    <%
                        int rowId=0;
                        int tableId=0;
                        for(int j=0; j<mainCnt;j++){
                            if(mainObj.hasNext()){
                                Object[] o1 = mainObj.next();
                                out.print("<th>"+o1[0]+"</th>");
                                rowId=(Integer)o1[1];
                                tableId=(Integer)o1[2];
                            }
                        }
                    %>
                </tr>
                <tr>
                    <td>
                        <table class="tableList_1" width="100%" cellspacing="0">
                            <tbody>
                                <tr>

                                    <th colspan="<%=noOfCols + noOfCols%>" class="t-align-left ff"><%=data[3]%></th>
                                </tr>
                                <tr>
                                    <%
                                        for (TblReportColumnMaster master : list) {
                                            if(isTos){
                                            if (master.getFilledBy() != 3){
                                    %>
                        <th class="t-align-left ff"><%=master.getColHeader()%></th>
                        <%}}else{out.print("<th class='t-align-left ff'>"+master.getColHeader()+"</th>");}%>
                                    <%}%>
                                </tr>
                                <%
                                    for(TblBidderRank tbr : ranks){
                                        if(tbr.getRowId()==rowId && tbr.getTableId()==tableId){
                                            out.print("<tr>");
                                            for (TblReportColumnMaster master : list) {
                                                if (master.getFilledBy() == 1) {
                                                    out.print("<td>"+tbr.getCompanyName()+"</td>");
                                                }
                                                if (master.getFilledBy() == 2) {
                                                    out.print("<td>"+tbr.getAmount().setScale(2,0)+"</td>");
                                                }
                                                if (master.getFilledBy() == 3 && !isTos) {
                                                    out.print("<td>"+tbr.getRank()+"</td>");
                                                }
                                            }
                                            out.print("</tr>");
                                        }
                                    }
                                %>
                                <tr>
                                    <td colspan="<%=noOfCols + noOfCols%>" class="t-align-left formSubHead_1"><%=data[4]%></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <%}%>
            </table>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>

       <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
<%
 rcsb=null;
 data=null;
 list=null;
 objects=null;
 mainObj=null;
 ranks=null;
%>