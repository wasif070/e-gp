<%--
    Document   : TECProcess
    Created on : Dec 15, 2010, 6:37:01 PM
    Author     : Administrator
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluation Process</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">

            $(document).ready(function() {

                //Default Action
                $(".tabPanelArea_3").hide(); //Hide all content
                //$("ul.tabPanel_3 li:first").addClass("sMenu").show(); //Activate first tab
                $(".tabPanelArea_3:first").show(); //Show first tab content

                $('tab1').show();

                //On Click Event
                $("ul.tabPanel_1 li").click(function() {
                    $("ul.tabPanel_1 li").removeClass("sMenu"); //Remove any "active" class
                    $(this).addClass("sMenu"); //Add "active" class to selected tab
                    $(".tabPanelArea_3").hide(); //Hide all tab content
                    var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content

                    if(activeTab=='#tab1')
                    {
                        $('tab1').hide();
                        $('tab2').show();
                        //document.getElementById(activeTab.replace("#","")).innerHTML='This is first Tab';
                    }
                    else if (activeTab=='#tab2')
                    {
                        //document.getElementById(activeTab.replace("#","")).innerHTML='This is second Tab';
                        $('tab2').hide();
                        $('tab1').show();
                    }

                    $(activeTab).fadeIn(); //Fade in the active content
                    //return false;
                });

            });
        </script>
    </head>
    <body>

        <%  String tenderId = "", userId = "";
                    tenderId = request.getParameter("tenderId");
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userId = hs.getAttribute("userId").toString();
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Evaluation Process
                <span class="c-alignment-right"><a href="EvalComm.jsp?tenderid=<%=tenderId%>" class="action-button-goback">Go back</a></span>
                </div>
                <%
                            String msg = "";
                            String msgId="";
                            String msgTxt="";
                            msgId=request.getParameter("msgId");
                            msg = request.getParameter("msg");
                %>

                <% if (msg != null && msg.contains("true")) {%>
                <div class="responseMsg successMsg t_space">Validity Extension Request Sent Successfully</div>
                <%  } else if(msgId != null && msgId.equalsIgnoreCase("released")) {%>
                <div class="responseMsg successMsg t_space">Payment released successfully.</div>
                <%  } else if(msgId != null && msgId.equalsIgnoreCase("canceled")) {%>
                <div class="responseMsg successMsg t_space">Payment canceled successfully.</div>
                <%  } else if(msgId != null && msgId.equalsIgnoreCase("forfeited")) {%>
                <div class="responseMsg successMsg t_space">Payment forfeited successfully.</div>
                <%  } else if(msgId != null && msgId.equalsIgnoreCase("forfeitrequested")) {%>
                <div class="responseMsg successMsg t_space">Payment forfeit request submitted successfully.</div>
                <%  } else if(msgId != null && msgId.equalsIgnoreCase("releaserequested")) {%>
                <div class="responseMsg successMsg t_space">Payment release request submitted successfully.</div>
                <%  }%>

                <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
                <%//if (!"TEC".equalsIgnoreCase(request.getParameter("comType"))) {%>
                <jsp:include page="officerTabPanel.jsp" >
                    <jsp:param name="tab" value="7" />
                </jsp:include>
                <%//}%>
                <div class="tabPanelArea_1">
                    <%
                                pageContext.setAttribute("TSCtab", "5");
                                pageContext.setAttribute("comType", request.getParameter("comType"));
                    %>
                   
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td class="table-section-header_title"><strong>Tender validity   /   Security extension</strong></td>
                        </tr>
                        <%
                                    List<SPTenderCommonData> listExtend = tenderCommonService.returndata("showExtendLink", tenderId, null);
                                  // if-else condition commented out to enable tender validity extend option for temporary situation.
                                    // if (!listExtend.isEmpty() && listExtend.size() > 0) {
                        %>
                        <%//} else {
                                                                //Checking TEC memeber
                                                                List<SPTenderCommonData> checkTEC = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, userId);
                                                                if (!checkTEC.isEmpty()) {

                                                                    //Checking Tender Submission Date should be less than current Date then only show Extend link

                                                                    //Checking atleast one bidder has accpeted the Extension request then only show Extend link
                                                                    List<SPTenderCommonData> checkSubmissionDtAndBidderAccpt = tenderCommonService.returndata("CheckSubmissionDateAndAcceptStatus", tenderId, null);
                                                                    if (!checkSubmissionDtAndBidderAccpt.isEmpty()) {
                                                                        /*if (checkSubmissionDtAndBidderAccpt.get(0).getFieldName1().equals("0")) {
 *                                                                           As per discusion with urmil no need to chek the date of validitiy and security. 01/05/2012 comments by nishit   
 *                                                                      */
                        %>
                        <tr>
                            <td colspan="5" class="t-align-left ff">Tender/  Validity - <a href="TenderExtReq.jsp?tenderId=<%=tenderId%>">Extend</a> </td>
                        </tr>
                        <%/*}*/
                                            }
                                        }
                                    //}%>
                    </table>
                    <%
                                List<SPTenderCommonData> listPEUser = tenderCommonService.returndata("TenExtReqData", tenderId, null);
                                if (!listPEUser.isEmpty() && listPEUser.size() > 0) {
                    %>

                    <div class="t_space" style="width: 100%; float: left;">
                        <ul class="tabPanel_1 t_space">
                            <li class="sMenu"><a href="#tab1">Pending</a></li>
                            <li class=""><a href="#tab2">Processed</a></li>
                        </ul>
                    </div>
                    <div class="tabPanelArea_3" id="tab1" style="width: 100%;display: block; float: left;">
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="4%" class="t-align-center">Sl. No. </th>
                                <th width="20%" class="t-align-center">Extension No</th>
                                <th width="57%" class="t-align-center">Extension Request By</th>
                                <th width="19%" class="t-align-center">Action</th>
                            </tr>
                            <%
                                                                int i = 1;
                                                                TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                                                                List<SPTenderCommonData> listas = tenderCommonService1.returndata("ValidityExtensionRequestPending", userId, tenderId);
                                                                if (!listas.isEmpty()) {
                                                                    for (SPTenderCommonData sptcd : listas) {
                            %>
                            <tr>
                                <td class="t-align-center"><%=i%></td>
                                <td class="t-align-center">Latest Extension </td>
                                <td class="t-align-center"><%=sptcd.getFieldName4()%></td>
                                <td class="t-align-center"><a href="TenExtReqView.jsp?tenderId=<%=sptcd.getFieldName1()%>&ExtId=<%=sptcd.getFieldName6()%>">View</a></td>
                            </tr>
                            <%

                                                                                                    i++;
                                                                                                }
                                                                                            } else {%>
                            <tr>
                                <td class="t-align-left" colspan="4">No Record Found</td>
                            </tr>
                            <%}
                            %>
                        </table>
                    </div>
                    <div class="tabPanelArea_3" id="tab2" style="width: 100%;display: none; float: left;">
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="4%" class="t-align-center">Sl. No. </th>
                                <th width="20%" class="t-align-center">Extension No</th>
                                <th width="57%" class="t-align-center">Extension Request By</th>
                                <th width="19%" class="t-align-center">Action</th>
                            </tr>
                            <%
                                                                int j = 1;

                                                                List<SPTenderCommonData> list = tenderCommonService.returndata("ValidityExtensionRequestApproved", userId, tenderId);
                                                                if (!list.isEmpty()) {
                                                                    for (SPTenderCommonData sptcd : list) {
                            %>
                            <tr>
                                <td class="t-align-center"><%=j%></td>
                                <%if (j == 1) {%>
                                <td class="t-align-center">Tender Security</td>
                                <%} else {%>
                                <td class="t-align-center"><%=(j - 1)%>-Extension </td>
                                <%}%>
                                <td class="t-align-center"><%=sptcd.getFieldName4()%></td>
                                <td class="t-align-center"><a href="TenExtReqView.jsp?tenderId=<%=sptcd.getFieldName1()%>&ExtId=<%=sptcd.getFieldName6()%>">View</a></td>
                            </tr>
                            <%

                                                                                                    j++;
                                                                                                }
                                                                                            } else {%>
                            <tr>
                                <td class="t-align-left" colspan="4">No Record Found</td>
                            </tr>
                            <%}
                            %>
                        </table>
                    </div>




                    <div>&nbsp;</div>
                    <%                                                    } else {
                    %>
                    <div class="tabPanelArea_1">
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr><td>No Record Found.</td></tr>

                        </table>
                    </div>
                    <%}%>


                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td class="table-section-header_title"><strong>Tender Bidder List </strong></td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th width="4%" class="t-align-center">Sl. No. </th>
                            <th width="61%" class="t-align-center">Company Name</th>
                            <th width="18%" class="t-align-center">Payment Date</th>
                            <th width="19%" class="t-align-center">Action</th>
                        </tr>
                        <%
                                    int n = 1;
                                    TenderCommonService tenderCommonService2 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                                    List<SPTenderCommonData> listas1 = tenderCommonService2.returndata("GetBidderDefaultList", tenderId, null);
                                    if (!listas1.isEmpty()) {
                                        for (SPTenderCommonData sptcd : listas1) {
                        %>
                        <tr>
                            <td class="t-align-center"><%=n%></td>
                            <td class="t-align-center"><%=sptcd.getFieldName1()%></td>
                            <td class="t-align-center"><%=sptcd.getFieldName4()%></td>
                            <%if (!sptcd.getFieldName7().equalsIgnoreCase("offline")) {%>
                            <td class="t-align-center">
<!--                                <a href="../partner/MakePaymentView.jsp?payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts">View</a>
                                | <a href="../partner/MakePaymentView.jsp?action=requestforfeit&payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts">Forfeit</a>
                                | <a href="../partner/MakePaymentView.jsp?action=requestrelease&payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts">Release</a>-->

                                <a href="../partner/TenderPaymentDetails.jsp?payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts">View</a>
<!--                                | <a href="../partner/TenderPaymentReleaseForfeit.jsp?action=requestforfeit&payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts">Forfeit</a>
                                | <a href="../partner/TenderPaymentReleaseForfeit.jsp?action=requestrelease&payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts">Release</a>-->
                            </td>
                            <%} else {%>
                            <td class="t-align-center">
<!--                                <a href="../partner/TenderPaymentReleaseForfeit.jsp?action=forfeit&payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts">Forfeit</a>
                                | <a href="../partner/TenderPaymentReleaseForfeit.jsp?action=release&payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ts">Release</a>-->
                            </td>
                            <%}%>
                        </tr>
                        <%

                                                                                                n++;
                                                                                            }
                                                                                        } else {%>
                        <tr>
                            <td class="t-align-center" colspan="4">No Record Found</td>
                        </tr>
                        <%}
                        %>
                    </table>

                    <div>&nbsp;</div>


                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td class="table-section-header_title"><strong>Tender Performance Security </strong></td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th width="4%" class="t-align-center">Sl. No. </th>
                            <th width="61%" class="t-align-center">Company Name</th>
                            <th width="18%" class="t-align-center">Payment Date</th>
                            <th width="19%" class="t-align-center">Action</th>
                        </tr>
                        <%
                                    int i = 1;
                                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                                    List<SPTenderCommonData> listas = tenderCommonService1.returndata("GetPerformanceSecurityList", tenderId, null);
                                    if (!listas.isEmpty()) {
                                        for (SPTenderCommonData sptcd : listas) {
                        %>
                        <tr>
                            <td class="t-align-center"><%=i%></td>
                            <td class="t-align-center"><%=sptcd.getFieldName1()%></td>
                            <td class="t-align-center"><%=sptcd.getFieldName4()%></td>
                            <td class="t-align-center">
<!--                                <a href="../partner/MakePaymentView.jsp?action=forfeit&payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ps">Forfeit</a> |
                                <a href="../partner/MakePaymentView.jsp?action=release&payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ps">Release</a></td>-->
<!--                                <a href="../partner/TenderPaymentReleaseForfeit.jsp?action=forfeit&payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ps">Forfeit</a> |
                                <a href="../partner/TenderPaymentReleaseForfeit.jsp?action=release&payId=<%=sptcd.getFieldName3()%>&uId=<%=sptcd.getFieldName5()%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=sptcd.getFieldName6()%>&payTyp=ps">Release</a>-->
                            </td>
                        </tr>
                        <%

                                                                                                i++;
                                                                                            }
                                                                                        } else {%>
                        <tr>
                            <td class="t-align-center" colspan="4">No Record Found</td>
                        </tr>
                        <%}
                        %>
                    </table>
                </div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>

    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
