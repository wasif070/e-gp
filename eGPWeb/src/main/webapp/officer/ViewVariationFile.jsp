<%--
    Document   : ViewCTerminationWFFile
    Created on : Aug 31, 2011, 6:28:44 PM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsTrackVariation"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsContractTerminationService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <% ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");%>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Variation Order</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
    </head>
    <body>
        <%
            ConsolodateService ConsolodateService =
                            (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            String childid = "";
            if(request.getParameter("childid")!=null)
            {
                childid = request.getParameter("childid");
            }
            String tenderId = "";
            if(request.getParameter("tenderId")!=null && !"null".equalsIgnoreCase(request.getParameter("tenderId")))
            {

                tenderId = request.getParameter("tenderId");
                pageContext.setAttribute("tenderId", tenderId);
            }
            else
            {
                List<Object[]> object= ConsolodateService.getlotIdandTenderID(Integer.parseInt(childid));
                if(!object.isEmpty())
                {
                    Object[] obj = object.get(0);
                    tenderId = obj[0].toString();
                    pageContext.setAttribute("tenderId", tenderId);
                }
            }
            String lotId = "";
            if(request.getParameter("lotId")!=null && !"null".equalsIgnoreCase(request.getParameter("lotId")))
            {
                lotId = request.getParameter("lotId");
                pageContext.setAttribute("lotId", lotId);
            }
            else
            {                
                List<Object[]> object= ConsolodateService.getlotIdandTenderID(Integer.parseInt(childid));
                if(!object.isEmpty())
                {
                    Object[] obj = object.get(0);
                    lotId = obj[1].toString();
                    pageContext.setAttribute("lotId", lotId);
                }
            }
            
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%--<%@include  file="../resources/common/AfterLoginTop.jsp"%>--%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1 t_space">
                <div class="pageHead_1">Contract Details</div>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                <table width="100%" cellspacing="0" class="t_space">
                    <tr><td  style="margin-left:20% ;background-color: #FFFF99" width="5%"></td><td>&nbsp;&nbsp;New Items added in Original BSR</td></tr><tr><td>&nbsp;</td></tr><tr><td style="background-color: #CDC0B0" width="5%"></td><td>&nbsp;&nbsp;Items Updated in Original BSR</td>
                        <td colspan="3" style="text-align: right;">&nbsp;</td>
                    </tr>
                </table>    
                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                <tr>
                    <th width="3%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                    <th width="10%" class="t-align-center"><%=bdl.getString("CMS.group")%></th>
                    <th width="10%" class="t-align-center"><%=bdl.getString("CMS.desc")%></th>
                    <th width="10%" class="t-align-center"><%=bdl.getString("CMS.UOM")%>
                    </th>
                    <th width="10%" class="t-align-center"><%=bdl.getString("CMS.qty")%>
                    </th>
                    <th width="10%" class="t-align-center"><%=bdl.getString("CMS.rate")%>
                    </th>
                    <th width="23%" class="t-align-center"><%=bdl.getString("CMS.sdate")%>
                    </th>
                    <th width="23%" class="t-align-center"><%=bdl.getString("CMS.edate")%>
                    </th>
                </tr>
                <%
                     String styleClass = "";
                     List<TblCmsTrackVariation> list = ConsolodateService.getVariOrder(childid);
                     if(!list.isEmpty())
                     {
                         for(int i=0; i<list.size(); i++)
                         {                             
                %>
                <tr <%if(list.get(i).getFlag().equalsIgnoreCase("I")){out.print("class='" + styleClass + "' style=background-color:#FFFF99");}else{out.print("class='" + styleClass + "' style=background-color:#CDC0B0");}%>>
                            <td class="t-align-center"><%=list.get(i).getSrno()%></td>
                            <td class="t-align-center"><%=list.get(i).getGroupname()%></td>
                            <td class="t-align-center"><%=list.get(i).getDescription()%></td>
                            <td class="t-align-center"><%=list.get(i).getUom()%></td>
                            <td class="t-align-center"><%=list.get(i).getQty()%></td>
                            <td class="t-align-center"><%=list.get(i).getRate()%></td>
                            <td class="t-align-center"><%=DateUtils.customDateFormate(DateUtils.convertStringtoDate(list.get(i).getStartDate().toString(),"yyyy-MM-dd HH:mm:ss"))%></td>
                            <td class="t-align-center"><%=DateUtils.customDateFormate(DateUtils.convertStringtoDate(list.get(i).getEndDate().toString(),"yyyy-MM-dd HH:mm:ss"))%></td>
                </tr>
                <%
                         }
                     }
                %>
                </table>
                 <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                <tr>
                                                    <td class="t_align_left ff" width="10%">BOQ Forms</td>
                                                    <td class="t_align_left"><a target="_blank" href="ViewForms.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&goback=false">View</a></td>
                                                </tr>

                                            </table>
            </div>
            
        </div>

    </body>
</html>
