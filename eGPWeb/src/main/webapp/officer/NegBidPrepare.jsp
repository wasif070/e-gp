<%--
    Document   : SeekEvalClari
    Created on : Dec 30, 2010, 4:36:51 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.NegotiationSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<jsp:useBean id="negotiationProcessSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Negotiation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="dashboard_div">
            <%  String tenderId;
                tenderId = request.getParameter("tenderId");

                String userId = "", usrId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                if (request.getParameter("uId") != null && !"".equalsIgnoreCase(request.getParameter("uId"))) {
                    usrId = request.getParameter("uId");
                }

                int negId = 0;
                if(request.getParameter("negId")!=null){
                    negId = Integer.parseInt(request.getParameter("negId"));
                }
                String bidid="",type="";
                int isBidModify = 0;
                
                String finalStatus = "";
                finalStatus = negotiationProcessSrBean.getFinalSubStatus(negId);
            %>
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">
                    Negotiation
                    <span style="float: right; text-align: right;">
                        <a class="action-button-goback" href="NegotiationProcess.jsp?tenderId=<%=tenderId%>&usrId=<%=userId%>" title="Bid Dashboard">Go Back To Dashboard</a>
                    </span>
                </div>
                <%
                    pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%
                       boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                %>
                <div>&nbsp;</div>
                <%if(!"TEC".equalsIgnoreCase(request.getParameter("comType"))) { %>
                <jsp:include page="officerTabPanel.jsp" >
                     <jsp:param name="tab" value="7" />
                </jsp:include>
                <% } %>
                <div class="tabPanelArea_1">
                <%
                     pageContext.setAttribute("TSCtab", "7");
                %>
                <%@include  file="../resources/common/AfterLoginTSC.jsp"%>
                    <%
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            if (isTenPackageWis) {
                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                    %>
                   <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td colspan="2" class="t-align-center ff">Tender/Proposal Details</td>
                        </tr>

                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="22%" class="t-align-left ff">Form Name</th>
                            <th width="78%" class="t-align-left">Action</th>
                        </tr>
                        <%
                            for (SPCommonSearchData formdetails : commonSearchService.searchData("GetNegTenderFormsByLotId", tenderId, usrId, "0", usrId, null, ""+negId, null, null, null)) {
                        %>
                        <tr>
                            <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                            <td class="t-align-left">
                               <% if(formdetails.getFieldName6()==null && "".equals(finalStatus)){ %>
                                    <a href="NegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=formdetails.getFieldName3()%>&uId=<%=usrId%>&lotId=0&action=Edit&negId=<%=negId%>">Edit</a>
                               <% }else{
                                    isBidModify++;
                                    if("".equals(finalStatus)){
                               %>
                                    <a href="NegModifyBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=formdetails.getFieldName3()%>&uId=<%=usrId%>&lotId=0&action=Edit&negId=<%=negId%>">Edit</a>&nbsp;|&nbsp;
                                    <% } 
                                       for (SPCommonSearchData negoformdetails : commonSearchService.searchData("GetNegBidderFormsData", tenderId, usrId,formdetails.getFieldName1() , null, null,null, null, null,null)) 
                                        {
                                              if(negoformdetails.getFieldName4() != null)
                                            {
                                                type ="negbiddata";

                                            }
                                            else
                                            {
                                                type ="biddata";
                                            }  
                                              bidid=negoformdetails.getFieldName3();
                                        }
                                    %>
                                    <% if(formdetails.getFieldName6()==null){ %>
                                        <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=bidid%>&uId=<%=usrId%>&lotId=0&action=View&type=biddata&negId=<%=negId%>">View</a>
                                    <% }else{ %>
                                        <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=bidid%>&uId=<%=usrId%>&lotId=0&action=View&type=negbiddata&negId=<%=negId%>">View</a>
                                    <% } %>
                               <%
                                  } %>
                            </td>
                        </tr>
                        <% }%>
                    </table>
                        <% }
                             } else {
                                 for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-center ff">Tender/Proposal Details</td>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%  }
                            for (SPCommonSearchData lotList : commonSearchService.searchData("gettenderlotbytenderid", tenderId, "", "", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                            <tr>
                                <td width="22%" class="t-align-left ff">Lot No. :</td>
                                <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Lot Description :</td>
                                <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="22%" class="t-align-left ff">Form Name</th>
                                <th width="23%" class="t-align-left">Action</th>
                            </tr>
                            <%
                                 for (SPCommonSearchData formdetails : commonSearchService.searchData("GetNegTenderFormsByLotId", tenderId, usrId, lotList.getFieldName3(), usrId, null, null, ""+negId, null, null)) {
                            %>
                            <tr>
                                <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                                <td class="t-align-left ff">
                                    <% if(formdetails.getFieldName6()==null){ %>
                                    <a href="NegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=formdetails.getFieldName3()%>&lotId=<%=lotList.getFieldName3()%>&uId=<%=usrId%>&action=Edit&negId=<%=negId%>">Edit</a>
                               <% }else{
                                    isBidModify++;
                                     if("".equalsIgnoreCase(finalStatus)){
                                %>
                                    <a href="NegModifyBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=formdetails.getFieldName3()%>&uId=<%=usrId%>&lotId=<%=lotList.getFieldName3()%>&action=Edit&negId=<%=negId%>">Edit</a>&nbsp;|&nbsp;
                                     <% } 
                                        for (SPCommonSearchData negoformdetails : commonSearchService.searchData("GetNegBidderFormsData", tenderId, usrId,formdetails.getFieldName1() , null, null,null, null, null,null)) 
                                        {
                                              if(negoformdetails.getFieldName4() != null)
                                            {
                                                type ="negbiddata";

                                            }
                                            else
                                            {
                                                type ="biddata";
                                            }  
                                              bidid=negoformdetails.getFieldName3();
                                        }
                                    %>
                                    <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=bidid%>&uId=<%=usrId%>&lotId=0&action=View&negId=<%=negId%>&type=<%=type%>">View</a>
                               <% } %>
                                </td>
                            </tr>
                            <% }%>
                        </table>
                        <%     }
                                    }
                        %>
                        <% if(isBidModify != 0 && "".equalsIgnoreCase(finalStatus)){ %>
                     <table width="100%" cellspacing="0">
                        <tr>
                            <td colspan="2" class="t-align-center ff">
                                <form name="frmNegotiateBid" id="idfrmNegotiateBid" method="post" action="<%=request.getContextPath()%>/NegotiationSrBean">
                                    <input type="hidden" name="action" id="action" value="NegotiateBid" />
                                    <input type="hidden" name="hidtenderId" id="hidtenderId" value="<%=request.getParameter("tenderId")%>" />
                                    <input type="hidden" name="hidnegId" id="hidnegId" value="<%=request.getParameter("negId")%>" />
                                    <label class="formBtn_1 t_space">
                                        <input type="submit" name="btnsubmit" id="idbtnsubmit" value="Submit" />
                                    </label>
                                </form>
                            </td>
                        </tr>
                     </table>
                     <% } %>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
