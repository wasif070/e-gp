<%--
Document   : APP Pachakge Details
Created on : Oct 23, 2010, 6:13:00 PM
Author     : Sanjay
--%>
<%@page import="java.math.MathContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblAppFrameworkOffice"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.APPService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page buffer="10kb"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>APP Package Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
    </head>
    <jsp:useBean id="appServlet" scope="request" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <jsp:useBean id="appDtBean" scope="request" class="com.cptu.egp.eps.web.databean.APPDtBean"/>
    <jsp:useBean id="appViewDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AppViewPkgDtBean"/>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%  APPService appService = (APPService) AppContext.getSpringBean("APPService");
                            java.util.List<TblAppFrameworkOffice> appListFrameworkOfc = new java.util.ArrayList<TblAppFrameworkOffice>();
                            boolean isEdit = false;
                            boolean isRevision = false;

                            if (request.getParameter("action") != null) {
                                if ("Edit".equalsIgnoreCase(request.getParameter("action"))) {
                                    isEdit = true;
                                }
                            }
                            if (request.getParameter("action") != null) {
                                if ("Revise".equalsIgnoreCase(request.getParameter("action"))) {
                                    isEdit = true;
                                    isRevision = true;
                                }
                            }
                        %>
                        <%              long chkdates = 0;
                            int prjId = 0;
                            int appId = -1;

                            if (request.getParameter("appId") != null) {
                                appId = Integer.parseInt(request.getParameter("appId"));
                            }
                            int pkgId = 0;

                            if (request.getParameter("pkgId") != null) {
                                pkgId = Integer.parseInt(request.getParameter("pkgId"));
                            }
                            String logUserId = "0";

                            if (session.getAttribute("userId") != null) {
                                logUserId = session.getAttribute("userId").toString();
                            }
                            appServlet.setLogUserId(logUserId);
                        %>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td>
                                        <div class="pageHead_1">Package Details <span class="c-alignment-right"><a href="APPDashboard.jsp?appID=<%=appId%>" class="action-button-goback"> Go Back To Dashboard</a></span></div>

                                        <div class="stepWiz_1 t_space">
                                            <ul>
                                                <%
                                                    AppAdvSearchService appAdvSearch = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
                                                    appAdvSearch.setLogUserId(logUserId);
                                                    chkdates = appAdvSearch.countForQuery("TblAppPqTenderDates", "appid=" + appId + "and packageId=" + pkgId);
                                                    if (!isEdit) {%>
                                                <li>Create APP</li>
                                                <li class="sMenu">&gt;&gt;&nbsp;&nbsp;Add Package Detail</li>
                                                <li>&gt;&gt;&nbsp;&nbsp;
                                                    <%
                                                        if (chkdates != 0) {%>
                                                    <a style="text-decoration: underline;" href="EditPackageDates.jsp?appId=<%=appId%>&pkgId=<%=pkgId%>">Edit Package Dates</a>
                                                    <%  } else { %>
                                                    Add Package Dates
                                                    <% } %>
                                                </li>
                                                <%} else {%>
                                                <li>APP</li>
                                                    <% if (isRevision) { %>
                                                <li class="sMenu">&gt;&gt;&nbsp;&nbsp;Revise Package Detail</li>
                                                    <% } else { %>
                                                <li class="sMenu">&gt;&gt;&nbsp;&nbsp;Edit Package Detail</li>
                                                    <% } %>
                                                <li>&gt;&gt;&nbsp;&nbsp;
                                                    <%  if (chkdates != 0) { %>
                                                    <% if (isRevision) {%>
                                                    <a style="text-decoration: underline;" href="EditPackageDates.jsp?appId=<%=appId%>&pkgId=<%=pkgId%>&action=Revise">Revise Package Dates</a>
                                                    <% } else {%>
                                                    <a style="text-decoration: underline;" href="EditPackageDates.jsp?appId=<%=appId%>&pkgId=<%=pkgId%>">Edit Package Dates</a>
                                                    <% } %>

                                                    <%  } else { %>
                                                    Add Package Dates
                                                    <% } %>
                                                </li>
                                                <%}%>
                                            </ul>
                                        </div>
                                        <form id="frmRedirect" name="frmRedirect" method="post">
                                            <input type="hidden" name="appid" id="appid" value="<%=appId%>"/>
                                        </form>
                                        <%
                                            boolean isAddDatesConf = false;
                                            if ("Next".equalsIgnoreCase(request.getParameter("btnNext")) || "Update".equalsIgnoreCase(request.getParameter("update")) || "Revise".equalsIgnoreCase(request.getParameter("btnRevise"))) {
                                                try {
                                                    String procNature = "";
                                                    String serviceType = "";
                                                    String emeType = "";
                                                    String pkgNo = "";
                                                    String pkgDesc = "";
                                                    String W1 = "", W2 = "", W3 = "", W4 = "";
                                                    String WC1 = "", WC2 = "", WC3 = "", WC4 = "";
                                                    String bidCategory = "";
                                                    String workCategory = "";
                                                    //String depoplanWork = "";
                                                    String DepoPlanWork = "";
                                                    String entrustingAgency = "";
                                                    String timeFrame = "";
                                                    float aloBud = 0;
                                                    float estCost = 0;
                                                    String cpvCode = "";
                                                    float pkgEstCost = 0;
                                                    int aaEmpId = 0;
                                                    String isPQReq = "";
                                                    String reoiRfa = "";
                                                    int procMtdId = 0;
                                                    String procType = "";
                                                    String sourceFund = "";
                                                    String appStatus = "Pending";
                                                    String wfStatus = "Pending";
                                                    if ("Revise".equalsIgnoreCase(request.getParameter("btnRevise"))) {
                                                        appStatus = "BeingRevised";
                                                    }
                                                    int noOfStages = 0;
                                                    String action = "";
                                                    int totLot = 0;
                                                    StringBuilder lotNo = new StringBuilder();
                                                    StringBuilder lotDesc = new StringBuilder();
                                                    StringBuilder qty = new StringBuilder();
                                                    StringBuilder unit = new StringBuilder();
                                                    StringBuilder lotEstCost = new StringBuilder();

                                                    if ("Next".equalsIgnoreCase(request.getParameter("btnNext"))) {
                                                        action = "Create";
                                                    } else if ("Update".equalsIgnoreCase(request.getParameter("update"))) {
                                                        action = "Update";
                                                    } else if ("Revise".equalsIgnoreCase(request.getParameter("btnRevise"))) {
                                                        action = "Revise";
                                                        isRevision = true;
                                                    }
                                                    pkgId = Integer.parseInt(request.getParameter("hdnPkgId"));
                                                    appId = Integer.parseInt(request.getParameter("hdnAppId"));
                                                    procNature = request.getParameter("procurenature");
                                                    if (request.getParameter("servicetype") != null) {
                                                        serviceType = request.getParameter("servicetype");
                                                    }
                                                    emeType = request.getParameter("emergencytype");
                                                    pkgNo = request.getParameter("packageno");
                                                    Boolean pkgNoFlag = appService.chkPkgNo("varPkgNo", Integer.parseInt(logUserId));
                                                    if (pkgNoFlag) {
                                                        response.sendRedirect("AddPackageDates.jsp?appId=" + appId + "&pkgId=" + pkgId + "&msg=alreadyExist");
                                                    } else {
                                                        pkgDesc = request.getParameter("packagedesc");
                                                        //aprojit-Start
                                                        if (request.getParameter("All") != null) {
                                                            bidCategory = "W1, W2, W3, W4";
                                                            WC1= "1"; WC2="2" ; WC3="3"; WC4="4";
                                                        } else {
                                                            if (request.getParameter("W1") != null) {
                                                                WC1= "1";
                                                                W1 = request.getParameter("W1");
                                                            }
                                                            if (request.getParameter("W2") != null) {
                                                                WC2="2" ;
                                                                if (request.getParameter("W1") == null) {
                                                                    W2 = request.getParameter("W2");
                                                                } else {
                                                                    W2 = ", " + request.getParameter("W2");
                                                                }
                                                            }
                                                            if (request.getParameter("W3") != null) {
                                                                WC3="3";
                                                                if (request.getParameter("W1") == null && request.getParameter("W2") == null) {
                                                                    W3 = request.getParameter("W3");
                                                                } else {
                                                                    W3 = ", " + request.getParameter("W3");
                                                                }
                                                            }
                                                            if (request.getParameter("W4") != null) {
                                                                WC4="4";
                                                                if (request.getParameter("W1") == null && request.getParameter("W2") == null && request.getParameter("W3") == null) {
                                                                    W4 = request.getParameter("W4");
                                                                } else {
                                                                    W4 = ", " + request.getParameter("W4");
                                                                }
                                                            }
                                                            bidCategory = W1 + W2 + W3 + W4;
                                                        }
                                                        workCategory = request.getParameter("workcategory");
//                                                        depoplanWork = request.getParameter("depoplanWork");
//                                                        if ("DepositWork".equalsIgnoreCase(depoplanWork)) {
//                                                            DepoPlanWork = "Deposit Work";
//                                                            entrustingAgency = request.getParameter("entrustagency");
//                                                            timeFrame = null;
//                                                        }
//                                                        if ("PlanedWork".equalsIgnoreCase(depoplanWork)) {
//                                                            DepoPlanWork = "Planned Work";
//                                                            timeFrame = request.getParameter("timeframe");
//                                                            entrustingAgency = null;
//                                                        }
                                                        //aprojit-End
                                                        if (request.getParameter("allocatedbudget") != null && !"".equalsIgnoreCase(request.getParameter("allocatedbudget"))) {
                                                            aloBud = Float.parseFloat(request.getParameter("allocatedbudget"));
                                                        }
                                                        if (request.getParameter("packagecost") != null && !"".equalsIgnoreCase(request.getParameter("packagecost"))) {
                                                            estCost = Float.parseFloat(request.getParameter("packagecost"))*1000000;
                                                        };
                                                        cpvCode = request.getParameter("specialization");

                                                        if (request.getParameter("packagecost") != null) {
                                                            pkgEstCost = Float.parseFloat(request.getParameter("packagecost"))*1000000;
                                                        }
                                                        aaEmpId = Integer.parseInt(request.getParameter("authority"));

                                                        if (request.getParameter("pqrequires") != null) {
                                                            isPQReq = request.getParameter("pqrequires");
                                                        }
                                                        if (request.getParameter("rfarequires") != null) {
                                                            reoiRfa = request.getParameter("rfarequires");
                                                        }

                                                        if (!"Services".equalsIgnoreCase(procNature)) {
                                                            reoiRfa = "";
                                                        }
                                                        if (!"Works".equalsIgnoreCase(procNature)) {
                                                            isPQReq = "";
                                                        }
                                                        procMtdId = Integer.parseInt(request.getParameter("procuremethod"));
                                                        procType = request.getParameter("procuretype");
                                                        sourceFund = request.getParameter("sourcefund");
                                                        totLot = Integer.parseInt(request.getParameter("hdnTotLot"));

                                                        for (int i = 1; i <= totLot; i++) {
                                                            if (i == totLot) {
                                                                if (request.getParameter("lotno_" + i) != null) {
                                                                    lotNo.append(request.getParameter("lotno_" + i));
                                                                    lotDesc.append(request.getParameter("lotdesc_" + i));
                                                                    qty.append(request.getParameter("quantity_" + i));
                                                                    unit.append(request.getParameter("unit_" + i));
                                                                    String LotEstimatedCost = request.getParameter("estimatecost_" + i);
                                                                    BigDecimal bd = new BigDecimal(LotEstimatedCost);
                                                                    MathContext mc = new MathContext(8);
                                                                    LotEstimatedCost = String.format("%.4f",bd.multiply(new BigDecimal("1000000"),mc)).toString();
                                                                    lotEstCost.append(LotEstimatedCost);
                                                                }
                                                            } else if (request.getParameter("lotno_" + i) != null) {
                                                                lotNo.append(request.getParameter("lotno_" + i)).append("@$");
                                                                lotDesc.append(request.getParameter("lotdesc_" + i)).append("@$");
                                                                qty.append(request.getParameter("quantity_" + i)).append("@$");
                                                                unit.append(request.getParameter("unit_" + i)).append("@$");
                                                                String LotEstimatedCost = request.getParameter("estimatecost_" + i);
                                                                BigDecimal bd = new BigDecimal(LotEstimatedCost);
                                                                MathContext mc = new MathContext(8);
                                                                LotEstimatedCost = String.format("%.4f",bd.multiply(new BigDecimal("1000000"),mc)).toString();
                                                                lotEstCost.append(LotEstimatedCost).append("@$");
                                                            }
                                                        }
                                                        appServlet.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                                        String msg = appServlet.addPkgDetail(appId, procNature, serviceType, pkgNo, pkgDesc, DepoPlanWork, entrustingAgency, timeFrame, bidCategory, workCategory, aloBud, estCost, cpvCode, pkgEstCost, aaEmpId, isPQReq, reoiRfa, procMtdId, procType, sourceFund, appStatus, wfStatus, noOfStages, action, pkgId, lotNo.toString(), lotDesc.toString(), qty.toString(), unit.toString(), lotEstCost.toString(), emeType, WC1, WC2, WC3, WC4, session.getAttribute("userId").toString());
                                                        TenderSrBean tenderSrBean = new TenderSrBean();
                                                                //delete pe office
                                                        tenderSrBean.mapPEWithPackage(appId, pkgId, request.getParameterValues("officeid"), request.getParameterValues("officeName"), request.getParameterValues("officeCity"), request.getParameterValues("officeAddress"), false);
                                                        if (msg.contains("Success") || msg.contains("alreadyExist")) {
                                                            pkgId = Integer.valueOf(msg.substring(msg.indexOf("_") + 1, msg.length()).toString());
                                                            //save framework pe ofc
                                                            //String[] t1 = request.getParameterValues("officeid");
                                                            //String[] t2 = request.getParameterValues("officeName");
                                                            //String[] t3 = request.getParameterValues("officeCity");
                                                            //String[] t4 = request.getParameterValues("officeAddress");
                                                            if (procMtdId == 18) {                                                                
                                                                //add pe office
                                                                tenderSrBean.mapPEWithPackage(appId, pkgId, request.getParameterValues("officeid"), request.getParameterValues("officeName"), request.getParameterValues("officeCity"), request.getParameterValues("officeAddress"), true);
                                                            }
                                                            msg = "Success_" + pkgId;
                                                            if (request.getParameter("btnNext") != null || request.getParameter("update") != null || request.getParameter("btnRevise") != null) {
                                                                //response.sendRedirect("");
%>
                                        <form id="frmInsUpdate" name="frmInsUpdate" method="post">
                                            <input type="hidden" name="appId" id="appId" value="<%=appId%>"/>
                                            <input type="hidden" name="pkgId" id="pkgId" value="<%=pkgId%>"/>
                                            <% if ("Create".equalsIgnoreCase(action)) { %>
                                            <input type="hidden" name="msg" id="msg" value="success"/>
                                            <% } else { %>
                                            <input type="hidden" name="msg" id="msg" value="updateSuccess"/>
                                            <% } %>
                                        </form>
                                        <%
                                            if ("Update".equalsIgnoreCase(request.getParameter("update")) || isRevision) {
                                                AppAdvSearchService appAdvSearchService = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
                                                appAdvSearchService.setLogUserId(logUserId);

                                                if (!((appAdvSearchService.countForQuery("TblAppPqTenderDates", "appid = " + Integer.parseInt(request.getParameter("hdnAppId")) + " and packageId = " + Integer.parseInt(request.getParameter("hdnPkgId")))) > 0)) {
                                                    isAddDatesConf = true;
                                                } else {
                                                    isAddDatesConf = false;
                                                }
                                            }
                                            // revising APP  and Dates exists
                                            if (isRevision && !isAddDatesConf) {
                                                response.sendRedirect("EditPackageDates.jsp?appId=" + appId + "&pkgId=" + pkgId + "&msg=r");

                                            } else if (isAddDatesConf || !"Update".equalsIgnoreCase(request.getParameter("update"))) {
                                                //in revising APP and dates are not added
                                                if (isRevision) {
                                                    response.sendRedirect("AddPackageDates.jsp?appId=" + appId + "&pkgId=" + pkgId + "&msg=success&isRevision=true");
                                                } //if dates are not added
                                                else {
                                                    response.sendRedirect("AddPackageDates.jsp?appId=" + appId + "&pkgId=" + pkgId + "&msg=success");
                                                }
                                        %>
                                        <!--                                                            <script>
                                                                                                        document.getElementById("frmInsUpdate").action = "AddPackageDates.jsp";
                                                                                                        document.getElementById("frmInsUpdate").submit();
                                                                                                    </script>-->
                                        <%
                                        } else {
                                            response.sendRedirect("EditPackageDates.jsp?appId=" + appId + "&pkgId=" + pkgId + "&msg=updateSuccess");

                                        %>
                                        <!--                                                            <script>
                                                                                                        document.getElementById("frmInsUpdate").action = "EditPackageDates.jsp";
                                                                                                        document.getElementById("frmInsUpdate").submit();
                                                                                                    </script>-->
                                        <%                        }
                                                        }
                                                    }
                                                }

                                            } catch (Exception ex) {
											System.out.println(ex.getStackTrace() + ""+ ex.getMessage());
                                                response.sendRedirect("AddPackageDetail.jsp?msg=Error&appId=" + appId);
                                            }
                                        } else {
                                        %>
                                        <script language="javascript" type="text/javascript">
                                            function setSerType(obj, flag) {
                                                if ("Services" == obj.options[obj.selectedIndex].text) {
                                                    $('#thLotNo').show();
                                                    $('#tdLotNo').show();
                                                    $('#trSerType').show();
                                                    $('#trRFReq').show();
                                                    $('#trPQReq').hide();
                                                    $('#trBidderCategory').hide();
                                                    $('#trWorkCategory').hide();
                                                    if (flag) {
                                                        document.getElementById("cmbRFARequires").options[0].selected = "selected";
                                                    }
                                                    $.post("<%=request.getContextPath()%>/APPServlet", {projectId: $('#cmbRFARequires').val(), cmbId: $('#cmbServiceType').val(), procType: $('#cmbProcureType').val(), param4: $('#hdnPM').val(), procNature: $('#cmbProcureNature').val(), funName: 'getPM'}, function (j) {
                                                        $("select#cmbProcureMethod").html(j);
                                                    });
                                                    var delCnt = eval(document.getElementById("hdnDelLot").value);
                                                    $('#thLotBtnSelect').hide();
                                                    $('#tdLotBtnSelect').hide();
                                                    $('#trLotBtn').hide();
                                                                                                var counter = eval(document.getElementById("hdnTotLot").value);
                                                                                                var delCnt = eval(document.getElementById("hdnDelLot").value);
                                                                                                for(var i=2;i<=counter;i++){
                                                                                                    if(document.getElementById("chkPackageDetail_"+i) != null){
                                                                                                        $("tr[id='trLot_"+i+"']").remove();
                                                                                                        $('span.#lotMsg').css("display","none");
                                                                                                        delCnt++;
                                                                                                    }
                                                                                                }
                                                    document.getElementById("hdnDelLot").value = delCnt;
                                                    document.getElementById("txtLotNo_1").value = document.getElementById("txtPackageNo").value;
                                                    document.getElementById("txtLotDesc_1").value = document.getElementById("txtaPackageDesc").value;
                                                    document.getElementById("txtLotNo_1").readOnly = true;
                                                    document.getElementById("txtLotDesc_1").readOnly = true;
                                                    document.getElementById("lblLotNo").innerHTML = "Package ";
                                                    document.getElementById("lblLotDesc").innerHTML = "Package ";
                                                    document.getElementById("spanQty").style.visibility = "hidden";
                                                    document.getElementById("spanUnit").style.visibility = "hidden";
                                                    serviceType = "Services";
                                                } else if ("Works" == obj.options[obj.selectedIndex].text) {
                                                    document.getElementById("txtLotNo_1").value = '1';
                                                    $('#thLotNo').hide();
                                                    $('#tdLotNo').hide();
                                                    $('#trSerType').hide();
                                                    $('#trRFReq').hide();
                                                    $('#trPQReq').show();
                                                    $('#trBidderCategory').show();
                                                    $('#trWorkCategory').show();
                                                    if (flag) {
                                                        document.getElementById("cmbPQRequires").options[0].selected = "selected";
                                                    }
                                                    $.post("<%=request.getContextPath()%>/APPServlet", {projectId: $('#cmbPQRequires').val(), param4: $('#hdnPM').val(), procNature: $('#cmbProcureNature').val(), funName: 'getPM'}, function (j) {
                                                        $("select#cmbProcureMethod").html(j);
                                                    });
                                                    $('#thLotBtnSelect').hide();
                                                    $('#tdLotBtnSelect').hide();
                                                    $('#trLotBtn').hide();
                                                    
                                                    
                                                                                                var counter = eval(document.getElementById("hdnTotLot").value);
                                                                                                var delCnt = eval(document.getElementById("hdnDelLot").value);
                                                                                                for(var i=2;i<=counter;i++){
                                                                                                    if(document.getElementById("chkPackageDetail_"+i) != null){
                                                                                                        $("tr[id='trLot_"+i+"']").remove();
                                                                                                        $('span.#lotMsg').css("display","none");
                                                                                                        delCnt++;
                                                                                                    }
                                                                                                }
                                                    if (flag)
                                                    {
                                                        if (serviceType == "Services")
                                                        {
                                                            document.getElementById("txtLotNo_1").value = "";
                                                            document.getElementById("txtLotDesc_1").value = "";
                                                        }
                                                    }
                                                    document.getElementById("lblLotNo").innerHTML = "Lot ";
                                                    document.getElementById("lblLotDesc").innerHTML = "Works ";
                                                    document.getElementById("txtLotNo_1").readOnly = false;
                                                    document.getElementById("txtLotDesc_1").readOnly = false;
                                                    document.getElementById("spanQty").style.visibility = "hidden";
                                                    document.getElementById("spanUnit").style.visibility = "hidden";
                                                    serviceType = "Works";
                                                } else {
                                                    if(flag)
                                                    {
                                                    document.getElementById("txtLotNo_1").value = '';
                                                    }
                                                    $('#thLotNo').show();
                                                    $('#tdLotNo').show();
                                                    $('#trSerType').hide();
                                                    $('#trRFReq').hide();
                                                    $('#trPQReq').hide();
                                                    $('#trBidderCategory').hide();
                                                    $('#trWorkCategory').hide();
                                                    $.post("<%=request.getContextPath()%>/APPServlet", {projectId: 'No', procType: $('#cmbProcureType').val(), param4: $('#hdnPM').val(), procNature: $('#cmbProcureNature').val(), funName: 'getPM'}, function (j) {
                                                        $("select#cmbProcureMethod").html(j);
                                                    });
                                                    $('#thLotBtnSelect').show();
                                                    $('#tdLotBtnSelect').show();
                                                    $('#trLotBtn').show();
                                                   
                                                    if (flag)
                                                    {
                                                        if (serviceType == "Services")
                                                        {
                                                            document.getElementById("txtLotNo_1").value = "";
                                                            document.getElementById("txtLotDesc_1").value = "";
                                                        }
                                                    }
                                                    document.getElementById("lblLotNo").innerHTML = "Lot ";
                                                    document.getElementById("lblLotDesc").innerHTML = "Lot ";
                                                    document.getElementById("txtLotNo_1").readOnly = false;
                                                    document.getElementById("txtLotDesc_1").readOnly = false;
                                                    document.getElementById("spanQty").style.visibility = "visible";
                                                    document.getElementById("spanUnit").style.visibility = "visible";
                                                    serviceType = "Goods";
                                                }
                                            }
                                            //Nitish Start 
                                            function setPC(obj)
                                            {
                                                var appType = obj.value;
                                                if(appType != "")
                                                {
                                                    $('#msgProcNature').html('');
                                                }
                                                else
                                                {
                                                    $('#msgProcNature').html('Please select APP Type.'); 
                                                }
                                            }
                                            //Nitish END
                                            
                                            
                                            
                                            
                                            //aprojit-Start
                                            
                                            function setBidderCategory(obj) {
                                                if (document.getElementById('ckhAll').checked == true) {
                                                    document.getElementById('chkW1').checked = true;
                                                    document.getElementById('chkW2').checked = true;
                                                    document.getElementById('chkW3').checked = true;
                                                    document.getElementById('chkW4').checked = true;
                                                } else {
                                                    if ("ckhAll" == obj.id) {
                                                        document.getElementById('chkW1').checked = false;
                                                        document.getElementById('chkW2').checked = false;
                                                        document.getElementById('chkW3').checked = false;
                                                        document.getElementById('chkW4').checked = false;
                                                    }
                                                }
                                            }
                                            // aprojit-End
                                        </script>

                                        <form method="POST" id="frmAddPackageDetail" action="AddPackageDetail.jsp">
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <% if (request.getParameter("msg") != null && "Error".equalsIgnoreCase(request.getParameter("msg"))) {%>
                                                <tr>
                                                    <td colspan="2"><div class="responseMsg errorMsg"><%=AppMessage.addPackageErrMsg%></div></td>
                                                </tr>
                                                <% } %>
                                                <% if (request.getParameter("msg") != null && "alreadyExist".equalsIgnoreCase(request.getParameter("msg"))) { %>
                                                <tr>
                                                    <td colspan="2"><div class="responseMsg errorMsg">APP Package already exist</div></td>
                                                </tr>
                                                <% } %>

                                                <tr>
                                                    <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                                </tr>
                                                <%
                                                    java.util.List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
                                                    appListDtBean = appServlet.getAPPDetails("App", String.valueOf(appId), "");
                                                    String prjName = appListDtBean.get(0).getFieldName6();
                                                    String budgetType = appListDtBean.get(0).getFieldName5();
                                                    prjId = Integer.parseInt(appListDtBean.get(0).getFieldName2().toString());
                                                    String srcFund = "";
                                                    String activityName = appListDtBean.get(0).getFieldName8();                                                    
                                                    Boolean flag = false;
                                                    String devPartner = "";
                                                %>
                                                <%
                                                    if (isEdit && appId != 0 && pkgId != 0) {
                                                        appViewDtBean.populateInfo(appId, pkgId, "AppPackage", 0);
                                                    }
                                                    if (isEdit) {
                                                        prjId = appViewDtBean.getProjectId();
                                                        prjName = appViewDtBean.getProjectName().trim();
                                                        devPartner = appServlet.getDPDetails("ProjectPartner", String.valueOf(prjId));
                                                        srcFund = appServlet.getDPDetails("SourceOfFund", String.valueOf(prjId));

                                                        if (appViewDtBean.getProcurementMethodId() == 18) {
                                                            TenderSrBean tSrBean = new TenderSrBean();
                                                            appListFrameworkOfc = tSrBean.getAppFrameworkOfficeByPkgId(appId, pkgId);
                                                        }

                                                        if (srcFund == null || "".equals(srcFund)) {
                                                            if(!activityName.equals("")){
                                                                srcFund = "Own Fund";
                                                            }else{
                                                                if ("Development".equalsIgnoreCase(appViewDtBean.getBudgetType())) {
                                                                    srcFund = "Aid Grant / Credit";
                                                                } else if ("Recurrent".equalsIgnoreCase(appViewDtBean.getBudgetType())) {
                                                                    srcFund = "Government";
                                                                } else if ("Own Fund".equalsIgnoreCase(appViewDtBean.getBudgetType())) {
                                                                    srcFund = "Own Fund";
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        devPartner = appServlet.getDPDetails("ProjectPartner", String.valueOf(prjId));
                                                        srcFund = appServlet.getDPDetails("SourceOfFund", String.valueOf(prjId));
                                                        if (srcFund == null || "".equals(srcFund)) {
                                                            if(!activityName.equals("") ){
                                                                srcFund = "Own Fund";
                                                            }else{
                                                                if ("Development".equalsIgnoreCase(appListDtBean.get(0).getFieldName5())) {
                                                                    srcFund = "Aid Grant / Credit";
                                                                } else if ("Recurrent".equalsIgnoreCase(appListDtBean.get(0).getFieldName5())) {
                                                                    srcFund = "Government";
                                                                } else if ("Own Fund".equalsIgnoreCase(appListDtBean.get(0).getFieldName5())) {
                                                                    srcFund = "Own Fund";
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (!"".equalsIgnoreCase(prjName)) {
                                                        flag = true;
                                                    } else {
                                                        flag = false;
                                                    }
                                                %>
                                                <tr>
                                                    <td width="25%" class="ff">APP ID :</td>
                                                    <td width="75%">
                                                        <%
                                                            if (isEdit) {
                                                                out.print(appViewDtBean.getAppId());
                                                            } else {
                                                                out.print(appListDtBean.get(0).getFieldName1());
                                                            }
                                                        %>
                                                        <input type="hidden" name="hdnAppId" id="hdnAppId" value="<%=appListDtBean.get(0).getFieldName1()%>"/>
                                                        <input type="hidden" name="hdnPkgId" id="hdnPkgId" value="<%=pkgId%>"/>
                                                        <input type="hidden" name="hdnPrjId" id="hdnPrjId" value="<%=prjId%>"/>
                                                        <% if (isRevision) { %>
                                                        <input type="hidden" name="action" id="action" value="Revise" />
                                                        <% } %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Financial Year :</td>
                                                    <td>
                                                        <%
                                                            if (isEdit) {
                                                                out.print(appViewDtBean.getFinancialYear());
                                                            } else {
                                                                out.print(appListDtBean.get(0).getFieldName4());
                                                            }
                                                        %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Budget Type :</td>
                                                    <td>
                                                        <%
                                                            if (isEdit) {
                                                                out.print(appViewDtBean.getBudgetType());
                                                            } else {
                                                                out.print(appListDtBean.get(0).getFieldName5());
                                                            }
                                                        %>
                                                    </td>
                                                </tr>
                                                <%if (!"".equalsIgnoreCase(prjName)) {%>
                                                <tr>
                                                    <td class="ff">Project Name :</td>
                                                    <td>
                                                        <%
                                                            if (isEdit) {
                                                                out.print(appViewDtBean.getProjectName());
                                                            } else {
                                                                out.print(prjName);
                                                            }
                                                        %>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td class="ff">Letter Ref. No. :</td>
                                                    <td>
                                                        <%
                                                            if (isEdit) {
                                                                out.print(appViewDtBean.getAppCode());
                                                            } else {
                                                                out.print(appListDtBean.get(0).getFieldName3());
                                                            }
                                                        %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Procurement Category : <span>*</span></td>
                                                    <td>
                                                        <select name="procurenature" class="formTxtBox_1" id="cmbProcureNature" style="width:200px;" onChange="setSerType(this, true);" onblur="setPC(this)">
                                                            <option value="">- Select Procurement Category -</option>
                                                            <option value="Goods" <%if (isEdit) {
                                                                    if ("Goods".equalsIgnoreCase(appViewDtBean.getProcurementnature())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Goods</option>
                                                            <option value="Works" <%if (isEdit) {
                                                                    if ("Works".equalsIgnoreCase(appViewDtBean.getProcurementnature())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Works</option>
                                                            <option value="Services" <%if (isEdit) {
                                                                    if ("Services".equalsIgnoreCase(appViewDtBean.getProcurementnature())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Services</option>
                                                        </select>
                                                        <%if (isEdit) {%>
                                                        <script>
                                                            procNature = '<%=appViewDtBean.getProcurementnature()%>';
                                                        </script>
                                                        <%}%>
                                                        <span id="msgProcNature" class="reqF_2"></span>
                                                    </td>
                                                </tr>
                                                <tr id="trSerType" <%if (isEdit) {
                                                        if (!"Services".equalsIgnoreCase(appViewDtBean.getProcurementnature())) {%>style="display:none"<%}
                                                        } else {%>style="display:none"<%}%>>
                                                    <td class="ff">Service Type : <span>*</span></td>
                                                    <td>
                                                        <select name="servicetype" class="formTxtBox_1" id="cmbServiceType" style="width:200px;" onblur="setcmbServiceType(this)">
                                                            <option value="">- Select Service Type -</option>
                                                            <%--         <option value="Stand alone services" <%if (isEdit) {
                                                                    if ("Stand alone services".equalsIgnoreCase(appViewDtBean.getServicesType())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Stand alone services</option>
                                                            <option value="Professional &amp; Intellectual Services" <%if (isEdit) {
                                                                    if ("Professional &amp; Intellectual Services".equalsIgnoreCase(appViewDtBean.getServicesType().replace("&", "&amp;"))) {
                                                                        out.print("selected");
                                                                    }
}%>>Professional &amp; Intellectual Services</option> --%>
                                                            <option value="Consulting Services" <%if (isEdit) {
                                                                        if ("Consulting Services".equalsIgnoreCase(appViewDtBean.getServicesType())) {
                                                                            out.print("selected");
                                                                        }
                                                                        }else{
                                                                               out.print("selected"); 
                                                                           }
                                                            %>
                                                            >Consulting Services</option>
                                                            <option value="Non-Consulting Services" <%if (isEdit) {
                                                                        if ("Non-Consulting Services".equalsIgnoreCase(appViewDtBean.getServicesType())) {
                                                                            out.print("selected");
                                                                        }
                                                                        } else{
                                                                               out.print("disabled"); 
                                                                           }

                                                            %>
                                                            >Non-Consulting Services</option>
                                                        </select>
                                                        <span id="msgSerType" style="color: red;">&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <!--Display show of Type of Emergency by Proshanto Kumar Saha -->
                                                <tr style="display:none">
                                                    <td class="ff">Type of Emergency : </td>
                                                    <td>
                                                        <select name="emergencytype" class="formTxtBox_1" id="cmbEmergencyType" style="width:200px;">
                                                            <option value="Normal" <%if (isEdit) {
                                                                    if ("Normal".equalsIgnoreCase(appViewDtBean.getEmeType())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Normal</option>
                                                            <option value="Urgent(Catastrophe)" <%if (isEdit) {
                                                                    if ("Urgent(Catastrophe)".equalsIgnoreCase(appViewDtBean.getEmeType())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Urgent(Catastrophe)</option>
                                                            <option value="National disaster" <%if (isEdit) {
                                                                    if ("National disaster".equalsIgnoreCase(appViewDtBean.getEmeType())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>National disaster</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <!--Display show of Type of Emergency by Proshanto Kumar Saha -->
                                                <tr>
                                                    <td class="ff">Package No. : <span>*</span></td>
                                                    <td>
                                                        <%--<input name="packageno" type="text" class="formTxtBox_1" id="txtPackageNo" style="width:200px;" maxlength="51" value="<%if(isEdit){out.print(appViewDtBean.getPackageNo());}%>" onBlur="if(checkPkgNo(this)){setLotNo();}"/>--%>
                                                        <input name="packageno" type="text" class="formTxtBox_1" id="txtPackageNo" style="width:187px;" value="<%if (isEdit) {
                                                                out.print(appViewDtBean.getPackageNo());
                                                            }%>" onBlur="if (!checkPkgNo(this)) {
                                                                        setLotNo();
                                                                    }"/>
                                                        <span id="pkgnoMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                        <span class="reqF_2" id="msgPackageNo"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        Please specify the Package/Lot as <span style="color: red; font-weight: bold;">TENDER: &lt;reference No.&gt;</span> if the Package/Lot will be procured using e-GP System
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Package Description : <span>*</span></td>
                                                    <td><textarea name="packagedesc" rows="5" class="formTxtBox_1" id="txtaPackageDesc" style="width:600px;" onBlur="setLotDesc();"><%if (isEdit) {
                                                            out.print(appViewDtBean.getPackageDesc());
                                                        }%></textarea>
                                                        <span class="reqF_2" id="msgPackageDesc"></span>
                                                    </td>
                                                </tr>
                                                <!--aprojit- Start !-->
                                                 <tr id="trWorkCategory" style="display:none">
                                                    <td class="ff">Work Type : <span>*</span></td>
                                                    <td>
                                                        <select name="workcategory" class="formTxtBox_1" id="cmbWorkCategory" style="width:200px;" onblur="setcmbWorkCategory()">
                                                            <option value="">- Select Work Category -</option>
                                                            <option value="Small" <%if (isEdit) {
                                                                    if ("Small".equalsIgnoreCase(appViewDtBean.getWorkCategory())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Small</option>
                                                            <option value="Medium" <%if (isEdit) {
                                                                    if ("Medium".equalsIgnoreCase(appViewDtBean.getWorkCategory())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Medium</option>                                                            
                                                            <option value="Large" <%if (isEdit) {
                                                                    if ("Large".equalsIgnoreCase(appViewDtBean.getWorkCategory())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Large</option>
                                                        </select>                        
                                                        <span id="msgWorkCategory" class="reqF_2"></span>
                                                    </td>
                                                </tr>
                                                <tr id="trBidderCategory" style="display:none">
                                                    <td class="ff">Work Category : <span>*</span></td>
                                                    <td>
                                                        <input type="checkbox" name="All" id="ckhAll" value="All" <%if (isEdit) {
                                                                if ("W1, W2, W3, W4".equalsIgnoreCase(appViewDtBean.getBidderCategory())) {
                                                                    out.print("checked");
                                                                }
                                                        }%> onblur="setbidderCategory(this)" onchange="setBidderCategory(this);"/> All
                                                        <input type="checkbox" name="W1" id="chkW1" value="W1" <%if (isEdit) {
                                                                if (appViewDtBean.getBidderCategory().contains("W1")) {
                                                                    out.print("checked");
                                                                }
                                                            }%> onblur="setbidderCategory(this)" onchange="setBidderCategory(this);"/> W1
                                                        <input type="checkbox" name="W2" id="chkW2" value="W2" <%if (isEdit) {
                                                                if (appViewDtBean.getBidderCategory().contains("W2")) {
                                                                    out.print("checked");
                                                                }
                                                            }%> onblur="setbidderCategory(this)" onchange="setBidderCategory(this);"/> W2
                                                        <input type="checkbox" name="W3" id="chkW3" value="W3" <%if (isEdit) {
                                                                if (appViewDtBean.getBidderCategory().contains("W3")) {
                                                                    out.print("checked");
                                                                }
                                                            }%> onblur="setbidderCategory(this)" onchange="setBidderCategory(this);"/> W3
                                                        <input type="checkbox" name="W4" id="chkW4" value="W4" <%if (isEdit) {
                                                                if (appViewDtBean.getBidderCategory().contains("W4")) {
                                                                    out.print("checked");
                                                                }
                                                            }%> onblur="setbidderCategory(this)" onchange="setBidderCategory(this);"/> W4
                                                        <span class="reqF_2" id="msgBidderCategory"></span>
                                                    </td>
                                                </tr>                 
<!--                                                <tr>
                                                    <td class="ff">Deposit work / Planned work : <span>*</span></td>
                                                    <td>
                                                        <select name="depoplanWork" class="formTxtBox_1" id="cmbdepoplanWork" style="width:200px;" onChange="setDPType(this);">
                                                            <option value="">- Select Category -</option>
                                                            <option value="DepositWork" <%if (isEdit) {
                                                                    if ("Deposit Work".equalsIgnoreCase(appViewDtBean.getDepoplanWork())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Deposit Work</option>
                                                            <option value="PlanedWork" <%if (isEdit) {
                                                                    if ("Planned Work".equalsIgnoreCase(appViewDtBean.getDepoplanWork())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Planned Work</option>
                                                        </select>
                                                        <span id="msgdepoplanWork" class="reqF_2"></span>
                                                    </td>
                                                </tr>
                                                    <tr id="trEntrustingAgency" <%if (isEdit) {
                                                            if (!"Deposit Work".equalsIgnoreCase(appViewDtBean.getDepoplanWork())) {%>style="display:none"<%}
                                                        } else {%>style="display:none"<%}%>>
                                                    <td class="ff">Entrusting Agency : <span>*</span></td>
                                                    <td>
                                                        <input name="entrustagency" type="text" class="formTxtBox_1" id="txtEntrustingAgency" style="width:200px;" value="<%if (isEdit) {
                                                                out.print(appViewDtBean.getEntrustingAgency());
                                                            }%>"  />
                                                        <span id="msgEntrustAgency" class="reqF_2"></span>
                                                    </td>
                                                </tr>-->
                                                    <tr id="trTimeFrame" <%if (isEdit) {
                                                            if (!"Planned Work".equalsIgnoreCase(appViewDtBean.getDepoplanWork())) {%>style="display:none"<%}
                                                        } else {%>style="display:none"<%}%>>
                                                    <td class="ff">Time Frame : <span>*</span></td>
                                                    <td>
                                                        <input name="timeframe" type="text" class="formTxtBox_1" id="txtTimeFrame" style="width:200px;" value="<%if (isEdit) {
                                                                out.print(appViewDtBean.getTimeFrame());
                                                            }%>" />
                                                        <span id="msgTimeFrame" class="reqF_2"></span>
                                                    </td>
                                                </tr>
                                                <!--aprojit- End !-->
                                                <tr style="display: none;">
                                                    <td class="ff">Allocated Budget (In Nu.) :</td>
                                                    <%  BigDecimal bd = new BigDecimal(0); %>
                                                    <td>
                                                        <input name="allocatedbudget" type="text" class="formTxtBox_1" id="txtAllocatedBudget" style="width:200px;" value="<%if (isEdit) {
                                                                if ("0.00".equalsIgnoreCase(appViewDtBean.getAllocateBudget().setScale(2, 0).toString())) {
                                                                    out.print("");
                                                                } else {
                                                                    out.print(appViewDtBean.getAllocateBudget().setScale(2, 0));
                                                                }
                                                            }%>" onBlur="chkEstCost();
                                                                    CONVERTWORD(this.value, 'figVal');"/>
                                                        <br/><span id="allBudMsg" style="color: red;">&nbsp;</span>
                                                        <span id="figVal">&nbsp;</span>
                                                    </td>
                                                    <%--if(isEdit){ %>
                                                    <script language="javascript">
                                                          CONVERTWORD(<% if("0.00".equalsIgnoreCase(appViewDtBean.getAllocateBudget().setScale(2, 0).toString())){ out.print(""); }else{ out.print(appViewDtBean.getAllocateBudget().setScale(2, 0)); } %>,'figVal');
                                                    </script>
                                                    <% } --%>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table width="100%" cellspacing="0" id="tblLots" class="tableList_1 t_space">
                                                            <tr>
                                                                <th id="thLotBtnSelect" width="10%" class="t-align-center"  <% if(isEdit){ if(!"Goods".equalsIgnoreCase(appViewDtBean.getProcurementnature())){%>style="display:none"<%}}else{%>style="display:none"<%}%> >Select</th>
                                                                <th width="15%" class="t-align-center" id="thLotNo">
                                                                    <label id="lblLotNo">
                                                                        <%
                                                                            if (isEdit) {
                                                                                if ("Services".equalsIgnoreCase(appViewDtBean.getProcurementnature())) {
                                                                                    out.print("Package");
                                                                                } else {
                                                                                    out.print("Lot");
                                                                                }
                                                                            } else {
                                                                                out.print("Lot");
                                                                            }
                                                                        %>
                                                                    </label>No.
                                                                    <span class="txt_2">*</span>
                                                                </th>
                                                                <th width="30%" class="t-align-center">
                                                                    <label id="lblLotDesc">Lot</label> Description
                                                                    <span class="txt_2">*</span>
                                                                </th>
                                                                <th width="15%" class="t-align-center">
                                                                    Quantity
                                                                    <span id="spanQty" name="spanQty" style="visibility:hidden;" class="txt_2">*</span>

                                                                </th>
                                                                <th width="15%" class="t-align-center">
                                                                    Unit <br />(i.e. Nos., Kg, etc.)
                                                                    <span id="spanUnit" name="spanUnit" style="visibility:visible;" class="txt_2">*</span>
                                                                </th>
                                                                <th width="20%" class="t-align-center">
                                                                    <!--Change BTN to Nu. by Proshanto-->
                                                                    Estimated Cost in<br />
                                                                    million (Nu.)
                                                                    <span class="txt_2">*</span>
                                                                </th>
                                                            </tr>
                                                            <input type="hidden" name="hdnTotLot" id="hdnTotLot" value="<%if (isEdit) {
                                                                    out.print(appViewDtBean.getPkgLotDeail().size());
                                                                } else {
                                                                    out.print("1");
                                                                }%>"/>
                                                            <input type="hidden" name="hdnDelLot" id="hdnDelLot" value="0"/>
                                                            <%
                                                                if (isEdit) {
                                                                    if (appViewDtBean.getPkgLotDeail().size() > 0) {
                                                                        for (int i = 0; i < appViewDtBean.getPkgLotDeail().size(); i++) {
                                                            %>
                                                            <tr id="trLot_<%=(i + 1)%>">
                                                                <td id="tdLotBtnSelect" class="t-align-center"  <% if(isEdit){ if(!"Goods".equalsIgnoreCase(appViewDtBean.getProcurementnature())){%>style="display:none"<%}}else{%>style="display:none"<%}%>>
                                                                     <input type="checkbox" name="packagedetail_<%=(i+1)%>" id="chkPackageDetail_<%=(i+1)%>" value="1" /> 
                                                                </td>
                                                                
                                                                <td class="t-align-center" id="tdLotNo">
                                                                    <input name="lotno_<%=(i + 1)%>" type="text" class="formTxtBox_1" id="txtLotNo_<%=(i + 1)%>" style="width:80px;" <%if ("Services".equalsIgnoreCase(appViewDtBean.getProcurementnature())) {%>readOnly<%}%> value="<%=appViewDtBean.getPkgLotDeail().get(i).getLotNo()%>"  onBlur="chkLotNoBlank(this);
                                                                            chkLotNo(this);"/><span id="msgLotNo_<%=(i + 1)%>" style="color: red;">&nbsp;</span>
                                                                </td>
                                                                <td class="t-align-center">
                                                                    <textarea name="lotdesc_<%=(i + 1)%>" cols="20" rows="3" class="formTxtBox_1" id="txtLotDesc_<%=(i + 1)%>" style="width:250px;" onBlur="chkLotDetBlank(this);" <%if ("Services".equalsIgnoreCase(appViewDtBean.getProcurementnature())) {%>readOnly<%}%> ><%=appViewDtBean.getPkgLotDeail().get(i).getLotDesc()%></textarea>
                                                                    <span id="msgLotDesc_<%=(i + 1)%>" style="color: red;">&nbsp;</span>
                                                                </td>
                                                                <td class="t-align-center"><input name="quantity_<%=(i + 1)%>" type="text" maxlength='16' class="formTxtBox_1" id="txtQuantity_<%=(i + 1)%>" style="width:80px;" value="<% if (!"0.00".equals(appViewDtBean.getPkgLotDeail().get(i).getQuantity().toString())) {
                                                                        out.print(appViewDtBean.getPkgLotDeail().get(i).getQuantity());
                                                                    }%>" onBlur="chkQtyBlank(this);"/><span id="msgLotQty_<%=(i + 1)%>" style="color: red; ">&nbsp;</span></td>
                                                                <td class="t-align-center"><input name="unit_<%=(i + 1)%>" type="text" class="formTxtBox_1" id="txtUnit_<%=(i + 1)%>" style="width:80px;" value="<%=appViewDtBean.getPkgLotDeail().get(i).getUnit()%>" onBlur="chkUnitBlank(this);"/><span id="msgLotUnit_<%=(i + 1)%>" style="color: red; ">&nbsp;</span></td>
                                                                <td class="t-align-center"><input name="estimatecost_<%=(i + 1)%>" type="text" autocomplete='off' class="formTxtBox_1" id="txtEstimateCost_<%=(i + 1)%>" style="width:80px;" onChange="setPkgEstCost(this);" value="<% out.print(appViewDtBean.getPkgLotDeail().get(i).getLotEstCost().divide(new BigDecimal("1000000"),8,BigDecimal.ROUND_HALF_UP));%>" onBlur="chkEstBlank(this);
                                                                        CONVERTWORD(((this.value)*1000000), 'PckfigVal_<%=(i + 1)%>');"/>
                                                                    <span id="msgLotCost_<%=(i + 1)%>" style="color: red; ">&nbsp;</span> <br /><span id="PckfigVal_<%=(i + 1)%>"></span></td>
                                                            </tr>
                                                            <%if (isEdit) { %>
                                                            <script language="javascript">
                                                                CONVERTWORD(<% if ("0.00".equalsIgnoreCase(appViewDtBean.getPkgLotDeail().get(i).getLotEstCost().setScale(2, 0).toString())) {
                                                                        out.print("");
                                                                    } else {
                                                                        out.print(appViewDtBean.getPkgLotDeail().get(i).getLotEstCost().setScale(2, 0));
                                                                    }%>, 'PckfigVal_<%=(i + 1)%>');
                                                            </script>
                                                            <% } %>
                                                            <%
                                                                    }
                                                                }
                                                            } else {
                                                            %>
                                                            <tr id="trLot_1">
                                                                  <td id="tdLotBtnSelect" class="t-align-center"  <% if(isEdit){ if(!"Goods".equalsIgnoreCase(appViewDtBean.getProcurementnature())){%>style="display:none"<%}}else{%>style="display:none"<%}%> >
                                                                      <input type="checkbox" name="packagedetail_1" id="chkPackageDetail_1" value="1"/></td>
                                                                <td class="t-align-center" id="tdLotNo">
                                                                    <input name="lotno_1" type="text" class="formTxtBox_1" id="txtLotNo_1" style="width:80px;" onBlur="chkLotNoBlank(this);
                                                                            chkLotNo(this);"/>
                                                                    <span id="msgLotNo_1" style="color: red; ">&nbsp;</span>
                                                                </td>
                                                                <td class="t-align-center">
                                                                    <textarea name="lotdesc_1" cols="20" rows="3" class="formTxtBox_1" id="txtLotDesc_1" style="width:250px;" onBlur="chkLotDetBlank(this);"></textarea>
                                                                    <span id="msgLotDesc_1" style="color: red; ">&nbsp;</span>
                                                                </td>
                                                                <td class="t-align-center">
                                                                    <input name="quantity_1" type="text" class="formTxtBox_1" id="txtQuantity_1" maxlength='16' style="width:80px;" onBlur="chkQtyBlank(this);"/>
                                                                    <span id="msgLotQty_1" style="color: red; ">&nbsp;</span>
                                                                </td>
                                                                <td class="t-align-center">
                                                                    <input name="unit_1" type="text" class="formTxtBox_1" id="txtUnit_1" style="width:80px;" onBlur="chkUnitBlank(this);"/>
                                                                    <span id="msgLotUnit_1" style="color: red; ">&nbsp;</span>
                                                                </td>
                                                                <td class="t-align-center">
                                                                    <input name="estimatecost_1" type="text" class="formTxtBox_1" id="txtEstimateCost_1" autocomplete='off' style="width:80px;" onChange="setPkgEstCost(this);" onBlur="chkEstBlank(this);
                                                                            CONVERTWORD(((this.value)*1000000), 'PckfigVal_1');"/>
                                                                    <span id="msgLotCost_1" style="color: red; ">&nbsp;</span>
                                                                    <br /><span id="PckfigVal_1"></span>
                                                                </td>
                                                            </tr>
                                                            <%}%>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="trLotBtn" <% if(isEdit){ if(!"Goods".equalsIgnoreCase(appViewDtBean.getProcurementnature())){%>style="display:none"<%}}else{%>style="display:none"<%}%>>
                                                    <td colspan="2" class="t-align-right">
                                                        <span id="lotMsg" style="color: red; font-weight: bold; display: none">&nbsp;</span>
                                                        <a id="linkAddLot" class="action-button-add" >Add Lot</a>
                                                        <a id="linkDelLot" class="action-button-delete" >Remove Lot</a>
                                                    </td>
                                                </tr>
                                                <tr id="trWarnMsg" style="display:none">
                                                    <td class="ff t-align-left" colspan="2">
                                                        <div id="warnMsg" class="responseMsg noticeMsg"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <!--Change BTN to Nu. by Proshanto-->
                                                    <td class="ff">Package Est. Cost in million (Nu.)</td>
                                                    <td><input name="packagecost" readOnly type="text" class="formTxtBox_1" id="txtPackageCost" style="width:187px;" value="<%if (isEdit) {
                                                            out.print(appViewDtBean.getPkgEstCost().divide(new BigDecimal("1000000"),8,BigDecimal.ROUND_HALF_UP));
                                                        }%>" />
                                                        <br /><span id="pkgEstCost"></span>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="ff">Category : <span>*</span></td>
                                                    <td>
                                                        <textarea name="specialization" rows="5" class="formTxtBox_1" maxlength="500" id="txtaCPV" style="width:600px;" readonly onBlur="settxtaCPV()"><%if (isEdit) {
                                                                out.print(appViewDtBean.getCpvCode());
                                                            }%></textarea>
                                                        &nbsp;<a id="hrefCPV" href="javascript:void(0);" onclick="loadCPVTree()" class="action-button-select" >Select Categories</a>
                                                        <span class="reqF_2" id="msgCPV"></span>
                                                    </td>
                                                </tr>
                                                <%appListDtBean = appServlet.getAPPDetails("AllProcurementRoles", "", "");%>
                                                <tr>
                                                    <td class="ff">Approving Authority : <span>*</span></td>
                                                    <td>
                                                        <select name="authority" class="formTxtBox_1" id="cmbAuthority" style="width:200px;">
                                                       <!--     <option value="">- Select Contract Approving Authority -</option> -->
                                                            <%
                                                                for (CommonAppData commonApp : appListDtBean) {
                                                                    if ("PD".equalsIgnoreCase(commonApp.getFieldName2())) {
                                                                        if (!"".equalsIgnoreCase(prjName)) {
                                                                            if(!commonApp.getFieldName2().equalsIgnoreCase("Hope")){
                                                                            continue;
                                                                            }
                                                                            
                                                            %>
                                                            <option value="<%=commonApp.getFieldName1()%>" <% if (isEdit) {
                                                                    if (Integer.parseInt(commonApp.getFieldName1()) == appViewDtBean.getAaEmpId()) {
                                                                        out.print("selected");
                                                                    }
                                                                }else{
                                                                     if (commonApp.getFieldName2().equalsIgnoreCase("Hope")) {
                                                                        out.print("selected");
                                                                    }   
                                                                }
                                                                %>   >
                                                                <%if (commonApp.getFieldName2().equalsIgnoreCase("Hope")) {
                                                                        out.print("HOPA");
                                                                    } else if (commonApp.getFieldName2().equalsIgnoreCase("PE")) {
                                                                        out.print("PA");
                                                                    } else {
                                                                        out.print(commonApp.getFieldName2());
                                                                    }%></option>
                                                                <%
                                                                    }
                                                                } else {
                                                                    String temp = commonApp.getFieldName2();

                                                                    if(!commonApp.getFieldName2().equalsIgnoreCase("Hope")){ //approving authority options except hopa hide by aprojit
                                                                    continue;
                                                                    }
                                                                %>
                                                            <option value="<%=commonApp.getFieldName1()%>" <%if (isEdit) {
                                                                    if (Integer.parseInt(commonApp.getFieldName1()) == appViewDtBean.getAaEmpId()) {
                                                                        out.print("selected");
                                                                    }
                                                                }else{
                                                                     if (commonApp.getFieldName2().equalsIgnoreCase("Hope")) {
                                                                        out.print("selected");
                                                                    }
                                                                }
                                                                %>   >
                                                                <%if (commonApp.getFieldName2().equalsIgnoreCase("Hope")) {
                                                                        out.print("HOPA");
                                                                    } else if (commonApp.getFieldName2().equalsIgnoreCase("PE")) {
                                                                        out.print("PA");
                                                                    } else {
                                                                        out.print(commonApp.getFieldName2());
                                                                    }%></option>
                                                                <%
                                                                        }
                                                                    }
                                                                %>
                                                        </select>
                                                        <span class="reqF_2" id="msgAuthority"></span>
                                                    </td>
                                                </tr>
                                                <tr id="trPQReq" <%if (isEdit) {
                                                        if (!"Works".equalsIgnoreCase(appViewDtBean.getProcurementnature())) {%>style="display:none"<%}
                                                        } else {%>style="display:none"<%}%>>
                                                    <td class="ff">PQ Requires : <span>*</span></td>
                                                    <td>
                                                        <select name="pqrequires" class="formTxtBox_1" id="cmbPQRequires" style="width:200px;" >
                                                            <option value="">-- Select PQ Requires --</option>
                                                            <option value="Yes" disabled <%if (isEdit) {
                                                                    if ("Yes".equalsIgnoreCase(appViewDtBean.getIsPQRequired())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>Yes</option>
                                                            <option value="No" <%if (isEdit) {
                                                                    if ("No".equalsIgnoreCase(appViewDtBean.getIsPQRequired())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>No</option>
                                                        </select>
                                                        <span id="msgPQType" style="color: red">&nbsp;</span>
                                                        <%if (isEdit) {%>
                                                        <script>
                                                            pqReq = '<%=appViewDtBean.getIsPQRequired()%>';
                                                        </script>
                                                        <%}%>
                                                    </td>
                                                </tr>
                                                <tr id="trRFReq" <%if (isEdit) {
                                                        if (!"Services".equalsIgnoreCase(appViewDtBean.getProcurementnature())) {%>style="display:none"<%}
                                                        } else {%>style="display:none"<%}%>>
                                                    <td class="ff">REOI / RFP Requires: <span>*</span></td> <!--REOI / RFA / RFP Requires:  -->
                                                    <td>
                                                        <select name="rfarequires" class="formTxtBox_1" id="cmbRFARequires" style="width:200px;">
                                                            <option value="">- Select REOI / RFP Requires -</option> <!--Select REOI / RFA / RFP Requires -->
                                                            <option <%if (isEdit) {
                                                                    if ("REOI".equalsIgnoreCase(appViewDtBean.getReoiRfaRequired().trim())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%> value="REOI">REOI</option>
                                                       <%--     <option <%if (isEdit) {
                                                                    if ("RFA".equalsIgnoreCase(appViewDtBean.getReoiRfaRequired().trim())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%> value="RFA">RFA</option> --%>
                                                            <option <%if (isEdit) {
                                                                    if ("RFP".equalsIgnoreCase(appViewDtBean.getReoiRfaRequired().trim())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%> value="RFP">RFP</option>
                                                        </select>
                                                        <span id="msgRFAType" style="color: red">&nbsp;</span>
                                                        <%if (isEdit) {%>
                                                        <script>
                                                            reoReq = '<%=appViewDtBean.getReoiRfaRequired()%>';
                                                        </script>
                                                        <%}%>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td class="ff">Procurement Type : <span>*</span></td>
                                                    <td>
                                                        <!--Change NCT to NCB and ICT to ICB by Proshanto-->
                                                        <select name="procuretype" class="formTxtBox_1" id="cmbProcureType" style="width:200px;" onblur="setcmbProcureType()">
                                                            <option value="">- Select Procurement Type -</option>
                                                            <option value="NCT" <%if (isEdit) {
                                                                    if ("NCT".equalsIgnoreCase(appViewDtBean.getProcurementType())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>NCB </option>
                                                                <option value="ICT" disabled <%if (isEdit) {
                                                                    if ("ICT".equalsIgnoreCase(appViewDtBean.getProcurementType())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>ICB</option>
                                                        </select>
                                                        <span class="reqF_2" id="msgProcureType"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Procurement Method : <span>*</span></td>
                                                    <td>
                                                        <select name="procuremethod" class="formTxtBox_1" id="cmbProcureMethod" style="width:200px;" onChange="showHideSelectPeOffice();" onblur="setcmbProcureMethod()">
                                                            <option value="">- Select Procurement Method -</option>
                                                        </select>
                                                        <input type="hidden" name="hdnPM" id="hdnPM" value="<%if (isEdit) {
                                                                out.print(appViewDtBean.getProcMethodName(appViewDtBean.getProcurementMethodId()));
                                                            }%>"/>
                                                        <span class="reqF_2" id="msgProcureMethod"></span>
                                                    </td>
                                                </tr>
                                                <%if (isEdit) {%>
                                                <script>
                                                    setSerType(document.getElementById("cmbProcureNature"), false);
                                                </script>
                                                <%}%>
                                                
                                                <%--Nitish Start --%>
                                                <tr>
                                                    <td class="ff">Source of Fund : </td>
                                                    <%--<td><%=srcFund%>
                                                        <input type="hidden" name="sourcefund" id="cmbSourceFund" value="<%=srcFund%>"/>
                                                    </td>--%>
                                                    
                                                    <%-- YES Project Name and (Budget Type ==  Anything --%>
                                                    
                                                    <%if( !"".equalsIgnoreCase(prjName) ) {%>
                                                        <td><%=srcFund%></td>
                                                        <input type="hidden" name="sourcefund" id="cmbSourceFund" value="<%=srcFund%>"/>
                                                    <%}%>
                                                    
                                                    <%-- YES Acitivity Name and (Budget Type ==  Recurrent/Capital --%>
                                                    
                                                    <%if( !"".equalsIgnoreCase(activityName)  && (budgetType.equalsIgnoreCase("Capital") || budgetType.equalsIgnoreCase("Recurrent")) ) {%>
                                                    <td>
                                                        <select name="sourcefund" class="formTxtBox_1" id="cmbSourceOfFund" style="width:200px;" onblur="setSourceFund(this)">
                                                            <option value="">- Select Source of Fund -</option>
                                                            <option value="RGOB"<%if (isEdit) {
                                                                    if ("RGOB".equalsIgnoreCase(appViewDtBean.getSourceOfFund())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>RGOB</option>
                                                            <option value="DONOR"<%if (isEdit) {
                                                                    if ("DONOR".equalsIgnoreCase(appViewDtBean.getSourceOfFund())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>DONNER</option>
                                                            <option value="OWN FUND"<%if (isEdit) {
                                                                    if ("OWN FUND".equalsIgnoreCase(appViewDtBean.getSourceOfFund())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>OWN FUND</option>
                                                        </select>
                                                        <span class="reqF_2" id="msgSourceFund"></span>
                                                    </td>     
                                                    <%}%>
                                                    
                                                    <%-- No Acitivity Name, No project but Budget Type ==  Recurrent --%>
                                                    
                                                    <%if( "".equalsIgnoreCase(activityName) && "".equalsIgnoreCase(prjName) && (budgetType.equalsIgnoreCase("Recurrent")) ) {%>
                                                    <td>
                                                        <select name="sourcefund" class="formTxtBox_1" id="cmbSourceOfFund" style="width:200px;" onblur="setSourceFund(this)">
                                                            <option value="">- Select Source of Fund -</option>
                                                            <option value="RGOB"<%if (isEdit) {
                                                                    if ("RGOB".equalsIgnoreCase(appViewDtBean.getSourceOfFund())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>RGOB</option>
                                                            <option value="OWN FUND"<%if (isEdit) {
                                                                    if ("OWN FUND".equalsIgnoreCase(appViewDtBean.getSourceOfFund())) {
                                                                        out.print("selected");
                                                                    }
                                                                }%>>OWN FUND</option>
                                                        </select>
                                                        <span class="reqF_2" id="msgSourceFund"></span>
                                                    </td>     
                                                    <%}%>
                                                    
                                                    <%-- YES Acitivity Name, but Budget Type ==  Own Fund --%>
                                                    
                                                    <%if( !"".equalsIgnoreCase(activityName)  && (budgetType.equalsIgnoreCase("Own Fund"))  ) {%>
                                                    <td>
                                                        <%=srcFund%>
                                                        <input type="hidden" name="sourcefund" id="cmbSourceFund" value="<%=srcFund%>"/>
                                                    </td>     
                                                    <%}%>
                                                    
                                                    <%-- No Acitivity Name, No project but Budget Type ==  Own Fund --%>
                                                    
                                                    <%if( "".equalsIgnoreCase(activityName) && "".equalsIgnoreCase(prjName) && (budgetType.equalsIgnoreCase("Own Fund")) ) {%>
                                                    <td>
                                                        <%=srcFund%>
                                                        <input type="hidden" name="sourcefund" id="cmbSourceFund" value="<%=srcFund%>"/>
                                                    </td>     
                                                    <%}%>
                                                    
                                                <%--
                                                    <td>
                                                        <select name="sourceOfFund" class="formTxtBox_1" id="cmbSourceOfFund" style="width:200px;">
                                                            <option value="">- Select Source of Fund -</option>
                                                            <option value="RGOB">RGOB</option>
                                                            <option value="DONNER">DONNER</option>
                                                            <option value="OWN FUND">OWN FUND</option>
                                                        </select>
                                                    </td>
                                                --%>        
                                                <%--Nitish END --%>
                                                </tr>
                                                <%if (!"".equalsIgnoreCase(devPartner)) {%>
                                                <tr id="trDevPrt">
                                                    <td class="ff">Development Partners : </td>
                                                    <td>
                                                        <label id="lblDevPrt" name="lbldevPrt"><%=devPartner%></label>
                                                    </td>
                                                </tr>
                                                <%}%>

<%--    //Hide Office Framework for FrameWork Contract Mmethod:: nitish--24/05/2017

                                                <tr id="showHidePeOffice" <%
                                                    if (isEdit) {
                                                        if (appViewDtBean.getProcurementMethodId() != 18) {%>
                                                    style="display: none"
                                                    <% }
                                                    } else {
                                                    %>
                                                    style="display: none"
                                                    <%
                                                        }
                                                    %>>
                                                    <td class="ff">Select Hierarchy Node :</td>
                                                    <td colspan="3"><input type="hidden" name="viewType" id="viewType" value="Live"/>
                                                        <%
                                                            AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
                                                            String strUserTypeId = "";
                                                            Object objUserId = session.getAttribute("userId");
                                                            if (objUserId != null) {
                                                                strUserTypeId = session.getAttribute("userTypeId").toString();
                                                            }
                                                            if (objUserId != null) {
                                                                if (Integer.parseInt(strUserTypeId) == 5) {
                                                                    Object[] objData = mISService.getOrgAdminOrgName(Integer.parseInt(objUserId.toString()));
                                                                    out.print(objData[1].toString());
                                                                    out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                                                    out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                                                    out.print("<input type=\"hidden\" id=\"uTypeId\" name=\"uTypeId\" value=\"" + Integer.parseInt(strUserTypeId) + "\"/>");
                                                                } else {%>
                                                        <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 300px;"
                                                               id="txtdepartment" onblur="showHide();
                                                                       checkCondition();"  readonly style="width: 200px;"/>
                                                        <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                                        <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=addpackage', '', 'width=350px,height=400px,scrollbars=1', '');">
                                                            <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                        </a>

                                                        <% }
                                                        } else {%>

                                                        <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 300px;"
                                                               id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                                                        <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                                        <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=addpackage', '', 'width=350px,height=400px,scrollbars=1', '');">
                                                            <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                        </a>
                                                    </td> <%}%>

                                                </tr>

                                                <tr id="showHideMapPeTable"
                                                    <%
                                                        if (isEdit) {
                                                            if (appViewDtBean.getProcurementMethodId() != 18) {%>
                                                    style="display: none"
                                                    <% }
                                                    } else {
                                                    %>
                                                    style="display: none"
                                                    <%
                                                        } %>
                                                    >
                                                    <td colspan='2'>
                                                        <table id="resultListMap" width="100%" cellspacing="0" class="tableList_1 t_space" >
                                                            <thead>
                                                                <tr>
                                                                    <th width="7%" class="t-align-center">Select All<input type="checkbox" onclick="checkUncheck('map', this);" id="mapallchk"/></th>
                                                                    <th class="t-align-left" width="5%">PA Office Name</th>
                                                                    <th class="t-align-left" width="20%">Dzongkhag/District</th>
                                                                    <th class="t-align-left" width="15%">Address</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                        <label class="formBtn_1">
                                                            <input type="button" name="AddPeOfc" id="AddPeOfc" value="Add to List" onclick="AddPeOfcToList();"/>
                                                        </label>
                                                    </td>
                                                </tr>    

                                                <tr id="showHideUnmapPeTable" 
                                                    <%
                                                        if (isEdit) {
                                                            if (appViewDtBean.getProcurementMethodId() != 18) {%>
                                                    style="display: none"
                                                    <% }
                                                    } else {
                                                    %>
                                                    style="display: none"
                                                    <%
                                                        } %>
                                                    >
                                                    <td colspan='2'>
                                                        <table id="resultListUnmap" width="100%" cellspacing="0" class="tableList_1 t_space" >
                                                            <thead>
                                                                <tr>
                                                                    <th width="7%" class="t-align-center">Select All<input type="checkbox" onclick="checkUncheckUnmap('map', this);" id="unmapallchk"/></th>
                                                                    <th class="t-align-left" width="5%">PA Office Name</th>
                                                                    <th class="t-align-left" width="20%">Dzongkhag/District</th>
                                                                    <th class="t-align-left" width="15%">Address</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <%
                                                                    if (isEdit) {
                                                                        int i = 0;
                                                                        if (appViewDtBean.getProcurementMethodId() == 18) {
                                                                            for (TblAppFrameworkOffice peOfc : appListFrameworkOfc) {
                                                                %>
                                                                <tr class="peOfficeTableRow">
                                                                    <td class="t-align-center"><input name="officeid" type="checkbox" value="<%=peOfc.getPeofficeId()%>" id="chkUnmap<%=i%>" chk="map" /></td>
                                                                    <td class="t-align-center"><%=peOfc.getOfficeName()%>
                                                                        <input type="hidden" name="officeName" id="officeName" value= "<%=peOfc.getOfficeName()%>" />
                                                                    </td>
                                                                    <td class="t-align-center"><%=peOfc.getState()%>
                                                                        <input type="hidden" name="officeCity" id="officeCity" value= "<%=peOfc.getState()%>" />
                                                                    </td>
                                                                    <td class="t-align-center"><%=peOfc.getAddress()%>
                                                                        <input type="hidden" name="officeAddress" id="officeAddress" value= "<%=peOfc.getAddress()%>" />
                                                                    </td>
                                                                </tr>
                                                                <%
                                                                        i++;
                                                                    }
                                                                    if (i == 0) {
                                                                %>
                                                                <tr><td colspan='9' class='t-align-center'><span style='color:red;'>No Records Found</span></td></tr>
                                                                <%
                                                                            }
                                                                        }
                                                                    }
                                                                %>
                                                            </tbody>
                                                        </table>
                                                        <label class="formBtn_1">
                                                            <input type="button" name="RemovePeOfc" id="RemovePeOfc" value="Remove" onclick="RemovePeOfcFromList();"/>
                                                        </label>
                                                        <span id="msgFCPEOfc" class="reqF_2"></span>
                                                    </td>
                                                </tr>
Nitish End 24/05/2017--%>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td align="left">
                                                        <%if (!isEdit) {%>
                                                        <label class="formBtn_1"><input type="submit" name="btnNext" id="btnNext" value="Next" onClick="return validateFun(false);"/></label>&nbsp;&nbsp;
                                                        <%} else {%>
                                                        <%  if (isRevision) { %>
                                                        <label class="formBtn_1"><input type="submit" name="btnRevise" id="btnRevise" value="Revise" onClick="return validateFun(true);"/></label>
                                                        <% } else { %>
                                                        <label class="formBtn_1"><input type="submit" name="update" id="btnUpdate" value="Update" onClick="return validateFun(true);"/></label>
                                                        <% }%>
                                                        <label class="formBtn_1">
                                                            <input type="button" name="cancel" id="btnCancel" value="Cancel" onclick="location.href = 'APPDashboard.jsp?appID=<%=appId%>';"/>
                                                        </label>
                                                        <%}%>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div>&nbsp;</div>
                                        </form>
                                        <% }%>
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>

                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                </div>
            </div>
        </body>

        <script type="text/javascript">

            function loadOffice() {
                var deptId = $('#txtdepartmentid').val();
                $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId, funName: 'officeList'}, function (j) {
                    $("#resultListMap tbody").html(j);
                });
                checkSelectAll();
            }

            function checkUncheck(val, chkall) {
                $("#resultListMap tbody tr td input:checkbox[chk='" + val + "']").each(function () {
                    if ($(chkall).attr('checked')) {
                        $(this).attr('checked', true);
                    } else {
                        $(this).removeAttr('checked');
                    }
                });
            }

            function checkUncheckUnmap(val, chkall) {
                $("#resultListUnmap tbody tr td input:checkbox[chk='" + val + "']").each(function () {
                    if ($(chkall).attr('checked')) {
                        $(this).attr('checked', true);
                    } else {
                        $(this).removeAttr('checked');
                    }
                });
            }

            function checkSelectAll()
            {
                var i = parseInt($('#resultListMap tr').length);
                if (i > 2) {
                    $('#mapallchk').attr('checked', true);
                }
            }
            function showHideSelectPeOffice(obj) {
                if ('18' == $("#cmbProcureMethod").find('option:selected').val()) {
                    //$('#showHidePeOffice').show();
                    //$('#showHideMapPeTable').show();
                    //$('#showHideUnmapPeTable').show();  //Hide Office Framework for FrameWork Contract Mmethod:: nitish--24/05/2017
                } else {
                    $('#showHidePeOffice').hide();
                    $('#showHideMapPeTable').hide();
                    $('#showHideUnmapPeTable').hide();
                    $('#resultListMap tbody tr').remove();
                    $('#resultListUnmap tbody tr').remove();
                    $('#txtdepartment').val("");

                }
            }

            function AddPeOfcToList() {
                $("#resultListMap tbody tr td input[id^='chkUnmap']").each(function () {
                    if ($(this).attr('checked')) {
                        var flag = 1;
                        var officerId = $(this).val();
                        $("#resultListUnmap tbody tr td input[id^='chkUnmap']").each(function () {
                            if ($(this).val() == officerId)
                                flag = 0;
                        });
                        if (flag == 1) {
                            var row = $(this).closest('tr').clone();
                            $('#resultListUnmap tbody').append(row);
                            $(this).closest('tr').remove();
                        } else if (flag == 0) {
                            $(this).closest('tr').remove()
                        }
                        ;
                    }
                });
            }
            function RemovePeOfcFromList() {
                $("#resultListUnmap tbody tr td input[id^='chkUnmap']").each(function () {
                    if ($(this).attr('checked')) {
                        $(this).closest('tr').remove();
                    }
                });
            }

            $('#frmAddPackageDetail').submit(function () {
                $('#resultListMap tbody tr').remove();
                $("#resultListUnmap tbody tr td input[id^='chkUnmap']").each(function () {
                    $(this).attr('checked', true);
                });
                return true;
            });

    </script>

    <script type="text/javascript">
        var procNature = "";
        var pqReq = "";
        var reoReq = "";
        var serviceType = "";
        var flagPkgNo = true;
        var flagAllBud = true;
        var counter = 1;
        var delCnt = 0;
        $(function () {
            $('#linkAddLot').click(function () {
                counter = eval(document.getElementById("hdnTotLot").value);
                delCnt = eval(document.getElementById("hdnDelLot").value);
                if ((eval(counter - delCnt)) >= 5) {
                    $('span.#lotMsg').css("display", "block");
                    $('span.#lotMsg').css("color", "red");
                    $('span.#lotMsg').html('Max. 5 lots are allowed');
                } else {
                    $('span.#lotMsg').css("display", "none");
                    var htmlEle = "<tr id='trLot_" + (counter + 1) + "'>" +
                            "<td class='t-align-center'><input type='checkbox' name='packagedetail_" + (counter + 1) + "' id='chkPackageDetail_" + (counter + 1) + "' value='" + (counter + 1) + "'/></td>" +
                            "<td class='t-align-center'><input name='lotno_" + (counter + 1) + "' type='text' class='formTxtBox_1' id='txtLotNo_" + (counter + 1) + "' style='width:80px;' onBlur='chkLotNoBlank(this);chkLotNo(this);'/><span id='msgLotNo_" + (counter + 1) + "' style='color: red; ' >&nbsp;</span></td>" +
                            "<td class='t-align-center'><textarea name='lotdesc_" + (counter + 1) + "' cols='20' rows='3' class='formTxtBox_1' id='txtLotDesc_" + (counter + 1) + "' style='width:250px;' onBlur='chkLotDetBlank(this);'></textarea><span id='msgLotDesc_" + (counter + 1) + "' style='color: red; '>&nbsp;</span></td>" +
                            "<td class='t-align-center'><input name='quantity_" + (counter + 1) + "' type='text' class='formTxtBox_1' id='txtQuantity_" + (counter + 1) + "' style='width:80px;' maxlength='16' onBlur='chkQtyBlank(this);'/><span id='msgLotQty_" + (counter + 1) + "' style='color: red; '>&nbsp;</span></td>" +
                            "<td class='t-align-center'><input name='unit_" + (counter + 1) + "' type='text' class='formTxtBox_1' id='txtUnit_" + (counter + 1) + "' style='width:80px;' onBlur='chkUnitBlank(this);'/><span id='msgLotUnit_" + (counter + 1) + "' style='color: red; '>&nbsp;</span></td>" +
                            "<td class='t-align-center'><input name='estimatecost_" + (counter + 1) + "' type='text' class='formTxtBox_1' autocomplete='off' id='txtEstimateCost_" + (counter + 1) + "' style='width:80px;' onChange='setPkgEstCost(this);' onblur=\"chkEstBlank(this); CONVERTWORD(((this.value)*1000000),'PckfigVal_" + (counter + 1) + "');\"/><br /><span id='msgLotCost_" + (counter + 1) + "' style='color: red; '>&nbsp;</span><span id='PckfigVal_" + (counter + 1) + "'></span></td>" +
                            "</tr>";
                    $("#tblLots").append(htmlEle);
                    document.getElementById("hdnTotLot").value = (counter + 1);
                }
            });
        });

        $(function () {
            $('#linkDelLot').click(function () {
                counter = eval(document.getElementById("hdnTotLot").value);
                delCnt = eval(document.getElementById("hdnDelLot").value);

                var tmpCnt = 0;
                for (var i = 1; i <= counter; i++) {
                    if (document.getElementById("chkPackageDetail_" + i) != null && document.getElementById("chkPackageDetail_" + i).checked) {
                        tmpCnt++;
                    }
                }

                if (tmpCnt == 0)
                {
                    $('span.#lotMsg').css("display", "block");
                    $('span.#lotMsg').css("color", "red");
                    $('span.#lotMsg').html('Please select lot(s)');
                } else
                {
                    if (tmpCnt == (eval(counter - delCnt))) {
                        $('span.#lotMsg').css("display", "block");
                        $('span.#lotMsg').css("color", "red");
                        $('span.#lotMsg').html('Min 1 lot required');
                    } else {
                        if (confirm("Do you really want to remove selected lot(s)?"))
                        {
                            for (var i = 1; i <= counter; i++) {
                                if (document.getElementById("chkPackageDetail_" + i) != null && document.getElementById("chkPackageDetail_" + i).checked) {
                                    $("tr[id='trLot_" + i + "']").remove();
                                    $('span.#lotMsg').css("display", "none");
                                    delCnt++;
                                }
                            }
                            document.getElementById("hdnDelLot").value = delCnt;
                        }
                    }

                    var tmpTotEstCost = 0;
                    for (var i = 1; i <= counter; i++) {
                        if (document.getElementById("txtEstimateCost_" + i) != null) {
                            if (document.getElementById("txtEstimateCost_" + i).value != "") {
                                tmpTotEstCost = eval(eval(tmpTotEstCost) + eval(parseFloat(document.getElementById("txtEstimateCost_" + i).value).toFixed(2)));
                            }
                        }
                    }
                    document.getElementById("txtPackageCost").value = tmpTotEstCost;
                }
            });
        });
        $(function () {
            $('#cmbPQRequires').change(function () {
                if ($('#cmbPQRequires').val() != '') {
                    $('#msgPQType').html('');
                    $.post("<%=request.getContextPath()%>/APPServlet", {projectId: $('#cmbPQRequires').val(), procType: $('#cmbProcureType').val(), param4: $('#hdnPM').val(), procNature: $('#cmbProcureNature').val(), funName: 'getPM'}, function (j) {
                        $("select#cmbProcureMethod").html(j);
                    });
                } else {
                    $('#msgPQType').html('<br/>Please select PQ Requires');
                }
            });
        });

        $(function () {
            $('#cmbProcureType').change(function () {
                if ($('#cmbProcureType').val() != '') {
                    $('#msgPQType').html('');
                    var PQRequires;

                    if ($('#cmbProcureNature').val() != "Services") {
                        if ($('#cmbPQRequires').val() == "") {
                            PQRequires = "No";
                        } else {
                            PQRequires = $('#cmbPQRequires').val();
                        }
                    } else {
                        PQRequires = $('#cmbRFARequires').val();
                    }

                    $.post("<%=request.getContextPath()%>/APPServlet", {projectId: PQRequires, procType: $('#cmbProcureType').val(), param4: $('#hdnPM').val(), procNature: $('#cmbProcureNature').val(), funName: 'getPM'}, function (j) {
                        $("select#cmbProcureMethod").html(j);
                    });
                }
            });
        });
        $(function () {
            $('#cmbRFARequires').change(function () {
                if ($('#cmbRFARequires').val() != '') {
                    $.post("<%=request.getContextPath()%>/APPServlet", {projectId: $('#cmbRFARequires').val(), cmbId: $('#cmbServiceType').val(), procType: $('#cmbProcureType').val(), param4: $('#hdnPM').val(), funName: 'getPM'}, function (j) {
                        $("select#cmbProcureMethod").html(j);
                    });
                    $('#msgRFAType').html('');
                } else {
                    $('#msgRFAType').html('<br/>Please select REOI / RFA / RFP Requires');
                }
            });
        });

        $(function () {
            $('#cmbServiceType').change(function () {
                $.post("<%=request.getContextPath()%>/APPServlet", {projectId: $('#cmbRFARequires').val(), cmbId: $('#cmbServiceType').val(), procType: $('#cmbProcureType').val(), param4: $('#hdnPM').val(), procNature: $('#cmbProcureNature').val(), funName: 'getPM'}, function (j) {
                    $("select#cmbProcureMethod").html(j);
                });
            });
        });
        //      function chkEstCost()
        //      {
        //  var iChars = "!@#$%^&*()+=-[]\\\';,/{}|\":<>?";
        //  var txtAllocateVal = document.getElementById("txtAllocatedBudget").value;
        //          document.getElementById("allBudMsg").innerHTML = "";
        //
        //  if(txtAllocateVal!="")
        //  {
        //
        //      if(!/^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/.test(txtAllocateVal)){
        //            document.getElementById("allBudMsg").innerHTML = "<br/>Please enter numeric values only. Max 2 decimals allowed";
        //            return false;
        //      }
        //
        //       for (var i = 0; i < txtAllocateVal.length; i++) {
        //            if (iChars.indexOf(txtAllocateVal.charAt(i)) != -1) {
        //                document.getElementById("allBudMsg").innerHTML = "<br />Special Characters are not allowed";
        //                return false;
        //            }
        //       }
        //  }else{
        //      document.getElementById("allBudMsg").innerHTML = "";
        //  }
        //
        //  if(document.getElementById("txtAllocatedBudget").value.indexOf(".")==0){
        //        document.getElementById("txtAllocatedBudget").value = '0'+document.getElementById("txtAllocatedBudget").value;
        //        //return false;
        //  }
        //
        //  if(document.getElementById("txtAllocatedBudget").value.indexOf(".")!=-1)
        //  {
        //      //alert(((document.getElementById("txtAllocatedBudget").value).substr(parseInt(document.getElementById("txtAllocatedBudget").value.indexOf(".")+1),document.getElementById("txtAllocatedBudget").value.length)).length);
        //      if(((document.getElementById("txtAllocatedBudget").value).substr(parseInt(document.getElementById("txtAllocatedBudget").value.indexOf(".")+1),document.getElementById("txtAllocatedBudget").value.length)).length > 2)
        //      {
        //            document.getElementById("allBudMsg").innerHTML = "</br>Please enter numeric values only. Max 2 decimals allowed";
        //            return false;
        //      }
        //      else
        //      {
        //            document.getElementById("allBudMsg").innerHTML = "";
        //      }
        //  }
        //
        //  if(eval(document.getElementById("txtAllocatedBudget").value) < eval(document.getElementById("txtPackageCost").value)){
        //      $('#trWarnMsg').show();
        //      document.getElementById("warnMsg").innerHTML = "Estimated cost is higher than allocated budget";
        //  }else{
        //      $('#trWarnMsg').hide();
        //
        //  }
        //}

        function setPkgEstCost(obj) {
            var totCnt = document.getElementById("hdnTotLot").value;
            var totEstCost = 0;
            for (var i = 1; i <= totCnt; i++) {
                if (document.getElementById("txtEstimateCost_" + i) != null) {
                    if (document.getElementById("txtEstimateCost_" + i).value != "") {
                        totEstCost = eval(eval(totEstCost) + eval(parseFloat(document.getElementById("txtEstimateCost_" + i).value).toFixed(8)));
                        totEstCost = roundNumber(totEstCost, 8);
                    }
                }
            }
            document.getElementById("txtPackageCost").value = totEstCost;
            CONVERTWORD((totEstCost*1000000), 'pkgEstCost')
            //chkEstCost();
        }

        function roundNumber(num, dec)
        {
            var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
            return result;
        }

        function chkLotNo(obj) {
            var totCnt = document.getElementById("hdnTotLot").value;
            if (obj.value == "") {
                if ("Services" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                    document.getElementById("msgLotNo_" + ((obj.id.substr(obj.id.indexOf("_") + 1)))).innerHTML = "<br/>Please enter Package No";
                } else {
                    document.getElementById("msgLotNo_" + ((obj.id.substr(obj.id.indexOf("_") + 1)))).innerHTML = "<br/>Please enter Lot No";
                }
                return false;
            }
            for (var i = 1; i <= totCnt; i++) {
                if ((obj.id.substr(obj.id.indexOf("_") + 1)) != i) {
                    if (obj.value == document.getElementById("txtLotNo_" + i).value) {
                        document.getElementById("msgLotNo_" + ((obj.id.substr(obj.id.indexOf("_") + 1)))).innerHTML = "<br/>Lot no already exist";
                        obj.focus();
                        return false;
                    } else {
                        document.getElementById("msgLotNo_" + ((obj.id.substr(obj.id.indexOf("_") + 1)))).innerHTML = "";
                    }
                }
            }
        }

        function chkLotNoBlank(obj) {
            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
            if (trim(obj.value) != "") {
                //if(!alphanumeric(obj.value)){
                //    document.getElementById("msgLotNo_"+i).innerHTML = "<br/>Please enter alphanumeric Values only";
                //    return true;
                //}else{
                document.getElementById("msgLotNo_" + i).innerHTML = "";
                return false;
                //}
            } else {
                if ("Services" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                    document.getElementById("msgLotNo_" + i).innerHTML = "<br/>Please enter Package No";
                } else {
                    document.getElementById("msgLotNo_" + i).innerHTML = "<br/>Please enter Lot No";
                }
                return true;
            }
        }

        function chkLotDetBlank(obj) {
            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
            if (trim(obj.value) != "") {
                document.getElementById("msgLotDesc_" + i).innerHTML = "";
                return false;
            } else {
                if ("Services" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                    document.getElementById("msgLotDesc_" + i).innerHTML = "<br/>Please enter Package Description";
                } else {
                    document.getElementById("msgLotDesc_" + i).innerHTML = "<br/>Please enter Lot Description";
                }

                return true;
            }
        }

        function chkQtyBlank(obj) {
            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
            document.getElementById("msgLotQty_" + i).innerHTML = "";
            if (trim(obj.value) != "") {
                if (obj.value.indexOf(".") == 0) {
                    obj.value = '0' + obj.value;
                }
                if (!numericDE(obj.value)) {
                    document.getElementById("msgLotQty_" + i).innerHTML = "<br/>Please enter numeric values only. Max 2 digits allowed after decimal point";
                    return true;
                } else {
                    document.getElementById("msgLotQty_" + i).innerHTML = "";
                    return false;
                }
            } else {
                if ("Goods" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                    document.getElementById("msgLotQty_" + i).innerHTML = "<br/>Please enter Quantity";
                }
                return true;
            }
        }

        function chkUnitBlank(obj) {
            var iChars = "!@#$%^&*()+=-[]\\\';,/{}|\":<>?";
            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
            var Value = obj.value;
            document.getElementById("msgLotUnit_" + i).innerHTML = "";
            if (trim(obj.value) != "") {
                if (!noNumeric(obj.value)) {
                    document.getElementById("msgLotUnit_" + i).innerHTML = "<br/>Numbers not allowed";
                    return true;
                }
                for (var r = 0; r < Value.length; r++)
                {
                    if (parseInt(Value.charAt(r)) >= 0 || parseInt(Value.charAt(r)) <= 9)
                    {
                        document.getElementById("msgLotUnit_" + i).innerHTML = "<br/>Numbers not allowed";
                        return true;
                    }
                }

                if (/^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/.test(obj.value)) {
                    document.getElementById("msgLotUnit_" + i).innerHTML = "<br/>Numbers not allowed";
                    return true;
                }
                //                    else if(Value.indexOf(".") > 0){
                //                      document.getElementById("msgLotUnit_"+i).innerHTML = "<br/>Decimal value is not allowed.";
                //                        return true;
                //                    }
                else {

                    for (var j = 0; j < obj.value.length; j++) {
                        if (iChars.indexOf(obj.value.charAt(j)) != -1) {
                            document.getElementById("msgLotUnit_" + i).innerHTML = "<br />Special Characters are not allowed";
                            return true;
                        }
                    }
                    document.getElementById("msgLotUnit_" + i).innerHTML = "";
                    return false;
                }

            } else {
                if ("Goods" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                    document.getElementById("msgLotUnit_" + i).innerHTML = "<br/>Please enter Unit";
                }
                return true;
            }
        }
        function chkEstBlank(obj) {
            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
            document.getElementById("msgLotCost_" + i).innerHTML = "";
            if (trim(obj.value) != "") {
                if (obj.value.indexOf(".") == 0) {
                    obj.value = '0' + obj.value;
                }
                if (!numericDE(obj.value)) {
                    document.getElementById("msgLotCost_" + i).innerHTML = "<br/>Please enter Positive numerical value. Maximum 8 digits are allowed after decimal point.";
                    return true;
                } else {
                    document.getElementById("msgLotCost_" + i).innerHTML = "";
                    return false;
                }
            } else {
                document.getElementById("msgLotCost_" + i).innerHTML = "<br/>Please enter Estimated Cost";
                return true;
            }
        }

        function checkPkgNo(obj)
        {
            var varPkgNo = trim(obj.value);
            var iChars = "!@#$%^&*()+=-[]\\';,{}|\":<>?";
            document.getElementById("msgPackageNo").innerHTML = "";
            if (trim(varPkgNo).length > 50) {
                document.getElementById("msgPackageNo").innerHTML = "<br />Max 50 characters are allowed"
                return false;
            }
            //                for (var j = 0; j < obj.value.length; j++) {
            //                    if (iChars.indexOf(obj.value.charAt(j)) != -1) {
            //                        document.getElementById("msgPackageNo").innerHTML = "<br />Special Characters are not allowed.";
            //                        // $('#msgPackageNo').html("Special Characters are not allowed.");
            //                        return true;
            //                    }
            //                }
            if (varPkgNo != "")
            {
                $('span.#pkgnoMsg').html("Checking for unique Package No...");
                $.post("<%=request.getContextPath()%>/APPServlet", {param1: varPkgNo, param2:<%=pkgId%>, funName: 'verifyPkgNo'}, function (j) {
                    if (j.toString().indexOf("OK", 0) != -1) {
                        $('span.#pkgnoMsg').css("color", "green");
                    } else if (j.toString().indexOf("Package", 0) != -1) {
                        $('span.#pkgnoMsg').css("color", "red");
                    }
                    $('span.#pkgnoMsg').html(j);
                    flagPkgNo = false;
                });
            } else
            {
                $('span.#pkgnoMsg').html("");
                flagPkgNo = true;
            }
        }

        function numeric(value) {
            return /^\d+$/.test(value);
        }
        function numericDE(value) {
            return  /^(\d+(\.\d{1,8})?)$/.test(value);
        }
        function alphanumeric(value) {
            return /^[\sa-zA-Z0-9\'\-]+$/.test(value);
        }

        function noNumeric(value) {
            return /[^0-9]/.test(value);
        }

        function setLotNo() {
            if ("Services" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                document.getElementById("txtLotNo_1").value = document.getElementById("txtPackageNo").value;
            }

        }

        function setLotDesc() 
        {
            if ("Services" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) 
            {
                document.getElementById("txtLotDesc_1").value = document.getElementById("txtaPackageDesc").value;
            }
            if ("Works" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) 
            {
                document.getElementById("txtLotDesc_1").value = document.getElementById("txtaPackageDesc").value;
            }
            //Nitish Start
            var packageDescription = document.getElementById("txtaPackageDesc").value;
            if(packageDescription != "")
            {
                $('#msgPackageDesc').html('');
            }
            else
            {
                $('#msgPackageDesc').html('Please enter Package Description');
            }
        }
        
        function settxtaCPV() 
        {
            var txtaCPV = document.getElementById("txtaCPV").value;
            if(txtaCPV != "")
            {
                $('#msgCPV').html('');
            }
            else
            {
                $('#msgCPV').html('Please Select Categories');
            }
        }
        
        function setcmbProcureType() 
        {
            var txtaCPV = document.getElementById("cmbProcureType").value;
            if(txtaCPV != "")
            {
                $('#msgProcureType').html('');
            }
            else
            {
                $('#msgProcureType').html('Please Select Procurement Type');
            }
        }
        
        function setcmbProcureMethod() 
        {
            var cmbProcureMethod = document.getElementById("cmbProcureMethod").value;
            if(cmbProcureMethod != "")
            {
                $('#msgProcureMethod').html('');
            }
            else
            {
                $('#msgProcureMethod').html('Please select Procurement Method');
            }
        }
        
        function setcmbWorkCategory() 
        {
            var cmbWorkCategory = document.getElementById("cmbWorkCategory").value;
            if(cmbWorkCategory != "")
            {
                $('#msgWorkCategory').html('');
            }
            else
            {
                $('#msgWorkCategory').html('Please select Work Category');
            }
        }
        
        function setbidderCategory(obj)
        {
            var bidderCategory = obj.value;
            if(bidderCategory != "")
            {
                $('#msgBidderCategory').html('');
            }
            else
            {
                $('#msgBidderCategory').html('Please select Bidder Category');
            }
        }
        
        function setcmbServiceType(obj)
        {
            var cmbServiceType = obj.value;
            if(cmbServiceType != "")
            {
                $('#msgSerType').html('');
            }
            else
            {
                $('#msgSerType').html('Please select Service Type');
            }
        }
        
        function setSourceFund(obj)
        {
            var cmbServiceType = obj.value;
            if(cmbServiceType != "")
            {
                $('#msgSourceFund').html('');
            }
            else
            {
                $('#msgSourceFund').html('Please select Source of Fund');
            }
        }
        
        
        //Nitish END
        
        function validateFun(varFlag) {
            var flag1 = true;
            
            //Nitish Start
            $('#msgSourceFund').html('');
            if (document.getElementById('cmbSourceOfFund').value == "") 
            {
                $('#msgSourceFund').html('<br/>Please select Source of Fund');
                flag1 = false;
            }
            //Nitish END
            
            $('#msgProcNature').html('');
            if (document.getElementById('cmbProcureNature').value == "") {
                $('#msgProcNature').html('<br/>Please select Procurement Category');
                flag1 = false;
            }
            $('#msgPackageNo').html('');
            if (trim(document.getElementById('txtPackageNo').value) == "") {
                $('#msgPackageNo').html('<br/>Please enter Package No');
                flag1 = false;
            }
            $('#msgPackageDesc').html('');
            if (document.getElementById('txtaPackageDesc').value.length == 0) {
                $('#msgPackageDesc').html('<br/>Please enter Package Description');
                flag1 = false;
            }
            if (document.getElementById('txtaPackageDesc').value.length > 2000) {
                $('#msgPackageDesc').html('<br/>Maximum 2000 characters are allowed');
                flag1 = false;
            }
            //aprojit-Start
            if ("Works" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                $('#msgBidderCategory').html('');
                if (document.getElementById('chkW1').checked == false && document.getElementById('chkW2').checked == false && document.getElementById('chkW3').checked == false && document.getElementById('chkW4').checked == false) {
                    $('#msgBidderCategory').html('<br/>Please select Bidder Category');
                    flag1 = false;
                }
                $('#msgWorkCategory').html('');
                if (document.getElementById('cmbWorkCategory').value == "") {
                    $('#msgWorkCategory').html('<br/>Please select Work Category');
                    flag1 = false;
                }
            }
            
            $('#msgdepoplanWork').html('');
//            if (document.getElementById('cmbdepoplanWork').value == "") {
//                $('#msgdepoplanWork').html('<br/>Please select a Category');
//                flag1 = false;
//            }
//            if ("DepositWork" == document.getElementById("cmbdepoplanWork").options[document.getElementById("cmbdepoplanWork").selectedIndex].value) {
//                if (document.getElementById("txtEntrustingAgency") != null) {
//                    if ($('#txtEntrustingAgency').val() == '') {
//                        $('#msgEntrustAgency').html('<br/>Please enter Entrusting Agency');
//                        flag1 = false;
//                    } else {
//                        $('#msgEntrustAgency').html('');
//                    }
//                }
//            }
//            if ("PlanedWork" == document.getElementById("cmbdepoplanWork").options[document.getElementById("cmbdepoplanWork").selectedIndex].value) {
//                if (document.getElementById("txtTimeFrame") != null) {
//                    if ($('#txtTimeFrame').val() == '') {
//                        $('#msgTimeFrame').html('<br/>Please enter Time Frame');
//                        flag1 = false;
//                    } else {
//                        $('#msgTimeFrame').html('');
//                    }
//                }
//            }
            //aprojit-End
            $('#msgCPV').html('');
            if (document.getElementById('txtaCPV').value == "") {
                $('#msgCPV').html('<br/>Please Select Categories');
                flag1 = false;
            }
            $('#msgAuthority').html('');
            if (document.getElementById('cmbAuthority').value == "") {
                $('#msgAuthority').html('<br/>Please select Contract Approving Authority');
                flag1 = false;
            }
            $('#msgProcureMethod').html('');
            if (document.getElementById('cmbProcureMethod').value == "") {
                $('#msgProcureMethod').html('<br/>Please select Procurement Method');
                flag1 = false;
            } else if ('18' == $("#cmbProcureMethod").find('option:selected').val()) {
                var rowCount = $('#resultListUnmap tbody tr').length;
                if (rowCount < 1) {
                    $('#msgFCPEOfc').html('<br/>Please add PA office');
                    //flag1 = false; // As there is no PA office is selected so this flag condition is stopped :: nitish
                }
            }
            $('#msgProcureType').html('');
            if (document.getElementById('cmbProcureType').value == "") {
                $('#msgProcureType').html('<br/>Please Select Procurement Type');
                flag1 = false;
            }

            if ("Services" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                if (document.getElementById("cmbServiceType") != null) {
                    if ($('#cmbServiceType').val() == '') {
                        $('#msgSerType').html('<br/>Please select Service Type');
                        flag1 = false;
                    } else {
                        $('#msgSerType').html('');
                    }
                }
            }

            var flag2 = true;
            if ("Services" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                if (document.getElementById("cmbRFARequires") != null) {
                    if ($('#cmbRFARequires').val() == '') {
                        $('#msgRFAType').html('<br/>Please select REOI / RFA / RFP Requires');
                        flag2 = false;
                    } else {
                        $('#msgRFAType').html('');
                    }
                }
            }


            if ("Works" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                if (document.getElementById("cmbPQRequires") != null) {
                    if ($('#cmbPQRequires').val() == '') {
                        $('#msgPQType').html('<br/>Please select PQ Requires');
                        flag2 = false;
                    } else {
                        $('#msgPQType').html('');
                    }
                }
            }

            var flag = true;

            counter = eval(document.getElementById("hdnTotLot").value);
            //alert(counter);
            for (var i = 1; i <= counter; i++) {
                if (document.getElementById("txtLotNo_" + i) != null) {
                    if (document.getElementById("txtLotNo_" + i).value == "") {

                        if ("Services" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                            $('#msgLotNo_' + i).html('<br/>Please enter Package No');
                        } else {
                            $('#msgLotNo_' + i).html('<br/>Please enter Lot No');
                        }
                        flag = false;
                    } else {
                        /*                        var iChars = "!@#$%^&*()+=-[]\\\';,/{}|\":<>?";
                         for (var j = 0; j < document.getElementById("txtLotNo_"+i).value.length; j++) {
                         if (iChars.indexOf(document.getElementById("txtLotNo_"+i).value.charAt(j)) != -1) {
                         document.getElementById("msgLotNo_"+i).innerHTML = "<br />Special Characters are not allowed";
                         flag = false;
                         }
                         }
                         */
                        if (document.getElementById("txtLotNo_" + i).value.length > 50) {
                            $('#msgLotNo_' + i).html('<br/>Maximum 50 characters are allowed');
                            flag = false;
                        }
                        if (document.getElementById("txtUnit_" + i).value.length > 50) {
                            $('#msgLotUnit_' + i).html('<br/>Maximum 50 characters are allowed');
                            flag = false;
                        }
                        //if(!alphanumeric(document.getElementById("txtLotNo_"+i).value)){
                        //    $('#msgLotNo_'+i).html('<br/>Please enter alphanumeric Values only.');
                        //    flag = false;
                        //}
                    }
                }
                if (document.getElementById("txtLotDesc_" + i) != null) {
                    if (trim(document.getElementById("txtLotDesc_" + i).value) == "") {
                        if ("Services" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                            $('#msgLotDesc_' + i).html('<br/>Please enter Package Description');
                        } else {
                            $('#msgLotDesc_' + i).html('<br/>Please enter Lot Description');
                        }
                        flag = false;
                    } else {
                        /*                var iChars = "!@#$%^&*()+=-[]\\\';,/{}|\":<>?";
                         for (var j = 0; j < document.getElementById("txtLotDesc_"+i).value.length; j++) {
                         if (iChars.indexOf(document.getElementById("txtLotDesc_"+i).value.charAt(j)) != -1) {
                         document.getElementById("msgLotDesc_"+i).innerHTML = "<br />Special Characters are not allowed";
                         flag = false;
                         }
                         }
                         */
                        if (document.getElementById("txtLotDesc_" + i).value.length > 2000) {
                            $('#msgLotDesc_' + i).html('<br/>Maximum 2000 characters are allowed');
                            flag = false;
                        }
                        if(/#/.test(document.getElementById("txtLotDesc_" + i).value)){
                           $('#msgLotDesc_' + i).html('<br/>"#" is not allowed in the Lot Description');
                           flag = false; 
                        }
                    }
                }
                if ("Goods" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                    if (document.getElementById("txtQuantity_" + i) != null) {
                        if (document.getElementById("txtQuantity_" + i).value == "") {
                            $('#msgLotQty_' + i).html('<br/>Please enter Quantity');
                            flag = false;
                        } else {
                            if (!numericDE(document.getElementById("txtQuantity_" + i).value)) {
                                $('#msgLotQty_' + i).html('<br/>Please enter numeric values only. Max 2 digits allowed after decimal');
                                flag = false;
                            }
                        }
                    }
                }
                if ("Goods" == document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text) {
                    if (document.getElementById("txtUnit_" + i) != null) {
                        if (document.getElementById("txtUnit_" + i).value == "") {
                            $('#msgLotUnit_' + i).html('<br/>Please enter Unit');
                            flag = false;
                        } else {
                            if (!noNumeric(document.getElementById("txtUnit_" + i).value)) {
                                $('#msgLotUnit_' + i).html('<br/>Numbers not allowed');
                                flag = false;
                            }
                        }
                    }
                }
                if (document.getElementById("txtEstimateCost_" + i) != null) {
                    //alert(document.getElementById("txtEstimateCost_"+i).value   );
                    if (document.getElementById("txtEstimateCost_" + i).value == "") {
                        $('#msgLotCost_' + i).html('<br/>Please enter Estimated Cost');
                        flag = false;
                    } else {
                        if (!numericDE(document.getElementById("txtEstimateCost_" + i).value)) {
                            $('#msgLotCost_' + i).html('<br/>Please enter Positive numerical value. Maximum 8 digits are allowed after decimal point.');
                            flag = false;
                        }
                    }
                }
            }

            var flag3 = true;
            var totCnt = document.getElementById("hdnTotLot").value;
            for (var i = 1; i <= totCnt; i++) {
                for (var j = 1; j <= totCnt; j++) {
                    if (document.getElementById("txtLotNo_" + j) != null && document.getElementById("txtLotNo_" + i) != null) {
                        if (document.getElementById("txtLotNo_" + j).value == "") {
                            if ("Services" != document.getElementById("cmbProcureNature").options[document.getElementById("cmbProcureNature").selectedIndex].text ) {
                                document.getElementById("msgLotNo_" + j).innerHTML = "<br/>Please enter Lot No";
                            } else {
                                document.getElementById("msgLotNo_" + j).innerHTML = "<br/>Please enter Package No";
                            }
                            document.getElementById("txtLotNo_" + i).focus();
                            flag3 = false;
                            break;
                        }
                        if (i != j) {
                            if (document.getElementById("txtLotNo_" + j).value == document.getElementById("txtLotNo_" + i).value) {
                                document.getElementById("msgLotNo_" + j).innerHTML = "<br/>Lot no already exist";
                                document.getElementById("txtLotNo_" + i).focus();
                                flag3 = false;
                                break;
                            }
                        }
                    }

                }
            }

            if (document.getElementById("pkgnoMsg") != null && (document.getElementById("pkgnoMsg").innerHTML == "OK" || document.getElementById("pkgnoMsg").innerHTML == "&nbsp;")) {
                flagPkgNo = true;
            } else {
                flagPkgNo = false;
            }

            if (document.getElementById("allBudMsg") != null && (document.getElementById("allBudMsg").innerHTML == "" || document.getElementById("allBudMsg").innerHTML == "&nbsp;")) {
                flagAllBud = true;
            } else {
                flagAllBud = false;
            }

            if (flag && flag1 && flag2 && flag3 && flagPkgNo && flagAllBud) {

                if (varFlag) {
                    varFlag = false;
                    if (document.getElementById("cmbProcureNature") != null) {
                        if (procNature != document.getElementById("cmbProcureNature").value) {
                            varFlag = false;
                        }
                    }
                    if (document.getElementById("cmbPQRequires") != null) {
                        if (document.getElementById("cmbPQRequires").value != pqReq) {
                            varFlag = false;
                        }
                    }
                    if (document.getElementById("cmbRFARequires") != null) {
                        if (document.getElementById("cmbRFARequires").value != reoReq) {
                            varFlag = false;
                        }
                    }
                }

                if (varFlag) {
                    if (confirm('Do you really want to update Package details? Modification in Package details will remove package dates.')) {
                        return true;
                    } else {
                        return false;
                    }
                }
                //else{
                //    if($("#frmAddPackageDetail").valid())
                //    {
                //        if(confirm('Click "Ok" to add the package dates else "Cancel" to Re-enter the details')){
                //          return true;
                //        }else{
                //            return false;
                //        }
                //    }
                // }
            } else {
                return false;
            }
        }
        function loadCPVTree()
        {
            window.open('../resources/common/CPVTree.jsp', 'CPVTree', 'menubar=0,scrollbars=1,width=700px');
        }

    </script>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabApp");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script>
        function redirectPage() {
            document.getElementById("frmRedirect").action = "APPDashboard.jsp";
            document.getElementById("frmRedirect").submit();
        }
    </script>
    <script language="javascript">
        function CONVERTWORD(no, msgPlace)
        {
            no = parseFloat(no).toFixed(2);
            if (document.getElementById(msgPlace))
            {
                //document.getElementById(msgPlace).innerHTML = DoIt(eval(no));
                document.getElementById(msgPlace).innerHTML = CurrencyConverter(eval(no));
            }
        }
        
    </script>
</html>
