<%--
    Document   : SearchPackageReport
    Created on : Feb 11, 2011, 2:01:38 PM
    Author     : Administrator
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.web.databean.AppReportDtBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.AppReportSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppPkgDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.APPReportService"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblAskProcurement"%>
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consolidated Annual Procurement Plan</title>
        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/home.css"/>
        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css"/>
        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/theme_1.css"/>
<!--        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css"/>
        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/jQuery/ui.jqgrid.css"/>
        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/jQuery/ui.multiselect.css"/>-->

<!--        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/grid.locale-en.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.jqGrid.min.js"></script>-->

<!--        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        
        String isPDF = "abc";
        if (request.getParameter("isPDF") != null) {
            isPDF = request.getParameter("isPDF");
        }
    %>
    <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
    <jsp:useBean id="appReportSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AppReportSrBean"/>
    <jsp:useBean id="peAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.PEAdminSrBean"/>
    <jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
    <jsp:useBean id="appServlet" scope="request" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <body onload="hide();">
        <div class="mainDiv">
            <div class="fixDiv" style="display: inline-block; min-width: 100%;">
            <%if(!isPDF.equalsIgnoreCase("true")) {%>
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <%}%>
            <!--Middle Content Table Start-->
            <%

                APPReportService appReportService = (APPReportService) AppContext.getSpringBean("APPReportService");
                AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
                CommonSearchDataMoreService commonService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                int columnSpan=0;
                long totalDays = 0;
                long dateDiff = 0;
                int appId = 0;
                int cmpAPPId = 0;
                int cntr = 0;
                
                if (request.getParameter("appId") != null && !"".equalsIgnoreCase(request.getParameter("appId"))) {
                    appId = Integer.parseInt(request.getParameter("appId"));
                }

                int userId = 0;
                String strUserId = "";
                int userTIdPkgRpt=0;
                if (!isPDF.equalsIgnoreCase("true")) {
                    if (session.getAttribute("userId") != null) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                        strUserId = session.getAttribute("userId").toString();
                        //appReportService.setAuditTrail(new AuditTrail(request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    }
                    if (session.getAttribute("userTypeId") != null) {
                        userTIdPkgRpt = Integer.parseInt(session.getAttribute("userTypeId").toString());
                    }
                    if(session.getAttribute("userId")!=null)
                    {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                        strUserId = session.getAttribute("userId").toString();
                        advAppSearchSrBean.setLogUserId(strUserId);
                        appReportSrBean.setLogUserId(strUserId);
                        peAdminSrBean.setLogUserId(strUserId);
                        appServlet.setLogUserId(strUserId);

                    }
                } else {
                    if (request.getParameter("uidforpdf") != null) {
                        userId = Integer.parseInt(request.getParameter("uidforpdf").toString());
                    }
                    if (request.getParameter("UTID") != null) {
                        userTIdPkgRpt = Integer.parseInt(request.getParameter("UTID").toString());
                    }
                }
            %>
        <script>
        function showHide()
        {
                if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                        document.getElementById('tblSearchBox').style.display = 'table';
                        document.getElementById('collExp').innerHTML = '- Advanced Search';
                }else{
                        document.getElementById('tblSearchBox').style.display = 'none';
                        document.getElementById('collExp').innerHTML = '+ Advanced Search';
                }
        }
        function hide()
        {
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
        }
        function validate()
        {
            document.getElementById("msgProjectName").innerHTML = "";
            document.getElementById("msgMinistry").innerHTML = "";
            document.getElementById("msgPEOffice").innerHTML = "";
            if(document.getElementById("cmbBudType")!=null && document.getElementById("cmbBudType").value==1)
            {
                if(document.getElementById("cmbPrjName")!=null && (document.getElementById("cmbPrjName").value=='' || document.getElementById("cmbPrjName").value=='0')){
                //if(document.getElemenetById("cmbPrjName")!=null && document.getElementById("cmbPrjName").value==""){
                    document.getElementById("msgProjectName").innerHTML = "Please select project.";
                    return false;
                }
            }

            <%
                    if (userTIdPkgRpt == 1) {
            %>
            if(document.getElementById("txtdepartment")!=null && document.getElementById("txtdepartment").value=="")
            {
                document.getElementById("msgMinistry").innerHTML = 'Please select Hierarchy Node.';
                return false;
            }
                    <%                        
                    }
            %>
            <%
                                if (userTIdPkgRpt == 1 || userTIdPkgRpt == 5) {
            %>
//            if(document.getElementById("cmboffice")!=null && (document.getElementById("cmboffice").value=="" || document.getElementById("cmboffice").value=="0"))
//            {
//                document.getElementById("msgPEOffice").innerHTML = 'Please select PA Office';
//                return false;
//            }

            if (document.getElementById("cmboffice").length <= 1) {

                document.getElementById("msgPEOffice").innerHTML = 'No PA Office Found';
                return false;
            }
                    <%                        }
            %>

            return true;
        }

        function getOfficesForDept(){
            $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:$('#txtdepartmentid').val(),funName:'Office'}, function(j){
                $("select#cmboffice").html(j);
            });
        }

        $(function() {
            $('#cmbBudType').change(function() {
                if(document.getElementById("cmbBudType").value=="1"){
                    document.getElementById("spanPrjName").style.visibility = "visible";
                }
                else{
                    document.getElementById("spanPrjName").style.visibility = "hidden";
                }
            });
        });


        function getProject()
        {
            var budType = "";
            var officeId = 0;
            if(document.getElementById("cmboffice")!=null)
            {
                officeId = document.getElementById("cmboffice").value;
                if(document.getElementById("cmbBudType")!=null)
                {
                    if(document.getElementById("cmbBudType").value=="0"){
                        budType =  'All';
                    }
                    if(document.getElementById("cmbBudType").value=="1"){
                        budType =  'Aid or Grant';
                    }
                    if(document.getElementById("cmbBudType").value=="2"){
                        budType =  'Government';
                    }
                    if(document.getElementById("cmbBudType").value=="3"){
                        budType =  'Own fund';
                    }
                }

                if(budType!="" && officeId!=0)
                {
                    $.post("<%=request.getContextPath()%>/APPReportServlet", {param2:budType,param1:officeId,funName:'getProject'},  function(j){
                        $("select#cmbPrjName").html(j);
                    });
                }
            }
        }
        </script>
       <!--Dashboard Content Part Start-->
       <div>&nbsp;</div>
       <div  id="print_area">
        <div class="pageHead_1">Consolidated Annual Procurement Plan
            <span style="float: right" class="noprint">
                 <%if(!isPDF.equalsIgnoreCase("true") && (appId!=0 || (request.getParameter("submit") != null && "Generate".equalsIgnoreCase(request.getParameter("submit")) ))) {%>
            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                <%}%>
                <%
                    String hierarchy_Node = "";
                    int deptId = 0;
                    int peOfficeId = 0;
                    int budTypeId = 0;
                    int officeId = 0;
                    int  prjId = 0;
                    String financialYear = "";
                    String procNature = "";
                    // Following if condition is check weather page is called from Report -> Annual Procurement Plan?
                    if (request.getParameter("submit") != null && "Generate".equalsIgnoreCase(request.getParameter("submit"))) {
                        String action = "Generate Annual Procurement Plan Report";
                        MakeAuditTrailService makeAuditTrailService  = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                        try{
                                if (request.getParameter("txtdepartmentid") != null && !"".equalsIgnoreCase(request.getParameter("txtdepartmentid"))) {
                                    deptId = Integer.parseInt(request.getParameter("txtdepartmentid"));
                                }

                                if (request.getParameter("cmboffice") != null && !"".equalsIgnoreCase(request.getParameter("cmboffice"))) {
                                    officeId = Integer.parseInt(request.getParameter("cmboffice"));
                                }

                                if (request.getParameter("cmbBudType") != null && !"".equalsIgnoreCase(request.getParameter("cmbBudType"))) {
                                    budTypeId = Integer.parseInt(request.getParameter("cmbBudType"));
                                }

                                if (request.getParameter("cmbPrjName") != null && !"".equalsIgnoreCase(request.getParameter("cmbPrjName"))) {
                                    prjId = Integer.parseInt(request.getParameter("cmbPrjName"));
                                }

                                if (request.getParameter("cmbFinYear") != null && !"".equalsIgnoreCase(request.getParameter("cmbFinYear"))) {
                                    financialYear = request.getParameter("cmbFinYear");
                                }

                                if (request.getParameter("cmbProcNature") != null && !"".equalsIgnoreCase(request.getParameter("cmbProcNature"))) {
                                    procNature = request.getParameter("cmbProcNature");
                                }
                                
                           }catch(Exception e){
                                action = "Error in "+action+" "+e.getMessage();
                           }finally{
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), userId, "userId", EgpModule.Report.getName(), action, "");
                                action=null;
                           }
                    } 
                    else if(appId!= 0) // This  if condition is check weather page is called from APP -> Consolidated Annual Procurement Report?
                    {
                        // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="appId";
                        int auditId=appId;
                        String auditAction="View Consolidated APP";
                        String moduleName=EgpModule.APP.getName();
                        String remarks="";
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                                  
                    }
                    String folderName = pdfConstant.APPVIEW;
                    String id = String.valueOf(appId);
                    String reqQuery = "";
                    String reqURL = request.getRequestURL().toString();
                    reqQuery = "appId=" + appId + "&uidforpdf=" + userId + "&UTID=" + userTIdPkgRpt
                                +"&txtdepartmentid="+deptId+"&cmboffice="+officeId+"&cmbBudType="+budTypeId+"&cmbPrjName="+prjId
                                +"&cmbFinYear="+financialYear+"&cmbProcNature="+procNature+"&submit=Generate";
                    if((appId> 0 || (request.getParameter("submit") != null && "Generate".equalsIgnoreCase(request.getParameter("submit")))) && !(isPDF.equalsIgnoreCase("true"))){
                              pdfCmd.genrateCmd(reqURL, reqQuery.replaceAll("= &", "=&"), folderName, id);
                    }
            %>
                 <%if(!isPDF.equalsIgnoreCase("true") && (appId!=0 || (request.getParameter("submit") != null && "Generate".equalsIgnoreCase(request.getParameter("submit")) ))) {%>
                <a class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?folderName=<%=folderName%>&id=<%=id%>" >Save As PDF </a>&nbsp;
                 <%}%>
                 <%/********** Commented by Dipal: bug id: 5738 ********************/
                 if(!isPDF.equalsIgnoreCase("true") && appId!=0) { %>
                 <a class="action-button-goback" href="<%=request.getContextPath()%>/officer/APPDashboard.jsp?appID=<%=appId%>" >Go Back To Dashboard</a>&nbsp;
                 <%}%>
            </span>

        </div>
                
            <form name="frmAppReport" id="frmAppReport" method="post">
<%
        if(appId==0)
        {
             if(isPDF.equalsIgnoreCase("true")){
                out.print("<div style='display:none;'");
             }
%>
                <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                <table id="tblSearchBox" border="0" cellspacing="10" cellpadding="0" class="formStyle_1 formBg_1 t_space noprint" width="100%">
                    <tr>
                        <td width="22%" class="ff">Hierarchy Node : <%if(userTIdPkgRpt==1  || userTIdPkgRpt==19 || userTIdPkgRpt==20){%><span id="spanMinId">*</span><%}%></td>
                        <td width="31%">
                            <%
                            if(userTIdPkgRpt==1 || userTIdPkgRpt==19 || userTIdPkgRpt==20)
                            {
                            %>
                            <input name="txtdepartment" type="text" class="formTxtBox_1" id="txtdepartment" style="width:200px;" />
                            <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=PackageReport', '', 'width=350px,height=400px,scrollbars=1','');">
                            <img src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" name="deptTreeIcn" width="25" height="25" id="deptTreeIcn" style="vertical-align: bottom;">
                            </a>
                            <%
                            }
                            else if(userTIdPkgRpt==5)
                            { 
                                deptId = peAdminSrBean.getDepartmentIdByUserId(userId);
                                String deptName = "";
                                deptName = peAdminSrBean.getDepartmentNameByUserId(userId);
                                
                                if(deptName!=null && !"".equals(deptName)){
                                    out.print(deptName);
                                }else{
                                    out.print("-");
                                }
                            }
                            else if(userTIdPkgRpt==4)
                            {
                                deptId = appReportService.getDepartmentIdByPEId(userId);
                                String deptName = "";
                                deptName = appReportService.getDepartmentNameByDeptId(deptId);
                                if("".equals(deptName) || deptName==null){
                                    out.print("-");
                                }else{
                                    out.print(deptName);
                                }
                            }
                            else if(userTIdPkgRpt==3){
                            Object[] objData = mISService.getOrgAdminOrgNameByUserId(Integer.parseInt(session.getAttribute("userId").toString()));
                            out.print(objData[1].toString());
                            deptId = Integer.parseInt(objData[0].toString());
                            String deptName = "";
                            deptName =  objData[1].toString();
                            out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                            out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"txtdepartmentid\" value=\"" + objData[0] + "\"/>");
                            //out.print("<input type=\"hidden\" id=\"uTypeId\" name=\"uTypeId\" value=\"" + userTIdPkgRpt + "\"/>");
                            }
                            %>
                            <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" value="<%=deptId%>"/>
                            <span id="msgMinistry" class="reqF_1"></span>
                        </td>
                        <%--<td width="14%"><span class="ff">Financial Year :</span></td>--%>
                        <td class="ff">Financial Year :</td>
                        <td width="33%">
                            <select name="cmbFinYear" class="formTxtBox_1" id="cmbFinYear" style="width:200px;" onchange="getProject();">
                                <option value=" ">--Select Financial Year--</option>
                                <%
                                for (SelectItem finYear : advAppSearchSrBean.getFinancialYearList()){
                                    //if (advAppSearchSrBean.getDefaultFinYear().equalsIgnoreCase(finYear.getObjectId().toString())) {
                                %>
                                <%--<option value="<%=finYear.getObjectValue()%>" selected="selected"><%=finYear.getObjectValue()%></option>--%>
                                <% //} else {%>
                                <option value="<%=finYear.getObjectValue()%>"><%=finYear.getObjectValue()%></option>
                                <% } %>
                            </select></td>
                    </tr>
                    <tr>
                        <%
                        if(userTIdPkgRpt==5)
                        {
                        %>
                            <script>
                                getOfficesForDept();
                            </script>
                        <%
                        }
                        %>
                        <td class="ff">PA Office : <%if(userTIdPkgRpt==1 || userTIdPkgRpt==5 || userTIdPkgRpt==19 || userTIdPkgRpt==20){%><span id="spanOffId"></span><%}%></td>
                        <td>
                        <%
                        if(userTIdPkgRpt==1 || userTIdPkgRpt==5  || userTIdPkgRpt==19 || userTIdPkgRpt==20)
                        {
                        %>
                            <select name="cmboffice" class="formTxtBox_1" id="cmboffice" style="width:200px;" onChange="getProject();">
                                <option value="">- Select PA Office -</option>
                            </select>
                        <%
                        }
                        else if(userTIdPkgRpt==4)
                        {
                            officeId = appReportService.getOfficeIdByPEId(userId);
                            String officeName = "";
                            officeName = appReportService.getOfficeNameByPEId(userId);
                            out.print(officeName);
                        %>
                            <input name="cmboffice" id="cmboffice" value="<%=officeId%>" type="hidden"/>
                        <%
                        }else if(userTIdPkgRpt==3){%>
                        <select name="cmboffice" class="formTxtBox_1" id="cmboffice" style="width:200px;" onChange="getProject();">
                             <option value="0">-- Select Office --</option>
                           <%  List<Object[]> cmbLists = mISService.getOfficeList(session.getAttribute("userId").toString(), "" + userTIdPkgRpt);
                            for (Object[] cmbList : cmbLists) {
                                String selected = "";
                                if (request.getParameter("offId") != null) {
                                    if (cmbList[0].toString().equals(request.getParameter("offId"))) {
                                        selected = "selected";
                                    }
                                }
                                out.print("<option value='" + cmbList[0] + "' " + selected + ">" + cmbList[1] + "</option>");
                                selected = null;
                            }
                            if (cmbLists.isEmpty()) {
                                out.print("<option value='0'>No Office Found.</option>");
                            }
                        }
                        hierarchy_Node = appReportService.getDepartmentNameByDeptId(deptId);
                        %></select>
                        <span id="msgPEOffice" class="reqF_1"></span>
                        </td>
                        <td class="ff">Budget  Type  :</td>
                        <td><select name="cmbBudType" class="formTxtBox_1" id="cmbBudType" style="width:200px;" onChange="getProject();">
                                <option value="0" selected="selected">--Select Budget Type--</option>
                                <option value="1">Capital</option>
                                <option value="2">Recurrent</option>
                                <option value="3">Own Fund</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td class="ff">Project  Name : <span id="spanPrjName" style="visibility:hidden">*</span> </td>
                        <td>
                            <select name="cmbPrjName" class="formTxtBox_1" id="cmbPrjName" style="width:200px;">
                                <option value="">--Select Project Name--</option>
                            </select>
                            <span id="msgProjectName" class="reqF_1"></span>
                        </td>
                        <td class="ff">Procurement Category :</td>
                        <td><select name="cmbProcNature" class="formTxtBox_1" id="cmbProcNature" style="width:200px;">
                                <option value="" selected="selected">--Select Procurement  Category--</option>
                                <option value="Goods">Goods</option>
                                <option value="Works">Works</option>
                                <option value="Services">Services</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="ff t-align-center">
                        <label class="formBtn_1">
                            <input type="submit" name="submit" id="submit" value="Generate" onClick="return validate();"/>
                        </label>
                        <label class="formBtn_1 l_space">
                            <input type="button" name="reset" id="reset" value="Reset" onClick="resetForm();" />
                        </label>
                        </td>
                    </tr>
                </table>                
<%
            if(isPDF.equalsIgnoreCase("true")) {
                out.print("</div>");
            }
        }
        else
        {
%>
                <table>
                    <tr>
                        <td>
<%
                            Object objAry[] = appReportService.getAppDetailByAppId(appId);
%>
                            <input type="hidden" name="hdnDeptId" id="hdnDeptId" value="<%=objAry[0]%>" />
                            <input type="hidden" name="hdnPEOfficeId" id="hdnPEOfficeId" value="<%=objAry[1]%>" />
                            <input type="hidden" name="hdnPrjId" id="hdnPrjId" value="<%=objAry[2]%>" />
                            <input type="hidden" name="hdnBudTypeId" id="hdnBudTypeId" value="<%=objAry[3]%>" />
                            <input type="hidden" name="hdnFinYearId" id="hdnFinYearId" value="<%=objAry[4]%>" />
                        </td>
                    </tr>
                </table>
<%
        }
%>
                
                <table width="100%" cellspacing="0" class="tableList_1 t_space b_space">
<%

        String procNat = "";
        String budType = "";
        String ministry = "";
        String division = "";
        String agency = "";
        String peCode = "";
        String officeName = "";
        String prjName = "";
        String prjCode = "";
        double totEstcost = 0;
        List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
        CommonAppData appData=new CommonAppData();

    if((deptId!=0  || officeId!=0 || !"".equalsIgnoreCase(financialYear) || budTypeId!=0 || prjId!=0 || !"".equalsIgnoreCase(procNature)) || (appId!=0))
    {
        boolean retVal = false;
        CommonAppPkgDetails listTop;
        Object obj[];

        if(appId!=0)
        {
            obj = appReportSrBean.getAllDetailByAppId(appId);
            if(obj!=null){
                deptId = (Short)obj[0];
                peOfficeId = (Integer)obj[1];
                financialYear = (String)obj[2];
                budTypeId = Integer.parseInt(String.valueOf((Byte)obj[3]));
                prjId = (Integer)obj[4];
            }

            appListDtBean = appServlet.getAPPDetails("GetAppMinistry", String.valueOf(appId), "");
            
            
            if(appListDtBean!=null && !appListDtBean.isEmpty()){
                appData=appListDtBean.get(0);
                if(!"".equalsIgnoreCase(appData.getFieldName6())){
                    officeId = Integer.parseInt(appData.getFieldName6());
                }else{
                    officeId = 0;
                }
            }
        }

        listTop = appReportService.getAppDetails("getTopInfo", deptId, officeId, financialYear.trim(), budTypeId, prjId, procNature, 0,0).get(0);
        retVal = appReportSrBean.getAllList(deptId, officeId, financialYear, budTypeId, prjId, procNature, appId);

        if(null!=listTop.getMinistry() && !"".equalsIgnoreCase(listTop.getMinistry()) && !"NULL".equalsIgnoreCase(listTop.getMinistry()))
            ministry = listTop.getMinistry();
        if(null!=listTop.getDivision() && !"".equalsIgnoreCase(listTop.getDivision()) && !"NULL".equalsIgnoreCase(listTop.getDivision()))
            division = listTop.getDivision();
        if(null!=listTop.getAgency() && !"".equalsIgnoreCase(listTop.getAgency()) && !"NULL".equalsIgnoreCase(listTop.getAgency()))
            agency = listTop.getAgency();
        if(null!=listTop.getPeCode() && !"".equalsIgnoreCase(listTop.getPeCode()) && !"NULL".equalsIgnoreCase(listTop.getPeCode()))
            peCode = listTop.getPeCode();
        if(null!=listTop.getOfficeName() && !"".equalsIgnoreCase(listTop.getOfficeName()) && !"NULL".equalsIgnoreCase(listTop.getOfficeName()))
            officeName = listTop.getOfficeName();
        if(null!=listTop.getProjectName() && !"".equalsIgnoreCase(listTop.getProjectName()) && !"NULL".equalsIgnoreCase(listTop.getProjectName()))
            prjName = listTop.getProjectName();
        if(null!=listTop.getProjectCode() && !"".equalsIgnoreCase(listTop.getProjectCode()) && !"NULL".equalsIgnoreCase(listTop.getProjectCode()))
            prjCode = listTop.getProjectCode();

        listTop = null;
	
	boolean isServiceExist=false;
	boolean isWorkExist=false;
	boolean isGoodsExist=false;
        
            if (appReportSrBean.isServiceOwn())
                {
			columnSpan = 17;
			isServiceExist=true;
                }
                else if (appReportSrBean.isServiceRev())
                {
			columnSpan = 17;
			isServiceExist=true;
                }
                else if (appReportSrBean.isServiceDev())
                {
			columnSpan = 17;
			isServiceExist=true;
                }
                //Change columnSpan 16 to 17 by Proshanto Kumar Saha
                else if (appReportSrBean.isWorkOwn())
                {
			columnSpan = 17;
			isWorkExist=true;
                }
                else if (appReportSrBean.isWorkRev())
                {
			columnSpan = 17;
			isWorkExist=true;
                }
                else if (appReportSrBean.isWorkDev())
                {
			columnSpan = 17;
			isWorkExist=true;
                }
                //Change columnSpan 15 to 16 by Proshanto Kumar Saha
                else if (appReportSrBean.isGoodsOwn())
                {
			columnSpan = 16;
			isGoodsExist=true;
                }
                else if (appReportSrBean.isGoodsRev())
                {
			columnSpan = 16;
			isGoodsExist=true;
                }
                else if (appReportSrBean.isGoodsDev())
                {
			columnSpan = 16;
			isGoodsExist=true;
                }
		//out.println(columnSpan);     
		//out.println("isGoodsExist="+isGoodsExist);
		//out.println("isWorkExist="+isWorkExist);
		//out.println("isServiceExist="+isServiceExist);
		
		int columnSpanGoods=0,columnSpanWork=0;
		
		if(isWorkExist)
		{
			columnSpanGoods=2;
		}
		if(isServiceExist)
		{
			columnSpanWork=2;
			columnSpanGoods=3;	
		}
		//out.println("columnSpanGoods="+columnSpanGoods);
		//out.println("columnSpanWork="+columnSpanWork);
		
		
        
        if(appId!=0)
        {
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            List<SPTenderCommonData> details = tenderCommonService.returndata("GetAppDetailById", appData.getFieldName6(), "0");
%>
          <tr>
            <td colspan="<%=columnSpan%>" class="t-align-center">
                <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1" width="100%"> 
                    <tr>
                        <td style="width: 130px" class="ff" align="left">Hierarchy Node	: </td>
                        <td style="width: 287px" align="left" colspan="3"><%=details.get(0).getFieldName1()%></td>
                    </tr>
<!--                    <tr>
                        <td class="ff" align="left">Division :</td>
                        <td align="left" colspan="3"><%=details.get(0).getFieldName2()%></td>
                    </tr>
                    <tr>
                        <td width="15%" class="ff">Organization : </td>
                        <td width="85%" colspan="3"><% if(appData.getFieldName3()!=null ){ out.print(appData.getFieldName3()); }else{ out.print("-"); }%></td>
                    </tr>-->
                    <%  if(appId==0){ %>
                    <tr>
                        <td class="ff">Procuring Agency Name and Code :</td>
                        <td colspan="3">
                            <%
                                if(!"".equalsIgnoreCase(officeName))
                                    out.print(officeName);
                                if(!"".equalsIgnoreCase(peCode))
                                    out.print(" & "+peCode);
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="ff">Budget Type :</td>
                        <td width="15%" >
                            <% if(budTypeId==1){ %>
<!--                                Development-->Capital
                            <% }else if(budTypeId ==3 ){ %>
                                Own Fund
                            <% }else{ %>
<!--                                Revenue-->Recurrent
                            <% } %>
                        </td>
                        <td width="20%" class="ff">Project / Programme Name and Code :</td>
                        <td width="57%">
                            <%
                                if(!"".equalsIgnoreCase(prjName))
                                    out.print(prjName);
                                if(!"".equalsIgnoreCase(prjCode))
                                    out.print(" & "+prjCode);
                            %>
                        </td>
                    </tr>
                    <% } %>
                            </table>
                        </td>
                    </tr>
                    
        <% }else{
            List<SPCommonSearchDataMore> list = commonService.geteGPData("getMinistryDetails", ""+deptId, ""+officeId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        %>
        <tr>
            <td colspan="<%=columnSpan%>" class="t-align-center">
                <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1" width="100%">
                    <tr>
                        <td width="15%" class="ff">Hierarchy Node : </td>
                        <td width="85%" colspan="3" ><%=hierarchy_Node%></td>
                    </tr>
<!--                    <tr>
                        <td width="15%" class="ff ">Division : </td>
                        <td width="85%" colspan="3"><%=list.get(0).getFieldName2()%></td>
                    </tr>
                    <tr>
                        <td width="15%" class="ff">Organization : </td>
                        <td width="85%" colspan="3"><%=list.get(0).getFieldName3()%></td>
                    </tr>-->
                </table>
            </td>
        </tr>
        <% } %>
        <% if(appReportSrBean.isGoodsDev())
        {
            totEstcost = 0;
            cntr = 0;
%>
                    <tr>
                        <th colspan="<%=columnSpan%>" class="t-align-center ff">CONSOLIDATED ANNUAL PROCUREMENT PLAN</th>
                    </tr>
                    <tr>
                        <th width="6%" class="t-align-center">Package<br />
                            No.</th>
                        <th width="10%" class="t-align-center">Description of <br />
                            Procurement <br />
                            Package
                            Goods</th>
                        <th width="11%" class="t-align-center"> Procurement<br />
                            Method and
                            Type </th>
                        <th width="9%" class="t-align-center">Contract<br />
                            Approving<br />
                            Authority</th>
                        <th width="7%" class="t-align-center">Source<br />
                            of Funds</th>
                        <th width="8%" class="t-align-center"> Estd. Cost
                            (In Nu.) </th>
                        <th width="8%" class="t-align-center"> Time Code
                            for Process </th>

                        <th width="3%" class="t-align-center"> Invite/
                            Advertise
                            Tender </th>
                        <th width="3%" class="t-align-center"> Tender
                            Opening </th>
                        <th width="3%" class="t-align-center">Tender<br />
                            Evaluation</th>
                        <th width="2%" class="t-align-center">Approval to<br />
                            Award</th>
                        <!--Code by Proshanto Kumar Saha-->
                        <th width="2%" class="t-align-center">Letter of Intent<br />
                            to Award</th>
                        <th width="3%" class="t-align-center">Notification<br />
                            of Award</th>
                        <th width="3%" class="t-align-center">Signing of<br />
                            Contract</th>
                        <th width="3%" class="t-align-center">Total<br />
                            time to<br />
                            Contract<br />
                            Signature</th>
                        <th colspan="<%=columnSpanGoods%>" width="2%" class="t-align-center">Time for<br />
                            Completion<br />
                            of Contract</th>
                    </tr>
                    <tr>
                        <td class="t-align-center">1</td>
                        <td class="t-align-center">2</td>
                        <td class="t-align-center">3</td>
                        <td class="t-align-center">4</td>
                        <td class="t-align-center">5</td>
                        <td class="t-align-center">6</td>
                        <td class="t-align-center">7</td>
                        <td class="t-align-center">8</td>
                        <td class="t-align-center">9</td>
                        <td class="t-align-center">10</td>
                        <td class="t-align-center">11</td>
                        <td class="t-align-center">12</td>
                        <td class="t-align-center">13</td>
                        <td class="t-align-center">14</td>
                        <td class="t-align-center">15</td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>">16</td>
                    </tr>
<%
            for(AppReportDtBean appReportData : appReportSrBean.getGoodsDevList())
            { %>
<%
                               if(!(cmpAPPId==appReportData.getAppId()) || cntr==0){
                                  cmpAPPId = appReportData.getAppId();
%>
                    <tr>
                        <td colspan="<%=columnSpan%>">
                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td width="225" class="ff">Procuring Agency Name and Code :</td>
                                    <td colspan="3">
                                        <%=appReportData.getMinistryDetails()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Budget Type :</td>
                                    <td width="200"><% if(appReportSrBean.isGoodsDev() || appReportSrBean.isServiceDev() || appReportSrBean.isWorkDev()){ %>
<!--                                Development-->Capital
                            <% }else if(appReportSrBean.isGoodsOwn() || appReportSrBean.isServiceOwn() || appReportSrBean.isWorkOwn()){ %>
                                Own Fund
                            <% }else{ %>
<!--                                Revenue-->Recurrent
                            <% } %></td>
                                    <td width="250">Project / Programme Name and Code :</td>
                                    <td>
                                        <% if(appReportData.getProjectNameCode()!=null){ out.print(appReportData.getProjectNameCode()); }else{ out.print("Not Available"); } %>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getPackageNo()%></td>
                        <td rowspan="3" valign="top" class="t-align-left">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableList_1">
                                <tr>
                                    <th>Lot No. </th>
                                    <th>Lot Description </th>
                                    <th>Qty. </th>
                                    <th>Unit </th>
                                </tr>

<%
                for(CommonAppPkgDetails lotList : appReportService.getAppDetails("getLotInfo", 0, 0, "", 0, 0, "", appReportData.getPackageId(),0))
                {
%>
                                <tr>
                                    <td><%=lotList.getLotNo()%></td>
                                    <td><%=lotList.getLotDesc()%></td>
                                    <td><%=lotList.getQuantity()%></td>
                                    <td><%=lotList.getUnit()%></td>
                                </tr>
<%
                }
%>
                            </table>

                        </td>
<!--Edit by aprojit --> <td rowspan="3" class="t-align-center"><%=appReportData.getProcMethod()%> (<%if(appReportData.getProcType().equalsIgnoreCase("NCT")){out.print("NCB");}else if(appReportData.getProcType().equalsIgnoreCase("ICT")){out.print("ICB");}%>)</td>
                        <td rowspan="3" class="t-align-center"><%if(appReportData.getAppAut().equalsIgnoreCase("PE")){out.print("PE");}else if(appReportData.getAppAut().equalsIgnoreCase("HOPE")){out.print("HOPA");}else {out.print(appReportData.getAppAut());} %></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getSrcFund()%></td>
                        <td rowspan="3" class="t-align-center">
                            <%=appReportData.getEstCost().setScale(2, BigDecimal.ROUND_FLOOR)%>
                            <% totEstcost += appReportData.getEstCost().doubleValue();
                            %>
                        </td>
                        <th class="t-align-center">Planned<br />
                            Dates</th>

                        <td class="t-align-center"><%=appReportData.getgPlanDtInAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtTenderEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtAppAwardDt()%></td>
                        <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center"><%=appReportData.getgPlanDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtSignCtcDt()%></td>
                        <td class="t-align-center"> - </td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>"><%=appReportData.getgPlanDtTotTimeComContractDt()%></td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Planned<br />
                            Days</th>

                        <td class="t-align-center">0</td>
                        <td class="t-align-center">
                            <%
                                totalDays = 0;
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtInAdvtDt(), appReportData.getgPlanDtTenderOpenDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtTenderOpenDt(), appReportData.getgPlanDtTenderEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtTenderEvalDt(), appReportData.getgPlanDtAppAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtAppAwardDt(), appReportData.getgPlanDtLintAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtLintAwardDt(), appReportData.getgPlanDtNtiAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <!--Code End by Proshanto Kumar Saha-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtNtiAwardDt(), appReportData.getgPlanDtSignCtcDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center"><%=totalDays%></td>
                        <td class="t-align-center"  colspan="<%=columnSpanGoods%>">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtSignCtcDt(), appReportData.getgPlanDtTotTimeComContractDt());
                                out.print(dateDiff);
                            %>
                        </td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Actual Dates</th>

                        <td class="t-align-center"><%=appReportData.getgActDtInAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtTenderEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtAppAwardDt()%></td>
                        <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center"><%=appReportData.getgActDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtSignCtcDt()%></td>
                        <td class="t-align-center">-</td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>">-</td>
                    </tr>
<%
                    cntr++;
            }
%>
                    <tr>
                        <td class="t-align-center"></td>
                        <td class="t-align-center">Total Value of Procurement</td>

                        <td class="t-align-center">&nbsp;</td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"><strong>Total : </strong>
                            <%=new BigDecimal(Double.toString(totEstcost)).setScale(2, 0).toPlainString()%>
                        </td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>"></td>
                    </tr>
                    <tr>
                        <td colspan="<%=columnSpan%>" class="no-border">&nbsp;</td>
                    </tr> 
<% } %>
                    
 <%       if(appReportSrBean.isGoodsRev())
        {
            totEstcost = 0;
            cntr=0;
%>

                    <tr>
                        <th colspan="<%=columnSpan%>" class="t-align-center ff">CONSOLIDATED ANNUAL PROCUREMENT PLAN</th>
                    </tr>
                    <tr>
                        <th width="6%" class="t-align-center">Package<br />
                            No.</th>
                        <th width="10%" class="t-align-center">Description of <br />
                            Procurement <br />
                            Package
                            Goods</th>
                        <th width="11%" class="t-align-center"> Procurement<br />
                            Method and
                            Type </th>
                        <th width="9%" class="t-align-center">Contract<br />
                            Approving<br />
                            Authority</th>
                        <th width="7%" class="t-align-center">Source<br />
                            of Funds</th>
                        <th width="8%" class="t-align-center"> Estd. Cost
                            (In Nu.) </th>
                        <th width="8%" class="t-align-center"> Time Code
                            for Process </th>
                        <th width="3%" class="t-align-center"> Invite/
                            Advertise
                            Tender </th>
                        <th width="3%" class="t-align-center"> Tender
                            Opening </th>
                        <th width="3%" class="t-align-center">Tender<br />
                            Evaluation</th>
                        <th width="2%" class="t-align-center">Approval to<br />
                            Award</th>
                         <!--Code by Proshanto Kumar Saha-->
                        <th width="2%" class="t-align-center">Letter of Intent<br />
                            to Award</th>
                        <th width="3%" class="t-align-center">Notification<br />
                            of Award</th>
                        <th width="3%" class="t-align-center">Signing of<br />
                            Contract</th>
                        <th width="3%" class="t-align-center">Total<br />
                            time to<br />
                            Contract<br />
                            Signature</th>
                        <th width="2%" class="t-align-center" colspan="<%=columnSpanGoods%>">Time for<br />
                            Completion<br />
                            of Contract</th>
                    </tr>
                    <tr>
                        <td class="t-align-center">1</td>
                        <td class="t-align-center">2</td>
                        <td class="t-align-center">3</td>
                        <td class="t-align-center">4</td>
                        <td class="t-align-center">5</td>
                        <td class="t-align-center">6</td>
                        <td class="t-align-center">7</td>
                        <td class="t-align-center">8</td>
                        <td class="t-align-center">9</td>
                        <td class="t-align-center">10</td>
                        <td class="t-align-center">11</td>
                        <td class="t-align-center">12</td>
                        <td class="t-align-center">13</td>
                        <td class="t-align-center">14</td>
                        <td class="t-align-center">15</td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>">16</td>
                    </tr>
<%
            for(AppReportDtBean appReportData : appReportSrBean.getGoodsRevList())
            {
%>
                <%
                          if(!(cmpAPPId==appReportData.getAppId()) || cntr==0){
                                  cmpAPPId = appReportData.getAppId();
%>
                <tr>
                    <td colspan="<%=columnSpan%>">
                        <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1" width="100%">
                            <tr>
                                <td width="225" class="ff">Procuring Agency Name and Code :</td>
                                <td colspan="3">
                                    <%=appReportData.getMinistryDetails()%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Budget Type :</td>
                                <td width="200"><% if(appReportSrBean.isGoodsDev() || appReportSrBean.isServiceDev() || appReportSrBean.isWorkDev()){ %>
<!--                                Development-->Capital
                            <% }else if(appReportSrBean.isGoodsOwn() || appReportSrBean.isServiceOwn() || appReportSrBean.isWorkOwn()){ %>
                                Own Fund
                            <% }else{ %>
<!--                                Revenue-->Recurrent
                            <% } %></td>
                                <td width="250">Project / Programme Name and Code :</td>
                                <td>
                                   <% if(appReportData.getProjectNameCode()!=null){ out.print(appReportData.getProjectNameCode()); }else{ out.print("Not Available"); } %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <% } %>    
                <tr>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getPackageNo()%></td>
                        <td rowspan="3" valign="top" class="t-align-left"><table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableList_1">
                                <tr>
                                    <th>Lot No. </th>
                                    <th>Lot Description </th>
                                    <th>Qty. </th>
                                    <th>Unit </th>
                                </tr>
<%
                for(CommonAppPkgDetails lotList : appReportService.getAppDetails("getLotInfo", 0, 0, "", 0, 0, "", appReportData.getPackageId(),0))
                {
%>
                                <tr>
                                    <td><%=lotList.getLotNo()%></td>
                                    <td><%=lotList.getLotDesc()%></td>
                                    <td><%=lotList.getQuantity()%></td>
                                    <td><%=lotList.getUnit()%></td>
                                </tr>
<%
                }
%>
                            </table></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getProcMethod()%> (<%if(appReportData.getProcType().equalsIgnoreCase("NCT")){out.print("NCB");}else if(appReportData.getProcType().equalsIgnoreCase("ICT")){out.print("ICB");}%>)</td>
                        <td rowspan="3" class="t-align-center"><%if(appReportData.getAppAut().equalsIgnoreCase("PE")){out.print("PE");}else if(appReportData.getAppAut().equalsIgnoreCase("HOPE")){out.print("HOPA");}else {out.print(appReportData.getAppAut());} %></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getSrcFund()%></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getEstCost().setScale(2, BigDecimal.ROUND_FLOOR)%><%totEstcost += appReportData.getEstCost().doubleValue();%></td>
                        <th class="t-align-center">Planned<br />
                            Dates</th>

                        <td class="t-align-center"><%=appReportData.getgPlanDtInAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtTenderEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtAppAwardDt()%></td>
                        <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center"><%=appReportData.getgPlanDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtSignCtcDt()%></td>
                        <td class="t-align-center"> - </td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>"><%=appReportData.getgPlanDtTotTimeComContractDt()%></td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Planned<br />
                            Days</th>
                        <td class="t-align-center">0</td>
                        <td class="t-align-center">
                            <%
                                totalDays = 0;
                                dateDiff =appReportSrBean.getDateDiff(appReportData.getgPlanDtInAdvtDt(), appReportData.getgPlanDtTenderOpenDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtTenderOpenDt(), appReportData.getgPlanDtTenderEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtTenderEvalDt(), appReportData.getgPlanDtAppAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtAppAwardDt(), appReportData.getgPlanDtLintAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtLintAwardDt(), appReportData.getgPlanDtNtiAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <!--Code End by Proshanto Kumar Saha-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtNtiAwardDt(), appReportData.getgPlanDtSignCtcDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                            </td>
                        <td class="t-align-center"><%=totalDays%></td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtSignCtcDt(), appReportData.getgPlanDtTotTimeComContractDt());
                                out.print(dateDiff);
                            %>
                        </td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Actual Dates</th>

                        <td class="t-align-center"><%=appReportData.getgActDtInAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtTenderEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtAppAwardDt()%></td>
                        <!--Code by Proshanto-->
                        <td class="t-align-center"><%=appReportData.getgActDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtSignCtcDt()%></td>
                        <td class="t-align-center">-</td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>">-</td>
                    </tr>
<%
                    cntr++;
                }
%>
                    <tr>
                        <td class="t-align-center"></td>
                        <td class="t-align-center">Total Value of Procurement</td>

                        <td class="t-align-center">&nbsp;</td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"><strong>Total : </strong><%=new BigDecimal(Double.toString(totEstcost)).setScale(2, 0).toPlainString()%></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>"></td>
                    </tr>
<%
        }
        if(appReportSrBean.isGoodsOwn())
        {
            totEstcost = 0;
            cntr=0;
%>

                    <tr>
                        <th colspan="<%=columnSpan%>" class="t-align-center ff">CONSOLIDATED ANNUAL PROCUREMENT PLAN</th>
                    </tr>
                    <tr>
                        <th width="6%" class="t-align-center">Package<br />
                            No.</th>
                        <th width="10%" class="t-align-center">Description of <br />
                            Procurement <br />
                            Package
                            Goods</th>
                        <th width="11%" class="t-align-center"> Procurement<br />
                            Method and
                            Type </th>
                        <th width="9%" class="t-align-center">Contract<br />
                            Approving<br />
                            Authority</th>
                        <th width="7%" class="t-align-center">Source<br />
                            of Funds</th>
                        <th width="8%" class="t-align-center"> Estd. Cost
                            (In Nu.) </th>
                        <th width="8%" class="t-align-center"> Time Code
                            for Process </th>
                        <th width="3%" class="t-align-center"> Invite/
                            Advertise
                            Tender </th>
                        <th width="3%" class="t-align-center"> Tender
                            Opening </th>
                        <th width="3%" class="t-align-center">Tender<br />
                            Evaluation</th>
                        <th width="2%" class="t-align-center">Approval to<br />
                            Award</th>
                        <!--Code by Proshanto Kumar Saha-->
                        <th width="2%" class="t-align-center">Letter of Intent<br />
                            to Award</th>
                        <th width="3%" class="t-align-center">Notification<br />
                            of Award</th>
                        <th width="3%" class="t-align-center">Signing of<br />
                            Contract</th>
                        <th width="3%" class="t-align-center">Total<br />
                            time to<br />
                            Contract<br />
                            Signature</th>
                        <th width="2%" class="t-align-center" colspan="<%=columnSpanGoods%>">Time for<br />
                            Completion<br />
                            of Contract</th>
                    </tr>
                    <tr>
                        <td class="t-align-center">1</td>
                        <td class="t-align-center">2</td>
                        <td class="t-align-center">3</td>
                        <td class="t-align-center">4</td>
                        <td class="t-align-center">5</td>
                        <td class="t-align-center">6</td>
                        <td class="t-align-center">7</td>
                        <td class="t-align-center">8</td>
                        <td class="t-align-center">9</td>
                        <td class="t-align-center">10</td>
                        <td class="t-align-center">11</td>
                        <td class="t-align-center">12</td>
                        <td class="t-align-center">13</td>
                        <td class="t-align-center">14</td>
                        <td class="t-align-center">15</td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>">16</td>
                    </tr>
<%
            for(AppReportDtBean appReportData : appReportSrBean.getGoodsOwnList())
            {
%>
                    <%
                              if(!(cmpAPPId==appReportData.getAppId()) || cntr==0){
                                  cmpAPPId = appReportData.getAppId();
%>
                    <tr>
                        <td colspan="<%=columnSpan%>">
                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td width="225" class="ff">Procuring Agency Name and Code :</td>
                                    <td colspan="3">
                                        <%=appReportData.getMinistryDetails()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Budget Type :</td>
                                    <td width="200"><% if(appReportSrBean.isGoodsDev() || appReportSrBean.isServiceDev() || appReportSrBean.isWorkDev()){ %>
<!--                                Development-->Capital
                            <% }else if(appReportSrBean.isGoodsOwn() || appReportSrBean.isServiceOwn() || appReportSrBean.isWorkOwn()){ %>
                                Own Fund
                            <% }else{ %>
<!--                                Revenue-->Recurrent
                            <% } %></td>
                                    <td width="250">Project / Programme Name and Code :</td>
                                    <td>
                                        <% if(appReportData.getProjectNameCode()!=null){ out.print(appReportData.getProjectNameCode()); }else{ out.print("Not Available"); } %>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getPackageNo()%></td>
                        <td rowspan="3" valign="top" class="t-align-left"><table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableList_1">
                                <tr>
                                    <th>Lot No. </th>
                                    <th>Lot Description </th>
                                    <th>Qty. </th>
                                    <th>Unit </th>
                                </tr>
<%
                for(CommonAppPkgDetails lotList : appReportService.getAppDetails("getLotInfo", 0, 0, "", 0, 0, "", appReportData.getPackageId(),0))
                {
%>
                                <tr>
                                    <td><%=lotList.getLotNo()%></td>
                                    <td><%=lotList.getLotDesc()%></td>
                                    <td><%=lotList.getQuantity()%></td>
                                    <td><%=lotList.getUnit()%></td>
                                </tr>
<%
                }
%>
                            </table></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getProcMethod()%> (<%if(appReportData.getProcType().equalsIgnoreCase("NCT")){out.print("NCB");}else if(appReportData.getProcType().equalsIgnoreCase("ICT")){out.print("ICB");}%>)</td>
                        <td rowspan="3" class="t-align-center"><%if(appReportData.getAppAut().equalsIgnoreCase("PE")){out.print("PE");}else if(appReportData.getAppAut().equalsIgnoreCase("HOPE")){out.print("HOPA");}else {out.print(appReportData.getAppAut());} %></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getSrcFund()%></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getEstCost().setScale(2, BigDecimal.ROUND_FLOOR)%><%totEstcost += appReportData.getEstCost().doubleValue();%></td>
                        <th class="t-align-center">Planned<br />
                            Dates</th>
                        <td class="t-align-center"><%=appReportData.getgPlanDtInAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtTenderEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtAppAwardDt()%></td>
                         <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center"><%=appReportData.getgPlanDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgPlanDtSignCtcDt()%></td>
                        <td class="t-align-center"> - </td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>"><%=appReportData.getgPlanDtTotTimeComContractDt()%></td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Planned<br />
                            Days</th>
                        <td class="t-align-center">0</td>
                        <td class="t-align-center">
                            <%
                                totalDays = 0;
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtInAdvtDt(), appReportData.getgPlanDtTenderOpenDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtTenderOpenDt(), appReportData.getgPlanDtTenderEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtTenderEvalDt(), appReportData.getgPlanDtAppAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtAppAwardDt(), appReportData.getgPlanDtLintAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>    
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtLintAwardDt(), appReportData.getgPlanDtNtiAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                         <!--End-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtNtiAwardDt(), appReportData.getgPlanDtSignCtcDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center"><%=totalDays%></td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getgPlanDtSignCtcDt(),appReportData.getgPlanDtTotTimeComContractDt());
                                out.print(dateDiff);
                            %>
                        </td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Actual Dates</th>

                        <td class="t-align-center"><%=appReportData.getgActDtInAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtTenderEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtAppAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getgActDtSignCtcDt()%></td>
                        <td class="t-align-center">-</td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>">-</td>
                    </tr>
<%
                cntr++;
            }
%>
                    <tr>
                        <td class="t-align-center"></td>
                        <td class="t-align-center">Total Value of Procurement</td>

                        <td class="t-align-center">&nbsp;</td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"><strong>Total : </strong><%=new BigDecimal(Double.toString(totEstcost)).setScale(2, 0).toPlainString()%></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center" colspan="<%=columnSpanGoods%>"></td>
                    </tr>
<%
        }
        if(appReportSrBean.isWorkDev())
        {
            totEstcost = 0;
            cntr=0;
%>

                    <tr>
                        <th colspan="<%=columnSpan%>" class="t-align-center ff">CONSOLIDATED ANNUAL PROCUREMENT PLAN</th>
                    </tr>
                    <tr>
                        <th width="6%" class="t-align-center">Package<br /> No.</th>
                        <th width="10%" class="t-align-center">Description of <br />
                            Procurement <br />
                            Package
                            Works</th>
                        <th width="11%" class="t-align-center"> Procurement<br />
                            Method and
                            Type </th>
                        <th width="9%" class="t-align-center">Contract<br />
                            Approving<br />
                            Authority</th>
                        <th width="7%" class="t-align-center">Source<br />
                            of Funds</th>
                        <th width="8%" class="t-align-center"> Estd. Cost
                            (In Nu.) </th>
                        <th width="8%" class="t-align-center"> Time Code
                            for Process </th>
                        <th width="3%" class="t-align-center"> Advertise Prequal (if applicable)</th>
                        <th width="3%" class="t-align-center"> Invite/Advertise Tender</th>
                        <th width="3%" class="t-align-center">Tender<br />
                            Opening</th>
                        <th width="3%" class="t-align-center">Tender<br />
                            Evaluation</th>
                        <th width="2%" class="t-align-center">Approval to<br />
                            Award</th>
                        <!--Code by Proshanto Kumar Saha-->
                        <th width="2%" class="t-align-center">Letter of Intent<br />
                            to Award</th>
                        <th width="3%" class="t-align-center">Notification<br />
                            of Award</th>
                        <th width="3%" class="t-align-center">Signing of<br />
                            Contract</th>
                        <th width="3%" class="t-align-center">Total<br />
                            time to<br />
                            Contract<br />
                            Signature</th>
                        <th width="2%" class="t-align-center" colspan="<%=columnSpanWork%>">Time for<br />
                            Completion<br />
                            of Contract</th>
                    </tr>
                    <tr>
                        <td class="t-align-center">1</td>
                        <td class="t-align-center">2</td>
                        <td class="t-align-center">3</td>
                        <td class="t-align-center">4</td>
                        <td class="t-align-center">5</td>
                        <td class="t-align-center">6</td>
                        <td class="t-align-center">7</td>
                        <td class="t-align-center">8</td>
                        <td class="t-align-center">9</td>
                        <td class="t-align-center">10</td>
                        <td class="t-align-center">11</td>
                        <td class="t-align-center">12</td>
                        <td class="t-align-center">13</td>
                        <td class="t-align-center">14</td>
                        <td class="t-align-center">15</td>
                        <!--added by Proshanto Kumar Saha-->
                        <td class="t-align-center">16</td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>">17</td>
                    </tr>
<%
            for(AppReportDtBean appReportData : appReportSrBean.getWorkDevList())
            {
%>
                    <%
                              if(!(cmpAPPId==appReportData.getAppId()) || cntr==0){
                                  cmpAPPId = appReportData.getAppId();
%>
                    <tr>
                        <td colspan="<%=columnSpan%>">
                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td width="225" class="ff">Procuring Agency Name and Code :</td>
                                    <td colspan="3">
                                        <%=appReportData.getMinistryDetails()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Budget Type :</td>
                                    <td width="200"><% if(appReportSrBean.isGoodsDev() || appReportSrBean.isServiceDev() || appReportSrBean.isWorkDev()){ %>
                                <!--Development-->Capital
                            <% }else if(appReportSrBean.isGoodsOwn() || appReportSrBean.isServiceOwn() || appReportSrBean.isWorkOwn()){ %>
                                Own Fund
                            <% }else{ %>
                                <!--Revenue-->Recurrent
                            <% } %></td>
                                    <td width="250">Project / Programme Name and Code :</td>
                                    <td>
                                        <% if(appReportData.getProjectNameCode()!=null){ out.print(appReportData.getProjectNameCode()); }else{ out.print("Not Available"); } %>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getPackageNo()%></td>
                        <td rowspan="3" valign="top" class="t-align-left"><table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableList_1">
                                <tr>
                                    <th>Lot No. </th>
                                    <th>Lot Description </th>
                                    <th>Qty. </th>
                                    <th>Unit </th>
                                </tr>
<%
                for(CommonAppPkgDetails lotList : appReportService.getAppDetails("getLotInfo", 0, 0, "", 0, 0, "", appReportData.getPackageId(),0))
                {
%>
                                <tr>
                                    <td><%=lotList.getLotNo()%></td>
                                    <td><%=lotList.getLotDesc()%></td>
                                    <td><%=lotList.getQuantity()%></td>
                                    <td><%=lotList.getUnit()%></td>
                                </tr>
<%
                }
%>
                            </table></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getProcMethod()%> (<%if(appReportData.getProcType().equalsIgnoreCase("NCT")){out.print("NCB");}else if(appReportData.getProcType().equalsIgnoreCase("ICT")){out.print("ICB");}%>)</td>
                        <td rowspan="3" class="t-align-center"><%if(appReportData.getAppAut().equalsIgnoreCase("PE")){out.print("PE");}else if(appReportData.getAppAut().equalsIgnoreCase("HOPE")){out.print("HOPA");}else {out.print(appReportData.getAppAut());} %></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getSrcFund()%></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getEstCost().setScale(2, BigDecimal.ROUND_FLOOR)%><%totEstcost += appReportData.getEstCost().doubleValue();%></td>
                        <th class="t-align-center">Planned<br />
                            Dates</th>
                        <td class="t-align-center"><% if("Yes".equalsIgnoreCase(appReportData.getIsPQRequired())){ out.print(appReportData.getwAdvtDt());  }else{ out.print("-"); }  %></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtInitAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtTenderEvalDt()%></td>
                        <td class="t-align-center">
                            <%=appReportData.getwPlanDtAppAwardDt()%>
                        </td>
                        <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center"><%=appReportData.getwPlanDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtSignCtcDt()%></td>
                        <td class="t-align-center"> - </td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>"><%=appReportData.getwPlanDtTotTimeComContractDt()%></td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Planned<br />
                            Days</th>

                        <td class="t-align-center">0</td>
                        <td class="t-align-center">
                            <%
                                totalDays = 0;
                            %>
                            <%
                                if("Yes".equalsIgnoreCase(appReportData.getIsPQRequired())){
                                    dateDiff = appReportSrBean.getDateDiff(appReportData.getwAdvtDt(), appReportData.getwPlanDtInitAdvtDt());
                                    out.print(dateDiff);
                                    totalDays += dateDiff;
                                }else{
                                    out.print("0");
                                }
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtInitAdvtDt(), appReportData.getwPlanDtTenderOpenDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtTenderOpenDt(), appReportData.getwPlanDtTenderEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtTenderEvalDt(), appReportData.getwPlanDtAppAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <!--Code By Proshanto Kumar Saha-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtAppAwardDt(), appReportData.getwPlanDtLintAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtLintAwardDt(), appReportData.getwPlanDtNtiAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <!--Code End By Proshanto Kumar Saha-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtNtiAwardDt(), appReportData.getwPlanDtSignCtcDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center"><%=totalDays%></td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtSignCtcDt(), appReportData.getwPlanDtTotTimeComContractDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Actual Dates</th>

                        <td class="t-align-center"><%=appReportData.getwActDtAdvtPreDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtTenderEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtAppAwardDt()%></td>
                        <!--Code by Proshanto-->
                        <td class="t-align-center"><%=appReportData.getwActDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtSignCtcDt()%></td>
                        <td class="t-align-center">-</td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>">-</td>
                    </tr>
<%
                cntr++;
            }
%>
                    <tr>
                        <td class="t-align-center"></td>
                        <td class="t-align-center">Total Value of Procurement</td>

                        <td class="t-align-center">&nbsp;</td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"><strong>Total : </strong><%=new BigDecimal(Double.toString(totEstcost)).setScale(2, 0).toPlainString()%></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>"></td>
                    </tr>
<%
        }
        if(appReportSrBean.isWorkRev())
        {
            totEstcost = 0;
            cntr =0;
%>

                    <tr>
                        <th colspan="<%=columnSpan%>" class="t-align-center ff">CONSOLIDATED ANNUAL PROCUREMENT PLAN</th>
                    </tr>
                    <tr>
                        <th width="6%" class="t-align-center">Package<br /> No.</th>
                        <th width="10%" class="t-align-center">Description of <br />
                            Procurement <br />
                            Package
                            Works</th>
                        <th width="11%" class="t-align-center"> Procurement<br />
                            Method and
                            Type </th>
                        <th width="9%" class="t-align-center">Contract<br />
                            Approving<br />
                            Authority</th>
                        <th width="7%" class="t-align-center">Source<br />
                            of Funds</th>
                        <th width="8%" class="t-align-center"> Estd. Cost
                            (In Nu.) </th>
                        <th width="8%" class="t-align-center"> Time Code
                            for Process </th>
                        <th width="3%" class="t-align-center"> Advertise Prequal (if applicable)</th>
                        <th width="3%" class="t-align-center"> Invite/Advertise Tender</th>
                        <th width="3%" class="t-align-center">Tender<br />
                            Opening</th>
                        <th width="3%" class="t-align-center">Tender<br />
                            Evaluation</th>
                        <th width="2%" class="t-align-center">Approval to<br />
                            Award</th>
                         <!--Code by Proshanto Kumar Saha-->
                        <th width="2%" class="t-align-center">Letter of Intent<br />
                            to Award</th>
                        <th width="3%" class="t-align-center">Notification<br />
                            of Award</th>
                        <th width="3%" class="t-align-center">Signing of<br />
                            Contract</th>
                        <th width="3%" class="t-align-center">Total<br />
                            time to<br />
                            Contract<br />
                            Signature</th>
                        <th width="2%" class="t-align-center" colspan="<%=columnSpanWork%>">Time for<br />
                            Completion<br />
                            of Contract</th>
                    </tr>
                    <tr>
                        <td class="t-align-center">1</td>
                        <td class="t-align-center">2</td>
                        <td class="t-align-center">3</td>
                        <td class="t-align-center">4</td>
                        <td class="t-align-center">5</td>
                        <td class="t-align-center">6</td>
                        <td class="t-align-center">7</td>
                        <td class="t-align-center">8</td>
                        <td class="t-align-center">9</td>
                        <td class="t-align-center">10</td>
                        <td class="t-align-center">11</td>
                        <td class="t-align-center">12</td>
                        <td class="t-align-center">13</td>
                        <td class="t-align-center">14</td>
                        <td class="t-align-center">15</td>
                        <!--added by Proshanto Kumar Saha-->
                        <td class="t-align-center">16</td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>">17</td>
                    </tr>
<%
            for(AppReportDtBean appReportData : appReportSrBean.getWorkRevList())
            {
%>
                    <%
                              if(!(cmpAPPId==appReportData.getAppId()) || cntr==0){
                                  cmpAPPId = appReportData.getAppId();
%>
                    <tr>
                        <td colspan="<%=columnSpan%>">
                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td width="225" class="ff">Procuring Agency Name and Code :</td>
                                    <td colspan="3">
                                        <%=appReportData.getMinistryDetails()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Budget Type :</td>
                                    <td width="200"><% if(appReportSrBean.isGoodsDev() || appReportSrBean.isServiceDev() || appReportSrBean.isWorkDev()){ %>
                                <!--Development-->Capital
                            <% }else if(appReportSrBean.isGoodsOwn() || appReportSrBean.isServiceOwn() || appReportSrBean.isWorkOwn()){ %>
                                Own Fund
                            <% }else{ %>
                                <!--Revenue-->Recurrent
                            <% } %></td>
                                    <td width="250">Project / Programme Name and Code :</td>
                                    <td>
                                        <% if(appReportData.getProjectNameCode()!=null){ out.print(appReportData.getProjectNameCode()); }else{ out.print("Not Available"); } %>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getPackageNo()%></td>
                        <td rowspan="3" valign="top" class="t-align-left"><table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableList_1">
                                <tr>
                                    <th>Lot No. </th>
                                    <th>Lot Description </th>
                                    <th>Qty. </th>
                                    <th>Unit </th>
                                </tr>
<%
                for(CommonAppPkgDetails lotList : appReportService.getAppDetails("getLotInfo", 0, 0, "", 0, 0, "", appReportData.getPackageId(),0))
                {
%>
                                <tr>
                                    <td><%=lotList.getLotNo()%></td>
                                    <td><%=lotList.getLotDesc()%></td>
                                    <td><%=lotList.getQuantity()%></td>
                                    <td><%=lotList.getUnit()%></td>
                                </tr>
<%
                }
%>
                            </table></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getProcMethod()%> (<%if(appReportData.getProcType().equalsIgnoreCase("NCT")){out.print("NCB");}else if(appReportData.getProcType().equalsIgnoreCase("ICT")){out.print("ICB");}%>)</td>
                        <td rowspan="3" class="t-align-center"><%if(appReportData.getAppAut().equalsIgnoreCase("PE")){out.print("PE");}else if(appReportData.getAppAut().equalsIgnoreCase("HOPE")){out.print("HOPA");}else {out.print(appReportData.getAppAut());} %></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getSrcFund()%></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getEstCost().setScale(2, BigDecimal.ROUND_FLOOR)%><%totEstcost += appReportData.getEstCost().doubleValue();%></td>
                        <th class="t-align-center">Planned<br />
                            Dates</th>
                        <td class="t-align-center"><% if("Yes".equalsIgnoreCase(appReportData.getIsPQRequired())){ out.print(appReportData.getwAdvtDt()); }else{ out.print("-"); }  %></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtInitAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtTenderEvalDt()%></td>
                        <td class="t-align-center"> 
                                    <%  if("TSTM".equals(appReportData.getProcMethod())){
                                           out.print(appReportData.getgPlanDtInAdvtDt()); 
                                        }else{
                                           out.print(appReportData.getwPlanDtAppAwardDt());
                                        }
                                   %>
                        </td>
                         <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center"><%=appReportData.getwPlanDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtSignCtcDt()%></td>
                        <td class="t-align-center"> - </td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>"><%=appReportData.getwPlanDtTotTimeComContractDt()%></td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Planned<br />
                            Days</th>

                        <td class="t-align-center">
                            0
                            </td>
                        <td class="t-align-center">
                            <% 
                                totalDays = 0;
                                if("Yes".equalsIgnoreCase(appReportData.getIsPQRequired())){
                                    dateDiff = appReportSrBean.getDateDiff(appReportData.getwAdvtDt(), appReportData.getwPlanDtInitAdvtDt());
                                    out.print(dateDiff);
                                    totalDays += dateDiff;
                                }else{
                                    out.print("0");
                                }
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtInitAdvtDt(), appReportData.getwPlanDtTenderOpenDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtTenderOpenDt(), appReportData.getwPlanDtTenderEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtTenderEvalDt(), appReportData.getwPlanDtAppAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtAppAwardDt(), appReportData.getwPlanDtLintAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtLintAwardDt(), appReportData.getwPlanDtNtiAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <!--Code End by Proshanto Kumar Saha-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtNtiAwardDt(), appReportData.getwPlanDtSignCtcDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center"><%=totalDays%></td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtSignCtcDt(), appReportData.getwPlanDtTotTimeComContractDt());
                                out.print(dateDiff);
                            %>
                        </td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Actual Dates</th>

                        <td class="t-align-center"><%=appReportData.getwActDtAdvtPreDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtTenderEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtAppAwardDt()%></td>
                        <!--Code by Proshanto-->
                        <td class="t-align-center"><%=appReportData.getwActDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtSignCtcDt()%></td>
                        <td class="t-align-center">-</td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>">-</td>
                    </tr>
<%
                    cntr++;
            }
%>
                    <tr>
                        <td class="t-align-center"></td>
                        <td class="t-align-center">Total Value of Procurement</td>

                        <td class="t-align-center">&nbsp;</td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"><strong>Total : </strong><%=new BigDecimal(Double.toString(totEstcost)).setScale(2, 0).toPlainString()%></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>"></td>
                    </tr>
<%
        }
        if(appReportSrBean.isWorkOwn())
        {
            totEstcost = 0;
            cntr =0;
%>

                    <tr>
                        <th colspan="<%=columnSpan%>" class="t-align-center ff">CONSOLIDATED ANNUAL PROCUREMENT PLAN</th>
                    </tr>
                    <tr>
                        <th width="6%" class="t-align-center">Package<br /> No.</th>
                        <th width="10%" class="t-align-center">Description of <br />
                            Procurement <br />
                            Package
                            Works</th>
                        <th width="11%" class="t-align-center"> Procurement<br />
                            Method and
                            Type </th>
                        <th width="9%" class="t-align-center">Contract<br />
                            Approving<br />
                            Authority</th>
                        <th width="7%" class="t-align-center">Source<br />
                            of Funds</th>
                        <th width="8%" class="t-align-center"> Estd. Cost
                            (In Nu.) </th>
                        <th width="8%" class="t-align-center"> Time Code
                            for Process </th>
                        <th width="3%" class="t-align-center"> Advertise Prequal (if applicable)</th>
                        <th width="3%" class="t-align-center"> Invite/Advertise Tender</th>
                        <th width="3%" class="t-align-center">Tender<br />
                            Opening</th>
                        <th width="3%" class="t-align-center">Tender<br />
                            Evaluation</th>
                        <th width="2%" class="t-align-center">Approval to<br />
                            Award</th>
                        <!--Code by Proshanto Kumar Saha-->
                        <th width="2%" class="t-align-center">Letter of Intent<br />
                            to Award</th>
                        <th width="3%" class="t-align-center">Notification<br />
                            of Award</th>
                        <th width="3%" class="t-align-center">Signing of<br />
                            Contract</th>
                        <th width="3%" class="t-align-center">Total<br />
                            time to<br />
                            Contract<br />
                            Signature</th>
                        <th width="2%" class="t-align-center" colspan="<%=columnSpanWork%>">Time for<br />
                            Completion<br />
                            of Contract</th>
                    </tr>
                    <tr>
                        <td class="t-align-center">1</td>
                        <td class="t-align-center">2</td>
                        <td class="t-align-center">3</td>
                        <td class="t-align-center">4</td>
                        <td class="t-align-center">5</td>
                        <td class="t-align-center">6</td>
                        <td class="t-align-center">7</td>
                        <td class="t-align-center">8</td>
                        <td class="t-align-center">9</td>
                        <td class="t-align-center">10</td>
                        <td class="t-align-center">11</td>
                        <td class="t-align-center">12</td>
                        <td class="t-align-center">13</td>
                        <td class="t-align-center">14</td>
                        <td class="t-align-center">15</td>
                        <!--Added by Proshanto-->
                        <td class="t-align-center">16</td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>">17</td>
                    </tr>
<%
            for(AppReportDtBean appReportData : appReportSrBean.getWorkOwnList())
            {
%>
                    <%
                              if(!(cmpAPPId==appReportData.getAppId()) || cntr==0){
                                  cmpAPPId = appReportData.getAppId();
%>
                    <tr>
                        <td colspan="<%=columnSpan%>">
                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td width="225" class="ff">Procuring Agency Name and Code :</td>
                                    <td colspan="3">
                                        <%=appReportData.getMinistryDetails()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Budget Type :</td>
                                    <td width="200"><% if(appReportSrBean.isGoodsDev() || appReportSrBean.isServiceDev() || appReportSrBean.isWorkDev()){ %>
<!--                                Development-->Capital
                            <% }else if(appReportSrBean.isGoodsOwn() || appReportSrBean.isServiceOwn() || appReportSrBean.isWorkOwn()){ %>
                                Own Fund
                            <% }else{ %>
<!--                                Revenue-->Recurrent
                            <% } %></td>
                                    <td width="250">Project / Programme Name and Code :</td>
                                    <td>
                                        <% if(appReportData.getProjectNameCode()!=null){ out.print(appReportData.getProjectNameCode()); }else{ out.print("Not Available"); } %>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getPackageNo()%></td>
                        <td rowspan="3" valign="top" class="t-align-left"><table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableList_1">
                                <tr>
                                    <th>Lot No. </th>
                                    <th>Lot Description </th>
                                    <th>Qty. </th>
                                    <th>Unit </th>
                                </tr>
<%
                for(CommonAppPkgDetails lotList : appReportService.getAppDetails("getLotInfo", 0, 0, "", 0, 0, "", appReportData.getPackageId(),0))
                {
%>
                                <tr>
                                    <td><%=lotList.getLotNo()%></td>
                                    <td><%=lotList.getLotDesc()%></td>
                                    <td><%=lotList.getQuantity()%></td>
                                    <td><%=lotList.getUnit()%></td>
                                </tr>
<%
                }
%>
                            </table></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getProcMethod()%> (<%if(appReportData.getProcType().equalsIgnoreCase("NCT")){out.print("NCB");}else if(appReportData.getProcType().equalsIgnoreCase("ICT")){out.print("ICB");}%>)</td>
                        <td rowspan="3" class="t-align-center"><%if(appReportData.getAppAut().equalsIgnoreCase("PE")){out.print("PE");}else if(appReportData.getAppAut().equalsIgnoreCase("HOPE")){out.print("HOPA");}else {out.print(appReportData.getAppAut());} %></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getSrcFund()%></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getEstCost().setScale(2, BigDecimal.ROUND_FLOOR)%><%totEstcost += appReportData.getEstCost().doubleValue();%></td>
                        <th class="t-align-center">Planned<br />
                            Dates</th>
                        <td class="t-align-center"><% if("Yes".equalsIgnoreCase(appReportData.getIsPQRequired())){ out.print(appReportData.getwAdvtDt()); }else{ out.print("-"); }  %></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtInitAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtTenderEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtAppAwardDt()%></td>
                        <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center"><%=appReportData.getwPlanDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwPlanDtSignCtcDt()%></td>
                        <td class="t-align-center"> - </td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>"><%=appReportData.getwPlanDtTotTimeComContractDt()%></td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Planned<br />
                            Days</th>

                        <td class="t-align-center">0</td>
                        <td class="t-align-center">
                             <%
                             
                             totalDays = 0;
                             if("Yes".equalsIgnoreCase(appReportData.getIsPQRequired())){
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwAdvtDt(), appReportData.getwPlanDtInitAdvtDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                             }else{
                                out.print("0");
                             }
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtInitAdvtDt(), appReportData.getwPlanDtTenderOpenDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtTenderOpenDt(), appReportData.getwPlanDtTenderEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtTenderEvalDt(), appReportData.getwPlanDtAppAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <!--Code by Proshanto Kumar Saha-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtAppAwardDt(), appReportData.getwPlanDtLintAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtLintAwardDt(), appReportData.getwPlanDtNtiAwardDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <!--Code End by Proshanto Kumar Saha-->
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtNtiAwardDt(), appReportData.getwPlanDtSignCtcDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center"><%=totalDays%></td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getwPlanDtSignCtcDt(), appReportData.getwPlanDtTotTimeComContractDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Actual Dates</th>

                        <td class="t-align-center"><%=appReportData.getwActDtAdvtPreDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtAdvtDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtTenderOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtTenderEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtAppAwardDt()%></td>
                        <!--added by Proshanto-->
                        <td class="t-align-center"><%=appReportData.getwActDtLintAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtNtiAwardDt()%></td>
                        <td class="t-align-center"><%=appReportData.getwActDtSignCtcDt()%></td>
                        <td class="t-align-center">-</td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>">-</td>
                    </tr>
<%
                    cntr++;
            }
%>
                    <tr>
                        <td class="t-align-center"></td>
                        <td class="t-align-center">Total Value of Procurement</td>

                        <td class="t-align-center">&nbsp;</td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"><strong>Total : </strong><%=new BigDecimal(Double.toString(totEstcost)).setScale(2, 0).toPlainString()%></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <!--Added by Proshanto-->
                        <td class="t-align-center"></td>
                        <td class="t-align-center" colspan="<%=columnSpanWork%>"></td>
                    </tr>
<%
        }
        if(appReportSrBean.isServiceDev())
        { 
            totEstcost = 0;
            cntr = 0;
%>

                    <tr>
                        <th colspan="<%=columnSpan%>" class="t-align-center ff">CONSOLIDATED ANNUAL PROCUREMENT PLAN</th>
                    </tr>
                    <tr>
                        <th width="6%" class="t-align-center">Package<br /> No.</th>
                        <th width="10%" class="t-align-center">Description of <br />
                            Procurement <br />
                            Package
                            Services</th>
                        <th width="11%" class="t-align-center"> Procurement<br />
                            Method and
                            Type </th>
                        <th width="9%" class="t-align-center">Contract<br />
                            Approving<br />
                            Authority</th>
                        <th width="7%" class="t-align-center">Source<br />
                            of Funds</th>
                        <th width="8%" class="t-align-center"> Estd. Cost
                            (In Nu.) </th>
                        <th width="8%" class="t-align-center"> Time Code
                            for Process </th>
                        <th width="3%" class="t-align-center"> Advertise EOI</th>
                        <th width="3%" class="t-align-center"> Issue RFP</th>
                        <th width="3%" class="t-align-center">Technical Proposal<br />
                            Opening</th>
                        <th width="3%" class="t-align-center">Technical Proposal<br />
                            Evaluation</th>
                        <th width="2%" class="t-align-center">Financial Proposal<br />
                            Opening & Evaluation</th>
                        <th width="3%" class="t-align-center">Notification</th>
                        <th width="3%" class="t-align-center">Approval</th>
                        <th width="3%" class="t-align-center">Signing of <br/>Contract</th>
                        <th width="3%" class="t-align-center">Total<br />
                            time to<br />
                            Contract<br />
                            Signature</th>
                        <th width="2%" class="t-align-center">Time for<br />
                            Completion<br />
                            of Contract</th>
                    </tr>
                    <tr>
                        <td class="t-align-center">1</td>
                        <td class="t-align-center">2</td>
                        <td class="t-align-center">3</td>
                        <td class="t-align-center">4</td>
                        <td class="t-align-center">5</td>
                        <td class="t-align-center">6</td>
                        <td class="t-align-center">7</td>
                        <td class="t-align-center">8</td>
                        <td class="t-align-center">9</td>
                        <td class="t-align-center">10</td>
                        <td class="t-align-center">11</td>
                        <td class="t-align-center">12</td>
                        <td class="t-align-center">13</td>
                        <td class="t-align-center">14</td>
                        <td class="t-align-center">15</td>
                        <td class="t-align-center">16</td>
                        <td class="t-align-center">17</td>
                    </tr>
<%
            for(AppReportDtBean appReportData : appReportSrBean.getServiceDevList())
            {
%>
                    <%
                               if(!(cmpAPPId==appReportData.getAppId()) || cntr==0){
                                  cmpAPPId = appReportData.getAppId();
%>
                    <tr>
                        <td colspan="<%=columnSpan%>">
                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td width="225" class="ff">Procuring Agency Name and Code :</td>
                                    <td colspan="3">
                                        <%=appReportData.getMinistryDetails()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Budget Type :</td>
                                    <td width="200"><% if(appReportSrBean.isGoodsDev() || appReportSrBean.isServiceDev() || appReportSrBean.isWorkDev()){ %>
                                <!--Development-->Capital
                            <% }else if(appReportSrBean.isGoodsOwn() || appReportSrBean.isServiceOwn() || appReportSrBean.isWorkOwn()){ %>
                                Own Fund
                            <% }else{ %>
                                <!--Revenue-->Recurrent
                            <% } %></td>
                                    <td width="250">Project / Programme Name and Code :</td>
                                    <td>
                                        <% if(appReportData.getProjectNameCode()!=null){ out.print(appReportData.getProjectNameCode()); }else{ out.print("Not Available"); } %>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getPackageNo()%></td>
                        <td rowspan="3" valign="top" class="t-align-left"><table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableList_1">
                                <tr>
                                    <th>Package No. </th>
                                    <th>Package Description </th>
                                    <th>Qty. </th>
                                    <th>Unit </th>
                                </tr>
<%
                for(CommonAppPkgDetails lotList : appReportService.getAppDetails("getLotInfo", 0, 0, "", 0, 0, "", appReportData.getPackageId(),0))
                {
%>
                                <tr>
                                    <td><%=lotList.getLotNo()%></td>
                                    <td><%=lotList.getLotDesc()%></td>
                                    <td><%=lotList.getQuantity()%></td>
                                    <td><%=lotList.getUnit()%></td>
                                </tr>
<%
                }
%>
                            </table></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getProcMethod()%> (<%if(appReportData.getProcType().equalsIgnoreCase("NCT")){out.print("NCB");}else if(appReportData.getProcType().equalsIgnoreCase("ICT")){out.print("ICB");}%>)</td>
                        <td rowspan="3" class="t-align-center"><%if(appReportData.getAppAut().equalsIgnoreCase("PE")){out.print("PE");}else if(appReportData.getAppAut().equalsIgnoreCase("HOPE")){out.print("HOPA");}else {out.print(appReportData.getAppAut());} %></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getSrcFund()%></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getEstCost().setScale(2, BigDecimal.ROUND_FLOOR)%><%totEstcost += appReportData.getEstCost().doubleValue();%></td>
                        <th class="t-align-center">Planned<br />
                            Dates</th>
                        <td class="t-align-center"><%=appReportData.getsPlanDtAdvtEOIDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtIsuRFPDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtTechProOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtTechProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtFinProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtNegoDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtAppDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtSignCtcDt()%></td>
                        <td class="t-align-center"> - </td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtTotTimeComContractDt()%></td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Planned<br />
                            Days</th>

                        <td class="t-align-center">0</td>
                        <td class="t-align-center">
                            <%
                                totalDays = 0;
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtAdvtEOIDt(), appReportData.getsPlanDtIsuRFPDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtIsuRFPDt(), appReportData.getsPlanDtTechProOpenDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtTechProOpenDt(), appReportData.getsPlanDtTechProEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtTechProEvalDt(), appReportData.getsPlanDtFinProEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtFinProEvalDt(), appReportData.getsPlanDtNegoDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;                                
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtNegoDt(), appReportData.getsPlanDtAppDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtAppDt(), appReportData.getsPlanDtSignCtcDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center"><%=totalDays%></td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtSignCtcDt(), appReportData.getsPlanDtTotTimeComContractDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Actual Dates</th>

                        <td class="t-align-center"><%=appReportData.getsActDtAdvtEOIDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtIsuRFPDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtTechProOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtTechProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtFinProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtNegoDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtAppDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtSignCtcDt()%></td>
                        <td class="t-align-center">-</td>
                        <td class="t-align-center">-</td>
                    </tr>
<%
                    cntr++;
            }
%>
                    <tr>
                        <td class="t-align-center"></td>
                        <td class="t-align-center">Total Value of Procurement</td>

                        <td class="t-align-center">&nbsp;</td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"><strong>Total : </strong><%=new BigDecimal(Double.toString(totEstcost)).setScale(2, 0).toPlainString()%></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                    </tr>
<%
        }
        if(appReportSrBean.isServiceRev())
        {
            totEstcost = 0;
            cntr = 0;
%>

                    <tr>
                        <th colspan="<%=columnSpan%>" class="t-align-center ff">CONSOLIDATED ANNUAL PROCUREMENT PLAN</th>
                    </tr>
                    <tr>
                        <th width="6%" class="t-align-center">Package<br /> No.</th>
                        <th width="10%" class="t-align-center">Description of <br />
                            Procurement <br />
                            Package
                            Services</th>
                        <th width="11%" class="t-align-center"> Procurement<br />
                            Method and
                            Type </th>
                        <th width="9%" class="t-align-center">Contract<br />
                            Approving<br />
                            Authority</th>
                        <th width="7%" class="t-align-center">Source<br />
                            of Funds</th>
                        <th width="8%" class="t-align-center"> Estd. Cost
                            (In Nu.) </th>
                        <th width="8%" class="t-align-center"> Time Code
                            for Process </th>
                        <th width="3%" class="t-align-center"> Advertise EOI</th>
                        <th width="3%" class="t-align-center"> Issue RFP</th>
                        <th width="3%" class="t-align-center">Technical Proposal<br />
                            Opening</th>
                        <th width="3%" class="t-align-center">Technical Proposal<br />
                            Evaluation</th>
                        <th width="2%" class="t-align-center">Financial Proposal<br />
                            Opening & Evaluation</th>
                        <th width="3%" class="t-align-center">Notification</th>
                        <th width="3%" class="t-align-center">Approval</th>
                        <th width="3%" class="t-align-center">Signing of <br/>Contract</th>
                        <th width="3%" class="t-align-center">Total<br />
                            time to<br />
                            Contract<br />
                            Signature</th>
                        <th width="2%" class="t-align-center">Time for<br />
                            Completion<br />
                            of Contract</th>
                    </tr>
                    <tr>
                        <td class="t-align-center">1</td>
                        <td class="t-align-center">2</td>
                        <td class="t-align-center">3</td>
                        <td class="t-align-center">4</td>
                        <td class="t-align-center">5</td>
                        <td class="t-align-center">6</td>
                        <td class="t-align-center">7</td>
                        <td class="t-align-center">8</td>
                        <td class="t-align-center">9</td>
                        <td class="t-align-center">10</td>
                        <td class="t-align-center">11</td>
                        <td class="t-align-center">12</td>
                        <td class="t-align-center">13</td>
                        <td class="t-align-center">14</td>
                        <td class="t-align-center">15</td>
                        <td class="t-align-center">16</td>
                        <td class="t-align-center">17</td>
                    </tr>
<%
            for(AppReportDtBean appReportData : appReportSrBean.getServiceRevList())
            {
%>
                    <%
                             if(!(cmpAPPId==appReportData.getAppId()) || cntr==0){
                                  cmpAPPId = appReportData.getAppId();
%>
                    <tr>
                        <td colspan="<%=columnSpan%>">
                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td width="225" class="ff">Procuring Agency Name and Code :</td>
                                    <td colspan="3">
                                        <%=appReportData.getMinistryDetails()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Budget Type :</td>
                                    <td width="200"><% if(appReportSrBean.isGoodsDev() || appReportSrBean.isServiceDev() || appReportSrBean.isWorkDev()){ %>
                                <!--Development-->Capital
                            <% }else if(appReportSrBean.isGoodsOwn() || appReportSrBean.isServiceOwn() || appReportSrBean.isWorkOwn()){ %>
                                Own Fund
                            <% }else{ %>
                                <!--Revenue-->Recurrent
                            <% } %></td>
                                    <td width="250">Project / Programme Name and Code :</td>
                                    <td>
                                        <% if(appReportData.getProjectNameCode()!=null){ out.print(appReportData.getProjectNameCode()); }else{ out.print("Not Available"); } %>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <% } %>
                     <tr>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getPackageNo()%></td>
                        <td rowspan="3" valign="top" class="t-align-left"><table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableList_1">
                                <tr>
                                    <th>Package No. </th>
                                    <th>Package Description </th>
                                    <th>Qty. </th>
                                    <th>Unit </th>
                                </tr>
<%
                for(CommonAppPkgDetails lotList : appReportService.getAppDetails("getLotInfo", 0, 0, "", 0, 0, "", appReportData.getPackageId(),0))
                {
%>
                                <tr>
                                    <td><%=lotList.getLotNo()%></td>
                                    <td><%=lotList.getLotDesc()%></td>
                                    <td><%=lotList.getQuantity()%></td>
                                    <td><%=lotList.getUnit()%></td>
                                </tr>
<%
                }
%>
                            </table></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getProcMethod()%> (<%if(appReportData.getProcType().equalsIgnoreCase("NCT")){out.print("NCB");}else if(appReportData.getProcType().equalsIgnoreCase("ICT")){out.print("ICB");}%>)</td>
                        <td rowspan="3" class="t-align-center"><%if(appReportData.getAppAut().equalsIgnoreCase("PE")){out.print("PE");}else if(appReportData.getAppAut().equalsIgnoreCase("HOPE")){out.print("HOPA");}else {out.print(appReportData.getAppAut());} %></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getSrcFund()%></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getEstCost().setScale(2, BigDecimal.ROUND_FLOOR)%><%totEstcost += appReportData.getEstCost().doubleValue();%></td>
                        <th class="t-align-center">Planned<br />
                            Dates</th>
                        <td class="t-align-center"><%=appReportData.getsPlanDtAdvtEOIDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtIsuRFPDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtTechProOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtTechProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtFinProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtNegoDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtAppDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtSignCtcDt()%></td>
                        <td class="t-align-center"> - </td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtTotTimeComContractDt()%></td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Planned<br />
                            Days</th>

                        <td class="t-align-center">0</td>
                        <td class="t-align-center">
                            <%
                                totalDays = 0;
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtAdvtEOIDt(), appReportData.getsPlanDtIsuRFPDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtIsuRFPDt(), appReportData.getsPlanDtTechProOpenDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtTechProOpenDt(), appReportData.getsPlanDtTechProEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtTechProEvalDt(), appReportData.getsPlanDtFinProEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtFinProEvalDt(), appReportData.getsPlanDtNegoDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtNegoDt(), appReportData.getsPlanDtAppDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtAppDt(), appReportData.getsPlanDtSignCtcDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center"><%=totalDays%></td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtSignCtcDt(), appReportData.getsPlanDtTotTimeComContractDt());
                                out.print(dateDiff);
                            %>
                        </td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Actual Dates</th>

                        <td class="t-align-center"><%=appReportData.getsActDtAdvtEOIDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtIsuRFPDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtTechProOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtTechProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtFinProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtNegoDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtAppDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtSignCtcDt()%></td>
                        <td class="t-align-center">-</td>
                        <td class="t-align-center">-</td>
                    </tr>
<%
                cntr++;
            }
%>
                    <tr>
                        <td class="t-align-center"></td>
                        <td class="t-align-center">Total Value of Procurement</td>

                        <td class="t-align-center">&nbsp;</td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"><strong>Total : </strong><%=new BigDecimal(Double.toString(totEstcost)).setScale(2, 0).toPlainString()%></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                    </tr>
<%
        }
        if(appReportSrBean.isServiceOwn())
        {
            totEstcost = 0;
            cntr = 0;
%>

                    <tr>
                        <th colspan="<%=columnSpan%>" class="t-align-center ff">CONSOLIDATED ANNUAL PROCUREMENT PLAN</th>
                    </tr>
                    <tr>
                        <th width="6%" class="t-align-center">Package<br /> No.</th>
                        <th width="10%" class="t-align-center">Description of <br />
                            Procurement <br />
                            Package
                            Services</th>
                        <th width="11%" class="t-align-center"> Procurement<br />
                            Method and
                            Type </th>
                        <th width="9%" class="t-align-center">Contract<br />
                            Approving<br />
                            Authority</th>
                        <th width="7%" class="t-align-center">Source<br />
                            of Funds</th>
                        <th width="8%" class="t-align-center"> Estd. Cost
                            (In Nu.) </th>
                        <th width="8%" class="t-align-center"> Time Code
                            for Process </th>
                        <th width="3%" class="t-align-center"> Advertise EOI</th>
                        <th width="3%" class="t-align-center"> Issue RFP</th>
                        <th width="3%" class="t-align-center">Technical Proposal<br />
                            Opening</th>
                        <th width="3%" class="t-align-center">Technical Proposal<br />
                            Evaluation</th>
                        <th width="2%" class="t-align-center">Financial Proposal<br />
                            Opening & Evaluation</th>
                        <th width="3%" class="t-align-center">Notification</th>
                        <th width="3%" class="t-align-center">Approval</th>
                        <th width="3%" class="t-align-center">Signing of <br/>Contract</th>
                        <th width="3%" class="t-align-center">Total<br />
                            time to<br />
                            Contract<br />
                            Signature</th>
                        <th width="2%" class="t-align-center">Time for<br />
                            Completion<br />
                            of Contract</th>
                    </tr>
                    <tr>
                        <td class="t-align-center">1</td>
                        <td class="t-align-center">2</td>
                        <td class="t-align-center">3</td>
                        <td class="t-align-center">4</td>
                        <td class="t-align-center">5</td>
                        <td class="t-align-center">6</td>
                        <td class="t-align-center">7</td>
                        <td class="t-align-center">8</td>
                        <td class="t-align-center">9</td>
                        <td class="t-align-center">10</td>
                        <td class="t-align-center">11</td>
                        <td class="t-align-center">12</td>
                        <td class="t-align-center">13</td>
                        <td class="t-align-center">14</td>
                        <td class="t-align-center">15</td>
                        <td class="t-align-center">16</td>
                        <td class="t-align-center">17</td>
                    </tr>
<%
            for(AppReportDtBean appReportData : appReportSrBean.getServiceOwnList())
            {
%>
                    <%
                               if(!(cmpAPPId==appReportData.getAppId()) || cntr==0){
                                  cmpAPPId = appReportData.getAppId();
%>
                    <tr>
                        <td colspan="<%=columnSpan%>">
                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td width="225" class="ff">Procuring Agency Name and Code :</td>
                                    <td colspan="3">
                                        <%=appReportData.getMinistryDetails()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Budget Type :</td>
                                    <td width="200"><% if(appReportSrBean.isGoodsDev() || appReportSrBean.isServiceDev() || appReportSrBean.isWorkDev()){ %>
                                <!--Development-->Capital
                            <% }else if(appReportSrBean.isGoodsOwn() || appReportSrBean.isServiceOwn() || appReportSrBean.isWorkOwn()){ %>
                                Own Fund
                            <% }else{ %>
                                <!--Revenue-->Recurrent
                            <% } %></td>
                                    <td width="250">Project / Programme Name and Code :</td>
                                    <td>
                                        <% if(appReportData.getProjectNameCode()!=null){ out.print(appReportData.getProjectNameCode()); }else{ out.print("Not Available"); } %>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getPackageNo()%></td>
                        <td rowspan="3" valign="top" class="t-align-left"><table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableList_1">
                                <tr>
                                    <th>Package No. </th>
                                    <th>Package Description </th>
                                    <th>Qty. </th>
                                    <th>Unit </th>
                                </tr>
<%
                for(CommonAppPkgDetails lotList : appReportService.getAppDetails("getLotInfo", 0, 0, "", 0, 0, "", appReportData.getPackageId(),0))
                {
%>
                                <tr>
                                    <td><%=lotList.getLotNo()%></td>
                                    <td><%=lotList.getLotDesc()%></td>
                                    <td><%=lotList.getQuantity()%></td>
                                    <td><%=lotList.getUnit()%></td>
                                </tr>
<%
                }
%>
                            </table></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getProcMethod()%> (<%if(appReportData.getProcType().equalsIgnoreCase("NCT")){out.print("NCB");}else if(appReportData.getProcType().equalsIgnoreCase("ICT")){out.print("ICB");}%>)</td>
                        <td rowspan="3" class="t-align-center"><%if(appReportData.getAppAut().equalsIgnoreCase("PE")){out.print("PE");}else if(appReportData.getAppAut().equalsIgnoreCase("HOPE")){out.print("HOPA");}else {out.print(appReportData.getAppAut());} %></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getSrcFund()%></td>
                        <td rowspan="3" class="t-align-center"><%=appReportData.getEstCost().setScale(2, BigDecimal.ROUND_FLOOR)%><%totEstcost += appReportData.getEstCost().doubleValue();%></td>
                        <th class="t-align-center">Planned<br />
                            Dates</th>
                        <td class="t-align-center"><%=appReportData.getsPlanDtAdvtEOIDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtIsuRFPDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtTechProOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtTechProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtFinProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtNegoDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtAppDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtSignCtcDt()%></td>
                        <td class="t-align-center"> - </td>
                        <td class="t-align-center"><%=appReportData.getsPlanDtTotTimeComContractDt()%></td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Planned<br />
                            Days</th>

                        <td class="t-align-center">0</td>
                        <td class="t-align-center">
                            <%
                                totalDays =0;
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtAdvtEOIDt(), appReportData.getsPlanDtIsuRFPDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtIsuRFPDt(), appReportData.getsPlanDtTechProOpenDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtTechProOpenDt(), appReportData.getsPlanDtTechProEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtTechProEvalDt(), appReportData.getsPlanDtFinProEvalDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtFinProEvalDt(), appReportData.getsPlanDtNegoDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtNegoDt(), appReportData.getsPlanDtAppDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtAppDt(), appReportData.getsPlanDtSignCtcDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                        <td class="t-align-center"><%=totalDays%></td>
                        <td class="t-align-center">
                            <%
                                dateDiff = appReportSrBean.getDateDiff(appReportData.getsPlanDtSignCtcDt(), appReportData.getsPlanDtTotTimeComContractDt());
                                out.print(dateDiff);
                                totalDays += dateDiff;
                            %>
                        </td>
                    </tr>
                    <tr>
                        <th class="t-align-center">Actual Dates</th>

                        <td class="t-align-center"><%=appReportData.getsActDtAdvtEOIDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtIsuRFPDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtTechProOpenDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtTechProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtFinProEvalDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtNegoDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtAppDt()%></td>
                        <td class="t-align-center"><%=appReportData.getsActDtSignCtcDt()%></td>
                        <td class="t-align-center">-</td>
                        <td class="t-align-center">-</td>
                    </tr>
<%
                    cntr++;
            }
%>
                    <tr>
                        <td class="t-align-center"></td>
                        <td class="t-align-center">Total Value of Procurement</td>

                        <td class="t-align-center">&nbsp;</td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"><strong>Total : </strong><%=new BigDecimal(Double.toString(totEstcost)).setScale(2, 0).toPlainString()%></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                        <td class="t-align-center"></td>
                    </tr>
<%
        }
    }
%>
                </table>
                </form>

                <script type="text/javascript">
                            $(document).ready(function() {

                                $("#print").click(function() {
                                    //alert('sa');
                                    printElem({ leaveOpen: true, printMode: 'popup' });
                                });

                            });
                            function printElem(options){
                                //alert(options);

                                $('#print_area').printElement(options);
                                //$('#trLast').hide();
                            }

                    </script>
                <%if(appReportSrBean.isReportData()){%>
<!--                <div align="center">
                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                    <%
                         // String reqURL = request.getRequestURL().toString();
                         // String reqQuery = "appId=" + appId;
                         // String folderName = pdfConstant.APPVIEW;
                         // String id = String.valueOf(appId);
                    %>
                    <a class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?reqURL=<%=reqURL%>&reqQuery=<%=reqQuery%>&folderName=<%=folderName%>&id=<%=id%>" >Save As PDF </a>&nbsp;
                </div>-->
                <%}%>
                </div>
                <!--Dashboard Content Part End-->
                <%if(!isPDF.equalsIgnoreCase("true")) {%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <%}%>
            
        </div>
        <form name="frmresetForm"  id="frmresetForm" method="post" action="<%=request.getContextPath()%>/officer/SearchPackageReport.jsp"></form>
    </body>
        <script type="text/javascript" language="Javascript">
            function resetForm(){
                document.getElementById("frmresetForm").submit();
            }
        </script>
        <script>
        var headSel_Obj = document.getElementById("headTabReport");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

