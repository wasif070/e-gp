<%--
    Document   : TSCFormation
    Created on : Nov 16, 2010, 11:06:27 AM
    Author     : TaherT
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommitteDtBean" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.OfficeMemberDtBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.PEOfficeCreationService" %>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum" %>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommitteMemDtBean"%>
<%@page import="com.cptu.egp.eps.web.databean.TenderCommitteDtBean"%>
<jsp:useBean id="tenderCommitteSrBean" class="com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean"/>
<jsp:useBean id="tenderCommitteDtBean" class="com.cptu.egp.eps.web.databean.TenderCommitteDtBean"/>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <title>Technical Sub Committee Details</title>
        <script type="text/javascript">                           
            $(function() {
                $('#btnSearch').click(function() {
                    if($('#cmboffice').val()!=null){
                        $.post("<%=request.getContextPath()%>/CommMemServlet", {officeId:$('#cmboffice').val(),pe1count:$('#pe1count').val(),funName:'memberSearch',tenderId:$('#hdnTenderId').val()}, function(j){
                            var i = j.toString();                            
                            var tmpcnt= i.substring(i.indexOf("#", i.indexOf("</tr>",0))+1,i.length);
                            var cnt = $('#pe1count').val();
                            $('#pe1count').val(eval(eval(cnt)+eval(tmpcnt)));
                            $("#tbodype").html(j.substring(0, i.indexOf("#",0)));
                        });
                    }else{
                        $("#tbodype").html(null);
                    }
                });
            });
            $(function() {
                $('#btnIndiSearch').click(function() {
                    if($('#txtEmailid').val()!=""){
                        $.post("<%=request.getContextPath()%>/CommMemServlet", {emailId:$('#txtEmailid').val(),pe2count:$('#pe1count').val(),funName:'indiMemSearch'}, function(j){
                            var i = j.toString();
                            var tmpcnt= i.substring(i.indexOf("#", i.indexOf("</tr>",0))+1,i.length);
                            var cnt = $('#pe1count').val();
                            $('#pe1count').val(eval(eval(cnt)+eval(tmpcnt)));
                            $("#tbodype1").html(j.substring(0, i.indexOf("#",0)));
                        });
                    }else{
                        $("#tbodype1").html(null);
                    }
                });
            });
            $(function() {
                $('#frmComm').submit(function() {                                        
                    if($('#txtaRemarks').val() != undefined){
                    if($.trim($('#txtaRemarks').val())==""){
                        if($('#txtaRemarks').val()==""){
                            $('#divRem').html("Please enter Remarks");
                            return false;
                        }else{
                            $('#divRem').html(null);
                        }
                    }
                    }else{
                    if($.trim($('#txtcommitteeName').val())==""){
                        $('#errmsg').html("Please enter Committee Name.");
                        $('#errmsg').css("color", "red");
                        return false;
                    }else{                        
                        if(($.trim($('#txtcommitteeName').val()).length)>100){
                            $('#errmsg').html("Maximum 100 Characters are allowed.");
                            $('#errmsg').css("color", "red");
                            return false;
                        }else{
                            $('#errmsg').html(null);                            
                        }
                    }                    
                    var minin = $('#minmem').val();
                    var maxin = $('#maxmem').val();                    
                    var len=$('#members').children()[1].children.length;
                    if(len<minin){
                        jAlert("Minimum "+minin+" members required.","Min member alert", function(RetVal) {
                        });
                        return false;
                    }
                    if(len>maxin){
                        jAlert("Maximum "+maxin+" members required.","Max member alert", function(RetVal) {
                        });
                        return false;
                    }
                    var cp=0;
                    var ms=0;
                    for(var i=0;i<len;i++){                        
                        if($('#cmbMemRole'+i).val()=="cp"){
                            cp++;
                        }
                        if($('#cmbMemRole'+i).val()=="ms"){
                            ms++;
                        }
                    }                    
                    if(cp>1){
                        jAlert("There must be 1 Chairperson.","Chairperson alert", function(RetVal) {
                        });
                        return false;
                    }
                    if(cp==0){
                        jAlert("There must be 1 Chairperson.","Chairperson alert", function(RetVal) {
                        });
                        return false;
                    }
                    if(ms>1){
                        jAlert("There should be only 1 or 0 Member Secretary.","Member Secretary alert", function(RetVal) {
                        });
                        return false;
                    }
                }
                });
            });
            $(function() {
                $('#spe').click(function() {
                    $("#pe1").css("display","block");
                    $("#pe2").css("display","none");
                    $("#pe3").css("display","none");
                    $("#spe").addClass('sMenu');
                    $("#ope").removeClass('sMenu');
                    $("#icm").removeClass('sMenu');
                });
                $('#ope').click(function() {
                    $("#pe2").css("display","block");
                    $("#pe1").css("display","none");
                    $("#pe3").css("display","none");
                    $("#ope").addClass('sMenu');
                    $("#spe").removeClass('sMenu');
                    $("#icm").removeClass('sMenu');
                });
                $('#icm').click(function() {
                    $("#pe3").css("display","block");
                    $("#pe2").css("display","none");
                    $("#pe1").css("display","none");
                    $("#icm").addClass('sMenu');
                    $("#ope").removeClass('sMenu');
                    $("#spe").removeClass('sMenu');
                });
            });
            function delRow(row){
                var curRow = $(row).parents('tr');
                var len=$('#members').children()[1].children.length;
                var id=$(row).attr("id").substring(6,$(row).attr("id").length);
                for(var i=id;i<len;i++){
                    $('#memid'+eval(eval(i)+eval(1))).attr("id","memid"+(i));
                    $('#memgovid'+eval(eval(i)+eval(1))).attr("id","memgovid"+(i));
                    $('#cmbMemRole'+eval(eval(i)+eval(1))).attr("id","cmbMemRole"+(i));
                    $('#memfrm'+eval(eval(i)+eval(1))).attr("id","memfrm"+(i));
                    $('#delRow'+eval(eval(i)+eval(1))).attr("id","delRow"+(i));
                }
                curRow.remove();
            }

            $(function() {
                $('#cmborg').change(function() {
                    $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:$('#cmborg').val(),funName:'Office'}, function(j){
                        $("select#cmboffice").html(j);
                    });
                });
            });
             $(function() {
                $('#txtcommitteeName').blur(function() {
                     if($.trim($('#txtcommitteeName').val())==""){
                        $('#errmsg').html("Please enter Committe Name.");
                        $('#errmsg').css("color", "red");
                        return false;
                    }else{
                        if(($.trim($('#txtcommitteeName').val()).length)>100){
                            $('#errmsg').html("Maximum 100 Characters are allowed.");
                            $('#errmsg').css("color", "red");
                            return false;
                        }else{
                            $('#errmsg').html(null);
                        }
                    }    
                });
            });
            $(function() {
                // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
                $( "#dialog:ui-dialog" ).dialog( "destroy" );
                $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    resizable:false,
                    draggable:true,
                    height: 500,
                    width: 600,
                    modal: true,
                    buttons: {
                        "Add": function() {
                            var memcnt=0;
                            $(function() {
                                //if(eval(($(":checkbox[checked='true']").length)+($('#members').children()[1].children.length))<=$('#maxmem').val()){
                                var len=$('#members').children()[1].children.length;
                                var i =len;
                                $(":checkbox[checked='true']").each(function(){
                                    memcnt++;
                                    var a = $(this).attr("id");
                                    var b = a.substring(3, a.length);
                                    var addUser=true;
                                    for(var j=0;j<len;j++){
                                        if($('#memid'+j).val()+"_"+$('#memgovid'+j).val()==$("#chk"+b).val()){
                                            addUser=false;
                                            break;
                                        }
                                    }
                                    if(addUser){
                                            var temp = $("#chk"+b).val();
                                            var uId = temp.split("_", temp.length)[0];
                                            var gId = temp.split("_", temp.length)[1];
                                        $( "#members tbody" ).append("<tr>"+
                                            "<td class='t-align-left'><input id='memgovid"+i+"' type='hidden' name='memGovIds' value='"+gId+"'/><input id='memid"+i+"' type='hidden' name='memUserIds' value='"+uId+"'/>"+$("#spn"+b).val()+"</td>"+
                                            "<td class='t-align-left'>"+
                                            "<select name='memberRoles' class='formTxtBox_1' id='cmbMemRole"+i+"' style='width:200px;'>"+
                                            "<option selected='selected' value='cp'>Chairperson</option>"+
                                            /*"<option value='ms'>Member Secretary</option>"+*/
                                            "<option value='m'>Member</option>"+
                                            "</select>"+
                                            "</td>"+
                                            "<td class='t-align-center'><input id='memfrm"+i+"' type='hidden' name='memberFroms' value='"+$("#pestat"+b).val()+"'/>"+$("#pestat"+b).val()+"</td>"+
                                            "<td class='t-align-center'><a id='delRow"+i+"' class='action-button-delete' onclick='delRow(this)'>Remove</a></td>"+
                                            "</tr>");
                                        i++;
                                    }
                                    else{
                                             jAlert("Member already added in the committee.","Same member alert", function(RetVal) {
                                             });
                                    }
                                });
            <%--}else{
                jAlert("Maximum "+$('#maxmem').val()+" members.","Max member alert", function(RetVal) {
                });
            }--%>
                                });
                               if(memcnt==0){
                                    jAlert("Please select the member.","Evaluation member alert", function(RetVal) {
                                                                });
                               }else{
                                $( this ).dialog( "close" );
                               }
                            },
                            Cancel: function() {
                                $( this ).dialog( "close" );
                            }
                        },
                        close: function() {
                        }
                    });

                    $("#addmem" ).click(function(){
                        $("#dialog-form").dialog("open");
                        $("input[type='checkbox']").removeAttr("checked");
                    });
                });
        </script>
    </head>
    <body>
        <%
                    boolean isEdit = false;
                    int tenderId = 0;
                    int j = 0;
                    if (request.getParameter("tenderid") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderid"));
                    }
                    Map<String,String> userTransMap = new HashMap<String, String>();
                    tenderCommitteSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    if ("Submit".equals(request.getParameter("submit"))) {
                        tenderCommitteDtBean.setCommitteeName(request.getParameter("committeeName"));
                        tenderCommitteDtBean.setCommitteeType(request.getParameter("committeeType"));
                        tenderCommitteDtBean.setMaxMembers(request.getParameter("maxMembers"));
                        tenderCommitteDtBean.setMemUserIds(request.getParameterValues("memUserIds"));
                        tenderCommitteDtBean.setMemGovIds(request.getParameterValues("memGovIds"));
                        tenderCommitteDtBean.setMemberFroms(request.getParameterValues("memberFroms"));
                        tenderCommitteDtBean.setMemberRoles(request.getParameterValues("memberRoles"));
                        tenderCommitteDtBean.setMinMemFromPe("0");
                        tenderCommitteDtBean.setMinMemOutSide("0");
                        tenderCommitteDtBean.setMinMembers(request.getParameter("minMembers"));
                        tenderCommitteDtBean.setTenderid(request.getParameter("tenderid"));
                        tenderCommitteDtBean.setUserid(request.getSession().getAttribute("userId").toString());
                        tenderCommitteDtBean.setCommitteeId(request.getParameter("committeeId"));
                        if ("true".equals(request.getParameter("isedit"))) {
                            tenderCommitteSrBean.createUpdateCommitte(tenderCommitteDtBean, true);
                        } else {
                            tenderCommitteSrBean.createUpdateCommitte(tenderCommitteDtBean, false);
                        }

                        tenderId = Integer.parseInt(request.getParameter("tenderid"));
                        response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&tsedit=y");
                    }else if ("Publish".equals(request.getParameter("publish"))) {
                            tenderCommitteSrBean.publishComm(request.getParameter("committeeId"),request.getParameter("remarks"),""+tenderId,"tsc");
                            //response.sendRedirect("EvalCommTSC.jsp?tenderid=" + tenderId + "&ispub=y");
                            response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&action=add&msgId=tscpublished");
                     }
                    else {
                                        if (request.getParameter("tenderid") != null) {
                                            tenderId = Integer.parseInt(request.getParameter("tenderid"));
                                        }
                                        int cnt = 0;
                                        java.util.List<OfficeMemberDtBean> officeMemberDtBeans = tenderCommitteSrBean.findOfficeMember(Integer.parseInt(request.getParameter("tenderid")), "samepetsc");
                                        CommitteDtBean bean = tenderCommitteSrBean.findCommitteDetails(Integer.parseInt(request.getParameter("tenderid")), "tsccom");
                                        if ("y".equals(request.getParameter("isedit")) || "y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub"))) {
                                            isEdit = true;
                                        }
                                        java.util.List<CommitteMemDtBean> comMemberDtBeans = new java.util.ArrayList<CommitteMemDtBean>();
                                        if (isEdit) {
                                            comMemberDtBeans = tenderCommitteSrBean.committeUser(Integer.parseInt(request.getParameter("tenderid")), "TSCMember");
                                        }
                                         if(!("y".equalsIgnoreCase(request.getParameter("ispub"))|| "y".equals(request.getParameter("isview")))){
                %>
          <div id="dialog-form" title="Add Committee Member">
            <%--<form>--%>
            <fieldset>
                <table width="100%" cellspacing="0">
                    <tr>
                        <td colspan="4">
                            <ul class="tabPanel_1">                                                                
                                <li><a href="javascript:void(0);" class="sMenu" id="spe">Same PE</a></li>
                                <li><a href="javascript:void(0);" id="ope">Other PE</a></li>
                                <li><a href="javascript:void(0);" id="icm">External Member</a></li>
                            </ul>
                            <div class="tabPanelArea_1">

                                <%--Same PE--%>

                                <div id="pe1" style="display: block;">
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th width="10%" class="t-align-center">Select</th>
                                            <th class="t-align-center" width="30%">Member Name</th>
                                            <th class="t-align-center" width="30%">Member Designation</th>
                                            <th class="t-align-center" width="30%">Procurement Role</th>
                                        </tr>
                                        <%
                                        if(officeMemberDtBeans != null && officeMemberDtBeans.size() > 0)
                                        {
                                        for (OfficeMemberDtBean omdb : officeMemberDtBeans) {
                                            if(!((omdb.getUserid()==Integer.parseInt(session.getAttribute("userId").toString())) && omdb.getProcureRole().contains("PE"))){%>
                                        <tr>
                                            <td class="t-align-center">
                                                <label><input id="chk<%=cnt%>" type="checkbox" value="<%=omdb.getUserid()%>_<%=omdb.getGovUserId()%>" /></label></td>
                                            <td class="t-align-center"><input type="hidden" id="spn<%=cnt%>" value="<%=omdb.getEmpName()%>"/><%=omdb.getEmpName()%></td>
                                            <td class="t-align-center"><input type="hidden" id="pestat<%=cnt%>" value="Same PE"/><%=omdb.getDesgName()%></td>
                                            <td class="t-align-center"><%=omdb.getProcureRole()%></td>
                                        </tr>
                                        <%cnt++;
                                                                }}
                                    }
                                    else
                                    {
                                    %>
                                           <tr>
                                               <td class="t-align-center" colspan="4">
                                                    <label>
                                                        <font color='red'><b>No Record Found</b></font>
                                                    </label>
                                                </td>
                                           </tr>

                                    <%
                                    }
                                    %>
                                    </table>
                                </div>

                                <%--Other PE Search--%>

                                <div  id="pe2" style="display: none;">
                                    <input type="hidden" id="pe1count" value="<%=officeMemberDtBeans.size()%>">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">
                                        <tr>
                                            <td width="26%" class="ff t-align-left">Organisation Name :</td>
                                            <td width="74%" class="ff t-align-left">
                                                <select id="cmborg" style="width: 200px;" class="formTxtBox_1">
                                                    <%
                                                                            for (SelectItem orgList : tenderCommitteSrBean.getDeptList("departmentType", Operation_enum.EQ, "Organization")) {
                                                    %>
                                                    <option value="<%=orgList.getObjectId()%>"><%=orgList.getObjectValue()%></option>
                                                    <%}%>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="26%" class="ff t-align-left">PE Office :</td>
                                            <td width="74%" class="ff t-align-left">
                                                <select id="cmboffice" style="width: 200px;" class="formTxtBox_1">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="26%" class="t-align-left">&nbsp;</td>
                                            <td width="74%" class="t-align-left">
                                                <label class="formBtn_1">
                                                    <input type="button" id="btnSearch" value="Search"/>
                                                </label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th width="10%" class="t-align-center">Select</th>
                                            <th class="t-align-center" width="30%">Member Name</th>
                                            <th class="t-align-center" width="30%">Member Designation</th>
                                            <th class="t-align-center" width="30%">Procurement Role</th>
                                        </tr>
                                        <tbody id="tbodype">

                                        </tbody>
                                    </table>
                                </div>

                                <%--Indi Details--%>
                                <div  id="pe3" style="display: none;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">                                        
                                        <tr>
                                            <td class="ff t-align-left" width="14%">e-mail ID :</td>
                                            <td  class="t-align-left" width="86%">
                                                <input type="text" class="formTxtBox_1" id="txtEmailid" style="width:200px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="14%" class="t-align-left">&nbsp;</td>
                                            <td width="86%" class="t-align-left">
                                                <label class="formBtn_1">
                                                    <input type="button" id="btnIndiSearch" value="Search"/>
                                                </label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th width="13%" class="t-align-center">Select</th>
                                            <th class="t-align-center" width="40%">Member Name</th>
                                            <th class="t-align-center" width="47%">Member e-mail ID</th>
                                        </tr>
                                        <tbody id="tbodype1">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <%--</form>--%>
        </div>
        <%}%>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">
        <div class="pageHead_1">Technical Sub Committee Details<span style="float: right;"><a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=tenderId%>">Go back</a></span></div>
          <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
         %>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <%--<%@include file="officerTabPanel.jsp"%>--%>
        <div id="users-contain" class="ui-widget">
             <div class="tabPanelArea_1">
            <form action="TSCFormation.jsp" method="post" id="frmComm">               
                                  
                    <input type="hidden" value="<%=tenderId%>" name="tenderid" id="hdnTenderId"/>
                    <input type="hidden" value="<%=isEdit%>" name="isedit"/>                    
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <%--<tr>
                            <td width="26%" colspan="2" class="t-align-left ff">Evaluation Committee</td>
                        </tr>--%>
                        <tr>
                            <td width="26%" class="t-align-left ff">Committee Name :
                            <%if(!"y".equals(request.getParameter("isview")) && !"y".equals(request.getParameter("ispub"))){%>
                                <span class="mandatory">*</span>
                                <%}%>
                            </td>
                            <td width="74%" class="t-align-left">
                                <label>
                                    <%if("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub"))){
                                            out.print(comMemberDtBeans.get(0).getCommName());
                                    }else{%>
                                        <input name="committeeName" type="text" class="formTxtBox_1" id="txtcommitteeName" style="width:400px;" value="<%if (isEdit) {%><%=comMemberDtBeans.get(0).getCommName()%><%}%>" />
                                    <%}%>
                                </label>
                                <input name="committeeId" type="hidden" class="formTxtBox_1" value="<%if (isEdit) {%><%=comMemberDtBeans.get(0).getCommId()%><%} else {%>0<%}%>"/>
                                <div class='reqF_1' id="errmsg"></div>
                            </td>
                        </tr>
                        <%if(!"y".equals(request.getParameter("isview"))){%>
                        <tr>
                            <td class="t-align-left ff">Minimum Members Required :</td>
                            <td class="t-align-left"><input type="hidden" name="minMembers" id="minmem" value="<%=bean.getMinMemReq()%>"/><%=bean.getMinMemReq()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Maximum Members Required :</td>
                            <td class="t-align-left"><input type="hidden" name="maxMembers" id="maxmem" value="<%=bean.getMaxMemReq()%>"/><%=bean.getMaxMemReq()%></td>
                        </tr>
                        <%}%>
                    </table>
                    <input name="committeeType" type="hidden" id="comType" value="<%=bean.getpNature()%>"/>
                    <div>&nbsp;</div>
                    <%if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){%><div class="t-align-center"><a id="addmem" class="action-button-add">Add Members</a></div><%}%>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="members">
                        <thead>
                            <tr>
                                <th width="40%" class="t-align-center ff">Members Name</th>
                                <th width="30%" class="t-align-center ff">Committee Role<%-- <%if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){%><span class="mandatory">*</span><%}%>--%></th>
                                <th width="17%" class="t-align-center ff">Members From</th>
                                <%if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){%><th width="13%" class="t-align-center ff">Action</th><%}%>
                            </tr>
                        </thead>
                        <tbody>

                            <%if (isEdit) {
                                int memCnt=0;
                                for (CommitteMemDtBean cmdb : comMemberDtBeans) {
                                    userTransMap.put(String.valueOf(cmdb.getUserId()), "3");
                                    memCnt++;
                            %>
                            <tr
                                 <%if(Math.IEEEremainder(memCnt,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
                                >
                                <td class="t-align-left">
                                    <input id="memgovid<%=j%>" type="hidden" name="memGovIds" value="<%=cmdb.getGovUserId()%>"/>
                                    <input id="memid<%=j%>" type="hidden" name="memUserIds" value="<%=cmdb.getUserId()%>"/><%=cmdb.getEmpName()%></td>
                                <td class="t-align-left">
                                     <%if("y".equals(request.getParameter("isview"))||"y".equals(request.getParameter("ispub"))){
                                        if (cmdb.getMemRole().equals("cp")) {
                                            out.print("Chairperson");
                                        }/*else if (cmdb.getMemRole().equals("ms")) {
                                            out.print("Member Secretary");
                                        }*/else if (cmdb.getMemRole().equals("m")) {
                                            out.print("Member");
                                        }
                                    }else{%>
                                    <select name="memberRoles" class="formTxtBox_1" id="cmbMemRole<%=j%>" style="width:200px;">
                                        <option <%if (cmdb.getMemRole().equals("cp")) {%>selected<%}%> value="cp">Chairperson</option>
                                        <!--<option <%//if (cmdb.getMemRole().equals("ms")) {%>selected<%//}%> value="ms">Member Secretary</option>-->
                                        <option <%if (cmdb.getMemRole().equals("m")) {%>selected<%}%> value="m">Member</option>
                                    </select>
                                    <%}%>
                                </td>
                                <td class="t-align-center"><input id="memfrm<%=j%>" type="hidden" name="memberFroms" value="<%=cmdb.getMemFrom()%>"/><%=cmdb.getMemFrom()%></td>
                                <%if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){%><td class="t-align-center"><a id="delRow<%=j%>" class="action-button-delete" onclick="delRow(this)">Remove</a></td><%}%>
                            </tr>
                            <%j++;
                                                            }
                                                        }%>
                        </tbody>
                    </table>
                    <div>&nbsp;</div>
                       <%if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){%>
                    <div class="t-align-center">
                        <label class="formBtn_1">
                            <input name="submit" type="submit" value="Submit" id="btnSubmit"/>
                        </label>
                    </div>
                    <%}if("y".equals(request.getParameter("ispub"))){%>
                    <table>
                        <tr>
                            <td>Remarks <span class="mandatory">*</span>: </td>
                            <td>
                                <textarea name="remarks" rows="3" class="formTxtBox_1" id="txtaRemarks" style="width:400px;"></textarea>
                                <div id="divRem" style="color: red;"></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <div class="t-align-center">
                        <label class="formBtn_1">
                            <input name="publish" type="submit" value="Publish" id="btnPublish"/>
                        </label>
                    </div>
                    <%}%>
                    
                
            </form>
                    <%if(request.getParameter("isaa")!=null && request.getParameter("isaa").equalsIgnoreCase("yes")){%>
                    <jsp:useBean id="tscApptoAaSrBean" class="com.cptu.egp.eps.web.servicebean.TscApptoAaSrBean" scope="request" />
                    <%
                                String status = tscApptoAaSrBean.getStatus(tenderId);
                               // if(status.equalsIgnoreCase("pending")){
                    %>
                    <form name="tscAa" id="tscAa" action="<%=request.getContextPath()%>/EvalTscAppToAa" method="post">
                    <div class="t-align-center t_space">
                        <input type="hidden" name="tenderId" value="<%=tenderId%>">
                        <input type="hidden" name="action" value="update">
                        <label class="formBtn_1">
                            <input name="Approve" type="submit" value="Approve" />
                        </label>&nbsp;&nbsp;
                        <label class="formBtn_1">
                            <input name="Reject" type="submit" value="Reject" />
                        </label>
                    </div>
                    </form>
                    <%
                       // }
                                }
                     %>
                      <%
                        if(isEdit){
                        List<CommitteDtBean> hisBean = tenderCommitteSrBean.findCommitteDetailsHistory(Integer.parseInt(request.getParameter("tenderid")), "TsccomHistory");
                        int kComm=1;
                        for(CommitteDtBean dtBean : hisBean){
                        comMemberDtBeans.clear();
                        comMemberDtBeans = tenderCommitteSrBean.committeUserHistory(Integer.parseInt(request.getParameter("tenderid")), "TSCMemberHistory",dtBean.getCommitteeId());
                    %>
                  <br/><div style="text-align: center; background-color: #e5dfec; font-weight: bold;height: 18px; padding-top: 5px">Committee History <%=kComm %></div>
                  <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td width="27%" class="t-align-left ff">Committee Name : </td>
                            <td width="73%" class="t-align-left">
                                <label><%=comMemberDtBeans.get(0).getCommName()%></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Minimum Members Required :</td>
                            <td class="t-align-left"><input type="hidden" name="minMembers" id="minmem" value="<%=dtBean.getMinMemReq()%>"/><%=dtBean.getMinMemReq()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Maximum Members required :</td>
                            <td class="t-align-left"><input type="hidden" name="maxMembers" id="maxmem" value="<%=dtBean.getMaxMemReq()%>"/><%=dtBean.getMaxMemReq()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Minimum Members from the Same PE :</td>
                            <td class="t-align-left"><input name="minMemFromPe" type="hidden" id="mininmem" value="<%=dtBean.getMinMemFromPe()%>"/><%=dtBean.getMinMemFromPe()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Minimum Members Outside PE :</td>
                            <td class="t-align-left" id="outmem"><input name="minMemOutSide" type="hidden" id="minoutmem" value="<%=dtBean.getMinMemOutSidePe()%>"/><%=dtBean.getMinMemOutSidePe()%></td>
                        </tr>
                    </table>
                  <div>&nbsp;</div>
                  <table width="100%" cellspacing="0" class="tableList_1 t_space" id="members">
                        <thead>
                            <tr>
                                <th width="17%" class="t-align-left ff">Committee Member's Name</th>
                                <th width="53%" class="t-align-left ff">Committee Role </th>
                                <th width="17%" class="t-align-left ff">Members From</th>
                            </tr>
                        </thead>
                        <tbody>

                            <%
                                for(CommitteMemDtBean cmdb : comMemberDtBeans){
                            %>
                            <tr>
                                <td class="t-align-left">
                                    <input id="memgovid<%=j%>" type="hidden" name="memGovIds" value="<%=cmdb.getGovUserId()%>"/>
                                    <input id="memid<%=j%>" type="hidden" name="memUserIds" value="<%=cmdb.getUserId()%>"/><%=cmdb.getEmpName()%></td>
                                <td class="t-align-left">
                                     <%
                                        if (cmdb.getMemRole().equals("cp")) {
                                            out.print("Chairperson");
                                        }/*else if (cmdb.getMemRole().equals("ms")) {
                                            out.print("Member Secretary");
                                        }*/else if (cmdb.getMemRole().equals("m")) {
                                            out.print("Member");
                                        }
                                    %>
                               </td>
                               <td class="t-align-left"><input id="memfrm<%=j%>" type="hidden" name="memberFroms" value="<%=cmdb.getMemFrom()%>"/><%=cmdb.getMemFrom()%></td>
                           </tr>
                                <%j++;}%>
                        </tbody>
                    </table>
        </div>
        <%kComm++;}}if("y".equalsIgnoreCase(request.getParameter("isview"))) 
            {
                request.setAttribute("comMemberDtBeans", comMemberDtBeans);
                request.setAttribute("listEmpTras", userTransMap);
    %>
            <jsp:include page="../resources/common/EmpTransferHist.jsp" />
            <jsp:include page="../resources/common/OpeningComCommentsInclude.jsp">
          
            <jsp:param name="tenderId" value="<%=tenderId %>" />
            <jsp:param name="comType" value="3" />
        </jsp:include>
            <%}%>
                    </div>
        </div>        
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%
                    }
        %>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
