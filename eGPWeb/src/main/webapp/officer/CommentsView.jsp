<%-- 
    Document   : CommentsView
    Created on : Dec 12, 2010, 1:49:01 PM
    Author     : test
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowFileHistory"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Comments</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <style>
            ul li{
                margin-left: 15px;
            }
        </style>

        
    </head>
    <body onload="getDocData()">
        <%
            String hisid = "";
            if (request.getParameter("wfhistoryid") != null) {
                hisid = request.getParameter("wfhistoryid");
            }
            
            int uid = 0;
            if(session.getAttribute("userId") != null){
               Integer ob1 = (Integer)session.getAttribute("userId");
                uid = ob1.intValue();
            }

            String comments = "";
            int objectId = 0;
            short activityId = 0;
            int childId = 0;
            int userId = 0;

            WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
            List<TblWorkFlowFileHistory> tblWorkFlowFileHistory = null;
            if (hisid != null) {
                tblWorkFlowFileHistory = workFlowSrBean.getWfFileHistoryById(Integer.parseInt(hisid));
                if (tblWorkFlowFileHistory != null && tblWorkFlowFileHistory.size() > 0) {
                    TblWorkFlowFileHistory fileHistory = tblWorkFlowFileHistory.get(0);
                    comments = fileHistory.getRemarks();
                    objectId = fileHistory.getObjectId();
                    activityId = fileHistory.getActivityId();
                    childId = fileHistory.getChildId();
                    userId = fileHistory.getFromUserId();
                }
            }
        %>
        <div class="contentArea_1">
            <div class="txt_1 c_t_space" >
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <th class="t-align-left">Comments</th>
                    </tr>
                    <tr>
                        <td class="t-align-left">
                            <% if(comments.contains("<ol>")){ %>
                            <div style="padding-left: 30px;">
                                <%  if (!"".equals(comments)) {
                                        out.print(comments);
                                    } else {
                                        out.print("No Comments added");
                                    }
                                %>
                            </div>
                            <% }else{ %>
                            <div>
                                <% if (!"".equals(comments)) {
                                                out.print(comments);
                                            } else {
                                                out.print("No Comments added");
                                            }
                                %>
                            </div>
                            <% } %>
                            
                        </td>
                    </tr>
                    <tr>
                        <th class="t-align-left">Uploaded Documents</th>
                    </tr>
                    <tr>
                        <td valign="top">
                            <div id="docData">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
    <script type="text/javascript">
      function getDocData(){
          $.post("<%=request.getContextPath()%>/workflowdocservlet", {action:'gethistorydocs',objectid:<%=objectId%>,activityid:<%=activityId%>,childid:<%=childId%>,userid:<%=userId%>}, function(j){
                 if($.trim(j)!=""){
                     document.getElementById("docData").innerHTML = j;
                 }else{
                     document.getElementById("docData").innerHTML ="<span style='color:red;text-align:center;'>No Records found</span>";
                 }
                 
          });
      }
    </script>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
