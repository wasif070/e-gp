<%--
    Document   : BidFormTable
    Created on : Nov 18, 2010, 3:08:48 PM
    Author     : Sanjay
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="java.util.ListIterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

    <table width="100%" cellspacing="0" class="tableList_1" id="FormMatrix">
    <jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />

<%
    int userId = 0;
    /*
    if(session.getAttribute("userId") != null) {
        if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
            //userId = Integer.parseInt(session.getAttribute("userId").toString());
        }
    }
    */
    if(request.getParameter("uId")!=null && !"".equalsIgnoreCase(request.getParameter("uId"))){
        userId = Integer.parseInt(request.getParameter("uId"));
    }
    
    int userTypeId = 0;
    if(session.getAttribute("userTypeId") != null) {
        if(!"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())) {
            userTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
        }
    }
%>
<%
    int isTotal = 0;
    int FormulaCount = 0;
    int cols = 0;
    int rows = 0;
    int tableId = 0;
    int showOrHide = 0;

    int arrFormulaColid[] = null;
    int arrWordFormulaColid[] = null;
    int arrOriValColId[] = null;

    short columnId = 0;
    short filledBy = 0;
    short dataType = 0;

    String colHeader = "";
    String colType = "";
    String tableIndex = "1";
    String inputtype = "";
    String textAreaValue = "";

%>
<%
    if(request.getParameter("tableId")!=null && !"".equalsIgnoreCase(request.getParameter("tableId"))){
        tableId = Integer.parseInt(request.getParameter("tableId"));
    }
    if(request.getParameter("cols")!=null && !"".equalsIgnoreCase(request.getParameter("cols"))){
        cols = Integer.parseInt(request.getParameter("cols"));
    }

    if(request.getParameter("rows")!=null && !"".equalsIgnoreCase(request.getParameter("rows"))){
        rows = Integer.parseInt(request.getParameter("rows"));
    }

    if(request.getParameter("TableIndex")!=null && !"".equalsIgnoreCase(request.getParameter("TableIndex"))){
        tableIndex = request.getParameter("TableIndex");
    }

    int bidId = 0;
    if (request.getParameter("bidId") != null) {
        bidId = Integer.parseInt(request.getParameter("bidId"));
    }

    String action = "";
    if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action"))){
         action = request.getParameter("action");
    }

    boolean isEdit = false;
    if("Edit".equalsIgnoreCase(action)){
        isEdit = true;
    }

    boolean isView = false;
    if("View".equalsIgnoreCase(action)){
        isView = true;
    }

    boolean isEncrypt = false;
    if("Encrypt".equalsIgnoreCase(action)){
        isEncrypt = true;
    }

    boolean isMultiTable = false;
    if(request.getParameter("isMultiTable")!=null && !"".equalsIgnoreCase(request.getParameter("isMultiTable"))){
        if("yes".equalsIgnoreCase(request.getParameter("isMultiTable"))){
            isMultiTable = true;
        }
    }

    
%>
<tbody id="MainTable<%=tableId%>">
<input type="hidden" name="tableCount<%=tableId%>" value="1"  id="tableCount<%=tableId%>">
<input type="hidden" name="originalTableCount<%=tableId%>" value="1"  id="originalTableCount<%=tableId%>">

<script>
        var verified = true;
        var lbl_ids=new Array();
        var lbl_countid=0;
</script>
<%
        short fillBy[] = new short[cols];

        arrFormulaColid = new int[cols];
        arrWordFormulaColid = new int[cols];
        arrOriValColId = new int[cols];
        for(int i=0;i<arrFormulaColid.length;i++)
        {
                arrFormulaColid[i] = 0;
                arrWordFormulaColid[i] = 0;
                arrOriValColId[i] = 0;
        }

        ListIterator<CommonFormData> tblColumnsDtl = tenderBidSrBean.getNegTableColumns(tableId,bidId, true).listIterator();
        ListIterator<CommonFormData> tblCellsDtl = tenderBidSrBean.getTableCells(tableId).listIterator();
        ListIterator<CommonFormData> tblFormulaDtl = tenderBidSrBean.getTableFormulas(tableId).listIterator();

        ListIterator<CommonFormData> tblBidTableId = null;
        ListIterator<CommonFormData> tblBidData = null;
        ListIterator<CommonFormData> tblBidCellData = null;

        int cntRow = 0;
        int TotalBidCount=0;
        int editedRowCount = 0;
        StringBuilder bidTableIds = new StringBuilder();

        if(isEdit || isView || isEncrypt){
            tblBidTableId = tenderBidSrBean.getBidTableId(bidId, tableId,userId).listIterator();
            tblBidData = tenderBidSrBean.getNegBidData(bidId,userId).listIterator();

            if(isMultiTable){
                //tblCellsDtl = tenderBidSrBean.getBidCellData(tableId, userId, bidId).listIterator();
            }

            while(tblBidTableId.hasNext()){
                //tblBidTableId.next();
                CommonFormData bidTableIdDt = tblBidTableId.next();
                bidTableIds.append(bidTableIdDt.getBidTableId() + " , ");
                cntRow++;
            }

            

            if(isEncrypt){
%>
            <input type="hidden" id="hdnBidTableIds" name="hdnBidTableIds" value="<%=bidTableIds%>">
<%
            }
%>
            <script>
                arrBidCount[gblCnt++]='<%=cntRow%>';
            </script>
<%

            if(tblBidTableId.hasPrevious())
                tblBidTableId.previous();

                if(tblBidTableId.hasNext()){
                    tblBidTableId.next();
                    if(rows > 1 && isTotal == 1){
                        editedRowCount = (rows*cntRow) - cntRow;
                    }else{
                        editedRowCount = (rows*cntRow);
                    }
                }
                TotalBidCount= cntRow;
         }else{
            cntRow = 1;
         }
        
%>
        <script>
            var rowcount = parseInt("<%=rows%>")*parseInt("<%=cntRow%>");
            arrCompType[<%=tableIndex%>] = new Array(rowcount);
            arrCellData[<%=tableIndex%>] = new Array(rowcount);
            arrDataTypesforCell[<%=tableIndex%>] = new Array(rowcount);
            <%if(isEdit || isView || isEncrypt){%>
            totalBidTable[<%=tableIndex%>] = '<%=cntRow%>';
            <%}%>
            for (i=1;i<=rowcount;i++)
            {
                arrCompType[<%=tableIndex%>][i]=new Array(<%=cols%>);
                arrCellData[<%=tableIndex%>][i]=new Array(<%=cols%>);
                arrDataTypesforCell[<%=tableIndex%>][i]=new Array(<%=cols%>);
            }

            if(rowcount == 1)
            {
                    i = <%=cols%>;
            }
        </script>
        <%if(isEdit || isView || isEncrypt){%>
            <script>
		document.getElementById("tableCount<%=tableId%>").value = "<%=cntRow%>";
		document.getElementById("originalTableCount<%=tableId%>").value = "<%=cntRow%>";
                //arrBidCount.push(document.getElementById("tableCount"+<%//=tableId%>).value);
            </script>
            <input type="hidden" value="<%=editedRowCount%>" name="eRowCount<%=tableId%>" id="editRowCount">
<%
                while(tblBidTableId.hasPrevious()){
                    tblBidTableId.previous();
                }
        }

        if(true){
            int cnt = 0;
            int counter = 0;
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                if(formulaData.getFormula().contains("TOTAL")){
                    isTotal = 1;
                    arrFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
%>
                    <script>
                        isColTotalforTable[<%=tableIndex%>] = 1;
                        for(var i=1;i<=<%=cols%>;i++){
                            if(i == <%=formulaData.getColumnId()%>){
                                arrColTotalIds[<%=tableIndex%>][i-1] = '<%=formulaData.getColumnId()%>';
                            }
                        }
                    </script>
<%
                }
                cnt++;
                FormulaCount = cnt;
            }
%>
            <script>
                    var totalWordArr = new Array();
                    var q = 0;
            </script>
<%
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }

            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                for(int x=0;x<arrFormulaColid.length;x++){
                    if(formulaData.getFormula().contains("WORD("+arrFormulaColid[x]+")")){
%>
                    <script>
                        var TotalWordColId = "<%=formulaData.getColumnId()%>";
                        totalWordArr[q] = "<%=formulaData.getColumnId()%>";
                        q++;
                        arrColTotalWordsIds[<%=tableIndex%>][<%=x%>] = '<%=formulaData.getColumnId()%>';
                    </script>
<%
			arrWordFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                    }
                }
            }
%>
            <script>
                arrTableFormula[<%=tableIndex%>] = new Array(<%=FormulaCount%>); // Stores Formula
                arrFormulaFor[<%=tableIndex%>] = new Array(<%=FormulaCount%>); // Stores where to Apply Formula
                arrIds[<%=tableIndex%>] = new Array(<%=FormulaCount%>);
            </script>
<%
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
%>
		<script>
                    arrTableFormula[<%=tableIndex%>][<%=counter%>] = '<%=formulaData.getFormula()%>';
                    arrFormulaFor[<%=tableIndex%>][<%=counter%>] = '<%=formulaData.getColumnId()%>';
		</script>
<%
                counter++;
            }
%>
            <script language="JavaScript" >
                arrColIds[<%=tableIndex%>] =new Array();
                arrStaticColIds[<%=tableIndex%>] =new Array();
                arrForLabelDisp[<%=tableIndex%>] = new Array();
            </script>
<%
        }else{
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                if(formulaData.getFormula().contains("TOTAL")){
                    isTotal = 1;
                    arrFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                }
            }
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                CommonFormData formulaData = tblFormulaDtl.next();
                for(int x=0;x<arrFormulaColid.length;x++){
                    if(formulaData.getFormula().contains("WORD("+arrFormulaColid[x]+")")){
			arrWordFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                    }
                }
            }
        }

        if(isEdit || isView || isEncrypt){
            if(isMultiTable){
                //rows = editedRowCount;
                while(tblBidTableId.hasPrevious())
                {
                    tblBidTableId.previous();
                }
            }
        }

        if(!(isEdit || isView || isEncrypt)) // Form Add Start
        {
            for (short i = -1; i <= rows; i++) {
                if(i == 0){
%>
             <tr id="ColumnRow">
<%
                    for(short j=0;j<cols;j++){

                        if(tblColumnsDtl.hasNext()){
                            CommonFormData colData = tblColumnsDtl.next();

                            colHeader = colData.getColumnHeader();
                            colType = colData.getColumnType();
                            filledBy = colData.getFillBy();
                            showOrHide = Integer.parseInt(colData.getShowOrHide());
                            dataType = colData.getDataType();
                            
                            colData = null;
                        }

                        fillBy[j] = filledBy;
%>
                <th id="addTD<%= j + 1 %>">
                    <%= colHeader %>
                    <%if(showOrHide==2){%>
                    <script>
                        document.getElementById("addTD<%= j + 1 %>").visibility = "collapse";
                    </script>
                    <%}%>
                </th>
<%
                    }
%>
             </tr>
<%
                }
                if(i > 0){
%>
             <tr id="row<%=tableId%>_<%=i%>">
<%
                    int cnt = 0;
                    for(int j=0; j<cols; j++){
                        String cellValue = "";
%>
                <td id="td<%=tableId%>_<%=i%>_<%=columnId%>" align="center"
                <%if(showOrHide==2){%>
                    <script>
                        document.getElementById("td<%=tableId%>_<%=i%>_<%=columnId%>").visibility = "collapse";
                    </script>
                <%}%>
                >
<%
                        if(tblCellsDtl.hasNext()){
                            cnt++;
                            CommonFormData cellData = tblCellsDtl.next();
                            dataType = cellData.getCellDataType();
                            filledBy = cellData.getCellDataType();
                            //filledBy = cellData.getFillBy();
                            cellValue = cellData.getCellValue();
                            columnId = cellData.getColumnId();


                            if(true){
                                String cValue = "";
                                cValue = cellValue.replaceAll("\r\n", "");
%>
                            <script>
                                arrCellData[<%=tableIndex%>][<%=i%>][<%=columnId%>] = '<%=cValue.trim()%>';
                                arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = '';
                             </script>
<%
                            }
                            if(fillBy[j] == 1){

%>
                                    <label  name="lbl@<%=tableId%>_<%=i%>_<%=columnId%>" id="lbl@<%=tableId%>_<%=i%>_<%=columnId%>"
                                                <%if(dataType==3 || dataType==4 || dataType==8){%>
                                                value="<%=cellValue%>"
                                                <%}else{%>
                                                value=""
                                                <%}%>
                                                >
                                                <%=cellValue%>
                                        </label>
                                <script>
                                    lbl_ids[lbl_countid]="lbl@<%=tableId%>_<%=i%>_<%=columnId%>";
                                    lbl_countid++;
				</script>
<%
                                if(true){
%>
                                <script>
                                    arrStaticColIds[<%=tableIndex%>].push('<%=columnId%>');
				</script>
<%
                               }
                            } else if(fillBy[j] == 2 || fillBy[j] == 3){
                                if(dataType != 2){
                                    if(i == (rows)){
                                        if(FormulaCount>0){
                                            if(isTotal == 1){
                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    if(arrWordFormulaColid[columnId-1] == columnId){
                                                        if(fillBy[j] == 3 && dataType == 3){
%>
                                                        <label
								readonly
								tabindex="-1"
								name="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>"
								id="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>"
                                                                title="label" value="<%=textAreaValue%>">
                                                        </label>
                                                        <!--<textarea style="display:none" class="formTxtBox_1"
                                                                readonly
								tabindex="-1"
								cols="30"
								name="row<%=tableId%>_<%=i%>_<%=columnId%>"
								id="row<%=tableId%>_<%=i%>_<%=columnId%>"
							><%//=textAreaValue%></textarea>-->
<%
                                                        }else {
%>
                                                        <textarea class="formTxtBox_1"
                                                            readonly
                                                            tabindex="-1"
                                                            cols="30"
                                                            name="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                        ><%=textAreaValue%></textarea>
<%
                                                        }
                                                    }
                                                    out.print("</td>");
                                                    continue;
                                                 }else{
                                                    inputtype = "text";
                                                 }
                                            }
                                        }
                                    }

                                    if(i == (rows-1) && isTotal == 1){
                                        //out.print("<b>GenerateViewFormula <b>");
                                    }

                                    //out.print("FillBy : "+fillBy[j]);
                                    //out.print("  Datatype : "+dataType);
                                   if(i != (rows))
                                   {
%>
                                       <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                        if(fillBy[j] != 3){
                                            if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this);"
<%
                                            }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this);"
<%
                                            } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this);"
<%
                                            }
                                        }
%>
                                        />
<%
                                   }
                                   else if(i == (rows))
                                   {
                                        if(FormulaCount > 0)
                                        {
                                            if(isTotal == 1)
                                            {
                                                if(arrFormulaColid[columnId-1] == columnId)
                                                {
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                                    if(fillBy[j] != 3){
                                                        if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this);"
<%
                                                        }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this);"
<%
                                                        } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this);"
<%
                                                        }
                                                    }
%>
                                        />
<%
                                                }
                                            }else{
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                                if(fillBy[j] != 3){
                                                    if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this);"
<%
                                                    }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this);"
<%
                                                    } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this);"
<%
                                                    }
                                                }
%>
                                        />
<%
                                            }
                                        }else{
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
<%
                                            if(fillBy[j] != 3){
                                                if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this);"
<%
                                                }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this);"
<%
                                                } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this);"
<%
                                                }
                                            }
%>
                                        />
<%
                                        }
                                   }
                           }

                                if(dataType == 2){
                                    if(i == (rows)){
                                        if(FormulaCount > 0){
                                            if(isTotal == 1){
                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    out.print("</td>");
                                                    continue;
                                                }else{
                                                    inputtype = "text";
                                                }
                                            }
                                        }
                                    }

                                //out.print("cellId : "+cellData.getCellId());
                                    if(fillBy[j] == 3){
%>
                                        <label name="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>"
                                        title="label" value="<%=textAreaValue%>">
                                        </label>
                                        <textarea cols="30" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" readOnly class="formTxtBox_1"></textarea>
<%
                                        if(true){
%>
                                        <script>
					if(arrForLabelDisp[<%=tableIndex%>].length != i+1)
						arrForLabelDisp[<%=tableIndex%>].push('<%=tableId%>_<%=columnId%>');
                                        </script>
<%
                                        }
                                    }else{
%>
                                        <textarea cols="30" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" class="formTxtBox_1" <%if(isEdit || isView || isEncrypt){out.print("readOnly");}%> ></textarea>
<%

                                    }
                                }
                                if(true){
%>
                                    <script>
                                       <%if(dataType==2){%>
                                        arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "TEXTAREA";
                                       <%}else{%>
                                           arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "TEXT";
                                       <%}%>
                                        arrDataTypesforCell[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "<%=dataType%>";

                                        if(arrColIds[<%=tableIndex%>].length != i+1)
                                                arrColIds[<%=tableIndex%>].push('<%=columnId%>');

                                    </script>
<%
                                }
                            }

                        } // celldtl
%>
                </td>
<%
                    }
%>
            </tr>
<%
                } // if(i>0)
            } //for(row)
        }
        else // Start Edit here
        {

            int cntBidTable = 0;
            int tmpRowId = 0;
             int bidTableId = 0;
            int TableId = 0;
            
            while(tblBidTableId.hasNext())
            {
                CommonFormData bidTableIdDt = tblBidTableId.next();
                cntBidTable++;
                while(tblCellsDtl.hasPrevious()){
                        tblCellsDtl.previous();
                }
                for (short i = -1; i <= rows; i++) {
                    if((i == 0) && (cntBidTable==1)){
%>
             <tr id="ColumnRow">
                 <th>&nbsp;</th>
<%
                        for(short j=0;j<cols;j++){
                            if(tblColumnsDtl.hasNext()){
                                CommonFormData colData = tblColumnsDtl.next();

                                colHeader = colData.getColumnHeader();
                                colType = colData.getColumnType();
                                filledBy = colData.getFillBy();
                                showOrHide = Integer.parseInt(colData.getShowOrHide());
                                bidTableId = colData.getBidTableId();
                                TableId =  colData.getTableId();
                                dataType = colData.getDataType();
                                colData = null;
                            }
                            fillBy[j] = filledBy;
%>
                <input type="hidden" name="hidbidTableId<%=TableId%>" id="hidbidTableId<%=TableId%>" value="<%=bidTableId%>">
                <th id="addTD<%= j + 1 %>">
                    <%= colHeader %>
                    <%if(showOrHide==2){%>
                    <script>
                        document.getElementById("addTD<%= j + 1 %>").visibility = "collapse";
                    </script>
                    <%}%>
                </th>
<%
                        }
%>
             </tr>
<%
                    }
                    if(i > 0){
                        tmpRowId++;
%>
             <tr id="row<%=tableId%>_<%=tmpRowId%>">
                 <td id="td<%=tableId%>_<%=tmpRowId%>">
                 <%  if(tmpRowId != (rows)){ %>
                 <input type="checkbox" name="row<%=tableId%>_<%=tmpRowId%>" id="row<%=tableId%>_<%=tmpRowId%>" onclick="enablecheckbox('<%=tableId%>','<%=tmpRowId%>','<%=columnId%>',this.checked)" value="1" />
                 <% }else{ %>
                    &nbsp;
                 <% } %>
                 </td>
<%
                        int cnt = 0;
                        for(int j=0; j<cols; j++){
                            String cellValue = "";
%>


                <td id="td<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" align="center"
                <%if(showOrHide==2){%>
                    <script>
                        document.getElementById("td<%=tableId%>_<%=tmpRowId%>_<%=columnId%>").visibility = "collapse";
                    </script>
                <%}%>
                >
<%
                            if(tblCellsDtl.hasNext()){
                                cnt++;
                                CommonFormData cellData = tblCellsDtl.next();
                                dataType = cellData.getCellDataType();
                                cellValue = cellData.getCellValue();
                                columnId = cellData.getColumnId();
                                colType = cellData.getColumnType();
                                if(true){
                                    String cValue = "";
                                    cValue = cellValue.replaceAll("\r\n", "");
%>
                            <script>
                                arrCellData[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = '<%=cValue.trim()%>';
                                arrCompType[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = '';
                             </script>
<%
                                }
                                if(fillBy[j] == 1){
                                    if("2".equalsIgnoreCase(colType)){
                                        if(tmpRowId != (rows)){
                                 %>
                                 <label  name="lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                        <%if(dataType==4 || dataType==8){%>
                                        value="<%=cellValue%>"
                                        <%}else{%>
                                        value=""
                                        <%}%>
                                        >
                                        <%=cellValue%>
                                </label>
                                       
                                <input type="text" class="formTxtBox_1" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                       <%
                            while(tblBidData.hasPrevious()){
                                tblBidData.previous();
                            }

                            while(tblBidData.hasNext()){
                                CommonFormData bidData = tblBidData.next();
                                if((bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
%>
                         value="<%=bidData.getCellValue()%>"
<%
                                }
                            }
                        %> readonly
                        <%

                            if(isEdit)
                            {
                                if(fillBy[j] != 3){
                                    if(dataType==3){
                                        if("2".equals(colType)){ %>
                                            onBlur="checkwithOldValue('lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>',this); CheckFloat1('<%=tableId%>',this);"
<%                                      }else{ %>
                                            onBlur="CheckFloat1('<%=tableId%>',this);"
<%                                      }
                                    }else if(dataType==4){
%>
                            onBlur="CheckNumeric('<%=tableId%>',this);" readOnly
<%
                                    }else if(dataType==8){
%>
                            onBlur="moneywithminus('<%=tableId%>',this);" readOnly
<%
                                    }
                                }
                            }
                        %>
                       />
                    <input type="hidden" name="hidcolId_<%=tmpRowId%>" id="hidcolId_<%=tmpRowId%>" value="<%=columnId%>">
                    <input type="hidden" name="hid<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="id<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" value="<%=cellValue%>" />
                                    <%    }
                                      }else{ %>
                                    <label  name="lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                <%if(dataType==4 || dataType==8){%>
                                                value="<%=cellValue%>"
                                                <%}else{%>
                                                value=""
                                                <%}%>
                                                >
                                                <%=cellValue%>
                                        </label>
                                    <% } %>
                                <script>
                                    lbl_ids[lbl_countid]="lbl@<%=tableId%>_<%=tmpRowId%>_<%=columnId%>";
                                    lbl_countid++;
				</script>
<%
                                    if(true){
%>
                                <script>
                                    //arrStaticColIds[<%=tableIndex%>].push('<%=columnId%>');
				</script>
<%
                                    }
                                } else if(fillBy[j] == 2 || fillBy[j] == 3){
                                    if(dataType != 2){
                                        if(tmpRowId == (editedRowCount))
                                        {
                                            if(isTotal == 1)
                                            {
                                                if(cntRow < TotalBidCount)
                                                    if(i%(rows - 1)==0)
                                                        break;

                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    if(arrWordFormulaColid[columnId-1] == columnId){
                                                        if(fillBy[j] == 3 && dataType == 3){
%>
                                                        <label
                                                            readonly
                                                            tabindex="-1"
                                                            name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>_<%=tableId%>"
                                                            id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>_<%=tableId%>"
                                                            title="label"	value="<%=textAreaValue%>" >
                                                        <%=textAreaValue%>
                                                        </label>
<%
                                                        } else{
%>
                                                        <textarea
                                                            readonly
                                                            tabindex="-1"
                                                            name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                            id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
                                                        ><%=textAreaValue%></textarea>
<%
                                                        }
                                                    }
                                                    out.print("</td>");
                                                    continue;
                                                }else{
                                                        inputtype = "text";
                                                }
                                            }
                                        }

                                       //out.print(" FillBy : "+fillBy[j]);
                                       //out.print(" Datatype : "+dataType);
                                       //out.print("tmpRowId : "+tmpRowId);
                                       //out.print("rows : "+rows);
                                       if(tmpRowId != (rows))
                                       {

%>
                                       <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formTxtBox_1"

                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
                                        <%if(isView || isEncrypt){%>
                                        readOnly
                                        <%}%>
<%
                                        if(isEdit)
                                        {
                                            if(fillBy[j] != 3){
                                                if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this);"
<%
                                                }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this);" readOnly
<%
                                                }else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this);" readOnly
<%
                                                }
                                            }
                                        }
                                            while(tblBidData.hasPrevious()){
                                                tblBidData.previous();
                                            }
                                            while(tblBidData.hasNext()){
                                                CommonFormData bidData = tblBidData.next();
                                                if((bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
%>
                                         value="<%=bidData.getCellValue()%>"
<%
                                                }
                                            }
%>
                                        />
<%
                                       }
                                       else if(tmpRowId == (rows))
                                       {
                                           //out.print("FormulaCount : "+FormulaCount);
                                           //out.print("isTotal : "+isTotal);
                                            if(FormulaCount > 0)
                                            {
                                                if(isTotal == 1)
                                                {
                                                    if(arrFormulaColid[columnId-1] == columnId)
                                                    {
%>
                                        <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formTxtBox_1"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
                                        <%if(isView || isEncrypt){%>
                                        readOnly
                                        <%}%>
<%
                                                    if(isEdit)
                                                    {
                                                        if(fillBy[j] != 3){
                                                            if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this);" readOnly
<%
                                                            }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this);" readOnly
<%
                                                            } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this);" readOnly
<%
                                                            }
                                                        }
                                                    }

                                                        while(tblBidData.hasPrevious()){
                                                            tblBidData.previous();
                                                        }
                                                        while(tblBidData.hasNext()){
                                                            CommonFormData bidData = tblBidData.next();
                                                            if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
%>
                                         value="<%=bidData.getCellValue()%>"
<%
                                                            }
                                                        }
%>
                                        />
<%
                                                    }
                                                }else{
%>
                                        <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formTxtBox_1"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
                                        <%if(isView || isEncrypt){%>
                                        readOnly
                                        <%}%>
<%
                                                    if(isEdit)
                                                    {
                                                        if(fillBy[j] != 3){
                                                            if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this);" readOnly
<%
                                                            }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this);" readOnly
<%
                                                            } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this);" readOnly
<%
                                                            }
                                                        }
                                                    }

                                                        while(tblBidData.hasPrevious()){
                                                            tblBidData.previous();
                                                        }
                                                        while(tblBidData.hasNext()){
                                                            CommonFormData bidData = tblBidData.next();
                                                            if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
%>
                                         value="<%=bidData.getCellValue()%>"
<%
                                                            }
                                                        }%>
                                        />
<%
                                                }
                                            }else{
%>
                                        <input type="text" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" class="formTxtBox_1"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly
                                        <%}%>
                                        <%if(isView || isEncrypt){%>
                                        readOnly
                                        <%}%>
<%
                                            if(isEdit)
                                            {
                                                if(fillBy[j] != 3){
                                                    if(dataType==3){
%>
                                        onBlur="CheckFloat('<%=tableId%>',this);" readOnly
<%
                                                    }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this);" readOnly
<%
                                                    } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this);" readOnly
<%
                                                    }
                                                }
                                            }

                                                while(tblBidData.hasPrevious()){
                                                    tblBidData.previous();
                                                }
                                                while(tblBidData.hasNext()){
                                                    CommonFormData bidData = tblBidData.next();
                                                        if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
%>
                                         value="<%=bidData.getCellValue()%>"
<%
                                                        }
                                                    }
%>
                                        />
<%
                                            }
                                        }
                                    }

                                    if(dataType == 2){
                                        if(tmpRowId == (rows)){
                                            if(FormulaCount > 0){
                                                if(isTotal == 1){
                                                    if(arrFormulaColid[columnId-1] != columnId){
                                                        out.print("</td>");
                                                        continue;
                                                    }else{
                                                        inputtype = "text";
                                                    }
                                                }
                                            }
                                        }
                                        //out.print("cellId : "+cellData.getCellId());
                                        if(fillBy[j] == 3){
%>
                                        <label name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>_<%=tableId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>_<%=tableId%>"
                                        title="label" value="<%=textAreaValue%>">
                                        </label>
                                        <textarea cols="30" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" readOnly class="formTxtBox_1"><%
                                            while(tblBidData.hasPrevious()){
                                                tblBidData.previous();
                                            }
                                            while(tblBidData.hasNext()){
                                                CommonFormData bidData = tblBidData.next();
                                                if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                    out.print(bidData.getCellValue());
                                                }
                                            }
%></textarea>
<%
                                            if(true){
%>
                                        <script>
					if(arrForLabelDisp[<%=tableIndex%>].length != i+1)
						arrForLabelDisp[<%=tableIndex%>].push('<%=tableId%>_<%=columnId%>');
                                        </script>
<%
                                            }
                                        }else{
%>
                                        <textarea cols="30" name="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>" id="row<%=tableId%>_<%=tmpRowId%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" class="formTxtBox_1" <%if(isEdit || isView || isEncrypt){out.print("readOnly");}%>><%
                                            while(tblBidData.hasPrevious()){
                                                tblBidData.previous();
                                            }
                                            while(tblBidData.hasNext()){
                                                CommonFormData bidData = tblBidData.next();
                                                if(((int) bidData.getBidTableId() == (int)bidTableIdDt.getBidTableId()) && ((int) bidData.getCellId() == (int)cellData.getCellId()) && ((int)bidData.getRowId() == (int)cellData.getRowId()) && ((int)bidData.getTableId() == tableId)){
                                                    out.print(bidData.getCellValue());
                                                }
                                            }
%></textarea>
<%

                                        }
                                    }
                                    if(true){
%>
                                    <script>
                                       <%if(dataType==2){%>
                                        arrCompType[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = "TEXTAREA";
                                       <%}else{%>
                                           arrCompType[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = "TEXT";
                                       <%}%>
                                        arrDataTypesforCell[<%=tableIndex%>][<%=tmpRowId%>][<%=columnId%>] = "<%=dataType%>";

                                        if(arrColIds[<%=tableIndex%>].length != i+1)
                                                arrColIds[<%=tableIndex%>].push('<%=columnId%>');

                                    </script>
<%
                                    }
                                }
                            } // celldtl
%>
                </td>
<%
                        }
%>
            </tr>
<%
                    }
                } // edn of for loop for row
            }
        }
%>
        </tbody>
        <div>
        <tr>
            <td colspan="<%=cols%>" style="display:none">
                <label class="formBtn_1" id="lblAddTable">
                    <input type="button" name="btn<%=tableId%>" id="bttn<%=request.getParameter("tableIndex")%>" value="Add Table" onClick="AddTable(this.form,<%=tableId%>,this)"/>
                </label>
            </td>
        </tr>
        </div>
    </table>
<script language="javascript">
    function enablecheckbox(tableId,rowId,columnId,status){

        var colId = document.getElementById('hidcolId_'+rowId).value;
        var id = 'row'+tableId+'_'+rowId+'_'+colId;

        if(status==false){
            document.getElementById(id).readOnly = true;
            document.getElementById('row'+tableId+'_'+rowId).bgColor = "white";
        }else{
            document.getElementById(id).readOnly = false;
            document.getElementById('row'+tableId+'_'+rowId).bgColor = "#f5f5f5";
        }
    }

    function checkwithOldValue(lableId,objText){
        var oldVal = parseInt(document.getElementById(lableId).innerHTML);
        var newVal = objText.value;

        if(newVal > oldVal){
            jAlert("Please Enter less Quantity."," Enter Valid Quantity", function(){});
            objText.value = oldVal;
        }
    }
</script>