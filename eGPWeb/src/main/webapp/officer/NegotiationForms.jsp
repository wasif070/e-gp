<%--
    Document   : Negotiation Forms
    Created on : Dec 30, 2010, 4:36:51 PM
    Author     : Rikin
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.NegotiationSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<jsp:useBean id="negotiationFormsSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationFormsSrBean"></jsp:useBean>
<jsp:useBean id="negotiationProcessSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Select forms for revision</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
        function SelectService(val){
           // alert(val);
         //if(val==18 || val==14 || val==13 || val==15){
          /*if($("#formSelect_1").attr('checked') || $("#formSelect_2").attr('checked') || $("#formSelect_3").attr('checked') || $("#formSelect_4").attr('checked')){
              $("#formSelect_2").attr('checked',true);
              $("#formSelect_3").attr('checked',true);
              $("#formSelect_4").attr('checked',true);
              $("#formSelect_1").attr('checked',true);
          }else{
              $("#formSelect_1").removeAttr('checked');
              $("#formSelect_2").removeAttr('checked');
              $("#formSelect_3").removeAttr('checked');
              $("#formSelect_4").removeAttr('checked');
          }*/
                if($(val).attr('checked')){
                       // alert(doucment.getElementById(idName).value);
                            $("#formSelect_2").attr('checked',true);
                            $("#formSelect_3").attr('checked',true);
                            $("#formSelect_4").attr('checked',true);
                            $("#formSelect_1").attr('checked',true);
                }else{
                    $("#formSelect_1").removeAttr('checked');
                    $("#formSelect_2").removeAttr('checked');
                    $("#formSelect_3").removeAttr('checked');
                    $("#formSelect_4").removeAttr('checked');
                }
                        //$(this).attr('checked',true);
                
         /*}else if(val==14){
           if($("#formSelect_3").attr('checked')){
                  $("#formSelect_4").attr('checked',true);
                  $("#formSelect_1").attr('checked',true);
                  $("#formSelect_2").attr('checked',true);
          }else{
              if($("#formSelect_1").attr('checked') || $("#formSelect_2").attr('checked')){
                  $("#formSelect_3").attr('checked',true);
                  jAlert("Selection is Invalid","Negotiation Form", function(RetVal) {
                                });
                    //return false;
              }else{
                  $("#formSelect_3").removeAttr('checked');
                  $("#formSelect_4").removeAttr('checked');
              }
          }  
         }else if(val==13){
              if($("#formSelect_2").attr('checked')){
                  $("#formSelect_1").attr('checked',true);
                  $("#formSelect_3").attr('checked',true);
                  $("#formSelect_4").attr('checked',true);
              }else{
                  $("#formSelect_1").removeAttr('checked');
                  $("#formSelect_2").removeAttr('checked');
                  $("#formSelect_3").removeAttr('checked');
                  $("#formSelect_4").removeAttr('checked');
              }
          }else if(val==15){
              if(!$("#formSelect_4").attr('checked')){
                  if($("#formSelect_1").attr('checked') || $("#formSelect_2").attr('checked') || $("#formSelect_3").attr('checked')){
                      $("#formSelect_4").attr('checked',true);
                      jAlert("Selection is Invalid","Negotiation Form", function(RetVal) {
                                });
                            //return false;
                  }
                }
            }*/
        }
        function SelectAll(){
            //alert($("#selectAll").attr('checked'));
            if($("#selectAll").attr('checked')){
                $(":checkbox[checked='false']").each(function(){
                    $(this).attr('checked',true);
                }); 
            }else{
                $(":checkbox[checked='true']").each(function(){
                    $(this).removeAttr('checked');
                }); 
            }
        }
        function CheckSelected(){
            var count = 0;
            $(":checkbox[checked='true']").each(function(){
                    count++;
                });
                if(count==0){
                    jAlert("Please Select Atleast one Form","Negotiation Form", function(RetVal) {
                                });
                                return false;
                }
        }
        </script>
    </head>
    
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="dashboard_div">
            <%  String tenderId;
                tenderId = request.getParameter("tenderId");

                String userId = "", usrId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                if (request.getParameter("uId") != null && !"".equalsIgnoreCase(request.getParameter("uId"))) {
                    usrId = request.getParameter("uId");
                }

                int negId = 0;
                if(request.getParameter("negId")!=null){
                    negId = Integer.parseInt(request.getParameter("negId"));
                }

                
                

                String finalStatus = "";
                finalStatus = negotiationProcessSrBean.getFinalSubStatus(negId);
            %>
           
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">
                    Select forms for revision
                    <span style="float: right; text-align: right;">
                        <a class="action-button-goback" href="NegotiationProcess.jsp?tenderId=<%=tenderId%>" title="Bid Dashboard">Go Back To Dashboard</a>
                    </span>
                </div>
                <%
                    pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
                <%
                       boolean isTenPackageWis1 = (Boolean) pageContext.getAttribute("isTenPackageWise");
                %>
               
                <% pageContext.setAttribute("tab", "7");%>
                <%@include  file="/officer/officerTabPanel.jsp"%>
                     
               
                 <div class="tabPanelArea_1">
                 <%-- Start: Common Evaluation Table --%>
                      <%@include file="/officer/EvalCommCommon.jsp" %>
                    <%-- End: Common Evaluation Table --%>
               
        
                <%  pageContext.setAttribute("TSCtab", "7"); %>
            <div class="t_space">
            <%@include file="../resources/common/AfterLoginTSC.jsp" %>
            </div>
             
                
               <div class="tabPanelArea_1">
                <form name="frmNegotiateForm" id="idfrmNegotiateForm" method="post" action="<%=request.getContextPath()%>/NegotiationFormsSrBean" onsubmit="return CheckSelected();">
                    <input type="hidden" name="action" id="action" value="negotiateform" />
                    <%
                    boolean isSerivce = false;
                        if(afterLoginTSCTabprocNature.toString().equals("Services")){
                            isSerivce =  true;
                        }
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                    if (isTenPackageWis1) 
                    {
                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) 
                                {
                    %>

                   <table width="100%" cellspacing="0" class="tableList_1" style="float: left;">
                        <tr>
                            <td colspan="2" class="t-align-center ff">Tender/Proposal Details</td>
                        </tr>

                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space" style="float: left;">
                        <tr>
                            <th width="10%" class="t-align-left">Select All <br />
                            <input id="selectAll" type="checkbox"  class="checkbox" onclick="SelectAll();"/>
                            </th>
                            <th width="90%" class="t-align-left ff">Form Name</th>
                        </tr>
                        <%
                            boolean flag = false;
                            List <SPCommonSearchData> listForm = commonSearchService.searchData("GetNegTenderFormsByLotId", tenderId, usrId, "0", usrId, null, null, null, null, null);
                            
                            if(isSerivce){
                                List<SPCommonSearchData> myList = new ArrayList<SPCommonSearchData>(listForm);
                                Map<Integer,String[]> formMap = new HashMap<Integer, String[]>();
                                for(int d = 0;d<myList.size();d++){
                                    formMap.put(Integer.parseInt(myList.get(d).getFieldName3()), new String[] {myList.get(d).getFieldName1(),myList.get(d).getFieldName2()});
                                    if("18".equalsIgnoreCase(myList.get(d).getFieldName3()) || "13".equalsIgnoreCase(myList.get(d).getFieldName3()) || "14".equalsIgnoreCase(myList.get(d).getFieldName3()) || "15".equalsIgnoreCase(myList.get(d).getFieldName3())){
                                        listForm.remove(myList.get(d));
                                    }
                                }
                                myList.clear();
                                myList = null;
        %>
                    <%if(formMap.containsKey(18)){%>
                        <tr>
                            <td class="t-align-center"><input id="formSelect_1" type="checkbox" name="nameform" value="<%=formMap.get(18)[0]%>" class="checkbox" <% if(negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formMap.get(18)[0]),negId)){ %>checked="checked"<% } %> onclick="SelectService(this);"/></td>
                            <td class="t-align-left ff"><%=formMap.get(18)[1]%></td>
                        </tr>
                        <%}if(formMap.containsKey(13)){%>
                        <tr>
                            <td class="t-align-center"><input id="formSelect_2" type="checkbox" name="nameform" value="<%=formMap.get(13)[0]%>" class="checkbox" <% if(negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formMap.get(13)[0]),negId)){ %>checked="checked"<% } %> onclick="SelectService(this);"/></td>
                            <td class="t-align-left ff"><%=formMap.get(13)[1]%></td>
                        </tr>
                        <%}if(formMap.containsKey(14)){%>
                        <tr>
                            <td class="t-align-center"><input id="formSelect_3" type="checkbox" name="nameform" value="<%=formMap.get(14)[0]%>" class="checkbox" <% if(negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formMap.get(14)[0]),negId)){ %>checked="checked"<% } %> onclick="SelectService(this);"/></td>
                            <td class="t-align-left ff"><%=formMap.get(14)[1]%></td>
                        </tr>
                        <%}if(formMap.containsKey(15)){%>
                        <tr>
                            <td class="t-align-center"><input  id="formSelect_4" type="checkbox" name="nameform" value="<%=formMap.get(15)[0]%>" class="checkbox" <% if(negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formMap.get(15)[0]),negId)){ %>checked="checked"<% } %> onclick="SelectService(this);"/></td>
                            <td class="t-align-left ff"><%=formMap.get(15)[1]%></td>
                        </tr>
                            <%}
                            formMap.clear();
                            formMap  = null;
                       }
                            int indexValue = 4;
                            for (SPCommonSearchData formdetails : listForm) {
                                indexValue ++;
                                flag= false;
                                flag = negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formdetails.getFieldName1()),negId);
                        %>
                        <tr>
                            <td class="t-align-center"><input id="formSelect_<%=indexValue%>" type="checkbox" name="nameform" value="<%=formdetails.getFieldName1()%>" class="checkbox" <% if(flag==true){ %>checked="checked"<% } %>/></td>
                            <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                        </tr>
                        <% }%>
                    </table>
                        <% 
                            }
                             } else 
                             {
                                 for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) 
                                 {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-center ff">Tender/Proposal Details</td>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%  }
                            for (SPCommonSearchData lotList : commonSearchService.searchData("gettenderlotbytenderid", tenderId, "", "", null, null, null, null, null, null)) 
                            {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                            <tr>
                                <td width="22%" class="t-align-left ff">Lot No. :</td>
                                <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Lot Description :</td>
                                <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                            </tr>
                        </table>
                     <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="10%" class="t-align-left">Select All <br />
                                <input id="selectAll" type="checkbox"  class="checkbox" onclick="SelectAll();"/>
                            </th>
                            <th width="90%" class="t-align-left ff">Form Name</th>
                        </tr>
                            <%
                                 boolean flag = false;
                                 List <SPCommonSearchData> listForm = commonSearchService.searchData("GetNegTenderFormsByLotId", tenderId, usrId, "0", usrId, null, null, null, null, null);
                                 
                            if(isSerivce)
                            {
                                List<SPCommonSearchData> myList = new ArrayList<SPCommonSearchData>(listForm);
                                Map<Integer,String[]> formMap = new HashMap<Integer, String[]>();
                                for(int d = 0;d<myList.size();d++){
                                    formMap.put(Integer.parseInt(myList.get(d).getFieldName3()), new String[] {myList.get(d).getFieldName1(),myList.get(d).getFieldName2()});
                                    if("18".equalsIgnoreCase(myList.get(d).getFieldName3()) || "13".equalsIgnoreCase(myList.get(d).getFieldName3()) || "14".equalsIgnoreCase(myList.get(d).getFieldName3()) || "15".equalsIgnoreCase(myList.get(d).getFieldName3())){
                                        listForm.remove(myList.get(d));
                                    }
                                }
                                myList.clear();
                                myList = null;
        %>
                    <%if(formMap.containsKey(18)){%>
                         <tr>
                            <td class="t-align-center"><input id="formSelect_1" type="checkbox" name="nameform" value="<%=formMap.get(18)[0]%>" class="checkbox" <% if(negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formMap.get(18)[0]),negId)){ %>checked="checked"<% } %> onclick="SelectService(18);"/></td>
                            <td class="t-align-left ff"><%=formMap.get(18)[1]%></td>
                        </tr>
                        <%}if(formMap.containsKey(13)){%>
                        <tr>
                            <td class="t-align-center"><input id="formSelect_2" type="checkbox" name="nameform" value="<%=formMap.get(13)[0]%>" class="checkbox" <% if(negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formMap.get(13)[0]),negId)){ %>checked="checked"<% } %> onclick="SelectService(13);"/></td>
                            <td class="t-align-left ff"><%=formMap.get(13)[1]%></td>
                        </tr>
                        <%}if(formMap.containsKey(14)){%>
                        <tr>
                            <td class="t-align-center"><input id="formSelect_3" type="checkbox" name="nameform" value="<%=formMap.get(14)[0]%>" class="checkbox" <% if(negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formMap.get(15)[0]),negId)){ %>checked="checked"<% } %> onclick="SelectService(14);"/></td>
                            <td class="t-align-left ff"><%=formMap.get(14)[1]%></td>
                        </tr>
                        <%}if(formMap.containsKey(15)){%>
                        <tr>
                            <td class="t-align-center"><input  id="formSelect_4" type="checkbox" name="nameform" value="<%=formMap.get(16)[0]%>" class="checkbox" <% if(negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formMap.get(15)[0]),negId)){ %>checked="checked"<% } %> onclick="SelectService(15);"/></td>
                            <td class="t-align-left ff"><%=formMap.get(15)[1]%></td>
                        </tr>
                            <%}
                            formMap.clear();
                            formMap  = null;
                       }
                                 int indexValue = 4;
                                 for (SPCommonSearchData formdetails : listForm)
                                 {
                                     flag= false;
                                     flag = negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formdetails.getFieldName1()),negId);
                            %>
                            <tr>
                                <td class="t-align-center"><input id="formSelect_<%=indexValue%>" type="checkbox" name="nameform" value="<%=formdetails.getFieldName1()%>" <% if(flag==true){ %>checked="checked"<% } %>/></td>
                                <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                            </tr>
                            <%   } %>
                        </table>
                        <%       }
                                    }
                        %>
                     <table width="100%" cellspacing="0" style="float: left;">
                        <tr>
                            <td colspan="2" class="t-align-center ff">
                                <input type="hidden" name="hidtenderId" id="hidtenderId" value="<%=request.getParameter("tenderId")%>" />
                                <input type="hidden" name="negUserId" value="<%=usrId%>" />
                                <input type="hidden" name="hidnegId" id="hidnegId" value="<%=request.getParameter("negId")%>" />
                                <label class="formBtn_1 t_space">
                                    <input type="submit" name="btnsubmit" id="idbtnsubmit" value="Submit" />
                                </label>
                            </td>
                        </tr>
                     </table>
                   </form>
               
            <%@include file="../resources/common/Bottom.jsp" %>
               </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>
    </body>
     
            <!--Dashboard Footer End-->
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
