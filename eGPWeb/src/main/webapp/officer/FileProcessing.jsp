<%-- 
    Document   : FileProcessing
    Created on : Dec 11, 2010, 5:24:49 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.dao.daointerface.HibernateQueryDao"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblGovUserAuditDetails"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserTypeMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.WorkFlowService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.APPService"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TscApptoAaSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowDocuments"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkflowRuleEngine"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.ArrayList"%>

<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="com.cptu.egp.eps.web.utility.SHA1HashEncryption"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Process file in Workflow</title>
        <!-- <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="Include/pngFix.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>-->


        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
       
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <!-- rich text -->
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <!-- jalert -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
           
              function Validate(){
                   var validatebool="";
                  if(CKEDITOR.instances.txtComments.getData() == 0){
                    document.getElementById("s_textarea").innerHTML="Please enter Comments.";
                    validatebool="false";
                }
                if($("#txtAction option:selected").text()=='Reject' && $("#txtActivitiyID").val()=="13")
                    {
                        $(".err").remove();
                        if(!$("#reTendering").attr('checked') && !$("#reTenderingNo").attr('checked')) {
                        $("#reTendering").parent().append("<div class='err' style='color:red;'>Please select Re - Tendering</div>");
                        validatebool="false";
                       }
                    }
                    /* Added by Dohatec for Tender Publish by workflow Approver Start*/
                    if($("#txtAction option:selected").text()=='Approve' && $("#txtActivitiyID").val()=="2" && $("#txtTenderDocumentTimeElapsed").val()=="false" )
                         {
                        $(".err").remove();
                       jAlert("Scheduled Tender Publication Date and Time Elapsed. Please return to Initiator / PE"," Alert ", "Alert");
                        validatebool="false";
                       }
                    /* Added by Dohatec for Tender Publish by workflow Approver End*/
                var rev1 = document.getElementById('txtAction');
                var prid = rev1.options[rev1.selectedIndex].value;
                if(prid == 'SelectAction'){
                    jAlert("Please select action"," Alert ", "Alert");
                    validatebool="false";
                }

        
                if (validatebool=='false'){
                    return false;
                }  
              }

                $(document).ready(function() {
                CKEDITOR.instances.txtComments.on('blur', function()
                {
                    if(CKEDITOR.instances.txtComments.getData() != 0)
                    {
                        document.getElementById("s_textarea").innerHTML="";
                        return true;
                    }else{
                        document.getElementById("s_textarea").innerHTML = "Please enter Comments.";
                        return false;
                    }
                });
            });
             
        </script>

        <script type="text/javascript">
             function GetCal(txtname,controlname){
                new Calendar({
                          inputField: txtname,
                          trigger: controlname,
                          showTime: false,
                          dateFormat:"%d/%m/%Y",
                          onSelect: function() {
                                  var date = Calendar.intToDate(this.selection.get());
                                  LEFT_CAL.args.min = date;
                                  LEFT_CAL.redraw();
                                  this.hide();
                          }
                  });

                  var LEFT_CAL = Calendar.setup({
                            weekNumbers: false
                        })
            }

        </script>

       <script type="text/javascript">
            function ActionChanges(){
                if($("#txtAction option:selected").text()=='Reject' && $("#txtActivitiyID").val()=="13"){                 
                        $("#reTenderTr").show()
                    }else{
                        $("#reTenderTr").hide()
                    }
                }
        </script>

    </head>
    
        <%
        APPService appService = (APPService)AppContext.getSpringBean("APPService");
        WorkFlowService workFlowService = (WorkFlowService)AppContext.getSpringBean("WorkFlowService");
         CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
         MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
         TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
         CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
       String activityid =  request.getParameter("activityid");
       String objectid = request.getParameter("objectid");
       String childid = request.getParameter("childid");
       String eventid = request.getParameter("eventid");
       String lotId = request.getParameter("lotId");
       String wpId = request.getParameter("wpId");
       String fromaction = request.getParameter("fromaction");
       String strWF = "";
       String loi = "";
       /*Dohatec Start*/
       String s_tenderid = "0";
       String s_lotId = "0";
       String s_rId = "0";
       String evalType = "";
       String evalRptToAaid = "0";
       String activity_Name = "";
       Short activity_ID = Short.parseShort(activityid);
       
       if(request.getParameter("loi") != null){
           loi = request.getParameter("loi");
       }
       
      
        %>
        <input name="txtActivitiyID" type="hidden" value="<%=activity_ID%>" id="txtActivitiyID"  />
        <%
       String rpt = "0";
       List<SPTenderCommonData> activityNameList = tenderCommonService.returndata("getActivityName", activityid, null);
       if(!activityNameList.isEmpty())for (SPTenderCommonData activityName : activityNameList) { activity_Name = activityName.getFieldName1(); }
        if(activityid.equalsIgnoreCase("13") || activity_Name.trim().equalsIgnoreCase("Contract Approval Workflow") ){
        s_tenderid = objectid;
        evalType = "eval";
        String procNature = commonService.getProcNature(s_tenderid).toString();
        
        if (!procNature.equals("Services")) {
        List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("getProcurementNatureForEval", s_tenderid, null);
      //  if (!tenderLotList.isEmpty()) {
                for (SPTenderCommonData tenderLot : tenderLotList) { s_lotId = tenderLot.getFieldName3(); }
                }

                
                List<SPCommonSearchDataMore> getreportID = null;
                String reportID = null;
                getreportID = dataMore.geteGPData("getCRReportId", s_tenderid,"0","0");
                if(getreportID!=null && (!getreportID.isEmpty())){  reportID = getreportID.get(0).getFieldName1();   }
                List<SPCommonSearchDataMore> bidderRounds = new ArrayList<SPCommonSearchDataMore>();//Round List Initialized
                bidderRounds = dataMore.geteGPData("getRoundsforEvaluation",s_tenderid,reportID,"L1");
                for(SPCommonSearchDataMore round : bidderRounds){ s_rId = round.getFieldName1(); }
      //      }
        }
        /*Dohatec End*/
        switch(Integer.parseInt(activityid))                 
        {
            case 1:strWF="for App";break;
            case 2:strWF="for Tender Notice";break;
            case 3:strWF="for PreTender Meeting";break;
            case 4:strWF="for Amendment Approval";break;
            case 5:strWF="for Tender Opening";break;
            case 6:strWF="for Evaluation Committee";break;
            case 7:strWF="for Package Approve";break;    
            case 8:strWF="for Technical Sub Committee";break;    
            case 9:strWF="for Cancel Tender";break;    
            case 10:strWF="for Contract Termination";break;    
            case 11:strWF="for Variation Order";break;
            case 12:strWF="for Repeat Order";break;
            case 13:strWF="for Contract Approval";break;
        }
       int tenderId = 0;       
       if(request.getParameter("tenderId")!=null){
       tenderId = Integer.parseInt(request.getParameter("tenderId"));
       }
       int uid = 0;
       if(session.getAttribute("userId") != null){
           Integer ob1 = (Integer)session.getAttribute("userId");
           uid = ob1.intValue();
       }
       List<CommonAppData> listofAPPDetails = appService.getAPPDetailsBySP("APP", ""+objectid, "");
       String appCode= "";
      
       if(!listofAPPDetails.isEmpty()){
          appCode = listofAPPDetails.get(0).getFieldName3();
       }
       //int uid = 94;
       String ispublisdateReq = "No";
       String action = "WorkFlowAction";
       String modName = null;
       String eventName = null;
       int actid = 0;
       int objid = 0;
       int chilid = 0;
       String fileOnHand = "No";
       String msg = "not";
       if(activityid != null){
       actid = Integer.parseInt(activityid);
       objid = Integer.parseInt(objectid);
       chilid = Integer.parseInt(childid);
       }
       if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
        objid = chilid;
       }
       boolean isTenPackageWise = false;
       CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
       List<SPCommonSearchData> searchData =  commonSearchService.searchData("getFlagStatus", ""+actid, ""+objid, ""+childid, "", "", "", "", "", "");
       if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
        objid = Integer.parseInt(objectid);
       }
       
       if(actid==2){
           List<SPTenderCommonData> tenderData = tenderCommonService.returndata("tenderinfobar", objectid, null);       
           if ("Package".equalsIgnoreCase(tenderData.get(0).getFieldName2())) {
                isTenPackageWise = true;
           }

       }
                               
       if(!searchData.isEmpty()){
            String retval  = searchData.get(0).getFieldName1();

            String fromloc = "";
            if(request.getParameter("fromloc")!= null){
                fromloc = request.getParameter("fromloc");
            }
            if("1".equalsIgnoreCase(retval) && !"defaultworkflow".equalsIgnoreCase(fromloc)){
                //response.sendRedirectFilter(request.getContextPath()+"/officer/PendingProcessing.jsp?viewtype=processed&msg=alreadyprocess");
                response.sendRedirect("PendingProcessing.jsp?viewtype=processed&msg=alreadyprocess");
            }
       }
       
       TblWorkflowRuleEngine wfr = null;
       short wflevel = 1;
       byte wfroleid = 1;
       List<CommonAppData> wfactions = null;
       WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
       List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = null;
       if("Submit".equals(request.getParameter("submit")))
       {
           String moduleName = request.getParameter("moduleName");
           eventName = request.getParameter("eventName");//Cancel Tender
           Date pdate = null;
           StringBuffer docidsBuff = new StringBuffer();
           String actionval =  request.getParameter("action");
           String[] stracwf = actionval.split("_");
           Integer level = Integer.parseInt(stracwf[1]);
           String wfaction = stracwf[0];
           String comments = request.getParameter("txtComments");
           String action_param = stracwf[0];
           String officdesig = "";
           String[] od = new String[3];
           String ofcname = "";
           String desig = "";

           /* Dohatec Start*/
           String Retender ="";
           if("Reject".equalsIgnoreCase(action_param) && activityid.equalsIgnoreCase("13") ){
                String reTender = request.getParameter("reTendering");
                           if("Yes".equalsIgnoreCase(reTender)){
                action_param = "Rejected / Re-Tendering";
             }
           }
           /*Dohatec End*/

         //For evaluation Module
         TscApptoAaSrBean tscAppToAaSrBean = new TscApptoAaSrBean();
         AuditTrail auditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
         String auditAction = "";
         int auditId = objid;
         String auditIdType= "";
         
         if(Integer.parseInt(eventid) == 1){
             auditIdType = "appId";
         }else if(Integer.parseInt(eventid) == 9 || Integer.parseInt(eventid) ==10 || Integer.parseInt(eventid) ==11){
             auditIdType = "contractId";
         }else{
             auditIdType = "tenderId";
         }
         
         if (actid == 8) {
            if (action_param.equalsIgnoreCase("Forward")) {
                            int userId = Integer.parseInt(session.getAttribute("userId").toString());
                            tscAppToAaSrBean.insertData(objid, userId);
            } else if (action_param.equalsIgnoreCase("Approve") || action_param.equalsIgnoreCase("Reject") || actionval.equalsIgnoreCase("Conditional Approval")) {
                if (action_param.equalsIgnoreCase("Conditional Approval")) {
                    String av = "Approve";
                    tscAppToAaSrBean.updateData(objid, av);
                } else {
                    tscAppToAaSrBean.updateData(objid, action_param);
                }
                
            }
        }
        auditAction = "Work Flow action : "+action_param;
      
        //getting wfdocids
         if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
            objectid = childid;
         }
         List<TblWorkFlowDocuments> tblWorkFlowDocuments =
                   workFlowSrBean.getWorkFlowDocuments(Integer.parseInt(objectid),
                   Integer.parseInt(activityid), Integer.parseInt(childid),uid);
         if(tblWorkFlowDocuments.size()>0){
              Iterator it =  tblWorkFlowDocuments.iterator();
              while(it.hasNext()){
              TblWorkFlowDocuments tblWorkFlowDocument = (TblWorkFlowDocuments) it.next();
                 docidsBuff.append(tblWorkFlowDocument.getWfDocumentId()+",");
                  }
             }
         if(docidsBuff.length()>0){
            docidsBuff.deleteCharAt(docidsBuff.length()-1);
         }         

          if(actid == 4 || actid == 1){
               int tempchildid = Integer.parseInt(objectid);
               objectid = request.getParameter("objectid");
           tblWorkFlowLevelConfig = workFlowSrBean.getWorkFlowLevelConfig(uid,
                  Short.parseShort(eventid), Integer.parseInt(objectid), tempchildid);
           }else{
                   tblWorkFlowLevelConfig = workFlowSrBean.getWorkFlowLevelConfig(uid,
                  Short.parseShort(eventid), Integer.parseInt(objectid), Integer.parseInt(childid));
           }

         List<CommonMsgChk> returnMsg = null;
         String isFileProcessedOrNot = "No";
         if(tblWorkFlowLevelConfig.size()>0){
           Iterator wflc =   tblWorkFlowLevelConfig.iterator();
           while(wflc.hasNext()){
               TblWorkFlowLevelConfig twfl = (TblWorkFlowLevelConfig)wflc.next();
               if(twfl.getFileOnHand().equalsIgnoreCase("Yes")){
                   isFileProcessedOrNot = twfl.getFileOnHand();
                   break;
                   }
               }
          }
         
         makeAuditTrailService.generateAudit(auditTrail,auditId , auditIdType, EgpModule.WorkFlow.getName(), auditAction+" "+strWF, comments);
         
         MessageProcessSrBean messageProcessSrbean = new MessageProcessSrBean();
         MailContentUtility mc = new MailContentUtility();
         SendMessageUtil smu = new SendMessageUtil();
         String fromEmailId = "";
         List gettingfromemail =    messageProcessSrbean.getLoginmailIdByuserId(uid);
         if(gettingfromemail.size()>0){
            TblLoginMaster loginMaster2 = (TblLoginMaster)gettingfromemail.get(0);
            fromEmailId = loginMaster2.getEmailId();
         }
            
         if(isFileProcessedOrNot.equalsIgnoreCase("Yes") || fromaction.equalsIgnoreCase("workflow") ){
                 if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                    objid = chilid;
                 }
                 
                 workFlowSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                 returnMsg =  workFlowSrBean.addUpdWffileprocessing(actid, objid, chilid, uid, action_param, comments, level, "system", pdate, docidsBuff.toString());
                 if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                    objid = Integer.parseInt(objectid);
                 }
             if(returnMsg.size()>0){
                 /* Added by Dohatec for Tender Publish by workflow Approver Start*/
                 if("2".equalsIgnoreCase(activityid) && "2".equalsIgnoreCase(eventid) && action_param.equalsIgnoreCase("Approve")){
                      String reqURL = request.getRequestURL().toString() ;
                      reqURL = reqURL.replace( "FileProcessing.jsp","PublishViewTender.jsp");
                       String reqQuery ="id="+objectid ;
                       List<SPTenderCommonData> detailsPE = tenderSrBean.TenderPEuserIdandName(objectid);
                      Object objUsrId = session.getAttribute("userId");
                        int usId = 0;
                        if (objUsrId != null) {
                         usId = Integer.parseInt(objUsrId.toString());
                        }
                       String PEName="";
                       if (!detailsPE.isEmpty() || !(detailsPE==null))
                       {
                       PEName = detailsPE.get(0).getFieldName2();
                       }
                        String folderName = pdfConstant.TENDERNOTICE;
                        List<TblTenderDetails> detailsTenderPublish = tenderSrBean.getTenderDetails(Integer.parseInt(objectid));
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                        Calendar calendar = java.util.Calendar.getInstance();
                        String subDate = dateFormat1.format(detailsTenderPublish.get(0).getSubmissionDt());
                        String subDate1 = dateFormat1.format(detailsTenderPublish.get(0).getSubmissionDt());
                        calendar.setTime(dateFormat1.parse(subDate));
                        calendar.add(Calendar.DATE, +detailsTenderPublish.get(0).getTenderValDays());
                        Date tenderValidityDt = calendar.getTime();
                        calendar.setTime(dateFormat1.parse(subDate1));
                        calendar.add(Calendar.DATE, +detailsTenderPublish.get(0).getTenderSecurityDays());
                        Date tenderSecurityDt = calendar.getTime();
                        tenderSrBean.updateStatus(Integer.parseInt(objectid),reqURL,reqQuery,folderName,tenderValidityDt,tenderSecurityDt,PEName);
                        String remarks = "Approving Authority Published";
                        String eSignature = SHA1HashEncryption.encodeStringSHA1(remarks);
                        String OpeningDate = "";
                        for (CommonTenderDetails commonTenderDetailsInfo : tenderSrBean.getAPPDetails(Integer.parseInt(objectid), "tender")) {
                            OpeningDate = String.valueOf(DateUtils.formatDate(commonTenderDetailsInfo.getOpeningDt()));
                            }
                        tenderSrBean.insertInToTenderAuditTrail(Integer.parseInt(objectid),"Approved",usId,eSignature,remarks,OpeningDate);
                 }
                 /* Added by Dohatec for Tender Publish by workflow Approver End*/
               CommonMsgChk msgchk =  returnMsg.get(0);
                   if(msgchk.getFlag() == true){
                       
                       int userId = workFlowService.getWorkFlowLevelUser( new Short(eventid), new Short(stracwf[1]), new Short(activityid) , Integer.parseInt(objectid));
                       List<TblLoginMaster> listlogin = commonService.getUserType(userId);
                       TblUserTypeMaster tblUserTypeMaster = listlogin.get(0).getTblUserTypeMaster();
                       
                       if(tblUserTypeMaster.getUserTypeId()==6){
                            officdesig = workFlowSrBean.getOfficeNameAndDesignationByDP(userId);
                        }else{
                            officdesig = workFlowSrBean.getOfficialAndDesignationName(userId);
                        }
                       
                        od = officdesig.split(",");
                        ofcname = od[0];
                        desig = od[1];
                        String[] mails = new String[1];
                        mails[0] = listlogin.get(0).getEmailId();
                       List list = mc.conWorkflowFileProcess(eventName, moduleName, appCode,ofcname, desig, fromEmailId);
                       String sub = list.get(0).toString();
                       String mailText = list.get(1).toString();
                       smu.setEmailTo(mails);
                       smu.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                       smu.setEmailSub(sub);
                       smu.setEmailMessage(mailText);
                       smu.sendEmail();
                       msg = "File processed successfully";
                  }
             }
         }
         makeAuditTrailService.generateAudit(auditTrail,auditId , auditIdType, EgpModule.WorkFlow.getName(), "Process File in workflow "+strWF, comments);
         if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
            objectid = request.getParameter("objectid");
         }
             if(fromaction.equalsIgnoreCase("dashboard") || fromaction.equalsIgnoreCase("pkgdashboard")){
               response.sendRedirect("APPDashboard.jsp?appID="+objid+"&msg="+msg);
             }else if(fromaction.equalsIgnoreCase("pending") && !("2".equalsIgnoreCase(activityid) && "2".equalsIgnoreCase(eventid) && action_param.equalsIgnoreCase("Approve"))){ /* Modified by Dohatec for Tender Publish by workflow Approver*/
                  response.sendRedirect("PendingProcessing.jsp?viewtype=processed&msg="+msg);
             }else if(fromaction.equalsIgnoreCase("pending") && "2".equalsIgnoreCase(activityid) && "2".equalsIgnoreCase(eventid) && action_param.equalsIgnoreCase("Approve")){ /* Added by Dohatec for Tender Publish by workflow Approver*/
                  response.sendRedirect("TenderPdfGenerate.jsp?tenderid="+objid);
             }else if(fromaction.equalsIgnoreCase("notice")){
                  response.sendRedirect("Notice.jsp?tenderid="+objid+"&msg="+msg);
             }else if(fromaction.equalsIgnoreCase("opening")){
                  response.sendRedirect("OpenComm.jsp?isview=y&tenderid="+objid+"&msg="+msg);
             }else if(fromaction.equalsIgnoreCase("eval")){
                 if(loi.equalsIgnoreCase("loi")){
                    response.sendRedirect("LOI.jsp?isview=y&tenderid="+objid+"&msg="+msg); 
                 }else{
                    response.sendRedirect("EvalComm.jsp?isview=y&tenderid="+objid+"&msg="+msg);
                 }
             }else if(fromaction.equalsIgnoreCase("amendment")){
                  response.sendRedirect("Amendment.jsp?tenderid="+objid+"&msg="+msg);
             }else if(fromaction.equalsIgnoreCase("workflow")){
                   response.sendRedirect("PendingProcessing.jsp?viewtype=pending");
             }else if(fromaction.equalsIgnoreCase("pretender")){
                  response.sendRedirect("PreTenderMeeting.jsp?tenderId="+objid+"&msg="+msg);
            }else if(fromaction.equalsIgnoreCase("tsceval")){
                  response.sendRedirect("EvalComm.jsp?tenderid="+objectid+"&msg="+msg);
          }else if(fromaction.equalsIgnoreCase("Termination")){
                  response.sendRedirect("TabContractTermination.jsp?tenderId="+objectid+"&msg="+msg);
          }else if(fromaction.equalsIgnoreCase("Variation")){
                  response.sendRedirect("DeliverySchedule.jsp?tenderId="+Integer.toString(tenderId)+"&msg="+msg);
          }else if(fromaction.equalsIgnoreCase("RepeatOrder")){
                  response.sendRedirect("repeatOrderMain.jsp?tenderId="+Integer.toString(tenderId)+"&msg="+msg);
          }else if(fromaction.equalsIgnoreCase("VariforServices")){
                  response.sendRedirect("WorkScheduleMain.jsp?tenderId="+Integer.toString(tenderId)+"&msg="+msg);
          }else{%>
          <script type="text/javascript">
          jAlert("File already processed"," Alert ", "Alert");
          </script>
        <%
            
        

       } 

       if(eventName.equals("Cancel Tender") && action_param.equalsIgnoreCase("Approve"))
       {
            
            CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");
            List<Object> list2 = committeMemberService.cancelTenderMail(childid);
            MailContentUtility mc2 = new MailContentUtility();
            SendMessageUtil smu2 = new SendMessageUtil(); 
            String mailTextNew = null;
            if(!list2.isEmpty()){
                 mailTextNew = mc2.cancelTenderMail(childid, list2.get(0).toString(), list2.get(1).toString());
            }
            for (int i = 2; i <list2.size(); i++) {
                 String[] mail = {list2.get(i).toString()};
                 smu2.setEmailTo(mail);
                 smu2.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                 smu2.setEmailSub("Tender Cancel Notice");
                 smu2.setEmailMessage(mailTextNew);
                 smu2.sendEmail();
            }

       }

}else{
               if(actid == 4 || actid == 1){
               int tempchildid = Integer.parseInt(objectid);
           tblWorkFlowLevelConfig = workFlowSrBean.getWorkFlowLevelConfig(uid,
                  Short.parseShort(eventid), Integer.parseInt(objectid), tempchildid);
           }
           else
           {
                   if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                        objectid = childid;
                   }
                   tblWorkFlowLevelConfig = workFlowSrBean.getWorkFlowLevelConfig(uid,
                   Short.parseShort(eventid), Integer.parseInt(objectid), Integer.parseInt(childid));
                   if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                        objectid = request.getParameter("objectid");
                   }
           }
           if(tblWorkFlowLevelConfig.size()>0) {
             Iterator wflc =   tblWorkFlowLevelConfig.iterator();
             while(wflc.hasNext()){
          TblWorkFlowLevelConfig twfl = (TblWorkFlowLevelConfig) wflc.next();
          if(twfl.getFileOnHand().equalsIgnoreCase("Yes") && !fromaction.equalsIgnoreCase("workflow")){
           wflevel =  twfl.getWfLevel();
           wfroleid =  twfl.getWfRoleId();
           fileOnHand = twfl.getFileOnHand();
           }else if(fromaction.equalsIgnoreCase("workflow")){
              wflevel =  twfl.getWfLevel();
              wfroleid =  twfl.getWfRoleId();
              fileOnHand = twfl.getFileOnHand();
              }
             }
             }
              /* Added by Dohatec for Tender Publish by workflow Approver Start*/
             if("2".equalsIgnoreCase(activityid) && "2".equalsIgnoreCase(eventid) && wfroleid==2){
                List<TblTenderDetails> detailsTenderPublishDates = tenderSrBean.getTenderDetails(Integer.parseInt(objectid));
                SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
                //String pubDate = dateFormat2.format(detailsTenderPublishDates.get(0).getTenderPubDt());
                Date pubDates = detailsTenderPublishDates.get(0).getTenderPubDt();
                Date CurrDate = new  Date();
                if (CurrDate.compareTo(pubDates)>0)
                {
                    %>
                     <input name="txtTenderDocumentTimeElapsed" type="hidden" value="<%=false%>" id="txtTenderDocumentTimeElapsed"  />
                     <%
                }
                else
                    {%>
                     <input name="txtTenderDocumentTimeElapsed" type="hidden" value="<%=true%>" id="txtTenderDocumentTimeElapsed"  />
                     <%
                }
           }
               /* Added by Dohatec for Tender Publish by workflow Approver End*/
       List<TblWorkflowRuleEngine> tblWorkflowRuleEngine =  workFlowSrBean.isPublishDateRequire(Short.parseShort(eventid));
       wfactions = workFlowSrBean.getWorkFlowActions(action, eventid, "");
      if(wfactions.size()>0){
      CommonAppData data =  wfactions.get(0);
      eventName = data.getFieldName3();
      modName =  data.getFieldName4();
      }
        if(tblWorkflowRuleEngine.size()>0) {
        wfr =   tblWorkflowRuleEngine.get(0);
       if(wfr.getIsPubDateReq().equalsIgnoreCase("Yes"))
           ispublisdateReq = wfr.getIsPubDateReq();
       }
       
            %>
            <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <div class="contentArea_1">
           <div class="pageHead_1">
               <% 
                if((!fromaction.equalsIgnoreCase("workflow") && fileOnHand.equalsIgnoreCase("Yes") && tblWorkFlowLevelConfig.size() > 0) ||
                (fromaction.equalsIgnoreCase("workflow") && !fileOnHand.equalsIgnoreCase("Yes") && wfroleid == 2 && wfroleid == 3 && tblWorkFlowLevelConfig.size() > 0)){%>Process file in Workflow<%}else{%>Approved Workflow<%}%>
            <div align="right" style="width: 30%;float: right;" >
                <% if(fromaction.equalsIgnoreCase("dashboard") || fromaction.equalsIgnoreCase("pkgdashboard")){%>
           <a class="action-button-goback" href="APPDashboard.jsp?appID=<%=objectid%>">Go back to Dashboard</a>
           <% }else if(fromaction.equalsIgnoreCase("notice")){ %>
           <a class="action-button-goback" href="Notice.jsp?tenderid=<%=objectid%>">Go back to Tender Dashboard</a>
           <% }else if(fromaction.equalsIgnoreCase("opening")){ %>
           <a class="action-button-goback" href="OpenComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
           <% }else if(fromaction.equalsIgnoreCase("eval")){ if(loi.equalsIgnoreCase("loi")){ %>
           <a class="action-button-goback" href="LOI.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
           <%}else{%>
           <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
           <% }}else if(fromaction.equalsIgnoreCase("amendment")){ %>
           <a class="action-button-goback" href="Amendment.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
           <% }else if(fromaction.equalsIgnoreCase("pretender")){ %>
           <a class="action-button-goback" href="PreTenderMeeting.jsp?tenderId=<%=objectid%>">Go back to Dashboard</a>
           <% }else if(fromaction.equalsIgnoreCase("tsceval")){ %>
            <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
           <% }else if(fromaction.equalsIgnoreCase("Termination")){  %>
           <a class="action-button-goback" href="TabContractTermination.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
           <% }else if(fromaction.equalsIgnoreCase("Variation")){  %>
           <a class="action-button-goback" href="DeliverySchedule.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
           <%}else if(fromaction.equalsIgnoreCase("RepeatOrder")){  %>
           <a class="action-button-goback" href="repeatOrderMain.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
           <%}else if(fromaction.equalsIgnoreCase("VariforServices")){  %>
           <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
           <%}else if(fromaction.equalsIgnoreCase("workflow")){  %>
           <a class="action-button-goback" href="PendingProcessing.jsp?viewtype=defaultworkflow">Go back to Dashboard</a>
           <%}else if(fromaction.equalsIgnoreCase("pending") && activityid.equalsIgnoreCase("13")){  %> <!-- Added By Dohatec-->
           <a class="action-button-goback" href="PendingProcessing.jsp?viewtype=pending">Go back to Dashboard</a>
           <%}%>
            </div></div>
            <div>&nbsp;</div>
            
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <form id="frmFileProcessing"  name="fileProcessing" method="post">
                            <input type="hidden" name="moduleName" value="<%=modName%>" />
                            <input type="hidden" name="eventName" value="<%=eventName%>" />
                             <tr>
                                 <td class="ff" width="12%">File Details :</td>
                                 <td width="88%"><% if(actid == 1 || actid == 7){ %>
                                     <a href="APPWorkflowView.jsp?appid=<%=objectid%>&isfileproc=Yes" target="_blank" >View</a>
                                     <% }else if(actid == 2){ %>
                                     <a href="../resources/common/ViewTender.jsp?id=<%=objectid%>" target="_blank" >View Tender Notice</a>&nbsp;|&nbsp;
                                     <% if(isTenPackageWise){ %>
                                        <a href="TenderDocView.jsp?tenderId=<%=objectid%>" target="_blank" >View Tender Document</a>
                                     <% }else{ %>
                                        <a href="LotwiseTenderDocView.jsp?tenderid=<%=objectid%>" target="_blank" >View Tender Document</a>
                                     <% } %>
                                     <% }else if(actid == 6){ %>
                                     <a href="CommFormation.jsp?tenderid=<%=objectid%>&isview=y" target="_blank" >View</a>
                                     <% }else if(actid == 4){ %>
                                     <a href="ViewAmendment.jsp?tenderId=<%=objectid%>" target="_blank" >View</a>
                                     <% }else if(actid == 5){ %>
                                     <a href="OpenCommFormation.jsp?tenderid=<%=objectid%>&isview=y" target="_blank" >View</a>
                                     <% }else if(actid == 3){ %>
                                     <a href="../resources/common/PreTenderQueRep.jsp?tenderId=<%=objectid%>&viewType=Prebid&view=Docs" target="_blank" >View</a>
                                     <% }else if(actid == 8){ %>
                                     <a href="TSCFormation.jsp?tenderid=<%=objectid%>&isview=y" target="_blank" >View</a>
                                     <% }else if(actid == 9){ %>
                                     <a href="<%=request.getContextPath()%>/resources/common/ViewTender.jsp?id=<%=objectid%>" target="_blank" >View Notice</a>&nbsp;|&nbsp;
                                     <a href="<%=request.getContextPath()%>/officer/ViewCancelTender.jsp?tenderid=<%=objectid%>" target="_blank" >View Cancel Reason</a>
                                     <% }else if(actid == 10){ %>
                                     <a href="ViewCTerminationWFFile.jsp?tenderId=<%=objectid%>&childid=<%=request.getParameter("childid")%>" target="_blank" >View</a>
                                     <%}else if (actid == 11){
                                        if(wpId!=null && !"".equalsIgnoreCase(wpId) && !"null".equalsIgnoreCase(wpId))
                                        {
                                            if("0".equalsIgnoreCase(wpId)){    
                                        %>
                                                <a href="ViewAllVariOrderForms.jsp?tenderId=<%=request.getParameter("tenderId")%>&childid=<%=request.getParameter("childid")%>&lotId=<%=lotId%>" target="_blank" >View</a>
                                        <%    
                                            }
                                        }else{
                                            if(wpId==null || "".equalsIgnoreCase(wpId) || "null".equalsIgnoreCase(wpId)){
                                            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                            List<Object[]> object= service.getTenderID(Integer.parseInt(childid));
                                            if(!object.isEmpty())
                                            {
                                                Object[] obj = object.get(0);
                                                wpId = obj[1].toString();
                                            }
                                            }
                                            if("0".equalsIgnoreCase(wpId)){
                                        %>
                                                <a href="ViewAllVariOrderForms.jsp?tenderId=<%=request.getParameter("tenderId")%>&childid=<%=request.getParameter("childid")%>&lotId=<%=lotId%>" target="_blank" >View</a>
                                        <%          
                                            }else{
                                        %>
                                     <a href="ViewVariationFile.jsp?tenderId=<%=request.getParameter("tenderId")%>&childid=<%=request.getParameter("childid")%>&lotId=<%=lotId%>" target="_blank" >View</a>
                                     <%}}}else if(actid == 12){%>
                                     <a href="CreateROWf.jsp?tenderId=<%=request.getParameter("tenderId")%>&childid=<%=request.getParameter("childid")%>&lotId=<%=lotId%>" target="_blank" >View</a>
                                     <%} else if(actid == 13){ %>
                                     <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <td class="ff" width="12%">Tender Opening Reports :</td>
                                        <td width="88%"><a href="OpeningReports.jsp?tenderId=<%=objectid%>&lotId=0&comType=null" target="_blank" >View</a></td>
                                        <jsp:include page="TERInclude.jsp">
                                        <jsp:param name="tenderId" value="<%=s_tenderid%>"/>
                                        <jsp:param name="lotId" value="<%=s_lotId%>"/>
                                        <jsp:param name="roundId" value="<%=s_rId%>"/>
                                        <jsp:param name="evalType" value="<%=evalType%>"/>
                                        </jsp:include>
                                    </table>
                                        <% } %>

                                 </td>
                             </tr>
                        <tr>
                            <td class="ff" style="font-weight: bold;" >Module Name :</td>
                            <td><%=modName%></td>
                        </tr>
                        <tr>
                             <td class="ff" style="font-weight: bold;" >Process Name :</td>
                             <%if(loi.equalsIgnoreCase("loi")){%>
                                <td>
                                    LOA Workflow
                                </td>
                             <%}else{%>
                                <td>
                                    <%=eventName%>
                                </td>
                             <%}%>
                        </tr>

                        <% if((!fromaction.equalsIgnoreCase("worktflow") && fileOnHand.equalsIgnoreCase("Yes") && tblWorkFlowLevelConfig.size() > 0) ||
                        (fromaction.equalsIgnoreCase("workflow") && !fileOnHand.equalsIgnoreCase("Yes") && wfroleid == 2 && tblWorkFlowLevelConfig.size() > 0)){ 
                        if((!fromaction.equalsIgnoreCase("workflow") && fileOnHand.equalsIgnoreCase("Yes") && tblWorkFlowLevelConfig.size() > 0) ||
                        (fromaction.equalsIgnoreCase("workflow") && !fileOnHand.equalsIgnoreCase("Yes") && wfroleid == 2 && wfroleid == 3 && tblWorkFlowLevelConfig.size() > 0)){
                        
                        
                        %>
                        <tr>
                            <td class="ff">Comments : <span>*</span></td>
                            <td>
                                <textarea name="txtComments" rows="3" class="formTxtBox_1" id="txtComments" style="width:400px;"></textarea>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    CKEDITOR.replace( 'txtComments',
                                    {
                                       toolbar : "egpToolbar"
                                    });
                                    //]]>
                                </script>
                                <span id="s_textarea" style="color: red; ">&nbsp;</span>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Action : <span>*</span></td>
                            <td>
                                <select name="action" class="formTxtBox_1" id="txtAction" style="width:200px;" onchange="ActionChanges();">
                                    <option value="SelectAction">-- Select Action --</option>
                                    <%
                                    boolean isTechinicalSigned = false;
                                     List<SPCommonSearchDataMore> listTechSign = dataMore.getCommonSearchData("isTERSentToAAApp", s_tenderid, s_lotId, "0","0");
                                    if(listTechSign!=null && (!listTechSign.isEmpty()) && (listTechSign.get(0).getFieldName1().equals("1"))){
                                        isTechinicalSigned = true;
                                    }
                                   Iterator ac = wfactions.iterator();
                                   short levelfromconfig = wflevel;
                                   while(ac.hasNext()){
                                    String actionNew = "";
                                    CommonAppData actionparam = (CommonAppData) ac.next();
                                       wflevel = levelfromconfig;
                                       if(!actionparam.getFieldName2().trim().equalsIgnoreCase("Pull")){
                                            if(actionparam.getFieldName2().trim().equalsIgnoreCase("Return")) {
                                              actionNew = "Return for Modification";
                                            }else{
                                              actionNew = actionparam.getFieldName2();
                                            }
                                       if((wfroleid == 1 || wfroleid == 3) && actionparam.getFieldName2().equalsIgnoreCase("Forward")){
                                               wflevel=(short)(wflevel+1);
                                               out.write("<option value='"+actionparam.getFieldName2()+"_"+wflevel+"'>"+actionNew+"</option>");
                                        }else if(wfroleid == 2 && actionparam.getFieldName2().equalsIgnoreCase("Approve") && !fromaction.equalsIgnoreCase("workflow")){
                                                wflevel=1;
                                                out.write("<option value='"+actionparam.getFieldName2()+"_"+wflevel+"'>"+actionNew+"</option>");
                                         }else if(wfroleid == 2 && actionparam.getFieldName2().equalsIgnoreCase("Pull") && fromaction.equalsIgnoreCase("workflow")){
                                                wflevel=wflevel;
                                                out.write("<option value='"+actionparam.getFieldName2()+"_"+wflevel+"'>"+actionNew+"</option>");
                                         }else if((wfroleid == 2 || wfroleid == 3) &&actionparam.getFieldName2().equalsIgnoreCase("Return") && !fromaction.equalsIgnoreCase("workflow")){
                                             
                                             if(!activityid.equalsIgnoreCase("13")){ // This condition added by dTec to eliminate return option form NOA approval(7th march,2017)
                                                wflevel=(short)(wflevel-1);
                                                out.write("<option value='"+actionparam.getFieldName2()+"_"+wflevel+"'>"+actionNew+"</option>");
                                             }
//                                         }else if(wfroleid == 2 &&actionparam.getFieldName2().equalsIgnoreCase("Reject") && !fromaction.equalsIgnoreCase("workflow")){
//                                                  wflevel = 1;
//                                                 out.write("<option value='"+actionparam.getFieldName2()+"_"+wflevel+"'>"+actionNew+"</option>");
//                                        }else if(wfroleid == 2 &&actionparam.getFieldName2().equalsIgnoreCase("Conditional Approval") && !fromaction.equalsIgnoreCase("workflow")){
//                                                  wflevel = 1;
//                                                 out.write("<option value='"+actionparam.getFieldName2()+"_"+wflevel+"'>"+actionparam.getFieldName2()+"</option>");
//                                        }
                                        }else if(wfroleid == 4 && actionparam.getFieldName2().equalsIgnoreCase("An Objection")){
                                                  wflevel=(short)(wflevel-1);
                                                 out.write("<option value='"+actionparam.getFieldName2()+"_"+wflevel+"'>"+ actionNew +"</option>");
                                        }else if(wfroleid == 4 && actionparam.getFieldName2().equalsIgnoreCase("No Objection")){
                                                 wflevel=(short)(wflevel+1);
                                                 out.write("<option value='"+actionparam.getFieldName2()+"_"+wflevel+"'>"+actionNew+"</option>");
                                            }
                                       else if(wfroleid == 2 && actionparam.getFieldName2().equalsIgnoreCase("Re-evaluation") && !fromaction.equalsIgnoreCase("workflow") && activityid.equalsIgnoreCase("13") && !isTechinicalSigned) {
                                                String procMethod = commonService.getProcMethod(objectid).toString();
                                              // if((procMethod.equalsIgnoreCase("LTM") || procMethod.equalsIgnoreCase("OTM") || procMethod.equalsIgnoreCase("RFQU") || procMethod.equalsIgnoreCase("OSTETM")))
                                                 wflevel = 1;
                                                 out.write("<option value='"+actionparam.getFieldName2()+"_"+wflevel+"'>"+actionNew+"</option>");
                                            }
                                        }
                                     }
                                   
                                    %>

                                </select>

                            </td>
                        </tr>
                        <%}%>
                         <tr id="reTenderTr" style="display: none;">
                                            <td class="ff">Re - Tendering :  &nbsp;<span class="mandatory">*</span></td>
                                            <td><input name="reTendering" type="radio" value="yes" id="reTendering"/> Yes &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input name="reTendering" type="radio" value="No" id="reTenderingNo" /> No</td>
                        </tr>
                        <% if(!fromaction.equalsIgnoreCase("workflow")){ %>
                        <tr>
                            <td class="ff">Upload Document   :</td>
                            <td><label id="filenames"></label>
                                <div><img src="../resources/images/Dashboard/uploadIcn.png" alt="Upload" class="linkIcon_1" />
                                    <a href="#" title="Upload" onclick="window.open('UploadFileDialog.jsp?activityid='+<%= actid %>+'&objectid='+<%= objid %>+'&userid='+<%= uid %> +'&childid='+<%= chilid %>+'', 'UpLoadFileDialog', 'width=700,height=500')">Click here If any relevant documents to be uploaded </a></div>
                            </td>
                        </tr>
                        <tr ><td class="ff" ><label id="filelable"></label>
                            </td>
                            <td>
                                <div id="docData">
                             </div>
                            </td>
                         
                         </tr>
                        <% } } %>
                        <tr>
                            <td class="ff">Workflow History :</td>
                            <td>
                                <table width="100%" cellspacing="0" class="tableList_1">
                              <tr>
                                  <th style="width: 4%">Sl. <br/>No</th>
                                <th>ID</th>
                                <th>Processed By</th>
                                <th>Processed Date and Time</th>
                                <th>Action</th>
                                <th>Comments</th>
                                <th>To Be Processed By</th>
                                <th>Download</th>
                              </tr>
                              <%
                              String hisaction = null;
                               int c = 2;
                                String colorchange = "style='background-color:#F0E68C;' ";
                              if(fromaction.equalsIgnoreCase("pkgdashboard") || (Integer.parseInt(objectid) != Integer.parseInt(childid) && Integer.parseInt(activityid) == 1 )){
                                  hisaction = "pkghistory";
                              }
                              else {
                                    hisaction = "History";
                              }
                              if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                                objectid = childid;
                              }
                             List<CommonSPWfFileHistory> commonSpWfFileHistory = workFlowSrBean.getWfHistory(hisaction, Integer.parseInt(activityid),Integer.parseInt(objectid),Integer.parseInt(childid) ,uid, 1, 10,"processDate","desc","","","",0,"","");
                             if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                                objectid = request.getParameter("objectid");
                             }
                              request.setAttribute("commonSpWfFileHistory", commonSpWfFileHistory);
                                    if(commonSpWfFileHistory.size()>0){
                                        Iterator wf = commonSpWfFileHistory.iterator();
                                        int i = 1;
                                        while(wf.hasNext()){
                                            CommonSPWfFileHistory spwfh =(CommonSPWfFileHistory) wf.next();
                                               
                                             %>
                                             <tr <% if(c == i) out.print(colorchange); %>>
                                  <td class="t-align-center"><%=spwfh.getRowNumber()%></td>
                                  <td class="t-align-center"><%=spwfh.getChildId()%></td>
                                  <td class="t-align-left"><%= spwfh.getFileSentFrom()%></td>
                                  <td class="t-align-center"><%=DateUtils.gridDateToStr(spwfh.getProcessDate())%></td>
                                  <td class="t-align-center">
                                      <%
                                        if("Approve".equalsIgnoreCase(spwfh.getAction())){
                                                out.print("Approved");
                                        } else if ("Forward".equalsIgnoreCase(spwfh.getAction())){
                                                out.print("Forwarded");
                                        } else if ("Pull".equalsIgnoreCase(spwfh.getAction())){
                                                out.print("Pulled");
                                        } else if ("Return".equalsIgnoreCase(spwfh.getAction())){
                                                out.print("Returned");
                                        } else if ("Reject".equalsIgnoreCase(spwfh.getAction())){
                                                out.print("Rejected");
                                        } else if ("Conditional Approval".equalsIgnoreCase(spwfh.getAction())){
                                                out.print("Conditional Approved");
                                        } else{
                                                out.print("<cell>" + spwfh.getAction() + "</cell>");
                                        }
                                      %>
                                  </td>
                                  <td class="t-align-center">
                                   <a href="#" title="comments" onclick="window.open('CommentsView.jsp?wfhistoryid=<%=spwfh.getWfHistoryId() %>', 'Comments', 'resizable=yes,scrollbars=yes,width=500,height=300')">View</a>
                                   </td>
                                  <td class="t-align-left"><%= spwfh.getFileSentTo()%></td>
                                  <td class="t-align-center">
                                       <% 
                                       String docids = "";
                                       if("".equals(spwfh.getDocId())){
                                                %>
                                      <label>No Files Uploaded</label>
                                      <% }else{
                                      docids = spwfh.getDocId();
                                         docids = docids.replaceAll(",", "dc");
                                            %>
                                      <a href="#" title="download" onclick="window.open('DownLoadFilesDialog.jsp?activityid=<%=actid%>&objectid=<%= objid %>&docids=<%=docids%>&childid=<%= chilid %>', 'FileDownloadDialog', 'width=700,height=500')"><img src='../resources/images/Dashboard/Download.png' alt='Download' ></a>
                                      <% } %>
                                  </td>
                                             </tr>
                                  <%
                                    
                                   if(c == i)
                                      c+=2;
                                     i++;
                                    }
                                        }else{ %>
                                      <tr><td class="t-align-center" colspan="8"> <label>No Records Found</label></td></tr>
                                      <% } %>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Workflow Level :</td>
                            <td><jsp:include page="WorkFlowLevelViewGrid.jsp"></jsp:include></td>
                        </tr>
                        <%  if((!fromaction.equalsIgnoreCase("workflow") && fileOnHand.equalsIgnoreCase("Yes") && tblWorkFlowLevelConfig.size() > 0) ||
                        (fromaction.equalsIgnoreCase("workflow") && !fileOnHand.equalsIgnoreCase("Yes") && wfroleid == 2 && wfroleid == 3 && tblWorkFlowLevelConfig.size() > 0)){ %>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="center"><label class="formBtn_1">
                                    <input type="submit" name="submit" id="tbnAdd" value="Submit" onclick="return Validate();" />
                                    <input type="hidden" name="eventid" id="eventid" value="<%=eventid%>" />
                                    <input type="hidden" name="childid" id="childid" value="<%=childid%>" />
                                    <input type="hidden" name="objectid" id="objectid" value="<%=objectid%>" />
                                    <input type="hidden" name="activityid" id="activityid" value="<%=activityid %>" />
                                    <input type="hidden" name="loi" id="loi" value="<%=loi%>"/> 
                                    <input type="hidden" name="fromaction" id="fromaction" value="<%=fromaction%>" />
                                    <input name="txtActivitiyID" type="hidden" value="<%=activity_ID%>" id="txtActivitiyID"  />
                                </label>
                            </td>
                        </tr>
                        <% } %>
                        </form>
                    </table>
                
            
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
             <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <% } %>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabWorkFlow");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
<script type="text/javascript">
  function getDocData(){
      $.post("<%=request.getContextPath()%>/workflowdocservlet", {action:'getdocs',objectid:<%=objectid%>,activityid:<%=activityid%>,childid:<%=childid%>,userid:<%=uid%>}, function(j){
             document.getElementById("docData").innerHTML = j;
      });
  }
</script>