<%--
    Document   : ProgressReportUploadDoc
    Created on : Aug 19, 2011, 11:10:29 AM
    Author     : dixit
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetailDocs"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrDocument"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ProgressReportUploadDocService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceDocument"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Reference Document</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        //uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:50}
                    },
                    messages: {
                        //uploadDocFile: { required: "<div class='reqF_1'>Please select Document.</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please enter Description.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed.</div>"}
                    }
                });
            });
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if(document.getElementById("uploadDocFile").value=="")
                    {document.getElementById("docspan").innerHTML="please select Document";return false;}
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                            browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpld').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body onload="getQueryData();">
        <%@include file="../resources/common/AfterLoginTop.jsp" %>
        <%
        ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
                    String tenderId = "";
                    String keyId = "";
                    String wpId = "";
                    String lotId = "";
                    String isedit = "";
                    String docx = "";
                    String module = "";
                    if (session.getAttribute("userId")==null) {
                        response.sendRedirect("SessionTimedOut.jsp");
                    }
                    if (request.getParameter("tenderId")!= null) {
                        tenderId = request.getParameter("tenderId");
                    }
                    if (request.getParameter("keyId")!= null) {
                        keyId = request.getParameter("keyId");
                    }
                    if (request.getParameter("wpId")!= null) {
                        wpId = request.getParameter("wpId");
                    }
                    if (request.getParameter("lotId")!= null) {
                        lotId = request.getParameter("lotId");
                    }
                    if (request.getParameter("isedit")!= null) {
                        isedit = request.getParameter("isedit");
                    }
                    if (request.getParameter("docx")!= null) {
                        docx = request.getParameter("docx");
                    }
                    if (request.getParameter("module")!= null) {
                        module = request.getParameter("module");
                    }
                    /*ProgressReportUploadDocService prudS = (ProgressReportUploadDocService) AppContext.getSpringBean("ProgressReportUploadDocService");                    
                    List<Object[]> prViewList = null;*/
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
                    ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <div class="dashboard_div">
                    <div class="contentArea_1">
                        <div class="pageHead_1">
                            <%if("Workprogram".equalsIgnoreCase(docx)){%>
                            Work Program Reference Document
                            <%}else{%>
                            Delivery Schedule Reference Document
                            <%}%>
                            <span style="float: right; text-align: right;">         
                                <a class="action-button-goback" href="DeliverySchedule.jsp?tenderId=<%=tenderId%>" title="Go back">Go Back</a>
                            </span>
                        </div>
                        <%if(!"Download".equalsIgnoreCase(docx)){%>
                        <div class="t_space"></div>
                        <%
                            String message="";
                            if(request.getParameter("msg")!=null)
                            {
                                message = request.getParameter("msg");
                            }
                        %>
                        <% if (message!=null) {
                           if ("edit".equalsIgnoreCase(message)) {
                        %>
                        <div class='responseMsg successMsg'><%=bdl.getString("CMS.dates.edit")%></div>
                        <%}else if ("sent".equalsIgnoreCase(message)) {%>
                        <div class='responseMsg successMsg'><%=bdl.getString("CMS.works.sentToTen")%></div>
                        <%}}%>
                        <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/DeliveryScheduleUploadDocServlet?funName=Upload" enctype="multipart/form-data" name="frmUploadDoc">
                            <input type="hidden" name="tenderId" value="<%=tenderId%>" />
                            <input type="hidden" name="keyId" value="<%=keyId%>" />
                            <input type="hidden" name="wpId" value="<%=wpId%>" />
                            <input type="hidden" name="lotId" value="<%=lotId%>" />
                            <input type="hidden" name="isedit" value="<%=isedit%>" />
                            <input type="hidden" name="docx" value="<%=docx%>" />
                            <input type="hidden" name="module" value="<%=module%>" />
                            <%
                                        if (request.getParameter("fq") != null) {
                                            if (request.getParameter("fq").equals("Removed") || request.getParameter("fq").equals("Uploaded")) {
                            %>
                            <div class="responseMsg successMsg" style="margin-top: 10px;">File <%=request.getParameter("fq")%> Successfully</div>
                            <%} else {%>
                            <div> &nbsp;</div>
                            <div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
                            <%
                                            }
                                        }
                                        if (request.getParameter("fs") != null) {
                            %>
                            <div> &nbsp;</div>
                            <div class="responseMsg errorMsg">
                                Max. file size of a single file must not exceed <%=request.getParameter("fs")%>MB, Acceptable file types are : <%=request.getParameter("ft")%>.
                            </div>

                            <%}%>
                            <div>&nbsp;</div>
<!--                            <div  class='responseMsg noticeMsg t-align-left'>Note: Supporting Documents may be Quality Check document, Invoices, Delivery Challan, Measurement Book, etc</div>-->
                            <table width="90%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space ">

                                <tr>
<!--                                    <td colspan="2" class="ff t-align-left" >Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>-->
                                </tr>
                                <tr>
                                    <td width="10%" class="ff t-align-left">Document   :<span class="reqF_1">*</span> </td>
                                    <td width="80%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                                        <span id="docspan" class="reqF_1"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Description : <span class="reqF_1">*</span></td>
                                    <td>
                                        <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                        <div id="dvDescpErMsg" class='reqF_1'></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <label class="formBtn_1"><input type="submit" name="btnUpld" id="btnUpld" value="Upload" /></label>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="100%"  class="t-align-left">Instructions</th>
                                </tr>
                                <tr>
                                    <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");%>
                                    <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                        <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                                </tr>
                            </table>                        
                        </form>
                        <%}%>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="4%" class="t-align-center">Sl.  No.</th>
                                <th class="t-align-center" width="23%">File Name</th>
                                <th class="t-align-center" width="32%">File Description</th>
                                <th class="t-align-center" width="7%">File Size <br />
                                    (in KB)</th>
                                <th class="t-align-center" width="">Uploaded By</th>
                                <th class="t-align-center" width="18%">Action</th>
                            </tr>
                            <%
                                        String sttr = "";
                                        if("Workprogram".equalsIgnoreCase(docx))
                                        {
                                            sttr = "WP";
                                        }else{
                                            sttr = "DS";
                                        }    
                                        List<TblCmsWpDetailDocs> getDeliverySchdata = c_ConsSrv.getDeliveryDocsDetails(Integer.parseInt(keyId));
                                        if (!getDeliverySchdata.isEmpty()) {
                                            for (int i = 0; i < getDeliverySchdata.size(); i++) {
                                                if(sttr.equalsIgnoreCase(getDeliverySchdata.get(i).getProcess()))
                                                {    
                            %>
                            <tr>
                                <td class="t-align-center"><%=(i + 1)%></td>
                                <td class="t-align-left"><%=getDeliverySchdata.get(i).getDocumentName()%></td>
                                <td class="t-align-left"><%=getDeliverySchdata.get(i).getDocDescription()%></td>
                                <td class="t-align-center"><%=(Long.parseLong(getDeliverySchdata.get(i).getDocSize()) / 1024)%></td>
                                <td class="t-align-center">
                                        <%String str = "";
                                        if (2 == getDeliverySchdata.get(i).getUserTypeId()) {
                                            out.print("Supplier");
                                        }else{
                                            out.print("PE Officer");
                                        }%>
                                </td>
                                <td class="t-align-center">
                                    <a href="<%=request.getContextPath()%>/DeliveryScheduleUploadDocServlet?docName=<%=getDeliverySchdata.get(i).getDocumentName()%>&docSize=<%=getDeliverySchdata.get(i).getDocSize()%>&tenderId=<%=tenderId%>&wpDetailDocId=<%=getDeliverySchdata.get(i).getWpDetailDocId()%>&keyId=<%=keyId%>&prRepId=<%=getDeliverySchdata.get(i).getKeyId()%>&wpId=<%=wpId%>&lotId=<%=lotId%>&docx=<%=docx%>&module=<%=module%>&funName=download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                    &nbsp;
                                    <%if (3 == getDeliverySchdata.get(i).getUserTypeId()) {%>
                                    <a href="<%=request.getContextPath()%>/DeliveryScheduleUploadDocServlet?&docName=<%=getDeliverySchdata.get(i).getDocumentName()%>&docSize=<%=getDeliverySchdata.get(i).getDocSize()%>&tenderId=<%=tenderId%>&wpDetailDocId=<%=getDeliverySchdata.get(i).getWpDetailDocId()%>&keyId=<%=keyId%>&prRepId=<%=getDeliverySchdata.get(i).getKeyId()%>&wpId=<%=wpId%>&lotId=<%=lotId%>&docx=<%=docx%>&module=<%=module%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                    <%}%>
                                </td>
                            </tr>
                            <%}
                                                                                                                   }} else {%>
                            <tr>
                                <td colspan="6" class="t-align-center">No records found.</td>
                            </tr>
                            <%}%>
                        </table>

                        <div>&nbsp;</div>
                    </div>
                </div></div></div>
                <%@include file="../resources/common/Bottom.jsp" %>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

