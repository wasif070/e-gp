<%--
    Document   : createTenderextra
    Created on : Dec 9, 2010, 9:39:27 PM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.Date"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>


        <title>JSP Page</title>
    </head>
    <body>
        <%          if ("Submit".equals(request.getParameter("hdnbutton"))) {
                        String userid = "";
                        HttpSession hs = request.getSession();
                        TenderSrBean tenderSrBean = new TenderSrBean();
                        if (hs.getAttribute("userId") != null) {
                            userid = hs.getAttribute("userId").toString();
                            tenderSrBean.setLogUserId(userid);
                        }


                        String combineTenderSecurityAmount = request.getParameter("combineTenderSecurityAmount");
                        if(request.getParameter("chkSecAmt")!=null && !"".equalsIgnoreCase(request.getParameter("chkSecAmt").trim())){
                            combineTenderSecurityAmount = request.getParameter("securityAmountService");
                        }
                        System.out.println("Security Amount------->"+combineTenderSecurityAmount);
                       
                        String combineBidSecurityType = request.getParameter("combineBidSecurityType");
                        
                        //if ("Submit".equals(request.getParameter("submit"))) {
                        String docFessMethod = "Package wise";
                        if (request.getParameter("docFees") == null) {
                            docFessMethod = "Package wise";
                        } else {
                            docFessMethod = request.getParameter("docFees");
                        }
                        String docsFeesMode = "";
                        if (request.getParameter("docFeesMode") != null) {
                            docsFeesMode = request.getParameter("docFeesMode");
                        }
                        /*String[] docsFeesModeArray = null;
                        if (request.getParameterValues("docFeesMode") == null) {
                            docsFeesMode = "";
                        } else {
                            docsFeesModeArray = request.getParameterValues("docFeesMode");
                            for (int i = 0; i < docsFeesModeArray.length; i++) {
                                docsFeesMode += docsFeesModeArray[i];
                                docsFeesMode += "/";
                            }
                            docsFeesMode = docsFeesMode.substring(0, docsFeesMode.length() - 1);
                        }*/

                        String docOfficeAdd = "";
                        if (request.getParameter("nameAddressTenderDoc") == null) {
                            docOfficeAdd = "";
                        } else {
                            docOfficeAdd = request.getParameter("nameAddressTenderDoc");
                        }

                        int passingMarks = 0;
                        if (request.getParameter("passingMarks") == null || request.getParameter("passingMarks").trim().equals("")) {
                            passingMarks = 0;
                        } else {
                            passingMarks = Integer.parseInt(request.getParameter("passingMarks"));
                        }

                        String contractType = "";
                        if (request.getParameter("contractType") == null) {
                            contractType = "";
                        } else {
                            contractType = request.getParameter("contractType");
                        }
                        String deliverables = "";
                        if (request.getParameter("expRequired") == null) {
                        } else {
                            deliverables = request.getParameter("expRequired");
                        }

                        String otherDetails = "";
                        if (request.getParameter("otherDetails") == null) {
                        } else {
                            otherDetails = request.getParameter("otherDetails");
                        }
                        String foreignFirm = "";
                        if (request.getParameter("assoForiegnFirm") == null) {
                        } else {
                            foreignFirm = request.getParameter("assoForiegnFirm");
                        }
                        String docAvlMethod = "Package";
                        if (request.getParameter("docAvailable") == null) {
                        } else {
                            docAvlMethod = request.getParameter("docAvailable");
                        }
                        String securitySubOff = "";
                        if (request.getParameter("nameAddressTenderSub") == null) {
                        } else {
                            securitySubOff = request.getParameter("nameAddressTenderSub");
                        }
                        String invitationRefNo = "";
                        if (request.getParameter("invitationRefNo") == null) {
                        } else {
                            invitationRefNo = request.getParameter("invitationRefNo");
                        }
                        String tenderId = "";
                        if (request.getParameter("tenderId") == null) {
                        } else {
                            tenderId = request.getParameter("tenderId");
                        }
                        // Dohatec Start
                        String preQualDocPriceUSD = "";
                        if (request.getParameter("preQualDocPriceUSD") == null || request.getParameter("preQualDocPriceUSD").trim().equalsIgnoreCase("")) {
                            preQualDocPriceUSD = "0";
                        } else {
                            preQualDocPriceUSD = request.getParameter("preQualDocPriceUSD");
                        }
                        String combineTenderSecurityAmountUSD = "";
                        if (request.getParameter("combineTenderSecurityAmountUSD") == null || request.getParameter("combineTenderSecurityAmountUSD").trim().equalsIgnoreCase("")) {
                            combineTenderSecurityAmountUSD = "0";
                        } else {
                            combineTenderSecurityAmountUSD = request.getParameter("combineTenderSecurityAmountUSD");
                        }
                        // Dohatec End
                        String preQualDocPrice = null;
                        //if (request.getParameter("preQualDocPrice") == null) {
                        if (request.getParameter("preQualDocPrice") == null || request.getParameter("preQualDocPrice").trim().equalsIgnoreCase("")) {
                            preQualDocPrice = "0";
                        } else {
                            preQualDocPrice = request.getParameter("preQualDocPrice");
                        }
                        //tenderSrBean.updateSPAddYpTEndernotice(0, 0, userid, invitationRefNo, DateUtils.convertDateToStr(request.getParameter("tenderLastSellDate")), DateUtils.convertDateToStr(request.getParameter("preTenderMeetStartDate")), DateUtils.convertDateToStr(request.getParameter("preTenderMeetEndDate")), DateUtils.convertDateToStr(request.getParameter("preQualCloseDate")), DateUtils.convertDateToStr(request.getParameter("preQualOpenDate")), request.getParameter("eligibilityofTenderer"), request.getParameter("briefDescGoods"), deliverables, otherDetails, foreignFirm, docAvlMethod, request.getParameter("evaluationType"), docFessMethod, docsFeesMode, docOfficeAdd, DateUtils.convertDateToStr(request.getParameter("lastDateTenderirSub")), securitySubOff, request.getParameter("combineLocationLot"), request.getParameter("combineDocFeesLot"), request.getParameter("combineTenderSecurityAmount"), request.getParameter("combineComplTimeLotNo"), "update", Integer.parseInt(id), 0, request.getParameter("combineRefNo"), request.getParameter("combinePOS"), request.getParameter("combineLocation"), request.getParameter("combineIndicativeStartDate"), request.getParameter("combineIndicativeComplDate"), " ", " ", request.getParameter("combinetenderLotSecId"), contractType, DateUtils.convertDateToStr(request.getParameter("tenderpublicationDate")), "0.00", new BigDecimal(0.00));

                        Date preTenderMeetStartDate = null;
                        if ( request.getParameter("preTenderMeetStartDate")==null || request.getParameter("preTenderMeetStartDate").equals("")) {
                        } else {
                            preTenderMeetStartDate = DateUtils.convertDateToStr(request.getParameter("preTenderMeetStartDate"));
                        }
                        Date preTenderMeetEndDate = null;
                        if (request.getParameter("preTenderMeetEndDate")==null || request.getParameter("preTenderMeetEndDate").equals("")) {
                        } else {
                            preTenderMeetEndDate = DateUtils.convertDateToStr(request.getParameter("preTenderMeetEndDate"));
                        }

                        if (request.getParameter("QcbsHdn")!=null){
                            int weightforTech = Integer.parseInt(request.getParameter("weightPer"));
                            int weightforFin = 100 - weightforTech;
                            tenderSrBean.addEvalServiceWeightage(weightforTech, weightforFin, Integer.parseInt(tenderId));

                        }
                        String ProcurementMethod ="";                       
                        if (request.getParameter("hdnProcurementMethod")!=null){
                            ProcurementMethod = request.getParameter("hdnProcurementMethod");
                        }      
                        String eventType = "";
                        if (request.getParameter("hdnEventType")!=null){
                            eventType = request.getParameter("hdnEventType");
                        }
                        
                        
                    String W1 = "", W2 = "", W3 = "", W4 = "";
                    String W11 = "", W22 = "", W33 = "", W44 = "";
                    String WC1 = "", WC2 = "", WC3 = "", WC4 = "";
                    String bidCategory = "";
                    if (request.getParameter("All") != null) {
                        bidCategory = "W1, W2, W3, W4";
                        WC1= "1"; WC2="2" ; WC3="3"; WC4="4";
                        W11 = request.getParameter("W1");
                        W22 = request.getParameter("W2");
                        W33 = request.getParameter("W3");
                        W44 = request.getParameter("W4");
                    } else {
                        if (request.getParameter("W1") != null) {
                            WC1= "1";
                            W1 = request.getParameter("W1");
                            W11 = request.getParameter("W1");
                        
                        }
                        if (request.getParameter("W2") != null) {
                            WC2="2" ;
                            W22 = request.getParameter("W2");
                       
                            if (request.getParameter("W1") == null) {
                                W2 = request.getParameter("W2");
                               
                            } else {
                                W2 = ", " + request.getParameter("W2");
                            }
                        }
                        if (request.getParameter("W3") != null) {
                            WC3="3";
                            W33 = request.getParameter("W3");
                        
                            if (request.getParameter("W1") == null && request.getParameter("W2") == null) {
                                W3 = request.getParameter("W3");
                                 
                            } else {
                                W3 = ", " + request.getParameter("W3");
                            }
                        }
                        if (request.getParameter("W4") != null) {
                            WC4="4";
                            W44 = request.getParameter("W4");
                            if (request.getParameter("W1") == null && request.getParameter("W2") == null && request.getParameter("W3") == null) {
                                W4 = request.getParameter("W4");
                                
                            } else {
                                W4 = ", " + request.getParameter("W4");
                            }
                        }
                        bidCategory = W1 + W2 + W3 + W4;
                    }
                    
                        
                        
                        
                        
                        
                        //tenderSrBean.updateSPAddYpTEndernotice(0, 0, userid, invitationRefNo, DateUtils.convertDateToStr(request.getParameter("tenderLastSellDate")), DateUtils.convertDateToStr(request.getParameter("preTenderMeetStartDate")), DateUtils.convertDateToStr(request.getParameter("preTenderMeetEndDate")), DateUtils.convertDateToStr(request.getParameter("preQualCloseDate")), DateUtils.convertDateToStr(request.getParameter("preQualOpenDate")), request.getParameter("eligibilityofTenderer"), request.getParameter("briefDescGoods"), deliverables, otherDetails, foreignFirm, docAvlMethod, request.getParameter("evaluationType"), docFessMethod, docsFeesMode, docOfficeAdd, DateUtils.convertDateToStr(request.getParameter("lastDateTenderirSub")), securitySubOff, request.getParameter("combineLocationLot"), request.getParameter("combineDocFeesLot"), request.getParameter("combineTenderSecurityAmount"), request.getParameter("combineComplTimeLotNo"), "update", Integer.parseInt(tenderId), 0, request.getParameter("combineRefNo"), request.getParameter("combinePOS"), request.getParameter("combineLocation"), request.getParameter("combineIndicativeStartDate"), request.getParameter("combineIndicativeComplDate"), " ", " ", request.getParameter("combinetenderLotSecId"), contractType, DateUtils.convertDateToStr(request.getParameter("tenderpublicationDate")),preQualDocPrice , new BigDecimal(0.00));
                        tenderSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                        tenderSrBean.updateSPAddYpTEndernotice(bidCategory, W11, W22, W33, W44, 0, 0, userid, invitationRefNo, DateUtils.convertDateToStr(request.getParameter("tenderLastSellDate")), preTenderMeetStartDate, preTenderMeetEndDate, DateUtils.convertDateToStr(request.getParameter("preQualCloseDate")), DateUtils.convertDateToStr(request.getParameter("preQualOpenDate")), request.getParameter("eligibilityofTenderer"), request.getParameter("briefDescGoods"), deliverables, otherDetails, foreignFirm, docAvlMethod, request.getParameter("evaluationType"), docFessMethod, docsFeesMode, docOfficeAdd, DateUtils.convertDateToStr(request.getParameter("lastDateTenderSub")), securitySubOff, request.getParameter("combineLocationLot"), request.getParameter("combineDocFeesLot"), combineTenderSecurityAmount, combineTenderSecurityAmountUSD, request.getParameter("combineComplTimeLotNo"), request.getParameter("combineStartTimeLotNo"), "update", Integer.parseInt(tenderId), 0, request.getParameter("combineRefNo"), request.getParameter("combinePOS"), request.getParameter("combineLocation"), request.getParameter("combineIndicativeStartDate"), request.getParameter("combineIndicativeComplDate"), " ", " ", request.getParameter("combinetenderLotSecId"), contractType, DateUtils.convertDateToStr(request.getParameter("tenderpublicationDate")), preQualDocPrice, preQualDocPriceUSD, new BigDecimal(0.00),passingMarks,ProcurementMethod,combineBidSecurityType,eventType,"Create Tender Notice");
                        if(request.getParameter("chkSecAmt")!=null && !"".equalsIgnoreCase(request.getParameter("chkSecAmt").trim())){
                            combineTenderSecurityAmount = request.getParameter("securityAmountService");
                            tenderSrBean.serLotSercurity(tenderId, combineTenderSecurityAmount);
                        }
                        System.out.println("Security Amount------->"+combineTenderSecurityAmount);
                        response.sendRedirect("ConfigureKeyInfo.jsp?tenderId=" + tenderId);
                        tenderSrBean = null;
                    }
                    //tenderSrBean.updateSPAddYpTEndernotice(Integer.parseInt(id), 0, userid,commonTenderDetails.getReoiRfpRefNo(),commonTenderDetails.getDocEndDate(),commonTenderDetails.getPreBidStartDt(),commonTenderDetails.getPreBidEndDt(),commonTenderDetails.getSubmissionDt(),DateUtils.formatStdString(request.getParameter("preQualOpenDate")),commonTenderDetails.getEligibilityCriteria(),commonTenderDetails.getTenderBrief(),commonTenderDetails.getDeliverables(),commonTenderDetails.getOtherDetails(),commonTenderDetails.getForeignFirm(),commonTenderDetails.getDocAvlMethod(),request.getParameter("evaluationType"),commonTenderDetails.getDocFeesMethod(),commonTenderDetails.getDocFeesMode(),commonTenderDetails.getDocOfficeAdd(),DateUtils.formatStdString(request.getParameter("lastDateTenderSub")),commonTenderDetails.getSecuritySubOff(),commonTenderDetails.getLocation(),"docFees","TenderSecurityAmt");
                    //}
        %>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
