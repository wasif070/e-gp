<%-- 
    Document   : ViewAppRejTOExtReq
    Created on : Jun 16, 2011, 3:11:03 PM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        tenderId = request.getParameter("tenderId");
                    }
                    String extId = "";
                    if (request.getParameter("extId") != null) {
                        extId = request.getParameter("extId");
                    }
                    
                    // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    String idType="tenderId";
                    int auditId=Integer.parseInt(request.getParameter("tenderId"));
                    String auditAction="View Processed Oppening Date Extension Request";
                    String moduleName=EgpModule.Tender_Opening.getName();
                    String remarks="";
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
        %>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Approve Opening Date and Time Extension Request</title>
    </head>
    <body>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">

                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <div class="contentArea_1">
                    <div class="pageHead_1">Approve Opening Date and Time Extension Request<span style="float:right;"><a href="TOExtReqListing.jsp" class="action-button-goback">Go Back To Dashboard</a></span></div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <%
                                    TenderCommonService tCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    List<SPTenderCommonData> todview = tCommonService.returndata("getTenderExtComments", tenderId, extId);
                                    for (SPTenderCommonData details : todview) {%>
                        <tr>
                            <td width="21%" class="t-align-left ff">Newly Proposed Opening Date & Time:</td>
                            <td width="79%" class="t-align-left"><%=DateUtils.gridDateToStr(DateUtils.convertDateToStr(details.getFieldName3())).substring(0, DateUtils.gridDateToStr(DateUtils.convertDateToStr(details.getFieldName3())).length()-3) %></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Reason for Delay: </td>
                            <td class="t-align-left"><%=details.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Comments: </td>
                            <td class="t-align-left"><%=details.getFieldName2()%></td>
                        </tr>
                        <% }%>
                    </table>
                </div>
            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>

    </body>
</body>
<script type="text/javascript" language="Javascript">
    var headSel_Obj = document.getElementById("headTabEval");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }

</script>
</html>
