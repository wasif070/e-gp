<%-- 
    Document   : ViewTenderCombo
    Created on : Mar 31, 2011, 11:22:28 AM
    Author     : dixit
--%>

 <%
    StringBuffer colHeader = new StringBuffer();
    colHeader.append("'Sl. <br/> No.','Combo box Name','View','Action'");
 %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
         <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <%
                String tenderid="";
                if(!"".equalsIgnoreCase(request.getParameter("tenderId"))){
                tenderid = request.getParameter("tenderId");}
                String templetformid = "";
                if(!"".equalsIgnoreCase(request.getParameter("templetformid"))){
                templetformid = request.getParameter("templetformid");}

                String responsemsg = "";
                if(!"".equalsIgnoreCase(request.getParameter("msg"))){
                responsemsg = request.getParameter("msg");}
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Combo Box</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript"  language="javascript">
            /* jquery function for displaying jqgrid  */
            function loadGrid(){
                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    datatype: 'xml',
                    url : '<%=request.getContextPath()%>/CreateTenderComboSrBean?templetformID=<%=templetformid %>&action=fetchData&viewmessage=tenderer&tenderId=<%= tenderid %>',
                    height: 300,
                    colNames:[<%=colHeader %>],
                    colModel:[
                        {name:'Sr. No',index:'Sr. No', width:10,sortable: false, search: false, align:'center'},
                        {name:'listBoxName',index:'listBoxName', width:100, search: true,sortable: true,searchoptions: { sopt: ['eq', 'cn'] }},
                        {name:'process',index:'process', width:30,sortable: false, search: false},
                        {name:'action',index:'action', width:30,sortable: false, search: false,align:'center'}
                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    caption: " ",
                    searchGrid: {sopt:['cn','bw','eq','ne','lt','gt','ew']},
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false});
            }
            $(document).ready(function(){
                loadGrid();
            });
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr valign="top">
                        <%--<jsp:param name="userType" value="<%=userType.toString()%>"/>
                    </jsp:include>--%>
                        <%if("succ".equalsIgnoreCase(responsemsg)){%>
                        <div class="responseMsg successMsg t_space"><span>Combo box created successfully</span></div>
                        <%}%>
                        <td class="contentArea" style="vertical-align: top;" align="center" valign="middle">
                        <div class="pageHead_1 t-align-left">View Combo box
                            <%if(!"view".equalsIgnoreCase(request.getParameter("mode"))){%>
                        <div class="t-align-right b_space t_space">
<!--                            <a href="CreateTenderCombo.jsp?formId=<%=templetformid%>&tenderId=<%=tenderid%>" class="action-button-add"onclick="">Add Combo box</a>-->
                            <a href="TenderDocPrep.jsp?tenderId=<%= tenderid %><%if(request.getParameter("porlId")!=null){%>&porlId=<%=request.getParameter("porlId")%><%}%>" class="action-button-goback" title="Tender/Proposal Document">Tender Document</a>
                            </div>
                            <%}%>
<!--                        <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<a%= tenderid %>" class="action-button-goback" title="Tender Document">Tender Document</a></span>-->
                        
                        </div>
                        <div class="t-align-right t_space">

                        </div>
                        <table width="100%" cellspacing="0" class="t_space">
                            <tr valign="top">
                                <td>
                                    <div class="tabPanelArea_1">
                                        <div id="jQGrid" align="center" style="width: 98.3%;">
                                            <div><table id="list"></table></div>
                                            <div id="page"></div>
                                        </div>
                                    </div>
                                    <!--Bottom controls-->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pagging_1">

                                    </table>
                                    <div>&nbsp;</div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

            function deleteFile(tenderlistid)
            {
                $.alerts._show("Delete Combo", 'Do you really want to delete this combo?', null, 'confirm', function(r) {
                if (r == true){
                $.ajax({
                    url: "<%=request.getContextPath()%>/CreateTenderComboSrBean?tenderlistid="+tenderlistid+"&action=delete",
                    method: 'POST',
                    async: false,
                    success: function(j) {

                    }
                });
                jAlert("combo deleted successfully.","Combo Deleted", function(RetVal) {
                        });
                loadGrid();
                  }
        });

    }

    </script>
</html>
