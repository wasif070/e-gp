<%-- 
    Document   : EvalViewQueStatus
    Created on : May 9, 2011, 5:58:53 PM
    Author     : nishit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>View Posted Clarification</title>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
    <%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
    <%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
    <%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
    %>
</head>
<body>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->

        <div class="topHeader">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
        </div>
        <%
                    String tenderId = "0";
                    String formId = "0";
                    String uId = "0";
                    String st = "";
                    String strProcurementNature="";
                    boolean isMultiLots=false;
                    int intEnvelopcnt=0;
                    TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    
                    if (request.getParameter("tenderId") != null) {
                        tenderId = request.getParameter("tenderId");
                    }
                    if (request.getParameter("uId") != null) {
                        uId = request.getParameter("uId");
                    }
                    String userId = session.getAttribute("userId").toString();
                    if (request.getParameter("st") != null && !"".equalsIgnoreCase(request.getParameter("st"))) {
                        st = request.getParameter("st");
                    }
                    String viewIs = "";
                    boolean isView = false;
                    if (request.getParameter("viewIs") != null && !"".equalsIgnoreCase(request.getParameter("viewIs"))) {
                        viewIs = request.getParameter("viewIs");
                        if(viewIs.equals("Yes")){
                                isView = true;
                            }
                    }

                    /* START : CODE TO GET TENDER ENVELOPE COUNT */
                        List<SPTenderCommonData> lstCmnEnvelops =
                                objTSC.returndata("getTenderEnvelopeCount", tenderId, "0");

                        if(!lstCmnEnvelops.isEmpty()){
                            if(Integer.parseInt(lstCmnEnvelops.get(0).getFieldName1()) > 0){
                                intEnvelopcnt=Integer.parseInt(lstCmnEnvelops.get(0).getFieldName1());
                            }
                            if ("Yes".equalsIgnoreCase(lstCmnEnvelops.get(0).getFieldName2())){
                                isMultiLots=true;
                            }

                            strProcurementNature=lstCmnEnvelops.get(0).getFieldName3();
                        }
                        lstCmnEnvelops=null;

        %>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">View Posted Clarification
                <span style="float:right;">
                    <a href="Evalclarify.jsp?tenderId=<%=tenderId%>&st=cl" class="action-button-goback">Go back to Dashboard</a>
                </span>
            </div>
            <div class="t_space">
                <%
                CommonSearchService commonSearchService1 = (CommonSearchService)AppContext.getSpringBean("CommonSearchService");
                List<SPCommonSearchData> roleList = commonSearchService1.searchData("EvalMemberRole", tenderId, userId, null, null, null, null, null, null, null);
                boolean isCp = false;
                String role = "";
                if (!roleList.isEmpty()){
                    role = roleList.get(0).getFieldName2();
                    if(role.equalsIgnoreCase("cp")){
                            isCp = true;
                        }
                    roleList.clear();
                    roleList = null;
                }


                            if (request.getParameter("err") != null) {
                                String errMsg = request.getParameter("err");
                                    if (errMsg != null && errMsg.equals("Sucess")) {%>
                <div class="responseMsg successMsg" style="margin-top: 10px;">Question Posted <%=errMsg%>fully</div>
                <%} else if (errMsg != null) {%>
                <div class="responseMsg errorMsg" style="margin-top: 10px;">Problem While Posting Your Answer.</div>
                <%}
                            }%>
            </div>
            <%
                        pageContext.setAttribute("tenderId", tenderId);
                        int cntQuest = 0;
                        int cntStatus = 0;
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <% for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", uId, "0")) {
            %>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th colspan="2" class="t_align_left ff">Company Details</th>
                </tr>
                <tr>
                    <td width="22%" class="t-align-left ff">Company Name :</td>
                    <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                </tr>
            </table>
            <%
                                    } // END FOR LOOP OF COMPANY NAME
%>
            <form id="frmViewQueAns" name="frmEvalPost" action="<%=request.getContextPath()%>/ServletEvalanstoCP" method="POST">
                <input type="hidden" name="uId" value="<%=uId%>" />
                <input type="hidden" name="tenderId" value="<%=tenderId%>" />
                <input type="hidden" name="st" value="<%=st%>" />
                <% CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                    int formCnt = 0;
                    for (SPCommonSearchData formNames : commonSearchService.searchData("GetCPTenderForm", tenderId, userId,
                            uId, role, null, null, null, null, null)) {
                        formId = formNames.getFieldName1();
                        formCnt++;
                %>
                <table width="100%" cellspacing="0" class="tableList_k t_space">
                    <tr>
                       <th> <b>Form Name : </b><a href="ViewEvalBidform.jsp?tenderId=<%=tenderId%>&uId=<%=uId%>&formId=<%=formNames.getFieldName1()%>&lotId=0&bidId=<%=formNames.getFieldName3()%>&action=Edit&isSeek=true" target="_blank"><%=formNames.getFieldName2()%></a>
                                <span class="c-alignment-right"><a href="EvalDocList.jsp?tenderId=<%=tenderId%>&formId=<%=formNames.getFieldName1()%>&uId=<%=userId%>" class="action-button-download">Download Documents</a></span>
                       </th>

                    </tr>
                </table>
                <%
                    int memCount = 0;
                    String memId = "";
                    for (SPCommonSearchData sptcd : commonSearchService.searchData("GetComMemforEvalPost", formId, role, userId, tenderId, null, null, null, null, null)) {
                        memCount++;
                        memId = sptcd.getFieldName3();
                       /* for (SPCommonSearchData sptcd : commonSearchService.searchData("GetEvalComMem", tenderId, formId, null, null, null, null, null, null, null)) {
                        countMem++;*/
                %>

                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th colspan="4" class="t-align-left">TEC / TSC Member Name : <%=sptcd.getFieldName2()%>
                        </th>
                    </tr>
                    <tr>
                        <th width="4%" class="t-align-left">Sl. No.</th>
                        <th width="46%" class="t-align-left">Query</th>
                        <th width="45%" class="t-align-left">Clarification</th>
                        <%if(isCp){%>
                        <th width="5%" class="t-align-left">Note of Decedent</th>
                        <%}else{%>
                        <th width="5%" class="t-align-left">&nbsp;</th>
                        <%}%>
                    </tr>
                    <%
                        for (SPCommonSearchData question : commonSearchService.searchData("SeekClarTECCP",formId, uId,null, null, null, null, null, null, null)) {
                           // String evalCpId = question.getFieldName2();
                            cntQuest++;
                    %>
                    <tr>
                        <td class="t-align-left"><%=cntQuest%></td>
                        <td width="48%" class="t-align-left"><%=question.getFieldName1()%></td>
                        <td class="t-align-left">
                        <label><%=question.getFieldName2()%></label>
                        </td>
                        <td  class="t-align-center"  style="vertical-align: middle;">
                            <%if(isCp){
                                if(question.getFieldName6().equalsIgnoreCase("yes")){
                                %>
                                <input type="checkbox" name="decedentCb_<%=cntQuest%>" id="decedentCb_<%=cntQuest%>" checked="checked" disabled="true" />
                    <tbody>
                            <tr>
                                <td class="t-align-center">Remark</td>
                                <td colspan="3"><%=question.getFieldName8()%></td>
                            </tr>
                    </tbody>
                            <%}else{%>
                            <input type="checkbox" name="decedentCb_<%=cntQuest%>" id="decedentCb_<%=cntQuest%>" disabled="true" />
                                <%}
}%>
                        </td>
                    </tr>
                   <%
                        if(!"Services".equalsIgnoreCase(strProcurementNature)) {
                    %>
                    <tr>
                        <td colspan="2"  class="ff">
                            Evaluation Status :
                        </td>
                        <td colspan="2">
                            <%/*For Status And remark*/
                                SPCommonSearchData compliedStatus = commonSearchService.searchData("getEvalMemberStatus", tenderId, uId, formId, memId, null, null, null, null, null).get(0);
                            %>
                                <%out.println(compliedStatus.getFieldName1());%>
                            
                        </td>
                    </tr>
                    <%if(!"".equalsIgnoreCase(compliedStatus.getFieldName4())){/*Remark condtion Stats here*/%>
                    <tr>
                        <td colspan="2"  class="ff">
                            Evaluation Remarks :
                        </td>
                        <td colspan="2">
                            <label><%out.println(compliedStatus.getFieldName4());%></label>
                        </td>
                    </tr>
                    <%}/*Remeark Condtion ends here*/
                                if (compliedStatus != null) {
                                    compliedStatus = null;
                                }
                    %>
                 <%}/*If Not Service Conditon Ends here*/%>
                    <%
                        if (question != null) {
                                question = null;
                            }
                        }/*Quetion Loop Ends Here*/
                        if (cntQuest == 0) {
                    %>
                    <tr>
                        <td colspan="4" class="t-align-center">No Record Found</td>
                    </tr>
                    <%
                        }
                    %>
                </table>
                <%
                                                    if (sptcd != null) {
                                                        sptcd = null;
                                                    }
                                                }//for table Member Name
                                                if (memCount == 0) {%>
                <div class="tabPanelArea_1 t-align-center">
                    No Record Found
                </div>
                <%}


                                if (formNames != null) {
                                    formNames = null;
                                }
                            }//Form Name ends
                            if (formCnt == 0) {
                %>
                <div class="tabPanelArea_1 t-align-center">
                    No Record Found
                </div>
                <%}
                            if (commonSearchService != null) {
                                commonSearchService = null;
                                            }
                                            if (cntQuest != 0) {%>
<!--                <div class="t-align-center t_space">
                 <label class="formBtn_1">
                        <input type="hidden" id="counter" name="counter" value="<%=cntQuest%>"/>
                        <input type="hidden" id="cntStatus" name="cntStatus" value="<%=cntStatus%>"/>
                        <input name="btnPost" id="btnPost" type="submit" value="Submit" /></label>
                </div>-->
                <%}//For Button Hide Condition.
%>

            </form>

        </div>
        <div>&nbsp;</div>
        <!--Dashboard Content Part End-->
        <!--Dashboard Footer Start-->
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        <!--Dashboard Footer End-->
    </div>
</body>
<script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>

