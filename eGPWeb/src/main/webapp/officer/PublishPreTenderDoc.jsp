<%-- 
    Document   : PublishPreTenderDoc
    Created on : Dec 8, 2010, 2:45:27 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List" %>
<head>
            <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Publish Pre Tender Meeting Document</title>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

    <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
    <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

    <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
    <%--<script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
    <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
    <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
    <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
    <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
    <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

    <%--<script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>--%>
    <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            $("#frmPreTendQuery").validate({
                rules:{
                    txtRemarks:{required:true,maxlength:1000}
                },
                messages:
                    {
                    txtRemarks:{ required:"<div class='reqF_1'>Please enter Comments.</div>",
                        maxlength:"<div class='reqF_1'>Maximum 1000 characters are allowed.</div>"
                    }

                }
            });
        });

    </script>
</head>
<body>
    <div class="mainDiv">
        <div class="fixDiv">
            <%@include file="../resources/common/AfterLoginTop.jsp"%>
            <%
                        int userId = 0;
                        byte suserTypeId = 0;
                        int tenderId = 20;
                        int queryId = 1;

                        if (session.getAttribute("userTypeId") != null) {
                            suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                        }
                        if (session.getAttribute("userId") != null) {
                            userId = Integer.parseInt(session.getAttribute("userId").toString());
                        }
                        if (request.getParameter("tenderId") != null) {
                            tenderId = Integer.parseInt(request.getParameter("tenderId"));
                        }
                        if (request.getParameter("queryId") != null) {
                            queryId = Integer.parseInt(request.getParameter("queryId"));
                        }

                        List<SPTenderCommonData> getRepliedQuery = preTendDtBean.getDataFromSP("GetRepliedQuery", tenderId, 0);

                        pageContext.setAttribute("tenderId", tenderId);
                        pageContext.setAttribute("tab", "4");
            %>
            <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <div class="contentArea_1">
                    <div class="pageHead_1">
                        Publish Pre Tender Meeting Document
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>

                    <table width="100%"  border="0" cellspacing="0" cellpadding="0" >
                        <tr><td>
                                <div style="font-style: italic" class="t-align-left t_space">
                                    Fields marked with (<span class="mandatory">*</span>) are mandatory
                                </div>
                            </td></tr>
                        <tr><td>
                                <div class="t-align-left t_space">
                                    <span class="t-align-right" style="float:right;"><a href="PreTenderMeeting.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a></span>
                                </div>
                            </td></tr>
                        <tr><td>
                                <hr class='t_space'/>
                            </td></tr>
                        <tr><td>
                                <div class='t_space'>
                                    <%@include  file="officerTabPanel.jsp"%>
                                </div>
                            </td></tr>
                    </table>

                    <div t_space>
                        <form method="post" id="frmPreTendQuery" action="<%=request.getContextPath()%>/PreTendQuerySrBean?action=publishQuery">
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">

                                <tr>
                                    <td width="21%" class="t-align-left ff">View All Queries and Replies :</td>
                                    <td width="79%" class="t-align-left">
                                        <a href="<%=request.getContextPath()%>/resources/common/PreTenderQueRep.jsp?tenderId=<%=tenderid%>&viewType=Prebid&from=pubPreTenDoc">View</a>
                                    </td>
                                </tr>
                            </table>

                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td class="t-align-left ff">
              	Pre-Tender Meeting Document :
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="docData">
                                        </div>
                                    </td>
                                </tr>

                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">

                                <tr>
                                    <td width="10%" class="t-align-left ff">Comments : <span class="mandatory">*</span></td>
                                    <td width="90%" class="t-align-left">
                                        <textarea rows="5" id="txtRemarks" name="txtRemarks" class="formTxtBox_1" style="width:99%;"></textarea>
                                    </td>
                                </tr>

                            </table>
                            <div class="t-align-center t_space">
                                <label class="formBtn_1">
                                    <input name="publish" type="submit" value="Publish" />
                                </label>
                                <input type="hidden" name="hidTenderId" id="hidTenderId"  value="<%=tenderId%>"/>
                            </div>

                            <div>&nbsp;</div>
                        </form>
                    </div>
                    <!--Dashboard Content Part End-->
                    <!--Dashboard Footer Start-->
                    <%@include file="../resources/common/Bottom.jsp" %>
                    <!--Dashboard Footer End-->
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<form id="form2" method="post">
</form>
<script>
    function getDocData(){
    <%--$.ajax({
        url: "<%=request.getContextPath()%>/PreTendQuerySrBean?tenderId=<%=tenderId%>&action=docData&docAction=PreTenderMetDocs",
        method: 'POST',
        async: false,
        success: function(j) {
            document.getElementById("docData").innerHTML = j;
        }
    });--%>
            $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,action:'docData',docAction:'PreTenderMetDocs'}, function(j){
                document.getElementById("docData").innerHTML = j;
            });
        }
        getDocData();

        function downloadFile(docName,docSize,tender){
    <%--$.ajax({
             url: "<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download",
             method: 'POST',
             async: false,
             success: function(j) {
             }
     });--%>
             document.getElementById("form2").action= "<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download";
             document.getElementById("form2").submit();
         }

         function deleteFile(docName,docId,tender){
             $.ajax({
                 url: "<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docId="+docId+"&tender="+tender+"&funName=remove",
                 method: 'POST',
                 async: false,
                 success: function(j) {
                     jAlert("Document Deleted."," Delete Document ", function(RetVal) {
                     });
                 }
             });
             getDocData();
         }
 
</script>