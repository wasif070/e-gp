<%-- 
    Document   : Invoice
    Created on : Aug 4, 2011, 3:48:51 PM
    Author     : shreyansh
--%>


<%@page import="com.cptu.egp.eps.web.servicebean.CommonSearchServiceSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsNewBankGuarnatee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsNewBankGuarnateeService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Invoice</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        

    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Invoice</div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%
            CommonService commonServiceee = (CommonService) AppContext.getSpringBean("CommonService");
            String procnatureee = commonServiceee.getProcNature(request.getParameter("tenderId")).toString();
            List<Object[]> lotObj = commonServiceee.getLotDetails(request.getParameter("tenderId"));
            Object[] objj = null;
            if(lotObj!=null && !lotObj.isEmpty())
            {
                objj = lotObj.get(0);
                pageContext.setAttribute("lotId", objj[0].toString());
            }
            TenderTablesSrBean beanCommon = new TenderTablesSrBean();
            String tenderType = beanCommon.getTenderType(Integer.parseInt(request.getParameter("tenderId")));

            if("services".equalsIgnoreCase(procnatureee)||"works".equalsIgnoreCase(procnatureee)){
            %>
            <%@include file="../resources/common/ContractInfoBar.jsp" %>
            <%}%>
            <div>&nbsp;</div>
            <%
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                    dDocSrBean.setLogUserId(userId);
                }
                
                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                CmsNewBankGuarnateeService cmsNewBankGuarnateeService = (CmsNewBankGuarnateeService) AppContext.getSpringBean("CmsNewBankGuarnateeService"); 
                
                List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights",userId,request.getParameter("tenderId"));
                boolean allowReleaseForfeit = false;
                String strlotId = "";
            %>
            <%
                        pageContext.setAttribute("tab", "14");
                        ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");

            %>
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "4");
                            
                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1">
                     <% if(request.getParameter("msg") != null){
                     if("accept".equalsIgnoreCase(request.getParameter("msg"))){

    %>
    <div class='responseMsg successMsg'><%=bdl.getString("CMS.Inv.accept")%></div>
                   <%}
                     if("reject".equalsIgnoreCase(request.getParameter("msg"))){

    %>
    <div class='responseMsg successMsg'><%=bdl.getString("CMS.Inv.reject")%></div>
                   <%}

                     }

 %>
                    <div align="center">

                         <%
                                    String tenderId = request.getParameter("tenderId");
                                    boolean flag = true;
                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");

                                    //Code Comment Start by Proshanto Kumar Saha,Dohatec
                                    //List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    //Code Comment End by Proshanto Kumar Saha,Dohatec
                                    
                                    //Code Start by Proshanto Kumar Saha,Dohatec
                                    //This portion is used to collect data by checking contract sign is not done,performance security is given and tender validity is over.
                                    boolean checkStatus = false;
                                    List<SPCommonSearchDataMore> packageLotList=null;
                                    CommonSearchServiceSrBean objCommonSearchServiceSrBean=new CommonSearchServiceSrBean();
                                    checkStatus=objCommonSearchServiceSrBean.getCSignTValidityAndPSecurityStatus(tenderId, userId);
                                    if(checkStatus)
                                    {
                                     packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithoutContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    }
                                    else
                                    {
                                    packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    }
                                    //Code End by Proshanto Kumar Saha,Dohatec

                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    int i = 0;
                                    boolean flags = false;
                                    CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                    for (SPCommonSearchDataMore lotList : packageLotList) {

                                        flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));
                                        List<Object> wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                        List<Object>  wpIdAdvance = service.getWpIdAdvance(Integer.parseInt(lotList.getFieldName5()));
                                         if(wpIdAdvance.isEmpty())
                                            wpIdAdvance = wpid;
                                        strlotId = lotList.getFieldName5();
                        %>
                        
                        <form name="frmcons" method="post">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td><%=bdl.getString("CMS.lotdes")%></td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>
                                
                                <%
                                 //Added by Salahuddin
                                 TenderCommonService tcs1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                 List<SPTenderCommonData> listDP = tcs1.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                 boolean isIctTender = false;
                                 if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                    isIctTender = true;
                                 }
                                 //Added by Salahuddin
                                 
                                 int j = 0;
                                 String paymentToId = "";
                                 List<SPCommonSearchData> lstPayments= commonSearchService.searchData("getPaidTendererListForPE", tenderId, lotList.getFieldName5(), "Performance Security", userId, null, null, null, null, null);
                                 List<SPCommonSearchData> lstBankGuarantee = commonSearchService.searchData("getPaidTendererListForPE", tenderId, lotList.getFieldName5(), "Bank Guarantee", userId, null, null, null, null, null);

                                 // Dohatec Start
                                 String pending = "";
                                 List<SPCommonSearchData> lstBankGuaranteeICT = null;
                                 if(isIctTender)
                                 {
                                    lstBankGuaranteeICT = commonSearchService.searchData("getPaidTendererListForPEICT", tenderId, lotList.getFieldName5(), "Bank Guarantee", userId, null, null, null, null, null);
                                 }
                                 // Dohatec End

                                 CommonSearchDataMoreService objPayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                 int icountcon=0;
                                 if(lstPayments!=null && !lstPayments.isEmpty()){
                                 out.print(" <tr>");
                                 out.print("<td class='ff' width='20%'  class='t-align-left'>Performance Security</td>");
                                 out.print("<td><table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                                 out.print("<tr><th width='4%' class='t-align-center'>Sl. No.</th>");
                                 out.print("<th width='18%' class='t-align-center'>Contract No.</th>");
                                 out.print("<th width='35%' class='t-align-center'><span id='cmpname'>Company Name</span></th>");
                                 out.print("<th class='t-align-center' width='12%'>Payment Status</th>");
                                 out.print("<th class='t-align-center' width='12%'>Payment Amount</th>");
                                 out.print("<th class='t-align-center' width='12%'>Payment Date</th>");
                                 out.print("<th class='t-align-center' width='30%'>Action</th></tr>");
                                 
                                 int count = lstPayments.size();

                                 for (SPCommonSearchData sptcd : lstPayments)
                                 {
                                     //Code Comment Start by Proshanto Kumar Saha,Dohatec
                                     //List<Object[]> cntDetails = service.getContractNo(Integer.parseInt(sptcd.getFieldName9()));
                                     //Code Comment Start by Proshanto Kumar Saha,Dohatec

                                     //Code Start by Proshanto Kumar Saha,Dohatec
                                     //This portion is used to collect Contract no with or without contract sign by checking contract sign is not done,performance security is given and tender validity is over.
                                      List<Object[]> cntDetails=null;
                                       if(checkStatus)
                                            {
                                            cntDetails=service.getContractNoWithoutCSign(Integer.parseInt(sptcd.getFieldName9()));
                                            }
                                            else
                                            {
                                            cntDetails = service.getContractNo(Integer.parseInt(sptcd.getFieldName9()));
                                            }
                                     //Code End by Proshanto Kumar Saha,Dohatec
                                      
                                     j++;
                                     out.print("<tr");
                                     if(Math.IEEEremainder(j,2)==0){
                                     out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};                                     
                                     if(!isIctTender){
                                        out.print("<td class='t-align-center' >"+j+"</td>");
                                     }
                                     else
                                     {
                                        if(j==1)
                                            out.print("<td class='t-align-center' rowspan='"+count+"'>"+j+"</td>");
                                     }

                                     //if(!isIctTender)
                                        //out.print("<td class='t-align-center'>");
                                     //else
                                        //out.print("<td class='t-align-center' rowspan='"+count+"'>");
                                     if(cntDetails!=null && !cntDetails.isEmpty() ){
                                     if("yes".equalsIgnoreCase(cntDetails.get(0)[1].toString())){
                                         if(!isIctTender)
                                             {
                                                out.print("<td class='t-align-center'>"+cntDetails.get(0)[0]+" (For Repeat Order)"+"</td>");
                                            }
                                         }else{
                                         if(j==1)
                                         out.print("<td class='t-align-center'rowspan='"+count+"'>"+cntDetails.get(0)[0]+"</td>");
                                         }
                                     }
                                     //out.print("</td>");
                                     if("individualconsultant".equalsIgnoreCase(sptcd.getFieldName10()))
                                     {   %>
                                             <script type="text/javascript">
                                                 document.getElementById("cmpname").innerHTML='Individual Consultant Name';
                                             </script>
                                         <% 
                                     }   
                                     if(!isIctTender)
                                        {                                            
                                            out.print("<td class='t-align-left'>"+sptcd.getFieldName2()+"</td>");
                                        }
                                     else
                                         {
                                            if(j==1)
                                            out.print("<td class='t-align-left' rowspan='"+count+"'>"+sptcd.getFieldName2()+"</td>");
                                        }

                                     if("Forfeited".equalsIgnoreCase(sptcd.getFieldName8())){
                                         if(!isIctTender){                                             
                                             out.print("<td class='t-align-center'>Compensated</td>");
                                         }
                                         else
                                         {
                                             if(j==1)
                                             out.print("<td class='t-align-center' rowspan='"+count+"'>Compensated</td>");
                                         }
                                     }else{
                                         if(!isIctTender){                                            
                                             out.print("<td class='t-align-center'>"+sptcd.getFieldName8()+"</td>");
                                         }
                                         else
                                             {
                                                if(j==1)
                                                out.print("<td class='t-align-center' rowspan='"+count+"'>"+sptcd.getFieldName8()+"</td>");
                                             }
                                     }
                                     List<SPCommonSearchDataMore> lstPaymentDetail = objPayment.geteGPData("getTenderPaymentDetail", sptcd.getFieldName1(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                                     
                                      out.print("<td class='t-align-center'>");

                                    if(!isIctTender){
                                    if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){ %>
                                        <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <% }else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                        <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <% }else if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                        <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <% }}
                                      else
                                      {
                                            out.print(""+lstPaymentDetail.get(0).getFieldName3()+" "+lstPaymentDetail.get(0).getFieldName4());
                                      }
                                     out.print("</td>");
                                     if(!isIctTender)
                                     {
                                        out.print("<td class='t-align-center'>"+lstPaymentDetail.get(0).getFieldName6()+"</td>");
                                     }
                                     else
                                     {
                                         if(j==1)
                                         out.print("<td class='t-align-center' rowspan='"+count+"'>"+lstPaymentDetail.get(0).getFieldName6()+"</td>");
                                     }
                                     if(j==1){
                                     out.print("<td class='t-align-center' rowspan='"+count+"'>");
                                     if(!checkuserRights.isEmpty()){
                                         paymentToId = sptcd.getFieldName3();
                                     out.print("<a href='"+request.getContextPath()+"/partner/ViewTenderPaymentDetails.jsp?payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Payment Details</a>");
                                     if("Paid".equalsIgnoreCase(sptcd.getFieldName8()) || "Extended".equalsIgnoreCase(sptcd.getFieldName8())){
                                         if("ps".equalsIgnoreCase(sptcd.getFieldName6())) {
                                            String strActionRequest = "";
                                            List<SPTenderCommonData> lstPaymentActionRequest =
                                                                          tenderCommonService.returndata("getRequestActionFromPaymentId", sptcd.getFieldName1(), null);
                                            System.out.print(">>>>>>" + sptcd.getFieldName1());
                                            if(lstPaymentActionRequest.isEmpty()){
                                                allowReleaseForfeit = true;
                                            } else {
                                                        allowReleaseForfeit = false;
                                                strActionRequest = lstPaymentActionRequest.get(0).getFieldName2();
                                                    }                                                    
                                            if("Release".equalsIgnoreCase(strActionRequest)){
                                                strActionRequest = "Released";
                                            }
                                            if(allowReleaseForfeit){
                                                if(procnature.equalsIgnoreCase("goods")){
                                                        out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestrelease&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Release Request</a>");
                                                }else{
                                               // if(!lstBankGuarantee.isEmpty())//Commented by dohatec to fullfill the outstanding issue no 17 on june 30 2014
                                                    {
                                                        out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestrelease&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Release Request</a>");
                                                    }    
                                                        out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestforfeit&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Compensate Request</a>");
                                                    
                                                }} else {
                                                if("Forfeit".equalsIgnoreCase(strActionRequest))
                                                {
                                                    out.print("&nbsp;&nbsp;(Compensate)");
                                                }else{
                                                out.print("&nbsp;&nbsp;("+strActionRequest+")");
                                                }
                                                }
                                            /*Dohatec Start for Extend performance security */
                                            lstPaymentActionRequest = tenderCommonService.returndata("getExtensionRequestFromPaymentIdPE", sptcd.getFieldName1(), userId);
                                            if(!lstPaymentActionRequest.isEmpty()){
                                                strActionRequest = lstPaymentActionRequest.get(0).getFieldName2();
                                            }
                                            if("Extend".equalsIgnoreCase(strActionRequest)){
                                                strActionRequest = "Extended";
                                            }
                                            if(!("Extended".equalsIgnoreCase(strActionRequest)))
                                                {
                                                //Code Comment Start By Proshanto Kumar Saha,Dohatec
                                                //out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentExtension.jsp?action=extendValidityps&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Extension Request</a>");
                                                //Code Comment End By Proshanto Kumar Saha,Dohatec

                                                //Code Start By Proshanto Kumar Saha,Dohatec
                                                if(!checkStatus)
                                                {
                                                out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentExtension.jsp?action=extendValidityps&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Extension Request</a>");
                                                }
                                                //Code End By Proshanto Kumar Saha,Dohatec

                                                }
                                            else {
                                                 out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/ViewTenderPaymentExtensionDetails.jsp?payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Extension Request Details</a>");
                                                }
                                            /*Dohatec End  for Extend performance security */
                                         }
                                      }
                                      }
                                      out.print("</td>");
                                      }

                                      out.print("</tr>");
                                      icountcon++;
                                     }
                                     if(j==0){
                                        out.print("<tr><td colspan='7'>No records available</td></tr>");
                                     }
                                     out.print(" </table></td></tr>");
                                    } 
                                     out.print(" <tr>");
                                     if(!checkStatus)
                                     {
                                     out.print("<td class='ff' width='20%'  class='t-align-left'>New Performance Security</td>");
                                     }
                                    //out.print("<td class='ff' width='20%'  class='t-align-left'>New Performance Security</td>");
                                    out.print("<td>");

                                    // Dohatec Start
                                    //List<Object[]> processedByMaker = cmsNewBankGuarnateeService.fetchBankGuaranteeListMaker(Integer.parseInt(tenderId),Integer.parseInt(userId));
                                    List<Object[]> processedByMaker = null;
                                    if(isIctTender)
                                    {
                                        processedByMaker = cmsNewBankGuarnateeService.fetchBankGuaranteeListMakerICT(Integer.parseInt(tenderId),Integer.parseInt(userId));
                                    }
                                    else
                                    {
                                       processedByMaker = cmsNewBankGuarnateeService.fetchBankGuaranteeListMaker(Integer.parseInt(tenderId),Integer.parseInt(userId));
                                    }                                    
                                    // Dohatec End

                                    //  Dohatec Start
                                    List<Object[]> listBg;
                                    if(isIctTender) {
                                       listBg = cmsNewBankGuarnateeService.fetchBankGuaranteeListICT(Integer.parseInt(tenderId),Integer.parseInt(userId));
                                       System.out.print("list size : " + listBg.size());
                                    }
                                    else {  //  Old Version
                                       listBg = cmsNewBankGuarnateeService.fetchBankGuaranteeList(Integer.parseInt(tenderId),Integer.parseInt(userId));
                                    }
                                    //  Dohatec End

                                 if(!lstPayments.isEmpty() || true)
                                 {
                                     TblCmsNewBankGuarnatee tblCmsNewBankGuarnatee = cmsNewBankGuarnateeService.fetchBankGuarantee(Integer.parseInt(tenderId));
                                     //if(tblCmsNewBankGuarnatee==null){

                                     // Dohatec Start
                                        // If New Performance Security Amount Payment Status is Pending, New Performance Security request link should not be appeared for ICT.
                                        // For NCT same as previous version
                                        if(!isIctTender)
                                        {
                                            //Code Comment Start By Proshanto Kumar Saha,Dohatec
                                            ////Old Version (there was no condition for NCT)
                                            //out.print("<a href='"+request.getContextPath()+"/officer/RequestBankGuarantee.jsp?uId="+service.getL1bidderuserID(tenderId)+"&tenderId="+tenderId+"&lotId="+strlotId+"'>Request Bank Guarantee for New Performance Security</a>");
                                            //Code Comment End By Proshanto Kumar Saha,Dohatec

                                            //Code Start By Proshanto Kumar Saha,Dohatec
                                            if(!checkStatus)
                                            {
                                            out.print("<a href='"+request.getContextPath()+"/officer/RequestBankGuarantee.jsp?uId="+service.getL1bidderuserID(tenderId)+"&tenderId="+tenderId+"&lotId="+strlotId+"'>Request Bank Guarantee for New Performance Security</a>");
                                            }
                                            //Code End By Proshanto Kumar Saha,Dohatec
                                        }
                                        else // ICT
                                        {
                                            if((listBg.isEmpty() || listBg == null) && (processedByMaker.isEmpty() || processedByMaker == null))
                                            {
                                                //Code Comment Start By Proshanto Kumar Saha,Dohatec
                                                //out.print("<a href='"+request.getContextPath()+"/officer/RequestBankGuarantee.jsp?uId="+service.getL1bidderuserID(tenderId)+"&tenderId="+tenderId+"&lotId="+strlotId+"'>Request Bank Guarantee for New Performance Security</a>");
                                                //Code Comment End By Proshanto Kumar Saha,Dohatec

                                                //Code Start By Proshanto Kumar Saha,Dohatec
                                                if(!checkStatus)
                                                {
                                                out.print("<a href='"+request.getContextPath()+"/officer/RequestBankGuarantee.jsp?uId="+service.getL1bidderuserID(tenderId)+"&tenderId="+tenderId+"&lotId="+strlotId+"'>Request Bank Guarantee for New Performance Security</a>");
                                                }
                                                //Code End By Proshanto Kumar Saha,Dohatec
                                            }
                                        }
                                     // Dohatec End

                                    //out.print("<a href='"+request.getContextPath()+"/officer/RequestBankGuarantee.jsp?uId="+paymentToId+"&tenderId="+tenderId+"&lotId="+strlotId+"'>Request Bank Guarantee for New Performance Security</a>");
                                    // }else{
                                     //out.print("Bank Guarantee Requested");
                                    // }
                                    // Dohatec : New Condition is added -> "(isIctTender && (lstBankGuaranteeICT!=null && !lstBankGuaranteeICT.isEmpty()))"
                                    if((listBg!=null && !listBg.isEmpty()) || (processedByMaker!=null && !processedByMaker.isEmpty()) || (lstBankGuarantee!=null && !lstBankGuarantee.isEmpty()) || (isIctTender && (lstBankGuaranteeICT!=null && !lstBankGuaranteeICT.isEmpty()))){

                                     // Dohatec Start
                                     j = 0;
                                     int icountforBG=0;

                                     if(isIctTender){

                                         pending = "pending";
                                         
                                         out.print("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                                         out.print("<tr><th width='4%' class='t-align-center'>Sl. No.</th>");
                                         //out.print("<th width='7%' class='t-align-center'>Contract  No.</th>");
                                         out.print("<th width='38%' class='t-align-center'><span id='cmpname1'>Company Name</span></th>");
                                         out.print("<th class='t-align-center' width='12%'>Payment Status</th>");
                                         out.print("<th class='t-align-center' width='30%'>Action</th></tr>");
                                         
                                         for(Object[] obj1: listBg){
                                             j++;
                                             if("individualconsultant".equalsIgnoreCase(obj1[5].toString()))
                                             {   %>
                                                     <script type="text/javascript">
                                                         document.getElementById("cmpname1").innerHTML='Individual Consultant Name';
                                                     </script>
                                                 <%
                                             }
                                             out.print("<tr");
                                             if(Math.IEEEremainder(j,2)==0){
                                                 out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                                 out.print("<td class='t-align-center'>"+j+"</td>");
                                                 /*if("yes".equalsIgnoreCase(obj1[4].toString())){
                                                    out.print("<td class='t-align-center'>"+obj1[3]+" (For Repeat Order)</td>");
                                                 }else{
                                                    out.print("<td class='t-align-center'>"+obj1[3]+"</td>");
                                                 }*/
                                                 String strCompname = "";
                                                 if("1".equals(obj1[5].toString())){
                                                     strCompname = obj1[0].toString()+" "+obj1[1].toString();
                                                 }else{
                                                     strCompname = obj1[6].toString();
                                                 }
                                                 out.print("<td class='t-align-left'>"+strCompname+"</td>");
                                                 out.print("<td class='t-align-center'>Pending</td>");
                                                 out.print("<td class='t-align-center'>");
                                                 out.print("<a href='"+request.getContextPath()+"/officer/ViewBankGuarantee.jsp?uId="+paymentToId+"&tenderId="+tenderId+"&lotId="+strlotId+"'>View</a>");
                                                 out.print("</td>");
                                                 out.print("</tr>");
                                                  icountforBG++;

                                         }
                                     }else{ //  Old version
                                         out.print("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                                         out.print("<tr><th width='4%' class='t-align-center'>Sl. No.</th>");
                                         out.print("<th width='7%' class='t-align-center'>Contract  No.</th>");
                                         out.print("<th width='38%' class='t-align-center'><span id='cmpname1'>Company Name</span></th>");
                                         out.print("<th class='t-align-center' width='12%'>Payment Status</th>");
                                         out.print("<th class='t-align-center' width='30%'>Action</th></tr>");
                                         //j = 0;
                                         //int icountforBG=0;
                                         for(Object[] obj1: listBg){
                                             j++;
                                             if("individualconsultant".equalsIgnoreCase(obj1[5].toString()))
                                             {   %>
                                                     <script type="text/javascript">
                                                         document.getElementById("cmpname1").innerHTML='Individual Consultant Name';
                                                     </script>
                                                 <%
                                             }
                                             out.print("<tr");
                                             if(Math.IEEEremainder(j,2)==0){
                                                 out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                                 out.print("<td class='t-align-center'>"+j+"</td>");
                                                 if("yes".equalsIgnoreCase(obj1[4].toString())){
                                                    out.print("<td class='t-align-center'>"+obj1[3]+" (For Repeat Order)</td>");
                                                 }else{
                                                    out.print("<td class='t-align-center'>"+obj1[3]+"</td>");
                                                 }
                                                 String strCompname = "";
                                                 if("1".equals(obj1[6].toString())){
                                                     strCompname = obj1[0].toString()+" "+obj1[1].toString();
                                                 }else{
                                                     strCompname = obj1[7].toString();
                                                 }
                                                 out.print("<td class='t-align-left'>"+strCompname+"</td>");
                                                 out.print("<td class='t-align-center'>Pending</td>");
                                                 out.print("<td class='t-align-center'>");
                                                 
                                                 
                                                 out.print("<a href='"+request.getContextPath()+"/officer/ViewBankGuarantee.jsp?uId="+paymentToId+"&tenderId="+tenderId+"&lotId="+strlotId+"&bgId="+obj1[3]+"'>View</a>");
                                                 // Previous Code : &bgId="+obj1[2]
                                                 //out.print("<a href='"+request.getContextPath()+"/officer/ViewBankGuarantee.jsp?uId="+paymentToId+"&tenderId="+tenderId+"&lotId="+strlotId+"&bgId="+obj1[2]+"'>View</a>");

                                                 out.print("</td>");
                                                 out.print("</tr>");
                                                  icountforBG++;

                                         }
                                     }
                                     // Dohatec End

                                     for(Object[] obj1: processedByMaker){
                                         j++;
                                         out.print("<tr");
                                         if(Math.IEEEremainder(j,2)==0){
                                             out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                             out.print("<td class='t-align-center'>"+j+"</td>");
                                             if(!isIctTender){  // Added by dohatec
                                                 if("yes".equalsIgnoreCase(obj1[4].toString())){
                                                    out.print("<td class='t-align-center'>"+obj1[3]+" (For Repeat Order)</td>");
                                                 }else{
                                                    out.print("<td class='t-align-center'>"+obj1[3]+"</td>");
                                                 }
                                             }
                                             String strCompname = "";
                                             if("1".equals(obj1[6].toString())){
                                                 strCompname = obj1[0].toString()+" "+obj1[1].toString();
                                             }else{
                                                 if(isIctTender)    // Added by Dohatec
                                                    strCompname = obj1[6].toString();
                                                 else   // Old version
                                                    strCompname = obj1[7].toString();
                                             }
                                             out.print("<td class='t-align-left'>"+strCompname+"</td>");
                                             out.print("<td class='t-align-center'>Pending</td>");
                                             out.print("<td class='t-align-center'>");
                                             // Dohatec Start
                                             if(isIctTender)
                                             {
                                                out.print("<a href='"+request.getContextPath()+"/officer/ViewBankGuarantee.jsp?uId="+paymentToId+"&tenderId="+tenderId+"&lotId="+strlotId+"'>View</a>");
                                             }
                                             else
                                             {
                                                out.print("<a href='"+request.getContextPath()+"/officer/ViewBankGuarantee.jsp?uId="+paymentToId+"&tenderId="+tenderId+"&lotId="+strlotId+"&bgId="+obj1[2]+"'>View</a>");
                                             }
                                             // Dohatec End
                                             out.print("</td>");
                                             out.print("</tr>");
                                              icountforBG++;

                                     }

                                     // Dohatec Start
                                     if(isIctTender)
                                     {
                                         int size = lstBankGuaranteeICT.size();
                                         int k = 0;
                                         for (SPCommonSearchData sptcd : lstBankGuaranteeICT)
                                         {
                                             //List<Object[]> cntDetails = service.getContractNo(Integer.parseInt(sptcd.getFieldName9()));
                                             j++;
                                             out.print("<tr");
                                             if(Math.IEEEremainder(j,2)==0){
                                             out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                             out.print("<td class='t-align-center'>"+j+"</td>");
                                             
                                             out.print("<td class='t-align-left'>"+sptcd.getFieldName2()+"</td>");
                                             if("Forfeited".equalsIgnoreCase(sptcd.getFieldName8())){
                                                 out.print("<td class='t-align-center'>Compensated</td>");
                                             }else{
                                                 out.print("<td class='t-align-center'>"+sptcd.getFieldName8()+"</td>");
                                             }
                                             out.print("<td class='t-align-center'>");
                                             if(!checkuserRights.isEmpty()){
                                                 //out.print("<a href='"+request.getContextPath()+"/partner/ViewTenderPaymentDetails.jsp?payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Payment Details</a>");
                                             out.print("<a href='"+request.getContextPath()+"/partner/ViewTenderPaymentDetails.jsp?payId="+lstBankGuarantee.get(k).getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Payment Details</a>");
                                             if("Paid".equalsIgnoreCase(sptcd.getFieldName8()) || "Extended".equalsIgnoreCase(sptcd.getFieldName8())){
                                                 if("bg".equalsIgnoreCase(sptcd.getFieldName6())) {
                                                    String strActionRequest = "";
                                                    List<SPTenderCommonData> lstPaymentActionRequest =
                                                                                  tenderCommonService.returndata("getRequestActionFromPaymentId", lstBankGuarantee.get(k).getFieldName1(), null);
                                                    if(lstPaymentActionRequest.isEmpty()){
                                                        allowReleaseForfeit = true;
                                                    } else {
                                                        allowReleaseForfeit = false;
                                                        strActionRequest = lstPaymentActionRequest.get(0).getFieldName2();
                                                    }
                                                    if("Release".equalsIgnoreCase(strActionRequest)){
                                                        strActionRequest = "Released";
                                                    }
                                                    if(allowReleaseForfeit){
                                                        /*
                                                            out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestrelease&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Release Request</a>");
                                                            out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestforfeit&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Compensate Request</a>");
                                                        */

                                                        out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestrelease&payId="+lstBankGuarantee.get(k).getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Release Request</a>");
                                                         out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestforfeit&payId="+lstBankGuarantee.get(k).getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Compensate Request</a>");

                                                    } else {
                                                        if("Forfeit".equalsIgnoreCase(strActionRequest))
                                                        {
                                                            out.print("&nbsp;&nbsp;(Compensate)");
                                                        }else{
                                                        out.print("&nbsp;&nbsp;("+strActionRequest+")");
                                                        }
                                                    }
                                                 }
                                              }
                                              }
                                              out.print("</td></tr>");
                                              k++;
                                             }
                                     }
                                     else   // Old Version
                                     {
                                        for (SPCommonSearchData sptcd : lstBankGuarantee)
                                         {
                                            //Code Comment Start by Proshanto Kumar Saha,Dohatec
                                            // List<Object[]> cntDetails = service.getContractNo(Integer.parseInt(sptcd.getFieldName9()));
                                            //Code Comment End by Proshanto Kumar Saha,Dohatec

                                            //Code Start by Proshanto Kumar Saha,Dohatec
                                             //This portion is used to collect Contract no with or without contract sign by checking contract sign is not done,performance security is given and tender validity is over.
                                            List<Object[]> cntDetails=null;
                                            if(checkStatus)
                                            {
                                            cntDetails=service.getContractNoWithoutCSign(Integer.parseInt(sptcd.getFieldName9()));
                                            }
                                            else
                                            {
                                            cntDetails = service.getContractNo(Integer.parseInt(sptcd.getFieldName9()));
                                            }
                                            //Code End by Proshanto Kumar Saha,Dohatec

                                             j++;
                                             out.print("<tr");
                                             if(Math.IEEEremainder(j,2)==0){
                                             out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                             out.print("<td class='t-align-center'>"+j+"</td>");

                                             out.print("<td class='t-align-center'>");
                                             if(cntDetails!=null && !cntDetails.isEmpty() ){
                                             if("yes".equalsIgnoreCase(cntDetails.get(0)[1].toString())){
                                             out.print(cntDetails.get(0)[0]+" (For Repeat Order)");
                                             }else{
                                             out.print(cntDetails.get(0)[0]);
                                             }
                                             }
                                             out.print("</td>");

                                             out.print("<td class='t-align-left'>"+sptcd.getFieldName2()+"</td>");
                                             if("Forfeited".equalsIgnoreCase(sptcd.getFieldName8())){
                                                 out.print("<td class='t-align-center'>Compensated</td>");
                                             }else{
                                                 out.print("<td class='t-align-center'>"+sptcd.getFieldName8()+"</td>");
                                             }
                                             out.print("<td class='t-align-center'>");
                                             if(!checkuserRights.isEmpty()){
                                             out.print("<a href='"+request.getContextPath()+"/partner/ViewTenderPaymentDetails.jsp?payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Payment Details</a>");
                                             if("Paid".equalsIgnoreCase(sptcd.getFieldName8()) || "Extended".equalsIgnoreCase(sptcd.getFieldName8())){
                                                 if("bg".equalsIgnoreCase(sptcd.getFieldName6())) {
                                                    String strActionRequest = "";
                                                    List<SPTenderCommonData> lstPaymentActionRequest =
                                                                                  tenderCommonService.returndata("getRequestActionFromPaymentId", sptcd.getFieldName1(), null);
                                                    if(lstPaymentActionRequest.isEmpty()){
                                                        allowReleaseForfeit = true;
                                                    } else {
                                                        allowReleaseForfeit = false;
                                                        strActionRequest = lstPaymentActionRequest.get(0).getFieldName2();
                                                    }
                                                    if("Release".equalsIgnoreCase(strActionRequest)){
                                                        strActionRequest = "Released";
                                                    }
                                                    if(allowReleaseForfeit){
                                                            out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestrelease&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Release Request</a>");
                                                            out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestforfeit&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Compensate Request</a>");

                                                    } else {
                                                        if("Forfeit".equalsIgnoreCase(strActionRequest))
                                                        {
                                                            out.print("&nbsp;&nbsp;(Compensate)");
                                                        }else{
                                                        out.print("&nbsp;&nbsp;("+strActionRequest+")");
                                                        }
                                                    }
                                                 }
                                              }
                                              }
                                              out.print("</td></tr>");

                                         }
                                     }
                                     // Dohatec End
                                         if(j==0){
                                            out.print("<tr><td colspan='5'>No records available</td></tr>");
                                         }
                                         out.print("</table>");
                                 }}
                                 out.print("</td></tr>");
                                %>
                                <%
                                                                int count = 1;
                                                                if (!wpid.isEmpty() && wpid != null) {%>
                                <tr><td colspan="2">
                                        <%
                                            NOAServiceImpl noaServiceImpl = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
                                             List<Object[]> noalist = null;
                                            if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                                noalist = noaServiceImpl.getDetailsNOAforInvoiceICT(Integer.parseInt(tenderId),Integer.parseInt(lotList.getFieldName5()));
                                            else
                                                noalist = noaServiceImpl.getDetailsNOAforInvoice(Integer.parseInt(tenderId),Integer.parseInt(lotList.getFieldName5()));
                                            Object[] noaObj  = null;
                                            if(!noalist.isEmpty())
                                            {
                                                noaObj = noalist.get(0);
                                                if(noaObj[12]!=null && !"0.000".equalsIgnoreCase(noaObj[12].toString()))
                                                {
                                        %>
                                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                        <tr>
                                                            <td class="ff" width="88%">Advance Amount</td>
                                                            <td>
                                                            <%
                                                             List<Object[]> invIdData = null;
                                                             if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                                                invIdData = service.getInvoiceMasterDatawithInvAmtICT(wpIdAdvance.get(0).toString());
                                                             else
                                                                invIdData = service.getInvoiceMasterDatawithInvAmt(wpid.get(0).toString());
                                                         //   System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+wpid.get(0).toString());
                                                             if (!invIdData.isEmpty() && invIdData != null) {
                                                                for(int k=0;k<invIdData.size(); k++)
                                                                {
                                                                    Object[] objInvdata = invIdData.get(k);
                                                            %>
                                                            <table class="tableList_1 t_space">
                                                            <tr>
                                                                <th width="3%" class="t-align-center">Invoices</th>
                                                                <th width="3%" class="t-align-center">Status</th>
                                                            </tr>
                                                            <tr>
                                                                  <% if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) {%>
                                                                <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&invoiceNo=<%=objInvdata[0]%>&wpId=<%=wpIdAdvance.get(0)%>"><%=objInvdata[0].toString()%></a><%if("sendtope".equalsIgnoreCase(objInvdata[1].toString())) {%><span class="reqF_1">*</span><%}%></td>
                                                                 <%} else{ %>
                                                                <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&invoiceId=<%=objInvdata[0]%>&wpId=<%=wpid.get(0)%>"><%=objInvdata[2].toString()%></a><%if("sendtope".equalsIgnoreCase(objInvdata[1].toString())) {%><span class="reqF_1">*</span><%}%></td>
                                                                  <% } %>
                                                                <td class="t-align-left">
                                                                    <%
                                                                        String status = "";
                                                                        if (objInvdata[1] == null) {
                                                                            status = "-";
                                                                        } else if ("createdbyten".equalsIgnoreCase(objInvdata[1].toString())) {
                                                                            if (procnature.equalsIgnoreCase("works")) {
                                                                                status = "Invoice Generated by Contractor";
                                                                            } else {
                                                                                status = "Invoice Generated by Supplier";
                                                                            }
                                                                        } else if ("acceptedbype".equalsIgnoreCase(objInvdata[1].toString())) {
                                                                            status = "Invoice Accepted by PE";
                                                                        } else if ("sendtope".equalsIgnoreCase(objInvdata[1].toString())) {
                                                                            status = "In Process";
                                                                        } else if ("sendtotenderer".equalsIgnoreCase(objInvdata[1].toString())) {
                                                                            status = "Processed by Accounts Officer";
                                                                        } else if ("remarksbype".equalsIgnoreCase(objInvdata[1].toString())) {
                                                                            status = "Processed by PE";
                                                                        } else if ("rejected".equalsIgnoreCase(objInvdata[1].toString())) {
                                                                            status = "Invoice Rejected by PE";
                                                                        }
                                                                        out.print(status);
                                                                    %>
                                                                </td>
                                                            </tr>
                                                            </table>
                                                            <%}}else{%>Not Generated
                                                            <%}%>
                                                        </tr>
                                                    </table>
                                        <%}}%>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="26%" class="t-align-center">
                                                    <%if(procnature.equalsIgnoreCase("goods")){%>
                                                   <%=bdl.getString("CMS.Goods.Forms")%>
                                                <%}else{%>
                                                <%=bdl.getString("CMS.works.Forms")%>
                                                <%}%>
                                                </th>
                                                <th width="60%" class="t-align-center"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                <th width="20%" class="t-align-center"><%=bdl.getString("CMS.status")%></th>
                                            </tr>
                                            <%for (Object obj : wpid) {
											if((tenderType.equals("ICT") && wpIdAdvance.get(0).toString().equals(obj.toString())) || (tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes") && wpIdAdvance.get(0).toString().equals(obj.toString())))
                                                continue;
                                            else{
                                          //List<Object> invoicId = service.getInvoiceId(obj.toString());
                                           List<Object[]> invoicId = null;
                                         
                                           if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                             invoicId = service.getInvoiceIdListForICT(obj.toString());
                                          else
                                             invoicId = service.getInvoiceIdList(obj.toString());
                                          String toDisplay = service.getConsolidate(obj.toString());
                                          List<TblCmsPrMaster> Prlist = service.getPRHistory(Integer.parseInt(obj.toString()));

                                            %>
                                            <tr>
                                                <td style="text-align: center;"><%=count%></td>
                                                <td>
                                                    <%if(procnature.equalsIgnoreCase("goods")){%>
                                                    <%=toDisplay.replace("Consolidate", "Delivery Schedule") %>
                                                    <%}else{%>
                                                    <%=toDisplay.replace("Consolidate", "BoQ") %>
                                                    <%}%>
                                                </td>
                                                <td>
                                                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                                    <tr>
                                                        <th width="20%" class="t-align-center"><%=bdl.getString("CMS.PR.prno")%></th>
                                                        <th width="50%" class="t-align-center"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                        <th width="10%" class="t-align-center"><%=bdl.getString("CMS.action")%>
                                                         </th>
                                                     </tr>
                                                    <%
                                             int prId=0;
                                             if(!Prlist.isEmpty() && Prlist!=null){
                                             int k=1;
                                             for(int ii=0;ii<Prlist.size();ii++){
                                                    String temp[] = Prlist.get(ii).getProgressRepNo().split(" ");
                                                    String temp1[] = temp[4].split("-");
                                                    System.out.println(temp[4]);
                                                    String finalNo = temp[0]+" "+temp[1]+" "+temp[2]+" "+temp[3]+" "+DateUtils.customDateFormate(DateUtils.convertStringtoDate(temp[4].toString(),"yyyy-MM-dd"));
    %>
                                             <tr>
                                               <td class="t-align-center"><%=k+ii%></td>
                                             <td class="t-align-left"><%=finalNo%></td>
                                             <td class="t-align-left"><a href="ViewPr.jsp?repId=<%=Prlist.get(ii).getProgressRepId()%>&wpId=<%=obj.toString() %>&tenderId=<%=tenderId%>&flag=inv&lotId=<%= lotList.getFieldName5() %>">View</a></td>
                                             <%
                                                prId=Prlist.get(ii).getProgressRepId();
                                            }}%>
                                                    </table>
                                                </td>
                                                <td class="t-align-left">
                                                     <%if(!invoicId.isEmpty() && invoicId!=null){%>
                                                    <%}else{%>
                                                    Not Generated
                                                    <%}%>
                                                <%if(!invoicId.isEmpty() && invoicId!=null){                                                
                                                %>
                                                <div>
                                                    <table class="tableList_1 t_space">
                                                        <tr>
                                                     <th width="3%" class="t-align-center">Invoices</th>
                                                     <th width="3%" class="t-align-center">Status</th>
                                                        </tr>
                                                <%
                                                int ii=1;
                                                for(int invoicIndex=0; invoicIndex<invoicId.size(); invoicIndex++){
                                                Object[] invobj = invoicId.get(invoicIndex);
                                                %>
                                                <tr>
                                                     <% if(tenderType.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) {%>
                                                                <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>&invoiceNo=<%=invobj[0].toString()%>"><%=invobj[0].toString()%></a><%if("sendtope".equalsIgnoreCase(invobj[1].toString())) {%><span class="reqF_1">*</span><%}%></td>
                                                     <%} else{ %>
                                                             <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&invoiceId=<%=invobj[0]%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>"><%=invobj[2].toString()%></a><%if("sendtope".equalsIgnoreCase(invobj[1].toString())) {%><span class="reqF_1">*</span><%}%></td>
                                                     <% } %>
                                                          <td class="t-align-left">
                                                                    <%
                                                                                                                        String status = "";
                                                                                                                        if (invobj[1] == null) {
                                                                                                                            status = "-";
                                                                                                                        } else if ("createdbyten".equalsIgnoreCase(invobj[1].toString())) {
                                                                                                                            if (procnature.equalsIgnoreCase("works")) {
                                                                                                                                status = "Invoice Generated by Contractor";
                                                                                                                            } else {
                                                                                                                                status = "Invoice Generated by Supplier";
                                                                                                                            }
                                                                                                                        } else if ("acceptedbype".equalsIgnoreCase(invobj[1].toString())) {
                                                                                                                            status = "Invoice Accepted by PE";
                                                                                                                        } else if ("sendtope".equalsIgnoreCase(invobj[1].toString())) {
                                                                                                                            status = "In Process";
                                                                                                                        } else if ("sendtotenderer".equalsIgnoreCase(invobj[1].toString())) {
                                                                                                                            status = "Processed by Accounts Officer";
                                                                                                                        } else if ("remarksbype".equalsIgnoreCase(invobj[1].toString())) {
                                                                                                                            status = "Processed by PE";
                                                                                                                        } else if ("rejected".equalsIgnoreCase(invobj[1].toString())) {
                                                                                                                            status = "Invoice Rejected by PE";
                                                                                                                        }
                                                                                                                        out.print(status);
                                                                    %>
                                                                </td>
                                                </tr>
   <%
   ii++;
                                                } %>

                                                </table>
                                                </div>
<%}%></td>
                                            </tr>


                                            <%
                                                                                                                                count++;
                                                                                                                            }}%>

                                        </table>
                                    </td></tr>
                                    <%
                                                                        }
                                    %>


               <%
                                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                                        List<Object[]> mainForRO = ros.getMainLoopForROPESide(Integer.parseInt(strlotId));
                                        if(!mainForRO.isEmpty()){
                                            int icount = 1;
                                                for(Object[] objs : mainForRO){
                                                     for (Object[] objd : issueNOASrBean.getNOAListingForRO(Integer.parseInt(tenderId),(Integer) objs[0])) {
                                                         List<Object> wpids = ros.getWpIdForAfterRODone(Integer.parseInt(objs[1].toString()));

                        %>

                              <tr>
                                    <td colspan="4">
                                <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Repeat Order-<%=icount%></li>
                                </ul>

                                    </td>
                                <%
                                    int countR = 1;
                                    if (!wpids.isEmpty() && wpids != null) {%>
                                <tr><td colspan="2">
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="26%" class="t-align-center">
                                                    <%if(procnature.equalsIgnoreCase("goods")){%>
                                                   <%=bdl.getString("CMS.Goods.Forms")%>
                                                <%}else{%>
                                                <%=bdl.getString("CMS.works.Forms")%>
                                                <%}%>
                                                </th>
                                                <th width="60%" class="t-align-center"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                <th width="20%" class="t-align-center"><%=bdl.getString("CMS.status")%></th>
                                            </tr>
                                            <%
                                            int showR=0;
                                            for (Object obj : wpids) {
                                           List<Object[]> formsR = dDocSrBean.getAllBoQFormsForRO(Integer.parseInt(strlotId));
                                          //List<Object> invoicId = service.getInvoiceId(obj.toString());
                                           List<Object[]> invoicId = null;
                                        //  if(tenderType.equals("ICT"))
                                       //      invoicId = service.getInvoiceIdListForICT(obj.toString());
                                        //  else
                                             invoicId = service.getInvoiceIdList(obj.toString());
                                          List<TblCmsPrMaster> Prlist = service.getPRHistory(Integer.parseInt(obj.toString()));

                                            %>
                                            <tr>
                                                <td style="text-align: center;"><%=countR%></td>
                                                <td>
                                                   
                                                    <%="Delivery Schedule of "+formsR.get(showR)[1] %>
                                                </td>
                                                <td>
                                                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                                    <tr>
                                                        <th width="20%" class="t-align-center"><%=bdl.getString("CMS.PR.prno")%></th>
                                                        <th width="50%" class="t-align-center"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                        <th width="10%" class="t-align-center"><%=bdl.getString("CMS.action")%>
                                                         </th>
                                                     </tr>
                                                    <%
                                             int prId=0;
                                             if(!Prlist.isEmpty() && Prlist!=null){
                                             int k=1;
                                             for(int ii=0;ii<Prlist.size();ii++){
                                                    String temp[] = Prlist.get(ii).getProgressRepNo().split(" ");
                                                    String temp1[] = temp[4].split("-");
                                                    String finalNo = temp[0]+" "+temp[1]+" "+temp[2]+" "+temp[3]+" "+DateUtils.customDateFormate(DateUtils.convertStringtoDate(temp[4].toString(),"yyyy-MM-dd"));
    %>
                                             <tr>
                                               <td class="t-align-center"><%=k+ii%></td>
                                             <td class="t-align-left"><%=finalNo%></td>
                                             <td class="t-align-left"><a href="ViewPr.jsp?repId=<%=Prlist.get(ii).getProgressRepId()%>&wpId=<%=obj.toString() %>&tenderId=<%=tenderId%>&flag=inv&lotId=<%= lotList.getFieldName5() %>">View</a></td>
                                             <%
                                                prId=Prlist.get(ii).getProgressRepId();
                                            }}%>
                                                    </table>
                                                </td>
                                                <td class="t-align-left">
                                                     <%if(!invoicId.isEmpty() && invoicId!=null){%>
                                                    <%}else{%>
                                                    Not Generated
                                                    <%}%>
                                                <%if(!invoicId.isEmpty() && invoicId!=null){
                                                %>

                                                    <table class="tableList_1 t_space">
                                                    <tr>
                                                        <th width="3%" class="t-align-center">Invoices</th>
                                                        <th width="3%" class="t-align-center">Status</th>
                                                    </tr>
                                                    <%
                                                    int ii=1;
                                                    for(int invoicIndex=0; invoicIndex<invoicId.size(); invoicIndex++){
                                                    Object[] invobj = invoicId.get(invoicIndex);
                                                    %>
                                                        <tr>
                                                           <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&invoiceId=<%=invobj[0]%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>&cntId=<%=objd[11]%>"><%=invobj[2].toString()%></a><%if("sendtope".equalsIgnoreCase(invobj[1].toString())) {%><span class="reqF_1">*</span><%}%></td>
                                                           
                                                            <td class="t-align-left">
                                                                <%
                                                                    String status = "";
                                                                    if (invobj[1] == null) {
                                                                        status = "-";
                                                                    } else if ("createdbyten".equalsIgnoreCase(invobj[1].toString())) {
                                                                        if (procnature.equalsIgnoreCase("works")) {
                                                                            status = "Invoice Generated by Contractor";
                                                                        } else {
                                                                            status = "Invoice Generated by Supplier";
                                                                        }
                                                                    } else if ("acceptedbype".equalsIgnoreCase(invobj[1].toString())) {
                                                                        status = "Invoice Accepted";
                                                                    } else if ("sendtope".equalsIgnoreCase(invobj[1].toString())) {
                                                                        status = "Process";
                                                                    } else if ("sendtotenderer".equalsIgnoreCase(invobj[1].toString())) {
                                                                        status = "Processed by Accounts Officer";
                                                                    } else if ("remarksbype".equalsIgnoreCase(invobj[1].toString())) {
                                                                        status = "Processed";
                                                                    } else if ("rejected".equalsIgnoreCase(invobj[1].toString())) {
                                                                        status = "Invoice Rejected";
                                                                    }
                                                                    out.print(status);
                                                                %>
                                                            </td>
                                                        </tr>              
                                                    
                                       <%
                                       ii++;
                                                } %>
                                                    </table>
                                                    </td>
                                                </tr>


                                        <%}%>
                                        <%
                                            countR++;
                                            showR++;
                                            }%>
                                            </table>


                               <%
                            }  }icount++;} } }%>
                            </td></tr></table>
                        </form>
                    </div>
                </div></div></div>
                
        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
       <script>
            function Edit(wpId,tenderId,lotId){


                dynamicFromSubmit("EditDatesForBoq.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms");

            }
            function view(wpId,tenderId,lotId){
                dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
            }
        </script>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>

</html>
