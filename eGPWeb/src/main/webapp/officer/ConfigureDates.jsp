<%-- 
    Document   : ConfigureDates
    Created on : Jul 28, 2011, 3:24:14 PM
    Author     : dixit
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.GetCurrencyAmount"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.PerformanceSecurityICT"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblContractSign"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsDateConfig"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.model.table.TblNoaIssueDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");                    
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Configure Dates</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <title>Configure Dates</title>    
    </head>
    <body>
        <%
                String roundID = "";
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                String lotId = "";
                if (request.getParameter("lotId") != null) {
                    lotId = request.getParameter("lotId");
                }
                String ContractSignId = "";
                if (request.getParameter("contractSignId") != null) {
                    ContractSignId = request.getParameter("contractSignId");
                }
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                pageContext.setAttribute("tab", "14");

                /////// Added by Salahuddin ///////////

                Double conAmount;
                String cAmount="";
                
                TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", tenderId, null);
                boolean isIctTender = false;
                if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                    isIctTender = true;
                }

                //Added by Salahuddin
                //List<SPCommonSearchDataMore> perSecAmt = new ArrayList<SPCommonSearchDataMore>();
                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> perSecListing = new ArrayList<SPCommonSearchDataMore>();
                perSecListing = commonSearchDataMoreService.geteGPData("getDataForPerSec",tenderId,lotId);
                roundID = perSecListing.get(0).getFieldName2();

                /*PerformanceSecurityICT perfSecurityICT  = (PerformanceSecurityICT) AppContext.getSpringBean("PerformanceSecurityICT");
                List<GetCurrencyAmount> currencyAmount = new ArrayList<GetCurrencyAmount>();
                currencyAmount= perfSecurityICT.getCurrencyAmount(Integer.parseInt(tenderId));
                int count=0;
                if(currencyAmount.get(0).getValue1()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue2()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue3()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue4()!=0)
                    count=count+1;*/
                ////////// Added by Salahuddin ////////

        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Configure Dates
                    <span style="float: right; text-align: right;">
                    <a class="action-button-goback" href="CMSMain.jsp?tenderId=<%=tenderId%>" title="Go Back">Go Back</a>
                    </span>
                </div>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>

                <%@include  file="officerTabPanel.jsp"%>
                <div class="tabPanelArea_1">
                <%
                        pageContext.setAttribute("TSCtab", "3");
                         ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");

                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <form name="confidates" method="post" action='<%=request.getContextPath()%>/ConfigureDateServlet?tenderId=<%= tenderId %>&lotId=<%=lotId %>&contractSignId=<%=ContractSignId%>&action=saveConfigureDates'>
                    <div class="tabPanelArea_1">                    
                 <%
                    String message="";
                    if(request.getParameter("msg")!=null)
                    {
                        message = request.getParameter("msg");
                    }
                %>
                <%if(procnature.equalsIgnoreCase("works") && !"succ".equalsIgnoreCase(message) && !"update".equalsIgnoreCase(message)){%>
                <div  class='responseMsg noticeMsg t-align-left'>On configuration of Contract Start Date and End Date, system will allow Contractor to create Work Program</div><br/>
                <%}%>
                <%
                    if("succ".equalsIgnoreCase(message))
                    {
                %>
                        <%if(procnature.equalsIgnoreCase("goods")){%>
                        <div class="responseMsg successMsg t_space"><span><%=bdl.getString("CMS.goods.configuredates")%></span></div>
                        <br/><div  class='responseMsg noticeMsg t-align-left'>Please edit Days in Delivery Schedule if required</div>
                        <%}else if(procnature.equalsIgnoreCase("Services")){%>
                        <div class="responseMsg successMsg t_space"><span><%=bdl.getString("CMS.Service.configuredates")%></span></div>
                        <%}else{%>
                        <div class="responseMsg successMsg t_space"><span><%=bdl.getString("CMS.works.configuredates")%></span></div>
                        <%}%>
                        <br/>                        
                        
                <%  }else if("fail".equalsIgnoreCase(message)){%>
                        <div class="responseMsg errorMsg t_space"><span>Contract Date(s) Configuration failed</span></div>
                <%}else if("update".equalsIgnoreCase(message)){%>
                        <div class="responseMsg successMsg t_space"><span>Contract Date(s) extended successfully</span></div>
                        <br/>
                        <%if(procnature.equalsIgnoreCase("goods")){%>
                        <div  class='responseMsg noticeMsg t-align-left'>Please edit dates in Delivery Schedule if required</div>
                        <%}%>
                <%}else if("updatefail".equalsIgnoreCase(message)){%>
                        <div class="responseMsg errorMsg t_space"><span>Contract Date(s) extension failed</span></div>
                <%}%>
                    <div class="tabPanelArea_1 t_space">
                    <table cellspacing="0" class="tableList_1  t_space" width="100%">
                    <%
                         CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                         List<Object[]> NOAList = cmsConfigDateService.getNoaissueDetails(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                         List<TblTenderLotSecurity> appList = cmsConfigDateService.getTenderLotSecurity(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                         List<TblContractSign> ContractsignList = cmsConfigDateService.getContractSigningDate(Integer.parseInt(ContractSignId));
                         List<Object[]> getTentDtInServicesCase = cmsConfigDateService.getTentativeDatesInServicesCase(Integer.parseInt(tenderId));
                         List<Object[]> listofAppDates = cmsConfigDateService.getAPPDates(Integer.parseInt(tenderId));
                         Object[] noaObj = null;
                         if(!NOAList.isEmpty())
                         {
                             noaObj = NOAList.get(0);
                             System.out.println("the size of the list is ::::::::::::::::::::::"+NOAList.size());
                         }
                    %>
                    <tr>
                        <td class="ff" width="16%" class="t-align-left">Contract No :</td>
                        <td   class="t-align-left" width="19%" ><%=noaObj[0]%></td>
                        <td class="ff" width="14%"  class="t-align-left">Contract Name :</td>
                        <td  class="t-align-left"><%=noaObj[2]%></td>
                    </tr> 
                    
                    </table>
                    <%if(!"Services".equalsIgnoreCase(procnature)){%>
                    <%
                        if(!listofAppDates.isEmpty())
                        {
                            Object[] appObject = listofAppDates.get(0);
                    %>
                    
                    <table cellspacing="0" class="tableList_1  t_space" width="100%">
                    <tr>
                        <td width="18%" colspan="6" class="tableHead_1 t_space">Contract Details From APP</td>
                    </tr>
                    <tr>
                        <td class="" class="t-align-left" width="15%">Tentative Contract Start Date :</td>
                        <td class="t-align-left" width="11%"><%if(appObject[0]!=null){out.print(DateUtils.customDateFormate(DateUtils.convertStringtoDate(appObject[0].toString(),"yyyy-MM-dd")));}%></td>
                        <td class="" class="t-align-left" width="15%">Tentative Contract End Date :</td>
                        <td class="t-align-left" width="18%"><%if(appObject[1]!=null){out.print(DateUtils.customDateFormate(DateUtils.convertStringtoDate(appObject[1].toString(),"yyyy-MM-dd")));}%></td>
                        <td class=""  class="t-align-left" width="16%"></td>
                        
                    </tr>
                    </table>
                    <%}%>    
                    <%}%>
                    <table cellspacing="0" class="tableList_1  t_space" width="100%">
                    <tr>
                        <td width="18%" colspan="6" class="tableHead_1 t_space">Contract Details From Tender Notice</td>
                    </tr>

                    <%
                        if("Services".equalsIgnoreCase(procnature))
                        {
                                Object[] TentativeDt = getTentDtInServicesCase.get(0);
                    %>
                    <tr>
                        <td class="" class="t-align-left" width="20%">Contract Start Date :</td>
                        <td class="t-align-left" width="15%"><%if(TentativeDt[0]!=null){out.print(DateUtils.customDateFormate(DateUtils.convertStringtoDate(TentativeDt[0].toString(),"yyyy-MM-dd")));}%></td>
                        <td class="" class="t-align-left" width="19%">Contract End Date :</td>
                        <td class="t-align-left" width="15%"><%if(TentativeDt[1]!=null){out.print(DateUtils.customDateFormate(DateUtils.convertStringtoDate(TentativeDt[1].toString(),"yyyy-MM-dd")));}%></td>
                        <td class=""  class="t-align-left" width="16%"> Contract Duration : </td>
                        <td class="t-align-left" width="18%">
                        <%
                            Date TstDate = new Date();
                            Date TendDate = new Date(); 
                            if(TentativeDt[0]!=null){
                            TstDate =DateUtils.convertStringtoDate(TentativeDt[0].toString(),"yyyy-MM-dd");
                            TendDate = DateUtils.convertStringtoDate(TentativeDt[1].toString(),"yyyy-MM-dd");
                            long noofDays = (TendDate.getTime() - TstDate.getTime()) / (24*60*60*1000);
                            out.print(noofDays+" Days");
                            }
                        %>                         
                        </td>
                    <%
                        }else{
                    %>
                    <tr>
                        <td class="" class="t-align-left" width="20%">Contract Start Date :</td>
                        <td class="t-align-left" width="15%"><%if(!"".equalsIgnoreCase(appList.get(0).getStartTime()) && appList.get(0).getStartTime()!=null){out.print(DateUtils.customDateFormate(DateUtils.formatStdString(appList.get(0).getStartTime())));}%></td>
                        <td class="" class="t-align-left" width="19%">Contract End Date :</td>
                        <td class="t-align-left" width="15%"><%if(!"".equalsIgnoreCase(appList.get(0).getCompletionTime()) && appList.get(0).getCompletionTime()!=null){out.print(DateUtils.customDateFormate(DateUtils.formatStdString(appList.get(0).getCompletionTime())));}%></td>
                        <td class=""  class="t-align-left" width="16%"> Contract Duration : </td>
                        <td class="t-align-left" width="18%">
                        <%
                            Date CstartDate = new Date();
                            Date CendDate = new Date();
                            CstartDate = DateUtils.formatStdString(appList.get(0).getStartTime());
                            CendDate = DateUtils.formatStdString(appList.get(0).getCompletionTime());        
                            long noofDays = (CendDate.getTime() - CstartDate.getTime()) / (24*60*60*1000);                            
                        %>
                        <%=noofDays%> Days
                        </td>
                    </tr>
                    <%}%>
                    </table>
                    <table cellspacing="0" class="tableList_1  t_space" width="100%">
                    <tr>
                        <td class="tableHead_1 t_space" width="10%" colspan="4">Contract Details</td>
                    </tr>
                    <%if(!isIctTender){%>
                    <tr>
                        <td class="" class="t-align-left" width="12%">Contract Signing Date : </td>
                        <td class="t-align-left" width="9%">
                            <%=DateUtils.customDateFormate(ContractsignList.get(0).getContractSignDt())%>
                            <input type="hidden" name="ConsignDate" id="ContractsignDate" value="<%=ContractsignList.get(0).getContractSignDt()%>">
                            <%--<%=DateUtils.customDateFormate(NOAList.get(0).getContractSignDt())%>
                            <input type="hidden" name="ConsignDate" id="ContractsignDate" value="<%=NOAList.get(0).getContractSignDt()%>">--%>
                        </td>
                        <td class="" class="t-align-left" width="9%">Contract Value :</td>
                        <td class="t-align-left" width="30%"><%=new BigDecimal(noaObj[1].toString()).setScale(3)%></td>
                    </tr>
                    <%}else{PerformanceSecurityICT perfSecurityICT  = (PerformanceSecurityICT) AppContext.getSpringBean("PerformanceSecurityICT");
                List<GetCurrencyAmount> currencyAmount = new ArrayList<GetCurrencyAmount>();
                currencyAmount= perfSecurityICT.getCurrencyAmount(Integer.parseInt(tenderId), Integer.parseInt(roundID));
                int count=0;
                if(currencyAmount.get(0).getValue1()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue2()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue3()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue4()!=0)
                    count=count+1;%>
                        <%if(count>=1){%>
                        <tr>
                        <td  rowspan="<%=count%>" class="t-align-left" width="12%">Contract Signing Date : </td>
                        <td  rowspan="<%=count%>" class="t-align-left" width="9%">
                            <%=DateUtils.customDateFormate(ContractsignList.get(0).getContractSignDt())%>
                            <input type="hidden" name="ConsignDate" id="ContractsignDate" value="<%=ContractsignList.get(0).getContractSignDt()%>">
                            <%--<%=DateUtils.customDateFormate(NOAList.get(0).getContractSignDt())%>
                            <input type="hidden" name="ConsignDate" id="ContractsignDate" value="<%=NOAList.get(0).getContractSignDt()%>">--%>
                        </td>
                        <td rowspan="<%=count%>" class="t-align-left" width="9%">Contract Value :</td>
                        <td class="t-align-left" width="30%"><% conAmount = new Double(currencyAmount.get(0).getValue1()); cAmount  = new DecimalFormat("##.##").format(conAmount); out.println(""+cAmount+" (in " + currencyAmount.get(0).getCurrencyName1() + ")");%></td>
                    </tr>
                    <%}%>

                    <%if(count>=2){%>
                        <tr>
                        <td class="t-align-left" width="30%"><% conAmount = new Double(currencyAmount.get(0).getValue2()); cAmount  = new DecimalFormat("##.##").format(conAmount); out.println(""+cAmount+" (in " + currencyAmount.get(0).getCurrencyName2() + ")");%></td>
                    </tr>
                    <%}%>

                    <%if(count>=3){%>
                        <tr>
                        <td class="t-align-left" width="30%"><% conAmount = new Double(currencyAmount.get(0).getValue3()); cAmount  = new DecimalFormat("##.##").format(conAmount); out.println(""+cAmount+" (in " + currencyAmount.get(0).getCurrencyName3() + ")");%></td>
                    </tr>
                    <%}%>

                    <%if(count>=4){%>
                        <tr>
                        <td class="t-align-left" width="30%"><% conAmount = new Double(currencyAmount.get(0).getValue4()); cAmount  = new DecimalFormat("##.##").format(conAmount); out.println(""+cAmount+" (in " + currencyAmount.get(0).getCurrencyName4() + ")");%></td>
                    </tr>
                    <%}%>



                    <%}%>



                    </table>
                    <table cellspacing="0" class="tableList_1  t_space" width="100%">
                    <tr>
                        <td class="tableHead_1 t_space" width="10%" colspan="4">Contract effective Date</td>
                    </tr>
                    <tr>
                        <%   Date d_date  = null;
                             Date d_Enddate = null;
                             Date wpendDate = null;
                             String NoOfDays = "0";
                             boolean editFlag = false;
                             boolean isGood = false; String str ="";                               
                             List<TblCmsDateConfig> configdatalist = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId),Integer.parseInt(lotId),Integer.parseInt(ContractSignId));
                             if(!configdatalist.isEmpty())
                             {
                                 editFlag = true;
                                 d_date = configdatalist.get(0).getActualStartDt();
                                 d_Enddate = configdatalist.get(0).getActualEndDate();
                             }
                             TenderService tenderService = (TenderService) AppContext.getSpringBean("TenderService");
                             List<TblTenderDetails> checkproctype = tenderService.getTenderStatus(Integer.parseInt(tenderId));
                             List<Object[]> wpmaxdate = cmsConfigDateService.getmaxWpDate(Integer.parseInt(lotId));
                             if(wpmaxdate!=null)
                             {  Object[] wpobjj = wpmaxdate.get(0);
                                if(wpobjj[1]!=null)
                                {
                                    NoOfDays = wpobjj[1].toString();
                                }
                             }
                             if(!checkproctype.isEmpty())
                             {
                                if(("Goods".equalsIgnoreCase(checkproctype.get(0).getProcurementNature())||"Services".equalsIgnoreCase(checkproctype.get(0).getProcurementNature())) && editFlag==false)
                                {
                                    isGood= true;                                                                        
                                    if(wpmaxdate!=null)
                                    {   Object[] wpobj = wpmaxdate.get(0);
                                        if(wpobj[0]!=null)
                                        {
                                            wpendDate = DateUtils.convertStringtoDate(wpobj[0].toString(),"yyyy-MM-dd HH:mm:ss");
                                        }
                                        if(wpobj[1]!=null)
                                        {
                                            NoOfDays = wpobj[1].toString();
                                        }
                                    }  
                                }
                             }
                        %>
                        <td class="" class="t-align-left" width="14%">Actual Contract Start Date :
                            <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>"/>
                        <input type="hidden" name="enddate" id="EditedEnddate" <%if(editFlag){%>value="<%=configdatalist.get(0).getActualEndDate()%>"<%}%> />
                        </td>
                        <%
                            if(isGood)
                            {
                        %>
                        <td class="t-align-left" width="11%">
                            <input name="ConStartdate" type="text" class="formTxtBox_1" <%if(isGood){%>value="<%=DateUtils.customDateFormate(ContractsignList.get(0).getContractSignDt())%>"<%}%>readonly="true" id="CstartDate" style="width:100px;" onblur="return validateStartDate()"onfocus="GetCal('CstartDate','CstartDate');"/>
                            <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtCstartDate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('CstartDate','txtCstartDate');"/></a>
                            <span class="reqF_1" id="CstartDatespan"></span>
                        </td>
                        <td class="" class="t-align-left" width="13%"><%if(procnature.equalsIgnoreCase("works")){%>Actual Contract End Date : <%}else if(procnature.equalsIgnoreCase("services")){%>Actual Completion date<%}else{%>Actual Delivery Date :<%}%></td>
                        <td class="t-align-left" width="35%">
                            <input name="ConEnddate" type="text" class="formTxtBox_1" <%if(!"services".equalsIgnoreCase(procnature)){if(!isGood &&wpendDate!=null){%>value="<%=DateUtils.customDateFormate(wpendDate)%>"<%}}%> readonly="true" id="CendDate" style="width:100px;" onblur="return validateEndDate()"onfocus="GetCal('CendDate','CendDate');"/>
                            <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtCendDate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('CendDate','txtCendDate');"/></a>
                        <span class="reqF_1" id="ConEnddatespan"></span>
                        </td>
                        <%}else{%>
                        <td class="t-align-left" width="11%">
                            <input name="ConStartdate" type="text" class="formTxtBox_1" <%if(editFlag){%>value="<%=DateUtils.customDateFormate(d_date)%>"<%}%>readonly="true" id="CstartDate" style="width:100px;" onblur="return validateStartDate()"onfocus="GetCal('CstartDate','CstartDate');"/>
                            <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtCstartDate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('CstartDate','txtCstartDate');"/></a>
                            <span class="reqF_1" id="CstartDatespan"></span>
                            <input type="hidden" name="actContractId" id="" <%if(editFlag){%>value="<%=configdatalist.get(0).getActContractDtId()%>"<%}%> />                            
                        </td>                        
                        <td class="" class="t-align-left" width="13%"><%if(procnature.equalsIgnoreCase("works")){%>Actual Contract End Date : <%}else if(procnature.equalsIgnoreCase("services")){%>Actual Completion date<%}else{%>Actual Delivery Date :<%}%></td>
                        <td class="t-align-left" width="35%">
                            <input name="ConEnddate" type="text" class="formTxtBox_1" <%if(editFlag){%>value="<%=DateUtils.customDateFormate(d_Enddate)%>"<%}%> readonly="true" id="CendDate" style="width:100px;" onblur="return validateEndDate()"onfocus="GetCal('CendDate','CendDate');"/>
                            <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtCendDate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('CendDate','txtCendDate');"/></a>
                        <span class="reqF_1" id="ConEnddatespan"></span>
                        </td>
                        <%}%>
                    </tr>
                    </table>
                    </div>
                        <div align="center">
                            <span class="formBtn_1 t_space">
                                <%if(editFlag){%>
                                <input type="submit" name="submitbtn" id="subconfidate" value="Update" onclick="return validateConStartDate()"/>
                                <%}else{%>
                                <input type="submit" name="submitbtn" id="subconfidate" value="Submit" onclick="return validateConStartDate()"/>
                                <%}%>
                            </span>
                        </div>
                    </div>
                    </form>
                </div>
                </div>              
                </div>
                <div>&nbsp;</div>
                <%@include file="../resources/common/Bottom.jsp" %>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
            /*For jquery calander*/
            function GetCal(txtname,controlname){
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d-%b-%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        if('<%=checkproctype.get(0).getProcurementNature()%>'== 'Goods')
                        {
                            if(txtname == 'CstartDate')
                            {
                                validateStartDate();
                            }
                        }
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }            
            function validateConStartDate()
            {
                var flag = true;
                if(document.getElementById("CstartDate").value=="")
                {
                    document.getElementById("CstartDatespan").innerHTML="please select Contract Start Date";
                    flag = false;
                }
                else
                {
                   try{
                    $.ajax({
                        url: "<%=request.getContextPath()%>/ConfigureDateServlet?param1="+$('#CstartDate').val()+"&param2="+$('#ContractsignDate').val()+"&tenderId="+$('#tenderId').val()+"&action=validateConStartdate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            if(j.toString()!="")
                            {
                                document.getElementById("CstartDatespan").innerHTML=j;
                                flag = false;
                            }
                        }
                    });
                   }catch(e){
                   }                                                         
                }
                if(document.getElementById("CendDate").value=="")
                {
                    <%if(procnature.equalsIgnoreCase("goods")){%>
                    document.getElementById("ConEnddatespan").innerHTML="please select Delivery Date";
                    <%}else if(procnature.equalsIgnoreCase("services")){%>
                    document.getElementById("ConEnddatespan").innerHTML="please select Completion Date";    
                    <%}else{%>
                    document.getElementById("ConEnddatespan").innerHTML="please select Contract End Date";    
                    <%}%>    
                    flag = false;
                }
                else
                {
                   try{
                    $.ajax({
                        url: "<%=request.getContextPath()%>/ConfigureDateServlet?param1="+$('#CendDate').val()+"&param2="+$('#CstartDate').val()+"&param3="+$('#EditedEnddate').val()+"&tenderId="+$('#tenderId').val()+"&action=validateConEnddate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            if(j.toString()!="")
                            {
                                document.getElementById("ConEnddatespan").innerHTML=j;
                                flag = false;
                            }
                        }
                    });
                   }catch(e){
                   }
                }
                if('<%=checkproctype.get(0).getProcurementNature()%>'== 'Goods')
                {
                    var a = getDate(document.getElementById("CstartDate").value);
                    var enddate = new Date(a);
                    var b = enddate.getTime()+(parseInt('<%=NoOfDays%>')*24*60*60*1000);
                    var hdf = new Date(b);
                    var monthname = new Array("Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec");
                    var finalenddate = hdf.getDate()+"-"+monthname[hdf.getMonth()]+"-"+hdf.getFullYear();
                    var c = new Date(getDate(document.getElementById("CendDate").value));
                    if((new Date(c).getTime() < new Date(b).getTime()))
                    {
                        <%if(procnature.equalsIgnoreCase("goods")){%>
                        document.getElementById("ConEnddatespan").innerHTML="Difference of Actual Starts Date and Actual Delivery Date should be Greater than or Equal to No. of MAX Days (<%=NoOfDays%>)";
                        <%}else if(procnature.equalsIgnoreCase("services")){%>
                        document.getElementById("ConEnddatespan").innerHTML="Difference of Commencement Starts Date and Commencement Completion Date should be Greater than or Equal to No. of MAX Days (<%=NoOfDays%>)";    
                        <%}else{%>
                        document.getElementById("ConEnddatespan").innerHTML="Difference of Commencement Starts Date and Commencement End Date should be Greater than or Equal to No. of MAX Days (<%=NoOfDays%>)";
                        <%}%>    
                        flag = false;
                    }
                }
                return flag;
            }
            function getDate(date)
            {
                var splitedDate = date.split("-");
                return Date.parse(splitedDate[1]+" "+splitedDate[0]+", "+splitedDate[2]);
            }
            function validateStartDate()
            {
               if('<%=checkproctype.get(0).getProcurementNature()%>'== 'Goods')
                {
                   var a = getDate(document.getElementById("CstartDate").value);
                   var enddate = new Date(a);
                   var b = enddate.getTime()+(parseInt('<%=NoOfDays%>')*24*60*60*1000);
                   var hdf = new Date(b);
                   var monthname = new Array("Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec");
                   var finalenddate = hdf.getDate()+"-"+monthname[hdf.getMonth()]+"-"+hdf.getFullYear();
                   document.getElementById("CendDate").value =finalenddate;
                }
               if(document.getElementById("CstartDate").value!=""){
                   document.getElementById("CstartDatespan").innerHTML="";
               }
            }
            function validateEndDate()
            {
                if(document.getElementById("CendDate").value!=""){
                    document.getElementById("ConEnddatespan").innerHTML="";
                }
            }
            if('<%=checkproctype.get(0).getProcurementNature()%>'== 'Goods' && <%=editFlag%>==false)
            {
                validateStartDate();
            }
   </script>
</html>
