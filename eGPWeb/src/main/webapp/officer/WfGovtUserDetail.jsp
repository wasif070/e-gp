<%-- 
    Document   : ViewGovtUserDetail
    Created on : Nov 13, 2010, 2:31:37 PM
    Author     : parag
--%>

<%@page import="org.apache.log4j.Logger"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="govSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserSrBean" />
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod,java.util.List,com.cptu.egp.eps.model.view.VwEmpfinancePower" %>
<%@page import="java.util.List,com.cptu.egp.eps.model.table.TblEmployeeRoles,com.cptu.egp.eps.model.table.TblDepartmentMaster" %>
<%@page import="com.cptu.egp.eps.model.table.TblEmployeeOffices,com.cptu.egp.eps.dao.storedprocedure.SPGovtEmpRolesReturn" %>
<%
            Logger LOGGER = Logger.getLogger("WfGovtUserDetail.jsp");
            String str = request.getContextPath();

            //createGovtUser
            //editGovtUser
            String action = "createGovtUser";
            int userId = 0;
            int empId = 0;
            String mode="";
            String fromWhere="";

            if (request.getParameter("empId") != null) {
                empId = Integer.parseInt(request.getParameter("empId").toString());
            }
            
            
            if (request.getParameter("mode") != null) {
                mode = request.getParameter("mode").toString();
            }
            if (request.getParameter("userId") != null) {
                userId = Integer.parseInt(request.getParameter("userId"));
                govSrUser.setUserId(userId);
            }
            else{
                userId=govSrUser.takeUserIdFromEmpId(empId);
                govSrUser.setUserId(userId);
            }
            if(request.getParameter("fromWhere") != null){
                fromWhere = request.getParameter("fromWhere");
            }
            
            
            if("SearchUser".equalsIgnoreCase(fromWhere)){
                try {
                    empId = govSrUser.takeEmpIdFromUserId(userId);
                } catch (Exception e) {
                    LOGGER.error(" error in takeEmpIdFromUserId  === "+e);
                }
            }
            govSrUser.setEmpId(empId);
            govSrUser.setUserId(userId);
            

            Object[] object = null;
            object = govSrUser.getGovtUserData();

            

            List<SPGovtEmpRolesReturn> empRolesReturn = govSrUser.getEmpRoleses(empId);
            
            
            List<VwEmpfinancePower> vwFinancePower=govSrUser.getGovtFinanceRole();
            


%>
<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - New Government User Registration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>       
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
              
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                                                                          
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">View Government User Detail</div>
                                        <%-- <div class="pageHead_1">Create User</div>--%>
                                        <form method="POST" id="frmGovUser" action="<%=str%>/GovtUserSrBean?action=<%=action%>">

                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td class="ff">Unique e-mail ID : </td>
                                                    <td><% out.print((String) object[0]);%>
                                                        

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Full Name : </td>
                                                    <td><% out.print((String) object[2]);%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Name in Bangla : </td>
                                                    <td><% out.print("" + new String((byte[]) object[3]));%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">National Id : </td>
                                                    <td><%out.print((String) object[5]);%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Mobile No : </td>
                                                    <td><%out.print((String) object[4]);%>                                                       
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        <%if(!"SearchUser".equalsIgnoreCase(fromWhere)){%>
                                            <table width="100%" cellspacing="0" class="tableList_1 t_space" >
                                                <tr>
                                                    <th class="t-align-center" width="5%" align="center">Sr No. </th>
                                                    <th class="t-align-center" align="center">Employee Name </th>
                                                    <th class="t-align-center" align="center">Department Name </th>
                                                    <th class="t-align-center" align="center">Office</th>
                                                    <th class="t-align-center" align="center">Designation </th>
                                                    <th class="t-align-center" align="center">Procurement Role </th>                                                    
                                                    <th class="t-align-center" align="center">Financial Power</th>
                                                </tr>
                                                <%
                                                            /*List<TblEmployeeRoles> empRoles = govSrUser.getEmployeeRoles();
                                                            
                                                            String rEmp = "";
                                                            for (TblEmployeeRoles roles : empRoles) {
                                                            rEmp += roles.getTblProcurementRole().getProcurementRole() + ",";
                                                            }

                                                            departmentId = empRoles.get(0).getTblDepartmentMaster().getDepartmentId();
                                                            govSrUser.setDepartmentId(departmentId);

                                                            TblDepartmentMaster departmentMaster = govSrUser.getDepartmentMasterData();
                                                            govSrUser.setUserId(userId);
                                                            
                                                            Object[] object = govSrUser.getGovtUserData();
                                                            designationId = (Integer) object[6];
                                                            String employeeName = (String) object[2];
                                                            String designationName = govSrUser.takeDesignationName(designationId);*/

                                                            for (SPGovtEmpRolesReturn govtEmpRolesReturn : empRolesReturn) {
                                                                String roleId = govtEmpRolesReturn.getProcurementRoleId();
                                                                String roleName = govtEmpRolesReturn.getProcurementRole();
                                                                String finPowerBy = govtEmpRolesReturn.getFinPowerBy();
                                                                int deptId = govtEmpRolesReturn.getDepartmentId();
                                                                String empRoleId = govtEmpRolesReturn.getEmployeeRoleId();
                                                                String officeId = govtEmpRolesReturn.getOfficeId();
                                                                //govtEmpRolesReturn.
%>
                                                <tr>
                                                    <td class="t-align-center" width="5%" >1</td>
                                                    <td class="t-align-center"><%=govtEmpRolesReturn.getEmployeeName()%></td>
                                                    <td class="t-align-center"><%=govtEmpRolesReturn.getDepartmentname()%></td>
                                                    <td class="t-align-center"><%=govtEmpRolesReturn.getOfficeName()%></td>
                                                    <td class="t-align-center"><%=govtEmpRolesReturn.getDesignationName()%></td>
                                                    <td class="t-align-center"><%=govtEmpRolesReturn.getProcurementRole()%></td>
                                                   <%-- <td class="t-align-center" >
                                                        <a href="GovtUserFinanceRole.jsp?Edit=Edit&userId=<%=userId%>&empId=<%=empId%>&empRoleId=<%=empRoleId%>&roleId=<%=roleId%>&roleName=<%=roleName%>&finPowerBy=<%=finPowerBy%>&deptId=<%=deptId%>"> Assign</a> </td>--%>
                                                    <td class="t-align-center">
                                                        <a href="#" onclick="window.open('ViewGovtUserFinancePower.jsp?userId=<%=userId%>&empId=<%=empId%>&deptId=<%=deptId%>','window','width=900,height=200')" title="View">View</a>
                                                    </td>
                                                </tr>
                                                <%
                                                            }
                                                %>

                                            </table>
                                                <%if("finalSubmission".equalsIgnoreCase(mode)){ %>
                                                <table border="0" cellspacing="10" cellpadding="0" width="100%" >
                                                <tr class="t-align-center">
                                                    <td class="t-align-center">&nbsp;</td>
                                                    <td class="t-align-center">
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnSubmission" id="btnSubmission" value="Final Submission" onclick="finalSubmission();" />
                                                        </label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <%}%>
                                          <%}%>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabWorkFlow");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<form id="form1" method="post">
    <input type="hidden" name="hidEmailId" id="hidEmailId" value="<%=(String) object[0]%>" />
   <input type="hidden" name="hidPassword" id="hidPassword" value="<%=(String) object[1]%>" />
    <input type="hidden" name="hidMobNo" id="hidMobNo" value="<%=(String) object[4]%>" />
</form>
<script>
function finalSubmission(){
    var flag=false;
     //$.post("<%=request.getContextPath()%>/GovtUserSrBean", {userId:<%//userId%>,empId:<%=empId%>,action:'finalSubmission'}, function(j){
     //       alert(j);
     //       flag=true;
     // });
      document.getElementById("form1").action="<%=request.getContextPath()%>/GovtUserSrBean?userId=<%=userId%>&empId=<%=empId%>&action=finalSubmission";
      document.getElementById("form1").submit();
}

</script>
