<%-- 
    Document   : ViewPSHistoryDetails
    Created on : Dec 20, 2011, 2:36:45 PM
    Author     : shreyansh.shah
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvStaffSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTeamComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <% ResourceBundle srbd = null;
                    srbd = ResourceBundle.getBundle("properties.cmsproperty");
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment Schedule</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

    </head>
    <body>
        <%
                    String referpage = request.getHeader("referer");
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String type = "";
                    int tenderId = 0;
                    int lotId = 0;
                    int formMapId = 0;
                    int histCnt = 0;
                    String styleClass = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                    }
                    if (request.getParameter("formMapId") != null) {
                        formMapId = Integer.parseInt(request.getParameter("formMapId"));
                    }
                    if (request.getParameter("histCnt") != null) {
                        histCnt = Integer.parseInt(request.getParameter("histCnt"));
                    }
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                    boolean flag = false;
                    CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                    String serviceType = commService.getServiceTypeForTender(tenderId);
                    if ("Time based".equalsIgnoreCase(serviceType.toString())) {
                        flag = true;
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">
                        <%       out.print("Payment Schedule");
                        %>
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="<%=referpage%>" title="Go Back">Go Back</a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%if (request.getParameter("lotId") != null) {%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <%}%>
                    <form name="frm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post" >
                        <input type="hidden" name="addcount" id="addcount" value="" />
                        <input type="hidden" name="action" id="action" value="" />
                        <input type="hidden" name="tenderId" id="tenderId" value="2229" />
                        <input type="hidden" name="wpId" id="wpId" value="5" />
                        <input type="hidden" name="lotId" id="lotId" value="5" />
                        <div id="resultDiv" style="display: block;">
                            <div  id="print_area">

                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                    <tr>
                                        <th>S No</th>
                                        <%if (!flag) {%>
                                        <th>Milestone Name </th>
                                        <%}%>
                                        <th>Description</th>
                                        <%if (!flag) {%>
                                        <th>Payment as % of Contract Value</th>
                                        <%}%>
                                        <th>Mile Stone Date proposed by PE </th>
                                        <th>Mile Stone Date proposed by Consultant</th>
                                        <th>Creation Date</th>
                                    </tr>
                                    <%
                                                List<Object[]> list = cmss.getPaymentScheduleHistory(formMapId, histCnt);
                                                try {

                                                    if (list != null && !list.isEmpty()) {
                                                        for (int i = 0; i < list.size(); i++) {
                                                            if (i % 2 == 0) {
                                                                styleClass = "bgColor-white";
                                                            } else {
                                                                styleClass = "bgColor-Green";
                                                            }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=list.get(i)[2]%></td>
                                        <%if (!flag) {%>
                                        <td class="t-align-left"><%=list.get(i)[3]%></td>
                                        <% }%>
                                        <td class="t-align-left"><%=list.get(i)[5]%></td>
                                        <%if (!flag) {%>
                                        <td style="text-align :right;"><%=list.get(i)[4]%></td>
                                        <% }%>
                                        <td class="t-align-left"><%=DateUtils.customDateFormate((Date) list.get(i)[6])%></td>
                                        <td class="t-align-left"><%=DateUtils.customDateFormate((Date) list.get(i)[7])%></td>
                                        <%if(list.get(i)[8]!=null){%>
                                            <td class="t-align-center"><%=DateUtils.gridDateToStr((Date)list.get(i)[8])%></td>
                                        <%}else{%>
                                            <td class="t-align-center">-</td>
                                        <%}%>
                                    </tr>
                                    <%}


                                                        if (request.getParameter("lotId") != null) {
                                                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Payment Schedule history details", "");
                                                        }
                                                    } else {
                                                        out.print("<tr>");
                                                        out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                                                        out.print("</tr>");
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                    %>



                                </table>
                            </div>
                        </div>


                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" id="first" value="0"/>
                        <br />
                        <center>



                        </center>
                    </form>
                </div>

                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>

