<%--
    Document   : NOA
    Created on : Dec 24, 2010, 4:46:50 PM
    Author     : rishita
--%>


<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderForms"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPayment"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
                ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                /* Dohatec Start*/
                String procType = commonService.getProcurementType(request.getParameter("tenderId")).toString();
                /* Dohatec End*/
                String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
                String strProcNature = "";
                if("Services".equalsIgnoreCase(procnature))
                {
                    strProcNature = "Consultant";
                }else if("goods".equalsIgnoreCase(procnature)){
                    strProcNature = "Supplier";
                }else{
                    strProcNature = "Contractor";
                }
                String btnName = "";
                String form = "";
                String invalidStructure="";
                if (procnature.equalsIgnoreCase("goods")) {
                    form = bdl.getString("CMS.Goods.Forms");
                    invalidStructure = bdl.getString("CMS.Goods.Err.InvalidStructure");
                } else if (procnature.equalsIgnoreCase("works")) {
                    form =  bdl.getString("CMS.works.Forms");
                    invalidStructure = bdl.getString("CMS.works.Err.InvalidStructure");
                } else {
                    //form =  bdl.getString("CMS.services.Forms");
                }
                /* CAN BE USED FOR GETTING ROUNDID
                String roundIdNew="";
                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> perSecListing = new ArrayList<SPCommonSearchDataMore>();
                perSecListing = commonSearchDataMoreService.geteGPData("getDataForPerSec",tenderId,lotId);
                roundIdNew = perSecListing.get(0).getFieldName2();
 */
    %>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    String tenderId = "";
                    String roundId="";
                    if (request.getParameter("tenderId") != null) {
                        tenderId = request.getParameter("tenderId");
                    }
                    boolean isPE = false;

                    String pckLotId = "-1";
                    String procurementNature = commonService.getProcNature(tenderId).toString();
                    if(procurementNature.equalsIgnoreCase("Services")){
                        for(Object[] objPck: commonService.getPkgDetailWithAppPkgId(Integer.parseInt(tenderId))){
                            pckLotId = objPck[2].toString();
                        }
                    }
                    TenderCommonService tcs1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                     List<SPTenderCommonData> checkPE = tcs1.returndata("getPEOfficerUserIdfromTenderId", tenderId, session.getAttribute("userId").toString());
                                            if (!checkPE.isEmpty() && session.getAttribute("userId").toString().equals(checkPE.get(0).getFieldName1())) {
                                                isPE = true;
                                       }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Letter of Acceptance (LOA)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
        <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
        <script>

            function view(wpId,tenderId,lotId){
                dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&noa=noa");
            }


            function checkFor(lotId,tenderId){
                var frmObj = document.getElementById("frmcons_"+lotId);
                var count = document.getElementById("count_"+lotId).value;
                var flag = true;
                for(var i=0;i<count;i++){
                    if(document.getElementById("boqcheck_"+i).checked){
                        flag=false;
                    }
                    if(!flag){
                        break;
                    }
                }
                if(flag){
                    jAlert("Please Select <%=form%> to Consolidate ","NOA", function(RetVal) {
                    });
                }else{
                    document.getElementById("Boqbutton").style.display='none';
                    frmObj.action="<%=request.getContextPath()%>/ConsolidateServlet?lotId="+lotId+"&tenderId="+tenderId+"&action=consolidate";
                    frmObj.submit();
                }

            }
            function finish(lotId,tenderId){
                var frmObj = document.getElementById("frmcons_"+lotId);
                document.getElementById("finishconsolidation").style.display='none';
                frmObj.action="<%=request.getContextPath()%>/ConsolidateServlet?lotId="+lotId+"&tenderId="+tenderId+"&action=finish";
                frmObj.submit();


            }
            function finishForService(lotId,tenderId){
                var frmObj = document.getElementById("frmcons_"+lotId);
                frmObj.action="<%=request.getContextPath()%>/CMSSerCaseServlet?lotId="+lotId+"&tenderId="+tenderId+"&action=dumpForm";
                frmObj.submit();


            }
            function deleteRow(wpId,tenderId,lotId){
        var frmObj = document.getElementById("frmcons_"+lotId);
                frmObj.action="<%=request.getContextPath()%>/ConsolidateServlet?wpId="+wpId+"&action=delete&tenderId="+tenderId;
                frmObj.submit();
            }
            function Edit(wpId,tenderId,lotId){
                dynamicFromSubmit("DateEditInNOA.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
            }
            function finishdates(lotId,tenderId){
        var frmObj = document.getElementById("frmcons_"+lotId);
                frmObj.action="<%=request.getContextPath()%>/ConsolidateServlet?lotId="+lotId+"&tenderId="+tenderId+"&action=finishdates";
                frmObj.submit();
            }

        </script>
    </head>
    <body>


        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">
                    <%
                    boolean isIctTender = false;
                    String NOADA = "";
                    String bidderId = "0";
                    String pkgLotId = "0";
                    String pkg = "Lot";
                    if(procurementNature.equalsIgnoreCase("Services") ){
                            NOADA = "NOA";
                            pkg = "Package";
                            out.print("Letter of Acceptance (LOA)");
                        }else{
                            NOADA = "NOA";
                            out.print("Letter of Acceptance (LOA)");
                        }%>
                </div>
                 <%
                     if("y".equalsIgnoreCase(request.getParameter("msgFlag"))){
                 %>
                 <div class="responseMsg successMsg t_space" style="margin-top: 10px;"><span>Performance Security amount submitted successfully</span></div>
                 <%
                         }
                %>
                <%
                            String dateTender = "";
                            boolean isPerSec = false;
                            // Variable tenderId is defined by u on ur current page.
                            if (request.getParameter("tenderId") != null) {
                                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                            }
                            boolean isPerfSecurityAvail = false;
                            List<Object[]> os = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                            if(os!=null && !os.isEmpty() && "yes".equalsIgnoreCase(os.get(0)[1].toString())){
                                isPerfSecurityAvail = true;
                            }
                            
                            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            String userId = "";
                            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                //userId = Integer.parseInt(session.getAttribute("userId").toString());
                                userId = session.getAttribute("userId").toString();
                                issueNOASrBean.setLogUserId(userId);
                                commonSearchService.setLogUserId(userId);
                            }
                            //pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                            pageContext.setAttribute("tab", "13");
                            //List<SPCommonSearchData> submitDateSts = commonSearchService.searchData("chkSubmissionDt", tenderId, null, null, null, null, null, null, null, null);
                            //List<SPCommonSearchData> packageList = commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null);
                            CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            List<Object[]> objBusinessRule = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                            List<SPCommonSearchData> issueCon = commonSearchService.searchData("ProcureNOA", tenderId, "", "", null, null, null, null, null, null);
                            List<SPCommonSearchDataMore> checkIssued = null;
                            List<SPCommonSearchDataMore> perSecAmt = new ArrayList<SPCommonSearchDataMore>();

                            List<Object[]> obj_PS = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));//for checking performance security configured or not
                            

                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
                <%@include  file="officerTabPanel.jsp"%>

                <div class="tabPanelArea_1">
                    <%
                                if (request.getParameter("msg") != null) {
                                    if (request.getParameter("msg").equals("tt")) {
                    %>

                    <div class='responseMsg errorMsg'><%=invalidStructure%>. Please Select and Consolidate One By One</div>

                    <%} else if (request.getParameter("msg").equals("del")) {%>
                    <div class='responseMsg successMsg'>Consolidated <%= form%> deleted successfully</div>

                    <%} else if (request.getParameter("msg").equals("con")) {%>
                    <div class='responseMsg successMsg'>Selected <%= form%> has been consolidated successfully</div>
                    <% } else if (request.getParameter("msg").equals("fin")) {%>
                    <div class='responseMsg successMsg'><%= form%> consolidation completed successfully</div>
                    <% }else if (request.getParameter("msg").equals("t")) {%>
                    <div class='responseMsg errorMsg'><%= form%> consolidation failed</div>
                    <% }  else if (request.getParameter("msg").equals("edit")) {%>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.dates.edit")%></div>
                    <%} else if (request.getParameter("msg").equals("editfinish")) {%>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.dates.finalize")%></div>
                     <%} else if (request.getParameter("msg").equals("issuenoa")) {%>
                     <div class='responseMsg successMsg'><%=bdl.getString("CMS.NOAIssue")%></div>
                    <% }


                                }%>
                    <% if (request.getParameter("action") != null && "success".equals(request.getParameter("action"))) {%>
                    <div class='responseMsg successMsg'><%=NOADA%> Issued successfully</div>
                    <% }%>
                    <% if (request.getParameter("action") != null && "fais".equals(request.getParameter("action"))) {%>
                    <div class='responseMsg errorMsg'><%=NOADA%> has already issued</div>
                    <% }%>

                    <div align="center">
                        <%--<table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1">
                            <tr>
                                <%
                                List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("GetLotPkgDetail", tenderId, "0");
                                %>
                                <td width="15%" class="ff">Package  No. :</td>
                                <td class="t-align-left"><%if(!tenderLotList.isEmpty()){%><%=tenderLotList.get(0).getFieldName1() %><% } %></td>
                            </tr>
                            <tr>
                                <td class="ff">Package Description :</td>
                                <td class="t-align-left"><%if(!tenderLotList.isEmpty()){%><%=tenderLotList.get(0).getFieldName2() %><% } %></td>
                            </tr>
                        </table>--%>
                        <%          List<SPTenderCommonData> tenderValSec = tenderCommonService.returndata("getTenderDatesDiff", tenderId, null);
                                     boolean isUserPE = false;
                                     boolean isCreator = false;
                                     String field1 = "CheckCreator";
                                     WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                                     List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, tenderId, userId);
                                     if (editdata.size() > 0) {
                                        isCreator = true;
                                     }
                                        Object acc_objProRole = session.getAttribute("procurementRole");
                                        String acc_strProRole = "";
                                        if (acc_objProRole != null) {
                                            acc_strProRole = acc_objProRole.toString();
                                        }
                                        String acc_chk = acc_strProRole;
                                        String acc_ck1[] = acc_chk.split(",");
                                     for (int acc_iAfterLoginTop = 0; acc_iAfterLoginTop < acc_ck1.length; acc_iAfterLoginTop++) {
                                            if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("pe")) {
                                                isUserPE = true;
                                                break;
                                            }
                                        }
                                        boolean isNoaIssued = true;
                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    service.setLogUserId(session.getAttribute("userId").toString());
                                    int i = 0;
                                    for (SPCommonSearchDataMore lotList : packageLotList) {
                                        List<SPTenderCommonData> listTenderer = tenderCommonService.returndata("GetEvaluatedBiddersRoundWise", tenderId, lotList.getFieldName5());
                                        boolean showButton = false;
                                        pckLotId = lotList.getFieldName5();
                                        
                                        List<Object[]> forms = dDocSrBean.getAllBoQForms(Integer.parseInt(lotList.getFieldName5()),Integer.parseInt(tenderId));  
                                        List<SPCommonSearchDataMore> mores2 = service.getQualifiedUser(request.getParameter("tenderId"), pckLotId);
                                        List<Object[]> formsRO = dDocSrBean.getAllBoQFormsForNOA(Integer.parseInt(lotList.getFieldName5()),tenderID,Integer.parseInt(mores2.get(0).getFieldName1()));
                                        showButton = service.getWpDetailForButtonDisplay(lotList.getFieldName5(),formsRO.size());
                                        // showButtonList<Object> wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                        /* Dohatec Start */
                                        String roundIdNew="";
                                        List<SPCommonSearchDataMore> perSecListing1 = new ArrayList<SPCommonSearchDataMore>();
                                        perSecListing1 = commonSearchDataMoreService.geteGPData("getDataForPerSec",tenderId,lotList.getFieldName5());
                                        if(!perSecListing1.isEmpty() && perSecListing1 != null)
                                            {
                                                roundIdNew = perSecListing1.get(0).getFieldName2();
                                            }                                       
                                        List<Object> wpid;
                                            if("ICT".equalsIgnoreCase(procType) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                                {
                                                   wpid = service.getWpIdForTenderFormsForNoa(Integer.parseInt(lotList.getFieldName5()),tenderId,roundIdNew);                                               
                                                }
                                            else
                                                {
                                                    wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                                }
                                        /* Dohatec End */
                                        boolean isDateEdited = service.checkForDateEditedOrNot(Integer.parseInt(lotList.getFieldName5()));

                        %>
                        <form name="frmcons_<%=lotList.getFieldName5()%>" id="frmcons_<%=lotList.getFieldName5()%>" method="post">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space t_space">
                                <tr>
                                    <td width="20%" class="ff"><%=pkg%> No. :</td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td class="ff"><%=pkg%> Description :</td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>
                                
                                <%
                                String tenderInfoStatus1 = tenderSrBean.TenderReportStatus(tenderId);
                                if(!obj_PS.isEmpty() && obj_PS.get(0)[1].toString().equalsIgnoreCase("yes")){
                            %>
                            <tr>
                                <td width="20%">Performance Security</td>

                                <td class="t-align-left" width="80%">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                        <th>Name of Bidder/Consultant</th>
                                        <th>Performance Security</th>
                                        <%
                                        
                                        for (SPCommonSearchDataMore perSecListing : commonSearchDataMoreService.geteGPData("getDataForPerSec",tenderId,pckLotId)) {
                                            roundId = perSecListing.getFieldName2();
                                            bidderId = perSecListing.getFieldName3();
                                            boolean isPerfAvail = false;
                                            boolean isPerfView = false;
                                         if(isPerfSecurityAvail){
                                            List<SPCommonSearchDataMore> perfList = dataMore.geteGPData("PerfSecurityAvailChk",roundId);
                                            if(perfList!=null && (!perfList.isEmpty()) && perfList.get(0).getFieldName1().equals("1")){
                                                isPerfAvail = true;
                                                perfList.clear();
                                            }
                                            perfList = dataMore.geteGPData("PerfSecurityNaoChk",roundId);
                                            if(perfList!=null && (!perfList.isEmpty()) && perfList.get(0).getFieldName1().equals("1")){
                                                isPerfView = true;
                                                perfList.clear();
                                            }
                                        }
                                        %>
                                        <%
                                            List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", tenderId, null);
                                            //boolean isIctTender = false;
                                            if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                isIctTender = true;
                                            }
                                            //if(isIctTender){
                                        %>
                                        <tr>
                                            <td><%=perSecListing.getFieldName1() %></td>
                                            <% /*Dohatec start*/
                                            if(isIctTender && !("APPROVED".equalsIgnoreCase(tenderInfoStatus1))){
                                                %><td> Tender Not Approved</td><%
                                                }
                                            else{
                                                /*Dohatec end*/
                                            if(isPE){%>
                                            <td><%if(isIctTender){%>
                                                <%if(!isPerfAvail){%>
                                                    <a href="AddIctPerfSecAmount.jsp?tenderId=<%=tenderId %>&pkgId=<%=pckLotId%>&rId=<%=roundId%>">Add</a>
                                                <%}else{if(!isPerfView){%>
                                                    <a href="AddIctPerfSecAmount.jsp?tenderId=<%=tenderId %>&pkgId=<%=pckLotId%>&isedit=y&rId=<%=roundId%>">Edit</a>&nbsp;|&nbsp;
                                                    <a href="AddIctPerfSecAmount.jsp?tenderId=<%=tenderId%>&pkgId=<%=pckLotId%>&viewAction=view&isedit=y&rId=<%=roundId%>">View</a>
                                                <%}else{%>
                                                    <a href="AddIctPerfSecAmount.jsp?tenderId=<%=tenderId%>&pkgId=<%=pckLotId%>&viewAction=view&isedit=y&rId=<%=roundId%>">View</a>
                                                <%}}%>
                                                <%}else{ if(!isPerfAvail){%>
                                                <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderId %>&pkgId=<%=pckLotId%>&rId=<%=roundId%>">Add</a>
                                                <%}else{if(!isPerfView){%>
                                                <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderId%>&pkgId=<%=pckLotId%>&isedit=y&rId=<%=roundId%>">Edit</a>&nbsp;|&nbsp;
                                                <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderId%>&pkgId=<%=pckLotId%>&viewAction=view&isedit=y&rId=<%=roundId%>">View</a>
                                                <%}else{%>
                                                <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderId%>&pkgId=<%=pckLotId%>&viewAction=view&isedit=y&rId=<%=roundId%>">View</a>
                                                <%}}}%></td>
                                            <%}else{%>
                                            <td><a href="AddPerfSecAmount.jsp?tenderId=<%=tenderId%>&pkgId=<%=pckLotId%>&viewAction=view&isedit=y&rId=<%=roundId%>">View</a> </td>
                                            <%}
                                            }%>
                                        </tr>
                                        <% } %>
                                    </table>
                                </td>
                            </tr>

                            <%  }
                                %>
                                <%
                                if(!"services".equalsIgnoreCase(procnature)){
                                if(isCreator){
                                    int j = 0;
                                     List<SPCommonSearchDataMore> mores = service.getQualifiedUser(request.getParameter("tenderId"),pckLotId);
                                    if(!mores.isEmpty() && mores!=null){
                                        //out.print(mores.get(0).getFieldName1());
                                        List<SPCommonSearchDataMore> showmores = service.isConsolidateShowOrNot(mores.get(0).getFieldName1(),tenderId);
                                        if(tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                             forms = dDocSrBean.getAllBoQForms(Integer.parseInt(lotList.getFieldName5()),Integer.parseInt(tenderId),Integer.parseInt(mores.get(0).getFieldName1()));
                                             formsRO = dDocSrBean.getAllBoQFormsForNOA(Integer.parseInt(lotList.getFieldName5()),Integer.parseInt(tenderId),Integer.parseInt(mores.get(0).getFieldName1()));
                                             showButton = service.getWpDetailForButtonDisplay(lotList.getFieldName5(),formsRO.size());
                                        }
                                        //out.print(showmores.get(0).getFieldName1());
                                        boolean isShowDConsolidate = false;
                                        if(showmores.isEmpty()){
                                            isShowDConsolidate=true;
                                            }else{
                                             if(!"decline".equalsIgnoreCase(showmores.get(0).getFieldName1())){
                                                 isShowDConsolidate=true;
                                                 }
                                            }%>

                            <% 
                                                            if(isShowDConsolidate){
                                                                               
                                                                            if (!forms.isEmpty() && forms != null) {
                                %>

                                <tr><td colspan="2">

                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-left">Select</th>
                                                <th width="17%" class="t-align-left"><%=form%></th>
                                            </tr>
                                            <%


                                                 for (Object[] obj : forms) {


                                            %>

                                            <tr>
                                                <td class="t-align-center">
                                                    <input type="checkbox" value="check" id="boqcheck_<%=j%>" name="boqcheck_<%=j%>"/>
                                                    <input type="hidden" value="<%=obj[0].toString()%>"  id="tenderformid_<%=j%>" name="tenderformid_<%=j%>"/>
                                                    <input type="hidden" value="<%=obj[1].toString()%>"  id="tenderformname_<%=j%>" name="tenderformname_<%=j%>"/>
                                                </td>
                                                <td>
                                                    <%=obj[1]%>
                                                </td>
                                            </tr>
                                            <%j++;
                               }%>
                                            <input type="hidden" value="<%=j%>"  id="count_<%= lotList.getFieldName5()%>" name="count_<%= lotList.getFieldName5()%>"/>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <label class="formBtn_1">
                                                        <input type="button" name="Boqbutton" id="Boqbutton" value="Consolidate <%= form%>" onclick="checkFor(<%=lotList.getFieldName5()%>,<%=tenderId%>);" />
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td></tr>                             
                                <%}
                                        int count = 1;
                                        if (!wpid.isEmpty() && wpid != null) {%>
                                <tr><td colspan="2">
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="17%" class="t-align-center"><%=bdl.getString("CMS.consolidate")%></th>
                                                <th width="5%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>
                                            </tr>
                                            <%
                                            List<TblCmsWpDetail> wpDetails = null;
                                            for (Object obj : wpid) {
                                                wpDetails = service.getAllDetail(obj.toString());
                                                String toDisplay = service.getConsolidate(obj.toString());                 
                                            %>
                                            <tr>
                                                <td style="text-align: center;"> <%=count%></td>
                                                <td><%=toDisplay%></td>
                                                <td>
                                                    <%if (!isDateEdited && wpDetails.isEmpty() && !toDisplay.contains("Discount Form")) {%>
                                                    <a href="#" onclick="deleteRow(<%=obj.toString()%>,<%=tenderId%>, <%=lotList.getFieldName5()%>);">Delete</a> <%%>
                                                    <%} else if(!toDisplay.contains("Discount Form")) {%>
                                                    <a href="#" onclick="view(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">View</a>
                                                    <%}
                                                    if (procnature.equalsIgnoreCase("goods")) {
                                                        if (!wpDetails.isEmpty() && !isDateEdited && !toDisplay.contains("Discount Form") ) {%>
                                                    &nbsp;|&nbsp;<a href="#" onclick="Edit(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Edit Days</a>
                                                    <%  }
                                                    }%>
                                                </td>
                                            </tr>
                                            <%
                                                               count++;
                                             }
                                            if (!wpDetails.isEmpty() && !isDateEdited) {
                                                if (!listTenderer.isEmpty() && listTenderer.size() != 0) {
                                            %>
                                            <tr><td colspan="3" class="t-align-center">
                                                    <% if (procnature.equalsIgnoreCase("works")) {
                                                        isNoaIssued = false;
                                                    %>
                                                    <label class="formBtn_1">
                                                        <input type="button" name="Boqbutton" id="Boqbutton" value="Go to Issue LOA" onclick="finishdates(<%=lotList.getFieldName5()%>,<%=tenderId%>);" />
                                                    </label>
                                                    <%}else{%>
                                                    <label class="formBtn_1">
                                                        <input type="button" name="Boqbutton" id="Boqbutton" value="Finish Edit Days" onclick="finishdates(<%=lotList.getFieldName5()%>,<%=tenderId%>);" />
                                                    </label>
                                                    <%}%>
                                                </td>
                                            </tr><%}}%>
                                        </table>
                                    </td></tr>
                                    <%}
                                    if (showButton) {%>
                                                <tr><td colspan="3" class="t-align-center"><label class="formBtn_1">
                                            <input type="button" name="finishconsolidation" id="finishconsolidation" value="Finish Consolidation" onclick="finish(<%=lotList.getFieldName5()%>,<%=tenderId%>);" />
                                        </label>
                                    </td></tr>
                                                
                                    <%}else if (forms != null && !forms.isEmpty()){ %>
                                    <tr>
                                        <td colspan="2" class='successMsg' style="background: #e9f9e5;padding-left: 65px;">Please Select and Consolidate One By One</td>
                                    </tr>             
                                    <%    }
                                                 }else{%>

                                                <div align="left" class="responseMsg noticeMsg">
                                                    Evaluation Report acceptance is pending
                                                </div>
                                                <%} }else{%>
                                                <div align="left" class="responseMsg noticeMsg t_align_left">
                                                    Evaluation Report acceptance is pending
                                                </div>
                                                <%}} }else{
CMSService cmss = (CMSService)AppContext.getSpringBean("CMSService");
List<Object[]> serviceForms = cmss.getAllFormsForService(Integer.parseInt(tenderId));
%>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
    <% int serivceSrNO=1;
    List<Object> data=null;
    for(Object[] obj : serviceForms){
        if(!(obj[2].toString().equals("20"))){
            data = cmss.dataSucssDump(Integer.parseInt(tenderId), (Integer) obj[1]);
            if(data!=null && !data.isEmpty()){
                isDateEdited=true;
            }
        }
    }
    %>
    <% if(data==null || data.isEmpty() && isPE){
    if (!listTenderer.isEmpty() && listTenderer.size() != 0) {
         List<SPCommonSearchDataMore> more = cmss.getQualifiedUser(tenderId, pckLotId);
        if(more!=null && !more.isEmpty()){
        isNoaIssued = false;
    %>
    <tr><td colspan="3" align="center">
            <center>
            <label class="formBtn_1">
                                            <input type="button" value="Go to Issue LOA" onclick="finishForService(<%=lotList.getFieldName5()%>,<%=tenderId%>);" />
                                        </label>
            </center>
        </td></tr><%}}}%>
</table>

                                                <%}%>
                            </table>

                                <%

                                                                            boolean allowCancel = false;
                                                                            TenderCommonService commonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                                            for (SPTenderCommonData commonData : commonService1.returndata("CheckPE", tenderid, null)) {
                                                                                if (commonData.getFieldName1().equals(session.getAttribute("userId").toString())) {
                                                                                    allowCancel = true;
                                                                                }
                                                                            }
                                                                            if (isDateEdited) {
                                                                                if (allowCancel) {
                                %>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <td width="12%">Action</td>
                                    <td>
                                        <%
                                                                                checkIssued = commonSearchDataMoreService.geteGPData("CheckIssuedNOA", tenderId, lotList.getFieldName5(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                                                if (!(checkIssued.isEmpty())) {
                                                                                    if (checkIssued.get(0).getFieldName1().equals("yes") || checkIssued.get(0).getFieldName2().equals("yes")) {
                                                                                    out.print("Issued");
                                                                                } else {
                                                                                    if (issueCon.isEmpty()) {%>
                                        <a onclick="msgNoaBusCon();" href="javascript:void(0);">View Forms and Issue LOA</a><br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue LOA' link to Issue LOA</div>
                                        <%} else {
                                                                                   
                                                                                    List<Object[]> obj = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                                                                                    if (!obj.isEmpty() && obj.get(0)[1].toString().equalsIgnoreCase("yes") && !listTenderer.isEmpty()) {
                                                                                        perSecAmt = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "perSecAmt", tenderId, lotList.getFieldName5(), listTenderer.get(0).getFieldName3(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                                                        if (perSecAmt.isEmpty()) {
                                                                                            isPerSec = true;
                                                                                        }
                                                                                    }
                                                                                    if (isPerSec) {
                                        %>
                                        <a onclick="msgPerSecAmt();" href="javascript:void(0);">View Forms and Issue LOA</a><br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue LOA' link to Issue LOA</div>
                                        <%} else {
                                                                                        List<SPCommonSearchDataMore> listStatusAA = commonSearchDataMoreService.geteGPData("GetEvalRptApprovalNOA", tenderId, lotList.getFieldName5(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                                                        /*if(!listStatusAA.isEmpty()){
                                                                                        if(listStatusAA.get(0).getFieldName1().equalsIgnoreCase("approved")){*/

                                                                                        if (!listStatusAA.isEmpty()) {
                                                                                            if (listStatusAA.get(0).getFieldName1().equalsIgnoreCase("approved")) {
                                                                                                if (!tenderValSec.isEmpty()) {
                                                                                                    if (tenderValSec.get(0).getFieldName1() != null && tenderValSec.get(0).getFieldName2() != null) {
                                                                                                        int tenderValdays = Integer.parseInt(tenderValSec.get(0).getFieldName1());
                                                                                                        int tenderSecdays = Integer.parseInt(tenderValSec.get(0).getFieldName2());
                                                                                                        if (tenderValdays >= 0 && tenderSecdays > 7) {

                                                                                                            if (!listTenderer.isEmpty() && listTenderer.size() != 0) {
                                        %>
                                        <a href="<%=request.getContextPath()%>/officer/IssueNOA.jsp?tenderId=<%=tenderId%>&pckLotId=<%=lotList.getFieldName5()%>">View Forms and Issue LOA</a><br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue LOA' link to Issue LOA</div>
                                        <% } else {%>
                                        <a onclick="msgListTenderer();" href="javascript:void(0);">View Forms and Issue LOA</a><br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue LOA' link to Issue LOA</div>
                                        <% }
                                                                                                                            } else if ((!objBusinessRule.isEmpty() && objBusinessRule.get(0)[3].toString().equalsIgnoreCase("yes") && tenderValdays < 0)) {%>
                                        <a onclick="msgTenderValDays();" href="javascript:void(0);">View Forms and Issue LOA</a><br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue LOA' link to Issue LOA</div>
                                        <% } else if ((!objBusinessRule.isEmpty() && objBusinessRule.get(0)[2].toString().equalsIgnoreCase("yes") && tenderSecdays <= 7)) {%>
                                        <a onclick="msgSecDays();" href="javascript:void(0);">View Forms and Issue LOA</a><br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue LOA' link to Issue LOA</div>
                                        <% }else{%>
                                                <a href="<%=request.getContextPath()%>/officer/IssueNOA.jsp?tenderId=<%=tenderId%>&pckLotId=<%=lotList.getFieldName5()%>">View Forms and Issue LOA</a><br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue LOA' link to Issue LOA</div>
                                        <%}
                                                                                                                            }else{%>
                                                                                                                                <a href="<%=request.getContextPath()%>/officer/IssueNOA.jsp?tenderId=<%=tenderId%>&pckLotId=<%=lotList.getFieldName5()%>">View Forms and Issue NOA</a><br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue NOA' link to Issue NOA</div>
                                                                                                                            <%}
                                                                                                                        }
                                                                                                                    }
                                                                                                                    if (listStatusAA.get(0).getFieldName1().equalsIgnoreCase("Pending")) {%>
                                        <a onclick="msgNOAPending();" href="javascript:void(0);">View Forms and Issue LOA</a><br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue LOA' link to Issue LOA</div>
                                        <% } else if (listStatusAA.get(0).getFieldName1().equalsIgnoreCase("Rejected/Re-Tendering")) {%>
                                        <a onclick="msgNOARR();" href="javascript:void(0);">View Forms and Issue LOA</a><br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue LOA' link to Issue LOA</div>
                                        <% } else if (listStatusAA.get(0).getFieldName1().equalsIgnoreCase("seek Clarification")) {%>
                                        <a onclick="msgNOAPending();" href="javascript:void(0);">View Forms and Issue LOA</a><br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue LOA' link to Issue LOA</div>
                                        <% }%>
                                        <%//}else{%>

                                        <% }
                                                                                                    }
                                                                                                }%>
                                        <% }
                                                                                }%>
                                    </td>
                                </tr>
                                <% }
                                                                        }
                                            
                                            %>
                            </table>
                        </form>
                        <%
                            int deb_count = 0;
                            List<SPCommonSearchData> debarList = commonSearchService.searchData("getFinalSubUser", tenderId, null, null, null, null, null, null, null, null);
                        %>
                        <table class="tableList_1" width="100%" id="debarTab">
                            <tr>
                                <th colspan="5">Debarred Tenderers / Consultants' Information</th>
                            </tr>
                            <tr>
                                <th>Bidder/Consultant</th>
                                <th>Debarred By</th>
                                <th>Debared From</th>
                                <th>Debared To</th>
                                <th>Debarred Reason</th>
                            </tr>
                            <%
                                for(SPCommonSearchData debarData : debarList){
                                    List<SPCommonSearchData> debarSubList = commonSearchService.searchData("GetDebarmentUserStatus", tenderId, debarData.getFieldName1(), "0", null, null, null, null, null, null);
                            %>
                            <tr>
                                <td><%=debarData.getFieldName2()%></td>
                                <%if (!debarSubList.isEmpty() && debarSubList.get(0).getFieldName1().equalsIgnoreCase("yes")) {%>
                                <td><%=debarSubList.get(0).getFieldName7()%></td>
                                <td class="t-align-center"><%=debarSubList.get(0).getFieldName4()%></td>
                                <td class="t-align-center"><%=debarSubList.get(0).getFieldName5()%></td>
                                <td><%=debarSubList.get(0).getFieldName6()%></td>
                                <%deb_count++;}else{%>
                                <td class="t-align-center">-</td>
                                <td class="t-align-center">-</td>
                                <td class="t-align-center">-</td>
                                <td class="t-align-center">-</td>
                                <%}%>
                            </tr>
                            <%}%>
                        </table>
                        <%if(deb_count==0){%>
                        <script type="text/javascript">
                            $('#debarTab').hide();
                        </script>
                        <%}%>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space t_space">
                            <tr>
                                <th width="5%" class="t-align-left">Rank </th>
                                <th width="17%" class="t-align-left">Name of <%=strProcNature%> </th>                                
                                <th width="12%" class="t-align-left">Contract No.</th>
                                <%if(!isIctTender){%>
                                    <th width="15%" class="t-align-left">Contract Amount in Figure (in Nu.)</th>
                                    <th width="15%" class="t-align-left">Advance Contract Amount in Figure <br/> (in Nu.)</th>
                                <%}else{%>
                                    <th width="30%" class="t-align-left">Contract Details</th>
                                <%}%>
                                <th width="15%" class="t-align-left">Date and Time of Issue</th>
                                <th width="15%" class="t-align-left">Deadline of Acceptance of <%= NOADA %></th>
                                <th width="10%" class="t-align-left"><%= NOADA %> Acceptance Status</th>
                                <th width="11%" class="t-align-left"><%=(procnature.equals("Services") || procurementNature.equals("DPM"))? "Accept" : "Accept / Decline"%> Date and Time</th>
                                <%
                                                                            if (!objBusinessRule.isEmpty() && objBusinessRule.get(0)[1].toString().equalsIgnoreCase("yes")) {
                                %>
                                <th width="10%" class="t-align-left">Performance Security</th>
                                <% }%>
                                <th width="5%" class="t-align-center">Action</th>
                            </tr>
                            <%              
                                            
                            for (Object[] obj : issueNOASrBean.getDetails(Integer.parseInt(tenderId), Integer.parseInt(lotList.getFieldName5()))) {
                                List<TblTenderPayment> detailsPayment = issueNOASrBean.getPaymentDetails(Integer.parseInt(tenderId), (Integer) obj[0], (Integer) obj[8]);
                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
                                if("0".equalsIgnoreCase(bidderId)){
                                    bidderId = obj[8]+"";
                               }
                                //pkgLotId = obj[0]+"";
                                out.print("<tr>");
                                //out.print("<td cassl=\"t-align-center\">"+obj[6]+"</td>");
                                //out.print("<td>"+obj[7]+"</td>");
                                //for(SPTenderCommonData companyList : tenderCommonService.returndata("GetEvaluatedBiddersNOA",tenderId,lotList.getFieldName5())){
                                //  if(obj[8].toString().equalsIgnoreCase(companyList.getFieldName2())){
                                out.print("<td class=\"t-align-center\">" + obj[10] + "</td>");
                                out.print("<td>" + obj[11] + "</td>");
                                //}
                                //}
                                out.print("<td>" + obj[2] + "</td>");


                                if(!isIctTender)
                                {
                                    out.print("<td>" + obj[13] + "</td>");
                                    if(obj[12]!=null){
                                        out.print("<td>" + new BigDecimal((new BigDecimal(obj[13].toString()).doubleValue()*new BigDecimal(obj[12].toString()).doubleValue())/100).setScale(3,0) + "</td>");
                                    }else{
                                        out.print("<td>-</td>");
                                    }
                                }
                                else
                                {%>

                                    <td class="t-align-center"><a onclick="javascript:window.open('ViewContractAmountNOA.jsp?tenderid=<%=tenderId %>&roundId=<%=roundId%>&pckLotId=<%=pckLotId%>&bidderId=<%=bidderId%>', '', 'resizable=yes,scrollbars=1','');" href="javascript:void(0);">View</a> </td>

                                <%}


                                out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStrWithoutSec((Date) obj[3]) + "</td><td class=\"t-align-center\">");
                                //out.print("<td>"+obj[5]+"</td>");
                                if (obj[9] != null) {
                                    out.print(dateFormat1.format(obj[9]));
                                }
                                out.print("</td>");
                                if (obj[4].equals("pending")) {
                                    out.print("<td class=\"t-align-center\">Pending</td>");
                                } else if (obj[4].equals("approved"))   {
                                    out.print("<td class=\"t-align-center\">Accepted</td>");
                                } else {
                                    if(obj[14]!=null && obj[14].equals("By System"))
                                        {
                                            if(obj[4].equals("Performance Security not paid"))
                                            {
                                                out.print("<td class=\"t-align-center\">Accepted</td>");
                                            }else{
                                                out.print("<td class=\"t-align-center\">Declined By System</td>");
                                            }
                                        }
                                    else
                                        {
                                            out.print("<td class=\"t-align-center\">Declined</td>");
                                        }
                                    }
                                if (obj[5] != null) {
                                    out.print("<td class=\"t-align-center\">" + DateUtils.gridDateToStr((Date) obj[5]) + "</td>");
                                } else {
                                    out.print("<td class=\"t-align-center\">-</td>");
                                }
                                if (!objBusinessRule.isEmpty() && objBusinessRule.get(0)[1].toString().equalsIgnoreCase("yes")) {
                                    out.print("<td class=\"t-align-center\">");
                                    if (detailsPayment.isEmpty()) {
                                        if(obj[4].equals("Performance Security not paid") && obj[14]!=null && obj[14].equals("By System"))
                                        {
                                            out.print("Performance Security not paid in time");
                                        } else{
                                            out.print("Pending");
                                        }
                                    } else {
                                        if (detailsPayment.get(0).getIsVerified().equalsIgnoreCase("yes")) {
                                            out.print("Received");
                                        } else {
                                            out.print("Pending");
                                        }
                                    }
                                }
                                out.print("</td>");
                                //out.print("<td><a href=\"NOADocUpload.jsp?noaIssueId="+obj[1]+"\">upload</a></td><td>");
                                // - remove - out.print("<td class=\"t-align-center\"><a onclick=\"javascript:window.open('NOADocUpload.jsp?noaIssueId="+obj[1]+"', '', 'resizable=yes,scrollbars=1','');\" href=\"javascript:void(0);\">Upload NOA Ref Doc if any</a></td><td>");
                                out.print("<td class=\"t-align-center\">");
                                //if(!"Approved".equalsIgnoreCase(obj[4].toString())){out.print("<a href=\"MangeIssueNOA.jsp?noaIssueId="+obj[1]+"&lotNo="+obj[6]+"&tenderId="+tenderId+"\">Edit | </a>");}
                                //out.print("<a onclick=\"javascript:window.open('ViewNOAWG.jsp?NOAID="+obj[1]+"&tenderid="+tenderId+"', '', 'resizable=yes,scrollbars=1','');\" href=\"javascript:void(0);\">View</a>");
                                out.print("<a href=\"ViewNOAWG.jsp?NOAID=" + obj[1] + "&tenderid=" + tenderId + "\">View</a>");
                                out.print("</td></tr>");
                            %>

                            <%}%>

                        </table>
                        <% i++;
                                }
                                        if(!isNoaIssued){
                                            bidderId = "";
                                        }
                %>
                        <div><input type="hidden" value="<%=dateTender%>" id="dateTender" name="dateTender"/></div>
                        <jsp:include page="../resources/common/NOADocInclude.jsp" >
                            <jsp:param name="comtenderId" value="<%=tenderId%>" />
                            <jsp:param name="comlotId" value="<%=pckLotId%>" />
                            <jsp:param name="comuserId" value="<%=bidderId%>" />
                        </jsp:include>
                        
                    </div>
                </div>
            </div>
            <!--Dashboard Content Part End-->
            <div>&nbsp;</div>
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
        <script type="text/javascript">
            function msgTendervalidity(){
                var dateTender;
                if(document.getElementById('dateTender') != null){
                    if(document.getElementById('dateTender').value != ""){
                        jAlert("Tender/proposal validity is about to expire on "+ dateTender,"Failure");
                    }
                }
            }
            function msgTValidity(){
                var dateTender;
                if(document.getElementById('dateTender') != null){
                    if(document.getElementById('dateTender').value != ""){
                        jAlert("Tender/proposal validity date "+ dateTender + " has been expired. Please extend tender/proposal validity date first");
                    }
                }
            }

            function msgTenderValDays(){
                jAlert("Please Extend Tender/Proposal Validity First","Failure");
            }
            function msgSecDays(){
                jAlert("Please Extend Tender/Proposal Security First","Failure");
            }
            function msgListTenderer(){
                jAlert("Evaluation report yet not approved","Failure");
            }
            function msgNoaBusCon(){
                jAlert("Business rule not configured yet. Please configure business rule to proceed further.","'<%=NOADA%>' configuration");
            }
            function msgPerSecAmt(){
                jAlert("Performance Security Amount is not entered yet.","Performance Security");
            }
            /*function msgNOA(){
                jAlert("L1 Report yet not approved by approving authority","Approved", function(RetVal) {});
            }*/
            function msgNOAPending(){
                jAlert("Evaluation Report yet not finalized by Approving Authority","Approved", function(RetVal) {});
            }
            function msgNOARR(){
                jAlert("Approving Authority has Rejected Evaluation Report for this Lot","Approved", function(RetVal) {});
            }
        </script>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
<%
            issueNOASrBean = null;
%>