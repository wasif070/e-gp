<%--
    Document   : SendReportAA
    Created on : Jun 18, 2011, 11:55:12 AM
    Author     : nishit
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.EvaluationMatrix"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalReportDocs"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Send Report to Approving Authority / HOPA / AO</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>--%>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <%
        String s_tenderid = "0";
        if (request.getParameter("tenderid") != null) {
                s_tenderid = request.getParameter("tenderid");
            }

                String s_lotId = "0";
                        String s_rpt = "0";
                        String s_rId = "0";
                        /*Dohatec Start*/
                        Boolean hopeOnly = false;
                         if (request.getParameter("hopeOnly") != null) {
                            hopeOnly = true;
                        }
                        /*Dohatec End*/
                        if (request.getParameter("lotId") != null) {
                            s_lotId = request.getParameter("lotId");
                        }
                        if (request.getParameter("rpt") != null) {
                            s_rpt = request.getParameter("rpt");
                        }
                        if (request.getParameter("rId") != null) {
                            s_rId = request.getParameter("rId");
                        }
                        CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        boolean isAAMinister1 = false;
                        List<SPCommonSearchDataMore> lstChkUserType1 = dataMore.geteGPData("getTenderAAMinister", s_tenderid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        if(lstChkUserType1 !=null && !lstChkUserType1.isEmpty() && Integer.parseInt(lstChkUserType1.get(0).getFieldName1()) > 0){
                            isAAMinister1 = true;
                        }

        %>
        <script type="text/javascript">
            function validate(){
                $(".err").remove();
                var valid = true;
                if($("#cmboSendto").val() == "") {
                    $("#cmboSendto").parent().append("<div class='err' style='color:red;'>Please select Send To</div>");
                    valid = false;
                }
                /*if($.trim($("#textComments").val()) == "") {
                    $("#textComments").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                    valid = false;
                }*/
                if($.trim($("#textComments").val()).length > 2000) {
                    $("#textComments").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed</div>");
                    valid = false;
                }
                if(CKEDITOR.instances.textComments.getData() == 0){
                    $("#textComments").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                    valid=false;
                }
                 if(CKEDITOR.instances.textComments.getData().length > 2000)
                {
                    $("#textComments").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed</div>");
                    valid = false;
                }

                //alert($("#cmbName").val()+'value of combo');
                if($("#cmbName")==undefined || $("#cmbName").val() == "") {
                    $("#cmbName").parent().append("<div class='err' style='color:red;'>Please Select Officer</div>");
                    valid = false;
                }
                /*if($("#cmbName").val() == "0"){
                            $("#cmbName").parent().append("<div class='err' style='color:red;'>Please select diffrent Role</div>");
                        }*/
                if(!valid){
                    return false;
                } else {
                    document.getElementById("submit").style.display = 'none';
                }
            }
        </script>
        <script type="text/javascript">
            function setOfficer(){
                $.post("<%=request.getContextPath()%>/EvaluationServlet", {type:'',tenderid:$('#tenderid').val(),designation:$('#cmboSendto').val(),action:'empCombo'}, function(j){
                    $("#nameTd").html(j);
                    $("#nameTr").show();
                    if(document.getElementById("divSec")){
                       document.getElementById("divSec").innerHTML="";
                    }
                });
            }
            function getDocData(){
            $.post("<%=request.getContextPath()%>/EvalRptDocsServlet", {tenderid:<%=s_tenderid%>,lotId:<%=s_lotId%>,rId:<%=s_rId%>,funName:'docTable'}, function(j){
                    document.getElementById("queryData").innerHTML = j;
                });
            }
            function deleteFile(docId,docName,tenderId,lotId,rId){
               // alert(docId+docName+tenderId+lotId+rId);
                $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
                    if(r == true){
                        $.post("<%=request.getContextPath()%>/EvalRptDocsServlet", {docName:docName,docId:docId,tenderid:tenderId,lotId:lotId,rId:rId,funName:'docDelete'}, function(j){
                            jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                                getDocData();
                            });
                        });
                    }else{
                        getDocData();
                    }
                });
            }
        </script>
    </head>

    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="DashboardContainer">
                <div class="contentArea_1">
                    <%if(hopeOnly){%>
                    <div class="pageHead_1">Send Report to HOPA
                        <span class="c-alignment-right"><a href="Evalclarify.jsp?tenderId=<%=s_tenderid%>&st=rp&comType=TEC" title="Go Back" class="action-button-goback">Go Back</a></span>
                    </div>
                    <%}else{%>
                    <div class="pageHead_1">Send Report to Approving Authority / HOPA / AO
                        <span class="c-alignment-right"><a href="Evalclarify.jsp?tenderId=<%=s_tenderid%>&st=rp&comType=TEC" title="Go Back" class="action-button-goback">Go Back</a></span>
                    </div>
                    <%} %>

                    <%--<%@include file=""%>--%>
                    <%




                        pageContext.setAttribute("tenderId", s_tenderid);
                    %>
                    <%if (request.getParameter("msg") != null){
                            if("error".equalsIgnoreCase(request.getParameter("msg")) || "reerror".equalsIgnoreCase(request.getParameter("msg"))){
                %>
                    <div class="responseMsg errorMsg" style="margin-top: 10px;">There is problem while requesting your query. Please try again</div>
                    <%}if("re".equalsIgnoreCase(request.getParameter("msg"))){%>
                    <div class="responseMsg successMsg" style="margin-top: 10px;">File removed successfully.</div>
                    <%}}%>
                    <div class="t_space">
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    </div>
                        <%
                            //tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                            // CHECK docAvlMethod COLUMN WHETHER THERE IS "Lot" OR "Package"
                            String evalType = "eval";
                                if(request.getParameter("stat")!=null){
                                    evalType = request.getParameter("stat");
                                }
                            /*Dohatec Start*/
                            String strCmnProcurementNature = "";
                            String strCmnDocType = "";
                            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> lstCmnEnvelops = tenderCommonService1.returndata("getTenderEnvelopeCount", s_tenderid, "0");
                            if (!lstCmnEnvelops.isEmpty()) { strCmnProcurementNature = lstCmnEnvelops.get(0).getFieldName3();  }
                            if ("Services".equalsIgnoreCase(strCmnProcurementNature)) {strCmnDocType = "Proposal";}
                            else {strCmnDocType = "Tender";}
                            /*Dohatec End*/
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <td class="ff" width="12%"><%=strCmnDocType%> Opening Reports :</td>
                        <td width="88%"><a href="OpeningReports.jsp?tenderId=<%=s_tenderid%>&lotId=0&comType=null" target="_blank" >View</a></td>
                            <jsp:include page="TERInclude.jsp">
                                <jsp:param name="tenderId" value="<%=s_tenderid%>"/>
                                <jsp:param name="lotId" value="<%=s_lotId%>"/>
                                <jsp:param name="roundId" value="<%=s_rId%>"/>
                                <jsp:param name="evalType" value="<%=evalType%>"/>
                            </jsp:include>
                        </table>
                    <div style="font-style: italic" class="formStyle_1 t_space b_space ff">Fields marked with (<span class="mandatory">*</span>) are mandatory</div>
                    <form id="frmSendReportAA" action="<%=request.getContextPath()%>/EvaluationServlet" name="frmSendReportAA" method="POST">
                        <input type="hidden" name="action" value="addAARpt" />
                        <input type="hidden" name="rptMethod" value="<%=s_rpt%>" />
                        <input type="hidden" name="rId" value="<%=s_rId%>" />
                        <input type="hidden" name="eval_Type" value="<%=evalType%>" />
                        <table width="100%" cellspacing="10" class="tableView_1 t_space">
                            <tr>
                                <td width="15%" class="ff">Send To : <span class="mandatory">*</span>
                                    <input type="hidden" name="tenderid" id="tenderid" value="<%=s_tenderid%>" />
                                    <input type="hidden" name="lotId" id="lotId" value="<%=s_lotId%>" />
                                </td>
                                <td width="85%">
                                    <select class="formTxtBox_1" id="cmboSendto" name="sentRole" style="width:100px;" onchange="setOfficer();" >
                                        <option selected="selected" value="">- Select -</option>
                                        <%if(!hopeOnly){%>
                                        <option value="AA">AA</option>
                                        <%if(isAAMinister1){%>
                                            <option value="Secretary">Secretary</option>
                                        <%}%>
                                        <%}%>
                                        <option value="HOPE">HOPA</option>
                                        <%if(!hopeOnly){%>
                                        <option value="AO">AO</option>
                                        <%}%>
                                    </select></td>
                            </tr>
                            <tr id="nameTr" style="display: none">
                                <td class="ff">Officer's Name :</td>
                                <td id="nameTd">

                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="ff">Comments : <span class="mandatory">*</span></td>
                                <td>
                                    <textarea cols="100" rows="5" id="textComments" name="textComments" class="formTxtBox_1"></textarea>
                                    <script type="text/javascript">
                                                  CKEDITOR.replace( 'textComments',
                                                  {
                                                       toolbar : "egpToolbar"

                                                  });
                                     </script>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Reference Document :</td>
                                <td>
                                    <a href="javascript:void(0)" onclick="window.open('EvalRptDocUpload.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>&rId=<%=s_rId%>','window','resizable=yes,scrollbars=1');">Upload</a>
                                    <%--<a href="EvalRptDocUpload.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>&rId=<%=s_rId%>">Upload</a>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <label class="formBtn_1 l_space t-align-left">
                                        <input type="submit" id="submit" name="submit" value="Submit" onclick="return validate();"/>
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <div id="queryData"></div>
                    <%--<tr>
                        <th width="5%" class="t-align-center">S. No.</th>
                        <th class="t-align-center" width="25%">File Name</th>
                        <th class="t-align-center" width="55%">File Description</th>
                        <th class="t-align-center" width="5%">File Size <br />
                            (in Kb)</th>
                        <th class="t-align-center" width="10%">Action</th>
                    </tr>
                    <%

                            EvaluationService evalService = (EvaluationService)AppContext.getSpringBean("EvaluationService");
                            List<TblEvalReportDocs> list_disp = new ArrayList<TblEvalReportDocs>();
                            list_disp=evalService.getListOfDocs(Integer.parseInt(s_tenderid),Integer.parseInt(s_lotId),Integer.parseInt(s_rId));
                            int i=1;
                            if(list_disp!=null && !list_disp.isEmpty()){
                                for(TblEvalReportDocs tblEvalReportDocs : list_disp){


                    %>
                    <tr>
                        <td class="t-align-center"><%out.print(i++);%></td>
                        <td class="t-align-left"><%=tblEvalReportDocs.getDocumentName()%></td>
                        <td class="t-align-left"><%=tblEvalReportDocs.getDocDescription()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(tblEvalReportDocs.getDocSize())/1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/EvalRptDocsServlet?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>&docId=<%=tblEvalReportDocs.getEvalRptDocId()%>&docName=<%=tblEvalReportDocs.getDocumentName()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a> |
                            <a href="<%=request.getContextPath()%>/EvalRptDocsServlet?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>&docId=<%=tblEvalReportDocs.getEvalRptDocId()%>&docName=<%=tblEvalReportDocs.getDocumentName()%>&funName=Remove" title="Remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                        </td>
                    </tr>
                    <%}
                            }else{%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>--%>
                </table>
                </div>
            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
        <script type="text/javascript">
            var headSel_Obj = document.getElementById("headTabEval");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
            getDocData();
        </script>
    </body>
</html>
