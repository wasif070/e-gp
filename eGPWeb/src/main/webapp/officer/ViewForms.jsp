<%-- 
    Document   : ViewForms
    Created on : Nov 15, 2011, 6:45:45 PM
    Author     : shreyansh Jogi
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.ReportCreationSrBean"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    String isEdit = "";
                    if ("y".equalsIgnoreCase(request.getParameter("isedit"))) {
                        isEdit = request.getParameter("isedit");
                    }
                    ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View BOQ Forms</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>



        <%
                    int userid = 0;
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userid = Integer.parseInt(hs.getAttribute("userId").toString());
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                    CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                    String procType = cs.getProcurementType(request.getParameter("tenderId")).toString();

                     String lotId = request.getParameter("lotId");
                     String tenderId = request.getParameter("tenderId");
                     ReportCreationSrBean rcsb = new ReportCreationSrBean();
                     // java.util.List<Object[]> list11 = rcsb.findFormsForReport(tenderId,lotId);
                    /*Dohatec Start*/
                     java.util.List<Object[]> list11 ;
                     if("ICT".equalsIgnoreCase(procType) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                        {
                            list11 = rcsb.findFormsForReportForTenderForms(tenderId,lotId);
                        }
                     else
                        {
                            list11 = rcsb.findFormsForReport(tenderId,lotId);
                        }
                    /*Dohatec End*/
                    
                    List<Object[]> list = cs.getLotDetailsByPkgLotId(lotId, request.getParameter("tenderId"));
        %>
    </head>
    <body>
        <% if (request.getParameter("msg") != null) {
        %>
        <% if (request.getParameter("msg").equals("edit")) {%>

        <script>
            jAlert("<%=bdl.getString("CMS.dates.edit")%> ","Progress Report", function(RetVal) {
            });

        </script>
        <% }

                    }%>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">View BOQ Forms
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="DeliverySchedule.jsp?tenderId=<%=tenderId%>" title="Go Back">Go Back</a>
                        </span>
                     <!--   <span class="c-alignment-right"><a href="DeliverySchedule.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span>-->
                     
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    
                    <%
                                pageContext.setAttribute("TSCtab", "2");
                                if (request.getParameter("lotId") != null) {
                                    pageContext.setAttribute("lotId", request.getParameter("lotId"));
                                    lotId = request.getParameter("lotId");
                                }

                    %>                    
                    <div>&nbsp;</div>
                    <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                     <div>&nbsp;</div>
                        <div class="tabPanelArea_1">
                                <input type="hidden" name="lotId" value="<%=request.getParameter("lotId")%>" />
                                <%
                                            boolean flag = true;
                                            CommonService commonService_pr = (CommonService) AppContext.getSpringBean("CommonService");
                                            List<Object[]> list1 = commonService_pr.getLotDetailsByPkgLotId(request.getParameter("lotId"), tenderId);
                                            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                            int i = 0;

                                            for (Object[] lotList : list1) {
                                %>
                               
                                       <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                    <tr>
                                        <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                                        <td width="80%"><%=lotList[0].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td><%=bdl.getString("CMS.lotdes")%></td>
                                        <td class="t-align-left"><%=lotList[1].toString()%></td>
                                    </tr>
                                </table>

                                <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>" />
                                <input type="hidden" name="wpId" id="wpId" value="<%=request.getParameter("wpId")%>" />
                                <input type="hidden" name="action" value="preparepr" />
                                <div id="resultDiv" style="display: block;">
                                    <div  id="print_area">
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                                
                                                <th width="3%" class="t-align-center">Sr.No</th>
                                                <th width="20%" class="t-align-center">BOQ forms</th>
                                                <th width="15%" class="t-align-center">Action</th>
                                                <%
                                                if(!list11.isEmpty()){
                                                    int s=1;
                                            for(Object[] obj : list11 ){%>
                                            <tr>
                                            <td><%=s%></td>
                                            <td><%=obj[1] %></td>
                                            <td><a href="BOQFormsView.jsp?tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=lotId%>&formId=<%=obj[0]%>">View</a></td>
                                            </tr>
                                            <%
                                            s++;
                                            } }%>
                                                
                                            
                                        </table>
                                        <% }%>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <!--Dashboard Content Part End-->
                </div>
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
        </div>

    </body>

    <script>
        var headSel_Obj = document.getElementById("headTabReport");
        loadTable();
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

        function msgOfQualityChk(obj){
            var finalizeObj = document.getElementById("lbl_finalizeBtn");
            if(obj.checked){
                jConfirm("Are you sure Quality Check has been done?","Quality Checked", function(RetVal){
                    if(RetVal)
                    {
                        finalizeObj.style.visibility = "visible";
                    }else{document.prfrm.chkids.checked= false;}
                });
            }else{
                finalizeObj.style.visibility = "hidden";
            }
        }
    </script>
</html>

