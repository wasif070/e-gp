<%-- 
    Document   : LotDetails
    Created on : Dec 6, 2010, 7:32:40 PM
    Author     : Taher
--%>

<%@page import="com.cptu.egp.eps.model.table.TblTemplateGuildeLines"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
     <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
     <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />       
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lot Details</title>
    </head>
    <body>
       <%@include  file="../resources/common/AfterLoginTop.jsp"%>

       <div class="contentArea_1">
        <div class="pageHead_1">Tender Document View </div>
        <%       
                TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                   List<TblTenderLotSecurity> lots= null;
                   int tendid = 0;
                if(request.getParameter("tenderid") != null){
                    tendid = Integer.parseInt(request.getParameter("tenderid"));
                     lots = tenderDocumentSrBean.getLotDetails(tendid);
                }
               pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
        %>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <div class="t_space"></div>
          <div class="tabPanelArea_1 ">
              <%
                        
                        String stdName = "";
                        stdName=tenderDocumentSrBean.checkForDump(String.valueOf(tendid));
                        String stdTemplateId = "0";
                        stdTemplateId = tenderDocumentSrBean.findStdTemplateId(String.valueOf(tendid));
                        out.print("<table width='100%' cellspacing='0' class='tableList_1'><tr><td class='t-align-left ff' width='22%'>Standard Bidding Document :</td><td>"+stdName+"</td></tr></table>");

              %>
              <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                  <tr>
                      <td width='15%' class='t-align-left ff'>Guidance Notes : </td>
                      <td width='85%'>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <th>Sl. No.</th>
                                        <th>File Name</th>
                                        <th>File Description</th>
                                        <th>File Size In KB</th>
                                        <th>Action</th>
                                    </tr>

            <%
                                                java.util.List<com.cptu.egp.eps.model.table.TblTemplateGuildeLines> docs = defineSTDInDtlSrBean.getGuideLinesDocs(Integer.parseInt(stdTemplateId));
                                                if (docs != null) {
                                                    int size = 0;
                                                    if (docs.size() > 0) {
                                                        short j = 0;
                                                        for (TblTemplateGuildeLines ttsd : docs) {
                                                            j++;
                                                            size = Integer.parseInt(ttsd.getDocSize()) / 1024;
            %>
                                    <tr>
                                        <td style="text-align:center;"><%= j%></td>
                                        <td style="text-align:left;"><%= ttsd.getDocName()%></td>
                                        <td style="text-align:left;"><%= ttsd.getDescription()%></td>
                                        <td style="text-align:center;"><%=size%></td>
                                        <td style="text-align:center;">
                                            <a href="../GuideLinesDocUploadServlet?docName=<%= ttsd.getDocName()%>&templateId=<%= stdTemplateId%>&docSize=<%= ttsd.getDocSize()%>&action=downloadDoc" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                        </td>
                                    </tr>

                                    <%
                                                                                                    ttsd = null;
                                                                                                }
                                                                                            } else {
                                    %>
                                    <tr>
                                        <td colspan="5">
                                            No Records Found
                                        </td>
                                    </tr>
                                    <%                                                                                                        }
                                                                                            docs = null;
                                                                                        } else {
                                    %>
                                    <tr>
                                        <td colspan="5">
                                            No Records Found
                                        </td>
                                    </tr>
                                    <%                                                            }
                                    %>
                                </table>


                            </td>
                        </tr>
                    </table>
              <table width="100%" cellspacing="0" class="tableList_1">
            <tr>
                    <th width="10%" class="t-align-left">Lot No.</th>
                    <th width="80%" class="t-align-left">Lot Description</th>
                    <th width="10%" class="t-align-left">Action</th>
                </tr>
                <% if(lots.size()>0 && lots != null){
                     Iterator it = lots.iterator();
                     int i = 1;
                           while(it.hasNext()){

                               TblTenderLotSecurity tblTenderLotSecurity = (TblTenderLotSecurity)it.next();
                               %>
                               <tr>
                                   <td><%= i%></td>
                                   <td><%=tblTenderLotSecurity.getLotDesc() %></td>
                                   <td class="t-align-center"><a href="TenderDocPrep.jsp?tenderId=<%=tendid%>&porlId=<%=tblTenderLotSecurity.getAppPkgLotId() %>">View</a></td>
                               </tr>
                        <%
                        i++;
                               }
                         } %>
        </table>        
          </div>
        </div>
         <div>&nbsp;</div>

             <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
           
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
