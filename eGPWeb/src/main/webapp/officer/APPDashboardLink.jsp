<%-- 
    Document   : APPDashboardLink
    Created on : Dec 21, 2010, 2:58:10 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
       
    </head>
    <body>
        <%
                    int appId = 0;
                    if (!request.getParameter("appId").equals("")) {
                        appId = Integer.parseInt(request.getParameter("appId"));
                    }

                    int pckId = 0;
                    if (!request.getParameter("pckId").equals("")) {
                        pckId = Integer.parseInt(request.getParameter("pckId"));
                    }
                    String userid = "";
                    if (session.getAttribute("userId") != null) {
                        userid = session.getAttribute("userId").toString();
                    }
                    tenderSrBean.setLogUserId(userid);
                    
                    List<CommonSPReturn> details = tenderSrBean.insertSPAddYpTEndernotice(appId, pckId, userid);
                    java.math.BigDecimal id = null;
                    id = details.get(0).getId();
                    response.sendRedirect("CreateTender.jsp?id=" + id);
        %>
    </body>
    <%
                tenderSrBean = null;
    %>

     <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabApp");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
