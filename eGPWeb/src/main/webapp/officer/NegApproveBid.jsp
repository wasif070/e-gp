<%-- 
    Document   : NegApproveBid
    Created on : Jan 13, 2011, 11:45:43 AM
    Author     : Administrator
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.NegotiationSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<jsp:useBean id="negotiationFormsSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationFormsSrBean"></jsp:useBean>
<jsp:useBean id="negotiationProcessSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Accept / Reject / Resend Forms Revision</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript">
        function Validate(){
            var flag = true;
            $('.reqF_1').remove();
            var count=0;
            var rdcount=0;
             $(":radio[id='idStatus']").each(function(){
                    rdcount++;              
                });
                    $(":radio[checked='true']").each(function(){
                    count++;              
                });
                if(rdcount!=0 && count==0){
                    $('#idStatusLabel').parent().append("<div class='reqF_1'>Please select one option</div>")
                    flag =  false;
                }
            if($.trim($('#idRemarks').val()).length!=0){
                if($.trim($('#idRemarks').val()).length>100){
                    $('#idRemarks').parent().append("<div class='reqF_1'>Maximum 1000 Characters Allowed</div>")
                }
            }else{
                $('#idRemarks').parent().append("<div class='reqF_1'>Please Enter Remark</div>")
                flag = false;
            }
            return flag;
            
        }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <%  String tenderId;
                tenderId = request.getParameter("tenderId");

                String userId = "", usrId = "";
                
                String type="";
                String bidid="";

                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                if (request.getParameter("uId") != null && !"".equalsIgnoreCase(request.getParameter("uId"))) {
                    usrId = request.getParameter("uId");
                }

                int negId = 0;
                if(request.getParameter("negId")!=null){
                    negId = Integer.parseInt(request.getParameter("negId"));
                }

                int isBidModify = 0;
                
                String finalStatus = "";
                
                // get final submission status of negId
                finalStatus = negotiationProcessSrBean.getFinalSubStatus(negId);

                 int cnt=0;
                            
            %>
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <!-- include After Login Top Panel -->
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">
                    Accept / Reject / Resend Forms Revision
                    <span style="float: right; text-align: right;">
                        <%
                        String pageName = "NegotiationProcess";
                        %>
                        <a class="action-button-goback" href="<%=pageName%>.jsp?tenderId=<%=tenderId%>&uId=<%=usrId%>&negId=<%=negId%>" title="Bid Dashboard">Go Back To Dashboard</a>
                        <%
                        pageName = null;
                        %>
                    </span>
                </div>
                <%
                    pageContext.setAttribute("tenderId", tenderId);
                %>
                <!-- include Tender Information Bar -->
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%
                    // check if tender is package wise or lot wise.
                    boolean isTenPackageWis1 = (Boolean) pageContext.getAttribute("isTenPackageWise");
                    String tenderRefNo = (String)pageContext.getAttribute("tenderRefNo");
                %>
                <div>&nbsp;</div>
                
                <%if(!"TEC".equalsIgnoreCase(request.getParameter("comType"))) { %>
                <jsp:include page="officerTabPanel.jsp" >
                     <jsp:param name="tab" value="7" />
                </jsp:include>
                <% } %>
                
                 <div class="tabPanelArea_1">
                         <%-- Start: Common Evaluation Table --%>
                      <%@include file="/officer/EvalCommCommon.jsp" %>
                    <%-- End: Common Evaluation Table --%>
                    <div>&nbsp;</div>
                     <%
                     pageContext.setAttribute("TSCtab", "7");
                     %>
                     <%@include  file="../resources/common/AfterLoginTSC.jsp"%>
                <div class="tabPanelArea_1">
               <%
                     if("yes".equalsIgnoreCase(finalStatus)){
                %>
                    <%
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            // if Packeage is Tender wise fetch Package or lot details by tender id
                            if (isTenPackageWis1) {
                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                    %>
                   <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td colspan="2" class="t-align-center ff">Tender/Proposal Details</td>
                        </tr>

                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="22%" class="t-align-left ff">Form Name</th>
                            <th width="78%" class="t-align-left">Action</th>
                        </tr>
                        <%
                            // Get Tender forms from tenderId
                            boolean flag = false;
                           
                            
                            for (SPCommonSearchData formdetails : commonSearchService.searchData("GetNegTenderFormsByLotId", tenderId, usrId, "0", usrId, null, ""+negId, null, null, null)) 
                            {
                               
                                flag = false;
                                flag = negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formdetails.getFieldName1()),negId);
                                 if(flag==true)
                                 {
                                %>
                                  <tr>
                                    <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                                     <td class="t-align-left">
                                <%

                                        cnt=0;

                                         int negBidformId=0;
                                         List<SPCommonSearchData> neglist=commonSearchService.searchData("GetNegBidderFormsData", tenderId, usrId,formdetails.getFieldName1() , null, null,null, null, null,null);
                                        for (SPCommonSearchData negoformdetails : neglist)
                                        {
                                             cnt++;
                                            //out.println(formdetails.getFieldName2()+"=====test  ");

                                              if(negoformdetails.getFieldName4() != null)
                                            {
                                                type="negbiddata";
                                                negBidformId=Integer.parseInt(negoformdetails.getFieldName4());
                                            }
                                            else
                                            {
                                                type="biddata";
                                                negBidformId=0;
                                            }
                                              bidid=negoformdetails.getFieldName3();

                                                %>
                                                    <a href="../resources/common/ViewNegBidCompare.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%= bidid%>&uId=<%=usrId%>&negId=<%=negId%>&negBidformId=<%=negBidformId%>&lotId=0&type=<%=type%>&action=View">View</a>
                                                <%
                                           
                                                if(neglist.size() > 1 && cnt != neglist.size())
                                                    {
                                                        %>
                                                         &nbsp;|&nbsp;
                                                        <%
                                                    }
                                              }
                                        }
                                    
                                  %>
                                </td> </tr>
                        <%
                                }
                            out.println("</table>");
                              }
                            }else {
                                 // Get Lot or Package Details by Tender Id.
                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-center ff">Tender/Proposal Details</td>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%  }
                                // Get Tender Lot by tender Id
                                for (SPCommonSearchData lotList : commonSearchService.searchData("gettenderlotbytenderid", tenderId, "", "", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <td width="22%" class="t-align-left ff">Lot No. :</td>
                                <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Lot Description :</td>
                                <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="22%" class="t-align-left ff">Form Name</th>
                                <th width="23%" class="t-align-left">Action</th>
                            </tr>
                            <%
                                 boolean flag = false;
                                 // Get Negotiation Tender forms by Lot id.
                                 for (SPCommonSearchData formdetails : commonSearchService.searchData("GetNegTenderFormsByLotId", tenderId, usrId, lotList.getFieldName3(), usrId, null, ""+negId, null, null, null))
                                 {

                                  flag = false;
                                  flag = negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formdetails.getFieldName1()),negId);
                                  if(flag==true)
                                  {
                                %>
                                  <tr>
                                    <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                                     <td class="t-align-left">
                                <%

                                        cnt=0;

                                         int negBidformId=0;
                                         List<SPCommonSearchData> neglist=commonSearchService.searchData("GetNegBidderFormsData", tenderId, usrId,formdetails.getFieldName1() , null, null,null, null, null,null);
                                        for (SPCommonSearchData negoformdetails : neglist)
                                        {
                                             cnt++;
                                            //out.println(formdetails.getFieldName2()+"=====test  ");

                                              if(negoformdetails.getFieldName4() != null)
                                            {
                                                type="negbiddata";
                                                negBidformId=Integer.parseInt(negoformdetails.getFieldName4());
                                            }
                                            else
                                            {
                                                type="biddata";
                                                negBidformId=0;
                                            }
                                              bidid=negoformdetails.getFieldName3();

                                                %>
                                                    <a href="../resources/common/ViewNegBidCompare.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%= bidid%>&uId=<%=usrId%>&negId=<%=negId%>&negBidformId=<%=negBidformId%>&lotId=0&type=<%=type%>&action=View">View</a>
                                                <%

                                                if(neglist.size() > 1 && cnt != neglist.size())
                                                    {
                                                        %>
                                                         &nbsp;|&nbsp;
                                                        <%
                                                    }
                                              }
                                }

                                  %>
                                </td> </tr>
                        <%

                                }
                            %>
                        </table>
                        <%     }
                            }
                           }//FinalStatus is yes then show forms
                        %><%
                                int docCnt = 0;
                                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                List<SPTenderCommonData> sptcdList = tenderCS.returndata("NegotiationDocInfo", String.valueOf(negId), "revisedoc");
                                List<SPTenderCommonData> sptcdListOfficer = new ArrayList<SPTenderCommonData>();
                                List<SPTenderCommonData> sptcdListBidder = new ArrayList<SPTenderCommonData>();
                                if(sptcdList!=null && !sptcdList.isEmpty()){
                                    for(int i=0;i<sptcdList.size();i++){
                                        if(sptcdList.get(i).getFieldName5().trim().equalsIgnoreCase(session.getAttribute("userId").toString())){
                                            sptcdListOfficer.add(sptcdList.get(i));
                                        }else{
                                            sptcdListBidder.add(sptcdList.get(i));
                                        }
                                    }
                                    if(sptcdListOfficer!=null && !sptcdListOfficer.isEmpty()){
                                
                            %>
                          <table width="100%" cellpadding="0" cellspacing="0" class="tableList_1">   
                            <tr>
                                <td class="t-align-left ff" width="22%">Reference Documents Uploaded by Evaluation Committee :</td>
                                <td>
                                     <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="32%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%

                                

                                for (SPTenderCommonData sptcd : sptcdListOfficer) {
                                    docCnt++;
                    %>
                    <tr>
                        <td class="t-align-left"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=download&docType=revisedoc" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                             <% if("".equalsIgnoreCase(finalStatus) && sptcd.getFieldName5().trim().equalsIgnoreCase(session.getAttribute("userId").toString())){ %>
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=remove&docType=revisedoc"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                            <%}%>
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                                             sptcd = null;
                                         }
                                     }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
                                </td>
                            </tr>
                            <%}
                                    docCnt = 0;

                  // Check if final submission done by tenderer then an only docs of tendere will dispaly
                  String tenderBidSubStatus=negotiationProcessSrBean.checkFinalNegoFinalSubmissionDone(negId);
                  if(tenderBidSubStatus != null && tenderBidSubStatus.equalsIgnoreCase("yes")) // Check if final submission done by tenderer
                    {
                         if(sptcdListBidder!=null && !sptcdListBidder.isEmpty())
                        {%>
                            <tr>
                                <td class="t-align-left ff">Reference Documents Uploaded by Bidder/Consultant :</td>
                                <td>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="32%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%

                                

                                for (SPTenderCommonData sptcd : sptcdListBidder) {
                                    docCnt++;
                    %>
                    <tr>
                        <td class="t-align-left"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docType=revisedoc&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=download&ub=2" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                             <% if("".equalsIgnoreCase(finalStatus) && sptcd.getFieldName5().trim().equalsIgnoreCase(session.getAttribute("userId").toString())){ %>
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docType=revisedoc&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=remove&ub=2"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                            <%}%>
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                                             sptcd = null;
                                         }
                                     }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                    </table>
                                </td>
                            </tr>
                          </table>
                                <%}
                }
}%>
                        <!-- Display form for uploading Documents and Accept or Reject forms -->
                        <form name="frmapproveTenderNegBid" id="idfrmapproveTenderNegBid" method="post" action="<%=request.getContextPath()%>/NegotiationFormsSrBean">
                            <input type="hidden" name="hidnegId" id="hidnegId" value="<%=negId%>"/>
                            <input type="hidden" name="hidtenderId" id="hidtenderId" value="<%=tenderId%>"/>
                            <input type="hidden" name="action" id="action" value="approveTenderNegBid" />
                            <input type="hidden" name="usrId" id="usrId" value="<%=usrId%>" />
                            <input type="hidden" name="finalSubStatus" id="finalSubStatus" value="<%=finalStatus%>" />
                            <input type="hidden" name="tendRefNo" value="<%=tenderRefNo%>" />
                            
                            <table class="tableList_1" width="100%">
                                <tr>
                                    <td width="22%" class="t-align-left ff">Status : <%if("yes".equalsIgnoreCase(finalStatus)){%><span class="mandatory">*</span><%}%></td>
                                    <%if("yes".equalsIgnoreCase(finalStatus)){%>
                                    <td width="78%">
                                        <label><input type="radio" name="nameStatus" id="idStatus" value="Accept" />&nbsp;Accept&nbsp;&nbsp;</label>
                                        <label><input type="radio" name="nameStatus" id="idStatus" value="Reject" />&nbsp;Reject&nbsp;&nbsp;</label>
                                        <label id="idStatusLabel"><input type="radio" name="nameStatus" id="idStatus" value="Resend" />&nbsp;Resend&nbsp;&nbsp;</label>
                                    </td>
                                    <%}else{%>
                                    <td width="78%">
                                        <label >Reject</>
                                    </td>
                                    <%}%>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Remarks <span class="mandatory">*</span> : </td>
                                    <td class="t-align-left ff">
                                        <textarea style="width: 675px;" id="idRemarks" class="formTxtBox_1" rows="3" name="txtRemarks"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="t-align-center">
                                        <label class="formBtn_1 t_space">
                                            <input type="submit" name="btnsubmit" id="idbtnsubmit" value="Submit" onclick="return Validate();"/>
                                        </label>
                                    </td>
                                </tr>
                            </table>
                        </form>
                </div></div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
               <!-- include Bottom Panel -->
              <%@include file="../resources/common/Bottom.jsp" %>
            </div>
    </body>
</html>