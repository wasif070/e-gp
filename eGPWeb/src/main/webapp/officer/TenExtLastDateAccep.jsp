<%--
    Document   : TenExtLastDateAccep
    Created on : Nov 29, 2010, 3:21:34 PM
    Author     : Rajesh Singh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender validity extension request</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>

        <script type="text/javascript">

            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });


                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            //Onclick Event
            function Validate(){
                var temp='';

                if (document.getElementById('txtLastAcceptance').value=='')
                {
                    $('#spanLastAcceptance').html('<br/>Please select Last Acceptance Date.');
                    temp='true';
                }

                if (document.getElementById('txtComments').value=='')
                {
                    $('#spanComments').html('<br/>Please enter Comments.');
                    temp='true';
                }

                if(temp=='true')
                {
                    return false;
                }
            }

            function NewSecurityDate(){
                if (document.getElementById('txtLastAcceptance').value!='')
                {
                    $('#spanLastAcceptance').html('');
                }
                else
                {
                    $('#spanLastAcceptance').html('<br/>Please select Last Acceptance Date.');
                }
            }

            function Extension(){
                if (document.getElementById('txtComments').value!='')
                {
                    $('#spanComments').html('');
                }
                else
                {
                    $('#spanComments').html('<br/>Please enter Comments.');
                }
            }

        </script>
    </head>
    <body>
        <%
        HandleSpecialChar handleSpecialChar = new  HandleSpecialChar();
                    if (request.getParameter("btnhope") != null) {

                        //String dtXml = "";
                        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                        String dt1 = request.getParameter("txtLastAcceptance");
                        
                        if (dt1 != "") {
                            String[] webDtArr1 = dt1.split("/");
                            dt1 = webDtArr1[2] + "-" + webDtArr1[1] + "-" + webDtArr1[0];
                        }

                        String table="", updateString = "", whereCondition="";
                        table="tbl_TenderValidityExtDate ";
                        updateString=" lastValAcceptDt='"+dt1+"',notifyComments='"+handleSpecialChar.handleSpecialChar(request.getParameter("txtComments"))+"',extReplyDt='"+format.format(new Date())+"' ";
                        whereCondition=" valExtDtId=" + request.getParameter("ExtId");

                        //out.println("update "+table+" set " +updateString+ " where "+whereCondition);

                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderValidityExtDate", updateString, whereCondition).get(0);
                        //out.println(commonMsgChk.getMsg());
                        if(commonMsgChk.getFlag()==true)
                        {
                            response.sendRedirect("TenderExtReqListing.jsp");
                        }
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Validity Extension Request</div>
            <div class="mainDiv">
                <%
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
            </div>
            <div>&nbsp;</div>
            <hr />
            <div class="t-align-left t_space">
                <span><a href="#" class="action-button-goback">Go Back to Dashboard</a></span>
                <span style="font-style: italic" class="t-align-left" style="float:left; font-weight:normal;">
			Fields marked with (<span class="mandatory">*</span>) are mandatory
                </span>
            </div>
            <form id="frmhope" action="TenExtLastDateAccep.jsp?tenderId=<%=request.getParameter("tenderId") %>&ExtId=<%=request.getParameter("ExtId") %>" method="post">
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <%
                            List<SPTenderCommonData> list = tenderCommonService.returndata("GetValidityExtDetail", request.getParameter("ExtId"), null);
                            if (!list.isEmpty()) {
                %>
                    <tr>
                        <td width="21%" class="t-align-left ff">Tender Validity in no. of Days : <span class="mandatory">*</span></td>
                        <td width="79%" class="t-align-left"><%=list.get(0).getFieldName1()%></td>
                    </tr>

                    <tr>
                        <td class="t-align-left ff">Last Date  of Tender Validity : <span class="mandatory">*</span></td>
                        <td class="t-align-left"><%=list.get(0).getFieldName2()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">New  Date of Tender Validity : <span class="mandatory">*</span></td>
                        <td class="t-align-left"><%=list.get(0).getFieldName3()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Last Date of Tender Security Validity : <span class="mandatory">*</span></td>
                        <td class="t-align-left"><%=list.get(0).getFieldName4()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">New Date of Tender Security Validity : <span class="mandatory">*</span></td>
                        <td class="t-align-left"><%=list.get(0).getFieldName5()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Last Date for Acceptance of Tender Validity Extension Request : <span class="mandatory">*</span></td>
                        <td class="t-align-left"><input name="txtLastAcceptance" type="text" class="formTxtBox_1" id="txtLastAcceptance" style="width:70px;" readonly="true" onclick="GetCal('txtLastAcceptance','txtLastAcceptance');" onblur="NewSecurityDate();"/>
                            <a href="javascript:void(0);"  title="Calender"><img id="txtLastAcceptanceimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('txtLastAcceptance','txtLastAcceptanceimg');"/></a>
                            <span id="spanLastAcceptance" class="mandatory"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Comments : <span class="mandatory">*</span> </td>
                        <td class="t-align-left">
                            <textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1" onblur="Extension();"></textarea>
                            <span id="spanComments" class="mandatory"></span>
                        </td>
                    </tr>
                    <%}%>
                </table>
                <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input name="btnhope" id="btnhope" type="submit" value="Submit" onclick="return Validate();"/>
                    </label>
                </div>
            </form>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
