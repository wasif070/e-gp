<%--
    Document   : ViewProgressReport
    Created on : Jul 22, 2011, 2:03:46 PM
    Author     : shreyansh
--%>

<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Progress Report</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>



        <%
                    int userid = 0;
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userid = Integer.parseInt(hs.getAttribute("userId").toString());
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                     ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
                    String lotId = request.getParameter("lotId");
                    CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                    List<Object[]> list = cs.getLotDetailsByPkgLotId(lotId,request.getParameter("tenderId"));
        %>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">View Progress Report
                        <span class="c-alignment-right"><a href="ProgressReport.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <div>&nbsp;</div>
                    <%
                                pageContext.setAttribute("tab", "14");

                    %>
                    <%@include  file="officerTabPanel.jsp"%>
                    <div class="tabPanelArea_1">

                        <%
                                    pageContext.setAttribute("TSCtab", "2");

                        %>
                        <%@include  file="../resources/common/CMSTab.jsp"%>
                        <div class="tabPanelArea_1">
                            <form name="prfrm" id="prfrm"  method="post" action="<%=request.getContextPath()%>/ConsolidateServlet">
                                <%if (request.getParameter("isedit") != null) {%>
                                <input type="hidden" name="edit" value="edit" />
                                <%}%>
                                <input type="hidden" name="action" id="action" value="" />
                                <input type="hidden" name="firstdate" id="firstdate" value="1-Jun-2011" />
                                <input type="hidden" name="seconddate" id="seconddate" value="1-Sep-2011" />
                                <input type="hidden" name="save" id="save" value="" />
                                <input type="hidden" name="tenderId" value="<%=request.getParameter("tenderId")%>" />
                                <input type="hidden" name="lotId" value="<%=request.getParameter("lotId")%>" />
                                
                    <%
                                String tenderId = request.getParameter("tenderId");
                                boolean flag = true;
                                CommonService commonServicee = (CommonService) AppContext.getSpringBean("CommonService");
                                String procnaturee = commonServicee.getProcNature(request.getParameter("tenderId")).toString();
                                String strProcNaturee = "";
                                String strLotNo = "Lot No.";
                                String strLotDes = "Lot Description";
                                if("Services".equalsIgnoreCase(procnaturee))
                                {
                                    strLotNo = "Package No.";
                                    strLotDes = "Package Description";
                                }else if("goods".equalsIgnoreCase(procnaturee)){
                                }else{
                                }
                                //CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                               // List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                List<TblCmsPrMaster> Prlist = service.getPRHistory(Integer.parseInt(request.getParameter("wpId")));

                                int i = 0;
                                for (Object [] lotList : list) {
                    %>
                   

                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                            <tr>
                                <td width="20%"><%=strLotNo%></td>
                                <td width="80%"><%=lotList[0]%></td>
                            </tr>
                            <tr>
                                <td><%=strLotDes%></td>
                                <td class="t-align-left"><%=lotList[1]%></td>
                            </tr>
                        </table>
                            <% }%>
                                <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>" />
                                <input type="hidden" name="wpId" id="wpId" value="<%=request.getParameter("wpId")%>" />
                                <input type="hidden" name="action" value="preparepr" />
                                <div id="resultDiv" style="display: block;">
                                    <div  id="print_area">
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                                <th width="10%" class="t-align-center"><%=bdl.getString("CMS.PR.prno")%></th>
                                                <th width="15%" class="t-align-center"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                <th width="13%" class="t-align-center"><%=bdl.getString("CMS.action")%>
                                                 </th>
                                             </tr>
                                             <%if(!Prlist.isEmpty() && Prlist!=null){
                                                 int j=1;
                                             for(int ii=0;ii<Prlist.size();ii++){
                                                    String temp[] = Prlist.get(ii).getProgressRepNo().split(" ");
                                                    String temp1[] = temp[4].split("-");
                                                    System.out.println(temp[4]);
                                                    String finalNo = temp[0]+" "+temp[1]+" "+temp[2]+" "+temp[3]+" "+DateUtils.customDateFormate(DateUtils.convertStringtoDate(temp[4].toString(),"yyyy-MM-dd"));
    %>

                                             <tr>
                                             <td class="t-align-center"><%=j+ii%></td>
                                             <td class="t-align-center"><%=finalNo %></td>
                                             <td class="t-align-center"><a href="ViewPr.jsp?repId=<%=Prlist.get(ii).getProgressRepId()%>&wpId=<%=request.getParameter("wpId")%>&tenderId=<%=tenderId%>&lotId=<%= lotId  %>">View</a>
                                             &nbsp;|&nbsp;<a href="ProgressReportUploadDoc.jsp?prRepId=<%=Prlist.get(ii).getProgressRepId()%>&wpId=<%=request.getParameter("wpId")%>&tenderId=<%=tenderId%>">Upload / View Document</a>
                                             </td>
                                             <%}}else{%>
                                             <td class="t-align-center" colspan="3">No records Found</td>
                                             <%}%>
                                             </tr>
                                        </table>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--Dashboard Content Part End-->
                </div>
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
        </div>

    </body>

    <script>
        var headSel_Obj = document.getElementById("headTabReport");
        loadTable();
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>


