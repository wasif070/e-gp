<%-- 
    Document   : QuestionWindow
    Created on : Jan 12, 2011, 2:01:15 PM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<jsp:useBean scope="request" id="askProcurementSrBean" class="com.cptu.egp.eps.web.servicebean.AskProcurementSrBean" />
<jsp:useBean scope="request" id="askProcurementDtBean" class="com.cptu.egp.eps.web.databean.AskProcurementDtBean" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    boolean isView = false;
                    boolean isCreate = false;
                    if ("view".equals(request.getParameter("action"))) {
                        isView = true;
                        askProcurementSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    } else if ("create".equals(request.getParameter("action"))) {
                        askProcurementSrBean.setAuditTrail(null);
                        isCreate = true;
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%if (isView) {%>View Query<%} else {%>Post Reply<% }%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        
        <jsp:setProperty name="askProcurementDtBean" property="*"/>
        <script type="text/javascript">
            function regForSpace(value){
                return /^\s+|\s+$/g.test(value);
            }
            function Validate()
            {   
                var validatebool=true;
                
                if(document.getElementById('answer').value == ''){
                    document.getElementById("valAnswer").innerHTML="Please enter your reply";
                    validatebool="false";
                }else if(regForSpace(document.getElementById('answer').value)){
                    document.getElementById("valAnswer").innerHTML="Only Space is not allowed";
                    validatebool="false";
                }else{
                    document.getElementById("valAnswer").innerHTML="";
                }
                if (validatebool=='false'){
                    return false;
                }
            }
        </script>
    </head>
    <%
                int procQueId = 0;
                if (request.getParameter("procQueId") != null) {
                    procQueId = Integer.parseInt(request.getParameter("procQueId"));
                }
                String answerByName = "";
                if (session.getAttribute("userName") != null || !session.getAttribute("userName").equals("")) {
                    answerByName = session.getAttribute("userName").toString();
                }
    %>
    <body>
    <%
            askProcurementSrBean.setLogUserId(session.getAttribute("userId").toString());
    %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="contentArea_1">
                            <div class="pageHead_1"><%if (isView) {%>View Query<%} else {%>Post Reply<% }%><span style="float:right;"><a href="Question.jsp" class="action-button-goback">Go Back to Dashboard</a></span></div>
                            <%
                                        int userId = 0;
                                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                            userId = Integer.parseInt(session.getAttribute("userId").toString());
                                        }
                                        if ("Submit".equals(request.getParameter("Submit"))) {
                                            askProcurementSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                            if (askProcurementSrBean.updateAskProcurement(askProcurementDtBean)) {
                                                response.sendRedirect("Question.jsp?action=reply");
                                            }
                                        }
                            %>
                            <form id="frmQuestionAnswer" name="frmQuestionAnswer" action="QuestionWindow.jsp" method="post">
                                <table class="tableList_1 t_space" border="0" cellpadding="0" cellspacing="10" width="100%">
                                    <% for (Object[] obj : askProcurementSrBean.getListQuestion(procQueId)) {%>
                                    <tr>
                                        <td width="7%" class="ff">Category :</td>
                                        <td width="93%"><%=obj[1]%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Query :</td>
                                        <td><%=obj[3]%>
                                            <input type="hidden" name="question" value="<%=obj[3]%>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Reply : <%if (!isView) {%><span class="mandatory">*</span><% }%></td>
                                        <td><% if (isView) {
                                                 out.print(obj[6]);
                                             } else {%>
                                            <textarea name="answer" rows="5" class="formTxtBox_1" id="answer" style="width:600px;"></textarea>
                                            <script type="text/javascript">
                                                //<![CDATA[
//                                                CKEDITOR.replace( 'answer',
//                                                {
//                                                    toolbar : "egpToolbar"
//
//                                                });
                                                //]]>
                                            </script>
                                            <% }%>
                                            <div id="valAnswer" class="reqF_1"></div>
                                            <input type="hidden" name="procQueId" value="<%=obj[2]%>"/>
                                            <input type="hidden" name="queCatId" value="<%=obj[0]%>"/>
                                            <input type="hidden" name="postedDateString" value="<%=obj[4]%>"/>
                                            <input type="hidden" name="postedBy" value="<%=obj[5]%>"/>
                                            <input type="hidden" name="answeredBy" value="<%=userId%>"/>
                                            <input type="hidden" name="postedByName" value="<%=obj[9]%>"/>
                                            <input type="hidden" name="answeredByName" value="<%=answerByName%>"/>
                                            <input type="hidden" name="userTypeId" value="<%=obj[10]%>"/>
<!--                                            <input type="hidden" name="answerDateString" value="<%//obj[7]%>"/>-->
<!--                                            <input type="hidden" name="answeredBy" value="<%//obj[8]%>"/>-->
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td class="ff">Posted<br/>By :</td>
                                        <td><%=obj[9]%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Posted<br/>On :</td>
                                        <td><%=DateUtils.gridDateToStrWithoutSec((Date) obj[4])%>
                                        </td>
                                    </tr>
                                    <% }%>
                                </table>
                                <% if (!isView) {%>
                                &nbsp;
                                <div class="t-align-left" style="padding-left: 69px;">
                                    <label class="formBtn_1" >
                                        <input type="submit" name="Submit" id="Submit" value="Submit" onclick="return Validate();"/>
                                    </label>
                                </div>
                                <% }%>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <%
                askProcurementDtBean = null;
                askProcurementSrBean = null;
    %>
</html>
