<%-- 
    Document   : DANOA
    Created on : Jan 3, 2011, 3:35:27 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Draft Agreement</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Tender Dashboard</div>
                <%          String dateTender = "";
                            // Variable tenderId is defined by u on ur current page.
                            String tenderId = "";
                            if (request.getParameter("tenderId") != null) {
                                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                                tenderId = request.getParameter("tenderId");
                            }
                            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            //List<SPCommonSearchData> submitDateSts = commonSearchService.searchData("chkSubmissionDt", tenderId, null, null, null, null, null, null, null, null);
                            List<SPCommonSearchData> packageList = commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null);
                            String userId = "";
                            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                //userId = Integer.parseInt(session.getAttribute("userId").toString());
                                userId = session.getAttribute("userId").toString();
                                issueNOASrBean.setLogUserId(userId);
                                commonSearchService.setLogUserId(userId);
                            }
                            //pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                            pageContext.setAttribute("tab", "14");

                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>

                <%@include  file="officerTabPanel.jsp"%>
                <div class="tabPanelArea_1">
                    <div align="center">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1">
                            <%
                                        if (!packageList.isEmpty()) {
                            %>
                            <tr>
                                <td width="15%" class="ff">Package  No. :</td>
                                <td width="85%" class="t-align-left"><%=packageList.get(0).getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="ff">Package  Description :</td>
                                <td class="t-align-left"><%=packageList.get(0).getFieldName1()%></td>
                            </tr>
                            <% }%>
                            <tr>
                                <td class="ff">Draft Agreement</td>
                                <td>
                                    <%
                                                List<SPTenderCommonData> tenderValSec = tenderCommonService.returndata("getTenderDatesDiff", tenderId, null);
                                                if (!tenderValSec.isEmpty()) {
                                                    if (tenderValSec.get(0).getFieldName1() != null) {
                                                        int tenderValdays = Integer.parseInt(tenderValSec.get(0).getFieldName1());
                                                        if (tenderValdays > 7) {
                                    %>
                                    <a href="<%=request.getContextPath()%>/officer/DAIssueNOA.jsp?tenderId=<%=tenderId%>">Issue</a>
                                    <%
                                                                                            }
                                                                                        }
                                                                                        if (tenderValSec.get(0).getFieldName2() != null) {
                                                                                            int tenderSecdays = Integer.parseInt(tenderValSec.get(0).getFieldName2());
                                                                                            if (tenderSecdays > 7) {
                                    %>
                                    <a href="<%=request.getContextPath()%>/officer/DAIssueNOA.jsp?tenderId=<%=tenderId%>">Issue</a>
                                    <%
                                                        }
                                                    }
                                                }
                                    %>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="15%" class="t-align-left">Name of Bidder/Consultant </th>
                                <th width="12%" class="t-align-left">Contract No.</th>
                                <th width="11%" class="t-align-left">Date of Issue</th>
                                <th width="18%" class="t-align-left">Last Acceptance Date &amp; Time</th>
                                <th width="22%" class="t-align-left">Letter of Acceptance (LOA) Acceptance Status</th>
                                <th width="10%" class="t-align-left">Action</th>
                            </tr>
                            <tr>
                                <%
                                            for (Object[] obj : issueNOASrBean.getDetailsService(Integer.parseInt(tenderId))) {
                                                for (SPTenderCommonData companyList : tenderCommonService.returndata("GetEvaluatedBidders", tenderId, "0")) {
                                                    if (obj[5].toString().equalsIgnoreCase(companyList.getFieldName2())) {
                                                        out.print("<td>" + companyList.getFieldName1() + "</td>");
                                                    }
                                                }
                                                out.print("<td>" + obj[1] + "</td>");
                                                out.print("<td>" + obj[2] + "</td><td>");
                                                //out.print("<td>"+obj[5]+"</td>");
                                                if (obj[4] != null) {
                                                    out.print(obj[4]);
                                                }
                                                out.print("</td>");
                                                out.print("<td>" + obj[3] + "</td><td>");
                                                if (!"Approved".equalsIgnoreCase(obj[3].toString())) {
                                                    out.print("<a href=\"ManageDAIssueNOA.jsp?noaIssueId=" + obj[0] + "&tenderId="+tenderId+ "\">Edit</a> | ");
                                                    out.print("<a href=\"ViewNOADA.jsp?noaIssueId=" + obj[0] + "&tenderId="+tenderId+ "\">View</a>");
                                                }
                                                out.print("</td>");
                               
                                }
                                %>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!--Dashboard Content Part End-->
            <div>&nbsp;</div>
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
        <script type="text/javascript">
            function msgTendervalidity(){
                var dateTender;
                if(document.getElementById('dateTender') != null){
                    if(document.getElementById('dateTender').value != ""){
                        jAlert("Tender validity is about to expire on "+ dateTender,"Failure");
                    }
                }
            }
            function msgTValidity(){
                var dateTender;
                if(document.getElementById('dateTender') != null){
                    if(document.getElementById('dateTender').value != ""){
                        jAlert("Tender validity date "+ dateTender + " has been expired. Please extend tender validity date first");
                    }
                }
            }
        </script>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
<%
                                                issueNOASrBean = null;
%>