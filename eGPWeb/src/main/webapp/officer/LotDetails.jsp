<%-- 
    Document   : LotDetails
    Created on : Dec 6, 2010, 7:32:40 PM
    Author     : Taher
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
     <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
     <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />       
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lot Details</title>
    </head>
    <body>
       <%@include  file="../resources/common/AfterLoginTop.jsp"%>

       <div class="contentArea_1">
        <div class="pageHead_1">Lot Details</div>
        <%
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                        pageContext.setAttribute("tab", "2");                
                TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                   List<TblTenderLotSecurity> lots= null;
                   int tendid = 0;
                if(request.getParameter("tenderid") != null){
                    tendid = Integer.parseInt(request.getParameter("tenderid"));
                     lots = tenderDocumentSrBean.getLotDetails(tendid);
                }
                /*if("Use STD".equalsIgnoreCase(request.getParameter("submit"))){
                    tenderDocumentSrBean.dumpSTD(String.valueOf(tendid), request.getParameter("txtStdTemplate"), session.getAttribute("userId").toString());
                    response.sendRedirect("LotDetails.jsp?tenderid="+tendid);
               }*/
                   
        %>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <div class="t_space">
        <%@include  file="officerTabPanel.jsp"%>
        </div>
          <div class="tabPanelArea_1 ">
              <%
                    String stdName=tenderDocumentSrBean.checkForDump(String.valueOf(tendid));
                    if(stdName==null){
                    String stdTemplateId = tenderDocumentSrBean.findStdTemplateId(String.valueOf(tendid));
                    if(Integer.parseInt(stdTemplateId)!=0){
              %>
              <%--<form action="LotDetails.jsp" method="post">
              <table width="100%" cellspacing="0" class="tableList_1" >
                  <tr>
                      <td>Prepare Tender Document : </td>
                      <td>
                          <input type="hidden" value="<%=tendid%>" name="tenderid"/>
                          <input type="hidden" value="<%=stdTemplateId%>" name="txtStdTemplate"/>
                          <label class="formBtn_1">
                            <input name="submit" type="submit" value="Use STD" id="btnSubmit"/>
                        </label>
                      </td>
                  </tr>
              </table>
              </form>--%>
              <%}else{out.print("<table width='100%' cellspacing='0' class='b_space'><tr><td><div class=\"responseMsg noticeMsg\">SBD yet not mapped</div></td></tr></table>");}}else{out.print("<table width='100%' cellspacing='0' class='tableList_1'><tr><td width='10%'>SBD : </td><td>"+stdName+"</td></tr></table>");}%>
            <table width="100%" cellspacing="0" class="tableList_1">
            <tr>
                    <th width="10%" class="t-align-left">Lot No.</th>
                    <th width="80%" class="t-align-left">Lot Description</th>
                    <th width="10%" class="t-align-left">Action</th>
                </tr>
                <% if(lots.size()>0 && lots != null){
                     Iterator it = lots.iterator();
                     int i = 1;
                           while(it.hasNext()){

                               TblTenderLotSecurity tblTenderLotSecurity = (TblTenderLotSecurity)it.next();
                               %>
                               <tr>
                                   <td class="t-align-center"><%= tblTenderLotSecurity.getLotNo()%></td>
                                   <td><%=tblTenderLotSecurity.getLotDesc() %></td>
                                   <td class="t-align-center"><a href="TenderDocPrep.jsp?tenderId=<%=tendid%>&porlId=<%=tblTenderLotSecurity.getAppPkgLotId() %>">View</a></td>
                               </tr>
                        <%
                        i++;
                               }
                         } %>
        </table>        
          </div>
        </div>
         <div>&nbsp;</div>

             <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
           
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
