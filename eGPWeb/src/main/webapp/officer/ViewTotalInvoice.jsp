<%--
    Document   : ViewTotalInvoice
    Created on : Sep 3, 2011, 2:31:24 PM
    Author     : dixit
--%>

<%@page import="java.math.RoundingMode"%>
<%@page import="com.cptu.egp.eps.web.utility.GenerateChart"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Financial Progress Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.print.blocklink.js" type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <SCRIPT LANGUAGE="Javascript" SRC="/FusionCharts/FusionCharts.js"></SCRIPT>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
        <style type="text/css">
        <!--
        body {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        }
        -->
        </style>
    </head>
    <body>
        <%
            String tenderId = "";
            if (request.getParameter("tenderId") != null) {
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                tenderId = request.getParameter("tenderId");
            }
            String lotId = "";
            if(request.getParameter("lotId")!=null)
            {
                lotId = request.getParameter("lotId");
                pageContext.setAttribute("lotId", request.getParameter("lotId"));
            }
            String cntId = "0";
            if(request.getParameter("cntId")!=null){
                cntId = request.getParameter("cntId");
                pageContext.setAttribute("cntId", request.getParameter("cntId"));
            }
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
            String serviceType = commonService.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
            String strProcNature = "";
            if("Services".equalsIgnoreCase(procnature))
            {
                strProcNature = "Consultant";
            }else if("goods".equalsIgnoreCase(procnature)){
                strProcNature = "Supplier";
            }else{
                strProcNature = "Contractor";
            }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div  id="print_area">
            <div class="contentArea_1">
            <div class="pageHead_1">Financial Progress Report
                <span style="float: right; text-align: right;" class="noprint">
                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                    <%
                        if(!"services".equalsIgnoreCase(procnature))
                        {
                            String folderName = request.getContextPath()+"/officer/ProgressReport";
                            if(userTypeId == 2){
                                folderName = request.getContextPath()+"/tenderer/progressReport";
                                }
                    %>
                    <a class="action-button-goback" href="<%=folderName%>.jsp?tenderId=<%=tenderId%>" title="Go Back">Go Back</a>
                    <%folderName=null;%>
                    <%}else{
                        if(!"Time based".equalsIgnoreCase(serviceType.toString()))
                        {
                    %>
                            <a class="action-button-goback" href="SrvLumpSumPr.jsp?tenderId=<%=tenderId%>" title="Go Back">Go Back</a>
                      <%}else{%>
                            <a class="action-button-goback" href="ProgressReportMain.jsp?tenderId=<%=tenderId%>" title="Go Back">Go Back</a>
                    <%}}%>
                </span>
                </div>
                <%@include file="../resources/common/ContractInfoBar.jsp"%>
                <div class="tabPanelArea_1 t_space">
                <table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
                <tr>
                    <th width="12%" class="t-align-center">Total Invoice Generated by <%=strProcNature%> till Date <br/>(In Nu.)</th>
                    <th width="12%" class="t-align-center">Total Invoice Processed by Accounts Officer till Date<br/>(In Nu.)</th>
                </tr>
                <tr>
                    <td class="t-align-right" style="text-align: right;">
                        <%
                        AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
                        List<Object> totalIAmtfromImaster = null;
                        if(!"0".equals(cntId)){
                            totalIAmtfromImaster = accPaymentService.getTotalInvoiceAmtFromImasterForRO(Integer.parseInt(cntId));
                        }else{
                            totalIAmtfromImaster = accPaymentService.getTotalInvoiceAmtFromImaster(Integer.parseInt(tenderId));
                        }

                        if(totalIAmtfromImaster.get(0)!=null)
                        {
                            if(totalIAmtfromImaster.get(0)==null)
                            {
                                out.print(0);
                            }
                            else{
                            out.print(totalIAmtfromImaster.get(0).toString());
                            }
                        }else{out.print(0);}
                        %>
                    </td>
                    <td class="t-align-center">
                        <table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
                        <tr>
                            <th width="12%" class="t-align-center">Gross Amount</th>
                            <th width="12%" class="t-align-center">Net Amount</th>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                            <%
                            Object[] obj = null;
                            List<Object[]> totalRDeducted = null;
                            if(!"0".equals(cntId)){
                                totalRDeducted = accPaymentService.getAllTotalInvoiceAmtFromAcForRO(Integer.parseInt(cntId));
                            }else{
                                totalRDeducted = accPaymentService.getAllTotalInvoiceAmtFromAc(Integer.parseInt(lotId));
                            }

                            if(!totalRDeducted.isEmpty())
                            {
                                obj = totalRDeducted.get(0);
                                if(obj[0]==null)
                                {
                                    out.print(0);
                                }
                                else{
                                out.print(obj[0].toString());
                                }
                            }else{out.print(0);}
                        %>
                        </td>
                        <td style="text-align: right;">
                            <%
                                if(obj[13]==null)
                                {
                                    out.print(0);
                                }
                                else{
                                out.print(obj[13].toString());
                                }
                            %>
                        </td>
                        </tr>
                        </table>

                    </td>
                </tr>
                </table>
                <div class="tableHead_1 t_space t_space" align="left">Financial Details</div>
                <table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
                    <tr>
                        <th class="t-align-center">Invoice No </th>
                        <th class="t-align-center">Status</th>
                        <th class="t-align-center">Invoice Amount <br/>(Gross Amount)</th>
                        <th class="t-align-center">Invoice Submission Date <br/>By <%=strProcNature%></th>
                        <th class="t-align-center">Payment Date <br/>(by Accounts Officer) </th>
                        <th class="t-align-center">Paid Amount <br/>(Net Amount)</th>
                        <th class="t-align-center">VAT<br/>(in %)</th>
                        <th class="t-align-center">VAT<br/>(in Nu.)</th>
                        <th class="t-align-center">AIT<br/>(in %)</th>
                        <th class="t-align-center">AIT<br/>(in Nu.)</th>
                    </tr>

                        <%
                            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                            List<Object> wpidObject = service.getWpId(Integer.parseInt(lotId));
                            String wpId = "0";
                            if(wpidObject.get(0)!=null)
                            {
                                wpId = wpidObject.get(0).toString();
                            }
                             List<SPCommonSearchDataMore> spsdm = service.getFinancialDetails(tenderId,lotId, cntId);
                             BigDecimal PEAmount = new BigDecimal(0);
                             if(spsdm!=null)
                             {
                                for(int i=0; i<spsdm.size(); i++){
                        %>
                                    <tr>
                                        <td class="t-align-center"><a view='link' href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=<%=wpId%>&invoiceId=<%=spsdm.get(i).getFieldName1()%>&flag=fin"><%= (spsdm.get(i).getFieldName10()!= null) ? spsdm.get(i).getFieldName10() : "Invocie"+(i+1) %></a></td>
                                    <td class="t-align-center"><%if(!"".equalsIgnoreCase(spsdm.get(i).getFieldName2()) && spsdm.get(i).getFieldName2()!=null){%>
                                        <%
                                            String status = "";
                                            if (spsdm.get(i).getFieldName2() == null) {
                                                status = "-";
                                            } else if ("createdbyten".equalsIgnoreCase(spsdm.get(i).getFieldName2())) {
                                                if (procnature.equalsIgnoreCase("works")) {
                                                    status = "Invoice Generated by Contractor";
                                                } else {
                                                    status = "Invoice Generated by Supplier";
                                                }
                                            } else if ("acceptedbype".equalsIgnoreCase(spsdm.get(i).getFieldName2())) {
                                                PEAmount = PEAmount.add(new BigDecimal(spsdm.get(i).getFieldName3()));
                                                status = "Invoice Accepted by PE";
                                            } else if ("sendtope".equalsIgnoreCase(spsdm.get(i).getFieldName2())) {
                                                PEAmount = PEAmount.add(new BigDecimal(spsdm.get(i).getFieldName3()));
                                                status = "In Process";
                                            } else if ("sendtotenderer".equalsIgnoreCase(spsdm.get(i).getFieldName2())) {
                                                PEAmount = PEAmount.add(new BigDecimal(spsdm.get(i).getFieldName3()));
                                                status = "Processed by Accounts Officer";
                                            } else if ("remarksbype".equalsIgnoreCase(spsdm.get(i).getFieldName2())) {
                                                PEAmount = PEAmount.add(new BigDecimal(spsdm.get(i).getFieldName3()));
                                                status = "Processed by PE";
                                            } else if ("rejected".equalsIgnoreCase(spsdm.get(i).getFieldName2())) {
                                                status = "Invoice Rejected by PE";
                                            }
                                            out.print(status);
                                         }
                                        %>
                                    </td>
                                    <td class="t-align-right" style="text-align: right;"><%if(!"".equalsIgnoreCase(spsdm.get(i).getFieldName3()) && spsdm.get(i).getFieldName3()!=null){%><%=spsdm.get(i).getFieldName3()%><%}%></td>
                                    <td class="t-align-center"><%if(!"".equalsIgnoreCase(spsdm.get(i).getFieldName4()) && spsdm.get(i).getFieldName4()!=null){%><%=spsdm.get(i).getFieldName4()%><%}%></td>
                                    <td class="t-align-center"><%if(!"".equalsIgnoreCase(spsdm.get(i).getFieldName5()) && spsdm.get(i).getFieldName5()!=null){%><%=spsdm.get(i).getFieldName5()%><%}%></td>
                                    <td class="t-align-right" style="text-align: right;"><%if(!"".equalsIgnoreCase(spsdm.get(i).getFieldName6()) && spsdm.get(i).getFieldName6()!=null){%><%=spsdm.get(i).getFieldName6()%><%}%></td>
                                    <td class="t-align-right" style="text-align: right;"><%if(!"".equalsIgnoreCase(spsdm.get(i).getFieldName7()) && spsdm.get(i).getFieldName7()!=null){%><%=spsdm.get(i).getFieldName7()%><%}%></td>
                                    <td class="t-align-right" style="text-align: right;">
                                        <%
                                            if(!"".equalsIgnoreCase(spsdm.get(i).getFieldName7()) && spsdm.get(i).getFieldName7()!=null){
                                                out.print((new BigDecimal(spsdm.get(i).getFieldName3()).multiply(new BigDecimal(spsdm.get(i).getFieldName7()))).divide(new BigDecimal(100),3,RoundingMode.HALF_UP));
                                            }
                                        %>
                                    </td>
                                    <td class="t-align-right" style="text-align: right;"><%if(!"".equalsIgnoreCase(spsdm.get(i).getFieldName8()) && spsdm.get(i).getFieldName8()!=null){%><%=spsdm.get(i).getFieldName8()%><%}%></td>
                                    <td class="t-align-right" style="text-align: right;">
                                        <%
                                            if(!"".equalsIgnoreCase(spsdm.get(i).getFieldName8()) && spsdm.get(i).getFieldName8()!=null){
                                                out.print((new BigDecimal(spsdm.get(i).getFieldName3()).multiply(new BigDecimal(spsdm.get(i).getFieldName8()))).divide(new BigDecimal(100),3,RoundingMode.HALF_UP));
                                            }
                                        %>
                                    </td>
                                    </tr>
                                <%}}else{%>
                                    <td colspan="8" class="t-align-center">No records found.</td>
                                <%}
                             %>
                </table>

                        <%--/*<%
                List<Object[]> allRecordofTe = accPaymentService.getAllInvoiceAmtFromImaster(Integer.parseInt(tenderId));
                List<Object[]> allRecordofAC = accPaymentService.getAllInvoiceAmtFromAc(Integer.parseInt(lotId));

                int Interval = 0;
                double Contractvalue = Double.parseDouble(c_obj[2].toString());
                if(Contractvalue>100000)
                {
                    Interval = 4;
                }
                else if(Contractvalue>5000000)
                {
                     Interval = 9;
                }
                else if(Contractvalue>10000000)
                {
                     Interval = 14;
                }
                else if(Contractvalue>500000000)
                {
                     Interval = 19;
                }
                else if(Contractvalue>1000000000)
                {
                     Interval = 24;
                }
                String strXML="";
                //strXML += "<chart  caption='Sales' showLabels='1' showvalues='0'  connectNullData='0' PYAxisName='Revenue' SYAxisName='Quantity'  numDivLines='10' formatNumberScale='0' yAxisMaxValue='"+abc+"' >";
                strXML += "<chart  caption='Invoice' showLabels='1' showvalues='0'  connectNullData='0' PYAxisName='Revenue' SYAxisName='Quantity' adjustDiv='50' numDivLines='"+Interval+"' formatNumberScale='0' yAxisMaxValue='"+c_obj[2].toString()+"' >";

                strXML += "<categories>";

                    if(!allRecordofTe.isEmpty())
                    {
                        for(int i=1; i<=allRecordofTe.size(); i++)
                        {
                            strXML += "<category label='"+i+"' />";
                        }
                    }

                strXML += "</categories>";

                strXML += "<dataset seriesName='Contractor' color='AFD8F8' showValues='0'>";

                    if(!allRecordofTe.isEmpty())
                    {
                        for(int i=0; i<allRecordofTe.size(); i++)
                        {
                            Object[] objarray = allRecordofTe.get(i);
                            strXML += "<set value='"+objarray[1]+"' />";
                        }
                    }

                strXML += "</dataset>";

                strXML += "<dataset seriesName='Accountant' color='F6BD0F' showValues='0'>";
                    if(!allRecordofAC.isEmpty())
                    {
                        for(int i=0; i<allRecordofAC.size(); i++)
                        {
                            for(int j=0; j<allRecordofTe.size(); j++)
                            {
                                Object[] objarray = allRecordofTe.get(j);
                                Object[] objarrayAc = allRecordofAC.get(i);
                                if(objarray[0].toString().equalsIgnoreCase(objarrayAc[0].toString())){
                                strXML += "<set value='"+objarrayAc[1]+"' />";
                                }else{
                                    strXML += "<set value='No data Avaliable' />";
                                }
                            }
                        }
                    }
                    strXML += "</dataset>";
                    strXML += "</chart>";
                %>--%>

                <%--<jsp:include page="../resources/common/FusionChartRenderer.jsp" flush="true">
                    <jsp:param name="chartSWF" value="../FusionCharts/MSColumnLine3D.swf" />
                    <jsp:param name="strXML" value="<%=strXML %>" />
                    <jsp:param name="chartId" value="productSales" />
                    <jsp:param name="chartWidth" value="900" />
                    <jsp:param name="chartHeight" value="400" />
                    <jsp:param name="debugMode" value="false" />
                    <jsp:param name="registerWithJS" value="false" />
                </jsp:include>--%>
                <%
                if(totalIAmtfromImaster.get(0)!=null && !"null".equalsIgnoreCase(totalIAmtfromImaster.get(0).toString())){
                GenerateChart gc = new GenerateChart();
                BigDecimal percent = (new BigDecimal(totalIAmtfromImaster.get(0).toString()).multiply(new BigDecimal(100)));
                BigDecimal percentACC = (new BigDecimal(obj[13].toString()).multiply(new BigDecimal(100)));
                BigDecimal percentPE = (PEAmount.multiply(new BigDecimal(100)));

                BigDecimal finalPer = percent.divide(new BigDecimal(c_obj[2].toString()),2,RoundingMode.HALF_UP);
                BigDecimal finalPerACC = percentACC.divide(new BigDecimal(c_obj[2].toString()),2,RoundingMode.HALF_UP);
                BigDecimal finalPerPE = percentPE.divide(new BigDecimal(c_obj[2].toString()),2,RoundingMode.HALF_UP);                

               String pieByPEFileName = gc.genPieChartForPE(session, new PrintWriter(out),PEAmount,new BigDecimal(c_obj[2].toString()).setScale(3),finalPerPE);
               String pieByTenFileName = gc.genPieChartForTen(session, new PrintWriter(out),new BigDecimal(totalIAmtfromImaster.get(0).toString()).setScale(3, 0),new BigDecimal(c_obj[2].toString()).setScale(3),finalPer);
                String pieByAccFileName = gc.genPieChartForAcc(session, new PrintWriter(out),new BigDecimal(obj[13].toString()).setScale(3, 0),new BigDecimal(c_obj[2].toString()).setScale(3),finalPerACC);
                String pieByPE = request.getContextPath()+ "/servlet/DisplayChart?filename=" + pieByPEFileName;
                String pieByTen = request.getContextPath()+ "/servlet/DisplayChart?filename=" + pieByTenFileName;
                String pieByAcc = request.getContextPath()+ "/servlet/DisplayChart?filename=" + pieByAccFileName;
%>
<br />
<br />
<br />
<table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
    <tr>
        <td>
             <center><img src="<%=pieByTen%>" border="0" usemap="#<%= pieByTenFileName %>" /></center>
            
        </td>
        <td>
           <center><img src="<%=pieByPE%>" border="0" usemap="#<%= pieByPEFileName %>" /></center>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <center><img src="<%=pieByAcc%>" border="0" usemap="#<%= pieByAccFileName %>" /></center>
        </td>

    </tr>
</table><%}%>
                </div>
            </div>
            </div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
   <script type="text/javascript">
//    $(document).ready(function() {
//        $("#print").click(function() {
//            printElem({ leaveOpen: true, printMode: 'popup' });
//        });
//
//    });
//    function printElem(options){
//        $('#print_area').printElement(options);
//    }
</script>

</html>
