<%--
    Document   : EvalCertiService
    Created on : Mar 8, 2011, 4:59:28 PM
    Author     : Administrator
This page is used when marks are given to criteria..update and insert
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalServiceForms"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalServiceCriteria"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<jsp:useBean id="evalSerCertiSrBean" class="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean" scope="request" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="texxt/html; charset=UTF-8">
        <title>Evaluation Passing Points</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
var total = 0;
var chk = true;
//Fuction for calculating marks when user added the mark in mark text box.
function totalMark(mrk){
    $('span#totalMark').html('0');
    total = 0;
    var check = true;
    counter = document.getElementById("counter").value;
    //alert(counter+'count');
        for(i=1;i<=counter;i++){
            //$('span.#totalMark').html("");
            var totCount = "totalCount_"+i;
            //alert(totCount+'---Total Count');
            //alert(totCount);
            if(document.getElementById(totCount)!=null){
                totCount = document.getElementById(totCount).value;
                //alert(totCount+'-Value total count for each subCriteria');
                // alert(totCount+'totalCount Value');
                for(j=0;j<=totCount;j++){
                    var eleName = 'subCriMark_'+j+'_'+i;
                    var elename = 'subCriMark_'+j+'_'+i;
                    if(document.getElementById(eleName)!=null){
                        eleName = document.getElementById(eleName).value;
                        //  alert(eleName+'Value');
                        if($.trim(eleName)!=''){
                        if(digits($.trim(eleName))){
                            total =parseInt(total)+parseInt($.trim(eleName));
                            //alert(total);
                            $('span#totalMark').html(total);
                            //$('span.#totalMark').show();
                            if($('#isREOI').val()=='false'){
                                $('#totalMarkTr').show();
                            }
                            chk=true;
                        }
                        else{
                            document.getElementById(elename).value = '';
                           jAlert("Please enter positive numeral (0-9) only","Evaluation Criteria Alert", function(RetVal) {
                                });
                            return false;
                            chk = false;
                        }
                    }
                    }
                }
            }
        }
}
function validateTotal(){
    //Validation is done here.
    total = eval($('span#totalMark').html());
    if($('span.#totalMark').html().length==0){
        jAlert("Please Select Criteria.","Evaluation Criteria Alert", function(RetVal) {
        });
            return false;
      }
      var counter = document.getElementById("counter").value;
      //alert(counter);
      for(var d = 1;d<=counter;d++){
          var eleName = 'cmbCriteria_'+d;
         // alert(eleName+'element Name');
         
          eleName = document.getElementById(eleName).value;
         // alert(eleName+'element Value');
         // alert($(eleName).val()+' value of Criteria');
          if($.trim(eleName).length==0){
            jAlert("Please Select Criteria for Each Form.","Evaluation Criteria Alert", function(RetVal) {
            });
                return false;
          }
      }
      if($('#isREOI').val()=='false'){
         if(chk==false){
            jAlert("Points should be greater than or equal to 0.","Evaluation Criteria Alert", function(RetVal) {
            });
            return false;
          }
        if(total >100){
           jAlert("Total sum should not be greater than 100.","Evaluation Criteria Alert", function(RetVal) {
            });
            return false;
        } else if(total <100){
            jAlert("Total sum should not be lesser than 100.","Evaluation Criteria Alert", function(RetVal) {
            });
            return false;
          }
      }
          var formCnt = $('#counter').val();
          var formAll = 0;
          var disVar = 0;
          var errorText = 'Select SubCriteria';
          for(var i=1 ; i<=formCnt;i++){
              disVar = 0;
              var totCount = "totalCount_"+i;
              
            //alert(totCount+'---Total Count');
            if(document.getElementById(totCount)!=null){
                totCount = document.getElementById(totCount).value;
                //alert(totCount+'-Value total count for each subCriteria');
                // alert(totCount+'totalCount Value');
                for(j=0;j<=totCount;j++){
                    var eleName = 'subCriMark_'+j+'_'+i;
                    var elename = 'subCriMark_'+j+'_'+i;
                    if(document.getElementById(eleName)!=null){
                        eleName = document.getElementById(eleName).value;
                        if($('#isREOI').val()=='false'){
                            errorText = 'Enter Points';
                            if($.trim(eleName)==''){
                                disVar++;
                            }
                        }else{
                            //alert($('#'+elename).attr('checked')==false);
                            if($('#'+elename).attr('checked')==false){
                                //alert('herer');
                                disVar++;
                            }
                        }
                    }
                }
                //alert(disVar+'disVar');
                //alert(disVar+'---disVar--'+totCount+'---TotalCount');
                if(disVar==totCount){
                    formAll++;
                }
            }
            if(formAll!=0){
                jAlert("Please "+errorText+".","Evaluation Criteria Alert", function(RetVal) {
            });
            return false;
            }
          }
              /*$(":input[type='text']").each(function(){
                  if($(this).attr('id').split("_")[2]==i && $(this).val()!=""){
                      disVar = parseInt(disVar) + parseInt($(this).val());
                  }
              }); 
                  if(disVar==0){
                          formAll++;                      
                  }
              }*/
              //alert(formAll++);
              
          $(":input[type='text']").each(function(){
              if(formAll==0){
                  if($.trim($(this).val()).length==0){
                    $(this).attr('disabled',true);
                  }
              }              
        });        
        
}

//This function is check wheter input is text or digits
function digits(value) {
    //return /^\d+$/.test(value);
   // return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
   return /^\d+$/.test(value);
}
//For reseting a form.
function Reset(){
    $(":input:text").attr('value','0');
    $('span.#totalMark').html("0");
    return false;
}

function ForReoi(reoiVal){
    if(reoiVal){
         $('#totalMarkTr').hide();   
         $('span.#totalMark').html("100");
    }
}
    //This is for ajax call when in combo the values are changed
function setSubCriteria(name){        
    var id= name.id;
    var count = id.substring(id.indexOf('_')+1, id.length);
    //alert(count+'count');
    var objectId = $('#cmbCriteria_'+count).val();
    //alert(objectId+'objectId');
    var eleName = "cmbCriteria_"+count;
    //alert(eleName+'eleName');
    var criName = document.getElementById(eleName).options[document.getElementById(eleName).selectedIndex].text;
    //alert(criName+'criName');
    $.post("<%=request.getContextPath()%>/ServletEvalCertiService", {objectId:objectId,count:count,funName:'criteriaCombo',eleName:criName,isREOI:$('#isREOI').val()}, function(j){
            //alert(j.toString());
        $("#subCriteriaTd_"+count).html(j);
        $('#subCriteriaTr_'+count).show();
        if(j.toString().indexOf('</table>', 0)!=-1){
            totalMark(name);
        }
    });
}
</script>
    </head>
    <%
                String userId = "";
                boolean isREOI = false;
                String isCorrigen = "No";
                 
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                //boolean isSubDt = false;
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                if (request.getParameter("iscorri") != null && "true".equalsIgnoreCase(request.getParameter("iscorri"))) {
                    isCorrigen = "yes";
                }
                CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                if(commonService.getEventType(tenderId).toString().equals("REOI")){
                    isREOI = true;
                }
                //isREOI=false;
    %>
    <body onload="ForReoi(<%=isREOI%>);">
        <div class="dashboard_div">

            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
                <form id="frmEvalClari" name="frmEvalClari" action="<%=request.getContextPath()%>/ServletEvalCertiService" method="post" >
                 <%--<form id="frmEvalClari" name="frmEvalClari" method="post" >--%>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->

                <%
                    List<Object[]> mainCriteriaList = evalSerCertiSrBean.getMainCirteriaList();
                    List<Object []> criteriaList=evalSerCertiSrBean.getList();
                    List<Object []> subCriteriaList = null;
                    //* updateList of object obj[0] subCriteria,[1] maxMarks, [2] formId, [3] subCriteriaId, [4]MainCriteria
                    List<Object []> updateList=evalSerCertiSrBean.viewEvalSerMark(Integer.parseInt(tenderId),isCorrigen);
                    String action = "update";
                    /*for(Object[] obj : mainCriteriaList){
                        System.out.println("List of MainCriteria---->"+obj[1]);
                    }*/
                    //Get the List of records for updating a page.
                    
                    if(updateList.isEmpty()){/*If size is zero then it is a for inserting records*/
                        action = "add";
                    }else{
                            subCriteriaList = evalSerCertiSrBean.getSubCriteria();
                         }
                           // Variable tenderId is defined by u on ur current page.
                            String msg = "";
                            msg = request.getParameter("msg");
                %>
                <div class="contentArea_1">
                    <div class="pageHead_1">Evaluation Passing Points
                    <span style="float:right;">
                        <a href="TenderDocPrep.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go back to Dashboard</a>
                    </span>
                    </div>
                    <% if (msg != null && msg.contains("false")) {%>
                    <div class="responseMsg errorMsg t_space">Operation Failed, Please try again later.</div>
                    <%  }%>
                    <%
                                pageContext.setAttribute("tenderId", tenderId);
                                pageContext.setAttribute("tab", "2");
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%@include file="officerTabPanel.jsp" %>
                    <div class="tabPanelArea_1">
                        <div class="t-align-left">
                        <b><font color="red">If an evaluation criteria is not applicable, please select "Other"<%if(!isREOI){%> and enter 0 (zero) in max points field<%}%>.</font></b>
                        </div>
                        </BR>
                        <%
                                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="4%" class="t-align-center ff">Sl.<br/> No.</th>
                                <th width="54%" class="t-align-left ff">Form Name</th>
                                <th width="42%" class="t-align-left ff">Criteria</th>
                                <%--<th width="10%" class="t-align-left">Max Score</th>--%>
                            </tr>
                            <%
                                                                        int l = 0;
                                                                        Map<Integer,String> mainCrit = new HashMap<Integer, String>();
                                                                        /*Itreate over page.....For form detials*/
                                                                        for (SPCommonSearchData formdetails : commonSearchService.searchData("EvalSerFormsList", tenderId, userId, isCorrigen, null, null, null, null, null, null)) {
                                                                            l++;
                            %>
                            <%if(action.equalsIgnoreCase("update")){/*Conditin for Updating records*/
                                
                                /*This loop is for add distnct record formId wise main cirteria list to the map
                                */
                                if(l==1){
                                    for(int d = 0;d<updateList.size();d++){
                                        mainCrit.put(Integer.parseInt(updateList.get(d)[2].toString()), updateList.get(d)[4].toString());
                                    }/*Loop ends of itreating updatelist and put it in map which now contains only unique mainCriteria as per formId*/
                               }
                            %>
                            <tr>
                                <td class="t-align-center ff"><%=l%></td>
                                <td class="t-align-left ff"><%=formdetails.getFieldName2()%> 
                                    <input type="hidden" name="fromId_<%=l%>" value="<%=formdetails.getFieldName1()%>"/>
                                    <input type="hidden" name="pkgLotId" value="<%=formdetails.getFieldName4()%>"/>
                                    <input type="hidden" name="isCorri" value="<%=isCorrigen%>"/>
                                </td>
                                <%if(mainCrit.get(Integer.parseInt(formdetails.getFieldName1()))!=null){%>
                                <td>
                                    <select class="formTxtBox_1" id="cmbCriteria_<%=l%>" name="criteria_<%=l%>" style="width: 200px" onchange="setSubCriteria(this);">
                                        <option value="" >-- Select Criteria--</option>
                                        <%
                                                for(int d = 0; d<mainCriteriaList.size();d++){/*Itreating loop if mainCriteria*/
                                                   %>
                                                   <option value="<%=mainCriteriaList.get(d)[0]%>" <%if(mainCrit.get(Integer.parseInt(formdetails.getFieldName1())).trim().equalsIgnoreCase(mainCriteriaList.get(d)[1].toString().trim())){out.print("selected");}%>><%=mainCriteriaList.get(d)[1]%></option>
                                                    <%
                                                }/*For Loop Ends here*/
                                        %>
                                    </select>
                                </td>
                            </tr>
                            
                            <tr id="subCriteriaTr_<%=l%>">
                                <td colspan="3" id="subCriteriaTd_<%=l%>">
                                   
                                             <table width='100%' cellspacing='0' class='tableList_1'>
                                                 <tr>
                                                     <th width='80%' class='t-align-left ff'>Sub Criteria</th>
                                                     <%if(isREOI){%>
                                                     <th width='20%' class='t-align-left ff'>Select</th>
                                                     <%}else{%>
                                                     <th width='20%' class='t-align-left ff'>Max Points</th>
                                                     <%}%>
                                                 </tr>
                                                 <%
                                                 boolean isFistTime = true;
                                                 
                                                 int criCount = 0;
                                                 int cntp = 0;
                                                 for(int k=0;k<criteriaList.size();k++){
                                                   if(mainCrit.get(Integer.parseInt(formdetails.getFieldName1())).trim().equalsIgnoreCase(criteriaList.get(k)[2].toString().trim())){
                                                    //if(isFistTime){
                                                       criCount++;
                                                  %>
                                                
                                                <tr>
                                                     <td class='t-align-left ff'>
                                                         <%out.print(criteriaList.get(k)[1]);%>
                                                         <input type = 'hidden' value='<%=criteriaList.get(k)[0]%>' name='hdnSubCri_<%=l%>'/>
                                                         
                                                         
                                                     </td>
                                                     <td class='t-align-left ff' id="markTd_<%=criCount%>_<%=l%>" mytd="loop">
                                                 <%
                                                     for(int c=0;c<updateList.size();c++){/*This loop is for intreating the update list for and subCriteria mark */
                                                        if(updateList.get(c)[2].toString().equals(formdetails.getFieldName1().toString())){/*This conditon is form unique form id*/
                                                            if(updateList.get(c)[3].toString().trim().equals(criteriaList.get(k)[0].toString().trim()) && mainCrit.get(Integer.parseInt(formdetails.getFieldName1())).trim().equalsIgnoreCase(criteriaList.get(k)[2].toString().trim())){
                                                                if(updateList.get(c)[1]!=null){
                                                     if(isREOI){%>
                                                     <input type='checkbox' name='subCriMark_<%=l%>' id='subCriMark_<%=criCount%>_<%=l%>' style='width: 30px' checked value="1" /><br />
                                                         <%}else{%>
                                                                 <input type='text' name='subCriMark_<%=l%>' id='subCriMark_<%=criCount%>_<%=l%>' style='width: 30px' value="<%=updateList.get(c)[1]%>" onblur='return totalMark(this);'/><br />
                                                             <%}%>
                                                             <%}/*If condition Ends when mark is null*/%>
                                                             <%
                                                        }/*Update List and subcriteria List id match*/
                                                     }%>
                                                 <%}%>
                                                             </td>
                                                 
                                                     
                                                 </tr>
                                                 
                                                 
                                                 <%//}%>
                                                 <%}/*If condition ends here*/
                                                 }/*For loop of SubCriteria Ends here*//*For loop ends here*/%>
                                                 <input type = 'hidden' value='<%=criCount%>' id='totalCount_<%=l%>'/>
                                             </table>
                                </td>
                            </tr>
                            <%/*If condition if form is added letter*/}else{%>
                                <td>
                                    <select class="formTxtBox_1" id="cmbCriteria_<%=l%>" name="criteria_<%=l%>" style="width: 200px" onchange="setSubCriteria(this);">
                                        <option value="" selected="selected">-- Select Criteria--</option>
                                                    <%
                                                        /*For loop for criterai List*/
                                            for(int k=0;k<mainCriteriaList.size();k++){
                                                    %>
                                        <option value="<%=mainCriteriaList.get(k)[0]%>" ><%=mainCriteriaList.get(k)[1]%></option>
                                                         <%}/*For loop ends here*/%>
                                     </select>
                                </td>
                            </tr>
                            <tr id="subCriteriaTr_<%=l%>" style="display: none;">
                                <td colspan="3" id="subCriteriaTd_<%=l%>">

                                </td>
                            </tr>
                            <%}%>
                             
                            <%}/*Conditon of Updating is ends here*/else{/*Inserting record starts here*/%>
                            <tr>
                                <td class="t-align-left ff"><%=l%></td>
                                <td class="t-align-left ff"><%=formdetails.getFieldName2()%>
                                    <input type="hidden" name="fromId_<%=l%>" value="<%=formdetails.getFieldName1()%>"/>
                                    <input type="hidden" name="pkgLotId" value="<%=formdetails.getFieldName4()%>"/>
                                    <input type="hidden" name="isCorri" value="<%=isCorrigen%>"/>
                                </td>

                                <td>
                                    <select class="formTxtBox_1" id="cmbCriteria_<%=l%>" name="criteria_<%=l%>" style="width: 200px" onchange="setSubCriteria(this);">
                                                <option value="" selected="selected">-- Select Criteria--</option>
                                    <%
                                        /*For loop for criterai List*/
                                        for(int k=0;k<mainCriteriaList.size();k++){
                                    %>
                                    <option value="<%=mainCriteriaList.get(k)[0]%>" ><%=mainCriteriaList.get(k)[1]%></option>
                                         <%}/*For loop ends here*/%>
                                         </select>
                                </td>
                               <%-- <td class="t-align-center">
                                    <input type="text" name="fromlist_<%=l%>" id="fromlist<%=l%>" style="width: 25px; display: none;" />
                                    <input type="hidden" name="hdnfromlist<%=l%>" id="lotfromlist<%=l%>" value="<%=formdetails.getFieldName4()%>" />
                                </td>--%>
                            </tr>
                            <tr id="subCriteriaTr_<%=l%>" style="display: none;">
                                <td colspan="3" id="subCriteriaTd_<%=l%>">

                                </td>
                            </tr>
                            <% }//Upate ends here
                                                                        }/*Form Loop Ends here*/%>
                              
                            <%
                                if(action.equalsIgnoreCase("update")){/*This tr show last totalMark when it is in update*/%>
                                <tr id="totalMarkTr" <%=isREOI ? "style='display: none;'" : ""%>>
                                <td colspan="2" class="t-align-left ff">
                                    Total Points
                                </td>
                                <td class="t-align-center" >
                                     <span id="totalMark" style="color: red; padding-left: 75px;">100</span>
                                 </td>
                             </tr>
                             <%}else{/*When in insert*/%>
                             <tr id="totalMarkTr" style="display: none;">
                                <td colspan="2" class="t-align-left ff">
                                    Total Points
                                </td>
                                <td class="t-align-center" >
                                 <span id="totalMark" style="color: red;"></span>
                                </td>
                             </tr>
                             <%}%>
                        </table>
                        <div class="t-align-center t_space">
                            <input type="hidden" name="funName" value="<%=action%>" />
                            <input type="hidden" name="getcorri" value="<%=isCorrigen%>" />
                            <input type="hidden" name="tenderId" value="<%=tenderId%>" />
                            <input type="hidden" id ="isREOI"name="isREOI" value="<%=isREOI%>" />
                            <label class="formBtn_1">
                                <input name="btnSendfrom" id="btnSendfrom" type="submit" value="Submit" onclick="return validateTotal();"/>
                            </label>
                            <%--<label class="formBtn_1">
                                <input name="btnSendreset" type="button" value="Reset" />
                            </label>--%>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                    <!--Dashboard Content Part End-->
                </div>
              <input type="hidden" name="counter" id="counter" value="<%=l%>"/>
            </form>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
     <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }                        
       
        </script>
    <%if(action.equalsIgnoreCase("update")){%>
    <script type="text/javascript" language="Javascript">
         $("td[mytd='loop']").each(function(){
                var id1 = $(this).attr('id').split("_")[1];
                var id2 = $(this).attr('id').split("_")[2];                
                if($(this).html().toString().indexOf('subCriMark_', 0)==-1){
                   <%if(isREOI){%>
                        $(this).html("<input style='width: 30px' type='checkbox' value='1' id='subCriMark_"+id1+"_"+id2+"' name='subCriMark_"+id2+"' />");
                    <%}else{%>
                        $(this).html("<input type='text' onblur='return totalMark(this);' value='' style='width: 30px' id='subCriMark_"+id1+"_"+id2+"' name='subCriMark_"+id2+"' />");
                    <%}%>
                }
            });
        totalMark('0');
    </script>
    <%}%>
</html>
<%
    /*Making the object and list null*/
    if(criteriaList!=null){
            criteriaList.clear();
            criteriaList = null;
        }
    if(mainCriteriaList!=null){
            mainCriteriaList.clear();
            mainCriteriaList = null;
        }
    if(mainCrit!=null){
            mainCrit.clear();
            criteriaList = null;
        }                                                                    
    if(updateList!=null){
        updateList.clear();
        updateList = null;
        }
    if(subCriteriaList!=null){
            subCriteriaList.clear();
            subCriteriaList = null;
        }
%>
