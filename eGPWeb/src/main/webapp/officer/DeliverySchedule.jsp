<%-- 
    Document   : DeliverySchedule
    Created on : Jul 30, 2011, 11:22:36 AM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.WorkFlowServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsVariationOrder"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<jsp:useBean id="appSrBean" class="com.cptu.egp.eps.web.servicebean.APPSrBean" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
 CommonService commonService1 = (CommonService) AppContext.getSpringBean("CommonService");
 WorkFlowServiceImpl workFlowServiceImpl = (WorkFlowServiceImpl) AppContext.getSpringBean("WorkFlowService");
 commonService1.setUserId(session.getAttribute("userId").toString());
        String procnature1 = commonService1.getProcNature(request.getParameter("tenderId")).toString();
        String procType = commonService1.getProcurementType(request.getParameter("tenderId")).toString();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <%if("goods".equalsIgnoreCase(procnature1)){%>
        <title>Delivery Schedule</title>
         <%}else{%>
           <title>Work Program</title>
             <%}%>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>


    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            
            <div class="pageHead_1">
                <%if("goods".equalsIgnoreCase(procnature1)){%>
                Delivery Schedule
                <%}else{%>
                Work Program
                <%}%>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>            
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%                
                List<Object[]> lotObj = commonService1.getLotDetails(request.getParameter("tenderId"));
                Object[] objj = null;
                if(lotObj!=null && !lotObj.isEmpty())
                {
                    objj = lotObj.get(0);
                    pageContext.setAttribute("lotId", objj[0].toString());
                }
                if("services".equalsIgnoreCase(procnature1)||"works".equalsIgnoreCase(procnature1)){
            %>
            <%@include file="../resources/common/ContractInfoBar.jsp" %>
            <%}%>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "14");
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");
                        String userId = "";
                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                            userId = session.getAttribute("userId").toString();
                            appSrBean.setLogUserId(userId);
                        }
            %>
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "1");

                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1">
                    <% if (request.getParameter("msg") != null) {
                                    if ("edit".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.dates.edit")%> </div>
                    <%}
                         if ("sent".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.works.sentToTen")%></div>
                    <%}

                         if ("noedit".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.var.success")%></div>
                    <%}
                         if ("changed".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg errorMsg'><%=bdl.getString("CMS.var.Nochange")%></div>
                    <%}
                         if ("editt".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'>Variation Order edited successfully</div>
                    <%}

                                }
                    %>
                    <div align="center">

                        <%
                                    String tenderId = request.getParameter("tenderId");
                                    boolean flag = true;
                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    commonSearchDataMoreService.setLogUserId(session.getAttribute("userId").toString());
                                    List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    service.setLogUserId(session.getAttribute("userId").toString());
                                    int i = 0;
                                    int lotId=0;
                                    boolean flags = false;
                                    CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                    ccds.setLogUserId(session.getAttribute("userId").toString());
                                    boolean isWCC=false;
                                    boolean isConTer=false;
                                    for (SPCommonSearchDataMore lotList : packageLotList) {

                                        flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));
                                       // List<Object> wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                        /*Dohatec Start*/
                                        List<Object> wpid;
                                        if("ICT".equalsIgnoreCase(procType) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                        {
                                            wpid = service.getWpIdForTenderForms(Integer.parseInt(lotList.getFieldName5()),tenderId);
                                        }
                                        else
                                        {
                                            wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                        }
                                        /*Dohatec End*/
                                        lotId = Integer.parseInt(lotList.getFieldName5());
                                        isWCC = service.isWorkCompleteOrNot(lotList.getFieldName5());
                                        isConTer = service.isContractTerminatedOrNot(lotList.getFieldName5());

                        %>

                        <form name="frmcons" method ="post">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td><%=bdl.getString("CMS.lotdes")%></td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>

                                <%
                                                                        int count = 1;
                                                                        if (!wpid.isEmpty() && wpid != null) {%>
                                <tr><td colspan="2">
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                
                                                <th width="8%" class="t-align-center"><%if (procnature.equalsIgnoreCase("goods")) {%>
                                                    <%=bdl.getString("CMS.Goods.Forms")%>
                                                    <%} else {%>
                                                    <%=bdl.getString("CMS.works.Forms")%>
                                                    <%}%>
                                                </th><%if ( procnature.equalsIgnoreCase("works")) {%>
                                                <th width="12%" class="t-align-center">
                                                    Work Program submission date
                                                    </th><%}%>
                                                <th width="10%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>
                                                <%if (procnature.equalsIgnoreCase("works")) {%>
                                                <th width="10%" class="t-align-center"><%=bdl.getString("CMS.works.variation")%></th>
                                                <%}%>
                                            </tr>
                                            <%for (Object obj : wpid) {
                                                boolean checkitemreceived = service.checkForItemFullyReceivedOrNotByWpId(Integer.parseInt(obj.toString()));
                                                    String toDisplay = service.getConsolidate(obj.toString());
                                                    boolean isallowvariation = service.allowVariationOrderOrNot(Integer.parseInt(obj.toString()));
                                                    List<TblCmsVariationOrder> VOrderList = service.getVariationOrderList(Integer.parseInt(obj.toString()));
                                                    List<Object[]> MAXVOrderList = service.getMaxVariOrderList(Integer.parseInt(obj.toString()));
                                                    List<TblCmsVariationOrder> isvariorder = service.getListOfVariationOrder(Integer.parseInt(obj.toString()));
                                                    boolean forWorksdateEdit = service.isDateEditedInWorksTenSide(obj.toString(), "sendtope");
                                                    List<TblCmsVariationOrder> maxId = service.getMaxVarOrdId(Integer.parseInt(obj.toString()));
                                                    int mxid=0;
                                                    if(!maxId.isEmpty()){
                                                            mxid=maxId.get(maxId.size()-1).getVariOrdId();
                                                        }
                                                    Object date = service.getCreatedDate(obj.toString());

                                            %>


                                            <tr>
                                                <td style="text-align: center;"><%=count%></td>
                                                <td><%if (procnature.equalsIgnoreCase("goods")) {%>
                                                    <%=toDisplay.replace("Consolidate", "Delivery Schedule")%>
                                                    <%} else {%>
                                                    <%=toDisplay.replace("Consolidate", "BoQ")%>
                                                    <%}%>
                                                </td>
                                                <%if (procnature.equalsIgnoreCase("works")) {%><td>
                                                    <%if(forWorksdateEdit){
                                                        %>
                                                        <%= DateUtils.gridDateToStrWithoutSec((Date) date)%>
                                                    <%} else {%>
                                                     <%=bdl.getString("CMS.works.edidatesByTenderer")%>
                                                    <%}}%>
                                                </td>
                                                <td>
                                                    <%if (procnature.equalsIgnoreCase("goods")) {%>
                                                    <a href="#" onclick="view(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View</a>
                                                    <%}%>
                                                    <%if (forWorksdateEdit) {%>
                                                    <a href="#" onclick="view(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View</a>





                                                   

<%if (flags) {
if(!checkitemreceived){%>
                                             <%if (isWCC) {%>
                                        &nbsp;|&nbsp;<a href="#" onclick="jAlert('<%=bdl.getString("CMS.workcomplete")%>','Progress Report', function(RetVal) {
                                                    });">Edit Dates</a>
                                        <%} else if (isConTer) {%>
                                         &nbsp;|&nbsp;<a href="#" onclick="jAlert('<%=bdl.getString("CMS.works.contractTerminate")%>','Delivery Schedule', function(RetVal) {
                                                });">Edit Dates</a>
                                        <%}else{%>
                                         &nbsp;|&nbsp;<a href="#" onclick="Edit(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Edit Dates</a>
                                        <%}}%>



                                              
                                                    <%  
                                                      } else {%>
                                                    &nbsp;|&nbsp;<a href="#" onclick="jAlert('<%=bdl.getString("CMS.DateConfigPending")%>','Delivery Schedule', function(RetVal) {
                                            });">Edit Dates</a>
                                                    <%}%>
                                                    &nbsp;|&nbsp;<a href="ViewDSHistoryLinks.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>">View History</a>
                                                    &nbsp;|&nbsp;<a href="DeliveryScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>&keyId=<%=obj.toString()%>&docx=Workprogram&module=wp">Upload / Download Document</a>
                                                    <%} else {
                                                        if (procnature.equalsIgnoreCase("goods")) {
                                                            if (flags) {%>


                                                    <%if(!checkitemreceived){%>
                                             <%if (isWCC) {%>
                                       &nbsp;|&nbsp; <a href="#" onclick="jAlert('<%=bdl.getString("CMS.workcomplete")%>','Progress Report', function(RetVal) {
                                        });">Edit Days</a>
                                        <%} else if (isConTer) {%>
                                          &nbsp;|&nbsp; <a href="#" onclick="jAlert('<%=bdl.getString("CMS.contractTerminate")%>','Delivery Schedule', function(RetVal) {
                                    });">Edit Days</a>
                                        <%}else{%>
                                         &nbsp;|&nbsp;<a href="#" onclick="Edit(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Edit Days</a>
                                        <%}}%>

                                                                                             <%
                                                                                                                } else {%>
                                                    &nbsp;|&nbsp;<a href="#" onclick="jAlert('<%=bdl.getString("CMS.DateConfigPending")%>','Delivery Schedule', function(RetVal) {
                                });">Edit Days</a>
                                                    <%}%>
                                                    &nbsp;|&nbsp;<a href="ViewDSHistoryLinks.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>">View History</a>
                                                    &nbsp;|&nbsp;<a href="DeliveryScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>&keyId=<%=obj.toString()%>&docx=Delivery&module=ds">Upload / Download Document</a>
                                                    <%                                                        } else {%>
                                                    <%=bdl.getString("CMS.works.edidatesByTenderer")%>
                                                    <%}%>
                                                </td>
                                               

                                            <%  }%>
                                             <%if (procnature.equalsIgnoreCase("works")) {%>
                                                <td>
                                                     <%if (forWorksdateEdit) {
                                                    if (isallowvariation){
                                                      if (isWCC) {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.variation")%>','Variation Order', function(RetVal) {
                                    });">Variation Order</a>&nbsp;|&nbsp;
                                        <%} else if (isConTer) {%>
                                       <a href="#" onclick="jAlert('<%=bdl.getString("CMS.contractTerminatevari")%>','Variation Order', function(RetVal) {
                                    });">Variation Order</a>&nbsp;|&nbsp;
                                        <%}else{%>
                                         <a href="#" onclick="vari(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Variation Order</a>&nbsp;|&nbsp;
                                        <%}%>
                                                    <% } else {%>
                                                    <%
                                                        List<Object> getFileOnSt = new ArrayList<Object>();
                                                        String donor = "";
                                                        short activityid = 11;
                                                        int initiator = 0;
                                                        int approver = 0;
                                                        String WorkFlowStatus ="";
                                                        String VariOrderStatus ="";
                                                        int VariOId =0;
                                                        boolean iswfLevelExist = false;
                                                        if(!VOrderList.isEmpty()){
                                                            WorkFlowStatus = VOrderList.get(0).getVariOrdWFStatus();
                                                            VariOrderStatus = VOrderList.get(0).getVariOrdStatus();
                                                            VariOId = VOrderList.get(0).getVariOrdId();
                                                        }else{
                                                            if(!MAXVOrderList.isEmpty())
                                                            {
                                                                Object[] VOData = MAXVOrderList.get(0);
                                                                 VariOId = Integer.parseInt(VOData[0].toString());
                                                                 VariOrderStatus = VOData[1].toString();
                                                                 if(VOData[2]!=null)
                                                                 {
                                                                    WorkFlowStatus = VOData[2].toString();
                                                                 }
                                                            }
                                                        }                                                            
                                                            WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                                                            List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(VariOId, VariOId, activityid);
                                                            if (tblWorkFlowLevelConfig.size() > 0) {
                                                            Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                                            iswfLevelExist = true;
                                                            while (twflc.hasNext()) {
                                                                 TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                                                 TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                                                 if (workFlowlel.getWfRoleId() == 1) {
                                                                 initiator = lmaster.getUserId();
                                                                 }
                                                                 if (workFlowlel.getWfRoleId() == 2) {
                                                                     approver = lmaster.getUserId();
                                                                 }
                                                                 }}
                                                            getFileOnSt = appSrBean.getFileOnHandStatus(VariOId,VariOId,Integer.parseInt(userId),10);     
                                                    %>
                                                            <%--<a href="#" onclick="editvo(<%=mxid%>,<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Edit Variation Order</a>
                                                            &nbsp;|&nbsp;<a href="#" onclick="createwf(<%=mxid%>,<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Send to Contractor</a>--%>
                                                    <%
                                                            if("".equalsIgnoreCase(WorkFlowStatus) || WorkFlowStatus==null)
                                                            {
                                                                
                                                                donor = workFlowSrBean.isDonorReq("10",VariOId);
                                                                String[] norevs = donor.split("_");
                                                                String noOfReviewers = norevs[1];
                                                                if(!"0".equalsIgnoreCase(noOfReviewers))
                                                                {
                                                        %>
                                                                    <%--<a href="#" onclick="editvo(<%=mxid%>,<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Edit Variation Order</a>
                                                                    &nbsp;|&nbsp;<a href="#" onclick="createwf(<%=mxid%>,<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Send to Contractor</a>--%>
                                                        <%
                                                                }
                                                                if("0".equalsIgnoreCase(noOfReviewers))
                                                                {
                                                                    if(initiator!=approver)
                                                                    {
                                                                        if(tblWorkFlowLevelConfig.size()>0)
                                                                        {
                                                                            if(!"Yes".equalsIgnoreCase(getFileOnSt.get(0).toString())){
                                                       %>
                                                                                <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&action=View&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>">View</a> &nbsp;|&nbsp;
                                                                                <a href="workFlowHistory.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&fraction=Variation">View Workflow History</a>&nbsp;|&nbsp;
                                                       <%                         
                                                                            }else{
                                                       %>
                                                                                <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Edit&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>">Edit Workflow</a>&nbsp;|&nbsp;
                                                                                <a href="FileProcessing.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&fromaction=Variation">Process file in Workflow</a>&nbsp;|&nbsp;
                                                       <%                         
                                                                            }    
                                                                        }else{
                                                        %>
                                                                            <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Edit&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>">Edit Workflow</a>
                                                        <%                    
                                                                        }
                                                                    }else{
                                                                        
                                                                          if(tblWorkFlowLevelConfig.size()>0)
                                                                          {
                                                                            if("pending".equalsIgnoreCase(VariOrderStatus))
                                                                            {
                                                                %>
                                                                                <a href="CreateWf.jsp?varId=<%=VariOId%>&wpId=<%=obj.toString()%>&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&pe=yes">Issue to Contractor</a>&nbsp;|&nbsp;
                                                                <%
                                                                            }
                                                                            else
                                                                            {
                                                                                if(initiator!=approver)
                                                                                {
                                                                %>
                                                                                    &nbsp;|&nbsp;<a href="workFlowHistory.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&fraction=Variation">View Workflow History</a>&nbsp;|&nbsp;
                                                                <%
                                                                                }
                                                                %>
                                                                                <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&action=View&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>">View</a> &nbsp;|&nbsp;
                                                                <%
                                                                            }
                                                                          }else{
                                                                %>
                                                                            <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Edit&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&parentLink=960">Edit Workflow</a>
                                                                <%
                                                                          }
                                                                    }   
                                                                }else
                                                                {
                                                                    if(tblWorkFlowLevelConfig.size()>0)
                                                                    {
                                                                        if(!"approved".equalsIgnoreCase(VariOrderStatus))
                                                                        {
                                                        %>
                                                                            &nbsp;|&nbsp;<a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Edit&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>">Edit Workflow</a>
                                                        <%
                                                                        }                                                                        
                                                                        if(!"Yes".equalsIgnoreCase(getFileOnSt.get(0).toString()))
                                                                        {
                                                        %>
                                                                            &nbsp;|&nbsp;<a href="workFlowHistory.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&fraction=Variation">View Workflow History</a>
                                                        <%
                                                                        }
                                                                        else
                                                                        {
                                                        %>
                                                                            &nbsp;|&nbsp;<a href="FileProcessing.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&fromaction=Variation">Process file in Workflow</a>
                                                        <%
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if(noOfReviewers==null || noOfReviewers=="" || "".equalsIgnoreCase(noOfReviewers) || "null".equalsIgnoreCase(noOfReviewers))
                                                                        {
                                                                            if(workFlowServiceImpl.getWorkFlowRuleEngineData(10)){
                                                        %>
                                                                                <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Create&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&parentLink=959">Create Workflow</a>&nbsp;|&nbsp;
                                                        <%                  }else{
                                                        %>
                                                                                &nbsp;|&nbsp;<a href="#" onclick="jAlert('Workflow Configuration is pending in BusinessRule','Repeat Order', function(RetVal) {});">Create Workflow</a>
                                                        <%                        
                                                                            }
                                                                        }else{
                                                        %>                    
                                                                            <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Edit&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>">Edit Workflow</a>
                                                        <%              }    
                                                        
                                                                    }
                                                                }
                                                            }
                                                            else if("approved".equalsIgnoreCase(WorkFlowStatus))
                                                            {
                                                                if("pending".equalsIgnoreCase(VariOrderStatus))
                                                                {
                                                        %>
                                                        <a href="CreateWf.jsp?varId=<%=VariOId%>&wpId=<%=obj.toString()%>&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&pe=yes">Issue to Contractor</a>&nbsp;|&nbsp;
                                                        <%
                                                                }
                                                                else
                                                                {
                                                        %>
                                                                    &nbsp;|&nbsp;<a href="workFlowHistory.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&fraction=Variation">View Workflow History</a>
                                                                    &nbsp;|&nbsp;<a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&action=View&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>">View</a>
                                                        <%
                                                                }
                                                            }
                                                    
                                                    %>                                                   
                                                   <%--<a href="#" onclick="editvo(<%=mxid%>,<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Edit Variation Order</a>
                                                    &nbsp;|&nbsp;<a href="#" onclick="createwf(<%=mxid%>,<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Send to Contractor</a>--%>
                                                   <%}if("works".equalsIgnoreCase(procnature)){%>
    <a href="#" onclick="history(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Variation Order history </a>&nbsp;|&nbsp;
    <a href="variOrderUploadDoc.jsp?keyId=<%=obj.toString()%>&tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=obj.toString()%>">Upload / Download Document</a>    
 <%}else{%><center>-</center><%}%>
                                                    
                                                       <%}%>

                                                </td>
                                                <%}%>


                                            </tr>

                                            <%    count++;
                                            }%>

                                        </table>
                                    </td></tr>
                                    <%
                                                                            }
                                    %>

                                <%
                                            }%>

                           



                                             <%




                                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                                        List<Object[]> mainForRO = ros.getMainLoopForROPESide(lotId);
                                        if(!mainForRO.isEmpty()){
                                            int icount = 1;
                                                for(Object[] objs : mainForRO){
                                                    for (Object[] objd : issueNOASrBean.getNOAListingForRO(Integer.parseInt(tenderId),(Integer) objs[0])) {
                                                        boolean flagR = ccds.getConfigDatesdatabyPassingLotId(lotId,(Integer)objd[11]);
                                                         List<Object> wpid = ros.getWpIdForAfterRODone(Integer.parseInt(objs[1].toString()));
                                                          isWCC = service.isWorkCompleteOrNotForRO(lotId+"",(Integer)objd[11]);
                                                          isConTer = service.isContractTerminatedOrNotForRO(lotId+"",(Integer)objd[11]);

                        %>

                              <tr>
                                    <td colspan="3">
                                <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Repeat Order-<%=icount%></li>
                                </ul>

                                    </td>

                                    <%
    int calc=1;
    if (!wpid.isEmpty() && wpid != null) {%>
                                <tr><td colspan="2">
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>

                                                <th width="8%" class="t-align-center"><%if (procnature.equalsIgnoreCase("goods")) {%>
                                                    <%=bdl.getString("CMS.Goods.Forms")%>
                                                    <%} else {%>
                                                    <%=bdl.getString("CMS.works.Forms")%>
                                                    <%}%>
                                                </th>
                                                <th width="10%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>
                                                
                                            </tr>
                                            <%
                                            int show = 0;
                                            for (Object obj : wpid) {
                                                boolean checkitemreceived = service.checkForItemFullyReceivedOrNotByWpId(Integer.parseInt(obj.toString()));
                                                    List<Object[]> forms = dDocSrBean.getAllBoQFormsForRO(lotId);
                                                    List<TblCmsVariationOrder> maxId = service.getMaxVarOrdId(Integer.parseInt(obj.toString()));
                                                    int mxid=0;
                                                    if(!maxId.isEmpty()){
                                                            mxid=maxId.get(maxId.size()-1).getVariOrdId();
                                                        }

                                            %>


                                            <tr>
                                                <td style="text-align: center;"><%=calc%></td>
                                                <td>
                                                    <%="Delivery Schedule of " +forms.get(show)[1]%>
                                                   
                                                </td>
                                               
                                                <td>
                                                    <%if (procnature.equalsIgnoreCase("goods")) {%>
                                                    <a href="#" onclick="viewR(<%=obj.toString()%>,<%=tenderId%>,<%=lotId%>,<%=objd[11].toString()%>);">View</a>
                                                    <%}%>
                                                    <%if (procnature.equalsIgnoreCase("goods")) {
                                                            if (flagR) {%>
                                                    <%if(!checkitemreceived){%>
                                             <%if (isWCC) {%>
                                       &nbsp;|&nbsp; <a href="#" onclick="jAlert('<%=bdl.getString("CMS.workcomplete")%>','Progress Report', function(RetVal) {
                                        });">Edit Days</a>
                                        <%} else if (isConTer) {%>
                                          &nbsp;|&nbsp; <a href="#" onclick="jAlert('<%=bdl.getString("CMS.contractTerminate")%>','Delivery Schedule', function(RetVal) {
                                    });">Edit Days</a>
                                        <%}else{%>
                                         &nbsp;|&nbsp;<a href="#" onclick="Edit(<%=obj.toString()%>,<%=tenderId%>,<%=lotId %> );">Edit Days</a>
                                        <%}}%>
                                                             <%                                                   } else {%>
                                                    &nbsp;|&nbsp;<a href="#" onclick="jAlert('<%=bdl.getString("CMS.DateConfigPending")%>','Delivery Schedule', function(RetVal) {
                                });">Edit Days</a>
                                                    <%}%>
                                                    &nbsp;|&nbsp;<a href="ViewDSHistoryLinks.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=<%=obj.toString()%>&cntId=<%=objd[11].toString()%>">View History</a>
                                                    &nbsp;|&nbsp;<a href="DeliveryScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=<%=obj.toString()%>&keyId=<%=obj.toString()%>&docx=Delivery&module=ds">Upload / Download Document</a>
                                                    <%                                                        } else {%>
                                                    <%=bdl.getString("CMS.works.edidatesByTenderer")%>
                                                    <%}%>
                                                </td>


                                            <%  calc++;
                                                show++;
                                           }%>
                                                   </tr>

                                            <%   
                                            }%>

                                        </table>
                                    </td></tr>
                                    

                              <%}icount++;}%>
                              <%} %>

                               </table>



                                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                <tr>
                                                    <td class="t_align_left ff" width="10%">BOQ Forms</td>
                                                    <td class="t_align_left"><a target="_blank" href="ViewForms.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>">View</a></td>
                                                </tr>

                                            </table>
                        </form>

                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
    <script>
function Edit(wpId,tenderId,lotId){
              
        <%if (procnature.equalsIgnoreCase("goods")) {%>
dynamicFromSubmit("EditDatesForBoq.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms");
        <%} else {%>
dynamicFromSubmit("DateEditOnPeSide.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&pe=yes");
        <%}%>
}
function EditR(wpId,tenderId,lotId,cntId){
    dynamicFromSubmit("EditDatesForBoq.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms&cntId="+cntId);
}

function editvo(varId,wpId,tenderId,lotId){
    dynamicFromSubmit("EditVariationOrder.jsp?varId="+varId+"&wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&pe=yes");
}

function createwf(varId,wpId,tenderId,lotId){
    dynamicFromSubmit("CreateWf.jsp?varId="+varId+"&wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&pe=yes");
}

function view(wpId,tenderId,lotId){
        <%if (procnature.equalsIgnoreCase("goods")) {%>
            dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
        <%} else {%>
            dynamicFromSubmit("ViewDatesForWorks.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
        <%}%>
}

function viewR(wpId,tenderId,lotId,cntId){
    dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&cntId="+cntId);
}

function vari(wpId,tenderId,lotId){
dynamicFromSubmit("VariationOrder.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
}

function history(wpId,tenderId,lotId){
dynamicFromSubmit("VariationOrderHistory.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
}
    </script>
    <script>
var headSel_Obj = document.getElementById("headTabTender");
if(headSel_Obj != null){
headSel_Obj.setAttribute("class", "selected");
}
    </script>

</html>
