<%-- 
    Document   : LotWisePaymentView
    Created on : Dec 19, 2010, 12:47:29 PM
    Author     : rajesh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <title>Tender Opening Sheet</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>

            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Tender Opening Sheet</div>
                <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));

                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
<!--                    <tr>
                        <th>Perticular</th>
                        <th>
                            <%
                                     //CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                     //   List<SPCommonSearchData> submitDateSts = commonSearchService.searchData("LotPaymentListing", request.getParameter("tenderId"), request.getParameter("lotId"), null, null, null, null, null, null, null);
                                     //   if (!submitDateSts.isEmpty()) {
                                     //       for (SPCommonSearchData commonTenderDetails : submitDateSts) {
                            %>
                            <%//=commonTenderDetails.getFieldName1()%>
                            <%//}}%>
                        </th>
                    </tr>-->
                    <tr>
                        <td width="22%">
                            <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <th>Perticular</th>
                                </tr>
                                <tr>
                                    <td class="ff">Payment For : </td>
                                </tr>
                                <tr>
                                    <td class="ff" width="17%">Payment Instrument Type : </td>
                                </tr>
                                <tr>
                                    <td class="ff">Instrument Validity : </td>
                                </tr>
                                <tr>
                                    <td class="ff">Instrument  Reference Number : </td>
                                </tr>
                                <tr>
                                    <td class="ff">Amount :</td>
                                </tr>
                                <tr>
                                    <td class="ff">Date of instrument : </td>
                                </tr>
                                <tr>
                                    <td class="ff">Name of Bank : </td>
                                </tr>
                                <tr>
                                    <td class="ff">Name of Branch : </td>

                                </tr>
                                <tr>
                                    <td class="ff">Name of Officer : </td>

                                </tr>
                                <tr>
                                    <td class="ff">Comments : </td>

                                </tr>
                                <!--<tr>
                                    <td class="ff">
                                        Signature / Digital signature :
                                    </td>
                                 </tr>-->
                            </table>
                        </td>
                        <%
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                        List<SPCommonSearchData> submitDateSts = commonSearchService.searchData("LotPaymentListing", request.getParameter("tenderId"), request.getParameter("lotId"), null, null, null, null, null, null, null);
                                if (!submitDateSts.isEmpty()) {
                                        for (SPCommonSearchData commonTenderDetails : submitDateSts) {
                        %>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <th><%=commonTenderDetails.getFieldName1()%></th>
                                </tr>
                                <tr>
                                    <td><%=commonTenderDetails.getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td width="83%"><%=commonTenderDetails.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td><%=commonTenderDetails.getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td><%=commonTenderDetails.getFieldName5()%></td>
                                </tr>
                                <tr>
                                    <td><%=commonTenderDetails.getFieldName6()%></td>
                                </tr>
                                <tr>
                                    <td><%=commonTenderDetails.getFieldName7()%></td>
                                </tr>
                                <tr>
                                    <td><%=commonTenderDetails.getFieldName8()%></td>
                                </tr>
                                <tr>
                                    <td><%=commonTenderDetails.getFieldName9()%></td>
                                </tr>
                                <tr>
                                    <td><%=commonTenderDetails.getFieldName10()%></td>
                                </tr>
                                <tr>
                                    <td><%=commonTenderDetails.getFieldName11()%></td>
                                </tr>
                                <!--<tr>
                                        <td><%=commonTenderDetails.getFieldName12()%></td>
                                     </tr>-->
                            </table>
                        </td>
                    
                    <%}
                                }%>
                        </tr>
                </table>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->

            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
