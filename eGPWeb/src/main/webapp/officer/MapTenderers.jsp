<%--
    Document   : MapTenderers
    Created on : Dec 11, 2010, 5:32:58 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.model.table.TblAppPermission"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.LoginMasterSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean" %>
<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Select Tenderers/Consultants for the Tender</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
        <jsp:useBean id="mapTendererDtBean" class="com.cptu.egp.eps.web.databean.MapTendererDtBean"/>
        <jsp:setProperty property="*" name="mapTendererDtBean"/>
        <script type="text/javascript">
            $(function() {
                $('#cmbComRegAddContry').change(function() {
                    var comCountry = $('#cmbComRegAddContry').val();
                    comCountry = comCountry.substring(0, comCountry.indexOf("_", 0));
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:comCountry,funName:'stateComboValue'},  function(j){
                        j = j.toString().replace('selected', '');
                        $("select#cmbComRegAddState").html('<option value="" selected >Please select District</option>'+j);
                    });
                });
                $('#cmbCorHeadContry').change(function() {
                    var corCountry = $('#cmbCorHeadContry').val();
                    corCountry = corCountry.substring(0, corCountry.indexOf("_", 0));
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:corCountry,funName:'stateComboValue'},  function(j){
                        j = j.toString().replace('selected', '');
                        $("select#cmbCorHeadState").html('<option value="" selected >Please select District</option>'+j);
                    });
                });
                $('#cmbPerAddContry').change(function() {
                    var perCountry = $('#cmbPerAddContry').val()
                    perCountry = perCountry.substring(0, perCountry.indexOf("_", 0));
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:perCountry,funName:'stateComboValue'},  function(j){
                        j = j.toString().replace('selected', '');
                        $("select#cmbPerAddState").html('<option value="" selected >Please select District</option>'+j);
                    });
                });
            });

            /*$(function() {
                $('#btnSearch').click(function() {

                    $('#dis').css("display", "none");
                    if($('#cmbSearchBy').val()=='emailId'){

                        txt = $('#txtSearchVal').val();
                        if($('#txtSearchVal').val()==""){
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('Please enter e-mail ID.')
                            return false;
                        }

                        var SearchByval=$('#cmbSearchBy').val();
                        var val = EmailCheck(txt)

                        if (val == true) {
                            $('#SearchValError').html('')
                            $('#dis').css("display", "none");
                            //return true;
                        }
                        else {
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('<div class="reqF_1">Please enter valid e-mail ID.</div>')
                            return false;
                        }
                    }

                    else if($('#cmbSearchBy').val()=='companyName'){

                        if($('#txtSearchVal').val()==""){
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('Please enter Company Name.')
                            return false;
                        }
                    }

                    else if($('#cmbSearchBy').val()=='companyRegNumber'){
                        if($('#txtSearchVal').val()==""){
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('Please enter Registration no.')
                            return false;
                        }
                    }
                });
            });*/

            $(function() {
                $('#btnSearch').click(function() {
                    var cnt = 0;
                    if($('#txtEmailId').val() != ''){
                        cnt = 1;
                    }else if($('#cmbContryOfBusiness').html()!=null && $('#cmbContryOfBusiness').val() != ''){
                        cnt = 1;
                    }else if($('#txtCompanyName').val() != ''){
                        cnt = 1;
                    }else if($('#txtRegsNo').val() != ''){
                        cnt = 1;
                    }else if($('#cmbComRegAddContry').html()!=null && $('#cmbComRegAddContry').val() != ''){
                        cnt = 1;
                    }else if($('#cmbComRegAddState').val() != ''){
                        cnt = 1;
                    }else if($('#txtComRegCityTown').val() != ''){
                        cnt = 1;
                    }else if($('#txtComRegThanaUpzilla').val() != ''){
                        cnt = 1;
                    }else if($('#cmbCorHeadContry').html()!=null && $('#cmbCorHeadContry').val() != ''){
                        cnt = 1;
                    }else if($('#cmbCorHeadState').val() != ''){
                        cnt = 1;
                    }else if($('#txtCorHeadCityTown').val() != ''){
                        cnt = 1;
                    }else if($('#txtCorHeadThanaUpzilla').val() != ''){
                        cnt = 1;
                    }else if($('#cmbPerAddContry').html()!=null && $('#cmbPerAddContry').val() != ''){
                        cnt = 1;
                    }else if($('#cmbPerAddState').val() != ''){
                        cnt = 1;
                    }else if($('#txtPerAddCityTown').val() != ''){
                        cnt = 1;
                    }else if($('#txtPerAddThanaUpzilla').val() != ''){
                        cnt = 1;
                    }


                    if(cnt == 0){
                        $('#SearchValError').html('Please enter at least one criteria');
                        return false;
                    }else{
                        $('#SearchValError').html('');
                    }
                 });
            });

            function EmailCheck(value) {
                //alert('in');
                //alert(value);
                var c = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
                //alert(c);
                return c;

            }
            $(function() {
                $('#ummap').submit(function() {
                    var chkCnt=0;
                    var cnt=$("#tb1").children().children().length-1;
                    if(cnt>0){
                        for(var i =0;i<cnt;i++){
                            if($("#chkUnmap"+i).attr("checked")){
                                chkCnt++;
                            }
                        }
                        if(chkCnt==0){
                            jAlert("Please select Bidder/Consultant.","Map Bidder/Consultant Alert", function(RetVal) {
                            });
                            return false;
                        }else{
                             var cntNext=$("#tb2").children().children().length-2;
                             if($('#pMethod').val()=="SSS" || $('#pMethod').val()=="IC" || $('#pMethod').val()=="DPM") {
                                 if(chkCnt>1){
                                    //jAlert("Only one bidder can be mapped when procurement method is SSS or IC.","Map Bidder/Consultant Alert", function(RetVal) {
                                    if($('#pMethod').val()=="DPM"){
                                        jAlert("Only one bidder can be mapped when procurement method is DCM.","Map Bidder/Consultant Alert", function(RetVal) {
                                        });
                                    }
                                    else {
                                        jAlert("Only one bidder can be mapped when procurement method is SSS.","Map Bidder/Consultant Alert", function(RetVal) {
                                        });
                                    }
                                    return false;
                                 }
                                 if(cntNext>0){
                                    //jAlert("Only one bidder can be mapped when procurement method is SSS or IC.","Map Bidder/Consultant Alert", function(RetVal) {
                                    if($('#pMethod').val()=="DPM"){
                                        jAlert("Only one bidder can be mapped when procurement method is DCM.","Map Bidder/Consultant Alert", function(RetVal) {
                                        });
                                    }
                                    else {
                                        jAlert("Only one bidder can be mapped when procurement method is SSS.","Map Bidder/Consultant Alert", function(RetVal) {
                                        });
                                    }                          
                                    return false;
                                 }
                             }
                             else
                             {
                             //$('#chkUnmap').find(':checkbox').attr('checked', 'checked');
                                for(var i =0;i<cnt;i++){
                                    //$("#chkUnmap"+i).attr("checked", 'checked');
                                }
                             }
                        }
                     }else{
                        jAlert("No Bidder's/Consultant's found.","Map Bidder/Consultant Alert", function(RetVal) {
                        });
                        return false;
                     }
                });
            })
            $(function() {
                $('#map').submit(function() {
                    var chkCnt=0;
                    var cnt=$("#tb2").children().children().length-2;
                    if(cnt>0){
                        for(var i =0;i<cnt;i++){
                            if($("#chkMap"+i).attr("checked")){
                                chkCnt++;
                            }
                        }
                        if(chkCnt==0){
                            jAlert("Please select Bidder/Consultant.","Unmap Bidder/Consultant Alert", function(RetVal) {
                            });
                            return false;
                        } else {
                            for(var i =0;i<cnt;i++){
                                //$("#chkMap"+i).attr("checked", 'checked');
                        }
                        }
                     }else{
                        jAlert("No Bidder's/Consultant's found.","Unmap Bidder/Consultant Alert", function(RetVal) {
                        });
                        return false;
                     }
                });
            })
            $(function() { //Taher
                $('#cmbSearchBy').change(function() {
                    if($('#cmbSearchBy').val()=="state"){
                        $('#txtSearchVal').hide();
                        $('#txtSearchVal').attr("disabled", "disabled");
                        $('#cmbRegState').show();
                        $('#cmbRegState').removeAttr("disabled");
                        if($("#cmbRegState").val()==null){
                            //Change 136 to 150 by Proshanto
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:150,funName:'stateComboValue'},  function(j){
                                $("#cmbRegState").html(j);
                            });
                        }
                    }else{
                        $('#txtSearchVal').show();
                        $('#txtSearchVal').removeAttr("disabled");
                        $('#cmbRegState').hide();
                        $('#cmbRegState').attr("disabled","disabled");
                    }
                });
            });
            function checkUncheck(val,chkall){
                 $(":checkbox[chk='"+val+"']").each(function(){
                     if($(chkall).attr('checked')){
                         $(this).attr('checked', true);
                     }else{
                         $(this).removeAttr('checked');
                     }
                 });
            }
            function checkSelectAll()
            {
                if($('#tb1 tr').length > 2) {
                        $('#mapallchk').attr('checked', true);
                }
                if($('#tb2 tr').length > 2) {
                        $('#unmapallchk').attr('checked', true);
                }
            }
</script>
</head>
<body onload="checkSelectAll();">
<%
                    String cntName = "";
                    boolean isView=false;
                    if("y".equals(request.getParameter("isview"))){
                        isView=true;
                    }
                    String msg=null;
                    String tenderid = request.getParameter("tenderid");
                    String lotId = request.getParameter("lotId");
                    pageContext.setAttribute("tenderId", tenderid);
                    TenderSrBean tenderSrBean = new TenderSrBean();
                    List<Object[]> pMethodCPV = tenderSrBean.getPMethodCPV(tenderid);
                    String tempCPV = "0";
                    if(!"LTM".equals(pMethodCPV.get(0)[0]) && !"ICT".equals(pMethodCPV.get(0)[2])){
                        tempCPV ="1";
                    }
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String procN = commonService.getProcNature(tenderid).toString();
                    java.util.List<SPCommonSearchDataMore>  list1 = new java.util.ArrayList<SPCommonSearchDataMore>();
                    if("Search".equalsIgnoreCase(request.getParameter("btnSearch"))){
                        list1 = tenderSrBean.searchBidderForMapping(tenderid, mapTendererDtBean,true,lotId,tempCPV,procN);
                    }else{
                        if(!pMethodCPV.isEmpty()){
                            if(!"LTM".equals(pMethodCPV.get(0)[0]) && !"ICT".equals(pMethodCPV.get(0)[2])){
                                list1 = tenderSrBean.searchBidderForMapping(tenderid, null,false,lotId,"1",procN);
                            }
                            else{
                                list1 = tenderSrBean.searchBidderForMapping(tenderid, null,false,lotId,"0",procN);
                            }
                        }
                    }
                    if("Add to List".equalsIgnoreCase(request.getParameter("map"))){
                        msg="Company mapped successfully";
                        tenderSrBean.mapBidderWithTender(request.getParameterValues("userid"), Integer.parseInt(tenderid), true,lotId);
                        String pageName="MapTenderers.jsp?tenderid="+tenderid;
                        if(lotId!=null){
                            pageName+="&lotId="+lotId;
                        }
                        response.sendRedirect(pageName);
                    }
                    if("Remove from List".equalsIgnoreCase(request.getParameter("remove"))){
                        msg="Company removed successfully";
                        tenderSrBean.mapBidderWithTender(request.getParameterValues("userid"), Integer.parseInt(tenderid), false,lotId);
                        String pageName="MapTenderers.jsp?tenderid="+tenderid;
                        if(lotId!=null){
                            pageName+="&lotId="+lotId;
                        }
                        response.sendRedirect(pageName);
                    }
                    String uId="0";
                    if(session.getAttribute("userId")!=null){
                        uId=session.getAttribute("userId").toString();
                    }
                    LoginMasterSrBean loginMasterSrBean = new LoginMasterSrBean(uId);
                    CommonSearchDataMoreService commonSearchDataMoreServiceForMap = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> chkNMT = commonSearchDataMoreServiceForMap.geteGPData("chkMapTendererTender",tenderid);
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="pageHead_1">Select Bidders/Consultants for the Tender<span style="float: right;"><a class="action-button-goback" href="Notice.jsp?tenderid=<%=request.getParameter("tenderid")%>">Go back to Dashboard</a></span></div>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <div class="tabPanelArea_1 t_space">
            <%if(lotId!=null){%>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <%
                        for(Object[] lot : commonService.getLotDetailsByPkgLotId(lotId,tenderid)){
                    %>
                    <tr>
                        <td class="ff">Lot No.</td>
                        <td><%=lot[0]%></td>
                    </tr>
                    <tr>
                        <td class="ff">Lot Description.</td>
                        <td><%=lot[1]%></td>
                    </tr>
                    <%}%>
                </table>
            <%}%>
            <%if(!isView){%>
            <%if(msg!=null){%>
            <div class="responseMsg successMsg"><%=msg%></div><br/>
            <%}msg=null;%>
            <div class="formBg_1 t_space">
                <input type="hidden" id="pMethod" value="<%=pMethodCPV.get(0)[0]%>">
                <form action="MapTenderers.jsp?tenderid=<%=tenderid%><%if(lotId!=null){out.print("&lotId="+lotId);}%>" method="post" id="search" name="search">
<!--                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td class="ff" width="10%">Search by   : <span>*</span></td>
                            <td width="90%"><select name="searchBy" class="formTxtBox_1" id="cmbSearchBy" style="width: 200px;">
                                    <option value="companyRegNumber">Registration No</option>
                                    <option value="companyName">Company Name</option>
                                    <option value="emailId" selected>e-mail ID</option>
                                    <option value="state">District</option>
                                    <option value="city">City/Town</option>
                                    <option value="thana">Upzilla/Thana</option>
                                </select>&nbsp;
                                <input name="searchVal" type="text" class="formTxtBox_1" id="txtSearchVal" style="width:200px;"/>&nbsp;
                                <select name="searchVal" class="formTxtBox_1" id="cmbRegState" disabled style="display: none;">
                                </select>
                                <label class="formBtn_1">
                                    <input type="submit" name="btnSearch" id="btnSearch" value="Search" />
                                </label>
                            </td>
                        </tr>
                        <tr id="dis" style="display: none;"><td width="10%">&nbsp;</td><td class="t-align-left"><span id="SearchValError" class="reqF_1"></span></td></tr>
                    </table>-->
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td class="ff" width="20%">Email Id :</td>
			    <td width="30%"><input name="emailId" type="text" class="formTxtBox_1" id="txtEmailId" style="width:200px;" /></td> <!--disabled-->
                            <td class="ff" width="20%">Country of business :</td>
                            <td width="30%">
                                <%if(!chkNMT.isEmpty() && chkNMT.get(0).getFieldName1().equalsIgnoreCase("1")){%>
                                <label>Bhutan</label>
                                <%}else{%>
				<select name="contryOfBusiness" class="formTxtBox_1" id="cmbContryOfBusiness" style="width: 200px;" > <!--disabled-->
                                    <option value="" selected>Please select Country of business</option>
                                    <%for(SelectItem country : loginMasterSrBean.getCountryList()){%>
                                    <option value="<%=country.getObjectId()%>" ><%=country.getObjectValue()%></option>
                                    <%}%>
                                </select>
                                <% } %>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="20%">Company Name :</td>
			    <td width="30%"><input name="companyName" type="text" class="formTxtBox_1" id="txtCompanyName" style="width:200px;" /></td> <!--disabled-->
                            
                            <!--This portion is hidden because registration numbers for bidders are not available-->
                            <!--<td class="ff" width="20%">Registration No :</td>
                            <td width="30%">
			    	<input name="regsNo" type="text" class="formTxtBox_1" id="txtRegsNo" style="width:200px;" disabled/>
                            </td>-->  
                        </tr>
                        <tr>
                            <th colspan="4" class="formSubHead_1">Company Registered Address</th>
                        </tr>
                        <tr>
                            <td class="ff" width="20%">Country :</td>
                            <td width="30%">
                                <%if(!chkNMT.isEmpty() && chkNMT.get(0).getFieldName1().equalsIgnoreCase("1")){%>
                                <label>Bhutan</label>
                                <%}else{%>
                                <select name="comRegAddContry" class="formTxtBox_1" id="cmbComRegAddContry" style="width: 200px;">
                                    <option value="" selected >Please select Country</option>
                                    <%for (SelectItem country : tendererMasterSrBean.getCountryList()) {%>
                                    <option value="<%=country.getObjectId()%>_<%=country.getObjectValue()%>"><%=country.getObjectValue()%></option>
                                    <%}%>
                                </select>
                                <% } %>
                            </td>
                            <td class="ff" width="20%">District / Dzongkhag :</td>
                            <td width="30%">
                                <select name="comRegAddState" class="formTxtBox_1" id="cmbComRegAddState" style="width: 200px;">
                                    <%if(!chkNMT.isEmpty() && chkNMT.get(0).getFieldName1().equalsIgnoreCase("1")){%>
                                    <option value="" selected >Please select District</option>
                                    <%cntName = "Bhutan";
                                    for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) {%>
                                    <option value="<%=state.getObjectValue()%>" ><%=state.getObjectValue()%></option>
                                    <%}%>
                                    <%}else{%>
				    <option value="" selected >Please select District/Dzongkhag</option>
                                    <% } %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="20%">City / Town :</td>
                            <td width="30%"><input name="comRegCityTown" type="text" class="formTxtBox_1" id="txtComRegCityTown" style="width:200px;"/></td>
                            <td class="ff" width="20%">Dungkhag :</td>
                            <td width="30%">
                                <input name="comRegThanaUpzilla" type="text" class="formTxtBox_1" id="txtComRegThanaUpzilla" style="width:200px;"/>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="4" class="formSubHead_1">Corporate / Head office</th>
                        </tr>
                        <tr>
                            <td class="ff" width="20%">Country :</td>
                            <td width="30%">
                                <%if(!chkNMT.isEmpty() && chkNMT.get(0).getFieldName1().equalsIgnoreCase("1")){%>
                                <label>Bhutan</label>
                                <%}else{%>
                                <select name="corHeadContry"  class="formTxtBox_1" id="cmbCorHeadContry" style="width: 200px;">
                                    <option value="" selected >Please select Country</option>
                                    <%for (SelectItem country : tendererMasterSrBean.getCountryList()) {%>
                                    <option value="<%=country.getObjectId()%>_<%=country.getObjectValue()%>"><%=country.getObjectValue()%></option>
                                    <%}%>
                                </select>
                                <% } %>
                            </td>
                            <td class="ff" width="20%">District / Dzongkhag :</td>
                            <td width="30%">
                                <select name="corHeadState" class="formTxtBox_1" id="cmbCorHeadState" style="width: 200px;">
                                   <%if(!chkNMT.isEmpty() && chkNMT.get(0).getFieldName1().equalsIgnoreCase("1")){%>
                                    <option value="" selected >Please select District</option>
                                    <%cntName = "Bhutan";
                                    for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) {%>
                                    <option value="<%=state.getObjectValue()%>" ><%=state.getObjectValue()%></option>
                                    <%}%>
                                    <%}else{%>
				    <option value="" selected >Please select District/Dzongkhag</option>
                                    <% } %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="20%">City / Town :</td>
                            <td width="30%"><input name="corHeadCityTown" type="text" class="formTxtBox_1" id="txtCorHeadCityTown" style="width:200px;"/></td>
                            <td class="ff" width="20%">Dungkhag :</td>
                            <td width="30%">
                                <input name="corHeadThanaUpzilla" type="text" class="formTxtBox_1" id="txtCorHeadThanaUpzilla" style="width:200px;"/>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="4" class="formSubHead_1">Personal address</th>
                        </tr>
                        <tr>
                            <td class="ff" width="20%">Country :</td>
                            <td width="30%">
                                <%if(!chkNMT.isEmpty() && chkNMT.get(0).getFieldName1().equalsIgnoreCase("1")){%>
                                <label>Bhutan</label>
                                <%}else{%>
                                <select name="perAddContry" class="formTxtBox_1" id="cmbPerAddContry" style="width: 200px;">
                                    <option value="" selected >Please select Country</option>
                                    <% for (SelectItem country : tendererMasterSrBean.getCountryList()) {%>
                                    <option value="<%=country.getObjectId()%>_<%=country.getObjectValue()%>"><%=country.getObjectValue()%></option>
                                    <%}%>
                                </select>
                                <% } %>
                            </td>
                            <td class="ff" width="20%">District / Dzongkhag :</td>
                            <td width="30%">
                                <select name="perAddState" class="formTxtBox_1" id="cmbPerAddState" style="width: 200px;">
                                    <%if(!chkNMT.isEmpty() && chkNMT.get(0).getFieldName1().equalsIgnoreCase("1")){%>
                                    <option value="" selected >Please select District</option>
                                    <%cntName = "Bhutan";
                                    for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) {%>
                                    <option value="<%=state.getObjectValue()%>" ><%=state.getObjectValue()%></option>
                                    <%}%>
                                    <%}else{%>
				    <option value="" selected >Please select District/Dzongkhag</option>
                                    <% } %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="20%">City / Town :</td>
                            <td width="30%"><input name="perAddCityTown" type="text" class="formTxtBox_1" id="txtPerAddCityTown" style="width:200px;"/></td>
                            <td class="ff" width="20%">Dungkhag :</td>
                            <td width="30%">
                                <input name="perAddThanaUpzilla" type="text" class="formTxtBox_1" id="txtPerAddThanaUpzilla" style="width:200px;"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-center" colspan="4">
                                <label class="formBtn_1">
                                    <input type="submit" name="btnSearch" id="btnSearch" value="Search" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-center" colspan="4">
                                <div id="SearchValError" class="reqF_1"></div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div>&nbsp;</div>
            <%--<%if(!pMethodCPV.isEmpty() && "LTM".equals(pMethodCPV.get(0)[0])){%><span style="font-weight: bold">CPV Category : </span><%out.print(pMethodCPV.get(0)[1].toString());%><%}%>--%>
            <form action="MapTenderers.jsp?tenderid=<%=tenderid%><%if(lotId!=null){out.print("&lotId="+lotId);}%>" method="post" id="ummap" name="ummap">
               <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tb1">
                <tr>
                    <th width="7%" class="t-align-center">
                        Select All
			<input type="checkbox" onclick="checkUncheck('map',this);" id="mapallchk"/>
                    </th>
                    <th class="t-align-left" width="4%">Sl. No.</th>
                    <th class="t-align-left" width="21%">Company Name</th>
                    <th class="t-align-left" width="13%">e-mail ID</th>
                    <th class="t-align-center" width="15%">Company Registered Country</th>
                    <th class="t-align-center" width="10%">Company Registered Dzongkhag / District</th>
                    <th class="t-align-center" width="10%">Registration Type</th>
                    <th class="t-align-center" width="10%">Legal Status</th>
                    <th class="t-align-center" width="10%">Permissions</th>
                </tr>
                <%
                        int i=0;
                        
                        for(SPCommonSearchDataMore sptcd : list1){
                          
                        if(sptcd.getFieldName9().equalsIgnoreCase("unmapped")){
                            
                        //Filtering bidder based on WorkType and WorkCategory    
                        if (procN.equalsIgnoreCase("Works"))
                        { 
                            List<TblAppPermission> appPermissionList = commonService.getPermissionByTenderId(tenderid);
                            String WorkType = appPermissionList.get(0).getWorkType();
                            List<Object[]> permissions = tenderSrBean.getCompanyPermissions(sptcd.getFieldName8());
                            int count=0;
                         
                            for (Object[] obj : permissions) {
                                if (obj[0].toString().equalsIgnoreCase(WorkType))
                                {
                                    for(int k = 0; k < appPermissionList.size(); k++)
                                    {  
                                        if(obj[1].toString().equalsIgnoreCase(appPermissionList.get(k).getWorkCategroy()))
                                            count++;
                     
                                    }
                                }
                            }
                            if(count == 0)
                                continue;
                            
                        }// end
                 %>
                <tr>
    <td class="t-align-center"><input name="userid" type="checkbox" value="<%=sptcd.getFieldName8()%>" id="chkUnmap<%=i%>" chk="map" checked="checked"/></td>
                    <td class="t-align-center"><%=i+1%></td>
                    <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                    <td class="t-align-left"><%=sptcd.getFieldName3()%></td>
                    <td class="t-align-center"><%=sptcd.getFieldName4()%></td>
                    <td class="t-align-center"><%=sptcd.getFieldName5()%></td>
                    <td class="t-align-center">
                            <%if (sptcd.getFieldName6().equals("contractor")) {%>Bidder / Consultant<%}%>
                            <%if (sptcd.getFieldName6().equals("individualconsultant")) {%>Individual Consultant<%}%>
                            <%if (sptcd.getFieldName6().equals("govtundertaking")) {%> Government owned Enterprise<%}%>
                            <%if (sptcd.getFieldName6().equals("media")) {%>Media<%}%>
                    </td>
                    <td class="t-align-center">
                            <%if(sptcd.getFieldName7().equals("public")){%>Public Ltd<%}%>
                            <%if(sptcd.getFieldName7().equals("private")){%>Private Ltd<%}%>
                            <%if(sptcd.getFieldName7().equals("partnership")){%>Partnership<%}%>
                            <%if(sptcd.getFieldName7().equals("proprietor")){%>Proprietor<%}%>
                            <%if(sptcd.getFieldName7().equals("government")){%>Government Undertaking<%}%>
                    </td>
                    <td class="t-align-center">
                        <%
                            out.print(procN+"<br/>");
                            if(procN.equalsIgnoreCase("Works"))
                            {
                                String large = "";
                                String medium = "";
                                String small = "";
                                
                                
                               List<Object[]> permissions = tenderSrBean.getCompanyPermissions(sptcd.getFieldName8()); 
                                
                                for (Object[] obj : permissions) {
                                    if(obj[0].toString().equalsIgnoreCase("Large"))
                                    {
                                        large += obj[1].toString()+" ";
                                    }
                                    if(obj[0].toString().equalsIgnoreCase("Medium"))
                                    {
                                        medium += obj[1].toString()+" ";
                                    }
                                    if(obj[0].toString().equalsIgnoreCase("Small"))
                                    {
                                        small += obj[1].toString()+" ";
                                    }

                                }
                                if(!large.equals(""))
                                {
                                    out.print("Large: "+large+"<br/>");
                                }
                                if(!medium.equals(""))
                                {
                                    out.print("  Medium: "+medium+"<br/>");
                                }
                                if(!small.equals(""))
                                {
                                    out.print("  Small: "+small+"<br/>");
                                }
                            }
                        %>
                    </td>
                </tr>
                <%i++;}}if(i==0){out.print("<tr><td colspan='9' class='t-align-center'><span style='color:red;'>No Records Found</span></td></tr>");}%>
            </table>
            <div class="t-align-center t_space">
                <label class="formBtn_1"><input name="map" type="submit" value="Add to List"/></label>
            </div>
            </form>
            <%
                }
                java.util.List<SPCommonSearchDataMore>  list2 = new java.util.ArrayList<SPCommonSearchDataMore>();
                list2 = tenderSrBean.searchBidderForMapping(tenderid,mapTendererDtBean,false,lotId,tempCPV,procN);
                if(!list2.isEmpty()){
            %>
            <form action="MapTenderers.jsp?tenderid=<%=tenderid%><%if(lotId!=null){out.print("&lotId="+lotId);}%>" method="post" id="map" name="map">
                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tb2">
                <tr>
                    <th colspan="<%if(isView){out.print("8");}else{out.print("9");}%>">Selected Bidders/Consultants</th>
                </tr>
                <tr>
                    <%if(!isView){%><th width="7%" class="t-align-center">
                        Select All
			<input type="checkbox" onclick="checkUncheck('unmap',this);" id="unmapallchk"/>
                    </th>
                    <%}%>
                    <th class="t-align-left" width="4%">Sl. No.</th>
                    <th class="t-align-left" width="21%">Company Name</th>
<!--                    <th class="t-align-left" width="15%">Company Registration No</th>-->
                    <th class="t-align-left" width="13%">e-mail ID</th>
                    <th class="t-align-center" width="15%">Company Registered Country</th>
                    <th class="t-align-center" width="10%">Company Registered Dzongkhag / District</th>
                    <th class="t-align-center" width="10%">Registration Type</th>
                    <th class="t-align-center" width="10%">Legal Status</th>
                    <th class="t-align-center" width="10%">Permissions</th>
                </tr>
                <%
                        int j=0;
                        for(SPCommonSearchDataMore sptcd : list2){
                        if(sptcd.getFieldName9().equalsIgnoreCase("mapped")){
                            List<Object[]> permissions = tenderSrBean.getCompanyPermissions(sptcd.getFieldName8());
                 %>
                <tr>
    <%if(!isView){%><td class="t-align-center"><input name="userid" type="checkbox" value="<%=sptcd.getFieldName8()%>" id="chkMap<%=j%>"  chk="unmap" checked="checked"/></td><%}%>
                    <td class="t-align-center"><%=j+1%></td>
                    <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
<!--                    <td class="t-align-left"><%=sptcd.getFieldName2()%></td>-->
                    <td class="t-align-left"><%=sptcd.getFieldName3()%></td>
                    <td class="t-align-center"><%=sptcd.getFieldName4()%></td>
                    <td class="t-align-center"><%=sptcd.getFieldName5()%></td>
                    <td class="t-align-center">
                            <%if (sptcd.getFieldName6().equals("contractor")) {%>Bidder / Consultant<%}%>
                            <%if (sptcd.getFieldName6().equals("individualconsultant")) {%>Individual Consultant<%}%>
                            <%if (sptcd.getFieldName6().equals("govtundertaking")) {%> Government owned Enterprise<%}%>
                            <%if (sptcd.getFieldName6().equals("media")) {%>Media<%}%>
                    </td>
                    <td class="t-align-center">
                            <%if(sptcd.getFieldName7().equals("public")){%>Public Ltd<%}%>
                            <%if(sptcd.getFieldName7().equals("private")){%>Private Ltd<%}%>
                            <%if(sptcd.getFieldName7().equals("partnership")){%>Partnership<%}%>
                            <%if(sptcd.getFieldName7().equals("proprietor")){%>Proprietor<%}%>
                            <%if(sptcd.getFieldName7().equals("government")){%>Government Undertaking<%}%>
                    </td>
                    <td class="t-align-center">
                        <%
                            out.print(procN+"<br/>");
                            if(procN.equalsIgnoreCase("Works"))
                            {
                                String large = "";
                                String medium = "";
                                String small = "";
                                for (Object[] obj : permissions) {
                                    if(obj[0].toString().equalsIgnoreCase("Large"))
                                    {
                                        large += obj[1].toString()+" ";
                                    }
                                    if(obj[0].toString().equalsIgnoreCase("Medium"))
                                    {
                                        medium += obj[1].toString()+" ";
                                    }
                                    if(obj[0].toString().equalsIgnoreCase("Small"))
                                    {
                                        small += obj[1].toString()+" ";
                                    }

                                }
                                if(!large.equals(""))
                                {
                                    out.print("Large: "+large+"<br/>");
                                }
                                if(!medium.equals(""))
                                {
                                    out.print("  Medium: "+medium+"<br/>");
                                }
                                if(!small.equals(""))
                                {
                                    out.print("  Small: "+small+"<br/>");
                                }
                            }
                        %>
                    </td>
                </tr>
                <%j++;}}if(isView){if(j==0){out.print("<tr><td colspan='9' class='t-align-center'><span style='color:red;'>No Records Found</span></td></tr>");}}%>
            </table>
            <%if(!isView){%>
            <div class="t-align-center t_space">
                <label class="formBtn_1 l_space"><input name="remove" type="submit" value="Remove from List" /></label>
            </div>
            <%}%>
            </form>
            <%}%>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>

<%
    tenderSrBean = null;
    tendererMasterSrBean = null;
    list1=null;
    list2=null;
%>
