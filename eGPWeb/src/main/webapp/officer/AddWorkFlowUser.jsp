<%-- 
    Document   : SearchUser
    Created on : Nov 2, 2010, 2:26:33 PM
    Author     : parag
--%>

<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List,com.cptu.egp.eps.dao.storedprocedure.CommonAppData" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Search and Add User(s)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!-- <script type="text/javascript" src="../resources/js/pngFix.js"></script> -->
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        

        <script type="text/javascript" language="Javascript">
            function setofficeid(x){
               var check = false;

                var don = document.getElementById("isdp");
                if( don.value == "false"){
                     var temp1 = document.getElementById('txtdepartmentid');

                      if(temp1.value==''){
                         $("#msg_1").html("Please select Hierarchy Node");
                         $("#cmbmsg").html("Please select Office");
                         return check;
                      }else{
                          check = true;
                          $("#msg_1").html('');
                         $("#cmbmsg").html('');
                      }
                }else{

                     var org = document.getElementById("cmbOrganization");
                     var orgid = org.options[org.selectedIndex].value;
                     // alert(orgid);
                      if(orgid.value == ''){
                         $("#msg_1").html("Please select Hierarchy Node");
                      }else{
                          $("#msg_1").html('');
                          check = true;
                      }

                }

               var ob =   document.getElementById(x);
               var size = ob.childNodes.length;
              
                 if(check == true && ob.value!=""){
                   $("#cmbmsg").html('');
                   var officeid = ob.options[ob.selectedIndex].value;
                   var txt = document.getElementById('officeid');
                   txt.value = officeid;
               }else{
                     check = false;
                     $("#cmbmsg").html("Please select Office");
               }
              return check;
            }
  
        </script>
        <script type="text/javascript">

            function getOffices(){
        //alert($('#previousSelOffId').val(        ));
                $.post("<%=request.getContextPath()%>/ProjectSrBean", {deptId:$('#txtdepartmentid').val(),prevOffId:$('#previousSelOffId').val(),funnum:$('#funnum').val(),funName:$('#officetype').val()}, function(j){
                    $("select#cmbOffice").html(j);
                    //alert(j);
                });
            }

            function getdpOffices(){

                $.post("<%=request.getContextPath()%>/ProjectSrBean", {deptId:$('#cmbOrganization').val(),prevOffId:$('#previousSelOffId').val(),funnum:$('#funnum').val(),funName:$('#officetype').val()}, function(j){
                    $("select#cmbOffice").html(j);
                    //alert(j);
                });
            }

            $(function() {
          
                $('#cmbOrganization').change(function() {

                    $.post("<%=request.getContextPath()%>/ProjectSrBean", {deptId:$('#cmbOrganization').val(),prevOffId:$('#previousSelOffId').val(),funnum:$('#funnum').val(),funName:$('#officetype').val()}, function(j){
                        $("select#cmbOffice").html(j);
                        //alert(j);
                    });
                });
            });

        </script>
    </head>
    <body>
        <%
                    String userval = request.getParameter("userval");
                    String userid = request.getParameter("userid");
                    String wfroleid = request.getParameter("wfroleid");
                    String uid = request.getParameter("uid");
                    String donorval = "0";
                    donorval= request.getParameter("donorval");
                    String departmentName = "";
                    String departmentid = "";
                    String previousSelOffId = "0";
                    String previousDeptId = "0";
                    String emailId = "";
                    int officeId = 0;
                    int funnum = 0;
                    boolean isdp = false;
                    boolean isSearch = false;
                    String deptName = null;
                    String offices = null;
                    int did = 0;
                    List<CommonAppData> commonAppDatas = null;
                    List<CommonAppData> departmentList = null;
                    String officetype = null;
                    if ("Search".equalsIgnoreCase(request.getParameter("btnSearch")) && "20".equals(request.getParameter("donorval"))) {
                        departmentList = projSrUser.getDepartmentsByUserid("dpOrganization", uid, "");
                        request.setAttribute("departmentList", departmentList);
                        String searchfield1 = "DpSearchuser";
                        String searchfield2 = "0";
                        String Searchfield3 = "";
                        Searchfield3 = request.getParameter("cmbOffice");
                        if (Searchfield3 == null) {
                            Searchfield3 = "0";
                        }
                        previousSelOffId = request.getParameter("cmbOffice");
                        isSearch = true;
                        departmentName = request.getParameter("txtdepartment");
                        departmentid = request.getParameter("txtdepartmentid");
                        officetype = request.getParameter("officetype");
                        funnum = Integer.parseInt(request.getParameter("funnum"));
                        previousDeptId = request.getParameter("cmbOrganization");
                        commonAppDatas = projSrUser.getWfSearchList(searchfield1, searchfield2, Searchfield3);

                    } else if ("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {

                        String searchfield1 = "SearchUserByRole";
                        String searchfield2 = request.getParameter("cmbOffice");
                        String Searchfield3 = wfroleid;
                        previousSelOffId = request.getParameter("cmbOffice");
                        isSearch = true;
                        departmentName = request.getParameter("txtdepartment");
                        departmentid = request.getParameter("txtdepartmentid");
                        officetype = request.getParameter("officetype");
                        funnum = Integer.parseInt(request.getParameter("funnum"));
                        commonAppDatas = projSrUser.getWfSearchList(searchfield1, searchfield2, Searchfield3);

                    } else if ("Add User".equalsIgnoreCase(request.getParameter("btnAddUser"))) {
                        String selUser = request.getParameter("SelUserId");
                        int selUserId = Integer.parseInt(selUser.substring(0, selUser.indexOf("_")));
                        String selUserValue = selUser.substring(selUser.indexOf("_") + 1, selUser.length());
                    } else if (donorval.equals("20")) {
                        departmentList = projSrUser.getDepartmentsByUserid("dpOrganization", uid, "");
                        request.setAttribute("departmentList", departmentList);
                        officetype = "DpOffice";
                        funnum = 2;
                        isdp = true;
                    } else {
                        String field1 = "userorganization";
                        funnum = 1;
                        officetype = "DpOffice";
                    }


        %>
        <div class="dashboard_div">

            <div class="pageHead_1">Search and Add User(s)</div>
            <div>&nbsp;</div>
            <form method="post" >
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1"  width="100%">

                    <tr>
                        <td class="ff" width="150px"><label id="selorg">Select Hierarchy Node : <span>*</span></label></td>
                        <td>
                            <% if (!donorval.equals("20")) {%>
                            <input class="formTxtBox_1" value="<%=departmentName%>" name="txtdepartment" type="text" style="width: 200px;"
                                   id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                            <input type="hidden" value="<%=departmentid%>" name="txtdepartmentid" id="txtdepartmentid" />

                            <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=workflow', '', 'width=350px,height=400px,scrollbars=1','');">
                                <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                            </a>
                            <br /><span id="msg_1" style="color:red; "></span>
                            <% } else {%>
                            <select name="cmbOrganization" class="formTxtBox_1" id="cmbOrganization" style="width:200px;">

                                <% if (departmentList.size() > 0) {
                                         Iterator it = departmentList.iterator();
                                         while (it.hasNext()) {
                                             CommonAppData appdata = (CommonAppData) it.next();
                                             if (Integer.parseInt(previousDeptId) == Integer.parseInt(appdata.getFieldName1())) {
                                %>
                                <option selected="true" value="<%= appdata.getFieldName1()%>"><%=appdata.getFieldName2()%></option>
                                <% } else {%>
                                <option value="<%= appdata.getFieldName1()%>"><%=appdata.getFieldName2()%></option>
                                <% }
                     }
                 }%>
                            </select>
                            <br /><span id="msg_1" style="color:red;" ></span>
                            <% }%>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff" width="150px">Select Office : <span>*</span></td>
                        <td id="tdOffice"><select  name="cmbOffice" class="formTxtBox_1" id="cmbOffice" style="width:200px;">
                                <%=offices%>
                            </select>
                            <br /><span id="cmbmsg" style="color:red;"></span>
                            <%if ("Search".equalsIgnoreCase(request.getParameter("btnSearch")) && !"20".equals(request.getParameter("donorval")) && isSearch == true) {
                            %>
                            <script>
                                //alert('of');
                                getOffices();
                            </script>
                            <%} else if (isSearch == true) {
                            %>
                            <script>
                                //alert('dp');
                                getdpOffices();
                            </script>
                            <% }%>

                        </td>

                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><label class="formBtn_1">
                                <input type="submit" onclick="return setofficeid('cmbOffice')" name="btnSearch" id="btnSearch" value="Search" />
                                <input type="hidden" name="officetype" id="officetype" value="<%=officetype%>" />
                                <input type="hidden" name="officeid" id="officeid" value="" />
                                <input type="hidden" name="donorval" id="donorval" value="<%=donorval%>" />
                                <input type="hidden" name="previousSelOffId" id="previousSelOffId" value="<%=previousSelOffId%>"/>
                                <input type="hidden" name="funnum" id="funnum" value="<%=funnum%>" />
                                <input type="hidden" name="isdp" id="isdp" value="<%=isdp%>" />

                            </label>       </td>
                    </tr>
                </table>
            </form>
            <form method="post" >
                <table width="90%"  cellspacing="0"  class="tableList_1 t_space">

                    <tr>
                        <th><strong>Select</strong></th>
                        <th>
                            Name of Official,[Designation at Office]</th>

                    </tr>
                    <%
                                int count = 2;
                                if (commonAppDatas != null && commonAppDatas.size() > 0) {
                                    for (CommonAppData appData : commonAppDatas) {
                                        count++;
                    %>
                    <tr>
                        <td class="t-align-center"><input type="radio" name="SelUserId" id="rdSelUserId<%=count%>"
                                                        value="<%if (isSearch) {
                                                           out.print(appData.getFieldName2() + '_' + appData.getFieldName1());
                                                       }%>" /></td>
                        <td class="t-align-left"><%if (isSearch) {
                         out.print(appData.getFieldName1());
                     }%>
                            <input type="hidden" id="txtSelUserId<%=count%>" name="txtSelUserId<%=count%>"
                                   value="<%if (isSearch) {
                                out.print(appData.getFieldName1());
                            }%>"    />
                        </td>

                    </tr>
                    <%
                              }
                          } else {
                    %>
                    <tr>
                        <td colspan="2">No user found</td>
                    </tr>
                    <%  }%>
                    <tr>
                        <td colspan="2" class="t-align-center"><span class="formBtn_1">
                                <input type="hidden" id="hidCountId" name="hidCountId" value="<%=count%>" />
                                <input type="submit" name="btnAddUser" id="btnAddUser" value="Add User" onclick="return getData();" />
                                <input type="hidden" name="userval" id="userval" value="<%=userval%>"/>
                                <input type="hidden" name="userid" id="userid" value="<%=userid%>" />
                            </span></td>
                    </tr>
                </table>
            </form>
            

            <p>&nbsp;</p>
            <div>&nbsp;</div>

        </div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabWorkFlow");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>

<script type="text/javascript">
    function getData(){
        try{
            var selUser =$("input:radio[name='SelUserId']:checked").val();
        
            var selUserId = selUser.substring(0,selUser.indexOf("_",0));
            //alert(selUserId)
            var selUserValue = selUser.substring(selUser.indexOf("_",0) + 1,selUser.length);
        }catch(e){
            jAlert("Please select atleast one user."," Alert ", function(RetVal) {
            });
            return false;
        }
        
        var user_val = document.getElementById("userval").value;
        var user_id = document.getElementById("userid").value;
        if(user_val == null){
            return false;
        }

        var str = new String(user_id);
        var str2 =str.substring(str.length-1, str.length);
        var userlabel = "<label id='uservalue"+str2+"'>"+selUserValue+"</label><input type='hidden' name='txtuserid' id='txtuserid"+str2+"' value="+selUserId+" />";
        window.opener.document.getElementById("tdrev"+str2).innerHTML = userlabel;

        window.opener.document.getElementById("us"+str2).innerHTML = '';
     
        window.opener.document.getElementById(user_val).style.display = 'block';
        window.opener.document.getElementById("adduser"+str2).style.display = 'none';
        window.opener.document.getElementById("remuser"+str2).style.display = 'inline';
        window.opener.document.getElementById("removeicn"+str2).style.visibility="visible";
        window.opener.document.getElementById("removeicn"+str2).style.display="inline";
        window.opener.document.getElementById("addicn"+str2).style.visibility="hidden";
        window.opener.document.getElementById("addicn"+str2).style.display='none';
        
        window.close();
                return false
            }
</script>
<%if ("Search".equalsIgnoreCase(request.getParameter("btnSearch")) && !"20".equals(request.getParameter("donorval")) && isSearch == true) {
%>
<script>
    getOffices();
</script>
<%} else if (isSearch == true) {
%>
<script>
    getdpOffices();
</script>

<% }%>