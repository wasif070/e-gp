<%-- 
    Document   : ServicePsVariationOrder
    Created on : Dec 22, 2011, 12:10:36 PM
    Author     : shreyansh.shah
--%>



<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPsvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvWpvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvStaffSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTeamComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle" %>
<%@page import="java.math.BigDecimal" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <% ResourceBundle srbd = null;
                    srbd = ResourceBundle.getBundle("properties.cmsproperty");
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Work Schedule</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script language="javascript">
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function getDate(date)
            {
                var splitedDate = date.split("-");
                return Date.parse(splitedDate[1]+" "+splitedDate[0]+", "+splitedDate[2]);
            }
            function checkDate(dateofdlvry,id){
                var group = document.getElementById("group_"+id).value;
                var firstDate = getDate(document.getElementById("firstdate").value);
                var secondDate = getDate(document.getElementById("seconddate").value);
                var dod = getDate(document.getElementById(dateofdlvry).value);
                if((new Date(firstDate).getTime() <= new Date(dod).getTime()) && (new Date(dod).getTime() <=  new Date(secondDate).getTime())){
                    checkDiff(dod,id);
                }else{
                    jAlert("<%=srbd.getString("CMS.works.edidatesByTendererandPe")%> ","CMS", function(RetVal) {
                    });
                    document.getElementById(dateofdlvry).value="";
                }

            }
            function checkDiff(values,id){
                var edate = document.getElementById("edate_"+id).value;
                var sdate = document.getElementById("sdate_"+id).value;
                if(edate!=""){

                    var endDate = getDate(edate);
                    var sDate=getDate(sdate);

                    if(new Date(sDate).getTime() > new Date(endDate).getTime()){
                        jAlert("<%=srbd.getString("CMS.var.sdateenddatecheck")%>","Payment Schedule", function(RetVal) {
                        });
                        document.getElementById("edate_"+id).value="";
                    }

                }
            }
        //            function checkqty(id){
        //                $('.err').remove();
        //                if(!regForNumber(document.getElementById('Qtyenter_'+id).value) || document.getElementById('Qtyenter_'+id).value.split(".")[1].length > 3 || document.getElementById('Qtyenter_'+id).value.split(".")[0].length > 12){
        //                    $("#Qtyenter_"+id).parent().append("<div class='err' style='color:red;'><%=srbd.getString("CMS.var.ratecheck")%></div>");
        //                    document.getElementById('Qtyenter_'+id).value="";
        //
        //                }
        //                calculateGrandTotal();
        //            }
        function positive(value)
        {
            return /^\d*$/.test(value);

        }
    function checkDays (id){
        $('.err').remove();
        var values = document.getElementById('noOfDays_'+id).value;
        if(!regForNumber(document.getElementById('noOfDays_'+id).value)){
            $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Days must be numeric value </div>");
            document.getElementById('noOfDays_'+id).value="";
        }else if(document.getElementById('noOfDays_'+id).value.indexOf("-")!=-1){
            $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Days must be positive value </div>");
            document.getElementById('noOfDays_'+id).value="";
        }else if(!positive(document.getElementById('noOfDays_'+id).value)){
            $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Decimal values are not allowed</div>");
            document.getElementById('noOfDays_'+id).value="";
        }else if(document.getElementById('noOfDays_'+id).value.length > 5){
            $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Maximum 5 digits are allowed </div>");
            document.getElementById('noOfDays_'+id).value="";
        }

    }

    function f_date(id){
        var firstDate = getDate(document.getElementById("startDt_"+id).value);
        var values = document.getElementById("noOfDays_"+id).value;
        var enddate = new Date(firstDate).getTime()+(parseInt(values)*24*60*60*1000);
        var finalenddate="";
        var monthname = new Array("Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec");
        var enddate = new Date(enddate);
        finalenddate = enddate.getDate()+"-"+monthname[enddate.getMonth()]+"-"+enddate.getFullYear();
        if(values != "")
        {
            document.getElementById("endDt_"+id).value = finalenddate;
            return finalenddate;
        }
    }

    function GetCal(txtname,controlname,count)
    {
        new Calendar({
            inputField: txtname,
            trigger: controlname,
            showTime: false,
            dateFormat:"%d-%b-%Y",
            onSelect: function() {
                var date = Calendar.intToDate(this.selection.get());
                LEFT_CAL.args.min = date;
                LEFT_CAL.redraw();
                // f_date(count);
                this.hide();
            }
        });

        var LEFT_CAL = Calendar.setup({
            weekNumbers: false
        })
    }
        </script>
        <script type="text/javascript">
    /* check if pageNO is disable or not */
    function chkdisble(pageNo){
        $('#dispPage').val(Number(pageNo));
        if(parseInt($('#pageNo').val(), 10) != 1){
            $('#btnFirst').removeAttr("disabled");
            $('#btnFirst').css('color', '#333');
        }

        if(parseInt($('#pageNo').val(), 10) == 1){
            $('#btnFirst').attr("disabled", "true");
            $('#btnFirst').css('color', 'gray');
        }


        if(parseInt($('#pageNo').val(), 10) == 1){
            $('#btnPrevious').attr("disabled", "true")
            $('#btnPrevious').css('color', 'gray');
        }

        if(parseInt($('#pageNo').val(), 10) > 1){
            $('#btnPrevious').removeAttr("disabled");
            $('#btnPrevious').css('color', '#333');
        }

        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
            $('#btnLast').attr("disabled", "true");
            $('#btnLast').css('color', 'gray');
        }

        else{
            $('#btnLast').removeAttr("disabled");
            $('#btnLast').css('color', '#333');
        }

        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
            $('#btnNext').attr("disabled", "true")
            $('#btnNext').css('color', 'gray');
        }
        else{
            $('#btnNext').removeAttr("disabled");
            $('#btnNext').css('color', '#333');
        }
    }
        </script>

        <script type="text/javascript">
    /*  Handle First click event */
    $(function() {
        $('#btnFirst').click(function() {
            var totalPages=parseInt($('#totalPages').val(),10);
            var pageNo =$('#pageNo').val();
            $('#first').val(0);
            if(totalPages>0 && pageNo!="1")
            {
                $('#pageNo').val("1");
                loadTable();
                $('#dispPage').val("1");
            }
        });
    });
        </script>
        <script type="text/javascript">
    /*  Handle Last Button click event */
    $(function() {
        $('#btnLast').click(function() {
            var totalPages=parseInt($('#totalPages').val(),10);
            var pageNo=parseInt($('#pageNo').val(),10);
            var size = (parseInt($('#size').val())*totalPages)-(parseInt($('#size').val()));
            $('#first').val(size);
            if(totalPages>0){
                $('#pageNo').val(totalPages);
                loadTable();
                $('#dispPage').val(totalPages);
            }
        });
    });



        </script>
        <script type="text/javascript">
    /*  Handle Next Button click event */
    $(function() {
        $('#btnNext').click(function() {
            var pageNo=parseInt($('#pageNo').val());
            var first = parseInt($('#first').val());
            var max = parseInt($('#size').val());
            $('#first').val(first+max);

            //$('#size').val(max+parseInt(document.getElementById("size").value));
            var totalPages=parseInt($('#totalPages').val(),10);

            if(pageNo <= totalPages) {

                loadTable();
                $('#pageNo').val(Number(pageNo)+1);
                $('#dispPage').val(Number(pageNo)+1);
            }
        });
    });

        </script>
        <script type="text/javascript">
    /*  Handle Previous click event */
    $(function() {
        $('#btnPrevious').click(function() {
            var pageNo=parseInt($('#pageNo').val(),10);
            var first = parseInt($('#first').val());
            var max = parseInt($('#size').val());
            $('#first').val(first-max);
            //$('#size').val(max+parseInt(document.getElementById("size").value));
            var totalPages=parseInt($('#totalPages').val(),10);

            $('#pageNo').val(Number(pageNo)-1);
            loadTable();
            $('#dispPage').val(Number(pageNo)-1);
        });
    });
        </script>
        <script type="text/javascript">
    var count=0;
    var addcount = 0;
    function Search(){
        var pageNo=parseInt($('#dispPage').val(),10);
        var totalPages=parseInt($('#totalPages').val(),10);

        if(pageNo > 0)
        {
            if(pageNo <= totalPages) {
                $('#pageNo').val(Number(pageNo));
                loadTable();
                $('#dispPage').val(Number(pageNo));
            }
        }
    }

    function checkRate(id){
                $('.err').remove();

                if(!regForNumber(document.getElementById('PercOfVal_'+id).value)){
                    $("#PercOfVal_"+id).parent().append("<div class='err' style='color:red;'>Only numeric values are allowed</div>");
                    document.getElementById('PercOfVal_'+id).value="";
                }else if(document.getElementById('PercOfVal_'+id).value.split(".")[1] != undefined){
                    if(document.getElementById('PercOfVal_'+id).value.split(".")[1].length > 3 || document.getElementById('PercOfVal_'+id).value.split(".")[0].length > 12){
                        $("#PercOfVal_"+id).parent().append("<div class='err' style='color:red;'><%=srbd.getString("CMS.var.ratecheck")%></div>");
                        document.getElementById('PercOfVal_'+id).value="";
                    }
                }else if(document.getElementById('PercOfVal_'+id).value.indexOf("-")!=-1){
                    $("#PercOfVal_"+id).parent().append("<div class='err' style='color:red;'>Only positve values are allowed</div>");
                    document.getElementById('PercOfVal_'+id).value="";
                }
            }
    $(function() {
        $('#btnGoto').click(function() {
            var pageNo=parseInt($('#dispPage').val(),10);
            var totalPages=parseInt($('#totalPages').val(),10);
            var size = (parseInt($('#size').val())*pageNo)-$('#size').val();
            $('#first').val(size);
            if(pageNo > 0)
            {
                if(pageNo <= totalPages) {
                    $('#pageNo').val(Number(pageNo));
                    loadTable();
                    $('#dispPage').val(Number(pageNo));
                }
            }

        });
    });
    function addrow()
    {
        addcount = document.getElementById("listcount").value;
        count = document.getElementById("listcount").value;
        var lumsumtxt = '<tr><td class="t-align-left">\
                    <input class="formTxtBox_1" type="checkbox" name="chk'+count+' " id="chk'+count+'" /></td>'+
            '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="srNo_'+count+'" style=width:40px id="srNo_'+count+'" /></td>'+
            '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="Milestone_'+count+'" id="Milestone_'+count+'" /></td>'+
            '<td class="t-align-left"><input class="formTxtBox_1" style=width:200px type="text" name="Descritpion_'+count+'" id="Descritpion_'+count+'" /></td>'+
            '<td class="t-align-left"><input class="formTxtBox_1" type="text" onChange=checkRate('+count+') name="PercOfVal_'+count+'" id="PercOfVal_'+count+'" /></td>'+
            '<td class="t-align-left"><input class="formTxtBox_1" type="text" readonly="true" onchange=f_date(" + i + ") onclick =GetCal("datePropByPE_'+count+'","datePropByPE_'+count+'",'+count+') name="datePropByPE_'+count+'" id="datePropByPE_'+count+'" />&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_'+count+' src=../resources/images/Dashboard/calendarIcn.png alt=Select a Date border= style=vertical-align:middle onclick =GetCal("datePropByPE_'+count+'","calc_'+count+'",'+count+') /></a></td>'+
            '<td class="t-align-left"><input class="formTxtBox_1" type="text" readonly="true" onchange=f_date(" + i + ") onclick =GetCal("datePropByCon_'+count+'","datePropByCon_'+count+'",'+count+') name="datePropByCon_'+count+'" id="datePropByCon_'+count+'" />&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc1_'+count+' src=../resources/images/Dashboard/calendarIcn.png alt=Select a Date border= style=vertical-align:middle onclick =GetCal("datePropByCon_'+count+'","calc1_'+count+'",'+count+') /></a></td>'+
            '<input type=hidden name="srvPsVarId_'+count+ '" id="srvPsVarId_'+count+'" value="" /></td>';

        var servicetxt = '<tr><td class="t-align-left">\
                    <input class="formTxtBox_1" type="checkbox" name="chk'+count+' " id="chk'+count+'" /></td>'+
            '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="srNo_'+count+'" style=width:40px id="srNo_'+count+'" /></td>'+
            '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="Descritpion_'+count+'" id="Descritpion_'+count+'" /></td>'+
            '<td class="t-align-left"><input class="formTxtBox_1" type="text" readonly="true" onchange=f_date(" + i + ") onclick =GetCal("datePropByPE_'+count+'","datePropByPE_'+count+'",'+count+') name="datePropByPE_'+count+'" id="datePropByPE_'+count+'" />&nbsp;<a href=javascript:void(0) title=Calender ><img id=datePropByPE_'+count+' src=../resources/images/Dashboard/calendarIcn.png alt=Select a Date border= style=vertical-align:middle onclick =GetCal("datePropByPE_'+count+'","datePropByPE_'+count+'",'+count+') /></a></td>'+
            '<td class="t-align-left"><input class="formTxtBox_1" type="text" readonly="true" onchange=f_date(" + i + ") onclick =GetCal("datePropByConid_'+count+'","datePropByConid_'+count+'",'+count+') name="datePropByConid_'+count+'" id="datePropByConid_'+count+'" />&nbsp;<a href=javascript:void(0) title=Calender ><img id=datePropByConid_'+count+' src=../resources/images/Dashboard/calendarIcn.png alt=Select a Date border= style=vertical-align:middle onclick =GetCal("datePropByConid_'+count+'","datePropByConid_'+count+'",'+count+') /></a></td>'+
            '<input type=hidden name="srvPsVarId_'+count+ '" id="srvPsVarId_'+count+'" value="" /></td>';

  
        // onblur=checkqtyin('+count+')
        $("#resultTable tr:last").after(lumsumtxt);
        count++;
        document.getElementById("listcount").value = count;
    }
    function delRow(){
        var counter = 0;
        var listcount = document.getElementById("listcount").value;
        var trackid="";
        for(var i=0;i<listcount;i++){
            if(document.getElementById("chk"+i)!=null){
                if(document.getElementById("chk"+i).checked){
                    if(document.getElementById("srvPsVarId_"+i).value != "")
                    {
                        trackid=trackid+document.getElementById("srvPsVarId_"+i).value+",";
                    }
                   // document.getElementById("listcount").value = listcount-1;

                }
            }
        }
        $(":checkbox[checked='true']").each(function(){
            if(document.getElementById("listcount")!= null){
                var curRow = $(this).parent('td').parent('tr');
                curRow.remove();
                count--;
                counter++;
            }
        });

        if(counter==0){
            jAlert("Please select at least one item to remove","Payment Schedule", function(RetVal) {
            });
        }
        else
        {
            $.post("<%=request.getContextPath()%>/CMSSerCaseServlet", {action:'deletesrvpsvariation', val: trackid,tenderId:<%=request.getParameter("tenderId")%> },  function(j){
                jAlert("Selected item(s) deleted successfully","Variation Order", function(RetVal) {
                });

            });
        }
    }
    function changetable(){
        var pageNo=parseInt($('#pageNo').val(),10);
        var first = parseInt($('#first').val());
        var max = parseInt($('#size').val());
        var totalPages=parseInt($('#totalPages').val(),10);
        loadTable();

    }

    function onFire(){
        var countt = document.getElementById("listcount").value;
                        //alert("total count :"+countt);
        var flag = true;
        for(var i=0;i<countt;i++){
            if(document.getElementById("srNo_"+i) == null)
                {
                    continue;
                }
            if(document.getElementById("srNo_"+i).value=="" || document.getElementById("Milestone_"+i).value=="" || document.getElementById("Descritpion_"+i).value=="" || document.getElementById("PercOfVal_"+i).value=="" || document.getElementById("datePropByPE_"+i).value=="" || document.getElementById("datePropByCon_"+i).value==""){
                jAlert("It is mandatory to specify all Groups","Payment Schedule Variation Order", function(RetVal) {
                });
                flag=false;
            }

        }
        if(flag){
            document.getElementById("addcount").value=document.getElementById("listcount").value;
            document.getElementById("action").value="addsrvpsvari";
            document.forms["frm"].submit();
        }else{
            return false;
        }

    }

        </script>

    </head>
    <body>
        <%
                    String referpage = request.getHeader("referer");
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String type = "";
                    int tenderId = 0;
                    int lotId = 0;
                    int formMapId = 0;
                    int varOrdId = 0;
                    int srvBoqId = 0;
                    String styleClass = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                    }
                    if (request.getParameter("formMapId") != null) {
                        formMapId = Integer.parseInt(request.getParameter("formMapId"));
                    }
                    if (request.getParameter("varOrdId") != null) {
                        varOrdId = Integer.parseInt(request.getParameter("varOrdId"));
                    }
                    if (request.getParameter("srvBoqId") != null) {
                        srvBoqId = Integer.parseInt(request.getParameter("srvBoqId"));
                    }
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                    boolean flag = false;
                    CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                    String serviceType = commService.getServiceTypeForTender(tenderId);
                    if ("Time based".equalsIgnoreCase(serviceType.toString())) {
                        flag = false;
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">
                        <%       out.print("Payment Schedule Variation Order");
                        %>
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" title="Go Back">Go Back</a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%if (request.getParameter("lotId") != null) {%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <%}%>
                    <form name="frm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post" >
                        <input type="hidden" name="addcount" id="addcount" value="" />
                        <input type="hidden" name="action" id="action" value="" />
                        <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>" />
                        <input type="hidden" name="SrvFormMapId" id="SrvFormMapId" value="<%=request.getParameter("formMapId")%>" />
                        <input type="hidden" name="lotId" id="lotId" value="<%=lotId%>" />
                        <input type="hidden" name="varOrdId" id="varOrdId" value="<%=varOrdId%>" />
                        <input type="hidden" name="srvBoqId" id="srvBoqId" value="<%=srvBoqId%>" />
                        <%
                            if(request.getParameter("isEdit")!=null){
                        %>
                            <input type="hidden" name="isEdit" id="isEdit" value="true" />
                        <%
                            }
                        %>
                        <div id="resultDiv" style="display: block;">
                            <div  id="print_area">

                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                    <tr><td colspan="7" style="text-align: right;">
                                            <a class="action-button-add" id="addRow" onclick="addrow()">Add Item</a>
                                            <a class="action-button-delete" id="delRow" onclick="delRow()">Remove Item</a>
                                        </td></tr>
                                    <tr>
                                        <th width="5%">Check</th>
                                        <th>S No</th>
                                        <%if (!flag) {%>
                                        <th>Milestone Name </th>
                                        <%}%>
                                        <th>Description</th>
                                        <%if (!flag) {%>
                                        <th>Payment as % of Contract Value</th>
                                        <%}%>
                                        <th>Mile Stone Date proposed by PE </th>
                                        <th>Mile Stone Date proposed by Consultant</th>
                                    </tr>
                                    <%
                                                List<TblCmsSrvPaymentSch> list = cmss.getPaymentScheduleData(formMapId);
                                                if (!list.isEmpty()) {
                                                    for (int i = 0; i < list.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                    %>
                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"></td>
                                        <td class="t-align-left"><%=list.get(i).getSrNo()%></td>
                                        <%if (!flag) {%>
                                        <td class="t-align-left"><%=list.get(i).getMilestone()%></td>
                                        <% }%>
                                        <td class="t-align-left"><%=list.get(i).getDescription()%></td>
                                        <%if (!flag) {%>
                                        <td style="text-align :right;"><%=list.get(i).getPercentOfCtrVal()%></td>
                                        <% }%>
                                        <td class="t-align-left"><%=DateUtils.customDateFormate(list.get(i).getPeenddate())%></td>
                                        <td class="t-align-left"><%=DateUtils.customDateFormate(list.get(i).getEndDate())%></td>
                                    </tr>
                                    <%}
                                                }%>


                                    <%    List<TblCmsSrvPsvari> listVarOrder = cmss.getSrvPsVariationOrder(varOrdId);



                                                if (listVarOrder != null && !listVarOrder.isEmpty()) {
                                                    int i = 0;
                                                    for (i = 0; i < listVarOrder.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                    %>
                                    <tr>
                                        <td> <input class="formTxtBox_1" type="checkbox" name="chk<%=i%>" id="chk<%=i%>" /></td>
                                        <td class="t-align-left"style="text-align: left"><input type="text" name="srNo_<%=i%>" id="srNo_<%=i%>" value="<%=listVarOrder.get(i).getSrNo()%>"/></td>
                                            <%if (!flag) {%>
                                        <td class="t-align-left"><input type="text" name="Milestone_<%=i%>" id="Milestone_<%=i%>" value="<%=listVarOrder.get(i).getMilestone()%>"/></td>
                                            <%}%>
                                        <td class="t-align-left"><input type="text" style="width:200px" name="Descritpion_<%=i%>" id="Descritpion_<%=i%>" value="<%=listVarOrder.get(i).getDescription()%>" /></td>
                                            <%if (!flag) {%>
                                        <td class="t-align-left" style="text-align: left"><input type="text" name="PercOfVal_<%=i%>" id="PercOfVal_<%=i%>" value="<%=listVarOrder.get(i).getPercentOfCtrVal().setScale(2)%>" onchange="checkRate(<%=i%>);" /></td>
                                            <%}%>

                                        <td class="t-align-left">
                                            <input name="datePropByPE_<%=i%>" type="text" class="formTxtBox_1" value="<%=DateUtils.customDateFormate(listVarOrder.get(i).getPeenddate())%>" readonly="true" id="datePropByPE_<%=i%>" style="width:100px;" onfocus="GetCal('datePropByPE_<%=i%>','datePropByPE_<%=i%>');"/>
                                            <a href="javascript:void(0);" onclick="" title="Calender"><img id="calc2_<%=i%>" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('datePropByPE_<%=i%>','calc2_<%=i%>');"/></a>
                                            <span id="datePropByPEspan<%=i%>" class="reqF_1"></span>
                                        </td>
                                        <td class="t-align-left">
                                            <input name="datePropByCon_<%=i%>" type="text" class="formTxtBox_1" value="<%=DateUtils.customDateFormate(listVarOrder.get(i).getEndDate())%>" readonly="true" id="datePropByCon_<%=i%>" style="width:100px;" onfocus="GetCal('datePropByCon_<%=i%>','datePropByCon_<%=i%>');"/>
                                            <a href="javascript:void(0);" onclick="" title="Calender"><img id="calc3_<%=i%>" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('datePropByCon_<%=i%>','calc3_<%=i%>');"/></a>
                                            <span id="datePropByConspan<%=i%>" class="reqF_1"></span>
                                        </td>

                                    <input type="hidden" value="<%=listVarOrder.get(i).getSrvPsvariId()%>" name="srvPsVarId_<%=i%>" id="srvPsVarId_<%=i%>" />

                                    </tr>
                                    <%}
                                                    out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                                                } else {
                                                    out.print("<input type=hidden name=listcount id=listcount value=0 />");
                                                }
                                    %>
                                    <input type="hidden" name="forms" <%if (flag) {%>value="timebase"<%} else {%>value="lumpsum"<%}%> />
                                </table>
                                <table width="100%" cellspacing="0" class="t_space">
                                    <tr><td colspan="7" style="text-align: right;">
                                            <a class="action-button-add" id="addRow" onclick="addrow()">Add item</a>
                                            <a class="action-button-delete" id="delRow" onclick="delRow()">Remove item</a>
                                        </td></tr>
                                </table>
                            </div>
                        </div>


                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" id="first" value="0"/>
                        <br />
                        <center>
                            <label class="formBtn_1">
                                <input type="button" name="swpbuttonadd" id="swpbuttonadd" value="Submit" onclick="onFire();" />
                            </label>
                        </center>
                    </form>
                </div>

                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>



