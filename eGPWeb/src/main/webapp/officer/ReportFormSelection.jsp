<%-- 
    Document   : ReportFormSelection
    Created on : Dec 22, 2010, 11:15:25 AM
    Author     : TaherT
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.web.servicebean.ReportCreationSrBean" %>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Select Form</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                    $('#search').submit(function() {
                        var vbool=true;
                         var cnt=$('#cnt').val();
                         var chk=0;
                         for(var i=0;i<cnt;i++){
                            if($('#formId'+i).attr("checked")){                                
                                chk++;
                            }
                         }
                         if(chk==0){
                              jAlert("Please Select Form.","Form Selection Alert", function(RetVal) {
                              });
                             vbool = false;
                         }
                         if(chk!=0 && $('#hdnFormId').val()!="0"){
                             vbool = confirm('Change in selection of forms will delete the report formula. Do you want to proceed further?');
                         }
                         return vbool;
                    });
                });
        </script>
    </head>
    <body>
        <%
            String reportType="TOR/TER";
            if(request.getParameter("reportType")!=null)
            {
                reportType=request.getParameter("reportType");
            }
           
             String tenderid = request.getParameter("tenderid");
             String repId = request.getParameter("repId");
             String plId=request.getParameter("plId");
             ReportCreationSrBean rcsb = new ReportCreationSrBean();
             boolean isNot=false;
             List<Object> forms = null;
             if("y".equals(request.getParameter("isNot"))){
                isNot=true;
             }
             if("Build Formula".equalsIgnoreCase(request.getParameter("btnFormula"))){
                rcsb.selectFormForRep(repId,request.getParameterValues("formId"),isNot);
                response.sendRedirect(request.getParameter("pageName")+".jsp?tenderid="+tenderid+"&repId="+repId+"&reportType="+reportType);
             }
             if(isNot){
                forms = rcsb.getFormsForReport(repId);
             }
             java.util.List<Object[]> list = rcsb.findFormsForReport(tenderid,plId);
             String htmlValue=null;
             String pageNameT=null;
             if(!list.isEmpty()){
                 if(list.get(0)[2].toString().equalsIgnoreCase("item wise")){
                    htmlValue="radio";
                    pageNameT="ItemWiseFormula";
                 }else{
                    htmlValue="checkbox";
                    pageNameT="LotWiseFormula";
                 }
             }
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <%                   
                    pageContext.setAttribute("tenderId", tenderid);
        %>
        <div class="pageHead_1">Select Form<span style="float: right;"><a class="action-button-goback" href="Notice.jsp?tenderid=<%=tenderid%>">Go back to Dashboard</a></span></div>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <input type="hidden" id="hdnFormId" value="<%if(forms!=null && (!forms.isEmpty())){out.print(forms.size());}else{out.print("0");}%>"/>
        <form action="ReportFormSelection.jsp?tenderid=<%=tenderid%>&repId=<%=repId%>&reportType=<%=reportType%><%if(isNot){out.print("&isNot=y");}%>" method="post" id="search">
            <div class="tabPanelArea_1">
                <div class="tableList_1 t-align-right">

                    Field Marked (<span class="mandatory">*</span>) is Mandatory
                </div>              
                <table class="tableList_1 t_space" width="100%" cellspacing="0">
                    <tbody><tr>
                            <th colspan="2" class="t-align-left ff">Select form for Formula</th>
                        </tr>
                        <tr>
                            <th class="t-align-left ff">Select</th>
                            <th>Form Name</th>

                        </tr>
                        <%int i=0;for(Object[] objects : list){%>
                        <tr>
                            <td class="t-align-left" width="6%">
                                <input name="formId" id="formId<%=i%>" type="<%=htmlValue%>" value="<%=objects[0]%>" <%if(forms!=null){for(Object form : forms){if(objects[0].toString().equals(form.toString())){out.print("checked");}}}%>/>
                                <!--<label for="checkbox"></label>-->
                            </td>
                            <td class="t-align-left" width="94%"><%=objects[1]%></td>
                        </tr>
                        <%i++;}%>
                        <%if(list.isEmpty()){out.print("<tr><td colspan='2' class='t-align-center' style='color:red;'>No BOQ Forms available.</td></tr>");}%>
                    </tbody></table>
                    <input type="hidden" id="cnt" value="<%=list.size()%>">
                    <%if(htmlValue!=null){%>
                    <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input name="btnFormula" id="button3" value="Build Formula" type="submit">
                    </label>
                        <input type="hidden" value="<%=pageNameT%>" name="pageName"/>
                    <%if(isNot && forms!=null && !forms.isEmpty()){%>
                    <a id="refNextBtn" href="<%=pageNameT%>.jsp?tenderid=<%=tenderid%>&repId=<%=repId%>" class="anchorLink">Next</a>
                    <script type="text/javascript">
                        var refNextBtnCnt = 0;
                        $(":checkbox[checked='true']").each(function(){
                            refNextBtnCnt++;
                        });
                        if(refNextBtnCnt==0){
                            $('#refNextBtn').hide();
                        }
                    </script>
                    <%}%>
                 </div>
                 <%}%>            
            </div>
        </form>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
