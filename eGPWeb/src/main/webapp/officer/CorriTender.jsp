<%--
    Document   : CorriTender
    Created on : Nov 27, 2010, 3:11:46 PM
    Author     : TaherT, Rajesh Singh,Rishita
--%>

<%@page import="java.math.BigDecimal"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblCorrigendumDetail"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<jsp:useBean id="configDocumentFeesSrBean" class="com.cptu.egp.eps.web.servicebean.ConfigDocumentFeesSrBean"/>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ConfigDocumentFeesService" %>
<html>
    <head>
        <%
                    int i = 0;
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Prepare Amendment / Corrigendum</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>


        <%--<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />--%>

        <%--Start -- This is done by Rajesh--%>
        <script type="text/javascript">
            var holiArray = new Array();
            function numeric(value) {
                return /^\d+$/.test(value);
            }
            //Start Dohatec for ICT
            function chkAmountLotBlank(obj){
                    var boolcheck7='true';
                    var i = (obj.id.substr(obj.id.indexOf("_")+1));

                    if(obj.value!='')
                    {
                        if(numeric(obj.value))
                        {
                            document.getElementById("amountLot_"+i).innerHTML="";
                            //if(obj.value!='0')
                            //{
                                obj.value = ($.trim(obj.value) * 1);
                                boolcheck7='true';
                                tenderSecAmt(obj);
                            /*}
                            else
                            {
                                boolcheck7='false';
                                document.getElementById("amountLot_"+i).innerHTML = "<br/>Only 0 value is not allowed";
                                $(".tenderSecAmtInWords_" + i).remove();
                            }*/

                            if(obj.value=='0')
                            {
                                document.getElementById("tenderSecurityAmountUSD_"+i).value = '0';
                                document.getElementById("tenderSecurityAmountUSD_"+i).readOnly = true;
                                document.getElementById("amountLotUSD_"+i).innerHTML="";
                                $(".tenderSecAmtInWordsUSD_" + i).remove();
                            }
                            else
                            {
                                if(document.getElementById("tenderSecurityAmountUSD_"+i).readOnly)
                                {
                                    document.getElementById("tenderSecurityAmountUSD_"+i).removeAttribute("readonly");
                                    document.getElementById("tenderSecurityAmountUSD_"+i).value = '';
                                    document.getElementById("amountLotUSD_"+i).innerHTML="";
                                    $(".tenderSecAmtInWordsUSD_" + i).remove();
                                }
                            }
                        }
                        else
                        {
                            document.getElementById("amountLot_"+i).innerHTML="<br/>Please enter Numeric Data";
                            var temp = obj.id.split("_");
                            var countTSA = temp[1];
                            $(".tenderSecAmtInWords_" + i).remove();
                            boolcheck7='false';
                        }

                    }
                    else{
                        document.getElementById("amountLot_"+i).innerHTML = "<br/>Please enter Tender Security Amount";
                        var temp = obj.id.split("_");
                        var countTSA = temp[1];
                        $(".tenderSecAmtInWords_" + i).remove();
                        boolcheck7='false';
                    }

                    if(boolcheck7=='false'){
                        document.getElementById("boolcheck7").value=boolcheck7;
                    }
                    else
                    {
                        document.getElementById("boolcheck7").value=boolcheck7;
                    }
                }

            function chkAmountLotBlankUSD(obj){
                    var boolcheckUSD='true';
                    var i = (obj.id.substr(obj.id.indexOf("_")+1));
                    if(obj.value!='')
                    {
                        if(numeric(obj.value))
                        {
                            document.getElementById("amountLotUSD_"+i).innerHTML="";
                            //if(obj.value!='0')
                            //{
                                obj.value = ($.trim(obj.value) * 1);
                                boolcheck7='true';
                                tenderSecAmtUSD(obj);
                            /*}
                            else
                            {
                                boolcheck7='false';
                                document.getElementById("amountLotUSD_"+i).innerHTML = "<br/>Only 0 value is not allowed";
                                $(".tenderSecAmtInWordsUSD_" + i).remove();
                            }*/
                        }
                        else
                        {
                            document.getElementById("amountLotUSD_"+i).innerHTML="<br/>Please enter Numeric Data";
                            var temp = obj.id.split("_");
                            var countTSA = temp[1];
                            $(".tenderSecAmtInWordsUSD_" + i).remove();
                            boolcheckUSD='false';
                        }

                    }
                    else{
                        document.getElementById("amountLotUSD_"+i).innerHTML = "<br/>Please enter Tender Security Amount";
                        var temp = obj.id.split("_");
                        var countTSA = temp[1];
                        $(".tenderSecAmtInWordsUSD_" + i).remove();
                        boolcheckUSD='false';
                    }

                    if(boolcheckUSD=='false'){
                        document.getElementById("boolcheckUSD").value=boolcheckUSD;
                    }
                    else
                    {
                        document.getElementById("boolcheckUSD").value=boolcheckUSD;
                    }
                }
                //End Dohatec for ICT

            //Function for Required
            function required(controlid)
            {
                var temp=controlid.length;
                if(temp <= 0 ){
                    return false;
                }else{
                    return true;
                }
            }

            //Function for MaxLength
            function Maxlenght(controlid,maxlenght)
            {
                var temp=controlid.length;
                if(temp>=maxlenght){
                    return false;
                }else
                    return true;
            }

            //Function for digits
            function digits(control) {
                return /^\d+$/.test(control);
            }

            //Function for CompareToForToday
            function CompareToForToday(first)
            {
                var mdy = first.split('/')  //Date and month split
                var mdyhr = mdy[2].split(' ');  //Year and time split
                var mdyhrtime = mdyhr[1].split(':');
                if (mdyhrtime[1] == undefined) {
                    var valuedate = new Date(mdyhr[0], mdy[1] - 1, mdy[0]);
                } else
                {
                    var valuedate = new Date(mdyhr[0], mdy[1] - 1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                }

                var d = new Date();
                if (mdyhrtime[1] == undefined) {
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                } else
                {
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes());
                }
                return Date.parse(valuedate) > Date.parse(todaydate);
            }
            
            function CompareHoliday(first)
            {
                var mdy = first.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split
                var mdyhrtime=mdyhr[1].split(':');
                if(mdyhrtime[1] == undefined){
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                }else
                {
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                }
                var result = valuedate.toString();
                return result.split(" ")[0];
            }
            
            function findHoliday3(compVal) {
                            //var compVal = comp.value;
                            var cnt = 0;
                            if (compVal != null && compVal != "") {
                                for (var i = 0; i < holiArray.length; i++) {
                                    if (CompareToForEqual(holiArray[i], compVal)) {
                                        cnt++;
                                    }
                                }
                                
                            
                                
                            }
                            
                            if (cnt != 0) {
                                return true;
                            }
                            else 
                            {
                                return false;
                            }
                            
                        }
            
            
            
            function openCloseWeekend()
            {
                var first = document.getElementById('txtpreQualCloseDate').value;
                var mdy = first.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split
                var mdyhrtime=mdyhr[1].split(':');
                if(mdyhrtime[1] == undefined){
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                }else
                {
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                }
                var result = valuedate.toString();
                var result2 = result.split(" ")[0];
                if(result2=="Sat" || result2=="Sun")
                {
                    document.getElementById("demoClose").innerHTML = "Weekend!";
                    document.getElementById("demoOpen").innerHTML = "Weekend!";
                }
                else
                {
                    document.getElementById("demoClose").innerHTML = "";
                    document.getElementById("demoOpen").innerHTML = "";
                }
            }
            
            
            
            function docSellHoliday()
            {
                var first = document.getElementById('txttenderLastSellDate').value;
                var mdy = first.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split
                var mdyhrtime=mdyhr[1].split(':');
                if(mdyhrtime[1] == undefined){
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                }else
                {
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                }
                var result = valuedate.toString();
                var result2 = result.split(" ")[0];
                if(result2=="Sat" || result2=="Sun")
                {
                    document.getElementById("demoDocSell").innerHTML = "Weekend!";
                }
                else
                {
                    document.getElementById("demoDocSell").innerHTML = "";
                }
            }
            function secSubHoliday()
            {
                var first = document.getElementById('txtlastDateTenderSub').value;
                var mdy = first.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split
                var mdyhrtime=mdyhr[1].split(':');
                if(mdyhrtime[1] == undefined){
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                }else
                {
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                }
                var result = valuedate.toString();
                var result2 = result.split(" ")[0];
                if(result2=="Sat" || result2=="Sun")
                {
                    document.getElementById("demoSecSub").innerHTML = "Weekend!";
                }
                else
                {
                    document.getElementById("demoSecSub").innerHTML = "";
                }
            }

            function meetStartHoliday()
            {
                var first = document.getElementById('txtpreTenderMeetStartDate').value;
                var mdy = first.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split
                var mdyhrtime=mdyhr[1].split(':');
                if(mdyhrtime[1] == undefined){
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                }else
                {
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                }
                var result = valuedate.toString();
                var result2 = result.split(" ")[0];
                if(result2=="Sat" || result2=="Sun")
                {
                    document.getElementById("demoMeetStart").innerHTML = "Weekend!";
                }
                else
                {
                    document.getElementById("demoMeetStart").innerHTML = "";
                }
            }

            function meetEndHoliday()
            {
                var first = document.getElementById('txtpreTenderMeetEndDate').value;
                var mdy = first.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split
                var mdyhrtime=mdyhr[1].split(':');
                if(mdyhrtime[1] == undefined){
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                }else
                {
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                }
                var result = valuedate.toString();
                var result2 = result.split(" ")[0];
                if(result2=="Sat" || result2=="Sun")
                {
                    document.getElementById("demoMeetEnd").innerHTML = "Weekend!";
                }
                else
                {
                    document.getElementById("demoMeetEnd").innerHTML = "";
                }
            }
            
            
            function CompareToForEqual(value,params)
            {

                var mdy = value.split('/')  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');
                var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                return Date.parse(date) == Date.parse(datep);
            }

            //Function for CompareToForGreater
            function CompareToForGreater(value,params)
            {
                if(value!='' && params!=''){

                    var mdy = value.split('/')  //Date and month split
                    var mdyhr=mdy[2].split(' ');  //Year and time split
                    var mdyp = params.split('/')
                    var mdyphr=mdyp[2].split(' ');


                    if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                    {
                        //alert('Both Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                    }
                    else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                    {
                        //alert('Both DateTime');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], parseFloat(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], parseFloat(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('one Date and One DateTime');
                        var a = mdyhr[1];  //time
                        var b = mdyphr[1]; // time

                        if(a == undefined && b != undefined)
                        {
                            //alert('First Date');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                            var mdyphrsec=mdyphr[1].split(':');
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                        }
                        else
                        {
                            //alert('Second Date');
                            var mdyhrsec=mdyhr[1].split(':');
                            var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                        }
                    }

                    return Date.parse(date) > Date.parse(datep);
                }
                else
                {
                    return false;
                }
            }

            //Function for CompareToWithoutEqual
            function CompareToWithoutEqual(value,params)
            {
                var mdy = value.split('/')  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');


                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                {
                    //alert('Both Date');
                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                }
                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                {
                    //alert('Both DateTime');
                    var mdyhrsec=mdyhr[1].split(':');
                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec=mdyphr[1].split(':');
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                }
                else
                {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if(a == undefined && b != undefined)
                    {
                        //alert('First Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('Second Date');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                    }
                }
                return Date.parse(date) > Date.parse(datep);
            }


            //Function for CompareToWithoutEqual
            function CompareToWithEqual(value,params)
            {
                var mdy = value.split('/');  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');


                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                {
                    //alert('Both Date');
                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                }
                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                {
                    //alert('Both DateTime');
                    var mdyhrsec=mdyhr[1].split(':');
                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec=mdyphr[1].split(':');
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                }
                else
                {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if(a == undefined && b != undefined)
                    {
                        //alert('First Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('Second Date');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                    }
                }
                return Date.parse(date) >= Date.parse(datep);
            }
            //

            //Function for CompareToForGreater
            function CompareToForSmaller(value,params)
            {
                if(value!='' && params!=''){

                    var mdy = value.split('/')  //Date and month split
                    var mdyhr=mdy[2].split(' ');  //Year and time split
                    var mdyp = params.split('/')
                    var mdyphr=mdyp[2].split(' ');


                    if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                    {
                        //alert('Both Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                    }
                    else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                    {
                        //alert('Both DateTime');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], parseFloat(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], parseFloat(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('one Date and One DateTime');
                        var a = mdyhr[1];  //time
                        var b = mdyphr[1]; // time

                        if(a == undefined && b != undefined)
                        {
                            //alert('First Date');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                            var mdyphrsec=mdyphr[1].split(':');
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                        }
                        else
                        {
                            //alert('Second Date');
                            var mdyhrsec=mdyhr[1].split(':');
                            var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                        }
                    }

                    return Date.parse(date) < Date.parse(datep);
                }
                else
                {
                    return false;
                }
            }

        </script>
        <%--End -- This is done by Rajesh--%>


        <script type="text/javascript">
            function regForSpace(value){
                return /^\s+|\s+$/g.test(value);
            }
            //Pre-Qualification document price (TK) :
            //
            function docFeeValidate(chk){
               var boolcheck1='true';
                var id =chk.id.substring(10, chk.id.length);
                if(chk.value==""){//regForSpace(chk.value) &&
                    document.getElementById('divdocfee'+id).innerHTML="Please enter Document Fees(Amount in Nu.)";
                    var temp = chk.id.substring(10, chk.id.length)
                    var countDF = temp;
                    $(".docFeesInWords_" + countDF).remove();
                    boolcheck1=false;
                }else if(regForSpace(chk.value)){
                    document.getElementById('divdocfee'+id).innerHTML="Please enter Document Fees(Amount in Nu.)";
                    var temp = chk.id.substring(10, chk.id.length);
                    var countDF = temp;
                    $(".docFeesInWords_" + countDF).remove();
                    boolcheck1=false;
                }else{
                    if(numeric(chk.value))
                        {
                            document.getElementById('divdocfee'+id).innerHTML='';
                            boolcheck1='true';
                            docFeesWord(chk);
                        }
                        else
                        {
                            document.getElementById('divdocfee'+id).innerHTML="Please enter numeric Data";

                            var temp = chk.id.substring(10, chk.id.length)
                            var countDF = temp;
                            $(".docFeesInWords_" + countDF).remove();
                            boolcheck1=false;
                        }
                }

                    if(boolcheck1==false){
                       document.getElementById("boolcheck").value=boolcheck1;
                    }else
                    {
                       document.getElementById("boolcheck").value='true';
                    }
            }

            function inStartValidate(chk){
                var boolcheck2='true';
                var id =chk.id.substring(22, chk.id.length);
                var hdnField = document.getElementById('hdnindicativeStartDate'+id).value;
                if(chk.value==""){
                    document.getElementById('divisd'+id).innerHTML="Please enter indicative start date";
                    boolcheck2=false;
                }else{
                    if(!CompareToWithEqual(chk.value,hdnField)){
                        document.getElementById('divisd'+id).innerHTML="Date can\'t be pre pone";
                        boolcheck2=false;
                    }else{
                        document.getElementById('divisd'+id).innerHTML='';
                        boolcheck2='true';
                    }
                }
                if(boolcheck2==false){
                                document.getElementById("boolcheck").value=boolcheck2;
                            }else
                                {
                                    document.getElementById("boolcheck").value='true';
                                }
            }
            function inEndValidate(chk){
                var boolcheck3='true';
                var id =chk.id.substring(22, chk.id.length);
                var hdnField = document.getElementById('hdnindicativeComplDate'+id).value;
                var startField = document.getElementById('txtindicativeStartDate'+id).value;
                if(chk.value==""){
                    document.getElementById('divied'+id).innerHTML="Please enter indicative end date";
                    boolcheck3=false;
                }else{
                    if(!CompareToWithEqual(chk.value,hdnField)){
                        document.getElementById('divied'+id).innerHTML="Date can\'t be pre pone";
                        boolcheck3=false;
                    }else{
                        if(!CompareToForGreater1(chk.value,startField)){
                            document.getElementById('divied'+id).innerHTML="Indicative end date is smaller than start date";
                            boolcheck3=false;
                        }else{
                            document.getElementById('divied'+id).innerHTML='';
                            boolcheck3='true';
                        }
                    }
                }
                if(boolcheck3==false){
                   document.getElementById("boolcheck").value=boolcheck3;
                }else
                   {
                       document.getElementById("boolcheck").value='true';
                   }
            }

        </script>
        <%--END --This is blur event%>--%>

        <script type="text/javascript">


            function calcDays(date1,date2){
                var dat1 = date1.split("/");
                var dt1=dat1[2].split(" ");
                var dat2 = date2.split("/");
                var dt2=dat2[2].split(" ");
                var sDate = new Date(dt1[0]+"/"+dat1[1]+"/"+dat1[0]);
                var eDate = new Date(dt2[0]+"/"+dat2[1]+"/"+dat2[0]);
                var daysApart = Math.abs(Math.round((sDate-eDate)/86400000));
                return daysApart;
            }
            function inStartValSub(){
                var vbool1=true;
                for(var i=0;i<document.getElementById('pcount').value;i++){
                    var hdnField = document.getElementById('hdnindicativeStartDate'+i).value;
                    if(document.getElementById('txtindicativeStartDate'+i).value==""){
                        document.getElementById('divisd'+i).innerHTML="Please enter indicative start date";
                        vbool1=false;
                    }else{
                        if(!CompareToWithEqual(document.getElementById('txtindicativeStartDate'+i).value,hdnField)){
                            document.getElementById('divisd'+i).innerHTML="Date can\'t be pre pone";
                            vbool1=false;
                        }else{
                            vbool1=true;
                            document.getElementById('divisd'+i).innerHTML='';
                        }
                    }
                }
                if(vbool1){
                    inEndValSub();
                }else{
                    document.getElementById("boolcheck").value=false;
                    document.getElementById('stencheck').value="false";
                }
            }

            function inEndValSub(){ //rajesh
                var vbool2=true;
                for(var i=0;i<document.getElementById('pcount').value;i++){
                    var hdnField = document.getElementById('hdnindicativeComplDate'+i).value;
                    var startField = document.getElementById('txtindicativeStartDate'+i).value;
                    if(document.getElementById('txtindicativeComplDate'+i).value==""){
                        document.getElementById('divied'+i).innerHTML="Please enter indicative end date";
                        vbool2=false;
                    }else{
                        if(!CompareToWithEqual(document.getElementById('txtindicativeComplDate'+i).value,hdnField)){
                            document.getElementById('divied'+i).innerHTML="Date can\'t be pre pone";
                            vbool2=false;
                        }else{
                            if(!CompareToForGreater1(document.getElementById('txtindicativeComplDate'+i).value,startField)){
                                document.getElementById('divied'+i).innerHTML="Indicative end date is smaller than start date";
                                vbool2=false;
                            }else{
                                vbool2=true;
                                document.getElementById('divied'+i).innerHTML='';
                            }
                        }
                    }
                }
                if(!vbool2){
                    document.getElementById('stencheck').value="false";
                    document.getElementById("boolcheck").value=false;
                }
            }
            function CompareToForGreater1(value,params)
            {

                var mdy = value.split('/')  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');


                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                {
                    //alert('Both Date');
                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                }
                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                {
                    //alert('Both DateTime');
                    var mdyhrsec=mdyhr[1].split(':');
                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec=mdyphr[1].split(':');
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                }
                else
                {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if(a == undefined && b != undefined)
                    {
                        //alert('First Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('Second Date');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                    }
                }

                return Date.parse(date) > Date.parse(datep);
            }
        </script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            function GetCalWithouTime(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
    </head>
    <body onload="MakePreBidEndDateDisable();">
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <%
                    TenderSrBean tenderSrBean = new TenderSrBean();
                    TenderCommonService tenderCommonService = (TenderCommonService) com.cptu.egp.eps.web.utility.AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> holidayList = tenderCommonService.returndata("getHolidayDatesBD", null, null);
                    out.print("<script type='text/javascript'>");
                    for(SPTenderCommonData holidays : holidayList){
                        out.print("holiArray.push('"+holidays.getFieldName1()+"');");
                    }
                    out.print("</script>");
                    int lotno = 0;
                    String tenid = "";
                    int phasingCounter = 0;
                    if (!request.getParameter("id").equals("")) {
                        tenid = request.getParameter("id");
                    }

                    String userid = "";
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userid = hs.getAttribute("userId").toString();
                    }
                    java.util.List<TblCorrigendumDetail> corrigendumDetail = null;
                    boolean isEdit = false;
                    if ("y".equals(request.getParameter("isedit"))) {
                        corrigendumDetail = tenderSrBean.corrigendumDetails(Integer.parseInt(request.getParameter("corrid")));
                        isEdit = true;

                    }
                    //pageContext.setAttribute("tenderId", tenid);
                    String id = request.getParameter("id");
        %>
        <input type="hidden" id="boolcheck" value="true"/>
        <input type="hidden" id="stencheck" value="true"/>
        <input type="hidden" id="boolcheckUSD" value="true"/>

        <%     /* Dohatec Start - rokib*/
            ConfigDocumentFeesService configDocumentFeesService = (ConfigDocumentFeesService) AppContext.getSpringBean("ConfigDocumentFeesService");
                /* Dohatec End - rokib*/
        %>

        <div class="mainDiv">
            <div class="contentArea_1">
                <%--<%@include  file="../resources/common/AfterLoginTop.jsp"%>--%>

                <div class="pageHead_1">Prepare Amendment / Corrigendum
                    <span style="float:right;"> <a class="action-button-goback" href="Amendment.jsp?tenderid=<%=tenid%>">Go back to Dashboard</a> </span>

                </div>
                <form id="frmCreateTender" name="frmCreateTender" method="POST" action="CorriInset.jsp">
                    <input type="hidden" id="boolcheck7" value="true"/>
                    <input type="hidden" value="<%=request.getParameter("corrid")%>" name="corrid"/>
                    <input type="hidden" value="<%=DateUtils.formatStdDate(new java.util.Date())%>" name="currDate" id="hdnCurrDate"/>
                    <%Object[] objects = tenderSrBean.findTenderRuleEngine(tenid);
                                if (objects != null) {%>
                    <input type="hidden" value="<%=objects[0]%>" name="minExtDays" id="hdnMinExtDays"/>
                    <input type="hidden" value="<%=objects[1]%>" name="timeAllowed" id="hdnTimeAllowed"/>
                    <%}%>
                    <%--<%@include file="../resources/common/TenderInfoBar.jsp" %>--%>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <%
                                    for (CommonTenderDetails commonTenderDetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "tender")) {
                                        List<Object[]> obj = tenderSrBean.getConfiForTender(Integer.parseInt(id));
                        %>
                        <tr>
                            <td style="font-style: italic" colspan="4" class="ff t-align-left" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                        </tr>
                        <tr>
                            <td width="25%" class="ff">Ministry :</td>
                            <td width="25%"><label id="lblMinistry"><%=commonTenderDetails.getMinistry()%></label>
                            </td>
                            <%--         <td width="25%" class="ff">Division :</td>
                            <td width="25%"><label id="lblDivision"><%=commonTenderDetails.getDivision()%></label></td>
                        </tr>
                        <tr>
                            <td class="ff">Organization :</td>
<td><%=commonTenderDetails.getAgency()%></td> --%>
                            <td class="ff">Procuring Agency Name :</td>
                            <td><%=commonTenderDetails.getPeOfficeName()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Procuring Agency Code :</td>
                            <td><%=commonTenderDetails.getPeCode()%></td>
                            <td class="ff">Procuring Agency Dzongkhag / District :</td>
                            <td><%=commonTenderDetails.getPeDistrict()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Procurement Category :</td>
                            <td><%=commonTenderDetails.getProcurementNature()%></td>
                            <td class="ff">Procurement Type :</td>
                            <td><%=commonTenderDetails.getProcurementType()%>
                                 <input type="hidden" value="<%=configDocumentFeesService.getMaxDocumentPriceInBDT(commonTenderDetails.getProcurementType())%>" id="docPriceBDTMaxID" name="docPriceBDTMaxID"/>
                                 <input type="hidden" value="<%=configDocumentFeesService.getMaxDocumentPriceInUSD(commonTenderDetails.getProcurementType())%>" id="docPriceUSDMaxID" name="docPriceUSDMaxID"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Event Type  :</td>
                            <td><% if (commonTenderDetails.getEventType() != null) {%><%=commonTenderDetails.getEventType()%><% }%></td>
                            <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {%> <td class="ff">Invitation for :</td>
                            <td><% if (commonTenderDetails.getInvitationFor() != null) {%><%=commonTenderDetails.getInvitationFor()%><% }%></td> <% }%>
                        </tr><% if (commonTenderDetails.getProcurementNature().equals("Services")) {
                                                                    String msg = "";
                                                                    if (commonTenderDetails.getEventType() != null) {
                                                                        if (commonTenderDetails.getEventType().equals("REOI")) {
                                                                            msg = "Request for expression of interest for selection of";
                                                                        } else if (commonTenderDetails.getEventType().equals("RFP")) {
                                                                            msg = "RFP for selection of";
                                                                        }
                                                                    }
                        %>
                        <tr><% if (!msg.equals("")) {%>
                            <td class="ff"><%=msg%></td>
                            <td><label><%=commonTenderDetails.getReoiRfpFor()%></label><%--<input name="expInterestSel" type="text" class="formTxtBox_1" id="txtexpInterestSel" style="width:200px;"  />--%></td><%}%>
                            <td class="ff">Contract Type : </td>
                            <td><%=commonTenderDetails.getContractType()%></td>
                        </tr><% }%>
                        <tr><%
                                                                String msgTender = "";
                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")
|| commonTenderDetails.getEventType().contains("TSTM")
) {
                                                                    msgTender = "Invitation Reference No.";
                                                                } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                                                    msgTender = "REOI No.";
                                                                } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {
                                                                    msgTender = "RFP No.";
                                                                }else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {
                                                                    msgTender = "RFA No.";
                                                                }
                            %>
                            <td class="ff"><%=msgTender%><% if (!msgTender.equals("")) {%> : </td>
                            <td><%=commonTenderDetails.getReoiRfpRefNo()%></td><% }%>
                            <td class="ff"></td>
                            <td></td>
                        </tr>
                    </table>

                    <div class="tableHead_22">Key Information and Funding Information :</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                        <tr>
                            <td width="25%" class="ff">Procurement Method : </td>
                            <td width="25%">
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQ")){%><%="Limited Enquiry Method (LEM)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM")){%><%="Open Tendering Method (OTM)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LTM")){%><%="Limited Tendering Method (LTM)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("TSTM")){%><%="Two Stage Tendering Method (TSTM)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("QCBS")){%><%="Quality Cost Based Selection (QCBS)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LCS")){%><%="Least Cost Selection (LCS)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SFB")){%><%="Selection under a Fixed Budget (SFB)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("DC")){%><%="Design Contest (DC)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SBCQ")){%><%="Selection Based on Consultants Qualification (SBCQ)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SSS")){%><%="Single Source Selection (SSS)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("IC")){%><%="Selection of Independent Individual Consultant(IC)"%><%}%>
                               <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("CSO")){%><%="Community Service Organisation (CSO)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("DPM")){%><%="Direct Contracting Method (DCM)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OSTETM")){%><%="One stage Two Envelopes Tendering Method (OSTETM)"%><%}%> 
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQU")){%><%="Request For Quotation (RFQ)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQL")){%><%="Request For Quotation (RFQ)"%><%}%>
                               <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("FC")){%><%="Framework Contract(FC)"%><%}%>
                            </td>
                            <td width="25%" class="ff">Budget Type :</td>
                            <td width="25%"><%=commonTenderDetails.getBudgetType()%></td>
                        </tr>
                        <tr>
                            <td width="25%" class="ff">Source of Funds :</td>
                            <td width="25%"><%=commonTenderDetails.getSourceOfFund()%></td>
                            <% if (commonTenderDetails.getDevPartners() != null) {%>
                                <td width="25%" class="ff">Development Partner :</td>
                                <td width="25%"><%=commonTenderDetails.getDevPartners()%></td>
                                <% } %>
                        </tr>
                        <%
                                List<Object> allocatedCost=tenderSrBean.getAllocatedCost(Integer.parseInt(id));
                             if (!allocatedCost.isEmpty() && !(allocatedCost.get(0).toString().startsWith("0.00"))) {
                                if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LTM") || commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SFB")){
                                %>
                            <tr>

                                <td width="25%" class="ff">Allocated Budget Amount in Tk. :</td>
                                <td width="25%"><%=new BigDecimal(allocatedCost.get(0).toString()).setScale(2,0) %></td>
                                 <td width="25%" class="ff">&nbsp;</td>
                                 <td width="25%">&nbsp;</td>
                            </tr>
                            <% }  }%>
                    </table>

                    <div class="tableHead_22">Particular Information :</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td width="25%" class="ff">Project Code : </td>
                            <td width="25%"><% if (commonTenderDetails.getProjectCode() != null) {%><%=commonTenderDetails.getProjectCode()%><% } else {%>Not applicable<%}%></td>
                            <td width="25%" class="ff">Project Name : </td>
                            <td width="25%"><% if (commonTenderDetails.getProjectName() != null) {%><%=commonTenderDetails.getProjectName()%><% } else {%>Not applicable<%}%></td>
                        </tr>
                        <tr>
                            <td  width="25%" class="ff"><% if (!(commonTenderDetails.getEventType().equalsIgnoreCase("REOI") || commonTenderDetails.getEventType().equalsIgnoreCase("RFA") || commonTenderDetails.getEventType().equalsIgnoreCase("RFP"))) {%>Tender/Proposal <% }%>Package No. and Description :</td>
                            <td colspan="3" width="75%"><%=commonTenderDetails.getPackageNo()%><br/>
                            <%=commonTenderDetails.getPackageDescription()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Category : </td>
                            <td colspan="3"><%=commonTenderDetails.getCpvCode()%></td>
                        </tr>
                        <tr>
                            <%String msgpublication = "";
                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {
                                                                        msgpublication = "Tender/Proposal";
                                                                    } else if (!commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {
                                                                        msgpublication = commonTenderDetails.getEventType();
                                                                    } else {
                                                                        msgpublication = "Pre-Qaulification";
                                                                    }
                            %>
                            <td class="ff"><%=msgpublication%> Publication<br/>Date and Time : </td>
                            <td class="formStyle_1"><%if (commonTenderDetails.getTenderPubDt() != null) {
                                %><%=DateUtils.formatDate(commonTenderDetails.getTenderPubDt())%><%}%>
                                <input name="tenderpublicationDate" value="<%if (commonTenderDetails.getTenderPubDt() != null) {
                                       %><%=DateUtils.formatDate(commonTenderDetails.getTenderPubDt())%><%}%>" type="hidden" class="formTxtBox_1" id="txttenderpublicationDate" style="width:100px;"/>
                                <% //if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().equalsIgnoreCase("1 stage-TSTM")) {
                                    //if(commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") && commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM") && commonTenderDetails.getPqTenderId() == 0){
                                    %>
                            <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification<% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")
) {%>Tender/Proposal<% }%> Document last selling /<br/>download Date and Time : <span class="mandatory">*</span></td>
                            <td class="formStyle_1"><input name="tenderLastSellDate" value="<%int tlsd = 0;
                                 if (isEdit) {
                                     for (TblCorrigendumDetail detail : corrigendumDetail) {
                                         if (detail.getFieldName().equals("docEndDate")) {
                                             out.print(detail.getNewValue());
                                             tlsd++;
                                         }
                                     }
                                 }
                                 if (tlsd == 0) {%><%=DateUtils.formatDate(commonTenderDetails.getDocEndDate())%><%}%>" type="text" class="formTxtBox_1" id="txttenderLastSellDate" style="width:100px;" readonly="true"  onfocus="GetCal('txttenderLastSellDate','txttenderLastSellDate');" onblur="docSellHoliday();findHoliday(this,1); clearvalidation('spantxttenderLastSellDate'); "/>
                                <input type="hidden" value="<%=DateUtils.formatDate(commonTenderDetails.getDocEndDate())%>" id="hdntenderLastSellDate">
                                <img id="txttenderLastSellDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txttenderLastSellDate','txttenderLastSellDateimg');"/>
                                <span id="spantxttenderLastSellDate" class="reqF_1"></span>
                                <p id="demoDocSell" style="color:red"></p>
                            </td>
                            <% //}%>
                        </tr>
                        <%                                     //List<TblConfigPreTender> conPreTender = tenderSrBean.getConfigPreTenderDetails(commonTenderDetails.getProcurementMethodId());
                                                                List<SPTenderCommonData> sptcd = tenderCommonService.returndata("GetConfigPrebid", id, null);

                        %>
                        <tr>
                            <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre - Qualification <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>Pre - REOI <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Pre - RFP <% }else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>Pre - Application <%} else {%>Pre - Tender/Proposal <%}%>meeting Start<br/>Date and Time : <%for (SPTenderCommonData sptcd1 : sptcd) {
                                                                                                       if (sptcd1.getFieldName1().equalsIgnoreCase("Yes")) {%>
                                <input type="hidden" id="hdncheck" value="<%=sptcd1.getFieldName1()%>" />
                                <%}
                                                                        }%></td>
                            <td class="formStyle_1"><input value="<%int tmsd = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                                            if (detail.getFieldName().equals("preBidStartDt")) {
                                                                                out.print(detail.getNewValue());
                                                                                tmsd++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (tmsd == 0) {%><%=DateUtils.formatDate(commonTenderDetails.getPreBidStartDt())%><%}%>" name="preTenderMeetStartDate" type="text" class="formTxtBox_1" id="txtpreTenderMeetStartDate" onblur="meetStartHoliday();MakePreBidEndDateDisable();findHoliday(this,2); clearvalidation('spantxtpreTenderMeetStartDate');" style="width:100px;" readonly="true"  onfocus="GetCal('txtpreTenderMeetStartDate','txtpreTenderMeetStartDate');" />
                                <input type="hidden" value="<%=DateUtils.formatDate(commonTenderDetails.getPreBidStartDt())%>" id="hdnpreTenderMeetStartDate">
                                <img id="txtpreTenderMeetStartDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtpreTenderMeetStartDate','txtpreTenderMeetStartDateimg');"/>
                                <img src="../resources/images/Dashboard/Refresh.png" alt="Clear date" border="0" style="vertical-align:middle;"  onclick ="ClearPreBidStartDate();"/>
                                <span id="spantxtpreTenderMeetStartDate" class="reqF_1"></span>
                                <p id="demoMeetStart" style="color:red"></p>
                            </td>
                            <td class="ff" id="PreBidEndDateTag"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre - Qualification <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>Pre - REOI <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Pre - RFP <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>Pre - Application <% } else {%>Pre - Tender/Proposal <%}%>meeting End<br/>Date and Time : <%for (SPTenderCommonData sptcd1 : sptcd) {
                                                                        if (sptcd1.getFieldName1().equalsIgnoreCase("Yes")) {%><% }
                                                                                                                }%><span>*</span></td>
                            <td class="formStyle_1" ><input value="<%int tmed = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                                            if (detail.getFieldName().equals("preBidEndDt")) {
                                                                                out.print(detail.getNewValue());
                                                                                tmed++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (tmed == 0) {%><%=DateUtils.formatDate(commonTenderDetails.getPreBidEndDt())%><%}%>" name="preTenderMeetEndDate" type="text" class="formTxtBox_1" id="txtpreTenderMeetEndDate" style="width:100px;" readonly="true"  onfocus="GetCal('txtpreTenderMeetEndDate','txtpreTenderMeetEndDate');" onblur="meetEndHoliday();findHoliday(this,3); clearvalidation('spantxtpreTenderMeetEndDate'); "/>
                                <input type="hidden" value="<%=DateUtils.formatDate(commonTenderDetails.getPreBidEndDt())%>" id="hdnpreTenderMeetEndDate">
                                <img id="txtpreTenderMeetEndDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtpreTenderMeetEndDate','txtpreTenderMeetEndDateimg');"/>
                                <span id="spantxtpreTenderMeetEndDate" class="reqF_1"></span>
                                <span id="EnterPreBidStartDateFirstSpan" style="color: red;"></span>
                                <p id="demoMeetEnd" style="color:red"></p>
                            </td>
                        </tr>
                        <tr>
                                <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification <% }
                                                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender/Proposal <% }

                                                                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Proposal <% }
                                                                                                                                                                                                              if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>EOI <% }%> Closing<br/>Date and Time : <span class="mandatory">*</span></td>
                            <td class="formStyle_1"><input name="preQualCloseDate" type="text" class="formTxtBox_1" id="txtpreQualCloseDate" value="<%int pqcd = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                                            if (detail.getFieldName().equals("submissionDt")) {
                                                                                out.print(detail.getNewValue());
                                                                                pqcd++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (pqcd == 0) {%><%=DateUtils.formatDate(commonTenderDetails.getSubmissionDt())%><%}%>" style="width:100px;" readonly="true"  onfocus="GetCal('txtpreQualCloseDate','txtpreQualCloseDate');" onblur="findHoliday2(this);openCloseWeekend();openClose(this); clearvalidation('spantxtpreQualCloseDate');clearvalidation('spantxtpreQualOpenDate');"/>
                                <input type="hidden" value="<%=DateUtils.formatDate(commonTenderDetails.getSubmissionDt())%>" id="hdnpreQualCloseDate">
                                <img id="txtpreQualCloseDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtpreQualCloseDate','txtpreQualCloseDateimg');"/>
                                <span id="spantxtpreQualCloseDate" class="reqF_1"></span>
                                <p id="demoClose" style="color:red"></p>
                                <p id="holiClose" style="color:red"></p>
                            </td>
                                <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification <% }
                                                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender/Proposal <% }

                                                                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Proposal <% }
                                                                                                                                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>EOI <% }%> Opening<br/>Date and Time : <span class="mandatory">*</span></td>
                            <td class="formStyle_1"><input name="preQualOpenDate" type="text" class="formTxtBox_1" value="<%int pqod = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                                            if (detail.getFieldName().equals("openingDt")) {
                                                                                out.print(detail.getNewValue());
                                                                                pqod++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (pqod == 0) {%><%=DateUtils.formatDate(commonTenderDetails.getOpeningDt())%><%}%>" id="txtpreQualOpenDate" style="width:100px;" readonly="true" onblur="clearvalidation('spantxtpreQualOpenDate');"  /> <!--onblur="findHoliday(this,5);"-->
                                <input type="hidden" value="<%=DateUtils.formatDate(commonTenderDetails.getOpeningDt())%>" id="hdnpreQualOpenDate">
                                <span id="spantxtpreQualOpenDate" class="reqF_1"></span>
                                <p id="demoOpen" style="color:red"></p>
                                <p id="holiOpen" style="color:red"></p>
                            </td>
                        </tr>
                    </table>

                    <div class="tableHead_22">Information for Bidder/Consultant :</div>

                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td class="ff"><input type="hidden" id="hdnevenyType" value="<%=commonTenderDetails.getProcurementNature()%>"/><% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {%>Eligibility of Bidder <% } else if(commonTenderDetails.getProcurementNature().contains("Service")) {%>Eligibility of Consultant <% }%> : <span>*</span></td>
                            <td>
                                <textarea cols="100" rows="5" id="txtaeligibilityofTenderer" name="eligibilityofTenderer" class="formTxtBox_1" ><%int tec = 0;
                                    if (isEdit) {
                                        for (TblCorrigendumDetail detail : corrigendumDetail) {
                                            if (detail.getFieldName().equals("eligibilityCriteria")) {
                                                out.print(detail.getNewValue());
                                                tec++;
                                            }
                                        }
                                    }
                                    if (tec == 0){out.print(commonTenderDetails.getEligibilityCriteria());}%></textarea>
                                    <script type="text/javascript">
                                    //<![CDATA[
                                          CKEDITOR.replace('txtaeligibilityofTenderer',
                                          {
                                               toolbar : "egpToolbar"

                                          });
                                    //]]>
                                    </script>
                                    <span id="spantxtaeligibilityofTenderer"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="25%">Brief Description of <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods")) {%>Goods and Related Service<% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {%>Works<% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {%>assignment<% }%> : <span>*</span></td>
                            <td width="75%">
                                <textarea cols="100" rows="5" id="txtabriefDescGoods" name="briefDescGoods" class="formTxtBox_1"><%
                                    int tbd = 0;
                                    if (isEdit) {
                                        for (TblCorrigendumDetail detail : corrigendumDetail) {
                                            if (detail.getFieldName().equals("tenderBrief")) {
                                                out.print(detail.getNewValue());
                                                tbd++;
                                            }
                                        }
                                    }
                                    if (tbd == 0){out.print(commonTenderDetails.getTenderBrief());}
                                    %></textarea>
                                  <script type="text/javascript">
                                    //<![CDATA[
                                          CKEDITOR.replace('txtabriefDescGoods',
                                          {
                                               toolbar : "Basic"

                                          });
                                    //]]>
                                    </script>
                                <span id="spantxtabriefDescGoods" class="reqF_1" ></span>
                            </td>
                        </tr>
                        <% if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI") || commonTenderDetails.getEventType().equalsIgnoreCase("RFP") || commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>
                        <tr>
                            <td class="ff">Experience, Resources and<br />
                                delivery capacity required : <span>*</span></td>
                            <td><textarea cols="100" rows="5" id="txtaexpRequired" name="expRequired" class="formTxtBox_1">
                                    <%
                                    int erd = 0;
                                    if (isEdit) {
                                        for (TblCorrigendumDetail detail : corrigendumDetail) {
                                            if (detail.getFieldName().equals("deliverables")) {
                                                out.print(detail.getNewValue());
                                                erd++;
                                            }
                                        }
                                    }
                                    if (erd == 0){out.print(commonTenderDetails.getDeliverables());}
                                    %>
                                </textarea>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    CKEDITOR.replace( 'txtaexpRequired',
                                    {
                                        toolbar : "egpToolbar"

                                    });
                                    //]]>
                                </script>
                                <span id="spantxtaexpRequired" class="reqF_1" ></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Other details (if  applicable) : </td>
                            <td><textarea cols="100" rows="5" id="txtaotherDetails" name="otherDetails" class="formTxtBox_1">
                                    <%
                                        int ods = 0;
                                        if (isEdit) {
                                            for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                if (detail.getFieldName().equals("otherDetails")) {
                                                    out.print(detail.getNewValue());
                                                    ods++;
                                                }
                                            }
                                        }
                                        if (ods == 0){out.print(commonTenderDetails.getOtherDetails());}
                                     %>
                                </textarea>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    CKEDITOR.replace( 'txtaotherDetails',
                                    {
                                        toolbar : "egpToolbar"

                                    });
                                    //]]>
                                    </script>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff"> Association with
                                foreign  firm : </td>
                            <td><%=commonTenderDetails.getForeignFirm()%>
                            </td>
                        </tr>
                        <% }%>

                        <tr>
                            <td class="ff"> Evaluation Type :  </td>
                            <td><% //commonTenderDetails.getEvalType()
                                if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) { %>
                                    Package wise
                                    <% } 
                                if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods")) { %>
                                    Lot wise
                                    <% }%></td>
                        </tr>
                        <%if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods")) {%>
                        <tr><%for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "lot")) {
                                lotno++;
                            }
                            %>
                            <td class="ff"> Document Available :  </td>
                            <td><%
                                if(commonTenderDetails.getDocAvlMethod().equalsIgnoreCase("Lot")){
                            %>Lot wise<% }else if(commonTenderDetails.getDocAvlMethod().equalsIgnoreCase("Package")){ %>Package wise<% } %></td>
                        </tr>
                        <%

                            /*if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().equalsIgnoreCase("1 stage-TSTM")) {
                                if (lotno > 1) {*/
                        if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){
                        %>
                        <tr>
                            <td class="ff">Document Fees : </td>
                            <td>
                                <%=commonTenderDetails.getDocFeesMethod()%>
                            </td>
                        </tr>
                        <% }// }
                                                                }
                                    if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){
                                        if(commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Package wise")){
                                                                //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {
                                                                   // if (commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Package wise") || (commonTenderDetails.getEventType().equalsIgnoreCase("tender") && commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") && lotno == 1)) {
                                                                    //if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM") && commonTenderDetails.getPqTenderId() == 0) {
                                                                        //if(!commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise")){

                        %>
                        <tr>
                                <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification Document Price (in Nu.)<% }
                                if(commonTenderDetails.getEventType().equalsIgnoreCase("RFP")){%>RFP Document Price (In Nu.)<% }
                                if(commonTenderDetails.getEventType().equalsIgnoreCase("RFQ") || commonTenderDetails.getEventType().equalsIgnoreCase("RFQU") || commonTenderDetails.getEventType().equalsIgnoreCase("RFQL")){%>RFQ Document Price (In Nu.)<% }
                                                               if(commonTenderDetails.getEventType().equalsIgnoreCase("REOI")){%>REOI Document Price (In Nu.)<% }
                                                               if(commonTenderDetails.getEventType().equalsIgnoreCase("RFA")){%>RFA Document Price (In Nu.)<% }
                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {%>Tender Document Price (in Nu.)<% }%>
                                : <!-- <span class="mandatory">*</span>--></td>
                                <td><input name="preQualDocPrice" readonly onblur="documentPrice(this);" value="<%int pqdp = 0;
                                                                                                    if (isEdit) {
                                                                                                        for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                                                                            if (detail.getFieldName().equals("pkgDocFees")) {
                                                                                                                out.print(detail.getNewValue());
                                                                                                                pqdp++;
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    if (pqdp == 0) {
                                                                                                        if (commonTenderDetails.getPkgDocFees() != null) {
                                                                                                            out.print(commonTenderDetails.getPkgDocFees().setScale(0, 0));
                                                                                                        }
                                                                                                          }%>" type="text" class="formTxtBox_1" id="txtpreQualDocPrice" style="width:200px;" /><div id="preQualDocPriceInWords"></div><span id="spantxtpreQualDocPrice"></span> </td>
                        </tr>
                        <% }}
                          if(!obj.isEmpty()){
                                if(obj.get(0)[0].toString().equalsIgnoreCase("Yes") || obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                        %>

                         <!--  Start ICT Tender by Dohatec-->
                               <% if(commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")) {%>
                         <tr id="docspriceICT"> <td class="ff">Equivalent Tender Document Price (In USD) : <!--<span>*</span>--></td>

                            <td>
                                <span style="float: right;margin-right: 50%;" id="spantxtBDRate" class="reqF_1"><a href="http://www.bob.bt/" target="_blank;" style="color:red;"> See Bhutan Bank's Conversion Rate</a></span>
                                <input name="preQualDocPriceUSD" readonly value="<%int pqdp = 0;
                                                                            if (isEdit) {
                                                                                for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                                                    if (detail.getFieldName().equals("pkgDocFeesUSD")) {
                                                                                        out.print(detail.getNewValue());
                                                                                        pqdp++;
                                                                                    }
                                                                                }
                                                                            }
                                                                            if (pqdp == 0) {
                                                                                if (commonTenderDetails.getPkgDocFeesUSD() != null) {
                                                                                    out.print(commonTenderDetails.getPkgDocFeesUSD().setScale(0, 0));
                                                                                }
                                                                                  }%>" type="text" class="formTxtBox_1" id="preQualDocPriceUSD" style="width:200px;" onblur="documentPriceUSD(this);"/>
                               <div id="preQualDocPriceICTInWords"></div>
                                <span id="spantxtpreQualDocPricemanICT" class="reqF_1"></span>

                            </td>

                         </tr>

                        <% } %>
                        <!--  End ICT Tender by Dohatec-->

                        <tr>
                            <td class="ff">Mode of Payment  : </td>
                            <td>Payment through Bank<%//commonTenderDetails.getDocFeesMode()%></td>
                        </tr>
                        <% } }//}
                                                                //if (!(commonTenderDetails.getEventType().equalsIgnoreCase("REOI") || commonTenderDetails.getEventType().equalsIgnoreCase("RFP"))) {
                                                                    //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().equalsIgnoreCase("TSTM")) {%>
<!--                        <tr>
                            <td class="ff">Name and Address of the Office(s) Selling <% //if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-qualification <%// }%>
                                <%// if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender  <% //}%>Document :
                            </td>
                            <td><%--<textarea cols="100" rows="5" id="txtanameAddressTenderDoc" name="nameAddressTenderDoc" class="formTxtBox_1"><%=commonTenderDetails.getDocOfficeAdd()%></textarea>
                                <span id="spantxtanameAddressTenderDoc" class="reqF_1"></span>--%>
                                <%//commonTenderDetails.getDocOfficeAdd()%>
                            </td>
                        </tr>-->
                        <% //}
                                                                //}
                                                                //if (!commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {
                                                                  //  if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM")) {
                        %>
                        <% if(!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){ %>
                        <tr>
                            <td class="ff">Last Date and Time for Tender Security Submission :
                            </td>
                            <td><input name="lastDateTenderSub" value="<%int ldts = 0;
                                                                                                    if (isEdit) {
                                                                                                        for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                                                                            if (detail.getFieldName().equals("securityLastDt")) {
                                                                                                                out.print(detail.getNewValue());
                                                                                                                ldts++;
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    if (ldts == 0) {%><%=DateUtils.formatDate(commonTenderDetails.getSecurityLastDt())%><%}%>" type="text" class="formTxtBox_1" id="txtlastDateTenderSub" style="width:100px;" readonly="true"  onfocus="GetCal('txtlastDateTenderSub','txtlastDateTenderSub');" onblur="secSubHoliday();findHoliday(this,6);"/>
                                <input type="hidden" value="<%=DateUtils.formatDate(commonTenderDetails.getSecurityLastDt())%>" id="hdnlastDateTenderSub">
                                <img id="txtlastDateTenderSubimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtlastDateTenderSub','txtlastDateTenderSubimg');"/>
                                <span id="spantxtlastDateTenderSub" style="color: red;"></span>
                                <p id="demoSecSub" style="color:red"></p>
                            </td>
                        </tr>


                        <!--<tr>
                            <td class="ff">Name and Address
                                of the Office(s) for tender security submission :
                                <span class="mandatory">*</span>
                            </td>
                            <td><textarea cols="100" rows="5" id="txtanameAddressTenderSub" name="nameAddressTenderSub" class="formTxtBox_1">< %int sso = 0;
                                                                                                    if (isEdit) {
                                                                                                        for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                                                                            if (detail.getFieldName().equals("securitySubOff")) {
                                                                                                                out.print(detail.getNewValue());
                                                                                                                sso++;
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    if (sso == 0) {%>< %=commonTenderDetails.getSecuritySubOff()%>< %}%></textarea>
                                <span id="spantxtanameAddressTenderSub" class="reqF_1"></span>
                            </td>
                        </tr>-->
                        <% }//}
                           if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {
                                if(!commonTenderDetails.getEventType().equalsIgnoreCase("REOI")){
                                   if(commonTenderDetails.getPassingMarks() != 0){
                            %>
                            <tr>
                                <td class="ff">Passing Marks :</td>
                                <td><%=commonTenderDetails.getPassingMarks() %></td>
                                <td>&nbsp;</td>
                            </tr>
                            <%}}}%>
                    </table>

                    <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {%>
                    <%-- <div align="right">
                         <a class="action-button-add" id="addRow">Add Phasing of Services</a>onclick="addRow('dataTable')"
                         <a class="action-button-delete" id="delRow">Remove Phasing of Service</a>onclick="deleteRow('dataTable')"
                     </div>--%>

                    <table id="dataTable" width="100%" cellspacing="0" class="tableList_1 ">
                        <tr>
                            <%--<th class="t-align-center">Select</th>--%>
                            <th class="t-align-center">Ref. No. <br /> </th>
                            <th class="t-align-center">Phasing of service <br /> </th>
                            <th class="t-align-center">Location </th>
                            <th class="t-align-center" >Indicative Start Date <br />  (<span class="mandatory">*</span>)</th>
                            <th class="t-align-center">Indicative Completion Date <br />  (<span class="mandatory">*</span>) </th>
                        </tr>

                        <%//rishita
                             for (CommonTenderDetails phasingTableDetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "phase")) {
                                 String startDate = String.valueOf(phasingTableDetails.getIndStartDt());
                                 String[] splitDash = startDate.split("-");
                                 String[] splitSpace = splitDash[2].split(" ");
                                 String sDate = splitSpace[0] + "/" + splitDash[1] + "/" + splitDash[0];

                                 String endDate = String.valueOf(phasingTableDetails.getIndEndDt());
                                 String[] splitDashEnd = endDate.split("-");
                                 String[] splitSpaceEnd = splitDashEnd[2].split(" ");
                                 String sDateEnd = splitSpaceEnd[0] + "/" + splitDashEnd[1] + "/" + splitDashEnd[0];
                        %>
                        <tr>
                            <%--<td class="t-align-center"><input class="formTxtBox_1" type="checkbox" name="chk<%=phasingCounter%>" id="chk<%=phasingCounter%>"/></td>--%>
                            <td class="t-align-center">
                                <input name="refNo" value="<%
                                 int trn = 0;
                                 if (isEdit) {
                                     for (TblCorrigendumDetail detail : corrigendumDetail) {
                                         if (detail.getFieldName().equals("phasingRefNo@" + phasingTableDetails.getTenderPhasingId())) {
                                             out.print(detail.getNewValue());
                                             trn++;
                                         }
                                     }
                                 }
                                 if (trn == 0) {
                                       out.print(phasingTableDetails.getPhasingRefNo());}%>" type="text" class="formTxtBox_1" id="txtrefNo_<%=phasingCounter%>"  onBlur="chkRefNoBlank(this);"/><span id="refno_<%=phasingCounter%>" style="color: red;">&nbsp;</span>
                                <input readonly name="refNo<%=phasingCounter%>" value="<%=phasingTableDetails.getPhasingRefNo()%>" type="hidden" class="formTxtBox_1" id="txtrefNo<%=phasingCounter%>"/>
                            </td>
                            <td class="t-align-center">
                                <textarea cols="50" rows="5" id="txtaphasingService_<%=phasingCounter%>" name="phasingService" class="formTxtBox_1" onBlur="chkPhaseSerBlank(this);"><%
                                     int tps = 0;
                                     if (isEdit) {
                                         for (TblCorrigendumDetail detail : corrigendumDetail) {
                                             if (detail.getFieldName().equals("phasingOfService@" + phasingTableDetails.getTenderPhasingId())) {
                                                 out.print(detail.getNewValue());
                                                 tps++;
                                             }
                                         }
                                     }
                                     if (tps == 0) {
                                       out.print(phasingTableDetails.getPhasingOfService());}
                                    %></textarea><span id="phaseSer_<%=phasingCounter%>" style="color: red;">&nbsp;</span>
                            </td>
                            <td class="t-align-center">
                                <input name="locationRefNo" value="<%int tlrn = 0;
                                     if (isEdit) {
                                         for (TblCorrigendumDetail detail : corrigendumDetail) {
                                             if (detail.getFieldName().equals("location@" + phasingTableDetails.getTenderPhasingId())) {
                                                 out.print(detail.getNewValue());
                                                 tlrn++;
                                             }
                                         }
                                     }
                                     if (tlrn == 0) {
                                       out.print(phasingTableDetails.getLocation());}%>" type="text" class="formTxtBox_1" id="txtlocationRefNo_<%=phasingCounter%>" onBlur="chkLocRefBlank(this);"/><span id="locRef_<%=phasingCounter%>" style="color: red;">&nbsp;</span>
                                <input readonly name="locationRefNo<%=phasingCounter%>" value="<%=phasingTableDetails.getLocation()%>" type="hidden" class="formTxtBox_1" id="txtlocationRefNo<%=phasingCounter%>" />
                            </td>
                            <td class="t-align-center">
                                <input name="indicativeStartDate" value="<%int isd = 0;
                                                                 if (isEdit) {
                                                                     for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                                         if (detail.getFieldName().equals("indStartDt@" + phasingTableDetails.getTenderPhasingId())) {
                                                                             out.print(detail.getNewValue());
                                                                             isd++;
                                                                         }
                                                                     }
                                                                 }
                                                                 if (isd == 0) {%><%=sDate%><%}%>" type="text" class="formTxtBox_1" id="txtindicativeStartDate<%=phasingCounter%>" style="width:70px;" readonly="true"  onfocus="GetCalWithouTime('txtindicativeStartDate<%=phasingCounter%>','txtindicativeStartDate<%=phasingCounter%>');" onblur="inStartValidate(this);findHoliday(this,9);"/>
                                <input type="hidden" value="<%=sDate%>" id="hdnindicativeStartDate<%=phasingCounter%>">
                                <img id="txtindicativeStartDateimg<%=phasingCounter%>" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithouTime('txtindicativeStartDate<%=phasingCounter%>','txtindicativeStartDateimg<%=phasingCounter%>');"/>
                                <div style="color: red" id="divisd<%=phasingCounter%>"></div>
                            </td>
                            <td class="t-align-center">
                                <input name="indicativeComplDate" value="<%int icd = 0;
                                                                 if (isEdit) {
                                                                     for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                                         if (detail.getFieldName().equals("indEndDt@" + phasingTableDetails.getTenderPhasingId())) {
                                                                             out.print(detail.getNewValue());
                                                                             icd++;
                                                                         }
                                                                     }
                                                                 }
                                                                 if (icd == 0) {%><%=sDateEnd%><%}%>" type="text" class="formTxtBox_1" id="txtindicativeComplDate<%=phasingCounter%>" style="width:70px;" readonly="true" onfocus="GetCalWithouTime('txtindicativeComplDate<%=phasingCounter%>','txtindicativeComplDate<%=phasingCounter%>');" onblur="inEndValidate(this);findHoliday(this,10);"/>
                                <input type="hidden" value="<%=sDateEnd%>" id="hdnindicativeComplDate<%=phasingCounter%>">
                                <img id="txtindicativeComplDateimg<%=phasingCounter%>" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick ="GetCalWithouTime('txtindicativeComplDate<%=phasingCounter%>','txtindicativeComplDateimg<%=phasingCounter%>');" />
                                <div style="color: red" id="divied<%=phasingCounter%>"></div>
                            </td>
                        </tr>
                        <%  phasingCounter++;
                             }%>
                        <input type="hidden" value="<%=phasingCounter%>" id="pcount">
                    </table>

                    <% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th width="6%" class="t-align-left">Lot No.</th>
                                <% if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes") && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                                %>
                                <th width="34%" class="t-align-left">Identification of Lot</th>
                                <% }else if(!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){%>
                                <th width="44%" class="t-align-left">Identification of Lot</th>
                                <% }else if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){%>
                                <th width="44%" class="t-align-left">Identification of Lot</th>
                                <%}else{%>
                                <th width="54%" class="t-align-left">Identification of Lot</th>
                                <% } %>
                                <th width="10%" class="t-align-center">Location <span class="mandatory">*</span></th>
                                <% if((!"Package wise".equalsIgnoreCase(commonTenderDetails.getDocFeesMethod())) && (!obj.isEmpty()) && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){ %>
                                <th width="10%" class="t-align-center" id="thDocsFeesAmount">Document Fees<br />(Amount in Nu.)<span class="mandatory">*</span></th>
                                <% } if(!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                                /*if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM") && !commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {*/
                                %>
                                <!--<th width="10%" class="t-align-center">Tender/Proposal Security (Amount in BTN) <span class="mandatory">*</span></th>-->

                                <!-- ICT Start Nazmul-->
                                  <% if(commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")) {%>
                                    <th width="20%" class="t-align-center">Tender Security (Amount in Nu.) <span class="mandatory">*</span></th>
                                    <th width="15%" class="t-align-center">Equivalent Tender Security (Amount in USD) <span class="mandatory">*</span></th>
                                    <%}%>

                                    <% if(commonTenderDetails.getProcurementType().equalsIgnoreCase("NCT")) {%>
                                    <th width="20%" class="t-align-center">Tender Security (Amount in Nu.) <span class="mandatory">*</span></th>

                                  <%}%>
                               <!-- ICT end Nazmul-->

                                <% }%>
                                <th width="10%" class="t-align-center">Start Date <span class="mandatory">*</span> </th>
                                <th width="10%" class="t-align-center">Completion Date <span class="mandatory">*</span> </th>
                        </tr>
                        <%

                             for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "lot")) {

                        %>
                        <tr>
                            <td class="t-align-center"><label><%=commonTdetails.getLotNo()%></label> </td>
                            <td class="t-align-center"><label><%=commonTdetails.getLotDesc()%></label></td>
                            <td class="t-align-center">
                                <input value="<%
                                int tll = 0;
                                 if (isEdit) {
                                     for (TblCorrigendumDetail detail : corrigendumDetail) {
                                         if (detail.getFieldName().equals("location@" + commonTdetails.getTenderLotSecId())) {
                                             out.print(detail.getNewValue());
                                             tll++;
                                         }
                                     }
                                 }
                                 if (tll== 0) {
                                out.print(commonTdetails.getLocationSec());
                                                               }%>" name="locationlot" type="text" class="formTxtBox_1" id="locationlot_<%=i%>"  onblur="chkLocLotBlank(this);" style="width: 180px;"/><span id="locLot_<%=i%>" style="color: red;">&nbsp;</span>
                            </td>
<!--                            commonTenderDetails.getDocFeesMethod()-->
                            <%if((!"Package wise".equalsIgnoreCase(commonTenderDetails.getDocFeesMethod())) && (!obj.isEmpty()) && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){%>
                            <td class="t-align-center" id="tdDocFeeslot<%=i%>"><input value="<%int df = 0;
                                 if (isEdit) {
                                     for (TblCorrigendumDetail detail : corrigendumDetail) {
                                         if (detail.getFieldName().equals("docfess@" + commonTdetails.getTenderLotSecId())) {
                                             out.print(detail.getNewValue());
                                             df++;
                                         }
                                     }
                                 }
                                 if (df == 0){out.print(commonTdetails.getDocFess().setScale(0, 0));}%>" name="docFeeslot" type="text" class="formTxtBox_1" id="docFeeslot<%=i%>" onblur="docFeeValidate(this)"/><div id="divdocfee<%=i%>" style="color: red"></div></td>
                                <%}
                                                                 //if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM") && commonTenderDetails.getEventType().equalsIgnoreCase("Tender")) {
                                    //if (commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM") && !commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {
                                if(!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                                %>
                            <!-- ICT Start Dohatec-->
                               <% if(commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")) {%>
                               <td class="t-align-center"><div><input value="<%
                                int tsa = 0;
                                    if (isEdit) {
                                            for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                    if (detail.getFieldName().equals("tenderSecurityAmt@" + commonTdetails.getTenderLotSecId())) {
                                                            out.print(detail.getNewValue());
                                                            tsa++;
                                                    }
                                            }
                                    }if (tsa == 0) {
                                        out.print(commonTdetails.getTenderSecurityAmt().setScale(0, 0));
                                    }%>" name="tenderSecurityAmount" readonly type="text" class="formTxtBox_1" id="tenderSecurityAmount_<%=i%>"  onblur="chkAmountLotBlank(this);" /><span id="amountLot_<%=i%>" style="color: red;"></span></div>
                                <div style="text-align: left; padding: 9px 0 0 10px;"> 
                                    <input type="checkbox" disabled value ="1" name = "finanDeclaration_<%=i%>" id="chkfinanDeclaration_<%=i%>"  onchange="chkmsgDeclaration(<%=i%>);"
                                             <%if(commonTdetails.getBidSecurityType().equals("1") || commonTdetails.getBidSecurityType().equals("3"))
                                             { out.print("checked"); }%> /> Financial Institution Payment <br/>
                                    <input type="checkbox" disabled value="2" name="bidSecDeclaration_<%=i%>"  id="chkbidSecDeclaration_<%=i%>" onchange="chkmsgDeclaration(<%=i%>);"
                                          <%if(commonTdetails.getBidSecurityType().equals("2") || commonTdetails.getBidSecurityType().equals("3"))
                                            { out.print("checked"); }%>/> Bid Security Declaration <br/>
                                    <span id="msgDeclaration_<%=i%>" style="color: red;"></span>
                                   </div> 
                               </td>

                               <td class="t-align-center"><input value="<%
                                int tsaa = 0;
                                    if (isEdit) {
                                            for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                    if (detail.getFieldName().equals("tenderSecurityAmtUSD@" + commonTdetails.getTenderLotSecId())) {
                                                            out.print(detail.getNewValue());
                                                            tsaa++;
                                                    }
                                            }
                                    }if (tsaa == 0) {
                                        out.print(commonTdetails.getTenderSecurityAmtUSD().setScale(0, 0));
                                                                }%>" name="tenderSecurityAmountUSD" readonly type="text" class="formTxtBox_1" id="tenderSecurityAmountUSD_<%=i%>"
                                                                    <%
                                                                        if(commonTdetails.getTenderSecurityAmt().setScale(0, 0).toString().equals("0"))
                                                                        {   %>
                                                                            readonly="readonly"
                                                                        <%  }
                                                                    %>
                                                                onblur="chkAmountLotBlankUSD(this);" /><span id="amountLotUSD_<%=i%>" style="color: red;"></span></td>


                                <%}%>

                              <% if(commonTenderDetails.getProcurementType().equalsIgnoreCase("NCT")) {%>
                                <td class="t-align-center"><div><input value="<%
                                    int tsa = 0;
                                        if (isEdit) {
                                                for (TblCorrigendumDetail detail : corrigendumDetail) {
                                                        if (detail.getFieldName().equals("tenderSecurityAmt@" + commonTdetails.getTenderLotSecId())) {
                                                                out.print(detail.getNewValue());
                                                                tsa++;
                                                        }
                                                }
                                        }if (tsa == 0) {
                                            out.print(commonTdetails.getTenderSecurityAmt().setScale(0, 0));
                                      }%>" name="tenderSecurityAmount" readonly type="text" class="formTxtBox_1" id="tenderSecurityAmount_<%=i%>"  onblur="chkAmountLotBlank(this);" style="width:185px;" /><span id="amountLot_<%=i%>" style="color: red;"></span></div>
                                <div style="text-align: left; padding: 9px 0 0 10px;"> 
                                      <input type="checkbox"  disabled  value ="1" name = "finanDeclaration_<%=i%>" id="chkfinanDeclaration_<%=i%>"  onchange="chkmsgDeclaration(<%=i%>);"
                                             <%if(commonTdetails.getBidSecurityType().equals("1") || commonTdetails.getBidSecurityType().equals("3"))
                                             { out.print("checked"); }%> /> Financial Institution Payment <br/>
                                    <input type="checkbox" disabled   value="2" name="bidSecDeclaration_<%=i%>"  id="chkbidSecDeclaration_<%=i%>" onchange="chkmsgDeclaration(<%=i%>);"
                                          <%if(commonTdetails.getBidSecurityType().equals("2") || commonTdetails.getBidSecurityType().equals("3"))
                                            { out.print("checked"); }%>/> Bid Security Declaration <br/>
                                    <span id="msgDeclaration_<%=i%>" style="color: red;"></span>
                                   </div> 
                                </td>

                            <%}%>

                           <!-- ICT End Dohatec-->


                            <% }%>
                            <td class="t-align-center">
                                    <input name="startDateLotNo" type="text" value="<%
                                int tst = 0;
                                 if (isEdit) {
                                     for (TblCorrigendumDetail detail : corrigendumDetail) {
                                         if (detail.getFieldName().equals("startTime@" + commonTdetails.getTenderLotSecId())) {
                                             out.print(detail.getNewValue());
                                             tst++;
                                         }
                                     }
                                 }
                                 if (tst == 0) {
                                                            out.print(commonTdetails.getStartTime());}%>" class="formTxtBox_1" id="startTimeLotNo_<%=i%>" style="width:100px;" readonly="true" onfocus="GetCalWithouTime('startTimeLotNo_<%=i%>','startTimeLotNo_<%=i%>');"  onblur="findHoliday(this,7);"/><span id="startLot_<%=i%>" style="color: red;">&nbsp;</span>
                                    <img id="txtstartTimeLotNoimg_<%=i%>"  src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithouTime('startTimeLotNo_<%=i%>','txtstartTimeLotNoimg_<%=i%>');"/>
                            </td>
                            <td class="t-align-center">
                                <input value="<%
                                int tct = 0;
                                 if (isEdit) {
                                     for (TblCorrigendumDetail detail : corrigendumDetail) {
                                         if (detail.getFieldName().equals("completionTime@" + commonTdetails.getTenderLotSecId())) {
                                             out.print(detail.getNewValue());
                                             tct++;
                                         }
                                     }
                                 }
                                 if (tct == 0) {
                                out.print(commonTdetails.getCompletionTime());}%>" name="complTimeLotNo" type="text" class="formTxtBox_1" id="complTimeLotNo_<%=i%>" style="width:100px;" readonly="true"  onfocus="GetCalWithouTime('complTimeLotNo_<%=i%>','complTimeLotNo_<%=i%>');"  onblur="findHoliday(this,8);"/><span id="compLot_<%=i%>" style="color: red;">&nbsp;</span>
                                <img id="txtcomplTimeLotNoimg_<%=i%>"  src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCalWithouTime('complTimeLotNo_<%=i%>','txtcomplTimeLotNoimg_<%=i%>');"/>
                                <input type="hidden" id="tenderLotSecId<%=i%>" name="tenderLotSecId<%=i%>" value="<%=commonTdetails.getTenderLotSecId()%>" />
                            </td>
                        </tr>
                        <%i++;

                             }%>
                        <input type="hidden" id="docount" value="<%=i%>" />
                    </table>
                    <% }%>

                    <div class="tableHead_22 t_space">Procuring Agency Details:</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td class="ff" width="20%">Name of Official Inviting <% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%> Pre-Qualification<% }
                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%> Tender/Proposal<% }
                                                                                                            if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%> REOI<% }
                                                                                                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%> RFP<% }%> :</td>
                            <td width="30%"><%=commonTenderDetails.getPeName()%></td>
                            <td class="ff" width="20%"> Designation  of Official Inviting <% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%> Pre-Qualification<% }
                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%> Tender/Proposal<% }
                                                                                                            if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%> REOI<% }
                                                                                                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%> RFP<% }%> :</td>
                            <td width="30%"><%=commonTenderDetails.getPeDesignation()%></td>
                        </tr>

                        <tr>
                            <td class="ff">Address of Official Inviting <% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%> Pre-Qualification<% }
                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%> Tender/Proposal<% }
                                                                                                            if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%> REOI<% }
                                                                                                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%> RFP<% }%> : </td>
                            <td><% if (commonTenderDetails.getPeAddress().contains("Thana")) {
                                        out.print(commonTenderDetails.getPeAddress().replace("Thana", "Gewog"));
                                    } else {
                                        out.print(commonTenderDetails.getPeAddress());
                                    }
                                  %></td>
                            <td class="ff">Contact details of Official Inviting <% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%> Pre-Qualification<% }
                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%> Tender/Proposal<% }
                                                                                                            if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%> REOI<% }
                                                                                                                                                    if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%> RFP<% }%> :</td>
                            <td><%=commonTenderDetails.getPeContactDetails()%></td>
                        </tr>
                        <%
                                    }
                                    phasingCounter++;
                        %>
                        <tr>
                            <td colspan="4" class="ff mandatory">The procuring agency  reserves the right to accept or reject all Tenders / Pre-Qualifications / EOIs</td>
                        </tr>
                    </table>

                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td align="center">
                                <label class="formBtn_1"><input type="submit" name="submit" id="btnsubmit" value="Submit" onclick="return Validation();"/></label>&nbsp;&nbsp;
                                <input type="hidden" value="<%=phasingCounter%>" id="txtcounter" name="txtcounter"/><br/>
                                <input type="hidden" value="<%=id%>" name="id" />
                                <input type="hidden" value="<%=request.getParameter("isedit")%>" name="isedit" />

                            </td>
                        </tr>
                    </table>
                    <div>&nbsp;</div>
                </form>
                <!--Dashboard Content Part End-->
            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

        </script>
    <script type="text/javascript">
        function openClose(comp)
        {
            document.getElementById('txtpreQualOpenDate').value=comp.value;
        }
        function findHoliday2(comp) {
                        var compVal = comp.value;
                        var cnt = 0;
                        if (compVal != null && compVal != "") {
                            for (var i = 0; i < holiArray.length; i++) {
                                if (CompareToForEqual(holiArray[i], compVal)) {
                                    cnt++;
                                }
                            }



                        }
                        if (cnt != 0) {
                            document.getElementById("holiClose").innerHTML = "Holiday!";
                            document.getElementById("holiOpen").innerHTML = "Holiday!";
                        }
                        else 
                        {
                            document.getElementById("holiClose").innerHTML = "";
                            document.getElementById("holiOpen").innerHTML = "";
                        }
                        
                    }
                function findHoliday(comp,compi){
                    $(".err"+compi).remove();
                    var compVal  = comp.value;
                    var cnt = 0;
                    if(compVal!=null && compVal!=""){
                        for(var i=0;i<holiArray.length;i++){
                            if(CompareToForEqual(holiArray[i],compVal)){
                                cnt++;
                            }
                        }
                    }
                    if(cnt!=0){
                        $('#'+comp.id).parent().append("<div class='err"+compi+"' style='color:red;'>Holiday!</div>");
                    }
                }
                
            function clearvalidation(id)
            {
               document.getElementById(id).innerHTML = '' ;  
            }
                
            function Validation(){
                var vbool=true;

                //Experience, Resources and delivery capacity required
                if(CKEDITOR.instances.txtaexpRequired != null){
                    if(!required($.trim(CKEDITOR.instances.txtaexpRequired.getData())) || isCKEditorFieldBlank($.trim(CKEDITOR.instances.txtaexpRequired.getData().replace(/<[^>]*>|\s/g, ''))))
                    {
                        document.getElementById('spantxtaexpRequired').innerHTML='<div class="reqF_1">Please enter Experience, Resources and delivery capacity</div>';
                        vbool=false;
                    }
                    else
                    {
                        if(!Maxlenght(CKEDITOR.instances.txtaexpRequired.getData(),'2000')){
                            document.getElementById('spantxtaexpRequired').innerHTML='<div class="reqF_1">Maximum 2000 characters are allowed</div>';
                            vbool=false;
                        }
                        else
                        {
                            document.getElementById('spantxtaexpRequired').innerHTML='';
                        }
                    }
                }

                //Name & Address of the Office(s) Selling Tender Document
                if(document.getElementById('txtanameAddressTenderSub')!=null){
                    if(!required(document.getElementById('txtanameAddressTenderSub').value))
                    {
                        document.getElementById('spantxtanameAddressTenderSub').innerHTML='<div class="reqF_1">Please Enter Name & Address of the Office(s) Selling Tender Document</div>';
                        vbool=false;
                    }
                    else
                    {
                        if(!Maxlenght(document.getElementById('txtanameAddressTenderSub').value,'2000')){
                            document.getElementById('spantxtanameAddressTenderSub').innerHTML='<div class="reqF_1">Maximum 2000 characters are allowed</div>';
                            vbool=false;
                        }else
                        {
                            document.getElementById('spantxtanameAddressTenderSub').innerHTML='';
                        }
                    }
                }
            <%--if(document.getElementById('txtanameAddressTenderDoc')!=null){
                if(!required(document.getElementById('txtanameAddressTenderDoc').value))
                {
                    document.getElementById('spantxtanameAddressTenderDoc').innerHTML='<div class="reqF_1">Please Enter Name & Address of the Office(s) Selling Tender Document.</div>';
                    vbool=false;
                }
                else
                {
                    if(!Maxlenght(document.getElementById('txtanameAddressTenderDoc').value,'2000')){
                        document.getElementById('spantxtanameAddressTenderDoc').innerHTML='<div class="reqF_1">Maximum 2000 characters are allowed.</div>';
                        vbool=false;
                    }else
                        {
                            document.getElementById('spantxtanameAddressTenderDoc').innerHTML='';
                        }
                }
            }--%>


                    //Pre-Qualification Document last selling date & time
                    if(document.getElementById('txttenderLastSellDate')!=null){
                        if(!CompareToWithEqual(document.getElementById('txttenderLastSellDate').value,document.getElementById('hdntenderLastSellDate').value))
                        {
                            document.getElementById('spantxttenderLastSellDate').innerHTML='<div class="reqF_1">Date can\'t be pre pone</div>';
                            vbool=false;
                        }
                        else
                        {
                            if(!CompareToForGreater(document.getElementById('txttenderLastSellDate').value,document.getElementById('txttenderpublicationDate').value) || !CompareToForSmaller(document.getElementById('txttenderLastSellDate').value,document.getElementById('txtpreQualCloseDate').value))
                            {
                                document.getElementById('spantxttenderLastSellDate').innerHTML='<div class="reqF_1">Last Date and Time of document selling should be greater than Publication Date & Time and smaller than Closing Date & Time</div>';
                                vbool=false;
                            }else
                            {
                                document.getElementById('spantxttenderLastSellDate').innerHTML='';
                            }
                        }
                    }

                    //Pre - Qualification meeting Start Date & Time
                    if(document.getElementById('hdncheck')!=null && document.getElementById('hdncheck').value=='Yes'){
                        if(!required(document.getElementById('txtpreTenderMeetStartDate').value))
                        {
                            //document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='<div class="reqF_1">Please select Tender/Proposal Meet Start Date</div>';
                            //vbool=false;
                        }
                        else
                        {
                            if(document.getElementById('hdnpreTenderMeetStartDate').value!=null && document.getElementById('hdnpreTenderMeetStartDate').value!='')
                            {
                                
                                
                                if(!CompareToForGreater(document.getElementById('txtpreTenderMeetStartDate').value,document.getElementById('txttenderpublicationDate').value))
                                {
                                    document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='<div class="reqF_1">Meeting start date & time must be greater than the publication date & time</div>';
                                    vbool=false;
                                }
                                else if(!CompareToForToday(document.getElementById('txtpreTenderMeetStartDate').value))
                                {
                                    document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='<div class="reqF_1">Meeting start date & time must be greater than current date & time</div>';
                                    vbool=false;
                                }
                                
                                else
                                {
                                    
                                    if(!CompareToForSmaller(document.getElementById('txtpreTenderMeetStartDate').value,document.getElementById('txtpreQualCloseDate').value))
                                    {
                                        document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='<div class="reqF_1">Pre - Tender meeting Start Date & Time should be less than Closing Date & Time</div>';
                                        vbool=false;
                                    }
                                    else
                                    {
                                        document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='';
                                    }
                                }
                            }else
                                {
                                    
                                    if(!CompareToForGreater(document.getElementById('txtpreTenderMeetStartDate').value,document.getElementById('txttenderpublicationDate').value))
                                    {
                                        document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='<div class="reqF_1">Meeting start date & time must be greater than the publication date & time</div>';
                                        vbool=false;
                                    }
                                    else if(!CompareToForToday(document.getElementById('txtpreTenderMeetStartDate').value))
                                    {
                                        document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='<div class="reqF_1">Meeting start date & time must be greater than the current date & time</div>';
                                        vbool=false;
                                    }
                                    
                                    else
                                    {
                                        if(!CompareToForSmaller(document.getElementById('txtpreTenderMeetStartDate').value,document.getElementById('txtpreQualCloseDate').value))
                                        {
                                            document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='<div class="reqF_1">Pre - Tender meeting Start Date & Time should be less than Closing Date & Time</div>';
                                            vbool=false;
                                        }
                                        else
                                        {
                                            document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='';
                                        }
                                    }
                                }
                
                        }
                    }else
                    {
                        if(document.getElementById('hdnpreTenderMeetStartDate')!=null && document.getElementById('hdnpreTenderMeetStartDate').value!='' ){
                            if(CompareToWithEqual(document.getElementById('txtpreTenderMeetStartDate').value,document.getElementById('hdnpreTenderMeetStartDate').value)){
                                if(document.getElementById('txtpreTenderMeetStartDate').value!='')
                                {
                                    if(document.getElementById('txtpreTenderMeetEndDate').value!='')
                                    {
                                        
                                        if(!CompareToForGreater(document.getElementById('txtpreTenderMeetStartDate').value,document.getElementById('txttenderpublicationDate').value))
                                        {
                                            document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='<div class="reqF_1">Meeting start date & time must be greater than the publication date & time</div>';
                                            vbool=false;
                                        }
                                        else if(!CompareToForToday(document.getElementById('txtpreTenderMeetStartDate').value))
                                        {
                                            document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='<div class="reqF_1">Meeting start date & time must be greater than the current date & time</div>';
                                            vbool=false;
                                        }
                                        else
                                        {
                                            document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='';
                                        }
                                    }
                                    else
                                    {
                                        //document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='<div class="reqF_1">Please enter Meeting End Date & Time</div>';
                                        //vbool=false;
                                    }
                                }
                            }
                            else
                            {

                                document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='<div class="reqF_1">Date can\'t be pre pone</div>';
                                vbool=false;
                            }
                        }

                    }

                    if(document.getElementById('txtlastDateTenderSub')!=null){
                        if(!required($.trim(document.getElementById('txtlastDateTenderSub').value)))
                        {
                            document.getElementById('spantxtlastDateTenderSub').innerHTML='<div class="reqF_1">Please enter Last Date and Time for Tender Security Submission</div>';
                            vbool=false;
                        }
                        else
                        {
                            if(!CompareToForSmaller(document.getElementById('txtlastDateTenderSub').value,document.getElementById('txtpreQualCloseDate').value))
                                {
                                    document.getElementById('spantxtlastDateTenderSub').innerHTML='<div class="reqF_1">Tender Security Date and Time must not be after Tender closing Date and Time</div>';
                                    vbool=false;
                                }
                                else
                                {
                                    document.getElementById('spantxtlastDateTenderSub').innerHTML='';
                                }
                            //document.getElementById('spantxtlastDateTenderSub').innerHTML='';
                        }
                    }

                    //Pre - Qualification meeting End Date & Time
                    if(document.getElementById('hdncheck')!=null && document.getElementById('hdncheck').value=='Yes'){
                        if(!required(document.getElementById('txtpreTenderMeetEndDate').value))
                        {
                            //document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='<div class="reqF_1">Please select Tender/Proposal Meet End Date</div>';
                            //vbool=false;
                            if(document.getElementById("txtpreTenderMeetStartDate").value != "")
                            {
                                if(document.getElementById("txtpreTenderMeetEndDate").value == "")
                                {
                                    document.getElementById("spantxtpreTenderMeetEndDate").innerHTML = '<div class="reqF_1">Enter Pre Bid meeting end date.</div>';
                                    vbool=false;
                                }
                            }
                        }
                        else
                        {
                            if(document.getElementById('hdnpreTenderMeetEndDate')!=null && document.getElementById('hdnpreTenderMeetEndDate').value!='' )
                            {
                                if(!CompareToWithEqual(document.getElementById('txtpreTenderMeetEndDate').value,document.getElementById('hdnpreTenderMeetEndDate').value))
                                {
                                    document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='<div class="reqF_1">Date can\'t be pre pone</div>';
                                    vbool=false;
                                }
                                else
                                {
                                    if(!CompareToForGreater(document.getElementById('txtpreTenderMeetEndDate').value,document.getElementById('txtpreTenderMeetStartDate').value))
                                    {
                                        document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='<div class="reqF_1">Meeting End Date & Time must be greater than Meeting Start Date & Time</div>';
                                        vbool=false;
                                    }
                                    else
                                    {
                                        if(!CompareToForSmaller(document.getElementById('txtpreTenderMeetEndDate').value,document.getElementById('txtpreQualCloseDate').value))
                                        {
                                            //rajeshsingh
                                            document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='<div class="reqF_1">Pre - Tender meeting End Date & Time should be less than Closing Date & Time</div>';
                                            vbool=false;
                                        }
                                        else
                                        {
                                            document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='';
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if(!CompareToForGreater(document.getElementById('txtpreTenderMeetEndDate').value,document.getElementById('txtpreTenderMeetStartDate').value))
                                {
                                    document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='<div class="reqF_1">Meeting End Date & Time must be greater than Meeting Start Date & Time</div>';
                                    vbool=false;
                                }
                                else
                                {
                                    if(!CompareToForSmaller(document.getElementById('txtpreTenderMeetEndDate').value,document.getElementById('txtpreQualCloseDate').value))
                                    {
                                        //rajeshsingh
                                        document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='<div class="reqF_1">Pre - Tender meeting End Date & Time should be less than Closing Date & Time</div>';
                                        vbool=false;
                                    }
                                    else
                                    {
                                        document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='';
                                    }
                                }
                            }
                        }
                    }else
                    {
                        if(document.getElementById('hdnpreTenderMeetEndDate')!=null && document.getElementById('hdnpreTenderMeetEndDate').value!='' ){
                            if(CompareToWithEqual(document.getElementById('txtpreTenderMeetEndDate').value,document.getElementById('hdnpreTenderMeetEndDate').value)){
                                if(document.getElementById('txtpreTenderMeetEndDate').value!='')
                                {
                                    if(document.getElementById('txtpreTenderMeetStartDate').value!='')
                                    {
                                        if(!CompareToForGreater(document.getElementById('txtpreTenderMeetEndDate').value,document.getElementById('txtpreTenderMeetStartDate').value))
                                        {
                                            document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='<div class="reqF_1">Meeting End Date & Time should be greater than Meeting Start Date & Time</div>';
                                            vbool=false;
                                        }
                                        else
                                        {
                                            if(!CompareToForSmaller(document.getElementById('txtpreTenderMeetEndDate').value,document.getElementById('txtpreQualCloseDate').value))
                                            {
                                                document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='<div class="reqF_1">Pre - Tender meeting End Date & Time should be less than Closing Date & Time</div>';
                                                vbool=false;
                                            }
                                            else
                                            {
                                                document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='';
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //document.getElementById('spantxtpreTenderMeetStartDate').innerHTML='<div class="reqF_1">Please enter Meeting Start Date & Time</div>';
                                        //vbool=false;
                                    }
                                }
                            }
                            else
                            {

                                document.getElementById('spantxtpreTenderMeetEndDate').innerHTML='<div class="reqF_1">Date can\'t be pre pone</div>';
                                vbool=false;
                            }
                        }
                    }

                    //Pre-Qualification Closing Date & Time
                    if(document.getElementById('txtpreQualCloseDate')!=null){
                        
                        if(!CompareToWithEqual(document.getElementById('txtpreQualCloseDate').value,document.getElementById('hdnpreQualCloseDate').value))
                        {
                            document.getElementById('spantxtpreQualCloseDate').innerHTML='<div class="reqF_1">Date can\'t be pre pone</div>';
                            vbool=false;
                        }
                        else if(findHoliday3(document.getElementById('txtpreQualCloseDate').value))
                        {
                            clearvalidation('holiOpen');
                            clearvalidation('holiClose');
                            document.getElementById('spantxtpreQualCloseDate').innerHTML='<div class="reqF_1">Closing and Opening Date can not be holiday!</div>';
                            vbool=false;
                        }
                        else if(CompareHoliday(document.getElementById('txtpreQualCloseDate').value)=="Sat" ||CompareHoliday(document.getElementById('txtpreQualCloseDate').value)=="Sun")
                        {
                            clearvalidation('demoOpen');
                            clearvalidation('demoClose');
                            document.getElementById('spantxtpreQualCloseDate').innerHTML='<div class="reqF_1">Closing and Opening Date can not be weekend!</div>';
                            vbool=false;
                        }
                        
                        else
                        {
                            if(!CompareToForGreater(document.getElementById('txtpreQualCloseDate').value,document.getElementById('txttenderpublicationDate').value))
                            {
                                document.getElementById('spantxtpreQualCloseDate').innerHTML='<div class="reqF_1">Closing date & time must be greater than Publication Date & Time</div>';
                                vbool=false;
                            }
                            else
                            {
                                document.getElementById('spantxtpreQualCloseDate').innerHTML='';
                            }
                        }
                    }

                    //Pre-Qualification Opening Date & Time
                    if(document.getElementById('txtpreQualOpenDate')!=null){
                        if(!CompareToWithEqual(document.getElementById('txtpreQualOpenDate').value,document.getElementById('hdnpreQualOpenDate').value))
                        {
                            document.getElementById('spantxtpreQualOpenDate').innerHTML='<div class="reqF_1">Date can\'t be pre pone</div>';
                            vbool=false;
                        }
                        else if(findHoliday3(document.getElementById('txtpreQualOpenDate').value))
                        {
                            clearvalidation('holiOpen');
                            clearvalidation('holiClose');
                            document.getElementById('spantxtpreQualOpenDate').innerHTML='<div class="reqF_1">Closing and Opening Date can not be holiday!</div>';
                            vbool=false;
                        }
                        else if(CompareHoliday(document.getElementById('txtpreQualOpenDate').value)=="Sat" ||CompareHoliday(document.getElementById('txtpreQualOpenDate').value)=="Sun")
                        {
                            clearvalidation('demoOpen');
                            clearvalidation('demoClose');
                            document.getElementById('spantxtpreQualOpenDate').innerHTML='<div class="reqF_1">Closing and Opening Date can not be weekend!</div>';
                            vbool=false;
                        }

                        else
                        {
                            if(!CompareToForGreater(document.getElementById('txtpreQualOpenDate').value,document.getElementById('txtpreQualCloseDate').value))
                            {
                                //document.getElementById('spantxtpreQualOpenDate').innerHTML='<div class="reqF_1">Opening Date and Time should be greater than Closing date & time</div>';
                                //vbool=false;
                            }else
                            {
                                document.getElementById('spantxtpreQualOpenDate').innerHTML='';
                            }
                        }
                    }


                    //Pre-Qualification document price (TK)
                    if(document.getElementById('txtpreQualDocPrice')!=null){
                        if(document.getElementById('txtpreQualDocPrice').value!=''){
                            if(!rajesh(document.getElementById('txtpreQualDocPrice').value)){
                                document.getElementById('spantxtpreQualDocPrice').innerHTML='<div class="reqF_1">Enter 2 digits after Decimal</div>';
                                vbool=false;
                            }else{
                                var removedecimal=document.getElementById('txtpreQualDocPrice').value.split(".");
                                if($.trim(document.getElementById('txtpreQualDocPrice').value)=='0' || removedecimal[0]=='0')
                                {
                                   // document.getElementById('spantxtpreQualDocPrice').innerHTML='<div class="reqF_1">Only 0 value is not allowed</div>';
                                    //vbool=false;
                                }
                                else
                                {
                                    document.getElementById('spantxtpreQualDocPrice').innerHTML='';
                                }
                            }
                        }else
                        {
                            document.getElementById('spantxtpreQualDocPrice').innerHTML='<div class="reqF_1">Please enter Tender Document Price (in Nu.)</div>';
                            vbool=false;
                        }
                    }

                    //Pre-Qualification document price (USD) : Dohatec
                    if(document.getElementById('preQualDocPriceUSD')!=null){
                        if(document.getElementById('preQualDocPriceUSD').value!=''){
                            if(!rajesh(document.getElementById('preQualDocPriceUSD').value)){
                                document.getElementById('spantxtpreQualDocPricemanICT').innerHTML='<div class="reqF_1">Enter 2 digits after Decimal</div>';
                                vbool=false;
                            }else{
                                var removedecimal=document.getElementById('preQualDocPriceUSD').value.split(".");
                                if($.trim(document.getElementById('preQualDocPriceUSD').value)=='0' || removedecimal[0]=='0')
                                {
                                  //  document.getElementById('spantxtpreQualDocPricemanICT').innerHTML='<div class="reqF_1">Only 0 value is not allowed</div>';
                                  //  vbool=false;
                                }
                                else
                                {
                                    document.getElementById('spantxtpreQualDocPricemanICT').innerHTML='';
                                }
                            }
                        }else
                        {
                            document.getElementById('spantxtpreQualDocPricemanICT').innerHTML='<div class="reqF_1">Please enter Tender Document Price (in USD)</div>';
                            vbool=false;
                        }
                    }

                    var count = '<%=i%>';
                    if(count != 0)
                    {
                        for(var k=0;k<count;k++)
                        {
                            if(document.getElementById("startTimeLotNo_"+k)!=null){
                                if(document.getElementById("startTimeLotNo_"+k).value==""){
                                    document.getElementById("startLot_"+k).innerHTML="<br/>Please enter Start Date";
                                    vbool=false;
                                }else{
                                    var objdate=document.getElementById("startTimeLotNo_"+k).value;
                                    var yearobj=parseInt(objdate.split('/')[2]);
                                    var monthobj=(objdate.split('/')[1]);
                                    var dayobj=(objdate.split('/')[0]);
                                    var dateobj =  new Date(yearobj, (monthobj-1), dayobj);

                                    var objdate1=document.getElementById("txtpreQualCloseDate").value;
                                    var yearobj1=parseInt(objdate1.split('/')[2]);
                                    var monthobj1=(objdate1.split('/')[1]);
                                    var dayobj1=(objdate1.split('/')[0]);
                                    var dateobj1 =  new Date(yearobj1, (monthobj1-1), dayobj1);

                                        if(Date.parse(dateobj) > Date.parse(dateobj1))
                                        {
                                            document.getElementById("startLot_"+k).innerHTML="";
                                            //vbool='true';
                                        }
                                        else
                                        {
                                            document.getElementById("startLot_"+k).innerHTML="<br/>Start Date must be greater than Closing Date and Time";
                                            vbool=false;
                                        }
                                }
                            }

                            if(document.getElementById("complTimeLotNo_"+k)!=null){

                                if(document.getElementById("complTimeLotNo_"+k).value==""){
                                    document.getElementById("compLot_"+k).innerHTML="<br/>Please enter Completion Date";
                                    vbool=false;
                                }else{
                                    var objdate1=document.getElementById("startTimeLotNo_"+k).value;
                                    var yearobj1=parseInt(objdate1.split('/')[2]);
                                    var monthobj1=(objdate1.split('/')[1]);
                                    var dayobj1=(objdate1.split('/')[0]);
                                    var dateStart1 =  new Date(yearobj1, (monthobj1-1), dayobj1);


                                    var objdate11=document.getElementById("complTimeLotNo_"+k).value;
                                    var yearobj11=parseInt(objdate11.split('/')[2]);
                                    var monthobj11=(objdate11.split('/')[1]);
                                    var dayobj11=(objdate11.split('/')[0]);
                                    var dateobj11 =  new Date(yearobj11, (monthobj11-1), dayobj11);
                                    if(Date.parse(dateobj11) > Date.parse(dateStart1))
                                    {
                                        document.getElementById("compLot_"+k).innerHTML="";
                                        //vbool='true';
                                    }
                                    else
                                    {
                                        document.getElementById("compLot_"+k).innerHTML="<br/>Completion Date and Time Must be Greater than Start Date and Time";
                                        vbool=false;
                                    }
                                }
                            }
                        }
                    }

                    if(document.getElementById('hdnMinExtDays')!=null && document.getElementById('hdnTimeAllowed')!=null){
                        var cad=document.getElementById('hdnCurrDate').value;
                        var minExtDays = document.getElementById('hdnMinExtDays').value;
                        var timeAllowed = document.getElementById('hdnTimeAllowed').value;
                        var diff1 = eval(calcDays(document.getElementById('hdnpreQualCloseDate').value, document.getElementById('txttenderpublicationDate').value)*(timeAllowed/100));
                        var diff2 = calcDays(document.getElementById('hdnpreQualCloseDate').value,cad);
                        if(diff1>diff2){
                            var diff3= calcDays(document.getElementById('txtpreQualCloseDate').value,document.getElementById('hdnpreQualCloseDate').value);
                            if(minExtDays >= diff3){
                                var message="Amendment Business Rule Violated. Minimum "+minExtDays+" days must be added to Closing Date "
                                jAlert(message,"Amendment Alert", function(RetVal) {
                                });
                                vbool=false;
                            }
                        }
                    }

                    if(document.getElementById('docMsg')!=null){
                        if(document.getElementById('docMsg').innerHTML!=""){
                            vbool=false;
                        }
                    }

                    <%--if(document.getElementById('docount')==null){
                        if(document.getElementById('stencheck').value=="false"){
                            vbool = false;
                        }
                    }else{
                        for(var i=0;i<document.getElementById('docount').value;i++){
                            var chk = document.getElementById('docFeeslot'+i);
                            if(chk!=null){
                                if(chk.value==""){
                                    document.getElementById('divdocfee'+i).innerHTML="Please enter docfess.";
                                    vbool=false;
                                }else{
                                    if(!(/^(\d+(\.\d{1,2})?)$/.test(chk.value))){
                                        document.getElementById('divdocfee'+i).innerHTML="Enter 2 digits after Decimal.";
                                        vbool=false;
                                    }else{
                                        document.getElementById('divdocfee'+i).innerHTML='';
                                    }
                                }
                            }
                        }
                    }--%>

                    if(document.getElementById("txtindicativeStartDate0")!=null){
                    inStartValSub();
                        }

                    //Eligibility of Tenderer.
                    if(CKEDITOR.instances.txtaeligibilityofTenderer!=null){
                        if($.trim(CKEDITOR.instances.txtaeligibilityofTenderer.getData())=="")
                        {
                             var hdnEvenType = document.getElementById('hdnevenyType').value;
                            if(hdnEvenType.toString().toUpperCase() == 'GOODS' || hdnEvenType.toString().toUpperCase() == 'WORKS'){
                                document.getElementById('spantxtaeligibilityofTenderer').innerHTML='<div class="reqF_1">Please enter Eligibility of Bidder</div>';
                            }else{
                                document.getElementById('spantxtaeligibilityofTenderer').innerHTML='<div class="reqF_1">Please enter Eligibility of Consultant</div>';
                            }
                            vbool=false;
                        }
                        else
                        {
                            if($.trim(CKEDITOR.instances.txtaeligibilityofTenderer.getData()).length>2000){
                                document.getElementById('spantxtaeligibilityofTenderer').innerHTML='<div class="reqF_1">Maximum 2000 characters are allowed</div>';
                                vbool=false;
                            }else
                            {
                                document.getElementById('spantxtaeligibilityofTenderer').innerHTML='';
                            }
                        }
                    }
                    //Brief description of Works
                    if(CKEDITOR.instances.txtabriefDescGoods!=null){
                        if($.trim(CKEDITOR.instances.txtabriefDescGoods.getData())=="")
                        {
                            document.getElementById('spantxtabriefDescGoods').innerHTML='<div class="reqF_1">Please enter Brief Description</div>';
                            vbool=false;
                        }
                        else
                        {
                            if($.trim(CKEDITOR.instances.txtabriefDescGoods.getData()).length>2000){
                                document.getElementById('spantxtabriefDescGoods').innerHTML='<div class="reqF_1">Maximum 2000 characters are allowed</div>';
                                vbool=false;
                            }
                            else
                            {
                                document.getElementById('spantxtabriefDescGoods').innerHTML='';
                            }
                        }
                    }


                    //Taher F
                    var phaseCount = $('#pcount').val();
                    var secureCount = $('#docount').val();

//                    alert(phaseCount);
//                    alert(secureCount);
                    if(phaseCount!=null){
                        var refCount=0;
                        var phaseSer=0;
                        var locationLot=0;
                        var locationRef=0;
                        for(var i=0;i<phaseCount;i++){
//                            alert($('#txtrefNo_'+i).val());
//                            alert($('#txtaphasingService_'+i).val());
//                            alert($('#txtlocationRefNo_'+i).val());
//                            alert($('#locationlot_'+i).val());
                            if($.trim($('#txtrefNo_'+i).val())==""){
                               $('#refno_'+i).html("<br/>Please enter Ref. No.");
                               refCount++;
                            }else{
                                $('#refno_'+i).html(null);
                            }

                            if($.trim($('#txtaphasingService_'+i).val())==""){
                               $('#phaseSer_'+i).html("<br/>Please enter Phasing Service");
                               phaseSer++;
                            }else{
                                $('#phaseSer_'+i).html(null);
                            }

                            if($('#locationlot_'+i).html()!=null){
                                if($.trim($('#locationlot_'+i).val())==""){
                                   $('#locLot_'+i).html("<br/>Please enter Location");
                                   locationLot++;
                                }else{
                                    $('#locLot_'+i).html(null);
                                }
                            }
                            if($('#txtlocationRefNo_'+i).html()!=null){
                                if($.trim($('#txtlocationRefNo_'+i).val())==""){
                                   $('#locRef_'+i).html("<br/>Please enter Location");
                                   locationRef++;
                                }else{
                                    $('#locRef_'+i).html(null);
                                }
                            }

                        }
                        if(refCount>0 || phaseSer>0 || locationLot>0 || locationRef>0){
                            vbool=false;
                        }
                    }
                    //alert('154545:'+vbool);
                    if(secureCount!=null){
                        var locCount=0;
                        for(var i=0;i<secureCount;i++){
                            if($.trim($('#locationlot_'+i).val())==""){
                               $('#locLot_'+i).html("<br/>Please enter Location");
                               locCount++;
                            }else{
                                $('#locLot_'+i).html(null);
                            }
                        }
                        if(locCount>0){
                            vbool=false;
                        }
                    }
                    //alert('vbool : '+vbool);
                    //alert('boolcheck : '+document.getElementById("boolcheck").value);
                    if(vbool==false
                        || document.getElementById("boolcheck7").value=='false'
                        || document.getElementById("boolcheck").value=='false'
                        || document.getElementById("boolcheckUSD").value=='false'){
                        return false;
                    }
                   document.getElementById("btnsubmit").style.display="none";
                }
                function rajesh(value1)
                {
                    return (/^(\d+(\.\d{1,2})?)$/.test(value1));
                }
        </script>
    <script type="text/javascript">
        //Start Dohatec
        if(document.getElementById('txtpreQualDocPrice') !=null && document.getElementById('txtpreQualDocPrice').value != ''){
            document.getElementById('preQualDocPriceInWords').innerHTML =WORD(document.getElementById('txtpreQualDocPrice').value);
        }
        if(document.getElementById('preQualDocPriceUSD') !=null && document.getElementById('preQualDocPriceUSD').value !=''){
            document.getElementById('preQualDocPriceICTInWords').innerHTML =WORD(document.getElementById('preQualDocPriceUSD').value);
        }
        //End Dohatec

        var cntForWords = '<%=i %>';
        for(var cntI=0;cntI<cntForWords;cntI++){
            if(document.getElementById('tenderSecurityAmount_'+cntI) !=null && document.getElementById('tenderSecurityAmount_'+cntI).value !=''){
                $("#tenderSecurityAmount_"+cntI).parent().append("<div class='tenderSecAmtInWords_" + cntI + "'>" + WORD(document.getElementById('tenderSecurityAmount_'+cntI).value) + "</div>");
            }
            if(document.getElementById('tenderSecurityAmountUSD_'+cntI) !=null && document.getElementById('tenderSecurityAmountUSD_'+cntI).value !=''){
                $("#tenderSecurityAmountUSD_"+cntI).parent().append("<div class='tenderSecAmtInWordsUSD_" + cntI + "'>" + WORD(document.getElementById('tenderSecurityAmountUSD_'+cntI).value) + "</div>");
            }
            if(document.getElementById('docFeeslot'+cntI) !=null && document.getElementById('docFeeslot'+cntI).value !=''){
                $("#docFeeslot"+cntI).parent().append("<div class='docFeesInWords_" + cntI + "'>" + WORD(document.getElementById('docFeeslot'+cntI).value) + "</div>");
            }
        }
        function documentPrice(obj){
            document.getElementById('preQualDocPriceInWords').innerHTML = '';
            if(!required(obj.value)){
                document.getElementById('spantxtpreQualDocPrice').innerHTML='<div class="reqF_1">Please enter Tender Document Price</div>';
                return 'false';
            }else if(!numeric($.trim(obj.value))){
                document.getElementById('spantxtpreQualDocPrice').innerHTML='<div class="reqF_1">Please enter only numeric values</div>';
                return 'false';
            }else{
                var docPrice = obj.value.split('.');
                if(docPrice[0]=='0')
                {
                   // document.getElementById('spantxtpreQualDocPrice').innerHTML='<div class="reqF_1">Only 0 value is not allowed</div>';
                   // return 'false';
                }
                else
                {
                      /*  Start Dohatec  */
                    var docPriceBDT = obj.value;
                    if(parseFloat(docPriceBDT)>parseFloat(document.getElementById('docPriceBDTMaxID').value))
                    {
                        document.getElementById('spantxtpreQualDocPrice').innerHTML='<div class="reqF_1">Tender Document Price (In Nu.) can not be greater than '+ document.getElementById('docPriceBDTMaxID').value + ' </div>';
                        return 'false';
                    }
                    else
                    {
                        /*  End Dohatec    */
                        document.getElementById('spantxtpreQualDocPrice').innerHTML='';
                        obj.value = ($.trim(obj.value) * 1);
                        document.getElementById('preQualDocPriceInWords').innerHTML =WORD(obj.value);
                    }
                }
            }
        }
        //Start Dohatec
        function documentPriceUSD(obj){
            document.getElementById('preQualDocPriceICTInWords').innerHTML = '';
            if(!required(obj.value)){
                document.getElementById('spantxtpreQualDocPricemanICT').innerHTML='<div class="reqF_1">Please enter Tender Document Price</div>';
                return 'false';
            }else if(!numeric($.trim(obj.value))){
                document.getElementById('spantxtpreQualDocPricemanICT').innerHTML='<div class="reqF_1">Please enter only numeric values</div>';
                return 'false';
            }else{
                var docPrice = obj.value.split('.');

                if(docPrice[0]=='0')
                {
                  //  document.getElementById('spantxtpreQualDocPricemanICT').innerHTML='<div class="reqF_1">Only 0 value is not allowed</div>';
                  //  return 'false';
                }
                else
                {
                     /*  Start Dohatec  */

                        var docPriceICT = obj.value;
                        
                        if(parseFloat(docPriceICT)> parseFloat(document.getElementById('docPriceUSDMaxID').value))
                        {
                             document.getElementById('spantxtpreQualDocPricemanICT').innerHTML='<div class="reqF_1">Equivalent Tender Document Price (In USD) can not be greater than ' + document.getElementById('docPriceUSDMaxID').value + ' </div>';
                             return 'false';
                        }
                        else
                        {
                            /*  End Dohatec    */
                            document.getElementById('spantxtpreQualDocPricemanICT').innerHTML='';
                            obj.value = ($.trim(obj.value) * 1);
                            document.getElementById('preQualDocPriceICTInWords').innerHTML =WORD(obj.value);
                        }
                }
            }
        }
        function tenderSecAmt(obj){
            var temp = obj.id.split("_");
            var countTSA = temp[1];
            $(".tenderSecAmtInWords_" + countTSA).remove();
            $(obj).parent().append("<div class='tenderSecAmtInWords_" + countTSA + "'>" + WORD(obj.value) + "</div>");
        }
        function tenderSecAmtUSD(obj){
            var temp = obj.id.split("_");
            var countTSA = temp[1];
            $(".tenderSecAmtInWordsUSD_" + countTSA).remove();
            $(obj).parent().append("<div class='tenderSecAmtInWordsUSD_" + countTSA + "'>" + WORD(obj.value) + "</div>");
        }
        //End Dohatec
        function docFeesWord(obj){
            var temp = obj.id.substring(10, obj.id.length)
            var countDF = temp;
            $(".docFeesInWords_" + countDF).remove();
            $(obj).parent().append("<div class='docFeesInWords_" + countDF + "'>" + WORD(obj.value) + "</div>");
        }
        function MakePreBidEndDateDisable()
        {
            if(document.getElementById("txtpreTenderMeetStartDate").value == "")
            {
                document.getElementById("txtpreTenderMeetEndDate").style.visibility = 'hidden';
                document.getElementById("PreBidEndDateTag").style.visibility = 'hidden';
                document.getElementById("txtpreTenderMeetEndDateimg").style.visibility = 'hidden';
            }
            else
            {
                document.getElementById("txtpreTenderMeetEndDate").style.visibility = 'visible';
                document.getElementById("PreBidEndDateTag").style.visibility = 'visible';
                document.getElementById("txtpreTenderMeetEndDateimg").style.visibility = 'visible';
            }
        }
        function ClearPreBidStartDate()
        {
            document.getElementById("txtpreTenderMeetStartDate").value = "";
            document.getElementById("txtpreTenderMeetEndDate").value = "";
            document.getElementById("spantxtpreTenderMeetEndDate").innerHTML = '';
            document.getElementById("demoMeetStart").innerHTML = '';
            document.getElementById("demoMeetEnd").innerHTML = '';
            MakePreBidEndDateDisable();
        }
        
        
        
    </script>
</html>
