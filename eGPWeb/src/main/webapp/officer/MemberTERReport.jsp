<%-- 
    Document   : MemberTERReport
    Created on : Jun 21, 2011, 1:44:19 PM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluation Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    </head>
    <body>
        <%  String tenderId;
            tenderId = request.getParameter("tenderId");
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Evaluation Report</div>
                <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%                    
                    pageContext.setAttribute("tab","7"); 
                %>
                <%@include file="../officer/officerTabPanel.jsp" %>

                <div>&nbsp;</div>
                <%if (request.getParameter("msgId") != null) {
                                String msgId = "", msgTxt = "";
                                boolean isError = false;
                                msgId = request.getParameter("msgId");
                                if (!msgId.equalsIgnoreCase("")) {
                                    if (msgId.equalsIgnoreCase("error")) {
                                        isError = true;
                                        msgTxt = "There was some error.";
                                    } else if (msgId.equalsIgnoreCase("initiated") || msgId.equalsIgnoreCase("closed")) {
                                        msgTxt = "PQ process initiated successfully.";
                                    } else {
                                        msgTxt = "";
                                    }
                %>
                <%if (isError) {%>
                <div class="responseMsg errorMsg"><%=msgTxt%></div>
                <%} else {%>
                <div class="responseMsg successMsg"><%=msgTxt%></div>
                <%}%>
                <%}
                     }%>


                     <%-- Start: Common Evaluation Table --%>
                     <%@include file="/officer/EvalCommCommon.jsp" %>
                     <%-- End: Common Evaluation Table --%>

                <%
                     pageContext.setAttribute("TSCtab", "3");
                %>                
                <div class="t_space">
                <%@include  file="../resources/common/AfterLoginTSC.jsp"%>
                </div>      
                <div class="tabPanelArea_1">
                    <%
                    /*TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                              List<SPTenderCommonData> evalMemStatusCount =
                                            tenderCS.returndata("GetEvalMemfinalStatusCount", tenderId, "0");
                                    List<SPTenderCommonData> getBidderEvaluatedCount =
                                            tenderCS.returndata("getBidderEvaluatedCount", tenderId, "0");
                            // IF THIS COUNT MATCHED WITH BIDDERS COUNT THEN SHOW BELOW BUTTON

                            if (Integer.parseInt(evalMemStatusCount.get(0).getFieldName1()) == getBidderEvaluatedCount.size()) {*/
                    %>
                    <jsp:include page="EvalProcessInclude.jsp">
                            <jsp:param name="tenderid" value="<%=tenderId%>"/>
                            <jsp:param name="stat" value="eval"/>
                             <jsp:param name="evalCount" value="<%=evalCount%>" />
                    </jsp:include>
                    <%//}else{out.print("<div class='responseMsg noticeMsg'>Chairperson has not finalized Evaluation status yet.</div>");}%>
                </div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

