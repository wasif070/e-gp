<%-- 
    Document   : EvalTeamStatus
    Created on : Feb 21, 2011, 12:09:52 PM
    Author     : Swati
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    
        String cuserId = "";
        String tenderid= "";
        String Uid = "";
        boolean ispending = true ;
        boolean isInd = false;
        boolean showLink = false ;
        if(request.getParameter("tenderid")!= null)
            tenderid = request.getParameter("tenderid");
        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
             cuserId = session.getAttribute("userId").toString();
         }
        if(request.getParameter("Uid")!= null)
            Uid = request.getParameter("Uid");
        
      TenderCommonService tenderCommonServiceNominee = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
      
// to check whether it is evalution config type is individual or Team
        for (SPTenderCommonData isIndividual : tenderCommonServiceNominee.returndata("getEvalConfigType", tenderid, "")) {
                if(isIndividual.getFieldName1().equalsIgnoreCase("ind")) {
                        isInd = true;
                    }
        }
        boolean is2Env = false;  
        String commonMemId = null;
        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
        List<SPCommonSearchDataMore> envDataMores = commonSearchDataMoreService.geteGPDataMore("GetEnvCntNCommonMember", tenderid);
        if(envDataMores!=null && (!envDataMores.isEmpty())){
            commonMemId = envDataMores.get(0).getFieldName2();
            if(envDataMores.get(0).getFieldName1().equals("2")){
                is2Env = true;
            }
        }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <% if(isInd){ %>
        <title>Declaration Report of each member of TEC</title>
        <% }else{ %>
        <title>Team Evaluation Consent Status</title>
        <% } %>
        <!--jalert -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
        <%
        String drop = "";
        if(request.getParameter("drop")!= null)
            drop = request.getParameter("drop");
        
       %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">
             <div class="pageHead_1">Tender Dashboard</div>
             <%pageContext.setAttribute("tenderId", tenderid);%>
             <%@include file="../resources/common/TenderInfoBar.jsp" %>
             <div>&nbsp;</div>
              <jsp:include page="officerTabPanel.jsp" >
                     <jsp:param name="tab" value="7" />
                </jsp:include>

              <div class="tabPanelArea_1">



              <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                          if(msgId.equalsIgnoreCase("configchanged")){
                            msgTxt="Configuration Modified successfully.";
                        } else  if(msgId.equalsIgnoreCase("configchangefailed")){
                            isError=true; msgTxt="Error while modifying Configuration.";
                        }  else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                   <div class="responseMsg errorMsg" style="margin-bottom: 12px;" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                   <%}%>
                <%}}%>


              <%@include  file="../officer/EvalCommCommon.jsp" %>


              <%
                          pageContext.setAttribute("TSCtab", "1");
              %>
               <div class="t_space">
                            <%@include file="../resources/common/AfterLoginTSC.jsp"%>
                        </div>

                        <div class="tabPanelArea_1">

   <% List<SPTenderCommonData> checkEval1 = tenderCommonService1.returndata("CheckCommitteeStatus", request.getParameter("tenderid"), "approved");
              if (checkEval1.size() > 0) {
   %>
                    <%--START: Evaluation MEMBERS TABLE--%>
    <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1">
                    <tr>
                        <% if(isInd){ %>
                        <th colspan="6" class="t-align-left">Declaration Report of each member of TEC</th>
                        <% }else { %>
                        <th colspan="4" class="t-align-left">Team Evaluation Consent Status</th>
                        <% } %>
                    </tr>
                    <tr>
                        <th width="4%" class="ff">Sl. No</th>
                        <th width="32%" class="ff">Committee Members </th>
                        <th width="20%" class="ff">Committee Role </th>
                        <th width="24%">Declaration Status </th>
                        <% if(isInd){ %>
                        <th width="30%" class="ff">Action</th>
                        <% } %>
                    </tr>
                    <%
                                int memberCnt = 0, ApprovedMemCnt = 0;
                                boolean isDeclain = false;
                                for (SPTenderCommonData commonTenderDetails : tenderCommonService1.returndata("getTenderEvaluationConfiginfo", tenderid, null)) {
                                   if (!"chairperson".equalsIgnoreCase(commonTenderDetails.getFieldName3())) {
                                    memberCnt++;
                                    ApprovedMemCnt = Integer.parseInt(commonTenderDetails.getFieldName8());
                    %>
                    <tr
                         <%if(Math.IEEEremainder(memberCnt,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
                        >
                        <td class="t-align-center"><%=memberCnt %></td>
                        <td class="ff">

                            <% if (commonTenderDetails.getFieldName5().equalsIgnoreCase("pending")) {
                                    List<SPTenderCommonData> checkvalue = tenderCommonService1.returndata("CheckValueIntblTosRptShare", request.getParameter("tenderid"), null);
                                    if (checkvalue.size() > 0) {
                                        if (cuserId.equals(commonTenderDetails.getFieldName7())) {
                                                List<SPTenderCommonData> evalconfig = tenderCommonService1.returndata("CheckValueInevalconfig", request.getParameter("tenderid"), null);
                                                                if (!evalconfig.isEmpty()) {
                                        %>
                                        
                                            <%=commonTenderDetails.getFieldName1()%>
                                   
                                        <%}else{%>
                                        <%=commonTenderDetails.getFieldName1()%>
                                        <%}%>
                                    <%} else {%>
                                        <%=commonTenderDetails.getFieldName1()%>
                                        <%}%>
                                    <% } else {%>
                                        <%=commonTenderDetails.getFieldName1()%>
                                    <%}%>
                            <%} else {%><%=commonTenderDetails.getFieldName1()%><%}%>
                            <%if(commonTenderDetails.getFieldName7().equals(commonTenderDetails.getFieldName10())){out.print("(Nominated Member)");}%>
                        </td>
                        <td class="t-align-center">
                            <%=commonTenderDetails.getFieldName3()%></td>
                        <td class="t-align-center">
                            <%  
                            for (SPTenderCommonData sptcd : tenderCommonService.returndata("getNomineeStatusforEval", tenderid, commonTenderDetails.getFieldName7())) {
                                    ispending = false;
                            %>

                            <% if(!isInd) { 
                                if(sptcd.getFieldName1().equalsIgnoreCase("Disagreed")){
                                    showLink = true;
                                }
                            %>
                            <% if(sptcd.getFieldName1().equalsIgnoreCase("Pending")){ %>

                            Pending <!--&nbsp; | &nbsp;-->
                                
                            <%      if(sptcd.getFieldName2().equalsIgnoreCase("Live")){                                
                                    if(is2Env && commonTenderDetails.getFieldName7().equals(commonMemId)){
                                        out.print("Decrypter can't be dropped");
                                    }else{
                             %>
                          <!--Drop Member Hide a href="< %=request.getContextPath()%>/EvalNominationServlet?funName=dropMember&tenderId=< %=tenderid%>&nomineeUserId=< %=commonTenderDetails.getFieldName7()%>&cuserId=< %=cuserId%>&committeeId=< %=commonTenderDetails.getFieldName6()%>">Drop Member</a-->
                          <%}} else if(sptcd.getFieldName2().equalsIgnoreCase("dropped")){%>
                                Dropped
                          <% }%>
                          
                            <%} else { %>
                            <%=sptcd.getFieldName1()%>
                            <% } %>

                           <% }else if(isInd){
                                if(sptcd.getFieldName1().equalsIgnoreCase("Agreed")){
                            %>
                            Consented
                            <% } else if(sptcd.getFieldName1().equalsIgnoreCase("Disagreed")){
                                isDeclain =  true;
                            %>
                            Declined
                           <%} else if(sptcd.getFieldName1().equalsIgnoreCase("Pending")){ %>
                           Pending
                            <% }
                                } // end if(isInd)
                                } //end for
                            %>

                        
                        </td>

                         <% if(isInd){ %>
                        <td class="t-align-center">
                            <% if(isDeclain) {
                            for (SPTenderCommonData sptcd : tenderCommonService.returndata("getNomineeStatusforEval", tenderid, commonTenderDetails.getFieldName7())) {
                                     if(sptcd.getFieldName2().equalsIgnoreCase("Live")){
                                    if(is2Env && commonTenderDetails.getFieldName7().equals(commonMemId)){
                                        out.print("Decrypter can't be dropped");
                                     }else{
                            %>
                            <!--Drop Member Hide a href="< %=request.getContextPath()%>/EvalNominationServlet?funName=dropMember&tenderId=< %=tenderid%>&nomineeUserId=< %=commonTenderDetails.getFieldName7()%>&cuserId=< %=cuserId%>&committeeId=< %=commonTenderDetails.getFieldName6()%>">Drop Member</a-->
                            <% }} else{%>
                                Dropped
                            <% }
                                } // end for
                           }%>
                           <% if(isDeclain) {%>
                            &nbsp;|&nbsp;<a href="#">Request Replacement</a>
                            <% } %>
                        </td>                        
                        <% } %>

                    </tr>
                    <% ispending = true ;
                       isDeclain = false;
                       }
                                } //end main for loop
                    %>
                    <% if (memberCnt == 0) {%>
                    <tr>
                        <td colspan="3" class="ff">No members found!</td>
                    </tr>
                    <%}%>
                </table>
                <% } %>
                <% if(showLink) {%>
                <table class="tableView_1 ff" border="0">
                    <tr>
                        <td>
                            <a href="<%=request.getContextPath()%>/EvalConfigurationServlet?func=modifyconfig&tenderId=<%=tenderid%>&refNo=<%=toextTenderRefNo%>">Modify the Evaluation Methodology</a>
                        </td>
                    </tr>
                </table>
                <% } %>

                </div>

                </div>
        </div>
     <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>
