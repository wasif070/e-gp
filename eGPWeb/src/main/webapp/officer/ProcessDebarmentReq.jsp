<%-- 
    Document   : ProcessDebarmentRequest
    Created on : Jan 12, 2011, 10:49:02 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentComments"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.CheckExtension"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentDocs"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentReq"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentDetails"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentTypes"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.web.servicebean.InitDebarmentSrBean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Process Debarment Requests </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>        
    </head>
    <body>
        <%
                    String debarId = request.getParameter("debId");
                    InitDebarmentSrBean srBean = new InitDebarmentSrBean();
                    String status = request.getParameter("stat");
                    //out.println(session.getAttribute("userId").toString());
                    if ("Submit".equalsIgnoreCase(request.getParameter("submit"))) {
                        Date debarStart=null;
                        Date debarEnd=null;
                        String temp =request.getParameter("debarIds");
                        if(request.getParameter("debarStart")!=null){
                            debarStart=DateUtils.formatStdString(request.getParameter("debarStart"));
                            debarEnd=DateUtils.formatStdString(request.getParameter("debarEnd"));
                        }
                        String debStatus=request.getParameter("debarStatus");
                        if((!"6".equals( request.getParameter("debarType"))) && "sendtoegp".equals(debStatus)){
                            debStatus="appdebarhope";
                        }
                        srBean.setLogUserId(session.getAttribute("userId").toString());
                        srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));
                        srBean.updateDebarByHope(debarId, temp.isEmpty() ? "" : temp.substring(0, temp.length()-1),debStatus, request.getParameter("comments"), session.getAttribute("userId").toString(),debarStart,debarEnd,request.getParameter("debarType"),request.getParameter("uname"),request.getParameter("cmpName"));
                        if("appdebarhope".equals(debStatus)){
                            // For NOA decline
                            CommonSearchDataMoreService csdms = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                            csdms.geteGPDataMore("DeclineTendererDebared",debarId);
                        }
                        String isprocess="";
                        //if("sendtocom".equals(request.getParameter("debarStatus"))){
                           isprocess="?isprocess=y";
                        //}
                        response.sendRedirect("HopeDebarListing.jsp"+isprocess);
                    }else if("Submit".equalsIgnoreCase(request.getParameter("hopecp"))){
                        String debStatus=request.getParameter("debarStatus");
                        if((!"6".equals( request.getParameter("debarType"))) && "sendtoegp".equals(debStatus)){
                          debStatus = "appdebarcomhope";
                        }
                        srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));
                        if(srBean.chaipersonReview(debarId, session.getAttribute("userId").toString(), request.getParameter("comments"), debStatus,"hopecp")){
                            if("appdebarcomhope".equals(debStatus)){
                                // For NOA decline
                                CommonSearchDataMoreService csdms = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                                csdms.geteGPDataMore("DeclineTendererDebared",debarId);
                            }
                            response.sendRedirect("HopeDebarListing.jsp");
                        }else{
                            out.print("<b>Error in HOPA Chairperson</b>");
                        }
                    } else {
                        List<Object[]> hopeAns = srBean.findHopeAns(debarId);
                        //0. debarIds, 1. debarStartDt, 2. debarEdnDt, 3. comments, 4. debarTypeId
                        int hopeAnsSize = hopeAns.size();                        
                        String condText=null;
                        if(hopeAnsSize==0){
                            condText="bype";
                        }else{
                            condText="byhope";
                        }                        
                        List<TblDebarmentDetails> dataList = srBean.getDebarDetailsForHope("tblDebarmentReq",Operation_enum.EQ,new TblDebarmentReq(Integer.parseInt(debarId)),"debarStatus",Operation_enum.EQ,condText);
                        SPTenderCommonData data = srBean.viewTendererDebarClari(Integer.parseInt(debarId), status).get(0);
                        List<TblDebarmentTypes> debarTypes = srBean.getAllDebars();
                        String[] debarIds = dataList.get(0).getDebarIds().split(",");
                        List<Object[]> wholeComList = srBean.getWholeCommitteComments(debarId);                        
                        String committeComments=null;
                        Object [] tendPeid = srBean.findPETenderHopeId(debarId);
                        long comAnsCnt = srBean.countCommitteeMember(debarId);
                        String debarTypeId=null;
                        int btnCount=0;
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="pageHead_1">Process Debarment Requests <span style="float:right;"> <a class="action-button-goback" href="HopeDebarListing.jsp<%if(!status.equals("sendtohope")){out.print("?isprocess=y");}%>">Go back</a> </span></div>
         <% 
         if(request.getParameter("fq")!=null){out.print("<br/><div class='responseMsg errorMsg'>"+request.getParameter("fq")+"</div>");}
          if(request.getParameter("fs")!=null){out.print("<br/><div class='responseMsg errorMsg'>Max file size of a single file must not exceed "+request.getParameter("fs")+" MB.</div>");}
          if(request.getParameter("ft")!=null){out.print("<br/><div class='responseMsg errorMsg'>Acceptable file types "+request.getParameter("ft")+".</div>");}
          if(request.getParameter("sf")!=null){out.print("<br/><div class='responseMsg errorMsg'>File already exist</div>");}
          %>
        <form method="post" action="ProcessDebarmentReq.jsp?debId=<%=debarId%>" >
            <input type="hidden" value="<%=objUserName%>" name="uname">
            <input type="hidden" value="<%=DateUtils.formatStdDate(new java.util.Date())%>" id="currDate"/>
            <input type="hidden" value="<%if (data.getFieldName4().equals("-")) {out.print(data.getFieldName5() + " " + data.getFieldName6());} else {out.print(data.getFieldName4());}%>" name="cmpName"/>
            <table class="tableList_1 t_space" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                    </tr>
                    <tr>
                        <th colspan="2">PA Section</th>
                    </tr>
                    <tr>

                        <td class="t-align-left ff" width="16%">Company Name :</td>
                        <td class="t-align-left" width="84%"><%if (data.getFieldName4().equals("-")) {
                                        out.print(data.getFieldName5() + " " + data.getFieldName6());
                                    } else {
                                        out.print(data.getFieldName4());
                                    }%></td>
                    </tr>
                    <tr>

                        <td class="t-align-left ff" valign="top">Clarification Sought :</td>
                        <td class="t-align-left" width="84%"><%out.print(data.getFieldName1());%></td>
                    </tr>
                     <%
                        List<TblDebarmentDocs>  debarmentDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(tendPeid[1].toString()));
                        if(!debarmentDocses.isEmpty()){
                  %>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5" class="t-align-left">Reference documents by PA</th>
                            </tr>
                            <tr>
                                <th class="t-align-center">Sl. No.</th>
                                <th class="t-align-center">Document Name</th>
                                <th class="t-align-center">Document Description</th>
                                <th class="t-align-center">File Size (In KB)</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        int count = 1;
                                        for (TblDebarmentDocs ttcd : debarmentDocses) {
                            %>
                            <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                                <td class="t-align-center"><%=count%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                                <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                                <td class="t-align-center">
                                    <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=tend&stat=<%=status%>&userId=<%=tendPeid[1]%>&uTid=3" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                </td>
                            </tr>
                            <%count++;}%>
                    </table>
                    </td>
                </tr>
                <%}%>
                    <tr>
                        <td class="t-align-left ff">Last  Date for Response :</td>
                        <td class="t-align-left"><%out.print(data.getFieldName2());%></td>
                    </tr>                    
                    <tr>
                        <td class="t-align-left ff" valign="top">Bidder's/Consultant's Response :</td>
                        <td class="t-align-left"><%=data.getFieldName3()%></td>
                    </tr>
                    <%
                            List<TblDebarmentDocs>  tenderDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(tendPeid[0].toString()));
                            if(!tenderDocses.isEmpty()){
                   %>
                    <tr>
                    <td>&nbsp;</td>
                    <td>                        
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5" class="t-align-left">Reference documents by Bidder/Consultant </th>
                            </tr>
                            <tr>
                                <th class="t-align-center">Sl. No.</th>
                                <th class="t-align-center">Document Name</th>
                                <th class="t-align-center">Document Description</th>
                                <th class="t-align-center">File Size (In KB)</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        int count = 1;
                                        for (TblDebarmentDocs ttcd : tenderDocses) {
                            %>
                            <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                                <td class="t-align-center"><%=count%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                                <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                                <td class="t-align-center">
                                    <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=tend&stat=<%=status%>&userId=<%=tendPeid[0]%>&uTid=2" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                </td>
                            </tr>
                            <%count++;}%>
                    </table>
                    </td>
                </tr>
                <%}%>               
                    <tr>
                        <td class="t-align-left ff" valign="top">Action :</td>
                        <td class="t-align-left">
                            <%if (status.equals("pesatisfy")) {
                                            out.print("Satisfactory");
                                        } //else if (status.equals("sendtohope")) {
                                         else{
                                            out.print("Un Satisfactory");
                                        }%>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff" valign="top">
                            Comments by PA :
                        </td>
                        <td class="t-align-left">
                            <%=data.getFieldName9()%>
                        </td>
                    </tr>
                    <tr>
                        <!--Change HOPE to HOPA by Proshanto-->
                        <th colspan="2">HOPA Section</th>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Debarment Type :</td>
                        <td class="t-align-left">
                            <%
                                int i = 0;
                                for (TblDebarmentTypes types : debarTypes) {
                                  if(hopeAnsSize==0){
                            %>
                                        <label><input type="radio" title="<%=types.getDebarType()%>" value="<%=types.getDebarTypeId()%>" name="debarType" id="debarType<%=i%>" onclick="getDisplayData(this);"  <%if(Integer.parseInt(data.getFieldName10())==types.getDebarTypeId()){debarTypeId=data.getFieldName10();out.print("checked");}%>/>&nbsp;<%=types.getDebarType()%></label>&nbsp;&nbsp;&nbsp;
                            <%
                                    }else{                                      
                                      if((Integer)hopeAns.get(0)[4]==types.getDebarTypeId()){
                                          out.print("<input type='hidden' value='"+hopeAns.get(0)[4]+"' name='debarType'/>");
                                          debarTypeId = hopeAns.get(0)[4].toString();
                                          out.print(types.getDebarType());
                                      }
                                    }
                                    i++;
                                }
                            %>
                            <input type="hidden" value="<%=data.getFieldName10()%>" id="debarType" />
                            <input type="hidden" value="<%=data.getFieldName10()%>" id="hdndebarType" />
                            <input type="hidden" value="<%=dataList.get(0).getDebarIds()%>," name="debarIds" id="debarIds"/>
                            <%if(hopeAnsSize==0){%>
                            <span class="c-alignment-right"><a id="addDet" class="action-button-add"  <%if("6".equals(debarTypeId)){out.print("style='display:none;'");}%>>Add Details</a></span>
                            <%}%>
                        </td>
                    </tr>
                    <tr id="trVal" <%if("6".equals(debarTypeId)){out.print("style='display:none;'");}%>>
                        <td class="t-align-left ff">&nbsp;</td>
                        <td class="t-align-left">
                            <%
                                String pageHead1=null;
                                String pageHead2=null;
                                boolean isTenderDeb = false;
                                if(hopeAnsSize!=0){
                                    if(hopeAns.get(0)[4].toString().equals("1")){
                                        pageHead1="Ref No.";
                                        pageHead2="Tender/Proposal Brief";
                                        isTenderDeb = true;
                                    }else if(hopeAns.get(0)[4].toString().equals("2")){
                                        pageHead1="Letter Ref. No.";
                                        pageHead2="Package No.";
                                    }else if(hopeAns.get(0)[4].toString().equals("3")){
                                        pageHead1="Project Name";
                                        pageHead2="Project Code";
                                    }else if(hopeAns.get(0)[4].toString().equals("4")){
                                        pageHead1="PA Office Name";
                                        pageHead2="PA Code";
                                    }else if(hopeAns.get(0)[4].toString().equals("5")){
                                        pageHead1="Department Name";
                                        pageHead2="Department Type";
                                    }
                                }else{
                                    if(data.getFieldName10().equals("1")){
                                        pageHead1="Ref No.";
                                        pageHead2="Tender/Proposal Brief";
                                        isTenderDeb = true;
                                    }else if(data.getFieldName10().equals("2")){
                                        pageHead1="Letter Ref. No.";
                                        pageHead2="Package No.";
                                    }else if(data.getFieldName10().equals("3")){
                                        pageHead1="Project Name";
                                        pageHead2="Project Code";
                                    }else if(data.getFieldName10().equals("4")){
                                        pageHead1="PA Office Name";
                                        pageHead2="PA Code";
                                    }else if(data.getFieldName10().equals("5")){
                                        pageHead1="Department Name";
                                        pageHead2="Department Type";
                                    }
                                 }
                                
                            %>
                            <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                                <tr>                                    
                                    <th class='t-align-center' id="thTender1" <%if(!isTenderDeb){out.print(" style='display: none;' ");}%>>Tender ID</th>
                                    <th class='t-align-center' width='30%'><label id='dlbHead1'><%=pageHead1%></label></th>
                                    <th class='t-align-center' width='50%'><label id='dlbHead2'><%=pageHead2%></label></th>
                                    <%if(hopeAnsSize==0){%>
                                    <th class='t-align-center'>Action</th>
                                    <%}%>
                                </tr>
                                <tbody id="tbodyVal">
                                    <%                                        
                                        List<SPTenderCommonData> list = srBean.searchDataForDebarType(data.getFieldName10(),session.getAttribute("userId").toString());                                        
                                        for(int j=0; j<debarIds.length;j++){
                                            for(SPTenderCommonData sptcd : list){
                                                if(sptcd.getFieldName1().equals(debarIds[j])){                                                    
                                                    out.print("<tr>");
                                                    String viewLink = null;
                                                    if(isTenderDeb){
                                                        out.print("<td>"+sptcd.getFieldName1()+"</td>");
                                                        viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + sptcd.getFieldName1() +"', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'>" + sptcd.getFieldName3() + "</a>";
                                                    }else{
                                                        out.print("<td style='display:none;'></td>");
                                                        viewLink = sptcd.getFieldName3();
                                                    }
                                                    out.print("<td><input type='hidden' value='"+sptcd.getFieldName1()+"' id='debId"+j+"'/>"+sptcd.getFieldName2()+"</td><td>"+viewLink+"</td>");
                                                    if(hopeAnsSize==0){
                                                        out.print("<td><a href='javascript:void(0)' class='action-button-delete t-align-center' onclick='delRow(this,"+sptcd.getFieldName1()+")'>Remove</a></td>");
                                                    }
                                                    out.print("</tr>");
                                                }
                                            }
                                        }                                        
                                    %>
                                </tbody>
                            </table>
                        </td>
                    </tr>                                        
                    <tr>
                        <td class="t-align-left ff" valign="top">Action :</td>
                        <td class="t-align-left">
                            <%if(hopeAnsSize==0){%>
                            <select name="debarStatus" class="formTxtBox_1" id="debarStatus" style="width: 180px;">
                                <option value="sendtoegp">Approve Debarment</option>
                                <option value="hopesatisfy">Disapprove Debarment</option>
                                <option value="sendtocom">Form Review Committee</option>
                            </select>
                            <%}else{                                  
                                    if(status.equals("hopesatisfy")){out.print("Disapprove Debarment");}
                                    if(status.equals("sendtocom")){out.print("Form Review Committee");}
                                    else{out.print("Approve Debarment");}
                            }%>
                        </td>
                    </tr>
                    <tr id="trDate">
                        <td class="t-align-left ff" valign="top">Debarment Period :<%if(hopeAnsSize==0){out.print("<span class='mandatory'>*</span>");}%></td>
                        <td class="t-align-left">
                            <%if(hopeAnsSize==0){%>
                            Start Date:
                            <input type="text" name="debarStart" id="dt1" onfocus="GetCal('dt1','dt1');" class="formTxtBox_1"/>
                            <img id="dtimg1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('dt1','dtimg1');"/>
                            &nbsp;&nbsp;&nbsp;&nbsp;End Date:
                            <input type="text" name="debarEnd" id="dt2" onfocus="GetCal('dt2','dt2');" class="formTxtBox_1"/>
                            <img id="dtimg2" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('dt2','dtimg2');"/>
                            <%}else{out.print("Start Date: "+DateUtils.customDateFormate((Date)hopeAns.get(0)[1])+"&nbsp;&nbsp;&nbsp;&nbsp;End Date: "+DateUtils.customDateFormate((Date)hopeAns.get(0)[2]));}%>
                        </td>
                    </tr>                    
                    <tr>
                        <td class="t-align-left ff" valign="top">Comments : <%if(hopeAnsSize==0){out.print("<span class='mandatory'>*</span>");}%></td>
                        <td class="t-align-left">
                             <%if(hopeAnsSize==0){%>
                            <textarea rows="5" id="comments" name="comments" class="formTxtBox_1" style="width: 400px;"></textarea>
                            <%}else{out.print(hopeAns.get(0)[3]);}%>
                        </td>
                    </tr>
                    <%if(!wholeComList.isEmpty()){%>
                    <tr>
                        <th colspan="2">Committee Section</th>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">&nbsp;</td>
                        <td class="t-align-left">
                            <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                                <tr>
                                    <th class='t-align-center' width='20%'>Employee Name</th>
                                    <th class='t-align-center' width='12%'>Employee Role</th>
                                    <th class='t-align-center' width='70%'>Comments</th>
                                </tr>
                                <%for(Object[] comments : wholeComList){%>
                                <tr>
                                    <%if(comments[3].equals("chairperson")){committeComments=comments[2].toString();}%>
                                    <td><%=comments[0]%></td>                                    
                                    <td class="t-align-center"><%if(comments[1].equals("cp")){out.print("Chair Person");}else{out.print("Member");}%></td>
                                    <td><%if(comments[2]!=null){out.print(comments[2]);}else{out.print("-");}%></td>
                                </tr>
                                <%}%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Chair Person Action: </td>
                        <td class="t-align-left">
                            <%if(committeComments==null){%>
                            <select name="debarStatus" class="formTxtBox_1" id="debarStatus" style="width: 180px;">
                                <option value="sendtoegp">Approve Debarment</option>
                                <option value="comsatisfy">Disapprove Debarment</option>
                            </select>
                            <%}else{if(status.equals("comsatisfy")){out.print("Disapprove Debarment");}else{out.print("Approve Debarment");}}%>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff" valign="top">Chairperson Comments :</td>
                        <td class="t-align-left">
                            <%if(committeComments==null){%>
                            <textarea rows="5" id="comments" name="comments" class="formTxtBox_1" style="width: 400px;"></textarea>
                            <%}else{out.print(committeComments);}%>
                        </td>
                    </tr>
                    <%}%>
                    <%
                        List<TblDebarmentComments> hopecp = srBean.getCommitteeMemComments(debarId, session.getAttribute("userId").toString(),"hopecp");
                        if(!wholeComList.isEmpty()){
                        if(wholeComList.size()==comAnsCnt || wholeComList.size()-1==comAnsCnt){ 
                        if(committeComments!=null){                        
                    %>
                    <tr>
                        <td class="t-align-left ff">Hopa Action on Committee Review: <%if(hopecp.isEmpty()){out.print("<span class='mandatory'>*<span>");}%></td>
                        <td class="t-align-left">
                            <%if(hopecp.isEmpty()){%>
                            <select name="debarStatus" class="formTxtBox_1" id="debarStatus" style="width: 180px;">
                                <option value="sendtoegp">Approve Debarment</option>
                                <option value="comsatisfy">Disapprove Debarment</option>
                            </select>
                            <%}else{if(status.equals("comsatisfy")){out.print("Disapprove Debarment");}else{out.print("Approve Debarment");}}%>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff" valign="top">Hopa Comments on Committee Review: <%if(hopecp.isEmpty()){out.print("<span class='mandatory'>*</span>");}%></td>
                        <td class="t-align-left">
                            <%if(hopecp.isEmpty()){%>
                            <textarea  rows="5" id="comments" name="comments" class="formTxtBox_1" style="width: 400px;"></textarea>
                            <%}else{out.print(hopecp.get(0).getComments());}%>
                        </td>
                    </tr>
                    <%}}}%>
                </tbody>
            </table>
            <%if(hopeAnsSize==0){%>
            <div class="t-align-center t_space">
                <label class="formBtn_1"><input name="submit" id="button3" value="Submit" type="submit" onclick="return validate();"></label>
            </div>
            <%btnCount++;}%>
            <%
                        if(!wholeComList.isEmpty()){
                        if(wholeComList.size()==comAnsCnt || wholeComList.size()-1==comAnsCnt){
                        if(committeComments!=null){
                        if(hopecp.isEmpty()){
                            btnCount++;
            %>
            <div class="t-align-center t_space">
                <label class="formBtn_1"><input name="hopecp" id="button3" value="Submit" type="submit" onclick="return validate();"></label>
            </div>
            <%}}}}%>
        </form>
        <%                
                if("sendtohope".equals(status)){
                out.print("<div style='color: red;font-weight: bold;'>Note : Please ensure reference documents if any are uploaded before clicking on Submit.</div>");
                CheckExtension ext = new CheckExtension();
                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                byte fileSize = configurationMaster.getFileSize();
            %>
            <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/InitDebarment?action=debarDocUpload&debId=<%=debarId%>&user=hope&stat=<%=status%>" enctype="multipart/form-data" name="frmUploadDoc" onsubmit="return validateFile();">
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td class="ff" width="15%"> Select Document   : <span class="mandatory">*</span></td>
                        <td width="85%">
                            <input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:450px; background:none;"/><br/>
                            Acceptable File Types <span class="formNoteTxt">(<%out.print(configurationMaster.getAllowedExtension().replace(",", ", "));%>)</span>
                            <br/>Maximum file size of single file should not exceed <%out.print(fileSize);%>MB.
                            <input type="hidden" value="<%=fileSize%>" id="fileSize"/>
                            <div id="dvUploadFileErMsg" class='reqF_1'></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Description : <span class="mandatory">*</span></td>
                        <td>
                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="txtDescription" style="width:200px;"/>
                            <div id="dvDescpErMsg" class='reqF_1'></div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <label class="formBtn_1" id="lbUpload">
                                <input type="submit" name="upload" id="btnUpload" value="Upload"/>
                            </label>
                        </td>
                    </tr>
                </table>
            </form>
            <%
                }
                List<TblDebarmentDocs>  hopeDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(session.getAttribute("userId").toString()));
                if(!hopeDocses.isEmpty()){
            %>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th colspan="5" class="t-align-left">Reference documents by HOPA</th>
                    </tr>
                    <tr>
                        <th class="t-align-center">Sl. No.</th>
                        <th class="t-align-center">Document Name</th>
                        <th class="t-align-center">Document Description</th>
                        <th class="t-align-center">File Size (In KB)</th>
                        <th class="t-align-center">Action</th>
                    </tr>
                    <%
                                int count = 1;
                                for (TblDebarmentDocs ttcd : hopeDocses) {
                    %>
                    <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                        <td class="t-align-center"><%=count%></td>
                        <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                        <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                        <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=pe&stat=<%=status%>&userId=<%=session.getAttribute("userId")%>&uTid=3" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                            <%if("sendtohope".equals(status)){%>
                            &nbsp;
                            <a href="<%=request.getContextPath()%>/InitDebarment?docId=<%=ttcd.getDebarDocId()%>&docName=<%=ttcd.getDocumentName()%>&action=debarDocDelete&debId=<%=debarId%>&user=pe&stat=<%=status%>&userId=<%=session.getAttribute("userId")%>" title="Remove"><img src="../resources/images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" /></a>
                            <%}%>
                        </td>
                    </tr>
                    <%count++;}%>
           </table>
           <%}%>
           <%
            if(btnCount==0){
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(session.getAttribute("userId").toString()), "userId", EgpModule.Debarment.getName(), "Viewed Debarment Request by Hopa", "");
                }
           %>
        <%@include file="../resources/common/Bottom.jsp" %>
        <%if(hopeAnsSize==0){%>
        <div id="dialog-form" title="Debarment">
            <fieldset>
                <div id="div1">
                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblData1">
                    <tr>
                        <th width="10%" class="t-align-center" width="20%">Select</th>
                        <th class="t-align-center" id="thTender2">Tender ID<br/><input type="text" value="" id="search1" class="formTxtBox_1" style="width: 100px;"/></th>
                        <th class="t-align-center" width="30%"><label id="lbHead1">Ref No.</label><br/><input type="text" value="" id="search2_1" class="formTxtBox_1" style="width: 100px;"/></th>
                        <th class="t-align-center" width="50%"><label id="lbHead2">Tender Brief</label></th>
                    </tr>
                    <tbody id="tbody1">
                        <%                            
                            List<SPTenderCommonData> debar1 = srBean.searchDataForDebarType("1",session.getAttribute("userId").toString());
                            for(SPTenderCommonData data1 : debar1){
                               String checked="";
                               if(data.getFieldName10().equals("1")){
                                   for(int j=0; j<debarIds.length;j++){
                                        if(data1.getFieldName1().equals(debarIds[j])){
                                            checked="checked";
                                        }
                                   }
                               }
                               String viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + data1.getFieldName1() +"', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'>" + data1.getFieldName3() + "</a>";
                               out.print("<tr><td><input "+checked+" chk='1' type='checkbox' value='"+data1.getFieldName1()+"'/></td><td mysearch='1'>"+data1.getFieldName1()+"</td><td mysearch='2'>"+data1.getFieldName2()+"</td><td>"+viewLink+"</td></tr>");
                            }
                        %>
                    </tbody>
                </table>                        
                </div>
                <div id="div2">
                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblData2">
                    <tr>
                        <th width="10%" class="t-align-center" width="20%">Select</th>
                        <th class="t-align-center" width="30%"><label id="lbHead1">Letter Ref. No.</label><br/><input type="text" value="" id="search2_2" class="formTxtBox_1" style="width: 100px;"/></th>
                        <th class="t-align-center" width="50%"><label id="lbHead2">Package No.</label></th>
                    </tr>
                    <tbody id="tbody2">
                         <%
                            List<SPTenderCommonData> debar2 = srBean.searchDataForDebarType("2",session.getAttribute("userId").toString());
                            for(SPTenderCommonData data1 : debar2){
                                String checked="";
                               if(data.getFieldName10().equals("1")){
                                   for(int j=0; j<debarIds.length;j++){
                                        if(data1.getFieldName1().equals(debarIds[j])){
                                            checked="checked";
                                        }
                                   }
                               }
                              out.print("<tr><td><input "+checked+" chk='2' type='checkbox' value='"+data1.getFieldName1()+"'/></td><td mysearch='2'>"+data1.getFieldName2()+"</td><td>"+data1.getFieldName3()+"</td></tr>");
                            }
                        %>
                    </tbody>
                </table>
                </div>
                <div id="div3">
                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblData3">
                    <tr>
                        <th width="10%" class="t-align-center" width="20%">Select</th>
                        <th class="t-align-center" width="30%"><label id="lbHead1">Project Name</label><br/><input type="text" value="" id="search2_3" class="formTxtBox_1" style="width: 100px;"/></th>
                        <th class="t-align-center" width="50%"><label id="lbHead2">Project Code</label></th>
                    </tr>
                    <tbody id="tbody3">
                        <%
                            List<SPTenderCommonData> debar3 = srBean.searchDataForDebarType("3",session.getAttribute("userId").toString());
                            for(SPTenderCommonData data1 : debar3){
                                String checked="";
                               if(data.getFieldName10().equals("1")){
                                   for(int j=0; j<debarIds.length;j++){
                                        if(data1.getFieldName1().equals(debarIds[j])){
                                            checked="checked";
                                        }
                                   }
                               }
                              out.print("<tr><td><input  "+checked+" chk='3' type='checkbox' value='"+data1.getFieldName1()+"'/></td><td mysearch='2'>"+data1.getFieldName2()+"</td><td>"+data1.getFieldName3()+"</td></tr>");
                            }
                        %>
                    </tbody>
                </table>
                </div>
                <div id="div4">
                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblData4">
                    <tr>
                        <th width="10%" class="t-align-center" width="20%">Select</th>
                        <th class="t-align-center" width="30%"><label id="lbHead1">PA Office Name</label><br/><input type="text" value="" id="search2_4" class="formTxtBox_1" style="width: 100px;"/></th>
                        <th class="t-align-center" width="50%"><label id="lbHead2">PA Code</label></th>
                    </tr>
                    <tbody id="tbody4">
                        <%
                            List<SPTenderCommonData> debar4 = srBean.searchDataForDebarType("4",session.getAttribute("userId").toString());
                            for(SPTenderCommonData data1 : debar4){
                                String checked="";
                               if(data.getFieldName10().equals("1")){
                                   for(int j=0; j<debarIds.length;j++){
                                        if(data1.getFieldName1().equals(debarIds[j])){
                                            checked="checked";
                                        }
                                   }
                               }
                              out.print("<tr><td><input "+checked+" chk='4'  type='checkbox' value='"+data1.getFieldName1()+"'/></td><td mysearch='2'>"+data1.getFieldName2()+"</td><td>"+data1.getFieldName3()+"</td></tr>");
                            }
                        %>
                    </tbody>
                </table>
                </div>
                <div id="div5">
                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblData5">
                    <tr>
                        <th width="10%" class="t-align-center" width="20%">Select</th>
                        <th class="t-align-center" width="30%"><label id="lbHead1">Department Name</label><br/><input type="text" value="" id="search2_5" class="formTxtBox_1" style="width: 100px;"/></th>
                        <th class="t-align-center" width="50%"><label id="lbHead2">Department Type</label></th>
                    </tr>
                    <tbody id="tbody5">
                        <%
                             List<SPTenderCommonData> debar5 = srBean.searchDataForDebarType("5",session.getAttribute("userId").toString());
                            for(SPTenderCommonData data1 : debar5){
                                String checked="";
                               if(data.getFieldName10().equals("1")){
                                   for(int j=0; j<debarIds.length;j++){
                                        if(data1.getFieldName1().equals(debarIds[j])){
                                            checked="checked";
                                        }
                                   }
                               }
                              out.print("<tr><td><input "+checked+" chk='5' type='checkbox' value='"+data1.getFieldName1()+"'/></td><td mysearch='2'>"+data1.getFieldName2()+"</td><td>"+data1.getFieldName3()+"</td></tr>");
                            }
                        %>
                    </tbody>
                </table>
                </div>
            </fieldset>
        </div>
        <%}%>
      <%}%>
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <script type="text/javascript">
             $(document).ready(function()
            {
                $('#search1').keyup(function(){
                    searchTable($(this).val(),1);
                });
                $('#search2_1').keyup(function(){
                    searchTable($(this).val(),2);
                });
                $('#search2_2').keyup(function(){
                    searchTable($(this).val(),2);
                });
                $('#search2_3').keyup(function(){
                    searchTable($(this).val(),2);
                });
                $('#search2_4').keyup(function(){
                    searchTable($(this).val(),2);
                });
                $('#search2_5').keyup(function(){
                    searchTable($(this).val(),2);
                });
            });
            function searchTable(inputVal,onsearch)
            {
                var table = $('#tblData'+$('#debarType').val());
                table.find('tr').each(function(index, row)
                {
                    var allCells = $(row).find('td[mysearch="'+onsearch+'"]');
                    if(allCells.length > 0)
                    {
                        var found = false;
                        allCells.each(function(index, td)
                        {
                            var regExp = new RegExp(inputVal, 'i');
                            if(regExp.test($(td).text()))
                            {
                                found = true;
                                return false;
                            }
                        });
                        if(found == true)$(row).show();else $(row).hide();
                    }
                });
            }
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function getDisplayData(rdBtn){
                var rdValue=rdBtn.value;
                $('#debarType').val(rdValue);
                $('#debarIds').val(null);
                $('#tbodyVal').html(null);
                $('#trVal').hide();
                if(rdValue=="6"){
                    $('#addDet').hide();
                    //$('#trVal').hide();
                }else{
                    $('#addDet').show();
                    //$('#trVal').show();
                    if(rdValue=="1"){
                        $('#thTender1').show();
                    }else{
                        $('#thTender1').hide();
                    }
                    //$('#trVal').show();
                    if(rdValue=="1"){
                        $('#dlbHead1').html("Ref No.");
                        $('#dlbHead2').html("Tender/Proposal Brief");
                    }if(rdValue=="2"){
                        $('#dlbHead1').html("Letter Ref. No.");
                        $('#dlbHead2').html("Package No.");
                    }if(rdValue=="3"){
                        $('#dlbHead1').html("Project Name");
                        $('#dlbHead2').html("Project Code");
                    }if(rdValue=="4"){
                        $('#dlbHead1').html("PA Office Name");
                        $('#dlbHead2').html("PA Code");
                    }if(rdValue=="5"){
                        $('#dlbHead1').html("Department Name");
                        $('#dlbHead2').html("Department Type");
                    }
                }
            }
            $(function() {
                $( "#dialog:ui-dialog" ).dialog( "destroy" );
                $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    resizable:false,
                    draggable:false,
                    height: 500,
                    width: 600,
                    modal: true,
                    buttons: {
                        "Add": function() {
                            $(function() {
                                var data="";
                                $("#tbodyVal").html(null);
                                $(":checkbox[checked='true']").each(function(){
                                    if($("#debarType").val()=="1"){
                                        $("#tbodyVal" ).append("<tr><td>"+$(this).parent('td').parent('tr').children()[1].innerHTML+"</td><td>"+$(this).parent('td').parent('tr').children()[2].innerHTML+"</td><td>"+$(this).parent('td').parent('tr').children()[3].innerHTML+"</td><td><a href='javascript:void(0)' class='action-button-delete t-align-center' onclick='delRow(this,"+$(this).val()+")'>Remove</a></td></tr>");
                                    }else{
                                        $("#tbodyVal" ).append("<tr><td>"+$(this).parent('td').parent('tr').children()[1].innerHTML+"</td><td>"+$(this).parent('td').parent('tr').children()[2].innerHTML+"</td><td><a href='javascript:void(0)' class='action-button-delete t-align-center' onclick='delRow(this,"+$(this).val()+")'>Remove</a></td></tr>");
                                    }
                                    data+=$(this).val()+",";
                                });
                                $('#debarIds').val(data);
                                $('#trVal').show();
                            });
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    },
                    close: function() {
                    }
                });

                $("#addDet" ).click(function(){
                    //if($('#debarType').val()!=$('#hdndebarType').val()){
                        $(":checkbox[checked='true']").each(function(){
                            /*alert('chk : '+$(this).attr('chk'));
                            alert('deb : '+$('#debarType').val());*/
                            if($(this).attr('chk')!=$('#debarType').val()){
                                 $(this).removeAttr("checked");
                            }
                        });
                    //}
                    $('#div1').hide();
                    $('#div2').hide();
                    $('#div3').hide();
                    $('#div4').hide();
                    $('#div5').hide();
                    $('#div'+$('#debarType').val()).show();
                    $("#dialog-form").dialog("open");
                });
            });
            function validate(){
                $(".err").remove();
                var valid = true;
                if($("#comments").val()=="") {
                    $("#comments").parent().append("<div class='err' style='color:red;'>Please enter Comments.</div>");
                    valid=false;
                }
                if($("#debarType").val() != "6") {
                        if($("#debarIds").val() == "") {
                            $("#debarIds").parent().append("<div class='err' style='color:red;'>Please Add Details of "+$("#debarType"+eval($("#debarType").val()-1)).attr('title')+".</div>");
                            valid = false;
                        }
                }
                if($('#debarStatus').val()!="hopesatisfy"){
                    if($("#dt1").val() == "") {
                          $("#dt2").parent().append("<div class='err' style='color:red;'>Please select Start Date.</div>");
                          valid = false;
                    }
                    if($("#dt2").val()=="") {
                          $("#dt2").parent().append("<div class='err' style='color:red;'>Please select End Date.</div>");
                          valid = false;
                    }
                    if($("#dt1").val()!="" && $("#dt2").val()!="") {

                        if(!CompareToForGreater($('#dt1').val(),$('#currDate').val())){
                            $("#dt2").parent().append("<div class='err' style='color:red;'>Start date must be greater than current date.</div>");
                            valid = false;
                        }else{
                            if(CompareToForGreater($('#dt1').val(),$('#dt2').val())){
                                $("#dt2").parent().append("<div class='err' style='color:red;'>Start date must be smaller than end date.</div>");
                                valid = false;
                            }
                        }
                    }
                }
                if(!valid){
                    return false;
                }
            }
            $(function() {
                $('#debarStatus').change(function() {
                    if($('#debarStatus').val()=="hopesatisfy"){
                       $('#trDate').hide();
                    }else{
                        $('#trDate').show();
                    }
                });
            });
        function CompareToForGreater(value,params)
                {

                var mdy = value.split('/')  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');


                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                {
                    //alert('Both Date');
                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                }
                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                {
                    //alert('Both DateTime');
                    var mdyhrsec=mdyhr[1].split(':');
                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec=mdyphr[1].split(':');
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                }
                else
                {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if(a == undefined && b != undefined)
                    {
                        //alert('First Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('Second Date');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                    }
                }

                return Date.parse(date) >= Date.parse(datep);
            }
            function validateFile(){
               $(".err").remove();
               var count = 0;
               if($("#uploadDocFile").val()=="") {
                    $("#uploadDocFile").parent().append("<div class='err' style='color:red;'>Please select File.</div>");
                      count++;
                }
                if($.trim($("#txtDescription").val())=="") {
                    $("#txtDescription").parent().append("<div class='err' style='color:red;'>Please enter Description.</div>");
                      count++;
                }
                if(count==0){
                    var browserName=""
                    var maxSize = parseInt($('#fileSize').val())*1024*1024;
                    var actSize = 0;
                    var fileName = "";
                    jQuery.each(jQuery.browser, function(i, val) {
                         browserName+=i;
                    });
                    $(":input[type='file']").each(function(){
                        if(browserName.indexOf("mozilla", 0)!=-1){
                            actSize = this.files[0].size;
                            fileName = this.files[0].name;
                        }else{
                            var file = this;
                            var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                            var filepath = file.value;
                            var thefile = myFSO.getFile(filepath);
                            actSize = thefile.size;
                            fileName = thefile.name;
                        }
                        if(parseInt(actSize)==0){
                            $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                            count++;
                        }
                        if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                            $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                            count++;
                        }
                        if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                            $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                            count++;
                        }
                        });
                }
                if(count==0){
                  $('#btnUpload').attr("disabled", "disabled");
                  return true;
                }else{
                   return false;
                }
            }
            function delRow(row,val){
                var curRow = $(row).closest('tr');
                var ids = $('#debarIds').val();
                $('#debarIds').val(ids.replace(val+",",''));
                $(":checkbox[checked='true']").each(function(){
                    if($(this).attr('chk')==$('#debarType').val() && $(this).val()==val){
                         $(this).removeAttr("checked");
                    }
                });
                curRow.remove();
            }
        </script>
</html>