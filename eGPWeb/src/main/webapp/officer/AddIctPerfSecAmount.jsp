<%--
    Document   : AddIctPerfSecAmount
    Created on : Sep 2, 2012, 3:53:57 PM
    Author     : Md. Khaled Ben Islam, G.M. Salahuddin
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TblTenderPerformanceSec"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.PerformanceSecurityICT"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPerfSecurity"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!--Added by salahuddin on 11/11/2012-->
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.GetCurrencyAmount"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

    <script type="text/javascript">
        /*function calcPer(rPrice, field, noOfCurrency){
            var per;
            if(field==1)
                per = $('#percentage1').val();
            else if(field==2)
                per = $('#percentage2').val();
            else if(field==3)
                per = $('#percentage3').val();
            else if(field==4)
                per = $('#percentage4').val();

            if(!isNaN(per)){
                if(field==1)
                    {
                        $('#pSecurityAmount1').val(Math.floor((rPrice*per)/100));
                        $('#labelSec1').html(Math.floor((rPrice*per)/100)) ;
                    }
                else if(field==2)
                    $('#pSecurityAmount2').val(Math.floor((rPrice*per)/100));
                else if(field==3)
                    $('#pSecurityAmount3').val(Math.floor((rPrice*per)/100));
                else
                    $('#pSecurityAmount4').val(Math.floor((rPrice*per)/100));
            }else{
                //$('#percentage').val('');
                if(field==1)
                per = $('#percentage1').val('');
            else if(field==2)
                per = $('#percentage2').val('');
            else if(field==3)
                per = $('#percentage3').val('');
            else if(field==4)
                per = $('#percentage4').val('');
                jAlert("Please enter digits only.","Performance Security alert", function(RetVal) {
                });
            }
        }*/
        function calPercentage()
        {
            var noOfCurrency = $('#noOfCurrency').val();
            var amount;
            var percentage = $('#percentage1').val();

            if(noOfCurrency>=1)
            {
                amount = $('#value1').val();                
                $('#pSecurityAmount1').val(Math.floor((amount*percentage)/100));
                $('#labelSec1').html(Math.floor((amount*percentage)/100)) ;
            }
            if(noOfCurrency>=2)
            {
                amount = $('#value2').val();
                $('#percentage2').val(percentage);
                $('#labelPer2').html(percentage);
                $('#pSecurityAmount2').val(Math.floor((amount*percentage)/100));
                $('#labelSec2').html(Math.floor((amount*percentage)/100)) ;
            }
            if(noOfCurrency>=3)
            {
                amount = $('#value3').val();
                $('#percentage3').val(percentage);
                $('#labelPer3').html(percentage);
                $('#pSecurityAmount3').val(Math.floor((amount*percentage)/100));
                $('#labelSec3').html(Math.floor((amount*percentage)/100)) ;
            }
            if(noOfCurrency>=4)
            {
                amount = $('#value4').val();
                $('#percentage4').val(percentage);
                $('#labelPer4').html(percentage);
                $('#pSecurityAmount4').val(Math.floor((amount*percentage)/100));
                $('#labelSec4').html(Math.floor((amount*percentage)/100)) ;
            }
        }
    </script>

    <head>        
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <%
                    // Variable tenderId is defined by u on ur current page.
                    String cAmount="";
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    pageContext.setAttribute("tab", "2");
                    TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                    List<TblTenderLotSecurity> lots = null;
                    int tendid = 0;
                    int pckgId = 0;
                    String roundId="0";
                    if(request.getParameter("rId")!=null){
                        roundId = request.getParameter("rId");
                    }
                    if (request.getParameter("tenderId") != null) {
                        tendid = Integer.parseInt(request.getParameter("tenderId"));
                        pckgId = Integer.parseInt(request.getParameter("pkgId"));
                        lots = tenderDocumentSrBean.getLotDetailsByLotId(tendid, pckgId);
                    }
                    TblTenderPerformanceSec tenderPerformanceSec = (TblTenderPerformanceSec) AppContext.getSpringBean("TblTenderPerformanceSec");
                    List<TblTenderPerfSecurity> listt = tenderPerformanceSec.getData(tendid, pckgId + "",roundId);
                    //System.out.println("The list size is #####################"+listt.size());
                    int noOfPerfsecurity = listt.size();
                    /*if("Use STD".equalsIgnoreCase(request.getParameter("submit"))){
                    tenderDocumentSrBean.dumpSTD(String.valueOf(tendid), request.getParameter("txtStdTemplate"), session.getAttribute("userId").toString());
                    response.sendRedirect("LotDetails.jsp?tenderid="+tendid);
                    }*/


                    if("view".equals(request.getParameter("viewAction")))
                    {
                            // Coad added by Dipal for Audit Trail Log.
                            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            String idType="tenderId";
                            int auditId=tendid;
                            String auditAction="View Performance security by PE";
                            String moduleName=EgpModule.Noa.getName();
                            String remarks="";
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    }


                    boolean action = false;
                    if("view".equals(request.getParameter("viewAction")))
                    {
                        action = true;
                    }
                    CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                    String roundAmount = "0";
                    List<SPCommonSearchDataMore> roundAmountLst =  dataMoreService.geteGPData("LowestAmtByRound",roundId);
                    if(roundAmountLst!=null && (!roundAmountLst.isEmpty())){
                        roundAmount = roundAmountLst.get(0).getFieldName1();
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Performance Security</title>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>

        <div class="contentArea_1">
            <div class="pageHead_1">Performance Security
                <span class="c-alignment-right"><a href="NOA.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go back</a></span>
            </div>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>

            <%
                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                String procnature = commonService.getProcNature(tendid + "").toString();

            %>

            <%
                //Added by Salahuddin
            
                String  tenderId="";
                if (request.getParameter("tenderId") != null) {
                        tenderId = request.getParameter("tenderId");
                    }
                PerformanceSecurityICT perfSecurityICT  = (PerformanceSecurityICT) AppContext.getSpringBean("PerformanceSecurityICT");
                List<GetCurrencyAmount> currencyAmount = new ArrayList<GetCurrencyAmount>();
                currencyAmount= perfSecurityICT.getCurrencyAmount(Integer.parseInt(tenderId), Integer.parseInt(roundId));
                int count=0;
                if(currencyAmount.get(0).getValue1()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue2()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue3()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue4()!=0)
                    count=count+1;                
            %>
            <%
                //Added by Salahuddin

                List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", tenderId, null);
                boolean isIctTender = false;
                if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                    isIctTender = true;
                }
                //if(isIctTender){
            %>

            <div class="tabPanelArea_1 ">
                <form name="frmPerSec" method="post" action="<%= request.getContextPath() %>/TenderPerformanceSecurity">
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <%
                                if (procnature.equalsIgnoreCase("services")) {
                            %>
                                <th width="10%" class="t-align-center">Package No.</th>
                                <th width="30%" class="t-align-left">Package Description</th>
                            <%} else {%>
                                <th width="10%" class="t-align-center">Lot No.</th>
                                <th width="30%" class="t-align-left">Lot Description</th>
                            <%}%>
                            <th width="10%" class="t-align-left">Currency</th>
                            <th width="15%" class="t-align-left">Lowest Amount</th>
                            <th width="15%" class="t-align-left">Percentage</th>
                            <th width="20%" class="t-align-left">Performance Security Amount</th>
                        </tr>
                        <%if(count>=1){%>
                        <tr>                        

                            <%if(currencyAmount.get(0).getValue1()!=0){%>

                            <td rowspan="<%=count%>" class="t-align-center">1</td>
                            <td rowspan="<%=count%>">Lot Description</td>                            

                            <td width="11%" class="t-align-center"><%=currencyAmount.get(0).getCurrencyName1()%>
                                <input type="hidden" value="<%=currencyAmount.get(0).getCurrencyName1()%>" name="currencyName1"/>
                                <input type="hidden" value="<%=roundId%>" name="rId"/>
                            </td>
                            <td width="17%" class="t-align-center"><%cAmount = new DecimalFormat("##.##").format(currencyAmount.get(0).getValue1()); out.println(cAmount);%>
                                <input type="hidden" value="<%=currencyAmount.get(0).getValue1()%>" name="value1" id="value1"/>
                                <input type="hidden" value="<%=currencyAmount.get(0).getCurrencyID1()%>" name="currencyID1"/>
                                <input type="hidden" value="<%=count%>" name="noOfCurrency" id="noOfCurrency"/>
                            </td>
                                                        
                            <td width="10%" class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    cAmount = new DecimalFormat("##.##").format(listt.get(0).getPercentage());
                                    if(action){                                            
                                            if(noOfPerfsecurity>=1)
                                                {
                                                    //cAmount = new DecimalFormat("##.##").format(listt.get(0).getPercentage());
                                                    out.println(cAmount);
                                                }

                                    }else {%>
                                    <input type="text" class="formTxtBox_1" style="text-align: center;" name="percentage1" id="percentage1" value="<%if(noOfPerfsecurity>=1){out.println(cAmount);}%>" onblur="calPercentage();" />
                                    <input type="hidden" name="forupdate1" value="<%if (!listt.isEmpty() && listt != null) {out.print(listt.get(0).getPerCostLotId());}%>"/>
                                <%}} else {%>
                                <input type="text" class="formTxtBox_1" style="text-align: center;" name="percentage1" id="percentage1" onblur="calPercentage('<%=noOfPerfsecurity%>');" />
                                <%}%>
                            </td>
                            <td width="22%" class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    cAmount = new DecimalFormat("##.##").format(listt.get(0).getPerSecurityAmt());
                                    if(action){
                                            if(noOfPerfsecurity>=1)
                                                {
                                                    //cAmount = new DecimalFormat("##.##").format(listt.get(0).getPerSecurityAmt());
                                                    out.println(cAmount);
                                                }
                                    }else {%>
                                    <input type="hidden" class="formTxtBox_1" name="pSecurityAmount1" id="pSecurityAmount1" value="<%if(noOfPerfsecurity>=1){out.println(cAmount);}%>" />
                                    <label id="labelSec1" ><%if(noOfPerfsecurity>=1){out.println(cAmount);}%></label>
                                <%}} else {%>
                                <input type="hidden" class="formTxtBox_1 "name="pSecurityAmount1" id="pSecurityAmount1" />
                                <label id="labelSec1" ></label>

                                <%}%>
                            </td>
                            <%}%>                        
                        </tr>
                        <%}%>



                        <%if(count>=2){%>
                        <tr>
                            <%if(currencyAmount.get(0).getValue2()!=0){%>
                            <td width="11%" class="t-align-center"><%=currencyAmount.get(0).getCurrencyName2()%>
                                <input type="hidden" value="<%=currencyAmount.get(0).getCurrencyName2()%>" name="currencyName2"/>                                
                            </td>
                            <td width="17%" class="t-align-center"><%cAmount = new DecimalFormat("##.##").format(currencyAmount.get(0).getValue2()); out.println(cAmount);%>
                                <input type="hidden" value="<%=currencyAmount.get(0).getValue2()%>" name="value2" id="value2"/>
                                <input type="hidden" value="<%=currencyAmount.get(0).getCurrencyID2()%>" name="currencyID2"/>
                            </td>

                            <td width="10%" class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    cAmount = new DecimalFormat("##.##").format(listt.get(1).getPercentage());
                                    if(action){
                                            if(noOfPerfsecurity>=2)
                                                {
                                                    //cAmount = new DecimalFormat("##.##").format(listt.get(1).getPercentage().toString());
                                                    out.println(cAmount);
                                                }

                                    }else {%>
                                    <input type="hidden" class="formTxtBox_1"name="percentage2" id="percentage2" value="<%if(noOfPerfsecurity>=2){out.println(cAmount);}%>" />
                                    <label id="labelPer2"><%if(noOfPerfsecurity>=2){out.println(cAmount);}%></label>
                                    <input type="hidden" name="forupdate2" value="<%if (!listt.isEmpty() && listt != null) {out.print(listt.get(1).getPerCostLotId());}%>"/>
                                <%}} else {%>
                                <input type="hidden" class="formTxtBox_1" name="percentage2" id="percentage2" />
                                <label id="labelPer2"></label>
                                <%}%>
                            </td>
                            <td width="22%" class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    cAmount = new DecimalFormat("##.##").format(listt.get(1).getPerSecurityAmt());
                                    if(action){
                                            if(noOfPerfsecurity>=2)
                                                {
                                                    //cAmount = new DecimalFormat("##.##").format(listt.get(1).getPerSecurityAmt().toString());
                                                    out.println(cAmount);
                                                }

                                    }else {%>
                                    <input type="hidden" class="formTxtBox_1" name="pSecurityAmount2" id="pSecurityAmount2" value="<%if(noOfPerfsecurity>=2){out.println(cAmount);}%>" />
                                    <label id="labelSec2"><%if(noOfPerfsecurity>=2){out.println(cAmount);}%></label>
                                <%}} else {%>
                                <input type="hidden" class="formTxtBox_1 "name="pSecurityAmount2" id="pSecurityAmount2" />
                                <label id="labelSec2" ></label>
                                <%}%>
                            </td>
                            <%}%>
                        </tr>
                        <%}%>

                        <%if(count>=3){%>
                        <tr>
                            <%if(currencyAmount.get(0).getValue3()!=0){%>
                            <td width="11%" class="t-align-center"><%=currencyAmount.get(0).getCurrencyName3()%>
                                <input type="hidden" value="<%=currencyAmount.get(0).getCurrencyName3()%>" name="currencyName3"/>
                            </td>
                            <td width="17%" class="t-align-center"><%cAmount = new DecimalFormat("##.##").format(currencyAmount.get(0).getValue3()); out.println(cAmount);%>
                                <input type="hidden" value="<%=currencyAmount.get(0).getValue3()%>" name="value3" id="value3"/>
                                <input type="hidden" value="<%=currencyAmount.get(0).getCurrencyID3()%>" name="currencyID3"/>
                            </td>

                            <td width="10%" class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    cAmount = new DecimalFormat("##.##").format(listt.get(2).getPercentage());
                                    if(action){
                                            if(noOfPerfsecurity>=3)
                                                out.println(cAmount);

                                    }else {%>
                                    <input type="hidden" class="formTxtBox_1" name="percentage3" id="percentage3" value="<%if(noOfPerfsecurity>=3){out.println(cAmount);}%>" />
                                    <label id="labelPer3"><%if(noOfPerfsecurity>=3){out.println(cAmount);}%></label>
                                    <input type="hidden" name="forupdate3" value="<%if (!listt.isEmpty() && listt != null) {out.print(listt.get(2).getPerCostLotId());}%>"/>
                                <%}} else {%>
                                <input type="hidden" class="formTxtBox_1" name="percentage3" id="percentage3"  />
                                <label id="labelPer3"></label>
                                <%}%>
                            </td>
                            <td width="22%" class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    cAmount = new DecimalFormat("##.##").format(listt.get(2).getPerSecurityAmt());
                                    if(action){
                                            if(noOfPerfsecurity>=3)
                                                out.println(cAmount);

                                    }else {%>
                                    <input type="hidden" class="formTxtBox_1" name="pSecurityAmount3" id="pSecurityAmount3" value="<%if(noOfPerfsecurity>=3){out.println(cAmount);}%>" />
                                    <label id="labelSec3" ><%if(noOfPerfsecurity>=3){out.println(cAmount);}%></label>
                                <%}} else {%>
                                <input type="hidden" class="formTxtBox_1" name="pSecurityAmount3" id="pSecurityAmount3" />
                                <label id="labelSec3" ></label>
                                <%}%>
                            </td>
                            <%}%>
                        </tr>
                        <%}%>


                        <%if(count>=4){%>
                        <tr>
                            <%if(currencyAmount.get(0).getValue4()!=0){%>
                            <td width="11%" class="t-align-center"><%=currencyAmount.get(0).getCurrencyName4()%>
                                <input type="hidden" value="<%=currencyAmount.get(0).getCurrencyName4()%>" name="currencyName4"/>
                            </td>
                            <td width="17%" class="t-align-center"><%cAmount = new DecimalFormat("##.##").format(currencyAmount.get(0).getValue4()); out.println(cAmount);%>
                                <input type="hidden" value="<%=currencyAmount.get(0).getValue4()%>" name="value4" id="value4"/>
                                <input type="hidden" value="<%=currencyAmount.get(0).getCurrencyID4()%>" name="currencyID4"/>
                            </td>

                            <td width="10%" class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    cAmount = new DecimalFormat("##.##").format(listt.get(3).getPercentage());
                                    if(action){
                                            if(noOfPerfsecurity>=4)
                                                out.println(cAmount);

                                    }else {%>
                                    <input type="hidden" class="formTxtBox_1" name="percentage4" id="percentage4" value="<%if(noOfPerfsecurity>=4){out.println(cAmount);}%>" />
                                    <label id="labelPer4"><%if(noOfPerfsecurity>=4){out.println(cAmount);}%></label>
                                    <input type="hidden" name="forupdate4" value="<%if (!listt.isEmpty() && listt != null) {out.print(listt.get(3).getPerCostLotId());}%>"/>
                                <%}} else {%>
                                <input type="hidden" class="formTxtBox_1" name="percentage4" id="percentage4" />
                                <label id="labelPer4"></label>
                                <%}%>
                            </td>
                            <td width="22%" class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    cAmount = new DecimalFormat("##.##").format(listt.get(3).getPerSecurityAmt());
                                    if(action){
                                            if(noOfPerfsecurity>=4)
                                                out.println(cAmount);

                                    }else {%>
                                    <input type="hidden" class="formTxtBox_1" name="pSecurityAmount4" id="pSecurityAmount4" value="<%if(noOfPerfsecurity>=4){out.println(cAmount);}%>" />
                                    <label id="labelSec4"><%if(noOfPerfsecurity>=4){out.println(cAmount);}%></label>
                                <%}} else {%>
                                <input type="hidden" class="formTxtBox_1" name="pSecurityAmount4" id="pSecurityAmount4"/>
                                <label id="labelSec4"></label>
                                <%}%>
                            </td>
                            <%}%>
                        </tr>
                        <%}%>

                     
                        <% if(!action){ %>
                        <tr>
                            <td colspan="6" class="t-align-center ff">

                                    <span class="formBtn_1">
                                        <input type="submit" name="submit" id="submit" value="<%if (request.getParameter("isedit") != null) {out.print("Update");}
                                            else{out.print("Submit");}
                                        %>" />                                        
                                    </span>&nbsp;
                                    <span class="formBtn_1">
                                        <a href="NOA.jsp?tenderId=<%=request.getParameter("tenderId")%>" style="color: #ffffff; padding: 0 8px; text-decoration: none;">Cancel</a>
                                    </span>

                                    <input type="hidden" name="NoOfCurrency" value="<%=count%>" />
                                    <input type="hidden" name="tenderid" value="<%=tendid%>" />
                                    <input type="hidden" value="ICT" name="tenderType"/>
                                    <input type="hidden" name="pckid" value="<%=request.getParameter("pkgId")%>" />                                    
                                    <input type="hidden" name="isEdit" value="<%if (request.getParameter("isedit") != null) {out.print(request.getParameter("isedit"));}%>" />
                            </td>
                        </tr>
                        <% } %>
                    </table>
                </form>
            </div>
        </div>
        <div>&nbsp;</div>

        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
    </body>
</html>