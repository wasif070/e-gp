<%--
    Document   : IssueNOA
    Created on : Dec 24, 2011, 12:18:43 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpMaster"%>
<%@page import="com.cptu.egp.eps.web.databean.IssueNOADtBean"%>
<%@page import="javax.print.DocFlavor.STRING"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        tenderId = request.getParameter("tenderId");
                    }

                    String pckLotId = "";
                    if (request.getParameter("pckLotId") != null) {
                        pckLotId = request.getParameter("pckLotId");
                    }

                    String NOADA = "";
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String procurementNature = commonService.getProcNature(tenderId).toString();
                    if(procurementNature.equalsIgnoreCase("Services")){
                        NOADA = "Draft Agreement";
                    }else{
                        NOADA = "NOA";
                    }
                  
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
           Issue Letter of Acceptance
        </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
        <jsp:useBean id="issueNOADtBean" scope="request" class="com.cptu.egp.eps.web.databean.IssueNOADtBean"/>
        <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
        <jsp:setProperty name="issueNOADtBean" property="*"/>

        <%if ("Submit".equals(request.getParameter("hdnbutton"))) 
        {
             // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType="tenderId";
            int auditId=Integer.parseInt(tenderId);
            String roundId="";
            String auditAction="Issuance of NOA";
            String moduleName=EgpModule.Noa.getName();
            String remarks="";
            CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            List<SPCommonSearchDataMore> envDataMores = commonSearchDataMoreService.geteGPData("GetTenderEnvCount", tenderId);
            
            //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+request.getParameter("tenderType"));
            ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            List<SPTenderCommonData> listTender = tenderCommonService.returndata("GetEvaluatedBiddersRoundWise", tenderId, pckLotId);
            for (SPTenderCommonData companyListDetails : listTender)   
            {
                roundId = companyListDetails.getFieldName3();
            }
            List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", tenderId, null);
            boolean isIctTender = false;
            if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                  isIctTender = true;
            }
            
            if(isIctTender)
            {
              
                //System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                int NoOfCurrency = Integer.parseInt(request.getParameter("NoOfCurrency"));
                if(NoOfCurrency>=1)
                {
                    issueNOADtBean.setContractAmt1(request.getParameter("contractAmt1"));
                    issueNOADtBean.setContractAmtWords1(request.getParameter("contractAmtWords1"));
                    issueNOADtBean.setPerSecAmt1(request.getParameter("perSecAmt1"));
                    issueNOADtBean.setPerSecAmtWords1(request.getParameter("perSecAmtWords1"));
                    issueNOADtBean.setCurrencyID1(request.getParameter("currencyID1"));
                }
                if(NoOfCurrency>=2)
                {
                    issueNOADtBean.setContractAmt2(request.getParameter("contractAmt2"));
                    issueNOADtBean.setContractAmtWords2(request.getParameter("contractAmtWords2"));
                    issueNOADtBean.setPerSecAmt2(request.getParameter("perSecAmt2"));
                    issueNOADtBean.setPerSecAmtWords2(request.getParameter("perSecAmtWords2"));
                    issueNOADtBean.setCurrencyID2(request.getParameter("currencyID2"));
                }
                if(NoOfCurrency>=3)
                {
                    issueNOADtBean.setContractAmt3(request.getParameter("contractAmt3"));
                    issueNOADtBean.setContractAmtWords3(request.getParameter("contractAmtWords3"));
                    issueNOADtBean.setPerSecAmt3(request.getParameter("perSecAmt3"));
                    issueNOADtBean.setPerSecAmtWords3(request.getParameter("perSecAmtWords3"));
                    issueNOADtBean.setCurrencyID3(request.getParameter("currencyID3"));
                }
                if(NoOfCurrency>=4)
                {
                    issueNOADtBean.setContractAmt4(request.getParameter("contractAmt4"));
                    issueNOADtBean.setContractAmtWords4(request.getParameter("contractAmtWords4"));
                    issueNOADtBean.setPerSecAmt4(request.getParameter("perSecAmt4"));
                    issueNOADtBean.setPerSecAmtWords4(request.getParameter("perSecAmtWords4"));
                    issueNOADtBean.setCurrencyID4(request.getParameter("currencyID4"));
                }
                //dohatec start
                if (issueNOASrBean.addIssueNOAForICT(issueNOADtBean, roundId, tenderId, false, NoOfCurrency, session.getAttribute("userId").toString()))
                {
                  
                if(request.getParameter("amountCnt")!= "" )// && Integer.parseInt(request.getParameter("amountCnt")) > 0)
                {
                   if(Integer.parseInt(request.getParameter("amountCnt")) > 0){
                   List<TblCmsWpMaster> masters = new ArrayList<TblCmsWpMaster>();

                   TblCmsWpMaster cmsWpMaster = new TblCmsWpMaster();


                   cmsWpMaster.setCreatedBy(Integer.parseInt(session.getAttribute("userId").toString()));
                   cmsWpMaster.setCreatedDate(new java.util.Date());
                   cmsWpMaster.setWpLotId(Integer.parseInt(request.getParameter("pckLotId")));
                   cmsWpMaster.setWpName("Consolidate");
                   cmsWpMaster.setIsDateEdited("no");
                   cmsWpMaster.setIsRepeatOrder("no");
                   masters.add(cmsWpMaster);
                   boolean flag = service.AddToWpMaster(masters);


                    if(flag){
                   for(int i=0;i<NoOfCurrency;i++){
                   List<TblCmsWpDetail> tcwds = new ArrayList<TblCmsWpDetail>();
                  
                   List<Object> os = service.getWpId(Integer.parseInt(request.getParameter("pckLotId")));
                    if (os != null && !os.isEmpty()) {
                    //for (Object obj : os) {
                           TblCmsWpDetail tblCmsWpDetail = new TblCmsWpDetail();
                               tblCmsWpDetail.setTblCmsWpMaster(new TblCmsWpMaster(Integer.parseInt(os.get(3).toString())));
                               tblCmsWpDetail.setWpQualityCheckReq("no");
                               tblCmsWpDetail.setWpItemStatus("");
                               tblCmsWpDetail.setWpSrNo("");
                               tblCmsWpDetail.setWpDescription("");
                               tblCmsWpDetail.setWpUom("");
                               tblCmsWpDetail.setWpRowId(0);
                               tblCmsWpDetail.setWpQualityCheckReq("");
                               tblCmsWpDetail.setWpItemStatus("");
                               tblCmsWpDetail.setWpNoOfDays(0);
                               tblCmsWpDetail.setCreatedBy(0);
                               tblCmsWpDetail.setUserTypeId(0);


                               tblCmsWpDetail.setWpNoOfDays(0);
                               tblCmsWpDetail.setIsRepeatOrder("no");
                               tblCmsWpDetail.setItemInvGenQty(new BigDecimal(0));
                               tblCmsWpDetail.setItemInvStatus("advance");
                               tblCmsWpDetail.setAmendmentFlag("original");
                            
                             
                               tblCmsWpDetail.setCurrencyName(request.getParameter("currencyName"+(i+1)));
                                tcwds.add(tblCmsWpDetail);
                       //    }

                    }
                   boolean inDetail = service.addToWpDetails(tcwds);
                   System.out.println("inDetail"+inDetail);
                   }
                    
                  }
                   
                    }
                    }
                //Code by Proshanto Kumar Saha
                List<Object[]> obj = issueNOASrBean.getDetails(Integer.parseInt(tenderId), Integer.parseInt(pckLotId));
                String noaId=obj.get(0)[1].toString();
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                //response.sendRedirect("NOA.jsp?action=success&tenderId=" + tenderId);
                response.sendRedirect("ViewNOAWG.jsp?NOAID=" + noaId + "&tenderid=" + tenderId);
                }
                else
                {
                    response.sendRedirect("NOA.jsp?action=fail&tenderId=" + tenderId);
                    auditAction="Error in "+auditAction;
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);         
                }
            }
            else
            {
            if (issueNOASrBean.addIssueNOA(issueNOADtBean, roundId, tenderId,false, session.getAttribute("userId").toString()))
            {
                //Code by Proshanto Kumar Saha
                List<Object[]> obj = issueNOASrBean.getDetails(Integer.parseInt(tenderId), Integer.parseInt(pckLotId));
                String noaId=obj.get(0)[1].toString();
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                //response.sendRedirect("NOADocUpload.jsp?noaIssueId="+issueNOASrBean.getNoaIssueId());
                //response.sendRedirect("NOA.jsp?action=success&tenderId=" + tenderId);
                response.sendRedirect("ViewNOAWG.jsp?NOAID=" + noaId+ "&tenderid=" + tenderId);
            }
            else
            {
                response.sendRedirect("NOA.jsp?action=fail&tenderId=" + tenderId);
                auditAction="Error in "+auditAction;
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);         
            }
            }
        }
        else
        {
        %>

        <script type="text/javascript">
            function regSpace(value){
                return /^\s+|\s+$/g.test(value);
            }
            function alphaNumric(value){                
                return /^\w+$/.test(value);
            }
            var bVal = true;
            $(function() {
                $('#txtContractNo').blur(function() {
                    $('.err1').remove();
                    if($('#txtContractNo') != null){
                        var flag = document.getElementById('txtContractNo').value;
                        if($('#txtContractNo').val() == ''){
                            $('#Msg').html('');
                        }else if(regSpace(flag)){
                            $('#Msg').html('');
                        }else if(flag.length > 100){
                            $('#Msg').html('');
                            /* for time beiing its commented for fix production issue.
                        }else if(!alphaNumric($('#txtContractNo').val())){
                            $('#Msg').html('');
                            $('#txtContractNo').parent().append("<div class = 'err1 reqF_1' >Please enter valid Contract No</div>")
                            return false;
                            */
                        }else{
                            $('#Msg').css("color","red");
                            $('#Msg').html("Checking for unique Contract No....");
                            $.post("<%=request.getContextPath()%>/CommonServlet", {contractNo:$.trim($('#txtContractNo').val()),funName:'verifyContractNo'},
                            function(j)
                            {
                                if(j.toString().indexOf("OK", 0)!=-1){
                                    $('#Msg').css("color","green");
                                    bVal = true;
                                }
                                else if(j.toString().indexOf("Contract No.", 0)!=-1){
                                    $('#Msg').css("color","red");
                                    bVal = false;
                                }
                                $('#Msg').html(j);
                            });
                        }
                    }else{
                        $('#Msg').html("");
                    }
                });
            });
        </script>
        <%
                    
                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    String userId = "";
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = session.getAttribute("userId").toString();
                        issueNOASrBean.setLogUserId(userId);
                        commonSearchService.setLogUserId(userId);
                        tenderCommonService.setLogUserId(userId);
                    }
        %>
        <script type="text/javascript">

            $(document).ready(function(){
                $("#frmIssueNOA").validate({

                    rules:{
                        contractNo:{requiredWithoutSpace:true,maxlength:100,spacevalidate:true}/*,
                        contractAmtString:{required:true,decimal:true},
                        perfSecAmtString:{required:true,decimal:true},
                        contractName:{required:true,maxlength:100}*/

                    },
                    messages:{
                        contractNo:{requiredWithoutSpace:"<div class='reqF_1'>Please enter Contract No.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 100 characters are allowed</div>",
                            spacevalidate:"<div class='reqF_1'>Only space Is not allowed</div>"
                        }/*,
                        contractName:{
                            required:"<div class='reqF_1'>Please enter Contract/Project Name</div>",
                            maxlength:"<div class='reqF_1'>Maximum 100 characters are allowed</div>"
                        },
                        contractAmtString:{
                            required:"<div class='reqF_1'>Please enter contract price in Figure (In Nu.)</div>",
                            decimal:"<div class='reqF_1'>Allows numbers and 2 digits after decimal.</div>"
                        },
                        perfSecAmtString:{
                            required:"<div class='reqF_1'>Please enter Performance Security Amount in Figure (In BTN)</div>",
                            decimal:"<div class='reqF_1'>Allows numbers and 2 digits after decimal.</div>"
                        }*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#frmIssueNOA').submit(function() {

                });
            });
            $(function() {
                $('#frmIssueNOA').submit(function() {
                    if($('#frmIssueNOA').valid()){
                        if(!bVal){
                           return false;
                        }else{
                            if($('#Submit')!=null){
                                $('#Submit').attr("disabled","true");
                                $('#hdnbutton').val("Submit");
                            }
                        }
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <div class="contentArea_1">
                <form id="frmIssueNOA" action="" method="POST">
                    <div class="pageHead_1">
                        Issue Letter of Acceptance
                        <span class="c-alignment-right">
                            <a href="NOA.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back To Dashboard</a>
                        </span>
                    </div>
                    <%
                                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "contractName",tenderId , pckLotId);
                                List<SPTenderCommonData> list = tenderCommonService.returndata("GetEvaluatedBidders",tenderId,pckLotId);
                                String useridB = "";
                                String roundId = "";
                    %>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                        <tr>
                            <td width="24%" class="ff">Contract No. : <span class="mandatory">*</span></td>
                            <td width="76%"><input name="contractNo" type="text" class="formTxtBox_1" id="txtContractNo" style="width:200px;" maxlength="101"/>
                                <div id="Msg" style="color: red; font-weight: bold"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="24%" class="ff">Advance Amount (in %) :
                            <td width="76%"><input name="amountCnt" type="text" class="formTxtBox_1" id="amountCnt" style="width:125px;" maxlength="12" onblur="return AdvAmtinPercentage()"/>
                                <span id="AdvAmtPercentageSpan" ></span>&nbsp;
                                <span id="AdvAmtinWordsSpan" class="ff"></span>
                                <span id="AdvAmtspan" class="reqF_1"></span>
                            </td>
                        </tr>                       
                        <tr>
                            <%
                                        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                        SimpleDateFormat simpl = new SimpleDateFormat("dd-MMM-yyyy");
                            %>
                            <td class="ff">Date of Issuance of Letter of Acceptance (LOA) :</td>
                            <td class="formStyle_1"><%=simpl.format(new java.util.Date())%>
                                <input type="hidden" value="<%=dateFormat.format(new java.util.Date())%>" name="contractDtString"/></td>
                        </tr>
                        <tr>
                            <td class="ff">Name of Bidder/Consultant :</td>
                            <td>
                                <% List<SPTenderCommonData> listTenderer = tenderCommonService.returndata("GetEvaluatedBiddersRoundWise", tenderId, pckLotId);
                                String rank = "";
                                for (SPTenderCommonData companyList : listTenderer) {
                                    useridB = companyList.getFieldName2();
                                %>
                                <input type="hidden" value="<%=companyList.getFieldName2()%>" name="hdUserId"/>
                                <input type="hidden" value="<%=companyList.getFieldName1()%>" name="companyName"/>
                                <%
                                BigDecimal salvageAmount=BigDecimal.ZERO;
                                         List<SPCommonSearchData> salvageAmtList=  commonSearchService.searchData("getSalvageTotalAmount",tenderId, pckLotId, useridB, null, null, null, null, null, null);
                                        if(salvageAmtList!=null &&! salvageAmtList.isEmpty()){
                                           if(salvageAmtList.get(0).getFieldName1()!=null){
                                          salvageAmount=new BigDecimal(salvageAmtList.get(0).getFieldName1()).setScale(3,0);
                                          }
                                   %>
                                      <input type="hidden" value="<%=salvageAmount%>" name="salvageAmtStr"/>
                               <%
                               
                                   }
                               %>

                                <%
                                    /*String reptType = issueNOASrBean.getReptType(Integer.parseInt(tenderId));
                                    if(reptType != null){
                                        if(reptType.charAt(0) == 'l' || reptType.charAt(0) == 'L'){
                                            rank = "L"+companyList.getFieldName5();
                                        }else if(reptType.charAt(0) == 'h' || reptType.charAt(0) == 'H'){
                                            rank = "H"+companyList.getFieldName5();
                                        }
                                    }*/
                                List<SPCommonSearchDataMore> envDataMores = commonSearchDataMoreService.geteGPData("GetTenderEnvCount", tenderId);
                                if(envDataMores!=null && (!envDataMores.isEmpty())){                                                                        
                                    if(envDataMores.get(0).getFieldName2().equals("1")){
                                        //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                                        rank = "T"+companyList.getFieldName5();
                                    }else if(envDataMores.get(0).getFieldName2().equals("2")){
                                        //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                                        rank = "L"+companyList.getFieldName5();
                                    }else if(envDataMores.get(0).getFieldName2().equals("3")){
                                        //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                                    }
                                }
                                roundId = companyList.getFieldName3();
                                %>
                                <input type="hidden" value="<%=rank %>" name="rank"/>
                                <input type="hidden" value="<%=companyList.getFieldName3()%>" name="roundId" />
                                <%=companyList.getFieldName1()%>
                                <% }%>
                            </td>
                        </tr>
                        <tr>
                            <td width="24%" class="ff">Contract/Project Name : </td>
                            <td width="76%"><label><% if(!packageLotList.isEmpty()){%><%=packageLotList.get(0).getFieldName1() %> <% } %></label>
                                <% if(!packageLotList.isEmpty()){%> <input name="contractName" type="hidden" value="<%=packageLotList.get(0).getFieldName1() %>" class="formTxtBox_1" id="txtprojectName" style="width:200px;" /><% } %>
                            </td>
                        </tr>
                        <%
                            //Added by Salahuddin

                            List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", tenderId, null);
                            boolean isIctTender = false;
                            if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                isIctTender = true;
                            }

                            List<SPCommonSearchDataMore> perSecAmt1 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "perSecAmt",tenderId , pckLotId, roundId);
                            int noOfCurrency = perSecAmt1.size();
                            //List<SPCommonSearchDataMore> perSecAmt2 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getCurrencyName",tenderId , ?);
                        %>

                        <%if(!isIctTender){%>
                        <tr>
                            <td class="ff">Contract price In Figure (In Nu.)</td>
                            <td width="76%">
                                <% if(!list.isEmpty()){ %>
                                <%=new BigDecimal(list.get(0).getFieldName4()).setScale(3, 0) %>
                                <input name="contractAmtString" type="hidden" class="formTxtBox_1" id="txtContPriceFig" style="width:200px;" value="<%=new BigDecimal(list.get(0).getFieldName4()).setScale(3, 0) %>" /></td>
                            <% } %>
                        </tr>
                        <tr>
                            <td class="ff">Contract price In Words (In Nu.)</td>
                            <td><label id="conPrice"></label>
                                <input id="contractAmtWords" name="contractAmtWords" type="hidden"/>
                            </td>
                        </tr>
                        <%}else{%>
                            <%if(noOfCurrency>=1){%>
                            <tr>
                                <td class="ff">Contract price In Figure (In <%List<SPCommonSearchDataMore> perSecAmt2 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getCurrencyName",tenderId , perSecAmt1.get(0).getFieldName3()); String currency=perSecAmt2.get(0).getFieldName1(); if(currency.equals("BTN")){currency="Nu."+" including VAT";}else if(currency.equals("Nu.")){currency="Nu."+" including VAT";} out.print(currency);%>)</td>
                              
                                <td width="76%">
                                    <% //Double value= Double.parseDouble(perSecAmt1.get(0).getFieldName2());
                                        //Integer iValue = Integer.valueOf(value.intValue());
                                        //out.println(iValue);
                                        out.println(perSecAmt1.get(0).getFieldName2());
                                    %>
                                    <input name="contractAmt1" type="hidden" class="formTxtBox_1" id="contractAmt1" style="width:200px;" value="<%=perSecAmt1.get(0).getFieldName2()%>" />
                                    <label id="contractPrice1"></label>
                                    <input id="contractAmtWords1" name="contractAmtWords1" type="hidden"/>
                                     <input id="currencyID1" name="currencyID1" type="hidden" value="<%out.println(perSecAmt1.get(0).getFieldName3());%>"/>
                                    <input id="currencyName1" name="currencyName1" type="hidden" value="<%=perSecAmt2.get(0).getFieldName1()%>"/>
                                </td>
                            </tr>
                            <%}%>

                            <script type="text/javascript">//For currency1        
                                document.getElementById('contractPrice1').innerHTML = "(" + WORD(document.getElementById('contractAmt1').value) + ")";
                                document.getElementById('contractAmtWords1').value =WORD(document.getElementById('contractAmt1').value);
                            </script>

                            <%if(noOfCurrency>=2){%>
                            <tr>
                                <td class="ff">Contract price In Figure (In <%List<SPCommonSearchDataMore> perSecAmt2 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getCurrencyName",tenderId , perSecAmt1.get(1).getFieldName3());  String currency=perSecAmt2.get(0).getFieldName1(); if(currency.equals("BTN")){currency="Nu."+" including VAT";}else if(currency.equals("Nu.")){currency="Nu."+" including VAT";} out.print(currency);%>)</td>
                                <td width="76%">
                                    <% //Double value= Double.parseDouble(perSecAmt1.get(1).getFieldName2());
                                        //Integer iValue = Integer.valueOf(value.intValue());
                                        //out.println(iValue);
                                        out.println(perSecAmt1.get(1).getFieldName2());
                                    %>
                                    <input name="contractAmt2" type="hidden" class="formTxtBox_1" id="contractAmt2" style="width:200px;" value="<%=perSecAmt1.get(1).getFieldName2()%>" />
                                    <label id="contractPrice2"></label>
                                    <input id="contractAmtWords2" name="contractAmtWords2" type="hidden"/>
                                    <input id="currencyName2" name="currencyName2" type="hidden" value="<%=perSecAmt2.get(0).getFieldName1()%>"/>
                                    <input id="currencyID2" name="currencyID2" type="hidden" value="<%out.println(perSecAmt1.get(1).getFieldName3());%>"/>
                                </td>
                            </tr>
                            <%}%>

                            <script type="text/javascript">//For currency2
                                document.getElementById('contractPrice2').innerHTML = "(" + WORD(document.getElementById('contractAmt2').value) + ")";
                                document.getElementById('contractAmtWords2').value =WORD(document.getElementById('contractAmt2').value);
                            </script>

                            <%if(noOfCurrency>=3){%>
                            <tr>
                                <td class="ff">Contract price In Figure (In <%List<SPCommonSearchDataMore> perSecAmt2 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getCurrencyName",tenderId , perSecAmt1.get(2).getFieldName3());String currency=perSecAmt2.get(0).getFieldName1(); if(currency.equals("BTN")){currency="Nu."+" including VAT";}else if(currency.equals("Nu.")){currency="Nu."+" including VAT";} out.print(currency);%>)</td>
                                <td width="76%">
                                    <% //Double value= Double.parseDouble(perSecAmt1.get(2).getFieldName2());
                                        //Integer iValue = Integer.valueOf(value.intValue());
                                        //out.println(iValue);
                                        out.println(perSecAmt1.get(2).getFieldName2());
                                    %>
                                    <input name="contractAmt3" type="hidden" class="formTxtBox_1" id="contractAmt3" style="width:200px;" value="<%=perSecAmt1.get(2).getFieldName2()%>" />
                                    <label id="contractPrice3"></label>
                                    <input id="contractAmtWords3" name="contractAmtWords3" type="hidden"/>
                                    <input id="currencyName3" name="currencyName3" type="hidden" value="<%=perSecAmt2.get(0).getFieldName1()%>"/>
                                    <input id="currencyID3" name="currencyID3" type="hidden" value="<%out.println(perSecAmt1.get(2).getFieldName3());%>"/>
                                </td>
                            </tr>
                            <%}%>

                            <script type="text/javascript">//For currency3
                                document.getElementById('contractPrice3').innerHTML = "(" + WORD(document.getElementById('contractAmt3').value) + ")";
                                document.getElementById('contractAmtWords3').value =WORD(document.getElementById('contractAmt3').value);
                            </script>

                            <%if(noOfCurrency>=4){%>
                            <tr>
                                <td class="ff">Contract price In Figure (In <%List<SPCommonSearchDataMore> perSecAmt2 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getCurrencyName",tenderId , perSecAmt1.get(3).getFieldName3());String currency=perSecAmt2.get(0).getFieldName1(); if(currency.equals("BTN")){currency="Nu."+" including VAT";}else if(currency.equals("Nu.")){currency="Nu."+" including VAT";} out.print(currency);%>)</td>
                                <td width="76%">
                                    <% //Double value= Double.parseDouble(perSecAmt1.get(3).getFieldName2());
                                        //Integer iValue = Integer.valueOf(value.intValue());
                                        //out.println(iValue);
                                        out.println(perSecAmt1.get(3).getFieldName2());
                                    %>
                                    <input name="contractAmt4" type="hidden" class="formTxtBox_1" id="contractAmt4" style="width:200px;" value="<%=perSecAmt1.get(3).getFieldName2()%>" />
                                    <label id="contractPrice4"></label>
                                    <input id="contractAmtWords4" name="contractAmtWords4" type="hidden"/>
                                    <input id="currencyName4" name="currencyName4" type="hidden" value="<%=perSecAmt2.get(0).getFieldName1()%>"/>
                                    <input id="currencyID4" name="currencyID4" type="hidden" value="<%out.println(perSecAmt1.get(3).getFieldName3());%>"/>
                                </td>
                            </tr>
                            <%}%>

                            <script type="text/javascript">//For currency4
                                document.getElementById('contractPrice4').innerHTML = "(" + WORD(document.getElementById('contractAmt4').value) + ")";
                                document.getElementById('contractAmtWords4').value =WORD(document.getElementById('contractAmt4').value);
                            </script>

                        <%}%>
                        <tr>
                            <td class="ff">No. of days from the date of issuance of <%=NOADA%></td>
                            <%
                                        List<SPCommonSearchData> issueCon = commonSearchService.searchData("ProcureNOA", tenderId, "", "", null, null, null, null, null, null);
                                        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                        String NOADate = "";
                                        Calendar calendar = java.util.Calendar.getInstance();
                                        List<SPCommonSearchData> holidayFun = null;
                                        if (issueCon.size() != 0) {
                                        holidayFun = commonSearchService.searchData("getBusinessDays", dateFormat.format(new java.util.Date()), issueCon.get(0).getFieldName1(), "", null, null, null, null, null, null);
                                        }
                                        if (holidayFun != null) {
                                        NOADate = holidayFun.get(0).getFieldName1();
                                        dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                                        //calendar.setTime(dateFormat.parse(NOADate));
                                        calendar.setTime(new Date());
                                        }
                                        if (issueCon.get(0).getFieldName2() != null) {
                                            calendar.add(Calendar.DATE, +Integer.parseInt(issueCon.get(0).getFieldName2()));
                                        } else {
                                            calendar.add(Calendar.DATE, +0);
                                        }
                            %>
                            <td><% if (issueCon.size() != 0) {%><%=issueCon.get(0).getFieldName1()%> <% }%>
                                <input type="hidden" <% if (issueCon.size() != 0) {%> value="<%=issueCon.get(0).getFieldName1()%>" <% }%> name="noaIssueDays"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff"><%=NOADA%> acceptance last date & time</td>
                            <td><%=simpl.format(DateUtils.formatStdString(NOADate))%>
                                <input type="hidden" value="<%=NOADate%>" name="noaAcceptDtString"/></td>
                        </tr>
                        <%
                        List<Object[]> obj = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                        if(!obj.isEmpty() && obj.get(0)[1].toString().equalsIgnoreCase("yes")){
                            List<SPCommonSearchDataMore> perSecAmt = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "perSecAmt",tenderId , pckLotId, roundId);
                        %>
                        
                        <%if(!isIctTender){%>
                        <tr>
                            <td class="ff">Performance security amount in Figure (In Nu.)</td>
                            <td width="76%"><%if(!perSecAmt.isEmpty()){ %><%=new BigDecimal(perSecAmt.get(0).getFieldName1()).setScale(3,0) %>
                                <input value="<%=new BigDecimal(perSecAmt.get(0).getFieldName1()).setScale(3,0) %>" name="perfSecAmtString" type="hidden" class="formTxtBox_1" id="txtSecurityAmtFig" style="width:200px;" />
                                <%}%></td>
                        </tr>
                        <tr>
                            <td class="ff">Performance security amount in Words (In Nu.)</td>
                            <td><label id="perSecAmt"></label>
                                <input type="hidden" name="perfSecAmtWords" id="perfSecAmtWords"/>
                            </td>
                        </tr>
                        <%}else{%>
                            <%if(noOfCurrency>=1){%>
                            <tr>
                            <td class="ff">Performance security amount in Figure (In <%List<SPCommonSearchDataMore> perSecAmt2 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getCurrencyName",tenderId , perSecAmt1.get(0).getFieldName3());out.print(perSecAmt2.get(0).getFieldName1());%>)</td>
                            <td width="76%">                                
                                <% //Double value= Double.parseDouble(perSecAmt1.get(0).getFieldName1());
                                     //Integer iValue = Integer.valueOf(value.intValue());
                                     //out.println(iValue);
                                     out.println(perSecAmt1.get(0).getFieldName1());
                                %>
                                    <input name="perSecAmt1" type="hidden" class="formTxtBox_1" id="perSecAmt1" style="width:200px;" value="<%=perSecAmt1.get(0).getFieldName1()%>" />
                                    <label id="perfSecurity1"></label>
                                    <input id="perSecAmtWords1" name="perSecAmtWords1" type="hidden"/>
                            </td>
                        </tr>
                        <%}%>

                        <script type="text/javascript">//For currency1
                                document.getElementById('perfSecurity1').innerHTML = "(" + WORD(document.getElementById('perSecAmt1').value) + ")";
                                document.getElementById('perSecAmtWords1').value =WORD(document.getElementById('perSecAmt1').value);
                            </script>

                        <%if(noOfCurrency>=2){%>
                            <tr>
                            <td class="ff">Performance security amount in Figure (In <%List<SPCommonSearchDataMore> perSecAmt2 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getCurrencyName",tenderId , perSecAmt1.get(1).getFieldName3());out.print(perSecAmt2.get(0).getFieldName1());%>)</td>
                            <td width="76%">
                                <% //Double value= Double.parseDouble(perSecAmt1.get(1).getFieldName1());
                                     //Integer iValue = Integer.valueOf(value.intValue());
                                     //out.println(iValue);
                                     out.println(perSecAmt1.get(1).getFieldName1());
                                %>
                                    <input name="perSecAmt2" type="hidden" class="formTxtBox_1" id="perSecAmt2" style="width:200px;" value="<%=perSecAmt1.get(1).getFieldName1()%>" />
                                    <label id="perfSecurity2"></label>
                                    <input id="perSecAmtWords2" name="perSecAmtWords2" type="hidden"/>
                            </td>
                        </tr>
                        <%}%>

                        <script type="text/javascript">//For currency2
                                document.getElementById('perfSecurity2').innerHTML = "(" + WORD(document.getElementById('perSecAmt2').value) + ")";
                                document.getElementById('perSecAmtWords2').value =WORD(document.getElementById('perSecAmt2').value);
                        </script>

                        <%if(noOfCurrency>=3){%>
                            <tr>
                            <td class="ff">Performance security amount in Figure (In <%List<SPCommonSearchDataMore> perSecAmt2 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getCurrencyName",tenderId , perSecAmt1.get(2).getFieldName3());out.print(perSecAmt2.get(0).getFieldName1());%>)</td>
                            <td width="76%">
                                <% //Double value= Double.parseDouble(perSecAmt1.get(2).getFieldName1());
                                     //Integer iValue = Integer.valueOf(value.intValue());
                                     //out.println(iValue);
                                     out.println(perSecAmt1.get(2).getFieldName1());
                                %>
                                    <input name="perSecAmt3" type="hidden" class="formTxtBox_1" id="perSecAmt3" style="width:200px;" value="<%=perSecAmt1.get(2).getFieldName1()%>" />
                                    <label id="perfSecurity3"></label>
                                    <input id="perSecAmtWords3" name="perSecAmtWords3" type="hidden"/>
                            </td>
                        </tr>
                        <%}%>

                        <script type="text/javascript">//For currency3
                                document.getElementById('perfSecurity3').innerHTML = "(" + WORD(document.getElementById('perSecAmt3').value) + ")";
                                document.getElementById('perSecAmtWords3').value =WORD(document.getElementById('perSecAmt3').value);
                        </script>

                        <%if(noOfCurrency>=4){%>
                            <tr>
                            <td class="ff">Performance security amount in Figure (In <%List<SPCommonSearchDataMore> perSecAmt2 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getCurrencyName",tenderId , perSecAmt1.get(3).getFieldName3());out.print(perSecAmt2.get(0).getFieldName1());%>)</td>
                            <td width="76%">
                                <% //Double value= Double.parseDouble(perSecAmt1.get(3).getFieldName1());
                                     //Integer iValue = Integer.valueOf(value.intValue());
                                     //out.println(iValue);
                                     out.println(perSecAmt1.get(3).getFieldName1());
                                %>
                                    <input name="perSecAmt4" type="hidden" class="formTxtBox_1" id="perSecAmt4" style="width:200px;" value="<%=perSecAmt1.get(3).getFieldName1()%>" />
                                    <label id="perfSecurity4"></label>
                                    <input id="perSecAmtWords4" name="perSecAmtWords4" type="hidden"/>
                            </td>
                        </tr>
                        <%}%>

                        <script type="text/javascript">//For currency4
                                document.getElementById('perfSecurity4').innerHTML = "(" + WORD(document.getElementById('perSecAmt4').value) + ")";
                                document.getElementById('perSecAmtWords4').value =WORD(document.getElementById('perSecAmt4').value);
                        </script>


                        <%}%>
                        <tr>
                            <td class="ff">No. of days for performance security submission</td>
                            <td><% if (issueCon.size() != 0) {%><%=issueCon.get(0).getFieldName2()%> <% }%>
                                <input type="hidden" <% if (issueCon.size() != 0) {%> value="<%=issueCon.get(0).getFieldName2()%>" <% }%> name="perSecSubDays"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Last date & time for Performance security submission</td>
                            <td><%=simpl.format(calendar.getTime())%>
                                <input type="hidden" value="<%=dateFormat.format(calendar.getTime())%>" name="perSecSubDtString"/>
                            </td>
                        </tr>
                        <% }


                                    /*if (issueCon != null && issueCon.get(0).getFieldName3() != null) {
                                    calendar.setTime(dateFormat.parse(NOADate));
                                    calendar.add(Calendar.DATE, +Integer.parseInt(issueCon.get(0).getFieldName3()));
                                    } else {
                                    calendar.add(Calendar.DATE, +0);
                                    }*/

                        %>
                        <tr>
                            <td class="ff">Signing of contract in no. of days from the date of issuance</td>
                            <td><% if (issueCon.size() != 0) {%><%=issueCon.get(0).getFieldName3()%> <% }%>
                                <input type="hidden" <% if (issueCon.size() != 0) {%> value="<%=issueCon.get(0).getFieldName3()%>" <% }%> name="contractSignDays"/>
                            </td>
                        </tr>
                        <%
                                    dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                    calendar.setTime(dateFormat.parse(dateFormat.format(new java.util.Date())));
                                    if (issueCon != null && issueCon.get(0).getFieldName3() != null) {
                                    //calendar.add(Calendar.DATE, +Integer.parseInt(holidayFun.get(0).getFieldName3()));
                                    calendar.add(Calendar.DATE, +Integer.parseInt(issueCon.get(0).getFieldName3()));
                                    } else {
                                    calendar.add(Calendar.DATE, +0);
                                    }
                        %>
                        <tr>
                            <td class="ff">Last Date & time of contract signing</td>
                            <td><%=simpl.format(calendar.getTime())%>
                                <input type="hidden" value="<%=dateFormat.format(calendar.getTime())%>" name="contractSignDtString"/>
                                <input type="hidden" value="<%=userId%>" name="createdBy"/>
                                <input type="hidden" value="<%=pckLotId %>" name="pkgLotId"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Upload <%=NOADA%> Reference Document if any</td>
                            <td class="t-align-left"><a onclick="javascript:window.open('NOADocUpload.jsp?tenderid=<%=tenderId %>&pckLotid=<%=pckLotId %>&userid=<%=useridB%>&roundId=<%=roundId%>', '', 'resizable=yes,scrollbars=1','');" href="javascript:void(0);">Upload / Remove</a></td>
                        </tr>
                        <!--                        <tr>
                                                    <td class="ff">Mode of Payment</td>
                                                    <td><%// if (issueCon.size() != 0) {%><label><%//issueCon.get(0).getFieldName4()%></label><% //}%>
                                                    </td>
                                                </tr>-->

                        <!--                        <tr>
                                                    <td class="ff">Clause Reference</td>
                                                    <td><textarea rows="4" cols="100" id="clauseRef" name="clauseRef"></textarea>
                                                    </td>
                                                </tr>-->
                    </table>
                    <%--<div id="dataTable">
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="8%" class="t-align-center">S. No.</th>
                            <th class="t-align-center" width="23%">File Name</th>
                            <th class="t-align-center" width="28%">File Description</th>
                            <th class="t-align-center" width="7%">File Size <br />
                                (in KB)</th>
                            <th class="t-align-center" width="18%">Action</th>
                        </tr>
                        <%

                                    int docCnt = 0;
                                    /*For data listing*/
                                    List<SPCommonSearchDataMore> sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("NoaDocInfo", tenderId, pckLotId, useridB, roundId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    for (SPCommonSearchDataMore sptcd : sPCommonSearchDataMores) {
                                        docCnt++;
                        %>
                        <tr>
                            <td class="t-align-center"><%=docCnt%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                            <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                            <td class="t-align-center">
                                <a href="<%=request.getContextPath()%>/ServletNOADoc?docName=<%=sptcd.getFieldName1()%>&tenderid=<%=tenderId %>&roundId=<%=roundId %>&userid=<%=useridB %>&pckLotid=<%=pckLotId %>&docSize=<%=sptcd.getFieldName3()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                &nbsp;
                                <a href="<%=request.getContextPath()%>/ServletNOADoc?docName=<%=sptcd.getFieldName1()%>&tenderid=<%=tenderId%>&roundId=<%=roundId %>&userid=<%=useridB %>&pckLotid=<%=pckLotId %>&docId=<%=sptcd.getFieldName4()%>&funName=remove" title="Remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                            </td>
                        </tr>

                        <%   if (sptcd != null) {
                                                 sptcd = null;
                                             }
                                         }%>
                        <% if (docCnt == 0) {%>
                        <tr>
                            <td colspan="5" class="t-align-center">No records found.</td>
                        </tr>
                        <%}%>
                    </table>
                </div>--%>
                     <jsp:include page="../resources/common/NOADocInclude.jsp" >
                            <jsp:param name="comtenderId" value="<%=tenderId%>" />
                            <jsp:param name="comlotId" value="<%=pckLotId%>" />
                            <jsp:param name="comuserId" value="<%=useridB%>" />
                            <jsp:param name="comroundId" value="<%=roundId%>" />
                        </jsp:include>
                                                    <%
                                                    if(!listTenderer.isEmpty() && listTenderer.size() != 0){
                                                    %>
                    <div class="t-align-center t_space">
                        <label class="formBtn_1">
                            <input name="Submit" id="Submit" type="submit" value="Submit" onclick="return validateAdvAmt();"/>
                            <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                        </label>
                    </div>
                    <% } %>
                    <input type="hidden" name="tenderType" id="tenderType" value="<%if(isIctTender){out.println("ICT");}else{out.println("NCT");}%>"/>
                    <input type="hidden" name="NoOfCurrency" id="NoOfCurrency" value="<%=noOfCurrency%>"/>
                </form>
               
            </div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script type="text/javascript">        
        //function contractPrice(obj){
        document.getElementById('conPrice').innerHTML =WORD(document.getElementById('txtContPriceFig').value);
        document.getElementById('contractAmtWords').value =WORD(document.getElementById('txtContPriceFig').value);
        //}
        //function performancePrice(obj){
        document.getElementById('perSecAmt').innerHTML =WORD(document.getElementById('txtSecurityAmtFig').value);
        document.getElementById('perfSecAmtWords').value =WORD(document.getElementById('txtSecurityAmtFig').value);
        //}
    </script>
    <%
                issueNOASrBean = null;
                issueNOADtBean = null;
                tenderSrBean = null;
    %>

    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        function AdvAmtinPercentage()
        {
            var flag = true;
            if(document.getElementById("amountCnt").value!="")
            {
                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("amountCnt").value))
                {
                    if(document.getElementById("amountCnt").value<=100)
                    {
                        document.getElementById("AdvAmtspan").innerHTML = "";
                        var num =parseFloat(((parseFloat(document.getElementById("amountCnt").value)*(parseFloat(document.getElementById("txtContPriceFig").value)))/(100)));                        
                        document.getElementById("AdvAmtPercentageSpan").innerHTML=num.toFixed(3);
                        //document.getElementById("AdvAmtinWordsSpan").innerHTML = "("+DoIt(num.toFixed(3))+")";
                        //Numeric to word currency conversion by Emtaz on 20/April/2016
                        document.getElementById("AdvAmtinWordsSpan").innerHTML = "("+CurrencyConverter(num.toFixed(3))+")";
                    }else
                    {
                        flag = false;
                        document.getElementById("AdvAmtspan").innerHTML = "Advance Amount can not be more than 100%";
                    }
                }else
                {
                    flag = false;
                    document.getElementById("AdvAmtspan").innerHTML = "Please Enter Numeric values";
                }

            }
            return flag;
        }
        function validateAdvAmt()
        {
            var flag = true;
            if(document.getElementById("amountCnt").value!="")
            {
                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("amountCnt").value))
                {
                    if(document.getElementById("amountCnt").value<=100)
                    {
                        document.getElementById("AdvAmtspan").innerHTML = "";
                    }
                    else
                    {
                        flag = false;
                        document.getElementById("AdvAmtspan").innerHTML = "Advance Amount can not be more than 100%";
                    }
                }
                else
                {
                    flag = false;
                    document.getElementById("AdvAmtspan").innerHTML = "Please Enter Numeric values";
                }
            }
            return flag;
        }

    </script>
    <%}%>
</html>
