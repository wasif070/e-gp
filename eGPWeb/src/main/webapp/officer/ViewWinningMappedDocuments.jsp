<%--
    Document   : WinningBidderHistory
    Created on : Apr 19, 2015, 1:55:24 PM
    Author     : Istiak (Dohatec)
--%>

<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Previous Winning Bidder History</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <%
                    String tenderId = request.getParameter("tenderId");
                    String pkgLotId = "0";
                    if(!"".equalsIgnoreCase(request.getParameter("pkgLotId")) && request.getParameter("pkgLotId") != null)
                    {
                        pkgLotId = request.getParameter("pkgLotId");
                    }
                    String userId = request.getParameter("uId");
                    String docId = request.getParameter("docId");

                    String flag = "gw"; // Goods - Works
                    if("s".equalsIgnoreCase(request.getParameter("flag")))
                    {
                        flag = "s"; //  Service
                    }

                    AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
                    String strUserTypeId = session.getAttribute("userTypeId").toString();
                    Object objUserId = session.getAttribute("userId");
                    if (objUserId != null) {
                        strUserTypeId = session.getAttribute("userTypeId").toString();
                    }
        %>
        <script type="text/javascript">

            function chkdisble(pageNo){

                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }
                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }

            function checkKey(e)
            {
                var keyValue = (window.event)? e.keyCode : e.which;
                if(keyValue == 13){
                    $('#pageNo').val('1')
                    $('#btnSearch').click();
                }
            }
            function checkKeyGoTo(e)
            {
                var keyValue = (window.event)? e.keyCode : e.which;
                if(keyValue == 13){
                    $(function() {
                        var pageNo=parseInt($('#dispPage').val(),10);
                        var totalPages=parseInt($('#totalPages').val(),10);
                        if(pageNo > 0)
                        {
                            if(pageNo <= totalPages) {
                                $('#pageNo').val(Number(pageNo));
                                $('#btnGoto').click();
                                $('#dispPage').val(Number(pageNo));
                            }
                        }
                    });
                }
            }

            function loadOffice() {
                var deptId=$('#txtdepartmentid').val();
                $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId, funName:'officeCombo'},  function(j){
                    $('#cmbOffice').children().remove().end()
                    $("select#cmbOffice").html(j);
                });
            }

            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        $('#dispPage').val(Number(pageNo)+1);
                        loadTable();
                    }
                });
            });

            $(function() {
                $('#btnLast').click(function() {
                    var deptId=document.getElementById('txtdepartmentid').value
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        $('#dispPage').val(totalPages);
                        loadTable();
                    }
                });
            });

            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        $('#dispPage').val(Number(pageNo) - 1);
                        loadTable();
                    }
                });
            });

            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        $('#dispPage').val("1");
                        loadTable();
                    }
                });
            });

            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));

                            loadTable();
                        }
                    }
                });
            });

            function loadTable()
            {
                $("#progressBar").show();
                $("#progressBar").css("visibility","visible");

                var departmentId = $('#txtdepartmentid').val();
                var procEntity = $('#cmbOffice').val();        
                var procNature = $('#procNature').val();       
                var procMethod = $('#cmbProcMethod').val();    
                var procType = $('#cmbType').val();            
                var oldTenderId = $('#oldTenderId').val();
                var size = $('#size').val();
                var pageNo = $('#dispPage').val();

                $.post("<%=request.getContextPath()%>/EvalCommonServlet", {funName: "PreviousAllMappedTenders", userId: <%=userId%>, pkgLotId: <%=pkgLotId%>, tenderId: <%=tenderId%>,docId: <%=docId%>, size: size, pageNo: pageNo, departmentId: departmentId, procEntity: procEntity, procNature: procNature, procMethod: procMethod, procType: procType, oldTenderId: oldTenderId, flag: "<%=flag%>"},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    $("#progressBar").hide();
                    sortTable();
                    if($('#noRecordFound').attr("value") == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();

                    var counter = $('#cntTenBrief').val();
                    for(var i=0;i<counter;i++)
                    {
                        try
                        {
                            var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                            var temp1 = $('#tenderBrief_'+i).html();
                            if(temp.length > 250){
                                temp = temp1.substr(0, 250);
                                $('#tenderBrief_'+i).html(temp+'...');
                            }
                        }
                        catch(e){}
                    }
                });
            }

            $(document).ready(function(){

                loadTable();

                $('#btnSearch').click(function(){
                    loadTable();
                });

                $('#btnReset').click(function(){
                    $('#pageNo').val("1");
                    $('#dispPage').val("1");
                    $('#txtdepartment').val("");
                    $('#txtdepartmentid').val("");
                    $('#cmbOffice').val("0");
                    $('#procNature').val("0");
                    $('#cmbProcMethod').val("0");
                    $('#cmbType').val("0");
                    $('#tenderId').val("");
                    $('#oldTenderId').val("");

                    loadTable();
                });
            });

        </script>
    </head>

    <body>

        <div class="dashboard_div">
            <!--Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Header End-->

            <div class="contentArea_1">
                <div class="pageHead_1">Previous Winning Bidder History
                    <span class="c-alignment-right">
                        <%
                            if(flag.equalsIgnoreCase("gw")){
                        %>
                                <a href="WinningBidderHistory.jsp?tenderId=<%=tenderId%>&pkgLotId=<%=pkgLotId%>&uId=<%=userId%>" class="action-button-goback">Go back</a>
                        <%
                            }else{
                        %>
                                <a href="WinningBidderHistory.jsp?tenderId=<%=tenderId%>&pkgLotId=<%=pkgLotId%>&uId=<%=userId%>&flag=<%=flag%>" class="action-button-goback">Go back</a>
                        <%  }%>
                    </span>
                </div>
                <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <div>&nbsp;</div>

                <div class="tabPanelArea_1">

                    <div class="formBg_1 t_space">
                        <form id="alltenderFrm" action="" method="post">
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td class="ff">Select Hierarchy Node :</td>
                                    <td colspan="3">
                                        <input type="hidden" name="viewType" id="viewType" value="Live"/>
                                        <%
                                            if (objUserId != null){
                                                if (Integer.parseInt(strUserTypeId) == 5) {
                                                    Object[] objData = mISService.getOrgAdminOrgName(Integer.parseInt(objUserId.toString()));
                                                    out.print(objData[1].toString());
                                                    out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                                    out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                                    out.print("<input type=\"hidden\" id=\"uTypeId\" name=\"uTypeId\" value=\"" + Integer.parseInt(strUserTypeId) + "\"/>");
                                                }else{%>
                                                    <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 300px;"
                                                           id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                                                    <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                                    <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                                        <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                    </a>

                                             <% }
                                            } else {%>

                                                <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 300px;"
                                                       id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                                                <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                                <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                                    <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                </a>
                                    </td> <%}%>

                                </tr>
                                <tr>
                                    <td class="ff">Procuring Entity :</td>
                                    <td colspan="3">
                                        <select name="office" class="formTxtBox_1" id="cmbOffice" style="width:305px;">
                                            <option value="0">-- Select Office --</option>
                                            <%
                                                List<Object[]> cmbLists = null;
                                                if(objUserId!=null){
                                                    if (Integer.parseInt(strUserTypeId) == 5 ) {
                                                        cmbLists = mISService.getOfficeList(objUserId.toString(), strUserTypeId);
                                                        for (Object[] cmbList : cmbLists) {
                                                            String selected = "";
                                                            if (request.getParameter("offId") != null) {
                                                                if (cmbList[0].toString().equals(request.getParameter("offId"))) {
                                                                    selected = "selected";
                                                                }
                                                            }
                                                            out.print("<option value='" + cmbList[0] + "' " + selected + ">" + cmbList[1] + "</option>");
                                                            selected = null;
                                                        }
                                                        if (cmbLists.isEmpty()) {
                                                            out.print("<option value='0'> No Office Found.</option>");
                                                        }
                                                    }
                                                }
                                            %>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="22%" class="ff">Procurement Category : </td>
                                    <td width="38%">
                                        <select name="procNature" id="procNature" class="formTxtBox_1" id="select2" style="width:208px;">
                                            <option value="0" selected="selected">-- Select Category --</option>
                                            <option value="1">Goods</option>
                                            <option value="2">Works</option>
                                            <option value="3">Service</option>
                                        </select>
                                    </td>
                                    <td width="15%" class="ff">Procurement Type :</td>
                                    <td width="25%">
                                        <select name="procType" class="formTxtBox_1" id="cmbType" style="width:208px;">
                                            <option value="0">-- Select Type --</option>
                                            <option value="NCT">NCB</option>
                                            <option value="ICT">ICB</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Procurement Method :</td>
                                    <td>
                                        <select name="procMethod" class="formTxtBox_1" id="cmbProcMethod" style="width:208px;">
                                            <option value="0" selected="selected">-- Select Procurement Method --</option>
                                            <%
                                                List<SelectItem> getProcMethods = advAppSearchSrBean.getProcMethodList();
                                                if(getProcMethods != null)
                                                {
                                                    for(SelectItem items : getProcMethods)
                                                    {
                                                        out.print("<option value='" + items.getObjectId() + "' >" + items.getObjectValue() + "</option>");
                                                    }
                                                }
                                            %>
                                        </select>
                                    </td>
                                    <td class="ff">Tender ID :</td>
                                    <td>
                                        <input name="oldTenderId" id="oldTenderId" onkeypress="checkKey(event);" type="text" class="formTxtBox_1" style="width:202px;" />
                                        <span class="reqF_1" id="msgTenderId"></span>
                                    </td>
                                </tr>
                                <tr>

                                    <td colspan="4" class="t-align-center">

                                        <label class="formBtn_1 t_space"><input type="button" name="search" id="btnSearch" value="Search"  /></label>&nbsp;&nbsp;
                                        <label class="formBtn_1 t_space"><input type="reset" name="btnReset" id="btnReset" value="Reset" /></label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="t-align-center" colspan="4">
                                        <div id="valAll" class="reqF_1"></div>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>

                    <br />

                    <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>">
                    <input type="hidden" name="pkgId" id="pkgId" value="<%=pkgLotId%>">
                    <input type="hidden" name="uId" id="uId" value="<%=userId%>">
                    <input type="hidden" name="docId" id="docId" value="<%=docId%>">




                    <div id="resultDiv" style="display: none">
                        <div class="tabPanelArea_1">

                            <div class="tableHead_1">Search Results</div>

                            <div id="progressBar" class="tableHead_1 t_space" align="center" style="visibility:hidden"  >
                                <img alt="Please Wait" src="<%=request.getContextPath()%>/resources/images/loading.gif"/>
                            </div>
                            <br/>
                            <table width="100%" cellspacing="0" class="tableList_1" id="resultTable" cols="@0,6">
                                <tr>
                                    <th width="5%" class="t-align-center">Sl. <br/>No.</th>
                                    <th width="15%" class="t-align-center">ID, <br />Reference No., <br />Tender Status</th>
                                    <th width="25%" class="t-align-center">Procurement Category, <br />Title</th>
                                    <th width="25%" class="t-align-center">Hierarchy Node, PA</th>
                                    <th width="15%" class="t-align-center"> Type, <br />Method</th>
                                    <th width="15%" class="t-align-center">Mapped<br/>History</th>
                                </tr>
                            </table>
                            <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                <tr>
                                    <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="text" name="size" onkeypress="checkKeyGoTo(event);" id="size" value="10" style="width:70px;" class="formTxtBox_1"/>
                                    </td>
                                    <td align="center"><input name="textfield3" type="text" onkeypress="checkKeyGoTo(event);" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="button" name="btnGoto" id="btnGoto" value="Go To Page" />
                                        </label></td>
                                    <td class="prevNext-container"><ul>
                                            <li>&laquo; <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                            <li>&#8249; <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                            <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                            <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                        </ul></td>
                                </tr>
                            </table>
                            <div align="center">
                                <input type="hidden" id="pageNo" value="1"/>
                            </div>
                        </div>
                    </div>

                    <div>&nbsp;</div>
                </div>
            </div>
            <!--Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Footer End-->
        </div>
    </body>

</html>

