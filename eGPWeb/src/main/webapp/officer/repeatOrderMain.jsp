<%--
    Document   : repeatOrderMain
    Created on : Jul 30, 2011, 11:22:36 AM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowEventConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.WorkFlowServiceImpl"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsRomaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsRoitems"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPayment"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsVariationOrder"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="appSrBean" class="com.cptu.egp.eps.web.servicebean.APPSrBean" />
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
            CommonService commonService1 = (CommonService) AppContext.getSpringBean("CommonService");
            WorkFlowServiceImpl workFlowServiceImpl = (WorkFlowServiceImpl) AppContext.getSpringBean("WorkFlowService");
            String procnature1 = commonService1.getProcNature(request.getParameter("tenderId")).toString();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Repeat Order</title>

        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>


    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">

            <div class="pageHead_1">
                Repeat Order
            </div>
            <%
                        String tenderId = request.getParameter("tenderId");
                           boolean isPerformanceSecurityPaid = false;
                        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", request.getParameter("tenderId"), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);%>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>
            <% pageContext.setAttribute("lotId", packageLotList.get(0).getFieldName5());%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%@include  file="../resources/common/ContractInfoBar.jsp"%>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "14");
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");
                        String userId = "";
                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                            userId = session.getAttribute("userId").toString();
                            appSrBean.setLogUserId(userId);
                        }
            %>
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "7");

                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1">
                    <% if (request.getParameter("msg") != null) {
                                    if ("edit".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.dates.edit")%> </div>
                    <%}
                         if ("sent".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.works.sentToTen")%></div>
                    <%}

                         if ("noedit".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.var.success")%></div>
                    <%}
                         if ("changed".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg errorMsg'><%=bdl.getString("CMS.var.Nochange")%></div>
                    <%}
                                }
                    %>

                        <%
                                    WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                                    boolean publish = false;
                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                                    int i = 0;
                                    int lotId = 0;
                                    int romId = 0;
                                    List<Object> romIds = service.checkForSecondWPRO(Integer.parseInt(packageLotList.get(0).getFieldName5()));
                                    if (!romIds.isEmpty()) {
                                        romId = Integer.parseInt(romIds.get(0).toString());
                                    }
                                    String rId="";
                                    boolean isPerfSecurityAvail = false;
                                    List<Object[]> os = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                                    if (os != null && !os.isEmpty() && "yes".equalsIgnoreCase(os.get(0)[1].toString())) {
                                        isPerfSecurityAvail = true;
                                    }
                                    boolean isPE=false;
                                    TenderCommonService tcs1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                     List<SPTenderCommonData> checkPE = tcs1.returndata("getPEOfficerUserIdfromTenderId", tenderId, session.getAttribute("userId").toString());
                                            if (!checkPE.isEmpty() && session.getAttribute("userId").toString().equals(checkPE.get(0).getFieldName1())) {
                                                isPE = true;
                                       }
                                    boolean checkForFinalise = false;
                                    boolean checkForFinaliseifNoarejects = false;
                                     boolean isROplacedOrNot = false;
                                    CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                    for (SPCommonSearchDataMore lotList : packageLotList) {

                                        List<Object> wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                        lotId = Integer.parseInt(lotList.getFieldName5());
                                        String status = ros.getStatusOfNOAForRO(lotId);
                                         boolean alloROwhenROComplete = service.doRo(lotId);
                                       
                        %>
                        <form action="<%=request.getContextPath()%>/ConsolidateServlet" method="post">
                            <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>" />
                            <input type="hidden" name="lotId" id="lotId" value="<%=lotList.getFieldName5()%>" />
                            <input type="hidden" name="cntrctvalue" id="cntrctvalue" value="<%=c_obj[2].toString()%>" />
                            <input type="hidden" name="action"  value="finaliseRO" />

                             <%
                                    if (request.getParameter("msg") != null) {
                                        if (request.getParameter("msg").equals("finaliseRO")) {
                        %>

                        <div class='responseMsg successMsg'>Repeat Order finalized successfully </div>

                        <%}
                                    }%>

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">

                                <tr>
                                    <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td><%=bdl.getString("CMS.lotdes")%></td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>
                            </table>
                           
     <% int showMesg = 0;
     for (Object obj : wpid) {
                                    isROplacedOrNot = service.isROPlacedOrNot(Integer.parseInt(obj.toString()));
                                     checkForFinalise = service.isROFinalizeOrNot(Integer.parseInt(lotList.getFieldName5()), "finalise");

                                if (!isROplacedOrNot && !checkForFinalise) {
                                showMesg++;
                               %>
                                                    <%}}%>
                                                    <%if(showMesg>0){%>
                                                      <br />
                                <div  class='responseMsg noticeMsg t-align-left'>Please click on 'Repeat Order' link to place Repeat Order</div>
                                <br />
                                <%}%>
   
                                <%
                                                                        int count = 1;
                                                                        int countt = 0;
                                                                        if (!wpid.isEmpty() && wpid != null) {%>
                                <tr><td colspan="2">
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="14%" class="t-align-center">
                                                    <%=bdl.getString("CMS.Goods.Forms")%>

                                                </th>
                                                <th width="6%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>

                                            </tr>
                                            <%
                                                
                                                short activityid = 12;
                                                String donor = "";
                                                String noOfReviewers = "";
                                                List<TblCmsRomaster> roMasterData = null;
                                                List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = null;
                                                if(romId!=0){
                                                    roMasterData= ros.getRomasterData(romId);
                                                    tblWorkFlowLevelConfig= workFlowSrBean.editWorkFlowLevel(roMasterData.get(0).getRomId(), roMasterData.get(0).getRomId(), activityid);
                                                    donor = workFlowSrBean.isDonorReq("11",romId);
                                                    String[] norevs = donor.split("_");
                                                    noOfReviewers = norevs[1];
                                                }                                                                                                
                                                checkForFinalise = service.isROFinalizeOrNot(Integer.parseInt(lotList.getFieldName5()), "finalise");


                                                for (Object obj : wpid) {
                                                    String toDisplay = service.getConsolidate(obj.toString());
                                                    isROplacedOrNot = service.isROPlacedOrNot(Integer.parseInt(obj.toString()));

                                            %>


                                            <tr>
                                                <td style="text-align: center;"><%=count%></td>
                                                <td>
                                                    <%=toDisplay%>

                                                </td>
                                                <td>

                                                    <%if("decline".equalsIgnoreCase(status) ){%>
                                                    <a href="#" onclick="view(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View</a>
                                                    <%if (!isROplacedOrNot && !checkForFinalise) {
                                                        if(noOfReviewers==null || "null".equalsIgnoreCase(noOfReviewers) || "".equalsIgnoreCase(noOfReviewers)) {
                                                    %>
                                                    &nbsp;|&nbsp;
                                                    <a href="#" onclick="repeatorder(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Repeat Order</a>
                                                    <%}} else {
                                                        countt++;
                                                        if (!checkForFinalise) {
                                                            if(noOfReviewers==null || "null".equalsIgnoreCase(noOfReviewers) || "".equalsIgnoreCase(noOfReviewers)) {
                                                    %>
                                                    &nbsp;|&nbsp;
                                                    <a href="#" onclick="editRO(<%=romId%>,<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Edit Repeat Order</a>
                                                    <%}}}%>
                                                    <%if(romId!=0){if("rejected".equalsIgnoreCase(roMasterData.get(0).getRptOrderWfstatus())){%>
                                                    &nbsp;|&nbsp;<a href="#" onclick="repeatorder(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Repeat Order</a>
                                                    <%}}%>

                                                    <%}else if(!alloROwhenROComplete){%>
                                                    <a href="#" onclick="view(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View</a>
                                                     <%if (!isROplacedOrNot && !checkForFinalise) {
                                                        if(noOfReviewers==null || "null".equalsIgnoreCase(noOfReviewers) || "".equalsIgnoreCase(noOfReviewers)) {
                                                     %>
                                                    &nbsp;|&nbsp;
                                                    <a href="#" onclick="repeatorder(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Repeat Order</a>
                                                    <%}} else {
                                                        countt++;
                                                        if (!checkForFinalise) {
                                                            if(noOfReviewers==null || "null".equalsIgnoreCase(noOfReviewers) || "".equalsIgnoreCase(noOfReviewers)) {
                                                    %>
                                                    &nbsp;|&nbsp;
                                                    <a href="#" onclick="editRO(<%=romId%>,<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Edit Repeat Order</a>
                                                    <%}}}%>
                                                    <%if(romId!=0){if("rejected".equalsIgnoreCase(roMasterData.get(0).getRptOrderWfstatus())){%>
                                                    &nbsp;|&nbsp;<a href="#" onclick="repeatorder(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Repeat Order</a>
                                                    <%}}%>

                                                    <%}else{%>
                                                    <a href="#" onclick="view(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View</a>
                                                    <%if (!isROplacedOrNot && !checkForFinalise) {
                                                        if(noOfReviewers==null || "null".equalsIgnoreCase(noOfReviewers) || "".equalsIgnoreCase(noOfReviewers)) {
                                                    %>
                                                    &nbsp;|&nbsp;
                                                    <a href="#" onclick="repeatorder(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Repeat Order</a>
                                                    <%}} else {
                                                        countt++;
                                                        if (!checkForFinalise) {
                                                            if(noOfReviewers==null || "null".equalsIgnoreCase(noOfReviewers) || "".equalsIgnoreCase(noOfReviewers)) {
                                                    %>
                                                    &nbsp;|&nbsp;
                                                    <a href="#" onclick="editRO(<%=romId%>,<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Edit Repeat Order</a>
                                                    <%}}}%>
                                                    <%if(romId!=0){if("rejected".equalsIgnoreCase(roMasterData.get(0).getRptOrderWfstatus())){%>
                                                    &nbsp;|&nbsp;<a href="#" onclick="repeatorder(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Repeat Order</a>
                                                    <%}}%>
                                                    <%}%>
                                            </tr>
                                            <%    count++;
                                            }%>                                            
                                            <tr>
                                                <td class="t-align-left ff">WorkFlow Configuration</td>
                                                <td class="t-align-left" colspan="2">
                                                    <%if (countt > 0) {
                                                   // if (checkForFinalise) {
                                                                                                                                                                    
                                                        List<Object> getFileOnSt = new ArrayList<Object>();                                                        
                                                        int initiator = 0;
                                                        int approver = 0;
                                                        boolean iswfLevelExist = false;
                                                        if (tblWorkFlowLevelConfig.size() > 0) {
                                                        Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                                        iswfLevelExist = true;
                                                        while (twflc.hasNext()) {
                                                             TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                                             TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                                             if (workFlowlel.getWfRoleId() == 1) {
                                                             initiator = lmaster.getUserId();
                                                             }
                                                             if (workFlowlel.getWfRoleId() == 2) {
                                                                 approver = lmaster.getUserId();
                                                             }
                                                             }}
                                                        getFileOnSt = appSrBean.getFileOnHandStatus(roMasterData.get(0).getRomId(),roMasterData.get(0).getRomId(),Integer.parseInt(userId),11);
                                                        if ("pending".equalsIgnoreCase(roMasterData.get(0).getRptOrderWfstatus())) {                                                          
                                                            
                                                            if(!"0".equalsIgnoreCase(noOfReviewers))
                                                            {
                                                                    //out.print("&nbsp;|&nbsp;<a href ='EditCT.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                                    //       + "&tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8] + "&lotId=" + lotList.getFieldName5()
                                                                    //       + "&contractSignId=" + contractSignId + "'>Edit Contract Termination</a>");
                                                            }                                                                          
                                                             
                                                            if("0".equalsIgnoreCase(noOfReviewers))
                                                            {
                                                               if(initiator!=approver)
                                                               {
                                                                   if(tblWorkFlowLevelConfig.size()>0)
                                                                   {
                                                                        if(!"Yes".equalsIgnoreCase(getFileOnSt.get(0).toString())){
                                                                            //out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=9&objectid=" + tblCmsContractTermination.getContractTerminationId() + "&action=View&childid=" + tblCmsContractTermination.getContractTerminationId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                            //out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+tblCmsContractTermination.getContractTerminationId()+"&childid="+tblCmsContractTermination.getContractTerminationId()+"&eventid=9&userid="+userId+"&fraction=Termination&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View Workflow History</a>");
                                                                        }else{
                                                                           out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=11&objectid=" + tenderId + "&action=Edit&childid=" + roMasterData.get(0).getRomId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>"); 
                                                                           out.print("&nbsp|&nbsp<a href='FileProcessing.jsp?activityid="+activityid+"&objectid="+ tenderId +"&childid="+ roMasterData.get(0).getRomId() +"&eventid=11&fromaction=RepeatOrder&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Process file in Workflow</a>");
                                                                        }
                                                                   }else{
                                                                        out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=11&objectid=" + tenderId + "&action=Edit&childid=" + roMasterData.get(0).getRomId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>");
                                                                   }
                                                               }else{ 
                                                                  if(tblWorkFlowLevelConfig.size()>0)
                                                                  {
                                                                    if("pending".equalsIgnoreCase(roMasterData.get(0).getRptOrderStatus()))
                                                                    {
                                                                        publish = true;                                                                    
                                                                        //out.print("<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=11&objectid=" + roMasterData.get(0).getRomId() + "&action=View&childid=" + roMasterData.get(0).getRomId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                    }
                                                                    else
                                                                    {
                                                                        if(initiator!=approver)
                                                                        {
                                                                            //out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+roMasterData.get(0).getRomId()+"&childid="+roMasterData.get(0).getRomId()+"&eventid=11&userid="+userId+"&fraction=RepeatOrder&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View Workflow History</a>");
                                                                        }
                                                                        //out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=11&objectid=" + roMasterData.get(0).getRomId() + "&action=View&childid=" + roMasterData.get(0).getRomId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                                    }
                                                                  }else{
                                                                      out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=11&objectid=" + tenderId + "&action=Edit&childid=" + roMasterData.get(0).getRomId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>");
                                                                  }
                                                               }    
                                                            }
                                                            else
                                                            {
                                                                if(tblWorkFlowLevelConfig.size()>0)
                                                                {
                                                                    if(!"approved".equalsIgnoreCase(roMasterData.get(0).getRptOrderStatus()))
                                                                    {
                                                                        out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=11&objectid=" + tenderId + "&action=Edit&childid=" + roMasterData.get(0).getRomId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>");
                                                                    }
                                                                    //listworkflowpack = appSrBean.getWorkflowHistorylink(Integer.parseInt(tenderId));                                                                    
                                                                    //if(!listworkflowpack.isEmpty()){
                                                                    if(!"Yes".equalsIgnoreCase(getFileOnSt.get(0).toString())){
                                                                        //out.print("&nbsp|&nbsp<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+roMasterData.get(0).getRomId()+"&childid="+roMasterData.get(0).getRomId()+"&eventid=11&userid="+userId+"&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"&fraction=RepeatOrder'>View Workflow History</a>");
                                                                    }else{
                                                                        out.print("&nbsp|&nbsp<a href='FileProcessing.jsp?activityid="+activityid+"&objectid="+ tenderId +"&childid="+ roMasterData.get(0).getRomId() +"&eventid=11&fromaction=RepeatOrder&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Process file in Workflow</a>");
                                                                    }
                                                                }else{
                                                                    String  flagForCheck  = service.contractValueCheck(lotId, c_obj[2].toString());
                                                                    if("done".equalsIgnoreCase(flagForCheck)){
                                                                        if(noOfReviewers==null || noOfReviewers=="" || "".equalsIgnoreCase(noOfReviewers) || "null".equalsIgnoreCase(noOfReviewers)){
                                                                            if(workFlowServiceImpl.getWorkFlowRuleEngineData(11)){
                                                                                out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=11&objectid=" + tenderId + "&action=Create&childid=" + roMasterData.get(0).getRomId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Create Workflow</a>");
                                                                            }else{%>&nbsp;|&nbsp;<a href="#" onclick="jAlert('Workflow Configuration is pending in BusinessRule','Repeat Order', function(RetVal) {});">Create Workflow</a><%}
                                                                        }else{
                                                                            out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=11&objectid=" + tenderId + "&action=Edit&childid=" + roMasterData.get(0).getRomId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>Edit Workflow</a>");     
                                                                        }
                                                                 }else if("50percentcheck".equalsIgnoreCase(flagForCheck)){%>
                                                                     &nbsp;|&nbsp;<a href="#" onclick="jAlert('Current repeat order contract value can not be greater than 50% of original contract value ','Repeat Order', function(RetVal) {
                                                    });">Create Workflow</a>
                                                                        <%}else{%>
                                                                         &nbsp;|&nbsp;<a href="#" onclick="jAlert('Current repeat Order contract value can not be zero','Repeat Order', function(RetVal) {
                                                    });">Create Workflow</a>
                                                                <%}}
                                                           }
                                                        } else if ("approved".equalsIgnoreCase(roMasterData.get(0).getRptOrderWfstatus())) {                                                            
                                                            if("pending".equalsIgnoreCase(roMasterData.get(0).getRptOrderStatus())) {
                                                                publish = true;
                                                                //out.print("<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+roMasterData.get(0).getRomId()+"&childid="+roMasterData.get(0).getRomId()+"&eventid=11&userid="+userId+"&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"&fraction=RepeatOrder'>View Workflow History</a>");
                                                                //out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=11&objectid=" + roMasterData.get(0).getRomId() + "&action=View&childid=" + roMasterData.get(0).getRomId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                            }
                                                            else{
                                                                //out.print("<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+roMasterData.get(0).getRomId()+"&childid="+roMasterData.get(0).getRomId()+"&eventid=11&userid="+userId+"&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"&fraction=RepeatOrder'>View Workflow History</a>");
                                                                //out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=11&objectid=" + roMasterData.get(0).getRomId() + "&action=View&childid=" + roMasterData.get(0).getRomId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                            }
                                                        }else if ("Rejected".equalsIgnoreCase(roMasterData.get(0).getRptOrderWfstatus())) {
                                                            //out.print("<a href='workFlowHistory.jsp?activityid="+activityid+"&objectid="+roMasterData.get(0).getRomId()+"&childid="+roMasterData.get(0).getRomId()+"&eventid=11&userid="+userId+"&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"&fraction=RepeatOrder'>View Workflow History</a>");
                                                            //out.print("&nbsp|&nbsp<a href='CreateWorkflow.jsp?activityid="+activityid+"&eventid=11&objectid=" + roMasterData.get(0).getRomId() + "&action=View&childid=" + roMasterData.get(0).getRomId() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotList.getFieldName5()+"'>View</a>");
                                                        //}
                                                    }
                                                }%>
                                                </td>
                                            </tr>
                                            <%if(publish){%>
                                            <tr><td colspan="3" class="t-align-center"><label class="formBtn_1">
                                                        <input type="submit" id="finishconsolidation" value="Finalise repeat order" />
                                                    </label>
                                                </td>
                                            </tr>
                                            <%}%>
                                        </table>
                                    </td></tr>
                                    <%
                                                                            }
                                    %>

                                <%
                                            }%>
                            </table>
                            <%
                           // if (checkForFinalise) {
                            List<Object[]> mainForRO = ros.getMainLoopForROPESide(lotId);
                                        if(!mainForRO.isEmpty()){
                                             int icount = 1;
                                                for(Object[] objs : mainForRO){
                                                     List<Object> roundId = ros.isPerfPaidOrNot((Integer)objs[0]);
                                                        List<Object> wpid = ros.getWpIdForRO(Integer.parseInt(objs[1].toString()));
                                                        checkForFinaliseifNoarejects = service.isROFinalizeOrNotInCaseOfNOAReject(lotId, Integer.parseInt(objs[1].toString()));
    
    %>


    <%--<table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td class="t-align-left ff">Repeat Order</td>
                                    <td class="t-align-left"><a href="CreateROWf.jsp?romId=<%=romId%>&tenderId=<%=tenderId%>" onclick="">Create Workflow</a>
                                    </td>
                                </tr>
                            </table>--%>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td colspan="2">
                                         <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Repeat Order-<%=icount%></li>
                                </ul>
                                         <%
                                                                        int countt = 1;
                                                                        if (!wpid.isEmpty() && wpid != null) {%>

                                        <table width="100%" cellspacing="0" class="tableList_1 ">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="14%" class="t-align-center">
                                                    <%=bdl.getString("CMS.Goods.Forms")%>

                                                </th>
                                                <th width="6%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>

                                            </tr>
                                            <%

                                                        for (Object obj : wpid) {
                                                            String toDisplay = service.getConsolidate(obj.toString());

                                            %>


                                            <tr>
                                                <td style="text-align: center;"><%=countt%></td>
                                                <td>
                                                    <%=toDisplay%>

                                                </td>
                                                <td>
                                                    <a href="#" onclick="ROview(<%=obj.toString()%>,<%=tenderId%>,<%=lotId%>,<%=objs[1].toString()%>) ">View Repeat Order </a>
                                                </td>    
                                            </tr>                                            
                                            <%    countt++;
                                                                                                                    }    } %>
                                       <tr>
                                            <td style="text-align: center;">Workflow Details</td>
                                            <td colspan="2">
                                                <%
                                                List<TblWorkFlowLevelConfig>  tblwflevele = workFlowSrBean.editWorkFlowLevel(Integer.parseInt(objs[1].toString()), Integer.parseInt(objs[1].toString()), (short)12);
                                                if(tblwflevele.size()>0)
                                                {    
                                                    String strdonar = workFlowSrBean.isDonorReq("11",Integer.parseInt(objs[1].toString()));
                                                    String[] strnorevs = strdonar.split("_");
                                                    String strnoOfReviewers = strnorevs[1];
                                                    int initiatorr = 0;
                                                    int approverr = 0;
                                                    boolean iswfLevelExistt = false;
                                                    if (tblwflevele.size() > 0) {
                                                        Iterator twflcc = tblwflevele.iterator();
                                                        iswfLevelExistt = true;
                                                        while (twflcc.hasNext()) {
                                                             TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflcc.next();
                                                             TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                                             if (workFlowlel.getWfRoleId() == 1) {
                                                             initiatorr = lmaster.getUserId();
                                                             }
                                                             if (workFlowlel.getWfRoleId() == 2) {
                                                                 approverr = lmaster.getUserId();
                                                             }
                                                        }
                                                    }
                                                    if(initiatorr!=approverr)
                                                    {
                                                        out.print("<a href='workFlowHistory.jsp?activityid=12&objectid="+objs[1].toString()+"&childid="+objs[1].toString()+"&eventid=11&userid="+userId+"&tenderId="+tenderId+"&lotId="+lotId+"&fraction=RepeatOrder'>View Workflow History</a>&nbsp|&nbsp");
                                                        out.print("<a href='CreateWorkflow.jsp?activityid=12&eventid=11&objectid=" + objs[1].toString() + "&action=View&childid=" + objs[1].toString() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotId+"'>View</a>");
                                                    }else{    
                                                        if(!"0".equalsIgnoreCase(strnoOfReviewers))
                                                        {    
                                                            out.print("<a href='workFlowHistory.jsp?activityid=12&objectid="+objs[1].toString()+"&childid="+objs[1].toString()+"&eventid=11&userid="+userId+"&tenderId="+tenderId+"&lotId="+lotId+"&fraction=RepeatOrder'>View Workflow History</a>&nbsp|&nbsp");
                                                        }
                                                        out.print("<a href='CreateWorkflow.jsp?activityid=12&eventid=11&objectid=" + objs[1].toString() + "&action=View&childid=" + objs[1].toString() + "&isFileProcessed=No&tenderId="+tenderId+"&lotId="+lotId+"'>View</a>");
                                                    }    
                                                }     
                                                %>
                                            </td>
                                        </tr>                                                                                 
                                        </table>
                                    </td>
                                </tr>
                                <%if(checkForFinaliseifNoarejects){%>
                                <tr>
                                    <td width="30%">Performance Security for Repeat Order</td>
                                    <td class="t-align-left" width="80%">
                                     <% int rndId=0;
                                     boolean noaissue = false;
                                     
                                     if(roundId!=null && !roundId.isEmpty()){
                                     rndId=Integer.parseInt(roundId.get(0).toString());
                                     noaissue = ros.isNOAIssueOrNOT(rndId+"");
                                     }%>
                                        <%if(roundId==null || roundId.isEmpty()){%>
                                        <a href="AddPerfSecForRO.jsp?tenderId=<%=tenderId%>&pkgId=<%=lotId%>&romId=<%=romId%>&rId=<%=objs[0].toString()%>">Add</a>
                                        <%}else{%><%if(!noaissue){%>
                                        <a href="AddPerfSecForRO.jsp?tenderId=<%=tenderId%>&pkgId=<%=lotId%>&romId=<%=romId%>&isedit=y&rId=<%=rndId%>">Edit</a>
                                        &nbsp;|&nbsp;<%}%>
                                        <a href="AddPerfSecForRO.jsp?tenderId=<%=tenderId%>&pkgId=<%=lotId%>&viewAction=view&romId=<%=romId%>&isedit=y&rId=<%=rndId%>">View</a>
                                        <%}%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="t-align-left">Letter of Acceptance (LOA)</td>
                                    
                                    <td class="t-align-left"><!--<a href="<%=request.getContextPath()%>/ConsolidateServlet?action=dumpitems&romId=<%=romId%>&tenderId=<%=tenderId%>&lotId=<%=lotId%>" onclick="">Publish</a>--->
                                    <%if(!noaissue){%>
                                    <%if(roundId==null || roundId.isEmpty()){%>
                                    <a href="#" onclick="jAlert('Performance security is pending yet ','Repeat Order', function(RetVal) {
                        });">Issue LOA</a>
                                        <%}else{
                                        if(!noaissue){%>
                                       <a href="<%=request.getContextPath()%>/officer/IssueNOAForRO.jsp?tenderId=<%=tenderId%>&pckLotId=<%=lotId%>&romId=<%=romId%>">Issue LOA</a>
                                        <%}}%>


                                    <br /><div style="margin-top: 10px" class='responseMsg noticeMsg'>Click on 'Issue LOA' link to Issue LOA</div><%}else{%>
                                    Issued
                                    <%}%>
                                    </td>
                                </tr>
                           
                                   
                                    <%if(noaissue){%>
                                    <tr>
                                        <td colspan="2">
                                             <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">LOA</li>
                                </ul>
                             <table width="100%" cellspacing="0" class="tableList_1 ">
                                    <tr>
                                        <th width="5%" class="t-align-left">Contract  No.</th>
                                        <th width="15%" class="t-align-left">Contract Amount in Figure (in Nu.)</th>
                                        <th width="11%" class="t-align-left">Date of issue of Letter of Acceptance</th>
                                        <th width="11%" class="t-align-left">Deadline of Acceptance of Letter of Acceptance</th>
                                        <th width="18%" class="t-align-left">Letter of Acceptance Status</th>
                                        <th width="11%" class="t-align-left">Accept / Decline Date & Time</th>
                                    </tr><%
                                                                                            List<SPCommonSearchDataMore> sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("ContractListingTenderSideForRO", c_obj[14].toString(), request.getParameter("tenderId"), lotId+"", rndId+"" , null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                                                            //List<SPTenderCommonData> contractlist = tenderCommonService.returndata("ContractListing", userid, tenderid);
                                                                                            String temp="";
                                                                                            for (SPCommonSearchDataMore contractlists : sPCommonSearchDataMores) {
                                                                                                temp = contractlists.getFieldName6();
                            %>
                            <tr>
                                <td class="t-align-center"><%=contractlists.getFieldName1()%></td>
                                <td style="text-align: right;"><%=contractlists.getFieldName2()%></td>
                                <td class="t-align-center"><%=contractlists.getFieldName3()%></td>
                                <td class="t-align-center"><%=contractlists.getFieldName9()%></td>
                                <td class="t-align-center"><% if (contractlists.getFieldName6().equalsIgnoreCase("pending")) {%>Pending<% } else if (contractlists.getFieldName6().equalsIgnoreCase("approved")) {%>Accepted<%} else {%>Declined<%}%></td>
                                <td class="t-align-center"><%=contractlists.getFieldName4()%></td>
                                
                            </tr>
                            <%}%>
                                    </table>
                                        
                        <%}%>
                        <tr>
                            <td colspan="2">
                                 
                                <%for (Object[] obj : issueNOASrBean.getNOAListingForROForPF(Integer.parseInt(tenderId),rndId)) {
                                        List<TblTenderPayment> detailsPayment = issueNOASrBean.getPaymentDetailsForRO(Integer.parseInt(tenderId), (Integer) obj[0], (Integer) obj[8],(Integer) obj[1]);
                                        SimpleDateFormat simpl = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                        %>
                       <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Contract Sign</li>
                                </ul>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="15%" class="t-align-left">Name of Bidder </th>
                                <th width="10%" class="t-align-left">Contract No.</th>
                                <th width="15%" class="t-align-left">Date of Issue</th>
                                <th width="15%" class="t-align-left">Last Acceptance Date and Time</th>
                                <th width="15%" class="t-align-left">Letter of Acceptance Status</th>
                                <%
                                List<Object[]> objPerSec = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                                if(!objPerSec.isEmpty() && objPerSec.get(0)[1].toString().equalsIgnoreCase("yes")){
                                %>
                                <th width="15%" class="t-align-left">Performance Security </th>
                                <%}%>
                                <th width="10%" class="t-align-left">Action</th>
                            </tr>
                            <tr>
                                <td style="text-align: left;"><%=obj[9]%></td>
                                <td class="t-align-center" > <%=obj[2]%></td>
                                <td class="t-align-center" > <%=DateUtils.gridDateToStrWithoutSec((Date) obj[3])%></td>
                                <%if (obj[5] != null) {%>
                                <td class="t-align-center" > <%=DateUtils.gridDateToStrWithoutSec((Date) obj[5])%></td>
                                <%}
                                    if (obj[4].toString().equalsIgnoreCase("approved")) {
                                %>
                                <td class="t-align-center">Accepted</td>
                                <%} else if (obj[4].toString().equalsIgnoreCase("declined")) {%>
                                <td class="t-align-center">Declined</td>
                                <% }
                                if(!objPerSec.isEmpty() && objPerSec.get(0)[1].toString().equalsIgnoreCase("yes")){
                                if (detailsPayment.isEmpty()) {%>
                                <td class="t-align-center" >Pending</td>
                                <%} else {
                                            isPerformanceSecurityPaid = true;
                                            %>
                                <td class="t-align-center" >Received</td>

                                <% }
                                }else{
                                    isPerformanceSecurityPaid  = true;
                                }
                                    if (issueNOASrBean.isAvlTCS((Integer) obj[1])) {
                                %>
                                <td class="t-align-center"><a href="ManageContractAgreement.jsp?noaIssueId=<%=obj[1]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&ro=RO">View</a></td>
                                <%} else if(isPerformanceSecurityPaid) {%>
                                <td class="t-align-center"><a href="ContractAgreement.jsp?noaIssueId=<%=obj[1]%>&tenderId=<%=tenderId%>&isRO=y">Enter Contract Details</a></td>
                                <%}else{%>
                                <td class="t-align-center">-</td>
                                <%}%>
                            </tr>
                        </table>
                        <%isPerformanceSecurityPaid=false;}%>
                            </td></tr><%}%></table>

                                    <% icount++;} }//}%>

                        </form>

                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
    <script>
        function editRO(romId,wpId,tenderId,lotId){
      
            dynamicFromSubmit("EditRepeatOrder.jsp?romId="+romId+"&wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&pe=yes");
    
        }
        function view(wpId,tenderId,lotId){
            dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&RO=ro");
        }
        function ROview(wpId,tenderId,lotId,romId){
            dynamicFromSubmit("ROView.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&romId="+romId);
        }
        function repeatorder(wpId,tenderId,lotId){
            dynamicFromSubmit("PlaceRepeatOrder.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
        }
        function finish(tenderId,lotId){
            document.frmcons.method="post";
            document.frmcons.action="CosolidateServlet?lotId="+lotId+"&action=finaliseRO&tenderId="+tenderId;
            document.frmcons.submit();
        }
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>

</html>
