<%-- 
    Document   : TenderDocument
    Created on : Nov 14, 2010, 3:48:49 PM
    Author     : rajesh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Administration - Default Workflow Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript">

                $(document).ready(function() {

                    //Default Action
                    $(".tabPanelArea_1").hide(); //Hide all content
                    $("ul.tabPanel_1 li:first").addClass("sMenu").show(); //Activate first tab
                    $(".tabPanelArea_1:first").show(); //Show first tab content

                    document.getElementById('tab1').innerHTML='<table width="100%" cellspacing="0" class="tableList_1 t_space"><tr><td class="t-align-left ff" colspan="4" >Standard  Tender Document :</td></tr><tr><th width="10%" class="t-align-center">Lot.  No.</th><th class="t-align-center" width="62%">Lot Description</th><th class="t-align-center" width="16%">Document</th><th class="t-align-center" width="12%">Status</th></tr><tr><td class="t-align-center">1</td><td class="t-align-center">Lot  Description -1</td><td class="t-align-center"><a href="#">Prepare</a></td><td class="t-align-center">Pending</td></tr><tr><td class="t-align-center">2</td><td class="t-align-center">Lot  Description -2</td><td class="t-align-center"><a href="#">Prepare</a></td><td class="t-align-center">Pending</td></tr><tr><td class="t-align-center">3</td><td class="t-align-center">Lot  Description -3</td><td class="t-align-center"><a href="#">Prepare</a></td><td class="t-align-center">Pending</td></tr></table>';

                    //On Click Event
                    $("ul.tabPanel_1 li").click(function() {
                        $("ul.tabPanel_1 li").removeClass("sMenu"); //Remove any "active" class
                        $(this).addClass("sMenu"); //Add "active" class to selected tab
                        $(".tabPanelArea_1").hide(); //Hide all tab content
                        var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content

                        if(activeTab=='#tab1')
                            {

                                document.getElementById(activeTab.replace("#","")).innerHTML='<table width="100%" cellspacing="0" class="tableList_1 t_space"><tr><td class="t-align-left ff" colspan="4" >Standard  Tender Document :</td></tr><tr><th width="10%" class="t-align-center">Lot.  No.</th><th class="t-align-center" width="62%">Lot Description</th><th class="t-align-center" width="16%">Document</th><th class="t-align-center" width="12%">Status</th></tr><tr><td class="t-align-center">1</td><td class="t-align-center">Lot  Description -1</td><td class="t-align-center"><a href="#">Prepare</a></td><td class="t-align-center">Pending</td></tr><tr><td class="t-align-center">2</td><td class="t-align-center">Lot  Description -2</td><td class="t-align-center"><a href="#">Prepare</a></td><td class="t-align-center">Pending</td></tr><tr><td class="t-align-center">3</td><td class="t-align-center">Lot  Description -3</td><td class="t-align-center"><a href="#">Prepare</a></td><td class="t-align-center">Pending</td></tr></table>';
                            }
                        else if (activeTab=='#tab2')
                            {
                                document.getElementById(activeTab.replace("#","")).innerHTML='This is second Tab';
                            }
                        else if (activeTab=='#tab3')
                            {
                                document.getElementById(activeTab.replace("#","")).innerHTML='This is third Tab';
                            }
                        else if (activeTab=='#tab4')
                            {
                                document.getElementById(activeTab.replace("#","")).innerHTML='This is fourth Tab';
                            }
                        else if (activeTab=='#tab5')
                            {
                                document.getElementById(activeTab.replace("#","")).innerHTML='This is fourth Tab';
                            }
                        else if (activeTab=='#tab6')
                            {
                                document.getElementById(activeTab.replace("#","")).innerHTML='This is fourth Tab';
                            }
                        else if (activeTab=='#tab7')
                            {
                                document.getElementById(activeTab.replace("#","")).innerHTML='This is fourth Tab';
                            }
                        else if (activeTab=='#tab8')
                            {
                                document.getElementById(activeTab.replace("#","")).innerHTML='This is fourth Tab';
                            }
                        else if (activeTab=='#tab9')
                            {
                                document.getElementById(activeTab.replace("#","")).innerHTML='This is fourth Tab';
                            }
                        else if (activeTab=='#tab10')
                            {
                                document.getElementById(activeTab.replace("#","")).innerHTML='This is fourth Tab';
                            }



                        $(activeTab).fadeIn(); //Fade in the active content
                        return false;
                    });

                });
            </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <table width="100%" cellspacing="0">
                    <tr valign="top">
                        <td><div class="dash_menu_1">
                                <ul>
                                    <li><a href="#"><img src="../resources/images/Dashboard/msgBoxIcn.png" />Message Box</a></li>
                                    <li><a href="#"><img src="../resources/images/Dashboard/appIcn.png" />APP</a></li>
                                    <li class="mSel"><a href="#"><img src="../resources/images/Dashboard/tenderIcn.png" />Tender</a></li>
                                    <li><a href="#"><img src="../resources/images/Dashboard/committeeIcn.png" />Evaluation</a></li>
                                    <li><a href="#"><img src="../resources/images/Dashboard/reportIcn.png" />Report</a></li>
                                    <li><a href="#"><img src="../resources/images/Dashboard/myAccountIcn.png" />My Account</a></li>
                                    <li><a href="#"><img src="../resources/images/Dashboard/helpIcn.png" />Help</a></li>
                                </ul>
                            </div>
                            <table width="100%" cellspacing="6" class="loginInfoBar">
                                <tr>
                                    <td align="left"><b>Friday 27/08/2010 21:45</b></td>
                                    <td align="center"><b>Last Login :</b> Friday 27/08/2010 21:45</td>
                                    <td align="right"><img src="../resources/images/Dashboard/userIcn.png" class="linkIcon_1" /><b>Welcome,</b> User   &nbsp;|&nbsp; <img src="../resources/images/Dashboard/logoutIcn.png" class="linkIcon_1" alt="Logout" /><a href="#" title="Logout">Logout</a></td>
                                </tr>
                            </table></td>
                        <td width="141"><img src="../resources/images/Dashboard/e-GP.gif" width="141" height="64" alt="e-GP" /></td>
                    </tr>
                </table>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Tender Dashboard</div>
            <div class="tableHead_1 t_space">Tender Detail</div>
            <table width="100%" cellspacing="10" class="tableView_1">
                <tr>
                    <td width="100" class="ff">Tender Id :</td>
                    <td>11</td>
                    <td width="124" class="ff">Tender No : </td>
                    <td>Test - UVM-01</td>
                </tr>
            
                <tr>
                    <td class="ff">Due date &amp; time :</td>
                    <td>02/06/2010 17:00</td>
                    <td class="ff">Opening date &amp; time : </td>
                    <td>02/06/2010 17:00</td>
                </tr>
                <tr>
                    <td class="ff">Department :</td>
                    <td colspan="3">NPCIL Group</td>
                </tr>
                <tr>
                    <td class="ff">Brief :</td>
                    <td colspan="3">Chittagong</td>
                </tr>
                <tr>
                    <td class="ff">&nbsp;</td>
                    <td colspan="3">
                        <a href="#" class="action-button-notice" title="Tender/Proposal Notice">Tender Notice</a>
                        <a href="#" class="action-button-download" title="Download Documents">Download Documents</a>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <ul class="tabPanel_1">
                <li class="sMenu"><a href="#tab1">Notice </a></li>
                <li class=""><a href="#tab2">Document</a></li>
                <li class=""><a href="#tab3">Workflow</a></li>
                <li class=""><a href="#tab4">Pre – Tender Meeting</a></li>
                <li class=""><a href="#tab5">Amendment</a></li>
                <li class=""><a href="#tab6">Opening</a></li>
                <li class=""><a href="#tab7">Evaluation</a></li>
                <li class=""><a href="#tab8">Payment</a></li>
                <li class=""><a href="#tab9">Letter of Acceptance (LOA)</a></li>
                <li class=""><a href="#tab10">Contract Signing</a></li>
            </ul>
            <div style="display: block;" class="tabPanelArea_1" id="tab1">
            </div>
            <div style="display: none;" class="tabPanelArea_1" id="tab2">
            </div>
            <div style="display: none;" class="tabPanelArea_1" id="tab3">
            </div>
            <div style="display: none;" class="tabPanelArea_1" id="tab4">
            </div>
            <div style="display: none;" class="tabPanelArea_1" id="tab5">
            </div>
            <div style="display: none;" class="tabPanelArea_1" id="tab6">
            </div>
            <div style="display: none;" class="tabPanelArea_1" id="tab7">
            </div>
            <div style="display: none;" class="tabPanelArea_1" id="tab8">
            </div>
            <div style="display: none;" class="tabPanelArea_1" id="tab9">
            </div>
            <div style="display: none;" class="tabPanelArea_1" id="tab10">
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <table width="100%" cellspacing="0" class="footerCss">
                <tr>
                    <td align="left">e-GP &copy; All Rights Reserved
                        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
                    <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
                </tr>
            </table>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
