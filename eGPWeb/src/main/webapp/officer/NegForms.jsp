<%--
    Document   : NegForms
    Created on : Dec 30, 2010, 4:36:51 PM
    Author     : Administrator
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.NegotiationSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<jsp:useBean id="negotiationFormsSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationFormsSrBean"></jsp:useBean>
<jsp:useBean id="negotiationProcessSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            int negId = 0;
                if(request.getParameter("negId")!=null){
                    negId = Integer.parseInt(request.getParameter("negId"));
                }
             String finalStatus = "";
                finalStatus = negotiationProcessSrBean.getFinalSubStatus(negId);
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%if(!"".equalsIgnoreCase(finalStatus)){%>
        <title>View Forms</title>
        <%}else{%>
        <title>View Revised Forms</title>
        <%}%>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
         <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <div class="dashboard_div">
            <%  String tenderId;
                tenderId = request.getParameter("tenderId");

                String userId = "", usrId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                if (request.getParameter("uId") != null && !"".equalsIgnoreCase(request.getParameter("uId"))) {
                    usrId = request.getParameter("uId");
                }

                

                int isBidModify = 0;

               
                String type="";
                String bidid="0";
            %>
           
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">
                    <%if(!"".equalsIgnoreCase(finalStatus)){%>
                        View Forms
                    <%}else{%>
                        View Revised Forms
                    <%}%>
                    <% if("".equalsIgnoreCase(finalStatus)){ %>
                    <span style="float: right; text-align: right;">
                        <a class="action-button-goback" href="NegotiationForms.jsp?tenderId=<%=tenderId%>&uId=<%=usrId%>&negId=<%=negId%>" title="Bid Dashboard">Go Back To Dashboard</a>
                    </span>
                    <% }else{ %>
                    <span style="float: right; text-align: right;">
                        <a class="action-button-goback" href="NegotiationProcess.jsp?tenderId=<%=tenderId%>" title="Bid Dashboard">Go Back To Dashboard</a>
                    </span>
                    <% } %>
                </div>
                
                <%
                    pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%
                      boolean isTenPackageWis1 = (Boolean) pageContext.getAttribute("isTenPackageWise");
                %>
                <div>&nbsp;</div>
                <%if(!"TEC".equalsIgnoreCase(request.getParameter("comType"))) { %>
                <jsp:include page="officerTabPanel.jsp" >
                     <jsp:param name="tab" value="7" />
                </jsp:include>
                <% } %>
                
                <div class="tabPanelArea_1">
                         <%-- Start: Common Evaluation Table --%>
                      <%@include file="/officer/EvalCommCommon.jsp" %>
                    <%-- End: Common Evaluation Table --%>
                    <div>&nbsp;</div>
               
                <%
                     pageContext.setAttribute("TSCtab", "7");
                %>
                <%@include  file="../resources/common/AfterLoginTSC.jsp"%>
                 <div class="tabPanelArea_1">
                    <%
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            
                            if (isTenPackageWis1) {
                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                    %>
                    
                    
                   <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td colspan="2" class="t-align-center ff">Tender Details</td>
                        </tr>

                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="22%" class="t-align-left ff">Form Name</th>
                            <th width="78%" class="t-align-left">Action</th>
                        </tr>
                        <%
                            boolean flag = false;
                            for (SPCommonSearchData formdetails : commonSearchService.searchData("GetNegTenderFormsByLotId", tenderId, usrId, "0", usrId, null, ""+negId, null, null, null)) {
                            flag = false;
                            flag = negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formdetails.getFieldName1()),negId);
                            if(flag==true)
                            {
                               int negBidformId=0;
                                for (SPCommonSearchData negoformdetails : commonSearchService.searchData("GetNegBidderFormsData", tenderId, usrId,formdetails.getFieldName1() , null, null,null, null, null,null)) 
                                {
                                      if(negoformdetails.getFieldName4() != null)
                                    {
                                        type ="negbiddata";
                                        negBidformId=Integer.parseInt(negoformdetails.getFieldName4());
                                    }
                                    else
                                    {
                                        type ="biddata";
                                    }  
                                      bidid=negoformdetails.getFieldName3();
                                 }
                        %>
                        <tr>
                            <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                            <td class="t-align-left">
                                <% if(formdetails.getFieldName6()==null && "".equals(finalStatus))
                                { %> 
                                    <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=bidid%>&uId=<%=usrId%>&negBidformId=<%=negBidformId%>&lotId=0&action=View&type=<%= type%>&negId=<%=negId%>">View</a>
                               <% 
                                }
                                else
                               {
                                 if(formdetails.getFieldName6()==null)
                                    {
                               %>
                                        <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=bidid%>&uId=<%=usrId%>&negBidformId=<%=negBidformId%>&lotId=0&action=View&type=<%= type%>&negId=<%=negId%>">View</a>
                                    <% 
                                    }
                                else
                                    {
                                    %>
                                         <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=bidid%>&uId=<%=usrId%>&negBidformId=<%=negBidformId%>&lotId=0&action=View&type=<%= type%>&negId=<%=negId%>">View</a>
                                    <% 
                                    } 
                               } %>
                            </td>
                        </tr>
                        <% } }%>
                    </table>
                        <% }
                             } else {
                                 for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-center ff">Tender Details</td>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%  }
                            for (SPCommonSearchData lotList : commonSearchService.searchData("gettenderlotbytenderid", tenderId, "", "", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                            <tr>
                                <td width="22%" class="t-align-left ff">Lot No. :</td>
                                <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Lot Description :</td>
                                <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="22%" class="t-align-left ff">Form Name</th>
                                <th width="23%" class="t-align-left">Action</th>
                            </tr>
                            <%
                                boolean flag = false;
                                 for (SPCommonSearchData formdetails : commonSearchService.searchData("GetNegTenderFormsByLotId", tenderId, usrId, lotList.getFieldName3(), usrId, null, null, null, null, null)) {
                                     flag = negotiationFormsSrBean.isSelectedForm(Integer.parseInt(formdetails.getFieldName1()),negId);
                                     if(flag==true){
                            %>
                            <tr>
                                <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                                <td class="t-align-left ff"><%= formdetails.getFieldName6() %>
                                    <%
                                     
                                    for (SPCommonSearchData negoformdetails : commonSearchService.searchData("GetNegBidderFormsData", tenderId, usrId,formdetails.getFieldName1() , null, null,null, null, null,null)) 
                                        {
                                              if(negoformdetails.getFieldName4() != null)
                                            {
                                                type ="negbiddata";

                                            }
                                            else
                                            {
                                                type ="biddata";
                                            }  
                                              bidid=negoformdetails.getFieldName3();
                                        }
                                    
                                    if(formdetails.getFieldName6()==null){ %>
                                    <%--<a href="NegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=formdetails.getFieldName3()%>&lotId=<%=lotList.getFieldName3()%>&uId=<%=usrId%>&action=Edit&negId=<%=negId%>">Edit</a>--%>
                                    <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=bidid%>&uId=<%=usrId%>&lotId=0&action=View&type=<%= type%>&negId=<%=negId%>">View</a>
                               <% }else{
                                     //if("".equalsIgnoreCase(finalStatus)){
                                %>
                                    <%--<a href="NegModifyBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=formdetails.getFieldName3()%>&uId=<%=usrId%>&lotId=<%=lotList.getFieldName3()%>&action=Edit&negId=<%=negId%>">Edit</a>&nbsp;|&nbsp;--%>
                                     <% //} %>  
                                    <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=bidid%>&uId=<%=usrId%>&lotId=0&action=View&type=<%= type%>&negId=<%=negId%>">View</a>
                               <% } } %>
                                </td>
                            </tr>
                            <% }%>
                        </table>
                        <%     }
                            }
                        %>
                        <%
                                int docCnt = 0;
                                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                List<SPTenderCommonData> sptcdList = tenderCS.returndata("NegotiationDocInfo", String.valueOf(negId), "revisedoc");
                                List<SPTenderCommonData> sptcdListOfficer = new ArrayList<SPTenderCommonData>();
                                List<SPTenderCommonData> sptcdListBidder = new ArrayList<SPTenderCommonData>();
                                if(sptcdList!=null && !sptcdList.isEmpty()){
                                    for(int i=0;i<sptcdList.size();i++){
                                        if(sptcdList.get(i).getFieldName5().trim().equalsIgnoreCase(session.getAttribute("userId").toString())){
                                            sptcdListOfficer.add(sptcdList.get(i));
                                        }else{
                                            sptcdListBidder.add(sptcdList.get(i));
                                        }
                                    }
                                    if(sptcdListOfficer!=null && !sptcdListOfficer.isEmpty()){
                                
                            %>
                          <table width="100%" cellpadding="0" cellspacing="0" class="tableList_1">   
                            <tr>
                                <td class="t-align-left ff" width="22%">Reference Documents Uploaded by Evaluation Committee :</td>
                                <td>
                                     <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="32%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%

                                

                                for (SPTenderCommonData sptcd : sptcdListOfficer) {
                                    docCnt++;
                    %>
                    <tr>
                        <td class="t-align-left"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=download&docType=revisedoc" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                             <% if("".equalsIgnoreCase(finalStatus) && sptcd.getFieldName5().trim().equalsIgnoreCase(session.getAttribute("userId").toString())){ %>
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=remove&docType=revisedoc"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                            <%}%>
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                                             sptcd = null;
                                         }
                                     }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
                                </td>
                            </tr>
                            <%}
                                    docCnt = 0;

                  // Check if final submission done by tenderer then an only docs of tendere will dispaly
                  String tenderBidSubStatus=negotiationProcessSrBean.checkFinalNegoFinalSubmissionDone(negId);
                  if(tenderBidSubStatus != null && tenderBidSubStatus.equalsIgnoreCase("yes")) // Check if final submission done by tenderer
                    {
                         if(sptcdListBidder!=null && !sptcdListBidder.isEmpty())
                        {%>
                            <tr>
                                <td class="t-align-left ff">Reference Documents Uploaded by Bidder/Consultant :</td>
                                <td>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="32%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%

                                

                                for (SPTenderCommonData sptcd : sptcdListBidder) {
                                    docCnt++;
                    %>
                    <tr>
                        <td class="t-align-left"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docType=revisedoc&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=download&ub=2" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                             <% if("".equalsIgnoreCase(finalStatus) && sptcd.getFieldName5().trim().equalsIgnoreCase(session.getAttribute("userId").toString())){ %>
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docType=revisedoc&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tenderId%>&negId=<%=negId%>&funName=remove&ub=2"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                            <%}%>
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                                             sptcd = null;
                                         }
                                     }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                    </table>
                                </td>
                            </tr>
                          </table>
                                <%
                         }
                    }

                }%>
                        <% if("".equalsIgnoreCase(finalStatus)){ %>
                        <form name="frmNegotiateBid" id="idfrmNegotiateBid" method="post" action="<%=request.getContextPath()%>/NegotiationFormsSrBean">
                        <input type="hidden" name="action" id="action" value="negotiateformbid" />
                        <input type="hidden" name="hidtenderId" id="hidtenderId" value="<%=request.getParameter("tenderId")%>" />
                        <input type="hidden" name="hidnegId" id="hidnegId" value="<%=request.getParameter("negId")%>" />
                        <input type="hidden" name="negUserId" value="<%=usrId%>" />
                        
                        <table width="100%" cellpadding="0" cellspacing="0" class="tableList_1">
                            <tr>
                                <td class="t-align-left ff" width="22%">
                                    Upload Reference Document : 
                                </td>
                                <td>
                                    <a href="NegotiationDocsUpload.jsp?negId=<%=negId%>&tenderId=<%=request.getParameter("tenderId")%>&uId=<%=usrId%>&docType=revisedoc">Upload</a>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="t-align-left ff" >Remarks</td>
                                <td>
                                    <textarea style="width: 675px;" id="idRemarks" class="formTxtBox_1" rows="3" name="txtRemarks"></textarea>
                                </td>
                            </tr>
                        </table>
                         <table width="100%" cellspacing="0">
                            <tr>
                                <td colspan="2" class="t-align-center ff">
                                    <label class="formBtn_1 t_space">
                                        <input type="submit" name="btnsubmit" id="idbtnsubmit" value="Submit" />
                                    </label>
                                </td>
                            </tr>
                         </table>
                       </form>
                     <% } %>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
