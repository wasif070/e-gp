<%--
    Document   : officerTabPanel
    Created on : Nov 16, 2010, 11:46:23 AM
        Author     : Administrator
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalReportMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPostQueConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderPostQueConfigService"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="OfficerTabId" class="com.cptu.egp.eps.web.servicebean.OfficerTabSrBean" />
<jsp:useBean id="preTendDt" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    <script>
        function msgDA(){
            jAlert("L1 Report yet not approved by approving authority","Approved", function(RetVal) {});
        }
        function msgNOA(){
            jAlert("L1 Report yet not approved by approving authority","Approved", function(RetVal) {});
        }
    </script>
    
        
    
    
        <%
            String tab="";
            String tenderid="";
            String uId = "";
            int evalCount = 0;
            boolean isLOIAccessible = false;
            boolean clarificationFlag = false;
            
            
            
            CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");

            if(pageContext.getAttribute("tab") != null && !"null".equalsIgnoreCase(pageContext.getAttribute("tab").toString())){
                tab =   pageContext.getAttribute("tab").toString();
            }
            if (request.getParameter("tab") != null && !"null".equalsIgnoreCase(request.getParameter("tab"))) {
                tab = request.getParameter("tab");
            }
            if(request.getParameter("tenderid") != null && !"null".equalsIgnoreCase(request.getParameter("tenderid")) ){
                tenderid =   request.getParameter("tenderid");
            } else  if(request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId")) ){
                tenderid =   request.getParameter("tenderId");
            }
            int tenderID = Integer.parseInt(tenderid);

            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    //userId = Integer.parseInt(session.getAttribute("userId").toString());
                    uId = session.getAttribute("userId").toString();
             }
            
            
            
            

            Boolean isPackageWise = false, isThisTenApproved = false,isthisTECTSCMember = false;
            if(pageContext.getAttribute("isTenPackageWise")!=null){
                isPackageWise = (Boolean) pageContext.getAttribute("isTenPackageWise");
            }

            if(pageContext.getAttribute("isThisTenderApproved")!=null){
                isThisTenApproved = (Boolean) pageContext.getAttribute("isThisTenderApproved");
            }

            TenderCommonService tcs = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            
            List<Object> object = tcs.getTenderCreator(tenderID);
            String tenderCreator = object.get(0).toString();
            if(uId.equals(tenderCreator))
            {
                clarificationFlag = true;
            }
            
            
            
            evalCount = tcs.GetEvalCount(tenderID);

            List<SPTenderCommonData> sptcdMore = tcs.returndata("tenderinfobarMore", tenderid, null);
            String tenderProcureNature1="";String tabLabel ="";
            if(sptcdMore!= null && sptcdMore.get(0)!= null)
            {
                tenderProcureNature1=sptcdMore.get(0).getFieldName2();
            }
             List<SPTenderCommonData> listTECTSCMember = tcs.returndata("chkTECTSCComMember", tenderid, uId);
                                if (!listTECTSCMember.isEmpty()) {
                                    isthisTECTSCMember = true;
                                }

             /* Start: CODE TO CHECK PE USER */
             boolean isCurUserPE = false;
              List<SPTenderCommonData> listChkCurUserPE  = tcs.returndata("getPEOfficerUserIdfromTenderId", tenderid, uId);

              if (!listChkCurUserPE.isEmpty()) {
                  if(uId.equalsIgnoreCase(listChkCurUserPE.get(0).getFieldName1())){
                    isCurUserPE = true;
                  }
              }
              listChkCurUserPE = null;
              
              /* End: CODE TO CHECK PE USER */

              //checking if the LOI section is accessible for current user
              isLOIAccessible = (tcs.IsLOIAccessible(tenderID, Integer.parseInt(uId)) || isCurUserPE) && tcs.LOIVisibilityStatus(tenderID);
              
              /* Start: CODE TO CHECK AUTHORISED USER */
             boolean isCurUserAU = false;
              List<SPCommonSearchDataMore> lstChkAuthorisedUser =
                      dataMore.geteGPData("getTenderAuthorisedUser", tenderid, uId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
              if(!lstChkAuthorisedUser.isEmpty()){
                if(uId.equalsIgnoreCase(lstChkAuthorisedUser.get(0).getFieldName1())){
                    isCurUserAU = true;
                }
              }
              /* End: CODE TO CHECK AUTHORISED USER */

              /* Start: CODE TO CHECK HOPE USER */
             boolean isCurUserHope = false;
              List<SPCommonSearchDataMore> lstChkUserType =
                      dataMore.geteGPData("getTenderHopeUser", tenderid, uId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
              if(!lstChkUserType.isEmpty()){
                if(uId.equalsIgnoreCase(lstChkUserType.get(0).getFieldName1())){
                    isCurUserHope = true;
                }
              }
              lstChkUserType = null;
              /* End: CODE TO CHECK HOPE USER */

              /* Start: CODE TO CHECK SECRETORY USER */
             boolean isCurUserSecretory = false;
              lstChkUserType =
                      dataMore.geteGPData("getTenderSecratory", tenderid, uId, "Secretary", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
              if(!lstChkUserType.isEmpty()){
                if(uId.equalsIgnoreCase(lstChkUserType.get(0).getFieldName1())){
                    isCurUserSecretory = true;
                }
              }
              lstChkUserType= null;
              /* End: CODE TO CHECK SECRETORY USER */

              /* Start: CODE TO CHECK MINISTER IS APPROVING AUTHORITY FOR THIS TENDER */
             boolean isAAMinister = false;
              lstChkUserType = dataMore.geteGPData("getTenderAAMinister", tenderid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
              if(lstChkUserType !=null && !lstChkUserType.isEmpty() && Integer.parseInt(lstChkUserType.get(0).getFieldName1()) == 3){
                  isAAMinister = true;
              }
              lstChkUserType= null;
              /* End: CODE TO CHECK MINISTER IS APPROVING AUTHORITY FOR THIS TENDER */

              /* Start: CODE TO GET TENDER STATUS */
              boolean isCurTenCancelled=false;
              List<SPTenderCommonData> listTenderStatus  = tcs.returndata("getTenderStatus", tenderid, null);
               if (!listTenderStatus.isEmpty()) {
                  if("cancelled".equalsIgnoreCase(listTenderStatus.get(0).getFieldName1())){
                    isCurTenCancelled = true;
                  }
              }
              listTenderStatus = null;
              /* End: CODE TO GET TENDER STATUS */

            List<SPTenderCommonData>  getPreBidDate = preTendDt.getDataFromSP("GetPrebidDate",Integer.parseInt(tenderid),0);

            long isPublishedCount = tcs.tenderPublishCnt(tenderid);
            
            long tenderUnderPrepCnt = tcs.tenderUnderPrepCnt(tenderid);
            
            boolean isCurUserTenderCreator = false;
            TenderCommonService tenderCommonServ = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            List<SPTenderCommonData> userRights = tenderCommonServ.returndata("CheckTenderUserRights", uId,tenderid);           
                        
            if( ( !userRights.isEmpty() ) && ( uId.equals(tenderCreator) ) ){
                isCurUserTenderCreator = true;
            }
            
            boolean isCurUserTSCMember=false;

            List<SPTenderCommonData> lstOTPCheckIsTSCMember =
                     tcs.returndata("CheckIsTSCMember", tenderid, session.getAttribute("userId").toString());

             if (!lstOTPCheckIsTSCMember.isEmpty()) {
                 if ("Yes".equalsIgnoreCase(lstOTPCheckIsTSCMember.get(0).getFieldName1())) {
                     isCurUserTSCMember = true;
                 }
             }
            lstOTPCheckIsTSCMember = null;

            CommonService officeTabCommonService = (CommonService) AppContext.getSpringBean("CommonService");
            String officeTabPMethod = officeTabCommonService.getProcMethod(tenderid).toString();
            String officeTabEventType = officeTabCommonService.getEventType(tenderid).toString();
            boolean officeTabIs2Env = false;
            List<SPCommonSearchDataMore> envDataMores = dataMore.geteGPData("GetTenderEnvCount", tenderid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            if(envDataMores!=null && (!envDataMores.isEmpty()) && envDataMores.get(0).getFieldName1().equals("2")){
                officeTabIs2Env = true;
                envDataMores = null;
            }
            boolean officeTabIsCase1 = false;//PQ, 1st stage of TSTM and OSTETM
            if (officeTabEventType.equals("1 stage-TSTM") || officeTabEventType.equals("PQ") || (officeTabPMethod.equals("OSTETM") && (!officeTabIs2Env))) {
                officeTabIsCase1 = true;
            }
            request.setAttribute("isCase1", officeTabIsCase1);
        %>
        <ul class="tabPanel_1 noprint" id="offTabPanel">
            
            
            <li class=""><a href="../officer/Notice.jsp?tenderid=<%=tenderid%>" <%if("1".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Notice</a></li>
             <li><a id="tcTab" href="../officer/TenderComm.jsp?tenderid=<%=tenderid%>&comType=TC" <%if("23".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>TC</a></li>

            
            <%if(isPublishedCount!=0){%>

               <li class="">
               
                   
               <%
                    boolean isTenderSentToPE=false, isTenderSentToTEC_CP=false;
                    String thisTenderPEId="0", thisTenderTEC_CPId="0";
              List<SPTenderCommonData> listForOpeningTab = tcs.returndata("getTenderInfoForOpeningTab", tenderid, null);
                if (!listForOpeningTab.isEmpty()) {

                    if("yes".equalsIgnoreCase(listForOpeningTab.get(0).getFieldName1())){
                        isTenderSentToPE=true;
                    }

                    if(!"0".equalsIgnoreCase(listForOpeningTab.get(0).getFieldName2())){
                        thisTenderPEId=listForOpeningTab.get(0).getFieldName2();
                    }

                    if("yes".equalsIgnoreCase(listForOpeningTab.get(0).getFieldName3())){
                        isTenderSentToTEC_CP=true;
                    }

                     if(!"0".equalsIgnoreCase(listForOpeningTab.get(0).getFieldName4())){
                        thisTenderTEC_CPId=listForOpeningTab.get(0).getFieldName4();
                    }



                }
%>

                

                <%if(isCurTenCancelled){%>
                <script>
                          $(document).ready(function(){
                              $('#tbOpening').click(function(){
                                  jAlert('Tender has been Cancelled.');
                                  return false;
                              });
                          });
                      </script>
                
               
                <a id="tabPayment" <%if(!isCurUserPE){%>style="display: none;"<%}%> href="../partner/ForTenderPayment.jsp?tab=8&tenderId=<%=tenderid%>" <%if("8".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Payment</a> </li>
                <li> <a id="tbOpening" href="" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a> 
                      <%} else if(!isTenderSentToPE){%>
                        <a href="../officer/OpenComm.jsp?tenderid=<%=tenderid%>" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
               <%} else if (isTenderSentToPE) {%>
                     <%if(!isTenderSentToTEC_CP){%>
                     <%if(uId.equalsIgnoreCase(thisTenderPEId)){%>
                        <a href="../officer/OpenComm.jsp?tenderid=<%=tenderid%>" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
                      <%} else {%>
                      <script>
                          $(document).ready(function(){
                              $('#tbOpening').click(function(){
                                  jAlert('Tender Opening step completed and handed over to PA/AU.');
                                  return false;
                              });
                          });
                      </script>
                        <a id="tbOpening" href="" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
                      <%}%>
                    <%} else if (isTenderSentToTEC_CP) {%>
                        <%if(uId.equalsIgnoreCase(thisTenderPEId)){%>
                            <%if(uId.equalsIgnoreCase(thisTenderPEId)){%>
                        <a href="../officer/OpenComm.jsp?tenderid=<%=tenderid%>" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
                            <%}%>
                        <%} else if (uId.equalsIgnoreCase(thisTenderTEC_CPId)) {%>
                        <a href="../officer/OpenComm.jsp?tenderid=<%=tenderid%>" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
                        <%} else {%>
                        <script>
                            $(document).ready(function(){
                                $('#tbOpening').click(function(){
                                    jAlert('Tender Opening step completed and handed over to PA/AU.');
                                    return false;
                                });
                            });
                        </script>
                        <a id="tbOpening" href="" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
                            <%}%>
                    <%}%>
               <%}%>
           </li>
           <%--<%
                        List<SPTenderCommonData> listPe = tcs.returndata("getPEOfficerUserIdfromTenderId", tenderid,null);
                        String peId = "";
                        if(!listPe.isEmpty()){
                                peId = listPe.get(0).getFieldName1();
                            }
                  if(uId.equalsIgnoreCase(peId)){
           %>
<!--                <li class=""><a href="../officer/EvalCommTSC.jsp?tenderid=<%=tenderid%>&tsedit=y" <%if("7".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Evaluation</a></li>-->
                <%}else{%>--%>


                   <%if(isCurUserTSCMember){%>
                    <li class=""><a href="../officer/EvalTSC.jsp?tenderId=<%=tenderid%>" <%if("7".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Evaluation</a></li>
                   <%} else {%>
                    <li class=""><a href="../officer/EvalComm.jsp?tenderid=<%=tenderid%>" <%if("7".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Evaluation</a></li>
                   <%}%>
<%--                    <li><a id="tcTab" href="../officer/TenderComm.jsp?tenderid=<%=tenderid%>&comType=TC" <%if("23".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>TC</a></li>
--%>
           <%}else{%>

       
           <li class="">

               <%
                    boolean isTenderSentToPE=false, isTenderSentToTEC_CP=false;
                    String thisTenderPEId="0", thisTenderTEC_CPId="0";
              List<SPTenderCommonData> listForOpeningTab = tcs.returndata("getTenderInfoForOpeningTab", tenderid, null);
                if (!listForOpeningTab.isEmpty()) {

                    if("yes".equalsIgnoreCase(listForOpeningTab.get(0).getFieldName1())){
                        isTenderSentToPE=true;
                    }

                    if(!"0".equalsIgnoreCase(listForOpeningTab.get(0).getFieldName2())){
                        thisTenderPEId=listForOpeningTab.get(0).getFieldName2();
                    }

                    if("yes".equalsIgnoreCase(listForOpeningTab.get(0).getFieldName3())){
                        isTenderSentToTEC_CP=true;
                    }

                     if(!"0".equalsIgnoreCase(listForOpeningTab.get(0).getFieldName4())){
                        thisTenderTEC_CPId=listForOpeningTab.get(0).getFieldName4();
                    }
                }
%>
                <%if(isCurTenCancelled){%>
                <script>
                          $(document).ready(function(){
                              $('#tbOpening').click(function(){
                                  jAlert('Tender has been Cancelled.');
                                  return false;
                              });
                          });
                      </script>
                <li> <a id="tabPayment" <%if(!isCurUserPE){%>style="display: none;"<%}%> href="../partner/ForTenderPayment.jsp?tab=8&tenderId=<%=tenderid%>" <%if("8".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Payment</a> </li>
                 <li><a id="tbOpening" href="" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a> </li>
                      <%} else if(!isTenderSentToPE){%>
                    <a href="../officer/OpenComm.jsp?tenderid=<%=tenderid%>" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
               <%} else if (isTenderSentToPE) {%>
                     <%if(!isTenderSentToTEC_CP){%>
                     <%if(uId.equalsIgnoreCase(thisTenderPEId)){%>
                        <a href="../officer/OpenComm.jsp?tenderid=<%=tenderid%>" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
                      <%} else {%>
                      <script>
                          $(document).ready(function(){
                              $('#tbOpening').click(function(){
                                  jAlert('Tender Opening step completed and handed over to PA/AU.');
                                  return false;
                              });
                          });
                      </script>
                      <a id="tbOpening" href="" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
                      <%}%>
                    <%} else if (isTenderSentToTEC_CP) {%>
                        <%if(uId.equalsIgnoreCase(thisTenderPEId)){%>
                            <%if(uId.equalsIgnoreCase(thisTenderPEId)){%>
                                <a href="../officer/OpenComm.jsp?tenderid=<%=tenderid%>" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
                            <%}%>
                        <%} else if (uId.equalsIgnoreCase(thisTenderTEC_CPId)) {%>
                            <a href="../officer/OpenComm.jsp?tenderid=<%=tenderid%>" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
                        <%} else {%>
                        <script>
                            $(document).ready(function(){
                                $('#tbOpening').click(function(){
                                    jAlert('Tender Opening step completed and handed over to PA/AU.');
                                    return false;
                                });
                            });
                        </script>
                      <a id="tbOpening" href="" <%if("6".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Opening</a>
                            <%}%>
                    <%}%>
               <%}%>
               </li>
               
               <%if(isCurUserTSCMember){%>
                            <li class=""><a href="../officer/EvalTSC.jsp?tenderId=<%=tenderid%>" <%if("7".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Evaluation</a></li>
                   <%} else {%>
                    <li class=""><a href="../officer/EvalComm.jsp?tenderid=<%=tenderid%>" <%if("7".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Evaluation</a></li>
                   <%}%>
                    
<!--                    <li><a id="tcTab" href="../officer/TenderComm.jsp?tenderid=<%=tenderid%>&comType=TC" <%if("23".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>TC</a></li>
                    
                    
               
               
           
           <%}%>
           
           
           
           
           
           <!-- moved part -->
            
            <% if ( tenderUnderPrepCnt > 0) { %>
                <% if( isCurUserHope || isCurUserTenderCreator ) { %>
                    <% if(isPackageWise.booleanValue()){%>
                        <li class=""><a href="../officer/TenderDocPrep.jsp?tenderId=<%= tenderid %>" <%if("2".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Document</a></li>
                    <% }else{%>
                        <li class=""><a href="../officer/LotDetails.jsp?tenderid=<%= tenderid %>" <%if("2".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Document</a></li>
                    <% }   
                } 
            }else{  
              if(isPackageWise.booleanValue()){ %>
                        <li class=""><a href="../officer/TenderDocPrep.jsp?tenderId=<%= tenderid %>" <%if("2".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Document</a></li>
                    <% } else{%>
                        <li class=""><a href="../officer/LotDetails.jsp?tenderid=<%= tenderid %>" <%if("2".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Document</a></li>
                    <% }  
                }
                    %>   
                
<!--            <li class=""><a href="../officer/TenderAdvList.jsp?tenderId=<%=tenderid%>" <%if("12".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Advt.</a></li>-->
                
            <%
                       TenderPostQueConfigService tenderPostQueConfigService = (TenderPostQueConfigService) AppContext.getSpringBean("TenderPostQueConfigService");
                       if (session.getAttribute("userId") != null) {
                            tenderPostQueConfigService.setLogUserId(session.getAttribute("userId").toString());
                       }
                       
                       List<TblTenderPostQueConfig> listt = tenderPostQueConfigService.getTenderPostQueConfig(tenderID);
                       if(!listt.isEmpty())
                       {
                            String isQueAnsConfig = listt.get(0).getIsQueAnsConfig();
                            if("Yes".equalsIgnoreCase(isQueAnsConfig) && clarificationFlag)
                            {
            %>
                                <li class=""><a href="../officer/AnsTenderer.jsp?tenderId=<%=tenderid%>" id="hrfQusAns" <%if("11".equalsIgnoreCase(tab)){%>class="sMenu"<%}%> title="Clarification on Tender/Proposal">Clarification</a></li>
            <%
                            }
                       }
%>

           <%if(isPublishedCount!=0){
               if(getPreBidDate.size() > 0){
                 if(getPreBidDate.get(0).getFieldName1() != null){
          if(!getPreBidDate.get(0).getFieldName1().endsWith("1900 00:00")){%>
            <li class="">
                <a href="../officer/PreTenderMeeting.jsp?tenderid=<%=tenderid%>" <%if("4".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>
                <%
                if(tenderProcureNature1 != null && tenderProcureNature1.equalsIgnoreCase("Services"))
                {
                %>
                    Pre Prop. Meeting
                <%
                }
                else
                {
                %>
                    Pre Tend. Meeting
                <%
                }
                %>
                </a></li>
           <%}}}%>
           <li class=""><a href="../officer/Amendment.jsp?tenderid=<%=tenderid%>" <%if("5".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Corrigendum/Amendment</a></li>
           <li class="">
               <a id="tabPayment" <%if(!isCurUserPE){%>style="display: none;"<%}%> href="../partner/ForTenderPayment.jsp?tab=8&tenderId=<%=tenderid%>" <%if("8".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Payment</a>
            </li>
            <%}%>
            
<!--            //moved part ends here-->
           
           
           
           
           
           
           
           
           
           <%--<%}%>--%>
            <%if(isPublishedCount!=0){%>

<!--           <li><a href="../officer/AnsTenderer.jsp?tenderId=<=tenderid%>" id="hrfQusAns" <if("11".equalsIgnoreCase(tab)){%>class="sMenu"<}%>  >Q & A</a></li>-->

            <%//rishita - Start - NOA
            if(!officeTabIsCase1){
            // - removed - List<SPTenderCommonData> listStatusAA = tcs.returndata("GetEvalRptApproval", tenderid, uId);
            //List<TblEvalReportMaster> tblEvalReportMaster = OfficerTabId.getStatusAA(Integer.parseInt(tenderid));
            /*if(!listStatusAA.isEmpty()){
                if(listStatusAA.get(0).getFieldName1().equalsIgnoreCase("approved")){*/
                    List<TblTenderDetails> tblTenderDetails = OfficerTabId.getProcurementNature(Integer.parseInt(tenderid));
                    String procNatureNOA = tblTenderDetails.get(0).getProcurementNature();
//                    if(!procNatureNOA.equalsIgnoreCase("Services")){
                    if(!"REOI".equalsIgnoreCase(officeTabEventType)){
                        
                        if(isLOIAccessible){
                            %>
                            <li class=""><a href="<%=request.getContextPath()%>/officer/LOI.jsp?tenderid=<%=tenderid %>" id="loi" <%if("24".equalsIgnoreCase(tab)){%>class="sMenu"<%}%> >Letter Of Intent</a></li>
                            <li class=""><a href="<%=request.getContextPath()%>/officer/NOA.jsp?tenderId=<%=tenderid %>" id="noa" <%if("13".equalsIgnoreCase(tab)){%>class="sMenu"<%}%> >Letter of Acceptance</a></li>
                        <%}
                        
                        
                        if(isCurUserPE ){ //NOA tab disable for 1st phase UAT test
            %>

          <!--
            < %      }else if(procNatureNOA.equalsIgnoreCase("Services") && !"REOI".equalsIgnoreCase(officeTabEventType)){ %
            <li class=""><a href="< %=request.getContextPath()%>/officer/NOA.jsp?tenderId=< %=tenderid %>" id="noa" < %if("13".equalsIgnoreCase(tab)){%>class="sMenu"< %}%> >Draft Agreement</a></li>-->
<!--            <li class=""><a href="<-%=request.getContextPath()%>/officer/NOA.jsp?tenderId=<-%=tenderid %>" id="danoa" < %//if("14".equalsIgnoreCase(tab)){%>class="sMenu"< %//}%> >Draft Agreement</a></li>-->
            <%}}if(isCurUserPE || isCurUserAU){
                boolean flaggOtp =  officeTabCommonService.getNoaStatus(tenderID);
                if(flaggOtp){
            %>
              <li class=""><a title="Private Discussion Forum" href="<%=request.getContextPath()%>/resources/common/PriDissForumTopicList.jsp?tenderId=<%=tenderid %>" id="cms" <%if("17".equalsIgnoreCase(tab)){%>class="sMenu"<%}%> >Private Forum</a></li>
            <%}}
                //} }
            //END - NOA
            // bug id = 3868
        //if(!procNatureNOA.equalsIgnoreCase("Services")){
            if("PQ".equalsIgnoreCase(officeTabEventType) || "REOI".equalsIgnoreCase(officeTabEventType) || "1 stage-TSTM".equalsIgnoreCase(officeTabEventType)){
            }else{
                 if(isCurUserPE && false){ //Contract Signing tab disable for 1st phase UAT test
        %>
            <li class=""><a href="<%=request.getContextPath()%>/officer/NOAListing.jsp?tenderId=<%=tenderid %>" id="noaListing" <%if("15".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Contract Signing</a></li>
             <% }}
            //} %>
<!--              <li class=""><a href="<%=request.getContextPath()%>/officer/CMS.jsp?tenderId=<%=tenderid %>" id="cms" <%if("14".equalsIgnoreCase(tab)){%>class="sMenu"<%}%> >CMS</a></li>-->
             <%
                if(isCurUserPE){
                    if("REOI".equalsIgnoreCase(officeTabEventType)){
                        // No contract siging for REOI services case.
                    }else{
                        if (false){ //CMS tab disable for 1st phase UAT test
             %>
                        <li class=""><a href="<%=request.getContextPath()%>/officer/CMSMain.jsp?tenderId=<%=tenderid %>" id="cms" <%if("14".equalsIgnoreCase(tab)){%>class="sMenu"<%}%> >CMS</a></li>
             <%          }
                    }
                }
             %>
<%--            <li><a href="../officer/AnsTenderer.jsp?tenderId=<%=tenderid%>" id="hrfQusAns" <%if("11".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>  >Q & A</a></li>
--%>
            <%}%>
            <%}%>
            
            
            
            
            
            
            <%
            if(isPublishedCount!=0){
                if (session.getAttribute("userTypeId").toString().equals("3")) {
                    Object objProRole = session.getAttribute("procurementRole");
                    String  strProRole = "";
                        if (objProRole != null) {
                            strProRole = objProRole.toString();
                        }
                    String  chk = strProRole;
                    String ck[] = chk.split(",");

                    for (int iAfterLoginTop = 0; iAfterLoginTop < ck.length; iAfterLoginTop++) {
                        if (ck[iAfterLoginTop].equalsIgnoreCase("PE") && false ) { //Complaint Mgmt tab disable for 1st phase UAT test
                            %><li class=""><a href="<%=request.getContextPath()%>/complaintOfficer.htm?tenderId=<%=tenderid %>" id="noaListing" <%if("16".equalsIgnoreCase(tab)){%>class="sMenu"<%}%>>Complaint Mgmt</a></li><%
                            break;
                        }
                    }
                }//end
            }
                    %>
        </ul>

    <% //if(!isThisTenApproved){ %>
<!--    <script>
            $(document).ready(function(){

             //have an alert box displayed on body load.
                $('#tabPayment').click(function(){
                    jAlert("Payment details can not be entered. Tender not yet published", '', function(r){if(r){return true;}else{return false;}});
                });
            });
         </script>-->
    <% //} %>
</html>
