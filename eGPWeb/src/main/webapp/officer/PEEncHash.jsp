<%-- 
    Document   : PEEncHash
    Created on : Nov 16, 2010, 11:06:27 AM
    Author     : TaherT
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommitteMemDtBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean"%>
<jsp:useBean id="tenderCommitteSrBean" class="com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean"/>
<%@page buffer="10kb"%>
<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />        
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <title>TOC members for Encryption/Decryption</title>
        <script type="text/javascript">
            $(function() {
                $('#frmEnc').submit(function() {                   
                    var check=0;
                    $(":checkbox[checked='true']").each(function(){
                        check++;
                    });
                    if(check<2){
                        jAlert("Minimum 2 members required.","Min member alert", function(RetVal) {
                        });
                        return false;
                    }else{
                        $("#hdnsubmit").attr('disabled', true);
                        $("#submit").val('Submit');
                        return true;                        
                    }
                });                
            });
        </script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="pageHead_1">TOC  members for Encryption/Decryption<span style="float:right;"><a href="Notice.jsp?tenderid=<%=request.getParameter("tenderid")%>" class="action-button-goback">Go back to dashboard</a></span></div>
        <%
             pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
             tenderCommitteSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
        %>        
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <%
                    String[] users=null;
                    int i=0;
                    Map<String,String> userTransMap = new HashMap<String, String>();
                    if("y".equals(request.getParameter("isedit")) || "y".equals(request.getParameter("isview"))){
                        users = tenderCommitteSrBean.findTenderHash(request.getParameter("tenderid"));
                    }
                    if ("Submit".equals(request.getParameter("submit"))) {                        
                        tenderCommitteSrBean.openComMemEncry(request.getParameterValues("userId"), request.getParameter("tenderid"), request.getSession().getAttribute("userId").toString());
                        response.sendRedirect("Notice.jsp?tenderid="+request.getParameter("tenderid"));
                    }
                    if ("Update".equals(request.getParameter("update"))) {
                        tenderCommitteSrBean.updateComMemEncry(request.getParameterValues("userId"), request.getParameter("tenderid"), request.getSession().getAttribute("userId").toString());
                        response.sendRedirect("Notice.jsp?tenderid="+request.getParameter("tenderid"));

                    }
                    java.util.List<CommitteMemDtBean> comMemberDtBeans = tenderCommitteSrBean.committeUser(Integer.parseInt(request.getParameter("tenderid")), "TOCApprovedMember");
        %>                       
        <form action="PEEncHash.jsp" method="post" id="frmEnc">
            <input type="hidden" value="<%=request.getParameter("tenderid")%>" name="tenderid"/>
            <table class="tableList_1 t_space" cellspacing="0" width="100%" id="members">
                <thead>
                    <tr>
                        <%if(!"y".equals(request.getParameter("isview"))){%><th class="t-align-center" width="8%">Select</th><%}%>
                        <th class="t-align-left" width="65%">Committee Member</th>
                        <th class="t-align-left" width="27%">Committee Role</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                    if(comMemberDtBeans.size()>0){
                    for (CommitteMemDtBean cmdb : comMemberDtBeans) {%>
                    <%if("y".equals(request.getParameter("isview"))){for(int j=0;j<users.length;j++){
                        userTransMap.put(String.valueOf(cmdb.getUserId()), "3");
                              if(cmdb.getUserId().equals(users[j])){%>
                    <tr>
                        
                        <td class="t-align-left"><%=cmdb.getEmpName()%></td>
                        <td class="t-align-left"><%if (cmdb.getMemRole().equals("m")) {%>Member<%} else if (cmdb.getMemRole().equals("ms")) {%>Member Secretary<%} else {%>Chairperson<%}%></td>
                    </tr>
                        <%}}}else{%>
                     <tr>
                        <td class="t-align-center">
                                <input name="userId" value="<%=cmdb.getUserId()%>" type="checkbox" <%if("y".equals(request.getParameter("isedit"))){for(int j=0;j<users.length;j++){
                              if(cmdb.getUserId().equals(users[j])){%>checked<%}}}%>/>
                        </td>
                        
                        <td class="t-align-left"><%=cmdb.getEmpName()%></td>
                        <td class="t-align-left"><%if (cmdb.getMemRole().equals("m")) {%>Member<%} else if (cmdb.getMemRole().equals("ms")) {%>Member Secretary<%} else {%>Chairperson<%}%></td>
                    </tr>
                    <%i++;}} }else{%>
                    <tr>
                <td colspan="3">TOC has not been configured.</td>
                </tr>
                    <% } %>
                </tbody>
            </table>
            <div class="t_space t-align-center">                
                    <%if("y".equals(request.getParameter("isedit"))){%>
                    <label class="formBtn_1">
                    <input name="update" value="Update" type="submit">
                    </label>
                    <%}else if("y".equals(request.getParameter("isview"))){%>
                    <%}else{%>
                    <label class="formBtn_1">
                    <input id="hdnsubmit" name="hdnsubmit" value="Submit" type="submit"/>
                    <input id="submit" name="submit" value="" type="hidden"/>
                    </label>
                    <%}%>                
            </div>
        </form>
<%if("y".equals(request.getParameter("isview"))){
    request.setAttribute("listEmpTras", userTransMap);
%>
    <jsp:include page="../resources/common/EmpTransferHist.jsp" />
<%}%>
        <br/>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
