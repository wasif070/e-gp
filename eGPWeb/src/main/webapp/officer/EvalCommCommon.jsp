<%--
    Document   : EvalCommCommon
    Created on : Feb 19, 2011, 11:34:14 AM
    Author     : Swati
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SpgetCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TendererTabServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>

<!-- Dohatec Start -->
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalServiceForms"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.GrandSummaryService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPostQueConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderPostQueConfigService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory"%>
<!-- Dohatec End -->

<%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            String dbOperation = "";
%>

<%
            String strCmnUserId = session.getAttribute("userId").toString();
            boolean isTenPackageWis = false;
            if (pageContext.getAttribute("isTenPackageWise") != null) {
                isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
            }

            TenderCommonService tenderCommonServiceCommon = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            CommonSearchService commonSearchServiceecc = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");

            boolean isTECCommitteeCreated = false;
            boolean isEditable = false;
            boolean isConfigEntry = false;
            boolean isTSCReq = false;
            boolean showMapping = false;
            boolean showViewMappping = false;
            boolean isCmnMultiLots = false;
            boolean cmnIsTenPublish = false;
            boolean isTORPORRptRecieved = false;
            boolean isUserEvalMember = false;
            boolean isUserEvalTSCMember = false;
            boolean isEvalSignDone = false;
            boolean isopeningDate = false;
            boolean isCmnConfigTeamWise = false;
            boolean isCmnConfigIndWise = false;
            String userId_TEC_CP = "";
            String tender_status = "";
            String comTenderId = "0";
            String strCmnProcurementNature = "";
            String strCmnDocType = "";
            int intCmnEnvelopcnt = 0;


            if (request.getParameter("tenderid") != null) {
                comTenderId = request.getParameter("tenderid");
            } else if (request.getParameter("tenderId") != null) {
                comTenderId = request.getParameter("tenderId");
            }
            TenderService tenderServiceComm = (TenderService) AppContext.getSpringBean("TenderService");
            for(TblTenderDetails details : tenderServiceComm.getTenderDetails(Integer.parseInt(comTenderId))){
                tender_status = details.getTenderStatus();
            }
            isopeningDate = tenderCommonServiceCommon.checkOpeningDate(Integer.parseInt(comTenderId));
            List<SPTenderCommonData> lstChkEvalMember =
                    tenderCommonServiceCommon.returndata("chkEvalMember", comTenderId, strCmnUserId);
            if (!lstChkEvalMember.isEmpty()) {
                if ("Yes".equalsIgnoreCase(lstChkEvalMember.get(0).getFieldName1())) {
                    isUserEvalMember = true;
                }
                if ("Yes".equalsIgnoreCase(lstChkEvalMember.get(0).getFieldName2())) {
                    isUserEvalTSCMember = true;
                }
                
                if ("Yes".equalsIgnoreCase(lstChkEvalMember.get(0).getFieldName3())) {
                    isEvalSignDone = true;
                }

                if ("team".equalsIgnoreCase(lstChkEvalMember.get(0).getFieldName4())) {
                    isCmnConfigTeamWise = true;
                } else  if ("ind".equalsIgnoreCase(lstChkEvalMember.get(0).getFieldName4())) {
                    isCmnConfigIndWise = true;
                }


            }
            
           

            
            List<SPTenderCommonData> lstTEC_CPInfo =
                    tenderCommonServiceCommon.returndata("EvalTECChairPerson_Info", comTenderId, "");
            String strComments = "";
            if (!lstTEC_CPInfo.isEmpty()) {
                userId_TEC_CP = lstTEC_CPInfo.get(0).getFieldName1();
                //userId_TEC_CP = lstTEC_CPInfo.get(0).getFieldName9();

                if ("yes".equalsIgnoreCase(lstTEC_CPInfo.get(0).getFieldName4())) {
                    isTECCommitteeCreated = true;
                    if ("yes".equalsIgnoreCase(lstTEC_CPInfo.get(0).getFieldName5()) || "yes".equalsIgnoreCase(lstTEC_CPInfo.get(0).getFieldName6())) {
                        isEditable = false;
                    } else {
                        isEditable = true;
                    }

                    if ("yes".equalsIgnoreCase(lstTEC_CPInfo.get(0).getFieldName7())) {
                        isTSCReq = true;
                    }

                    if ("yes".equalsIgnoreCase(lstTEC_CPInfo.get(0).getFieldName8())) {
                        isTORPORRptRecieved = true;
                    }
                }
            }
            lstTEC_CPInfo = null;

            CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");

            int uid = 0;
            if (session.getAttribute("userId") != null) {
                Integer ob1 = (Integer) session.getAttribute("userId");
                uid = ob1.intValue();
            }
            WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
            short eventid = 6;
            int objectid = 0;
            short activityid = 6;
            int childid = 1;
            short wflevel = 1;
            int initiator = 0;
            int approver = 0;
            boolean isInitiater = false;
            boolean evntexist = false;
            String fileOnHand = "No";
            String checkperole = "No";
            String chTender = "No";
            boolean ispublish = false;
            boolean iswfLevelExist = false;
            boolean isconApprove = false;
            String evalCommit = "";
            String objtenderId = comTenderId;
            if (objtenderId != null) {
                objectid = Integer.parseInt(objtenderId);
                childid = objectid;
            }
            String donor = "";
            
            donor = workFlowSrBean.isDonorReq(String.valueOf(eventid),
                    Integer.valueOf(objectid));
            String[] norevs = donor.split("_");
            int norevrs = -1;

            String strrev = norevs[1];
            if (!strrev.equals("null")) {

                norevrs = Integer.parseInt(norevs[1]);
            }
            String rptStatus = "pending";
            EvaluationService evalService = (EvaluationService)AppContext.getSpringBean("EvaluationService");
            List<Object[]> viewEvalRptList =  evalService.forReprotProcessView(Integer.parseInt(comTenderId), -1);
            if(viewEvalRptList!=null & !viewEvalRptList.isEmpty()){
                rptStatus = viewEvalRptList.get(0)[0].toString();
            }
            evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
            List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
            // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);
            if (tblWorkFlowLevelConfig.size() > 0) {
                Iterator twflc = tblWorkFlowLevelConfig.iterator();
                iswfLevelExist = true;
                while (twflc.hasNext()) {
                    TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                    TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                    if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                        fileOnHand = workFlowlel.getFileOnHand();
                    }
                    if (workFlowlel.getWfRoleId() == 1) {
                        initiator = lmaster.getUserId();
                    }
                    if (workFlowlel.getWfRoleId() == 2) {
                        approver = lmaster.getUserId();
                    }
                    if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                        isInitiater = true;
                    }

                }

            }
            String userid = "";
            if (session.getAttribute("userId") != null) {
                Object obj = session.getAttribute("userId");
                userid = obj.toString();
            }
            
            
            TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
//            boolean  isthisTCChairperson = false;
//            List<SPTenderCommonData> listOfTCChairperson = wfTenderCommonService.returndata("chkTCChairperson", comTenderId, Integer.toString(uid));
//                                if (!listOfTCChairperson.isEmpty()) {
//                                    isthisTCChairperson = true;
//                                }
                                
            String field1 = "CheckCreator";
            List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, comTenderId, userid);

            if (editdata.size() > 0) {
                checkperole = "Yes";
            }

            // for publish link

            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            List<SPTenderCommonData> checkEval = tenderCommonService1.returndata("EvalComWorkFlowStatus", comTenderId, "'Pending'");

            if (checkEval.size() > 0) {
                chTender = "Yes";
            }
            List<SPTenderCommonData> publist = tenderCommonService1.returndata("EvalComWorkFlowStatus", comTenderId, "'Approved','Conditional Approval'");
            SPTenderCommonData spTenderCommondata = null;

            if (publist.size() > 0) {
                ispublish = true;
                spTenderCommondata = publist.get(0);
                evalCommit = spTenderCommondata.getFieldName1();
                String conap = spTenderCommondata.getFieldName2();
                if (conap.equalsIgnoreCase("Conditional Approval")) {
                    isconApprove = true;
                }
            }

            // Get Tender Procurement Nature info


            /* START : CODE TO GET TENDER ENVELOPE COUNT */
            List<SPTenderCommonData> lstCmnEnvelops =
                    tenderCommonService1.returndata("getTenderEnvelopeCount", comTenderId, "0");

            if (!lstCmnEnvelops.isEmpty()) {
                if (Integer.parseInt(lstCmnEnvelops.get(0).getFieldName1()) > 0) {
                    intCmnEnvelopcnt = Integer.parseInt(lstCmnEnvelops.get(0).getFieldName1());
                }
                if ("Yes".equalsIgnoreCase(lstCmnEnvelops.get(0).getFieldName2())) {
                    isCmnMultiLots = true;
                }

                strCmnProcurementNature = lstCmnEnvelops.get(0).getFieldName3();
            }
            lstCmnEnvelops = null;
            /* START : CODE TO GET TENDER ENVELOPE COUNT */

            if ("Services".equalsIgnoreCase(strCmnProcurementNature)) {
                strCmnDocType = "Proposal";
            } else {
                strCmnDocType = "Tender";
            }



            List<SPTenderCommonData> cmnTenStatus =
                    tenderCommonService1.returndata("Checktenderpublishstatus", comTenderId, null);

            if (!cmnTenStatus.isEmpty()) {
                if (cmnTenStatus.get(0).getFieldName1().equalsIgnoreCase("approved")) {
                    cmnIsTenPublish = true;
                }
            }
            cmnTenStatus.clear();
            cmnTenStatus = null;
            String tempDataChk = null;
            if (!checkEval.isEmpty()) {
                tempDataChk = checkEval.get(0).getFieldName4();
            }
            if (!publist.isEmpty()) {
                tempDataChk = publist.get(0).getFieldName4();
            }

            String strCmnComType = "null";
            if (request.getParameter("comType") != null && !"null".equalsIgnoreCase(request.getParameter("comType"))) {
                strCmnComType = request.getParameter("comType");
            }

            
            List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights", Integer.toString(uid),comTenderId);
            List<SPTenderCommonData> chekTenderCreator = wfTenderCommonService.returndata("CheckTenderCreator", Integer.toString(uid),comTenderId);
            



            //added to check if the user is a initiator and fileonhand is yes
            
            
//            List<CommonAppData> wfdata = workFlowSrBean.editWorkFlowData("checkInitiator", comTenderId, userid);
//       
//            boolean isInitiatorAndFileOnHand = false;
//            if(!wfdata.isEmpty()){
//                isInitiatorAndFileOnHand = true;
//            }
            



%>

<table width="100%" cellspacing="0" class="tableList_1">
    <tr>
        <td width="30%" class="t-align-left ff">Evaluation Committee</td>
        <td width="80%" class="t-align-left">
            <%
                        com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean tenderCommitteSrBean = new com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean();
                        com.cptu.egp.eps.dao.storedprocedure.CommitteDtBean bean = tenderCommitteSrBean.findCommitteDetails(Integer.parseInt(comTenderId), "evalcom");
                        
                        long cntCommittee = committeMemberService.cntTECByTidAndCS(comTenderId, ""); //committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + comTenderId + " and tc.committeeType in ('TEC','PEC')")
                        long cntPendingCommittee = committeMemberService.cntTECByTidAndCS(comTenderId, "pending"); //committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + comTenderId + " and tc.committeeType in ('TEC','PEC') and tc.committeStatus='pending'")
                        //System.out.println(uid+"uid");
                       // System.out.println(isInitiater+"isInitiater");
                        //  System.out.println(initiator+"initiator");
                          
                        
                    
                        
                          
                          
                        if (bean.getpNature() != null) {
                         //ahsan   if(isthisTCChairperson || isInitiater || (uid == approver || uid == initiator)){
                                
                            if (cntCommittee == 0) {
                                if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){ //||isthisTCChairperson
                                 if((committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + comTenderId + " and tc.committeeType in ('TC') and tc.committeStatus='approved'")==0)){
                                                            %>
                                                            <a href="#" onclick='jAlert("TC has not been configured yet"," Alert ", "");'>Create</a>
                                                           &nbsp;|&nbsp;<a href="#" onclick='jAlert("TC has not been configured yet"," Alert ", "");' >Use Existing Committee</a>
                                                            
                                                            <% } else { %>
                                    <a href="CommFormation.jsp?tenderid=<%=comTenderId%>&parentLink=736">Create</a>&nbsp;
                                |&nbsp;<a href="MapCommittee.jsp?tenderid=<%=comTenderId%>&type=1&parentLink=939">Use Existing Committee</a>
            <%}}} else {
                                                                            if (cntPendingCommittee == 0) {%>
                                                                                <a href="CommFormation.jsp?tenderid=<%=comTenderId%>&isview=y&parentLink=903">View</a><%} else {%>

            <%
                                                                                if(!checkuserRights.isEmpty()   && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){// || isthisTCChairperson
                                                                                            if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                    || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true) || iswfLevelExist == false) {
            if(!"dump".equalsIgnoreCase(tempDataChk)){%>
            <a href="CommFormation.jsp?tenderid=<%=comTenderId%>&isedit=y&parentLink=894">Edit</a>&nbsp;| &nbsp;
            <% } } }%>

            <%
            if(!checkuserRights.isEmpty()   && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){ // || isthisTCChairperson
                
                boolean tst = publist.isEmpty();
            if ((!checkEval.isEmpty() || !publist.isEmpty())) {
                                                                                                                if ("dump".equalsIgnoreCase(tempDataChk)) {%>
            &nbsp;<a href="CommFormation.jsp?tenderid=<%=comTenderId%>&ispub=y&parentLink=904">Notify Committee Members</a>&nbsp;| &nbsp;
            <%} /*For cheking checkEval is empty or not*/ else if ((ispublish == true && (isInitiater == true /*|| isthisTCChairperson*/) && !evalCommit.equalsIgnoreCase("Approved")) || (!evalCommit.equalsIgnoreCase("Approved") && isInitiater == true && (initiator == approver && norevrs == 0) && initiator != 0 && approver != 0)) {%>
            &nbsp;<a href="CommFormation.jsp?tenderid=<%=comTenderId%>&ispub=y&parentLink=904">Notify Committee Members</a>&nbsp;| &nbsp;
            <% }%>
            <%}}%>
            <a href="CommFormation.jsp?tenderid=<%=comTenderId%>&isview=y&parentLink=903">View</a>
            <%
                                }
                            }
                        //ahsan     }
                        } else {
                            out.print("<div class='responseMsg noticeMsg'>Business rule yet not configured</div>");
                        }%>


        </td>
    </tr>
    <%
        List<SPTenderCommonData> getEvalCommPubDate =
                    tenderCommonService1.returndata("getEvalCommPubDate", comTenderId, null);
        if(getEvalCommPubDate!=null && !getEvalCommPubDate.isEmpty()){
    %>
    <tr>
        <td width="30%" class="t-align-left ff">Date and time of Committee Formation</td>
        <td width="80%" class="t-align-left"><%=getEvalCommPubDate.get(0).getFieldName1()%></td>
    </tr>
    <%}%>
    <%
                                if ((!checkEval.isEmpty() || !publist.isEmpty())) {
                                    if (!"dump".equalsIgnoreCase(tempDataChk)) {%>
    <tr>
        <td width="30%" class="t-align-left ff">Workflow</td>
        <td width="80%" class="t-align-left">
            <% if (evntexist) {

                                        if (fileOnHand.equalsIgnoreCase("yes") && !checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString()) && ispublish == false && isInitiater == true) {

            %>
<!--            <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=915'>Edit</a>

            &nbsp;|&nbsp;-->
            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=737">View</a> &nbsp;|&nbsp;

            <% } else if (iswfLevelExist == false && !checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString()) && ispublish == false) {
            %>

<!--            <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=915'>Edit</a>
            &nbsp;|&nbsp;-->
            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=737">View</a> &nbsp;|&nbsp;
            <%  } else if (iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {%>

            <label>Workflow yet not configured</label>
            <%                                          } else if (iswfLevelExist == true) {%>
            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=737">View</a>
            &nbsp;|&nbsp;
            <%
                 }
             } else if (ispublish == false && !checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())) {
                 List<CommonAppData> defaultconf = workFlowSrBean.editWorkFlowData("WorkFlowRuleEngine", Integer.toString(eventid), "");
                 if (defaultconf.size() > 0) {
            %>
            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&parentLink=123&submit=Submit&tc=tc">Create</a>

            <% } else {%>
            <a  href="#" onclick="checkdefaultconf()">Create</a>
            <%                         }
                             } else if (iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {%>
            <label>Workflow yet not configured</label>
            <% }%>
            <%
                                    if (isInitiater == true && (initiator != approver || (initiator == approver && norevrs > 0)) && ispublish == false && fileOnHand.equalsIgnoreCase("Yes")) {
            %>
            <% if (chTender.equalsIgnoreCase("Yes")) {%>
            <a href="FileProcessing.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&fromaction=eval" >Process file in Workflow</a>
            &nbsp;|&nbsp;
            <% }
                                                    }
            
            
    else if(isInitiater == false && (initiator != approver || (initiator == approver && norevrs > 0)) && ispublish == false && !fileOnHand.equalsIgnoreCase("Yes") && uid == approver){
    
    if (chTender.equalsIgnoreCase("Yes")) {%>
    
    <a href="FileProcessing.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&fromaction=pending" >Process file in Workflow</a>
            &nbsp;|&nbsp;
            <% }

    }

            %>
            <% if (iswfLevelExist) {%>
            <a href="workFlowHistory.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&userid=<%=uid%>&fraction=eval&parentLink=81" >View Workflow History</a>
            <% }%>

        </td>

    </tr>
    <% }    
                boolean isCase1Eval = false;
                if(request.getAttribute("isCase1")!=null){
                    isCase1Eval = (Boolean)request.getAttribute("isCase1");
                }
                if (ispublish == true && checkperole.equals("Yes") && (!isCase1Eval)) {
                    TenderSrBean tenderSrBeanForValSec = new TenderSrBean();
                    List<Object[]> objForChkingValSec = tenderSrBeanForValSec.getConfiForTender(Integer.parseInt(comTenderId));
                    if(!objForChkingValSec.isEmpty() && objForChkingValSec.get(0)[3].toString().equalsIgnoreCase("yes")){
                    if(tender_status.equals("Approved")){
                %>

                              
                                <tr><td width="30%" class="t-align-left ff">Validity and Security Extension</td>
    <td width="80%" class="t-align-left"><a href="TECProcess.jsp?tenderId=<%=comTenderId%>&comType=TEC">Extend</a></td></tr>
  <% } } } %>
                                <%}/*Check EvalCheck is empty or not*/%>




    <%
    List<SPTenderCommonData> lstTORReport =
                        tenderCommonService1.returndata("CheckValueIntblTosRptShare", comTenderId, null);
    if (lstTORReport.size() > 0) {
                    strComments = lstTORReport.get(0).getFieldName2();
    }
    //if (isTORPORRptRecieved && (isUserEvalMember || isUserEvalTSCMember)) {
    if (isTORPORRptRecieved && isUserEvalMember && isEvalSignDone) {
        
    %>
    <tr >
        <td width="30%" class="t-align-left ff"><%=strCmnDocType%> Opening Reports</td>
        <td width="80%" class="t-align-left">
            <%
                

                if (lstTORReport.size() > 0) {
                    //strComments = lstTORReport.get(0).getFieldName2();
            %>
            <%if (!isCmnMultiLots) {%>
            
            <a target="_blank" href="OpeningReports.jsp?tenderId=<%=comTenderId%>&lotId=0&comType=<%=strCmnComType%>">View</a>

            <%} else {%>
            <a href="OpeningReports.jsp?tenderId=<%=comTenderId%>&lotId=0&comType=<%=strCmnComType%>">View</a>


            <%
            // Multi Lots Case
            //if ( isCmnMultiLots) {
            if (false) {

                List<SPTenderCommonData> lstCmnLotsForTOR =
                    tenderCommonService1.returndata("getLoIdLotDesc_ForOpening", comTenderId, null);
                if (!lstCmnLotsForTOR.isEmpty()) {
            %>

            <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1 t_space">
     <tr>
         <th colspan="4">
             <%=strCmnDocType%> Opening Reports
         </th>
     </tr>
     <tr>
         <th>Lot No.</th>
         <th>Lot Description</th>
         <th>View</th>
     </tr>
   <%
               for (SPTenderCommonData objLot : lstCmnLotsForTOR) {
%>

<tr>
    <td width="15%" class="t-align-left"><%=objLot.getFieldName3()%></td>
    <td width="70%" class="t-align-left"><%=objLot.getFieldName2()%></td>
    <td width="15%" class="t-align-center">
            <a target="_blank" href="OpeningReports.jsp?tenderId=<%=comTenderId%>&lotId=<%=objLot.getFieldName1()%>&comType=<%=strCmnComType%>">View</a>
    </td>
</tr>

<%
               } // For Loop Ends
%>
</table>
            <%
                }

                lstCmnLotsForTOR = null;
            }
            %>




            <%}%>

            <%
                }
                lstTORReport = null;
            %>
        </td>
    </tr>  
            
    <%}%>
<tr>
        
        <td class="ff">
          PA's Comments for Opening Reports  
        </td>
        <td>
           <%=strComments%>
        </td>
    </tr>

    <!-- Dohatec Start -->

    <jsp:useBean id="tenderSrBean1" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <%-- comment out by ahsan
    <tr>
        <td class="ff">Contract Approval Workflow</td>

        <td class="t-align-left">
            <%                
                short wfActivityid = 13;
                short wfEventid = 12;
                boolean wfEvntexist = false;
                wfEvntexist = workFlowSrBean.isWorkFlowEventExist(wfEventid, objectid);
                String wfUrl = "";

                boolean isCancelled = true;
                int pqTenderId = 0;
                List<TblTenderDetails> tblTenderDetailTS = tenderSrBean1.getTenderStatus(String.valueOf(objectid));
                if (!tblTenderDetailTS.isEmpty()) {
                    pqTenderId = tblTenderDetailTS.get(0).getPqTenderId();
                    if ("Cancelled".equalsIgnoreCase(tblTenderDetailTS.get(0).getTenderStatus())) {
                        isCancelled = false;
                    }
                }
     
                SPTenderCommonData isFormOk = tenderSrBean1.isTenderFormOk(Integer.parseInt(objtenderId));
                List<TblTenderDetails> tenderDetails = workFlowSrBean.checkSubmissionDate(Integer.parseInt(objtenderId));

                boolean checksubmission = false;
                if (tenderDetails.size() > 0) {
                    TblTenderDetails wfTenderDetails = tenderDetails.get(0);
                    if (!"".equals(wfTenderDetails.getSubmissionDt()) && wfTenderDetails.getSubmissionDt() != null) {
                        checksubmission = true;
                    }
                }
                                
                String isSigned = tenderCommonService1.returndata("getWorkFlowEditEnable", comTenderId, "").get(0).getFieldName1();
                
                boolean istwfLevelExist = false;
                String wfFileOnHand = "No";
                int wfInitiator = 0;
                int wfApprover = 0;
                boolean isWfInitiater = false;
                String wfCheckperole = checkperole; //"No";
                String wfChTender = "No";
                boolean isWfpublish = false;
                boolean isWfConApprove = false;

                boolean b_isPublish = false;
                String tenderStatus = "";
                String tenderworkflow = "";

                List<SPTenderCommonData> wfPublist = wfTenderCommonService.returndata("TenderWorkFlowStatus", request.getParameter("tenderid"), "'Approved','Conditional Approval'");
                
                if (wfPublist.size() > 0) {
                    ispublish = true;
                    SPTenderCommonData wfSpTenderCommondata = wfPublist.get(0);
                    tenderStatus = wfSpTenderCommondata.getFieldName1();
                    tenderworkflow = wfSpTenderCommondata.getFieldName2();
                    if ("Approved".equalsIgnoreCase(tenderStatus)) {
                        b_isPublish = true;
                    }
                    if (tenderworkflow.equalsIgnoreCase("Conditional Approval")) {
                        isWfConApprove = true;
                    }
                }

                boolean approved = false;
                boolean forwarded = false;
                boolean editFlag = false;

                List<CommonSPWfFileHistory> commonSpWfFileHistory = workFlowSrBean.getWfHistory("History", (int)wfActivityid, (int)objectid,(int)childid ,uid, 1, 10,"processDate","desc","","","",0,"","");
                if(!commonSpWfFileHistory.isEmpty())
                {
                    for(int i = 0; i< commonSpWfFileHistory.size(); i++)
                    {
                        if(i == 0 && "Approve".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction()) && isSigned.equalsIgnoreCase("true"))
                        {
                            editFlag = true;
                            break;
                        }
                        if("Approve".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction()))
                        {
                            approved = true;
                            forwarded = true;
                            break;
                        }
                        if("Forward".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction()) && !forwarded)
                        {
                            forwarded = true;
                        }
                    }
                }

                short wfTlevel = 1;
                List<TblWorkFlowLevelConfig> wfTblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, wfActivityid);
                if (wfTblWorkFlowLevelConfig.size() > 0) {
                    Iterator wfTwflc = wfTblWorkFlowLevelConfig.iterator();
                    istwfLevelExist = true;
                    while (wfTwflc.hasNext()) {
                        TblWorkFlowLevelConfig workFlowlelT = (TblWorkFlowLevelConfig) wfTwflc.next();
                        TblLoginMaster wflmaster = workFlowlelT.getTblLoginMaster();
                        if (wfTlevel == workFlowlelT.getWfLevel() && uid == wflmaster.getUserId()) {
                            wfFileOnHand = workFlowlelT.getFileOnHand();
                        }
                        if (workFlowlelT.getWfRoleId() == 1) {
                            wfInitiator = wflmaster.getUserId();
                        }
                        if (workFlowlelT.getWfRoleId() == 2) {
                            wfApprover = wflmaster.getUserId();
                        }
                        if (wfFileOnHand.equalsIgnoreCase("yes") && uid == wflmaster.getUserId()) {
                            isWfInitiater = true;
                        }
                        if(wfTlevel == workFlowlelT.getWfLevel() && "yes".equalsIgnoreCase(workFlowlelT.getFileOnHand()) && !approved && !forwarded)
                        {
                            editFlag = true;
                        }
                    }
                }

                if(!editFlag)
                {
                    int noOfReviewer = 0;
                    List<CommonAppData> reviewer = workFlowSrBean.editWorkFlowData("workflowedit", "13", objtenderId);
                    Iterator it = reviewer.iterator();
                    while (it.hasNext()) {
                        CommonAppData commonAppData = (CommonAppData) it.next();
                        noOfReviewer = Integer.valueOf(commonAppData.getFieldName3());
                        if(noOfReviewer >= 0 && !editFlag && !approved && !forwarded)
                            editFlag = true;
                    }
                }

                String wfDonor = "";
                wfDonor = workFlowSrBean.isDonorReq(String.valueOf(wfEventid), Integer.valueOf(objectid));
                String[] wfNorevs = wfDonor.split("_");
                int wfNorevrs = -1;

                String wfStrrev = wfNorevs[1];
                if (!wfStrrev.equals("null")) {
                    wfNorevrs = Integer.parseInt(wfNorevs[1]);
                }

                if(wfEvntexist){
            
                        if(wfCheckperole.equals("Yes") && !b_isPublish == false && editFlag){ 
                            if(isCancelled){
                                wfUrl = "CreateWorkflow.jsp?activityid=" + wfActivityid + "&eventid=" + wfEventid + "&objectid=" + objectid + "&action=Edit&childid=" + childid + "&parentLink=916";
               %>
                            <a  href='CreateWorkflow.jsp?activityid=<%=wfActivityid%>&eventid=<%=wfEventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=916' onclick="return wfAlert()">Edit</a>&nbsp;|&nbsp;
                                <!--<a style="cursor: pointer" onclick="wfAlert()">Edit</a>&nbsp;|&nbsp;-->
                            <%}%>
                            <a  href="CreateWorkflow.jsp?activityid=<%=wfActivityid%>&eventid=<%=wfEventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=906">View</a>

                        <%} else if (istwfLevelExist == false && wfCheckperole.equals("Yes") && !b_isPublish == false && editFlag) {
                                  if (isCancelled) {
                                      wfUrl = "CreateWorkflow.jsp?activityid=" + wfActivityid + "&eventid=" + wfEventid + "&objectid=" + objectid + "&action=Edit&childid=" + childid + "&parentLink=916";
                        %>
                                    <a  href='CreateWorkflow.jsp?activityid=<%=wfActivityid%>&eventid=<%=wfEventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=916' onclick="return wfAlert()">Edit</a>&nbsp;|&nbsp;
                                    <!--<a style="cursor: pointer" onclick="wfAlert()">Edit</a>&nbsp;|&nbsp;-->
                                  <%}%>
                                  <a  href="CreateWorkflow.jsp?activityid=<%=wfActivityid%>&eventid=<%=wfEventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=906">View</a>

                        <%} else if (isCancelled && istwfLevelExist == false && !wfCheckperole.equalsIgnoreCase("Yes")) {%>
                                    <label>Workflow yet not configured</label>
                                    
                        <%} else if (istwfLevelExist == true) {%>
                                <a  href="CreateWorkflow.jsp?activityid=<%=wfActivityid%>&eventid=<%=wfEventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=906">View</a>
                        <%}
                        
                } else if (!b_isPublish == false && wfCheckperole.equals("Yes") && isSigned.equalsIgnoreCase("true")) {
                        if (isCancelled) {
                            wfUrl = "CreateWorkflow.jsp?activityid=" + wfActivityid + "&eventid=" + wfEventid + "&objectid=" + objectid + "&action=Create&childid=" + childid + "&parentLink=925";
                        %>
                            <a href="CreateWorkflow.jsp?activityid=<%=wfActivityid%>&eventid=<%=wfEventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&parentLink=925" onclick="return wfAlert()">Create</a>
                            <!--<a style="cursor: pointer" onclick="wfAlert()">Create</a>-->
                        <%}

                } else if (isCancelled && istwfLevelExist == false && !wfCheckperole.equalsIgnoreCase("Yes")) {%>
                        <label>Workflow yet not configured</label>
                <%}%>
                                    
                <%if (istwfLevelExist) {%>
                    &nbsp;| &nbsp;<a href="workFlowHistory.jsp?activityid=<%=wfActivityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=wfEventid%>&userid=<%=uid%>&fraction=eval&parentLink=75" >View Workflow History</a>
                <% }%>
                            
        </td>

    </tr>--%>
    <!-- Dohatec End -->
<%-- comment out by ahsan for bhutan
    <%if (isUserEvalMember || isUserEvalTSCMember) {%>
    <%
        if (!"".equalsIgnoreCase(userId_TEC_CP)) {
            if (isTECCommitteeCreated) {
    %>

    <tr>
        <td width="30%" class="t-align-left ff">Evaluation Configuration</td>
        <td>
            <%
                                        List<SPTenderCommonData> lstTenderTSCStatus =
                                                tenderCommonService1.returndata("getTenderTSCStatus", comTenderId, null);

                                        if (!lstTenderTSCStatus.isEmpty()) {
                                            showMapping = true;

                                            List<SPTenderCommonData> lstMappingInfo =
                                                    tenderCommonService1.returndata("getMappingInfo", comTenderId, null);

                                            if (!lstMappingInfo.isEmpty()) {
                                                if ("Yes".equalsIgnoreCase(lstMappingInfo.get(0).getFieldName1())) {
                                                    showViewMappping = true;
                                                }
                                            }

                                        }
                                        lstTenderTSCStatus = null;

                                        List<SPTenderCommonData> getTenderStatus =
                                                tenderCommonService1.returndata("Checktenderpublishstatus", comTenderId, null);

                                        if (!getTenderStatus.isEmpty()) {
                                            if (getTenderStatus.get(0).getFieldName1().equalsIgnoreCase("approved")) {


                                                List<SPCommonSearchData> getConfigurationDetails =
                                                        commonSearchServiceecc.searchData("chkConfigStatus", comTenderId, null, null, null, null, null, null, null, null);

                                                if (getConfigurationDetails.size() > 0) {

                                                    dbOperation = "update";
            %>
            <%
                                                  if (isEditable) {
            %>
            <%if (userid.equalsIgnoreCase(userId_TEC_CP)) {%>
            <a href="ViewConfiguration.jsp?tenderid=<%=comTenderId%>&comType=<%=strCmnComType%>">View Configuration</a>
            <%if(isCmnConfigTeamWise){%>
            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="EvalTeamStatus.jsp?tenderid=<%=comTenderId%>&comType=<%=strCmnComType%>">View Team Status</a>
            <%}%>
            <%if (isTSCReq && showMapping) {%>
            <%if (isTenPackageWis) {%>
<!--            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="../officer/MapEvalForms.jsp?tenderid=<%=comTenderId%>&lotId=0&comType=<%=strCmnComType%>">Share forms for TSC</a>-->
            <%} else {%>
<!--            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="../officer/MapEvalTenderLots.jsp?tenderid=<%=comTenderId%>&comType=<%=strCmnComType%>">Share forms for TSC</a>-->
            <%}%>

            <%if (showViewMappping) {%>
<!--            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="../officer/ViewEvaluationFormMapping.jsp?tenderId=<%=comTenderId%>&comType=<%=strCmnComType%>">View Shared Forms for TSC</a>-->
            <%}%>
            <%}%>


            <%} else {
                                   // Members Other than the TEC Chairperson
%>
            <a href="ViewConfiguration.jsp?tenderid=<%=comTenderId%>&comType=<%=strCmnComType%>">View Configuration</a>
            <%if (isTSCReq && showViewMappping) {%>
<!--            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="../officer/ViewEvaluationFormMapping.jsp?tenderId=<%=comTenderId%>&comType=<%=strCmnComType%>">View Shared Forms for TSC</a>-->
            <%}%>
            <%}%>

            <%} else {%>
            <a  href="ViewConfiguration.jsp?tenderid=<%=comTenderId%>&comType=<%=strCmnComType%>">View Configuration</a>
            <%if (userid.equalsIgnoreCase(userId_TEC_CP) && isCmnConfigTeamWise)   {%>
            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="EvalTeamStatus.jsp?tenderid=<%=comTenderId%>&comType=<%=strCmnComType%>">View Team Status</a>
            <%}%>
            <%if (isTSCReq) {%>
            <%if (userid.equalsIgnoreCase(userId_TEC_CP)) {%>

            <%if (isTenPackageWis) {%>
<!--            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="../officer/MapEvalForms.jsp?tenderId=<%=comTenderId%>&lotId=0&comType=<%=strCmnComType%>">Share forms for TSC</a>-->
            <%} else {%>
<!--            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="../officer/MapEvalTenderLots.jsp?tenderId=<%=comTenderId%>&comType=<%=strCmnComType%>">Share forms for TSC</a>-->
            <%}%>
            <%}%>
            <%if (showViewMappping) {%>
<!--            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="../officer/ViewEvaluationFormMapping.jsp?tenderId=<%=comTenderId%>&comType=<%=strCmnComType%>">View Shared Forms for TSC</a>-->
            <%}%>
            <%}%>

            <%}%>
            <% } else {
                                                                dbOperation = "add";%>

            <%if (userid.equalsIgnoreCase(userId_TEC_CP)) {%>

            <% boolean allowConfig = false;
                List<SPTenderCommonData> getTOSReportStatus =
                        tenderCommonService1.returndata("getTOSReportStatus", comTenderId, null);

                if (!getTOSReportStatus.isEmpty()) {
                    if ("yes".equalsIgnoreCase(getTOSReportStatus.get(0).getFieldName1())) {
                        allowConfig = true;
                    }
                }
                getTOSReportStatus = null;
            %>
            <%if (allowConfig) {%>
            <a href="EvalConfiguration.jsp?tenderid=<%=comTenderId%>&action=<%=dbOperation%>&comType=<%=strCmnComType%>&parentLink=739">Configure</a>
            <%} else {%>
            <script>
                $(document).ready(function(){
                    $('#tbConfiguration').click(function(){
                        jAlert('TOR/POR Report has not been recieved yet.');
                        return false;
                    });
                });
            </script>

            <a id="tbConfiguration" href="">Configure</a>
            <%}%>

            <%} else {%>
            Pending
            <%}%>
            <%
                                                }
                                            }
                                        }

                                        getTenderStatus = null;
            %>
        </td>
    </tr>    
    <%
                                TenderEstCost estcost = (TenderEstCost) AppContext.getSpringBean("TenderEstCost");
                                boolean Commonflag = false;
                                Commonflag = estcost.checkForLink(Integer.parseInt(comTenderId));
                                if(Commonflag){

    %>
    <tr>
     <td width="30%" class="t-align-left ff">Official Cost Estimate</td>
     <td>
<a href="ViewPckLotEstCost.jsp?tenderId=<%=comTenderId%>&isedit=n">View</a>
     </td>
    </tr>
    <%
         } %>
         

    <%                            if ("update".equalsIgnoreCase(dbOperation) && "pending".equalsIgnoreCase(rptStatus)) {
    %>
        <%--  comment out for hide tsc for bhutan by ahsan
            <%if (isTSCReq) {%>
            <tr>
                <td width="30%" class="t-align-left ff">TSC Formation Required</td>
                <td>
                    Yes
                </td>
            </tr>
            <%} else {
                if (userid.equalsIgnoreCase(userId_TEC_CP)) {
                    CommonSearchDataMoreService dataMoree = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> getTenderRefNo =
                            dataMoree.geteGPData("getTscMemberCount", comTenderId);

                    if (!getTenderRefNo.isEmpty()) {
            %>
             <tr>
                <td width="30%" class="t-align-left ff">TSC Formation Required</td>
                <td>
                    <!-- OnClick Event is added by Dohatec -->
                    <a href="<%=request.getContextPath()%>/EvalConfigurationServlet?func=requesttsc&tenderId=<%=comTenderId%>&refNo=<%=getTenderRefNo.get(0).getFieldName2()%>&action=add&comType=<%=strCmnComType%>" onclick="return CnfrmMessage()">Make Request for TSC formation to PE</a>
                </td>
            </tr>

            
            <%
                                            }
            %>

            <%} else {%>
<!--            No-->
            <%}%>
            <%}%>
       

    <%
                }
            }
        }

    %>


    <%
    if(isCmnConfigTeamWise){
    for (SPTenderCommonData getStatus : tenderCommonService1.returndata("getTECNomineeStatus", comTenderId, uid + "")) {
            if (getStatus.getFieldName1().equalsIgnoreCase("Pending")) {
    %>
    <tr>
        <td width="30%" class="t-align-left ff">Evaluation Nomination</td>
        <td>
            <a href="EvalNominationApproval.jsp?tenderid=<%=comTenderId%>&comType=<%=strCmnComType%>">Consent for Nomination</a>
        </td>
    </tr>
    <%
                        }
                    }
    }
                }
    %>
    --%>
    <%
       int suserTypeId =0;
       if (session.getAttribute("userTypeId") != null) {
            suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
       }
       List<SPTenderCommonData> list = tenderCommonServiceCommon.returndata("CheckPE", comTenderId, null);
       TendererTabServiceImpl tendererTabServiceImpl = (TendererTabServiceImpl) AppContext.getSpringBean("TendererTabServiceImpl");
       boolean flagg = true;
       flagg = tendererTabServiceImpl.isOpeningDateTime(Integer.parseInt(comTenderId));
       if(!flagg && list.get(0).getFieldName1().equalsIgnoreCase(strCmnUserId))
       {
    %>
        <%--  <tr>  <!-- Comment: Jira Issue "EBDP-10" disable debriefing under evaluation tab for now -->
            <td class="ff">Debriefing on Tender</td>
            <td>
                    <a href="<%=request.getContextPath()%>/officer/deBriefQueryView.jsp?tenderId=<%=comTenderId%>">View</a>
       
            </td>
        </tr> --%>
     <%}%>
</table>
<%
    int deb_count = 0;
    List<SPCommonSearchData> debarList = commonSearchServiceecc.searchData("getFinalSubUser", comTenderId, null, null, null, null, null, null, null, null);            
%>
<table class="tableList_1" width="100%" id="debarTab">
    <tr>
        <th colspan="5">Debarred Tenderers / Consultants’ Information</th>
    </tr>
    <tr>
        <th>Bidder/Consultant</th>
        <th>Debarred By</th>
        <th>Debared From</th>
        <th>Debared To</th>
        <th>Debarred Reason</th>
    </tr>
    <%
        for(SPCommonSearchData debarData : debarList){
            List<SPCommonSearchData> debarSubList = commonSearchServiceecc.searchData("GetDebarmentUserStatus", comTenderId, debarData.getFieldName1(), "0", null, null, null, null, null, null);
    %>
    <tr>
        <td><%=debarData.getFieldName2()%></td>
        <%if (!debarSubList.isEmpty() && debarSubList.get(0).getFieldName1().equalsIgnoreCase("yes")) {%>
        <td><%=debarSubList.get(0).getFieldName7()%></td>
        <td class="t-align-center"><%=debarSubList.get(0).getFieldName4()%></td>
        <td class="t-align-center"><%=debarSubList.get(0).getFieldName5()%></td>
        <td><%=debarSubList.get(0).getFieldName6()%></td>
        <%deb_count++;}else{%>
        <td class="t-align-center">-</td>
        <td class="t-align-center">-</td>
        <td class="t-align-center">-</td>
        <td class="t-align-center">-</td>
        <%}%>
    </tr>
    <%}%>
</table>

<!-- Dohatec Start -->
<!--<div id="dialog-confirm" title="Contract Approval Workflow">
    <p id="msgConfirm"></p>
</div>
<input type="hidden" id="wfUrl" value="<%//=wfUrl%>"/>-->
<!-- Dohatec End -->

<%if(deb_count==0){%>
<script type="text/javascript">
    $('#debarTab').hide();
    // Dohatec Starts
    function CnfrmMessage(){
        var flag = false;
        /*jConfirm("If you make request, the tender can not be evaluated unless TSC is formed and complete their function.\n\nAre you sure to request for TSC formation?","Confirm", function(RetVal){
             if(RetVal){
                flag = RetVal;
             }
         });*/
        flag = confirm("If you make request, the tender can not be evaluated unless \nTSC is formed and complete their function.\n\nAre you sure to request for TSC formation?")
        return flag;
    }
    // Dohatec End
</script>
<%}%>

<!-- Dohatec Start -->
    <script type="text/javascript">
        function wfAlert(){
        /*Edited 
            var msg = 'Please be double sure that you have consulted latest DOFP issued by Finance Division, \nfor creation of this particular work flow and selecting the competent  approval authority, \nand also be sure that number of  reviewer (s) including  DP’s reviewers(s) are correct. \nAny breach of confidentiality will fall under section 64 of the PPA.';
        */
            var msg = 'Please be double sure that you have consulted latest DOFP issued by Finance Division, \nfor creation of this particular work flow and selecting the competent  approval authority, \nand also be sure that number of  reviewer (s) including  DP’s reviewers(s) are correct.';

            return confirm(msg);
            /*$("#msgConfirm").text("Please be double sure that you have consulted latest DOFP issued by Finance Division, for creation of this particular work flow and selecting the competent  approval authority, and also be sure that number of  reviewer (s) including  DP’s reviewers(s) are correct. Any breach of confidentiality will fall under section 64 of the PPA.");
            $(function() {
                $( "#dialog-confirm" ).dialog({
                    resizable: false,
                    height:200,
                    width:430,
                    modal: true,
                    buttons: {
                        "Ok": function() {
                            $(this).dialog("close");
                            window.location = $('#wfUrl').val();
                        },
                        "Cancel": function() {
                            $(this).dialog("close");
                        }
                    }
                });
            });*/
        }
    </script>
<!-- Dohatec End -->

<%
// Set To Null
            userId_TEC_CP = null;
            comTenderId = null;
%>
