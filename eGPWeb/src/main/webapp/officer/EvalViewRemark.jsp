<%-- 
    Document   : EvalViewRemark
    Created on : Jan 4, 2011, 4:34:21 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluate Bidder/Consultant</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%
        String remark = request.getParameter("remark");
        %>
       
        <div class="contentArea_1">
                <div class="pageHead_1"></div>
                
                 <table width="100%" height="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td width="100%" valign="middle" class="t-align-left ff">Remark :</td>
                        </tr>
                        <tr>
                            <td width="100%" height="80%" class="t-align-left">
                                <label height="80%"><%=remark%></label>
                            </td>
                        </tr>
                 </table>
        </div>
        
     
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
