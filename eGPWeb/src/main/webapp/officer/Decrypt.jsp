<%-- 
    Document   : Decrypt
    Created on : Dec 18, 2010, 3:17:10 PM
    Author     : test
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderOpening"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean"%>
<jsp:useBean id ="CurrencySrBean" class="com.cptu.egp.eps.web.servicebean.ConfigureCurrencySrBean"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Decrypt</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

            int tenderId = 0;
            if(request.getParameter("tenderId")!=null){
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }

             String referer = "";
             if (request.getHeader("referer") != null) {
                referer=request.getHeader("referer");
             }

                TenderTablesSrBean beanCommon = new TenderTablesSrBean();
                String tenderType = beanCommon.getTenderType(Integer.parseInt(request.getParameter("tenderId")));


               // Coad added by Dipal for Audit Trail Log.
               
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                String idType="tenderId";
                int auditId=tenderId;
                String auditAction="Decrypt by Opening Committee Member";
                String moduleName=EgpModule.Tender_Opening.getName();
                String remarks="";
        %>
        <form id="frmDecrypt" name="frmDecrypt" method="post" action="">
                <input type="hidden" name="tenderid" value="<%= tenderId %>" />
                <input type="hidden" name="hdnFromDecryptPage" value="1" />
                <input type="hidden" name="hdnReferer" value="<%=referer%>" />
        </form>
        <%
        boolean check = false;
           TenderOpening tenOpen = new TenderOpening();
           check =  tenOpen.getTenderEncriptionData("getTenderEncData", request.getParameter("tenderId"), request.getParameter("formId"), "", "", "", "", "", "", "");
          String status="";
           if(check)
           {
               if(tenderType.equals("ICT")){
                List<Object[]> listTenderForm = new ArrayList<Object[]>();
                listTenderForm = CurrencySrBean.getTenderFormId(Integer.parseInt(request.getParameter("tenderId")));
                
                for(int i = 0;i<listTenderForm.size();i++)
                {
                if((listTenderForm.get(i)[0].toString()).equals(request.getParameter("formId")))
                    {
                    status = CurrencySrBean.updateColField(listTenderForm.get(i)[0].toString(), Integer.parseInt(request.getParameter("tenderId")));
                    if(status.equals("true"))
                        {
                         auditAction="Decrypt and update by Opening Committee Member";
                         makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                         response.sendRedirect(referer);
                        }
                    else
                        {
                            auditAction="Error after decryption by Opening Committee Member";
                            remarks="Error after decryption dutring update plain bid data";
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            out.print("<h1>Error after decryption</h1>");
                        }
                    }                    
                }
                if(status.equals("")){
                         auditAction="Decrypt by Opening Committee Member";
                         makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                         response.sendRedirect(referer);
                         }

                }else{
                if(status.equals(""))
                {
                         auditAction="Decrypt by Opening Committee Member";
                         makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                         response.sendRedirect(referer);

                }
                }
               // Coad added by Dipal for Audit Trail Log.
           %>
<!--           <script>
               jAlert("Form Decrypted successfully", 'Alert', function(r){
                   if(r){
                        document.getElementById("frmDecrypt").action = "OpenComm.jsp";                        
                        document.getElementById("frmDecrypt").submit();                        
                   }else{
                        document.getElementById("frmDecrypt").action = "OpenComm.jsp";                        
                        document.getElementById("frmDecrypt").submit();                        
                   }
               });
           </script>-->
            <%}else
            {
               
               // Coad added by Dipal for Audit Trail Log.
               auditAction="Error in Decrypt by Opening Committee Member";
               remarks=" Error in Decryption because form already Decrypted";
               makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
               out.print("<h1>Error in Decryption</h1>");
               %>
<!--             <script>
               jAlert("Form already Decrypted.", 'Alert', function(r){
                   if(r){
                        document.getElementById("frmDecrypt").action = "OpenComm.jsp";                        
                        document.getElementById("frmDecrypt").submit();                        
                   }else{
                        document.getElementById("frmDecrypt").action = "OpenComm.jsp";                        
                        document.getElementById("frmDecrypt").submit();                        
                   }
               });
           </script>-->
           <%
            }
           %>
    </body>

     <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
