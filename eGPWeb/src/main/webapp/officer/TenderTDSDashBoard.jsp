<%--
    Document   : TDSDashBoard
    Created on : Nov 10, 2010, 7:57:34 PM
    Author     : yanki
--%>

<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBeanTender" />

<%@page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <%
            int sectionId = Integer.parseInt(request.getParameter("sectionId"));
            int tenderId = Integer.parseInt(request.getParameter("tenderId"));
            int tenderStdId = Integer.parseInt(request.getParameter("tenderStdId"));
            int pkgOrLotId = -1;
            if(request.getParameter("porlId")!=null){
                pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
            }

            List<com.cptu.egp.eps.model.table.TblTenderIttHeader> subSectionDetail = createSubSectionSrBean.getSubSection_TDSAppl(sectionId);
            
            String contentType = createSubSectionSrBean.getContectType(sectionId);
            if(contentType.equalsIgnoreCase("ITT")){
                contentType = "BDS";
            }else if(contentType.equalsIgnoreCase("GCC")){
                contentType = "PCC";
            }
            /*     GSS-- editing TDS/PCC while corrigendum 07/14/2015      */
            boolean corriCrtNPending=false;
            String corriId="";
            if(request.getParameter("corriCrtNPending")!=null){
                if("true".equalsIgnoreCase(request.getParameter("corriCrtNPending"))){
                    corriCrtNPending=true;
                    }
                corriId=request.getParameter("corriId");
                }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%if(contentType.equals("PCC")){out.print("SCC");}else{out.print(contentType);}%> Dashboard</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%if(!"yes".equalsIgnoreCase(request.getParameter("noa"))){%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%}%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            
                            <div class="t_space">
                                <div class="pageHead_1"><%if(contentType.equals("PCC")){out.print("SCC");}else{out.print(contentType);}%> Dashboard
                                    <%if(!"yes".equalsIgnoreCase(request.getParameter("noa"))){%>
                                    <%if(pkgOrLotId == -1){%>
                                        <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>" class="action-button-goback">Go Back to Tender Document Preparation</a></span>
                                    <%}else{%>
                                        <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%= tenderId %>&porlId=<%= pkgOrLotId %><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>" title="Tender/Proposal Document" class="action-button-goback">Go Back to Tender Document Preparation</a></span>
                                    <%}%>
                                    <%}%>
                                </div>
                            </div>
                            <form action="DefineSTDInDtl.jsp" method="post">
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <%
                                for(int i=0;i<subSectionDetail.size();i++){
                                %>
                                    <tr>
                                        <td align="center" width="30%"><b>Sub Section No.</b></td>
                                        <td width="70%"><%=(i+1)%></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Name Of Sub Section</b></td>
                                        <td><%=subSectionDetail.get(i).getIttHeaderName() %></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Action</b></td>
                                        <td>
                                            <%
                                            if(!createSubSectionSrBean.isTdsSubClauseGenerated(subSectionDetail.get(i).getTenderIttHeaderId())){
                                            %>
                                                <%--<a href="#">Create</a>--%>
                                            <%
                                            } else {
                                            %>
                                            <a href='<%=request.getContextPath()%>/officer/PrepareTenderTDS.jsp?ittHeaderId=<%=subSectionDetail.get(i).getTenderIttHeaderId()%>&sectionId=<%=sectionId%>&tenderId=<%=request.getParameter("tenderId")%>&tenderStdId=<%=tenderStdId%>&edit=true&porlId=<%= pkgOrLotId %><%if(contentType.contains("PCC")){%>&parentLink=898<%}else if(contentType.contains("PDS") && i+1==4){%>&parentLink=896<%}%><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>'>Edit</a> |
                                                <a href="<%=request.getContextPath()%>/officer/viewTenderTDS.jsp?ittHeaderId=<%=subSectionDetail.get(i).getTenderIttHeaderId()%>&sectionId=<%=sectionId%>&tenderId=<%=request.getParameter("tenderId")%>&tenderStdId=<%=tenderStdId%>&porlId=<%= pkgOrLotId %><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>">View</a>
                                            <%
                                            }
                                            %>


                                        </td>
                                    </tr>
                                <%
                                }
                                subSectionDetail=null;
                                createSubSectionSrBean=null;
                                %>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <%if(!"yes".equalsIgnoreCase(request.getParameter("noa"))){%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <%}%>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
