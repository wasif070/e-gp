<%-- 
    Document   : ViewPackageEngineerEstimationDocs
    Created on : Jan 27, 2011, 4:12:27 PM
    Author     : Chalapathi.Bavisetti
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.APPSrBean"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.model.table.TblAppEngEstDoc"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Uploaded Files</title>
         <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
         <div class="pageHead_1">View Uploaded Files </div>
        <div>&nbsp;</div>
             <table border="0" <%--cellspacing="10"--%> width="100%" cellpadding="0" class="tableList_1 t_space">
                                <tr>

                                    <th>File Name</th>
                                    <th>Description</th>
                                    <th>Uploaded date&time</th>
                                    <th class="t-align-left" width="18%">Action</th>
                                </tr>
                                <%
                                            String packId = request.getParameter("pckId");
                                            String apptestId = request.getParameter("appId");
                                            int appId = Integer.parseInt(apptestId);
                                            int pckId = 0;
                                            pckId = Integer.parseInt(packId);
                                            APPSrBean appSrBean = new APPSrBean();
                                            int i = 1;
                                           List<TblAppEngEstDoc> tblAppEngEstDoc =  appSrBean.getAllListing(pckId);
                                           if(tblAppEngEstDoc.size()>0){

                                           for( TblAppEngEstDoc listings :tblAppEngEstDoc){
                                %>
                                <tr>
                                    <td align="left"><%= listings.getDocumentName()%></td>
                                    <td align="left"><%= listings.getDocumentDesc()%></td>
                                    <td align="left">
                                        <%= new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(new java.util.Date(listings.getUploadedDate().getTime()))%>
                                        <%//= new SimpleDateFormat("dd-MON-yyyy HH:mm:ss").format(new java.util.Date(listings.getUploadedDate().getTime()))%>
                                    </td>
                                    <td class="t-align-center"><a href="<%=request.getContextPath()%>/AppDownloadDocs?docName=<%=listings.getDocumentName()%>&appId=<%=appId%>&docId=<%=listings.getAppEngEstId()%>&docSize=<%= listings.getDocSize()%>&funName=download" ><img src="../resources/images/Dashboard/Download.png" /></a>
                                       
                                        </td>
                                </tr>
                                <%
                                    i++;
                                  } }else {
                                %>
                                <td colspan="4">No Data Found</td>
                                <%  } %>
                            </table>
    </body>
</html>
