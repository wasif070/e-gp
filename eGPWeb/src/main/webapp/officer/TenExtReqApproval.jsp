<%--
    Document   : TenExtReqApproval
    Created on : Nov 29, 2010, 3:21:34 PM
    Author     : Rajesh Singh
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender validity extension request</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/home.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>

        <script type="text/javascript">

            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });


                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            //Onclick Event
            function Validate(){
                var temp='';
                if (document.getElementById('txtNewProposalDate').value=='')
                {
                    $('#spannewproposal').html('<br/>Please select New Proposal Date.');
                    temp='true';
                }else
                {
                    if(CompareToForToday(document.getElementById('txtNewProposalDate').value)){
                        if(!CompareToForGreater(document.getElementById('txtNewProposalDate').value,document.getElementById('proposaldate').value))
                        {
                            $('#spannewproposal').html('<br/>New Validity Date should be greater than Old Validity Date.');
                            temp='true';
                        }
                        else
                        {
                            $('#spannewproposal').html('');
                        }
                    }
                    else
                    {
                        $('#spannewproposal').html('<br/>Please select Proposal Date greater than current date.');
                        temp='true';
                        
                    }
                }
                
                 if ($.trim(document.getElementById('txtExtension').value)=='')
                {
                    $('#spanextension').html('<br/>Please enter Extension reason');
                    temp='true';
                }
                try{
                                if($('#spanmandatory').html()=='*'){
                                    if(CompareToForToday(document.getElementById('txtNewSecurityDate').value)){
                                        if(!CompareToForGreater(document.getElementById('txtNewSecurityDate').value,document.getElementById('oldsecuritydate').value))
                                        {
                                            $('#spannewsecurity').html('<br/>New Security Date should be greater than Old Security Date.');
                                            temp='true';
                                        }
                                        else
                                        {
                                            $('#spannewsecurity').html('');
                                        }
                                    }
                                    else
                                    {
                                        $('#spannewsecurity').html('<br/>Please select Security Date greater than current date.');
                                        temp='true';
                                    }
                                }
                                
                                
                                if(document.getElementById('txtNewSecurityDate').value!=''){
                                    if(!NewSecurityDate()){
                                        temp='true';
                                    }
                                }
                            }
                            catch(e){}
                

                if(temp=='true') 
                {
                    return false;
                }
            }

            function CompareToForToday(first)
            {
                var mdy = first.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split

                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                var d = new Date();

                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());

                return Date.parse(valuedate) > Date.parse(todaydate);
            }

            //Function for CompareToForGreater
            function CompareToForGreater(value,params)
            {
               
                if(value!='' && params!=''){

                    var mdy = value.split('/')  //Date and month split
                    var mdyhr=mdy[2].split(' ');  //Year and time split
                    var mdyp = params.split('/')
                    var mdyphr=mdyp[2].split(' ');
                

                    if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                    {
                        //alert('Both Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                    }
                    else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                    {
                        //alert('Both DateTime');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], parseInt(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var mdyphrsec=mdyphr[1].split(':');

                        var datep =  new Date(mdyphr[0], parseInt(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('one Date and One DateTime');
                        var a = mdyhr[1];  //time
                        var b = mdyphr[1]; // time

                        if(a == undefined && b != undefined)
                        {
                            //alert('First Date');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                            var mdyphrsec=mdyphr[1].split(':');
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                        }
                        else
                        {
                            //alert('Second Date');
                            var mdyhrsec=mdyhr[1].split(':');
                            var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                        }
                    }
                    return Date.parse(date) > Date.parse(datep);
                }
                else
                {
                    return false;
                }
            }

            //OnBlur Event
            function NewProposalDate(){
                if (document.getElementById('txtNewProposalDate').value!='')
                {
                    $('#spannewproposal').html('');
                }
                else
                {
                    $('#spannewproposal').html('<br/>Please select New Proposal Date.');
                }
            }

            function NewSecurityDate(){
                var temp='';
                if (document.getElementById('txtNewSecurityDate').value!='')
                {
                    var mydate = document.getElementById('txtNewProposalDate').value;

                    var currdate = document.getElementById('txtNewSecurityDate').value;
                    if(daydiff(parseDate(mydate), parseDate(currdate))>= 28){
                        $('#spannewsecurity').html('');
                        temp='false';
                    }
                    else
                    {
                        $('#spannewsecurity').html('<br/>Please select New Security Date 28 days greater than Proposal Date.');
                        temp='true';
                    }
                }
                else
                {
                    $('#spannewsecurity').html('<br/>Please select New Security Date.');
                    temp='true';
                }
            
                if(temp=='true')
                {
                    return false;
                }
                else{
                    return true;
                }


            }

            function parseDate(str) {
                var mdy = str.split('/')
                return new Date(mdy[2], mdy[1], mdy[0]);
            }

            function daydiff(first, second) {
                return (second-first)/(1000*60*60*24)
            }


            function Extension(){
                if (document.getElementById('txtExtension').value!='')
                {
                    $('#spanextension').html('');
                }
                else
                {
                    $('#spanextension').html('<br/>Please enter Extension reason.');
                }
            }

        </script>
    </head>
    <body>
        <%
                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                    if (request.getParameter("btnhope") != null) {

                        String dtXml = "";
                        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");                        

                        String dt1 = request.getParameter("txtNewProposalDate");
                        String webDt2 = "";

                        String dt2 = request.getParameter("oldproposaldate");                        
                        if (dt2 != "") {
                            String[] webDtArr1 = dt2.split("/");
                            dt2 = webDtArr1[2] + "-" + webDtArr1[1] + "-" + webDtArr1[0];
                        }

                        if (dt1 != "") {
                            String[] webDtArr1 = dt1.split("/");
                            dt1 = webDtArr1[2] + "-" + webDtArr1[1] + "-" + webDtArr1[0];
                        }
                        if(request.getParameter("txtNewSecurityDate") != null){
                            webDt2 = request.getParameter("txtNewSecurityDate");
                        }
                        if (webDt2 != "") {
                            String[] webDtArr2 = webDt2.split("/");
                            webDt2 = webDtArr2[2] + "-" + webDtArr2[1] + "-" + webDtArr2[0];
                        }

                        String table = "", updateString = "", whereCondition = "";
                        table = "tbl_TenderValidityExtDate ";
                        if (webDt2.equalsIgnoreCase("")) {
                            updateString = " newPropValDt='" + dt1 + "', extReason='" + handleSpecialChar.handleSpecialChar(request.getParameter("txtExtension")) + "',extStatus='" + request.getParameter("select2") + "'";
                        }else{
                            // Added By Dohatec to resolve Online Tracking ID 9113 Start
                            java.text.SimpleDateFormat format1 = new java.text.SimpleDateFormat("yyyy-MM-dd");
                            String lastValAcceptDtStr = format.format(new Date());
                            try{ Date NewPropDate = format1.parse(dt1);
                            Date OldPropDate = format1.parse(dt2);
                            Date lastValAcceptDt = new Date();
                            Calendar c = Calendar.getInstance();
                            c.setTime(new Date()); // Now use today date.
                            c.add(Calendar.DATE, 10); // Adding 10 days
                            lastValAcceptDt =  c.getTime();
                         if (lastValAcceptDt.after(OldPropDate)) {
                             lastValAcceptDtStr = format1.format(OldPropDate);
                         }
                         else {
                             lastValAcceptDtStr = format1.format(lastValAcceptDt);
                         }
                            }
                            catch (Exception e){
                                System.out.println(e.toString());                            }
                      
                       
                            updateString = " newPropValDt='" + dt1 + "',tenderSecNewDt='" + webDt2 + "', extReason='" + handleSpecialChar.handleSpecialChar(request.getParameter("txtExtension")) + "',extStatus='" + request.getParameter("select2") + "'"+ ",lastValAcceptDt='" + lastValAcceptDtStr + "'";
                        }
                        // Added By Dohatec to resolve Online Tracking ID 9113 End
                        whereCondition = " valExtDtId=" + request.getParameter("ExtId");

                        //out.println("update "+table+" set " +updateString+ " where "+whereCondition);
                        String userid = "";
                        HttpSession hs = request.getSession();
                        if (hs.getAttribute("userId") != null) {
                            userid = hs.getAttribute("userId").toString();
                        }
                        Object objUser = null;
                        if (session.getAttribute("userName") != null) {
                            objUser = session.getAttribute("userName");
                        }
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        commonXMLSPService.setAuditTrail(objAuditTrail);
                        commonXMLSPService.setModuleName(EgpModule.Evaluation.getName());
                        
                        String auditValues[]=new String[4];
                        // Extenditon Id of tender
                        auditValues[0]="tenderId";
                        auditValues[1]= request.getParameter("tenderId");
                        auditValues[2]=request.getParameter("select2")+" Validity Extension Request";
                        auditValues[3]= request.getParameter("txtExtension");
                        
                        CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderValidityExtDate", updateString, whereCondition,auditValues).get(0);
                        
                        TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        //out.println(commonMsgChk.getMsg());
                        if (commonMsgChk.getFlag() == true) {
                            //Fire mail to bidders and TEC member regarding status of Extension.
                            if (request.getParameter("select2").equals("Approved")) {

                                //Insert All bidder Enteries in table tbl_TenderValAcceptance

                                for (SPTenderCommonData sptcd1 : tenderCommonService1.returndata("GetAllBidderForExt", request.getParameter("tenderId"), userid)) {

                                    //Send Mail to all bidders regarding Accepted.
                                    String[] tomailid = {sptcd1.getFieldName2()};
                                    String mailText = "";
                                    String frommailid = XMLReader.getMessage("emailIdNoReply");
                                    SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                    MailContentUtility mailContentUtility = new MailContentUtility();
                                    String peoffice = tenderCommonService1.getPEOfficeName(request.getParameter("tenderId"));
                                    for (SPTenderCommonData sptcd : tenderCommonService1.returndata("tenderinfobar", request.getParameter("tenderId"), null)) {
                                        mailText = mailContentUtility.ValidationAppRequestToBiddertoEmail(Integer.parseInt(sptcd.getFieldName1()), sptcd.getFieldName2(), objUser.toString(), peoffice);

                                        sendMessageUtil.setEmailTo(tomailid);
                                        sendMessageUtil.setSendBy(frommailid);
                                        sendMessageUtil.setEmailSub("Request for Validity/Security Extension");
                                        sendMessageUtil.setEmailMessage(mailText);
                                        userRegisterService.contentAdmMsgBox(tomailid[0], frommailid, HandleSpecialChar.handleSpecialChar("Request for Validity/Security Extension."), mailContentUtility.ValidationAppRequestToBidder(Integer.parseInt(sptcd.getFieldName1()), sptcd.getFieldName2(), objUser.toString(), peoffice));
                                        sendMessageUtil.sendEmail();
                                    }
                                }
                            }

                            response.sendRedirect("TenderExtReqListing.jsp");
                        }
                    }
        %>

        <!--Dashboard Header Start-->
        <div class="topHeader">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
        </div>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Validity Extension Request
                <span class="c-alignment-right"><a href="TenderExtReqListing.jsp"
                                                   class="action-button-goback">Go Back to Dashboard</a></span>
            </div>
            <div class="mainDiv">
                <%
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
            </div>

            <form id="frmhope" action="TenExtReqApproval.jsp?tenderId=<%=request.getParameter("tenderId")%>&ExtId=<%=request.getParameter("ExtId")%>" method="post">
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <%
                                List<SPTenderCommonData> list = tenderCommonService.returndata("GetValidityExtDetail", request.getParameter("ExtId"), null);
                                if (!list.isEmpty()) {
                    %>
                    <tr>
                        <td width="21%" class="t-align-left ff">Tender Validity in no. of Days : </td>
                        <td width="79%" class="t-align-left"><%=list.get(0).getFieldName1()%></td>
                    </tr>

                    <tr>
                        <td class="t-align-left ff">Last Date  of Tender Validity : </td>                        
                        <td class="t-align-left"><%=list.get(0).getFieldName2()%><INPUT id="proposaldate" type="hidden" name="proposaldate" value="<%
                                                            SimpleDateFormat form = new SimpleDateFormat("dd-MMM-yyyy");
                                                            Date d = new Date();
                                                            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                                                            d = form.parse(list.get(0).getFieldName2());
                                                            out.print(format.format(d));

                                                                                        %>"/>
                        <INPUT id="oldproposaldate" type="hidden" name="oldproposaldate" value="<%
                        d = form.parse(list.get(0).getFieldName2());
                        out.print(format.format(d)); %>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">New  Date of Tender Validity : <span class="mandatory">*</span></td>
                        <td class="t-align-left"><input name="txtNewProposalDate" type="text" class="formTxtBox_1" id="txtNewProposalDate" style="width:70px;" readonly="true" onclick="GetCal('txtNewProposalDate','txtNewProposalDate');" onblur="NewProposalDate();" value="<%

                                                            d = form.parse(list.get(0).getFieldName3());

                                                            out.print(format.format(d));
                                                        %>"/>
                            <a href="javascript:void(0);"  title="Calender"><img id="txtNewProposalDateImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('txtNewProposalDate','txtNewProposalDateImg');"/></a>
                            <span id="spannewproposal" class="mandatory"></span>
                        </td>
                    </tr>
                    <%
                        TenderSrBean tenderSrBeanForValSec = new TenderSrBean();
                        List<Object[]> objForChkingValSec = tenderSrBeanForValSec.getConfiForTender(Integer.parseInt(request.getParameter("tenderId")));
                        if(!objForChkingValSec.isEmpty() && objForChkingValSec.get(0)[2].toString().equalsIgnoreCase("yes")){
                        %>
                    <tr>
                        <td class="t-align-left ff">Last Date of Tender Security Validity : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName4()%>
                            <INPUT id="oldsecuritydate" type="hidden" name="oldsecuritydate" value="<%
                                                                d = form.parse(list.get(0).getFieldName4());
                                                                out.print(format.format(d));
                                   %>"/>
                            <INPUT id="securitydate" type="hidden" name="securitydate" value="<%=list.get(0).getFieldName2()%>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">New Date of Tender Security Validity : <span class="mandatory">*</span></td>
                        <td class="t-align-left"><input name="txtNewSecurityDate" type="text" class="formTxtBox_1" id="txtNewSecurityDate" style="width:70px;" readonly="true" onclick="GetCal('txtNewSecurityDate','txtNewSecurityDate');" onblur="NewSecurityDate();" value="<%
                                                            d = form.parse(list.get(0).getFieldName5());
                                                            out.print(format.format(d));
                                                        %>"/>
                            <a href="javascript:void(0);"  title="Calender"><img id="txtNewSecurityDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('txtNewSecurityDate','txtNewSecurityDateimg');"/></a>
                            <span id="spannewsecurity" class="mandatory"></span>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td class="t-align-left ff">Extension Reason : <span class="mandatory">*</span> </td>
                        <td class="t-align-left">
                            <textarea cols="100" rows="5" id="txtExtension" name="txtExtension" class="formTxtBox_1" onblur="Extension();"><%=list.get(0).getFieldName8()%></textarea>
                            <span id="spanextension" class="mandatory"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Action By : </td>
                        <td class="t-align-left">
                            <%=list.get(0).getFieldName7()%>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Action : <span class="mandatory">*</span></td>
                        <td class="t-align-left">
                            <select name="select2" class="formTxtBox_1" id="select2" style="width:100px;">
                                <option value="Approved">Approved</option>
                                <option value="Reject">Reject</option>
                            </select>

                        </td>
                    </tr>
                    <%}%>
                </table>
                <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input name="btnhope" id="btnhope" type="submit" value="Submit" onclick="return Validate();"/>
                    </label>
                </div>
            </form>
            <div>&nbsp;</div>
        </div>
        <!--Dashboard Content Part End-->
        <!--Dashboard Footer Start-->
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        <!--Dashboard Footer End-->
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
