<%--
    Document   : ViewQueAns
    Created on : Dec 31, 2010, 3:21:10 PM
    Author     : Nishith
--%>

<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData,com.cptu.egp.eps.web.servicebean.EvalClariPostBidderSrBean"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Question and Answer</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmViewQueAns").validate({
                    rules: {
                        evalNonCompRemarks: {required:true,maxlength:1000 }
                    },
                    messages: {
                        evalNonCompRemarks:{required:"<div class='reqF_1'>Please Enter Reason</div>",
                            maxlength:"<div class='reqF_1'>Maximum 1000 characters are allowed</div>"
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#frmViewQueAns').submit(function() {
                    if($('#frmViewQueAns').valid()){
                        $('#btnPost').attr('disabled',true);
                    }else{
                        $('#btnPost').removeAttr('disabled');
                    }
                });
            });
            function comapreMarks(){
                if(document.getElementById("evalMark").value!=null && document.getElementById("evalMark").value!=""){
                    var totalMrk = document.getElementById('totalMark').value;
                    var inputMrk = document.getElementById('evalMark').value;
                    var isError = true;
                    
                    isError=number(inputMrk);
                    if(!isError){
                        document.getElementById('errMrk').innerHTML="<br/>Only Numbers are Allowed.";
                        return false;
                    }
                    totalMrk
                    if(eval(inputMrk) > eval(totalMrk)){
                        document.getElementById('errMrk').innerHTML="<br/>Please Enter Mark Less then Total Mark.";
                        isError =  false;
                    }
                    else{
                        isError =  true;
                    }
                }else{
                    document.getElementById('errMrk').innerHTML="<br/>Please Enter Mark.";
                    isError = false;
                }
                if(isError){
                    return true;
                }else{
                    return false;
                }
            }
            function number(value) {
                return /^([0-9]+)$/.test(value);
            }
        </script>

    </head>
    <body>
        <div class="dashboard_div">

            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <%
                        boolean bol_isMark = false;
                        String tenderId;
                        tenderId = request.getParameter("tenderId");
                        String formId = "";
                        String userId = "", uId = "", st = "";
                        String lotId = "";
                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                            userId = session.getAttribute("userId").toString();
                        }
                        if (request.getParameter("uid") != null && !"".equalsIgnoreCase(request.getParameter("uId"))) {
                            uId = request.getParameter("uid");
                        }
                        
                        if (request.getParameter("formId") != null && !"".equalsIgnoreCase(request.getParameter("formId"))) {
                            formId = request.getParameter("formId");
                        }
                        if (request.getParameter("st") != null && !"".equalsIgnoreCase(request.getParameter("st"))) {
                            st = request.getParameter("st");
                        }
                        if(request.getParameter("pkgLotId") != null){
                                    lotId = request.getParameter("pkgLotId");
                            }


            %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">View Question and Answer<span style="float:right;"><a href="SeekEvalClari.jsp?tenderId=<%=tenderId%>&st=<%=st%>&uId=<%=uId%>&lnk=et" class="action-button-goback">Go Back</a></span></div>
                <div class="t_space">
                    <%
                                if (request.getParameter("err") != null) {
                                    String errorMsg = request.getParameter("err");
                                    
                                    if (errorMsg.equals("4")) {
                    %>
                    <div class="responseMsg errorMsg">Please Select Evaluation Status.</div>
                    <%}
                                                        if (errorMsg.equals("5")) {%>
                    <div class="responseMsg errorMsg">Please Enter Reason for Non Complied.</div>

                    <%                            }
                                                        if (errorMsg.equals("0")) {%>
                    <div class="responseMsg errorMsg">Your Answer is Not Inserted..</div>
                    <% }
                                }
                    %>
                    <%
                                pageContext.setAttribute("tenderId", tenderId);
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <%
                                boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                    %>
                    
                   
                </div>

                <div class="t_space">
                    <%//if (!"TEC".equalsIgnoreCase(request.getParameter("comType"))) {%>
                    <jsp:include page="officerTabPanel.jsp" >
                        <jsp:param name="tab" value="7" />
                    </jsp:include>
                    <% //}%>
                </div>
                <div class="tabPanelArea_1">
                    <%
                                pageContext.setAttribute("TSCtab", "3");
                                int cnt = 0;
                    %>
                    <%@include  file="../resources/common/AfterLoginTSC.jsp"%>
<%
  // Get Tender Procurement Nature info
TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
String strProcurementNature = "";
                        List<SPTenderCommonData> lstProcurementNature =
                                tenderCS.returndata("getTenderProcurementNature", tenderId, null);
                        if (!lstProcurementNature.isEmpty()) {
                            strProcurementNature = lstProcurementNature.get(0).getFieldName1();
                        }
                        lstProcurementNature = null;
%>
                    <div class="tabPanelArea_1">
                    <div class="tabPanelArea_1" style="border: none; padding: 0px;">
                         

                        <% for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", uId, "0")) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th colspan="2" class="t_align_left ff">Company Details</th>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Company Name :</td>
                                <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                            </tr>
                        </table>
                            <%@include  file="../officer/TSCComments.jsp" %>

                        <%
                                    } // END FOR LOOP OF COMPANY NAME
                        %>
                        <form id="frmViewQueAns" action="<%=request.getContextPath()%>/ViewQueAnsServlet" name="frmViewQueAns" method="POST">
                            <input type="hidden" name="tenderId" value="<%=tenderId%>"/>
                            <input type="hidden" name="userId" value="<%=uId%>"/>
                            <input type="hidden" name="st" value="<%=st%>"/>
                            <input type="hidden" name="formId" value="<%=formId%>"/>
                            <%
                                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                        /*for (SPCommonSearchData question : commonSearchService.searchData("GetEvalComMemQuestion", tenderId,
                                        formNames.getFieldName1(),
                                        sptcd.getFieldName2(), null, null, null, null, null, null)) {

                                        }*/
                                        if (isTenPackageWis) {
                                            for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th colspan="2" class="t_align_left ff">Tender Details</th>
                                </tr>

                                <tr>
                                    <td width="22%" class="t-align-left ff">Package No. :</td>
                                    <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Package Description :</td>
                                    <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                                </tr>
                            </table>
                                    <%}
                                        } // END packageList FOR LOOP
                                        else {

                                            for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                                    %>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th colspan="2" class="t_align_left ff">Tender Details</th>
                                        </tr>
                                    
                                        <tr>
                                            <td width="22%" class="t-align-left ff">Package No. :</td>
                                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left ff">Package Description :</td>
                                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                                        </tr>
                                    </table>

                                    <%}

                                           for (SPCommonSearchData lotList : commonSearchService.searchData("GetBidderPkgLots", tenderId, "Lot", uId, lotId, null, null, null, null, null)) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">

                                <tr>
                                    <td width="22%" class="t-align-left ff">Lot No. :</td>
                                    <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Lot Description :</td>
                                    <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                                </tr>
                            </table>
                            <%}//End Loop for LotWise
                                        }              // START FOR LOOP OF FORM NAME
                                        int formCount = 0;
                                        for (SPCommonSearchData formNames : commonSearchService.searchData("GetEvalFormByFormId", tenderId, strProcurementNature, null, uId, formId, null, null, null, null)) {
                                            formCount++;
                                            

                            %>

                            <%
                            int cntQuest = 0;
                            int memnameCnt = 0;
                            // START FOR LOOP OF TEC / TSC MEMBER NAME BY FORM-ID AND TENDER-ID.
                            //for (SPCommonSearchData sptcd : commonSearchService.searchData("GetEvalComMem", tenderId, formId, null, null, null, null, null, null, null)) {
                            for (SPCommonSearchData sptcd : commonSearchService.searchData("GetEvalComMemForBidderClari", tenderId, formId, userId, null, null, null, null, null, null)) {
                                memnameCnt++;
                            %>
                            <table width="100%" cellspacing="0" class="tableList_k t_space">
                                <tr>
                                    <th colspan="3" class="t-align-left">TEC / TSC Member Name : &nbsp;<%=sptcd.getFieldName3()%>
                                    </th>
                                </tr>
                                <tr>
                                    <th width="4%" class="t-align-center">Sl. No.</th>
                                    <th width="48%" class="t-align-center">Questions</th>
                                    <th width="48%" class="t-align-center">Answer</th>

                                </tr>
                                <%

                                    // START FOR LOOP OF QUESTION POSTED BY TEC / TSC MEMBERS.
                                    for (SPCommonSearchData question : commonSearchService.searchData("GetEvalComMemQuestionByMem", tenderId,
                                                formId,
                                                sptcd.getFieldName2(), uId, null, null, null, null, null)) {
                                        cntQuest++;
                                        // HIGHLIGHT THE ALTERNATE ROW
                                        if(Math.IEEEremainder(cntQuest,2)==0) {
                                %>
                                <tr class="bgColor-Green">
                                <% } else { %>
                                <tr>
                                <% } 
                                    // For fixing #1210 on 13/Nov/2013
                                    String strQuestion = question.getFieldName3();
                                    try{
                                        strQuestion = URLDecoder.decode(question.getFieldName3(),"UTF-8");
                                    }catch(Exception e) {
                                        System.out.print("Error while decoading ");
                                        e.printStackTrace();
                                        strQuestion = question.getFieldName3();
                                    }
                                    %>
                                    <td class="t-align-left"><%=cntQuest%></td>

                                    <td  class="t-align-left"><%=strQuestion%></td>

                                    <%if (!question.getFieldName4().equals("")) {%>
                                    <td  class="t-align-left">
                                        <%//=URLDecoder.decode(question.getFieldName4(),"UTF-8")%>
                                        <%=question.getFieldName4()%></td>
                                    <%} else {%>
                                    <td  class="t-align-left">
                                        <%//=URLDecoder.decode(question.getFieldName4(),"UTF-8")%>
                                        <%=question.getFieldName4()%>
                                    </td>
                                    <%}%>
                                </tr>
                                <%

                                    } // FOR Loop Question Ends Here

                                    if (cntQuest == 0) {
                                %>
                                <tr>
                                    <td colspan="3"  class="t-align-center">No queries posted yet</td>
                                </tr>
                                <%}%>
                            </table>
                            <%      }//Member Name Loop Ends Here..
                                                                        if (memnameCnt == 0) {%>
                          <table class="t_space tableList_k" width="100%">
                             <tr>
                                <td width="85%" class="t-align-left">No member has posted any queries yet</td>
                             </tr>
                          </table>
                            <%}//For Update Perpose.........
                                                                        boolean isUpdate = false;
                                                                        for (SPCommonSearchData isUpdateloop : commonSearchService.searchData("getEvalMemberStatus", tenderId, uId, formId, userId, null, null, null, null, null)) {
                                                                            if (!isUpdateloop.getFieldName1().equals("")) {
                                                                                isUpdate = true;
                                                                            }
                                                                            break;
                                                                        }
                            %>
                            <%if (isUpdate) {
                                                                            String type = "update";
                            %>



                            <%
                            for (SPCommonSearchData isUpdateloop : commonSearchService.searchData("getEvalMemberStatus", tenderId, uId, formId, userId, null, null, null, null, null)) {
    %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td width="10%" valign="top" class="t-align-left ff">Evaluation Status :</td>
                                    
                                         <td width="86%" class="t-align-left">
                                   <%           EvalClariPostBidderSrBean evalClariPostBidderSrBean = new EvalClariPostBidderSrBean();
                                                boolean isCheck = true;
                                              //  isCheck = evalClariPostBidderSrBean.isChecked(tenderId, userId);
                                                
                                                String methodType = "abc";
                                                String bidderStatus = "abc";
                                                bidderStatus = isUpdateloop.getFieldName1();
                                              // for (SPCommonSearchData bidStatus : commonSearchService.searchData("getBidderStatus", tenderId, userId, null, null, null, null, null, null, null)) {
                                              //           bidderStatus= bidStatus.getFieldName1();
                                              //    }
                                               for (SPCommonSearchData evalList : commonSearchService.searchData("getProcurementMethod", tenderId, null, null, null, null, null, null, null, null)) {

                                                         methodType = evalList.getFieldName1();
                                                        // out.println(methodType);

                                                        %><%    if (methodType.equalsIgnoreCase("OTM") || methodType.equalsIgnoreCase("LTM")) {
                                                 if(!isCheck){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify"/> Accept  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Reject
                                    <% } else{
                                        if(bidderStatus.equalsIgnoreCase("Accepted")){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked/> Accept  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Reject
                                    <% } else if(bidderStatus.equalsIgnoreCase("Rejected")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify"/> Accept  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail" checked/> Reject


                                    <%       }          }       } else if (methodType.equalsIgnoreCase("TSTM")) {
                                                if(!isCheck){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify"  id="techQualify"/> Responsive &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Unresponsive
                                    <% } else{
                                        if(bidderStatus.equalsIgnoreCase("Technically Qualified")){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify"  id="techQualify" checked/> Responsive &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Unresponsive
                                    <% } else if(bidderStatus.equalsIgnoreCase("Technically Not Qualified")){ %>
                                    <input name="techQualify" type="radio" value="techQualify"  id="techQualify"/> Responsive &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail" checked/> Unresponsive


                                    <%                  }         }     } else if (methodType.equalsIgnoreCase("REOI")) {
                                        if(!isCheck){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Excellent &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Very Good &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Good & Poor
                                    <% } else{
                                        if(bidderStatus.equalsIgnoreCase("Excellent")){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked/> Excellent &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Very Good &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Good & Poor
                                    <% } else if(bidderStatus.equalsIgnoreCase("Very Good")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Excellent &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail" checked/> Very Good &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Good & Poor
                                    <% } else if(bidderStatus.equalsIgnoreCase("Good & Poor")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Excellent &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Very Good &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2" checked/> Good & Poor

                                    <%                        }      }  } else if (methodType.equalsIgnoreCase("PQ")) {
                                        if(!isCheck){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Conditionally Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Disqualified
                                    <% } else{
                                        if(bidderStatus.equalsIgnoreCase("Qualified")){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked /> Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Conditionally Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Disqualified
                                    <% } else if(bidderStatus.equalsIgnoreCase("Conditionally Qualified")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail" checked/> Conditionally Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Disqualified
                                    <% } else if(bidderStatus.equalsIgnoreCase("Disqualified")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Conditionally Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2" checked/> Disqualified


                                    <%                   }     }    } else {
                                           if(!isCheck){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Technically Qualified  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Technically NOT Qualified
                                    <% } else{
                                        if(bidderStatus.equalsIgnoreCase("Technically Qualified")||bidderStatus.equalsIgnoreCase("complied")){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked/> Technically Qualified  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Technically NOT Qualified
                                    <% } else if(bidderStatus.equalsIgnoreCase("Technically NOT Qualified")||bidderStatus.equalsIgnoreCase("noncomplied")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Technically Qualified  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail" checked/> Technically NOT Qualified


                                    <%} }  }%>
                                    <input name="methodType" type="hidden" value="<%=methodType%>" />
                                    <%      }

                                    %>
            
                                </td>
                                </tr>
                                <tr>

                                    <td valign="top" class="t-align-left ff">Reason : <span class="mandatory">*</span></td>
                                    <td class="t-align-left">
                                        <textarea rows="5" id="evalNonCompRemarks" name="evalNonCompRemarks" class="formTxtBox_1" style="width:99%"><%= URLDecoder.decode(isUpdateloop.getFieldName4(), "UTF-8")%></textarea>
                                        <input type="hidden" name="type" value="<%=type%>"/>
                                        <input type="hidden" name="evalStatus" value="<%=isUpdateloop.getFieldName2()%>">
                                    </td>
                                </tr>
                            </table>
                            <%                                          //For Update Mark...........
                                for (SPCommonSearchData totalMark : commonSearchService.searchData("checkEvalServiceFrom", formId, uId, null, null, null, null, null, null, null)) {
                                    if (totalMark != null) {
                                        if (!totalMark.getFieldName1().equals("")) {

                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td width="10%" valign="top" class="t-align-left ff">Eval Marks :</td>
                                    <td width="10%" valign="top">
                                        <label><%=totalMark.getFieldName1()%></label>
                                        <input type="hidden" id="totalMark" value="<%=totalMark.getFieldName1()%>">
                                    </td>

                                    <td width="80%" class="t-align-left">
                                        <input type="text" id="evalMark" name="evalMark" class="formTxtBox_1" style="width:100px;" value="<%=isUpdateloop.getFieldName5()%>"/><span class="reqF_1" id="errMrk"></span>

                                    </td>
                                </tr>
                            </table>
                            <%}//totalMark contains Value....
                                    }//TotalMark !=null Condition Ends here
                                }//Total Marks Loop Ends here
                            %>
                            <%}//isUpdateLoop complets....

                            %>
                            <%} else//For Insert Pruposeeeeeeeeeeeeeeeee Start Here
                            {%>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td width="10%" valign="top" class="t-align-left ff">Evaluation Status :</td>
                                    <!--td width="90%" class="t-align-left">
                                        <input type="radio" name="Complied" id="CompliedY" value="Yes" value="Complied" checked="cheked" />
                                        Complied
                                        &nbsp;&nbsp;
                                        <input type="radio" name="Complied" id="CompliedN" value="No"/>
                                        Non Complied
                                    </td-->
                                     <td width="86%" class="t-align-left">
                                   <%           EvalClariPostBidderSrBean evalClariPostBidderSrBean = new EvalClariPostBidderSrBean();
                                                boolean isCheck = false;
                                                //isCheck = evalClariPostBidderSrBean.isChecked(tenderId, userId);
                                                String methodType = "abc";
                                                String bidderStatus = "abc";
                                              // for (SPCommonSearchData bidStatus : commonSearchService.searchData("getBidderStatus", tenderId, userId, null, null, null, null, null, null, null)) {
                                              //           bidderStatus= bidStatus.getFieldName1();
                                              //    }
                                               for (SPCommonSearchData evalList : commonSearchService.searchData("getProcurementMethod", tenderId, null, null, null, null, null, null, null, null)) {

                                                         methodType = evalList.getFieldName1();
                                                        // out.println(methodType);

                                                            if (methodType.equalsIgnoreCase("OTM") || methodType.equalsIgnoreCase("LTM")) {
                                                 if(!isCheck){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked/> Accept  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Reject
                                    <% } else{
                                        if(bidderStatus.equalsIgnoreCase("Accepted")){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked/> Accept  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Reject
                                    <% } else if(bidderStatus.equalsIgnoreCase("Rejected")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify"/> Accept  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail" checked/> Reject


                                    <%       }          }       } else if (methodType.equalsIgnoreCase("TSTM")) {
                                                if(!isCheck){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify"  id="techQualify" checked/> Responsive &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Unresponsive
                                    <% } else{
                                        if(bidderStatus.equalsIgnoreCase("Responsive")){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify"  id="techQualify" checked/> Responsive &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Unresponsive
                                    <% } else if(bidderStatus.equalsIgnoreCase("Unresponsive")){ %>
                                    <input name="techQualify" type="radio" value="techQualify"  id="techQualify"/> Responsive &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail" checked/> Unresponsive


                                    <%                  }         }     } else if (methodType.equalsIgnoreCase("REOI")) {
                                        if(!isCheck){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked/> Excellent &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Very Good &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Good & Poor
                                    <% } else{
                                        if(bidderStatus.equalsIgnoreCase("Excellent")){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked/> Excellent &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Very Good &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Good & Poor
                                    <% } else if(bidderStatus.equalsIgnoreCase("Very Good")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Excellent &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail" checked/> Very Good &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Good & Poor
                                    <% } else if(bidderStatus.equalsIgnoreCase("Good & Poor")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Excellent &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Very Good &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2" checked/> Good & Poor

                                    <%                        }      }  } else if (methodType.equalsIgnoreCase("PQ")) {
                                        if(!isCheck){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked/> Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Conditionally Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Disqualified
                                    <% } else{
                                        if(bidderStatus.equalsIgnoreCase("Qualified")){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked /> Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Conditionally Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Disqualified
                                    <% } else if(bidderStatus.equalsIgnoreCase("Conditionally Qualified")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail" checked/> Conditionally Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2"/> Disqualified
                                    <% } else if(bidderStatus.equalsIgnoreCase("Disqualified")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" /> Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Conditionally Qualified &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail2" id="techFail2" checked/> Disqualified


                                    <%                   }     }    } else {
                                           if(!isCheck){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked/> Technically Qualified  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Technically NOT Qualified
                                    <% } else{
                                        if(bidderStatus.equalsIgnoreCase("Technically Qualified")){
                                    %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked/> Technically Qualified  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail"/> Technically NOT Qualified
                                    <% } else if(bidderStatus.equalsIgnoreCase("Technically NOT Qualified")){ %>
                                    <input name="techQualify" type="radio" value="techQualify" id="techQualify" checked/> Technically Qualified  &nbsp;&nbsp;
                                    <input name="techQualify" type="radio" value="techFail" id="techFail" checked/> Technically NOT Qualified


                                    <%} }  }%>
                                    <input name="methodType" type="hidden" value="<%=methodType%>" />
                                    <%      }

                                    %>
                                    <span  name="spantechQualFail" id="sptechQualFail" style="color: Red;"  width="100%"></span>
                                </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="t-align-left ff">Reason : <span class="mandatory">*</span></td>
                                    <td class="t-align-left">
                                        <textarea rows="5" id="evalNonCompRemarks" name="evalNonCompRemarks" class="formTxtBox_1" style="width:99%"></textarea>

                                    </td>

                                </tr>
                            </table>
                            <%                                          //For Insert Mark...........

                                                            for (SPCommonSearchData totalMark : commonSearchService.searchData("checkEvalServiceFrom", formId, uId, null, null, null, null, null, null, null)) {
                                                                bol_isMark = true;
                                                                if (totalMark != null) {
                                                                    if (!totalMark.getFieldName1().equals("")) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td width="10%" valign="top" class="t-align-left ff">Eval Marks :</td>
                                    <td width="10%" valign="top">
                                        <label><%=totalMark.getFieldName1()%></label>
                                        <input type="hidden" id="totalMark" value="<%=totalMark.getFieldName1()%>">
                                    </td>
                                    <td width="80%" class="t-align-left">
                                        <input type="text" id="evalMark" name="evalMark" class="formTxtBox_1" style="width:100px;" /><span class="reqF_1" id="errMrk"></span>

                                    </td>
                                </tr>
                            </table>
                            <%             }//Mark is not null
                                                    }//if not null
                                                }//conditionEnds here
                                            }//loop ends for totalMarks
                                        }  // END formNames FOR LOOP
                                        if (formCount == 0) {
                            %>
                            <div class="tabPanelArea_1 t-align-center">
                                No Records Found
                            </div>
                            <%}//if formCounter = 0 then this condition execute.

                            %>
                            <div class="t-align-center t_space">
                                <%if (formCount != 0) {%>
                                <label class="formBtn_1">
                                    <input name="btnPost" id="btnPost" type="submit" value="Submit" <%if(bol_isMark){%>onclick="return comapreMarks();"<%}%>/></label>
                                    <%}//If No Recordds Found Sumbmit Button is Not Visible
                                    %>
                            </div>
                        </form>
                    </div>
                </div>
                    </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <!--    <script type="text/javascript">
            function HideandShow(){

                                    if($('#CompliedN').val){
                    //document.getElementById("nonCompiled_"+val).style.display='block';
                    $('#nonCompiled').show();
                }else{
                    //document.getElementById("nonCompiled_"+val).style.display='none';
                    $('#nonCompiled').hide();
                }

            }
        </script>-->
    <!--    <script type="text/javascript">
            function validate(){
                var f =document.frmViewQueAns;
                var isError = false;
                var isError1 = false;
                alert(document.getElementById('totalMark').value);

                //                if(document.getElementById().checked){
                //                    if(document.getElementById(remarkName).value==""){
                //                        document.getElementById(message).innerHTML="<br/>Please Enter Reson For Non Complied";
                //                        isError = false;
                //                    }else{
                //                        if(document.getElementById(remarkName).value.length>1000){
                //                            document.getElementById(message).innerHTML="<br/>Please Enter Less Then 1000 Character";
                //                            isError = false;
                //                        }
                //                    }
                if(document.getElementById('evalNonCompRemarks').value=="" || document.getElementById('evalNonCompRemarks').value==null){
                    document.getElementById('compRemark').innerHTML="<br/>Please Enter Reson";
                    isError = false;

                }else{
                    if(document.getElementById('evalNonCompRemarks').value.length>1000){
                        document.getElementById('compRemark').innerHTML="<br/>Maixmum 1000 charcter Allowed.";
                        isError1 = false
                    }else{
                        isError1 = true;
                    }

                }
                if(isError1){
                    return true;
                }else{
                    return false;
                }
                if(document.getElementById('evalMark').value=="" || document.getElementById('evalMark').value==null){
                    document.getElementById('compMark').innerHTML="<br/>Please Enter Mark";
                    isError = false;
                }else{
                     var totalMark = document.getElementById('totalMark').value;
                var actualMark = document.getElementById('evalMark').value;
                   alert(totalMark+'::'+actualMark)
                    if(totalMark > actualMark){
                        document.getElementById('compMark').innerHTML="<br/>Please Enter Mark Less then Total Mark";
                        isError = false;
                    }else{
                        isError = true;
                    }
                }

                if(isError){
                    return true;
                }else{
                    return false;
                }
            }
        </script>-->
</html>
