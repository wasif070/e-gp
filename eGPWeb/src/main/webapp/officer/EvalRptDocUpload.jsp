<%-- 
    Document   : EvalRptDocUpload
    Created on : Jun 21, 2011, 1:57:51 PM
    Author     : nishit
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalReportDocs"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalReportClarification"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="java.text.SimpleDateFormat" %>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    int userId = Integer.parseInt(session.getAttribute("userId").toString());
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Eval Report Documents</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:100}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please Select Document.</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please Enter Description.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                    }
                });
            });
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#button').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>
        
    </head>
    <body onunload="getData();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->


            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
                        String s_lotId= "0";
                        String s_tenderId = "0";
                        String s_rId = "0";

                        if (request.getParameter("lotId") != null) {
                            s_lotId = request.getParameter("lotId");
                        }
                        if (request.getParameter("tenderid") != null) {
                            s_tenderId = request.getParameter("tenderid");
                        }
                        if (request.getParameter("rId") != null) {
                            s_rId = request.getParameter("rId");
                        }


            %>
            <%--<%@include file="../resources/common/AfterLoginTop.jsp" %>--%>
            <div class="contentArea_1">
                <div class="pageHead_1">Evaluation Report Documents<%--<span style="float:right;"><a href="SendReportAA.jsp?tenderid=<%=s_tenderId %>&lotId=<%=s_lotId%>&rId=<%=s_rId%>" class="action-button-goback">Go Back</a></span>--%></div>
                <%-- <div class="t_space" align="right"><a href="Notice.jsp?tenderid=<%=postQualId%>" class="action-button-goback">Go Back Dashboard</a></div>--%>
                <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/EvalRptDocsServlet" enctype="multipart/form-data" name="frmUploadDoc">

                    <input type="hidden" name="lotId" value="<%=s_lotId%>"/>
                    <input type="hidden" name="tenderid" value="<%=s_tenderId%>"/>
                    <input type="hidden" name="roundId" value="<%=s_rId%>"/>

                    <div class="t_space">
                    <%
                                if (request.getParameter("fq") != null) {
                    %>

                    <div class="responseMsg errorMsg t_space"><%=request.getParameter("fq")%></div>
                    <%
                                }
                                if (request.getParameter("fs") != null) {
                    %>

                    <div class="responseMsg errorMsg t_space t_space">

                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed
                    </div>
                    <%
                                }
                    %>
                    <%
                    if (request.getParameter("msg") != null) {
                        if("re".equalsIgnoreCase(request.getParameter("msg"))){
                    %>
                    <div class="responseMsg successMsg t_space t_space">
                        Document Removed successfully
                    </div>
                    <%}if("reerror".equalsIgnoreCase(request.getParameter("msg")) || "uperror".equalsIgnoreCase(request.getParameter("msg"))){%>
                    <div class="responseMsg errorMsg t_space t_space">
                        Problem while processing the Query
                    </div>
                    <%}if("up".equalsIgnoreCase(request.getParameter("msg"))){%>
                    <div class="responseMsg successMsg t_space t_space">
                        Document Uploaded successfully
                    </div>
                    <%}%>
                    <%
                                }
                    %>
                     <%if (request.getParameter("msg") != null && "error".equalsIgnoreCase(request.getParameter("msg"))) {%>
                    <div class="responseMsg errorMsg" style="margin-top: 10px;">There is problem while requesting your query. Please try again</div>
                    <%}pageContext.setAttribute("tenderId", s_tenderId);%>
                    </div>
                    <div class="t_space">
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    </div>
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>

                            <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>
                        <tr>
                            <td width="10%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                            <td width="90%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Description : <span>*</span></td>
                            <td>
                                <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                <div id="dvDescpErMsg" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>

                                <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" /></label>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");%>
                            <td class="t-align-left">
                                Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
                </form>

                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="26%">File Name</th>
                        <th class="t-align-center" width="55%">File Description</th>
                        <th class="t-align-center" width="5%">File Size <br />
                            (in Kb)</th>
                        <th class="t-align-center" width="10%">Action</th>
                    </tr>
                    <%

                            EvaluationService evalService = (EvaluationService)AppContext.getSpringBean("EvaluationService");
                            List<TblEvalReportDocs> list_disp = new ArrayList<TblEvalReportDocs>();
                            list_disp=evalService.getListOfDocs(Integer.parseInt(s_tenderId),Integer.parseInt(s_lotId),Integer.parseInt(s_rId));
                            int i=1;
                            if(list_disp!=null && !list_disp.isEmpty()){
                                for(TblEvalReportDocs tblEvalReportDocs : list_disp){
                            
                               
                    %>
                    <tr>
                        <td class="t-align-center"><%out.print(i++);%></td>
                        <td class="t-align-left"><%=tblEvalReportDocs.getDocumentName()%></td>
                        <td class="t-align-left"><%=tblEvalReportDocs.getDocDescription()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(tblEvalReportDocs.getDocSize())/1024)%></td>
                        <td class="t-align-center"><a href="<%=request.getContextPath()%>/EvalRptDocsServlet?tenderid=<%=s_tenderId%>&lotId=<%=s_lotId%>&docId=<%=tblEvalReportDocs.getEvalRptDocId()%>&docName=<%=tblEvalReportDocs.getDocumentName()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>&nbsp;&nbsp;
                            <% if(tblEvalReportDocs.getUploadedBy() == userId) { %>
                            &nbsp;&nbsp;<a href="<%=request.getContextPath()%>/EvalRptDocsServlet?tenderid=<%=s_tenderId%>&lotId=<%=s_lotId%>&docId=<%=tblEvalReportDocs.getEvalRptDocId()%>&docName=<%=tblEvalReportDocs.getDocumentName()%>&funName=Remove" title="Remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                            <% } %>
                        </td>
                    </tr>
                    <%}
                            }else{%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
                
            </div>
            <div>&nbsp;</div>
            
            <%--<%@include file="../resources/common/Bottom.jsp" %>--%>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
<script language="javascript">
    function getData()
        {
           window.opener.getDocData();
        }
</script>
</html>
