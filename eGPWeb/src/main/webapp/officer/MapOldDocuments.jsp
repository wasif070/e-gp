<%-- 
    Document   : MapOldDocuments
    Created on : Sep 22, 2014, 3:45:05 PM
    Author     : G. M. Rokibul Hasan
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<html>
    <html>
        <head>
                    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Use Existing Committee </title>
            <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
            <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
            <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
            <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
            <!--jalert -->

            <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
            <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
            <script type="text/javascript">
                $(function() {
                    $('#search').submit(function() {
                        var vbool=true;
                        if($.trim($('#txtSearchVal').val())==""){
                            $('#dis').css("display", "table-row");
                            if($('#cmbSearchBy').val()=="comName"){
                                $('#SearchValError').html('Please enter Committee Name');
                            }else{
                                $('#SearchValError').html('Please enter Tender ID');
                            }
                            vbool=false;
                        }else{
                            if($('#cmbSearchBy').val()=="tendId"){
                                if(!digits($('#txtSearchVal').val())){
                                     $('#dis').css("display", "table-row");
                                    $('#SearchValError').html('Please enter the Numerals only');
                                    vbool=false;
                        }
                            }
                        }
                        return vbool;
                    });
                });
                function digits(value) {
                    return /^\d+$/.test(value);
                }
                $(function() {
                    $('#map').submit(function() {
                        var chkCnt=0;
                        var npCnt=0;
                        var cnt=$("#tb2").children().children().length-1;
                        if(cnt>0){
                            for(var i=1;i<=cnt;i++){
                                if($("#chkMap"+i).attr("checked")){
                                    //alert($("#chkMap"+i).attr('tenderid'));
                                    //alert('< %=request.getParameter("type")%>'=='2');
                                    if(('<%=request.getParameter("type")%>'=='2') && ($("#usealltenders").val().indexOf($("#chkMap"+i).attr('tenderId'), 0)==-1)){
                                        npCnt++;
                                    }
                                    chkCnt++;
                                }
                            }
                           /* if(chkCnt==0){
                                jAlert("Please select committee.","Committe Alert", function(RetVal) {
                                });
                                return false;
                            }
                            if(npCnt!=0){
                                jAlert("Opening Committee having Common Member from Evaluation Committee not found.","Committe Alert", function(RetVal) {
                                });
                                return false;
                            }*/
                        }else{
                            jAlert("No committee found.","Committe Alert", function(RetVal) {
                            });
                            return false;
                        }
                    });
                });
                function openInfo(commId){
                    window.open('ViewMember.jsp?commId='+commId,'ViewMembers','menubar=0,scrollbars=1,width=600,height=500')
                }
            </script>
            <script type="text/javascript">
                            function validateSec(){
                if(document.getElementById('txttenderSecurityValidity')!=null){
                    if(document.getElementById('txttenderSecurityValidity').value == ''){
                        document.getElementById('sptendersecval').innerHTML ='Please enter Tender Security Validity In No. of Days';
                        vboolean = false;
                    }else if(!regForNumber(document.getElementById('txttenderSecurityValidity').value) || document.getElementById('txttenderSecurityValidity').value.indexOf('.') > 0){
                        document.getElementById('sptendersecval').innerHTML ='Please enter digits (0-9) only';
                        vboolean = false;
                    }else{
                        document.getElementById('sptendersecval').innerHTML ='';
                    }
                }
            }
            function validateDays(){
                vboolean = true;
                //alert(document.getElementById("hdnProcureNature").value);
                if(document.getElementById('txttenderValidity')!=null){
                    if(document.getElementById('txttenderValidity').value == ''){
                        document.getElementById('sptenderval').innerHTML ='Please enter Tender Validity in No. of Days';
                        vboolean = false;
                    }else if(!regForNumber(document.getElementById('txttenderValidity').value) || document.getElementById('txttenderValidity').value.indexOf('.') > 0){
                        document.getElementById('sptenderval').innerHTML ='Please enter digits (0-9) only';
                        vboolean = false;
                    }else if(!chkMinLength(document.getElementById('txttenderValidity').value))
                    {
                        document.getElementById('sptenderval').innerHTML='Tender Validity in No. of Days must not be less than 14 days';
                        vboolean = false;
                    }
                    else
                    {
                         if(document.getElementById("hdnProcureNature").value != null && document.getElementById("hdnProcureNature").value == 'Services' )

                             {
                                if((document.getElementById('txttenderValidity').value)>90 && (document.getElementById('txttenderValidity').value)<120){
                                    document.getElementById('SPMsg').innerHTML=''
                                }else{
                                    document.getElementById('SPMsg').innerHTML='Ideally it should be between 90-120 Days'
                                }
                             }
                         else
                           {
                                if((document.getElementById('txttenderValidity').value)>60 && (document.getElementById('txttenderValidity').value)<90)
                                {
                                    document.getElementById('SPMsg').innerHTML=''
                                }
                                else
                                {
                                    document.getElementById('SPMsg').innerHTML='Ideally it should be between 60-90 Days'
                                }
                           }

                        if(document.getElementById('txttenderSecurityValidity') != null){
                            document.getElementById('txttenderSecurityValidity').value = parseInt(document.getElementById('txttenderValidity').value) + 28;
                        }
                        document.getElementById('sptenderval').innerHTML = '';
                    }
                }
            }
            
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function chkMinLength(value)
            {
                var ValueChk=value;
                if(ValueChk<14)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

             function numberZero(value,element)
            {
                var flag ;
                //alert(value.indexOf("."));
                if(isNaN(value)==false)
                {
                    if(value.indexOf(".")==-1)
                    {
                        if(value>0)
                        {
                            flag=true;
                        }
                        else
                        {
                            flag=false;
                        }
                    }
                    else
                    {
                        flag=false;
                    }
                }
                else
                {
                    flag=false;
                }
                //alert(flag);
                if(flag==false)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            function validateConfirm(){                               
                                   if(confirm(" Are you sure that your document is properly selected? ","confirm"))
                                    {
                                        return Validation();
                                    }
                                    else
                                    {
                                        return false;
                                    }

            }

                        function Validation(){
                var vbool = true;


                if(document.getElementById('tenderDocument').value == 0){
                     document.getElementById('valStdTemplate').innerHTML ='Please select Standard Bidding Document.';
                     return false;
                 }
                if(document.getElementById('txttenderValidity')!=null)
                {
                    if(document.getElementById('txttenderValidity').value == '')
                    {
                        document.getElementById('sptenderval').innerHTML ='Please enter Tender Validity in No. of Days';
                        vbool=false;
                    } else if (document.getElementById('txttenderValidity').value.split(".")[1] != undefined){
                        document.getElementById('sptenderval').innerHTML ='Please enter digits (0-9) only';
                        vbool=false;
                    }
                }
                if(document.getElementById('txttenderSecurityValidity')!=null)
                {
                    if(document.getElementById('txttenderSecurityValidity').value == '')
                    {
                        document.getElementById('sptendersecval').innerHTML ='Please enter Tender Security Validity In No. of Days';
                        vbool=false;
                    }
                }
                if(document.getElementById('cmbDomesticPreference') != null){
                    if(document.getElementById('cmbDomesticPreference').value == ""){
                        document.getElementById('valDomPre').innerHTML = 'Please select Domestic Preference Requires';
                        vbool = false;
                    }else{
                        document.getElementById('valDomPre').innerHTML = '';
                    }
                }

                if(document.getElementById('txtDomPrePer') != null){
                    if(document.getElementById('cmbDomesticPreference').value == "Yes"){
                        if(document.getElementById('txtDomPrePer').value == ""){
                            document.getElementById('valDomPrePer').innerHTML = 'Please enter Domestic Preference in %';
                            vbool = false;
                        }else if(!regForNumber(document.getElementById('txtDomPrePer').value)){
                            document.getElementById('valDomPrePer').innerHTML = 'Please enter Numerals (0-9) only and 3 Digits after Decimal';
                            vbool = false;
                        }else if(document.getElementById('txtDomPrePer').value.split(".")[1] != undefined){
                            if(!regForNumber(document.getElementById('txtDomPrePer').value) || document.getElementById('txtDomPrePer').value.split(".")[1].length > 3){
                                document.getElementById('valDomPrePer').innerHTML = 'Please enter Numerals (0-9) only and 3 Digits after Decimal';
                                vbool = false;
                            }else{
                                document.getElementById('valDomPrePer').innerHTML = '';
                            }
                        }else{
                            document.getElementById('valDomPrePer').innerHTML = '';
                        }
                    }else{
                        document.getElementById('valDomPrePer').innerHTML = '';
                    }
                }

                if(vbool == false || vboolean == false){
                    return false;
                }else{
                    var tenderValidity = document.getElementById('txttenderValidity').value;
                    var tenderSecurityValidity = document.getElementById('txttenderSecurityValidity').value;
                    var diff = tenderSecurityValidity - tenderValidity;
                    if(diff >= 28){
                        document.getElementById('SpErr').innerHTML ='';
                    }else{
                        document.getElementById('SPMsg').innerHTML=''
                        document.getElementById('SpErr').innerHTML ='Validity of Tender Security must be Tender Validity period plus 28 days';
                        return false;
                    }
                }
                
                if(vbool == true){
                    document.getElementById("btnSubmit").style.display = 'none';
                }
            }
            </script>
        </head>
        <body>
            <%
                        String msg=null;
                        String tenderid = request.getParameter("tenderid");
                        String type ="1";// request.getParameter("type");
                        java.util.List<SPCommonSearchData> list = new java.util.ArrayList<SPCommonSearchData>();
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                        if ("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {                            
                            String tendId = "";
                            if (!request.getParameter("searchVal").isEmpty()) {
                                tendId = request.getParameter("searchVal");
                            }                             
                            list = commonSearchService.searchData("SearchTenderDocument", tenderid, tendId, session.getAttribute("userId").toString(),null, null, null, null, null, null);
                        }                        
                        if ("Submit".equalsIgnoreCase(request.getParameter("map"))) {
                            String appAuthorityId = "",tenderDocumentID ="",tenderValidityDays="",tenderSecurityValidityDays="",oldTenderID="";

                            if (!request.getParameter("appAuthority").isEmpty()) {
                                appAuthorityId = request.getParameter("appAuthority");
                            }
                            if (!request.getParameter("tenderDocument").isEmpty()) {
                                tenderDocumentID = request.getParameter("tenderDocument");
                            }
                            if (!request.getParameter("tenderValidity").isEmpty()) {
                                tenderValidityDays = request.getParameter("tenderValidity");
                            }
                            if (!request.getParameter("tenderSecurityValidity").isEmpty()) {
                                tenderSecurityValidityDays = request.getParameter("tenderSecurityValidity");
                            }
                            if (!request.getParameter("oldTender").isEmpty()) {
                                oldTenderID = request.getParameter("oldTender");
                            }

                            SPCommonSearchData data = commonSearchService.searchData("DumpExistingSTD",oldTenderID, tenderid, session.getAttribute("userId").toString(),appAuthorityId , tenderDocumentID, tenderValidityDays, tenderSecurityValidityDays, null, null).get(0);
                            String action = null;
                            String pageName="Notice";
                            System.out.println(" data.getFieldName1() "+data.getFieldName1());
                            if(data.getFieldName1().equals("0")){
                                msg="Document couldn't be mapped";
                                action = "Error in Use Existing Tender Document";
                            }else{
                                action =  "Use Existing Tender Document";
                                String stdup = "";
                                TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                                if(!tenderDocumentSrBean.copySTDDocs4ZIP(tenderDocumentID,tenderid)){
                                stdup = "&stddocdump=error";
                            }
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(tenderid), "tenderId",  EgpModule.Tender_Document.getName(), action, "");
                                response.sendRedirect(pageName+".jsp?tenderid="+tenderid+stdup);
                            }
                        }
            %>
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <%
                        pageContext.setAttribute("tenderId", tenderid);
            %>
            <div class="pageHead_1">Search and Use Existing Tender Documents<span style="float: right;"><a class="action-button-goback" href="Notice.jsp?tenderid=<%=tenderid%>">Go back to Dashboard</a></span></div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <br/>
            <%if(msg!=null){%>
            <div class="responseMsg errorMsg"><%=msg%></div><br/>
            <%}%>
            <div class="formBg_1">
                <form action="MapOldDocuments.jsp?tenderid=<%=tenderid%>" method="post" id="search">
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td class="ff" width="10%">Search by   : <span>*</span></td>
                            <td width="90%"><label name="searchBy" class="ff" id="cmbSearchBy" style="width: 200px;">
                                    Tender ID
                                </label>&nbsp;
                                <input name="searchVal" type="text" class="formTxtBox_1" id="txtSearchVal" style="width:200px;"/>&nbsp;
                                <label class="formBtn_1">
                                    <input type="submit" name="btnSearch" id="btnSearch" value="Search" />
                                </label>
                            </td>
                        </tr>
                        <tr id="dis" style="display: none;"><td width="10%">&nbsp;</td><td class="t-align-left"><span id="SearchValError" class="reqF_1"></span></td></tr>
                    </table>
                </form>
            </div>
            <form action="MapOldDocuments.jsp?tenderid=<%=tenderid%>" method="post" id="map">
               <!-- <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tb2">   -->
                 <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" id="tb2">
                    <%if ("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {
                        if(list.isEmpty()){%>
                    <tr>
                        <td colspan="5" class="t-align-center"><span style="color: red;">No records found as per the business rule configured (Mismatch in Procurement Method , Procurement Category , Procurement Type and Budget Type). </span></td>
                    </tr>
                    <%}}%>
                    <%
                                int i = 1;
                                StringBuilder tenderIds = new StringBuilder();
                                StringBuilder usetenderIds = new StringBuilder();
                                //out.print("taher _ "+list.size());
                                for (SPCommonSearchData data : list) {
                                    tenderIds.append(data.getFieldName2()+",");
                    %>
                    <tr><td width="30%" class="ff">Estimated Cost (in Nu.) : </td><td><%=data.getFieldName7()%></td></tr>
                    <tr><td width="30%" class="ff">Approving Authority : </td>
                        <td><select name="appAuthority" class="formTxtBox_1" id="cmbappAuthority" style="width:auto;">
                                    <%  List<SPTenderCommonData> cmbAAList = tenderCommonService.returndata("AAFillCombo", tenderid, data.getFieldName7());
                                                            for (SPTenderCommonData sptcd1 : cmbAAList) {
                                                                if (sptcd1.getFieldName2().equalsIgnoreCase(list.get(0).getFieldName2())) {
                                    %>

                                    <option value="<%=sptcd1.getFieldName1()%>" selected="true"><%=sptcd1.getFieldName2()%></option>
                                    <%} else {%>
                                    <option value="<%=sptcd1.getFieldName1()%>"><%=sptcd1.getFieldName2()%></option>
                                    <% }
                                                            }%>
                                </select>
                        </td></tr>
                    <tr><td width="30%" class="ff">Standard Bidding Document : </td><td><%=data.getFieldName4()%></td></tr>
                    <tr><td width="30%" class="ff">Tender Validity in No. of Days : </td>
                    <td><input name="tenderValidity" onblur="validateDays();" type="text" class="formTxtBox_1" id="txttenderValidity" style="width:200px;"   value="<%=data.getFieldName5()%>" />
                                <div id="SPMsg"></div>
                                <div id="sptenderval" style="color: red;"></div></td>
                    <tr><td width="30%" class="ff">Tender Security Validity in No. of Days : </td>
                        <td><input name="tenderSecurityValidity" onblur="validateSec();" type="text" class="formTxtBox_1" id="txttenderSecurityValidity" style="width:200px;" maxlength="50"  value="<%=data.getFieldName6()%>" />
                                <div id="sptendersecval" style="color: red;">&nbsp;</div></td>
                    </tr>                    
                    <input type="hidden" name="tenderDocument" id="tenderDocument" value="<%=data.getFieldName3()%>"/>                   
                    <input type="hidden" name="oldTender" id="oldTender" value="<%=data.getFieldName8()%>"/>

                    <%                        
                       }
                    if(!list.isEmpty()){%>
                    <tr>
                        <td width="30%" class="ff"></td>
                        <td width="70%" class="ff"><span style="color: red;">Before publishing tender be sure that your document is properly selected.</span></td>
                    </tr>
                    <%}
                           if(type.equals("2") && tenderIds.length()>0){
                               list = commonSearchService.searchData("OpenCommitteeDumpCheck", tenderIds.substring(0, tenderIds.length()-1), tenderid, null, null,null,null, null, null, null);
                               for (SPCommonSearchData data : list) {
                                    usetenderIds.append(data.getFieldName1()+",");
                               }
                           }
                        //out.print("taher _ "+tenderIds);
                    %>
                </table>
                <%if(!list.isEmpty()){%>
                <input type="hidden" name="alltenders" id="alltenders" value="<%=(tenderIds.length()>0) ? tenderIds.substring(0, tenderIds.length()-1) : tenderIds %>"/>
                <input type="hidden" name="usealltenders" id="usealltenders" value="<%=(usetenderIds.length()>0) ? usetenderIds.substring(0, usetenderIds.length()-1) : usetenderIds%>"/>
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                    <td width="30%" class="ff"></td>
                    <td width="70%" class="ff">
                    <label class="formBtn_1">
                        <input name="map" type="submit" onclick="return validateConfirm();" value="Submit" id="btnMap"/>
                    </label>
                    </td>
                    </table>
                 <%}%>
            </form>
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        </body>
        <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
    </html>
