<%-- 
    Document   : CPEvalReport
    Created on : Jun 29, 2011, 3:32:27 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderOpening"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
                       String docAvailMtd = request.getParameter("dmethod");
                       String isCommCP = request.getParameter("isCommCP");
                       String tenderId= request.getParameter("tenderid");
                       String evalCount= request.getParameter("evalCount");
                       String lotId= request.getParameter("lotId");
                       String strProcurementNature = request.getParameter("pnature");
                       String strComType = request.getParameter("comtype");
                       String evalRptStatus = request.getParameter("evalRptStatus");
                       String strCurEvalStatus="evaluation";
                       String bidderStatus = "";
                       String eventType = request.getParameter("eventType");
                       boolean isService = strProcurementNature.equalsIgnoreCase("services");
                       boolean IsAllBidderEvaluated = true;
                       boolean IsFinancialReportDecrypted = false;
                       boolean ShowActionLink = true;
                       int NumberOfBiddersEvaluatedREOI = 0;
%>
<html>    
    <body>
        <table width="100%" cellspacing="0" class="tableList_1 t_space">
            <% 
                TenderOpening opening = new TenderOpening();
                CommonSearchService commonSearchService = (CommonSearchService)AppContext.getSpringBean("CommonSearchService");
                List<SPCommonSearchData> lotIdDesc = commonSearchService.searchData("get2EnvLoIdLotDesc", tenderId, null, null, null, null, null, null, null, null);
                for (SPCommonSearchData lotdsc : lotIdDesc) 
                {
                    List<SPCommonSearchData> priceFormList = commonSearchService.searchData("getPriceBidfromLotId", tenderId, lotdsc.getFieldName1(), null, null, null, null, null, null, null);
                    for (SPCommonSearchData priceList : priceFormList) 
                    {
                        if (opening.getBidPlainDataCount(priceList.getFieldName5()) != 0) 
                        {
                            IsFinancialReportDecrypted = true;
                        }
                    }
                }
                
                if(IsFinancialReportDecrypted)
                {
                    ShowActionLink = false;
                }
                if(evalRptStatus.equalsIgnoreCase("senttopa"))
                {
                    ShowActionLink = false;
                }

            %>
            <tr>
                <th class="t-align-center" width="4%"  height="21">Sl. No.</th>
                <th class="t-align-center" width="48%" height="21">List of <%=isService ? "Consultants" : "Tenderers"%></th>
                <th class="t-align-center" width="25%" height="21">Finalized Evaluation Status</th>
                <%if("true".equals(isCommCP) && ShowActionLink){%>
                <th class="t-align-center" width="23%" height="21"><strong>Action</strong></th>
                <%}%>
            </tr>
            <%
            boolean canCurDoFinalResponseProcess = false;

            if(request.getParameter("allowFinalResponseProcess")!=null){
                if("Yes".equalsIgnoreCase(request.getParameter("allowFinalResponseProcess"))){
                    canCurDoFinalResponseProcess = true;
                }
            }                       
                       // String s_lotId = "0";
                        //if("Lot".equalsIgnoreCase(docAvailMtd)){
                         //   s_lotId = lotId;
                        //}
                       TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                       ReportCreationService creationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");
                       CommonSearchDataMoreService tenderCS1 = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                       //CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                       List<SPTenderCommonData> companyList =
                                tenderCS.returndata("getFinalSubCompanyLotWise", tenderId, lotId);

                        if (!companyList.isEmpty()) {
                            int cnt = 1;
                            for (SPTenderCommonData companyname : companyList) {
                                //14 userId,15 tendererId,16 companyId
                                String[] jvSubData = creationService.jvSubContractChk(tenderId, companyname.getFieldName2());
                                StringBuilder name_link = new StringBuilder();
                                name_link.append("<a href='" + request.getContextPath() + "/tenderer/ViewRegistrationDetail.jsp?uId=" + companyname.getFieldName2() + "&tId=" + companyname.getFieldName7() + "&cId=" + companyname.getFieldName8() + "&top=no' target='_blank'>" + companyname.getFieldName3() + "</a>");
                                if (jvSubData[0] != null && jvSubData[0].equals("jvyes")) {
                                    name_link.delete(0, name_link.length());
                                    name_link.append("<a href='" + request.getContextPath() + "/tenderer/ViewJVCADetails.jsp?uId=" + companyname.getFieldName2() + "' target='_blank'>" + companyname.getFieldName3() + "</a>");
                                    name_link.append("<br/>");
                                    name_link.append("<a href='" + request.getContextPath() + "/tenderer/ViewJVCADetails.jsp?uId=" + companyname.getFieldName2() + "' target='_blank' style='color:red'>(JVCA - View Details)</a>");
                                }
                                if (jvSubData[1] != null && jvSubData[1].equals("subyes")) {
                                    name_link.append("<br/>");
                                    name_link.append("<a href='" + request.getContextPath() + "/tenderer/ViewTenderSubContractor.jsp?uId=" + companyname.getFieldName2() + "&tenderId=" + tenderId + "' target='_blank' style='color:red'>(Sub Contractor/Consultant - View Details)</a>");
                                }
            %>
            <tr <%if (Math.IEEEremainder(cnt, 2) == 0) {%>class="bgColor-Green"<%}%> >
                <td class="t-align-center" ><%=cnt%></td>

                <td class="t-align-left" ><%=name_link%></td>
                <%
                    bidderStatus = "-";
					//changes by dohatec for reevaluation
                    for (SPCommonSearchDataMore bidStatus : tenderCS1.geteGPData("getBidderStatusView", tenderId, companyname.getFieldName2(), lotId,evalCount)) {
                                                   /*Condition when updating lotid is same then status is assigned */
                                                                              
                                                                              if (bidStatus.getFieldName1()==null || "".equalsIgnoreCase(bidStatus.getFieldName1().trim())){
                                                                                  bidderStatus = "-";
                                                                                  IsAllBidderEvaluated = false;
                                                                              }else if(bidStatus.getFieldName1().trim().equalsIgnoreCase("Technically Unresponsive")||bidStatus.getFieldName1().trim().equalsIgnoreCase("Technically Non-responsive")) {
                                                                                  bidderStatus = "Technically Non-responsive";
                                                                              }else{
                                                                                  bidderStatus = bidStatus.getFieldName1();
                                                                              }
                                                        NumberOfBiddersEvaluatedREOI++;
                                                  }
                    
                %>
                <td class="t-align-center" ><%=bidderStatus%></td>
                <%if("true".equals(isCommCP) && ShowActionLink){%>
                
                <td class="t-align-center" >
                    <%                        
                        // START : REPORT CASE
                        boolean isFinalResponseEditable = true, isFinalResponseLotEditable = true;

                        List<SPCommonSearchDataMore> lstFinalResponseEdits =
                                tenderCS1.geteGPDataMore("getFinalResponseEditstatus", tenderId, lotId,(isService?"ter1":"ter2"),String.valueOf(evalCount));
                        if (!lstFinalResponseEdits.isEmpty()) {
                            if ("Block".equalsIgnoreCase(lstFinalResponseEdits.get(0).getFieldName3())) {
                                isFinalResponseEditable = false;
                            }

                            if ("Block".equalsIgnoreCase(lstFinalResponseEdits.get(0).getFieldName4())) {
                                isFinalResponseLotEditable = false;
                            }
                        }
                        lstFinalResponseEdits = null;
                        if ("Services".equalsIgnoreCase(strProcurementNature)) {
                            // START : SERVICES CASE
                                                                      
                         if (isFinalResponseEditable) {
                    %>
                            <%if(canCurDoFinalResponseProcess) {%>
                            <%if(!"0".equalsIgnoreCase(lotId)) {%>
                                <%if(isFinalResponseLotEditable){%>
                                    &nbsp;&nbsp;<a href="EvalComMemberReport.jsp?tenderId=<%=tenderId%>&uId=<%=companyname.getFieldName2()%>&stat=eval&comType=<%=strComType%>&lotId=<%=lotId%>">Finalize Responsiveness</a>
                                <%}%>
                            <%} else {%>
                            &nbsp;&nbsp;<a href="EvalComMemberReport.jsp?tenderId=<%=tenderId%>&uId=<%=companyname.getFieldName2()%>&stat=eval&comType=<%=strComType%>&lotId=<%=lotId%>">Finalize Responsiveness</a>
                            <%}%>
                            <%
                            } else {
                            //out.print("&nbsp;&nbsp;Member(s) Notification not yet received.");
                        }

                        }else {
                            out.print("&nbsp;&nbsp;Evaluated");
                        }
                      } // END : SERVICES CASE
                      else {
                          // GOODS AND WORKS CASE
                          // if(!isCPBidderEvalDone) {
                          if (isFinalResponseEditable) {
                    %>
                    <%if(canCurDoFinalResponseProcess) {%>
                    
                    
                     <%if(!"0".equalsIgnoreCase(lotId)) {%>
                                <%if(isFinalResponseLotEditable){%>
                                    &nbsp;&nbsp;<a href="EvalTenderer.jsp?userId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>&lnk=et&comType=<%=strComType%>&lotId=<%=lotId%>">Finalize Responsiveness</a>
                                <%}%>
                            <%} else {%>
                            &nbsp;&nbsp;<a href="EvalTenderer.jsp?userId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>&lnk=et&comType=<%=strComType%>&lotId=<%=lotId%>">Finalize Responsiveness</a>
                            <%}%>

                    <%
                        } else {
                            //out.print("&nbsp;&nbsp;Member(s) Notification not yet received.");
                        }
                    } else {
                            out.print("&nbsp;&nbsp;Evaluated");
                        }

                                                    //  SHOW BELOW "Give Answer To CP" LINK, IF FOUND IN "tbl_EvalCpMemClarification"

                                                    List<SPCommonSearchDataMore> evalCPExistCount =
                                                            tenderCS1.geteGPData("GetEvalCPClarificationLinkStatus", companyname.getFieldName2(), tenderId, lotId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                            //tenderCS.returndata("GetEvalCPClarificationLinkStatus", companyname.getFieldName2(), tenderId);
                                                    if (!evalCPExistCount.isEmpty()) {
                    %>
                    <br>&nbsp;&nbsp;<a href="EvalanstoCP.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>&comType=<%=strComType%>&lotId=<%=lotId%>">View Member's Clarification</a>
                    <%} // END IF%>
                </td>
                <%}%>
            </tr>
            <%
                                            
                                        } 
                        cnt++;
                                    }
                 
                //END FOR LOOP

                if(eventType.equals("REOI") && NumberOfBiddersEvaluatedREOI != companyList.size())
                {
                    IsAllBidderEvaluated = false;
                }
                request.setAttribute("IsAllBidderEvaluated", IsAllBidderEvaluated);
                
                }else{
            %>
            <tr>
                <td class="t-align-center ff mandatory" colspan="4">
                    No record found 
                </td>
            </tr>
            <% }%>
        </table>
    </body>
</html>
