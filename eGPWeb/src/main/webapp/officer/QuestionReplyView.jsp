<%-- 
    Document   : QuestionReplyView
    Created on : Dec 9, 2010, 9:56:18 PM
    Author     : parag
--%>
<%@page import="java.net.URLDecoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Query and Reply</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <%--<script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

    </head>
        <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="../resources/common/AfterLoginTop.jsp" ></jsp:include>
                <%
                            int userId = 0;
                            byte suserTypeId = 0;
                            int tenderId = 20;
                            int queryId = 1;
                            int cuserId = 0;
                            String viewType = "";

                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            if (request.getParameter("queryId") != null) {
                                queryId = Integer.parseInt(request.getParameter("queryId"));
                            }
                            //if (request.getParameter("cuserId") != null) {
                              //  cuserId = Integer.parseInt(request.getParameter("cuserId"));
                            //}
                            if (request.getParameter("viewType") != null) {
                                viewType = request.getParameter("viewType");
                            }

                            SPTenderCommonData getQueryText = preTendDtBean.getDataFromSP("GetQuestionText",queryId,0).get(0);
                            SPTenderCommonData getReplyText = preTendDtBean.getDataFromSP("GetReplyText", queryId, 0).get(0);

                             // Coad added by Dipal for Audit Trail Log.
                            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            String idType="tenderId";
                            int auditId=tenderId;
                            String auditAction="Bidder View Clarification from PA";
                            String moduleName=EgpModule.Pre_Tender_Query_Meeting.getName();
                            String remarks="User Id: "+session.getAttribute("userId")+" has viewed clarification fo QueryId: "+queryId;
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                %>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">View Query and Reply</div>
                        <%
                                    // Variable tenderId is defined by u on ur current page.
                                    pageContext.setAttribute("tenderId", tenderId);
                        %>

                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div>&nbsp;</div>
                        <% pageContext.setAttribute("tab", 11);%>
                        <%if (suserTypeId == 2) {%>
                        <jsp:include page="../tenderer/TendererTabPanel.jsp" >
                            <jsp:param name="tab" value="11" />
                        </jsp:include>
                        <%}else{%>
                        <jsp:include page="../officer/officerTabPanel.jsp" >
                            <jsp:param name="tab" value="11" />
                        </jsp:include>
                        <%}%>
                        <div>&nbsp;</div>

                        <form method="post" id="frmPreTendQuery" action="<%=request.getContextPath()%>/PreTendQuerySrBean?action=replyQuery">
                            <div class="tabPanelArea_1">
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <td width="9%" class="t-align-left ff">Query :</td>
                                        <td width="84%" class="t-align-left"><%=URLDecoder.decode(getQueryText.getFieldName2(),"UTF-8")%></td>
                                    </tr>
                                </table>
                                <%

                                    List<SPTenderCommonData> getPreBidDocumentData = null;
                                    getPreBidDocumentData = preTendDtBean.getDataFromSP("QuestionDocsMore",queryId,0);
                                    if(!getPreBidDocumentData.isEmpty())
                                    {
                                %>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="4%" class="t-align-center">Sl.  No.</th>
                                    <th class="t-align-center" width="23%">File Name</th>
                                    <th class="t-align-center" width="32%">File Description</th>
                                    <th class="t-align-center" width="7%">File Size <br />
                                        (in KB)</th>
                                    <th class="t-align-center" width="18%">Action</th>
                                </tr>
                                <%
                                    
                                        for (SPTenderCommonData preDocData : getPreBidDocumentData) {
                                        int count=0;
                                        count++;
                                %>
                                    <tr>
                                        <td class="t-align-center"><%=count %></td>
                                        <td class="t-align-left"><%=preDocData.getFieldName1()%></td>
                                        <td class="t-align-center"><%=preDocData.getFieldName2()%></td>
                                        <td class="t-align-center"><%=(Long.parseLong(preDocData.getFieldName3())/1024)%></td>
                                        <td class="t-align-center">
                                            <a href="<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName=<%=preDocData.getFieldName1()%>&docSize=<%=preDocData.getFieldName3()%>&tender=<%=tenderId%>&queryId=<%=queryId%>&docType=<%=preDocData.getFieldName10()%>&funName=download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                        </td>
                               <%
                                        }
                                    }
                               %>                                    
                                    </tr>
                                </table>
                                <div id="docdatabybidder"></div>
                                <div>&nbsp;</div>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <td width="9%" class="t-align-left ff">Reply :</td>
                                        <td width="84%" class="t-align-left">
                                            <%
                                                if(getReplyText.getFieldName2() != null){
                                                    out.println(URLDecoder.decode(getReplyText.getFieldName2(),"UTF-8"));
                                             }%>
                                       </td>
                                    </tr>
                                </table>

                                <div id="docData">                                    
                                </div>
                                <div>&nbsp;</div>
                                <%--<div class="t-align-center">
                                    <label class="formBtn_1">
                                      <input type="submit" name="button3" id="button3" value="Reply" />
                                      <input type="hidden" name="hidQueryId" id="hidQueryId" value="<%=queryId%>" />
                                    </label>
                                </div>--%>
                            </div>
                            <div>&nbsp;</div>
                        </form>
                        <!--Dashboard Content Part End-->
                        <!--Dashboard Footer Start-->
                        <%@include file="../resources/common/Bottom.jsp" %>
                        <!--Dashboard Footer End-->
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<form id="form2" method="post">
</form>
<script>
    function checkStatus(){
        if($('#cmbPostQuery').val() == "Reply"){
            $('#divReply').show();
        }else if($('#cmbPostQuery').val() == "Hold"){
            $('#divReply').hide();

        }else if($('#cmbPostQuery').val() == "Ignore"){
            $('#divReply').hide();
        }
    }

    function checkRepQuery(){
        if(document.getElementById("checkbox").checked){
            $('#lblRepQuery').show();
        }else{
            $('#lblRepQuery').hide();
        }
    }

    function getDocData(){
    <%--$.ajax({
     url: "<%=request.getContextPath()%>/PreTendQuerySrBean?tenderId=<%=tenderId%>&queryId=<%=queryId%>&action=docData&docAction=QuestionDocsMore",
     method: 'POST',
     async: false,
     success: function(jj) {
         document.getElementById("docdatabybidder").innerHTML = jj;
     }
 });--%>
         $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,queryId:<%=queryId%>,action:'docData',docAction:'QuestionReplyDocs',viewAction:'view'}, function(j){
             document.getElementById("docData").innerHTML = j;
             hidedoc();
         });
     }
     getDocData();

     function downloadFile(docName,docSize,tender){
    <%--$.ajax({
             url: "<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download",
             method: 'POST',
             async: false,
             success: function(j) {
             }
     });--%>
             document.getElementById("form2").action="<%=request.getContextPath()%>/PreTenderReplyUploadServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download";
             document.getElementById("form2").submit();
         }

         function deleteFile(docName,docId,tender){
    <%--$.ajax({
           url: "<%=request.getContextPath()%>/PreTenderReplyUploadServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=remove",
           method: 'POST',
           async: false,
           success: function(j) {
               jAlert("Document Deleted."," Delete Document ", function(RetVal) {
               });
           }
   });--%>
           $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
               if(r == true){
                   $.post("<%=request.getContextPath()%>/PreTenderReplyUploadServlet", {docName:docName,docId:docId,tender:tender,funName:'remove'}, function(j){
                       jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                           getDocData();
                       });
                   });
               }else{
                   getDocData();
               }
           });
       }
       function hidedoc()
       {
           var flag = false;
            var vCount = $('#tblresponseDoc').children()[0].children.length - 1;
            if(vCount > 1)
            {
                   flag = true;
            }
            if(vCount  == '1')
            {
                $('#tblresponseDoc tr').each(function() {
                    responseDocs = $(this).find("td:first").html();
                });
                if(responseDocs != 'No records found.')
                {
                    flag = true;
                }
            }
            if(!flag)
            {
                $('#docData').hide();
            }
       }
</script>

