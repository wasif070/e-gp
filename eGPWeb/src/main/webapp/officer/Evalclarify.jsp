<%--
Document   : EvalClarify
Created on : Dec 30, 2010, 2:16:46 PM
Author     : Kinjal Shah
--%>
<%--
Modified almost every function and add parameter for re-evaluation by dohatec
--%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalTscCommentsSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalTscnotification"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvalTSCNotificationService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPerfSecurity"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TblTenderPerformanceSec"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderOpening"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
 <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>


        <%
            String tenderId = request.getParameter("tenderId");
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List of Tenderers/Consultants for seeking clarification View Query – View Query / Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
         <script>

            function notify(){
                dynamicFromSubmit("/EvalTscNotifyServlet?tenderId=<%=tenderId%>");
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">

            <%
                        boolean isQueSentToCP = false, isExternalMem = false, allowNotify = true, isEvalNotificationGiven = false;
                        boolean isTSCNotificationDone = false;
                        boolean btnSendToTEC = false;
                        boolean btnNotifyToTEC = false;
                        ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                        String strST = "";
                        if(request.getParameter("st") != null)
                        {
                            strST = request.getParameter("st");
                        }
                        tenderId = request.getParameter("tenderId");
                        String sentBy = "", rolee = "", committeetype = "", strEvalStatus="evaluation";
                        String strUserTypeId="";
                        if (session.getAttribute("userId") != null) {
                            sentBy = session.getAttribute("userId").toString();
                        }

                        if (session.getAttribute("userTypeId")!= null) {
                            if("14".equalsIgnoreCase(session.getAttribute("userTypeId").toString())){
                                isExternalMem = true;
                            }
                        }
                          String strComType="null";
                        if(request.getParameter("comType") != null && !"null".equalsIgnoreCase(request.getParameter("comType")) ){
                            strComType =   request.getParameter("comType");
                        }

                //EvaluationService evalService1 = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
                CommonSearchService commonSearchService1 = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                CommonSearchDataMoreService tenderCS1 = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
               // int evalCount1 = evalService1.getEvaluationNo(Integer.parseInt(tenderId));
                String xmldata = "";
                String remark = "";
           if(request.getParameter("btnNotifyToTEC") != null || request.getParameter("btnSendToTEC") != null){
               String path = "";
                if (request.getParameter("btnSendToTEC") != null) {
                    String action = "Notifying TEC CP evaluation is finalized";
                    path = "Evalclarify.jsp?tenderId=" + tenderId + "&st="+strST + "&msgId=error";
                     try {

                          String evalStatus = "Evaluation";
                          SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                          HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                          remark = handleSpecialChar.handleSpecialChar(remark);
                         // xmldata = "<tbl_EvalSentQueToCp tenderId=\"" + tenderId + "\" sentBy=\"" + sentBy + "\" sentDt=\"" + format.format(new Date()) + "\" evalStatus=\"" + evalStatus + "\" remarks=\"" + remark + "\" sentFor=\"" + "question" + "\" />";
                         // xmldata = "<root>" + xmldata + "</root>";

                        //  CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        //  CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalSentQueToCp", xmldata, "").get(0);
                         CommonMsgChk commonMsgChk = addUpdate.addUpdOpeningEvaluation("NotifyChairpersonAfterPostedQuestion",tenderId,sentBy,format.format(new Date()),evalStatus,remark,"question","","","","","","","","","","","","","").get(0);

                          if (commonMsgChk != null || handleSpecialChar != null) {
                                 commonMsgChk = null;
                                 handleSpecialChar = null;
                         }

                                    String emailId = "";
                      String refNo = "";
                                    List<SPTenderCommonData> lstTCS = tenderCS.returndata("GetTecCpEmail", tenderId, null);
                                    for (SPTenderCommonData data : lstTCS) {
                                        emailId += data.getFieldName1() + ",";
                       refNo = data.getFieldName2();
                       }
                      String emailArr[] = {emailId};
                      SendMessageUtil sendMessageUtil = new SendMessageUtil();
                      MailContentUtility mailContentUtility = new MailContentUtility();

                                    List list1 = mailContentUtility.evalClari(tenderId, refNo);
                       String mailSub = list1.get(0).toString();
                       String mailText = list1.get(1).toString();
                       sendMessageUtil.setEmailTo(emailArr);
                       sendMessageUtil.setEmailSub(mailSub);
                       sendMessageUtil.setEmailMessage(mailText);
                       sendMessageUtil.sendEmail();
                       MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                       UserRegisterService userRegisterService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                       userRegisterService.contentAdmMsgBox(emailArr[0], XMLReader.getMessage("emailIdNoReply"), "e-GP: Queries posted by Evaluation Committee Member", msgBoxContentUtility.evalClari(tenderId, refNo));
                       //response.sendRedirect("Evalclarify.jsp?tenderId=" + tenderId + "&st="+strST + "&msgId=sc");
                       path = "Evalclarify.jsp?tenderId=" + tenderId + "&st="+strST + "&msgId=sc";
                       } catch (Exception ex) {
                          ex.printStackTrace();
                          //response.sendRedirect("Evalclarify.jsp?tenderId=" + tenderId + "&st="+strST + "&msgId=error");
                          action = "Error in "+action+" "+ex.getMessage();
                        }finally{
                            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(tenderId), "TenderId", EgpModule.Evaluation.getName(), action, "");
                            action = null;
                        }

                    }

                    if (request.getParameter("btnNotifyToTEC") != null) {
                        String action = "Notifying TEC CP evaluation is finalized";
                        path = "Evalclarify.jsp?tenderId=" + tenderId + "&st="+strST + "&msgId=error";
                        try {

                              SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                              HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                              remark = handleSpecialChar.handleSpecialChar(remark);
                             // xmldata = "<tbl_EvalSentQueToCp tenderId=\"" + tenderId + "\" sentBy=\"" + sentBy + "\" sentDt=\"" + format.format(new Date()) + "\" evalStatus=\"" + "Evaluation" + "\" remarks=\"" + remark + "\" sentFor=\"" + "evaluation" + "\" />";
                            //  xmldata = "<root>" + xmldata + "</root>";

                            //  CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                           //  CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalSentQueToCp", xmldata, "").get(0);
                             CommonMsgChk commonMsgChk =addUpdate.addUpdOpeningEvaluation("NotifyChairpersonAfterEvaluation",tenderId,sentBy,format.format(new Date()),"Evaluation",remark,"evaluation","","","","","","","","","","","","","").get(0);


                                if (commonMsgChk != null || handleSpecialChar != null) {
                                     commonMsgChk = null;
                                     handleSpecialChar = null;
                                }


                                        String emailId = "";
                              String refNo = "";
                                            List<SPTenderCommonData> lstTCS = tenderCS.returndata("GetTecCpEmail", tenderId, null);
                                            for (SPTenderCommonData data : lstTCS) {
                                                emailId += data.getFieldName1() + ",";
                               refNo = data.getFieldName2();
                               }
                                            CommonSearchDataMoreService commonSDMS = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                            SPCommonSearchDataMore peName = commonSDMS.geteGPData("GetLotPackageDetailsForIssue", "getPEOfficeName", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null).get(0);
                                            String s_peName = peName.getFieldName1();
                              String emailArr[] = {emailId.substring(0,emailId.length()-1)};
                              SendMessageUtil sendMessageUtil = new SendMessageUtil();
                              MailContentUtility mailContentUtility = new MailContentUtility();

                                            List list2 = mailContentUtility.evalClarification(tenderId, refNo,s_peName);
                               String mailSub = list2.get(0).toString();
                               String mailText = list2.get(1).toString();
                               sendMessageUtil.setEmailTo(emailArr);
                               sendMessageUtil.setEmailSub(mailSub);
                               sendMessageUtil.setEmailMessage(mailText);
                               sendMessageUtil.sendEmail();
                               MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                               UserRegisterService userRegisterService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                               userRegisterService.contentAdmMsgBox(emailArr[0], XMLReader.getMessage("emailIdNoReply"), "e-GP: Completion of Evaluation Process", msgBoxContentUtility.evalClarification(tenderId, refNo,s_peName));
                           //response.sendRedirect("Evalclarify.jsp?tenderId=" + tenderId + "&st="+strST +  "&msgId=nc");
                           path = "Evalclarify.jsp?tenderId=" + tenderId + "&st="+strST +  "&msgId=nc";
                        } catch (Exception ex) {
                             ex.printStackTrace();
                             action = "Error in "+action+" "+ex.getMessage();
                            //response.sendRedirect("Evalclarify.jsp?tenderId=" + tenderId + "&st="+strST + "&msgId=error");
                        }finally{
                            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(tenderId), "TenderId", EgpModule.Evaluation.getName(), action, "");
                            action = null;
                        }
                        //
                    }
                    response.sendRedirect(path);
                   }

            // IF ENTRY FOUND IN "tbl_EvalSentQueToCp" TABLE IN CASE OF "tec/pec" MEMBER.
                // GET THE TEC-MEMBER COUNT.
                        List<SPTenderCommonData> lstIsTEC =
                        tenderCS.returndata("IsTEC", tenderId, sentBy);

                        int isTECCnt = lstIsTEC.size();

                        lstIsTEC = null;

                // CHECK FOR USER-ROLL AND SET THE LINK AS PER THE ROLL.
                        List<SPCommonSearchData> lstRole =
                        commonSearchService1.searchData("EvalMemberRole", tenderId, sentBy, null, null, null, null, null, null, null);

                        if (!lstRole.isEmpty()) {
                            rolee = lstRole.get(0).getFieldName2();
                            committeetype = lstRole.get(0).getFieldName3();
                }
                        lstRole = null;


                // Get info whether Questions sent to Chaieperson or not
                    List<SPTenderCommonData> getQueSenttoCPStatus =
                    tenderCS.returndata("getQueSenttoCPStatus", tenderId, sentBy);
                        if (!getQueSenttoCPStatus.isEmpty()) {
                            if ("Yes".equalsIgnoreCase(getQueSenttoCPStatus.get(0).getFieldName1())) {
                                isQueSentToCP = true;
                        }

                             if ("Yes".equalsIgnoreCase(getQueSenttoCPStatus.get(0).getFieldName2())) {
                                isEvalNotificationGiven = true;
                    }
                        }
                        getQueSenttoCPStatus = null;

            // Get Tender Procurement Nature info
                        String strProcurementNature = "";
                        List<SPTenderCommonData> lstProcurementNature =
                                tenderCS.returndata("getTenderProcurementNature", tenderId, null);
                        if (!lstProcurementNature.isEmpty()) {
                            strProcurementNature = lstProcurementNature.get(0).getFieldName1();
                        }
                        lstProcurementNature = null;

                        // Get All Bidder Clarification Link info
                        boolean showAllQryResponse = false;
                        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        List<SPCommonSearchDataMore> clariLst = commonSearchDataMoreService.geteGPData("allBidderClariShow", tenderId,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        if(!clariLst.isEmpty()){
                            if("1".equalsIgnoreCase(clariLst.get(0).getFieldName1())){
                                showAllQryResponse = true;
                            }
                        }
                                //("allBidderClariShow", tenderId,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            %>
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            </div>
            <form id="frmEvalClari" name="frmEvalClari" action="Evalclarify.jsp?tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>" method="post">
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">List of Tenderers/Consultants for seeking clarification</div>
                <%
                        pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>

                    
                <% pageContext.setAttribute("tab", "7");%>
                <%@include  file="officerTabPanel.jsp"%>
                   

                <div class="tabPanelArea_1">
                        <%-- Start: CODE TO DISPLAY MESSAGES --%>
                         <%if(request.getParameter("msg")!=null){%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;">Evaluation Committee members are notified successfully</div>
                        <%}%>
                        <%if(request.getParameter("msgFlag")!=null){%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;">Performance Security amount submitted successfully</div>
                        <%}%>
                        <%if(request.getParameter("msgAA")!=null && "s".equalsIgnoreCase(request.getParameter("msgAA"))){%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;">Evaluation Report sent successfully</div>
                        <%}%>
                        <%if(request.getParameter("msgN")!=null && "ns".equalsIgnoreCase(request.getParameter("msgN"))){%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;">Members notified successfully</div>
                        <%}%>
                        <%if(request.getParameter("msgN")!=null && "nu".equalsIgnoreCase(request.getParameter("msgN"))){%>
                        <div class="responseMsg errorMsg" style="margin-bottom: 12px;">Members could not notified, Please try again</div>
                        <%}%>
                        <!-- Dohatec Start-->
                        <%if(request.getParameter("msgN")!=null && "nps".equalsIgnoreCase(request.getParameter("msgN"))){%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;">PE notified successfully</div>
                        <%}%>
                        <%if(request.getParameter("msgN")!=null && "npu".equalsIgnoreCase(request.getParameter("msgN"))){%>
                        <div class="responseMsg errorMsg" style="margin-bottom: 12px;">PE could not notified, Please try again</div>
                        <%}%>
                        <!-- Dohatec End-->
                        <%if(request.getParameter("msgGC")!=null && request.getParameter("msgGC").equals("success")){%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;">Clarification given to AA successfully</div>
                        <%}%>
                        <%if(request.getParameter("rep")!=null && request.getParameter("rep").equals("Ter1")){%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;">Tender Evaluation Report 1 prepared successfully</div>
                        <%}%>
                        <%if(request.getParameter("rep")!=null && request.getParameter("rep").equals("Per1")){%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;">Proposal Evaluation Report 1 prepared successfully</div>
                        <%}%>
                        <%if(request.getParameter("rep")!=null && request.getParameter("rep").equals("Ter2")){%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;">Tender Evaluation Report 2 prepared successfully</div>
                        <%}%>
                        <%if(request.getParameter("rep")!=null && request.getParameter("rep").equals("Per2")){%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;">Proposal Evaluation Report 2 prepared successfully</div>
                        <%}%>
                 <%if (request.getParameter("msgId") != null) {
                    String msgId = "", msgTxt = "";
                    boolean isError = false;
                    msgId = request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")) {
                        if (msgId.equalsIgnoreCase("error")) {
                            isError = true;
                            msgTxt = "There was some error";
                        } else {
                             // IF ROLL IS "CHAIR PERSON" (CP)
                                                if ("cp".equalsIgnoreCase(rolee)) {
                                msgTxt = "Successfully sent to Bidder/Consultant";
                                                } else if (msgId.equalsIgnoreCase("nc")) {
                                                    msgTxt = "Successfully notified to Chairperson";
                                                } else {
                                                    msgTxt = "Successfully sent to Chairperson";
                        }
                                            }
                    %>
                    <%if (isError) {%>
                        <div class="responseMsg errorMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                    <%} else {%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                    <%}%>
                    <%}
                         }
                    %>
                    <%-- End: CODE TO DISPLAY MESSAGES --%>

                    <%-- Start: Common Evaluation Table --%>
                    <%@include file="/officer/EvalCommCommon.jsp" %>
                    <%-- End: Common Evaluation Table --%>

                    <%
                                    if ("rp".equalsIgnoreCase(strST)) {
                        pageContext.setAttribute("TSCtab", "3");
                                    } else {
                        pageContext.setAttribute("TSCtab", "4");
                                    }
                %>




                        <div class="t_space">
                            <%@include file="../resources/common/AfterLoginTSC.jsp"%>
                        </div>
                        <div class="t_space">
                            &nbsp;
                        </div>



                        <%if(!isExternalMem) {%>
                        <div class="tabPanelArea_1">
                            <%}%>
                <%
                            // CHECK COMMITTEE
                            if (!"".equalsIgnoreCase(committeetype)) {
                                // IF TEC Committe
                                            if ("tec".equalsIgnoreCase(committeetype) || "pec".equalsIgnoreCase(committeetype)) {
                                    // IF TEC CHAIRPERSON THEN SHOW BELOW LINKS
                                                if ("cp".equalsIgnoreCase(rolee)) {
                            %>
<%if(showAllQryResponse){%>
                             <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td style="width: 40%" class="t-align-left ff">
                                        All Queries and Responses
                                    </td>
                                    <td class="t-align-left">
                                        &nbsp;&nbsp;
                                        <a href="AllQeryResp.jsp?tenderid=<%=tenderId%>&comType=<%=strComType%>&evalCount=<%=evalCount%>">View</a>
                                    </td>
                                </tr>
                            </table>
                                    <%}%>



                        <%if ("cl".equalsIgnoreCase(strST)) {
                            boolean isService = false;
                                                    if ("Services".equalsIgnoreCase(strProcurementNature)) {
                                                       isService = true;
                                            //List<SPTenderCommonData> lstEvalBidderStatus =
                                              //      tenderCS.returndata("GetEvalBidderStatus", "tenderId=" + tenderId + " And userId=" + companyname.getFieldName2() + " And evalBy=" + session.getAttribute("userId").toString(), null);
                                                   }
                          %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                                    <th width="5%" class="t-align-center" height="4%">Sl. No.</th>
                                    <%if(isService){%>
                                    <th width="25%" class="t-align-center" height="51%">List of Consultants</th>
                                    <%}else{%>
                                    <th width="25%" class="t-align-center" height="50%">List of Tenderers</th>
                                    <%}%>
                                    <%if(!"tsc".equalsIgnoreCase(committeetype)){%>
                                    <th width="20%" class="t-align-center" height="15%">Clarification Status</th>
                                    <%}%>
                                    <th width="55%" class="t-align-center"  height="30%"><strong>Action</strong></th>
                        </tr>
                     <%
                                    List<SPCommonSearchDataMore> companyList =
                                            tenderCS1.geteGPData("getFinalSubComp", tenderId, "0", strEvalStatus, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null) ;
                                            //tenderCS1.geteGPData("getFinalSubComp", tenderId, "0",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
String strFnlQuestCount="0";
                            if (!companyList.isEmpty()) {
                                int cnt = 1;

                            for (SPCommonSearchDataMore companyname : companyList) {
                        //14 userId,15 tendererId,16 companyId
                        String[] jvSubData = creationService.jvSubContractChk(tenderId, companyname.getFieldName2());
                        StringBuilder name_link = new StringBuilder();
                        name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewRegistrationDetail.jsp?uId="+companyname.getFieldName2()+"&tId="+companyname.getFieldName7()+"&cId="+companyname.getFieldName8()+"&top=no' target='_blank'>"+companyname.getFieldName3()+"</a>");
                        if(jvSubData[0]!=null && jvSubData[0].equals("jvyes")){
                            name_link.delete(0, name_link.length());
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+companyname.getFieldName2()+"&noback=y' target='_blank'>"+companyname.getFieldName3()+"</a>");
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+companyname.getFieldName2()+"&noback=y' target='_blank' style='color:red'>(JVCA - View Details)</a>");
                        }
                        if(jvSubData[1]!=null && jvSubData[1].equals("subyes")){
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewTenderSubContractor.jsp?uId="+companyname.getFieldName2()+"&tenderId="+tenderId+"' target='_blank' style='color:red'>(Sub Contractor/Consultant - View Details)</a>");
                        }
                        %>
                                <tr <%if (Math.IEEEremainder(cnt, 2) == 0) {%>class="bgColor-Green"<%}%> >
                                    <td class="t-align-center" width="5%"><%=cnt%></td>

                                    <td class="t-align-left" width="35%"><%=name_link%></td>
                                    <%if(!"tsc".equalsIgnoreCase(committeetype)){%>
                                    <td class="t-align-center"><%=companyname.getFieldName9() %></td>
                                    <%}%>
                                    <td class="t-align-center" width="60%">
                              <%
                                                boolean isCPBidderEvalDone=false;
                                                boolean isBidderClariAsked = false;
                                                boolean isBidderClariGiven = false;

                                                List<SPCommonSearchData> lstCPBidderEvalChk =
                                                commonSearchService1.searchData("getClarificationToCPLink", tenderId, companyname.getFieldName2(), strEvalStatus, String.valueOf(evalCount), null, null, null, null, null);
                                                if(!lstCPBidderEvalChk.isEmpty()){
                                                    isCPBidderEvalDone = true; // shows that evaluation CP has given finalize responsiveness process for this bidder
                                                }
                                                lstCPBidderEvalChk = null;


                                                List<SPCommonSearchData> lstBidderClariStatus =
                                                commonSearchService1.searchData("GetEvalBidderClarificationStatus", tenderId, companyname.getFieldName2(), strEvalStatus, null, null, null, null, null, null);

                                                if(!lstBidderClariStatus.isEmpty()){
                                                    isBidderClariAsked = true;
                                                    if("Yes".equalsIgnoreCase(lstBidderClariStatus.get(0).getFieldName1())){
                                                        isBidderClariGiven = true; // shows that biider has given his clarification
                                                    } else {
                                                        isBidderClariGiven = false;
                                                    }
                                                } else {
                                                    isBidderClariAsked = false;
                                                }
                                                lstBidderClariStatus = null;

                                                // START : Clarification Case
                                                List<SPTenderCommonData> evalBidderRespExist =
                                                        tenderCS.returndata("GetEvalBidderRespTenderId", tenderId, companyname.getFieldName2());

                                                if (!evalBidderRespExist.isEmpty()) {
                                                    // isBidderClariGiven = true; // shows that bidder has given his clarification (if clarification was asked)
                                                }

                                                //if (evalBidderRespExist.isEmpty()) {
                                                if (!isCPBidderEvalDone && !isBidderClariAsked && !isQueSentToCP) {
                                                    //if (!isQueSentToCP) {
                                        %>
                                            &nbsp;&nbsp;<a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>">Seek Clarification from <%=isService?"Consultant":"Bidder"%></a><br>
                    <%
                                            // }
                                         }
                                            // SET THE LINK OF "NO QUESTION" WHEN NO QUESTION FOUND FOR ANY BIDDER
                                            List<SPTenderCommonData> evalFormQuesCount =
                                                    tenderCS.returndata("GetEvalFormQuesCount", tenderId, companyname.getFieldName2());

                                            String questCount = evalFormQuesCount.get(0).getFieldName1();

                                            List<SPCommonSearchData> evalFormFinalQuesCount =
                                                commonSearchService1.searchData("GetEvalFormFinalQuesCount", tenderId, companyname.getFieldName2(), strEvalStatus, sentBy, null, null, null, null, null);
                                            strFnlQuestCount = evalFormFinalQuesCount.get(0).getFieldName1();

                                            if (!isCPBidderEvalDone  && !isBidderClariAsked && !isQueSentToCP && Integer.parseInt(questCount) == 0) {
                                                // if (!isQueSentToCP) {
                                %>
                                        &nbsp;&nbsp;<label>No Question Posted yet</label><br>
                                <%
                                                // }
                                            } else {
                                                // If Questions Count > 0
                                            // SHOW BELOW LINK IF - "EvalBidderResponse" EXISTS
                                                //if (evalBidderRespExist.isEmpty()) {
                                                if (!isCPBidderEvalDone) {
                                                    if(!isBidderClariAsked){
                              %>
                                                <%if(Integer.parseInt(strFnlQuestCount) > 0){%>
                                                    &nbsp;&nbsp;<a href="EvalClariPostBidder.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>">View Questions / Send Questions To <%=isService?"Consultant":"Bidder"%></a><br>
                                                <%} else {%>
                                                    &nbsp;&nbsp;<label>No Question Posted yet</label><br>
                                                <%}%>
                              <%
                                                    } else {
                                        %>
                                            &nbsp;&nbsp;<label>Posted To Bidder/Consultant</label><br>
                                            &nbsp;&nbsp;<a href="EvalViewQue.jsp?tenderId=<%=tenderId%>&uid=<%=companyname.getFieldName2()%>&st=<%=strST%>&comType=<%=strComType%>">View Query / Clarification</a><br>
                                         <%
                                    }
                                                } else {
                                                    if(isBidderClariAsked){
                                        %>
                                        &nbsp;&nbsp;<a href="EvalViewQue.jsp?tenderId=<%=tenderId%>&uid=<%=companyname.getFieldName2()%>&st=<%=strST%>&comType=<%=strComType%>">View Query / Clarification</a><br>
                                        <%
                                  }
                                                }
                                            }
                                        %>

                                        <%
                                            // VISIBLE BELOW LINK, IF RESPONSE TIME IS OVER
                                    if (Integer.parseInt(companyname.getFieldName4()) < 0) {
                              %>
                              <%
                                if ("Services".equalsIgnoreCase(strProcurementNature)) {
                                        // For Services Case
                                        %>

                                        <%
                                            List<SPTenderCommonData> lstEvalBidderStatus =
                                                    tenderCS.returndata("GetEvalBidderStatus", "tenderId=" + tenderId + " And userId=" + companyname.getFieldName2() + " And evalBy=" + session.getAttribute("userId").toString()+ " And evalCount=" + evalCount, null); //Change by dohatec for re-evaluation

                                            if (!lstEvalBidderStatus.isEmpty()) {
                                                if ("Yes".equalsIgnoreCase(lstEvalBidderStatus.get(0).getFieldName1())) {
                                        %>

                                        &nbsp;&nbsp;<a href="EvalBidderMarks.jsp?tenderId=<%=tenderId%>&uId=<%=companyname.getFieldName2()%>&comType=<%=strComType%>">View Evaluation Status</a><br>
                                        <%
                                        }
                                            }

                                            lstEvalBidderStatus = null;
                                        %>

                                        <%
                                            List<SPTenderCommonData> lstEvalTenderLinkStatus =
                                                        tenderCS.returndata("getEvaluateTenderer_linkStatus", "tenderId=" + tenderId + " And userId=" + companyname.getFieldName2() + " And evalBy=" + session.getAttribute("userId").toString()+ " And evalCount=" + evalCount, null); //Change by dohatec for re-evaluation
                                            boolean bol_linkEnable = false;
                                            List<SPCommonSearchDataMore> getLinkForEdit = tenderCS1.geteGPData("getLinkForEditViewEvalTenderer",tenderId,companyname.getFieldName2(),userid);
                                            if(getLinkForEdit!=null && !getLinkForEdit.isEmpty() && !"0".equalsIgnoreCase(getLinkForEdit.get(0).getFieldName1())){
                                                bol_linkEnable = true;
                                                getLinkForEdit.clear();
                                                getLinkForEdit = null;
                                            }
                                            if (!lstEvalTenderLinkStatus.isEmpty()) {
                                                if ("No".equalsIgnoreCase(lstEvalTenderLinkStatus.get(0).getFieldName1())) {

                                                    List<SPTenderCommonData> lstEvalTenderLinkStatusForTECMember =
                                                    tenderCS.returndata("getEvaluateTenderer_linkStatusForTECMember", tenderId, sentBy);

                                                    if(!lstEvalTenderLinkStatusForTECMember.isEmpty()){
                                                        if(Integer.parseInt(lstEvalTenderLinkStatusForTECMember.get(0).getFieldName1())==0){
                                                            if(bol_linkEnable){%>
                                                         <a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&lnk=et&comType=<%=strComType%>">Edit</a> |  <a href="EvalBidderMarks.jsp?tenderId=<%=tenderId%>&uId=<%=companyname.getFieldName2()%>&comType=<%=strComType%>">View</a><br>
                                                       <%}else{
                                        %>
                                        &nbsp;&nbsp;<a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&lnk=et&comType=<%=strComType%>">Evaluate <%=isService?"Consultant":"Tenderer"%></a><br>
                                        <%  }
                                                        }
                                                    }

                                                    lstEvalTenderLinkStatusForTECMember = null;
                                                }
                                            }
                                            lstEvalTenderLinkStatus = null;
                                        %>

                                        <%
                                            } else {
                                                List<SPCommonSearchData> lstClarificationToCP =
                                                commonSearchService1.searchData("getClarificationToCPLink", tenderId, companyname.getFieldName2(), strEvalStatus, String.valueOf(evalCount), null, null, null, null, null);
                                                if(lstClarificationToCP.isEmpty()){
                                                    boolean bol_linkEnable = false;
                                                List<SPCommonSearchDataMore> getLinkForEdit = tenderCS1.geteGPData("getLinkForEditViewEvalTenderer",tenderId,companyname.getFieldName2(),userid);
                                                if(getLinkForEdit!=null && !getLinkForEdit.isEmpty() && !"0".equalsIgnoreCase(getLinkForEdit.get(0).getFieldName1())){
                                                    bol_linkEnable = true;
                                                    getLinkForEdit.clear();
                                                    getLinkForEdit = null;
                                        }
                                                    if(bol_linkEnable){%>
                                                      <a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&lnk=et&comType=<%=strComType%>">Edit</a> |  <a href="EvalViewStatus.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>">View</a><br />
                                                    <%}else{
                                        %>
                                        &nbsp;&nbsp;<a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&lnk=et&comType=<%=strComType%>">Evaluate Bidder/Consultant</a><br>
                                        <%
                                               }
                                                } else {
                                        %>
                                             &nbsp;&nbsp;<a href="EvalViewStatus.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>">View Evaluation Status</a><br>
                                        <%
                                        }
                                        %>
                                        <%}%>
                                        <%
                                        } // END : Clarification Case
                                        %>
                                    </td>
                        </tr>
                        <%
                                cnt++;
                                } //END FOR LOOP
                            } else {
                        %>
                        <tr>
                            <td class="t-align-left" colspan="4">
                                <b> No record found! </b>
                            </td>
                        </tr>
                        <% }%>
                     </table>
                     <%}%>

                     <%
                                if ("Services".equalsIgnoreCase(strProcurementNature)) {
                                    if ("cl".equalsIgnoreCase(strST)) {
                            %>
                            <%
                                List<SPTenderCommonData> lstAllConsultantEvalLinkStatus =
                                        tenderCS.returndata("getAllConsultantEvaluationLinkStatus", tenderId, sentBy);

                                if (!lstAllConsultantEvalLinkStatus.isEmpty()) {
                                    if ("Yes".equalsIgnoreCase(lstAllConsultantEvalLinkStatus.get(0).getFieldName1())) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td style="width: 40%" class="t-align-left ff">
                                        All Consultant Evaluation
                                    </td>
                                    <td class="t-align-left">
                                        &nbsp;&nbsp;<a href="EvalServiceReport.jsp?tenderId=<%=tenderId%>&evalCount=<%=evalCount%>&comType=<%=strComType%>">View All Consultant Evaluation Report</a>
                                    </td>
                                </tr>
                            </table>
                            <%        }
                                }

                                lstAllConsultantEvalLinkStatus = null;
                            %>

                            <%
                                    } else if ("rp".equalsIgnoreCase(strST)) {
                            %>
                            <%
                                List<SPTenderCommonData> lstOverallTechScoreLinkStatus =
                                        tenderCS.returndata("getOverallTechScore_ForEvaluation", tenderId, null);

                                if (!lstOverallTechScoreLinkStatus.isEmpty()) {
                                    if (!"0".equalsIgnoreCase(lstOverallTechScoreLinkStatus.get(0).getFieldName1())) {
                                // Show this link only when all TEC members (except chairperson) have notified to the chairperson                                
                            %>
<!--                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td style="width: 40%" class="t-align-left ff">
                                        Overall Technical Evaluation Report
                                    </td>
                                    <td class="t-align-left">
                                        &nbsp;&nbsp;<a href="TechScore.jsp?tenderId=<%=tenderId%>&comType=<%=strComType%>&evalCount=<%=evalCount%>">View Overall Technical Evaluation Report</a>
                                    </td>
                                </tr>
                            </table>-->
                            <%                                
                                    }
                                }

                                lstOverallTechScoreLinkStatus = null;
                            }
                                }
                            %>

                            <%
                     if (!"cl".equalsIgnoreCase(strST)) {
                                    List<SPTenderCommonData> evalMemStatusCount =
                                            tenderCS.returndata("GetEvalMemfinalStatusCount", tenderId, "0");
                                    List<SPTenderCommonData> getBidderEvaluatedCount =
                                            tenderCS.returndata("getBidderEvaluatedCount", tenderId, "0");
                            // IF THIS COUNT MATCHED WITH BIDDERS COUNT THEN SHOW BELOW BUTTON

                            if (Integer.parseInt(evalMemStatusCount.get(0).getFieldName1()) == getBidderEvaluatedCount.size()) {
                      %>
                            <table width="100%" cellspacing="0" class="tableList_1  t_space" style="display: none;">
                            <tr>
                                <th  width="58%" class="t-align-center">Report Name</th>
                                <th  width="24%" class="t-align-center">Action</th>
                            </tr>
                            <%
                                    String tendId = request.getParameter("tenderId");
                                    String evalType = creationService.getTenderEvalType(tendId);
                                    String pgName = null;
                                    if (evalType.equalsIgnoreCase("item wise")) {
                                        pgName = "ItemWiseReport";
                                    } else {
                                        pgName = "TenderReport";
                                    }
                                    int intRptCnt=0;
                                    for (Object[] obj : creationService.getRepNameId(tendId)) {
                                        intRptCnt++;
                             %>
                            <tr>
                                <td><%=obj[1]%></td>
                                <td class="t-align-center"><a href="<%=pgName%>.jsp?tenderid=<%=tendId%>&repId=<%=obj[0]%>&isEval=y&comType=<%=strComType%>">Price Bid Comparative</a></td>
                            </tr>
                        <%}%>
                        <%if(intRptCnt==0){%>
                        <tr>
                            <td colspan="2" class="t-align-center"><b> No record found! </b></td>
                        </tr>
                        <%}%>
                     </table>

                     <br/>

                        <!-- Send to AA
                        %
                            boolean isSentToAA = false;

                            List<SPTenderCommonData> lstgetSentToAAStatus = tenderCS.returndata("getSentToAAEntry", tenderId, null);
                            if (!lstgetSentToAAStatus.isEmpty()) {
                                if ("yes".equalsIgnoreCase(lstgetSentToAAStatus.get(0).getFieldName1())) {
                                        isSentToAA = true; // Report Sent to Approving Authority
                                }
                            }
                        %>
                            < %if (!isSentToAA) {%>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <td>
                                        <a href="TECReport.jsp?tenderId=< %=tenderId%>&st=rp&comType=< %=strComType%>"><font size="2"><b>Send Report to AA</b></font></a>
                                </td>
                            </tr>
                        </table >
                    < %}%-->

                      <%}%>


<%
boolean canDoFinalResponseProcess = false;
//CommonSearchDataMoreService tenderCS1 = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");

                       List<SPCommonSearchDataMore> lstFinalResponseShow =
                                            tenderCS1.geteGPData("getFinalResponseLinkStatus", tenderId, strEvalStatus, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null) ;

                       if(!lstFinalResponseShow.isEmpty()){
                            if("Yes".equalsIgnoreCase(lstFinalResponseShow.get(0).getFieldName1())){
                                canDoFinalResponseProcess = true;
                            }
                        }
                       //Below code of CPFinalCnt is added to check whether CP has evaluated all bidders or not in case of individual
                       int CPFinalCnt = 0;
                       String cpuserId="";
                       boolean isIndi = false;
                       if ("Services".equalsIgnoreCase(strProcurementNature)) {
                           List<SPCommonSearchDataMore> memusersId = commonSearchDataMoreService.geteGPDataMore("getMemUserIdMadeEval",tenderid);
                           if(memusersId!=null && !memusersId.isEmpty()){
                                cpuserId = memusersId.get(0).getFieldName1();
                                isIndi = memusersId.get(0).getFieldName3().equals("ind");
                            }else{
                                cpuserId = session.getAttribute("userId").toString();
                            }
                           if(cpuserId.equals(sentBy)){
                               List<SPCommonSearchDataMore> companyList = tenderCS1.geteGPData("getFinalSubComp", tenderId, sentBy, strEvalStatus) ;
                               if(!companyList.isEmpty()){
                                   for (SPCommonSearchDataMore companyname : companyList) {
                                        if ("No".equalsIgnoreCase(companyname.getFieldName6())) {
                                            CPFinalCnt++;
                                        }
                                   }
                               }
                           }
                       }
%>

<%
String strAllowFinalResponse="No";
if(!canDoFinalResponseProcess || CPFinalCnt!=0){%>
        <div class='responseMsg noticeMsg' style='margin-top: 0px;vertical-align: top;'>
            Evaluation of all the Tenderers /Consultants is still not completed by all the committee members / Nominated member.
        </div>
<%
} else {
        strAllowFinalResponse = "Yes";
%>
<jsp:include page="EvalProcessInclude.jsp">
                            <jsp:param name="tenderid" value="<%=tenderId%>"/>
                            <jsp:param name="stat" value="eval"/>
                            <jsp:param name="pnature" value="<%=strProcurementNature%>"/>
                            <jsp:param name="comtype" value="<%=strComType%>"/>
                            <jsp:param name="allowFinalResponseProcess" value="<%=strAllowFinalResponse%>" />
                            <jsp:param name="evalCount" value="<%=evalCount%>" />
                           
                      </jsp:include>

<%}%>


                      <%}
                            } else {
                                    // tahert
                                    //IF TEC MEMBERS THEN SHOW BELOW LINKS
                                                    boolean isService = false;
                                                    if ("Services".equalsIgnoreCase(strProcurementNature)) {
                                                       isService = true;
                                            //List<SPTenderCommonData> lstEvalBidderStatus =
                                              //      tenderCS.returndata("GetEvalBidderStatus", "tenderId=" + tenderId + " And userId=" + companyname.getFieldName2() + " And evalBy=" + session.getAttribute("userId").toString(), null);
                                                   }
//fdsafdsafdsafdsas

                            %>
                            <%if(showAllQryResponse){%>
                             <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td style="width: 40%" class="t-align-left ff">
                                        All Queries and Responses
                                    </td>
                                    <td class="t-align-left">
                                        &nbsp;&nbsp;
                                        <a href="AllQeryResp.jsp?tenderid=<%=tenderId%>&comType=<%=strComType%>&evalCount=<%=evalCount%>">View</a>
                                    </td>
                                </tr>
                            </table>
                                    <%}%>

                                        <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                                    <th width="5%" class="t-align-center" height="4%">Sl. No.</th>
                                    <%if(isService){%>
                                        <th width="35%" class="t-align-center" height="51%">List of Consultants</th>
                                    <%}else{%>
                                        <th width="35%" class="t-align-center" height="50%">List of Tenderers</th>
                                    <%}%>
                                    <%if(!"tsc".equalsIgnoreCase(committeetype)){%>
                                    <th width="20%" class="t-align-center" height="15%">Clarification Status</th>
                                    <%}%>
                                    <th width="35%" class="t-align-center"  height="30%"><strong>Action</strong></th>
                        </tr>
                        <%
                                    /*List<SPTenderCommonData> companyList =
                                            tenderCS.returndata("getFinalSubComp", tenderId, sentBy);
                                            //tenderCS.returndata("getFinalSubComp", tenderId, "0");*/
                         List<SPCommonSearchDataMore> companyList =
                                            tenderCS1.geteGPData("getFinalSubComp", tenderId, sentBy, strEvalStatus, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null) ;
                            if (!companyList.isEmpty()) {
                                int cnt = 1;
                            for (SPCommonSearchDataMore companyname : companyList) {
                                // HIGHLIGHT THE ALTERNATE ROW
                                //14 userId,15 tendererId,16 companyId
                        String[] jvSubData = creationService.jvSubContractChk(tenderId, companyname.getFieldName2());
                        StringBuilder name_link = new StringBuilder();
                        name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewRegistrationDetail.jsp?uId="+companyname.getFieldName2()+"&tId="+companyname.getFieldName7()+"&cId="+companyname.getFieldName8()+"&top=no' target='_blank'>"+companyname.getFieldName3()+"</a>");
                        if(jvSubData[0]!=null && jvSubData[0].equals("jvyes")){
                            name_link.delete(0, name_link.length());
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+companyname.getFieldName2()+"&noback=y' target='_blank'>"+companyname.getFieldName3()+"</a>");
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+companyname.getFieldName2()+"&noback=y' target='_blank' style='color:red'>(JVCA - View Details)</a>");
                        }
                        if(jvSubData[1]!=null && jvSubData[1].equals("subyes")){
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewTenderSubContractor.jsp?uId="+companyname.getFieldName2()+"&tenderId="+tenderId+"' target='_blank' style='color:red'>(Sub Contractor/Consultant - View Details)</a>");
                        }
                                            if (Math.IEEEremainder(cnt, 2) == 0) {
                        %>
                        <tr class="bgColor-Green">

                                    <%                                                } else {
                                    %>
                        <tr>
                                    <%                                                }
                                    %>
                                    <td class="t-align-center" ><%=cnt%></td>
                                    <td class="t-align-left" >
                                        <%=name_link%>
                              <%
                                           // if ("Yes".equalsIgnoreCase(companyname.getFieldName5()) && "No".equalsIgnoreCase(companyname.getFieldName6())) {
                               if ("No".equalsIgnoreCase(companyname.getFieldName6())) {
                                                allowNotify=false;
                                        }
                                        %>
                                    </td>
                                    <%if(!"tsc".equalsIgnoreCase(committeetype)){%>
                                    <td class="t-align-center"><%=companyname.getFieldName9()%></td>
                                    <%}%>
                                    <td class="t-align-center" >
                                        <%
                                                boolean isCPBidderEvalDone=false;
                                                boolean isEvalMemBidderEvalDone=false;
                                                boolean isBidderClariAsked = false;
                                                boolean isBidderClariGiven = false;
                                                boolean isEvalMemClariAsked = false;

                                                List<SPCommonSearchData> lstCPBidderEvalChk =
                                                commonSearchService1.searchData("getClarificationToCPLink", tenderId, companyname.getFieldName2(), strEvalStatus, String.valueOf(evalCount), null, null, null, null, null);
                                                if(!lstCPBidderEvalChk.isEmpty()){
                                                    isCPBidderEvalDone = true; // shows that evaluation CP has given finalize responsiveness process for this bidder
                                                }
                                                lstCPBidderEvalChk = null;

                                                List<SPCommonSearchData> lstBidderClariStatus =
                                                commonSearchService1.searchData("GetEvalBidderClarificationStatus", tenderId, companyname.getFieldName2(), strEvalStatus, null, null, null, null, null, null);

                                                if(!lstBidderClariStatus.isEmpty()){
                                                    isBidderClariAsked = true;
                                                    if("Yes".equalsIgnoreCase(lstBidderClariStatus.get(0).getFieldName1())){
                                                        isBidderClariGiven = true; // shows that evaluation CP has given finalize responsiveness process for this bidder
                                                    } else {
                                                        isBidderClariGiven = false;
                                                    }
                                                } else {
                                                    isBidderClariAsked = false;
                                                }
                                                lstBidderClariStatus = null;

                                                //
                                                 List<SPTenderCommonData> lstMemBidderEvalStatus =
                                                    tenderCS.returndata("getEvaluateTenderer_linkStatusForTECMember", tenderId, sentBy);

                                                    if(!lstMemBidderEvalStatus.isEmpty()){
                                                        if(Integer.parseInt(lstMemBidderEvalStatus.get(0).getFieldName1()) > 0){
                                                            isEvalMemBidderEvalDone = true;
                                                        }
                                                    }
                                                //

                                                 //=====
                                                  // List<SPTenderCommonData> lstEvalMemClariAskedStatus =
                                                    //       tenderCS.returndata("GetEvalCPClarificationExist", companyname.getFieldName2(), sentBy);

                                                 List<SPCommonSearchDataMore> lstEvalMemClariAskedStatus =
                                                         tenderCS1.geteGPData("GetEvalCPClarificationExist", companyname.getFieldName2(), sentBy, tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                                 if (!lstEvalMemClariAskedStatus.isEmpty()) {
                                                     isEvalMemClariAsked = true;
                                                 }


                                                 //=====


                                    List<SPTenderCommonData> evalBidderRespExist =
                                            tenderCS.returndata("GetEvalBidderRespTenderId", tenderId, companyname.getFieldName2());

                                            //if (evalBidderRespExist.isEmpty()) {
                                            if (!isCPBidderEvalDone && !isEvalNotificationGiven &&  !isBidderClariAsked && !isQueSentToCP) {
                              %>
                                        &nbsp;&nbsp;<a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>">Seek Clarification from <%=isService?"Consultant":"Tenderer"%></a><br>
                              <%
                                                }
                                            //}

                                // SET THE LINK OF "NO QUESTION" WHEN NO QUESTION FOUND FOR ANY BIDDER
                                            List<SPTenderCommonData> evalFormQuesCount =
                                                    tenderCS.returndata("GetEvalFormQuesCount", tenderId, companyname.getFieldName2());
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              String questCount = evalFormQuesCount.get(0).getFieldName1();

                                            if (!isCPBidderEvalDone && !isEvalNotificationGiven && !isBidderClariAsked && !isQueSentToCP && Integer.parseInt(questCount) == 0) {
                                                // if (!isQueSentToCP) {
                               %>
                                        &nbsp;&nbsp;<label>No Question Posted yet</label><br>
                                        <%
                                                // }

                                            } else {
                                    // SHOW BELOW LINK IF - "EvalBidderResponse" EXISTS
                                                // If Questions Count > 0
                                            //if (evalBidderRespExist.isEmpty()) {
                                                if (isBidderClariAsked) {
                                %>
                                        &nbsp;&nbsp<label>Posted To Bidder/Consultant</label><br>
                                        &nbsp;&nbsp<a href="EvalViewQue.jsp?tenderId=<%=tenderId%>&uid=<%=companyname.getFieldName2()%>&st=<%=strST%>&comType=<%=strComType%>">View Query / Clarification</a><br>
                              <%
                                                }else if(Integer.parseInt(questCount)>0){%>
                                                  &nbsp;&nbsp<a href="EvalViewQue.jsp?tenderId=<%=tenderId%>&uid=<%=companyname.getFieldName2()%>&st=<%=strST%>&comType=<%=strComType%>&qtype=all">View Query / Clarification</a><br>
                                                <%}
                                            }
                                        %>

                                        <%
                                // VISIBLE BELOW LINK, IF RESPONSE TIME IS OVER
                                        boolean bol_linkEnable = false;
                                        List<SPCommonSearchDataMore> getLinkForEdit = tenderCS1.geteGPData("getLinkForEditViewEvalTenderer",tenderId,companyname.getFieldName2(),userid);
                                        if(getLinkForEdit!=null && !getLinkForEdit.isEmpty() && !"0".equalsIgnoreCase(getLinkForEdit.get(0).getFieldName1())){
                                            bol_linkEnable = true;
                                            getLinkForEdit.clear();
                                            getLinkForEdit = null;
                                        }

                                    if (Integer.parseInt(companyname.getFieldName4()) < 0) {

                                            if ("Services".equalsIgnoreCase(strProcurementNature)) {

                                            List<SPTenderCommonData> lstEvalBidderStatus =
                                                    tenderCS.returndata("GetEvalBidderStatus", "tenderId=" + tenderId + " And userId=" + companyname.getFieldName2() + " And evalBy=" + session.getAttribute("userId").toString()+ " And evalCount=" + evalCount, null); //Change by dohatec for re-evaluation

                                            if (!lstEvalBidderStatus.isEmpty()) {
                                                if ("Yes".equalsIgnoreCase(lstEvalBidderStatus.get(0).getFieldName1())) {
                              %>
                                        &nbsp;&nbsp<a href="EvalBidderMarks.jsp?tenderId=<%=tenderId%>&uId=<%=companyname.getFieldName2()%>&comType=<%=strComType%>">View Evaluation Status</a><br>

                                        <%
                                                }
                                            }
                                            lstEvalBidderStatus = null;
                                        %>
                                        <%

                                            List<SPTenderCommonData> lstEvalTenderLinkStatus =
                                                    tenderCS.returndata("getEvaluateTenderer_linkStatus", "tenderId=" + tenderId + " And userId=" + companyname.getFieldName2() + " And evalBy=" + session.getAttribute("userId").toString()+ " And evalCount=" + evalCount, null); //Change by dohatec for re-evaluation

                                            if (!lstEvalTenderLinkStatus.isEmpty()) {
                                                if ("No".equalsIgnoreCase(lstEvalTenderLinkStatus.get(0).getFieldName1())) {
                                                    if(!isEvalMemBidderEvalDone){
                                                        if(bol_linkEnable){%>
                                                            &nbsp;&nbsp<a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&lnk=et&comType=<%=strComType%>">Edit</a> | <a href="EvalBidderMarks.jsp?tenderId=<%=tenderId%>&uId=<%=companyname.getFieldName2()%>&comType=<%=strComType%>">View</a><br>
                                                        <%}else{
                                        %>
                                        &nbsp;&nbsp<a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&lnk=et&comType=<%=strComType%>">Evaluate <%=isService?"Consultant":"Tenderer"%></a><br>
                                        <%
                                                   }
                                                    }
                                                }
                                            }

                                            lstEvalTenderLinkStatus = null;
                                        %>

                                        <%
                                            } else {
                                                // GOODS AND WORKS CASE
                                                if(!isEvalMemBidderEvalDone){
                                                    if(bol_linkEnable){%>
                                                    &nbsp;&nbsp<a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&lnk=et&comType=<%=strComType%>">Edit</a> | &nbsp;&nbsp<a href="EvalViewStatus.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>">View</a><br>
                                                    <%}else{

                                        %>
                                        &nbsp;&nbsp<a href="SeekEvalClari.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&lnk=et&comType=<%=strComType%>">Evaluate <%=isService?"Consultant":"Tenderer"%></a><br>
                                        <%
                                               }
                                                } else {
                                        %>
                                        &nbsp;&nbsp<a href="EvalViewStatus.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>">View Evaluation Status</a><br>
                                         <%
                                                }
                                        } // GOODS AND WORKS CASE ENDS

                                            }
                                    //  SHOW BELOW "Give Answer To CP" LINK, IF FOUND IN "tbl_EvalCpMemClarification"

                                    List<SPCommonSearchDataMore> lstFinalResponseEdits = commonSearchDataMoreService.geteGPDataMore("getFinalResponseEditstatus", tenderId, "0",(isService ? "ter1" : "ter2"),String.valueOf(evalCount));
                                    boolean isTER2SignByAll = false;
                                    if(lstFinalResponseEdits!=null && (!lstFinalResponseEdits.isEmpty()) && lstFinalResponseEdits.get(0).getFieldName3().equalsIgnoreCase("block")){
                                        isTER2SignByAll = true;
                                    }
                                                if ((!isCPBidderEvalDone && isEvalMemClariAsked)) {
                              %>
                                        &nbsp;&nbsp<a href="EvalanstoCP.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>">Give Clarification To Chair Person</a><br>
                              <%
                                            } else if (isCPBidderEvalDone && isEvalMemClariAsked) {
                                                if(!isTER2SignByAll){
                                  %>
                                        &nbsp;&nbsp<a href="EvalanstoCP.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>">Give Clarification To Chair Person</a><br>

                                        <%}else{%>
                                            &nbsp;&nbsp<a href="EvalViewanstoCp.jsp?uId=<%=companyname.getFieldName2()%>&tenderId=<%=tenderId%>&st=<%=strST%>&comType=<%=strComType%>">View Posted Clarification</a><br>
                                        <%}}%>
                                   </td>
                        </tr>
                        <%
                                cnt++;
                                } //END FOR LOOP
                                } else {
                                %>
                                <tr>
                                    <td class="t-align-left" colspan="4">
                                        <b> No record found! </b>
                                    </td>
                                </tr>
                                <% }%>
                            </table>
                            <%
                            String evalType = "eval";
                            if (!companyList.isEmpty()) {
                                boolean isTERfirst = false;
                                boolean isTERsecond = false;
                                boolean isNotified = false;
                                List<SPTenderCommonData> lstTECMemberFinalNotification =
                                        tenderCS.returndata("GetEvalTECMemberFinalNotification", tenderId, sentBy);
                                if (!lstTECMemberFinalNotification.isEmpty()) {

                                    if (Integer.parseInt(lstTECMemberFinalNotification.get(0).getFieldName1()) > 0) {
                            // IF TEC/PEC Member has done evaluation for atleast one bidder
                                        if (Integer.parseInt(lstTECMemberFinalNotification.get(0).getFieldName2()) == 0) {
                            // IF TEC/PEC Member has yet given the final notification to chairperson
                             if (allowNotify) {
                                isTERfirst = true;
                                 }// end if  - notify count
                                 isTERsecond = true;

                                        } // end if - bidders count func.
                                        else{
                                            isNotified=true; // Member has already given final notification to CP
                                        }
                                    }
                                    String lotPkgLab = null;
                                    List<SPTenderCommonData> tenderLotList = null;
                                    String repLabel = "Tender";
                                    if(afterLoginTSCTabprocNature.toString().equals("Services")){
                                        lotPkgLab = "Package";
                                        repLabel = "Proposal";
                                        tenderLotList = tenderCommonService.returndata("getlotorpackagebytenderid",tenderId,"Package,1");
                                    }else{
                                        lotPkgLab = "Lot";
                                        tenderLotList = tenderCommonService.returndata("getProcurementNatureForEval", tenderId, null);
                                    }
                                    int configCount = 0;
                                    if(!afterLoginTSCTabprocNature.toString().equals("Services")){
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="15%"><%=lotPkgLab%> No.</th>
                                    <th width="50%"><%=lotPkgLab%> Description</th>
                                    <th width="35%">Action</th>
                                </tr>
                                <%
                                    for (SPTenderCommonData tenderLot : tenderLotList) {
                                        boolean isTER1Config = false;
                                        boolean isTER2Config = false;
                                        if(creationService.evalRepCount(tenderId, tenderLot.getFieldName3(), evalType, "TER1","createdBy="+session.getAttribute("userId").toString(),evalCount)!=0){
                                            isTER1Config = true;
                                            configCount++;
                                        }
                                        if(creationService.evalRepCount(tenderId, tenderLot.getFieldName3(), evalType, "TER2","createdBy="+session.getAttribute("userId").toString(),evalCount)!=0){
                                            isTER2Config = true;
                                            configCount++;
                                        }
                                        int cntChk=0;
                                %>
                                <tr>
                                    <td class="t-align-center"><%=tenderLot.getFieldName1()%></td>
                                    <td><%=tenderLot.getFieldName2()%></td>
                                    <td class="t-align-center">
                                        <%if(isTERfirst && !isNotified){%>
                                        <a href="<%=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%=tenderId%>&lotId=<%=tenderLot.getFieldName3()%>&config=y&stat=<%=evalType%>&frm=cl&parentLink=936&evalCount=<%=String.valueOf(evalCount)%>">Fill Evaluation Form (<%=repLabel%> Evaluation Report 1)</a>&nbsp;|&nbsp;
                                        <%cntChk++;}%>
                                        <%if(isTER1Config){%>
                                        <a href="<%=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%=tenderId%>&lotId=<%=tenderLot.getFieldName3()%>&isview=y&stat=<%=evalType%>&ismem=y&frm=cl&evalCount=<%=String.valueOf(evalCount)%>">View <%=repLabel%> Evaluation Report 1</a>
                                        <%cntChk++;}%>
                                        <br/>
                                        <%if(isTERfirst && !isNotified){%>
                                        <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderId%>&lotId=<%=tenderLot.getFieldName3()%>&config=y&stat=<%=evalType%>&frm=cl&parentLink=938&evalCount=<%=String.valueOf(evalCount)%>">Fill Evaluation Form (<%=repLabel%> Evaluation Report 2)</a>&nbsp;|&nbsp;
                                        <%cntChk++;}%>
                                        <%if(isTER2Config){%>
                                        <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderId%>&lotId=<%=tenderLot.getFieldName3()%>&isview=y&stat=<%=evalType%>&ismem=y&frm=cl&evalCount=<%=String.valueOf(evalCount)%>">View <%=repLabel%> Evaluation Report 2</a>
                                        <%cntChk++;}%>
                                        <%if(cntChk==0){out.print("<span class='ff'>Evaluation Pending</span>");}%>
                                    </td>
                                </tr>
                                <%}%>
                            </table>
                             <%}%>
                            <%

                             if (isTECCnt == 0) {

                                    List<SPTenderCommonData> lstTECMemberQuecnt =
                                            tenderCS.returndata("getTECMemberQuecnt", tenderId, sentBy);
                                    if (Integer.parseInt(lstTECMemberQuecnt.get(0).getFieldName1()) > 0) {
                                        btnSendToTEC = true;
                                    }
                                    lstTECMemberQuecnt = null;
                            } // end if
                             if((configCount==(tenderLotList.size()*2) || (allowNotify && afterLoginTSCTabprocNature.toString().equals("Services"))) && !isNotified){
                              btnNotifyToTEC = true;
                          }
                        }
                            if(isTERsecond && !allowNotify){
                        %>
                            <script>
                                $(document).ready(function(){
                                    $('#btnNotifyToTEC').click(function(){
                                        //alert('Evaluation for all forms has not been completed for one or more bidders.');
                                        alert('Evaluation of all the Tenderers / Consultants are pending.');
                                        return false;
                                    });
                                });
                            </script>
                            <%}
                            %>

                            <%}%>

                            <%
                            // All Consultant Evaluation
                                if ("Services".equalsIgnoreCase(strProcurementNature)) {
                                    if ("cl".equalsIgnoreCase(strST)) {
                            %>
                            <%
                                List<SPTenderCommonData> lstAllConsultantEvalLinkStatus =
                                        tenderCS.returndata("getAllConsultantEvaluationLinkStatus", tenderId, sentBy);

                                if (!lstAllConsultantEvalLinkStatus.isEmpty()) {
                                    if ("Yes".equalsIgnoreCase(lstAllConsultantEvalLinkStatus.get(0).getFieldName1())) {
                            %>

                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                                    <td width="40%" class="t-align-left ff">All Consultant Evaluation</td>
                                    <td class="t-align-left">
                                        &nbsp;&nbsp;<a href="EvalServiceReport.jsp?tenderId=<%=tenderId%>&evalCount=<%=evalCount%>&comType=<%=strComType%>">View All Consultant Evaluation Report</a>
                            </td>
                        </tr>
                     </table>
                            <%
                                }
                                        }
                                        lstAllConsultantEvalLinkStatus = null;
                                    }
                                }
                            %>

                            <%
                                }//END TEC MEM

                            // End :IF Code for TEC Committe
                            } else {
                                // IF TSC COMMITTEE
                                             EvalTSCNotificationService etscns = (EvalTSCNotificationService) AppContext.getSpringBean("EvalTSCNotificationService");
                                                List<TblEvalTscnotification> tets = etscns.findEvalTscNtfctn(tenderId);

                                                if(!tets.isEmpty()){
                                                isTSCNotificationDone = true;
                                                }

                                List<SPTenderCommonData> evalTscReq =
                                        tenderCS.returndata("getEvalConfigType", tenderId, null);

                                if ("yes".equalsIgnoreCase(evalTscReq.get(0).getFieldName2())) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                                    <th width="5%" class="t-align-center" height="4%">Sl. No.</th>
                                    <th width="35%" class="t-align-center" height="51%">List of Tenderers/Consultants</th>
                                    <%if(!"tsc".equalsIgnoreCase(committeetype)){%>
                                    <th width="20%" class="t-align-center" height="15%">Clarification Status</th>
                                    <%}%>
                                    <th width="35%" class="t-align-center"  height="30%"><strong>Action</strong></th>
                        </tr>
                        <%
                                    /*List<SPTenderCommonData> companyList =
                                            tenderCS.returndata("getFinalSubComp", tenderId, "0");*/
                        List<SPCommonSearchDataMore> companyList =
                                            tenderCS1.geteGPData("getFinalSubComp", tenderId, "0", strEvalStatus, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null) ;
                            if (!companyList.isEmpty()) {
                                int cnt = 1;
                            for (SPCommonSearchDataMore companyname : companyList) {
                                //14 userId,15 tendererId,16 companyId
                        String[] jvSubData = creationService.jvSubContractChk(tenderId, companyname.getFieldName2());
                        StringBuilder name_link = new StringBuilder();
                        name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewRegistrationDetail.jsp?uId="+companyname.getFieldName2()+"&tId="+companyname.getFieldName7()+"&cId="+companyname.getFieldName8()+"&top=no' target='_blank'>"+companyname.getFieldName3()+"</a>");
                        if(jvSubData[0]!=null && jvSubData[0].equals("jvyes")){
                            name_link.delete(0, name_link.length());
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+companyname.getFieldName2()+"&noback=y' target='_blank'>"+companyname.getFieldName3()+"</a>");
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+companyname.getFieldName2()+"&noback=y' target='_blank' style='color:red'>(JVCA - View Details)</a>");
                        }
                        if(jvSubData[1]!=null && jvSubData[1].equals("subyes")){
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewTenderSubContractor.jsp?uId="+companyname.getFieldName2()+"&tenderId="+tenderId+"' target='_blank' style='color:red'>(Sub Contractor/Consultant - View Details)</a>");
                        }
                                // HIGHLIGHT THE ALTERNATE ROW
                                            if (Math.IEEEremainder(cnt, 2) == 0) {
                        %>
                        <tr class="bgColor-Green">
                                    <% } else {%>
                        <tr>
                                    <% }%>
                          <td class="t-align-center" ><%=cnt%></td>
                          <td class="t-align-left" ><%=name_link%></td>
                          <%if(!"tsc".equalsIgnoreCase(committeetype)){%>
                          <td class="t-align-center" ><%=companyname.getFieldName9()%></td>
                          <%}%>
                          <td class="t-align-center">
                              <%
                              String strTSCPost="Post Comments";
                              String qStrTSCPost="";
                                if(isTSCNotificationDone){
                                    strTSCPost="View Comments";
                                    qStrTSCPost="&notify=y";
                                }
                                %>

                              <a href="ViewTscForm.jsp?tenderId=<%=tenderId%>&uId=<%=companyname.getFieldName2()%>&st=<%=strST%>&comType=<%=strComType%><%=qStrTSCPost%>"><%=strTSCPost%></a> |
                                        <a href="EvalViewQue.jsp?tenderId=<%=tenderId%>&uid=<%=companyname.getFieldName2()%>&st=<%=strST%>&comType=<%=strComType%>">View Query / Clarification</a>
                          </td>
                        </tr>
                        <%
                                cnt++;
                                } //END FOR LOOP
                            } else {
                        %>
                        <tr>
                            <td class="t-align-left" colspan="4">
                                <b> No record found! </b>
                            </td>
                        </tr>
                        <% }%>
                     </table>

                            <%
                                    }
                                } // TSC Mem Code End

                            %>
                            <%} else {%>
                         <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td colspan="3">Not Authorised Member</td>
                        </tr>
                         </table>
                            <%}%>

                            <%if(!isExternalMem) {%>
                        </div>
                        <%}%>
<%
                            List<SPCommonSearchDataMore> sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("TSCUploadReport", tenderId, sentBy, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                            EvalTscCommentsSrBean etcsb = new EvalTscCommentsSrBean();
                            boolean flag = etcsb.displayNotifyButton(Integer.parseInt(tenderId));
                            boolean isTSCCP = false;
                            List<SPCommonSearchDataMore> sPCommonSearchDataMoresTSC = commonSearchDataMoreService.geteGPData("chkTSCMember", tenderId, sentBy, "1", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            if(sPCommonSearchDataMoresTSC!=null && (!sPCommonSearchDataMoresTSC.isEmpty()) && (!sPCommonSearchDataMoresTSC.get(0).getFieldName1().equals("0"))){
                                isTSCCP=true;
                            }
                            //if(tets.isEmpty() && !flag && isTSCCP){
                            if(!isTSCNotificationDone && flag && isTSCCP){
                        %>
                       <div class="t-align-center t_space">
                        <label class="formBtn_1">
                            <input name="btnnotifytope" type="button" value="Notify Evaluation Committee for Completion of Evaluation" onclick="notify();"/>
                        </label>
                     </div>
                       <%
                            if(!sPCommonSearchDataMores.isEmpty() && sPCommonSearchDataMores.get(0).getFieldName1().equalsIgnoreCase("yes")){
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr><td>
                            <a href="TSCUploadReport.jsp?tenderid=<%=request.getParameter("tenderId")%>&st=cl">Upload Recommendation Report</a>
                            </td></tr>
                            </table>
                            <% } }
                            CommonSearchDataMoreService service = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> list5 = service.geteGPData("CheckForTEC", request.getParameter("tenderId"), userid, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            if(!list5.isEmpty() && list5!=null && "cl".equalsIgnoreCase(strST) && list5.get(0).getFieldName3().equalsIgnoreCase("yes")){
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr><td>
                            <a href="TSCUploadReport.jsp?tenderid=<%=request.getParameter("tenderId")%>&st=cl&download=on"> Download TSC's Recommendation Report</a>
                            </td></tr>
                           </table>
                            <% } %>
                            <%if(btnSendToTEC){%>
                            <div class="t-align-center t_space">
                                <label class="formBtn_1">
                                            <input name="btnSendToTEC" type="submit" value="Click here to notify Chairperson once you have posted all questions" />
                                </label>
                            </div>
                            <%}%>
                            <%if(btnNotifyToTEC){%>
                            <div class="t-align-center  t_space">
                                <label class="formBtn_1">
                                            <input id="btnNotifyToTEC" name="btnNotifyToTEC" type="submit" value="Notify Chairperson, if Evaluation is finalized" />
                                </label>
                            </div>
                            <%}%>


                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
        </form>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabEval");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>