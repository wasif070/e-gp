<%--
    Document   : TenderClosing
    Created on : Dec 19, 2010, 4:28:24 PM
    Author     : Administrator
--%>

<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            <%if (request.getParameter("type").equalsIgnoreCase("C"))
                {%>
            Close Tender
            <% }
            else if (request.getParameter("type").equalsIgnoreCase("CP"))
            {%>
            Send to TEC Chairperson
            <% }
            else if (request.getParameter("type").equalsIgnoreCase("P"))
            {%>
            Send to PA/AU
            <%}%>
        </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $("#frmTenderCl").validate({
                    rules: {
                        txtComments:{required:true,maxlength:1000}
                    },
                    messages: {
                        txtComments:{required:"<div class='reqF_1'>Please enter Comments.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 1000 characters are allowed.</div>"}
                    }
                });

            });

            function checkSubmit(){
                var vbool = false;
                if($("#frmTenderCl").valid()){
                    $('#hdnbuttonTenderClose').attr("disabled","disabled");
                    $('#buttonTenderClose').val("Submit");
                    vbool = true;
                }
                return vbool;
            }
        </script>
    </head>
    <body>
        <%
            TenderCommonService objTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            String tenderId = "", userId = "", strGovUserId = "0", strRedirectPath = "";
            tenderId = request.getParameter("tenderId");
            HttpSession hs = request.getSession();
            if (hs.getAttribute("userId") != null)
            {
                userId = hs.getAttribute("userId").toString();
            }

            if (hs.getAttribute("govUserId") != null)
            {
                strGovUserId = hs.getAttribute("govUserId").toString();
            }

            /*  Added By Darshan for checking tender alredy closed or not
            *   Eventun issue id 510 Fixed date : 08/03/2013
            */
             SPTenderCommonData tenderCommonData = objTenderCommonService.getClosedTenderInfo(tenderId);
            // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType = "tenderId";
            int auditId = Integer.parseInt(tenderId);
            String moduleName = EgpModule.Tender_Opening.getName();
            String auditAction = null;
            String remarks = "";
        %>

        <%
            HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
            List<SPTenderCommonData> listForSendToNm;
            String typ = "", nmSentTo = "";

            if (request.getParameter("type") != null)
            {
                typ = request.getParameter("type");
            }

            if ("P".equalsIgnoreCase(typ))
            {
                // Get PE Name
                listForSendToNm = objTenderCommonService.returndata("getSentToUserName", tenderId, "getPEName");
                if (!listForSendToNm.isEmpty())
                {
                    if (!"null".equalsIgnoreCase(listForSendToNm.get(0).getFieldName1()))
                    {
                        nmSentTo = listForSendToNm.get(0).getFieldName1();
                    }
                }
                listForSendToNm = null;
            }
            else if ("CP".equalsIgnoreCase(typ))
            {
                // Get TEC/PEC Chairperson Name
                listForSendToNm = objTenderCommonService.returndata("getSentToUserName", tenderId, "getTECChairPersonName");
                if (!listForSendToNm.isEmpty())
                {
                    if (!"null".equalsIgnoreCase(listForSendToNm.get(0).getFieldName1()))
                    {
                        nmSentTo = listForSendToNm.get(0).getFieldName1();
                    }
                }
                listForSendToNm = null;
            }

            if (request.getParameter("buttonTenderClose") != null)
            {
                String tenderXml = "", strTenREfNo = "";
                if (request.getParameter("hdnTenderRefNo") != null)
                {
                    strTenREfNo = request.getParameter("hdnTenderRefNo");
                }

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


                if (request.getParameter("type").equalsIgnoreCase("C"))
                {
                    // Tender Close Case
                    tenderXml = "<root><tbl_TenderClose tenderId=\"" + tenderId
                            + "\" closingDate=\"" + format.format(new Date()) + "\" "
                            + "comments=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtComments"))
                            + "\" createdBy=\"" + userId + "\" "
                            + "closedByGovUserId=\"" + strGovUserId + "\" /></root>";

                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                    CommonMsgChk commonMsgChk =
                            commonXMLSPService.insertDataBySP("insert", "tbl_TenderClose", tenderXml, "").get(0);

                    remarks = request.getParameter("txtComments");

                    if (commonMsgChk.getFlag().equals(true))
                    {
                        // Code added by Dipal For generate Audit Trail
                        auditAction = "Tender Opening Process Closed";
                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                        //response.sendRedirect("OpenComm.jsp?tenderid=" + tenderId + "&msgId=tenderclosed");
                        strRedirectPath = "OpenComm.jsp?tenderid=" + tenderId + "&msgId=tenderclosed";
                    }
                    else
                    {
                        // Code added by Dipal For generate Audit Trail
                        auditAction = "Error in Tender Opening Process Closed";
                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                        //response.sendRedirect("TenderClosing.jsp?tenderid=" + tenderId + "&type=" + request.getParameter("type") + "&msgId=error");
                        strRedirectPath = "TenderClosing.jsp?tenderid=" + tenderId + "&type=" + request.getParameter("type") + "&msgId=error";
                    }

                }
                else if (request.getParameter("type").equalsIgnoreCase("CP"))
                {
                    // FOR SENT TO TEC/PEC CHAIRPERSON CASE

                    String peOfficerUserId = "";
                    String peOfficeNm = "";
                    String userId_TEC_CP = "";
                    String userNm_TEC_CP = "";
                    String govUserId_TEC_CP = "";
                    String listId = "";

                    List<SPTenderCommonData> lstOfficerUserId =
                            objTenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
                    if (!lstOfficerUserId.isEmpty())
                    {
                        peOfficerUserId = lstOfficerUserId.get(0).getFieldName1(); // Get PE UserId
                        peOfficeNm = lstOfficerUserId.get(0).getFieldName3(); // Get PE Name
                    }
                    lstOfficerUserId = null;

                    if (!userId.equalsIgnoreCase(peOfficerUserId))
                    {
                        // Current User is Not the PE Officer
                        response.sendRedirect("OpenComm.jsp?tenderid=" + tenderId + "&msgId=error");
                        //strRedirectPath="OpenComm.jsp?tenderid=" + tenderId + "&msgId=error";
                    }

                    List<SPTenderCommonData> lstTEC_CPInfo =
                            objTenderCommonService.returndata("EvalTECChairPerson", tenderId, "");


                    if (!lstTEC_CPInfo.isEmpty())
                    {

                        userId_TEC_CP = lstTEC_CPInfo.get(0).getFieldName1(); // Get TEC/PEC Chairperson UserId
                        userNm_TEC_CP = lstTEC_CPInfo.get(0).getFieldName4(); // Get TEC/PEC Chairperson Name
                        govUserId_TEC_CP = lstTEC_CPInfo.get(0).getFieldName5(); // Get TEC/PEC Chairperson GovUserId
                        listId = lstTEC_CPInfo.get(0).getFieldName3();

                        tenderXml = "<root><tbl_TosRptShare listingId=\"" + listId + "\" tenderId=\"" + tenderId + "\" comments=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtComments")) + "\" sentBy=\"" + userId + "\" sentTo=\"" + userId_TEC_CP + "\" sentDate=\"" + format.format(new Date()) + "\" sentByGovUserId=\"" + strGovUserId + "\" sentToGovUserId=\"" + govUserId_TEC_CP + "\" /></root>";

                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TosRptShare", tenderXml, "").get(0);

                        if (commonMsgChk.getFlag().equals(true))
                        {
                            AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
                            //for 1st phase
                            commonMsgChk = addUpdate.addUpdOpeningEvaluation("AutoInsertEvalConfigNomination",tenderId,userId_TEC_CP).get(0); 
                            if(commonMsgChk.getFlag().equals(true)){
                            /* START : CODE TO SEND MAIL */
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            String mailSubject = "e-GP System: PA has sent TOR Report to you";
                            String strFrmEmailId = "";

                            List<SPTenderCommonData> frmEmail =
                                    objTenderCommonService.returndata("getEmailIdfromUserId", userId, null);
                            if (!frmEmail.isEmpty())
                            {
                                strFrmEmailId = frmEmail.get(0).getFieldName1(); // Email Id of Sender (PE)
                            }


                            List<SPTenderCommonData> emails =
                                    objTenderCommonService.returndata("getEmailIdfromUserId", userId_TEC_CP, null);

                            String[] mail =
                            {
                                emails.get(0).getFieldName1()
                            }; // Email Id of TEC/PEC Chairperson

                            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();

                            userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contTOR_POR_SentToTEC_CP(tenderId, strTenREfNo, frmEmail.get(0).getFieldName1(), peOfficeNm));

                            msgBoxContentUtility = null;
                            /* END : CODE TO SEND MAIL */

                            strFrmEmailId = null;
                            frmEmail = null;

                            //response.sendRedirect("OpenComm.jsp?tenderid=" + tenderId + "&msgId=sendtocp");
                            strRedirectPath = "OpenComm.jsp?tenderid=" + tenderId + "&msgId=sendtocp";
                            }
                        }
                        else
                        {
                            //response.sendRedirect("TenderClosing.jsp?tenderid=" + tenderId + "&type=" + request.getParameter("type") + "&msgId=error");
                            strRedirectPath = "TenderClosing.jsp?tenderid=" + tenderId + "&type=" + request.getParameter("type") + "&msgId=error";
                        }

                    }

                    lstTEC_CPInfo = null;

                    peOfficerUserId = null;
                    peOfficeNm = null;
                    userId_TEC_CP = null;
                    userNm_TEC_CP = null;
                    listId = null;

                }
                else
                {
                    // Sent to PE Case
                    String PEId = "";
                    String PEName = "";
                    String PE_GovUserId = "";

                    List<SPTenderCommonData> peId =
                            objTenderCommonService.returndata("GetUseridForPE", tenderId, "");

                    if (!peId.isEmpty())
                    {
                        PEId = peId.get(0).getFieldName1();
                        PEName = peId.get(0).getFieldName2();
                        PE_GovUserId = peId.get(0).getFieldName3();

                    }
                    else
                    {
                        PEId = "0";
                    }
                    peId = null;
                    /*
                    tenderXml = "<root><tbl_TOSListing tenderId=\"" + tenderId
                            + "\" sentDate=\"" + format.format(new Date()) + "\" "
                            + "comments=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtComments"))
                            + "\" sentBy=\"" + userId + "\" sentTo=\"" + PEId + "\" sentByGovUserId=\"" + strGovUserId + "\" sentToGovUserId=\"" + PE_GovUserId + "\" /></root>";
                   
                   
                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                    CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TOSListing", tenderXml, "").get(0); */
                    AddUpdateOpeningEvaluation dataMoree = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
                    CommonMsgChk commonMsgChk =
                            dataMoree.addUpdOpeningEvaluation("InsertForSendingToPE", tenderId,format.format(new Date()),handleSpecialChar.handleSpecialChar(request.getParameter("txtComments")),userId,PEId,strGovUserId,PE_GovUserId,"","","","","","","","","","","","").get(0);
                    
                    if (commonMsgChk.getFlag().equals(true))
                    {

                        String peOfficerUserId = "";
                        String peOfficeNm = "";
                        String peDepartment = "";

                        List<SPTenderCommonData> lstOfficerUserId =
                                objTenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
                        if (!lstOfficerUserId.isEmpty())
                        {
                            peOfficerUserId = lstOfficerUserId.get(0).getFieldName1();
                            peOfficeNm = lstOfficerUserId.get(0).getFieldName3();
                            peDepartment = lstOfficerUserId.get(0).getFieldName4();
                        }


                        /* START : CODE TO SEND MAIL TO PE FROM TOC CHAIRPERSON */
                        String mailSubject = "e-GP: TOR is sent by Opening Committee Chairperson";
                        String strFrmEmailId = "";

                        List<SPTenderCommonData> frmEmail =
                                objTenderCommonService.returndata("getEmailIdfromUserId", userId, null);
                        if (!frmEmail.isEmpty())
                        {
                            strFrmEmailId = frmEmail.get(0).getFieldName1();
                        }


                        List<SPTenderCommonData> emails =
                                objTenderCommonService.returndata("getEmailIdfromUserId", peOfficerUserId, null);

                        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

                        String[] mail =
                        {
                            emails.get(0).getFieldName1()
                        };

                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();

                        userRegisterService.contentAdmMsgBox(mail[0], strFrmEmailId, HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contTOR_POR_SentToPE(tenderId, strTenREfNo, strFrmEmailId, peOfficeNm, peDepartment));

                        mail = null;
                        msgBoxContentUtility = null;
                        /* END : CODE TO SEND MAIL TO PE FROM TOC CHAIRPERSON */

                        emails = null;
                        frmEmail = null;
                        lstOfficerUserId = null;

                        // Code added by Dipal For generate Audit Trail
                        remarks = request.getParameter("txtComments");
                        auditAction = "Sent Opening Reports to PA";
                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                        //response.sendRedirect("OpenComm.jsp?tenderid=" + tenderId + "&msgId=senttope");
                        strRedirectPath = "OpenComm.jsp?tenderid=" + tenderId + "&msgId=senttope";
                    }
                    else
                    {
                        // Code added by Dipal For generate Audit Trail
                        remarks = request.getParameter("txtComments");
                        auditAction = "Error in Sent Opening Reports to PA";
                        makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

                        //response.sendRedirect("TenderClosing.jsp?tenderid=" + tenderId + "&type=" + request.getParameter("type") + "&msgId=error");
                        strRedirectPath = "TenderClosing.jsp?tenderid=" + tenderId + "&type=" + request.getParameter("type") + "&msgId=error";
                    }

                    PEId = null;
                    PEName = null;

                }

                tenderXml = null;

                response.sendRedirect(strRedirectPath); // Finally Redirect
            }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">
                    <%if (request.getParameter("type").equalsIgnoreCase("C"))
                        {%>
                    Close Tender
                    <%}
                    else if (request.getParameter("type").equalsIgnoreCase("CP"))
                    {%>
                    Send to TEC Chairperson
                    <% }
                    else if (request.getParameter("type").equalsIgnoreCase("P"))
                    {%>
                    Send to PA
                    <%}%>

                    <span style="float: right;" ><a href="OpenComm.jsp?tenderid=<%=tenderId%>" class="action-button-goback">Go Back</a></span>
                </div>
                <%
                    pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
                <jsp:include page="officerTabPanel.jsp" >
                    <jsp:param name="tab" value="6" />
                </jsp:include>

                <%-- Start: CODE TO DISPLAY MESSAGES --%>
                <%
                boolean isErrorRaised =((request.getParameter("msgId") != null ||(tenderCommonData.getFieldName1() != null && !tenderCommonData.getFieldName1().equals("") &&
                            tenderCommonData.getFieldName2() != null && !tenderCommonData.getFieldName2().equals("")))&& !"P".equalsIgnoreCase(typ) && !"CP".equalsIgnoreCase(typ));
                boolean isTenderClosed = (tenderCommonData.getFieldName1() != null && !tenderCommonData.getFieldName1().equals("") &&
                            tenderCommonData.getFieldName2() != null && !tenderCommonData.getFieldName2().equals(""));
                if (isErrorRaised) {
                        String msgId = "", msgTxt = "";
                        String strPadding="";
                        boolean isError = false;
                        msgId = ((request.getParameter("msgId")==null) && isTenderClosed)?"closed":request.getParameter("msgId");
                        if (!msgId.trim().equals("")) {
                            if (msgId.equalsIgnoreCase("error"))
                            {
                                isError = true;
                                msgTxt = "There is some error.";
                            } else if(tenderCommonData.getFieldName1() != null && !tenderCommonData.getFieldName1().equals("") &&
                            tenderCommonData.getFieldName2() != null && !tenderCommonData.getFieldName2().equals("")){
                                isError = true;
                                msgTxt = "This tender is already closed by you (from other location). Please click on 'Opening' tab to view details and for further process.";
                                strPadding = "style='padding-top: 20px;'";
                            } else {
                                msgTxt = "";
                            }

                %>
                <%if (isError && msgTxt != "")
                    {%>
                    <div <%=strPadding%>>&nbsp;</div>
                <div class="responseMsg errorMsg"><%=msgTxt%></div>
                <%}
                else if (msgTxt != "")
                {%>
                <div>&nbsp;</div>
                <div class="responseMsg successMsg"><%=msgTxt%></div>
                <%}%>
                <%}
                        msgId = null;
                        msgTxt = null;
                  }%>
                <%-- End: CODE TO DISPLAY MESSAGES --%>

                <%if((tenderCommonData.getFieldName1() == null && tenderCommonData.getFieldName2() == null) || "P".equalsIgnoreCase(typ) || "CP".equalsIgnoreCase(typ)){%>
                <div class="tabPanelArea_1">
                    <form id="frmTenderCl" action="TenderClosing.jsp?tenderId=<%=tenderId%>&type=<%=request.getParameter("type")%>" method="POST" onsubmit="return checkSubmit();">
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <%if ("CP".equalsIgnoreCase(typ) || "P".equalsIgnoreCase(typ))
                                {%>
                            <% if ("P".equalsIgnoreCase(typ))
                                {%>
                            <tr>
                                <td class="ff" width="15%" align="right"> Tender Creator: &nbsp;</td>
                                <td class="t-align-left">
                                    <%=nmSentTo%>
                                </td>
                            </tr>
                            <%}
                            else if ("CP".equalsIgnoreCase(typ))
                            {%>
                            <tr>
                                <td class="ff" width="15%" align="right"> TEC Chairperson: &nbsp;</td>
                                <td class="t-align-left">
                                    <%=nmSentTo%>
                                </td>
                            </tr>
                            <%}%>
                            <%}%>

                            <tr>
                                <td class="ff" width="15%" align="right">Comments : <span class="mandatory">*</span>&nbsp;&nbsp; </td>
                                <td class="t-align-left">
                                    <textarea cols="7"  rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"
                                              style="width:90%;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="t-align-center">
                                    <label class="formBtn_1">
                                        <input type="submit" name="hdnbuttonTenderClose" id="hdnbuttonTenderClose" value="Submit"/>
                                        <input type="hidden" name="buttonTenderClose" id="buttonTenderClose"/>
                                    </label>
                                </td>
                            </tr>
                        </table>

                        <input type="hidden" name="hdnTenderRefNo" value="<%=toextTenderRefNo%>">
                    </form>
                </div>
                <div>&nbsp;</div>
                <%}%>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>

        <%
// Nullify String Variables
            tenderId = null;
            userId = null;
            strGovUserId = null;
            strRedirectPath = null;

// Nullify List Objects

        %>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
