<%--
    Document   : TenderDocPrep
    Created on : 29-Nov-2010, 12:37:32 PM
    Author     : Yagnesh
--%>

<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.List, java.util.ArrayList, com.cptu.egp.eps.model.table.TblTenderGrandSum, com.cptu.egp.eps.model.table.TblTenderForms, com.cptu.egp.eps.model.table.TblTenderCells"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderMaster, com.cptu.egp.eps.model.table.TblTenderGrandSumDetail, com.cptu.egp.eps.model.table.TblTenderCoriGrandSumDetail" %>
<jsp:useBean id="grandSummary" class="com.cptu.egp.eps.web.servicebean.GrandSummarySrBean"  />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Grand Summary</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    

    </head>
    <body>
        <%
        int tenderId = 0;
        int tenderSectionId = 0;
        int tenderGSId = 0;
        int pkgOrLotId = -1;
        if (request.getParameter("porlId") != null) {
            pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
        }
        boolean isEdit = false;
        if (request.getParameter("tenderId") != null) {
            tenderId = Integer.parseInt(request.getParameter("tenderId"));
        }
        if (request.getParameter("sectionId") != null) {
            tenderSectionId = Integer.parseInt(request.getParameter("sectionId"));
        }
        if (request.getParameter("gsId") != null) {
            tenderGSId = Integer.parseInt(request.getParameter("gsId"));
            isEdit = true;
        }
        int userId = 0;
        if (session.getAttribute("userId") != null) {
            userId = Integer.parseInt(session.getAttribute("userId").toString());
        }
        String operation = "";
        if (request.getParameter("saveGrandSummary") != null) {
            operation = request.getParameter("saveGrandSummary");
        }
        String corriId=null;
        if (request.getParameter("corriId") != null) {
            corriId = request.getParameter("corriId");
        }

        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        CommonService cservice = (CommonService) AppContext.getSpringBean("CommonService");
        String procurementMethod = "";
        procurementMethod = String.valueOf(cservice.getProcMethod(String.valueOf(tenderId)));
    

        // Following if case is perform save or edit operation into database
        if ("Save".equals(operation) || "Edit".equals(operation)) 
        {

            String gsFormId[] = request.getParameterValues("chkFormId");
            String gsName = request.getParameter("grandSummaryName");


            TblTenderGrandSum tblGS = new TblTenderGrandSum();
            tblGS.setCreatedBy(userId);
            tblGS.setCreationDate(new java.util.Date());
            tblGS.setSummaryName(gsName);
            tblGS.setPkgLotId(pkgOrLotId);
            tblGS.setTblTenderMaster(new TblTenderMaster(tenderId));

            List<TblTenderGrandSumDetail> lstGsDtl = new ArrayList<TblTenderGrandSumDetail>();
            List<TblTenderCoriGrandSumDetail> lstCorriGsDtl = new ArrayList<TblTenderCoriGrandSumDetail>();
            TblTenderGrandSumDetail tblGSDtl = null;
            TblTenderCoriGrandSumDetail tblCorriGSDtl = null;
            if(request.getParameterValues("chkFormId") != null)
            {
                if(corriId != null && !corriId.equalsIgnoreCase("") && !corriId.equalsIgnoreCase("null"))
                {
                    for (int i = 0; i < gsFormId.length; i++) 
                    {
                        //System.out.println("-----------------   corri edit  ---------------------------------");
                        tblCorriGSDtl = new TblTenderCoriGrandSumDetail();
                        
                        tblCorriGSDtl.setTenderFormId(Integer.parseInt(gsFormId[i]));
                        //System.out.println("FORM ID="+Integer.parseInt(gsFormId[i]));
                        
                        tblCorriGSDtl.setTenderTableId(Integer.parseInt(request.getParameter("tableId_" + gsFormId[i])));
                       // System.out.println("Tender table ID="+request.getParameter("tableId_" + gsFormId[i]));
                        
                        tblCorriGSDtl.setCellId(Integer.parseInt(request.getParameter("cellId_"+gsFormId[i])));
                        //System.out.println("Tender Cell ID="+Integer.parseInt(request.getParameter("cellId_"+gsFormId[i])));
                        
                        tblCorriGSDtl.setTenderSumId(tenderGSId);
                        //System.out.println("Tender Grand Sum Id="+tenderGSId);
                        
                        tblCorriGSDtl.setCorrigendumId(Integer.parseInt(corriId));
                       //System.out.println("Corri ID="+Integer.parseInt(corriId));
                        
                        lstCorriGsDtl.add(tblCorriGSDtl);
                        //System.out.println("--------------------------------------------------");
                    }
                }
                else
                {
                    for (int i = 0; i < gsFormId.length; i++) 
                    {
                        //System.out.println("----------------   simple edit or Add   ----------------------------------");
                        tblGSDtl = new TblTenderGrandSumDetail();
                        tblGSDtl.setTenderFormId(Integer.parseInt(gsFormId[i]));
                       // System.out.println(gsFormId[i] + "<br>");
                        tblGSDtl.setTenderTableId(Integer.parseInt(request.getParameter("tableId_" + gsFormId[i])));
                       // System.out.println(request.getParameter("tableId_" + gsFormId[i]) + "<br>");
                        tblGSDtl.setCellId(Integer.parseInt(request.getParameter("cellId_"+ gsFormId[i])));
                       // System.out.println(request.getParameter("cellId_"+gsFormId[i]) + "<br>");
                        lstGsDtl.add(tblGSDtl);
                       // System.out.println("--------------------------------------------------");
                    }
                }
            }
            int delFlag = -1;
            int insertedGSId = -1;
            String strOpration = "Create Grand Summary";
            AuditTrail auditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            if ("Save".equals(operation)) 
            {
                // System.out.println("======================== in save=================");
                if(corriId != null && !corriId.equalsIgnoreCase("") && !corriId.equalsIgnoreCase("null"))
                {
                   // System.out.println("======================== in save corri exist=================");
                    insertedGSId = grandSummary.insertCorriGrandSummary(tblGS, lstCorriGsDtl, corriId);
                }
                 else
                {
                  //  System.out.println("======================== in save without corri=================");
                    insertedGSId = grandSummary.insertGrandSummary(tblGS, lstGsDtl, delFlag);
                }
            }
            else 
            {
               // System.out.println("======================== in edit =================");
                if(corriId != null && !corriId.equalsIgnoreCase("") && !corriId.equalsIgnoreCase("null"))
                {
                   // System.out.println("======================== in corri id condition toperform insert check list size ======="+lstCorriGsDtl.size());
                   grandSummary.insertCorriGrandSummary(lstCorriGsDtl, tenderGSId, corriId);
                   insertedGSId=tenderGSId;
                   strOpration = "Update Grand Summary for corrigendum";
                }
                else
                {
                    strOpration = "Update Grand Summary";
                    delFlag = tenderGSId;
                    insertedGSId = grandSummary.insertGrandSummary(tblGS, lstGsDtl, delFlag);
                }
            }
            String remarks="Officer (User id):"+session.getAttribute("userId")+" has "+strOpration;
            makeAuditTrailService.generateAudit(auditTrail,tenderId, "tenderId", EgpModule.Tender_Document.getName(), strOpration, remarks);
            if (pkgOrLotId == -1) {
                response.sendRedirect("ViewGrandSummary.jsp?tenderId=" + tenderId + "&sectionId=" + tenderSectionId + "&gsId=" + insertedGSId +"&corriId="+corriId);
            } else {
                response.sendRedirect("ViewGrandSummary.jsp?tenderId=" + tenderId + "&sectionId=" + tenderSectionId + "&gsId=" + insertedGSId + "&porlId=" + pkgOrLotId+"&corriId="+corriId);
            }
        } 
        // Following else case is use for get data of grand summary to generate grand summary page.
        else
        {
                String gsName = "Grand Summary";
                boolean dataExistinCoriDetailTable=false;


                java.util.Map<Integer, Integer> hmGSFormId = null;
                java.util.Map<Integer, Integer> hmGSCellId = null;
                if (isEdit) 
                {
                     gsName = grandSummary.getGrandSummaryName(tenderGSId);
                    if(corriId != null && !corriId.equalsIgnoreCase("") && !corriId.equalsIgnoreCase("null")) 
                    {
                         dataExistinCoriDetailTable=grandSummary.checkCoriExistInCoriDetailTbl(tenderGSId+"",corriId);
                    }
                     //System.out.println("==================== data temp exiswt"+dataExistinCoriDetailTable);
                     if(dataExistinCoriDetailTable)
                     {
                        // passing true will return FormId in hash map
                        hmGSFormId = grandSummary.getCorriGSFormsDtl(tenderGSId+"",corriId);
                           // System.out.println("========================================");
                            for (Map.Entry<Integer, Integer> entry : hmGSFormId.entrySet()) 
                           {
                               //System.out.println(" form id="+entry.getValue().toString());
                           }
                        // passing true will return CellId in hash map
                        hmGSCellId = grandSummary.getCorriGSFormsTblDtl(tenderGSId+"",corriId);
                            for (Map.Entry<Integer, Integer> entry : hmGSCellId.entrySet()) 
                           {
                             //  System.out.println(" table id="+entry.getKey().toString()+" cell id="+entry.getValue().toString());
                           }
                     }
                     else
                     {
                          // passing true will return FormId in hash map
                        hmGSFormId = grandSummary.getGSFormsDtl(tenderGSId, true);
                        // passing true will return CellId in hash map
                        hmGSCellId = grandSummary.getGSFormsTblDtl(tenderGSId);
                     }
                }
        %>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->
            <!--Page Content Start-->

            <div class="contentArea_1">
                <%
                    pageContext.setAttribute("tenderId", tenderId);
                %>
                <div class="pageHead_1">
                    Create Grand Summary
                    <%if (pkgOrLotId==-1) {%>
                    <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%= tenderId%>" title="Tender/Proposal Doc. Preparation" class="action-button-goback">Tender Doc. Preparation</a></span>
                    <%} else {%>
                    <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%= tenderId%>&porlId=<%= pkgOrLotId%>" title="Tender/Proposal Doc. Preparation" class="action-button-goback">Tender Doc. Preparation</a></span>
                    <%}%>
                </div>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <form name="frmGrandSummary" id="frmGrandSummary" method="post">
                    <input type="hidden" name="tenderId" value="<%= tenderId%>" />
                    <input type="hidden" name="corriId" value="<%= corriId%>" />
                    <input type="hidden" name="sectionId" value="<%= tenderSectionId%>" />
                     <input type="hidden" name="procMethod" id="procMethod" value="<%= procurementMethod%>" />
                    <%if (pkgOrLotId != -1) {%>
                    <input type="hidden" name="porlId" id="porlId" value="<%= pkgOrLotId%>"/>
                    <%}%>
                    <%if (isEdit) {%>
                    <input type="hidden" name="gsId" value="<%= tenderGSId%>" />
                    <%}%>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="formStyle_1 t_space">
                        <tr>
                            <td width="15%" class="ff">Form Name : <span>*</span></td>
        <td width="85%"><input type="text" name="grandSummaryName" class="formTxtBox_1" id="grandSummaryName" value="<%= gsName%>" readonly  />
                                                  
                              
                            <br /><span id="mailMsg" style="color: red; ">&nbsp;</span>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="10%" class="t-align-left">Select</th>
                            <th width="70%" class="t-align-left">Form Name</th>
                            <th width="20%" class="t-align-left">Column</th>
                        </tr>
                        <%
    boolean showBtn = true;
    List<TblTenderForms> tnForms = null;
    if (request.getParameter("porlId") != null && !"0".equals(request.getParameter("porlId"))) {
        tnForms = grandSummary.getTenderPriceBidForm(tenderSectionId, pkgOrLotId);
    } else {
        tnForms = grandSummary.getTenderPriceBidForm(tenderSectionId);
    }
    if((tnForms != null && tnForms.size() == 0) || tnForms == null)
    {
        showBtn = false;
    }
    if (tnForms != null) 
    {
        if (tnForms.size() > 0) 
        {
            for (int j = 0; j < tnForms.size(); j++) 
            {
               
                        %>
                        <tr>
                            <td class="t-align-center"> 
                                <input type="checkbox" name="chkFormId" id="chkFormId_<%=j%>" value="<%= tnForms.get(j).getTenderFormId()%>"
                                       <%
                                       
                                           if (isEdit) 
                                           {
                                               if (hmGSFormId.containsValue(tnForms.get(j).getTenderFormId())) 
                                               {
                                                   out.print(" checked ");
                                               }
                                               /********* commented as per discssed with Darshan bhai becasue wroung logic written
                                               else
                                                   {
                                                     if(j == 0)
                                                         {
                                                            out.print(" checked ");
                                                         }
                                                   }
                                                ********************************/
                                           } else {
                                               if(!procurementMethod.equals("LTM"))
                                                    out.print(" checked ");
                                           }
                                           
                                           if(tnForms.get(j).getFormStatus() != null && (tnForms.get(j).getFormStatus().equalsIgnoreCase("cp") || tnForms.get(j).getFormStatus().equalsIgnoreCase("c")))
                                           {
                                               out.println(" disabled ");
                                           }                               
                                       %>
                                       />
                            </td>
                            <td class="t-align-left">
                                <%= tnForms.get(j).getFormName()%>
                                <%
                                    if(tnForms.get(j).getFormStatus() != null && (tnForms.get(j).getFormStatus().equalsIgnoreCase("cp") || tnForms.get(j).getFormStatus().equalsIgnoreCase("c")))
                                       {
                                           out.println("<font color='red'> (Form Canceled) </font>");
                                       }  
                                %>
                                <input type="hidden" name="formName_<%=j%>" id="formName_<%=j%>" value="<%= tnForms.get(j).getFormName()%>" />
                            </td>
                            <td class="t-align-left">
                                <%
                                    List<TblTenderCells> tCells = grandSummary.getColumnsForGS(tnForms.get(j).getTenderFormId());
                                    if (tCells != null) 
                                    {
                                    if (tCells.size() > 0) 
                                    {
                                          boolean isChecked=false; 
                                          String selTableId=null;                                                                                 
                                        for (int jj = 0; jj < tCells.size(); jj++) 
                                        {

                                    %>
                                        <input type="radio" name="cellId_<%= tnForms.get(j).getTenderFormId()%>" id="cellId_<%=j%>_<%=jj%>" 
                                           <%
                                               if (isEdit) {
                                                   for (Map.Entry<Integer, Integer> entry : hmGSCellId.entrySet()) 
                                                   {

                                                       if((entry.getKey().toString().equalsIgnoreCase(tCells.get(jj).getTblTenderTables().getTenderTableId()+""))
                                                           && (entry.getValue().toString().equalsIgnoreCase(tCells.get(jj).getCellId()+"") ))
                                                       {
                                                           out.print(" checked ");
                                                           isChecked=true;
                                                           selTableId = entry.getKey().toString();
                                                       }
                                                   }

                                               } else {
                                                   if(j == 0){
                                                       if(jj == 0){
                                                           out.print(" checked ");
                                                       }
                                                   }
                                                   else{
                                                   out.print(" checked ");
                                                   }
                                               }

                                               if(tnForms.get(j).getFormStatus() != null && (tnForms.get(j).getFormStatus().equalsIgnoreCase("cp") || tnForms.get(j).getFormStatus().equalsIgnoreCase("c")))
                                               {
                                                   out.println(" disabled ");
                                               } 
                                            %>
                                            value= "<%= tCells.get(jj).getCellId()%>" onclick="setTablevlaue('tableId_<%= tnForms.get(j).getTenderFormId()%>','<%= tCells.get(jj).getTblTenderTables().getTenderTableId()%>');"
                                           />
                                        <% 
                                            //By Emtaz on 24/April/2016 BTN->Nu.
                                            String BDTtoNU = tCells.get(jj).getCellvalue().replaceAll("BTN", "Nu.");
                                            out.print(BDTtoNU);
                                        %>
                                        <%
                                        }
                                    
                                      if(!isChecked) 
                                      {                                      
                                            %>
                                            <input type="hidden" name="tableId_<%= tnForms.get(j).getTenderFormId()%>" id="tableId_<%= tnForms.get(j).getTenderFormId()%>" value="<%= tCells.get(0).getTblTenderTables().getTenderTableId()%>" />
                                            <%
                                       }
                                      else
                                      {
                                           %>
                                           <input type="hidden" name="tableId_<%= tnForms.get(j).getTenderFormId()%>" id="tableId_<%= tnForms.get(j).getTenderFormId()%>" value="<%= selTableId%>" />
                                           <%
                                      }
                                    } 
                                    else 
                                    {
                                        if (showBtn) {
                                            showBtn = false;
                                        }
                                    }
                                    tCells = null;
                                } else {
                                    if (showBtn) {
                                        showBtn = false;
                                    }
                                }
                                %>
                            </td>
                        </tr>
                        <%
                                                        }
                                                    } else {
                        %>
                        <tr>
                            <td class="t-align-center" colspan="3">No records found</td>
                        </tr>
                        <%                                                }
                                                    tnForms = null;
                                                } else {
                        %>
                        <tr>
                            <td class="t-align-center" colspan="3">No records found</td>
                        </tr>
                        <%                                    }
                        %>
                    </table>
                    <%if (showBtn) {%>
                    <div class="t-align-center t_space">
                        <label class="formBtn_1">
                            <input name="saveoredit" id="saveoredit" type="button"
                                   <%
                                       if (isEdit) {
                                   %>
                                   value="Edit"
                                   <%                                                              } else {
                                   %>
                                   value="Save"
                                   <%                                   }
                                   %>
                                   onclick="validation();"
                                   />

                            <%
                                       if (isEdit) {
                                   %>
                                   <input type="hidden" name="saveGrandSummary" id="saveGrandSummary"  value="Edit"/>
                                   <%                                                              } else {
                                   %>
                                   <input type="hidden" name="saveGrandSummary" id="saveGrandSummary" value="Save" />
                                   <%                                   }
                                   %>


                        </label>
                    </div>
                    <%} else {%>
                    <div>&nbsp;</div>
                    <div class="responseMsg noticeMsg">
                        Please Complete Form First
                    </div>
                    <%}%>
                </form>
            </div>
            <!--Page Content End-->
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <%
                    }
        %>
    </body>
    <script>
        function setTablevlaue(hdntblid,valtblid)
        {
            //alert(hdntblid+"   "+valtblid);
            document.getElementById(hdntblid).value=valtblid;
        }
        function validation(){
            $('span.#mailMsg').html("");
            var Gval = document.getElementById("grandSummaryName").value.length;
            var valid = false;
            //var validForm=false;
            var atLeastoneSelected = false;
            var chkLength = document.getElementsByName("chkFormId").length;
            
            for(var i = 0;i<chkLength;i++)
            {
                if(!document.getElementById("chkFormId_"+i).disabled && document.getElementById("chkFormId_"+i).checked)
                {
                    atLeastoneSelected = true;
                    var rbtnLegth = document.getElementsByName("cellId_"+document.getElementById("chkFormId_"+i).value).length;
                    valid=false;
                    for(var j = 0 ; j<rbtnLegth;j++)
                    {
                        if(document.getElementById("cellId_"+i+"_"+j).checked)
                        {
                            valid= true;
                            break;
                        }
                    }
                    if(!valid)
                    {
                        break;
                    }
                }

            }
           
            if(Gval==0){
                $('span.#mailMsg').css("color","red");
                $('span.#mailMsg').html("Please enter Form name");
            }
            else if(!atLeastoneSelected)
            {
                jAlert("At least one form name and one column must be selected.","Create Grand Summary ",function(RetVal) {
                 });
            }
            else if(!valid)
            {
                var formName = document.getElementById("formName_"+i).value;
                jAlert("Please select Column value for selected Form : "+formName,"Create Grand Summary ",function(RetVal) {
                 });
            }
            // this section hide for the issue "EGPBDONE-447"
//            else if(document.getElementById("procMethod").value == "LTM")      
//            {
//             jConfirm("Be sure that you have selected ‘Bidder’s Quoted Price’ form ONLY.","Confirmation",function(RetVal) {
//                 if(RetVal == true){
//                    document.getElementById("saveoredit").style.display = 'none';
//                    document.frmGrandSummary.method="post";
//                    document.frmGrandSummary.action="CreateGrandSummary.jsp";
//                    document.frmGrandSummary.submit();
//                 }
//                 });
//            }
            else
            {
                document.getElementById("saveoredit").style.display = 'none';
                document.frmGrandSummary.method="post";
                document.frmGrandSummary.action="CreateGrandSummary.jsp";
                document.frmGrandSummary.submit();
            }

          //  jConfirm("Please select only tenderer quoted price form ","aaaa",function(RetVal) {

           //         if(RetVal == true)


          //       });
           
        }
    </script>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
