<%--
    Document   : TenderRepInclude.jsp
    Created on : Apr 9, 2011, 11:58:05 AM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.web.databean.ReportGenerateDtBean"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblReportColumnMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ReportCreationSrBean"%>
<jsp:useBean id ="DomesticPreference" class="com.cptu.egp.eps.web.servicebean.DomesticPreferenceSrBean"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
    <body>
        <%
            /**
            ***************Comment for Round Logic**************
            case 1 : For 1st time roundId=0 & frId = 0 means from SP, here rank on page
            case 2 : Now on 2nd time for saved to display roundId=avail & frId=0(getSavedReportData)
                     Here rank from new field of DtBean
            case 3 : For new bidder means 2nd bidder where roundId=0 & frId = avail(getSavedReportData)
                     Here rank from new field of DtBean
            **/
            boolean isTos=false;
            boolean isTor=false;
            boolean isTer=false;
            boolean isEval=false;            
            boolean isSave=false;
            String autoAppender="L";
            if("y".equals(request.getParameter("isTOR"))){
                isTor = true;
                autoAppender="";
            }
            if("true".equals(request.getParameter("istos"))){
                isTos=true;
            }
            if("true".equals(request.getParameter("ister"))){
                isTos=true;
            }
            if("true".equals(request.getParameter("isEval"))){
                isEval=true;
            }
            if("true".equals(request.getParameter("isSave"))){
                isSave=true;
            }
           
            String tenderid = request.getParameter("tenderid");
            String repId = request.getParameter("repId");
            ReportCreationSrBean rcsb = new ReportCreationSrBean();            
            String tend_rep_UserId = null;
            if(session.getAttribute("userId")!=null){
                tend_rep_UserId = session.getAttribute("userId").toString();
            }else{
                tend_rep_UserId = request.getParameter("tendrepUserId");
            }
            String roundId = "0";
            if(request.getParameter("rId")!=null){
                roundId = request.getParameter("rId");
            }
            String finalroundId  = "0";
            if(request.getParameter("frId")!=null){
                finalroundId = request.getParameter("frId");
            }
            int evalCount = 0;
            if(request.getParameter("evalCount")!=null){
                evalCount = Integer.parseInt(request.getParameter("evalCount"));
            }
            String lotId = "0";
            if(request.getParameter("lotId")!=null){
                lotId = request.getParameter("lotId");
            }

            rcsb.setRoundId(roundId);            
            rcsb.setFinalroundId(finalroundId);
            
            List<ReportGenerateDtBean> dtBean = rcsb.getReportData(tenderid, repId,tend_rep_UserId,isTos,isTor,"0",lotId);
            if(dtBean!=null && (!dtBean.isEmpty())){
                boolean isLottery = false;
                double L1amount = 0;
                double amount = 0;
                List<SPCommonSearchDataMore> pMethodEstCost = null;
                Object data[] = null;
                int reportTableId=0;
                int noOfCols=0;
                java.util.List<TblReportColumnMaster> list=null;
                CommonSearchDataMoreService moreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> Winner = moreService.getCommonSearchData("GetWinnerInfo",tenderid);//Finding the winner of RFP
                if(lotId.equalsIgnoreCase("0")){
                data = rcsb.getReportTabIdNoOfCols(repId);
                reportTableId = Integer.parseInt(data[0].toString());
                noOfCols = Integer.parseInt(data[1].toString());
                list = rcsb.getReportColumns(reportTableId);
                pMethodEstCost=moreService.getCommonSearchData("GetPMethodEstCostTender",tenderid);                                
                } else{
                data = rcsb.getReportTabIdNoOfColsLots(repId,lotId);
                reportTableId = Integer.parseInt(data[0].toString());
                noOfCols = Integer.parseInt(data[1].toString());
                list = rcsb.getReportColumns(reportTableId);
                pMethodEstCost=moreService.getCommonSearchData("GetPMethodEstCostTenderLot",tenderid,lotId);
                }
                //Object data[] = rcsb.getReportTabIdNoOfCols(repId);
                //int reportTableId = Integer.parseInt(data[0].toString());
               // int noOfCols = Integer.parseInt(data[1].toString());
                //java.util.List<TblReportColumnMaster> list = rcsb.getReportColumns(reportTableId);
                
                
                //if(lotId.equalsIgnoreCase("0")){
                //    pMethodEstCost=moreService.getCommonSearchData("GetPMethodEstCostTender",tenderid);
                //} else{
                //    pMethodEstCost=moreService.getCommonSearchData("GetPMethodEstCostTenderLot",tenderid,lotId);
                //}
                List<SPCommonSearchDataMore> envDataMores = moreService.geteGPData("GetTenderEnvCount", tenderid);
                boolean isL1 = false;
                if(envDataMores!=null && (!envDataMores.isEmpty())){                    
                    if(envDataMores.get(0).getFieldName2().equals("2")){
                        //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                        isL1 = true;
                    }
                }
                String pMethod = pMethodEstCost.get(0).getFieldName1();
                boolean isSFB = pMethod.equals("SFB");
                BigDecimal estCost = new BigDecimal(pMethodEstCost.get(0).getFieldName2()!=null ? pMethodEstCost.get(0).getFieldName2():"0.00");

                //domestic Preference Start
               // System.out.print(DomesticPreference.IsDomesticPreference(Integer.valueOf(tenderid)));
               // if(DomesticPreference.IsDomesticPreference(Integer.valueOf(tenderid)).equals("Yes"))
               // {
        %>             
        <table class="tableList_1" width="100%" cellspacing="0" id="repTable">
                <tbody>
                    <%if(!data[3].toString().equals("")){%>
                    <tr>
                        <th colspan="<%=noOfCols + noOfCols%>" class="t-align-left ff"><%=data[3]%></th>
                    </tr>
                    <%}%>
                    <tr>
                        <% if(isSave == true){ //roundId.equals("0") %>
                            <th class="t-align-left ff" width="20">Select Winner</th>
                        <% } %>
                        <%
                            for (TblReportColumnMaster master : list) {
                                if(isTos && !isEval && isTer){
                                   if (master.getFilledBy() != 3){
                        %>
                        <th class="t-align-left ff"><%=master.getColHeader()%></th>
                        <%}} else{
                                String th = master.getColHeader();
                                //Nitish Start
                                if(th.equals("Name of Tenderer"))
                                {
                                    th = "Name of Bidder";
                                }
                                if(th.equals("Quoted Amount (in BDT)"))
                                {
                                    th = "Quoted Amount (in Nu.)";
                                }
                                if(th.equals("% of Deviaiton"))
                                {
                                    th = "% of Deviation";
                                }
                                if(th.contains("BTN")){
                                    th = th.replace("BTN", "Nu.");
                                }
                                //Nitish End
                                out.print("<th class='t-align-left ff'>"+th+"</th>");
                                }
                             } %>
                    </tr>
                   <%
                            int counter=1;
                            int rank=1;
                            int fRank = 0;
                            boolean isFirstDisq = true;
                            StringBuilder bidderIds = new StringBuilder();
							//changes made by dohatec for re-evaluation
                            List<SPCommonSearchDataMore> pqBidder = moreService.geteGPData("PostDisqualifyUsers",tenderid,repId,String.valueOf(evalCount),String.valueOf(isTor));
                            for(ReportGenerateDtBean rgdb : dtBean){
                                out.print("<tr id='bidcnt_"+counter+"'>");
                                if(isSave == true)//roundId.equals("0")
                                        out.print("<td><input type='radio' required name='rdbWinner' value='"+rgdb.getBidderId()+"' required/></td>");
                                for (TblReportColumnMaster master : list) {                                    
                                    /*
                                    1-Company
                                    2-Auto
                                    3-Auto Number
                                    4-Official Cost Estimate
                                    */
                                    //deviation
                     
                                    if (master.getFilledBy() == 2 && master.getGovCol().equals("no")) {
                                        for (int r = 0; r < rgdb.getAutoColumnColId().length; r++) {
                                            if(rgdb.getAutoColumnColId()[r]!=null && Integer.parseInt(rgdb.getAutoColumnColId()[r]) == master.getColumnId()){
                                                out.print("<td style='text-align:right;'>");
                                                if(rgdb.getAutoColumn()[r]==null){
                                                    out.print("0.00");
                                                }else{
                                                    if(rgdb.getAutoColumn()[r].startsWith("W_")){
                                                      String wordData = rgdb.getAutoColumn()[r].substring(2, rgdb.getAutoColumn()[r].length());
                                                  %>
                                                   <span id="autoW_<%=counter%>" style="text-align: left;"></span>
                                                   <script type="text/javascript">
                                                       document.getElementById('autoW_<%=counter%>').innerHTML = DoIt(<%=new BigDecimal(wordData).setScale(3, 0)%>);
                                                       $('#autoW_<%=counter%>').closest('td').attr('style','text-align:left');
                                                   </script>
                                                  <%
                                                    }else{
                                                        out.print(new BigDecimal(rgdb.getAutoColumn()[r]).setScale(3, 0));
                                                    }
                                                }
                                                out.print("</td>");
                                            }
                                        }
                                    }
                                    //quoted amount
                                    if (master.getFilledBy() == 2 && master.getGovCol().equals("yes")) {
                                            out.print("<td style='text-align:right;'>");
                                            if(rgdb.getGovColumn()==null || "".equals(rgdb.getGovColumn())){
                                                out.print("0.00");
                                            }else{                                               
                                                out.print(new BigDecimal(rgdb.getGovColumn()).setScale(3, 0));
                                            }
                                            out.print("</td>");
                                    }
                                    //name of the tenderer
                                    if (master.getFilledBy() == 1) {
                                        for (int r = 0; r < rgdb.getCompanyNameColId().length; r++) {
                                            if (rgdb.getCompanyNameColId()[r]!=null && Integer.parseInt(rgdb.getCompanyNameColId()[r]) == master.getColumnId()) {
                                                out.print("<td style='text-align:left;'>");
                                                out.print(rgdb.getCompanyName()[r]);
                                                if(counter==1 && !isSave && autoAppender.equals("L") && isL1){
                                                    out.print("<span style='color:green;'> <b>(Winner)</b></span>");
                                                }
                                                else if(Winner!=null && Winner.size()>0 && Winner.get(0).getFieldName1().equalsIgnoreCase(rgdb.getBidderId()))
                                                {
                                                    out.print("<span style='color:green;'> <b>(Winner)</b></span>");
                                                }
                                                if(autoAppender.equals("L") && isSFB){
                                                    BigDecimal l1amount = BigDecimal.ZERO;
                                                    if(rgdb.getGovColumn()!=null && !"".equals(rgdb.getGovColumn())){                                                            
                                                            l1amount = new BigDecimal(rgdb.getGovColumn());
                                                    }
                                                    if(l1amount.compareTo(estCost)==1){
                                                        out.print("<span style='color:red;'> <b>(Above Official Cost Estimate)</b></span>");
                                                    }
                                                    l1amount = null;
                                                }
                                                out.print("</td>");
                                            }
                                        }
                                    }
                                    if (master.getFilledBy() == 3 && (!isTos || isEval || !isTer)) {
                                        for (int r = 0; r < rgdb.getAutoNumberColId().length; r++) {
                                            if (rgdb.getAutoNumberColId()[r]!=null && Integer.parseInt(rgdb.getAutoNumberColId()[r]) == master.getColumnId()) {
                                                  if(rgdb.getAutoNumber()==null){
                                                      if(rgdb.getGovColumn()==null || "".equals(rgdb.getGovColumn())){
                                                            amount = 0.0;
                                                       }else{
                                                            amount = new BigDecimal(rgdb.getGovColumn()).setScale(3, 0).doubleValue();
                                                        }
                                                      for(SPCommonSearchDataMore pqUser : pqBidder){
                                                          if(pqUser.getFieldName1().equals(rgdb.getBidderId())){
                                                                fRank=-1;
                                                            }
                                                      }
                                                      if(counter==1){
                                                        if(fRank!=-1){
                                                            fRank = rank;
                                                        }
                                                        L1amount = amount;                                                        
                                                      }else{                                                        
                                                        if(amount!=L1amount){
                                                            L1amount = amount;
                                                            if(fRank==-1){
                                                                fRank = rank;
                                                                rank++;
                                                            }else{
                                                                rank++;
                                                                fRank = rank;                                                                
                                                            }
                                                        }else{
                                                            if(fRank==-1){
                                                               L1amount = amount;                                                               
                                                            }else if(fRank!=-1){
                                                                if(fRank==rank && counter==2 && isFirstDisq){
                                                                    isLottery = true;
                                                                }
                                                            }
                                                        }                                                        
                                                      }
                                                  }else{
                                                      rank = Integer.parseInt(rgdb.getAutoNumber()[r]);
                                                      if(counter==1){
                                                          fRank = rank;
                                                          bidderIds.append("<input type='hidden' value='"+rgdb.getBidderId()+"' name='lotteryBidder'/>");
                                                      }else{
                                                          if(fRank==rank){
                                                             bidderIds.append("<input type='hidden' value='"+rgdb.getBidderId()+"' name='lotteryBidder'/>");
                                                             isLottery = true;
                                                          }
                                                      }
                                                  }
                                                out.print("<td class='t-align-center'>"+autoAppender+rank+"</td>");
                                            }
                                        }
                                    }
                                    if (master.getFilledBy() == 4) {
                                        for (int r = 0; r < rgdb.getEstCostId().length; r++) {
                                            if (rgdb.getEstCostId()[r]!=null && Integer.parseInt(rgdb.getEstCostId()[r]) == master.getColumnId()) {
                                                out.print("<td style='text-align:right;'>"+new BigDecimal(rgdb.getEstCost()[r]).setScale(3, 0)+"</td>");
                                            }
                                        }
                                    }                                    
                                }
                                out.print("</tr>");
                                if(fRank==-1){
                                    fRank = rank;
                                    if(counter==1){
                                        isFirstDisq = false;
                                    }
                               %>
                <script type="text/javascript">
                    $('#bidcnt_<%=counter%>').hide();
                </script>
                    <%
                                }
                                counter++;
                            }
                    %>
                    <%if(!data[4].toString().equals("")){%>
                    <tr>
                        <td colspan="<%=noOfCols + noOfCols%>" class="t-align-left formSubHead_1"><%=data[4]%></td>
                    </tr>
                    <%}%>
                </tbody></table>
                <%
                    data=null;
                    list=null;
                    request.setAttribute("isLottery", isLottery);
                    request.setAttribute("lotteryBidder", bidderIds);
                }else{
                    // When PE selects Form(s) for PCR that are only non mandatory than below message will display.
                    out.print("<table class='tableList_1' width='100%' cellspacing='0'><tr><td class='t-align-center mandatory' style='font-weight: bold;'>No Data to Display.</td></tr></table>");
                }
                %>
    </body>
</html>
<%
    rcsb=null;    
    dtBean=null;
%>