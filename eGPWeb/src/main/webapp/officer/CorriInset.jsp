<%--
    Document   : CorriInset
    Created on : Dec 9, 2010, 8:13:55 PM
    Author     : TaherT
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <title>JSP Page</title>
    </head>
    <body>
        <%
                    if ("Submit".equals(request.getParameter("submit"))) {
                        TenderSrBean tenderSrBean1 = new TenderSrBean();
                        String tid = request.getParameter("id");
                        String location[]=null;
                        if(request.getParameterValues("locationRefNo")!=null){
                            location = request.getParameterValues("locationRefNo");
                        }
                        if(request.getParameterValues("locationlot")!=null){
                            location = request.getParameterValues("locationlot");
                        }
                        //Start Dohatec
                        // For ICT 2 parameters are added :: 1. preQualDocPriceUSD, 2. tenderSecurityAmountUSD
                        if ("y".equals(request.getParameter("isedit"))) {
                            tenderSrBean1.createCorri(request.getParameter("id"), Integer.parseInt(request.getParameter("corrid")), request.getParameter("lastDateTenderSub"), request.getParameter("preTenderMeetStartDate"), request.getParameter("preQualOpenDate"), request.getParameter("preTenderMeetEndDate"), request.getParameter("preQualCloseDate"), request.getParameter("nameAddressTenderSub"), request.getParameter("tenderLastSellDate"), request.getParameterValues("indicativeStartDate"), request.getParameterValues("indicativeComplDate"), request.getParameterValues("docFeeslot"), request.getParameter("preQualDocPrice"), request.getParameter("preQualDocPriceUSD"),request.getParameter("eligibilityofTenderer"),request.getParameter("briefDescGoods"),location,request.getParameterValues("tenderSecurityAmount"),request.getParameterValues("tenderSecurityAmountUSD"),request.getParameterValues("complTimeLotNo"),request.getParameterValues("txtrefNo"),request.getParameterValues("phasingService"),request.getParameterValues("startDateLotNo"),request.getParameter("expRequired"),request.getParameter("otherDetails"),true);
                        } else {
                            tenderSrBean1.createCorri(request.getParameter("id"), Integer.parseInt(request.getParameter("corrid")), request.getParameter("lastDateTenderSub"), request.getParameter("preTenderMeetStartDate"), request.getParameter("preQualOpenDate"), request.getParameter("preTenderMeetEndDate"), request.getParameter("preQualCloseDate"), request.getParameter("nameAddressTenderSub"), request.getParameter("tenderLastSellDate"), request.getParameterValues("indicativeStartDate"), request.getParameterValues("indicativeComplDate"), request.getParameterValues("docFeeslot"), request.getParameter("preQualDocPrice"), request.getParameter("preQualDocPriceUSD"),request.getParameter("eligibilityofTenderer"),request.getParameter("briefDescGoods"),location,request.getParameterValues("tenderSecurityAmount"),request.getParameterValues("tenderSecurityAmountUSD"),request.getParameterValues("complTimeLotNo"),request.getParameterValues("txtrefNo"),request.getParameterValues("phasingService"),request.getParameterValues("startDateLotNo"),request.getParameter("expRequired"),request.getParameter("otherDetails"),false);
                        }
                        //End Dohatec
                        tenderSrBean1 = null;
                        response.sendRedirect("Amendment.jsp?tenderid=" + tid);
                    }
        %>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
