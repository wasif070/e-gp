<%-- 
    Document   : TenderEvalReport
    Created on : Apr 1, 2011, 6:48:06 PM
    Author     : TaherT
--%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblBidderRank"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CompareReportT1"%>
<%@page import="com.cptu.egp.eps.web.servicebean.Comparison"%>
<%@page import="java.util.Collections"%>
<%@page import="com.sun.org.apache.xpath.internal.compiler.OpCodes"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalBidderStatus"%>
<%@page import="java.util.List"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation  T1 Report</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>        
    </head>
</head>
<body>
    <%
                String tendId = request.getParameter("tenderid");
                String evalStat = null;
                EvalServiceCriteriaService criteriaService = (EvalServiceCriteriaService) AppContext.getSpringBean("EvalServiceCriteriaService");
                if ("eval".equals(request.getParameter("stat"))) {
                    evalStat = "evaluation";
                } else {
                    evalStat = "reevaluation";
                }
                if ("Save".equals(request.getParameter("submit"))) {
                    List<TblBidderRank> bidlist = new ArrayList<TblBidderRank>();
                    String[] userIds = request.getParameterValues("userIds");
                    String[] amount = request.getParameterValues("amount");
                    String[] cmpName = request.getParameterValues("cmpName");
                    for (int i = 0; i < userIds.length; i++) {
                        bidlist.add(new TblBidderRank(0, new TblTenderMaster(Integer.parseInt(tendId)), 0, Integer.parseInt(userIds[i]), new Date(), Integer.parseInt(session.getAttribute("userId").toString()), (i + 1), 0, new BigDecimal(amount[i]), 0, 0, cmpName[i]));
                    }
                    if (criteriaService.insertBidderRank(tendId, session.getAttribute("userId").toString(), bidlist)) {
                        response.sendRedirect("TenderEvalReport.jsp?tenderid="+tendId+"&stat="+evalStat);
                    } else {
                        out.print("<h1>Error in saving report</h1>");
                    }
                } else {
                    List<Object[]> list = criteriaService.getBidderEvalStatus(tendId, evalStat);
                    Collections.sort(list, new CompareReportT1());
    %>
    <%@include  file="../resources/common/AfterLoginTop.jsp" %>
    <div class="tabPanelArea_1">
        <div class="pageHead_1">Evaluation T1 Report</div>
        <form action="TenderEvalReport.jsp?tenderid=<%=tendId%>&stat=<%=request.getParameter("stat")%>" method="post">
            <table class="tableList_1 t_space" cellspacing="0" width="100%">
                <tr>
                    <th class="t-align-center">Consultant Name</th>
                    <th class="t-align-center">Marks</th>
                    <th class="t-align-center">Rank</th>
                </tr>
                <%
                                int cnt = 1;
                                for (Object[] obj : list) {
                %>
                <tr>
                    <td>
                        <%String data = null;
                                                if (obj[3].toString().equals("-")) {
                                                    data = obj[1].toString() + " " + obj[2].toString();
                                                    out.print(data);
                                                } else {
                                                    data = obj[3].toString();
                                                    out.print(data);
                                                }%>
                        <input type="hidden" value="<%=data%>" name="cmpName">
                    </td>
                    <td>
                        <input type="hidden" value="<%=obj[4]%>" name="userIds"/>
                        <input type="hidden" value="<%=obj[0]%>" name="amount"/>
                        <%=obj[0]%>
                    </td>                    
                    <td>T<%=cnt%></td>
                </tr>                
                <%
                        cnt++;
                    }
                    if(list.isEmpty()){
                       out.print("<tr><td colspan='3' class='ff t-align-center' style='color:red;'>No Records Found</td></tr>");
                    }
                %>
            </table>
            <%if(!list.isEmpty()){%>
            <div align="center">
                <br/>
                <label class="formBtn_1">
                    <input type="submit" name="submit" value="Save"/>
                </label>
            </div>
            <%}%>
        </form>
    </div>
    <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    <%}%>
</body>
</html>
