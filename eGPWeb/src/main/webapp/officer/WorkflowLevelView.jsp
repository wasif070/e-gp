<%-- 
    Document   : WorkflowLevelView
    Created on : Oct 28, 2010, 7:44:16 PM
    Author     : dhruti
    Modified By: Dohatec
--%>

<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementRole"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Workflow : Add Users</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
<script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script language="JavaScript" type="text/javascript">
            function changerole(x){
            // alert(x);
             var str = new String(x);
             var str2 =str.substring(str.length-1, str.length);
            // alert(str2);
             var rev=document.getElementById(x)
            var wfrole = rev.options[rev.selectedIndex].value;;
            //alert(wfrole);
            if(wfrole == "Donor"){
                var rev1 = document.getElementById('cmdreviewer'+str2);
               var lab =  document.getElementById('labelreviewer'+str2);
                //alert(rev1);
               for (m=rev1.options.length-1;m>0;m--) {
                  // alert(rev1.options[m].value);
                   if(rev1.options[m].value == '20') {
                      // alert(rev1.options[m].value);
                       rev1.style.display='none';
                        //rev1.options[m].selected=true;
                        lab.innerHTML = 'Development Partner';
                   }
                     
               }

            }

            }

            function removeuser(y) {
                //alert(y);
                var str = new String(y);
                var str2 =str.substring(str.length-1, str.length);
                //alert(str2);
                var rem = document.getElementById(y);
                var add = document.getElementById('adduser'+str2);
                var label = document.getElementById('uservalue'+str2);
                label.style.display='none';
               rem.style.display='none';
               add.style.display='block';
            }

        </script>
</head>
<body>
    <%
    
     String fieldName = "WorkFlowRuleEngine";
     String eventid = request.getParameter("eventid");
     String objectid = request.getParameter("objectid");
     String activityid = request.getParameter("activityid");
     String childid = request.getParameter("childid");
     String Action = request.getParameter("action");
     Date actionDate = new Date();
     String moduleName = null;
     String eventName = null;
      String[] startsBy = null;
      String[] endsBy = null;
      int uid = 0;
      String userid = "";
       if(session.getAttribute("userId") != null){
           Integer ob1 = (Integer)session.getAttribute("userId");
           uid = ob1.intValue();
          userid  = String.valueOf(uid);
       }
      List<TblProcurementRole> procurementRoles = null;
       WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
       List<TblWorkFlowLevelConfig> tblworkFlowLevelconfig = null;
     List<CommonAppData> editdata =  workFlowSrBean.editWorkFlowData(fieldName , eventid, "");
      Iterator it = editdata.iterator();
      StringBuffer stby = new StringBuffer();
      StringBuffer endby = new StringBuffer();
        while(it.hasNext()){
           CommonAppData  commonAppData= (CommonAppData) it.next();
           moduleName = commonAppData.getFieldName1();
           eventName = commonAppData.getFieldName2();
           stby.append(commonAppData.getFieldName3()+",");
           endby.append(commonAppData.getFieldName4()+",");

            }
           if(stby.length()>0){
            stby.deleteCharAt(stby.length()-1);
            endby.deleteCharAt(endby.length()-1);
            }

            String str1 = stby.toString();
             startsBy = str1.split(",");
            String str2 = endby.toString();
             endsBy = str2.split(",");
             procurementRoles =  workFlowSrBean.getRoles();
             int editObjectid = Integer.parseInt(objectid);
             int editChildid = Integer.parseInt(childid);
             short editactivityid = Short.parseShort(activityid);
             if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                editObjectid = Integer.parseInt(childid);
             }
              tblworkFlowLevelconfig =
                     workFlowSrBean.editWorkFlowLevel(editObjectid, editChildid, editactivityid);
              if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                    editObjectid = Integer.parseInt(objectid);
                 }
              
    %>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
      <%@include file="../resources/common/AfterLoginTop.jsp" %>
    
  </div>
       <div class="pageHead_1">Workflow : Add Users
           <div align="right" style="width: 30%;float: right;" >
               <a class="action-button-goback" href="<%=request.getHeader("Referer")%>">Go back</a>
           </div>

       </div>
  <div>&nbsp;</div>

  <form method="POST" id="frmWorkflowUsers" action="">

  <div class="tabPanelArea_1">
  <table width="100%" cellspacing="0" class="tableList_1 t_space">
    <tr>
      <th class="t-align-center">Level No.</th>
      <th class="t-align-center">Workflow Role</th>
      <th class="t-align-center">Procurement Role</th>
      <th class="t-align-center">Name of Official ,[Designation at Office]</th>
      
    </tr>
    <tr>
      <td class="t-align-center">1.</td>
      <td class="t-align-center">Initiator</td>
      <td class="t-align-center">
		
                    <%
                    TblWorkFlowLevelConfig wfl= tblworkFlowLevelconfig.get(0);
                    Iterator rls =  procurementRoles.iterator();
                    while(rls.hasNext()){
                      TblProcurementRole procurementRole =(TblProcurementRole)rls.next();
                     if(wfl.getProcurementRoleId() == procurementRole.getProcurementRoleId()) {
                out.write("<label id='"+procurementRole.getProcurementRoleId()+"' >"+procurementRole.getProcurementRole()+"</label>");
                     }
                    }
               TblLoginMaster  loginMaster = wfl.getTblLoginMaster();

             String str=workFlowSrBean.getOfficialAndDesignationName(loginMaster.getUserId());


%>
            
       </td>
                    <td class="t-align-center"><label id="uservalue1"><%=str%></label>
                        <input type="hidden" name="txtuserid" value="<%=loginMaster.getUserId()%>" id="txtuserid1" /></td>
    
    </tr>
                    <%
                    if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                    objectid = childid;
                   }
                    String donor = workFlowSrBean.isDonorReq(eventid,
                  Integer.valueOf(objectid));
                  if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                    objectid = request.getParameter("objectid");
                   }
                  String[] sarr =  donor.split("_");
                  int l = 2;
                  int val = tblworkFlowLevelconfig.size()-2;
                  for(int i=1;i<=val;i++){
                    
                   %>
    <tr>
       
      <td class="t-align-center"><%=l%></td>
      <td class="t-align-center">
        
          <%
           boolean check = false;
           TblWorkFlowLevelConfig revwfl =   tblworkFlowLevelconfig.get(i);
           if(revwfl.getWfRoleId() == 4){
          out.write("<option  value='Donor'>Donor</option>");
          check = true;
          }
           else out.write("<option value='Donor'>Reviewer</option>");
           %>
         
      </td>
      <td class="t-align-center">
	   
                   <%
                   TblWorkFlowLevelConfig revwflc =   tblworkFlowLevelconfig.get(i);
                    Iterator rev =  procurementRoles.iterator();
                    while(rev.hasNext()){
                        TblProcurementRole procurementRole =(TblProcurementRole)rev.next();
                            if(procurementRole.getProcurementRoleId() == revwflc.getProcurementRoleId())
                                 out.write("<label id='"+procurementRole.getProcurementRoleId()+"' >"+procurementRole.getProcurementRole()+"</label>");
                           
                          }
                     %>

       
                     <label id="labelreviewer<%=l%>"></label>
      </td>
      <td class="t-align-center">
        <%
        TblLoginMaster  lm = revwflc.getTblLoginMaster();
        String userval = "";
        
        if(!check)
        userval =workFlowSrBean.getOfficialAndDesignationName(lm.getUserId());
        else userval = workFlowSrBean.getOfficeNameAndDesignationByDP(lm.getUserId());
        out.write("<label id='uservalue"+l+"'>"+userval+"</label>");
       out.write("<input type='hidden' name='txtuserid' value='"+lm.getUserId()+"' id='txtuserid"+l+"'");%>
         
      </td>

    </tr>
    <%
       l++;
       }
  %>
    <tr>
      <td class="t-align-center"><%=l%></td>
      <td class="t-align-center">Approver</td>
      <td class="t-align-center">
	 
            <%
                    TblWorkFlowLevelConfig wflc =   tblworkFlowLevelconfig.get(l-1);
                    Iterator endsrls =  procurementRoles.iterator();
                    while(endsrls.hasNext()){
                      TblProcurementRole procurementRole =(TblProcurementRole)endsrls.next();
                           if(procurementRole.getProcurementRoleId() == wflc.getProcurementRoleId())
                        out.write("<label id='"+procurementRole.getProcurementRoleId()+"' >"+procurementRole.getProcurementRole()+"</label>");
                           
                      }
                    %>
      
      </td>
      <td class="t-align-center">
          <%
          TblLoginMaster tlm =  wflc.getTblLoginMaster();
          String str3=workFlowSrBean.getOfficialAndDesignationName(tlm.getUserId());
          out.write("<label id='uservalue"+l+"'>"+str3+"</label>");
        out.write("<input type='hidden' name='txtuserid' value='"+tlm.getUserId()+"' id='txtuserid"+l+"' ");%>
          </td>
      
    </tr>
      <tr>
        <td class='t-align-center' colspan="5">
        <% if(Integer.parseInt(activityid) == 1){ %>
          <a class="action-button-goback" href="APPDashboard.jsp?appId=<%=objectid%>">Go back to Dashboard</a>
          <% }else if(Integer.parseInt(activityid) == 2 || Integer.parseInt(activityid)== 9){ %>
          <a class="action-button-goback" href="Notice.jsp?tenderid=<%=objectid%>">Go back to Tender Dashboard</a>
          <% } %>
        </td>
      </tr>
  </table>

	</div>
    <div>&nbsp;</div>
  </form>
    <!--Dashboard Content Part End-->
    <!--Dashboard Footer Start-->
    <table width="100%" cellspacing="0" class="footerCss">
    <tr>
      <td align="left">e-GP &copy; All Rights Reserved
        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
      <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
    </tr>
    </table>
    <!--Dashboard Footer End-->
</div>
</body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabWorkFlow");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

