
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.APPService"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CommonSearchServiceSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementRole"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Workflow : Add Users</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <!-- Dohatec Start -->
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <!-- Dohatec End -->


    </head>
    <body>
        <%
            UserRegisterService service = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            APPService appService = (APPService) AppContext.getSpringBean("APPService");
            TenderCommonService tenderService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            CommitteMemberService comService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");
            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");

            String fieldName = "WorkFlowRuleEngine";
            String eventid = request.getParameter("eventid");
            String objectid = request.getParameter("objectid");
            String activityid = request.getParameter("activityid");
            String childid = request.getParameter("childid");
            String action = request.getParameter("action");
            String tenderId = request.getParameter("tenderId");
            String wpId = request.getParameter("wpId");
            Date actionDate = new Date();
            String moduleName = null;
            String eventName = null;
            String[] startsBy = null;
            String[] endsBy = null;
            int uid = 0;
            String userid1 = "";
            String ofcname = "";
            String desig = "";
            String strSubject = "";
            String loi = "";

            List<CommonAppData> listofAPPDetails = appService.getAPPDetailsBySP("APP", "" + objectid, "");
            List<SPTenderCommonData> listofTenderDetails = tenderService.returndata("tenderinfobar", objectid, null);
            String appCode = "";

            if (request.getParameter("modulename") != null && request.getParameter("modulename").equalsIgnoreCase("Tender")) {
                strSubject = "Tender : " + objectid + " File to be processed in Workflow";
                if (!listofTenderDetails.isEmpty()) {
                    appCode = listofTenderDetails.get(0).getFieldName2();
                }
            } else {
                if (!listofAPPDetails.isEmpty()) {
                    appCode = listofAPPDetails.get(0).getFieldName3();
                }
                strSubject = "Letter Ref. No. : " + appCode + " File to be processed in Workflow";
            }

            MessageProcessSrBean messageProcessSrbean = new MessageProcessSrBean();
            if (session.getAttribute("userId") != null) {
                Integer ob1 = (Integer) session.getAttribute("userId");
                uid = ob1.intValue();
                userid1 = String.valueOf(uid);
            }
            int noRows = 0;
            String donor = null;
            List<TblProcurementRole> procurementRoles = null;
            WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
            //if("Submit".equals(request.getParameter("btnsubmit"))){
    //changed by nafiul for auto workflow generation

            Object accw_objProRole = session.getAttribute("procurementRole");
            String accw_chk = "";
            if (accw_objProRole != null) {
                accw_chk = accw_objProRole.toString();
            }

            String accw_ck1[] = accw_chk.split(",");
            boolean isAu = false, isPE = false;
            for (int accw_iAfterLoginTop = 0; accw_iAfterLoginTop < accw_ck1.length; accw_iAfterLoginTop++) {
                if (accw_ck1[accw_iAfterLoginTop].equalsIgnoreCase("AU")) {
                    isAu = true;
                }
                if (accw_ck1[accw_iAfterLoginTop].equalsIgnoreCase("PE")) {
                    isPE = true;

                }
            }

            if (true) {
                // end changed by nafiul for auto workflow generation
                eventid = request.getParameter("eventid");
                objectid = request.getParameter("objectid");
                activityid = request.getParameter("activityid");
                String tenderID = request.getParameter("tenderId");
                String childId = request.getParameter("childid");
                String tc = request.getParameter("tc");
                loi = request.getParameter("loi");
                StringBuffer stsBy = new StringBuffer();
                StringBuffer endBy = new StringBuffer();
    //           String startsbyid =  request.getParameter("startsby");
    //           String endsbyid =    request.getParameter("endsby");
                String startsbyid = isAu ? "5" : (isPE ? "1" : "0");
                String endsbyid = "6";
                //String endsbyid = "1";
                StringBuffer wfRoleids = new StringBuffer();
                StringBuffer procurids = new StringBuffer();
                StringBuffer fileOnHand = new StringBuffer();
                StringBuffer WfLevel = new StringBuffer();

                //changed by nafiul for auto workflow generation
                String aprverId = "";
                List<SPCommonSearchData> commonSearchDatas = commonSearchService.searchData("AAFillComboAPP", objectid, "HOPE", "2", activityid, childId, null, null, null, null);
                if (tc.equalsIgnoreCase("tc")) {
                    int approverId = tenderService.getTenderCommitteeChairperson(Integer.parseInt(objectid));
                    aprverId = Integer.toString(approverId);
                }
                //int approverId = tenderService.getTenderCommitteeChairperson(6496);
                else{
                    aprverId = commonSearchDatas.get(0).getFieldName1();
                }
                //todo: tender committee chairman should be approver
                //String aprverId = "5036";
                List<CommonAppData> editdata1 = workFlowSrBean.editWorkFlowData(fieldName, eventid, "");
                Iterator it1 = editdata1.iterator();
                while (it1.hasNext()) {
                    CommonAppData commonAppData = (CommonAppData) it1.next();
                    moduleName = commonAppData.getFieldName1();
                    eventName = commonAppData.getFieldName2();

                }

                String temps = userid1 + "@" + aprverId + "@";
    //end changed by nafiul for auto workflow generation

                String[] userids = temps.split("@");
                StringBuffer uids = new StringBuffer();

                for (int ui = 0; ui < userids.length; ui++) {
                    uids.append(userids[ui] + "@$");
                }
                uids.delete(uids.length() - 2, uids.length());

                wfRoleids.append("1@$");

                for (int wf = 2; wf <= (userids.length - 1); wf++) {
                    if (request.getParameter("wfrole" + wf) != null) {
                        if (request.getParameter("wfrole" + wf).equalsIgnoreCase("Reviewer")) {
                            wfRoleids.append("3@$");
                        } else if (request.getParameter("wfrole" + wf).equalsIgnoreCase("Donor")) {
                            wfRoleids.append("4@$");
                        }
                    } else if (request.getParameter("revlabl" + wf) != null) {
                        if (request.getParameter("revlabl" + wf).equalsIgnoreCase("Reviewer")) {
                            wfRoleids.append("3@$");
                        } else if (request.getParameter("revlabl" + wf).equalsIgnoreCase("Donor")) {
                            wfRoleids.append("4@$");
                        }
                    }
                }

                wfRoleids.append("2");
                procurids.append(startsbyid + "@$");
                String prbuf = request.getParameter("procureids");

                if (prbuf != null && !"".equals(prbuf)) {
                    String[] prids = prbuf.split("@");
                    for (int k = 0; k < prids.length; k++) {
                        procurids.append(prids[k]);
                        procurids.append("@$");
                    }
                }

                procurids.append(endsbyid);

                String[] prId = procurids.toString().split("@\\$");
                String prcId = procurids.toString();
                prcId = prcId.replace("@$@", "@");

                fileOnHand.append("YES@$");
                for (int f = 1; f < userids.length; f++) {
                    fileOnHand.append("NO@$");
                }
                for (int lev = 0; lev < userids.length; lev++) {
                    WfLevel.append(lev + 1 + "@$");
                }

                WfLevel.delete(WfLevel.length() - 2, WfLevel.length());
                fileOnHand.delete(fileOnHand.length() - 2, fileOnHand.length());
                if ("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)) {
                    objectid = childId;
                }

                // List<CommonMsgChk> listdata = null;
                workFlowSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                List listdata = workFlowSrBean.addWorkFlowLevel(Integer.valueOf(eventid), Integer.valueOf(objectid),
                        actionDate, Integer.valueOf(activityid), Integer.valueOf(childid), fileOnHand.toString(), action, 0,
                        userid1, WfLevel.toString(), uids.toString(), wfRoleids.toString(),
                        prcId);

                if ("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)) {
                    objectid = request.getParameter("objectid");
                }
                if (listdata.size() > 0) {
                    CommonMsgChk msgchk = (CommonMsgChk) listdata.get(0);

                    String officdesig = "";
                    String[] od = new String[3];

                    StringBuffer mailto = new StringBuffer();
    //            moduleName = request.getParameter("modulename");
    //            eventName = request.getParameter("eventname");
                    String fromEmailId = "";
                    List gettingfromemail = messageProcessSrbean.getLoginmailIdByuserId(uid);
                    if (gettingfromemail.size() > 0) {
                        TblLoginMaster loginMaster2 = (TblLoginMaster) gettingfromemail.get(0);
                        fromEmailId = loginMaster2.getEmailId();
                    }
                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();

                    SendMessageUtil smu = new SendMessageUtil();
                    if (userids.length > 0) {
                        for (int j = 0; j < userids.length; j++) {
                            System.out.println("activityID " + activityid + " :: eventId " + eventid + " :: objectid " + objectid + " :: module name " + moduleName);
                            if (userids[j] == null || userids[j].toString().equals("")) {
                                continue;
                            }
                            if ("20".equalsIgnoreCase(prId[j])) {
                                officdesig = workFlowSrBean.getOfficeNameAndDesignationByDP(Integer.parseInt(userids[j].toString()));
                            } else {
                                officdesig = workFlowSrBean.getOfficialAndDesignationName(Integer.parseInt(userids[j].toString()));
                            }
                            od = officdesig.split(",");
                            ofcname = od[0];
                            desig = od[1];
                            int toid = Integer.parseInt(userids[j]);
                            List l = messageProcessSrbean.getLoginmailIdByuserId(toid);
                            TblLoginMaster loginMaster = (TblLoginMaster) l.get(0);
                            String tomailid = loginMaster.getEmailId();
                            service.contentAdmMsgBox(tomailid, XMLReader.getMessage("emailIdNoReply"), strSubject, msgBoxContentUtility.conWorkflow(eventName, moduleName, appCode, ofcname, desig, fromEmailId));
                            // mailto.append(tomailid+",");

                            String[] multimails = new String[1];
                            multimails[0] = tomailid;
                            MailContentUtility mc = new MailContentUtility();
                            List list = mc.conWorkflow(eventName, moduleName, appCode, ofcname, desig, fromEmailId, tenderId);
                            String sub = list.get(0).toString();
                            String mailText = list.get(1).toString();
                            smu.setEmailTo(multimails);
                            smu.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                            smu.setEmailSub(sub);
                            smu.setEmailMessage(mailText);
                            smu.sendEmail();
                        }

                        /*Doahtec Start Tender Cancel*/
                        Integer noOfReviewer = 0;
                        List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData("workflowedit", activityid, objectid);
                        Iterator it = editdata.iterator();
                        while (it.hasNext()) {
                            CommonAppData commonAppData = (CommonAppData) it.next();
                            noOfReviewer = Integer.valueOf(commonAppData.getFieldName3());
                        }
                        if (Integer.valueOf(activityid).intValue() == 9 && noOfReviewer == 0 && userids[0].equalsIgnoreCase(userids[1])) {
                            List listdata1 = workFlowSrBean.addWorkFlowLevel(Integer.valueOf(eventid), Integer.valueOf(objectid),
                                    actionDate, Integer.valueOf(activityid), Integer.valueOf(childid), fileOnHand.toString(), "Tender Cancel", 0,
                                    userids[0], WfLevel.toString(), userids[0], wfRoleids.toString(),
                                    prcId);
                            if (listdata.size() > 0) {
                                CommonMsgChk msgchk1 = (CommonMsgChk) listdata.get(0);
                                System.out.println(" Tender Cancel Status: " + msgchk1.getMsg());
                            }
                        }
                        /*Dohatec End Tender Cancel*/
                    }

                }

                if (Integer.valueOf(activityid).intValue() == 1) {
                    response.sendRedirect("APPDashboard.jsp?appID=" + objectid);
                    return;
                } else if (Integer.valueOf(activityid).intValue() == 2 || Integer.valueOf(activityid).intValue() == 9) {
                    response.sendRedirect("Notice.jsp?tenderid=" + objectid);
                    return;
                } else if (Integer.valueOf(activityid).intValue() == 5) {
                    response.sendRedirect("OpenComm.jsp?tenderid=" + objectid);
                    return;
                } else if (Integer.valueOf(activityid).intValue() == 6) {
                    response.sendRedirect("EvalComm.jsp?tenderid=" + objectid);
                    return;
                } else if (Integer.valueOf(activityid).intValue() == 4) {
                    response.sendRedirect("Amendment.jsp?tenderid=" + objectid);
                    return;
                } else if (Integer.valueOf(activityid).intValue() == 3) {
                    response.sendRedirect("PreTenderMeeting.jsp?tenderId=" + objectid);
                    return;
                } else if (Integer.valueOf(activityid).intValue() == 8 || Integer.valueOf(activityid).intValue() == 13) { // Activity Id = 13 : Added By Dohatec){
                    if (loi.equalsIgnoreCase("Loi")) {
                            response.sendRedirect("LOI.jsp?tenderid=" + objectid);
                        }
                    else response.sendRedirect("EvalComm.jsp?tenderid=" + objectid);
                    return;
                } else if (Integer.valueOf(activityid).intValue() == 10) {
                    response.sendRedirect("TabContractTermination.jsp?tenderId=" + tenderID);
                    return;
                } else if (Integer.valueOf(activityid).intValue() == 11) {
                    if (wpId != null && !"".equalsIgnoreCase(wpId) && !"null".equalsIgnoreCase(wpId)) {
                        if ("0".equalsIgnoreCase(wpId)) {
                            response.sendRedirect("WorkScheduleMain.jsp?tenderId=" + tenderID);
                            return;
                        }
                    } else {
                        response.sendRedirect("DeliverySchedule.jsp?tenderId=" + tenderID);
                        return;
                    }
                } else if (Integer.valueOf(activityid).intValue() == 12) {
                    response.sendRedirect("repeatOrderMain.jsp?tenderId=" + tenderID);
                    return;
                }
            } else {
                List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(fieldName, eventid, "");
                Iterator it = editdata.iterator();
                StringBuffer stby = new StringBuffer();
                StringBuffer endby = new StringBuffer();
                StringBuffer nstby = new StringBuffer();
                StringBuffer nendby = new StringBuffer();
                while (it.hasNext()) {
                    CommonAppData commonAppData = (CommonAppData) it.next();
                    moduleName = commonAppData.getFieldName1();
                    eventName = commonAppData.getFieldName2();
                    stby.append(commonAppData.getFieldName3() + ",");
                    endby.append(commonAppData.getFieldName4() + ",");

                }
                if (stby.length() > 0) {

                    String str = stby.toString();
                    String[] str1 = str.split(",");
                    for (int j = 0; j < str1.length; j++) {
                        boolean check = false;
                        if (nstby.length() > 0) {
                            String str2 = nstby.toString();
                            for (int k = 0; k < str2.split(",").length; k++) {
                                if (str1[j].equals(str2.split(",")[k])) {
                                    check = true;
                                }
                            }
                            if (!check) {
                                nstby.append(str1[j] + ",");
                            }
                            continue;
                        } else {
                            nstby.append(str1[j] + ",");
                        }
                    }
                    nstby.deleteCharAt(nstby.length() - 1);
                }
                if (endby.length() > 0) {
                    String str = endby.toString();
                    String[] str1 = str.split(",");
                    for (int j = 0; j < str1.length; j++) {
                        boolean check = false;
                        if (endby.length() > 0) {
                            String str2 = nendby.toString();
                            for (int k = 0; k < str2.split(",").length; k++) {
                                if (str1[j].equals(str2.split(",")[k])) {
                                    check = true;
                                }
                            }
                            if (!check) {
                                nendby.append(str1[j] + ",");
                            }
                            continue;
                        } else {
                            nendby.append(str1[j] + ",");
                        }
                    }
                    nendby.deleteCharAt(nendby.length() - 1);
                }

                String str1 = nstby.toString();
                startsBy = str1.split(",");
                String str2 = nendby.toString();
                endsBy = str2.split(",");
                procurementRoles = workFlowSrBean.getRoles();
                if ("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)) {
                    objectid = childid;
                }
                donor = workFlowSrBean.isDonorReq(eventid,
                        Integer.valueOf(objectid));
                String[] sarr = donor.split("_");
                noRows = Integer.parseInt(sarr[1]);
                noRows += 2;

                CommonSearchServiceSrBean commonSearchServiceSrBean = new CommonSearchServiceSrBean();
                if ("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)) {
                    objectid = request.getParameter("objectid");
                }


        %>

        <!--Dashboard Header Start-->
        <div class="topHeader">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
        </div>
        <div class="contentArea_1">
            <div align="right">
                <% if (Integer.parseInt(activityid) == 1) {%>
                <a class="action-button-goback" href="APPDashboard.jsp?appID=<%=objectid%>">Go back to Dashboard</a>
                <% } else if (Integer.parseInt(activityid) == 2 || Integer.parseInt(activityid) == 9) {%>
                <a class="action-button-goback" href="Notice.jsp?tenderid=<%=objectid%>">Go back to Tender Dashboard</a>
                <% } else if (Integer.parseInt(activityid) == 5) {%>
                <a class="action-button-goback" href="OpenComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                <% } else if (Integer.parseInt(activityid) == 6) {%>
                <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                <% } else if (Integer.parseInt(activityid) == 4) {%>
                <a class="action-button-goback" href="Amendment.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                <% } else if (Integer.parseInt(activityid) == 3) {%>
                <a class="action-button-goback" href="PreTenderMeeting.jsp?tenderId=<%=objectid%>">Go back to Dashboard</a>
                <% } else if (Integer.parseInt(activityid) == 8 || Integer.parseInt(activityid) == 13) {%> <!-- Activity Id 13 = Contract Approval Workflow : Added By Dohatec -->
                <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                <% } else if (Integer.parseInt(activityid) == 10) {%>
                <a class="action-button-goback" href="TabContractTermination.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
                <%} else if (Integer.parseInt(activityid) == 11) {
                    if (wpId != null && !"".equalsIgnoreCase(wpId) && !"null".equalsIgnoreCase(wpId)) {
                        if ("0".equalsIgnoreCase(wpId)) {
                %>
                <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
                <%
                    }
                } else {
                %>
                <a class="action-button-goback" href="DeliverySchedule.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
                <%}
                } else if (Integer.parseInt(activityid) == 12) {%>
                <a class="action-button-goback" href="repeatOrderMain.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
                <%}%>
            </div>
            <% if (Integer.parseInt(activityid) == 1) {%>
            <div> <jsp:include page="InformationBar.jsp">
                    <jsp:param name="appId" value="<%=objectid%>" />
                </jsp:include></div>
                <%  } else if (Integer.parseInt(activityid) == 2 || Integer.parseInt(activityid) == 5
                        || Integer.parseInt(activityid) == 6 || Integer.parseInt(activityid) == 4
                        || Integer.parseInt(activityid) == 3 || Integer.parseInt(activityid) == 8 || Integer.parseInt(activityid) == 9 || Integer.parseInt(activityid) == 13) { // Activity Id 13 = Contract Approval Workflow : Added By Dohatec
      pageContext.setAttribute("tenderId", objectid);%>

            <div> <%@include file="../resources/common/TenderInfoBar.jsp" %></div>
            <%
            } else if (Integer.parseInt(activityid) == 10 || Integer.parseInt(activityid) == 11 || Integer.parseInt(activityid) == 12) {
                pageContext.setAttribute("lotId", request.getParameter("lotId"));
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
            %>
            <div> <%@include file="../resources/common/ContractInfoBar.jsp" %></div>
            <% }%>
            <div class="tableHead_1 t_space">Workflow : Add Users</div>


            <table width="100%" cellspacing="0" class="tableList_1">
                <form method="POST" name="frmWorkflowUsers" action="WorkflowLevelCreate.jsp" id="frmWorkflowUsers" onsubmit="return valid_uservalues(<%=noRows%>);">
                    <tr>
                        <th width="13px" >Level No.</th>
                        <th width="100px">Workflow Role</th>
                        <th>Procurement Role</th>
                        <th>Name of Official ,[Designation at Office]</th>
                        <th>Action</th>
                    </tr>
                    <tr>
                        <td class="t-align-center">1</td>
                        <td class="t-align-center" style="font-weight:bold;">Initiator</td>
                        <td class="t-align-center">
                            <select name="startsby"  onchange="changeProRole(this.id)" class="formTxtBox_1" id="cmbstartsby1" style="width:100px;">
                                <%
                                    Iterator rls = procurementRoles.iterator();
                                    boolean stusernames = false;
                                    String stOffcAndDesg = " ";
                                    while (rls.hasNext()) {
                                        TblProcurementRole procurementRole = (TblProcurementRole) rls.next();
                                        for (int i = 0; i < startsBy.length; i++) {
                                            Byte b = Byte.valueOf(startsBy[i]);
                                            String procureRole = "";
                                            if (procurementRole.getProcurementRole().equalsIgnoreCase("HOPE")) {
                                                procureRole = "HOPA";
                                            } else if (procurementRole.getProcurementRole().equalsIgnoreCase("PE")) {
                                                procureRole = "PA";
                                            } else {
                                                procureRole = procurementRole.getProcurementRole();
                                            }
                                            if (procurementRole.getProcurementRoleId() == b) {
                                                out.write("<option  value='" + procurementRole.getProcurementRoleId()
                                                        + "' />" + procureRole + "</option>");
                                                if (!stusernames) {
                                                    stOffcAndDesg = commonSearchServiceSrBean.getUserNames("AAFillComboAPP", objectid, Integer.toString(procurementRole.getProcurementRoleId()), Integer.toString(1), activityid, childid, Integer.toString(1), 0);
                                                    stusernames = true;

                                                }
                                            }
                                        }
                                    }

                                %>

                            </select>
                        </td>
                        <td  class="t-align-center">
                            <div id="us1">
                                <%=stOffcAndDesg%>
                            </div>

                        </td>

                        <td>&nbsp;</td>


                    </tr>
                    <%
                        String[] norevs = donor.split("_");
                        int l = 2;
                        int val = Integer.parseInt(norevs[1]);
                        int c = 2;
                        String colorchange = "style='background-color:#E4FAD0;' ";
                        for (int i = 1; i <= val; i++) {


                    %>

                    <tr <% if (c == l && val > 1) {
                            out.print(colorchange);
                        } else if (val == 1) {
                            out.print(colorchange);
                        }%> >

                        <td class="t-align-center"><%=l%></td>
                        <td class="t-align-center">
                            <%
                                String displaystyle = "display:inline";
                                boolean check = false;
                                if (!norevs[0].equalsIgnoreCase("no") && norevs[0] != null && val > 1) {
                                    displaystyle = "display:none";
                                    check = true;
                            %>
                            <select name="wfrole<%=l%>" style="font-weight:bold; width:170px;" onchange="changerole(this.id)" class="formTxtBox_1" id='cmbreviewer<%=l%>' >


                                out.write("<option selected='true' value="Donor" >Development Partner</option>");
                                out.write("<option value='Reviewer'>Reviewer</option>");


                            </select>
                            <%  } else if (!norevs[0].equalsIgnoreCase("no") && norevs[0] != null && val == 1) {
                                displaystyle = "display:none";
                                check = true;
                            %>
                            <label id="revlab" style="font-weight:bold;">Development Partner</label>
                            <input id="revlabl<%=l%>" name="revlabl<%=l%>" value="Donor" type="hidden" />
                            <%  } else {
                            %>
                            <label id="revlab" style="font-weight:bold;"> Reviewer</label>
                            <input id="revlabl<%=l%>" name="revlabl<%=l%>" value="Reviewer" type="hidden" />
                            <%  } %>
                        </td>
                        <td class="t-align-center">
                            <% if (check) {%>
                            <label id="labelreviewer<%=l%>" >Development Partner</label>
                            <input type="hidden" name="donorval<%=l%>"  value="20" id="donorval<%=l%>" />
                            <%  } else {%>
                            <label id="labelreviewer<%=l%>" ></label>
                            <input type="hidden" name="donorval<%=l%>"  value="" id="donorval<%=l%>" />
                            <% } %>


                            <%
                                String revOffcAndDesg = " ";
                                String revdisplay = "display:table-cell;";
                                String donordisplay = "display:none";
                                String procureRolesCombo = "";

                                if (check) {
                                    revOffcAndDesg = commonSearchServiceSrBean.getUserNames("AAFillComboAPP", objectid, Integer.toString(20), Integer.toString(3), activityid, childid, Integer.toString(l), 0);
                                    revdisplay = "display:none;";
                                    donordisplay = "display:table-cell;";

                                } else if (!check) {
                                    procureRolesCombo = commonSearchServiceSrBean.getProcureRoles(l, 0);
                                }


                            %>
                            <div id="prostr<%=l%>">
                                <%=procureRolesCombo%>
                            </div>


                        </td>

                        <td  id="tddon<%=l%>" class="t-align-center" style="<%=donordisplay%>">
                            <div id="us<%=l%>">
                                <%=revOffcAndDesg%>
                            </div>

                        </td>

                        <td id="tdrev<%=l%>" class="t-align-center" style="<%=revdisplay%>">

                        </td>

                        <td id="tdlnk<%=l%>" class="t-align-center" style="<%=revdisplay%>" >
                            <% if (action.equalsIgnoreCase("Create")) {

                            %>
                            <img src="../resources/images/Dashboard/addIcn.png" alt="add user" id="addicn<%=l%>" class="linkIcon_1" />
                            <a href="#" id='adduser<%=l%>'   onclick=" var b = document.getElementById('cmdreviewer<%=l%>');
                    var d = document.getElementById('donorval<%=l%>');
                    window.open('AddWorkFlowUser.jsp?userval=uservalue' +<%=l%> + '&userid=txtuserid' +<%=l%> + '&wfroleid=' + b.options[b.options.selectedIndex].value + '&uid=' +<%=userid1%> + '&donorval=' + d.value + '', 'window', 'resizable=yes,scrollbars=yes,width=900,height=500')">Select User</a>
                            <% }%>
                            <img src="../resources/images/Dashboard/removeIcn.png" style="display:none;"  alt="remove user" id="removeicn<%=l%>" class="linkIcon_1" />
                            <a href="javascript:removeuser('remuser<%=l%>')" style="display:none;" id='remuser<%=l%>' >Change User</a>
                        </td>

                        <td id="tdempty<%=l%>" style="<%=donordisplay%>">&nbsp;</td>


                    </tr>
                    <%
                            if (c == l) {
                                c += 2;
                            }
                            l++;

                        }
                    %>
                    <tr <% if (c == l && val != 1) {
                            out.print(colorchange);
                        }%>>
                        <td class="t-align-center"><%=l%></td>
                        <td class="t-align-center" style="font-weight:bold;">Approver</td>
                        <td class="t-align-center">
                            <select name="endsby" onchange="changeProRole(this.id)" class="formTxtBox_1" id="cmbendsby<%=l%>" style="width:100px;">
                                <%
                                    List<Object> cpId = null;
                                    int isPECP = 0;
                                    if (Integer.parseInt(activityid) == 13 && Integer.parseInt(eventid) == 12) {
                                        cpId = comService.getCommitteeCPId(objectid);
                                        if (cpId != null && !cpId.isEmpty()) {
                                            if (cpId.get(0).toString().equals(userid1)) {
                                                isPECP = 1;
                                            }
                                        }
                                    }

                                    Iterator endsrls = procurementRoles.iterator();
                                    boolean endusernames = false;
                                    String endOffcAndDesg = " ";
                                    while (endsrls.hasNext()) {
                                        TblProcurementRole procurementRole = (TblProcurementRole) endsrls.next();
                                        for (int i = 0; i < endsBy.length; i++) {
                                            Byte b = Byte.valueOf(endsBy[i]);
                                            String procureRoleA = "";
                                            if (procurementRole.getProcurementRole().equalsIgnoreCase("HOPE")) {
                                                procureRoleA = "HOPA";
                                            } else if (procurementRole.getProcurementRole().equalsIgnoreCase("PE")) {
                                                procureRoleA = "PA";
                                            } else {
                                                procureRoleA = procurementRole.getProcurementRole();
                                            }
                                            if (procurementRole.getProcurementRoleId() == b && procurementRole.getProcurementRoleId() != isPECP) {
                                                out.write("<option  value='" + procurementRole.getProcurementRoleId()
                                                        + "' />" + procureRoleA + "</option>");
                                                if (!endusernames) {
                                                    endOffcAndDesg = commonSearchServiceSrBean.getUserNames("AAFillComboAPP", objectid, Integer.toString(procurementRole.getProcurementRoleId()), Integer.toString(2), activityid, childid, Integer.toString(l), 0);
                                                    endusernames = true;
                                                }
                                            }
                                        }
                                    }

                                %>

                            </select>
                        </td>
                        <td class="t-align-center">
                            <div id="us<%=l%>">
                                <%=endOffcAndDesg%>
                            </div>


                        </td>
                        <td>&nbsp;</td>

                    </tr>
                    <tr>
                        <td class="t-align-center" colspan="5"><span id="wfsub" class="formBtn_1">
                                <input name="tenderId" type="hidden" value="<%=request.getParameter("tenderId")%>" />
                                <%
                                    if (request.getParameter("activityid").equalsIgnoreCase("13") && request.getParameter("eventid").equalsIgnoreCase("12")) {
                                %>
                                <input type="button"  value="Submit" id="btnWfSubmit" name="btnWfSubmit" onclick="dis(this.id);" />
                                <input type="hidden" name="btnsubmit" id="btnsubmit"/>
                                <%
                                } else {
                                %>
                                <input type="submit"  value="Submit" id="btnsubmit" name="btnsubmit" onclick="dis(this.id);" />
                                <%}%>

                                <input type="hidden" value="<%=eventid%>" name="eventid" />
                                <input type="hidden" value="<%=activityid%>" name="activityid" id="activityid"/>
                                <input type="hidden" value="<%=objectid%>"  name="objectid" id="objectid" />
                                <input type="hidden" value="<%=childid%>" name="childid" id="childid" />
                                <input type="hidden" value="<%=action%>" name="action" />
                                <input type="hidden" value="<%=moduleName%>" name="modulename" />
                                <input type="hidden" value="<%=eventName%>" name="eventname" />
                                <input type="hidden" value="<%=l%>" name="trw" id="trw" />
                                <input type="hidden" value="" name="userids" id="userids" />
                                <input type="hidden" value="" name="procureids" id="procureids" />
                                <input type="hidden" value="<%=wpId%>" name="wpId" />
                                <!-- Dohatec Start -->
                                <input type="hidden" value="<%=request.getParameter("activityid") + " " + request.getParameter("eventid")%>" name="evntActId" id="evntActId"/>
                                <!-- Dohatec End -->
                            </span></td>
                    </tr>
                </form>
            </table>
        </div>


        <!-- Dohatec Start -->
        <!-- Dialog Box -->
        <div id="dialog-confirm" title="Contract Approval Workflow" style="display:none">
            <p id="msgConfirm">
                Are you sure that you have created workflow and number of  Reviewers Correctly\Rightly? Be sure before clicking ‘Yes’ button.
            </p>
        </div>
        <!-- Dohatec End -->

        <!--Dashboard Content Part End-->
        <!--Dashboard Footer Start-->
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->

        </body>
    <%
        String str = "headTabWorkFlow";
        if (request.getParameter("hed") != null) {
            str = request.getParameter("hed").trim();
        }
    %>
    <script type="text/javascript">
         var headSel_Obj = document.getElementById('<%=str%>');
         if (headSel_Obj != null) {
             headSel_Obj.setAttribute("class", "selected");
         }

    </script>
    <script language="JavaScript" type="text/javascript">
        function changerole(x) {
            //alert(x);
            var str = new String(x);
            var str2 = str.substring(str.length - 1, str.length);
            //alert(str2);
            var rev = document.getElementById(x);
            var wfrole = rev.options[rev.selectedIndex].value;
            var labl = document.getElementById("us" + str2);

            var tddon = document.getElementById("tddon" + str2);
            var tdrev = document.getElementById("tdrev" + str2);
            var tdlnk = document.getElementById("tdlnk" + str2);
            var tdempty = document.getElementById("tdempty" + str2);

            if (wfrole == "Donor") {

                var rev1 = document.getElementById('cmdreviewer' + str2);
                var lab = document.getElementById('labelreviewer' + str2);
                var txt = document.getElementById("donorval" + str2);
                var proroles = document.getElementById('prostr' + str2);
                tdrev.innerHTML = '';
                tdrev.style.display = 'none';
                tddon.style.display = 'table-cell';
                tdlnk.style.display = 'none';
                tdempty.style.display = 'table-cell';
                proroles.innerHTML = "";
                //alert(txt);
                for (m = rev1.options.length - 1; m > 0; m--) {
                    //alert(rev1.options[m].value);
                    //if(rev1.options[m].value == '20') {
                    //alert(rev1.options[m].value);
                    //alert(rev1);
                    rev1.style.display = 'none';
                    lab.style.display = 'inline';
                    //rev1.options[m].selected=true;
                    lab.innerHTML = 'Development Partner';
                    txt.value = '20';
                    //alert('end');
                    //}

                }

                labl.innerHTML = '';
                 $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName: 'AAFillComboAPP', objectId: $('#objectid').val(), Prorole: '20', childId: $('#childid').val(), activityId: $('#activityid').val(), wfroleId: '4', revId: str2}, function (j) {
                    $(labl).html(j);
                });

            } else {
                var proroles = document.getElementById('prostr' + str2);

                var lab = document.getElementById('labelreviewer' + str2);
                var txt = document.getElementById("donorval" + str2);
                var rem = document.getElementById("remuser" + str2);
                var add = document.getElementById('adduser' + str2);
                var remicn = document.getElementById("removeicn" + str2);
                var addicn = document.getElementById("addicn" + str2);

                lab.style.display = 'none';
                txt.value = "";
                tdrev.style.display = 'table-cell';
                tddon.style.display = 'none';
                tdlnk.style.display = 'table-cell';
                tdempty.style.display = 'none';

                labl.innerHTML = '';
                rem.style.display = 'none';
                add.style.display = 'inline';
                remicn.style.display = "none";
                remicn.style.display = 'none';
                addicn.style.display = "table-cell";
                addicn.style.display = "inline";
                var test = "";

                 $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName: 'proRoles', revId: str2, selected: '0'}, function (j) {
                    $(proroles).html(j);
                });

            }

        }

        function changeProRole(x) {

            var str = new String(x);
            var str2 = null;
            var Prorole = 0;
            var revid = 0;
            var wfrolid = 0;
            if (str != null)
                revid = str.substring(str.length - 1, str.length);
            str2 = str.substr(0, str.length - 1);
            var labl = document.getElementById("us" + revid);

            if (str2 == 'cmbstartsby') {
                wfrolid = 1;
                var temp = document.getElementById(str);
                // alert(temp);
                Prorole = temp.options[temp.selectedIndex].value;

                 $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName: 'AAFillComboAPP', objectId: $('#objectid').val(), Prorole: Prorole, childId: $('#childid').val(), activityId: $('#activityid').val(), wfroleId: wfrolid, revId: revid}, function (j) {
                    $(labl).html(j);
                });
            } else if (str2 == 'cmbendsby') {
                wfrolid = 2;
                var temp = document.getElementById(str);
                Prorole = temp.options[temp.selectedIndex].value;

                 $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName: 'AAFillComboAPP', objectId: $('#objectid').val(), Prorole: Prorole, childId: $('#childid').val(), activityId: $('#activityid').val(), wfroleId: wfrolid, revId: revid}, function (j) {
                    $(labl).html(j);
                });
            } else {
                wfrolid = 3;
                var temp = document.getElementById(str);
                Prorole = temp.options[temp.selectedIndex].value;
                var selectedval = document.getElementById("revlev" + revid);
                selectedval.value = Prorole;
                var tddon = document.getElementById("tddon" + revid);
                var tdlnk = document.getElementById("tdlnk" + revid);
                var labl = document.getElementById("us" + revid);
                var tdrev = document.getElementById("tdrev" + revid);
                if (Prorole != 21) {
                    labl.innerHTML = '';
                    tddon.style.display = 'none';
                    tdlnk.style.display = 'table-cell';
                    removeuser('remuser' + revid);
                } else {
                    var rem = document.getElementById('remuser' + revid);
                    var add = document.getElementById('adduser' + revid);
                    var remicn = document.getElementById("removeicn" + revid);
                    var addicn = document.getElementById("addicn" + revid);
                    rem.style.display = 'none';
                    add.style.display = 'inline';
                    remicn.style.display = "none";
                    remicn.style.display = 'none';
                    addicn.style.display = "table-cell";
                    addicn.style.display = "inline";
                    tddon.style.display = 'table-cell';
                    tdlnk.style.display = 'none';
                    tdrev.innerHTML = '';
                    $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName: 'AAFillComboAPP', objectId: $('#objectid').val(), Prorole: Prorole, childId: $('#childid').val(), activityId: $('#activityid').val(), wfroleId: wfrolid, revId: revid}, function (j) {
                        $(labl).html(j);
                    });
                }
            }
        }


        function removeuser(y) {
            //alert(y);
            var str = new String(y);
            var str2 = str.substring(str.length - 1, str.length);
            //alert(str2);
            var rem = document.getElementById(y);
            var add = document.getElementById('adduser' + str2);
            var label = document.getElementById('uservalue' + str2);
            var remicn = document.getElementById("removeicn" + str2);
            var addicn = document.getElementById("addicn" + str2);
            var tdrev = document.getElementById("tdrev" + str2);
            //alert(remicn);
            tdrev.innerHTML = '';
            label.style.display = 'none';
            label.innerHTML = "";
            rem.style.display = 'none';
            add.style.display = 'inline';
            remicn.style.display = "none";
            remicn.style.display = 'none';
            addicn.style.display = "table-cell";
            addicn.style.display = "inline";

            var b = document.getElementById('cmdreviewer' + str2);
            var d = document.getElementById('donorval' + str2);
            window.open('AddWorkFlowUser.jsp?userval=uservalue' + str2 + '&userid=txtuserid' + str2 + '&wfroleid=' + b.options[b.options.selectedIndex].value + '&uid=' +<%=userid1%> + '&donorval=' + d.value + '', 'window', 'resizable=yes,scrollbars=yes,width=900,height=500');
        }
        function valid_uservalues(norows) {
            //alert(norows);
            var check = true;
            var uvalues = new Array();
            for (var i = 1; i <= norows; i++) {
                var lab = document.getElementById("txtuserid" + i);
                //alert(lab);
                if (lab != null) {
                    uvalues[i - 1] = lab.value;
                }

                var donorcheck = false;
                var came = false;
                for (var w = 2; w < norows; w++) {
                    if (document.getElementById("cmbreviewer" + w) != null) {
                        var wfrev = document.getElementById("cmbreviewer" + w);
                        var wfrole = wfrev.options[wfrev.selectedIndex].value;
                        came = true;

                    }
                    if (wfrole != null) {
                        if (wfrole == 'Donor') {
                            donorcheck = true;
                        }

                    }

                }

                if (donorcheck == false && came == true) {
                    jAlert("Please select Development Partner", " Alert ", "Alert");
                    $("#wfsub").show();
                    return false;
                }

                // alert(uvalues[i-1]);
                //alert('cam');
                if (lab != null) {
                    if (lab.value == "" || lab.value == 0) {
                        //alert('cam1');
                        jAlert("User not available", " Alert ", "Alert");
                        $("#wfsub").show();
                        // alert("Please select official");
                        check = false;
                        break;
                    }
                } else if (lab == null) {
                    //alert('cam2');
                    jAlert(" Please select user", " Alert ", "Alert");
                    $("#wfsub").show();
                    // alert("Please select official");
                    check = false;
                    break;
                }
            }
            if (check) {

                // Dohatec Start
                if ($('#evntActId').val() == '13 12')
                    return true;
                // Dohatec End

                //alert(uvalues.length);
                for (var j = 1; j < uvalues.length - 1; j++) {
                    if (uvalues[0] == uvalues[j] || uvalues[uvalues.length - 1] == uvalues[j]) {
                        check = false;
                        jAlert("Reviewer can't be as same as Initiator/Approver", " Alert ", "Alert");
                        $("#wfsub").show();
                    }

                }
            }

            if (check) {

                // Dohatec Start
                if ($('#evntActId').val() == '13 12')
                    return true;
                // Dohatec End

                for (var j = 1; j < uvalues.length - 1; j++) {
                    for (var k = j; k < uvalues.length; k++) {

                        if (uvalues[j] == uvalues[k + 1]) {
                            //alert('user already added');
                            jAlert("user already added", " Alert ", "Alert");
                            $("#wfsub").show();
                            return false;
                        }

                    }

                }
            }

            // alert(check);
            return check;
        }


        function dis(ds) {

            // Dohatec Start
        <%if (request.getParameter("activityid").equalsIgnoreCase("13") && request.getParameter("eventid").equalsIgnoreCase("12")) {%>
            if ($('#evntActId').val() == '13 12')
            {
                $(function () {
                    $("#dialog-confirm").dialog({
                        resizable: false,
                        height: 150,
                        width: 430,
                        modal: true,
                        buttons: {
                            "Yes": function () {
                                $(this).dialog("close");
                                $('#btnsubmit').val('Submit');
                                $('form#frmWorkflowUsers').submit();
                            },
                            "No": function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            }
        <%}%>
            // Dohatec End

            var buf = "";
            var trws = document.getElementById("trw").value;
            var prbuf = "";
            var test = "";
            for (var h = 2; h < trws; h++) {

                if (document.getElementById("revlev" + h) != null) {
                    test = document.getElementById("revlev" + h).value;
                    prbuf += test;
                    prbuf += '@';
                } else if (document.getElementById("donorval" + h).value != null && document.getElementById("donorval" + h).value != "") {
                    test = document.getElementById("donorval" + h).value;
                    prbuf += test;
                    prbuf += '@';
                }
            }
            //alert(trws);
            for (var i = 1; i <= trws; i++) {
                var uid = document.getElementById('txtuserid' + i);
                //alert(uid);
                buf += uid.value;
                buf += '@';
            }

            //alert(buf);
            // alert(prbuf);

            document.getElementById('procureids').value = prbuf;
            document.getElementById('userids').value = buf;
            //$("#wfsub").hide(); - Previous Code

            // Dohatec Start
            if ($('#evntActId').val() != '13 12') {
                $("#wfsub").hide();
            }
            // Dohatec End

        }

        function changeuser(us) {
            var selusid = document.getElementById(us);
            var str = new String(us);
            var str2 = str.substring(str.length - 1, str.length);
            var uid = document.getElementById('txtuserid' + str2);
            uid.value = selusid.options[selusid.selectedIndex].value;

        }

    </script>

    <%}%>


</html>


