<%--
    Document   : ViewAmendment
    Created on : Nov 16, 2010, 7:32:19 PM
    Author     : TaherT,Karan,Swati
--%>

<%@page import="java.io.File"%>
<%@page import="com.cptu.egp.eps.web.utility.FilePathUtility"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.List"%>
<%@page  import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            String titlePublishView = "";
            if("y".equals(request.getParameter("ispub"))){
                titlePublishView = "Publish ";
            }else{
                titlePublishView = "View ";
            }
        %>
         <%
                            if("Publish".equals(request.getParameter("publish"))){
                            try{
                                String reqURL = request.getRequestURL().toString() ;
                                String reqQuery = "id="+request.getParameter("tenderId");
                                String folderName = pdfConstant.TENDERNOTICE;
                                boolean isTenSecUpdated=tenderSrBean.isTenderSectionsUpdated(request.getParameter("tenderId"), request.getParameter("corrid"));
                                tenderSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                tenderSrBean.publishCorri(request.getParameter("tenderId"),request.getParameter("corrid"),request.getParameter("remarks"),"publish",String.valueOf(request.getSession().getAttribute("userId")),reqURL,reqQuery,folderName);
                                tenderSrBean.sendCorriDetailMail(request.getParameter("tenderId"));
                                
                                
                                TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                
                                String tId = request.getParameter("tenderId"); // Set Tender ID in this variable
                                
                                //Get the selected SBD for this tender.
                                List<SPTenderCommonData> getSBDTemplateIdForTender = tenderCommonService1.returndata("getSBDTemplateIdForTender", tId , null);
                                
                                String templateId = getSBDTemplateIdForTender.get(0).getFieldName1(); // Set SBD template ID of this tender in this variable
                                
                                //Get Clarification days based on SBD. templateId of related SBD is passed.
                                List<SPTenderCommonData> getClarificationDays = tenderCommonService1.returndata("getClarificationDays", templateId, null);

                                if (getClarificationDays.size() > 0) 
                                {
                                    String submissionDate = tenderCommonService1.getTenderSubmissionDate(Integer.parseInt(tId));
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    Date ldate = new Date();
                                    ldate = format.parse(submissionDate);

                                    // Number of days prior to tender submission/closing date for posting clarification query
                                    int ClarificationDays = Integer.parseInt(getClarificationDays.get(0).getFieldName1());

                                    // Setting last date of posting clarification query
                                    GregorianCalendar cal = new GregorianCalendar();
                                    cal.setTime(ldate);
                                    cal.add(Calendar.DATE, -ClarificationDays);
                                    ldate = cal.getTime();
                                    
                                    //ldate = new Date(ldate.getTime() - ClarificationDays * 24 * 3600 * 1000);

                                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                                    submissionDate = formatter.format(ldate);
                                    response.sendRedirect("TenderClariAfterSBDSubmit.jsp?tenderId="+tId+"&lastdate="+submissionDate);
                                    return;
                                }
                                
                                if(!isTenSecUpdated){
                                response.sendRedirect("Amendment.jsp?tenderid="+request.getParameter("tenderId")+"&msgId=published");
                                }
                                else{
                                       File    file = new File(FilePathUtility.getFilePath().get("TenderSecUploadServlet")+"\\"+request.getParameter("tenderId"));
                                       if(file.exists()){
                                           if(file.isDirectory()){
                                               for(File f:file.listFiles()){
                                                   if(f.getName().endsWith(".zip"))
                                                       f.delete();
                                                   }
                                           
                                           }
                                           }
                                response.sendRedirect("TenderPdfGenerate.jsp?tenderid="+request.getParameter("tenderId")+"&isCorriPub=true");
                                     
                                  }
                                return;
                           }catch(Exception e){
                               e.printStackTrace();
                           }
                                }
                        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=titlePublishView%>Amendment / Corrigendum Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                 $('#frmViewAmendment').validate({
                    rules:{ remarks: {required:true}
                    },
                    messages:{remarks: {required:"<div class='reqF_1'>Please enter Comments</div>"}
                    }
                });
            });
            function chekFormMark(eventType){
                /*if(eventType=='REOI' || eventType=='reoi'){
                    jAlert("Please select Criteria, Sub Criteria and Technical for all Technical forms "," Alert ", "Alert");
                }else{*/
                    jAlert("Please configure Criteria, Sub Criteria and Technical Score for all Technical forms"," Alert ", "Alert");
                //}
            }

            function checkGSExist(tenderId,corriId)
            {
             var returnFlag=true;
              $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/CreateTenderFormSrvt",
                        data:"action=checkGrandSumNeedAtCorri&tenderId="+tenderId+"&corriId="+corriId,
                        async: false,
                        success: function(j)
			{
				var alertMessage="";
				if(j.toString() != null && $.trim(j.toString()) != "0" && $.trim(j.toString()) == "Deleted")
				{
				    alertMessage="You haven't updated Grand Summary Report after Cancellation of form(s). Please update Grand Summary Report first and then try to proceed further.";
                                    alert(alertMessage);
                                    returnFlag=false;
				}
                                else if(j.toString() != null && $.trim(j.toString()) != "0" && $.trim(j.toString()) == "Added")
				{
                                    alertMessage="You haven't updated Grand Summary Report after addition of new form(s). Please update Grand Summary If you want to add new forms or Do you want to continue to publish it?";
                                    if(confirm(alertMessage))
                                    {
                                        returnFlag= true;
                                    }
                                    else
                                    {
                                        returnFlag=false;
                                    }

                                }
			}
		});

           return returnFlag;

            }

        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1"><%=titlePublishView%>Amendment / Corrigendum Details<span style="float: right;"><a class="action-button-goback" href="Amendment.jsp?tenderid=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a></span></div>
            <%
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        String userTypId="0", userType="Visiter";

                        HttpSession hs = request.getSession();
                         if (hs.getAttribute("userTypeId") != null) {
                            userTypId = hs.getAttribute("userTypeId").toString();
                             if(userTypId.equalsIgnoreCase("2")){
                                userType="Tenderer"; // userType is Tenderer";
                             }else if(userTypId.equalsIgnoreCase("3")){
                                userType="Officer"; // userType is Officer";
                             }else if(userTypId.equalsIgnoreCase("7")){
                                userType="ScheduleBank"; // userType is ScheduleBank";
                             }

                        }



            %>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>



                        <%
                            String tenderId = "";
                            int corrigendumCnt=0;
                            if (!request.getParameter("tenderId").equals("")) {
                                tenderId= request.getParameter("tenderId");
                            }

                            tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            //for (SPTenderCommonData sptcd : tenderCommonService.returndata("CorrigendumInfo", tenderId, userType)) {

                %>
                <%
                  List<SPTenderCommonData> lstCorrigendums =
                        tenderCommonService.returndata("CorrigendumInfo", tenderId, "masterInfo");

                  String corriId="";
                  //if(!lstCorrigendums.isEmpty()){
                    corrigendumCnt = lstCorrigendums.size()+1;
                  //}

                //for (SPTenderCommonData sptcd : tenderCommonService.returndata("CorrigendumInfo", tenderId, "masterInfo")) {
                    for (SPTenderCommonData sptcd : lstCorrigendums) {
                         corrigendumCnt--;
                %>
                <div class="tabPanelArea_1 t_space">
                <table width="100%" cellspacing="10" class="tableView_1">
                     <tr>
                        <td colspan="2" width="100%" class="ff">
                             <% if ("pending".equalsIgnoreCase(sptcd.getFieldName2())) {%>
                                <div style="background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px; width: 33%;">
                                &nbsp;Corrigendum / Amendment No. : &nbsp;<%=(corrigendumCnt)%>
                                 <% if ("pending".equalsIgnoreCase(sptcd.getFieldName2())) {%>
                                <span class="mandatory">&nbsp;(<%=sptcd.getFieldName2()%>)</span>
                            <%}%>
                                &nbsp;
                            </div>
                            <%} else {%>
                                <div style="background-color:#f2dbdb;color: #943634; line-height:20px;border:1px solid #943634;font-family: arial, verdana; font-size: 13px; width: 25%;">
                                &nbsp;Corrigendum / Amendment No. : &nbsp;<%=(corrigendumCnt)%>
                                &nbsp;
                            </div>
                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td width="22%"  class="ff">Amendment / Corrigendum Text :</td>
                        <td width="78%"><%=sptcd.getFieldName3()%></td>
                    </tr>
                    <%  if("y".equals(request.getParameter("ispub"))){
                        if ("pending".equalsIgnoreCase(sptcd.getFieldName2())) {
                            corriId=sptcd.getFieldName1();
                    %>
                    <form id="frmViewAmendment" action="ViewAmendment.jsp" method="post">
                    <tr>
                        <td class="ff">Comments : <span class="mandatory">*</span></td>
                        <td><textarea name="remarks" rows="3" class="formTxtBox_1" id="txtRemarks" style="width:400px;"></textarea></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <input type="hidden" value="<%=tenderId%>" name="tenderId"/>
                            <input type="hidden" value="<%=sptcd.getFieldName1()%>" name="corrid"/>
                             <label class="formBtn_1">
                                 <input type="submit" name="publish" id="btnPublish" value="Publish" /><%--onclick="return checkGSExist('<%=tenderId%>','<%=corriId%>');"/>--%>
                             </label>
                        </td>
                    </tr>
                    </form>
                    <%}}%>
                </table>


                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th class="t-align-center" width="34%">Field Name</th>
                        <th class="t-align-center" width="33%">Old Value</th>
                        <th class="t-align-center" width="33%">New Value</th>
                    </tr>
                    <%
                        int detailCnt = 0, docCnt = 0;

                        for (SPTenderCommonData sptcd1 : tenderCommonService.returndata("CorrigendumInfo", sptcd.getFieldName1(), "detailInfo")) {
                            detailCnt++;
                    %>
                    <tr
                        <%if(Math.IEEEremainder(detailCnt,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                        <%}%>

                        >
                        <td>
                            <%
                                if("securityLastDt".equals(sptcd1.getFieldName1())){
                                    out.print("Last date & time for Tender Security Submission");
                                }else if("preBidStartDt".equals(sptcd1.getFieldName1())){
                                    out.print("Meeting Start Date & Time");
                                }else if("openingDt".equals(sptcd1.getFieldName1())){
                                    out.print("Opening Date & Time");
                                }else if("preBidEndDt".equals(sptcd1.getFieldName1())){
                                    out.print("Meeting End Date & Time");
                                }else if("submissionDt".equals(sptcd1.getFieldName1())){
                                    out.print("Closing Date & Time");
                                }else if("securitySubOff".equals(sptcd1.getFieldName1())){
                                    out.print("Name & Address of the Office(s) for tender security submission");
                                }else if("docEndDate".equals(sptcd1.getFieldName1())){
                                    out.print("Document last selling date & time");
                                }else if("pkgDocFees".equals(sptcd1.getFieldName1())){
                                    out.print("Tender Document  Price (TK)");
                                }else if("pkgDocFeesUSD".equals(sptcd1.getFieldName1())){  // For ICT
                                    out.print("Equivalent Tender Document  Price (Amount in USD)");
                                }else if("eligibilityCriteria".equals(sptcd1.getFieldName1())){
                                    out.print("Eligibility of Consultant");
                                }else if("tenderBrief".equals(sptcd1.getFieldName1())){
                                    out.print("Brief Description of Goods,Works or Service");
                                }else if(sptcd1.getFieldName1().contains("indStartDt")){
                                    out.print("Indicative Start Date(Ref No : "+tenderSrBean.phaseSecureDetail(true, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("indEndDt")){
                                    out.print("Indicative Completion Date(Ref No : "+tenderSrBean.phaseSecureDetail(true, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("docfess")){
                                    out.print("Document Fees(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("location")){
                                    out.print("Location(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("tenderSecurityAmt@")){
                                    out.print("Tender Security (Amount in Nu.)(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("tenderSecurityAmtUSD@")){    // For ICT
                                    out.print("Equivalent Tender Security (Amount in USD)(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("completionTime")){
                                    out.print("Completion Date(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("phasingRefNo")){
                                    out.print("Ref. No(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("phasingOfService")){
                                    out.print("Phasing of service(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }else if(sptcd1.getFieldName1().contains("startTime")){
                                    out.print("Start Date(Lot No : "+tenderSrBean.phaseSecureDetail(false, sptcd1.getFieldName1().substring(sptcd1.getFieldName1().indexOf("@")+1, sptcd1.getFieldName1().length()))+")");
                                }
                                else if(sptcd1.getFieldName1().contains("TDS/PDS")||sptcd1.getFieldName1().contains("PCC")){
                                      String[] corriStr=sptcd1.getFieldName1().split("_");
                                      for(int i=0;i<corriStr.length-2;i++)
                                          out.print(corriStr[i]+"</br>");
                                      
                                    }
                            %>
                        </td>
                        <td ><%=sptcd1.getFieldName2()%></td>
                        <td ><%=sptcd1.getFieldName3()%></td>
                    </tr>
                    <%}
                                                    if (detailCnt == 0) {%>
                    <tr>
                        <td colspan="3" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
<!--                START NEW CODE-->
                <%
                int CorriLotCnt=0;
                for (SPTenderCommonData sptcdLotIds : tenderCommonService.returndata("getCorriDocLotIds", tenderId , sptcd.getFieldName1())) {
                            CorriLotCnt++;

                    if (!"0".equalsIgnoreCase(sptcdLotIds.getFieldName1())){
                    // Tender Docs are Lot Wise

                        List<SPTenderCommonData> lotList = tenderCommonService.returndata("getlotorpackagebytenderid",
                                            tenderId,
                                            "Lot," + sptcdLotIds.getFieldName1());

                        if (!lotList.isEmpty() && lotList.size()>0){%>
<!--                        START: LOT DESCRIPTION TABLE-->
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th class="t-align-center" width="10%">Lot No.</th>
                                <th class="t-align-center">Lot Description  </th>
                            </tr>
                            <tr>
                                <td class="t-align-center" ><%=lotList.get(0).getFieldName1()%></td>
                                <td><%=lotList.get(0).getFieldName2()%></td>
                            </tr>
                        </table>
                            <!-- END: LOT DESCRIPTION TABLE-->
                       <% } %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="4%" class="t-align-center">Sl. No.</th>
                                <th class="t-align-center" width="23%">File Name</th>
                                <th class="t-align-center" width="32%">File Description</th>
                                <th class="t-align-center" width="7%">File Size <br />
                                    (in KB)</th>
                                <th class="t-align-center" width="18%">Action</th>
                            </tr>
                            <%
                             int lotDocCnt=0;

                                for (SPTenderCommonData sptcdLotDocs : tenderCommonService.returndata("lotDocInfo", sptcd.getFieldName1() , sptcdLotIds.getFieldName1())) {
                                // LOT WISE DOCS COMES HERE
                            lotDocCnt++;
                            %>
                            <tr
                                <%if(Math.IEEEremainder(lotDocCnt,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                        <%}%>
                                >
                                <td class="t-align-center" ><%=lotDocCnt%></td>
                                <td ><%=sptcdLotDocs.getFieldName1()%></td>
                                <td ><%=sptcdLotDocs.getFieldName2()%></td>
                                <td class="t-align-center" >
                                    <%=(Long.parseLong(sptcdLotDocs.getFieldName3())) / 1024%>
                                </td>
                                <td class="t-align-center" >
                                    <a title="Download" href="<%=request.getContextPath()%>/ServletCorriDocUpload?docName=<%=sptcdLotDocs.getFieldName1()%>&docSize=<%=sptcdLotDocs.getFieldName3()%>&tenderId=<%=tenderId%>&funName=download">
                                        Download
                                    </a>
                                </td>
                            </tr>
                            <%}%>
                        </table>
                        <%

                    } else { %>
<!--                    START: FOR PACAGE WISE DOCS-->
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="4%" class="t-align-center">Sl. No.</th>
                                <th class="t-align-center" width="23%">File Name</th>
                                <th class="t-align-center" width="32%">File Description</th>
                                <th class="t-align-center" width="7%">File Size <br />
                                    (in KB)</th>
                                <th class="t-align-center" width="18%">Action</th>
                            </tr>
                            <%
                             int PgkDocCnt=0;

                                for (SPTenderCommonData sptcdPkgDocs : tenderCommonService.returndata("CorrigendumInfo", sptcd.getFieldName1() , "docInfo")) {
                                // LOT WISE DOCS COMES HERE
                            PgkDocCnt++;
                            %>
                            <tr
                                <%if(Math.IEEEremainder(PgkDocCnt,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                        <%}%>
                                >
                                <td ><%=PgkDocCnt%></td>
                                <td ><%=sptcdPkgDocs.getFieldName1()%></td>
                                <td ><%=sptcdPkgDocs.getFieldName2()%></td>
                                <td >
                                    <%=(Long.parseLong(sptcdPkgDocs.getFieldName3()))/1024%>
                                </td>
                                <td >
<!--                                    <a href="#" title="Download">Download</a>-->
                                    <a title="Download" href="<%=request.getContextPath()%>/ServletCorriDocUpload?docName=<%=sptcdPkgDocs.getFieldName1()%>&docSize=<%=sptcdPkgDocs.getFieldName3()%>&tenderId=<%=tenderId%>&funName=download" title="Download">
                                    Download
                                    <!-- <img src="../resources/images/Dashboard/Download.png" alt="Download" />-->
                                </a>
                                </td>
                            </tr>
                            <%}%>
                        </table>
<!--                    END FOR PACAGE WISE DOCS-->
                    <%}%>

                <%}%>
                </div>

<!--               END NEW CODE-->
                <%}%>

            </div>
            </div>
            <br/>

        <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
