<%-- 
    Document   : ViewTER
    Created on : Jul 19, 2011, 11:52:43 AM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    String tenderid = request.getParameter("tenderid");
                    String lotId = request.getParameter("lotId");
                    String reptType = request.getParameter("type");
        %>
        <title>Tender Evaluation Report <%=reptType%></title>
    </head>
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <div class="tabPanelArea_1">
            <div class="pageHead_1">Tender Evaluation Report <%=reptType%></div>
            <%
                        ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                        EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                        CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        
                        int evalCount = evalService.getEvaluationNo(Integer.parseInt(tenderid));

                        //List<SPCommonSearchDataMore> TECMembers = creationService.getOpeningReportData("getTECMembers", tenderid, lotId, "TER" + reptType); //lotid ,"TOR1"
                         List<SPCommonSearchDataMore> TECMembers = dataMoreService.getCommonSearchData("getTECMembers", tenderid, lotId, "TER" + reptType,"",String.valueOf(evalCount)); //lotid ,"TOR1"
                        List<SPCommonSearchDataMore> tendOpeningRpt = creationService.getOpeningReportData("getTenderOpeningRpt", tenderid, lotId, null);//lotid
                        //List<Object[]> finalSubmissionUser = creationService.getFinalSubmissionUser(tenderid);
                        List<Object[]> finalSubmissionUserTer2 = creationService.getFinalSubmissionUserForTer2(tenderid,lotId,"0",String.valueOf(evalCount));
                        String pNature = commonService.getProcNature(request.getParameter("tenderid")).toString();
                        String stat = request.getParameter("stat");

                        boolean list1 = true;
                        if (tendOpeningRpt.isEmpty()) {
                            list1 = false;
                        }
                        String tenderPaidORFree = commonService.tenderPaidORFree(tenderid, lotId);
            %>
            <table class="tableList_3 t_space" cellspacing="0" width="100%">
                <%
                            int counter = 0;
                            for (SPCommonSearchDataMore TECMem : TECMembers) {
                                if (!TECMem.getFieldName2().equalsIgnoreCase("cp")) {
                                     List<Object[]> ter1Criteria = creationService.getTERCriteria("TER" + reptType, pNature);
                                     int evalSize = 0;
                %>
                <tr id="tr1_<%=counter%>">
                    <td><%=TECMem.getFieldName1()%></td>
                </tr>
                <tr id="tr2_<%=counter%>">
                    <td>
                        <%if(reptType.equals("2")){%>
                        <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>
                        <th width="35%">Name of Bidder/Consultant</th>
                        <% 
                         for(Object[] data : ter1Criteria){%>
                         <th><%=data[1]%></th>
                         <%}%>
                    </tr>
                    <%
                        List<Object[]> evalRepData = creationService.evalRepData(tenderid, lotId, stat, "TER2",TECMem.getFieldName5(),false,evalCount);
                       //0 tfs.userId,1 tcm.companyId,2 tcm.companyName,3 ttm.firstName,4 ttm.lastName
                           for(Object[] data : finalSubmissionUserTer2){
                           //String userBidder = null;
                           /*if(!data[1].toString().equals("1")){
                               userBidder = data[2].toString();
                           }else{
                                userBidder = data[3].toString()+" "+data[4].toString();
                           }*/
                    %>
                    <tr>
                        <td><%=data[2].toString()%></td>
                        <%
                            for(Object[] data2 : ter1Criteria){
                              if(!data2[1].toString().equalsIgnoreCase("Evaluation Status")){
                        %>
                        <td class="t-align-center">
                            <%
                                String evalValue=null;
                                //0. et.userId,1. et.criteriaId,2. et.value
                                evalSize = evalRepData.size();
                                for(Object[] objects : evalRepData){
                                    boolean tempbool = data[0].toString().equals(objects[0].toString()) && data2[0].toString().equals(objects[1].toString());
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("yes")){
                                        evalValue = "Yes";
                                    }
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("no")){
                                        evalValue = "No";
                                    }
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("na")){
                                        evalValue = "NA";
                                    }
                                }
                              
                                    out.print(evalValue);%>
                         </td>
                         <%
                             evalValue=null;
                              }}
                          %>
                          <td class="t-align-center">
                          <%
                                String evalStatus = creationService.getBidderStatus(data[0].toString(), tenderid, lotId,evalCount);
                                if("Technically Unresponsive".equalsIgnoreCase(evalStatus)){
                                    out.print("Technically Non-responsive");
                                }else{
                                    out.print(evalStatus);
                                }
                          %>
                          </td>
                    </tr>
                    <%
                           }
                    %>
                </tbody></table>
                        <% }else{%>
                        <table class="tableList_3" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <th width="50%">Criteria</th>
                                    <%
                                                                        for (Object[] data : finalSubmissionUserTer2) {
                                                                            String userBidder = null;
                                                                            if (!data[1].toString().equals("1")) {
                                                                                userBidder = data[2].toString();
                                                                            } else {
                                                                                userBidder = data[3].toString() + " " + data[4].toString();
                                                                            }
                                    %>
                                    <th>
                                        <%=userBidder%>
                                    </th>
                                    <%
                                                                        }
                                    %>
                                </tr>
                                <%
                                                                    List<Object[]> evalRepData = creationService.evalRepData(tenderid, lotId, stat, "TER" + reptType, TECMem.getFieldName5(), false,evalCount);
                                                                    //0. ter.criteriaId,1. ter.criteria
                                                                    for (Object[] data : ter1Criteria) {
                                                                        //if(data[1].toString().equals("Tender Validity") && (eventType.equals("PQ")||eventType.equals("1st stage of TSTM"))){
                                                                        if (data[1].toString().equals("Tender Validity") && (list1 && tendOpeningRpt.get(0).getFieldName20().equals("null"))) {
                                                                            continue;
                                                                        }
                                                                        if (data[1].toString().equals("Bid Security") && tenderPaidORFree.equals("free")) {
                                                                            continue;
                                                                        }
                                %>
                                <tr>
                                    <td class="table-description" ><%=data[1]%></td>
                                    <%for (Object[] data2 : finalSubmissionUserTer2) {%>
                                    <td class="t-align-center">
                                        <%
                                            String evalValue = null;
                                            //0. et.userId,1. et.criteriaId,2. et.value
                                            evalSize = evalRepData.size();
                                            for (Object[] objects : evalRepData) {
                                                boolean tempbool = data2[0].toString().equals(objects[0].toString()) && data[0].toString().equals(objects[1].toString());
                                                if (tempbool && objects[2].toString().equalsIgnoreCase("yes")) {
                                                    evalValue = "Yes";
                                                }
                                                if (tempbool && objects[2].toString().equalsIgnoreCase("no")) {
                                                    evalValue = "No";
                                                }
                                                if (tempbool && objects[2].toString().equalsIgnoreCase("na")) {
                                                    evalValue = "NA";
                                                }
                                            }


                                            out.print(evalValue);
                                        %>
                                    </td>
                                    <%
                                            evalValue = null;
                                        }
                                    %>
                                </tr>
                                <%}%>
                            </tbody></table>
                            <% } %>
                    </td>
                </tr>
                <% if (evalSize == 0) {%>
                <script type="text/javascript">
                    $('#tr1_<%=counter%>').hide();
                    $('#tr2_<%=counter%>').hide();
                </script>
                <% }%>
                <%   counter++;
                                }
                            }%>
            </table>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>
