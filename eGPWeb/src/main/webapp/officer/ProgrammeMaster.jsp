<%-- 
    Document   : ProgrammeMaster
    Created on : Oct 28, 2010, 2:59:59 PM
    Author     : Naresh.Akurathi
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="progmasterDtBean" class="com.cptu.egp.eps.web.databean.ProgrammeMasterDtBean"/>
<jsp:useBean id="progmasterSrBean" class="com.cptu.egp.eps.web.servicebean.ProgrammeMasterSrBean"/>
<jsp:setProperty property="*" name="progmasterDtBean"/>

<%@page import="com.cptu.egp.eps.model.table.TblProgrammeMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ProgrammeMasterSrBean"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Programme Master</title>
        <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>


        <script type="text/javascript">

        $(function() {

            $('#txtCode').blur(function() {
                $.post("<%=request.getContextPath()%>/CommonServlet", {pCode:$('#txtCode').val(),funName:'checkpCode'},  function(j){
                  if(j.toString().indexOf("e-GP", 0)!=-1){
                        $('span.#mymsg1').css("color","green");
                        $('#progForm').submit(function(){
                            return true;
                       });

                    }
                    else if(j.toString().indexOf("Programme", 0)!=-1){
                        $('span.#mymsg1').css("color","red");
                        $('#progForm').submit(function(){
                            return false;
                        });

                    }
                    $('span.#mymsg1').html(j);
                });
            });

        });
        </script>

        <script type="text/javascript">
        $(function() {

            $('#txtName').blur(function() {
                $.post("<%=request.getContextPath()%>/CommonServlet", {pName:$('#txtName').val(),funName:'checkpName'},  function(j){

                    if(j.toString().indexOf("e-GP", 0)!=-1){
                        $('span.#mymsg2').css("color","green");
                        $('#progForm').submit(function(){
                            return true;
                        });

                    }
                    else if(j.toString().indexOf("Programme", 0)!=-1){$('span.#mymsg2').css("color","red");
                        $('#progForm').submit(function(){
                            return false;
                        });

                    }
                    $('span.#mymsg2').html(j);
                });
            });
        });
        </script>




    </head>
    <body>
        <%!

        String forCode = "";
        String forName = "";
        int forId = 0;

        String valueId = "";
        boolean code;
        boolean name;

        TblProgrammeMaster tpm = new TblProgrammeMaster();
        ProgrammeMasterSrBean prgMasterSrBean = new ProgrammeMasterSrBean();

        %>

        <%
        String msg = "";
        //valueId = request.getParameter("id");
            if("Submit".equals(request.getParameter("button"))){//Runs when user click on Submit button
                    valueId = request.getParameter("id");
                    int id = Integer.parseInt(valueId);


                    if(id != 0){ //runs for update operation
                            progmasterSrBean.updateProgrammeMaster(progmasterDtBean,id);
                            msg="Data was Updated.";
                        }else{ //runs for adding operation
                            msg = progmasterSrBean.addProgrammeMaster(progmasterDtBean);

                        }

                }else{
                valueId = request.getParameter("id");
                if(valueId == null)
                 {}
                else{ //runs for diaplay & editing the data
                        int id=Integer.parseInt(valueId);
                        Iterator it = prgMasterSrBean.getData(id).iterator();

                        while(it.hasNext())
                            {
                                tpm = (TblProgrammeMaster)it.next();

                                forCode = tpm.getProgCode();
                                forName = tpm.getProgName();
                                forId = tpm.getProgId();

                            }
                      }
                }


        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="/resources/common/Top.jsp"%>
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Programme Master</div>
            <form id="progForm" method="post" action="ProgrammeMaster.jsp">
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                    <tr>
                        <td>&nbsp;</td>
                        <td><font color="maroon"><b><%= msg%></b></font></td>
                    </tr>
                    <tr>
                        <td class="ff">Programme Code : <span>*</span></td>
                        <td><input name="progCode" id="txtCode" type="text" class="formTxtBox_1" value="<%=forCode%>" style="width:200px;" />
                        <span id="mymsg1" style="color: red;">&nbsp;</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Programme Name : <span>*</span></td>
                        <td><input name="progName" id="txtName" type="text" class="formTxtBox_1" value="<%=forName%>" style="width:200px;" />
                        <span id="mymsg2" style="color: red;">&nbsp;</span>
                        </td>
                    </tr>

                    <tr>
                        <td class="ff">&nbsp;</td>
                        <td><input name="id"  type="hidden" value="<%= forId%>" />
                        </td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td><label class="formBtn_1">
                                <input type="submit" name="button" id="button" value="Submit" />
                            </label>
                        </td>

                        <td colspan="3"><a href="#"><img src="resources/images/Dashboard/viewIcn.png" alt="Go Back" class="linkIcon_1" /></a></td>
                    </tr>
                </table>
            </form>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@include file="/resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
            <script>
                var obj = document.getElementById('lblProgInfoCreate');
                if(obj != null){
                    if(obj.innerHTML == 'Create'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabContent");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>


