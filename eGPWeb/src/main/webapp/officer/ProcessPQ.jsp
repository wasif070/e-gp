<%--
    Document   : ProcessPQ
    Created on : Jun 22, 2011, 12:26:49 PM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblPostQualification"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TblPostQualificationService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Initiate Post Qualification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%          String tenderId;
                    tenderId = request.getParameter("tenderId");
                    String pkgLotId = "0";
                    List<Object[]>  list = new ArrayList<Object[]>();
                    if(request.getParameter("pkgLotId") != null && !"".equals(request.getParameter("pkgLotId"))){
                        pkgLotId = request.getParameter("pkgLotId");
                    }
                    int countInitiate = 0;
                    String lotPkgNoName = "";
                    if(pkgLotId.equals("0")){
                        lotPkgNoName = "Package ";
                    }else{
                        lotPkgNoName = "Lot ";
                    }
                    String s_queryString  = "";
                    EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                    CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");

                    int evalCount = evalService.getEvaluationNo(Integer.parseInt(tenderId));
                    if("0".equalsIgnoreCase(pkgLotId)){
                        list = cs.getPkgDetialByTenderId(Integer.parseInt(tenderId));
                        s_queryString = "&pkgLotId=0";
                    }else{
                    list = cs.getLotDetailsByTenderIdAndPkgIdforEval(tenderId, pkgLotId);
                    s_queryString = "&pkgLotId="+list.get(0)[2];
                    }
                    List<Object[]> listcompany = cs.getCompanyNameAndEntryDate(tenderId,pkgLotId,evalCount);
                    TblPostQualificationService service = (TblPostQualificationService) AppContext.getSpringBean("PostQualificationService");
                    //List<TblPostQualification> list1 = service.getAll(pkgLotId);
                    //System.out.println("list------------"+list.size());


        %>
        <script>
            function checkBidder(){


                $.ajax({
                    type: "POST",
                    url: "<%=request.getContextPath()%>/CommMemServlet",
                    data:"funName=checkBidderexist&tenderId="+$('#tenderId').val()+"&"+"pkgId="+$('#pkgId').val(),
                    async: false,
                    success: function(j){
                        if(j.toString()=="0"){

                            jAlert("Price Comparison Report is not yet saved","Post Qualification", function(RetVal) {
                            });
                        }
                        else{
                            document.bidder.method="post";
                            document.bidder.action="PostQualification.jsp?tenderId=<%=tenderId%><%=s_queryString%>";
                            document.bidder.submit();

                        }
                    }
                });

            }
        </script>
    </head>
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Initiate Post Qualification
                    <span class="c-alignment-right"><a href="InitiatePQ.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go back</a></span>
                </div>
                <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <div>&nbsp;</div>
                <div class="tabPanelArea_1">
                    <%if ("initiated".equalsIgnoreCase(request.getParameter("msgId"))) {%>
                    <div class="responseMsg successMsg">PQ Process initiated successfully</div>
                    <%} else if ("closed".equalsIgnoreCase(request.getParameter("msgId"))) {%>
                    <div class="responseMsg successMsg">PQ Process Completed successfully</div>
                    <%} else if ("edit".equalsIgnoreCase(request.getParameter("msgId"))) {%>
                    <div class="responseMsg successMsg">Details edited successfully</div>
                    <%}else if ("closedcrdel".equalsIgnoreCase(request.getParameter("msgId"))) {%>
                    <div class="responseMsg successMsg">Please save Price Comparison Report again to proceed further</div>
                    <%}%>
                    <div>&nbsp;</div>
                    <form name="bidder" method="post">
                        <table width="100%" cellspacing="0" class="tableList_1">

                            <tr>
                                <td  class="ff" widtd="10%" valign="middle" class="t-align-center"><%=lotPkgNoName%>No.</td>
                                <td class="t-align-left"><%=list.get(0)[0]%></td>
                            </tr><tr>
                                <td class="ff" width="20%" valign="middle" class="t-align-center"><%=lotPkgNoName%>Description</td>
                                <td class="t-align-left"><%=list.get(0)[1]%></td>
                            </tr><tr>
                                <td  class="ff" width="15%" valign="middle" class="t-align-center">Post Qualification</td>
                                <%
                                            boolean flag = false;
                                            int initiate = 0;
                                            int qualify = 0;
                                            int disqualify = 0;
                                            int noa = 0;
                                            if (!listcompany.isEmpty() && listcompany != null) {

                                                for (int j = 0; j < listcompany.size(); j++) {
                                                    if ("initiated".equalsIgnoreCase(listcompany.get(j)[4].toString())) {
                                                        initiate++;
                                                    } else if ("Disqualify".equalsIgnoreCase(listcompany.get(j)[4].toString())) {
                                                        disqualify++;
                                                    } else if ("qualify".equalsIgnoreCase(listcompany.get(j)[4].toString())) {
                                                        qualify++;
                                                        if ("Performance Security not paid".equalsIgnoreCase(listcompany.get(j)[6].toString()) || "rejected".equalsIgnoreCase(listcompany.get(j)[6].toString())) {
                                                        noa++;
                                                        }
                                                    }
                                                }
                                                if (qualify > 0 && initiate==0) {
                                                    if (noa > 0 ) {
                                %>
                                <td id="tdInitiateLink" class="t-align-left"><a id="initiateLink" href="#" onclick="checkBidder()" >Initiate</a></td>
                                <%} else {%>
                                <td class="t-align-left">Bidder/Consultant Qualified</td>
                                <%}
                                            }else if (initiate > 0) {%>
                                <td class="t-align-left">Initiated</td>
                                <%} else {%>
                                <td class="t-align-left" id="tdInitiateLink1"><a id="initiateLink1" href="#" onclick="checkBidder()" >Initiate</a></td>
                                <%}
                                                                            } else {%>
                                <td class="t-align-left" id="tdInitiateLink2"><a href="#" id="initiateLink2" onclick="checkBidder()">Initiate</a></td>
                                <% }
                                %>



                            </tr>
                        </table>
                    </form>
                    <br />
                    <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>">
                    <input type="hidden" name="pkgId" id="pkgId" value="<%=pkgLotId%>">
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th width="10%" valign="middle" class="t-align-center">Rank</th>
                            <th width="20%" valign="middle" class="t-align-center">Bidder/Consultant</th>
                            <th width="20%" valign="middle" class="t-align-center">Post Qualification Status</th>
                            <th width="12%" valign="middle" class="t-align-center">Letter of Acceptance (LOA) Acceptance Status</th>
                            <th width="12%" valign="middle" class="t-align-center">Date and Time of Post Qualification</th>
                            <!-- Dohatec Start : 19.Apr.15 : Istiak -->
                            <th width="10%" valign="middle" class="t-align-center">Previous Winning Bidder History</th>
                            <!-- Dohatec End -->
                            <th width="25%" valign="middle" class="t-align-center">Action</th>
                        </tr>
                        <%
                                    String userId = request.getParameter("userId");
                                    if (userId == null) {
                                        userId = "";
                                    }

                                    if (listcompany != null && !listcompany.isEmpty()) {
                                        int i = 0;
                                        for (Object[] obj : listcompany) {
                                            String strTenderer_Consultnat = obj[0].toString();
                                            if(obj[7].toString().equals("1")){
                                                strTenderer_Consultnat = obj[8].toString() +" "+obj[9].toString();
                                            }
                        %>

                        <tr>
                            <td class="t-align-center">L<%=obj[5].toString()%></td>
                            <td class="t-align-center"><%=strTenderer_Consultnat%></td>
                            <td class="t-align-center"> <%if ("initiated".equalsIgnoreCase(obj[4].toString())) {%>
                                Pending
                                <%} else {%>
                                <%
                                    if ("Qualify".equalsIgnoreCase(obj[4].toString()) ) {
                                        out.print("Qualified");
                                    } else {
                                        out.print("Disqualified");
                                    }
                                %>
                                <%}%>
                            </td>
                            <td class="t-align-center">
                                <%
                                if ("DisQualify".equalsIgnoreCase(obj[4].toString()) ) {%>
                                --
                                <%
                                }else{
                                    if("pending".equalsIgnoreCase(obj[6].toString())){
                                        countInitiate++;
                                    }
                                    if ("rejected".equalsIgnoreCase(obj[6].toString()) ) {%>
                                        Declined
                                    <% }else if ("pending".equalsIgnoreCase(obj[6].toString()) ) {%>
                                    Pending
                                    <%}else{%>
                                        <%=obj[6].toString()%>
                                <%} }%>
                            </td>
                            <td class="t-align-center"><%if(obj[1]!=null){
                                                                        String str[] = obj[1].toString().split("-");
                                                                        if ("1900".equalsIgnoreCase(str[0])) {
                                                                            out.print("-");
                                                                        } else {
                                                                            out.print(DateUtils.gridDateToStr((Date) obj[1]));
                                                                        }
                                                                        }%></td>
                            <!-- Dohatec Start : 19.Apr.15 : Istiak -->
                            <td class="t-align-center">
                                <a href="WinningBidderHistory.jsp?tenderId=<%=tenderId%>&pkgLotId=<%=pkgLotId%>&uId=<%=obj[3].toString()%>">View</a>
                            </td>
                            <!-- Dohatec End -->
                            <td class="t-align-center">
                                <%   if ("initiated".equalsIgnoreCase(obj[4].toString())) {%>
                                <a href="EditPostQualification.jsp?pkgLotId=<%=pkgLotId%>&postQualId=<%=obj[2].toString()%>&uId=<%=obj[3].toString()%>&tenderId=<%=tenderId%>">Edit</a>
                                &nbsp;|&nbsp;<a href="ViewPostQualificationDtl.jsp?uId=<%=obj[3].toString()%><%=s_queryString%>&tenderId=<%=tenderId%>&postQualId=<%=obj[2].toString() %>">View</a>&nbsp;|&nbsp;
                                <%
                                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                String role = "",sentBy = "";
                                if (session.getAttribute("userId") != null) {
                                    sentBy = session.getAttribute("userId").toString();
                                }
                                CommonSearchService commonSearchService1 = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                List<SPCommonSearchData> lstRole =
                                commonSearchService1.searchData("EvalMemberRole", tenderId, sentBy, null, null, null, null, null, null, null);
                                if (!lstRole.isEmpty()) {
                                    role = lstRole.get(0).getFieldName2();
                                }
                                List<SPCommonSearchDataMore> chkDateTime = commonSearchDataMoreService.geteGPData("chkSiteVisitDateNTime",tenderId);
                                if("cp".equalsIgnoreCase(role)&& (!chkDateTime.isEmpty()) && "no".equalsIgnoreCase(chkDateTime.get(0).getFieldName2())){
                                %>
                                <a href="PostQualEstDoc.jsp?postQualId=<%=obj[2].toString()%>&tenderId=<%=tenderId%>">Upload Report</a>
                                &nbsp;|&nbsp;
                                <% }else if("cp".equalsIgnoreCase(role) && (!chkDateTime.isEmpty()) && "yes".equalsIgnoreCase(chkDateTime.get(0).getFieldName1())){
                                %>
                                <a href="PostQualEstDoc.jsp?postQualId=<%=obj[2].toString()%>&tenderId=<%=tenderId%>">Upload Report</a>
                                &nbsp;|&nbsp;
                                <% } %>
                                <a href="PQStatus.jsp?postQualId=<%=obj[2].toString()%>&uId=<%=obj[3].toString()%>&tenderId=<%=tenderId%>&pkgId=<%=pkgLotId%>">Complete</a>
                            </td>
                        </tr>
                        <% } else {

                        %>
                        <a href="ViewPostQualificationDtl.jsp?uId=<%=obj[3].toString()%><%=s_queryString%>&tenderId=<%=tenderId%>&postQualId=<%=obj[2].toString() %>">View</a>
                        <% }
                                                                    i++;
                                                                }
                                                            } else {%>
                        <td class="t-align-center" colspan="7">No records found</td>
                        <%}

                        %>
                    </table>

                    <div>&nbsp;</div>
                </div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        var countInitiate = '<%=countInitiate%>'
        if(countInitiate != '0'){
            if($('#initiateLink').html() != null){
                $('#tdInitiateLink').html("Initiated");
            }else if($('#initiateLink1').html() != null){
                $('#tdInitiateLink1').html("Initiated");
            }else if($('#initiateLink2').html() != null){
                $('#tdInitiateLink2').html("Initiated");
            }
        }
    </script>
</html>
