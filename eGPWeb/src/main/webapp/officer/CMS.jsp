<%-- 
    Document   : CMS
    Created on : Jul 25, 2011, 12:46:45 PM
    Author     : shreyansh
--%>

<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CMS</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script>
            function Edit(wpId,tenderId,lotId){
                dynamicFromSubmit("EditDatesForBoq.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms");
            }
            function view(wpId,tenderId,lotId){
                dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
            }
        </script>
    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">CMS</div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "14");

            %>
            <%@include  file="officerTabPanel.jsp"%>
           <div class="tabPanelArea_1">
            
           <%
                        pageContext.setAttribute("TSCtab", "1");

            %>
                    <%@include  file="../resources/common/CMSTab.jsp"%>
                    <div class="tabPanelArea_1">
                        <div align="center">
                    
                    <%
                    ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
                                String tenderId = request.getParameter("tenderId");
                                boolean flag = true;
                                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                int i = 0;
                                for (SPCommonSearchDataMore lotList : packageLotList) {
                                    List<Object> wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                    boolean chkContract = service.checkContractSigning(tenderId,Integer.parseInt(lotList.getFieldName5()));
                                     boolean flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));
                    %>
                    <form name="frmcons" method="post">
                      
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                            <tr>
                                <td width="20%">Lot No.</td>
                                <td width="80%"><%=lotList.getFieldName3()%></td>
                            </tr>
                            <tr>
                                <td>Lot Description</td>
                                <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                            </tr>
                            
                            <%
                                                                                int count = 1;
                                                                                  if(chkContract){
                                                                                if (!wpid.isEmpty() && wpid != null) {%>
                            <tr><td colspan="2">
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th width="2%" class="t-align-center">S.No</th>
                                            <th width="17%" class="t-align-center">Consolidate</th>
                                            <th width="5%" class="t-align-center">Action</th>
                                        </tr>
                                        <%for (Object obj : wpid) {
                                                                                    String toDisplay = service.getConsolidate(obj.toString());

                                        %>


                                        <tr>
                                            <td style="text-align: center;"><%=count%></td>
                                            <td><%=toDisplay%></td>
                                            <td><a href="#" onclick="view(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View</a>

                                              <%if(flags){%>
                                                &nbsp;|&nbsp;<a href="#" onclick="Edit(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%> );">Edit Dates</a>
                                                <%} else {%>
                                                    &nbsp;|&nbsp; <a href="#" onclick="jAlert('<%=bdl.getString("CMS.DateConfigPending")%>','Delivery Schedule', function(RetVal) {
                                            });">Edit Dates</a>
                                                    <%}%>
                                            </td>



                                        </tr>


                                        <%
                                                               count++;
                                                           }%>
                                       
                                    </table>
                                </td></tr>
                                <%
                                                                                }
                                    }else{
                                   flag = false;

                                                    }%>
        
<%
}                                    %>

<%if(!flag){%>
<script>
      jAlert("Contract Signing is pending","Consolidate", function(RetVal) {
                                });

</script>
<%}%>



                        </table>
                    </form>

                    </div>
                    </div></div></div>
        </div>
                <script>
                var headSel_Obj = document.getElementById("headTabTender");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            </html>
