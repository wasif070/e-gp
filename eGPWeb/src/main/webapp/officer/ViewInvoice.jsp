<%--
    Document   : ViewInvoice
    Created on : Aug 9, 2011, 12:33:34 PM
    Author     : shreyansh
--%>

<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.math.RoundingMode"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceDetails"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceMaster"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvRemarks"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceDocument"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Invoice</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <%
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");
        %>
        <%
                    ConsolodateService cs = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");
                    String tenderId = request.getParameter("tenderId");
                    String lotId = "";
                    if(request.getParameter("lotId")!=null)
                    {
                        lotId = request.getParameter("lotId");
                    }
                    String wpId = "";
                    if(request.getParameter("wpId")!=null)
                    {
                        wpId = request.getParameter("wpId");
                    }
                    String userTypeIdd = "";
                    if (session.getAttribute("userTypeId") != null) {
                    userTypeIdd = session.getAttribute("userTypeId").toString();
                    }
                     TenderTablesSrBean beanCommon1 = new TenderTablesSrBean();
                     String tenderType1 = beanCommon1.getTenderType(Integer.parseInt(request.getParameter("tenderId")));
                     List<Object> objInvoiceId = null;
                     //String invoiceId = "";

                     int total = 0;
                     int c = 0;
                     String[] invoiceIds = new String[4];
                     BigDecimal[] grossAmount = new BigDecimal[4];
                     
                    if(!tenderType1.equals("ICT"))
                    {
                        //invoiceId = request.getParameter("invoiceId");
                        invoiceIds[0] = request.getParameter("invoiceId");
                        total = 1;
                        pageContext.setAttribute("invoiceId", request.getParameter("invoiceId"));
                    }
                    else if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                    {
                        pageContext.setAttribute("invoiceNo", request.getParameter("invoiceNo"));
                        objInvoiceId = cs.getInvoiceIdForICT(request.getParameter("invoiceNo"), Integer.parseInt(wpId));
                        // Dohatec Start
                        total = objInvoiceId.size();
                        for(c = 0; c < total; c++)
                        {
                            invoiceIds[c] = objInvoiceId.get(c).toString();
                        }
                        // Dohatec End
                        //invoiceId = objInvoiceId.get(0).toString();

                        //System.out.println("sss"+invoiceId);
                        pageContext.setAttribute("invoiceId", objInvoiceId.get(0).toString());
                    }

                    boolean flag = true;
                    CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");

                    String conType = commService.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
        %>
    <div class="dashboard_div">
        <%

            CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
            String procCase = commService.getProcNature(request.getParameter("tenderId")).toString();
            String strProcNature = "";
            if("Services".equalsIgnoreCase(procCase))
            {
                strProcNature = "Consultant";
            }else if("goods".equalsIgnoreCase(procCase)){
                strProcNature = "Supplier";
            }else{
                strProcNature = "Contractor";
            }
            /*double strVatPlusAit = 0;
            double strAdvVatPlusAdvAit = 0;
            double netVatandAitStr = 0;*/
           
            AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
            accPaymentService.setLogUserId(session.getAttribute("userId").toString());

            //List<TblCmsInvoiceAccDetails> invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(invoiceId));

            //  Dohatec Start
          /*  String[] curArr = new String[5];
            String[] totalArr = new String[5];
            String[] cntrAmnt = new String[5];
             double strVatPlusAit[] = new double[5];
            double strAdvVatPlusAdvAit[] = new double[5];
            double netVatandAitStr[] = new double[5];*/
            List<Object[]> totalAmt = null;
            List<TblCmsInvoiceAccDetails> invoicedetails = null;
            List<List<TblCmsInvoiceAccDetails>> invoicedetailsAll = new ArrayList<List<TblCmsInvoiceAccDetails>>();
            /*if(tenderType1.equals("ICT"))
            {*/
                for(c = 0; c < total; c++)
                {
                    invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(invoiceIds[c]));
                    //grossAmount[c] = invoicedetails.get(0).getGrossAmt();
                    invoicedetailsAll.add(c, invoicedetails);
                }
                //invoicedetails = invoicedetailsAll.get(0);
                
            //}
            /*else
            {
                invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(invoiceId));
            }*/
                boolean isEdit = false;
                if (!invoicedetailsAll.isEmpty()) {
                    isEdit = true;
                }

            // Dohatec End

            /*boolean isEdit = false;
            if (!invoicedetails.isEmpty()) {
                isEdit = true;
            }*/
            boolean HideGeneratedbyTenderer = true;
            boolean HideSendToTenderer = true;
            boolean HideRemarksbype = true;
         //   List<Object> invstatusObject = accPaymentService.getInvoiceStatus(Integer.parseInt(request.getParameter("invoiceId")));
            //List<Object> invstatusObject = accPaymentService.getInvoiceStatus(Integer.parseInt(invoiceId));
            List<Object> invstatusObject = accPaymentService.getInvoiceStatus(Integer.parseInt(invoiceIds[0]));

            if (!invstatusObject.isEmpty()) {
                if ("acceptedbype".equalsIgnoreCase(invstatusObject.get(0).toString()) || "createdbyten".equalsIgnoreCase(invstatusObject.get(0).toString()) || "rejected".equalsIgnoreCase(invstatusObject.get(0).toString())) {
                    HideGeneratedbyTenderer = false;
                }
                if ("sendtotenderer".equalsIgnoreCase(invstatusObject.get(0).toString())) {
                    HideSendToTenderer = false;
                }
                if ("remarksbype".equalsIgnoreCase(invstatusObject.get(0).toString())) {
                    HideRemarksbype = false;
                }
                int conIdd = cs.getContractId(Integer.parseInt(tenderId));
                MakeAuditTrailService makeauditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                if("2".equalsIgnoreCase(userTypeIdd))
                {
                    if (!"sendtope".equalsIgnoreCase(invstatusObject.get(0).toString()) || !"remarksbype".equalsIgnoreCase(invstatusObject.get(0).toString())) {
                        makeauditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conIdd, "contractId", EgpModule.Payment.getName(), "View Invoice", "");
                    }
                }else{
                    if ("acceptedbype".equalsIgnoreCase(invstatusObject.get(0).toString()) || "rejected".equalsIgnoreCase(invstatusObject.get(0).toString()) || "remarksbype".equalsIgnoreCase(invstatusObject.get(0).toString()) || "sendtotenderer".equalsIgnoreCase(invstatusObject.get(0).toString())) {
                        makeauditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getHeader("referer")), conIdd, "contractId", EgpModule.Payment.getName(), "View Invoice", "");
                    }
                }
            }
        %>
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div  id="print_area">
        <div class="contentArea_1">
            <div class="pageHead_1"><%=bdl.getString("CMS.Inv.View.Title")%>
                <span class="c-alignment-right noprint">
                    <%
                        if(!HideRemarksbype || !HideSendToTenderer){
                    %>
                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                    <%}%>
                    <%
                    if("services".equalsIgnoreCase(procCase)){%>
                    <a class="action-button-goback" href="InvoiceServiceCase.jsp?tenderId=<%=request.getParameter("tenderId")%>" title="Go Back">Go Back</a>
                    <%}else{%>
                    <%if(!"fin".equalsIgnoreCase(request.getParameter("flag"))){%>
                    <a href="Invoice.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                    <%}else{%>
                    <a href="ViewTotalInvoice.jsp?tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=request.getParameter("lotId")%>" class="action-button-goback">Go Back</a>
                    <%}}%>
                </span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
             <%

                             pageContext.setAttribute("TSCtab", "4");


                            pageContext.setAttribute("lotId", request.getParameter("lotId"));
                            pageContext.setAttribute("wpId", request.getParameter("wpId"));


                %>
            <div class="t_space">
                <%@include file="../resources/common/ContractInfoBar.jsp"%>
                </div>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "14");

                        //  Dohatec Start
                         List<TblCmsInvoiceMaster> InvMasdata =null;
                            InvMasdata = cs.getTblCmsInvoiceMaster(Integer.parseInt(invoiceIds[0]));

                        if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                            //totalAmt = cs.getInvoiceTotalAmtForICT(Integer.parseInt(invoiceIds[0]),Integer.parseInt(request.getParameter("wpId")));
                            totalAmt = cs.returndata("getInvoiceTotalAmtForICT", invoiceIds[0], request.getParameter("wpId"));
                        else
                            if("no".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                               totalAmt =  cs.getInvoiceTotalAmt(invoiceIds[0]);
                           else
                               totalAmt =  cs. getInvoiceTotalAmtAdv(invoiceIds[0]);

                        String[] curArr = new String[totalAmt.size()];
                        String[] totalArr = new String[totalAmt.size()];
                        String[] cntrAmnt = new String[totalAmt.size()];
                        double strVatPlusAit[] = new double[totalAmt.size()];
                        double strAdvVatPlusAdvAit[] = new double[totalAmt.size()];
                        double netVatandAitStr[] = new double[totalAmt.size()];
                        
                        String totalAmount = "";
                        System.out.print("totalAmt"+totalAmt.size());
                        c = 0;
                        for(Object[] oob : totalAmt)
                        {
                            if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                curArr[c] = oob[1].toString();
                                totalArr[c] = oob[0].toString();
                                cntrAmnt[c] =  oob[3].toString();
                            }else{
                                curArr[c] = "BTN";
                                cntrAmnt[c] =  c_obj[2].toString();
                            }
                            c++;
                        }
                        //  Dohatec End
            %>
            <%if(!"fin".equalsIgnoreCase(request.getParameter("flag"))){%>
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1">
            <%}%>

                    <div align="center">


                        <div class="tabPanelArea_1">

                            <div align="center">
                                <%
                                            String message = "";
                                            if (request.getParameter("msg") != null) {
                                                message = request.getParameter("msg");
                                            }
                                            if ("Remarkssucc".equalsIgnoreCase(message)) {
                                %>
                                <div class="t_space"><div class="responseMsg successMsg t_space" align="left"><span><%=bdl.getString("CMS.Inv.RemarksbyPE.SuccMSg")%></span></div></div>
                                <%} else if ("Remarksfail".equalsIgnoreCase(message)) {%>
                                <div class="t_space"><div class="responseMsg errorMsg t_space" align="left"><span><%=bdl.getString("CMS.Inv.RemarksbyPE.FailMSg")%></span></div></div>
                                <%}else if("accept".equalsIgnoreCase(message)){%>
                                <div class="t_space"><div class="responseMsg successMsg t_space" align="left"><span>Invoice accepted successfully</span></div></div>
                                <%}else if("reject".equalsIgnoreCase(message)){%>
                                <div class="t_space"><div class="responseMsg successMsg t_space" align="left"><span>Invoice rejected successfully</span></div></div>
                                <%}%>
                                <form action='<%=request.getContextPath()%>/AccPaymentServlet' method="post" name="AccPaymentfrm">
                                    <%
                                      // List<TblCmsInvoiceMaster> InvMasdata =null;
                                       List<Object>  InvMasdataObj = null;
                                       cs.setLogUserId(session.getAttribute("userId").toString());
                                       NOAServiceImpl noaServiceImpl = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
                                       List<Object[]> noalist = null;
                                            if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                                noalist = noaServiceImpl.getDetailsNOAforInvoiceICT(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                                            else
                                                noalist = noaServiceImpl.getDetailsNOAforInvoice(Integer.parseInt(tenderId),Integer.parseInt(lotId));


                                       Object[] noaObj = null;
                                       if(noalist!=null && !noalist.isEmpty())
                                       {noaObj = noalist.get(0);}
                                       //InvMasdata = cs.getTblCmsInvoiceMaster(Integer.parseInt(invoiceId));
                                    //   InvMasdata = cs.getTblCmsInvoiceMaster(Integer.parseInt(invoiceIds[0]));

                                       if(!"services".equalsIgnoreCase(procCase)){
                                       if(!InvMasdata.isEmpty())
                                       {
                                           if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                           {
                                    %>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                         <%
                                      int incr = 0;
                                      if(noalist!=null && !noalist.isEmpty())
                                       for(Object[] noaObjj:noalist){
                                        %>
                                         <tr>
                                    <td class="ff" width="60%">Advance Amount</td>

                                    <%
                                        if(noaObjj[12]!=null && !"0.000".equalsIgnoreCase(noaObjj[12].toString())){
                                            out.print("<td width='20%'>"+noaObjj[12].toString()+" (In %)</td>");
                                        }
                                        BigDecimal big = BigDecimal.ZERO;
                                        if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                            big = new BigDecimal(noaObjj[13].toString()).multiply(new BigDecimal(noaObjj[12].toString()));
                                        %>
                                    <td width="20%"><%=big.divide(new BigDecimal(100),3,RoundingMode.HALF_UP).setScale(3,RoundingMode.HALF_UP)%> (In <%=noaObjj[14].toString()%>)
                                        <% } else { %>
                                     <td width="12%"><%=InvMasdata.get(0).getTotalInvAmt()%><br/><%=bdl.getString("CMS.Inv.InBDT")%></td>
                                        <% } %>
                                </tr> <% incr ++ ;} %>
                                    </table>
                                    <%}else{%>
                                    <%@include  file="../resources/common/CommonViewInvoice.jsp"%>
                                    <%}}}else{
                                    if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                       {
                                    %>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <td class="ff" width="88%">Advance Amount</td>
                                                <%
                                                    if(noaObj[12]!=null && !"0.000".equalsIgnoreCase(noaObj[12].toString())){
                                                        out.print("<td width='5%'>"+noaObj[12].toString()+"<br/>(In %)</td>");
                                                    }
                                                %>
                                                <td width="12%"><%=InvMasdata.get(0).getTotalInvAmt()%><br/><%=bdl.getString("CMS.Inv.InBDT")%></td>
                                            </tr>
                                        </table>
                                     <%}else{
                                   if("time based".equalsIgnoreCase(conType)){
                             %>
                                            <%
                                                List<Object[]> AttSheetslist = cmss.ViewAttSheetDtls(InvMasdata.get(0).getTillPrid());
                                                String weeks_days[] = new String[7];
                                                MonthName monthName = new MonthName();
                                                BigDecimal TotalAmount = new BigDecimal(0);
                                                if(AttSheetslist!=null && !AttSheetslist.isEmpty()){
                                            %>
                                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                                <th width="3%" class="t-align-left">Sr.No.</th>
                                                <th width="20%" class="t-align-left">Name of Employees</th>
                                                <th width="15%" class="t-align-left">Position Assigned</th>
                                                <th width="9%" class="t-align-left">Home / Field</th>
                                                <th width="5%" class="t-align-left">No. of Days</th>
                                                <th width="5%" class="t-align-left">Rate</th>
                                                <th width="18%" class="t-align-left">Month</th>
                                                <th width="18%" class="t-align-left">Year</th>
                                                <th width="18%" class="t-align-left">Invoice Amount</th>
                                            </tr>
                                            <%
                                                for(Object obj[] : AttSheetslist){
                                                TotalAmount = TotalAmount.add((BigDecimal)obj[10]) ;
                                            %>
                                            <tr>
                                                <td width="3%" class="t-align-left"><%=obj[0]%></td>
                                                <td width="20%" class="t-align-left"><%=obj[1]%></td>
                                                <td width="15%" class="t-align-left"><%=obj[2]%></td>
                                                <td width="9%" class="t-align-left"><%=obj[7]%></td>
                                                <td width="5%" style="text-align: right"><%=obj[3]%></td>
                                                <td width="5%" style="text-align: right"><%=obj[4]%></td>
                                                <td width="5%" class="t-align-left"><%=monthName.getMonth((Integer) obj[5]) %>
                                                <td width="5%" style="text-align: right"><%=obj[6]%></td>
                                                <td width="5%" style="text-align: right"><%=obj[10]%></td>
                                           </tr>
                                               <%}%>
                                               <tr><td class="t-align-left" colspan=7></td>
                                               <td class="t-align-left ff">Total Amount (In Nu.)</td>
                                               <td class="t-align-right ff" style="text-align: right;"><%=TotalAmount%></td>
                                               </tr>
                                            </table>
                                                <%}%>

                              <%@include  file="../resources/common/CommonViewInvoice.jsp"%>
                              <%}else{%>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                        <th>S No</th>
                                        <th>Milestone Name </th>
                                        <th>Description</th>
                                        <th>Payment as % of Contract Value</th>
                                        <th>Mile Stone Date proposed by PE </th>
                                        <th>Mile Stone Date proposed by Consultant</th>
                                        <th>Invoice Amount <br/><%=bdl.getString("CMS.Inv.InBDT")%></th>
                                        <%

                                            List<TblCmsSrvPaymentSch> listPaymentData = cmss.getPaymentScheduleDatabySrvPSId(InvMasdata.get(0).getTillPrid());
                                            if(!listPaymentData.isEmpty()){
                                        %>
                                        <tr>
                                            <td class="t-align-left"><%=listPaymentData.get(0).getSrNo()%></td>
                                            <td class="t-align-left"><%=listPaymentData.get(0).getMilestone()%></td>
                                            <td class="t-align-left"><%=listPaymentData.get(0).getDescription()%></td>
                                            <td class="t-align-left"><%=listPaymentData.get(0).getPercentOfCtrVal()%></td>
                                            <td class="t-align-left"><%=DateUtils.customDateFormate(listPaymentData.get(0).getPeenddate())%></td>
                                            <td class="t-align-left"><%=DateUtils.customDateFormate(listPaymentData.get(0).getEndDate())%></td>
                                            <td><%=new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*listPaymentData.get(0).getPercentOfCtrVal().doubleValue())/100).setScale(3,0)%>
                                        </tr>
                                    </table>
                                    <%}}}
                                       if(!InvMasdata.isEmpty())
                                       {
                                           if(InvMasdata.get(0).getConRemarks()!=null)
                                           {
                                               if("No".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                               {
                                    %>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                        <tr><td width="20%" class="ff">Remarks by Consultant</td>
                                            <td>
                                                <%
                                                    out.print(InvMasdata.get(0).getConRemarks());
                                                %>
                                            </td>
                                        </tr>
                                    </table>
                                    <%}}}}%>
                                    <input type="hidden" name="action" id="action" value="saveRemarks" />
                                    <input type="hidden" name="status" id="status" value="" />
                                     <%

                                    if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                    %>
                                        <input type="hidden" name="InvoiceId" id="InvoiceId" value="<%=request.getParameter("invoiceNo")%>" />
                                    <%}else{ %>
                                        <input type="hidden" name="InvoiceId" id="InvoiceId" value="<%=request.getParameter("invoiceId")%>" />
                                    <% } %>
                                    <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>" />
                                    <input type="hidden" name="lotId" id="tenderId" value="<%=lotId%>" />
                                    <input type="hidden" name="wpId" id="wpId" value="<%=wpId%>" />
                                    <%
                                        if (HideGeneratedbyTenderer) {
                                    %>

                                    <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                            <!-- Dohatec Start -->
                                            <%//if(tenderType1.equals("ICT")){
                                                if(isEdit){
                                                    for(c = 0; c < total; c++)
                                                    {
                                            %>
                                                <tr>
                                                    <td class="ff" width="25%" class="t-align-left">Invoice Amount (In <%=curArr[c].toString()%>) : </td>
                                                    <td class="t-align-left"><%=invoicedetailsAll.get(c).get(0).getInvoiceAmt()%>
                                                        <input type="hidden" id="invoiceAmount_<%=c%>" name ="invoiceAmount" <%if(isEdit) {%>value ="<%=invoicedetailsAll.get(c).get(0).getInvoiceAmt()%>"<%}%>>
                                                        &nbsp;<span id="invoiceAmountWrd_<%=c%>" class="ff"></span>
                                                    </td>
                                                </tr>
                                                <!-- Dohatec End -->
                                            <%}}//}else{%>
                                                <!--<tr>
                                                    <td class="ff" width="25%" class="t-align-left"><%=bdl.getString("CMS.Inv.invamount")%></td>
                                                    <td class="t-align-left"> <%if (isEdit) {%><%=invoicedetailsAll.get(0).get(0).getInvoiceAmt()%><%}%></td>
                                                </tr>-->
                                            <%//}%>
                                    </table>
                                    <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.deduction")%></div>
                                    <table cellspacing="0" class="tableList_1" width="100%">
                                        <tr><td class="ff" width="25%">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td class="ff" width="25%">Total Advance Adjustment till Date :</td>
                                            <td>
                                                <%
                                                int ti = 0;
                                                    List<Object[]> objCurBefore =  Collections.EMPTY_LIST;
                                                    List<Object[]> objAdjustmentBefore = null;
                                                    Object[] obj = null;
                                                   BigDecimal salvageAdjustmentTillDate=BigDecimal.ZERO.setScale(3,0);
                                                   // String curForAdjustment[] =
                                                    String[] curForAdjustment = new String[4];
                                                    String[] amountForAdjustment = new String[4];
                                                    List<Object[]> totalRDeducted = accPaymentService.getAllTotalInvoiceAmtForWp(Integer.parseInt(wpId));
                                                    double inttotalRDeducted = 0.0;
                                                    BigDecimal bigadvadjAmt = BigDecimal.ZERO;
                                                    if(!totalRDeducted.isEmpty())
                                                    {
                                                        obj = totalRDeducted.get(0);
                                                        if(obj[1]==null)
                                                        {
                                                            inttotalRDeducted = new BigDecimal(0).setScale(3,0).doubleValue();
                                                        }
                                                        else{
                                                            inttotalRDeducted = new BigDecimal(obj[1].toString()).setScale(3,0).doubleValue();
                                                            salvageAdjustmentTillDate=new BigDecimal(obj[14].toString()).setScale(3,0);
                                                        }
                                                    }
                                                    double strAdvanceAmount = 0;
                                                    String AdvanceAmount = accPaymentService.getAdvanceContractAmount(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                                                    String salvageAmountStr = accPaymentService.getSalavageContractAmount(Integer.parseInt(tenderId), Integer.parseInt(lotId));
                                                    BigDecimal salvageAmount=new BigDecimal(salvageAmountStr).setScale(3,0);
                                                    if(!"".equalsIgnoreCase(AdvanceAmount))
                                                    {   strAdvanceAmount = new BigDecimal(AdvanceAmount.toString()).doubleValue();
                                                    }else{strAdvanceAmount = new BigDecimal(0).doubleValue();}
                                            if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                            {
                                              objCurBefore = accPaymentService.getCurrencyInAccGenerateBefore(Integer.parseInt(wpId),Integer.parseInt(tenderId));
                                                    if(!objCurBefore.isEmpty() && obj[1]!=null)
                                                        {

                                                        for(int i=0;i<objCurBefore.size();i++){
                                                            for(int j=0;j<total;j++)
                                                                {
                                                                if(curArr[j].equals(objCurBefore.get(i)[1].toString()))
                                                                    continue;
                                                                else if (j+1 == total){
                                                                    curForAdjustment[ti] = objCurBefore.get(i)[1].toString();
                                                                    amountForAdjustment[ti] = objCurBefore.get(i)[0].toString();
                                                                    ti++;
                                                                    }

                                                                }
                                                            }
                                                 }
                                             // System.out.print("aaaaaaaa");
                                            }
                                                    //BigDecimal bigadvadjAmt= new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,0);
                                                %>
                                                <table width="100%" style="border:none;">
                                                     <tr><% if(!tenderType1.equals("ICT")){ %>
                                                        <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(inttotalRDeducted).setScale(3,0)%></td>
                                                        <td style="text-align: right;border:none;padding:0px;">

                                                            <%  for(c = 0; c < total; c++){
                                                                bigadvadjAmt= new BigDecimal((new BigDecimal(cntrAmnt[c].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,BigDecimal.ROUND_HALF_UP);
                                                            %>
                                                                <%=new BigDecimal((bigadvadjAmt.doubleValue()*inttotalRDeducted)/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                            <%}%>
                                                        </td>
                                                    </tr>
                                                                     <%}else if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                            for(int j=0;j<total;j++){
                                                String AdjustmentBefore = "0";
                                                double advAdj = 0;
                                                if(!objCurBefore.isEmpty() && obj[1]!=null)
                                                {
                                                objAdjustmentBefore = cs.returndata("getAdjustAmountGenerateBefore",wpId, curArr[j]);
                                                 AdjustmentBefore = objAdjustmentBefore.get(0)[0].toString();
                                                //   objAdjustmentBefore = accPaymentService.getAdjustAmountGenerateBefore(Integer.parseInt(wpId),curArr[j]);
                                                advAdj = (Double.valueOf(cntrAmnt[j])*Double.valueOf(AdvanceAmount))/100;
                                                }
                                             %>
                                              <tr><td style="text-align: left;border:none;padding:0px;"><%=AdjustmentBefore%></td>
                                            <td style="text-align: right;border:none;padding:0px;width:110px;"><%=new BigDecimal((advAdj * Double.valueOf(AdjustmentBefore))/100).setScale(3,BigDecimal.ROUND_HALF_UP)%> &nbsp;(In  <%=curArr[j]%>)</td>
                                             <%}%> </tr>
                                          <%  if(!objCurBefore.isEmpty() && obj[1]!=null)
                                             {
                                              for(int i=0;i<ti;i++)
                                                  {
                                                    objAdjustmentBefore = cs.returndata("getAdjustAmountGenerateBefore",wpId, curForAdjustment[i]);

                                                   double advAdj = (Double.valueOf(amountForAdjustment[i])*Double.valueOf(AdvanceAmount))/100;
                                                   %>
                                        <tr><td style="text-align: left;border:none;padding:0px;"><%=objAdjustmentBefore.get(0)[0].toString()%></td>
                                            <td style="text-align: right;border:none;padding:0px;width:110px;"><%=new BigDecimal((advAdj * Double.valueOf(objAdjustmentBefore.get(0)[0].toString()))/100).setScale(3,BigDecimal.ROUND_HALF_UP)%> &nbsp;(In  <%=curForAdjustment[i]%>)</td>

                                             <% }%> </tr> <% }}  %>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advAmount")%></td>
                                            <td   class="t-align-left">
                                                <table width="100%" style="border:none;">
                                                    <tr>
                                                        <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strAdvanceAmount).setScale(3,0)%></td>
                                                        <td style="text-align: right;border:none;padding:0px;">
                                                            <%if(!tenderType1.equals("ICT")){%>
                                                                <%=new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%>
                                                            <%} else if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){
                                                                for(c=0; c < total; c++){
                                                            %>
                                                                    <%=new BigDecimal((new BigDecimal(cntrAmnt[c].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                            <%}}%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjAmount")%></td>
                                            <td   class="t-align-left">
                                                <table width="100%" style="border:none;">
                                                    <tr>
                                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAdvAdjAmt()%><%}%></td>
                                                        <td style="text-align: right;border:none;padding:0px;">
                                                            <%if(isEdit){
                                                                 if(!tenderType1.equals("ICT")){ %>
                                                                    <%=new BigDecimal((bigadvadjAmt.doubleValue()*invoicedetailsAll.get(0).get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[0].toString() +") <br/>"%>
                                                                <%}else if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")) {
                                                                    for(c = 0; c < total; c++) {
                                                                        bigadvadjAmt= new BigDecimal((new BigDecimal(cntrAmnt[c].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,BigDecimal.ROUND_HALF_UP);
                                                                %>
                                                                <%=new BigDecimal((bigadvadjAmt.doubleValue()*invoicedetailsAll.get(c).get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>

                                                            <%}}}%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                              <% if(salvageAmount.compareTo(BigDecimal.ZERO)>0 && !tenderType1.equals("ICT") ){ %>
                             <tr>
                                 <td class="ff" width="25%">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="ff" width="25%">Total Salvage Adjustment till Date :</td>
                                <td>
                                     <table width="100%" style="border:none;">
                                     <tr>
                                   <td style="text-align: left;border:none;padding:0px;"> <%=salvageAdjustmentTillDate %> </td>
                                   <td style="text-align: right;border:none;padding:0px;"><span id="SalvageAdjpercentage0"><%= salvageAdjustmentTillDate.multiply(salvageAmount).divide(new BigDecimal(100).setScale(3,0)) %></span><span id="SalvageAdjinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span></td>
                                     </tr>
                                     </table>
                                </td>
                            </tr>
                            <tr>
                                 <td class="ff" width="25%">Total Salvage Amount:</td>
                                <td>

                                <table width="100%" style="border:none;">
                                     <tr>
                                   <td style="text-align: left;border:none;padding:0px;"></td>
                                    <td style="text-align: right;border:none;padding:0px;"> <%= salvageAmount %> <%=bdl.getString("CMS.Inv.InBDT")%></td>
                                    </tr>
                                 </table>
                                </td>
                                <td class="ff" width="25%"> Salvage Adjustment :</td>
                                <td class="t-align-left">
                                <table width="100%" style="border:none;">
                                     <tr>
                                   <td style="text-align: left;border:none;padding:0px;"><span id="SalvageAdjAmtPercentage0"><%= invoicedetailsAll.get(0).get(0).getSalvageAdjAmt() %></span></td>
                                    <td style="text-align: right;border:none;padding:0px;"> <span id="SalvageAdjAmtinbdt0"><%= invoicedetailsAll.get(0).get(0).getSalvageAdjAmt().multiply(salvageAmount).divide(new BigDecimal(100).setScale(3,0)) %><%=bdl.getString("CMS.Inv.InBDT")%></span></td>
                                    </tr>
                                 </table>


                                </td>
                            </tr>
                            <% } %>

                                        <tr>
                                            <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.VAT")%>
                                            <td   class="t-align-left">
                                                <table width="100%" style="border:none;">
                                                <tr>
                                                    <td <%if(!tenderType1.equals("ICT")){%>style="text-align: left;border:none;padding:0px;"<%}else if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){%>style="text-align: right;border:none;padding:0px;"<%}%>><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getVatAmt().toString() + " (In " + curArr[0].toString() + ")"%><%}%></td>
                                                    <%if(!tenderType1.equals("ICT")){%>
                                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(0).get(0).getVatAmt().doubleValue())/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                                    <%}%>
                                                </tr>
                                                </table>
                                            </td>
                                            <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjVAT")%></td>
                                            <td   class="t-align-left">
                                                <table width="100%" style="border:none;">
                                                    <tr>
                                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAdvVatAmt()%><%}%></td>
                                                        <td style="text-align: right;border:none;padding:0px;">
                                                            <%if(isEdit){
                                                                for(c = 0; c < total; c++) {
                                                            %>
                                                                <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getAdvVatAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                            <%}}%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.AIT")%>
                                            <td   class="t-align-left">
                                                <table width="100%" style="border:none;">
                                                <tr>
                                                    <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAitAmt()%><%}%></td>
                                                    <td style="text-align: right;border:none;padding:0px;">
                                                        <%if(isEdit){
                                                            for(c = 0; c < total; c++){
                                                                if((tenderType1.equals("ICT") && curArr[c].equals("BTN")) || (tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes") && curArr[c].equals("BTN"))){
                                                        %>
                                                                    <%=new BigDecimal(((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue() - invoicedetailsAll.get(c).get(0).getVatAmt().doubleValue())*invoicedetailsAll.get(c).get(0).getAitAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                                <%}else{%>
                                                                    <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getAitAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                        <%}}}%>
                                                    </td>
                                                </tr>
                                                </table>
                                            </td>
                                            <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjAIT")%></td>
                                            <td   class="t-align-left">
                                                <table width="100%" style="border:none;">
                                                <tr>
                                                    <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAdvAitAmt()%><%}%></td>
                                                    <td style="text-align: right;border:none;padding:0px;">
                                                        <%if(isEdit){
                                                            for(c = 0; c < total; c++){
                                                        %>
                                                            <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getAdvAitAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                        <%}}%>
                                                    </td>
                                                </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.VATAIT")%></td>
                                            <td   class="t-align-left">
                                                <table width="100%" style="border:none;">
                                                <tr>
                                            <%
                                            if(!tenderType1.equals("ICT")){
                                                if (isEdit) {
                                                    if (invoicedetailsAll.get(0).get(0).getVatAmt() != null && invoicedetailsAll.get(0).get(0).getAitAmt() != null) {
                                                        strVatPlusAit[0] = (invoicedetailsAll.get(0).get(0).getVatAmt().doubleValue() + invoicedetailsAll.get(0).get(0).getAitAmt().doubleValue());
                                                    } else if (invoicedetailsAll.get(0).get(0).getVatAmt() != null && invoicedetailsAll.get(0).get(0).getAitAmt() == null) {
                                                        strVatPlusAit[0] = invoicedetailsAll.get(0).get(0).getVatAmt().doubleValue();
                                                    } else if (invoicedetailsAll.get(0).get(0).getVatAmt() == null && invoicedetailsAll.get(0).get(0).getAitAmt() != null) {
                                                        strVatPlusAit[0] = invoicedetailsAll.get(0).get(0).getAitAmt().doubleValue();
                                                    }
                                                }
                                            %>

                                                    <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strVatPlusAit[0]).setScale(3,0)%></td>
                                                    <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*strVatPlusAit[0])/100).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>

                                            <%}else if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes")){%>
                                                <td style="text-align: left;border:none;padding:0px;"></td>
                                                    <td style="text-align: right;border:none;padding:0px;">
                                                        <%if(isEdit){
                                                            for(c = 0; c < total; c++){
                                                                if(curArr[c].equals("BTN") || curArr[c].equals("Taka")){
                                                                    strVatPlusAit[c] = invoicedetailsAll.get(c).get(0).getVatAmt().doubleValue() + (((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()- invoicedetailsAll.get(c).get(0).getVatAmt().doubleValue())*invoicedetailsAll.get(c).get(0).getAitAmt().doubleValue())/100);
                                                        %>
                                                                    <%=new BigDecimal(strVatPlusAit[c]).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                                <%}else{
                                                                    strVatPlusAit[c] = (invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getAitAmt().doubleValue())/100;
                                                                %>
                                                                    <%=new BigDecimal(strVatPlusAit[c]).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                        <%}}}%>
                                                    </td>
                                            <%}%>
                                                </tr>
                                                </table>
                                            </td>
                                            <td   class="ff"><%=bdl.getString("CMS.Inv.advVATAIT")%>
                                            </td>
                                            <td   class="t-align-left">
                                                <table width="100%" style="border:none;">

                                                <%
                                                    if (isEdit) {
                                                        for(c = 0; c< total; c++){
                                                        if (invoicedetailsAll.get(c).get(0).getAdvVatAmt() != null && invoicedetailsAll.get(c).get(0).getAdvAitAmt() != null) {
                                                            strAdvVatPlusAdvAit[c] = (invoicedetailsAll.get(c).get(0).getAdvVatAmt().doubleValue() + invoicedetailsAll.get(c).get(0).getAdvAitAmt().doubleValue());
                                                        } else if (invoicedetailsAll.get(c).get(0).getAdvVatAmt() != null && invoicedetailsAll.get(c).get(0).getAdvAitAmt() == null) {
                                                            strAdvVatPlusAdvAit[c] = invoicedetailsAll.get(c).get(0).getAdvVatAmt().doubleValue();
                                                        } else if (invoicedetailsAll.get(c).get(0).getAdvVatAmt() == null && invoicedetailsAll.get(c).get(0).getAitAmt() != null) {
                                                            strAdvVatPlusAdvAit[c] = invoicedetailsAll.get(c).get(0).getAdvAitAmt().doubleValue();
                                                        }
                                                %>
                                                <tr>
                                                    <%if(c < 1){%>
                                                        <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strAdvVatPlusAdvAit[0]).setScale(3,0)%></td>
                                                    <%}else{%>
                                                        <td style="text-align: left;border:none;padding:0px;"></td>
                                                    <%}%>
                                                    <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*strAdvVatPlusAdvAit[0])/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%><%}%></td>
                                                </tr>
                                                <%}}%>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- Dohatec Start -->

                                            <tr style="display:none;">
                                                <td class="ff"><%=bdl.getString("CMS.Inv.netVATAIT")%></td>
                                                <td class="t-align-left">
                                                    <table width="100%" style="border:none;">
                                                    <tr>
                                                    <%
                                                        if (isEdit) {
                                                            if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                                            {
                                                            for(c=0;c<total;c++)
                                                                {
                                                                 double bigadvadjAmtt = 0.0;
                                                                 bigadvadjAmtt= (Double.parseDouble(cntrAmnt[c])*strAdvanceAmount)/100;
                                                                 bigadvadjAmtt = (bigadvadjAmtt*invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100;
                                                                 netVatandAitStr[c] = bigadvadjAmtt + strVatPlusAit[c] -((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*strAdvVatPlusAdvAit[c])/100);
                                                                 
                                                                }
                                                            }
                                                            else{
                                                            for(c = 0; c < total; c++){
                                                                  double bigadvadjAmtt = 0.0;
                                                                  bigadvadjAmtt = (bigadvadjAmt.doubleValue()* invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100;
                                                                  netVatandAitStr[c] =  strVatPlusAit[c] - strAdvVatPlusAdvAit[c];

                                                    %>

                                                            <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(netVatandAitStr[c]).setScale(3,0)%></td>
                                                            <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*netVatandAitStr[c])/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%><%}%></td>
                                                        <%}}}%>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td class="ff"></td>
                                                <td class="ff"></td>
                                            </tr>

                                        <!-- Dohatec End -->
                                        <tr>
                                            <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.liquditydamage")%></td>
                                            <td   class="t-align-left">
                                                <table width="100%" style="border:none;">
                                                <tr>
                                                    <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getldAmt()%><%}%></td>
                                                    <td style="text-align: right;border:none;padding:0px;">
                                                        <%if(isEdit){
                                                            for(c = 0; c < total; c++){
                                                        %>
                                                            <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getldAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                        <%}}%>
                                                    </td>
                                                </tr>
                                                </table>
                                            </td>
                                            <td   class="ff"><%=bdl.getString("CMS.Inv.penalty")%>
                                            </td>
                                            <td   class="t-align-left">
                                                <table width="100%" style="border:none;">
                                                <tr>
                                                    <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getPenaltyAmt()%><%}%></td>
                                                    <td style="text-align: right;border:none;padding:0px;">
                                                        <%if(isEdit){
                                                            for(c = 0; c < total; c++){
                                                        %>
                                                            <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getPenaltyAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                        <%}}%>
                                                    </td>
                                                </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.addition")%></div>
                                    <table cellspacing="0" class="tableList_1" width="100%">
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.Bonus")%></td>
                                            <td   class="t-align-left" width="25%">
                                                <table width="100%" style="border:none;">
                                                <tr>
                                                    <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getBonusAmt()%><%}%></td>
                                                    <td style="text-align: right;border:none;padding:0px;">
                                                        <%if(isEdit){
                                                            for(c = 0; c < total; c++){
                                                        %>
                                                            <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getBonusAmt().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                        <%}}%>
                                                    </td>
                                                </tr>
                                                </table>
                                            </td>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.InterestOnDP")%></td>
                                            <td   class="t-align-left" width="25%">
                                                <table width="100%" style="border:none;">
                                                <tr>
                                                    <td style="text-align: left;border:none;padding:0px;"><%if(isEdit && invoicedetailsAll.get(0).get(0).getInterestonDP()!=null){%><%=invoicedetailsAll.get(0).get(0).getInterestonDP()%><%}%></td>
                                                    <td style="text-align: right;border:none;padding:0px;">
                                                        <%if(isEdit && invoicedetailsAll.get(0).get(0).getInterestonDP()!=null){%>
                                                            <%if(isEdit){
                                                            for(c = 0; c < total; c++){
                                                        %>
                                                                <%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getInterestonDP().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                        <%}}}%>
                                                    </td>
                                                </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.retention")%></div>
                                    <table cellspacing="0" class="tableList_1" width="100%">
                                        <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.retentionmoneytilldate")%></td>
                                        <td   class="t-align-left">
                                            <%
                                                double dbtotalRDeducted = 0.0;
                                                if(!totalRDeducted.isEmpty())
                                                {
                                                    Object[] objj = totalRDeducted.get(0);
                                                    if(objj[2]==null)
                                                    {
                                                        dbtotalRDeducted = new BigDecimal(0).setScale(3,0).doubleValue();
                                                    }
                                                    else{
                                                        dbtotalRDeducted =  new BigDecimal(objj[2].toString()).setScale(3,0).doubleValue();
                                                    }
                                                }
                                            %>
                                                <table width="100%" style="border:none;">
                                                <tr>
                                                    <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(dbtotalRDeducted).setScale(3, 0)%></td>
                                                    <td style="text-align: right;border:none;padding:0px;">
                                                        <%for(c = 0; c < total; c++){%>
                                                            <%=new BigDecimal((new BigDecimal(cntrAmnt[c].toString()).doubleValue()*dbtotalRDeducted)/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>
                                                        <%}%>
                                                    </td>
                                                </tr>
                                                </table>
                                        </td>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.retentionmoney")%></td>
                                            <td   class="t-align-left">
                                                    <table width="100%" style="border:none;">
                                                    <tr>
                                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){if(invoicedetailsAll.get(0).get(0).getRetentionMadd()!=null){%><%=invoicedetailsAll.get(0).get(0).getRetentionMadd()%>&nbsp;<span style="color: #f00;">(Added)</span><%}else if(invoicedetailsAll.get(0).get(0).getRetentionMdeduct()!=null){%><%=invoicedetailsAll.get(0).get(0).getRetentionMdeduct()%>&nbsp;<span style="color: #f00;">(Deducted)</span><%}}%></td>
                                                        <td style="text-align: right;border:none;padding:0px;">
                                                            <%if(isEdit){
                                                                for(c = 0; c < total; c++){
                                                                    if(invoicedetailsAll.get(c).get(0).getRetentionMadd()!=null){
                                                                        out.print(new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getRetentionMadd().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>");
                                                                    }
                                                                    else if(invoicedetailsAll.get(c).get(0).getRetentionMdeduct()!=null){
                                                                        out.print(new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getRetentionMdeduct().doubleValue())/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>");
                                                                    }
                                                                }
                                                            }%></td>
                                                    </tr>
                                                    </table>
                                            </td>
                                        </tr>
                                        <tr> <td colspan="2"><ol style="padding-right: 5px;margin-left: 20px;"><li>Retention money at a percentage as specified in Schedule II will be
                                                     deductable from each bill due to a Contractor until completion of the whole Works or delivery.</li>
                                                 <li>On completion of the whole Works, half the total amount retained shall be repaid to
                                                     the Contractor and the remaining amount may also be paid to the Contractor if an unconditional
                                                     Bank guarantee is furnished for that remaining amount.</li>
                                                 <li >The remaining amount or the Bank guarantee, under Sub-Rule (2), shall be returned,
                                                     within the period specified in schedule II , after issuance of all Defects Correction Certificate
                                                     under Rule 39(29) by the Project Manager or any other appropriate Authority ..</li>
                                                 <li >Deduction of retention money shall not be applied to small Works Contracts if no
                                                     advance payment has been made to the Contractor and in such case the provisions of Sub-Rule
                                                     (7) and (8) of Rule 27 shall be applied.</li></ol>
                                             </td><td></td>
                                        </tr>
                                    </table>
                                    <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails")%> <%if(!tenderType1.equals("ICT")){%>&nbsp;&nbsp;<%=invoicedetailsAll.get(0).get(0).getGrossAmt()+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></div>
                                    <table class="tableList_1" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%> <span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if (isEdit) {%><%=invoicedetailsAll.get(0).get(0).getModeOfPayment()%><%}%>
                                                <span id="modeofPaymentspan" class="reqF_1"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%> <span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if (isEdit) {%><%=DateUtils.customDateFormate(invoicedetailsAll.get(0).get(0).getDateOfPayment())%><%}%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%> <span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if (isEdit) {%><%=invoicedetailsAll.get(0).get(0).getBankName()%><%}%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%> <span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if (isEdit) {%><%=invoicedetailsAll.get(0).get(0).getBranchName()%><%}%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if (isEdit) {%><%=invoicedetailsAll.get(0).get(0).getInstrumentNo()%><%}%>
                                            </td>
                                        </tr>
                                    </table>
                                    <%
                                        double NetVat = 0;
                                        if(invoicedetailsAll.get(0).get(0).getVatAmt()!=null)
                                        {
                                            if(invoicedetailsAll.get(0).get(0).getAdvVatAmt()!=null)
                                            {
                                                NetVat = (((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(0).get(0).getVatAmt().doubleValue())/100) - ((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(0).get(0).getAdvVatAmt().doubleValue())/100)) ;
                                            }
                                        }
                                    %>
                                    <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails.vat")%><%if(!tenderType1.equals("ICT")){%>&nbsp;&nbsp;<%=new BigDecimal(NetVat).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></div>
                                    <%if(invoicedetailsAll.get(0).get(0).getVatdateOfPayment()!=null){%>
                                    <table class="tableList_1" cellspacing="0" width="100%">
                                        <tr>
                                        <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%><span class="mandatory">*</span></td>
                                        <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getVatmodeOfPayment()%><%}%>
                                            <span id="vatmodeofPaymentspan" class="reqF_1"></span>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%><span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if(isEdit){%><%=DateUtils.customDateFormate(invoicedetailsAll.get(0).get(0).getVatdateOfPayment())%><%}%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%><span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getVatbankName()%><%}%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%><span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getVatbranchName()%><%}%>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getVatinstrumentNo()%><%}%>
                                            </td>
                                        </tr>
                                    </table>
                                    <%}%>
                                    <%
                                        double NetAit = 0;
                                        if(invoicedetailsAll.get(0).get(0).getVatAmt()!=null)
                                        {
                                            if(invoicedetailsAll.get(0).get(0).getAdvVatAmt()!=null)
                                            {
                                                NetAit = (((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(0).get(0).getAitAmt().doubleValue())/100) - ((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(0).get(0).getAdvAitAmt().doubleValue())/100)) ;
                                            }
                                        }
                                    %>
                                    <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails.ait")%> <%if(!tenderType1.equals("ICT")){%>&nbsp;&nbsp;<%=new BigDecimal(NetAit).setScale(3,0)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></div>
                                    <%if(invoicedetailsAll.get(0).get(0).getAitdateOfPayment()!=null){%>
                                    <table class="tableList_1" cellspacing="0" width="100%">
                                        <tr>
                                        <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%><span class="mandatory">*</span></td>
                                        <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAitmodeOfPayment()%><%}%>
                                            <span id="aitmodeofPaymentspan" class="reqF_1"></span>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%><span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if(isEdit){%><%=DateUtils.customDateFormate(invoicedetailsAll.get(0).get(0).getAitdateOfPayment())%><%}%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%><span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAitbankName()%><%}%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%><span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAitbranchName()%><%}%>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetailsAll.get(0).get(0).getAitinstrumentNo()%><%}%>
                                            </td>
                                        </tr>
                                    </table>
                                    <%}%>
                                    <div class="tableHead_1 t_space" align="left">Net Amount</div>
                                    <table class="tableList_1" cellspacing="0" width="100%">

                                            <%
                                                /*double totalDeductedAmount = 0;
                                                double totalDeductedAmountt = 0;
                                                double totalAddedAmount = 0;*/
                                                double totalDeductedAmount[] = new double[4];
                                                double totalDeductedAmountt[] = new double[4];
                                                double totalAddedAmount[] = new double[4];
                                                BigDecimal bigdeductedAmt = BigDecimal.ZERO;

                                                for(c = 0; c < total; c++){
                                                    bigdeductedAmt = BigDecimal.ZERO.setScale(3,BigDecimal.ROUND_HALF_UP);
                                            %>
                                            <tr>
                                                <td class="ff" bgcolor="#dddddd">Total Amount Deducted <%="(In "+ curArr[c].toString()%>) :</td>
                                                <td>
                                                    <%

                                                        if(isEdit)
                                                        {
                                                          /*if(invoicedetailsAll.get(c).get(0).getAdvAdjAmt()!=null)
                                                          {
                                                              totalDeductedAmount[c] = totalDeductedAmount[c] +(invoicedetailsAll.get(c).get(0).getAdvAdjAmt()).doubleValue();
                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getldAmt()!=null)
                                                          {
                                                              totalDeductedAmount[c] = totalDeductedAmount[c] +(invoicedetailsAll.get(c).get(0).getldAmt()).doubleValue();
                                                              totalDeductedAmountt[c] = totalDeductedAmountt[c] +(invoicedetailsAll.get(c).get(0).getldAmt()).doubleValue();
                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getPenaltyAmt()!=null)
                                                          {
                                                              totalDeductedAmount[c] = totalDeductedAmount[c] +(invoicedetailsAll.get(c).get(0).getPenaltyAmt()).doubleValue();
                                                              totalDeductedAmountt[c] = totalDeductedAmountt[c] +(invoicedetailsAll.get(c).get(0).getPenaltyAmt()).doubleValue();
                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getRetentionMdeduct()!=null)
                                                          {
                                                              totalDeductedAmount[c] = totalDeductedAmount[c] + (invoicedetailsAll.get(c).get(0).getRetentionMdeduct()).doubleValue();
                                                              totalDeductedAmountt[c] = totalDeductedAmountt[c] + (invoicedetailsAll.get(c).get(0).getRetentionMdeduct()).doubleValue();
                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getRetentionMadd()!=null)
                                                          {
                                                              totalAddedAmount[c] = totalAddedAmount[c] + (invoicedetailsAll.get(c).get(0).getRetentionMadd()).doubleValue();
                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getBonusAmt()!=null)
                                                          {
                                                              totalAddedAmount[c] = totalAddedAmount[c] + (invoicedetailsAll.get(c).get(0).getBonusAmt()).doubleValue();
                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getInterestonDP()!=null)
                                                          {
                                                              totalAddedAmount[c] = totalAddedAmount[c] + (invoicedetailsAll.get(c).get(0).getInterestonDP()).doubleValue();
                                                          }*/
                                                          if(invoicedetailsAll.get(c).get(0).getAdvAdjAmt()!=null)
                                                          {
                                                                totalDeductedAmount[c] = totalDeductedAmount[c] + new BigDecimal((new BigDecimal((new BigDecimal(cntrAmnt[c].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,BigDecimal.ROUND_HALF_UP).doubleValue()*invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                                                
                                                          }
                                                           if(invoicedetailsAll.get(c).get(0).getSalvageAdjAmt()!=null)
                                                          {
                                                                totalDeductedAmount[c] = totalDeductedAmount[c] + invoicedetails.get(0).getSalvageAdjAmt().multiply(salvageAmount).divide(new BigDecimal(100).setScale(3,0)).doubleValue();
                                                                
                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getldAmt()!=null)
                                                          {
                                                              totalDeductedAmount[c] = totalDeductedAmount[c] + new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getldAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                                              totalDeductedAmountt[c] = totalDeductedAmountt[c] +new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getldAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getPenaltyAmt()!=null)
                                                          {
                                                              totalDeductedAmount[c] = totalDeductedAmount[c] + new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getPenaltyAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                                              totalDeductedAmountt[c] = totalDeductedAmountt[c] + new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getPenaltyAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getRetentionMdeduct()!=null)
                                                          {
                                                              totalDeductedAmount[c] = totalDeductedAmount[c] + new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getRetentionMdeduct().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                                              totalDeductedAmountt[c] = totalDeductedAmountt[c] + new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getRetentionMdeduct().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getRetentionMadd()!=null)
                                                          {
                                                              totalAddedAmount[c] = totalAddedAmount[c] + new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getRetentionMadd().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                                              System.out.println(totalAddedAmount[c]);
                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getBonusAmt()!=null)
                                                          {
                                                              totalAddedAmount[c] = totalAddedAmount[c] + new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getBonusAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();

                                                          }
                                                          if(invoicedetailsAll.get(c).get(0).getInterestonDP()!=null)
                                                          {
                                                              totalAddedAmount[c] = totalAddedAmount[c] + new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*invoicedetailsAll.get(c).get(0).getInterestonDP().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                                          }
                                                          if(netVatandAitStr[c]>0)
                                                          {
                                                                /*totalDeductedAmount = totalDeductedAmount+netVatandAitStr;
                                                                totalDeductedAmountt = totalDeductedAmountt+netVatandAitStr;*/
                                                              if(!tenderType1.equals("ICT"))
                                                              {
                                                               // totalDeductedAmount[c] = totalDeductedAmount[c] + netVatandAitStr[c];
                                                              //  totalDeductedAmountt[c] = totalDeductedAmountt[c] + netVatandAitStr[c];
                                                              //  bigdeductedAmt =  new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*totalDeductedAmountt[c])/100).setScale(3,BigDecimal.ROUND_HALF_UP).add(new BigDecimal((bigadvadjAmt.doubleValue()*invoicedetailsAll.get(c).get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP));
                                                                  bigdeductedAmt = new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*netVatandAitStr[c])/100).setScale(3,BigDecimal.ROUND_HALF_UP).add(new BigDecimal(totalDeductedAmount[c]).setScale(3, BigDecimal.ROUND_HALF_UP));
                                                              }
                                                              else if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                                              {
                                                                 //bigdeductedAmt =  new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*totalDeductedAmountt[c])/100).setScale(3,BigDecimal.ROUND_HALF_UP).add(new BigDecimal((bigadvadjAmt.doubleValue()*invoicedetailsAll.get(c).get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP));
                                                                 bigdeductedAmt = new BigDecimal(netVatandAitStr[c]).setScale(3,BigDecimal.ROUND_HALF_UP).add(new BigDecimal(totalDeductedAmountt[c]).setScale(3, BigDecimal.ROUND_HALF_UP));
                                                              }
                                                          }
                                                          if(netVatandAitStr[c]<0)
                                                          {
                                                              totalAddedAmount[c] = totalAddedAmount[c]-netVatandAitStr[c];
                                                          }
                                                           if(netVatandAitStr[c]==0.0){
                                                                  System.out.println("final netvat:"+netVatandAitStr[c]);
                                                                  bigdeductedAmt=new BigDecimal(totalDeductedAmount[c]).setScale(3, BigDecimal.ROUND_HALF_UP);
                                                               }
                                                          //out.print(new BigDecimal(totalDeductedAmount).setScale(3,0));
                                                        }
                                                    %>
                                                    <%--<span style="padding-left:95px;"><%if(isEdit){out.print(new BigDecimal((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*totalDeductedAmount)/100).setScale(3,0));}%> (In BTN)</span>--%>
                                                    <table width="100%" style="border:none;">
                                                    <tr>
                                                        <%
                                                            //BigDecimal bigdeductedAmt =  new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*totalDeductedAmountt)/100).setScale(3,0).add(new BigDecimal((bigadvadjAmt.doubleValue()*invoicedetailsAll.get(c).get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,0));
                                                        %>
                                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=bigdeductedAmt+" "+ "(In "+ curArr[c].toString() +")"%><%}%></td>
                                                        <td style="text-align: right;border:none;padding:0px;"><%--<%=new BigDecimal(totalDeductedAmount).setScale(3,0)%>&nbsp;(in %)--%></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                        </tr>
                                        <tr>
                                                <td class="ff" bgcolor="#dddddd">Total Amount Added (<%="In "+ curArr[c].toString()%>) :</td>
                                                <td>
                                                    <table width="100%" style="border:none;">
                                                    <tr>
                                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%>
                                                                <%=new BigDecimal(totalAddedAmount[c]).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[c]+")"%>
                                                                <%--<%=new BigDecimal((invoicedetailsAll.get(c).get(0).getInvoiceAmt().doubleValue()*totalAddedAmount[c])/100).setScale(3,0)+" "+ "(In "+ curArr[c].toString() +") <br/>"%>--%>
                                                            <%}%></td>
                                                        <td style="text-align: right;border:none;padding:0px;"><%--<%=new BigDecimal(totalAddedAmount).setScale(3,0)%>&nbsp;(in %)--%></td>
                                                    </tr>
                                                    </table>
                                                <%--<%out.print(new BigDecimal(totalAddedAmount).setScale(3,0));%>
                                                <span style="padding-left:88px;"><%if(isEdit){out.print(new BigDecimal((invoicedetailsAll.get(0).get(0).getInvoiceAmt().doubleValue()*totalAddedAmount)/100).setScale(3,0));}%> (In BTN)</span>--%>
                                                </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="25%"  class="t-align-left">Net Amount <%="(In "+ curArr[c].toString() +")"%></td>
                                            <td   class="t-align-left" style="text-align: right"><%if (isEdit) {%><%=invoicedetailsAll.get(c).get(0).getGrossAmt()%><%}%>
                                                <input type="hidden" id="grosshiddenamt_<%=c%>" name ="netAmount" <%if(isEdit) {%>value ="<%=invoicedetailsAll.get(c).get(0).getGrossAmt()%>"<%}%>>
                                                &nbsp;<span id="grossinWords_<%=c%>" class="ff"></span>
                                            </td>
                                        </tr>
                                    <%}}%>
                                    </table>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <td class="t-align-left ff" width="15%">
                                                <%=bdl.getString("CMS.Inv.Upload")%>
                                            </td>
                                            <td>
                                                <%if(!"fin".equalsIgnoreCase(request.getParameter("flag"))){%>
                                                <%if(!tenderType1.equals("ICT")){%>
                                                    <a class="noprint" href="AccPaymentDocUpload.jsp?tenderId=<%=request.getParameter("tenderId")%>&invoiceId=<%=request.getParameter("invoiceId")%>&lotId=<%=lotId%>&wpId=<%=wpId%>">Upload</a>
                                                <%}else if(tenderType1.equals("ICT") || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                                                              {%>
                                                    <a class="noprint" href="AccPaymentDocUpload.jsp?tenderId=<%=request.getParameter("tenderId")%>&invoiceNo=<%=request.getParameter("invoiceNo")%>&lotId=<%=lotId%>&wpId=<%=wpId%>">Upload</a>
                                                <%}%>
                                                <%}%>
                                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                    <tr>
                                                        <th width="8%" class="t-align-left"><%=bdl.getString("CMS.Srno")%></th>
                                                        <th class="t-align-left" width="23%"><%=bdl.getString("CMS.Inv.FileName")%></th>
                                                        <th class="t-align-left" width="28%"><%=bdl.getString("CMS.Inv.FileDescription")%></th>
                                                        <th class="t-align-left" width="9%"><%=bdl.getString("CMS.Inv.FileSize")%><br /><%=bdl.getString("CMS.Inv.inKB")%></th>
                                                        <th class="t-align-left" width=""><%=bdl.getString("CMS.Doc.Uplodedby")%></th>
                                                        <th class="t-align-left" width="18%"><%=bdl.getString("CMS.action")%></th>
                                                    </tr>
                                                    <%
                                                            //List<TblCmsInvoiceDocument> getInvoiceDocData = accPaymentService.getInvoiceDocDetails(Integer.parseInt(invoiceId));
                                                            List<TblCmsInvoiceDocument> getInvoiceDocData = accPaymentService.getInvoiceDocDetails(Integer.parseInt(invoiceIds[0]));
                                                            if (!getInvoiceDocData.isEmpty()) {
                                                                for (int i = 0; i < getInvoiceDocData.size(); i++) {
                                                    %>
                                                    <tr>
                                                        <td class="t-align-left"><%=(i + 1)%></td>
                                                        <td class="t-align-left"><%=getInvoiceDocData.get(i).getDocumentName()%></td>
                                                        <td class="t-align-left"><%=getInvoiceDocData.get(i).getDocDescription()%></td>
                                                        <td class="t-align-left"><%=(Long.parseLong(getInvoiceDocData.get(i).getDocSize()) / 1024)%></td>
                                                            <td class="t-align-left">
                                                                <%String str = "";
                                                                if (2 == getInvoiceDocData.get(i).getUserTypeId()) {
                                                                    str = "supplier";
                                                                    out.print(strProcNature);
                                                                } else {
                                                                    if("25".equalsIgnoreCase(accPaymentService.getProcurementRoleID(Integer.toString(getInvoiceDocData.get(i).getUploadedBy()))))
                                                                    {
                                                                        str = "accountant";
                                                                        out.print("Account Officer");
                                                                    }else{
                                                                        str = "PE";
                                                                        out.print("PE Officer");
                                                                    }

                                                                }%>
                                                            </td>
                                                        <td class="t-align-left">
                                                            <a href="<%=request.getContextPath()%>/AccPaymentDocServlet?docName=<%=getInvoiceDocData.get(i).getDocumentName()%>&docSize=<%=getInvoiceDocData.get(i).getDocSize()%>&tenderId=<%=tenderId%>&invoiceDocId=<%=getInvoiceDocData.get(i).getInvoiceDocId()%>&invoiceId=<%=getInvoiceDocData.get(i).getTblCmsInvoiceMaster().getInvoiceId()%>&supplier=<%if (2 == getInvoiceDocData.get(i).getUserTypeId()) {%>yes<%}%>&str=<%=str%>&funName=download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                        </td>
                                                    </tr>
                                                    <%}
                                                                                                                                        } else {%>
                                                    <tr>
                                                        <td colspan="6" class="t-align-left"><%=bdl.getString("CMS.Inv.NoRecord")%></td>
                                                    </tr>
                                                    <%}%>
                                                </table>
                                            </td>
                                        </tr>
                                        <%
                                          if (HideGeneratedbyTenderer) {
                                                if (HideSendToTenderer) {
                                                    if (HideRemarksbype) {
                                        %>
                                        <%if(!"fin".equalsIgnoreCase(request.getParameter("flag"))){%>
                                        <tr>
                                            <td class="t-align-left ff" width="15%">
                                                Remarks : <span class="reqF_1">*</span>
                                            </td>
                                            <td width="100%" class="t-align-left">
                                                <label>
                                                    <textarea name="txtRemarks" rows="4" class="formTxtBox_1" id="txtReply" style="width:738px;"></textarea>
                                                </label>
                                                <span class="reqF_1" id="spantxtReply" ></span>
                                            </td>
                                        </tr>
                                        <table class="t_space" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="t-align-left">
                                                    <span class="formBtn_1"><input type="submit" id="SavePaymentid"name="SavePaymentname" value="Send To Accounts Officer" onclick="return validateRemarks()"/></span>&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <%}%>
                                        <%      }
                                             }
                                        %>
                                    </table>



                                        <%
                                            //List<TblCmsInvRemarks> getRemarks = accPaymentService.getRemarksDetails(Integer.parseInt(invoiceId));
                                            List<TblCmsInvRemarks> getRemarks = accPaymentService.getRemarksDetails(Integer.parseInt(invoiceIds[0]));

                                            if (!getRemarks.isEmpty()) {
                                        %>
                                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.Remarks.ViewCaps")%></div>
                                        <table class="tableList_1" cellspacing="0" width="100%">
                                        <%
                                                for (int i = 0; i < getRemarks.size(); i++) {
                                        %>
                                        <tr>
                                            <td class="t-align-left" width="15%">
                                                <%=(i + 1)%>
                                            </td>
                                            <td>
                                                <%=getRemarks.get(i).getRemarks()%>
                                            </td>
                                        </tr>
                                        <%}
                                     }}%>
                                    </table>
                                    <%if("rejected".equalsIgnoreCase(invstatusObject.get(0).toString()) || "acceptedbype".equalsIgnoreCase(invstatusObject.get(0).toString())){
                                       //List<Object> inv = accPaymentService.getInvoiceRemarks(Integer.parseInt(invoiceId));
                                        List<Object> inv = accPaymentService.getInvoiceRemarks(Integer.parseInt(invoiceIds[0]));
    %>                                  <br />
                                    <table class="tableList_1" cellspacing="0" width="100%">
                                        <tr><td class="t_align_left ff" width="15%">
                                              Remarks By PE
                                            </td>
                                            <td><%=inv.get(0)%></td>
                                        </tr>
                                    </table>
                                    <%}%>



                                    <%if ("createdbyten".equalsIgnoreCase(invstatusObject.get(0).toString())) {%>
                                    <%if(!"fin".equalsIgnoreCase(request.getParameter("flag"))){%>
                                    <br />
                                    <table class="tableList_1" cellspacing="0" width="100%">
                                        <tr><td  class="t-align-left ff" width="15%">
                                                Remarks<span class="mandatory">*</span>
                                            </td>
                                            <td>
                                                <textarea name="remarks" id="remarks" rows="5" cols="40" class="formTxtBox_1"></textarea>
                                            </td></tr>
                                    </table>
                                    <br />
                                    <input type="hidden" name="lotId" value="<%=lotId%>">
                                    <input type="hidden" name="tenderId" value="<%=tenderId%>">
                                    <span class="formBtn_1"><input type="button" value="Accept" onclick="acceptinv()" /></span>
                                    <%if(request.getParameter("Sheetflag")==null){%>
                                    <span class="formBtn_1"><input type="button" value="Reject" onclick="rejectinv()" /></span>
                                    <%}%>
                                      <%}}%>
                                </form>
                            </div>
                        </div>
                    </div>
                </div></div></div></div>


        <%@include file="../resources/common/Bottom.jsp" %>
    </div></body>
    <script>
        function Edit(wpId,tenderId,lotId){


            dynamicFromSubmit("EditDatesForBoq.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms");

        }
        function view(wpId,tenderId,lotId){
            dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
        }
    </script>
    <script>
        //document.getElementById("grossinWords").innerHTML = '('+(DoIt(document.getElementById("grosshiddenamt").value))+')';

        // Dohatec Start
        AmntInWord();

        function AmntInWord()
        {
            var totalNetAmt = document.getElementsByName("netAmount").length;
            for(var i = 0; i < totalNetAmt; i++)
            {
                //document.getElementById("grossinWords_"+i).innerHTML = '('+(DoIt(document.getElementById("grosshiddenamt_"+i).value))+')';
                //Numeric to word currency conversion by Emtaz on 20/April/2016
                document.getElementById("grossinWords_"+i).innerHTML = '('+(CurrencyConverter(document.getElementById("grosshiddenamt_"+i).value))+')';
            }

            var totalInvoice = document.getElementsByName("invoiceAmount").length;
            for(var i = 0; i < totalNetAmt; i++)
            {
                //document.getElementById("invoiceAmountWrd_"+i).innerHTML = '('+(DoIt(document.getElementById("invoiceAmount_"+i).value))+')';
                document.getElementById("invoiceAmountWrd_"+i).innerHTML = '('+(CurrencyConverter(document.getElementById("invoiceAmount_"+i).value))+')';
            }
        }
        // Dohatec End

        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        function validateRemarks()
        {
           var flag = true;
           if(document.getElementById("txtReply").value=="")
           {
               document.getElementById("spantxtReply").innerHTML= "Please enter Remarks";
               flag = false;
           }
           else
           {
               if(document.getElementById("txtReply").value.length>2000)
               {
                   document.getElementById("spantxtReply").innerHTML= "Maximum 2000 characters are allowed for the remarks";
                   flag = false;
               }
           }
           return flag;
        }
    </script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#print").click(function() {
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        $('#print_area').printElement(options);
    }
</script>
<script type="text/javascript">
            function acceptinv(){
                if(document.getElementById("remarks").value==""){
                    jAlert("It is mandatory to specify remarks","Invoice", function(RetVal) {
                    });
                }else if(document.getElementById("remarks").value.length>2000) {
                    jAlert("Maximum 2000 characters are allowed for the remarks","Invoice", function(RetVal) {
                    });
                }else{

                    document.getElementById("action").value="acceptInvoice";
                    document.getElementById("status").value="accept";
                    document.forms["AccPaymentfrm"].submit()
                }

            }
            function rejectinv(){

                if(document.getElementById("remarks").value==""){
                    jAlert("It is mandatory to specify remarks","Invoice", function(RetVal) {
                    });
                }
                else if(document.getElementById("remarks").value.length>2000) {
                    jAlert("Maximum 2000 characters are allowed for the remarks","Invoice", function(RetVal) {
                    });
                }
                else{
                    document.getElementById("action").value="acceptInvoice";
                    document.getElementById("status").value="reject";
                    document.forms["AccPaymentfrm"].submit()
                }

            }
        </script>
</html>
