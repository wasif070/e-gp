<%--
    Document   : PreTenderReply
    Created on : Nov 27, 2010, 6:32:39 PM
    Author     : parag
--%>

<%@page import="java.net.URLDecoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Pre - Tender Meeting</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <%--<script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <%
                            int userId = 0;
                            byte suserTypeId = 0;
                            int tenderId = 20;
                            int queryId = 1;
                            int cuserId = 0;
                            String viewType = "";

                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            if (request.getParameter("queryId") != null) {
                                queryId = Integer.parseInt(request.getParameter("queryId"));
                            }
                            if (request.getParameter("cuserId") != null) {
                                cuserId = Integer.parseInt(request.getParameter("cuserId"));
                            }
                            if (request.getParameter("viewType") != null) {
                                viewType = request.getParameter("viewType");
                            }

                            SPTenderCommonData getQueryText = preTendDtBean.getDataFromSP("GetQueryText", queryId, 0).get(0);
                            SPTenderCommonData getReplyText = preTendDtBean.getDataFromSP("GetReplyText", queryId, 0).get(0);

                            pageContext.setAttribute("tenderId", tenderId);

                %>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <%@include  file="../resources/common/TenderInfoBar.jsp" %>
                        <div class="pageHead_1" style="margin-top: 15px;">Pre – Tender Meeting Replies View
                        <span class="c-alignment-right"><a href="PreTenderMeeting.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a></span>
                        </div>
                        <%if (suserTypeId != 2) {%>
                        <%--<%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div>&nbsp;</div>
                        <% pageContext.setAttribute("tab", 4);%>
                        <jsp:include page="officerTabPanel.jsp" >
                            <jsp:param name="tab" value="4" />
                        </jsp:include>--%>
                        <%}%>
                        <div>&nbsp;</div>

                        <form method="post" id="frmPreTendQuery" action="<%=request.getContextPath()%>/PreTendQuerySrBean?action=replyQuery">
                            <div class="tabPanelArea_1">
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <td width="16%" class="t-align-left ff">Query :</td>
                                        <td width="84%" class="t-align-left"><%=URLDecoder.decode(getQueryText.getFieldName2())%></td>
                                    </tr>
                                    <tr>
                                        <td width="16%" class="t-align-left ff">Rephrase Query :</td>
                                        <td width="84%" class="t-align-left">
                                            <% 
                                             if(getReplyText.getFieldName3() != null){
                                                    out.println(getReplyText.getFieldName3());
                                             }
                                            %></td>
                                    </tr>
                                      <tr>
                                        <td class="t-align-left ff">Download Reference Document : </td>
                                        <td class="t-align-left">
                                             <table width="90%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th width="4%" class="t-align-center">Sl.  No.</th>
                    <th class="t-align-center" width="23%">File Name</th>
                    <th class="t-align-center" width="32%">File Description</th>
                    <th class="t-align-center" width="7%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-center" width="18%">Action</th>
                </tr>
                <%
                            boolean showOnlyDoc = true;
                            String preDocType = request.getParameter("Prebid");
                            
                            if(request.getParameter("queryId") != null){
                                queryId = Integer.parseInt(request.getParameter("queryId"));
                            }
                           
                            //String tender = request.getParameter("tender");
                            int docCnt = 0;
                            TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                            java.util.List<SPTenderCommonData> docData=null;
                            if("Prebid".equalsIgnoreCase(preDocType)){
                                   docData =  tenderCS.returndata("PreTenderQueDocuments", ""+tenderId,""+queryId);
                            }else{
                               // docData =  tenderCS.returndata("QuestionDocsBy", ""+tenderId,userId);
                                         docData =  tenderCS.returndata("PreTenderQueDocuments", ""+tenderId,""+queryId);
                            }

                            for (SPTenderCommonData sptcd : docData) {
                                docCnt++;
                %>
                <tr>
                    <td class="t-align-center"><%=docCnt%></td>
                    <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                    <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                    <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3())/1024)%></td>
                    <td class="t-align-center">
                        <a href="<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tender=<%=tenderId%>&queryId=<%=queryId%>&docType=<%=preDocType%>&funName=download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                    </td>
                </tr>

                      <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                <% if (docCnt == 0) {%>
                <tr>
                    <td colspan="5" class="t-align-center">No records found.</td>
                </tr>
                <%}%>
            </table>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="16%" class="t-align-left ff">Reply :</td>
                                        <td width="84%" class="t-align-left">
                                            <%
                                                if(getReplyText.getFieldName2() != null){
                                                    out.println(URLDecoder.decode(getReplyText.getFieldName2()));
                                             }%>
                                       </td>
                                    </tr>
                                </table>
                                <div id="docData">                                  
                                </div>
                                <div>&nbsp;</div>
                                <%--<div class="t-align-center">
                                    <label class="formBtn_1">
                                      <input type="submit" name="button3" id="button3" value="Reply" />
                                      <input type="hidden" name="hidQueryId" id="hidQueryId" value="<%=queryId%>" />
                                    </label>
                                </div>--%>
                            </div>
                            <div>&nbsp;</div>
                        </form>
                        <!--Dashboard Content Part End-->
                        <!--Dashboard Footer Start-->
                        <%@include file="../resources/common/Bottom.jsp" %>
                        <!--Dashboard Footer End-->
                    </div>
                </div>
            </div>
        </div>
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<form id="form2" method="post">
</form>
<script>
    function checkStatus(){
        if($('#cmbPostQuery').val() == "Reply"){
            $('#divReply').show();
        }else if($('#cmbPostQuery').val() == "Hold"){
            $('#divReply').hide();

        }else if($('#cmbPostQuery').val() == "Ignore"){
            $('#divReply').hide();
        }
    }

    function checkRepQuery(){
        if(document.getElementById("checkbox").checked){
            $('#lblRepQuery').show();
        }else{
            $('#lblRepQuery').hide();
        }
    }

    function getDocData(){
    <%-- $.ajax({
      url: "<%=request.getContextPath()%>/PreTendQuerySrBean?tenderId=<%=tenderId%>&queryId=<%=queryId%>&action=docData&docAction=PreTenderReplyDocs",
      method: 'POST',
      async: false,
      success: function(j) {
          document.getElementById("docData").innerHTML = j;
      }
  });--%>
          $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,queryId:<%=queryId%>,action:'docData',docAction:'PreTenderReplyDocs'}, function(j){
              document.getElementById("docData").innerHTML = j;
          });
      }
      getDocData();

      function downloadFile(docName,docSize,tender){
    <%--$.ajax({
             url: "<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download",
             method: 'POST',
             async: false,
             success: function(j) {
             }
     });--%>
             document.getElementById("form2").action="<%=request.getContextPath()%>/PreTenderReplyUploadServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download";
             document.getElementById("form2").submit();
         }

         function deleteFile(docName,docId,tender){
    <%--$.ajax({
           url: "<%=request.getContextPath()%>/PreTenderReplyUploadServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=remove",
           method: 'POST',
           async: false,
           success: function(j) {
               jAlert("Document Deleted."," Delete Document ", function(RetVal) {
               });
           }
   });--%>
           $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
               if(r == true){
                   $.post("<%=request.getContextPath()%>/PreTenderReplyUploadServlet", {docName:docName,docId:docId,tender:tender,funName:'remove'}, function(j){
                       jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                           getDocData();
                       });
                   });
               }else{
                   getDocData();
               }
           });
       }

</script>
