<%-- 
    Document   : SchedBankEditUser
    Created on : Oct 23, 2010, 7:47:22 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="scBankDevPartUserDtBean" class="com.cptu.egp.eps.web.databean.ScBankDevPartUserDtBean" scope="page"/>
<%

            String partnerType = request.getParameter("partnerType");
            String type = "";
            String title = "";
            String userType = "";
            if (partnerType != null && partnerType.equals("devPartner")) {
                type = "Development";
                title = "Development Partner";
                userType = "dev";
            }
            else {
                type = "ScheduleBank";
                title = "Schedule Bank";
                userType = "sb";
            }

            String userID = request.getParameter("userId");
            if (userID != null) {
                scBankDevPartUserDtBean.setUserId(Integer.parseInt(userID));
                scBankDevPartUserDtBean.populateInfo();
            }
%>
<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - <%=title%> User Role Updation</title>
        <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">

            $(document).ready(function(){
                $("#frmSchedBankUser").validate({
                    rules:{
                        fullName:{required:true,maxlength:200},
                        bngName:{maxlength:200},
                        ntnlId:{maxlength:25},
                        mobileNo:{required:true,number:true,minlength:8,maxlength:8}
                    },

                    messages:{
                        fullName:{required:"<div class='reqF_1'>Please Enter Full Name.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 200 characters are allowed.</div>"
                        },
                        bngName:{maxlength:"<div class='reqF_1'>Maximum 200 characters are allowed.</div>"
                        },
                        ntnlId:{maxlength:"<div class='reqF_1'>Maximum 25 characters are allowed.</div>"},

                        mobileNo:{required:"<div class='reqF_1'>Please Enter Mobile no.</div>",
                            number:"<div class='reqF_1'>Please Enter Numbers only.</div>",
                            minlength:"<div class='reqF_1'>Minimum 8 digits required.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 8 digits are allowed.</div>"
                        }
                    }

                });

            });
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header Start-->
                <!--Dashboard Header End-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <!--Dashboard Content Part Start-->
                            <div class="pageHead_1"> Edit <%=title%> User</div>
                             <!-- Success failure -->
                            <%
                                      String msg = request.getParameter("msg");
                                      if (msg != null && msg.equals("fail")) {%>
                            <div class="responseMsg errorMsg">User Updation Failed.</div>
                            <%}
                                                                    else if (msg != null && msg.equals("success")) {%>
                            <div class="responseMsg successMsg">User Updated Successfully</div>
                            <%}%>
                            <!-- Result Display End-->
                            <form method="POST" id="frmSchedBankUser" action="<%=request.getContextPath()%>/ScBnkDevpartUserSrBean">
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td class="ff">Branch : <span>*</span></td>
                                        <td><input name="office" id="txtBranch" value="${scBankDevPartUserDtBean.branch}" type="text" class="formTxtBox_1"  readonly="readonly" style="width:200px;" />
                                            <input type="hidden" name="partnerType" value="<%=type%>"/>
                                            <input type="hidden" name="action" value="update"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">e-mail ID : <span>*</span></td>
                                        <td><input name="mailId" id="txtMail" type="text" class="formTxtBox_1"  value="${scBankDevPartUserDtBean.mailId}" readonly="readonly"  style="width:200px;" />
                                            <input type="hidden" name="partnerId" value="${scBankDevPartUserDtBean.partnerId}"/>
                                            <input type="hidden" name="isAdmin" value="${scBankDevPartUserDtBean.isAdmin}"/>
                                            <input type="hidden" name="officeId" value="${scBankDevPartUserDtBean.sdBankDevId}"/>
                                            <input type="hidden" name="partnerId" value="${scBankDevPartUserDtBean.partnerId}"/>
                                            <input type="hidden" name="userId" value="${scBankDevPartUserDtBean.userId}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Full Name : <span>*</span></td>
                                        <td><input name="fullName" id="txtaFullName" type="text" value="${scBankDevPartUserDtBean.fullname}" class="formTxtBox_1" style="width:200px;" /></td>
                                    </tr>
                                    <%-- <tr>
                                         <td class="ff">Name in Bangla : </td>
                                         <td><input name="bngName" id="txtBngName" type="text"  class="formTxtBox_1" style="width:200px;" /></td>
                                     </tr>--%>
                                    <tr>
                                        <td class="ff">National Id : <span>*</span></td>
                                        <td><input name="ntnlId" id="txtNtnlId" type="text" class="formTxtBox_1" style="width:200px;"/></td>
                                    </tr>
                                    <%--<tr>
                                        <td class="ff">Phone No : <span>*</span></td>
                                        <td><input name="phoneNo" id="txtPhone" type="text" class="formTxtBox_1" style="width:200px;" /></td>
                                    </tr>--%>
                                    <tr>
                                        <td class="ff">Mobile No : <span>*</span></td>
                                        <td><input name="mobileNo" id="txtMob" type="text" value="${scBankDevPartUserDtBean.mobileNum}" class="formTxtBox_1" style="width:200px;" /></td>
                                    </tr>
                                    <%--<tr>
                                        <td class="ff">fax No : <span>*</span></td>
                                        <td><input name="faxNo" type="text" class="formTxtBox_1" id="txtFax" style="width:200px;" /></td>
                                    </tr>--%>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><label class="formBtn_1">
                                                <input type="submit" name="btnUpdate" id="btnUpdate" value="Update" />
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <!--Dashboard Content Part End-->
                </table>
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMngUser");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
