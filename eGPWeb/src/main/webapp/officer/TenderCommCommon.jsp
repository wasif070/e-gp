<%-- 
    Document   : TenderCommCommon
    Created on : Apr 3, 2016, 2:30:45 PM
    Author     : SRISTY
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SpgetCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TendererTabServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>

<!-- Dohatec Start -->
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalServiceForms"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.GrandSummaryService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPostQueConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderPostQueConfigService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory"%>
<!-- Dohatec End -->

<%
    response.setHeader("Expires", "-1");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");
    String dbOperation = "";
%>

<%
    String strCmnUserId = session.getAttribute("userId").toString();
    boolean isTenPackageWis = false;
    if (pageContext.getAttribute("isTenPackageWise") != null) {
        isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
    }

    TenderCommonService tenderCommonServiceCommon = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    CommonSearchService commonSearchServiceecc = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");

    boolean isTECCommitteeCreated = false;
    boolean isEditable = false;
    boolean isConfigEntry = false;
    boolean isTSCReq = false;
    boolean showMapping = false;
    boolean showViewMappping = false;
    boolean isCmnMultiLots = false;
    boolean cmnIsTenPublish = false;
    boolean isTORPORRptRecieved = false;
    //boolean isUserEvalMember = false;
    //boolean isUserEvalTSCMember = false;
    //boolean isEvalSignDone = false;
    boolean isopeningDate = false;
    //boolean isCmnConfigTeamWise = false;
    //boolean isCmnConfigIndWise = false;
    String userId_TEC_CP = "";
    String tender_status = "";
    String comTenderId = "0";
    String strCmnProcurementNature = "";
    String strCmnDocType = "";
    int intCmnEnvelopcnt = 0;

    if (request.getParameter("tenderid") != null) {
        comTenderId = request.getParameter("tenderid");
    } else if (request.getParameter("tenderId") != null) {
        comTenderId = request.getParameter("tenderId");
    }
    TenderService tenderServiceComm = (TenderService) AppContext.getSpringBean("TenderService");
    for (TblTenderDetails details : tenderServiceComm.getTenderDetails(Integer.parseInt(comTenderId))) {
        tender_status = details.getTenderStatus();
    }
    isopeningDate = tenderCommonServiceCommon.checkOpeningDate(Integer.parseInt(comTenderId));
    //List<SPTenderCommonData> lstChkEvalMember = tenderCommonServiceCommon.returndata("chkEvalMember", comTenderId, strCmnUserId);
    /*if (!lstChkEvalMember.isEmpty()) {
                if ("Yes".equalsIgnoreCase(lstChkEvalMember.get(0).getFieldName1())) {
                    isUserEvalMember = true;
                }
                if ("Yes".equalsIgnoreCase(lstChkEvalMember.get(0).getFieldName2())) {
                    isUserEvalTSCMember = true;
                }
                
                if ("Yes".equalsIgnoreCase(lstChkEvalMember.get(0).getFieldName3())) {
                    isEvalSignDone = true;
                }

                if ("team".equalsIgnoreCase(lstChkEvalMember.get(0).getFieldName4())) {
                    isCmnConfigTeamWise = true;
                } else  if ("ind".equalsIgnoreCase(lstChkEvalMember.get(0).getFieldName4())) {
                    isCmnConfigIndWise = true;
                }


            }*/

    //List<SPTenderCommonData> lstTEC_CPInfo = tenderCommonServiceCommon.returndata("EvalTECChairPerson_Info", comTenderId, "");
    /* String strComments = "";
            if (!lstTEC_CPInfo.isEmpty()) {
                userId_TEC_CP = lstTEC_CPInfo.get(0).getFieldName1();
                //userId_TEC_CP = lstTEC_CPInfo.get(0).getFieldName9();

                if ("yes".equalsIgnoreCase(lstTEC_CPInfo.get(0).getFieldName4())) {
                    isTECCommitteeCreated = true;
                    if ("yes".equalsIgnoreCase(lstTEC_CPInfo.get(0).getFieldName5()) || "yes".equalsIgnoreCase(lstTEC_CPInfo.get(0).getFieldName6())) {
                        isEditable = false;
                    } else {
                        isEditable = true;
                    }

                    if ("yes".equalsIgnoreCase(lstTEC_CPInfo.get(0).getFieldName7())) {
                        isTSCReq = true;
                    }

                    if ("yes".equalsIgnoreCase(lstTEC_CPInfo.get(0).getFieldName8())) {
                        isTORPORRptRecieved = true;
                    }
                }
            }
            lstTEC_CPInfo = null;*/
    CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");

    int uid = 0;
    if (session.getAttribute("userId") != null) {
        Integer ob1 = (Integer) session.getAttribute("userId");
        uid = ob1.intValue();
    }
    WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
    short eventid = 6;
    int objectid = 0;
    short activityid = 6;
    int childid = 1;
    short wflevel = 1;
    int initiator = 0;
    int approver = 0;
    boolean isInitiater = false;
    boolean evntexist = false;
    String fileOnHand = "No";
    String checkperole = "No";
    String chTender = "No";
    boolean ispublish = false;
    boolean iswfLevelExist = false;
    boolean isconApprove = false;
    String evalCommit = "";
    String objtenderId = comTenderId;
    if (objtenderId != null) {
        objectid = Integer.parseInt(objtenderId);
        childid = objectid;
    }
    String donor = "";
    donor = workFlowSrBean.isDonorReq(String.valueOf(eventid),
            Integer.valueOf(objectid));
    String[] norevs = donor.split("_");
    int norevrs = -1;

    String strrev = norevs[1];
    if (!strrev.equals("null")) {

        norevrs = Integer.parseInt(norevs[1]);
    }
    String rptStatus = "pending";
    EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
    List<Object[]> viewEvalRptList = evalService.forReprotProcessView(Integer.parseInt(comTenderId), -1);
    if (viewEvalRptList != null & !viewEvalRptList.isEmpty()) {
        rptStatus = viewEvalRptList.get(0)[0].toString();
    }
    evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
    List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
    // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);
    if (tblWorkFlowLevelConfig.size() > 0) {
        Iterator twflc = tblWorkFlowLevelConfig.iterator();
        iswfLevelExist = true;
        while (twflc.hasNext()) {
            TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
            TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
            if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                fileOnHand = workFlowlel.getFileOnHand();
            }
            if (workFlowlel.getWfRoleId() == 1) {
                initiator = lmaster.getUserId();
            }
            if (workFlowlel.getWfRoleId() == 2) {
                approver = lmaster.getUserId();
            }
            if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                isInitiater = true;
            }

        }

    }
    String userid = "";
    if (session.getAttribute("userId") != null) {
        Object obj = session.getAttribute("userId");
        userid = obj.toString();
    }
    String field1 = "CheckHOPE";
    List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, userid, "");

    if (editdata.size() > 0) {
        checkperole = "Yes";
    }

    // for publish link
    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    List<SPTenderCommonData> checkEval = tenderCommonService1.returndata("TenderComWorkFlowStatus", comTenderId, "'Pending'");

    if (checkEval.size() > 0) {
        chTender = "Yes";
    }
    List<SPTenderCommonData> publist = tenderCommonService1.returndata("TenderComWorkFlowStatus", comTenderId, "'Approved','Conditional Approval'");
    SPTenderCommonData spTenderCommondata = null;

    if (publist.size() > 0) {
        ispublish = true;
        spTenderCommondata = publist.get(0);
        evalCommit = spTenderCommondata.getFieldName1();
        String conap = spTenderCommondata.getFieldName2();
        if (conap.equalsIgnoreCase("Conditional Approval")) {
            isconApprove = true;
        }
    }

    // Get Tender Procurement Nature info
    /* START : CODE TO GET TENDER ENVELOPE COUNT */
 /*List<SPTenderCommonData> lstCmnEnvelops =
                    tenderCommonService1.returndata("getTenderEnvelopeCount", comTenderId, "0");

            if (!lstCmnEnvelops.isEmpty()) {
                if (Integer.parseInt(lstCmnEnvelops.get(0).getFieldName1()) > 0) {
                    intCmnEnvelopcnt = Integer.parseInt(lstCmnEnvelops.get(0).getFieldName1());
                }
                if ("Yes".equalsIgnoreCase(lstCmnEnvelops.get(0).getFieldName2())) {
                    isCmnMultiLots = true;
                }

                strCmnProcurementNature = lstCmnEnvelops.get(0).getFieldName3();
            }
            lstCmnEnvelops = null;*/
 /* END : CODE TO GET TENDER ENVELOPE COUNT */
    if ("Services".equalsIgnoreCase(strCmnProcurementNature)) {
        strCmnDocType = "Proposal";
    } else {
        strCmnDocType = "Tender";
    }

    List<SPTenderCommonData> cmnTenStatus
            = tenderCommonService1.returndata("Checktenderpublishstatus", comTenderId, null);

    if (!cmnTenStatus.isEmpty()) {
        if (cmnTenStatus.get(0).getFieldName1().equalsIgnoreCase("approved")) {
            cmnIsTenPublish = true;
        }
    }
    cmnTenStatus.clear();
    cmnTenStatus = null;
    String tempDataChk = null;
    if (!checkEval.isEmpty()) {
        tempDataChk = checkEval.get(0).getFieldName4();
    }
    if (!publist.isEmpty()) {
        tempDataChk = publist.get(0).getFieldName4();
    }

    String strCmnComType = "null";
    if (request.getParameter("comType") != null && !"null".equalsIgnoreCase(request.getParameter("comType"))) {
        strCmnComType = request.getParameter("comType");
    }

    TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights", Integer.toString(uid), comTenderId);
    List<SPTenderCommonData> chekTenderCreator = wfTenderCommonService.returndata("CheckTenderCreator", Integer.toString(uid),comTenderId);
%>

<table width="100%" cellspacing="0" class="tableList_1">
    <tr>
        <td width="30%" class="t-align-left ff">Tender Committee</td>
        <td width="80%" class="t-align-left">
            <%--<a href="TenderCommFormation.jsp?tenderid=<%=comTenderId%>&parentLink=736"> Create TC</a>--%>
            <%
                com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean tenderCommitteSrBean = new com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean();
                com.cptu.egp.eps.dao.storedprocedure.CommitteDtBean bean = tenderCommitteSrBean.findCommitteDetails(Integer.parseInt(comTenderId), "TenderCom");
                if (bean.getpNature() != null) {
                    if ((committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + comTenderId + " and tc.committeeType in ('TC', 'PC')") == 0)) {
                        if (!checkuserRights.isEmpty()  && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())) {%>
                            <a href="TenderCommFormation.jsp?tenderid=<%=comTenderId%>&parentLink=968">Create</a>&nbsp;
                            |&nbsp;<a href="MapCommittee.jsp?tenderid=<%=comTenderId%>&type=4&parentLink=969">Use Existing Committee</a>
                        <%}
                    } 
                    else if ((committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + comTenderId + " and tc.committeeType in ('TC', 'PC') and tc.committeStatus='pending'") == 0)) {%> 
                        <a href="TenderCommFormation.jsp?tenderid=<%=comTenderId%>&isview=y&parentLink=966">View</a>
                    <%} else {%>

            <%
                if (!checkuserRights.isEmpty()  && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())) {
                    //if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                    // || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true) || iswfLevelExist == false) {
                    if ("pending".equalsIgnoreCase(checkEval.get(0).getFieldName1())) {
            if(!"dump".equalsIgnoreCase(tempDataChk)){%>
            <a href="TenderCommFormation.jsp?tenderid=<%=comTenderId%>&isedit=y&parentLink=965">Edit</a>&nbsp;| &nbsp;
            <% } }
                }%>
            <% if (!checkuserRights.isEmpty()  && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())) {
                    if ("pending".equalsIgnoreCase(checkEval.get(0).getFieldName1())) {%>
            &nbsp;<a href="TenderCommFormation.jsp?tenderid=<%=comTenderId%>&ispub=y&parentLink=967">Notify Committee Members</a>&nbsp;| &nbsp;
            <%}
                }
            %>

            <!-- Previous Notify Committee link-->
            <%--<%
            if(!checkuserRights.isEmpty()){
            if ((!checkEval.isEmpty() || !publist.isEmpty())) {
                if ("dump".equalsIgnoreCase(tempDataChk)) {%>
            &nbsp;<a href="TenderCommFormation.jsp?tenderid=<%=comTenderId%>&ispub=y&parentLink=904">Notify Committee Members</a>&nbsp;| &nbsp;
            <%} /*For cheking checkEval is empty or not*/ 
                else if ((ispublish == true && isInitiater == true && !evalCommit.equalsIgnoreCase("Approved")) || (!evalCommit.equalsIgnoreCase("Approved") && isInitiater == true && (initiator == approver && norevrs == 0) && initiator != 0 && approver != 0)) {%>
            &nbsp;<a href="TenderCommFormation.jsp?tenderid=<%=comTenderId%>&ispub=y&parentLink=904">Notify Committee Members</a>&nbsp;| &nbsp;
            <% }%>
            <%}
            }%>--%>
            <a href="TenderCommFormation.jsp?tenderid=<%=comTenderId%>&isview=y&parentLink=966">View</a>
            <%
                    }
                } else {
                    out.print("<div class='responseMsg noticeMsg'>Business rule yet not configured</div>");
                }%>


        </td>
    </tr>
    <%
        List<SPTenderCommonData> getTenderCommPubDate
                = tenderCommonService1.returndata("getTenderCommPubDate", comTenderId, null);
        if (getTenderCommPubDate != null && !getTenderCommPubDate.isEmpty()) {
    %>
    <tr>
        <td width="30%" class="t-align-left ff">Date and time of Committee Formation</td>
        <td width="80%" class="t-align-left"><%=getTenderCommPubDate.get(0).getFieldName1()%></td>
    </tr>
    <%}%>
    <%--<%
        if ((!checkEval.isEmpty() || !publist.isEmpty())) {
            if (!"dump".equalsIgnoreCase(tempDataChk)) {%>
    <tr>
        <td width="30%" class="t-align-left ff">Workflow</td>
        <td width="80%" class="t-align-left">
            <% if (evntexist) {

                if (fileOnHand.equalsIgnoreCase("yes") && checkperole.equals("Yes") && ispublish == false && isInitiater == true) {

            %>
            <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=915'>Edit</a>

            &nbsp;|&nbsp;
            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=737">View</a> &nbsp;|&nbsp;

            <% } else if (iswfLevelExist == false && checkperole.equals("Yes") && ispublish == false) {
            %>

            <a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=915'>Edit</a>
            &nbsp;|&nbsp;
            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=737">View</a> &nbsp;|&nbsp;
            <%  } else if (iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {%>

            <label>Workflow yet not configured</label>
            <%                                          } else if (iswfLevelExist == true) {%>
            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=737">View</a>
            &nbsp;|&nbsp;
            <%
                 }
             } else if (ispublish == false && checkperole.equals("Yes")) {
                 List<CommonAppData> defaultconf = workFlowSrBean.editWorkFlowData("WorkFlowRuleEngine", Integer.toString(eventid), "");
                 if (defaultconf.size() > 0) {
            %>
            <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&parentLink=123">Create</a>

            <% } else {%>
            <a  href="#" onclick="checkdefaultconf()">Create</a>
            <%  }
               } else if (iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {%>
            <label>Workflow yet not configured</label>
            <% }%>
            <%
                                    if (isInitiater == true && (initiator != approver || (initiator == approver && norevrs > 0)) && ispublish == false && fileOnHand.equalsIgnoreCase("Yes")) {
            %>
            <% if (chTender.equalsIgnoreCase("Yes")) {%>
            <a href="FileProcessing.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&fromaction=eval" >Process file in Workflow</a>
            &nbsp;|&nbsp;
            <% }
                                                    }%>
            <% if (iswfLevelExist) {%>
            <a href="workFlowHistory.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&userid=<%=uid%>&fraction=eval&parentLink=81" >View Workflow History</a>
            <% }%>

        </td>

    </tr>
    <% }  }%> --%> 


</table>


<%
// Set To Null
    userId_TEC_CP = null;
    comTenderId = null;
%>

