<%-- 
    Document   : DomesticPrefFormula
    Created on : Oct 23, 2012, 11:13:28 AM
    Author     : akms
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />


<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Formula</title>
    </head>
    <body>
        <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1 t_space tableView_1">
           
              <tr>
               <td style="color:Red;">
                    Domestic preference calculated on the basis following formula :-
                    <ol style="padding-left:25px;">
                         <li style="margin:2px;">EXW price per Line Item= Quantity*(Unit price EXW- VAT for Goods manufactured in Bhutan
                                                                                                 [Nu.])
</li>
                        <li style="margin:2px;">Quoted Price (Without Domestic Preference) = (EXW price per Line Item +

                                 Inland transportation, Insurance and other local costs for the delivery of the Goods to their final destination [Nu.])</li>
                                                
                                                      
                    </ol>
               </td>
           </tr>
        </table>
    </body>
</html>
