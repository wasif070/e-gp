<%-- 
    Document   : PreTenderRule
    Created on : Dec 18, 2010, 5:51:01 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Pre Tender meeting rule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmPreTender").validate({
                    rules: {
                        cmbTenderType: {required: true},
                        cmbProcMethod: {required: true},
                        cmbProcType: {required: true},
                        cmbPreTenderMeeting:{required:true},
                        daysFromPublication:{required:true,digits:true},
                        postingOfQue:{required:true,digits:true},
                        daysAllow:{required:true,digits:true}


                    },
                    messages: {
                        cmbTenderType: { required: "<div class='reqF_1'> Please select tender type.</div>"},
                            
                        cmbProcMethod: { required: "<div class='reqF_1'>Please select procurement methods.</div>"},
                            
                        cmbProcType: { required: "<div class='reqF_1'>Please select procurement type.</div>"},

                        cmbPreTenderMeeting:{required:"<div class='reqF_1'>Please select pre tender meeting required option.</div>"},

                        daysFromPublication:{required:"<div class='reqF_1'>Please enter days from Publication when pre Bid meeting is scheduled.</div>",
                            digits:"<div class='reqF_1'>Allows numerals (0-9) only.</div>"},

                        postingOfQue:{required:"<div class='reqF_1'>Please enter Posting of questions allowed before no. of days from tender closing date.</div>",
                            digits:"<div class='reqF_1'>Allows numerals (0-9) only.</div>"},

                        daysAllow:{required:"<div class='reqF_1'>Please enter Days allowed for uploading Pre Bid meeting,after Pre Bid meeting is over.</div>",
                            digits:"<div class='reqF_1'>Allows numerals (0-9) only.</div>"}
                    }

                });

            });
        </script>

    </head>
    <body>
        <form id="frmPreTender" method="POST" action="">
            <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <div class="topHeader">
                    <table width="100%" cellspacing="0">
                        <tr valign="top">
                            <td><div id="ddtopmenubar" class="topMenu_1">
                                    <!--<div class="dash_menu_1">-->
                                    <ul>
                                        <li><a href="#"><img src="../resources/images/Dashboard/msgBoxIcn.png" />Message Box</a></li>
                                        <li><a href="#" rel="ddsubmenu1"><img src="../resources/images/Dashboard/configIcn.png" />Configuration</a></li>
                                        <li><a href="#" rel="ddsubmenu2"><img src="../resources/images/Dashboard/tenderIcn.png" />SBD</a></li>
                                        <li><a href="#"><img src="../resources/images/Dashboard/docLibIcn.png" />Content</a></li>
                                        <li><a href="#" rel="ddsubmenu3"><img src="../resources/images/Dashboard/committeeIcn.png" />Manage Users</a></li>
                                        <li><a href="#"><img src="../resources/images/Dashboard/reportIcn.png" />Report</a></li>
                                        <li><a href="#"><img src="../resources/images/Dashboard/myAccountIcn.png" />Profile</a></li>
                                    </ul>
                                </div>
                                <script type="text/javascript">
                                    ddlevelsmenu.setup("ddtopmenubar") //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
                                </script>
                                <!--Multilevel Menu 1 (rel="ddsubmenu1")-->
                                <ul id="ddsubmenu1" class="ddsubmenustyle">
                                    <li><a href="#">Financial Year Details </a>
                                        <ul>
                                            <li><a href="#">Sub Menu 1</a></li>
                                            <li><a href="#">Sub Menu 2</a></li>
                                            <li><a href="#">Sub Menu 3</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Configure calendar</a></li>
                                    <li><a href="#">Item 3</a></li>
                                </ul>


                                <!--Multilevel Menu 2 (rel="ddsubmenu2")-->
                                <ul id="ddsubmenu2" class="ddsubmenustyle">
                                    <li><a href="#">Item 1</a>
                                        <ul>
                                            <li><a href="#">Sub Menu 1</a></li>
                                            <li><a href="#">Sub Menu 2</a></li>
                                            <li><a href="#">Sub Menu 3</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Item 2</a></li>
                                    <li><a href="#">Item 3</a></li>
                                </ul>

                                <!--Multilevel Menu 3 (rel="ddsubmenu3")-->
                                <ul id="ddsubmenu3" class="ddsubmenustyle">
                                    <li><a href="#">Menu 1</a></li>
                                    <li><a href="#">Menu 2</a></li>
                                    <li><a href="#">Menu 3</a></li>
                                    <li><a href="#">Menu 4</a></li>
                                </ul>

                                <table width="100%" cellspacing="6" class="loginInfoBar">
                                    <tr>
                                        <td align="left"><b>Friday 27/08/2010 21:45</b></td>
                                        <td align="center"><b>Last Login :</b> Friday 27/08/2010 21:45</td>
                                        <td align="right"><img src="../resources/images/Dashboard/userIcn.png" class="linkIcon_1" /><b>Welcome,</b> User   &nbsp;|&nbsp; <img src="../resources/images/Dashboard/logoutIcn.png" class="linkIcon_1" alt="Logout" /><a href="#" title="Logout">Logout</a></td>
                                    </tr>
                                </table></td>
                            <td width="141"><img src="../resources/images/Dashboard/e-GP.gif" width="141" height="64" alt="e-GP" /></td>
                        </tr>
                    </table>
                </div>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <div class="pageHead_1">Pre Tender Meeting Rule</div>
                <div>&nbsp;</div>
                <div style="font-style: italic" class="t-align-left"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>
                <div>&nbsp;</div>
                <div align="right" class="t_space"><a href="#" class="action-button-add">Add Rule</a> <a href="#" class="action-button-delete no-margin">Remove Rule</a></div>
                <div>&nbsp;</div>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th class="t-align-center">Select</th>
                        <th class="t-align-center">Tender Type<br />(<span class="mandatory">*</span>)</th>
                        <th class="t-align-center">Procurement Method<br />(<span class="mandatory">*</span>)</th>
                        <th class="t-align-center">Procurement Type<br />(<span class="mandatory">*</span>)</th>
                        <th class="t-align-center">Pre Tender Meeting Required<br />(<span class="mandatory">*</span>)</th>
                        <th class="t-align-center">Days from Publication when pre Bid meeting is  scheduled<br />(<span class="mandatory">*</span>)</th>
                        <th class="t-align-center">Posting of questions allowed before no. of days from tender closing date<br />(<span class="mandatory">*</span>)</th>
                        <th class="t-align-center">Days allowed for uploading Pre Bid meeting, after  Pre Bid meeting is over<br />(<span class="mandatory">*</span>)</th>
                    </tr>
                    <tr>
                        <td class="t-align-center"><input type="checkbox" name="cmbTenderType" id="cmbTenderType"  value="" /></td>
                        <td class="t-align-center"><select name="select2" class="formTxtBox_1" id="select2">
                                <option>Value 1</option>
                                <option>Value 2</option>
                            </select></td>
                        <td class="t-align-center"><select name="cmbProcMethod" class="formTxtBox_1" id="cmbProcMethod">
                                <option>Value 1</option>
                                <option>Value 2</option>
                            </select></td>
                        <td class="t-align-center"><select name="cmbProcType" class="formTxtBox_1" id="cmbProcType">
                                <option>Value 1</option>
                                <option>Value 2</option>
                            </select></td>
                        <td class="t-align-center"><select name="cmbPreTenderMeeting" class="formTxtBox_1" id="cmbPreTenderMeeting">
                                <option>Value 1</option>
                                <option>Value 2</option>
                            </select></td>
                        <td class="t-align-center"><input name="daysFromPublication" type="text" class="formTxtBox_1" id="daysFromPublication" /></td>
                        <td class="t-align-center"><input name="postingOfQue" type="text" class="formTxtBox_1" id="postingOfQue" /></td>
                        <td class="t-align-center"><input name="daysAllow" type="text" class="formTxtBox_1" id="daysAllow" /></td>
                    </tr>
                    <tr>
                        <td colspan="8" class="t-align-center"><span class="formBtn_1">
                                <input type="submit" name="button2" id="button2" value="Submit" />
                            </span></td>
                    </tr>
                </table>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <table width="100%" cellspacing="0" class="footerCss">
                    <tr>
                        <td align="left">e-GP &copy; All Rights Reserved
                            <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
                        <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
                    </tr>
                </table>
                <!--Dashboard Footer End-->
            </div>

        </form>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
