<%--
    Document   : ViewDebarClarification
    Created on : Jan 9, 2011, 6:56:01 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentReq"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentDetails"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="java.rmi.server.Operation"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentDocs"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.CheckExtension"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.web.servicebean.InitDebarmentSrBean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Bidder's/Consultant's Debarment Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <style type="text/css">
            @import "<%=request.getContextPath()%>/resources/dojo/dijit/themes/tundra/tundra.css";
        </style>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/dojo/dojo/dojo.js" djConfig="parseOnLoad: true">
        </script>

        <script type="text/javascript">
            dojo.require("dijit.ProgressBar");

            function doProgress() {
                var button = window.document.getElementById("btnUpload");
                button.disabled = true;
                var max = 100;
                var prog = 0;
                var counter = 0;
                getProgress();
                doProgressLoop(prog, max, counter);
            }

            function doProgressLoop(prog, max, counter) {
                var x = dojo.byId('progress-content').innerHTML;
                var y = parseInt(x);
                if (!isNaN(y)) {
                    prog = y;
                }
                jsProgress.update({ maximum: max, progress: prog });
                counter = counter + 1;
                dojo.byId('counter').innerHTML = counter;
                if (prog < 100) {
                    setTimeout("getProgress()", 500);
                    setTimeout("doProgressLoop(" + prog + "," + max + "," + counter + ")", 1000);
                }
            }

            function getProgress() {
                dojo.xhrGet({url: '<%=request.getContextPath()%>/progress', // http://localhost:8080/file-upload/progress
                    load: function (data) { dojo.byId('progress-content').innerHTML = data; },
                    error: function (data) { dojo.byId('progress-content').innerHTML = "Error retrieving progress"; }
                });
            }

        </script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script type="text/javascript">
              function validate(){
                $(".err").remove();
                if($("#comments").val()=="") {
                    $("#comments").parent().append("<div class='err' style='color:red;'>Please enter Comments.</div>");
                    return false;
                }
              }
             $(function() {
                $('#debarStatus').change(function() {
                    if($('#debarStatus').val()=="pesatisfy"){
                     $('#hideTr').hide();
                    }else{
                      $('#hideTr').show();
                    }
                });
            });
            function validateFile(){
               var count = 0;
               $(".err").remove();               
                if($.trim($("#txtDescription").val())=="") {
                    $("#txtDescription").parent().append("<div class='err' style='color:red;'>Please enter Description.</div>");
                        count++;
                }
                if($("#uploadDocFile").val()=="") {
                    $("#uploadDocFile").parent().append("<div class='err' style='color:red;'>Please select File.</div>");
                        count++;
                }else{
                    var browserName=""
                    var maxSize = parseInt($('#fileSize').val())*1024*1024;
                    var actSize = 0;
                    var fileName = "";
                    jQuery.each(jQuery.browser, function(i, val) {
                         browserName+=i;
                    });
                    $(":input[type='file']").each(function(){
                        if(browserName.indexOf("mozilla", 0)!=-1){
                            actSize = this.files[0].size;
                            fileName = this.files[0].name;
                        }else{
                            var file = this;
                            var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                            var filepath = file.value;
                            var thefile = myFSO.getFile(filepath);
                            actSize = thefile.size;
                            fileName = thefile.name;
                        }
                        if(parseInt(actSize)==0){
                            $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                            count++;
                        }
                        if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                            $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                            count++;
                        }
                        if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                            $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                            count++;
                        }
                    });
                }
                if(count==0){
                    //$(form).submit();
                    doProgress();
                    return true;
                }else{
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <%
            Logger LOGGER = Logger.getLogger("ViewDebarClarification.jsp");
            String debarId = request.getParameter("debId");
            InitDebarmentSrBean srBean = new InitDebarmentSrBean();
            srBean.setLogUserId(session.getAttribute("userId").toString());
            String status = request.getParameter("stat");
            if("Submit".equalsIgnoreCase(request.getParameter("submit"))){
                srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));
                srBean.updateDebarByPE(debarId, request.getParameter("debarStatus"), request.getParameter("comments"), request.getParameter("hopeId"),request.getParameter("cmpName"),request.getParameter("uname"));
                response.sendRedirect("DebarmentListing.jsp?msg=hope&isprocess=y");
            }else if("Submit".equalsIgnoreCase(request.getParameter("submitDoc"))){
                srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));
                int q = srBean.updateDebarStatus(debarId,"pending",request.getParameter("lastRespDate"),session.getAttribute("userId").toString());
                if(q==1){
                    response.sendRedirect("DebarmentListing.jsp?tend=msg");
                }else{
                    out.print("<h1>Error in updating</h1>");
                }
           }else{
            long mainCnt = srBean.isResponseAvail(debarId);
            boolean respAvail=true;
            if(mainCnt!=0){
                respAvail=false;
            }
            Object[] objdata = null;
            SPTenderCommonData data = null;
            Object[] allIds=srBean.findPETenderHopeId(debarId);
            if(respAvail){
                try{
                    objdata = srBean.viewTendererDebarClari(debarId,status).get(0);
                }catch(Exception e){
                    LOGGER.error("Error in (objdata) ViewDebarClarification.jsp "+e);
                }
            }else{
                try{
                    data = srBean.viewTendererDebarClari(Integer.parseInt(debarId),status).get(0);
                }catch(Exception e){
                    LOGGER.error("Error in (data) ViewDebarClarification.jsp "+e);
                }
            }            
            List<TblDebarmentTypes> debarTypes = srBean.getAllDebars();
            List<Object[]> hopeAns = null;
            String data1 = null;
            String debarTypeId=null;
            String lastRespDate=null;
            int btnCount = 0;
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="pageHead_1"><%if(status.equals("withpe")){%>Send Clarification to Bidder/Consultant<%}else{%>View Debarment Clarification<%}%> <span style="float:right;"> <a class="action-button-goback" href="DebarmentListing.jsp<%if(!(status.equals("pending") || status.equals("withpe"))){out.print("?isprocess=y");}%>">Go back</a> </span></div>
        <%if(status.equals("withpe")){%>
        <br/><div style="color: red;">Note : Please ensure uploading reference documents if any before clicking on Submit to send your clarification request to Bidder/Consultant.</div>
        <%}%>
        <%
          if(request.getParameter("fq")!=null){out.print("<br/><div class='responseMsg errorMsg'>"+request.getParameter("fq")+"</div>");}
          if(request.getParameter("fs")!=null){out.print("<br/><div class='responseMsg errorMsg'>Max file size of a single file must not exceed "+request.getParameter("fs")+" MB.</div>");}
          if(request.getParameter("ft")!=null){out.print("<br/><div class='responseMsg errorMsg'>Acceptable file types "+request.getParameter("ft")+".</div>");}
          if(request.getParameter("sf")!=null){out.print("<br/><div class='responseMsg errorMsg'>File already exist</div>");}
        %>
        <form method="post" action="ViewDebarClarification.jsp?debId=<%=debarId%>" >
        <input type="hidden" value="<%if(respAvail){if(objdata[2].toString().equals("-")){out.print(objdata[3]+" "+objdata[4]);}else{out.print(objdata[2]);}}else{if(data.getFieldName4().equals("-")){out.print(data.getFieldName5()+" "+data.getFieldName6());}else{out.print(data.getFieldName4());}}%>" name="cmpName"/>
        <table class="tableList_1 t_space" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                </tr>
                <tr>

                    <td class="t-align-left ff" width="16%">Company Name :</td>
                    <td class="t-align-left" width="84%"><%if(respAvail){if(objdata[2].toString().equals("-")){out.print(objdata[3]+" "+objdata[4]);}else{out.print(objdata[2]);}}else{if(data.getFieldName4().equals("-")){out.print(data.getFieldName5()+" "+data.getFieldName6());}else{out.print(data.getFieldName4());}}%></td>
                </tr>
                <tr>

                    <td class="t-align-left ff" valign="top">Clarification :</td>
                    <td class="t-align-left" width="84%"><%if(respAvail){out.print(objdata[0]);}else{out.print(data.getFieldName1());}%></td>
                </tr>
                <!--For Listing in which Field Tenderer is Debared-->
                <tr id="trVal">
                    <td class="t-align-left ff">Debarment Type :</td>
                    <td class="t-align-left">
                    <%
                        if(!allIds[2].toString().equals("0")){
                            hopeAns = srBean.findHopeAns(debarId);
                        }else{
                           data1 = allIds[3].toString();
                        }
                        for (TblDebarmentTypes types : debarTypes) {
                            if(hopeAns==null){
                                 if(Integer.parseInt(data1)==types.getDebarTypeId()){
                                     debarTypeId = data1;
                                     out.print(types.getDebarType());
                                 }
                            }else{
                                if(Integer.parseInt(hopeAns.get(0)[4].toString())==types.getDebarTypeId()){
                                 debarTypeId = hopeAns.get(0)[4].toString();
                                 out.print(types.getDebarType());
                                }
                            }

                         }
                    %>
                    </td>
                </tr>
                <%if(!"6".equals(debarTypeId)){%>
                <tr id="trVal">
                <td class="t-align-left ff">&nbsp;</td>
                <td class="t-align-left">
                <%
                                String hopePEId = null;
                                String condText=null;
                                if(allIds[2].toString().equals("0")){
                                    condText="bype";
                                    hopePEId = allIds[1].toString();
                                }else{
                                    condText="byhope";
                                    hopePEId = allIds[2].toString();
                                }
                                List<TblDebarmentDetails> dataList = srBean.getDebarDetailsForHope("tblDebarmentReq",Operation_enum.EQ,new TblDebarmentReq(Integer.parseInt(debarId)),"debarStatus",Operation_enum.EQ,condText);
                                String[] debarIds = dataList.get(0).getDebarIds().split(",");
                                String pageHead1=null;
                                String pageHead2=null;
                                boolean isTenderDeb = false;
                                if(!allIds[2].toString().equals("0")){
                                    if(hopeAns.get(0)[4].toString().equals("1")){
                                        pageHead1="Ref No.";
                                        pageHead2="Tender/Proposal Brief";
                                        isTenderDeb = true;
                                    }else if(hopeAns.get(0)[4].toString().equals("2")){
                                        pageHead1="Letter Ref. No.";
                                        pageHead2="Package No.";
                                    }else if(hopeAns.get(0)[4].toString().equals("3")){
                                        pageHead1="Project Name";
                                        pageHead2="Project Code";
                                    }else if(hopeAns.get(0)[4].toString().equals("4")){
                                        pageHead1="PA Office Name";
                                        pageHead2="PA Code";
                                    }else if(hopeAns.get(0)[4].toString().equals("5")){
                                        pageHead1="Department Name";
                                        pageHead2="Department Type";
                                    }
                                }else{
                                    if(data1.equals("1")){
                                        pageHead1="Ref No.";
                                        pageHead2="Tender/Proposal Brief";
                                        isTenderDeb = true;
                                    }else if(data1.equals("2")){
                                        pageHead1="Letter Ref. No.";
                                        pageHead2="Package No.";
                                    }else if(data1.equals("3")){
                                        pageHead1="Project Name";
                                        pageHead2="Project Code";
                                    }else if(data1.equals("4")){
                                        pageHead1="PA Office Name";
                                        pageHead2="PA Code";
                                    }else if(data1.equals("5")){
                                        pageHead1="Department Name";
                                        pageHead2="Department Type";
                                    }
                                }
                            %>
                            <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                                <tr>
                                    <%if(isTenderDeb){%>
                                    <th class='t-align-center'>Tender/Proposal ID</th>
                                    <%}%>
                                    <th class='t-align-center' width='30%'><label id='dlbHead1'><%=pageHead1%></label></th>
                                    <th class='t-align-center' width='50%'><label id='dlbHead2'><%=pageHead2%></label></th>
                                </tr>
                                <tbody id="tbodyVal">
                                    <%
                                        List<SPTenderCommonData> list = srBean.searchDataForDebarType(debarTypeId,hopePEId);
                                        for(int j=0; j<debarIds.length;j++){
                                            for(SPTenderCommonData sptcd : list){
                                                if(sptcd.getFieldName1().equals(debarIds[j])){
                                                    out.print("<tr>");
                                                    String viewLink = null;
                                                    if(isTenderDeb){
                                                        out.print("<td>"+sptcd.getFieldName1()+"</td>");
                                                        viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + sptcd.getFieldName1() +"', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'>" + sptcd.getFieldName3() + "</a>";
                                                    }else{
                                                        viewLink = sptcd.getFieldName3();
                                                    }
                                                    out.print("<td><input type='hidden' value='"+sptcd.getFieldName1()+"' id='debId"+j+"'/>"+sptcd.getFieldName2()+"</td><td>"+viewLink+"</td>");
                                                    out.print("</tr>");
                                                }
                                            }
                                        }
                                    %>
                                </tbody>
                            </table>
                        </td>
                 </tr>
                 <%}%>
                <tr>
                    <td class="t-align-left ff">Last  Date for Response :</td>
                    <td class="t-align-left"><%if(respAvail){lastRespDate=DateUtils.formatStdDate((Date)objdata[1]);out.print(DateUtils.formatStdDate((Date)objdata[1]));}else{lastRespDate=data.getFieldName2();out.print(data.getFieldName2());}%></td>
                </tr>
                <%
                boolean ResponseTimeElapsed = false;
                String HopeName = "",HopeUserID="",CommentsFromOfficer="";
                 if(respAvail){
                    try{
                        InitDebarmentSrBean srBean_nr = new InitDebarmentSrBean();
                        srBean_nr.setLogUserId(session.getAttribute("userId").toString());
                        List<SPTenderCommonData> datalst = srBean_nr.viewTendererDebarClari(Integer.parseInt(debarId),"tendererNotResponded");
                        for (SPTenderCommonData stcd : datalst)
                            {
                            System.out.println("stcd.getFieldName1  " + stcd.getFieldName1());
                             if (stcd.getFieldName1().equalsIgnoreCase("Yes")) { ResponseTimeElapsed=true; 
                             HopeName = stcd.getFieldName2() ;
                             HopeUserID = stcd.getFieldName3();
                             CommentsFromOfficer = stcd.getFieldName4();
                             }
                        }
                  
                }catch(Exception e){
                    LOGGER.error("Error in (data_nr) ViewDebarClarification.jsp "+e);
                }}
                
                %>
                <%if(!respAvail){%>
                 <%
                        List<TblDebarmentDocs>  tenderDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(allIds[0].toString()));
                        if(!tenderDocses.isEmpty()){
                  %>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5" class="t-align-left">Reference documents by Bidder/Consultant </th>
                            </tr>
                            <tr>
                                <th class="t-align-center">Sl. No.</th>
                                <th class="t-align-center">Document Name</th>
                                <th class="t-align-center">Document Description</th>
                                <th class="t-align-center">File Size (In KB)</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        int count = 1;
                                        for (TblDebarmentDocs ttcd : tenderDocses) {
                            %>
                            <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                                <td class="t-align-center"><%=count%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                                <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                                <td class="t-align-center">
                                    <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=tend&stat=<%=status%>&userId=<%=allIds[0]%>&uTid=2" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                </td>
                            </tr>
                            <%count++;}%>
                    </table>
                    </td>
                </tr>
                <%}%>
                  <%
                        if(!allIds[2].equals("0")){
                        List<TblDebarmentDocs>  hopeDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(allIds[2].toString()));
                        if(!hopeDocses.isEmpty()){
                  %>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5" class="t-align-left">HOPA Documents</th>
                            </tr>
                            <tr>
                                <th class="t-align-center">Sl. No.</th>
                                <th class="t-align-center">Document Name</th>
                                <th class="t-align-center">Document Description</th>
                                <th class="t-align-center">File Size (In KB)</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        int count = 1;
                                        for (TblDebarmentDocs ttcd : hopeDocses) {
                            %>
                            <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                                <td class="t-align-center"><%=count%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                                <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                                <td class="t-align-center">
                                    <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=tend&stat=<%=status%>&userId=<%=allIds[2]%>&uTid=3" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                </td>
                            </tr>
                            <%count++;}%>
                    </table>
                    </td>
                </tr>
                <%}}%>
                <tr>
                    <td class="t-align-left ff" valign="top">Response :</td>
                    <td class="t-align-left"><%=data.getFieldName3()%></td>
                </tr>
                <tr>
                    <td class="t-align-left ff" valign="top">Action :</td>
                    <td class="t-align-left">
                        <%if(status.equals("pending")){%>
                        <select name="debarStatus" class="formTxtBox_1" id="debarStatus" style="width: 120px;">
                            <option value="pesatisfy">Satisfactory</option>
                            <option value="sendtohope">Un Satisfactory</option>
                        </select>
                        <%}else{if(status.equals("pesatisfy")){out.print("Satisfactory");}else{out.print("Un Satisfactory");}}%>
                    </td>
                </tr>
                <tr>
                    <td class="t-align-left ff" valign="top">
                    Comments : <%if(status.equals("pending")){out.print("<span class='mandatory'>*</span>");}%>
                    </td>
                    <td class="t-align-left">
                        <%if(status.equals("pending")){%>
                        <textarea rows="5" id="comments" name="comments" class="formTxtBox_1" style="width: 99%;"></textarea>
                        <%}else{%>
                        <%=data.getFieldName9()%>
                        <%}%>
                    </td>
                </tr>
                <tr <%if(!status.equals("sendtohope")){%>style="display: none;"<%}%> id="hideTr">
                    <td class="t-align-left ff" valign="top">
                    Send To :
                    </td>
                    <td class="t-align-left">                    
                    <%=data.getFieldName7()%>
                    <input type="hidden" value="<%=data.getFieldName8()%>" name="hopeId"/>
                    </td>
                </tr>
                <%}%>

                <%if(ResponseTimeElapsed){%>
                 <%
                        if(!allIds[2].equals("0")){
                        List<TblDebarmentDocs>  hopeDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(allIds[2].toString()));
                        if(!hopeDocses.isEmpty()){
                  %>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5" class="t-align-left">HOPA Documents</th>
                            </tr>
                            <tr>
                                <th class="t-align-center">Sl. No.</th>
                                <th class="t-align-center">Document Name</th>
                                <th class="t-align-center">Document Description</th>
                                <th class="t-align-center">File Size (In KB)</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        int count = 1;
                                        for (TblDebarmentDocs ttcd : hopeDocses) {
                            %>
                            <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                                <td class="t-align-center"><%=count%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                                <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                                <td class="t-align-center">
                                    <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=tend&stat=<%=status%>&userId=<%=allIds[2]%>&uTid=3" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                </td>
                            </tr>
                            <%count++;}%>
                    </table>
                    </td>
                </tr>
                <%}}%>

                <tr>
                    <td class="t-align-left ff" valign="top">Action :</td>
                    <td class="t-align-left">
                        <%if(status.equals("pending")){%>
                        <select name="debarStatus" class="formTxtBox_1" id="debarStatus" style="width: 120px;">
                            <option value="sendtohope">Un Satisfactory</option>
                        </select>
                        <%}else{out.print("Un Satisfactory");}%>
                    </td>
                </tr>
                <tr>
                    <td class="t-align-left ff" valign="top">
                    Comments : <%if(status.equals("pending")){out.print("<span class='mandatory'>*</span>");}%>
                    </td>
                    <td class="t-align-left">
                        <%if(status.equals("pending")){%>
                        <textarea rows="5" id="comments" name="comments" class="formTxtBox_1" style="width: 99%;"></textarea>
                        <%}else{%>
                        <%=CommentsFromOfficer%>
                        <%}%>
                    </td>
                </tr>
                <tr  id="hideTr">
                    <td class="t-align-left ff" valign="top">
                    Send To :
                    </td>
                    <td class="t-align-left">
                    <%=HopeName%>
                    <input type="hidden" value="<%=HopeUserID%>" name="hopeId"/>
                    </td>
                </tr>
                <%}%>
            </tbody>
        </table>
         <%if(status.equals("pending")){%>
         <%if(!respAvail){btnCount++;%>
        <div class="t-align-center t_space">
            <input type='hidden' name='uname' value="<%=objUserName%>"/>
            <label class="formBtn_1"><input name="submit" id="button3" value="Submit" type="submit" onclick="return validate();"></label>
        </div>
         <%}if(ResponseTimeElapsed){btnCount++;%>
        <div class="t-align-center t_space">
            <input type='hidden' name='uname' value="<%=objUserName%>"/>
            <label class="formBtn_1"><input name="submit" id="button3" value="Submit" type="submit" onclick="return validate();"></label>
        </div>
        <%}}%>
        </form>
            <%
                if("withpe".equals(status)){
                CheckExtension ext = new CheckExtension();
                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("officer");
                byte fileSize = configurationMaster.getFileSize();
            %>
            <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/InitDebarment?action=debarDocUpload&debId=<%=debarId%>&user=pe&stat=<%=status%>" enctype="multipart/form-data" name="frmUploadDoc" onsubmit="return validateFile();">
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td class="ff" width="15%"> Select Document   : <span class="mandatory">*</span></td>
                        <td width="85%">
                            <input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:450px; background:none;"/><br/>
                            Acceptable File Types <span class="formNoteTxt">(<%out.print(configurationMaster.getAllowedExtension().replace(",", ", "));%>)</span>
                            <br/>Maximum file size of single file should not exceed <%out.print(fileSize);%>MB.
                            <input type="hidden" value="<%=fileSize%>" id="fileSize"/>
                            <div id="dvUploadFileErMsg" class='reqF_1'></div>
                            <div class="tundra">
                            <div dojoType="dijit.ProgressBar" style="width: 300px" jsId="jsProgress" id="downloadProgress">
                            </div>
                            </div>
                            <div style="display:none;">
                                Content from Progress Servlet: <span id="progress-content">---</span><br/>
                                Counter: <span id="counter">---</span><br/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Description : <span class="mandatory">*</span></td>
                        <td>
                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="txtDescription" style="width:200px;"/>
                            <div id="dvDescpErMsg" class='reqF_1'></div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <label class="formBtn_1" style="visibility:visible;" id="lbUpload">
                                <input type="submit" name="upload" id="btnUpload" value="Upload"/>
                            </label>
                        </td>
                    </tr>
                </table>
            </form>
            <%
                }
                List<TblDebarmentDocs>  debarmentDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(session.getAttribute("userId").toString()));
                if(!debarmentDocses.isEmpty()){
            %>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th colspan="5" class="t-align-left">Reference documents by PA</th>
                    </tr>
                    <tr>
                        <th class="t-align-center">Sl. No.</th>
                        <th class="t-align-center">Document Name</th>
                        <th class="t-align-center">Document Description</th>
                        <th class="t-align-center">File Size (In KB)</th>
                        <th class="t-align-center">Action</th>
                    </tr>
                    <%
                                int count = 1;
                                for (TblDebarmentDocs ttcd : debarmentDocses) {
                    %>
                    <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                        <td class="t-align-center"><%=count%></td>
                        <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                        <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                        <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=pe&stat=<%=status%>&userId=<%=session.getAttribute("userId")%>&uTid=3" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                            <%if("withpe".equals(status)){%>
                            &nbsp;
                            <a href="<%=request.getContextPath()%>/InitDebarment?docId=<%=ttcd.getDebarDocId()%>&docName=<%=ttcd.getDocumentName()%>&action=debarDocDelete&debId=<%=debarId%>&user=pe&stat=<%=status%>&userId=<%=session.getAttribute("userId")%>" title="Remove"><img src="../resources/images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" /></a>
                            <%}%>
                        </td>
                    </tr>
                    <%count++;}%>
            </table>
           <%}%>
           <%
                if("withpe".equals(status)){
                    btnCount++;
                    out.print("<form method='post'><input type='hidden' name='uname' value='"+objUserName+"'><input type='hidden' value='"+lastRespDate+"' name='lastRespDate'/><br/><div align='center'><label class='formBtn_1'><input type='submit' name='submitDoc' value='Submit'/></label></div></form>");
                }
           %>
        <%
            if(false){//status.equals("sendtocom")
            List<Object[]> comList = srBean.isCommitteeExist(debarId);
        %>
        <table width="100%" cellspacing="0" class="tableList_1">
            <tr>
                 <td width="30%" class="t-align-left ff">Debarment Committee</td>
                 <td width="80%" class="t-align-left">
                     <%
                        if(comList.isEmpty()){
                            out.print("<a href='DebarmentCommittee.jsp?debId="+debarId+"'>Create</a>");
                        }else{
                            if(comList.get(0)[1].toString().equalsIgnoreCase("pending")){
                                out.print("<a href='DebarmentCommittee.jsp?debId="+debarId+"&comId="+comList.get(0)[0]+"&cName="+comList.get(0)[2]+"&isedit=y'>Edit</a>");
                                out.print("&nbsp;|&nbsp;");
                                out.print("<a href='DebarmentCommittee.jsp?debId="+debarId+"&comId="+comList.get(0)[0]+"&cName="+comList.get(0)[2]+"&isview=y'>View</a>");
                                out.print("&nbsp;|&nbsp;");
                                out.print("<a href='DebarmentCommittee.jsp?debId="+debarId+"&comId="+comList.get(0)[0]+"&cName="+comList.get(0)[2]+"&ispub=y'>Publish</a>");
                            }
                            if(comList.get(0)[1].toString().equalsIgnoreCase("published")){
                                out.print("<a href='DebarmentCommittee.jsp?debId="+debarId+"&comId="+comList.get(0)[0]+"&cName="+comList.get(0)[2]+"&isview=y'>View</a>");
                            }
                        }
                     %>
                 </td>
            </tr>
        </table>       
        <%}%>
         <%
            if(btnCount==0){
                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(session.getAttribute("userId").toString()), "userId", EgpModule.Debarment.getName(), "Debarment Clarification View by PA", "");
            }
        %>
        <%@include file="../resources/common/Bottom.jsp" %>
        <%}%>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>