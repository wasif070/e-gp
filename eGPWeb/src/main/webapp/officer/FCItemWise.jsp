<%-- 
    Document   : FCItemWise
    Created on : Apr 26, 2016, 11:12:28 AM
    Author     : NAHID
--%>

<%@page import="com.cptu.egp.eps.model.table.TblTenderFrameWork"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblAppPackages"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblAppFrameworkOffice"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.AbstractList"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Framework Contract Office Wise Item</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>

    </head>
    <jsp:useBean id="appServlet" scope="request" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    
    <body>
        <%
            
            pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
            pageContext.setAttribute("tab", "2");
            TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
            List<TblTenderLotSecurity> lots = null;
            int tendid = 0;
            int pckgNo = 0;
            List<TblTenderDetails> tenderDetails = new ArrayList<TblTenderDetails>();
            if (request.getParameter("tenderid") != null) {
                tendid = Integer.parseInt(request.getParameter("tenderid"));
                lots = tenderDocumentSrBean.getLotDetails(tendid);
                tenderDetails = tenderSrBean.getTenderDetails(tendid);
            }
            /*if("Use STD".equalsIgnoreCase(request.getParameter("submit"))){
            tenderDocumentSrBean.dumpSTD(String.valueOf(tendid), request.getParameter("txtStdTemplate"), session.getAttribute("userId").toString());
            response.sendRedirect("LotDetails.jsp?tenderid="+tendid);
            }*/
            java.util.List<TblAppFrameworkOffice> appListFrameworkOfcs = new java.util.ArrayList<TblAppFrameworkOffice>();
            TenderSrBean tSrBean = new TenderSrBean();
            TblTenderMaster tenderMaster = tSrBean.getTenderMasterData(tendid);
            int pkgId = tenderMaster.getTblAppPackages().getPackageId();
            //String pkgNo = tenderDetails.get(0).getPackageNo();
            //java.util.List<TblAppPackages> appListDtBean = new java.util.ArrayList<TblAppPackages>();
            //appListDtBean = appServlet.getAPPDetailsByPackageNo(pkgNo);
            List<TblTenderFrameWork> tenderFramework = new ArrayList<TblTenderFrameWork>();
            if (tenderDetails.size() > 0 && tenderDetails.get(0).getProcurementMethodId() == 18) {
                appListFrameworkOfcs = tSrBean.getAppFrameworkOfficeByPkgId(pkgId);
                if ("Save".equalsIgnoreCase(request.getParameter("btnSave"))){
                    tenderSrBean.addTblTenderFrameWork(tenderMaster.getAppId(), pkgId, tendid, request.getParameterValues("officeid"), request.getParameterValues("officeName"), request.getParameterValues("officeCity"), request.getParameterValues("officeAddress"), request.getParameterValues("item"), request.getParameterValues("quantity"), request.getParameterValues("unit"), Integer.parseInt(session.getAttribute("userId").toString()), true);
                }
                tenderFramework = tenderSrBean.findTblTenderFrameWork(tendid);
            }
            int c = 0;
        %>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <form method="POST" id="frmItemWise" action="FCItemWise.jsp?tenderid=6476">
                    <div class="contentArea_1">
                        <div class="pageHead_1">Framework Contract Office Wise Item
                            <span class="c-alignment-right"><a href="TenderDashboard.jsp?tenderid=<%=tendid%>" class="action-button-goback">Go back to Tender Dashboard</a></span>
                        </div>
                        <div class="tableHead_1 t_space">Tender Detail</div>
                        <table id="peOfcList" width="100%" cellspacing="0" class="tableList_1 t_space">
                            <thead>
                                <tr>
                                    <th width="10%" class="t-align-center">Select All <input type="checkbox" onclick="checkUncheck('map', this);" id="mapallchk"/></th>
                                    <th class="t-align-left" width="30%">PE Office Name</th>
                                    <th class="t-align-left" width="20%">Dzongkhag/District</th>
                                    <th class="t-align-left" width="40%">Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    int i = 0;
                                    for (TblAppFrameworkOffice appFrameworkOfc : appListFrameworkOfcs) {

                                %>
                                <tr>
                                    <td class="t-align-center"><input name="officeid" type="checkbox" value="<%=appFrameworkOfc.getFrameWorkOfficeId()%>" id="chkUnmap<%=i + 1%>" chk="map"/></td>
                                    <td class="t-align-left"><%=appFrameworkOfc.getOfficeName()%><input type="hidden" name="officeName" id="officeName" value="<%=appFrameworkOfc.getOfficeName()%>" /></td>
                                    <td class="t-align-left"><%=appFrameworkOfc.getState()%><input type="hidden" name="officeCity" id="officeCity" value="<%=appFrameworkOfc.getState()%>" /></td>
                                    <td class="t-align-left"><%=appFrameworkOfc.getAddress()%><input type="hidden" name="officeAddress" id="officeAddress" value="<%=appFrameworkOfc.getAddress()%>" /></td>
                                </tr>
                                <%
                                        i++;
                                    }
                                %>
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                            <td class="ff">Item :</td>
                            <td><input name="Item" type="text" class="formTxtBox_1" id="txtItem" style="width:200px;" value=""/></td>
                            <td class="ff">Unit :</td>
                            <td><input name="Unit" type="text" class="formTxtBox_1" id="txtUnit" style="width:200px;" value=""/></td>
                            <td class="ff">Quantity :</td>
                            <td><input name="Quantity" type="text" class="formTxtBox_1" id="txtQuantity" style="width:200px;" value=""/></td>
                            <td><label class="formBtn_1"><input type="button" name="add" id="btnAdd" value="Add" onClick="AddPeOfcToList();"/></label></td>                          
                        </table>
                        <span id="msgPeOfcList" class="reqF_2"></span>
                        <table id="pdOfcItemList" width="100%" cellspacing="0" class="tableList_1 t_space">
                            <thead>
                                <tr>
                                    <th width="10%" class="t-align-center">Select All <input type="checkbox" onclick="checkUncheckUnmap('map', this);" id="unmapallchk"/></th>
                                    <th class="t-align-left" width="25%">PE Office Name</th>
                                    <th class="t-align-left" width="15%">Dzongkhag/District</th>
                                    <th class="t-align-left" width="35%">Address</th>
                                    <th class="t-align-left" width="5%">Item</th>
                                    <th class="t-align-left" width="5%">Unit</th>
                                    <th class="t-align-left" width="5%">Quantity</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <label class="formBtn_1"><input type="button" name="ClearTbl" id="btnClear" value="Remove" onClick="RemovePeOfcFromList();"/></label>
                        <label class="formBtn_1"><input type="Submit" name="btnSave" id="btnSave" value="Save" /></label>
                        <span id="msgFCPEOfc" class="reqF_2"></span>
                    </div>
                </form>
                <%@include file="../resources/common/Bottom.jsp" %>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        function checkUncheck(val, chkall) {
            $("#peOfcList tbody tr td input:checkbox[chk='" + val + "']").each(function () {
                if ($(chkall).attr('checked')) {
                    $(this).attr('checked', true);
                } else {
                    $(this).removeAttr('checked');
                }
            });
        }
        function checkUncheckUnmap(val, chkall) {
            $("#pdOfcItemList tbody tr td input:checkbox[chk='" + val + "']").each(function () {
                if ($(chkall).attr('checked')) {
                    $(this).attr('checked', true);
                } else {
                    $(this).removeAttr('checked');
                }
            });
        }
        function AddPeOfcToList() {
            if($("#txtItem").val()=="" || $("#txtUnit").val()=="" || $("#txtQuantity").val()==""){
                $('#msgPeOfcList').html('<br/>Please enter item');
                return;
            }
            var flag = false;
            $("#peOfcList tbody tr td input[id^='chkUnmap']").each(function () {
                if ($(this).attr('checked')) {
                    var row = $(this).closest('tr').clone();
                    row.append("<td>" + $("#txtItem").val() + "<input type='hidden' name='item' id='item' value='" + $("#txtItem").val() + "' /></td>");
                    row.append("<td>" + $("#txtUnit").val() + "<input type='hidden' name='unit' id='unit' value='" + $("#txtUnit").val() + "' /></td>");
                    row.append("<td>" + $("#txtQuantity").val() + "<input type='hidden' name='quantity' id='quantity' value='" + $("#txtQuantity").val() + "' /></td>");
                    $('#pdOfcItemList tbody').append(row);
                    flag = true;
                }
            });
            if (flag) {
                $('#msgPeOfcList').html('');
                $("#peOfcList tbody tr td input[id^='chkUnmap']").each(function () {
                    $(this).removeAttr('checked');
                });
                $("#mapallchk").removeAttr('checked');
            }
            else{
                $('#msgPeOfcList').html('<br/>Please select PE Office');
            }
        }
        function RemovePeOfcFromList() {
            $("#pdOfcItemList tbody tr td input[id^='chkUnmap']").each(function () {
                if ($(this).attr('checked')) {
                    $(this).closest('tr').remove();
                }
            });
            $("#unmapallchk").removeAttr('checked');
        }
        function validateForm() {
            var flag1 = true;
            var rowCount = $('#pdOfcItemList tbody tr').length;
            if (rowCount < 1) {
                $('#msgFCPEOfc').html('<br/>Please add PE office');
                flag1 = false;
            }
            return flag1;
        }
        $('#btnSave').click(function (e) {
            if(!validateForm()){
                e.preventDefault();
            }
        });

    </script>


    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
