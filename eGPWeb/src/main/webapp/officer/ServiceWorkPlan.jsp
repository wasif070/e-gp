<%-- 
    Document   : ServiceWorkPlan
    Created on : Dec 5, 2011, 11:32:20 AM
    Author     : shreyansh.shah
--%>
<%@page import="java.util.ResourceBundle" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <% ResourceBundle srbd = null;
                    srbd = ResourceBundle.getBundle("properties.cmsproperty");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=srbd.getString("CMS.service.workPlan.title")%></title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script language="javascript">
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function getDate(date)
            {
                var splitedDate = date.split("-");
                return Date.parse(splitedDate[1]+" "+splitedDate[0]+", "+splitedDate[2]);
            }
            function checkDate(dateofdlvry,id){
                var group = document.getElementById("group_"+id).value;
                var firstDate = getDate(document.getElementById("firstdate").value);
                var secondDate = getDate(document.getElementById("seconddate").value);
                var dod = getDate(document.getElementById(dateofdlvry).value);
                if((new Date(firstDate).getTime() <= new Date(dod).getTime()) && (new Date(dod).getTime() <=  new Date(secondDate).getTime())){
                    checkDiff(dod,id);
                }else{
                    jAlert("<%=srbd.getString("CMS.works.edidatesByTendererandPe")%> ","CMS", function(RetVal) {
                    });
                    document.getElementById(dateofdlvry).value="";
                }

            }
            function checkDiff(values,id){
                var edate = document.getElementById("edate_"+id).value;
                var sdate = document.getElementById("sdate_"+id).value;
                if(edate!=""){

                    var endDate = getDate(edate);
                    var sDate=getDate(sdate);

                    if(new Date(sDate).getTime() > new Date(endDate).getTime()){
                        jAlert("<%=srbd.getString("CMS.var.sdateenddatecheck")%>","Service WorkPlan", function(RetVal) {
                        });
                        document.getElementById("edate_"+id).value="";
                    }

                }
            }
            //            function checkqty(id){
            //                $('.err').remove();
            //                if(!regForNumber(document.getElementById('Qtyenter_'+id).value) || document.getElementById('Qtyenter_'+id).value.split(".")[1].length > 3 || document.getElementById('Qtyenter_'+id).value.split(".")[0].length > 12){
            //                    $("#Qtyenter_"+id).parent().append("<div class='err' style='color:red;'><%=srbd.getString("CMS.var.ratecheck")%></div>");
            //                    document.getElementById('Qtyenter_'+id).value="";
            //
            //                }
            //                calculateGrandTotal();
            //            }
            function positive(value)
            {
                return /^\d*$/.test(value);

            }
            function checkDays (id){
                $('.err').remove();
                var values = document.getElementById('noOfDays_'+id).value;
                if(!regForNumber(document.getElementById('noOfDays_'+id).value)){
                    $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Days must be numeric value </div>");
                    document.getElementById('noOfDays_'+id).value="";
                }else if(document.getElementById('noOfDays_'+id).value.indexOf("-")!=-1){
                    $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Days must be positive value </div>");
                    document.getElementById('noOfDays_'+id).value="";
                }else if(!positive(document.getElementById('noOfDays_'+id).value)){
                    $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Decimal values are not allowed</div>");
                    document.getElementById('noOfDays_'+id).value="";
                }else if(document.getElementById('noOfDays_'+id).value.length > 5){
                    $("#noOfDays_"+id).parent().append("<div class='err' style='color:red;'>Maximum 5 digits are allowed </div>");
                    document.getElementById('noOfDays_'+id).value="";
                }

            }

            function f_date(id){
                var firstDate = getDate(document.getElementById("startDt_"+id).value);
                var values = document.getElementById("noOfDays_"+id).value;
                var enddate = new Date(firstDate).getTime()+(parseInt(values)*24*60*60*1000);
                var finalenddate="";
                var monthname = new Array("Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec");
                var enddate = new Date(enddate);
                finalenddate = enddate.getDate()+"-"+monthname[enddate.getMonth()]+"-"+enddate.getFullYear();
                if(values != "")
                {
                    document.getElementById("endDt_"+id).value = finalenddate;
                    return finalenddate;
                }
            }

            function GetCal(txtname,controlname,count)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d-%b-%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        f_date(count);
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
        <script type="text/javascript">
            /* check if pageNO is disable or not */
            function chkdisble(pageNo){
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            /*  load Grid for the User Registration Report */
            function loadTable()
            {

                $('#tohide').hide();
                if($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/CMSSerCaseServlet", {tenderId: <%=request.getParameter("tenderId")%>,size: $("#size").val(),pageNo: $("#first").val(),action:'serviceworkplan',SrvFormMapId:<%=request.getParameter("formMapId")%>,srvBoqId:<%=request.getParameter("formMapId")%>},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            /*  Handle First click event */
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    $('#first').val(0);
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Last Button click event */
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var size = (parseInt($('#size').val())*totalPages)-(parseInt($('#size').val()));
                    $('#first').val(size);
                    if(totalPages>0){
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });



        </script>
        <script type="text/javascript">
            /*  Handle Next Button click event */
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val());
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first+max);

                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo <= totalPages) {

                        loadTable();
                        $('#pageNo').val(Number(pageNo)+1);
                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            /*  Handle Previous click event */
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first-max);
                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    $('#pageNo').val(Number(pageNo)-1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)-1);
                });
            });
        </script>
        <script type="text/javascript">
            var count=0;
            var addcount = 0;
            function Search(){
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            }
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var size = (parseInt($('#size').val())*pageNo)-$('#size').val();
                    $('#first').val(size);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }

                });
            });
            function addrow()
            {
                addcount = document.getElementById("listcount").value;
                count = document.getElementById("listcount").value;
                var newTxt = '<tr><td class="t-align-left">\
                    <input class="formTxtBox_1" type="checkbox" name="chk'+count+' " id="chk'+count+'" /></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="srNo_'+count+'" style=width:40px id="srNo_'+count+'" /></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="Activity_'+count+'" id="Activity_'+count+'" /></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" readonly="true" onchange=f_date(" + i + ") onclick =GetCal("startDt_'+count+'","startDt_'+count+'",'+count+') name="startDt_'+count+'" id="startDt_'+count+'" />&nbsp;<a href=javascript:void(0) title=Calender ><img id=calc_'+count+' src=../resources/images/Dashboard/calendarIcn.png alt=Select a Date border= style=vertical-align:middle onclick =GetCal("startDt_'+count+'","calc_'+count+'",'+count+') /></a></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="noOfDays_'+count+ '" id="noOfDays_'+count+'"  onblur="f_date('+count+')" onchange="checkDays('+count+')"/></td>'+
                    '<td class="t-align-left"><input  class="formTxtBox_1" type="text" readonly="true" name="endDt_'+count+ '" id="endDt_'+ count+'" /></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="remarks_'+count+ '" id="remarks_'+count+'" />'+
                    '<input type=hidden name="srvwplanid_'+count+ '" id="srvwplanid_'+count+'" value="" /></td>'+
                    '</tr>';
                // onblur=checkqtyin('+count+')
                $("#resultTable tr:last").after(newTxt);
                count++;
                document.getElementById("listcount").value = count;
            }
            function delRow(){
                var counter = 0;
                var  newcounter = 0;
                $(":checkbox[checked='true']").each(function(){
                    if(document.getElementById("listcount")!= null){
                        var curRow = $(this).parent('td').parent('tr');
                        curRow.remove();
                        count--;
                        //document.getElementById("listcount").value = count;
                        counter++;
                    }
                });
                $(":checkbox").each(function(){
                    newcounter++;
                });
                if(counter==0){
                    jAlert("Please select at least one item to remove","Service WorkPlan", function(RetVal) {
                    });
                }else{
                    calculateGrandTotal();
                }
            }
            function changetable(){
                var pageNo=parseInt($('#pageNo').val(),10);
                var first = parseInt($('#first').val());
                var max = parseInt($('#size').val());
                var totalPages=parseInt($('#totalPages').val(),10);
                loadTable();

            }
            function onFire(){
                var countt = document.getElementById("listcount").value;
                var flag = true;
                for(var i=0;i<countt;i++){
                    if(document.getElementById("srNo_"+i).value=="" || document.getElementById("Activity_"+i).value=="" || document.getElementById("startDt_"+i).value=="" || document.getElementById("noOfDays_"+i).value=="" || document.getElementById("endDt_"+i).value=="" || document.getElementById("remarks_"+i).value==""){
                        jAlert("It is mandatory to specify Start Date and No of Days of all Groups","Service WorkPlan", function(RetVal) {
                        });
                        flag=false;
                    }

                }
                if(flag){
                    document.getElementById("addcount").value=document.getElementById("listcount").value;
                    document.getElementById("action").value="addserviceworkplan";
                    document.forms["frm"].submit();
                }else{
                    return false;
                }

            }

        </script>
    </head>
    <body onload="loadTable();">
        <%
                    String type = "";
                    if (request.getParameter("Type") != null) {
                        type = request.getParameter("Type");

                    }
                    if (request.getParameter("tenderId") != null) {
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1"><%=srbd.getString("CMS.service.workPlan.title")%>
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=request.getParameter("lotId")%>&formMapId=<%=request.getParameter("formMapId")%>" title="Go Back To WorkSchedule">Go Back</a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <div>&nbsp;</div>
                    <%@include  file="officerTabPanel.jsp"%>
                    <div class="tabPanelArea_1">

                        <%
                                    pageContext.setAttribute("TSCtab", "1");

                        %>
                        <%@include  file="../resources/common/CMSTab.jsp"%>
                        <div class="tabPanelArea_1" >
                            <% if (request.getParameter("msg") != null) {
                                            if ("succ".equalsIgnoreCase(request.getParameter("msg"))) {
                            %>
                            <div class='responseMsg successMsg'>Work Schedule updated Successfully</div>
                            <%} else if ("fail".equalsIgnoreCase(request.getParameter("msg"))) {%>
                            <div class='responseMsg errorMsg'>Work Schedule Submittion Failed</div>
                            <%}
                                }%>
                                <form name="frm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post">
                                <input type="hidden" name="addcount" id="addcount" value="" />
                                <input type="hidden" name="action" id="action" value="" />
                                <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>" />
                                <input type="hidden" name="srvBoqId" id="srvBoqId" value="<%=request.getParameter("srvBoqId")%>" />
                                <input type="hidden" name="lotId" id="lotId" value="<%=request.getParameter("lotId")%>" />
                                <input type="hidden" name="SrvFormMapId" id="SrvFormMapId" value="<%=request.getParameter("formMapId")%>" />
                                <input type="hidden" name="type" id="type" value="<%=type%>" />
                                <div id="resultDiv" style="display: block;">
                                    <div  id="print_area">

                                        <table width="100%" cellspacing="0" class="t_space">
                                            <% if (type.equalsIgnoreCase("add")) {%>
                                            <tr><td colspan="5" style="text-align: right;">
                                                    <a class="action-button-add" id="addRow" onclick="addrow()">Add Item</a>
                                                    <a class="action-button-delete" id="delRow" onclick="delRow()">Remove Item</a>
                                                </td></tr>
                                                <% }%>
                                        </table>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">

                                            <tr>
                                                <% if (type.equalsIgnoreCase("add")) {%>
                                                <th width="5%" class="t-align-center">Check</th>
                                                <% }%>
                                                <th width="5%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.srNo")%></th>
                                                <th width="35%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.activity")%></th>
                                                <th width="15%" class="t-align-center"><%=srbd.getString("CMS.sdate")%></th>
                                                <th width="5%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.noOfDays")%>
                                                </th>
                                                <th width="15%" class="t-align-center"><%=srbd.getString("CMS.edate")%>
                                                </th>
                                                <th width="25%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.remarks")%>
                                                </th>
                                            </tr>
                                        </table>
                                        <table width="100%" cellspacing="0" class="t_space">
                                            <% if (type.equalsIgnoreCase("add")) {%>
                                            <tr><td colspan="6" style="text-align: right;">
                                                    <a class="action-button-add" id="addRow" onclick="addrow()">Add item</a>
                                                    <a class="action-button-delete" id="delRow" onclick="delRow()">Remove item</a>
                                                </td></tr>
                                                <% }%>
                                        </table>
                                    </div>
                                </div>

                                <div id="tohide">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                        <tr>
                                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                Total Records per page : <select name="size" id="size" onchange="changetable();" style="width:40px;">
                                                    <option value="1000" selected>10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>

                                                </select>
                                            </td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" onKeydown="javascript: if (event.keyCode==13) Search();" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1">
                                                    <input type="submit" name="button"  id="btnGoto" value="Go To Page" />
                                                </label></td>
                                            <td  class="prevNext-container">
                                                <ul>
                                                    <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                    <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <input type="hidden" id="pageNo" value="1"/>
                                <input type="hidden" id="first" value="0"/>
                                <br />
                                <center>


                                    <label class="formBtn_1">
                                        <% if (type.equalsIgnoreCase("add")) {%>
                                        <input type="button" name="swpbuttonadd" id="swpbuttonadd" value="Submit" onclick="onFire();" />
                                        <% } else {%>
                                        <input type="button" name="swpbuttonedit" id="swpbuttonedit" value="Update" onclick="onFire();" />
                                        <% }%>
                                    </label>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
