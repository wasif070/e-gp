<%-- 
    Document   : InformationBar
    Created on : Nov 16, 2010, 12:32:37 PM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <jsp:useBean id="appServlet" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <body>
        <%
                    int appid = 0;
                    if (!request.getParameter("appId").equals("")) {
                        appid = Integer.parseInt(request.getParameter("appId"));
                    }
            String logUserId="0";
            if(session.getAttribute("userId")!=null){
                logUserId=session.getAttribute("userId").toString();
            }
            appServlet.setLogUserId(logUserId);
            
        %>
        <div class="tableHead_1 t_space">APP Information Bar :</div>
        <% java.util.List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
                    appListDtBean = appServlet.getAPPDetails("App", request.getParameter("appId"), "");%>
        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
            <tr><% for (CommonAppData details : appListDtBean) { %>
                <td width="10%" class="ff">APP ID	: </td>
                <td width="20%"><%=details.getFieldName1()%></td>
                <td width="10%" class="ff">Letter Ref. No. : </td>
                <td width="20%" ><%=details.getFieldName3()%></td>
                <td width="20%" class="ff">Project Name (If Applicable): </td>
                <td width="20%"><% if(!"".equals(details.getFieldName6())){ out.print(details.getFieldName6()); }else{ out.print("Not Applicable"); } %></td>
            </tr>
            <tr>
                <td class="ff">Financial Year : </td>
                <td><%=details.getFieldName4()%></td>
                <td class="ff">Budget Type : </td>
                <td><%=details.getFieldName5()%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <% }%>
        </table>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabApp");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
</html>
