<%--
    Document   : PrepareTenderTDS
    Created on : 24-Oct-2010, 3:13:11 PM
    Author     : yanki
--%>

<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBeanTender" />
<%--<jsp:useBean id="sectionClauseSrBean" class="com.cptu.egp.eps.web.servicebean.SectionClauseSrBean" />
<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBean" />--%>
<jsp:useBean id="prepareTDSSrBean" class="com.cptu.egp.eps.web.servicebean.PrepareTenderTDSSrBean" />

<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderIttClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderIttSubClause" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
    <%
        int ittHeaderId = Integer.parseInt(request.getParameter("ittHeaderId"));
        int sectionId = Integer.parseInt(request.getParameter("sectionId"));
        int tenderId = Integer.parseInt(request.getParameter("tenderId"));
        int tenderStdId = Integer.parseInt(request.getParameter("tenderStdId"));
        int pkgOrLotId = -1;
        if(request.getParameter("porlId")!=null){
            pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
        }
                       TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

        String contentType = createSubSectionSrBean.getContectType(sectionId);
        String contentType1 = "";
        String instruction = "";
        String clauseInstr = "";
        if(contentType.equalsIgnoreCase("ITT")){
            contentType1 = "BDS";
            instruction = "Instructions for completing Tender/Proposal Data Sheet are provided in italics in parenthesis for the relevant ITB clauses";
            clauseInstr = "Amendments of, and Supplements to, Clauses in the Instructions to Tenderers/Consultants";
        }else if(contentType.equalsIgnoreCase("GCC")){
            contentType1 = "PCC";
            instruction = "Instructions for completing the Particulaprr Conditions of Contract are provided in italics in parenthesis for the relevant GCC Clauses.";
            clauseInstr = "Amendments of, and Supplements to, Clauses in the General Conditions of Contract";
        }

        List<SPTenderCommonData> tblTdsSubClause = null;
        List<SPTenderCommonData> tblCorrigendumDetails=null;
        tblTdsSubClause = prepareTDSSrBean.getTDSSubClause(ittHeaderId);
         /*     GSS-- editing TDS/PCC while corrigendum 14/07/2014      */
            boolean corriCrtNPending=false;
            String corriId="-1";
            if(request.getParameter("corriCrtNPending")!=null&&request.getParameter("corriId")!=null){
                if("true".equalsIgnoreCase(request.getParameter("corriCrtNPending"))){
                    corriCrtNPending=true;
                    }
                corriId=request.getParameter("corriId");
               tblCorrigendumDetails=tenderCommonService.returndata("CorrigendumInfo", corriId, "detailInfo");
                }
    %>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.blockUI.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="../ckeditor/adapters/jquery.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script>

            $(document).ready(function(){
                $("#addClause").bind("click", function(){
                     $("#updateAllTop").html('Save All Clauses');
                    $("#updateAllBottom").html('Save All Clauses');
                    
                    //updateAllBottom
                   // alert(temp1);
                    var lastTrId = "";
                    $('tr[id^="tr_"]').each(function(){
                        lastTrId = $(this).attr("id");
                    });

                    var total = ($("#total").val()*1) + 1;
                    var name = "tds_"+total;
                    var add = "";
                    
                    add = "<a class=\"action-button-add\" id=\"addClause_" + total + "\" href=\"javascript:;\" onclick=\"saveTDS('" + total + "');\" >Save BDS Clause</a>";
                        

                    var content = "<tr id=\"tr_" + lastTrId.split("_")[1] + "\"><td colspan=\"2\"></td></tr>" +
                            "<tr  id=\"tr_" + ((lastTrId.split("_")[1]*1)+1) + "\"><td colspan='2'><textarea cols='80' id='" + name + "' name='" + name +
                            "' rows='10'></textarea><input type = 'hidden' name='hd_" + name + "' id = 'hd_" + name +
                            "' value='<%=ittHeaderId%>' />" + add + "</td></tr>";
                    $("#tdsInfo").append(content);
                    $("#total").val(($("#total").val()*1)+1);
                    //CKEDITOR.replace(name);
                    //$("#"+name).ckeditor(config);
                    CKEDITOR.replace( $("#"+name).attr("id"),
                    {
                        toolbar : 'egpToolbar'
                    });
                });
                $('textarea[name^="tds_"]').each(function(){
                    //$("#"+$(this).attr("id")).ckeditor(config);
                    CKEDITOR.replace($(this).attr("id"),
                    {
                        toolbar : 'egpToolbar'
                    });
                });
                
            });

            function validate(){
                var ele = true;
                $(".error").remove();
                $('textarea[name^="tds_"]').each(function(){
                    var counter = $(this).attr("id");

                    if(CKEDITOR.instances[counter].getData().trim().length == 0){
                        $(this).parent().append("<span class='error' style='color:red'>Please Enter <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Details</span> ");
                        ele = false;
                    }
                });
                return ele;
            }
            function updateTDS(counter){
           // alert(counter+'counter')
                //headerId_clauseid_ittsubclauseid_tdsSubClauseId
                $.blockUI({ message: null });
                //alert($("#hd_tds_"+counter).val()+"for id");
                var ele = $("#hd_tds_"+counter).val().split("_");
                //alert(ele+"ele")
                var orderNo = $("#hd_orderNo_"+counter);
                //alert(ele[3]+"subClaseId");
                //alert($("#hd_orderNo_"+counter).val()+"Order No");
                if(ele[3] == null || ele[3] == "null"){
                    //can't be updated...
                } else {
                    //return false;
                    $.ajax({
                        url: "<%=request.getContextPath()%>/PrepareTenderTDSSrBean",
                        data: {"action": "update",
                                "headerId": ele[0],
                                "tdsSubClauseId": ele[3],
                                "tdsInfo": CKEDITOR.instances["tds_"+counter].getData()
                                <% if(corriCrtNPending){  %> 
                                   ,
                                "corriId":"<%= corriId %>",
                                "hd_corigendum_field":$("#hd_corigendum_field_"+counter).val(),
                                "hd_corrigendum_field_oldvalue":$("#hd_corigendum_field_oldvalue_"+counter).html(),
                                "insertcorrigendum":"true",
                                "corriDetailId":$("#hd_corrigendum_detail_id_"+counter).val()
                                

                                <% } %>

                            },
                        method: "POST",
                        context: document.body,
                        success: function(response){
                            $.unblockUI();
                            if(response == "OK"){
                                jAlert("<%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Information updated successfully", "success")
                            } else if(response == "OK corrigendum"){
                               jAlert("<%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Information updated successfully in Corrigendum. Changes will be made permanent once corrigendum published.To view old, new values and publish the corrigendum go to Corrigendum Tab","success" ,function(result){
                                
                             location.reload();
                               });
                            } else if(response == "error corrigendum"){
                               jAlert("<%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> ERROR occured while updating <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> details in corrigendum", "ERROR");
                            }
                            else if(response == "nothing to update"){
                               jAlert("<%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Nothing to update  in corrigendum", "ERROR");
                            }
                            else {
                                jAlert("ERROR occured while updating <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> record", "ERROR");
                            }
                        }, error: function (msg1,msg2, msg3){
                            alert("msg11" + msg1.responseText);
                                                    }
                    });
                }
                
                
            }
            
            
            
            function deleteTDS(counter){
                jConfirm("Do you really want to remove this <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Clause", "Confirm", function(result){
                    if(result){
                        $.blockUI({ message: null });
                        var ele = $("#hd_tds_"+counter).val().split("_");
                        $.ajax({
                            url: "<%=request.getContextPath()%>/PrepareTenderTDSSrBean",
                            data: {"action": "delete",
                                    "headerId": ele[0],
                                    "tdsSubClauseId": ele[3]
                                },
                            method: "POST",
                            context: document.body,
                            success: function(response){
                                $.unblockUI();
                                if(response == "OK"){
                                    jAlert("<%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Information removed successfully", "success", function(){
                                        $.blockUI({ message: '<h1>Just a moment...</h1>' });
                                        window.location.reload();
                                    });
                                    /*$("#hd_tds_"+counter).parent().parent().remove();
                                    $("#hd_tds_"+counter).parent().parent().prev().remove();
                                    $("#total").val(($("#total").val()*1)-1);*/
                                } else {
                                    jAlert("ERROR occured while removing record", "ERROR");
                                }
                            },
                            error: function (msg1,msg2, msg3){
                                alert("msg12" + msg1.responseText);
                            }
                        });
                    }
                });
            }
            function saveTDS(counter){
                $.blockUI({ message: null });
                $.ajax({
                    url: "<%=request.getContextPath()%>/PrepareTenderTDSSrBean",
                    data: {"action": "add",
                            "headerId": '<%=request.getParameter("ittHeaderId")%>',
                            "tdsInfo": $.trim(CKEDITOR.instances["tds_"+counter].getData()),
                            //"orderNo": $("#hd_orderNo_"+(($("#total").val()*1)-1)).val()
                            "orderNo": $("#hd_orderNo_"+(($("#total").val()*1)-1)).val()
                        },
                    method: "POST",
                    context: document.body,
                    success: function(response){
                        $.unblockUI();
                        if(response.split(",")[0] == "OK"){
                            jAlert("<%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Information saved successfully", "success",function(){
                                $.blockUI({ 
                                    message: '<h1>Just a moment...</h1>'
                                });
                                window.location.reload();
                            });
                        } else {
                            jAlert("Error occured while saving <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Information:" + response, "Error");
                        }
                    },
                    error: function (msg1,msg2, msg3){
                        alert("msg13" + msg1.responseText);
                    }
                });
            }
        </script>

<script type="text/javascript">
    function updateAllTDS(){
         var myform=document.getElementById("prepareTdsFrm");
         //alert(myform+"myform");
         myform.action = '<%=request.getContextPath()%>/PrepareTenderTDSSrBean?ittHeaderId=<%=ittHeaderId%>&sectionId=<%=sectionId%>&tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&edit=true&porlId=<%=pkgOrLotId%>&action=updateAll';
         //alert(myform.action+"   myform.action");
         myform.submit();
    }                          
</script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">Prepare <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%></div>
                            </div>
                            <%if(pkgOrLotId == -1){%>
                            <table width="100%" cellspacing="10"  class="tableView_1" >
                                <tr>
                                    <td  align="left">
                                    <a href="TenderDocPrep.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>" class="action-button-goback">Go Back to Tender Document Preparation</a>
                                    </td>
                                    <td align="right">
                                    <a href="TenderTDSDashBoard.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&sectionId=<%=sectionId%><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>" class="action-button-goback">Go Back to Tender <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Dashboard</a>
                                    </td>
                                </tr>
                            </table>
                            <%}else{%>
                            <table width="100%" cellspacing="10"  class="tableView_1" >
                                <tr>
                                    <td  align="left">
                                    <a href="TenderDocPrep.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&porlId=<%= pkgOrLotId %><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>" class="action-button-goback">Go Back to Tender Document Preparation</a>
                                    </td>
                                    <td align="right">
                                    <a href="TenderTDSDashBoard.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&sectionId=<%=sectionId%>&porlId=<%= pkgOrLotId %><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %>" class="action-button-goback">Go Back to Tender <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Dashboard</a>
                                    </td>
                                </tr>
                            </table>
                            <%}%>
                            <table width="100%" border="0" cellspacing="10" class="tableView_1">
                                <tr>
                                    <td align="left">
                                        <img src="../resources/images/Dashboard/addIcn.png" width="16" height="16" class="linkIcon_1" />
                                        <a id="addClause" href="javascript:void(0);">Add New Clause</a> &nbsp;&nbsp;
                                 <%  if(!corriCrtNPending) { %>       <a class="action-button-edit" id="updateAllTop"  onclick="updateAllTDS();">Update All Clauses</a>  <% } %>
                                    </td>
                                    
                                </tr>
                            </table>
                            <form id="prepareTdsFrm" action="<%=request.getContextPath()%>/PrepareTDSSrBean?action=save" method="post" onsubmit="return validate();">
                                <input type="hidden" name="sectionId" id="sectionId" value="<%=request.getParameter("sectionId")%>" />
                                <input type="hidden" name="templateId" id="templateId" value="<%=request.getParameter("templateId")%>" />


                                        <table width="100%" cellspacing="0" id="tdsInfo" class="tableList_1 t_space">
                                            <tr>
                                                <td colspan="2"><%=instruction%></td>
                                            </tr>
                                            <tr>
                                                <td><b><%=contentType%> Clause</b></td>
                                                <td><%=clauseInstr%></td>
                                            </tr>
                                    <%
                                        //tblTdsSubClause
                                        String ittClauseId = "-1";
                                        
                                        
                                        int k=1,i;
                                        for(i=0;i<tblTdsSubClause.size(); i++){
                                            if(!ittClauseId.equals(tblTdsSubClause.get(i).getFieldName2())){
                                                ittClauseId = tblTdsSubClause.get(i).getFieldName2();
                                                %>
                                                <tr id="tr_<%=k++%>">
                                                    <td colspan="2"><%=tblTdsSubClause.get(i).getFieldName3()%></td>
                                                </tr>
                                                <%
                                            }
                                            %>
                                                <tr id="tr_<%=k++%>">
                                                    <td colspan="2"><%=tblTdsSubClause.get(i).getFieldName5()%></td>
                                                </tr>
                                                <tr id="tr_<%=k++%>">
                                                    <td colspan="2">
                                                         <% if(corriCrtNPending && ! corriId.equals("-1")){ %>
                                                        <input type="hidden" name="hd_corigendum_field_<%=i+1%>" id="hd_corigendum_field_<%=i+1%>" value="<%= contentType1+"_"+tblTdsSubClause.get(i).getFieldName8()+"_"+tblTdsSubClause.get(i).getFieldName3()+"_"+tblTdsSubClause.get(i).getFieldName7() %>" />
                                                        <span style="display:none" id="hd_corigendum_field_oldvalue_<%=i+1%>"><%= tblTdsSubClause.get(i).getFieldName6() %></span>
                                                    <%   int counter=0;
                                                         String corrigendumEditedValue=null;
                                                       for(SPTenderCommonData corrigendumDetail:tblCorrigendumDetails)
                                                        {
                                                         if(corrigendumDetail.getFieldName1().contains(tblTdsSubClause.get(i).getFieldName7())){
                                                             counter++;
                                                             corrigendumEditedValue=corrigendumDetail.getFieldName3();
                                                        %>
                                                        <input type="hidden" name="hd_corrigendum_detail_id_<%=i+1%>" id="hd_corrigendum_detail_id_<%=i+1%>" value="<%= corrigendumDetail.getFieldName4() %>" />
                                                        
                                                      <% break;
                                                         }}
                                                       if(counter!=0){ %> 
                                                         <textarea cols="80" id="tds_<%=i+1%>" name="tds_<%=i+1%>" rows="10">
                                                            <%out.print(corrigendumEditedValue);%>
                                                        </textarea>
                                                        <span style="color:red">
                                                           Note:The above sub clause details are edited  for corrigendum. To view and publish the changes Go to Corrigendum Tab and Publish/View
                                                        <br/>
                                                        </span>
                                                        <% }else{ %>
                                                         <textarea cols="80" id="tds_<%=i+1%>" name="tds_<%=i+1%>" rows="10">
                                                            <%if(tblTdsSubClause.get(i).getFieldName6() != null){out.print(tblTdsSubClause.get(i).getFieldName6());}%>
                                                        </textarea>
                                                        <%  } }else{ %>
                                                        <textarea cols="80" id="tds_<%=i+1%>" name="tds_<%=i+1%>" rows="10">
                                                            <%if(tblTdsSubClause.get(i).getFieldName6() != null){out.print(tblTdsSubClause.get(i).getFieldName6());}%>
                                                        </textarea>
                                                        <% } %>
                                                        <input type="hidden" name="hd_tds_<%=i+1%>" id="hd_tds_<%=i+1%>" value="<%=ittHeaderId+"_"+tblTdsSubClause.get(i).getFieldName2()+"_"+tblTdsSubClause.get(i).getFieldName4()+"_"+tblTdsSubClause.get(i).getFieldName9()%>" />
                                                        <input type="hidden" name="hd_orderNo_<%=i+1%>" id="hd_orderNo_<%=i+1%>" value="<%=tblTdsSubClause.get(i).getFieldName10()%>" />
                                                        <a class="action-button-edit" href="javascript:void(0);" onclick="updateTDS('<%=i+1%>');" >Update <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Details</a>
                                                        <%
                                                        if(ittClauseId.equals("")){
                                                        %>
                                                            <a class="action-button-delete" href="javascript:void(0);" onclick="deleteTDS('<%=(i+1)%>');">Delete <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Details</a>
                                                        <%
                                                        }
                                                        %>
                                                    </td>
                                                </tr>
                                            <%
                                        }
                                        %>
                                        </table>
                            <table width="100%" border="0" cellspacing="10" class="tableView_1">
                                <tr>
                                    <td align="right">
                              <%  if(!corriCrtNPending) { %>            <a id="updateAllBottom" class="action-button-edit"  onclick="updateAllTDS();" >Update All Clauses</a> <% } %>
                                    </td>
                                    
                                </tr>
                            </table>
                                        <input type="hidden" id="total" name ="total" value="<%=i%>" />
                            </form>

                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
            <%
            if(request.getParameter("msg")!=null && "sucess".equalsIgnoreCase(request.getParameter("msg"))){%>
            <script type="text/javascript">
                jAlert("<%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Information saved successfully", "Success","Sucess");
            </script>
            <%} if(request.getParameter("msg")!=null && "error".equalsIgnoreCase(request.getParameter("msg"))){%>
                <script type="text/javascript">
                jAlert("Error occured while saving <%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Information:", "Error", "Error");
            </script>
            <% }
             if(request.getParameter("msg")!=null && "updateAll".equalsIgnoreCase(request.getParameter("msg"))){%>
                <script type="text/javascript">
                jAlert("<%if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Information updated successfully", "Success", "Success");
            </script>
            <%}%>
            
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>
</html>
