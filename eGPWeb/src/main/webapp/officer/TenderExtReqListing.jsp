<%--
    Document   : TenderExtReqListing
    Created on : Nov 30, 2010, 12:42:04 PM
    Author     : Rajesh Singh,Rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Validity Extension Request</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/home.css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script type="text/javascript">
            function chkdisble(pageNo){
                
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }
                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else {
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);
                        $('#btnPrevious').removeAttr("disabled");
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();

                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function loadTable()
            { 
                $.post("<%=request.getContextPath()%>/TenderExtReqListingServlet", {status: $("#status").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    if($('#noRecordFound').attr("value") == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }

                    $("#pageNoTot").html($("#pageNo").val());
                    chkdisble($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                });
            }
        </script>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <div class="contentArea_1">
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <div class="pageHead_1">Validity Extension Request
                <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('6');">Save as PDF</a></span>
                </div>
                <ul class="tabPanel_1 t_space">
                    <li class="sMenu" id="pendingTab"><a href="javascript:void(0);" onclick="changeStatus();">Pending</a></li>
                    <li id="processTab" ><a href="javascript:void(0);" onclick="changeS();">Processed</a></li>
                </ul>
                <div class="tabPanelArea_1" id="tab1" style="display: block;">
                    <table style="display: none">
                        <tr>
                            <td>
                                <input type="text" value="Pending" id="status" name="status"/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" id="resultTable" class="tableList_3" cols="@0,6">
                        <tr>
                            <th width="4%" class="t-align-left">Sl. No. </th>
                            <th width="14%" class="t-align-left">Tender ID</th>
                            <th width="13%" class="t-align-left">Ref. No.</th>
                            <th width="24%" class="t-align-left">Hierarchy Node</th>
                            <th width="16%" class="t-align-left">Office</th>
                            <th width="18%" class="t-align-left">Status</th>
                            <th width="11%" class="t-align-left">Action</th>
                        </tr>
                    </table>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                        <tr>
                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                            <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                </label></td>
                            <td  class="prevNext-container"><ul>
                                    <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                    <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                </ul></td>
                        </tr>
                    </table>
                    <div align="center">
                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" name="size" id="size" value="10"/>
                    </div>
                </div>
                <form id="formstyle" action="" method="post" name="formstyle">

                   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                   <%
                     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                     String appenddate = dateFormat1.format(new Date());
                   %>
                   <input type="hidden" name="fileName" id="fileName" value="ValidityExtensionRequest_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="ValidityExtensionRequest" />
                </form>

                <div>&nbsp;</div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        loadTable();
        var headSel_Obj = document.getElementById("headTabEval");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

        function changeStatus(){
            document.getElementById('status').value = 'Pending';
            $("#processTab").removeClass("sMenu");
            $("#pendingTab").addClass("sMenu");
            loadTable();
        }
        function changeS(){
            document.getElementById('status').value = 'Approved';
            $("#pendingTab").removeClass("sMenu");
            $("#processTab").addClass("sMenu");
            loadTable();
        }

    </script>
</html>
