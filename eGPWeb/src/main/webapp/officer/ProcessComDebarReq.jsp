<%-- 
    Document   : ProcessComDebarReq
    Created on : Jan 26, 2011, 03:21:34 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentDocs"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentComments"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentReq"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentDetails"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentTypes"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.web.servicebean.InitDebarmentSrBean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Process Debarment Requests </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function getDisplayData(rdBtn){
                var rdValue=rdBtn.value;
                $('#debarType').val(rdValue);
                $('#debarIds').val(null);
                $('#tbodyVal').html(null);
                if(rdValue=="6"){
                    $('#addDet').hide();
                    $('#trVal').hide();
                }else{
                    $('#addDet').show();
                    $('#trVal').show();
                    if(rdValue=="1"){
                        $('#dlbHead1').html("Ref No.");
                        $('#dlbHead2').html("Tender/Proposal Brief");
                    }if(rdValue=="2"){
                        $('#dlbHead1').html("Letter Ref. No.");
                        $('#dlbHead2').html("Package No.");
                    }if(rdValue=="3"){
                        $('#dlbHead1').html("Project Name");
                        $('#dlbHead2').html("Project Code");
                    }if(rdValue=="4"){
                        $('#dlbHead1').html("PE Office Name");
                        $('#dlbHead2').html("PE Code");
                    }if(rdValue=="5"){
                        $('#dlbHead1').html("Department Name");
                        $('#dlbHead2').html("Department Type");
                    }
                }
            }
            $(function() {
                $( "#dialog:ui-dialog" ).dialog( "destroy" );
                $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    resizable:false,
                    draggable:false,
                    height: 500,
                    width: 600,
                    modal: true,
                    buttons: {
                        "Add": function() {
                            $(function() {
                                var data="";
                                $("#tbodyVal").html(null);
                                $(":checkbox[checked='true']").each(function(){
                                    $("#tbodyVal" ).append("<tr><td>"+$(this).parent('td').parent('tr').children()[1].innerHTML+"</td><td>"+$(this).parent('td').parent('tr').children()[2].innerHTML+"</td></tr>");
                                    data+=$(this).val()+",";
                                });
                                $('#debarIds').val(data);
                                $('#trVal').show();
                            });
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    },
                    close: function() {
                    }
                });

                $("#addDet" ).click(function(){
                    if($('#debarType').val()!=$('#hdndebarType').val()){
                        $("input[type='checkbox']").removeAttr("checked");
                    }
                    $('#div1').hide();
                    $('#div2').hide();
                    $('#div3').hide();
                    $('#div4').hide();
                    $('#div5').hide();
                    $('#div'+$('#debarType').val()).show();
                    $("#dialog-form").dialog("open");
                });
            });
            function validate(){
                $(".err").remove();
                var valid = true;
                if($.trim($("#comments").val())=="") {
                    $("#comments").parent().append("<div class='err' style='color:red;'>Please enter Comments.</div>");
                    valid=false;
                }               
                if(!valid){
                    return false;
                }
            }
            $(function() {
                $('#debarStatus').change(function() {
                    if($('#debarStatus').val()=="hopesatisfy"){
                       $('#trDate').hide();
                    }else{
                        $('#trDate').show();
                    }
                });
            });
        function CompareToForGreater(value,params)
                {

                var mdy = value.split('/')  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');


                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                {
                    //alert('Both Date');
                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                }
                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                {
                    //alert('Both DateTime');
                    var mdyhrsec=mdyhr[1].split(':');
                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec=mdyphr[1].split(':');
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                }
                else
                {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if(a == undefined && b != undefined)
                    {
                        //alert('First Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('Second Date');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                    }
                }

                return Date.parse(date) >= Date.parse(datep);
            }
        </script>
    </head>
    <body>
        <%
                    String debarId = request.getParameter("debId");
                    InitDebarmentSrBean srBean = new InitDebarmentSrBean();
                    String status = request.getParameter("stat");
                    if ("Submit".equalsIgnoreCase(request.getParameter("submit"))) {
                        srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));
                        if(srBean.committeMemComments(debarId, request.getParameter("comments"), session.getAttribute("userId").toString(),"committee")){
                            response.sendRedirect("ComDebarListing.jsp ");
                        }else{
                            out.print("<b>Error in CommitteMemComments</b>");
                        }
                    }else if("Submit".equalsIgnoreCase(request.getParameter("cpsubmit"))) {
                        srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));
                        if(srBean.committeMemComments(debarId, request.getParameter("comments"), session.getAttribute("userId").toString(),"chairperson")){
                            response.sendRedirect("ComDebarListing.jsp ");
                        }else{
                            out.print("<b>Error in CommitteChairperson</b>");
                        }
                        /*if(srBean.chaipersonReview(debarId, session.getAttribute("userId").toString(), request.getParameter("comments"), request.getParameter("debarStatus"),"chairperson")){
                            response.sendRedirect("ComDebarListing.jsp ");
                        }else{
                            out.print("<b>Error in ChairpersonReview</b>");
                        }*/
                        
                    } else {
                        List<Object[]> hopeAns = srBean.findHopeAns(debarId);
                        //0. debarIds, 1. debarStartDt, 2. debarEdnDt, 3. comments, 4. debarTypeId
                        int hopeAnsSize = hopeAns.size();
                        String condText=null;
                        if(hopeAnsSize==0){
                            condText="bype";
                        }else{
                            condText="byhope";
                        }
                        List<TblDebarmentDetails> dataList = srBean.getDebarDetailsForHope("tblDebarmentReq",Operation_enum.EQ,new TblDebarmentReq(Integer.parseInt(debarId)),"debarStatus",Operation_enum.EQ,condText);
                        SPTenderCommonData data = srBean.viewTendererDebarClari(Integer.parseInt(debarId), status).get(0);
                        List<TblDebarmentTypes> debarTypes = srBean.getAllDebars();
                        String[] debarIds = dataList.get(0).getDebarIds().split(",");
                        List<TblDebarmentComments> commList = srBean.getCommitteeMemComments(debarId, session.getAttribute("userId").toString(),"committee");
                        List<Object[]> wholeComList = srBean.getWholeCommitteComments(debarId);
                        long isChairperson  = srBean.isChairperson(debarId, session.getAttribute("userId").toString());
                        String committeComments=null;
                        long comAnsCnt = srBean.countCommitteeMember(debarId);
                        Object [] tendPeid = srBean.findPETenderHopeId(debarId);
                        String debarTypeId = null;
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="pageHead_1">Process Debarment Requests <span style="float:right;"> <a class="action-button-goback" href="ComDebarListing.jsp">Go back</a> </span></div>
        <form method="post" action="ProcessComDebarReq.jsp?debId=<%=debarId%>" >
            <table class="tableList_1 t_space" cellspacing="0" width="100%">
                <tbody>
                    <tr>
                        <th colspan="2">PE Section</th>
                    </tr>
                    <tr>

                        <td class="t-align-left ff" width="16%">Company Name :</td>
                        <td class="t-align-left" width="84%"><%if (data.getFieldName4().equals("-")) {
                                        out.print(data.getFieldName5() + " " + data.getFieldName6());
                                    } else {
                                        out.print(data.getFieldName4());
                                    }%></td>
                    </tr>
                    <tr>

                        <td class="t-align-left ff" valign="top">Clarification Sought :</td>
                        <td class="t-align-left" width="84%"><%out.print(data.getFieldName1());%></td>
                    </tr>
                    <%
                        List<TblDebarmentDocs>  debarmentDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(tendPeid[1].toString()));
                        if(!debarmentDocses.isEmpty()){
                    %>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5" class="t-align-left">Reference documents by PE</th>
                            </tr>
                            <tr>
                                <th class="t-align-center">Sl. No.</th>
                                <th class="t-align-center">Document Name</th>
                                <th class="t-align-center">Document Description</th>
                                <th class="t-align-center">File Size (In KB)</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        int count = 1;
                                        for (TblDebarmentDocs ttcd : debarmentDocses) {
                            %>
                            <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                                <td class="t-align-center"><%=count%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                                <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                                <td class="t-align-center">
                                    <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=tend&stat=<%=status%>&userId=<%=tendPeid[1]%>&uTid=3" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                </td>
                            </tr>
                            <%count++;}%>
                    </table>
                    </td>
                </tr>
                <%}%>
                    <tr>
                        <td class="t-align-left ff">Last  Date for Response :</td>
                        <td class="t-align-left"><%out.print(data.getFieldName2());%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff" valign="top">Bidder's/Consultant's Response :</td>
                        <td class="t-align-left"><%=data.getFieldName3()%></td>
                    </tr>
                    <%
                            List<TblDebarmentDocs>  tenderDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(tendPeid[0].toString()));
                            if(!tenderDocses.isEmpty()){
                    %>
                    <tr>
                    <td>&nbsp;</td>
                    <td>                        
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5" class="t-align-left">Reference documents by Bidder/Consultant </th>
                            </tr>
                            <tr>
                                <th class="t-align-center">Sl. No.</th>
                                <th class="t-align-center">Document Name</th>
                                <th class="t-align-center">Document Description</th>
                                <th class="t-align-center">File Size (In KB)</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        int count = 1;
                                        for (TblDebarmentDocs ttcd : tenderDocses) {
                            %>
                            <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                                <td class="t-align-center"><%=count%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                                <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                                <td class="t-align-center">
                                    <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=tend&stat=<%=status%>&userId=<%=tendPeid[0]%>&uTid=2" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                </td>
                            </tr>
                            <%count++;}%>
                    </table>
                    </td>
                </tr>
                <%}%>
                    <tr>
                        <td class="t-align-left ff" valign="top">PE Action :</td>
                        <td class="t-align-left">
                            <%if (status.equals("pesatisfy")) {
                                            out.print("Satisfactory");
                                        } //else if (status.equals("sendtohope")) {
                                         else{
                                            out.print("Un Satisfactory");
                                        }%>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff" valign="top">
                            PE Comments :
                        </td>
                        <td class="t-align-left">
                            <%=data.getFieldName9()%>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">HOPA Section</th>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Debarment Type : </td>
                        <td class="t-align-left">
                            <%
                                int i = 0;
                                for (TblDebarmentTypes types : debarTypes) {
                                  if(hopeAnsSize==0){
                            %>
                                        <label><input type="radio" value="<%=types.getDebarTypeId()%>" name="debarType" id="debarType<%=i%>" onclick="getDisplayData(this);"  <%if(Integer.parseInt(data.getFieldName10())==types.getDebarTypeId()){out.print("checked");}%>/>&nbsp;<%=types.getDebarType()%></label>&nbsp;&nbsp;&nbsp;
                            <%
                                    }else{
                                      if((Integer)hopeAns.get(0)[4]==types.getDebarTypeId()){
                                          debarTypeId = hopeAns.get(0)[4].toString();
                                          out.print(types.getDebarType());
                                      }
                                    }
                                    i++;
                                }
                            %>
                            <input type="hidden" value="<%=data.getFieldName10()%>" id="debarType" />
                            <input type="hidden" value="<%=data.getFieldName10()%>" id="hdndebarType" />
                            <input type="hidden" value="<%=dataList.get(0).getDebarIds()%>," name="debarIds" id="debarIds"/>
                            <%if(hopeAnsSize==0){%>
                            <span class="c-alignment-right"><a id="addDet" class="action-button-add">Add Details</a></span>
                            <%}%>
                        </td>
                    </tr>
                    <%if(!"6".equals(debarTypeId)){%>
                    <tr id="trVal">
                        <td class="t-align-left ff">&nbsp;</td>
                        <td class="t-align-left">
                            <%
                                String pageHead1=null;
                                String pageHead2=null;
                                boolean isTenderDeb = false;
                                if(hopeAnsSize!=0){
                                    if(hopeAns.get(0)[4].toString().equals("1")){
                                        pageHead1="Ref No.";
                                        pageHead2="Tender/Proposal Brief";
                                        isTenderDeb = true;
                                    }else if(hopeAns.get(0)[4].toString().equals("2")){
                                        pageHead1="Letter Ref. No.";
                                        pageHead2="Package No.";
                                    }else if(hopeAns.get(0)[4].toString().equals("3")){
                                        pageHead1="Project Name";
                                        pageHead2="Project Code";
                                    }else if(hopeAns.get(0)[4].toString().equals("4")){
                                        pageHead1="PE Office Name";
                                        pageHead2="PE Code";
                                    }else if(hopeAns.get(0)[4].toString().equals("5")){
                                        pageHead1="Department Name";
                                        pageHead2="Department Type";
                                    }
                                }else{
                                    if(data.getFieldName10().equals("1")){
                                    pageHead1="Ref No.";
                                    pageHead2="Tender/Proposal Brief";
                                    isTenderDeb = true;
                                    }else if(data.getFieldName10().equals("2")){
                                        pageHead1="Letter Ref. No.";
                                        pageHead2="Package No.";
                                    }else if(data.getFieldName10().equals("3")){
                                        pageHead1="Project Name";
                                        pageHead2="Project Code";
                                    }else if(data.getFieldName10().equals("4")){
                                        pageHead1="PE Office Name";
                                        pageHead2="PE Code";
                                    }else if(data.getFieldName10().equals("5")){
                                        pageHead1="Department Name";
                                        pageHead2="Department Type";
                                    }
                                }                                
                            %>
                            <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                                <tr>
                                    <%if(isTenderDeb){%>
                                    <th class='t-align-center'>Tender/Proposal ID</th>
                                    <%}%>
                                    <th class='t-align-center' width='30%'><label id='dlbHead1'><%=pageHead1%></label></th>
                                    <th class='t-align-center' width='50%'><label id='dlbHead2'><%=pageHead2%></label></th>
                                </tr>
                                <tbody id="tbodyVal">
                                    <%
                                        String hopeId = srBean.getCommitteDetails(session.getAttribute("userId").toString(),debarId).get(0)[1].toString();
                                        List<SPTenderCommonData> list = srBean.searchDataForDebarType(data.getFieldName10(),hopeId);
                                        for(int j=0; j<debarIds.length;j++){
                                            for(SPTenderCommonData sptcd : list){
                                                if(sptcd.getFieldName1().equals(debarIds[j])){
                                                    out.print("<tr>");
                                                    String viewLink = null;
                                                    if(isTenderDeb){
                                                        out.print("<td>"+sptcd.getFieldName1()+"</td>");
                                                        viewLink = "<a onclick=\"javascript:window.open('"+request.getContextPath()+"/resources/common/ViewTender.jsp?id=" + sptcd.getFieldName1() +"', '', 'resizable=yes,scrollbars=1','');\" href='javascript:void(0);'>" + sptcd.getFieldName3() + "</a>";
                                                    }else{
                                                       viewLink = sptcd.getFieldName3();
                                                    }
                                                    out.print("<td><input type='hidden' value='"+sptcd.getFieldName1()+"' id='debId"+j+"'/>"+sptcd.getFieldName2()+"</td><td>"+viewLink+"</td>");
                                                    out.print("</tr>");
                                                }
                                            }
                                        }
                                    %>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <%}%>
                    <tr>
                        <td class="t-align-left ff" valign="top">HOPA Action :</td>
                        <td class="t-align-left">
                            <%if(hopeAnsSize==0){%>
                            <select name="debarStatus" class="formTxtBox_1" id="debarStatus" style="width: 180px;">
                                <option value="sendtoegp">Approve Debarment</option>
                                <option value="hopesatisfy">Disapprove Debarment</option>
                                <option value="sendtocom">Form Review Committee</option>
                            </select>
                            <%}else{                                
                                if(status.equals("hopesatisfy")){out.print("Disapprove Debarment");}
                                else if(status.equals("sendtocom")){out.print("Form Review Committee");}
                                else{out.print("Approve Debarment");}
                                //if(status.equals("sendtoegp")){out.print("Approve Debarment");}
                            }%>
                        </td>
                    </tr>
                    <tr id="trDate">
                        <td class="t-align-left ff" valign="top">Debarment Period :</td>
                        <td class="t-align-left">
                            <%if(hopeAnsSize==0){%>
                            Start :
                            <input type="text" name="debarStart" id="dt1" onfocus="GetCal('dt1','dt1');" class="formTxtBox_1"/>
                            <img id="dtimg1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('dt1','dtimg1');"/>
                            &nbsp;&nbsp;&nbsp;&nbsp;End :
                            <input type="text" name="debarEnd" id="dt2" onfocus="GetCal('dt2','dt2');" class="formTxtBox_1"/>
                            <img id="dtimg2" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('dt2','dtimg2');"/>
                            <%}else{out.print("Start : "+DateUtils.customDateFormate((Date)hopeAns.get(0)[1])+"&nbsp;&nbsp;&nbsp;&nbsp;End : "+DateUtils.customDateFormate((Date)hopeAns.get(0)[2]));}%>
                        </td>
                    </tr>
                    <%
                        List<TblDebarmentDocs>  hopeDocses = srBean.getAllDocs("debarmentId",Operation_enum.EQ,Integer.parseInt(debarId),"uploadedBy",Operation_enum.EQ,Integer.parseInt(tendPeid[2].toString()));
                        if(!hopeDocses.isEmpty()){
                  %>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5" class="t-align-left">Reference documents by HOPA Documents</th>
                            </tr>
                            <tr>
                                <th class="t-align-center">Sl. No.</th>
                                <th class="t-align-center">Document Name</th>
                                <th class="t-align-center">Document Description</th>
                                <th class="t-align-center">File Size (In KB)</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        int count = 1;
                                        for (TblDebarmentDocs ttcd : hopeDocses) {
                            %>
                            <tr class="<%if(count%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                                <td class="t-align-center"><%=count%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                                <td class="t-align-left"><%=ttcd.getDocumentDesc()%></td>
                                <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");out.print(twoDForm.format(Double.parseDouble((ttcd.getDocSize()))/1024));%></td>
                                <td class="t-align-center">
                                    <a href="<%=request.getContextPath()%>/InitDebarment?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocSize()%>&action=debarDocDownload&debId=<%=debarId%>&user=tend&stat=<%=status%>&userId=<%=tendPeid[2]%>&uTid=3" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                </td>
                            </tr>
                            <%count++;}%>
                    </table>
                    </td>
                </tr>
                <%}%>
                    <tr>
                        <td class="t-align-left ff" valign="top">HOPA Comments :</td>
                        <td class="t-align-left">
                             <%if(hopeAnsSize==0){%>
                            <textarea rows="5" id="comments" name="comments" class="formTxtBox_1" style="width: 400px;"></textarea>
                            <%}else{out.print(hopeAns.get(0)[3]);}%>
                        </td>
                    </tr>
                    <!--
                    For single comments of committe member
                    Below one is commented.
                    -->
                    <%if(isChairperson==0){%>
                    <tr>
                        <td class="t-align-left ff" valign="top">Comments : <%if(commList.isEmpty()){out.print("<span class='mandatory'>*</span>");}%></td>
                        <td class="t-align-left">
                             <%if(commList.isEmpty()){%>
                            <textarea rows="5" id="comments" name="comments" class="formTxtBox_1" style="width: 400px;"></textarea>
                            <%}else{out.print(commList.get(0).getComments());}%>
                        </td>
                    </tr>
                    <%}%>
                    <%if(isChairperson!=0){%>
                    <%if(!wholeComList.isEmpty()){%>
                    <tr>
                        <th colspan="2">Committee Section</th>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">&nbsp;</td>
                        <td class="t-align-left">
                            <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                                <tr>
                                    <th class='t-align-center' width='20%'>Employee Name</th>
                                    <th class='t-align-center' width='12%'>Employee Role</th>
                                    <th class='t-align-center' width='70%'>Comments</th>
                                </tr>
                                <%for(Object[] comments : wholeComList){%>                                
                                <tr>
                                    <%if(comments[3].equals("chairperson")){committeComments=comments[2].toString();}if(true){%>
                                    <td><%=comments[0]%></td>
                                    <td><%if(comments[1].equals("cp")){out.print("Chair Person");}else{out.print("Member");}%></td>
                                    <td><%=comments[2]%></td>
                                    <%}%>
                                </tr>
                                <%}%>
                            </table>
                        </td>
                    </tr>
                    <%}%>                   
                    <!--%
                        if(wholeComList.size()==comAnsCnt || wholeComList.size()-1==comAnsCnt){
                    %>
                    <tr>
                        <td class="t-align-left ff">Chairperson Action : <!%if(committeComments==null){out.print("<span class='mandatory'>*<span>");}%></td>
                        <td class="t-align-left">
                            <!%if(committeComments==null){%>
                            <select name="debarStatus" class="formTxtBox_1" id="debarStatus" style="width: 180px;">
                                <option value="sendtoegp">Approve Debarment</option>
                                <option value="comsatisfy">Disapprove Debarment</option>
                            </select>
                            <!%}else{if(status.equals("comsatisfy")){out.print("Disapprove Debarment");}else{out.print("Approve Debarment");}}%>
                        </td>
                    </tr>-->
                    <tr>
                        <td class="t-align-left ff" valign="top">Chairperson Comments : <%if(committeComments==null){out.print("<span class='mandatory'>*</span>");}%></td>
                        <td class="t-align-left">
                            <%if(committeComments==null){%>
                            <textarea  rows="5" id="comments" name="comments" class="formTxtBox_1" style="width: 400px;"></textarea>
                            <%}else{out.print(committeComments);}%>
                        </td>
                    </tr>
                    <%//}%>
                    <%}%>
                </tbody>
            </table>
            <%if(isChairperson!=0){
                //if(wholeComList.size()==comAnsCnt || wholeComList.size()-1==comAnsCnt){
                if(committeComments==null){%>
                <div class="t-align-center t_space">
                    <label class="formBtn_1"><input name="cpsubmit" id="button3" value="Submit" type="submit" onclick="return validate();"></label>
                </div>            
            <%}else{
               //For Chairperson
               MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
               makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(session.getAttribute("userId").toString()), "userId", EgpModule.Debarment.getName(), "Viewed Debarment Request by debarment Committee CP", "");
            }}//}%>
<!--        For single comments of committe member
            Below one is commented.
-->
            <%if(isChairperson==0){if(commList.isEmpty()){%>
            <div class="t-align-center t_space">
                <label class="formBtn_1"><input name="submit" id="button3" value="Submit" type="submit" onclick="return validate();"></label>
            </div>
            <%}else{
               //For Committee Member
               MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
               makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(session.getAttribute("userId").toString()), "userId", EgpModule.Debarment.getName(), "Viewed Debarment Request by debarment Committee Member", "");
            }}%>
        </form>
        <%@include file="../resources/common/Bottom.jsp" %>        
      <%}%>
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
