<%--
    Document   : ViewNegQuestions
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<%@page import="com.cptu.egp.eps.model.table.TblNegQuery"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTables" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderColumns" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCells" %>
<jsp:useBean id="negotiationdtbean"  class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Negotiation Questions</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
          <%
            int tenderId =0;
            int negId =0;
            boolean flagNeg = false;
            String action = "";
            String msg = "";
            String type = "";
            if(request.getParameter("tenderId") != null){
                tenderId= Integer.parseInt(request.getParameter("tenderId").toString());
            }
            if(request.getParameter("negId") != null){
                negId= Integer.parseInt(request.getParameter("negId").toString());
            }
            if (request.getParameter("flag") != null) {
                if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                    flagNeg = true;
                }
            }
            if(request.getParameter("type")!=null){
                type = request.getParameter("type");
            }
            if(request.getParameter("action")!=null){
                action = request.getParameter("action");
            }
             if(flagNeg){
               if("createQuestion".equalsIgnoreCase(action)){
                    msg = "Question Posted Successfully";
               }
            }
            pageContext.setAttribute("tenderId", tenderId);
          %>
          <%@include file="../resources/common/AfterLoginTop.jsp" %>
        <div class="dashboard_div">
            
            <div class="contentArea_1">
            <div class="pageHead_1">
                Negotiation Questions
                <span class="c-alignment-right">
                    <a href="NegotiationProcess.jsp?tenderId=<%=tenderId %>" class="action-button-goback" title="Go Back To Dashboard">Go Back To Dashboard</a>
                </span>
            </div>
                
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%pageContext.setAttribute("tab", "7");%>
            <br/>
            <%@include file="/officer/officerTabPanel.jsp" %>
             <div class="tabPanelArea_1">
             <%-- Start: Common Evaluation Table --%>
                      <%@include file="/officer/EvalCommCommon.jsp" %>
                    <%-- End: Common Evaluation Table --%>
                <div class="t_space">
                        <%@include file="../resources/common/AfterLoginTSC.jsp" %>
                        </div>     
                    
              <div class="tabPanelArea_1">
            <% if(!"View".equalsIgnoreCase(type)){ %>
         
                <div class="b_space">
                    <% if(flagNeg){ %>
                    <div id="successMsg" class="responseMsg successMsg" style="display:block"><%=msg%></div>
                    <% }else{ %>
                        <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                     <% } %>
                </div>

                 
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <td width="30%" class="t-align-left ff">Negotiation Questions:</td>
                        <td width="70%">
                            <a href="AddNegQuestion.jsp?tenderId=<%=tenderId%>&negId=<%=negId%>">Post Question</a>
                        </td>
                    </tr>
                </table>
            
            <% } %>
            <!--Middle Content Table Start-->
            
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th width="4%" class="t-align-left">Sl. No</th>
                    <th width="73%" class="t-align-left">Questions</th>
                    <th width="73%" class="t-align-left">Documents Availability</th>
                    <th width="20%" class="t-align-left">Action</th>
                </tr>
                <%
                    int i=1;
                    boolean avail = false;
                    List<TblNegQuery> listQuery = negotiationdtbean.viewQueries(tenderId,negId);
                    TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    if(!listQuery.isEmpty()){
                        for(TblNegQuery Query:listQuery){
                            avail = negotiationdtbean.isDOCAvail(Query.getNegQueryId());
                %>
                <tr>
                    <td class="t-align-left"><%=i%></td>
                    <td class="t-align-left"><%=Query.getQueryText()%></td>
                    <td class="t-align-left"><% if(avail==true){ out.println("Yes"); }else{ out.println("No"); } %></td>
                    <td class="t-align-left"><a href="../resources/common/ViewQuery.jsp?tenderId=<%=tenderId%>&negId=<%=negId%>&queryId=<%=Query.getNegQueryId()%>">View</a></td>
                </tr>
                <% i++; } }else{ %>
                <tr>
                    <td colspan="5" class="t-align-center">No records found.</td>
                </tr>
                <% } %>
            </table>
            </div>
            </div>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>