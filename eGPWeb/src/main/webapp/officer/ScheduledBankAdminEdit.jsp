<%--
Document   : OrganizationAdminRegister
Created on : Oct 23, 2010, 6:13:00 PM
Author     : Sanjay
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Scheduled Bank Admin Edit</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="Include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="../resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">Development Partner Admin - Edit Details</div>
                                        <form id="frmSBAdmin" name="frmDPAdmin" method="post">
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">                                                
                                                <tr>
                                                    <td class="ff">Unique e-mail ID : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="text" id="txtEmailId" name="EmailId" />
                                                    </td>
                                                </tr>                                                
                                                <tr>

                                                    <td class="ff">Full Name : <span>*</span></td>

                                                    <td><input class="formTxtBox_1" type="text" id="txtFullName" name="FullName" maxlength="200"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">National Id : <span>*</span></td>

                                                    <td><input class="formTxtBox_1" type="text" id="txtNationalId" name="NationalId" maxlength="25"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Phone No : <span>*</span></td>
                                                    <td><input class="formTxtBox_1" type="text" name="PhoneNo" id="txtPhoneNo" maxlength="20"/>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="ff">Mobile No : <span>*</span></td>
                                                    <td><input class="formTxtBox_1" type="text" name="MobileNo" id="txtMobileNo" maxlength="20"/>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="update" id="btnUpdate" value="Update" />
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabMngUser");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
