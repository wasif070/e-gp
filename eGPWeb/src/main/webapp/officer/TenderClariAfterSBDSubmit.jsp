<%-- 
    Document   : TenderClariAfterSBDSubmit
    Created on : 12-Feb-2017, 17:46:24
    Author     : Nafiul
--%>

<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>JSP Page</title>
            <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    </head>
    <body>
        <%
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            String tenderId = "";
            String lastdate = "";
            String queryId = "0";
            if (!request.getParameter("tenderId").equals("")) {
                tenderId = request.getParameter("tenderId");
            }
            if (!request.getParameter("lastdate").equals("")) {
                lastdate = request.getParameter("lastdate");
            }
            queryId = tenderCommonService.getTenderPostQueConfig(Integer.parseInt(tenderId));
            String submitValue = "";
            if (queryId.equalsIgnoreCase("0")) {
                submitValue = "Submit";
            }else submitValue = "Update";
            
        %>
        <form id= "configtenderclari" name="configtenderclarification" method="post" action='<%=request.getContextPath()%>/TenderPostQueConfigServlet?action=saveclarification&tenderId=<%=tenderId%>&IsUpdatedBasedOnSBD=YES'>
            <input type="hidden" id ="hiddentenderid" value="<%= tenderId%>" />
            <input type="hidden" name="queryId" value="<%=queryId%>" />
            <input type="hidden" name="claricombo" id="claricomboid" value="Yes" />
            <input type="hidden" name="lastdate" id="lastdate" value="<%=lastdate%>" />
            <input type="hidden" name="submitlastdate" id="submitlastdate" value="<%=submitValue%>" />
        </form>
    </body>
    <script>document.getElementById('configtenderclari').submit();</script>
</html>
