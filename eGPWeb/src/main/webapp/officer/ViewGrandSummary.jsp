<%--
    Document   : TenderDocPrep
    Created on : 29-Nov-2010, 12:37:32 PM
    Author     : Yagnesh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderGrandSum"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderForms"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCells"%>
<%@page import="java.util.Map"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderGrandSumDetail" %>
<jsp:useBean id="grandSummary" class="com.cptu.egp.eps.web.servicebean.GrandSummarySrBean"  />

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Grand Summary</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script language="javascript" type="text/javascript">
            
              function CONVERTWORD(no,msgPlace)
              {
                no = parseFloat(no).toFixed(3);
                if(document.getElementById(msgPlace))
                {
                    //document.getElementById(msgPlace).innerHTML = DoIt(eval(no));
                     document.getElementById(msgPlace).innerHTML = CurrencyConverter(eval(no));
                }
              }
        </script>
    </head>
    <body>
        <%
            int tenderId = 0;
            int tenderSectionId = 0;
            int tenderGSId = 0;
            int pkgOrLotId = -1;
            if(request.getParameter("porlId")!=null){
                pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
            }
            boolean isEdit = false;
            if (request.getParameter("tenderId") != null) {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }
            if (request.getParameter("sectionId") != null) {
                tenderSectionId = Integer.parseInt(request.getParameter("sectionId"));
            }
            if (request.getParameter("gsId") != null) {
                tenderGSId = Integer.parseInt(request.getParameter("gsId"));
                isEdit = true;
            }
            int userId = 0;
            if(session.getAttribute("userId") != null){
                userId = Integer.parseInt(session.getAttribute("userId").toString());
            }
             String corriId=null;
            if (request.getParameter("corriId") != null && !request.getParameter("corriId").equalsIgnoreCase("") && !request.getParameter("corriId").equalsIgnoreCase("null")) 
            {
                corriId = request.getParameter("corriId");
            }

             // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType="tenderId";
            int auditId=Integer.parseInt(request.getParameter("tenderId"));
            String auditAction="View Grand Summary";
            String moduleName=EgpModule.Tender_Document.getName();
            String remarks="Officer (User id):"+session.getAttribute("userId")+" has viewed Grand Summary";
            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks); 
            
            String gsName = "";
            boolean dataExistinCoriDetailTable=false;
            java.util.Map<Integer, Integer> hmGSFormId = null;
            java.util.Map<Integer, Integer> hmGSCellId = null;
            if (isEdit) 
                {
                     gsName = grandSummary.getGrandSummaryName(tenderGSId);
                    if(corriId != null && !corriId.equalsIgnoreCase("") && !corriId.equalsIgnoreCase("null")) 
                    {
                         dataExistinCoriDetailTable=grandSummary.checkCoriExistInCoriDetailTbl(tenderGSId+"",corriId);
                    }
                     //System.out.println("==================== data temp exiswt"+dataExistinCoriDetailTable);
                     if(dataExistinCoriDetailTable)
                     {
                        // passing true will return FormId in hash map
                        hmGSFormId = grandSummary.getCorriGSFormsDtl(tenderGSId+"",corriId);
                           // System.out.println("========================================");
                            for (Map.Entry<Integer, Integer> entry : hmGSFormId.entrySet()) 
                           {
                               //System.out.println(" form id="+entry.getValue().toString());
                           }
                        // passing true will return CellId in hash map
                        hmGSCellId = grandSummary.getCorriGSFormsTblDtl(tenderGSId+"",corriId);
                            for (Map.Entry<Integer, Integer> entry : hmGSCellId.entrySet()) 
                           {
                             //  System.out.println(" table id="+entry.getKey().toString()+" cell id="+entry.getValue().toString());
                           }
                     }
                     else
                     {
                          // passing true will return FormId in hash map
                        hmGSFormId = grandSummary.getGSFormsDtl(tenderGSId, true);
                        // passing true will return CellId in hash map
                        hmGSCellId = grandSummary.getGSFormsTblDtl(tenderGSId);
                     }
                }
        %>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->
            <!--Page Content Start-->
            <div class="contentArea_1">
            <div class="pageHead_1">
                View Grand Summary
                <%if(pkgOrLotId == -1){%>
                    <span style="float: right; text-align: right;">
                        <a href="TenderDocPrep.jsp?tenderId=<%= tenderId%>" title="Tender/Proposal Doc. Preparation" class="action-button-goback">Tender Doc. Preparation</a>
                    </span>
                <%}else{%>
                    <span style="float: right; text-align: right;">
                        <a href="TenderDocPrep.jsp?tenderId=<%= tenderId%>&porlId=<%= pkgOrLotId %>" title="Tender/Proposal Doc. Preparation" class="action-button-goback">Tender Doc. Preparation</a>
                    </span>
                <%}%>
            </div>
            <%
                //pageContext.setAttribute("tenderId", tenderId);
            %>
            <%--<%@include file="../resources/common/TenderInfoBar.jsp" %>--%>
                <input type="hidden" name="tenderId" value="<%= tenderId %>" />
                <input type="hidden" name="sectionId" value="<%= tenderSectionId %>" />
                <%if(isEdit){%>
                    <input type="hidden" name="gsId" value="<%= tenderGSId %>" />
                <%}%>
                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="tableHead_1 t_space">
                    <tr>
                        <td width="200" class="ff">Grand Summary Name :</td>
                        <td><%= gsName %></td>
                    </tr>
                </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space" style="table-layout: fixed;">
                    <tr>
                        <th width="35%">Form Name</th>
                        <th width="25%">Column Name</th>
                        <!--<th width="40%" class="t-align-center">Total</th>-->
                    </tr>
                    <%
                        List<TblTenderForms> tnForms = grandSummary.getTenderPriceBidForm(tenderSectionId);
                        if(tnForms != null){
                            if(tnForms.size() > 0){
                                int k = 1;
                                for(int j=0;j<tnForms.size();j++){
                                    if(hmGSFormId.containsValue(tnForms.get(j).getTenderFormId())){
                    %>
                                    <tr>
                                        <td style="word-wrap: break-word;">
                                            <%= tnForms.get(j).getFormName() %>
                                        </td>
                                        <td style="word-wrap: break-word;">
                                            <%
                                                List<TblTenderCells> tCells = grandSummary.getColumnsForGS(tnForms.get(j).getTenderFormId());
                                                if(tCells != null){
                                                    if(tCells.size() > 0){
                                                        for(int jj=0;jj<tCells.size();jj++){
                                                            if(hmGSCellId.containsValue(tCells.get(jj).getCellId())){
                                                                //BTN->Nu. by Emtaz on 24/April/2016
                                                                String ChangeBDTtoNu = tCells.get(jj).getCellvalue().replaceAll("BTN", "Nu.");
                                                                out.print(ChangeBDTtoNu);
                                                            }
                                                        }
                                                    }
                                                    tCells = null;
                                                }
                                            %>
                                        </td>
<!--                                        <td style="word-wrap: break-word;">
                                            <input type="text" class="formTxtBox_1" name="totalValue_<%//= k %>" id="totalValue_<%//= k %>" />
                                        </td>-->
                                    </tr>
                    <%
                                        k++;
                                    }
                                }
                    %>
<!--                                <tr>
                                    <td style="word-wrap: break-word;">&nbsp;</td>
                                    <td style="word-wrap: break-word;">&nbsp;</td>
                                    <td style="word-wrap: break-word;">
                                        <input type="text" class="formTxtBox_1" id="finalTotalValue" name="finalTotalValue" onblur="CONVERTWORD(this.value,'totalToWord');"/>
                                        <br/><span id="totalToWord"></span>
                                    </td>
                                </tr>-->
                    <%
                            }else{
                    %>
                            <tr>
                                <td class="t-align-center" colspan="3">No records found</td>
                            </tr>
                    <%
                            }
                            tnForms = null;
                        }else{
                    %>
                        <tr>
                            <td class="t-align-center" colspan="3">No records found</td>
                        </tr>
                    <%
                        }
                    %>
                </table>
                <div>&nbsp;</div>
            <!--Page Content End-->
            <!--Middle Content Table End-->
            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
