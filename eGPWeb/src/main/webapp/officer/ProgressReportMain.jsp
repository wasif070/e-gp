<%-- 
    Document   : ProgressReportMain
    Created on : Dec 12, 2011, 4:29:42 PM
    Author     : shreyansh Jogi
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvAttSheetMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsDateConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPayment"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<jsp:useBean id="cmsVendorReqWccSrBean" class="com.cptu.egp.eps.web.servicebean.CmsVendorReqWccSrBean" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <script>
            function View(wpId,tenderId,lotId){
                dynamicFromSubmit("SrvPreparePR.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&frmSrv=true");
            }
            function Edit(wpId,tenderId,lotId){
                dynamicFromSubmit("SrvPreparePR.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&isedit=y&frmSrv=true");
            }
            function ViewPR(wpId,tenderId,lotId){
                dynamicFromSubmit("SrvViewProgressReport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&frmSrv=true");
            }
        </script>
        <%
            ResourceBundle bdl = null;
            CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
            bdl = ResourceBundle.getBundle("properties.cmsproperty");
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            boolean isPerformanceSecurityPaid = false;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Progress Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
        <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Progress Report</div>
                <%
                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        tenderId = request.getParameter("tenderId");
                    }
                    String userId = "";
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = session.getAttribute("userId").toString();
                    }
                    pageContext.setAttribute("tab", "10");
                    CommonService commonServicee = (CommonService) AppContext.getSpringBean("CommonService");
                    String procnaturee = commonServicee.getProcNature(request.getParameter("tenderId")).toString();
                    String strProcNaturee = "";
                    String strLotNo = "Lot No.";
                    String strLotDes = "Lot Description";
                    if("Services".equalsIgnoreCase(procnaturee))
                    {
                        strLotNo = "Package No.";
                        strLotDes = "Package Description";
                        strProcNaturee = "Consultant";
                    }else if("goods".equalsIgnoreCase(procnaturee)){
                        strProcNaturee = "Supplier";
                    }else{
                        strProcNaturee = "Contractor";
                    }
                    //String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();

                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%
                CommonService commonServiceee = (CommonService) AppContext.getSpringBean("CommonService");
                String procnatureee = commonServiceee.getProcNature(request.getParameter("tenderId")).toString();
                List<Object[]> lotObj = commonServiceee.getLotDetails(request.getParameter("tenderId"));
                Object[] objj = null;
                if(lotObj!=null && !lotObj.isEmpty())
                {
                    objj = lotObj.get(0);
                    pageContext.setAttribute("lotId", objj[0].toString());
                }%>
                <%@include file="../resources/common/ContractInfoBar.jsp" %>
                <div>&nbsp;</div>

                <%@include file="officerTabPanel.jsp"%>
                <div class="tabPanelArea_1">
                    <%
                        pageContext.setAttribute("TSCtab", "2");

                    %>
                    <%@include  file="../resources/common/CMSTab.jsp"%>
                    <div class="tabPanelArea_1">
                        <% if (request.getParameter("msg") != null) {
                         if ("updated".equalsIgnoreCase(request.getParameter("msg"))) {
                    %>
                    <div class='responseMsg successMsg t_align_left'>Action performed successfully</div>
                    <%}}%>
                        <div align="center">
                            <%
                                ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                for (Object[] obj : issueNOASrBean.getNOAListing(Integer.parseInt(tenderId))) {
                                    boolean isWCC = service.isWorkCompleteOrNot(obj[0].toString());
                                    boolean isConTer = service.isContractTerminatedOrNot(obj[0].toString());
                                    String wpId = service.getWpIdForService(Integer.parseInt(obj[0].toString()));
                                    boolean flaB = service.checkForPrEntry(Integer.parseInt(wpId));
                                    boolean flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(obj[0].toString()));
                                    boolean checkitemreceived = service.checkForItemFullyReceivedOrNotByWpId(Integer.parseInt(wpId));
                            %>

                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <td class="ff" width="15%" class="t-align-left"><%=strLotNo%></td>
                                    <td   class="t-align-left"><%=obj[6]%></td>
                                </tr>
                                <tr>
                                    <td class="ff" width="12%"  class="t-align-left"><%=strLotDes%></td>
                                    <td  class="t-align-left"><%=obj[7]%></td>
                                </tr>
                            </table>
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">    
                                <tr>
                                    <td colspan="2">
                                        <span style="float: left; text-align: left;font-weight:bold;">
                                            <a href="ViewTotalInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=obj[0].toString()%>" class="">Financial Progress Report</a>
                                        </span>
                                    </td>
                                </tr>    
                            </table>
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <td class="ff" width="15%" class="t-align-left">Attendance and Progress Report</td>

                                    <td  class="t-align-left">
                                        <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                            <tr>
                                                <th width="15%" class="t-align-left">Sr.No</th>
                                                <th class="t-align-left">Attendance and Progress Report</th>
                                                <th class="t-align-left">Month-Year</th>
                                                <th class="t-align-left">Created Date</th>
                                                <th class="t-align-left">Action</th>
                                                <%
                                                    List<TblCmsSrvAttSheetMaster> os = cmss.getAllAttSheetDtls(Integer.parseInt(tenderId));
                                                    List<Object> oss = cmss.getWpIdFrmLotId(Integer.parseInt(obj[0].toString()));
                                                    boolean flag = true;


                                                    MonthName monthName = new MonthName();
                                                    if (os != null && !os.isEmpty()) {
                                                        int cc = 1;
                                                        for (TblCmsSrvAttSheetMaster master : os) {
                                                            if (oss != null && !oss.isEmpty()) {
                                                                flag = cmss.editLinkForAttOrNot(oss.get(0).toString(), master.getAttSheetId());
                                                            }
                                                %>
                                            <tr>
                                                <td width="12%" class="t-align-left"><%=cc%></td>
                                                <td class="t-align-left">Attendance and Progress Report - <%=cc%></td>
                                                <td class="t-align-left"><%=monthName.getMonth(master.getMonthId()) + " - " + master.getYear()%></td>
                                                <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec(master.getCreatedDt())%></td>

                                                <td class="t-align-left"><a href="ViewSheetdtls.jsp?sheetId=<%=master.getAttSheetId()%>&tenderId=<%=tenderId%>">View</a>
                                                    <%
                                                        if ("pending".equalsIgnoreCase(cmss.checkAttStatus(master.getAttSheetId() + ""))) {%>&nbsp;|&nbsp;
                                                    <a href="EditSheetdtls.jsp?sheetId=<%=master.getAttSheetId()%>&tenderId=<%=tenderId%>&lotId=<%=obj[0].toString()%>">Approval/Rejection</a>
                                                    <%}%>
                                                    &nbsp;|&nbsp;<a href="AttanshtUploadDoc.jsp?tenderId=<%=tenderId%>&wpId=0&keyId=<%=master.getAttSheetId()%>&docx=AttSheet&module=PR">Upload / Download Document</a>
                                                </td>
                                            </tr>

                                            <%cc++;
                                }
                            } else {%>
                                            <tr>
                                                <td colspan="5">No records found</td>
                                            </tr>
                                            <%}%>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <td width="15%" class="t_align_left ff">
                                        Reimbursable Expenses 
                                    </td>
                                    <td class="t_align_left">
                                        <%if (flaB) {
                                        %>

                                        <a href="#" onclick="Edit(<%=wpId%>,<%=tenderId%>,<%=obj[0]%>);">Edit</a>&nbsp|&nbsp;
                                        <a href="#" onclick="ViewPR(<%=wpId%>,<%=tenderId%>,<%=obj[0]%>);">View</a>
                                        <%} else {
                                            if (flags) {
                                        %>

                                        <%if (!checkitemreceived) {%>
                                        <%if (isWCC) {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.PR.workcomplete")%>','Progress Report', function(RetVal) {
                                        });">Prepare</a>&nbsp;|&nbsp;
                                        <%} else if (isConTer) {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.PR.contractTerminate")%>','Progress Report', function(RetVal) {
                                    });">Prepare</a>&nbsp;|&nbsp;
                                        <%} else {%>
                                        <a href="#" onclick="View(<%=wpId%>,<%=tenderId%>,<%=obj[0]%>);">Prepare</a>&nbsp;|&nbsp;
                                        <%}
                                            }%>

                                        <%} else {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.DateConfigPending")%>','Progress Report', function(RetVal) {
                                });">Prepare</a>&nbsp;|&nbsp;
                                        <%}%>
                                        <a href="#" onclick="ViewPR(<%=wpId%>,<%=tenderId%>,<%=obj[0]%>);">View</a>
                                        <%}%>
                                    </td>
                                </tr>
                            </table>
                            <%
                                List<Object[]> objectlist =    issueNOASrBean.getDetailsNOAforInvoice(Integer.parseInt(tenderId), Integer.parseInt(obj[0].toString()));
                                 if(!objectlist.isEmpty()){
                                     Object[] object = objectlist.get(0);
                                     if (issueNOASrBean.isAvlTCS((Integer) object[1])) {
                                             int contractSignId = 0;
                                             contractSignId = issueNOASrBean.getContractSignId();
                                             CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
                                             cmsWcCertificateServiceBean.setLogUserId(userId);
                                             TblCmsWcCertificate tblCmsWcCertificate =
                                                     cmsWcCertificateServiceBean.getCmsWcCertificateForContractSignId(contractSignId);
                                             out.print("<table cellspacing='0' class='tableList_1' width='100%'>");
                                             if (tblCmsWcCertificate == null) {
                                                 out.print("<tr><td class='ff' width='20%' colspan='2' class='t-align-left'><div  class='responseMsg noticeMsg t-align-left'>"
                                                 + "On receiving the request of Issue Work Completion Certificate from "+strProcNaturee+" ");
                                                 out.print("system will display a link 'Issue Work Completion Certificate' for further processing</div></td></tr>");
                                             }
                                             out.print("<tr>");
                                             out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                             out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                             CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                                             cmsContractTerminationBean.setLogUserId(userId);
                                             //if(!isConTer)
                                             //{
                                                if(tblCmsWcCertificate == null){
                                                    if(cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(obj[0].toString()),0)){
                                                        out.print("<a href ='WorkCompletionCertificate.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                             + "&contractSignId=" + contractSignId + "&lotId=" + obj[0].toString() + "'>Issue Work Completion Certificate</a>&nbsp;&nbsp;&nbsp;&nbsp;");
                                                    }
                                                 }
                                                 if(tblCmsWcCertificate != null){
                                                    if(cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(obj[0].toString()),0) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                         out.print("<a href ='WorkCompletionCertificate.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                         + "&contractSignId=" + contractSignId + "&lotId=" + obj[0].toString() + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()+"'>Issue Work Completion Certificate</a>&nbsp;&nbsp;&nbsp;&nbsp;");
                                                    }
                                                    out.print("<a href ='ViewWCCertificate.jsp?wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                         + "&tenderId=" + tenderId + "&lotId=" + obj[0].toString() + "'>View Work Completion Certificate</a>");
                                                    //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                   // if(!"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                            out.print("&nbsp|&nbsp<a href ='WCCUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                 + "&contractSignId=" + contractSignId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                                 + "'>Upload / Download Files</a>");
                                                  //  }
                                                }
                                             //}else{
                                             //    out.print("<a href='#' onclick='CTerminationMsg()'>Issue Work Completion Certificate</a>");
                                             //}
                                             out.print(" </td>");
                                             out.print(" </tr>");
                                             /*out.print("<td class='ff' width='20%'  class='t-align-left'> Contract Termination </td>");
                                             out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                             if (tblCmsContractTermination == null) {
                                                 out.print("<a href ='ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                         + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Terminate</a> ");
                                             } else {
                                                 out.print("<a href ='ViewContractTermination.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                         + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>ViewContractTermination</a>");
                                                 out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                 out.print("<a href ='ContractTeminationUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                         + "&contractSignId=" + contractSignId + "&contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                         + "'>Upload/Download Files</a>");
                                             }*/
                                             out.print("</table>");
                                         }
                                 }
                            %>        

                            <%}%>

                        </div>
                    </div></div>
            </div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script>
var headSel_Obj = document.getElementById("headTabTender");
if(headSel_Obj != null){
headSel_Obj.setAttribute("class", "selected");
}
    </script>
</html>
<%
    issueNOASrBean = null;
%>
