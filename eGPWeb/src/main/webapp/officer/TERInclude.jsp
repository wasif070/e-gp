<%-- 
    Document   : TERInclude
    Created on : Aug 10, 2011, 10:44:48 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvaluationMatrix"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%
            String tenderId = request.getParameter("tenderId");
            String lotId = request.getParameter("lotId");
            String roundId = request.getParameter("roundId");
            String evalType = request.getParameter("evalType");

            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            String pMethod = commonService.getProcMethod(tenderId).toString();
            String eventType = commonService.getEventType(tenderId).toString();
            String procNature = commonService.getProcNature(tenderId).toString();
            boolean is2Env = false;
            String T1L1String = "&roundId=0";
            CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            List<SPCommonSearchDataMore> envDataMores = dataMore.geteGPData("GetTenderEnvCount", tenderId);
            if(envDataMores!=null && (!envDataMores.isEmpty())){
                if(envDataMores.get(0).getFieldName1().equals("2")){
                    is2Env = true;
                }
                if(envDataMores.get(0).getFieldName2().equals("3")){
                    //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                    envDataMores = dataMore.geteGPDataMore("GetT1L1RoundIdByL1RoundId", tenderId);
                        if (envDataMores != null && (!envDataMores.isEmpty())) {
                            T1L1String = "&roundId="+envDataMores.get(0).getFieldName1();
                    }
                }
            }            
            String repLabel = null;
            if (procNature.equals("Services")) {
                repLabel = "Proposal";
            } else {
                repLabel = "Tender";
            }
            EvaluationMatrix matrix = new EvaluationMatrix();
            boolean[] cases = matrix.getEvaluationCase(procNature, eventType, pMethod, is2Env);
            boolean isCase1 = cases[0];
            boolean isCase2 = cases[1];
            boolean isCase3 = cases[2];
            matrix = null;
            cases = null;
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            EvaluationService evalService = (EvaluationService)AppContext.getSpringBean("EvaluationService");
            int evalCount = evalService.getEvaluationNo(Integer.parseInt(tenderId));
            List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("GetLotPkgDetail", tenderId, lotId);

            String lotPkgNo = null;
            String lotPkgDesc = null;
            if ("0".equals(lotId)) {
                lotPkgNo = "Package No.";
                lotPkgDesc = "Package Description";
            } else {
                lotPkgNo = "Lot No.";
                lotPkgDesc = "Lot Description";
            }
%>
<tbody>
<!--    <tr>
        <td width="20%" class="t-align-left ff"><%//=lotPkgNo%> :</td>
        <td width="80%" class="t-align-left"><%//=tenderLotList.get(0).getFieldName1()%>
            <input type="hidden" value="<%//=tenderLotList.get(0).getFieldName1()%>" name="lotPckNoTxt"/>
        </td>
    </tr>-->
<!--    <tr>
        <td class="t-align-left ff"><%//=lotPkgDesc%> :</td>
        <td class="t-align-left"><%//=tenderLotList.get(0).getFieldName2()%>
            <input type="hidden" value="<%//=tenderLotList.get(0).getFieldName2()%>" name="lotPckDescTxt"/>
        </td>
    </tr>-->
    <!--All Evaluation reports are hide for the 1st phase-->
     <% if(evalCount>0){ %>
<!--    <tr>
        <td class="t-align-left ff">Previous Evaluation Report</td>
        <td class="t-align-left">
            <a href="<%//=request.getContextPath()%>/officer/ReEvaluation.jsp?tenderId=<%//=tenderId%>" target="_blank">View</a>
        </td>
    </tr>-->
    <%}%>
<!--    <tr>
        <td class="t-align-left ff"><%//=repLabel%> Evaluation Report 1</td>
        <td>
            <a href="<%//=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%//=tenderId%>&lotId=<%//=lotId%>&isview=y&stat=<%//=evalType%>&sign=y&in=y&evalCount=<%//=evalCount%>" target="_blank">View</a>
        </td>
    </tr>-->
    <%if(!procNature.equalsIgnoreCase("Services")){%>
<!--    <tr>
        <td class="t-align-left ff"><%//=repLabel%> Evaluation Report 2</td>
        <td>
            <a href="<%//=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%//=tenderId%>&lotId=<%//=lotId%>&isview=y&stat=<%//=evalType%>&sign=y&in=y&evalCount=<%//=evalCount%>" target="_blank">View</a>
        </td>
    </tr>-->
    <%}%>
    <%
                if (isCase2 || isCase3) {
                    List<SPCommonSearchDataMore> TERALL = dataMore.getCommonSearchData("isTERSignedByAll", tenderId, lotId, "4", "'TER1','TER2','TER3','TER4'", roundId,String.valueOf(evalCount));
                    if (TERALL != null && (!TERALL.isEmpty()) && TERALL.get(0).getFieldName1().equals("1")) {
    %>
    <%if(procNature.equalsIgnoreCase("Services")){%>
<!--    <tr>
        <td class="t-align-left ff"><%//=repLabel%> Evaluation Report 2</td>
        <td>
            <a href="<%//=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%//=tenderId%>&lotId=<%//=lotId%>&isview=y&stat=<%//=evalType%>&sign=y&in=y&evalCount=<%//=evalCount%>&rId=<%//=roundId%><%//=T1L1String%>" target="_blank">View</a>
        </td>
    </tr>-->
    <%}%>
<!--    <tr>
        <td class="t-align-left ff"><%//=repLabel%> Evaluation Report 3</td>
        <td>
            <a href="<%//=request.getContextPath()%>/officer/TER3.jsp?tenderid=<%//=tenderId%>&lotId=<%//=lotId%>&rId=<%//=roundId%>&in=y" target="_blank">View</a> 
            Dohatec start : 22 April 2015 : 11:20 AM : Md. Wahid Abdullah  
            Issue Details : TER1 and TER2 have save as PDF option but TER3(Financial Evaluation) and TER3(Final evaluation) don't have "Save as PDF" option. Thus TER3 and TER4 can't be downloaded as PDF 
           <a href="<%//=request.getContextPath()%>/officer/TER3.jsp?tenderid=<%//=tenderId%>&lotId=<%//=lotId%>&isview=y&rId=<%//=roundId%>&in=y&evalCount=<%//=evalCount%>" target="_blank">View</a>
            Dohatec end : 22 April 2015 : 11:20 AM : Md. Wahid Abdullah  
        </td>
    </tr>
    <tr>
        <td class="t-align-left ff"><%//=repLabel%> Evaluation Report 4</td>
        <td>
            <a href="<%//=request.getContextPath()%>/officer/TER4.jsp?tenderid=<%//=tenderId%>&lotId=<%//=lotId%>&rId=<%//=roundId%>&in=y" target="_blank">View</a> 
            Dohatec start : 22 April 2015 : 11:20 AM : Md. Wahid Abdullah  
            Issue Details : TER1 and TER2 have save as PDF option but TER3(Financial Evaluation) and TER3(Final evaluation) don't have "Save as PDF" option. Thus TER3 and TER4 can't be downloaded as PDF 
           <a href="<%//=request.getContextPath()%>/officer/TER4.jsp?tenderid=<%//=tenderId%>&lotId=<%//=lotId%>&isview=y&rId=<%//=roundId%>&in=y&evalCount=<%//=evalCount%>" target="_blank">View</a>
            Dohatec end : 22 April 2015 : 11:20 AM : Md. Wahid Abdullah  
        </td>
    </tr>-->
    <%}
                }%>
</tbody>