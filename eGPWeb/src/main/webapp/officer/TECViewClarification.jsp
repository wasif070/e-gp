<%-- 
    Document   : TSEViewClarification
    Created on : Dec 15, 2010, 4:18:09 PM
    Author     : rajesh
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Officer - Evaluation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <title>Committe Evalution</title>
        <!--jalert -->

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" >
            function checkTender(){
                jAlert("there is no Tender Notice"," Alert ", "Alert");
            }


            function checkdefaultconf(){
                jAlert("Default workflow configuration hasn't crated"," Alert ", "Alert");
            }
            function checkDates(){
                jAlert(" please configure dates"," Alert ", "Alert");
            }


        </script>

       
    </head>
    <body>

        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="pageHead_1">Tender Dashboard</div>
        <%
                    // Variable tenderId is defined by u on ur current page.
                    pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                    //pageContext.setAttribute("TSCtab", "1");
        %>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>

        <div class="tabPanelArea_1">
           
<div>&nbsp;</div>
            <div class="tabPanelArea_1">
                <%-- Sub Panal --%>
                <%
                            pageContext.setAttribute("TSCtab", "2");
                %>
                <%@include  file="../resources/common/AfterLoginTSC.jsp"%>
                <table width="100%" cellspacing="0" cellpadding="5" border="0"  class="tableList_1">
                    <%
                                TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                List<SPTenderCommonData> checkEval2 = tenderCommonService1.returndata("GetReportDetails", request.getParameter("rcid"),null);
                                if (!checkEval2.isEmpty()) {
                                    String responsetxt=checkEval2.get(0).getFieldName5();
                    %>
                    <tr>

                        <td class="t-align-left ff" width="16%">Query :</td>
                        <td class="t-align-left" width="84%">
                            <%=checkEval2.get(0).getFieldName1()%>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff" width="16%">Expected Date for Response :</td>
                        <td class="t-align-left" width="84%">
                            <%=checkEval2.get(0).getFieldName2()%>
                        </td>
                    </tr>
                    <%if(!responsetxt.equals("")){%>
                    <tr>

                        <td class="t-align-left ff" width="16%">Response :</td>
                        <td class="t-align-left" width="84%">
                            <%=checkEval2.get(0).getFieldName5()%>
                        </td>
                    </tr>
                    <tr>

                        <td class="t-align-left ff" width="16%">Response Date:</td>
                        <td class="t-align-left" width="84%">
                            <%=checkEval2.get(0).getFieldName6()%>
                        </td>
                    </tr>
                    <%}%>
                    <%}%>

                </table>
            </div>
        </div>
        <br/>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
