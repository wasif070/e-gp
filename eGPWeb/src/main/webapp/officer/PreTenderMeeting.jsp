<%-- 
    Document   : PreTenderMeeting
    Created on : Nov 29, 2010, 5:27:38 PM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<jsp:useBean id="dateUtil" class="com.cptu.egp.eps.web.utility.DateUtils" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.Calendar" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Pre – Tender Meeting</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <%--<script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" >
            function checkTender(){
                jAlert("there is no Tender Notice"," Alert ", "Alert");
            }
            function checkdefaultconf(){
                jAlert("Default workflow configuration hasn't crated"," Alert ", "Alert");
            }
            function checkDates(){
                jAlert(" please configure dates"," Alert ", "Alert");
            }
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
                <%
                            boolean flag = false;
                            String replyAction = "";
                            String msg = "";

                            if (request.getParameter("flag") != null) {
                                if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                                    flag = true;
                                }
                            }

                            if (request.getParameter("replyAction") != null) {
                                replyAction = request.getParameter("replyAction");
                            }

                            int userId = 0;
                            byte suserTypeId = 0;
                            int tenderId = 20;

                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderid") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderid"));
                            }
                            if (request.getParameter("tenderId") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }


                            preTendDtBean.setUserId(userId);
                            preTendDtBean.setTenderId(tenderId);
                            List<SPTenderCommonData> getPreBidDates = preTendDtBean.getDataFromSP("GetPrebidDate", tenderId, 0);
                            //List<SPTenderCommonData>  getPendingQuery = preTendDtBean.getDataFromSP("GetPendingQuery",tenderId,0);
                            //List<SPTenderCommonData> getRepliedQuery = preTendDtBean.getDataFromSP("GetRepliedQuery",tenderId,0);
                            //List<SPTenderCommonData> getHoldQuery = preTendDtBean.getDataFromSP("GetHoldQuery",tenderId,0);
                            // List<SPTenderCommonData> getIgnoreQuery = preTendDtBean.getDataFromSP("GetIgnoreQuery",tenderId,userId);

                            List<SPTenderCommonData> checkPE = preTendDtBean.getDataFromSP("CheckPE", tenderId, 0);
                            boolean isPreBidPublished = preTendDtBean.isPrebidPublished(tenderId);



                            String preBidStartDate = "";
                            String preBidEndDate = "";

                            if (getPreBidDates.size() > 0) {
                                if (getPreBidDates.get(0).getFieldName1() != null) {
                                    preBidStartDate = getPreBidDates.get(0).getFieldName1();
                                }
                                if (getPreBidDates.get(0).getFieldName2() != null) {
                                    preBidEndDate = getPreBidDates.get(0).getFieldName2();
                                }
                            }
                            try {
                                String currdate = new SimpleDateFormat("dd-MMM-yyyy HH:mm").format(new java.util.Date(new Date().getTime()));

                                Date currentdate = dateUtil.gridStrToDateWithoutSec(currdate);
                                Date enddate = dateUtil.gridStrToDateWithoutSec(preBidEndDate);
                                if (currentdate.equals(enddate) || currentdate.after(enddate)) {

                                    String reqURL = request.getRequestURL().toString();
                                    reqURL = reqURL.replace("officer/PreTenderMeeting", "resources/common/PreTenderQueRep");
                                    String reqQuery = "tenderId=" + tenderId + "&viewType=Prebid";
                                    String folderName = pdfConstant.PRETENDERMEETING + "_QueAns";
                                    String genId = Integer.toString(tenderId);
                                    pdfCmd.genrateCmd(reqURL, reqQuery, folderName, genId);

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            boolean isUserISPE = true;
                            if (checkPE.size() > 0) {
                                if (userId == Integer.parseInt(checkPE.get(0).getFieldName1())) {
                                    isUserISPE = true;
                                }
                            }


                            List<SPTenderCommonData> getConfigPrebid = preTendDtBean.getDataFromSP("GetConfigPrebid", tenderId, 0);


                            java.text.SimpleDateFormat sd = new java.text.SimpleDateFormat("dd-MMM-yyyy HH:mm");
                            java.util.Calendar calendar = java.util.Calendar.getInstance();
                            java.util.Calendar currentDate = java.util.Calendar.getInstance();

                            int preBidPublishDate = 0;
                            java.util.Date edate = null;
                            java.util.Date cdate = null;
                            cdate = sd.parse(sd.format(currentDate.getTime()));
                            if(preBidEndDate!=null && !preBidEndDate.equalsIgnoreCase("") )
                            {
                                edate = sd.parse(preBidEndDate);
                            }

                            if (getConfigPrebid.size() > 0) {
                                if (getConfigPrebid != null) {
                                    if (getPreBidDates.size() > 0) {
                                        if (getPreBidDates != null) {
                                            if (getPreBidDates.get(0).getFieldName2() != null) {
                                                calendar.setTime(sd.parse(getPreBidDates.get(0).getFieldName2()));
                                                edate = sd.parse(getPreBidDates.get(0).getFieldName2());
                                                if (getConfigPrebid.get(0).getFieldName4() != null) {
                                                    preBidPublishDate = Integer.parseInt(getConfigPrebid.get(0).getFieldName4());
                                                    calendar.add(Calendar.DATE, +preBidPublishDate);
                                                }
                                            }
                                        }
                                    }
                                }
                            }



                            if (flag) {
                                if ("reply".equalsIgnoreCase(replyAction)) {
                                    msg = " Query replied successfully";
                                } else if ("hold".equalsIgnoreCase(replyAction)) {
                                    msg = "Query put on hold";
                                } else if ("ignore".equalsIgnoreCase(replyAction)) {
                                    msg = " Query Ignored";
                                } else if ("publishQuery".equalsIgnoreCase(replyAction)) {
                                    msg = "Pre Tender Meeting Documents published successfully";
                                }
                            }
                %>
                <div class="dashboard_div">

                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">

                        <%
                                    // Variable tenderId is defined by u on ur current page.
                                    pageContext.setAttribute("tenderId", tenderId);
                                    pageContext.setAttribute("tab", "4");

                                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    //Regarding workflow
                                    int uid = 0;
                                    if (session.getAttribute("userId") != null) {
                                        Integer ob1 = (Integer) session.getAttribute("userId");
                                        uid = ob1.intValue();
                                    }
                                    WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                                    short eventid = 3;
                                    int objectid = 0;
                                    short activityid = 3;
                                    int childid = 1;
                                    short wflevel = 1;
                                    int initiator = 0;
                                    int approver = 0;
                                    boolean isInitiater = false;
                                    boolean evntexist = false;
                                    String fileOnHand = "No";
                                    String checkperole = "No";
                                    String chTender = "No";
                                    boolean ispublish = false;
                                    boolean iswfLevelExist = false;
                                    boolean isconApprove = false;
                                    String pretenderstatus = "";

                                    // chalapathi

                                    objectid = tenderId;
                                    childid = objectid;
                                    evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
                                    List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
                                    // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);
                                    String donor = "";
                                    donor = workFlowSrBean.isDonorReq(String.valueOf(eventid),
                                            Integer.valueOf(objectid));
                                    String[] norevs = donor.split("_");
                                    int norevrs = -1;

                                    String strrev = norevs[1];
                                    if (!strrev.equals("null")) {

                                        norevrs = Integer.parseInt(norevs[1]);
                                    }
                                    if (tblWorkFlowLevelConfig.size() > 0) {
                                        Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                        iswfLevelExist = true;
                                        while (twflc.hasNext()) {
                                            TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                            TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                            if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                                                fileOnHand = workFlowlel.getFileOnHand();
                                            }
                                            if (workFlowlel.getWfRoleId() == 1) {
                                                initiator = lmaster.getUserId();
                                            }
                                            if (workFlowlel.getWfRoleId() == 2) {
                                                approver = lmaster.getUserId();
                                            }
                                            if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                                                isInitiater = true;
                                            }

                                        }

                                    }
                                    String userid = "";
                                    if (session.getAttribute("userId") != null) {
                                        Object obj = session.getAttribute("userId");
                                        userid = obj.toString();
                                    }

                                    String field1 = "CheckPE";
                                    List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, userid, "");

                                    if (editdata.size() > 0) {
                                        checkperole = "Yes";
                                    }
                                    // for publish link

                                    List<SPTenderCommonData> checkAmendment = tenderCommonService1.returndata("PreTenderMetWorkFlowStatus", Integer.toString(tenderId), "'Pending'");

                                    if (checkAmendment.size() > 0) {
                                        chTender = "Yes";
                                    }
                                    List<SPTenderCommonData> publist = tenderCommonService1.returndata("PreTenderMetWorkFlowStatus", Integer.toString(tenderId), "'Approved','Conditional Approval'");

                                    if (publist.size() > 0) {
                                        ispublish = true;
                                        SPTenderCommonData spTenderCommondata = publist.get(0);
                                        pretenderstatus = spTenderCommondata.getFieldName1();
                                        String conap = spTenderCommondata.getFieldName2();
                                        if (conap.equalsIgnoreCase("Conditional Approval")) {
                                            isconApprove = true;
                                        }
                                    }
                                    boolean isdocExist = false;
                                    List<SPTenderCommonData> getPreBidDocumentDates = null;
                                    getPreBidDocumentDates = preTendDtBean.getDataFromSP("PreTenderMetDocs", tenderId, 0);
                                    if (getPreBidDocumentDates.size() > 0) {
                                        isdocExist = true;
                                    }

                                    
                                    String filereply = "";
                                    filereply = request.getParameter("msg");
                        %>
                        <div class="pageHead_1">Pre – Tender Meeting</div>

                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <%
                               String subDate = pageContext.getAttribute("tendinfoSubDate").toString();
                                Date subDt = DateUtils.gridStrToDateWithoutSec(subDate);
                                
                        %>
                        <div class="t_space">
                            <%@include  file="officerTabPanel.jsp"%>
                        </div>
                        <%--  <div>&nbsp;</div>--%>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="4">
                                    <% if (filereply != null && !filereply.contains("not")) {%>
                                    <div class="responseMsg successMsg">File processed successfully</div>
                                    <%  } else if (filereply != null && filereply.contains("not")) {%>
                                    <div class="responseMsg errorMsg">File has not processed successfully</div>
                                    <%  }%>
                                    <table width="100%" cellspacing="0" class="tableList_1">
                                        <tr>
                                            <td colspan="6" >
                                                <div>
                                                    <%if (flag) {%>
                                                    <div id="successMsg" class="responseMsg successMsg" style="display:block"><%=msg%></div>
                                                    <%} else {%>
                                                    <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                                                    <%}%>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="15%" class="t-align-left ff">Meeting Start Date and Time :</td>
                                            <td width="20%" class="t-align-left"><%=preBidStartDate%>  </td>
                                            <td width="15%" class="t-align-left ff">Meeting End Date and Time :</td>
                                            <td width="20%" class="t-align-left"><%=preBidEndDate%> </td>
                                            <td width="15%" class="t-align-left ff">View All Queries :</td>
                                            <td width="10%"
                                                <% List<SPTenderCommonData> getRepliedQuery = null;
                                                            getRepliedQuery = preTendDtBean.getDataFromSP("GetRepliedQuery", tenderId, 0);
                                                            if ( getRepliedQuery.size() > 0) {%>
                                                class="t-align-left"><a href="<%=request.getContextPath()%>/resources/common/PreTenderQueRep.jsp?tenderId=<%=tenderId%>&viewType=Prebid" >View</a>
                                                <%} else {%>
                                                class="t-align-center">-
                                                <%}%>
                                            </td>
                                        </tr>
                                                <%--   <tr>
                                            <td width="25%" class="t-align-left ff">Workflow</td>
                                            <td width="60%" colspan="5" class="t-align-left">
                                                <%          String currdate = new SimpleDateFormat("dd-MMM-yyyy HH:mm").format(new java.util.Date(new Date().getTime()));
                                                            Date currentdate = dateUtil.gridStrToDateWithoutSec(currdate);
                                                            Date enddate = dateUtil.gridStrToDateWithoutSec(preBidEndDate);
                                                           // if (currentdate.after(enddate) || currentdate.equals(enddate)) { %>
                                                <%if (evntexist) {
                                                if ("yes".equalsIgnoreCase(fileOnHand) && "Yes".equals(checkperole) && ispublish == false && isInitiater == true) {
                                                %>
                                                <!--<a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=900'>Edit</a>-->

                                                &nbsp;|&nbsp;
                                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=901">View</a>
                                                &nbsp;|&nbsp;

                                                <% } else if (iswfLevelExist == false && "Yes".equals(checkperole) && ispublish == false) {
                                                %>

                                                <!--<a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&parentLink=900'>Edit</a>-->
                                                &nbsp;|&nbsp;
                                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=900">View</a>
                                                &nbsp;|&nbsp;
                                                <%  } else if (iswfLevelExist == false && !"Yes".equalsIgnoreCase(checkperole)) {%>

                                                <label>Workflow yet not configured</label>
                                                <%                                              } else if (iswfLevelExist == true) {%>
                                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>&parentLink=900">View</a>
                                                &nbsp;|&nbsp;
                                                <%
                                                                                                                    }
                                                                                                                } else if (ispublish == false && "Yes".equals(checkperole)) {
                                                                                                                    List<CommonAppData> defaultconf = workFlowSrBean.editWorkFlowData("WorkFlowRuleEngine", Integer.toString(eventid), "");
                                                                                                                    if (defaultconf.size() > 0) {
                                                %>
                                                <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&parentLink=87&submit=Submit">Create</a>
                                                <% } else {%>
                                                <a  href="#" onclick="checkdefaultconf()">Create</a>
                                                <%                         }
                                                                                                                } else if (iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {%>
                                                <label>Workflow yet not configured</label>
                                                <% }
                                                                                                                if (isInitiater == true && (initiator != approver || (initiator == approver && norevrs > 0)) && ispublish == false && "Yes".equalsIgnoreCase(fileOnHand)) {

                                                                                                                    if ("Yes".equalsIgnoreCase(chTender) && isdocExist == true && (edate != null && cdate.after(edate))) {%>
                                                <a href="FileProcessing.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=objectid%>&eventid=<%=eventid%>&fromaction=pretender" >Process file in Workflow</a>&nbsp;|&nbsp;
                                                <% }
                                                                                                                }
                                                                                                                if (iswfLevelExist) {%>
                                                <a href="workFlowHistory.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&userid=<%=uid%>&fraction=pretender&parentLink=902" >View Workflow History</a>

                                                <% }
                                                            /*}else{ */
                                                %>
                <!--    class="t-align-center">--->
                                                <%// } %>

                                            </td>
                                        </tr> --%>
                                    </table>
                                </td>
                            </tr>

                        </table>
                        <ul class="tabPanel_1 t_space">
                            <li><a href="javascript:void(0);" class="sMenu" id="hrfPending" onclick="getQueryData('GetPendingQuery');" >Pending</a></li>
                            <li><a href="#" id="hrfReply" onclick="getQueryData('GetRepliedQuery');">Replied</a></li>
                            <li><a href="#" id="hrfHold" onclick="getQueryData('GetHoldQuery');">Hold</a></li>
                            <li><a href="#" id="hrfIgnore" onclick="getQueryData('GetIgnoreQuery');">Ignored</a></li>
                        </ul>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td>
                                    <div id="queryData">

                                    </div>
                                </td>
                            </tr>
                        </table>
                        <%--<table width="100%" cellspacing="0" class="tableList_1 t_space">
                          <tr>
                            <th width="9%" class="t-align-left">S. No.</th>
                            <th class="t-align-left" width="66%">Query</th>
                            <th class="t-align-left" width="16%">Action</th>
                          </tr>
                            <tbody id="tbody0" >

                  </tbody>
          </table>--%>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <td width="68%" class="t-align-left ff">
                  	Upload Pre Tender Meeting Document :
                                </td>
                                <td width="32%" class="t-align-left">
                                    <%
                                                if ((pretenderstatus.equalsIgnoreCase("pending") || pretenderstatus.equals("")) && ispublish == false && isInitiater == true && fileOnHand.equalsIgnoreCase("Yes") && (edate != null && cdate.after(edate)) && (cdate.before(subDt))) {%>
                                    <a href="<%=request.getContextPath()%>/officer/PreTenderMetDocUpload.jsp?tenderId=<%=tenderId%>&prebidStatus=<%=pretenderstatus%>&workflowStatus=<%=ispublish%>">Upload</a> &nbsp; | &nbsp;
                                    <%} else if (iswfLevelExist == false) {%>

                                    <label>Workflow yet not configured</label>
                                    <%                                       }
                                        if ((ispublish == true && isInitiater == true && !pretenderstatus.equalsIgnoreCase("Approved") && (edate != null && cdate.after(edate))) || (!pretenderstatus.equalsIgnoreCase("Approved") && isInitiater == true && (initiator == approver && norevrs == 0) && initiator != 0 && approver != 0 && (edate != null && cdate.after(edate)))) {%>
                                    <a href="PublishPreTenderDoc.jsp?tenderId=<%=tenderId%>">Publish</a>
                                    <% }%>
                                </td>
                            </tr>
                        </table>

                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr><td>
                                    <div id="docData">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <%if (preBidPublishDate > 0) {
                                        if (cdate.after(edate)) {
                                            if (!isPreBidPublished) {%>
                        <table width="100%" cellspacing="0" class="t_space">
                            <tr><td>
                                    <div id="noticeMsg" class="responseMsg noticeMsg" style="display:block">Pre-Tender Meeting document should be published till <%=new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").format(calendar.getTime())%></div>
                                </td>
                            </tr>
                        </table>
                        <%      }
                                        }
                                    }
                        %>

                        <div>&nbsp;</div>

                    </div>
                </div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>

    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<form id="form2" method="post">
</form>
<script>
    function getDocData(){
    <%--$.ajax({
     url: "<%=request.getContextPath()%>/PreTendQuerySrBean?tenderId=<%=tenderId%>&action=docData&docAction=PreTenderMetDocs",
     method: 'POST',
     async: false,
     success: function(j) {
         document.getElementById("docData").innerHTML = j;
     }
    });--%>
         $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,action:'docData',docAction:'PreTenderMetDocs'}, function(j){
             document.getElementById("docData").innerHTML = j;
         });
     }

     function downloadFile(docName,docSize,tender){
    <%--$.ajax({
             url: "<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download",
             method: 'POST',
             async: false,
             success: function(j) {
             }
     });--%>
             document.getElementById("form2").action= "<%=request.getContextPath()%>/PreTenderMetDocUploadServlet?docName="+docName+"&docSize="+docSize+"&tenderId="+tender+"&funName=download";
             document.getElementById("form2").submit();
         }

         function deleteFile(docName,docId,tender){
    <%--$.ajax({
           url: "<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docId="+docId+"&tender="+tender+"&funName=remove",
           method: 'POST',
           async: false,
           success: function(j) {
               jAlert("Document Deleted."," Delete Document ", function(RetVal) {
               });
           }
   });--%>
           $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
               if(r == true){
                   $.post("<%=request.getContextPath()%>/PreTenderMetDocUploadServlet", {docName:docName,docId:docId,tender:tender,funName:'remove'}, function(j){
                       jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                           getDocData();
                       });
                   });
               }else{
                   getDocData();
               }
           });
    
       }
  

    <%--    function forTabDisplay(str)
    {
        if(str == "pending"){
            document.getElementById("hrfPending").className ="sMenu" ;
            document.getElementById("hrfReply").className="";
            document.getElementById("hrfHold").className="";
            document.getElementById("hrfIgnore").className="";
            $('#div0').show();
            $('#div1').hide();
            $('#div2').hide();
            $('#div3').hide();
        }else if(str == "reply"){
            document.getElementById("hrfPending").className ="" ;
            document.getElementById("hrfReply").className="sMenu";
            document.getElementById("hrfHold").className="";
            document.getElementById("hrfIgnore").className="";
            $('#tr0').hide();
            $('#tr1').show();
             $('#div0').hide();
            $('#div1').show();
            $('#div2').hide();
            $('#div3').hide();
        }
        else if(str == "hold"){
            document.getElementById("hrfPending").className ="" ;
            document.getElementById("hrfReply").className="";
            document.getElementById("hrfHold").className="sMenu";
            document.getElementById("hrfIgnore").className="";
            $('#div0').hide();
            $('#div1').hide();
            $('#div2').show();
            $('#div3').hide();
        }
        else{
            document.getElementById("hrfPending").className ="" ;
            document.getElementById("hrfReply").className="";
            document.getElementById("hrfHold").className="";
            document.getElementById("hrfIgnore").className="sMenu";
            $('#div0').hide();
            $('#div1').hide();
            $('#div2').hide();
            $('#div3').show();
        }
    }--%>

        function getQueryData(queryData){
          $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,action:'getQueryData',queryAction:queryData}, function(j){
              document.getElementById("queryData").innerHTML = j;
          });
          if(queryData == "GetPendingQuery"){
              document.getElementById("hrfPending").className ="sMenu" ;
              document.getElementById("hrfReply").className="";
              document.getElementById("hrfHold").className="";
              document.getElementById("hrfIgnore").className="";
          }else if(queryData == "GetRepliedQuery"){
              document.getElementById("hrfPending").className ="" ;
              document.getElementById("hrfReply").className="sMenu";
              document.getElementById("hrfHold").className="";
              document.getElementById("hrfIgnore").className="";
          }
          else if(queryData == "GetHoldQuery"){
              document.getElementById("hrfPending").className ="" ;
              document.getElementById("hrfReply").className="";
              document.getElementById("hrfHold").className="sMenu";
              document.getElementById("hrfIgnore").className="";
          }
          else if(queryData == "GetIgnoreQuery"){
              document.getElementById("hrfPending").className ="" ;
              document.getElementById("hrfReply").className="";
              document.getElementById("hrfHold").className="";
              document.getElementById("hrfIgnore").className="sMenu";
          }
      }

      getQueryData('GetPendingQuery');
      getDocData();

      function ruleAlert(){
          var dateValue = '<%=new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").format(calendar.getTime())%>';
          jAlert('Pre-Tender Meeting document should be published till '+dateValue,'Pre-Tender Meeting', function(RetVal) {
          });

          var vendDate = '<%=preBidEndDate%>';
          var today = new Date()
          var d1 = new Date(vendDate);
          alert(d1);

      }
    
  
</script>
<%
            preTendDtBean = null;
            getPreBidDates = null;
%>