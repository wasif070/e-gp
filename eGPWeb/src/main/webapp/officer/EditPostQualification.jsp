<%-- 
    Document   : EditPostQualification
    Created on : Jun 23, 2011, 11:45:30 AM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblPostQualification"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TblPostQualificationService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="java.lang.String" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Post Qualification Process</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function GetCalWithouTime(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }                

            function ValidateSiteVisitDate() {
                clear();
                var errFlag=true;
               if ($("#cmbReqSiteVisit").val() == "Yes") {
                    if ($("#txtSiteVisitDate").val() == ""){
                           $('#siteDateNospan').html('Please enter Site Visit Date and Time.');
                           errFlag=false;
                    } else {
                        var d =new Date();
                        var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());
                        var value=$("#txtSiteVisitDate").val();
                        var mdy = value.split('/')  //Date and month split
                        var mdyhr= mdy[2].split(' ');  //Year and time split
                        var mdyhrtime=mdyhr[1].split(':');
                        var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                        if(mdyhrtime[1] != undefined){
                                todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
                                valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                        }

                        if(Date.parse(valuedate) <= Date.parse(todaydate)){
                           $('#siteDateNospan').html('Date must be greater than the current date and time.');
                            errFlag=false;
                        }
                    }
               }

               /*if ($("#txtComments").val() == ""){
                   $('#commentsErr').html('Please enter Comments');
                   errFlag=false;
               }else{
                    $('#commentsErr').html('');
                }*/

               if (errFlag==false){
                   return false;
               } else {
                   document.getElementById("btnSubmit").style.display='none';
               }
            }

             function clear(){
                $('#siteDateNospan').html('');
                $('#commentsErr').html('');
            }

            function changeval(){
               
              if ($('#cmbReqSiteVisit').val() == "Yes")
                        $('#trSiteVisitDt').show();
                    else
                        $('#trSiteVisitDt').hide();
            }

            $(document).ready(function() {

                $("#frmPostQualification").validate({
                    rules: {
                        txtComments:{required:true,maxlength: 500}
                    },
                    messages: {
                        txtComments:{required:"<div class='reqF_1'>Please Enter Comment.</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 500 characters only</div>"}
                    }
                });

                $('#cmbReqSiteVisit').change(function() {
                    if ($('#cmbReqSiteVisit').val() == "Yes")
                        $('#trSiteVisitDt').show();
                    else
                        $('#trSiteVisitDt').hide();
                    });
                });
        </script>
    </head>
    <body onload="changeval();">
        <%
            //TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        TblPostQualificationService service = (TblPostQualificationService)AppContext.getSpringBean("PostQualificationService");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            HandleSpecialChar handleSpecialChar = new HandleSpecialChar();

            String tenderId="",pkqualId="", userId="", createdBy="", pkgLotId="",siteVisitVal="", siteVisitDt="", strComments="";
            tenderId = request.getParameter("tenderId");
            userId = request.getParameter("uId");
            pkgLotId = request.getParameter("pkgLotId");
             pkqualId = request.getParameter("postQualId");
             HttpSession hs = request.getSession();
            if (hs.getAttribute("userId") != null) {
                createdBy = hs.getAttribute("userId").toString();
            }
            if (request.getParameter("btnSubmit")!=null)
                {
                    String strXml = "";
                   
                    siteVisitVal=request.getParameter("cmbReqSiteVisit");
                    if(request.getParameter("txtSiteVisitDate")!=null){
                        siteVisitDt=request.getParameter("txtSiteVisitDate").toString();
                    }
                    strComments=request.getParameter("txtComments");
                    strComments = handleSpecialChar.handleSpecialChar(strComments);
                   if (siteVisitDt != null) {
                        if (!siteVisitDt.trim().equalsIgnoreCase("")) {
                            String[] temp = siteVisitDt.split(" ");
                            String[] dtArr = temp[0].split("/");
                             siteVisitDt = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0]+" "+temp[1];
                        }
                    }
                    DateUtils utils = new DateUtils();
                    
                     // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String idType="tenderId";

                    int auditId=Integer.parseInt(tenderId);
                    String auditAction="Edit PQ by TEC CP";
                    String moduleName=EgpModule.Evaluation.getName();
                    String remarks="";
                    if(request.getParameter("txtComments")!=null)
                        remarks=request.getParameter("txtComments");
                    
                    TblPostQualification qualification = new TblPostQualification();
                    qualification.setComments(strComments);
                    qualification.setCreatedBy(Integer.parseInt(createdBy));
                    qualification.setEntryDate(new Date());
                    qualification.setPkgLotId(Integer.parseInt(pkgLotId));
                    qualification.setPostQaulId(Integer.parseInt(pkqualId));
                    qualification.setPostQualStatus("Initiated");
                    qualification.setUserId(Integer.parseInt(userId));
                    qualification.setTblTenderMaster(new TblTenderMaster(Integer.parseInt(tenderId)));
                    qualification.setSiteVisit(siteVisitVal);
                    qualification.setSiteVisitStatus("");
                    qualification.setSiteVisitComments("");
                    qualification.setSiteVisitDate(null);
                    qualification.setSiteVisitReqDt(utils.convertStringtoDate(siteVisitDt, "yyyy-MM-dd HH:mm"));
                    qualification.setNoaStatus("pending");
                    qualification.setRank(Integer.parseInt(request.getParameter("rank")));
                    boolean flag = service.updatePQ(qualification);
                    if (flag) {
                        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                        TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                        List<Object[]> list = cs.getLotDetailsByTenderIdAndPkgIdforEval(tenderId, pkgLotId);
                        List<SPTenderCommonData> datas = tenderCommonService1.returndata("tenderinfobar", request.getParameter("tenderId"), null);
                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                        MailContentUtility mailContentUtility = new MailContentUtility();
                        String companyname = cs.getConsultantName(userId);
                        if ("yes".equalsIgnoreCase(siteVisitVal)) {

                            String tomailId = "";
                            String frommailid = XMLReader.getMessage("emailIdNoReply");
                            tomailId = tenderCommonService1.getEmailID(userId);
                            String peoffice = tenderCommonService1.getPEOfficeName(request.getParameter("tenderId"));
                            String mailText = mailContentUtility.sendMessageTobidderForPQ(Integer.parseInt(tenderId), datas.get(0).getFieldName2().toString(), peoffice, list.get(0)[0].toString(), list.get(0)[1].toString(), companyname, DateUtils.convertStringtoDate(siteVisitDt, "yyyy-MM-dd HH:mm"),siteVisitVal);
                            userRegisterService.contentAdmMsgBox(tomailId, frommailid, HandleSpecialChar.handleSpecialChar("Post Qualification Process"), mailText);

                        }
                        CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        List<SPCommonSearchDataMore> dataMore = csdms.geteGPData("GetTECMemberEmail", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        for (int k = 0; k < dataMore.size(); k++) {

                            if (!createdBy.equalsIgnoreCase(dataMore.get(k).getFieldName2().toString())) {
                                String tomailId = "";
                                String frommailid = XMLReader.getMessage("emailIdNoReply");
                                tomailId = dataMore.get(k).getFieldName1().toString();
                                String peoffice = tenderCommonService1.getPEOfficeName(request.getParameter("tenderId"));
                                String mailText = mailContentUtility.sendMessageTobidderForPQ(Integer.parseInt(tenderId), datas.get(0).getFieldName2().toString(), peoffice, list.get(0)[0].toString(), list.get(0)[1].toString(), companyname, DateUtils.convertStringtoDate(siteVisitDt, "yyyy-MM-dd HH:mm"),siteVisitVal);
                                userRegisterService.contentAdmMsgBox(tomailId, frommailid, HandleSpecialChar.handleSpecialChar("Post Qualification Process"), mailText);
                            }

                        }
                        
                       makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                       response.sendRedirect("ProcessPQ.jsp?tenderId=" + tenderId + "&msgId=edit&userId="+userId+"&pkgLotId="+pkgLotId );
                    } 
                    else 
                    {
                        auditAction="Error in "+auditAction;
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                        
                       response.sendRedirect("PostQualification.jsp?uId=" + userId + "&tenderId=" + tenderId + "&msgId=error" );
                    }

                }
        %>
        <!--Dashboard Header Start-->
        <div class="topHeader">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
        </div>
        <!--Dashboard Header End-->

        <form id="frmPostQualification" action="EditPostQualification.jsp?uId=<%=request.getParameter("uId")%>&tenderId=<%=request.getParameter("tenderId")%>" name="frmPostQualification" method="POST">
            <input type="hidden" name="pkgLotId" value="<%=pkgLotId%>" />
            <input type="hidden" name="postQualId" value="<%=pkqualId%>" />
            <input type="hidden" name="userId" value="<%=userId%>" />
            <div class="dashboard_div">
                <!--Dashboard Content Part Start-->
                <div class="contentArea_1">
                    <div class="pageHead_1">Edit Post Qualification
                    <span style="float:right;">
                    <a href="InitiatePQ.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a>
                    </span>
                    </div>

                    <% pageContext.setAttribute("tenderId", tenderId);%>

                    <%@include file="../resources/common/TenderInfoBar.jsp" %>

                    <div>&nbsp;</div>
                    <%
                         pageContext.setAttribute("TSCtab", "6");
                    %>
                    <%@include  file="../resources/common/AfterLoginTSC.jsp"%>
                    <%if (request.getParameter("msgId") != null) {
                        String msgId = "", msgTxt = "";
                        boolean isError = false;
                        msgId = request.getParameter("msgId");
                        if (!msgId.equalsIgnoreCase("")) {
                            if (msgId.equalsIgnoreCase("error")) {
                                isError = true;
                                msgTxt = "There was some error.";
                            } else {
                                msgTxt = "";
                            }
                    %>
                    <%if (isError) {%>
                    <div class="responseMsg errorMsg"><%=msgTxt%></div>
                    <%} else {%>
                    <div class="responseMsg successMsg"><%=msgTxt%></div>
                    <%}%>
                    <%}
                            }%>
                    <div class="tabPanelArea_1 t_space">
                        <table border="0" width="100%" cellspacing="0" class="tableList_1">

                    <%
                            List<Object[]> list = service.getAll(request.getParameter("uId"),pkgLotId);
                            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                            String companyName = commonService.getConsultantName(request.getParameter("uId"));
                            /*List<SPTenderCommonData> companyList = tenderCommonService.returndata("GetCompanyByUserId",
                                        request.getParameter("uId"),
                                        "0");*/

                            if (!list.isEmpty()) {
                    %>
                    <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="13%" class="t-align-left ff">Company Name : </td>
                            <td width="87%" class="t-align-left">
                            <%=companyName%>
                            </td>
                        </tr>
                        <% }%>
                        <tr>
                            <td class="t-align-left ff">Site Visit Requires?: </td>
                            <td class="t-align-left">
                                <select name="cmbReqSiteVisit" class="formTxtBox_1" id="cmbReqSiteVisit" style="width:50px;">
                                    <option value="Yes" <%if("yes".equalsIgnoreCase(list.get(0)[2].toString())){%> selected<%}%>>Yes</option>
                                    <option value="No" <%if("No".equalsIgnoreCase(list.get(0)[2].toString())){%> selected<%}%>>No</option>
                                </select>
                                    <input type="hidden" value="<%=list.get(0)[3]%>" name="rank"/>
                            </td>
                            
                        </tr>
                        <tr id="trSiteVisitDt">
                            <td class="t-align-left ff">Site Visit Date and Time : </td>
                            <td class="t-align-left">
                                <input value="<%
                      String[] temp = list.get(0)[0].toString().split("-");
                        if("1900".equalsIgnoreCase(temp[0])){
                            out.print("");
                            }else{
                          out.print(DateUtils.convertStrToDate((Date)list.get(0)[0]));
                          }
                       

                        %>" name="txtSiteVisitDate" type="text" class="formTxtBox_1" id="txtSiteVisitDate"
                                        style="width:12%;" readonly="true" onfocus="GetCal('txtSiteVisitDate','txtSiteVisitDate');" />
                                <img id="imgDtOfIns" name="imgDtOfIns" onclick="GetCal('txtSiteVisitDate','imgDtOfIns');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                                <span id="siteDateNospan" class="reqF_1"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Comments : </td>
                            <td class="t-align-left">
                                <textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"><%=list.get(0)[1]%></textarea>
                                <span id="commentsErr" class="reqF_1"></span>
                            </td>
                        </tr>
                    </table>

                    <div class="t-align-center t_space">
                        <label class="formBtn_1">
                            <input id="btnSubmit" name="btnSubmit" type="submit" onclick="return ValidateSiteVisitDate();" value="Submit" />
                        </label>
                    </div>

                    <div>&nbsp;</div>
                    </div>
                </div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
        </form>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

