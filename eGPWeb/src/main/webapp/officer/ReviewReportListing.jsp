<%--
    Document   : ReviewReportListing
    Created on : Oct 30, 2011, 10:30:16 AM
    Author     : darshan
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluation Report Review</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
    </head>
    <script type="text/javascript">
        function chkdisble(pageNo){
            //alert(pageNo);
            $('#dispPage').val(Number(pageNo));
            if(parseInt($('#pageNo').val(), 10) != 1){
                $('#btnFirst').removeAttr("disabled");
                $('#btnFirst').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnFirst').attr("disabled", "true");
                $('#btnFirst').css('color', 'gray');
            }


            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnPrevious').attr("disabled", "true")
                $('#btnPrevious').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) > 1){
                $('#btnPrevious').removeAttr("disabled");
                $('#btnPrevious').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnLast').attr("disabled", "true");
                $('#btnLast').css('color', 'gray');
            }

            else{
                $('#btnLast').removeAttr("disabled");
                $('#btnLast').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnNext').attr("disabled", "true")
                $('#btnNext').css('color', 'gray');
            }
            else{
                $('#btnNext').removeAttr("disabled");
                $('#btnNext').css('color', '#333');
            }
        }
    </script>
    <script type="text/javascript">
        function loadTable()
        {
            $.post("<%=request.getContextPath()%>/EvalutionReportsServlet", {funName : "viewReviewReports",pageNo: $("#pageNo").val(),tenderId: $("#tenderId").val(),refNo: $("#refNo").val(),size: $("#size").val()},  function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                sortTable();
                if($('#noRecordFound').attr('value') == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }
                chkdisble($("#pageNo").val());
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageNoTot").html($("#pageNo").val());
                $("#pageTot").html($("#totalPages").val());
                $('#resultDiv').show();
            });
        }
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                var pageNo =$('#pageNo').val();
                if(totalPages>0 && pageNo!="1")
                {
                    $('#pageNo').val("1");
                    loadTable();
                    $('#dispPage').val("1");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0)
                {
                    $('#pageNo').val(totalPages);
                    loadTable();
                    $('#dispPage').val(totalPages);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo < totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)+1);

                    $('#dispPage').val(Number(pageNo)+1);
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();
                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                }
            });
        });
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            });
        });
    </script>
    <body onload="loadTable();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <div class="contentArea_1">
                    <%
                                String referer = "", orgReferer = "";
                                if (request.getHeader("referer") != null) {
                                    referer = request.getHeader("referer");
                                }
                    %>
                    <div class="pageHead_1">Evaluation Report Review
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('5');">Save as PDF</a></span>
                    </div>

                    <div>&nbsp;</div>
                    <div class="formBg_1">
                        <table id="tblSearchBox" cellspacing="10" class="formStyle_1 t_space" width="100%">
                            <tr>
                                <td class="ff" width="20%">Tender ID :</td>
                                <td width="30%"><input type="text" class="formTxtBox_1" id="tenderId" style="width:200px;" /></td>
                                <td width="20%" class="ff">Reference No. :</td>
                                <td width="30%"><input type="text" class="formTxtBox_1" id="refNo" style="width:200px;" /></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center"><label class="formBtn_1">
                                        <input type="button" name="button" id="btnSearch" value="Search" />
                                    </label>
                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input type="button" name="Reset" id="btnReset" value="Reset" />
                                    </label></td>
                            </tr>
                            <td class="t-align-center" colspan="4">
                                <div id="valAll" class="reqF_1"></div>
                            </td>
                        </table>
                    </div>
                    <div class="tabPanelArea_1 t_space">
                        <table width="100%" cellspacing="0" id="resultTable" class="tableList_3 t_space" cols="@0,5">
                            <tr>
                                <th width="4%" class="t-align-center">Sl. No.</th>
                                <th width="9%" class="t-align-center">Tender ID<br/>Reference No.</th>
                                <th width="21%" class="t-align-left">Tender Brief / Title</th>
                                <th width="15%" class="t-align-left">Ministry / Division / Organization / PA</th>
                                <th width="14%" class="t-align-left">PE Office</th>
                                <th width="9%" class="t-align-center">Action</th>
                            </tr>
                        </table>
                        <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                            <tr>
                                <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                    </label></td>
                                <td align="right" class="prevNext-container"><ul>
                                        <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                        <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                        <li><a href="javascript:void(0)" id="btnNext">Next</a><font size="3"> &#8250;</font></li>
                                        <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                    </ul></td>
                            </tr>
                        </table>
                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" id="size" value="10"/>
                    </div>
                    <form id="formstyle" action="" method="post" name="formstyle">

                       <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                       <%
                         SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                         String appenddate = dateFormat1.format(new Date());
                       %>
                       <input type="hidden" name="fileName" id="fileName" value="EvaluationReport_<%=appenddate%>" />
                        <input type="hidden" name="id" id="id" value="EvaluationReport" />
                    </form>
                    <div>&nbsp;</div>
                    <!--Dashboard Content Part End-->
                </div>
<!--            </form>-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabEval");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

        $(function() {
            var count;
            $('#btnSearch').click(function() {
                $('#pageNo').val('1');
                count=0;
                if(document.getElementById('tenderId') !=null && document.getElementById('tenderId').value !=''){
                    count = 1;
                }else if(document.getElementById('refNo') !=null && document.getElementById('refNo').value !=''){
                    count = 1;
                }
                if(count != '0'){
                    $("#valAll").html('');
                    loadTable();
                }else{
                    $("#valAll").html('Please enter atleast one search criteria');
                    return false;
                }
            });
        });
        $(function() {
            $('#btnReset').click(function() {
                if(document.getElementById('tenderId').value != ''){
                    document.getElementById('tenderId').value = '';
                }
                if(document.getElementById('refNo').value != ''){
                    document.getElementById('refNo').value = '';
                }
                loadTable();
            });
        });
    </script>
</html>
