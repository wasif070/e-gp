<%-- 
    Document   : T1L1Include
    Created on : Sep 16, 2011, 5:55:55 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.model.table.TblEvalServiceWeightage"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.web.servicebean.T1L1SrBean"%>
<%@page import="com.cptu.egp.eps.web.databean.T1L1DtBean"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%
            String s_tenderId = request.getParameter("tenderid");
            String s_reportId = request.getParameter("reportId");
            String s_roundId = "0";
            String s_lotId = request.getParameter("lotId");
            String s_rId = request.getParameter("roundId");
            ;
            if (request.getParameter("rId") != null && !"".equalsIgnoreCase(request.getParameter("rId"))) {
                s_roundId = request.getParameter("rId");
            }
            //s_roundId = "68";
            // s_reportId = "452";
            String s_userId = "0";
            if (session.getAttribute("userId") != null) {
                s_userId = session.getAttribute("userId").toString();
            } else {
                s_userId = request.getParameter("tendrepUserId");
            }
            List<T1L1DtBean> list = new ArrayList<T1L1DtBean>();
            T1L1SrBean t1l1SrBean = new T1L1SrBean();
            t1l1SrBean.setLogUserId(s_userId);
            if ("0".equalsIgnoreCase(s_rId)) {
                list = t1l1SrBean.getT1L1Data(s_tenderId, s_reportId, s_roundId, s_userId);
            } else {
                list = t1l1SrBean.getT1L1SaveData(s_tenderId, s_rId);
            }
            EvalServiceCriteriaService t1l1criteriaService = (EvalServiceCriteriaService) AppContext.getSpringBean("EvalServiceCriteriaService");
            TblEvalServiceWeightage tblEvalServiceWeightage = new TblEvalServiceWeightage();
            tblEvalServiceWeightage = t1l1criteriaService.getTblEvalServiceByTenderId(Integer.parseInt(s_tenderId));
            int finWeigth = tblEvalServiceWeightage.getFinancialWeight();
            int tecWeigth = tblEvalServiceWeightage.getTechWeight();
            int counter = 1;
            if (request.getParameter("cnt") != null && !"null".equals(request.getParameter("cnt")) && !"".equals(request.getParameter("cnt"))) {
                counter = Integer.parseInt(request.getParameter("cnt"));
            }

%>

<table class="tableList_1 t_space" cellspacing="0" width="100%">
    <tr>
        <th class="t-align-center" width="3%" >Rank</th>
        <th class="t-align-center" width="20%">Name of Consultant/Applicant</th>
        <th class="t-align-center" width="7%">Proposal Price</th>
        <th class="t-align-center" width="15%">Technical Points (Max 100)</th>
        <th class="t-align-center" width="15%">Weighting<br />(<%=tecWeigth%>%)*[Technical Points]<br/><span style="color: red">A</span></th>
        <th class="t-align-center" width="15%">Financial Score<br/>(Max 100) Formula-100*[Fm/F]</th>
        <th class="t-align-center" width="15%">Weighting<br/>(<%=finWeigth%>%)*[ Financial Points ]<br/><span style="color: red;">B</span></th>
        <th class="t-align-center" width="10%">Combined Score <span style="color: red;">A+B</span></th>
    </tr>
    <%
                int rank = 1;
                double t1l1Amount = 0;
                boolean isLottery = false;
                double l1Score = 0;
                double t1Score = 0;
                int i = 1;
                for (T1L1DtBean t1L1DtBean : list) {
                    if (i == 1) {
                        t1l1Amount = t1L1DtBean.getAmount();
                    } else {
                        if (t1l1Amount != t1L1DtBean.getAmount()) {
                            t1l1Amount = t1L1DtBean.getAmount();
                            rank++;
                        } else {
                            if (rank == 1) {
                                isLottery = true;
                            }
                        }
                    }
                    if (t1L1DtBean.getL1Score() > 0) {
                        l1Score = t1L1DtBean.getL1Score();
                    }
                    if (t1L1DtBean.getT1Score() > 0) {
                        t1Score = t1L1DtBean.getT1Score();
                    }


    %>
    <tr>
        <td class="t-align-center"><%=i%></td>
        <td class="t-align-left"><%out.print(t1L1DtBean.getCompanyName());
                                                if (i == counter) {
                                                    out.print("<span style='color:green;'> <b>(Current Highest Scorer)</b></span>");
                                                }%></td>
        <td style="text-align: right"><%=new BigDecimal(String.valueOf(t1L1DtBean.getL1price())).setScale(3, 0)%></td>
        <td style="text-align: right"><%=new BigDecimal(String.valueOf((100 * t1Score) / tecWeigth)).setScale(3, 0)%></td>
        <td style="text-align: right"><%=new BigDecimal(String.valueOf(t1Score)).setScale(3, 0)%>
        <td style="text-align: right"><%=new BigDecimal(String.valueOf((100 * l1Score) / finWeigth)).setScale(3, 0)%></td>
        <td style="text-align: right"><%=new BigDecimal(String.valueOf(l1Score)).setScale(3, 0)%></td>
        <td style="text-align: right"><%=new BigDecimal(String.valueOf(t1l1Amount)).setScale(3, 0)%></td>
    </tr>
    <%
                    i++;
                }
                request.setAttribute("isLottery", isLottery);
                request.setAttribute("T1L1DtBean", list);
    %>
</table>
