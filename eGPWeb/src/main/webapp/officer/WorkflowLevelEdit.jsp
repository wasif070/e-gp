<%--
    Document   : WorkflowUsers
    Created on : Oct 28, 2010, 7:44:16 PM
    Author     : Dohatec
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.APPService"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CommonSearchServiceSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementRole"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Workflow : Add Users</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
<script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<!-- Dohatec Start -->
<link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
<!-- Dohatec End -->

</head>
<body>
    <%
     UserRegisterService  service = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
     APPService appService = (APPService)AppContext.getSpringBean("APPService");
     TenderCommonService tenderService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
     CommitteMemberService  comService = (CommitteMemberService)AppContext.getSpringBean("CommitteMemberService");

     String fieldName = "WorkFlowRuleEngine";
     String eventid = request.getParameter("eventid");
     String objectid = request.getParameter("objectid");
     String activityid = request.getParameter("activityid");
     String childid = request.getParameter("childid");
     String Action = request.getParameter("action");
     String wpId = request.getParameter("wpId");
     String isFileProcessed = request.getParameter("isFileProcessed");
     int fileOnHandUser = 1;
     Date actionDate = new Date();
     String moduleName = null;
     String eventName = null;
      String[] startsBy = null;
      String[] endsBy = null;
      String ofcname = "";
      String desig = "";
      String strSubject = "";

      List<CommonAppData> listofAPPDetails = appService.getAPPDetailsBySP("APP", ""+objectid, "");
      List<SPTenderCommonData> listofTenderDetails = tenderService.returndata("tenderinfobar", objectid, null);
      String appCode= "";

      if(request.getParameter("modulename") != null && request.getParameter("modulename").equalsIgnoreCase("Tender")){
        strSubject = "Tender : "+objectid+" File to be processed in Workflow";
        if(!listofTenderDetails.isEmpty()){
            appCode = listofTenderDetails.get(0).getFieldName2();
        }
      }else {
      if(!listofAPPDetails.isEmpty()){
          appCode = listofAPPDetails.get(0).getFieldName3();
        }
        strSubject = "Letter Ref. No. : "+appCode+" File to be processed in Workflow";
      }

      int uid = 0;
        MessageProcessSrBean messageProcessSrbean = new MessageProcessSrBean();
      String userid = "";
       if(session.getAttribute("userId") != null){
           Integer ob1 = (Integer)session.getAttribute("userId");
           uid = ob1.intValue();
          userid  = String.valueOf(uid);
       }
      int  noRows = 0;
      String donor = null;
      List<TblProcurementRole> procurementRoles = null;
       WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
       List<TblWorkFlowLevelConfig> tblworkFlowLevelconfig = null;
     if("Submit".equals(request.getParameter("btnsubmit"))){
            eventid = request.getParameter("eventid");
            objectid = request.getParameter("objectid");
            activityid = request.getParameter("activityid");
            Action = request.getParameter("action");
            childid = request.getParameter("childid");
            StringBuffer stsBy = new StringBuffer();
            StringBuffer endBy = new StringBuffer();
           String startsbyid =  request.getParameter("startsby");
           String endsbyid =    request.getParameter("endsby");
           StringBuffer wfRoleids = new StringBuffer();
           StringBuffer procurids = new StringBuffer();
           StringBuffer fileOnHand = new StringBuffer();
           StringBuffer WfLevel = new StringBuffer();
           String temps = new String(request.getParameter("userids"));

           String[] userids = temps.split("@");
           StringBuffer uids = new StringBuffer();
           String isfileproc = request.getParameter("isFileProcessed");
           String fileonhand = request.getParameter("fileOnHand");

           for(int ui = 0;ui<userids.length;ui++){
               uids.append(userids[ui]+"@$");
               }
           uids.delete(uids.length()-2, uids.length());
           
           wfRoleids.append("1@$");

           for(int rv = 2;rv<=(userids.length-1);rv++){
              if(request.getParameter("revlabl"+rv) != null){
                  if("Reviewer".equalsIgnoreCase(request.getParameter("revlabl"+rv))){
                    wfRoleids.append("3");
                    wfRoleids.append("@$");
                    }else{
                      wfRoleids.append("4");
                      wfRoleids.append("@$");
                    }
                }
             }

           wfRoleids.append("2");
           procurids.append(startsbyid+"@$");
           String prbuf = request.getParameter("procureids");

           String[] prids = prbuf.split("@");
           for(int k=0;k<prids.length;k++){
               procurids.append(prids[k]);
               procurids.append("@$");
               }

           procurids.append(endsbyid);
           String[] prId = procurids.toString().split("@\\$");

           String prcId = procurids.toString();
           prcId = prcId.replace("@$@","@");

           if (isfileproc.equalsIgnoreCase("No")){
           fileOnHand.append("YES@$");
           for(int f=1;f<userids.length;f++) {
               fileOnHand.append("NO@$");
           }
           }else{
           for(int f=1;f<=userids.length;f++) {
               if(Integer.parseInt(fileonhand) == f)
                fileOnHand.append("YES@$");
               else fileOnHand.append("NO@$");
           }
          }
           for(int lev = 0;lev<userids.length;lev++){
                  WfLevel.append(lev+1+"@$");
           }
           WfLevel.delete(WfLevel.length()-2, WfLevel.length());
           fileOnHand.delete(fileOnHand.length()-2, fileOnHand.length());
           if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                objectid = childid;
           }
          List listdata =  workFlowSrBean.addWorkFlowLevel(Integer.valueOf(eventid), Integer.valueOf(objectid),
                  actionDate,Integer.valueOf(activityid), Integer.valueOf(childid), fileOnHand.toString(), Action, 0,
                  userid, WfLevel.toString(), uids.toString(), wfRoleids.toString(),
                  prcId);
           if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                objectid = request.getParameter("objectid");
           }
          if(listdata.size()>0){
            CommonMsgChk msgchk = (CommonMsgChk)listdata.get(0);

            String officdesig = "";
            String[] od = new String[3];
            StringBuffer mailto = new StringBuffer();
            moduleName = request.getParameter("modulename");
            eventName = request.getParameter("eventname");
             String fromEmailId = "";
            List gettingfromemail =    messageProcessSrbean.getLoginmailIdByuserId(uid);
            if(gettingfromemail.size()>0){
            TblLoginMaster loginMaster2 = (TblLoginMaster)gettingfromemail.get(0);
            fromEmailId = loginMaster2.getEmailId();
            }

            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
            SendMessageUtil smu = new SendMessageUtil();

            if(userids.length>0){
                for(int j=0;j<userids.length;j++){
                    if("20".equalsIgnoreCase(prId[j])){
                        officdesig = workFlowSrBean.getOfficeNameAndDesignationByDP(Integer.parseInt(userids[j].toString()));
                    }else{
                        officdesig = workFlowSrBean.getOfficialAndDesignationName(Integer.parseInt(userids[j].toString()));
                    }
                    od = officdesig.split(",");
                    ofcname = od[0];
                    desig = od[1];
                    int toid = Integer.parseInt(userids[j]);
                    List l =    messageProcessSrbean.getLoginmailIdByuserId(toid);
                    TblLoginMaster loginMaster = (TblLoginMaster) l.get(0);
                    String tomailid = loginMaster.getEmailId();
                    service.contentAdmMsgBox(tomailid, XMLReader.getMessage("emailIdNoReply"), strSubject, msgBoxContentUtility.conWorkflow(eventName, moduleName, appCode,ofcname, desig,fromEmailId));

                    String[] multimails = new String[1];
                    multimails[0] = tomailid;
                    MailContentUtility mc = new MailContentUtility();
                    List list = mc.conWorkflow(eventName, moduleName, appCode,ofcname, desig,fromEmailId,objectid);
                    String sub = list.get(0).toString();
                    String mailText = list.get(1).toString();
                    smu.setEmailTo(multimails);
                    smu.setEmailFrom(XMLReader.getMessage("emailIdNoReply"));
                    smu.setEmailSub(sub);
                    smu.setEmailMessage(mailText);
                    smu.sendEmail();
               }
                   /*Doahtec Start Tender Cancel*/
                    Integer noOfReviewer = 0;
                    List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData("workflowedit", activityid, objectid);
                            Iterator it = editdata.iterator();
                            while (it.hasNext()) {
                                CommonAppData commonAppData = (CommonAppData) it.next();
                                noOfReviewer = Integer.valueOf(commonAppData.getFieldName3());
                            }
                    if( Integer.valueOf(activityid).intValue() == 9 && noOfReviewer == 0 && userids[0].equalsIgnoreCase(userids[1])){
                         List listdata1 =  workFlowSrBean.addWorkFlowLevel(Integer.valueOf(eventid), Integer.valueOf(objectid),
                            actionDate,Integer.valueOf(activityid), Integer.valueOf(childid), fileOnHand.toString(), "Tender Cancel", 0,
                            userid, WfLevel.toString(), userids[0], wfRoleids.toString(),
                            prcId);
                         if(listdata.size()>0){
                                CommonMsgChk msgchk1 = (CommonMsgChk)listdata.get(0);
                                System.out.println( " Tender Cancel Status: " + msgchk1.getMsg());
                         }
                     }
                     /*Dohatec End Tender Cancel*/
            }

         }



         if(Integer.valueOf(activityid).intValue() == 1){
              response.sendRedirect("APPDashboard.jsp?appID="+objectid);
              return;
          }else if(Integer.valueOf(activityid).intValue() == 2 || Integer.valueOf(activityid).intValue() == 9){
            response.sendRedirect("Notice.jsp?tenderid="+objectid);
             return;
          }else if(Integer.valueOf(activityid).intValue() == 5){
            response.sendRedirect("OpenComm.jsp?tenderid="+objectid);
             return;
          }else if(Integer.valueOf(activityid).intValue() == 6){
            response.sendRedirect("EvalComm.jsp?tenderid="+objectid);
             return;
          }else if(Integer.valueOf(activityid).intValue() == 4){
            response.sendRedirect("Amendment.jsp?tenderid="+objectid);
             return;
          }else if(Integer.valueOf(activityid).intValue() == 3){
            response.sendRedirect("PreTenderMeeting.jsp?tenderId="+objectid);
             return;
          }else if(Integer.valueOf(activityid).intValue() == 8 || Integer.valueOf(activityid).intValue() == 13){ // Activity Id = 13 : Added By Dohatec){
            response.sendRedirect("EvalComm.jsp?tenderid="+objectid);
             return;
          }else if(Integer.valueOf(activityid).intValue() == 10){
            response.sendRedirect("TabContractTermination.jsp?tenderId="+objectid);
             return;
          }else if(Integer.valueOf(activityid).intValue() == 11){
              if(wpId!=null && !"".equalsIgnoreCase(wpId) && !"null".equalsIgnoreCase(wpId)){
                if("0".equalsIgnoreCase(wpId)){
                    response.sendRedirect("WorkScheduleMain.jsp?tenderId="+objectid);
                     return;
                }
            }else{
            response.sendRedirect("DeliverySchedule.jsp?tenderId="+objectid);
             return;
          }}
          else if(Integer.valueOf(activityid).intValue() == 12){
              response.sendRedirect("repeatOrderMain.jsp?tenderId="+objectid);
             return;
          }
         } else{
     List<CommonAppData> editdata =  workFlowSrBean.editWorkFlowData(fieldName , eventid, "");
      Iterator it = editdata.iterator();
      StringBuffer stby = new StringBuffer();
      StringBuffer endby = new StringBuffer();
       StringBuffer nstby = new StringBuffer();
      StringBuffer nendby = new StringBuffer();
        while(it.hasNext()) {
           CommonAppData  commonAppData= (CommonAppData) it.next();
           moduleName = commonAppData.getFieldName1();
           eventName = commonAppData.getFieldName2();
           stby.append(commonAppData.getFieldName3()+",");
           endby.append(commonAppData.getFieldName4()+",");

            }
           if(stby.length()>0){

                    String str = stby.toString();
                    String[] str1 = str.split(",");
                    for(int j=0;j<str1.length;j++){
                        boolean check = false;
                        if(nstby.length()>0){
                         String str2= nstby.toString();
                        for(int k=0;k<str2.split(",").length;k++){
                            if(str1[j].equals(str2.split(",")[k])){
                                check = true;
                                  }
                            }
                           if(!check){
                               nstby.append(str1[j]+",");
                               }
                         continue;
                        }else nstby.append(str1[j]+",");
                    }
                     nstby.deleteCharAt(nstby.length()-1);
                    }
                  if(endby.length()>0){
                      String str = endby.toString();
                    String[] str1 = str.split(",");
                    for(int j=0;j<str1.length;j++){
                        boolean check = false;
                        if(endby.length()>0){
                         String str2= nendby.toString();
                        for(int k=0;k<str2.split(",").length;k++){
                            if(str1[j].equals(str2.split(",")[k])){
                                check = true;
                                  }
                            }
                           if(!check){
                               nendby.append(str1[j]+",");
                               }
                         continue;
                        }else nendby.append(str1[j]+",");
                    }
                       nendby.deleteCharAt(nendby.length()-1);
                       }

            String str1 = nstby.toString();
            startsBy = str1.split(",");
            String str2 = nendby.toString();
             endsBy = str2.split(",");
             procurementRoles =  workFlowSrBean.getRoles();
             int editObjectid = Integer.parseInt(objectid);
             int editChildid = Integer.parseInt(childid);
             if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                objectid = childid;
                editObjectid = Integer.parseInt(childid);
             }
             short editactivityid = Short.parseShort(activityid);
              tblworkFlowLevelconfig =
                     workFlowSrBean.editWorkFlowLevel(editObjectid, editChildid, editactivityid);
                donor = workFlowSrBean.isDonorReq(eventid,
                  Integer.valueOf(objectid));
                  String[] sarr =  donor.split("_");
                 noRows = Integer.parseInt(sarr[1]);
                 noRows+=2;
                 if("10".equalsIgnoreCase(activityid) || "11".equalsIgnoreCase(activityid) || "12".equalsIgnoreCase(activityid)){
                    objectid = request.getParameter("objectid");
                    editObjectid = Integer.parseInt(objectid);
                 }
               }

         Iterator it = tblworkFlowLevelconfig.iterator();
         List list = new ArrayList();
         if(isFileProcessed.equalsIgnoreCase("Yes")){
           int u = 1;
         while(it.hasNext()){
             TblWorkFlowLevelConfig tblwflc = (TblWorkFlowLevelConfig)it.next();
             TblLoginMaster  lm = tblwflc.getTblLoginMaster();
              list.add(new Integer(lm.getUserId()));
             if(tblwflc.getFileOnHand().equalsIgnoreCase("Yes")){
                 fileOnHandUser = tblwflc.getWfLevel();
               break;
               }

             }
         }

          if(tblworkFlowLevelconfig != null && tblworkFlowLevelconfig.size()>0){

               CommonSearchServiceSrBean commonSearchServiceSrBean = new CommonSearchServiceSrBean();
    %>

  <!--Dashboard Header Start-->
<div class="topHeader">
    <%@include file="../resources/common/AfterLoginTop.jsp" %>
  </div>
  <div class="contentArea_1">
  <div align="right">
       <% if(Integer.parseInt(activityid) == 1){ %>
          <a class="action-button-goback" href="APPDashboard.jsp?appID=<%=objectid%>">Go back to Dashboard</a>
          <% }else if(Integer.parseInt(activityid) == 2 || Integer.parseInt(activityid)== 9){ %>
          <a class="action-button-goback" href="Notice.jsp?tenderid=<%=objectid%>">Go back to Tender Dashboard</a>
          <% }else if(Integer.parseInt(activityid) == 5){ %>
          <a class="action-button-goback" href="OpenComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
          <% }else if(Integer.parseInt(activityid) == 6){ %>
          <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
           <% }else if(Integer.parseInt(activityid) == 4){ %>
          <a class="action-button-goback" href="Amendment.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
           <% }else if(Integer.parseInt(activityid) == 3){ %>
          <a class="action-button-goback" href="PreTenderMeeting.jsp?tenderId=<%=objectid%>">Go back to Dashboard</a>
           <% }else if(Integer.parseInt(activityid) == 8){ %>
          <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
          <!-- Added by Dohatec Start!-->
          <% }else if(Integer.parseInt(activityid) == 13){ %>
          <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
          <!-- Added by Dohatec End!-->
           <% } %>
  </div>
  <div >
    <% if(Integer.parseInt(activityid) == 1 ){ %>
 <div>  <jsp:include page="InformationBar.jsp">
         <jsp:param name="appId" value="<%=objectid%>" />
     </jsp:include></div>
  <%  }else if(Integer.parseInt(activityid) == 2
            ||Integer.parseInt(activityid)== 5 ||Integer.parseInt(activityid)== 6
            ||Integer.parseInt(activityid)== 4 ||Integer.parseInt(activityid)== 3 ||Integer.parseInt(activityid)== 8 || Integer.parseInt(activityid)== 9
                       || Integer.parseInt(activityid)== 13){
  pageContext.setAttribute("tenderId",objectid);%>

   <div> <%@include file="../resources/common/TenderInfoBar.jsp" %></div>
  <% } %>
  </div>
   <div class="tableHead_1 t_space">Workflow : Add Users</div>

  <table width="100%" cellspacing="0" class="tableList_1">
      <form method="POST" name="frmWorkflowUsers" action="WorkflowLevelEdit.jsp" id="frmWorkflowUsers" onsubmit="return valid_uservalues(<%=noRows%>);">
    <tr>
        <th width="13px" >Level No.</th>
        <th width="15px" >Workflow Role</th>
      <th>Procurement Role</th>
      <th>Name of Official ,[Designation at Office]</th>
       <th>Action</th>
    </tr>
    <%  if(isFileProcessed.equalsIgnoreCase("Yes")){  %>
    <tr>
        <td  class='t-align-center'>1.</td>
        <td  class='t-align-center' style="font-weight:bold;">Initiator</td>
      <td class='t-align-center'>

            <%

            TblWorkFlowLevelConfig wfl= tblworkFlowLevelconfig.get(0);
            Iterator rls =  procurementRoles.iterator();
            while(rls.hasNext()){
              TblProcurementRole procurementRole =(TblProcurementRole)rls.next();
             if(wfl.getProcurementRoleId() == procurementRole.getProcurementRoleId()) {
        out.write("<label id='"+procurementRole.getProcurementRoleId()+"' >"+procurementRole.getProcurementRole()+"</label>");
        out.write("<input type='hidden' value='"+procurementRole.getProcurementRoleId()+"' name='startsby' id='cmbstartsby' />");
             }
            }
       TblLoginMaster  loginMaster = wfl.getTblLoginMaster();

     String str=workFlowSrBean.getOfficialAndDesignationName(loginMaster.getUserId());

    %>
    <input type="hidden" name="startsby" id="cmbstartsby" value="" />
       </td>
        <td><label id="uservalue1"><%=str%></label>
            <input type="hidden" name="txtuserid" value="<%=loginMaster.getUserId()%>" id="txtuserid1" />
        </td>
        <td>&nbsp;</td>
    </tr>
    <%  }else{ %>
    <tr>
      <td class='t-align-center'>1</td>
      <td class='t-align-center' style="font-weight:bold;">Initiator</td>
      <td class='t-align-center'>
		<select name="startsby"  onchange="changeProRole(this.id)" class="formTxtBox_1" id="cmbstartsby1" style="width:100px;">
                    <%
                     String procureRole="";
                    TblWorkFlowLevelConfig wfl= tblworkFlowLevelconfig.get(0);
                    Iterator rls =  procurementRoles.iterator();
                    String stOffcAndDesg = " ";
                    while(rls.hasNext()){
                      TblProcurementRole procurementRole =(TblProcurementRole)rls.next();
                     if(wfl.getProcurementRoleId() == procurementRole.getProcurementRoleId()) {
                     if(procurementRole.getProcurementRole().equalsIgnoreCase("HOPE"))       //aprojit- Start
                         procureRole="HOPA";
                     else if(procurementRole.getProcurementRole().equalsIgnoreCase("PE"))       //aprojit- Start
                         procureRole="PA";
                     else
                         procureRole=procurementRole.getProcurementRole();
                out.write("<option selected='true' value='"+procurementRole.getProcurementRoleId()+
                      "' >"+procureRole+"</option>");                                       //aprojit- End
                     }else{
                          for(int i=0;i<startsBy.length;i++){
                            Byte b = Byte.valueOf(startsBy[i]);
                            if(procurementRole.getProcurementRoleId()==b){
                                if(procurementRole.getProcurementRole().equalsIgnoreCase("HOPE"))
                                     procureRole="HOPA";
                                else if(procurementRole.getProcurementRole().equalsIgnoreCase("PE"))
                                     procureRole="PA";
                                else
                                     procureRole=procurementRole.getProcurementRole();
                        out.write("<option value='"+procurementRole.getProcurementRoleId()+
                      "' >"+procureRole+"</option>");
                         }
                      }
                     }
                    }
               TblLoginMaster  loginMaster = wfl.getTblLoginMaster();
               stOffcAndDesg = commonSearchServiceSrBean.getUserNames("AAFillComboAPP", objectid, Integer.toString(wfl.getProcurementRoleId()),Integer.toString(1), activityid, childid, Integer.toString(1),loginMaster.getUserId());

%>

        </select>
      </td>
       <td class='t-align-center'>
            <div id="us1">
            <%=stOffcAndDesg%>
            </div>
       </td>
            <td>&nbsp;</td>
   </tr>

     <%  } %>
                    <%
                    String[] norevs =  donor.split("_");
                  int l = 2;
                  int val = Integer.parseInt(norevs[1]);
                  int norecords = tblworkFlowLevelconfig.size();
                  int exist = (norecords - 2);
                  int extr = 0;
                  int c = 2;
                  String colorchange = "style='background-color:#E4FAD0;' ";
                  if(val > exist)
                     extr =  val - exist;
                  for(int i=1;i<=exist;i++){

                   %>
    <tr <% if(c == l && val > 1) out.print(colorchange); else if(val == 1) out.print(colorchange); %> >
      <td class='t-align-center'><%=l%></td>
      <td class='t-align-center'>

          <%
           TblWorkFlowLevelConfig revwfl =   tblworkFlowLevelconfig.get(i);
           if(revwfl.getWfRoleId() == 4){
          out.write("<label id='revlab' style='font-weight:bold;'> Development Partner</label> <input id='revlabl' name='revlabl"+l+"' value='Donor' type='hidden' />");
          }
          else{
                out.write("<label id='revlab' style='font-weight:bold;'> Reviewer</label> <input id='revlabl' name='revlabl"+l+"' value='Reviewer' type='hidden' />");

          }
           %>

      </td>
      <td class='t-align-center'>
          <%

                 String displaystyle = "display:inline";
                 boolean check = false;
                if(revwfl.getProcurementRoleId() == 20){
                    displaystyle = "display:none";
                    check = true;
           %>
	    <label id="labelreviewer<%=l%>" >Development Partner</label>
         <input type="hidden" name="donorval<%=l%>"  value="20" id="donorval<%=l%>" />
         <% }else{
        %>
           <label id="labelreviewer<%=l%>" ></label>
         <input type="hidden" name="donorval<%=l%>"  value="" id="donorval<%=l%>" />
        <% } %>

                   <%

                    Iterator rev =  procurementRoles.iterator();
                    String revOffcAndDesg = " ";
                     String revdisplay = "display:table-cell;";
                    String donordisplay = "display:none";
                    String procureRolesCombo = "";
                    if(list.contains(revwfl.getTblLoginMaster().getUserId())){
                       Iterator pr =  procurementRoles.iterator();
                    while(pr.hasNext()){
                        TblProcurementRole procurementRole =(TblProcurementRole)pr.next();
                            if(procurementRole.getProcurementRoleId() == revwfl.getProcurementRoleId())
                                 out.write("<label id='"+procurementRole.getProcurementRoleId()+"' >"+procurementRole.getProcurementRole()+"</label>");

                          }
                        out.write("<input type='hidden' name='revlev"+l+"' value='"+revwfl.getProcurementRoleId()+"' id='revlev"+l+"' />");
                    }else if(!check){
                     procureRolesCombo =  commonSearchServiceSrBean.getProcureRoles(l,revwfl.getProcurementRoleId());
                    }
                     %>
                     <%=procureRolesCombo%>

      </td>
                     <%
        TblLoginMaster  lm = revwfl.getTblLoginMaster();
        String str2 = "";
        String usrvalue="";
        if(check == true && !list.contains(revwfl.getTblLoginMaster().getUserId())){

        revdisplay = "display:none;";
        donordisplay = "display:table-cell;";
        revOffcAndDesg = commonSearchServiceSrBean.getUserNames("AAFillComboAPP", objectid, Integer.toString(revwfl.getProcurementRoleId()),Integer.toString(3), activityid, childid, Integer.toString(l),lm.getUserId());
        }else if(list.contains(revwfl.getTblLoginMaster().getUserId())){
           revdisplay = "display:none;";
           donordisplay = "display:table-cell;";
            str2 = workFlowSrBean.getOfficialAndDesignationName(lm.getUserId());
           revOffcAndDesg = "<label id='uservalue"+l+"'>"+str2+"</label><input type='hidden' name='txtuserid' value='"+lm.getUserId()+"' id='txtuserid"+l+"' />";
        }else{

         str2 = workFlowSrBean.getOfficialAndDesignationName(lm.getUserId());

         usrvalue +="<label id='uservalue"+l+"'>"+str2+"</label><input type='hidden' name='txtuserid' value='"+lm.getUserId()+"' id='txtuserid"+l+"' />";

        }
          %>
      <td  id="tddon<%=l%>" class="t-align-center" style="<%=donordisplay%>">


        <div id="us<%=l%>">
         <%=revOffcAndDesg%>
        </div>
      </td>

        <td id="tdrev<%=l%>" class="t-align-center" style="<%=revdisplay%>">
          <%=usrvalue%>
      </td>

        <td id="tdlnk<%=l%>" class="t-align-center" style="<%=revdisplay%>" >

          <% if(Action.equalsIgnoreCase("Edit")) {

           %>
           <img src="../resources/images/Dashboard/removeIcn.png" alt="remove user" id="removeicn<%=l%>" class="linkIcon_1" />
           <a href="javascript:removeuser('remuser<%=l%>')" id='remuser<%=l%>' >Change User</a>
          <% } %>
          <img src="../resources/images/Dashboard/addIcn.png" style="display: none;" alt="add user" id="addicn<%=l%>" class="linkIcon_1" />
          <a href="#" id='adduser<%=l%>' style="display:none;"  onclick="var d =document.getElementById('donorval<%=l%>'); var b = document.getElementById('cmdreviewer<%=l%>'); window.open('AddWorkFlowUser.jsp?userval=uservalue'+<%=l%>+'&userid=txtuserid'+<%=l%>+'&wfroleid='+b.options[b.options.selectedIndex].value+'&uid='+<%=userid%>+'&donorval='+d.value+'','window','resizable=yes,scrollbars=yes,width=900,height=500')">Select User</a>

      </td>
          <td id="tdempty<%=l%>" style="<%=donordisplay%>">&nbsp;</td>
   </tr>
    <%
     if(c == l)
       c+=2;
       l++;
       }
      if(extr > 0){
          for(int i=1;i<=extr;i++){
  %>
    <tr <% if(c == l && val > 1) out.print(colorchange); else if(val == 1) out.print(colorchange); %> >
      <td class="t-align-center"><%=l%></td>
      <td class="t-align-center">

          <%
                 String displaystyle = "display:inline";
                 boolean check = false;
           if(!norevs[0].equalsIgnoreCase("no") && norevs[0] != null){
                out.write("<label id='revlab' style='font-weight:bold;'> Development Partner</label> <input id='revlabl' name='revlabl"+l+"' value='Donor' type='hidden' />");
                    displaystyle = "display:none";
                    check = true;
          }else{
              out.write("<label id='revlab' style='font-weight:bold;'> Reviewer</label> <input id='revlabl' name='revlabl"+l+"' value='Reviewer' type='hidden' />");
          }
           %>

      </td>
      <td class="t-align-center">
          <% if(check){ %>
          <label id="labelreviewer<%=l%>" >Development Partner</label>
         <input type="hidden" name="donorval<%=l%>"  value="20" id="donorval<%=l%>" />
         <%  }else{ %>
         <label id="labelreviewer<%=l%>" ></label>
         <input type="hidden" name="donorval<%=l%>"  value="" id="donorval<%=l%>" />
                <% } %>


                   <%
                    Iterator rev =  procurementRoles.iterator();
                    boolean revusernames = false;
                    String revOffcAndDesg = " ";
                    String revdisplay = "display:table-cell;";
                    String donordisplay = "display:none";
                    String procureRolesCombo = "";


                     if(check){
                        revOffcAndDesg = commonSearchServiceSrBean.getUserNames("AAFillComboAPP", objectid, Integer.toString(20),Integer.toString(3), activityid, childid, Integer.toString(l),0);
                        revdisplay = "display:none;";
                        donordisplay = "display:table-cell;";

                    }else if(!check){
                     procureRolesCombo =  commonSearchServiceSrBean.getProcureRoles(l,0);
                    }
                     %>
                    <div id="prostr<%=l%>">
                        <%=procureRolesCombo%>
                     </div>

      </td>
       <td  id="tddon<%=l%>" class="t-align-center" style="<%=donordisplay%>">
          <div id="us<%=l%>">
         <%=revOffcAndDesg%>
          </div>

      </td>
      <td id="tdrev<%=l%>" class="t-align-center" style="<%=revdisplay%>">

      </td>
       <td id="tdlnk<%=l%>" class="t-align-center" style="<%=revdisplay%>" >
      <% if(Action.equalsIgnoreCase("Edit")) {

           %>
            <img src="../resources/images/Dashboard/addIcn.png" alt="add user" id="addicn<%=l%>" class="linkIcon_1" />
            <a href="#" id='adduser<%=l%>'   onclick=" var b = document.getElementById('cmdreviewer<%=l%>');
                var d =document.getElementById('donorval<%=l%>'); window.open('AddWorkFlowUser.jsp?userval=uservalue'+<%=l%>+'&userid=txtuserid'+<%=l%>+'&wfroleid='+b.options[b.options.selectedIndex].value+'&uid='+<%=userid%>+'&donorval='+d.value+'','window','resizable=yes,scrollbars=yes,width=900,height=500')">Select User</a>
          <% } %>
          <img src="../resources/images/Dashboard/removeIcn.png" style="display: none;"  alt="remove user" id="removeicn<%=l%>" class="linkIcon_1" />
          <a href="javascript:removeuser('remuser<%=l%>')" style="display:none;" id='remuser<%=l%>' >Change User</a>
      </td>

          <td id="tdempty<%=l%>" style="<%=donordisplay%>">&nbsp;</td>
    </tr>
  <%  if(c == l)
       c+=2;
       l++; } } %>
    <tr <% if(c == l && val != 1) out.print(colorchange); %>>
      <td class='t-align-center'><%=l%></td>
      <td class='t-align-center' style="font-weight:bold;">Approver</td>
     <% TblWorkFlowLevelConfig wflc =   tblworkFlowLevelconfig.get(norecords-1);
      String endOffcAndDesg = " ";
       List<Object> cpId = null;
                    int isPECP = 0;
                    if(Integer.parseInt(activityid) == 13 && Integer.parseInt(eventid) == 12)
                    {
                    cpId = comService.getCommitteeCPId(objectid);
                    if(cpId!=null && !cpId.isEmpty()){
                        if(cpId.get(0).toString().equals(userid))
                            isPECP = 1;
                        }
                    }
     if(list.contains(wflc.getTblLoginMaster().getUserId())){

            %>
            <td class='t-align-center'>

            <%
                   
                    Iterator endsrls =  procurementRoles.iterator();
                    while(endsrls.hasNext()){
                      TblProcurementRole procurementRole =(TblProcurementRole)endsrls.next();
                           if(procurementRole.getProcurementRoleId() == wflc.getProcurementRoleId() && procurementRole.getProcurementRoleId()!=isPECP)
                        out.write("<label id='"+procurementRole.getProcurementRoleId()+"' >"+procurementRole.getProcurementRole()+"</label>");
                        out.write("<input type='hidden' value='"+procurementRole.getProcurementRoleId()+"' name='endsby' id='cmbendsby"+l+"' />");

                      }
                    %>

      </td>
            <%  }else{ %>
      <td class='t-align-center'>
	   <select name="endsby" onchange="changeProRole(this.id)" class="formTxtBox_1" id="cmbendsby<%=l%>" style="width:100px;">
            <%

                    Iterator endsrls =  procurementRoles.iterator();
                    String procureRoleA="";
                    while(endsrls.hasNext()){
                      TblProcurementRole procurementRole =(TblProcurementRole)endsrls.next();
                           if(procurementRole.getProcurementRoleId() == wflc.getProcurementRoleId()){
                                if(procurementRole.getProcurementRole().equalsIgnoreCase("HOPE"))       //aprojit- Start
                                    procureRoleA="HOPA";
                                else if(procurementRole.getProcurementRole().equalsIgnoreCase("PE"))       //aprojit- Start
                                    procureRoleA="PA";
                               else
                                    procureRoleA=procurementRole.getProcurementRole();
                        out.write("<option selected='true' value='"+procurementRole.getProcurementRoleId()+
                              "' >"+procureRoleA+"</option>");
                              }else{
                       for(int i=0;i<endsBy.length;i++){
                      Byte b = Byte.valueOf(endsBy[i]);
                      if(procurementRole.getProcurementRoleId()==b && procurementRole.getProcurementRoleId()!=isPECP){
                               if(procurementRole.getProcurementRole().equalsIgnoreCase("HOPE"))    //aprojit- Start   
                                    procureRoleA="HOPA";
                               else if(procurementRole.getProcurementRole().equalsIgnoreCase("PE"))    //aprojit- Start   
                                    procureRoleA="PA";
                               else
                                    procureRoleA=procurementRole.getProcurementRole();
                           out.write("<option  value='"+procurementRole.getProcurementRoleId()+
                              "' >"+procureRoleA+"</option>");
                          }
                         }
                      }

                      }
                    %>
        </select>
      </td>
        <% } if(list.contains(wflc.getTblLoginMaster().getUserId())){ %>
      <td class='t-align-left' >
          <%
          TblLoginMaster tlm =  wflc.getTblLoginMaster();
          String str3=workFlowSrBean.getOfficialAndDesignationName(tlm.getUserId());
          out.write("<label id='uservalue"+l+"'>"+str3+"</label>");
        out.write("<input type='hidden' name='txtuserid' value='"+tlm.getUserId()+"' id='txtuserid"+l+"' ");%>
          </td>   <% }else{ %>
           <td class='t-align-center'>
          <%
          TblLoginMaster tlm =  wflc.getTblLoginMaster();
          endOffcAndDesg = commonSearchServiceSrBean.getUserNames("AAFillComboAPP", objectid, Integer.toString(wflc.getProcurementRoleId()),Integer.toString(2), activityid, childid, Integer.toString(l),tlm.getUserId());
          %>
             <div id="us<%=l%>">
          <%=endOffcAndDesg%>
             </div>
             </td>
             <%  } %>

             <td>&nbsp;</td>
    </tr>
       <tr>
           <td class='t-align-center' colspan="5"><span id="wfsub" class="formBtn_1">
           <%
                if(request.getParameter("activityid").equalsIgnoreCase("13") && request.getParameter("eventid").equalsIgnoreCase("12")) {
           %>
                    <input type="button"  value="Submit" id="btnWfSubmit" name="btnWfSubmit" onclick="dis(this.id);" />
                    <input type="hidden" name="btnsubmit" id="btnsubmit"/>
           <%
                } else {
           %>
                <input type="submit"  value="Submit" id="btnsubmit" name="btnsubmit" onclick="dis(this.id);" />
           <%}%>
        <input type="hidden" value="<%=eventid%>" name="eventid" />
        <input type="hidden" value="<%=activityid%>" name="activityid" id="activityid" />
        <input type="hidden" value="<%=objectid%>"  name="objectid" id="objectid" />
        <input type="hidden" value="<%=childid%>" name="childid" id="childid" />
        <input type="hidden" value="<%=Action%>" name="action" />
        <input type="hidden" value="<%=moduleName%>" name="modulename" />
        <input type="hidden" value="<%=eventName%>" name="eventname" />
        <input type="hidden" value="<%=l%>" name="trw" id="trw" />
        <input type="hidden" value="" name="userids" id="userids" />
         <input type="hidden" value="" name="procureids" id="procureids" />
         <input type="hidden" value="<%=isFileProcessed%>" name="isFileProcessed" id="isFileProcessed" />
         <input type="hidden" value="<%=fileOnHandUser%>" name="fileOnHand" id="fileOnHand" />
         <input type="hidden" value="<%=wpId%>" name="wpId" id="wpId" />
        <!-- Dohatec Start -->
        <input type="hidden" value="<%=request.getParameter("activityid") + " " + request.getParameter("eventid")%>" name="evntActId" id="evntActId"/>
        <!-- Dohatec End -->
        </span>
        &nbsp;&nbsp;

        </td>
      </tr>
      </form>
  </table>

  </div>

        <!-- Dohatec Start -->
        <!-- Dialog Box -->
        <div id="dialog-confirm" title="Contract Approval Workflow" style="display:none">
            <p>
                Are you sure that you have created workflow and number of  Reviewers Correctly\Rightly? Be sure before clicking ‘Yes’ button.
            </p>
        </div>
        <!-- Dohatec End -->

    <!--Dashboard Content Part End-->
    <!--Dashboard Footer Start-->
     <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
    <!--Dashboard Footer End-->

</body>
    <%
        String str ="headTabWorkFlow";
        if(request.getParameter("hed")!=null){
            str = request.getParameter("hed").trim();
            }
    %>
     <script type="text/javascript">
        var headSel_Obj = document.getElementById('<%=str%>');
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
     <script language="JavaScript" type="text/javascript">
              function changerole(x){
             //alert(x);
             var str = new String(x);
             var str2 =str.substring(str.length-1, str.length);
             //alert(str2);
             var rev=document.getElementById(x);
            var wfrole = rev.options[rev.selectedIndex].value;
            var labl = document.getElementById("us"+str2);

             var tddon = document.getElementById("tddon"+str2);
               var tdrev = document.getElementById("tdrev"+str2);
               var tdlnk = document.getElementById("tdlnk"+str2);
               var tdempty = document.getElementById("tdempty"+str2);

            if(wfrole == "Donor"){

                var rev1 = document.getElementById('cmdreviewer'+str2);
               var lab =  document.getElementById('labelreviewer'+str2);
                var txt = document.getElementById("donorval"+str2);
                 var proroles = document.getElementById('prostr'+str2);
                tdrev.innerHTML='';
                 tdrev.style.display='none';
                tddon.style.display='table-cell';
                tdlnk.style.display='none';
                tdempty.style.display='table-cell';
                proroles.innerHTML = "";
                //alert(txt);
               for (m=rev1.options.length-1;m>0;m--) {
                   //alert(rev1.options[m].value);
                   //if(rev1.options[m].value == '20') {
                       //alert(rev1.options[m].value);
                     //alert(rev1);
                       rev1.style.display='none';
                       lab.style.display='inline';
                        //rev1.options[m].selected=true;
                        lab.innerHTML = 'Development Partner';
                        txt.value = '20';
                        //alert('end');
                   //}

               }

                labl.innerHTML='';
                $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName:'AAFillComboAPP',objectId:$('#objectid').val(),Prorole:'20',childId:$('#childid').val(),activityId:$('#activityid').val(),wfroleId:'4',revId:str2}, function(j){
                 $(labl).html(j);
                //alert(j);
               });

            }else{
                //alert(wfrole);
                var proroles = document.getElementById('prostr'+str2);

                var lab =  document.getElementById('labelreviewer'+str2);
               var txt = document.getElementById("donorval"+str2);
               var rem = document.getElementById("remuser"+str2);
               var add = document.getElementById('adduser'+str2);
                var remicn = document.getElementById("removeicn"+str2);
                var addicn = document.getElementById("addicn"+str2);

                lab.style.display='none';
                txt.value="";
                tdrev.style.display='table-cell';
                tddon.style.display='none';
                tdlnk.style.display='table-cell';
                tdempty.style.display='none';

                labl.innerHTML='';
                 rem.style.display='none';
               add.style.display='inline';
               remicn.style.display="none";
               remicn.style.display='none';
               addicn.style.display="table-cell";
               addicn.style.display="inline";
               var test = "";

                $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName:'proRoles',revId:str2,selected:'0'}, function(j){
                 $(proroles).html(j);
               });
            }

            }

            function changeProRole(x){
              var str = new String(x);
              var str2 = null;
              var Prorole = 0;
              var revid = 0;
              var wfrolid = 0;
              if(str != null)
              revid =str.substring(str.length-1, str.length);
              str2 = str.substr(0, str.length-1);

             if(str2 == 'cmbstartsby'){
                 wfrolid = 1;
                 var labl = document.getElementById("us"+1);
                 var temp = document.getElementById(str);
                 Prorole = temp.options[temp.selectedIndex].value;
                 $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName:'AAFillComboAPP',objectId:$('#objectid').val(),Prorole:Prorole,childId:$('#childid').val(),activityId:$('#activityid').val(),wfroleId:wfrolid,revId:revid}, function(j){
                 $(labl).html(j);
                 });
             }else if(str2 == 'cmbendsby'){
                 //alert('endsby');
                 wfrolid = 2;
                 var labl = document.getElementById("us"+revid);
                 var temp = document.getElementById(str);
                 Prorole = temp.options[temp.selectedIndex].value;

                  $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName:'AAFillComboAPP',objectId:$('#objectid').val(),Prorole:Prorole,childId:$('#childid').val(),activityId:$('#activityid').val(),wfroleId:wfrolid,revId:revid}, function(j){
                    $(labl).html(j);
                });
             }else{
                 wfrolid = 3;
                 var labl = document.getElementById("us"+revid);
                 var temp = document.getElementById(str);
                 Prorole =temp.options[temp.selectedIndex].value;

                 var selectedval = document.getElementById("revlev"+revid);
                 selectedval.value = Prorole;
                 var tddon = document.getElementById("tddon"+revid);
                 var tdlnk = document.getElementById("tdlnk"+revid);
                 var labl = document.getElementById("us"+revid);
                 var tdrev = document.getElementById("tdrev"+revid);

                 if(Prorole != 21){
                     labl.innerHTML='';
                     tddon.style.display='none';
                     tdlnk.style.display='table-cell';
                     removeuser('remuser'+revid);
                 }
                 else
                 {
                    var rem = document.getElementById('remuser'+revid);
                    var add = document.getElementById('adduser'+revid);
                    var remicn = document.getElementById("removeicn"+revid);
                    var addicn = document.getElementById("addicn"+revid);
                    rem.style.display='none';
                    add.style.display='inline';
                    remicn.style.display="none";
                    remicn.style.display='none';
                    addicn.style.display="table-cell";
                    addicn.style.display="inline";
                    tddon.style.display='table-cell';
                    tdlnk.style.display='none';
                    tdrev.innerHTML='';
                    $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName:'AAFillComboAPP',objectId:$('#objectid').val(),Prorole:Prorole,childId:$('#childid').val(),activityId:$('#activityid').val(),wfroleId:wfrolid,revId:revid}, function(j){
                        $(labl).html(j);
                    });
                }
             }
         }

            function removeuser(y) {
                var str = new String(y);
                var str2 =str.substring(str.length-1, str.length);
                //alert(str2);
                var rem = document.getElementById(y);
                var add = document.getElementById('adduser'+str2);
                var label = document.getElementById('uservalue'+str2);
                var remicn = document.getElementById("removeicn"+str2);
                var addicn = document.getElementById("addicn"+str2);
                var tdrev = document.getElementById("tdrev"+str2);
                //alert(remicn);
                tdrev.innerHTML='';
                label.style.display='none';
                label.innerHTML ="";
               rem.style.display='none';
               add.style.display='inline';
               remicn.style.display="none";
               remicn.style.display='none';
               addicn.style.display="table-cell";
               addicn.style.display="inline";

               var b = document.getElementById('cmdreviewer'+str2);
               var d =document.getElementById('donorval'+str2);
               window.open('AddWorkFlowUser.jsp?userval=uservalue'+str2+'&userid=txtuserid'+str2+'&wfroleid='+b.options[b.options.selectedIndex].value+'&uid='+<%=userid%>+'&donorval='+d.value+'','window','resizable=yes,scrollbars=yes,width=900,height=500');
            }

             function valid_uservalues(norows){
               //alert(norows);
                var check = true;
                var uvalues = new Array();
                for(var i=1;i<=norows;i++){
                  var lab=  document.getElementById("txtuserid"+i);
                   //alert(lab);
                   if(lab != null){
                        uvalues[i-1]=lab.value;
                   }

                 // alert(uvalues[i-1]);
                 //alert('cam');
                 if(lab != null){
                  if(lab.value == "" || lab.value == 0){
                      //alert('cam2');
                       jAlert("User not available"," Alert ", "Alert");
                       $("#wfsub").show();
                      // alert("Please select official");
                        check = false;
                        break;
                  }
                }else if(lab == null){
                     //alert('cam2');
                       jAlert(" Please select user"," Alert ", "Alert");
                       $("#wfsub").show();
                      // alert("Please select official");
                        check = false;
                        break;
                }
                }
                if(check){

                    // Dohatec Start
                    if($('#evntActId').val() == '13 12')
                        return true;
                    // Dohatec End

                    //alert(uvalues.length);
                    for(var j=1;j<uvalues.length-1;j++){
                        if(uvalues[0] == uvalues[j] || uvalues[uvalues.length-1] == uvalues[j]){
                             check=false;
                              jAlert("Reviewer can't be as same as Initiator/Approver"," Alert ", "Alert");
                              $("#wfsub").show();
                        }

                    }
                }

                if(check){

                    // Dohatec Start
                    if($('#evntActId').val() == '13 12')
                        return true;
                    // Dohatec End

                    for(var j=1;j<uvalues.length-1;j++){
                        for(var k=j;k<uvalues.length;k++){

                            if(uvalues[j] == uvalues[k+1]){
                                //alert('user already added');
                                jAlert("user already added"," Alert ", "Alert");
                                $("#wfsub").show();
                                 return false;
                            }

                        }

                    }
                }

                //alert(check);
               return check;
            }

            function dis(ds){

                  // Dohatec Start
                  <%if(request.getParameter("activityid").equalsIgnoreCase("13") && request.getParameter("eventid").equalsIgnoreCase("12")) {%>
                      if($('#evntActId').val() == '13 12')
                      {
                            $(function() {
                                $( "#dialog-confirm" ).dialog({
                                    resizable: false,
                                    height:150,
                                    width:430,
                                    modal: true,
                                    buttons: {
                                        "Yes": function() {
                                            $(this).dialog("close");
                                            $('#btnsubmit').val('Submit');
                                            $('form#frmWorkflowUsers').submit();
                                        },
                                        "No": function() {
                                            $(this).dialog("close");
                                        }
                                    }
                                });
                            });
                      }
                  <%}%>
                  // Dohatec End
                  
                  var buf = "";
                  var trws = document.getElementById("trw").value;
                  var prbuf = "";
                  var  test = "";
                  for(var h=2;h<trws;h++){

                    if(document.getElementById("revlev"+h)!= null ){
                     test = document.getElementById("revlev"+h).value;
                     prbuf+=test;
                    prbuf+='@';
                    }else if(document.getElementById("donorval"+h).value != null && document.getElementById("donorval"+h).value != ""){
                        test = document.getElementById("donorval"+h).value;
                         prbuf+=test;
                        prbuf+='@';
                    }
                  }
                  //alert(trws);
                  for(var i=1;i<=trws;i++){
                     var uid =  document.getElementById('txtuserid'+i);
                     //alert(uid);
                     buf+=uid.value;
                     buf+='@';
                  }

                  //alert(buf);
                 // alert(prbuf);

                  document.getElementById('procureids').value=prbuf;
                  document.getElementById('userids').value=buf;
                //$("#wfsub").hide(); - Previous Code

                // Dohatec Start
                if($('#evntActId').val() != '13 12'){
                    $("#wfsub").hide();
                }
                // Dohatec End

            }
           function changeuser(us){
               //alert(us);
                var selusid = document.getElementById(us);
                var str = new String(us);
                var str2 =str.substring(str.length-1, str.length);
                //alert(str2);
                var uid =  document.getElementById('txtuserid'+str2);
                uid.value = selusid.options[selusid.selectedIndex].value;

            }
        </script>

        <% } %>
</html>

