<!--
Document   : APPDashboard
Created on : Oct 23, 2010, 6:13:00 PM
Author     : Sanjay , Rishita, Rikin
-->
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AppAdvSearchService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowEventConfig"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<jsp:useBean id="appSrBean" class="com.cptu.egp.eps.web.servicebean.APPSrBean" />
<jsp:useBean id="appauditServlet" class="com.cptu.egp.eps.web.servlet.APPServlet" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Annual Procurement Plan (APP) Dashboard</title>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <!--jalert -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>


        <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>

        <%
            boolean IsAPPUsedInDeposit = false;
            String appCreatedUser = "";
            String userid = "";
            if (session.getAttribute("userId") != null) {
                userid = session.getAttribute("userId").toString();
            }
            tenderSrBean.setLogUserId(userid);
            appSrBean.setLogUserId(userid);
            appauditServlet.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));

            int appId = 0;
            if (!"".equalsIgnoreCase(request.getParameter("appID"))) {
                appId = Integer.parseInt(request.getParameter("appID"));
            }

        %>
        <script type="text/javascript">
            <%--function loadFirtTime(){
                $.post("<%=request.getContextPath()%>/SearchPckServlet", {pkgNo: " ",estimatedCost:0 ,status:"Pending",procurementNature:" ",procurementType:" ",appId:<%=appId%>},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                });
            }--%>
            function loadSearchPackageTable()
            {
                if (document.getElementById("txtPackageNo").value == '' && document.getElementById("txtEstimateCost").value == '' && document.getElementById("cmbProcureNature").value == "" && document.getElementById("cmbProcureType").value == "" && document.getElementById("cmbStatus").value == "")
                {
                    document.getElementById("Error").innerHTML = "Please select at least one search criteria.";
                    return false;
                }
            <%--$.post("<%=request.getContextPath()%>/SearchPckServlet", {pkgNo: $("#txtPackageNo").val(),estimatedCost:$("#txtEstimateCost").val(),status:$("#cmbStatus").val(),procurementNature:$("#cmbProcureNature").val(),procurementType:$("#cmbProcureType").val(),appId:1},  function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
            });--%>
            }
            $(document).ready(function () {
                $("#frmAPPDashboard").validate({
                    rules: {
                        estimatecost: {digits: true}
                    },
                    messages: {
                        estimatecost: {digits: "<div class='reqF_1'>Please Enter Numbers Only.</div>"}
                    }
                });
            });
            function clearTo()
            {
                if (document.getElementById("txtPackageNo").value != '')
                {
                    document.getElementById("Error").innerHTML = "";
                } else if (document.getElementById("txtEstimateCost").value != '')
                {
                    document.getElementById("Error").innerHTML = "";
                } else if (document.getElementById("cmbProcureNature").value != '- Select Procurement Category -')
                {
                    document.getElementById("Error").innerHTML = "";
                } else if (document.getElementById("cmbProcureType").value != '- Select Procurement Type -')
                {
                    document.getElementById("Error").innerHTML = "";
                } else if (document.getElementById("cmbStatus").value != '- Select Status -')
                {
                    document.getElementById("Error").innerHTML = "";
                }
            }
        </script>
        <form id="frmInsUpdate" name="frmInsUpdate" method="POST" action="APPDashboard.jsp?appID=<%=appId%>">
            <input type="hidden" name="appID" id="appID" value="<%=appId%>"/>
        </form>
        <script>
            function delPkg(pkgId) {
                if (confirm('Do you really want to remove the package?')) {
                    $.ajax({
                        url: "<%=request.getContextPath()%>/APPServlet?param1=" + pkgId + "&appId=<%=appId%>&funName=delPkg",
                        method: 'POST',
                        async: false,
                        success: function (j) {
                            if (j == false) {
                                location.href = 'APPDashboard.jsp?appID=<%=appId%>&msg=Error';
                            } else {
                                document.getElementById("frmInsUpdate").submit();
                            }

                        }
                    });
                }
            }

            function revisePackage(appId, PackageId) {
            var url = "/officer/AddPackageDetail.jsp?appId=" + appId + "&pkgId=" + PackageId + "&action=Revise";
                if (confirm('Do you really want to revise the package .Package should be processed through workflow again.')) {
                    //var url = "/officer/AddPackageDetail.jsp?appId="+appId+"&pkgId="+PackageId+"&action=Revise";
                    window.location = url;
                }         
            }  
            function confirmCreateTender(appId, PackageId) {
            var url = "/officer/APPDashboardLink.jsp?appId=" + appId + "&pckId=" + PackageId;  
                jConfirm("This action will create a tender in the system with a unique tender ID. If you sure about creating a tender click Yes", "Confirm Create Tender", function(RetVal) {
                    if (RetVal) {
                         window.location = url;
                    }
                });        
            }
            function alertforHopa(){
                jAlert('There is no HOPA in this Office');       
            }
            function cancelRevision() {
                if (confirm('Do you really want to cancel the revised package details .Package changes will be reverted')) {
                    return true;
                } else {
                    return false;
                }

            }

        </script>
        <script type="text/Javascript">
            function chkappWithNoPEOfc(){
            jAlert("No PA office found in One or more Contract Framework Packages"," Alert ", "Alert");
            }
            function chkispkgexistAtPub(){
            jAlert("No Packages found. Please add Package details before publishing"," Alert ", "Alert");
            }
            function chkispkgexist(){
            jAlert("To Process file in Workflow at least one Package required"," Alert ", "Alert");
            }
            function checkdefaultconf(){
            jAlert("Default workflow configuration hasn't crated"," Alert ", "Alert");
            }
            function checkDates(){
            jAlert("Please configure package dates"," Alert ", "Alert");
            }
            function pkgcheck(){
            jAlert("One Package is already processed in workflow,You can process this Package only once the previous package is processed "," Alert ", "Alert");
            }
            function showbusconfig(){
            jAlert("Tender payment business rules not yet configured"," Alert ", "Alert");
            }
            function showprocmethodconfig(){
            jAlert("Procurement Method business rules not yet configured"," Alert ", "Alert");
            }

        </script>
    </head>
    <jsp:useBean id="appServlet" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <%
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        List<CommonAppData> ImplementingAgencies = new ArrayList<CommonAppData>();
        ImplementingAgencies = appServlet.getAPPDetails("ImplementingAgency", String.valueOf(appId),"");
        if(ImplementingAgencies.size() > 0)
        {
            IsAPPUsedInDeposit = true;
        }
        
        int uid = 0;
        if (session.getAttribute("userId") != null) {
            Integer ob1 = (Integer) session.getAttribute("userId");
            uid = ob1.intValue();
        }
        WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
        short eventid = 1;
        int objectid = 0;
        String financialYear = "";
        short activityid = 1;
        int childid = 1;
        short wflevel = 1;
        int initiator = 0;
        int approver = 0;
        boolean isInitiater = false;
        boolean evntexist = false;
        String fileOnHand = "No";
        String checkperole = "No";
        String checkaurole = "No";
        String chPackage = "No";
        boolean ispublish = false;
        boolean iswfLevelExist = false;
        boolean isconApprove = false;
        String appStatus = "";
        String appWorkFlowStatus = "pending";
        boolean ispkgexist = false;
        int count = 2;
        String msg = "";
        String isFileProcessed = "No";
        msg = request.getParameter("msg");
        // package grid code
        String objappid = request.getParameter("appID");
        if (objappid != null) {
            objectid = Integer.parseInt(objappid);
            childid = objectid;
        }
        String donor = "";
        donor = workFlowSrBean.isDonorReq(String.valueOf(eventid),
                Integer.valueOf(objectid));
        String[] norevs = donor.split("_");
        int norevrs = -1;

        String strrev = norevs[1];
        if (!strrev.equals("null")) {

            norevrs = Integer.parseInt(norevs[1]);
        }

        AppAdvSearchService appAdvSearchService = (AppAdvSearchService) AppContext.getSpringBean("AppAdvSearchService");
        String procurementNature = " ";
        if (request.getParameter("procurenature") != null) {
            procurementNature = request.getParameter("procurenature");
        }

        String procurementType = " ";
        if (request.getParameter("procuretype") != null) {
            procurementType = request.getParameter("procuretype");
        }

        String pckNo = " ";
        if (request.getParameter("packageno") != null) {
            pckNo = request.getParameter("packageno");
        }

        String value = "";
        if (request.getParameter("estimatecost") != null) {
            value = request.getParameter("estimatecost");
        }

        Integer EstimatedCost = 0;
        if (value != null && !value.equals("")) {
            EstimatedCost = Integer.parseInt(value);
        }

        String status = "Pending,Approved,BeingRevised,Revised";
        if (request.getParameter("status") != null && !"".equalsIgnoreCase(request.getParameter("status"))) {
            status = request.getParameter("status");
        }

        List<CommonAppSearchData> commonData = appAdvSearchService.getSearchResults(" ", " ", procurementNature, 0, 0, " ", " ", procurementType, appId, " ", pckNo, new BigDecimal(EstimatedCost), 0, " ", " ", new BigDecimal(0), new BigDecimal(0), 1, 1000, status, 0);
        int srno = 1;
        boolean isAddDatesConf = false;
        StringBuilder strProc = new StringBuilder();

        if (commonData.size() > 0) {
            ispkgexist = true;
        }

        boolean isProcWork = false;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < commonData.size(); i++) {
            CommonAppSearchData commonAppSearchData = commonData.get(i);
            if ("Works".equalsIgnoreCase(commonAppSearchData.getProcurementNature())) {
                isProcWork = true;
            }
            if (!((appAdvSearchService.countForQuery("TblAppPqTenderDates", "appid = " + commonAppSearchData.getAppId() + " and packageId = " + commonAppSearchData.getPackageId())) > 0)) {
                isAddDatesConf = true;
                sb.append("false,");
            } else {
                sb.append("true,");
            }
        }
        if (sb != null) {
            if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            }
        }

    %>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <%--<jsp:include page="../resources/common/AfterLoginTop.jsp" ></jsp:include>--%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%-- <jsp:include page="../resources/common/AfterLoginLeft.jsp"/>--%>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <%                                java.util.List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
                                appListDtBean = appServlet.getAPPDetails("App", request.getParameter("appID"), "");
                                appCreatedUser = appListDtBean.get(0).getFieldName7();
                                
                                boolean isHopaExist = appSrBean.isHopaExist();
                            %>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td>
                                        <div class="pageHead_1">
                                            Annual Procurement Plan (APP) Dashboard
                                            <!--                                            <span style="float: right;" ><a onclick="setFocus()" class="action-button-view">View Package Details</a></span>-->
                                        </div>
                                        <% if (request.getParameter("msg") != null && "Error".equalsIgnoreCase(request.getParameter("msg"))) {%>
                                        <tr>
                                            <td colspan="2"><div class="responseMsg errorMsg"><%=appMessage.delPackageErrMsg%></div></td>
                                        </tr>
                                        <% } %>
                                        <%if (request.getParameter("from") != null && "msg".equalsIgnoreCase(request.getParameter("from"))) {%>
                                        <div>&nbsp;</div>
                                        <div class="responseMsg successMsg t_space">APP Packages published successfully</div>
                                        <%}%>
                                        <%if (request.getParameter("msg") != null && "Update".equalsIgnoreCase(request.getParameter("msg"))) {%>
                                        <div>&nbsp;</div>
                                        <div class="responseMsg successMsg t_space">Package Dates updated successfully</div>
                                        <%}%>
                                        <%if (request.getParameter("cancelPkgRevision") != null && "succ".equalsIgnoreCase(request.getParameter("cancelPkgRevision"))) {%>
                                        <div>&nbsp;</div>
                                        <div class="responseMsg successMsg t_space">Package Revision canceled successfully</div>
                                        <%}%>
                                        <%if (request.getParameter("cancelPkgRevision") != null && "fail".equalsIgnoreCase(request.getParameter("cancelPkgRevision"))) {%>
                                        <div>&nbsp;</div>
                                        <div class="responseMsg errorMsg ">Package Revision canceling failed.</div>
                                        <%}%>
                                        <form method="POST" id="frmAPPDashboard" action="">
                                            <div class="tableHead_1 t_space">APP Information Bar :</div>
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr><% for (CommonAppData details : appListDtBean) {%>
                                                    <td width="6%" class="ff">APP ID	: </td>
                                                    <td width="20%"><%=details.getFieldName1()%></td>
                                                    <td width="6%" class="ff" nowrap="nowrap">Letter Ref. No. : </td>
                                                    <td width="20%" ><%=details.getFieldName3()%></td>
                                                    <td width="7%" class="ff" nowrap="nowrap">Project Name (If Applicable) : </td>
                                                    <td width="45%">
                                                        <% if (!"".equals(details.getFieldName6())) {
                                                                out.print(details.getFieldName6());
                                                            } else {
                                                                out.print("Not Applicable");
                                                            }%>
                                                    </td>
                                                </tr>
                                                <tr> <%financialYear = details.getFieldName4();%>
                                                    <td class="ff" nowrap="nowrap">Financial Year : </td>
                                                    <td><%=details.getFieldName4()%></td>
                                                    <input type="hidden" id="financialYear" value="<%=details.getFieldName4()%>"/>
                                                    <td class="ff" nowrap="nowrap">Budget Type : </td>
                                                    <td><%=details.getFieldName5()%></td>
                                                    <% if(details.getFieldName8()!=null && !details.getFieldName8().equalsIgnoreCase("")) { %>
                                                        <td class="ff" nowrap="nowrap">Activity Name: </td>
                                                        <td><%=details.getFieldName8()%></td>
                                                    <% } else {  %>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    <% } %>
                                                </tr>
                                                <!--
                                                <tr>
                                                        <td class="ff">Project Name (If Applicable): </td>
                                                        <td><%=details.getFieldName6()%></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>-->
                                                <%

                                                    boolean isiniappsame = false;
                                                    evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
                                                    List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
                                                    // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);

                                                    count = tblWorkFlowLevelConfig.size();
                                                    if (tblWorkFlowLevelConfig.size() > 0) {
                                                        Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                                        iswfLevelExist = true;
                                                        while (twflc.hasNext()) {
                                                            TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                                            TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                                            if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                                                                fileOnHand = workFlowlel.getFileOnHand();
                                                            }
                                                            if (workFlowlel.getWfRoleId() == 1) {
                                                                initiator = lmaster.getUserId();
                                                            }
                                                            if (workFlowlel.getWfRoleId() == 2) {
                                                                approver = lmaster.getUserId();
                                                            }
                                                            if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                                                                isInitiater = true;
                                                            }
                                                            if (workFlowlel.getWfRoleId() == 1 && workFlowlel.getFileOnHand().equalsIgnoreCase("No")) {
                                                                isFileProcessed = "Yes";
                                                            }
                                                        }
                                                    }

                                                    if (initiator == approver) {
                                                        isiniappsame = true;
                                                    }
                                                    String field1 = "CheckPE";
                                                    List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, userid, "");
                                                    if (editdata.size() > 0) {
                                                        checkperole = "Yes";
                                                    }
                                                    
                                                    List<CommonAppData> editdata2 = workFlowSrBean.editWorkFlowData("CheckAu", userid, "");
                                                    if (editdata2.size() > 0) {
                                                        checkaurole = "Yes";
                                                    }

                                                    // for publish link
                                                    List<SPTenderCommonData> publist = tenderCommonService.returndata("AppWorkFlowStatus", request.getParameter("appID"), "");
                                                    if (publist.size() > 0) {
                                                        ispublish = true;
                                                        SPTenderCommonData spTenderCommondata = publist.get(0);
                                                        appStatus = spTenderCommondata.getFieldName1();
                                                        appWorkFlowStatus = spTenderCommondata.getFieldName2();
                                                        if (appStatus.equalsIgnoreCase("Approved")) {
                                                            appWorkFlowStatus = "Approved";
                                                        }

                                                        if (appWorkFlowStatus.equalsIgnoreCase("Conditional Approval")) {
                                                            isconApprove = true;
                                                        }
                                                    }
                                                     //Start Nitish -- 28/05/2017
                                                    
                                                    java.util.List<CommonAppData> appWithNoPEOfc = new java.util.ArrayList<CommonAppData>();
                                                    appWithNoPEOfc = appServlet.getAPPDetails("AppOfficeFramework", request.getParameter("appID"), "");
                                                    
                                        //this appWithNoPEOfc variable check if the method Framework Contract has office or not
                                        //Stored Procedure execute in p_getappcommondata --> search in -- "AppOfficeFramework"
                                                    
                                                    if (userid.equalsIgnoreCase(appCreatedUser)) {
                                                        /*if (appWithNoPEOfc.size() > 0) {
                                                        
                                                    //checking condition Procurement Method --> Framework Contract with PA office
                                                    //is now disable under mohsina apu --by Nitish
                                                        */
                                                %>
                                            <%--    <tr>
                                                    <td class="ff">Action : </td>
                                                    <td><a href="#" onclick="chkappWithNoPEOfc()" >Publish</a></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            --%>
                                                <%/*
                                                } else*/ if ((appWorkFlowStatus.equalsIgnoreCase("Approved") || appWorkFlowStatus.equalsIgnoreCase("Conditional Approval") || (initiator == approver && norevrs == 0)) && initiator != 0 && approver != 0 && !appStatus.equalsIgnoreCase("Approved") && isAddDatesConf == false && isInitiater == true && ispkgexist == true) {
                                                
                                                //End Nitish -- 28/05/2017
                                                %>
                                                <tr>
                                                    <td class="ff">Action : </td>
                                                    <td><a href="APPWorkflowView.jsp?appid=<%=details.getFieldName1()%>&action=pub&isIniAppSame=<%=isiniappsame%>">Publish</a></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>

                                                <%
                                                } else if ((appWorkFlowStatus.equalsIgnoreCase("Approved") || appWorkFlowStatus.equalsIgnoreCase("Conditional Approval") || (initiator == approver && norevrs == 0)) && initiator != 0 && approver != 0 && !appStatus.equalsIgnoreCase("Approved") && isAddDatesConf == false && isInitiater == true && ispkgexist == false) {%>
                                                <tr>
                                                    <td class="ff">Action : </td>
                                                    <td><a href="#" onclick="chkispkgexistAtPub()" >Publish</a></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <% } else if ((appWorkFlowStatus.equalsIgnoreCase("Approved") || appWorkFlowStatus.equalsIgnoreCase("Conditional Approval") || (initiator == approver && norevrs == 0)) && initiator != 0 && approver != 0 && !appStatus.equalsIgnoreCase("Approved") && isAddDatesConf == true && isInitiater == true && ispkgexist == true) {%>
                                                <tr>
                                                    <td class="ff">Action : </td>
                                                    <td><a href="#" onclick="checkDates()" >Publish</a></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <% }
                                                } else {
                                                %>
                                                <tr>
                                                    <td class="ff">Action : </td>
                                                    <td style="color: red">No Access</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <%
                                                        }
                                                    }
                                                %>

                                            </table>
                                            <% if (isProcWork && false) { %> 
                                            <div class="tableHead_1 t_space" style="margin-top: 3px;">Estimated Cost : </div>
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td width="180" class="ff">Template : </td>
                                                    <td width="287"><a href="../resources/EngineeringEstimationTemplate.xls" class="action-button-download">Download</a></td>
                                                    <td width="180"></td>
                                                    <td width="287"></td>
                                                </tr>
                                            </table>
                                            <% }%>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space" style="margin-top: 3px;">
                                                <tr>
                                                    <td width="50%"><div class="tableHead_1" >Workflow :</div></td>
                                                    <td width="10"><img src="<%=contextPath%>/resources/images/x.gif" width="10" /></td>
                                                    <td width="50%"><div class="tableHead_1">Annual Procurement Plan :</div></td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td width="50%">
                                                        <%
                                                            if (evntexist) {

                                                                if ((checkperole.equals("Yes") || checkaurole.equals("Yes")) && (appWorkFlowStatus.equalsIgnoreCase("Pending") || appWorkFlowStatus.equalsIgnoreCase("Rejected"))) {
                                                                    if (appWorkFlowStatus.equalsIgnoreCase("Pending") && !("Yes".equalsIgnoreCase(isFileProcessed))) {
                                                                        if (!"Approved".equalsIgnoreCase(appStatus)) {
                                                        %>
                                                        <%if (userid.equalsIgnoreCase(appCreatedUser)) {%>
                                                        <!--<a  href='CreateWorkflow.jsp?activityid=1&eventid=1&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&appId=<%=objectid%>&isFileProcessed=<%=isFileProcessed%>'>Edit</a>&nbsp;|&nbsp;-->
                                                        <% } %>
                                                        <%}%>
                                                        <% } else if (appWorkFlowStatus.equalsIgnoreCase("Rejected") && isInitiater == true) {%>
                                                        
                                                        <a href="FileProcessing.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&fromaction=dashboard" >Process file in Workflow</a>&nbsp;|&nbsp;
                                                        
                                                        <%}%>
                                                        <a  href="CreateWorkflow.jsp?activityid=1&eventid=1&objectid=<%=objectid%>&action=View&childid=<%=childid%>&appId=<%=objectid%>" >View</a>
                                                        <%
                                                        } else if ((checkperole.equals("Yes") || checkaurole.equals("Yes")) && appWorkFlowStatus.equalsIgnoreCase("Pending") && isFileProcessed.equalsIgnoreCase("Yes")) {
                                                        %>
                                                        <%if (userid.equalsIgnoreCase(appCreatedUser)) {%>
                                                        <!--<a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&isFileProcessed=<%=isFileProcessed%>'>Edit</a>&nbsp;|&nbsp;-->
                                                        <%}%>
                                                        <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>">View</a><!--&nbsp;|&nbsp;-->
                                                        <%
                                                        } else if (iswfLevelExist == false &&(checkperole.equals("Yes") || checkaurole.equals("Yes"))) {
                                                        %>
                                                        <%if (userid.equalsIgnoreCase(appCreatedUser)) {%>
                                                        <!--<a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&isFileProcessed=<%=isFileProcessed%>'>Edit</a>&nbsp;|&nbsp;-->
                                                        <%}%>
                                                        <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>">View</a><!--&nbsp;|&nbsp;-->

                                                        <%          } else if (iswfLevelExist == true && (checkperole.equals("Yes") || checkaurole.equals("Yes")) && !(isFileProcessed.equalsIgnoreCase("Yes"))) {
                                                        %>
                                                        <%if (userid.equalsIgnoreCase(appCreatedUser)) {%>
                                                        <!--<a  href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>&isFileProcessed=<%=isFileProcessed%>'>Edit</a>&nbsp;|&nbsp;-->
                                                        <%}%>
                                                        <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>">View</a><!--&nbsp;|&nbsp;-->
                                                        <%
                                                        } else if (iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {
                                                        %>
                                                        <label>Workflow yet not configured</label>
                                                        <%            } else if (iswfLevelExist == true) {
                                                        %>
                                                        <a  href="CreateWorkflow.jsp?activityid=1&eventid=1&objectid=<%=objectid%>&action=View&childid=<%=childid%>&appId=<%=objectid%>" >View</a> <!--&nbsp;|&nbsp;-->
                                                        <%
                                                            }
                                                        } else if ((checkperole.equals("Yes") || checkaurole.equals("Yes")) && (appStatus.equalsIgnoreCase("Pending") || appStatus == "")) {
                                                            List<CommonAppData> defaultconf = workFlowSrBean.editWorkFlowData("WorkFlowRuleEngine", Integer.toString(eventid), "");
                                                            if (defaultconf.size() > 0) {
                                                        %>
                                                        <%if (userid.equalsIgnoreCase(appCreatedUser)) {%>
                                                        <%if(isHopaExist){%>
                                                        <a  href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>&isFileProcessed=<%=isFileProcessed%>&submit=Submit">Create</a>
                                                        <% } else { %>
                                                        <a href='javascript:void(0);' onClick='alertforHopa();'>Create</a>
                                                        <% } %>
                                                        <% } else {
                                                        %>
                                                        <span style="color: red">Not Yet Created</span>
                                                        <%
                                                            }
                                                        %>
                                                        <%
                                                        } else {
                                                        %>
                                                        <a  href="#" onclick="checkdefaultconf()">Create</a>
                                                        <%            }
                                                        } else if (iswfLevelExist == false && !checkperole.equalsIgnoreCase("Yes")) {
                                                        %>
                                                        <label>Workflow yet not configured</label>
                                                        <%            }
                                                            if (isInitiater == true && fileOnHand.equalsIgnoreCase("Yes") && (initiator != approver || (initiator == approver && norevrs > 0))) {

                                                                if (appStatus.equalsIgnoreCase("pending") && appWorkFlowStatus.equalsIgnoreCase("pending") && isAddDatesConf == false && ispkgexist == true && (initiator != approver || (initiator == approver && norevrs > 0))) {
                                                        %>
                                                        
                                                        &nbsp;|&nbsp;<a href="FileProcessing.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&fromaction=dashboard" >Process file in Workflow</a>
                                                        
                                                        <%
                                                        } else if (appWorkFlowStatus.equalsIgnoreCase("pending") && ispkgexist == false && (initiator != approver || (initiator == approver && norevrs > 0))) {
                                                        %>
                                                        
                                                        &nbsp;|&nbsp;<a href="#" onclick="chkispkgexist()" >Process file in Workflow</a>
                                                        
                                                        <%            } else if (appWorkFlowStatus.equalsIgnoreCase("pending") && isAddDatesConf == true && (initiator != approver || (initiator == approver && norevrs > 0))) {
                                                        %>
                                                        
                                                        &nbsp;|&nbsp;<a href="#" onclick="checkDates()" >Process file in Workflow</a>
                                                        
                                                        <%                }
                                                            }

                                                            //List<SPTenderCommonData> listWFCount = tenderCommonService.returndata("GetWorkflowFileHistoryCount", ""+appId, "1");
                                                            List<Object[]> listworkflowpack = new ArrayList<Object[]>();
                                                            listworkflowpack = appSrBean.getWorkflowHistorylink(appId);
                                                            int cnt = 0;
                                                            String[] packforhistorylink = new String[listworkflowpack.size()];

                                                            if (!listworkflowpack.isEmpty()) {

                                                                for (cnt = 0; cnt < listworkflowpack.size(); cnt++) {
                                                                    packforhistorylink[cnt] = listworkflowpack.get(cnt)[1].toString();
                                                                }
                                                            }

                                                            if (iswfLevelExist && !listworkflowpack.isEmpty()) {
                                                        %>
                                                        &nbsp;|&nbsp;<a href="workFlowHistory.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&userid=<%=uid%>&fraction=app"  >View Workflow History</a>
                                                        <%
                                                            }
                                                        %>
                                                    </td>

                                                    <td width="50%">
                                                        <%if ("Approved".equalsIgnoreCase(appStatus) && userTypeId == 3) {%>
                                                        <a href="SearchPackageReport.jsp?appId=<%=appId%>" target="_blank">Consolidated Annual Procurement Plan</a>
                                                        <%}%>

                                                    </td>
                                                </tr>

                                                <%
                                                    if (msg != null && msg.contains("File")) {
                                                %>
                                                <tr>
                                                    <td colspan="2"><div class="responseMsg successMsg">File processed successfully</div></td>
                                                </tr>
                                                <%        } else if (msg != null && msg.contains("not")) {
                                                %>
                                                <tr>
                                                    <td colspan="2"><div class="responseMsg errorMsg">File has not processed successfully</div></td>
                                                </tr>
                                                <%        }
                                                %>

                                            </table>
                                            <div class="tableHead_1 t_space" style="margin-top: 3px;">Search Package :</div>
                                            <table border="0" cellspacing="10" cellpadding="0" class="formBg_1 formStyle_1 t_space" width="100%">
                                                <tr>
                                                    <td width="180" class="ff">Package No. : </td>
                                                    <td width="287"><input name="packageno" type="text" class="formTxtBox_1" id="txtPackageNo" style="width:200px;" maxlength="50" onblur="clearTo();" value="<%=pckNo.trim()%>" /></td>
                                                    <td width="180" class="ff">Procurement Category : </td>
                                                    <td width="287"><select name="procurenature" class="formTxtBox_1" id="cmbProcureNature" style="width:205px;" onchange="clearTo();">
                                                            <option selected="selected" value="">- Select Procurement Category -</option>
                                                            <option value="Goods" <% if ("Goods".equalsIgnoreCase(procurementNature)) { %>selected="selected"<% } %>>Goods</option>
                                                            <option value="Services" <% if ("Services".equalsIgnoreCase(procurementNature)) { %>selected="selected"<% } %>>Services</option>
                                                            <option value="Works" <% if ("Works".equalsIgnoreCase(procurementNature)) { %>selected="selected"<% } %>>Works</option>
                                                        </select></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Estimated Cost (In Nu.) : </td>
                                                    <td><input name="estimatecost" type="text" class="formTxtBox_1" id="txtEstimateCost" style="width:200px;" maxlength="50" onblur="clearTo();" value="<% if (EstimatedCost != 0) {
                                                            out.print(EstimatedCost);
                                                        } %>"/></td>
                                                    <td class="ff"><p> Procurement Type : </p>      </td>
                                                    <td>
                                                        <!--Change NCT to NCB and ICT to ICB -->
                                                        <select name="procuretype" class="formTxtBox_1" id="cmbProcureType" style="width:205px;" onchange="clearTo();">
                                                            <option selected="selected" value="">- Select Procurement Type -</option>
                                                            <option value="NCT" <% if ("NCT".equalsIgnoreCase(procurementType)) { %>selected="selected"<% } %>>NCB</option>
                                                            <option value="ICT" <% if ("ICT".equalsIgnoreCase(procurementType)) { %>selected="selected"<% } %>>ICB</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Status : </td>
                                                    <td>
                                                        <select name="status" class="formTxtBox_1" id="cmbStatus" style="width:205px;" onchange="clearTo();">
                                                            <option selected="selected" value="">- Select Status -</option>
                                                            <option value="Pending" <% if ("Pending".equalsIgnoreCase(status)) { %>selected="selected"<% } %>>Pending</option>
                                                            <option value="Approved" <% if ("Approved".equalsIgnoreCase(status)) { %>selected="selected"<% }%>>Approved</option>
                                                        </select>
                                                    </td>
                                                    <tr><td></td>
                                                        <td align="right"><span class="formBtn_1"><input type="submit" name="button" id="button" onclick="return loadSearchPackageTable();" value="Search" /></span></td>
                                                        <td align="left"><span class="formBtn_1"><input type="button" name="reset" value="Reset" id="reset" onclick="resetForm()" /></span></td>

                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="4"><span style="color: red" id="Error"></span></td>
                                                    </tr>
                                                </tr>
                                            </table>
                                        </form>
                                        <!--                                        <div class="tableHead_1 t_space">Package Details: </div>-->

                                        <div class="t-align-left t_space b_space">
                                            <!--<a href="<%=request.getContextPath()%>/help/tendernoticedocsprep/TenderNotice_Doc_PreparationSteps_25April2011.pdf" class="action-button-download" target="_blank">Steps for Tender/Proposal Preparation</a>&nbsp;-->
                                            <%if (userid.equalsIgnoreCase(appCreatedUser) && !IsAPPUsedInDeposit ) {%>
                                            <a href="AddPackageDetail.jsp?appId=<%=appId%>" class="action-button-add">Add New Package</a></div>
                                            <% } %>
                                        <div class="tableHead_1" style="text-align: left">Package Details : </div>
                                        <form id="frmappDashboadtbl" <%--action="<%=request.getContextPath()%>/APPFileUploadServlet"--%> name="frmappDashboadtbl" method="post">
                                            <table width="100%" cellspacing="0" cellpadding="0" id="resultTable" class="tableList_1" >
                                                <tr>
                                                    <th width="4%">Sl. <br/> No.</th>
                                                    <th width="33%">Package No., <br />
                                                        Package Description</th>
                                                    <th width="10%">Procurement Category, <br />
                                                        Procurement Type</th>
                                                    <th width="10%">Package Est. Cost <br />
                                                        (In Nu.)</th>
                                                    <!--th width="10%">Estimated Cost</th-->
                                                    <th width="6%">Tender ID</th>
                                                    <th width="8%">Tender Status</th>
                                                    <th width="8%">Status</th>
                                                    <th width="11%">Action</th>
                                                </tr>
                                                <%
                                                    int level = 0;

                                                    String dats = sb.toString();
                                                    String[] datsarr = dats.split(",");

                                                    List<Object[]> listpackageId = tenderCommonService.getPackagesPendingorApprove(appId);
                                                    if (commonData.size() == 0) {
                                                %>
                                                <tr>
                                                    <td colspan="9" style="color: red; text-align: center;">No Records found</td>
                                                </tr>
                                                <%            }
                                                    String styleClass = "";
                                                    boolean flag = false;
                                                    String tenderId = "";
                                                    String tenderStatus = "";
                                                    for (int i = 0; i < commonData.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                                        flag = false;
                                                        tenderId = "-";
                                                        strProc.delete(0, strProc.length());
                                                        CommonAppSearchData commonAppSearchData = commonData.get(i);
                                                        if (appWorkFlowStatus.equalsIgnoreCase("conditional approval") || appWorkFlowStatus.equalsIgnoreCase("approved")) {
                                                            short packactid = 1;
                                                            List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, commonAppSearchData.getPackageId(), packactid);
                                                            // package level workflow level config checking

                                                            if (tblWorkFlowLevelConfig.size() > 0) {
                                                                Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                                                iswfLevelExist = true;
                                                                while (twflc.hasNext()) {
                                                                    TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                                                    TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                                                    if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                                                                        fileOnHand = workFlowlel.getFileOnHand();
                                                                    }
                                                                    if (workFlowlel.getWfRoleId() == 1) {
                                                                        initiator = lmaster.getUserId();
                                                                    }
                                                                    if (workFlowlel.getWfRoleId() == 2) {
                                                                        approver = lmaster.getUserId();
                                                                    }
                                                                    if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                                                                        isInitiater = true;
                                                                    }

                                                                }

                                                            }
                                                        }
                                                %>
                                                <tr class="<%=styleClass%>">
                                                    <td class="t-align-center"><%=srno%></td>
                                                    <td align="left"><%=commonAppSearchData.getPackageNo()%><br/><%=commonAppSearchData.getPackageDesc()%></td>
                                                        <%
                                                            strProc.append(commonAppSearchData.getProcurementNature());
                                                            if (strProc.toString().equals("null")) {
                                                                strProc.delete(0, strProc.length());
                                                            }
                                                        %>
                                                    <!--Code By Proshanto Kumar Saha-->
                                                    <td style="text-align: center;"><%=strProc%><br/><% if ("NCT".equalsIgnoreCase(commonAppSearchData.getProcurementType())) { %>NCB<% } else if ("ICT".equalsIgnoreCase(commonAppSearchData.getProcurementType())) { %>ICB<% }%></td>
                                                    <!--Code End By Proshanto Kumar Saha-->
                                                    <td style="text-align: center;"><%=commonAppSearchData.getPkgEstCost().setScale(2, 0)%></td>
                                                    <%
                                                        String linkUpload = "";
                                                        String packageStatus = "";
                                                        String workflowStatus = "";
                                                        List<SPTenderCommonData> packstatus = tenderCommonService.returndata("PackageWorkFlowStatus", Integer.toString(commonAppSearchData.getPackageId()), "");

                                                        if (packstatus.size() > 0) {
                                                            SPTenderCommonData spTenderCommondata = packstatus.get(0);
                                                            packageStatus = spTenderCommondata.getFieldName1();
                                                            workflowStatus = spTenderCommondata.getFieldName2();
                                                        }


                                                    %>
                                                    <!--td style="text-align: center;"--><%--<a href="#">Upload</a>--%>
                                                       <!--%                                                            if (commonAppSearchData.getProcurementNature().equals("Works")) {
//
//                                                                if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && !packageStatus.equalsIgnoreCase("Approved") && (workflowStatus.equalsIgnoreCase("Pending") || workflowStatus.equalsIgnoreCase("Conditional Approval")) && (appWorkFlowStatus.equalsIgnoreCase("Conditional Approval") || appWorkFlowStatus.equalsIgnoreCase("Pending") || appWorkFlowStatus.equalsIgnoreCase("Approved"))) || iswfLevelExist == false) {
//                                                                    linkUpload = "<a href='" + request.getContextPath() + "/officer/APPDashboardDocs.jsp?appId=" + commonAppSearchData.getAppId() + "&pckId=" + commonAppSearchData.getPackageId() + "&pckno=" + commonAppSearchData.getPackageNo() + "&desc=" + commonAppSearchData.getPackageDesc() + " '>Click here, to upload Estimated Cost</a>";
//                                                                    out.print(linkUpload);
//                                                                } else if ((fileOnHand.equalsIgnoreCase("NO") && workflowStatus.equalsIgnoreCase("Pending")) || (iswfLevelExist == false && (appWorkFlowStatus.equalsIgnoreCase("Pending") || appStatus.equalsIgnoreCase("Pending")))
//                                                                        || (fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && workflowStatus.equalsIgnoreCase("Approved") && !packageStatus.equalsIgnoreCase("Approved"))) {
//                                                        %>
//                                                        <!--a href="#" title="View docs" onclick="window.open('ViewPackageEngineerEstimationDocs.jsp?appId=' +<!--%=commonAppSearchData.getAppId()%> + '&pckId=' +<!--%=commonAppSearchData.getPackageId()%> + '', 'View Uploaded Files', 'width=700,height=500')">View </a-->
                                                        <!--%  } else {
//                                                                out.print("-");
//                                                            }
//                                                        } else {%>-<!--%}%-->
                                                   <!--/td-->
                                                    <%
                                                        String link = "";
                                                        //java.math.BigDecimal id = null;

                                                        if (workflowStatus.equalsIgnoreCase("pending")) {
                                                            level++;
                                                        }

                                                        if (!IsAPPUsedInDeposit && isInitiater == true && fileOnHand.equalsIgnoreCase("Yes") && (initiator != approver || (initiator == approver && norevrs > 0))) {
                                                            if (datsarr[i].equals("true") && workflowStatus.equalsIgnoreCase("Pending") && (initiator != approver || (initiator == approver && norevrs > 0)) && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (appWorkFlowStatus.equalsIgnoreCase("Approved") || appWorkFlowStatus.equalsIgnoreCase("conditional approval"))) {
                                                                if (level == 1) {
                                                                    link += "<a href='FileProcessing.jsp?activityid=1&objectid=" + objectid + "&childid=" + commonAppSearchData.getPackageId() + "&eventid=" + eventid + "&fromaction=pkgdashboard' >Process file in Workflow</a>&nbsp;|&nbsp;";
                                                                } else {
                                                                    link += "<a href='#' onclick='pkgcheck()' >Process file in Workflow</a>&nbsp;|&nbsp;";
                                                                }
                                                            } else if (datsarr[i].equals("false") && workflowStatus.equalsIgnoreCase("Pending") && fileOnHand.equalsIgnoreCase("Yes") && (initiator != approver || (initiator == approver && norevrs > 0)) && initiator != approver && (appWorkFlowStatus.equalsIgnoreCase("Approved") || appWorkFlowStatus.equalsIgnoreCase("conditional approval"))) {
                                                                link += " <a href='#' onclick='checkDates()' >Process file in Workflow</a>&nbsp;|&nbsp;";
                                                            }
                                                        }
                                                       boolean reviseAfterPublish = appServlet.CheckAPPRevise(financialYear,commonAppSearchData.getAppId());
                                                        if (status.equals("Approved")) {
                                                            link += "<a   href='" + request.getContextPath() + "/resources/common/ViewPackageDetail.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=Package'>View</a>&nbsp;|&nbsp;";
                                                        } else {
                                                            if ((((workflowStatus.equalsIgnoreCase("Approved") || workflowStatus.equalsIgnoreCase("Conditional Approval")) && (packageStatus.equalsIgnoreCase("BeingRevised") || packageStatus.equalsIgnoreCase("Pending")) && appStatus.equalsIgnoreCase("Approved") && !packageStatus.equalsIgnoreCase("Approved")) || (appStatus.equalsIgnoreCase("Approved") && (initiator == approver && norevrs == 0) && !packageStatus.equalsIgnoreCase("Approved"))) && !IsAPPUsedInDeposit) {
                                                                if(userid.equalsIgnoreCase(appCreatedUser)){
                                                                if (!datsarr[i].equals("false")) {
                                                                    if (initiator == approver && norevrs == 0) {                                                                      
                                                                        link += "<a href='APPWorkflowView.jsp?appid=" + objectid + "&action=inisame&pkgno=" + commonAppSearchData.getPackageNo() +"&pkgstatus=" + packageStatus +"'>Publish</a>&nbsp;|&nbsp;";
                                                                    } else {                                                                       
                                                                        link += "<a href='APPWorkflowView.jsp?appid=" + objectid + "&pkgstatus=" + packageStatus + "&action=pub'>Publish</a>&nbsp;|&nbsp;";
                                                                    }
                                                                }
                                                                }

                                                            }

                                                            link += "<a   href='" + request.getContextPath() + "/resources/common/ViewPackageDetail.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=Package'>View</a>&nbsp;|&nbsp;";

                                                            if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && !packageStatus.equalsIgnoreCase("Approved") && (workflowStatus.equalsIgnoreCase("Pending") || workflowStatus.equalsIgnoreCase("Conditional Approval") || workflowStatus.equalsIgnoreCase("Rejected")) && (appWorkFlowStatus.equalsIgnoreCase("Conditional Approval") || appWorkFlowStatus.equalsIgnoreCase("Pending") || appWorkFlowStatus.equalsIgnoreCase("Approved") || appWorkFlowStatus.equalsIgnoreCase("Rejected"))) || iswfLevelExist == false) && !IsAPPUsedInDeposit ) {
                                                                //if the package is being revised
                                                                if (userid.equalsIgnoreCase(appCreatedUser)) {
                                                                    if (packageStatus.equalsIgnoreCase("BeingRevised")) {
                                                                        if(reviseAfterPublish){
                                                                        link += "<a href='" + request.getContextPath() + "/officer/AddPackageDetail.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&action=revise'>Revise Package Detail</a>&nbsp;|&nbsp;";
                                                                        }
                                                                    } else {
                                                                        link += "<a href='" + request.getContextPath() + "/officer/AddPackageDetail.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&action=Edit'>Edit Package Detail</a>&nbsp;|&nbsp;";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (datsarr[i].equals("false") && !IsAPPUsedInDeposit) {
                                                            if (userid.equalsIgnoreCase(appCreatedUser)) {
                                                                if (packageStatus.equalsIgnoreCase("BeingRevised")) {
                                                                    link += "<a href='" + request.getContextPath() + "/officer/AddPackageDates.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&isRevision=true'>Add Dates</a>&nbsp;|&nbsp;";
                                                                } else {
                                                                    link += "<a href='" + request.getContextPath() + "/officer/AddPackageDates.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "'>Add Dates</a>&nbsp;|&nbsp;";
                                                                }
                                                            }
                                                        } else {
                                                            if (((datsarr[i].equals("true") && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && !packageStatus.equalsIgnoreCase("Approved") && (workflowStatus.equalsIgnoreCase("Pending") || workflowStatus.equalsIgnoreCase("Conditional Approval") || workflowStatus.equalsIgnoreCase("Rejected")) && (appWorkFlowStatus.equalsIgnoreCase("Conditional Approval") || appWorkFlowStatus.equalsIgnoreCase("Pending") || appWorkFlowStatus.equalsIgnoreCase("Approved") || appWorkFlowStatus.equalsIgnoreCase("Rejected"))) || iswfLevelExist == false) && !IsAPPUsedInDeposit ) {
                                                                if (userid.equalsIgnoreCase(appCreatedUser)) {
                                                                    //if the package dates are being revised
                                                                    if (packageStatus.equalsIgnoreCase("BeingRevised")) {
                                                                        if(reviseAfterPublish){
                                                                        link += "<a href='" + request.getContextPath() + "/officer/EditPackageDates.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&action=revise'>Revise Dates</a>&nbsp;|&nbsp;";
                                                                        }
                                                                    } else {
                                                                        link += "<a href='" + request.getContextPath() + "/officer/EditPackageDates.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "'>Edit Dates</a>&nbsp;|&nbsp;";
                                                                    }
                                                                }
                                                            }
                                                            //List<CommonSPReturn> details = tenderSrBean.insertSPAddYpTEndernotice(appId, commonAppSearchData.getPackageId(), userid);
                                                            //for (CommonSPReturn d : details) {
                                                            //    id = d.getId();
                                                            //}
                                                            if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && !packageStatus.equalsIgnoreCase("Approved") && (workflowStatus.equalsIgnoreCase("Pending") || workflowStatus.equalsIgnoreCase("Conditional Approval") || workflowStatus.equalsIgnoreCase("Rejected")) && (appWorkFlowStatus.equalsIgnoreCase("Conditional Approval") || appWorkFlowStatus.equalsIgnoreCase("Pending") || appWorkFlowStatus.equalsIgnoreCase("Approved") || appWorkFlowStatus.equalsIgnoreCase("Rejected"))) || iswfLevelExist == false) && !IsAPPUsedInDeposit ) {
                                                                if (userid.equalsIgnoreCase(appCreatedUser)) {
                                                                    link += "<a href='#' onClick='delPkg(" + commonAppSearchData.getPackageId() + ");'>Remove</a>&nbsp;|&nbsp;";
                                                                }
                                                            }

                                                            //link += "<a href='CreateTender.jsp?id=" + id + "'>Create Tender</a>&nbsp;|&nbsp;";
                                                            if(!IsAPPUsedInDeposit)
                                                            {
                                                                for (cnt = 0; cnt < packforhistorylink.length; cnt++) {
                                                                    if (packforhistorylink[cnt].equals(commonAppSearchData.getPackageId().toString())) {
                                                                        link += "<a href='workFlowHistory.jsp?activityid=" + activityid + "&objectid=" + objectid + "&childid=" + commonAppSearchData.getPackageId() + "&eventid=" + eventid + "&userid=" + uid + "&fraction=app'>View Workflow History</a>&nbsp;|&nbsp;";
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        for (Object[] obj : listpackageId) {
                                                            if (commonAppSearchData.getPackageId() == Integer.parseInt(obj[0].toString())) {
                                                                tenderId = obj[1].toString();
                                                                tenderStatus = obj[2].toString();
                                                                if (!"Cancelled".equalsIgnoreCase(tenderStatus)) {
                                                                    flag = true;
                                                                }
                                                            }
                                                        }                                   
                                                        boolean busflag = appSrBean.isBusRuleConfig(commonAppSearchData.getPackageId());
                                                        boolean procmethodRuleflag = appSrBean.isprocMethodRuleConfig(commonAppSearchData.getPackageId());
                                                        
                                                       //boolean reviseAfterPublish= true;
                                                       
                                                        if (packageStatus.equalsIgnoreCase("Approved") && !flag && !IsAPPUsedInDeposit) {
                                                            if (/*userid.equalsIgnoreCase(appCreatedUser)*/ checkperole.equals("Yes") || checkaurole.equals("Yes")) {
                                                            if (!busflag) {
                                                                link += "<a href='javascript:void(0);' onclick='showbusconfig();' >Create Tender</a>&nbsp;|&nbsp;";
                                                            } else if (!procmethodRuleflag) {
                                                                link += "<a href='javascript:void(0);' onclick='showprocmethodconfig();' >Create Tender</a>&nbsp;|&nbsp;";                  
                                                            } else {
                                                                //link += "<a href='APPDashboardLink.jsp?appId=" + appId + "&pckId=" + commonAppSearchData.getPackageId() + "'>Create Tender/Proposal</a>&nbsp;|&nbsp;";
                                                                link += "<a href='javascript:void(0);' onClick=' confirmCreateTender(" + appId + "," + commonAppSearchData.getPackageId() + ");' >Create Tender</a>&nbsp;|&nbsp;";
                                                                if(reviseAfterPublish){  
                                                                    if(userid.equalsIgnoreCase(appCreatedUser)){
                                                                        link += "<a href='javascript:void(0);' onClick=' revisePackage(" + commonAppSearchData.getAppId() + "," + commonAppSearchData.getPackageId() + ");' >Revise</a> | ";
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        link += "<a href='../resources/common/PackageReport.jsp?appId=" + commonAppSearchData.getAppId() + "&pkgId=" + commonAppSearchData.getPackageId() + "&pkgType=package'>Report</a>";
                                                    %>
                                                    <td class="t-align-center"><%=tenderId%></td>
                                                    <%
                                                        boolean isawarded = false;
                                                        if (!"-".equalsIgnoreCase(tenderId)) {
                                                            isawarded = tenderCommonService.istenderAwarded(Integer.parseInt(tenderId));
                                                        }
                                                    %>
                                                    <td class="t-align-center"><% if ("Cancelled".equalsIgnoreCase(tenderStatus)) { %>Tender cancelled<% } else if ("-".equalsIgnoreCase(tenderId)) { %>Tender not floated<% } else %>Tender floated<% if (isawarded) { %><br />Contract awarded<% } else { %><br />Contract not awarded<% } %></td>
                                                    <td style="text-align: center;"><% if ("BeingRevised".equalsIgnoreCase(packageStatus)) { %><label style='color: red;font-weight: bold;\'>BeingRevised</label><%
                                                        if ((checkperole.equals("Yes") || checkaurole.equals("Yes")) && "Yes".equalsIgnoreCase(fileOnHand)) {
                                                            boolean canRevshow = false;
                                                            if (userid.equalsIgnoreCase(appCreatedUser)) {
                                                                if (canRevshow) { //hide by aprojit
                                                    %>  | <br/><a href="<%=request.getContextPath()%>/CommonServlet?appId=<%=appId%>&packageId=<%= commonAppSearchData.getPackageId()%>&funName=cancelPkgRevision" onclick="return cancelRevision();">cancel revision</a>
                                                    <%}
                                                                                                                                                              }
                                                                                                                                                          }
                                                                } else {%><%=packageStatus%><% }%></td>
                                                                                                                                                      <td class="t-align-center" ><%=link%></td>
                                                                                                                                                      </tr>
                                                                                                                                                      <%
                                                                                                                                                              srno++;
                                                                                                                                                          }
                                                                                                                                                      %>
                                                                                                                                                      </table>
                                                                                                                                                      <input type="text" id="txtFocus" name="txtFocus" style="display: none; height: 0px; width: 0px; border: 0px;"></input>
                                                                                                                                                      </form>
                                                                                                                                                      <form name="frmresetForm"  id="frmresetForm" method="post" action="<%=request.getContextPath()%>/officer/APPDashboard.jsp?appID=<%=appId%>"></form>
                                                                                                                                                      <div> &nbsp; </div>

                                                                                                                                                      </td>
                                                                                                                                                      </tr>
                                                                                                                                                      </table>
                                                                                                                                                      <!--Page Content End-->
                                                                                                                                                      </td>
                                                                                                                                                      </tr>
                                                                                                                                                      </table>
                                                                                                                                                      <!--Middle Content Table End-->
                                                                                                                                                      <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                                                                                                                                                          </div>
                                                                                                                                                          </div>
                                                                                                                                                          </body>
                                                                                                                                                          <script>
                                                                                                                                                          function setFocus(){
                                                                                                                                                              document.getElementById("txtFocus").style.display="block";
                                                                                                                                                              document.getElementById("txtFocus").focus();
                                                                                                                                                              document.getElementById("txtFocus").style.display="none";
                                                                                                                                                          }

                                                                                                                                                          function resetForm(){
                                                                                                                                                              document.getElementById("frmresetForm").submit();
                                                                                                                                                          }
                                                                                                                                                          </script>
                                                                                                                                                      <%
                                                                                                                                                          appServlet = null;
                                                                                                                                                          tenderSrBean = null;
                                                                                                                                                      %>

                                                                                                                                                      <script type="text/javascript" language="Javascript">
                                                                                                                                                      var headSel_Obj = document.getElementById("headTabApp");
                                                                                                                                                      if(headSel_Obj != null){
                                                                                                                                                          headSel_Obj.setAttribute("class", "selected");
                                                                                                                                                      }

                                                                                                                                                      </script>
                                                                                                                                                      </html>
