<%-- 
    Document   : ComReport
    Created on : Dec 1, 2010, 2:49:48 PM
    Author     : Kinjal Shah
    Purpose    : Display Comparative Report
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPReportCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    boolean bol_isPdf = true;
                    if (request.getParameter("isPDF")!=null && "true".equalsIgnoreCase(request.getParameter("isPDF"))){
                        bol_isPdf = false;
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Comparative Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
       
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <!--<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        <script type="text/javascript">
                    $(document).ready(function() {
                                $("#print").click(function() {
                                    printElem({ leaveOpen: true, printMode: 'popup' });
                                });

                            });
                            function printElem(options){
                                $('#print_area').printElement(options);
                            }
        </script>
    </head>
    <body>
        <%
                    String fId = "";
                    int tenderId = 0;
                    if (!request.getParameter("formId").equals("")) {
                        fId = request.getParameter("formId");
                    }
                    if (request.getParameter("tenderId") != null) 
                    {
                        tenderId = Integer.parseInt(request.getParameter("tenderId").toString());
                    }
                    
                     // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String idType="tenderId";
                    int auditId=tenderId;
                    String auditAction="Comparative report viewed by Opening Committee Member";
                    String moduleName=EgpModule.Tender_Opening.getName();
                    String remarks="User Id: "+session.getAttribute("userId")+" has viewed comparative report for Form id: "+fId+" of Tender Id: "+tenderId;
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
        %>
        <div class="mainDiv">
            <div class="fixDiv">
            <!--Dashboard Header Start-->

            <div class="topHeader">
                <%if(bol_isPdf){%>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <%}%>
            </div>
            <div class="contentArea_1">
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%if(bol_isPdf){%>
                    <span style="float:right;"><a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;&nbsp;<a class="action-button-savepdf" href="<%=request.getContextPath()%>/TorRptServlet?tenderId=<%=tenderId%>&formId=<%=fId%>&action=ComReport">Save As PDF</a>&nbsp;&nbsp;
                        <%

                                    int userTypeid = 0;
                                    
                                    if (session.getAttribute("userTypeId") != null) {
                                        userTypeid = Integer.parseInt(session.getAttribute("userTypeId").toString());
                                    }

                                    

                                    if (userTypeid == 2) {
                        %>
                        <a href="<%=request.getContextPath()%>/tenderer/TendererDashboard.jsp?tenderid=<%=tenderId%>" class="action-button-goback">Go Back To Dashboard</a>
                        <% } else {%>
<!--                        <a href="< %=request.getContextPath()%>/officer/OpenComm.jsp?tenderid=< %=tenderId%>" class="action-button-goback">Go Back To Dashboard</a>-->
                            <a href="<%=request.getHeader("referer")%>" class="action-button-goback">Go Back To Dashboard</a>
                        <% }%>
                    </span>
                    <%}%>
                    <div  id="print_area">
                    <div class="pageHead_1" style="margin-top: 15px;">View Comparative Report
                    
                </div>
                <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                            pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                            pageContext.setAttribute("userId", 0);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
                <%
                    List<Object[]> list_lorPkg = new ArrayList<Object[]>();
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    list_lorPkg = commonService.getPkgDetialByTenderId(tenderId);
                %>
               <table width="100%" cellspacing="0" class="tableList_1 t_space">
                   <tr>
                        <th width="25%" class="t-align-left ff">Package No</th>
                        <th width="75%" class="t-align-left ff">Package Description</th>
                    </tr>
                    <tr >
                        <td class="t-align-center"><%=list_lorPkg.get(0)[0]%></td>
                        <td class="t-align-left"><%=list_lorPkg.get(0)[1]%></td>
                    </tr>
               </table>
               <%
                  if(!isTenPackageWise){
                      list_lorPkg = commonService.getLotDetiasByFormId(Integer.parseInt(fId));
               %>
               <table width="100%" cellspacing="0" class="tableList_1 t_space">
                   <tr>
                        <th width="25%" class="t-align-left ff">Lot No</th>
                        <th width="75%" class="t-align-left ff">Lot Description</th>
                    </tr>
                    <tr>
                        <td class="t-align-center"><%=list_lorPkg.get(0)[0]%></td>
                        <td class="t-align-left"><%=list_lorPkg.get(0)[1]%></td>
                    </tr>
               </table>
                <%}%>
                <%

                            ReportCommonService reportCommonService = (ReportCommonService) AppContext.getSpringBean("ReportCommonService");
                            List<SPReportCommonData> reportList = reportCommonService.returnCompReport(Integer.parseInt(request.getParameter("formId")), 0);
                            if (!reportList.isEmpty()) {
                %>



                <%
                                                for (SPReportCommonData sprcd : reportList) {
                                                    String replacedReport = sprcd.getReportHtml().replaceAll("\n","<br/>");
                                                    replacedReport = replacedReport.replace("<td> 2.000</td>","<td> I Agree</td>");
                                                    out.println(replacedReport);
                                                }
                %>

                <%//}}
                            }
                            ;%>
            <span style="color: red;"><br/>System Generated Report based on the tender submitted by the Bidders / Consultants</span>
            </div>            
            </div>
            <%if(bol_isPdf){%>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <%}%>
            <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
