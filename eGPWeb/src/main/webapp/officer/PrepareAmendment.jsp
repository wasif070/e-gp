<%-- 
    Document   : PrepareAmendment
    Created on : Nov 16, 2010, 3:55:37 PM
    Author     : rajesh
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="java.util.List" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page buffer="10kb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Prepare Amendment / Corrigendum</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%> 
        <div class="pageHead_1">Prepare Amendment /Corrigendum</div>
        <%
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", request.getParameter("id"));
            %>
            <br/>
            <div class="t-align-right" style="vertical-align: middle;"><a class="action-button-goback" href="Amendment.jsp?tenderid=<%=request.getParameter("id")%>">Go back to Dashboard</a></div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%
                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                    //String id = "";
                    if (!request.getParameter("id").equals("")) {
                        id = request.getParameter("id");
                    }
                    String path = "";
                   
                    //This condition is used to submit data in database, this is button click event.
                    if (request.getParameter("btnPerpareAmendment") != null ) {
                        String action = "";
                        try{
                            if (!request.getParameter("id").equals("") && !request.getParameter("op").equals("edit")) {
                                action = "Corrigendum/Amendment Preparation";
                                String dtXml = "";
                                dtXml = "<root><tbl_CorrigendumMaster tenderId=\"" + request.getParameter("id") + "\" corrigendumText=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("textarea3").toString()) + "\" "
                                        + "corrigendumStatus=\"Pending\" workFlowStatus=\"Pending\"/></root>";


                                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_CorrigendumMaster", dtXml, "").get(0);
                                out.println(commonMsgChk.getMsg());
                                
                                path = "CorriTender.jsp?id="+request.getParameter("id")+"&corrid="+commonMsgChk.getId();
                                //response.sendRedirect("CorriTender.jsp?id="+request.getParameter("id")+"&corrid="+commonMsgChk.getId());
                            }
                            else if (request.getParameter("cid")!=null && !request.getParameter("cid").equals("") && request.getParameter("op").equals("edit")) {
                                action = "Edit Corrigendum/Amendment Preparation";
                                String dtXml = "";
                                dtXml = "<root><tbl_CorrigendumMaster corrigendumId=\"" + request.getParameter("cid") + "\" tenderId=\"" + request.getParameter("id") + "\" corrigendumText=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("textarea3").toString()) + "\" "
                                        + "corrigendumStatus=\"Pending\" workFlowStatus=\"Pending\"/></root>";

                                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("update", "tbl_CorrigendumMaster", dtXml, "").get(0);
                                out.println(commonMsgChk.getMsg());
                                //response.sendRedirect("CorriTender.jsp?id="+request.getParameter("id")+"&isedit=y&corrid="+request.getParameter("cid"));
                                path = "CorriTender.jsp?id="+request.getParameter("id")+"&isedit=y&corrid="+request.getParameter("cid");

                            }
                       }catch(Exception e){
                           action = "Error in "+ action + " "+e.getMessage();
                       }finally{
                           MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                           makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), Integer.valueOf(id), "TenderId", EgpModule.Corrigendum_Amendment.getName(), action, request.getParameter("textarea3"));
                           action = null;
                       }

                      response.sendRedirect(path);
                    }
       
        %>        
        <form id="frmPrepareAmendment" name="frmPrepareAmendment" method="POST" action="PrepareAmendment.jsp?id=<%=request.getParameter("id")%>&cid=<%=request.getParameter("cid")%>&op=<%=request.getParameter("op")%>">            
                <div class="tabPanelArea_1">                    
                    <div class="t-align-right">Field Marked with(<span class="mandatory">*</span>) is Mandatory</div>
                    <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">
                        <tr>
                            <td width="22%" class="ff">Amendment/ Corrigendum Text: <span>*</span></td>
                            <td width="78%">

                     <%
                                     //This condition is used to Get data with corrigendumid and show data in below page.
                    //if (!request.getParameter("cid").equals("") && request.getParameter("op").equals("edit")) {
                        //TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    // for( SPTenderCommonData sptcd : tenderCommonService.returndata("getCorrigenduminfo",i,null)){
                    List<SPTenderCommonData> list1 = tenderCommonService.returndata("getCorrigenduminfo", request.getParameter("id"), null);

                    %>
                    <%
                        if (list1.isEmpty()){%>
                        <textarea name="textarea3" rows="5" class="formTxtBox_1" id="textarea3" style="width:600px;" onblur="clear();">
                            </textarea>                                                    
                    <% }else {%>
                    <textarea name="textarea3" rows="5" class="formTxtBox_1" id="textarea3" style="width:600px;" onblur="clear();">
                    <%=list1.get(0).getFieldName3().toString()%>
                    </textarea>
                    
                    <% }%>
                    <script type="text/javascript">
                                  //<![CDATA[
                                  CKEDITOR.replace( 'textarea3',
                                  {
                                       toolbar : "egpToolbar"

                                  });
                                  //]]>
                            </script>
                    <%//}%>
                    <span id="s_txtamendment" style="color: red; ">&nbsp;</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="t-align-center">
                                <label class="formBtn_1">
                                    <input type="submit" name="btnPerpareAmendment" id="btnPerpareAmendment" value="Submit" onclick="return Validate();"/>
                                </label>
                            </td>
                        </tr>
                    </table>

                </div>                
        </form>
                <br/>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<script type="text/javascript">
        function Validate()
        {
            var validatebool="";

        if(CKEDITOR.instances.textarea3.getData() == 0){
                document.getElementById("s_txtamendment").innerHTML="Please enter Amendment / Corrigendum text";
                validatebool="false";
            }
            else
                {
                     if(CKEDITOR.instances.textarea3.getData().length <= 1000)
                    {
                        document.getElementById("s_txtamendment").innerHTML="";
                    }
                    else
                    {
                       document.getElementById("s_txtamendment").innerHTML="Maximum 1000 characters are allowed.";
                       validatebool="false";
                    }
                }
            if (validatebool=='false'){
                return false;
            }
            else{
                 document.getElementById("btnPerpareAmendment").style.display = 'none';
                }

        }


            $(document).ready(function() {
            CKEDITOR.instances.textarea3.on('blur', function()
            {
                var validatebool="";
                if(CKEDITOR.instances.textarea3.getData() != 0)
                {
                    if(CKEDITOR.instances.textarea3.getData().length <= 1000)
                    {
                        document.getElementById("s_txtamendment").innerHTML="";
                    }
                    else
                    {
                       document.getElementById("s_txtamendment").innerHTML="Maximum 1000 characters are allowed.";
                       validatebool="false";
                    }
                }else{
                    document.getElementById("s_txtamendment").innerHTML = "Message Text is not Available.";
                    validatebool="false";
                }
                if (validatebool=='false'){
                return false;
            }
            });
        });
</script>