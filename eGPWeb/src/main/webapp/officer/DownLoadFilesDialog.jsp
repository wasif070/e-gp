<%-- 
    Document   : DownLoadFilesDialog
    Created on : Nov 18, 2010, 8:42:49 PM
    Author     : test
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowDocuments"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
         <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Download files</title>
    </head>
    <body>
        <div class="pageHead_1">Download files </div>
        <div>&nbsp;</div>
       <div>
             <%
             String docIds = request.getParameter("docids");
             String activityid =  request.getParameter("activityid");
           String objectid = request.getParameter("objectid");
           String childid = request.getParameter("childid");
           
           if(docIds.length()>0)
               docIds = docIds.replace("dc", ",");
            WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
         List<TblWorkFlowDocuments> tblWorkFlowDocuments =
                   workFlowSrBean.getDocumetnsByIds(docIds);
         if(tblWorkFlowDocuments.size()>0)
         request.setAttribute("tblWorkFlowDocuments", tblWorkFlowDocuments);

             %>
     <table width="100%" cellspacing="0" class="tableList_1 t_space">
         <form method="post">
    <tr>
        <th>Sl. No</th>
        <th>FileName</th>
      <th>File Size(KB)</th>
      <th>Action</th>
    </tr>
    <%
    int c = 2;
    String colorchange = "style='background-color:#E4FAD0;' ";
    if(tblWorkFlowDocuments.size()>0){
    Iterator docs =  tblWorkFlowDocuments.iterator();
    int i= 1;
        while(docs.hasNext()){

        TblWorkFlowDocuments twfd = (TblWorkFlowDocuments)docs.next();
       %>
       <tr <% if(c == i) out.print(colorchange); %> >
    <%
        out.write("<td class='t-align-center'>"+i+"</td>");
        out.write("<td class='t-align-left'>"+twfd.getDocumentName()+"</td>");
        out.write("<td class='t-align-center'>"+twfd.getDocSize()+"</td>");
        out.write("<td class='t-align-center'><a  href=\""+request.getContextPath()+"/workflowdocservlet?docName="+twfd.getDocumentName()+"&docSize="+twfd.getDocSize()+"&action=download&docid="+twfd.getWfDocumentId()+"&objectid="+
                objectid+"&activityid="+activityid+"&childid="+childid+"&userid="+twfd.getUserId()+"\"><img alt='Download' src='../resources/images/Dashboard/Download.png'></a>");
        out.write("</tr>");
         if(c == i)
             c+=2;
        i++;

        }
    }
        %>


         </form>
     </table>
     </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabWorkFlow");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
