<%-- 
    Document   : APPWorkflowDocs
    Created on : Nov 12, 2010, 10:24:02 AM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.model.table.TblAppEngEstDoc" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Estimated Cost</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

top.window.moveTo(0,0);
if (document.all) {
top.window.resizeTo(screen.availWidth,screen.availHeight);
}
else if (document.layers||document.getElementById) {
if (top.window.outerHeight<screen.availHeight||top.window.outerWidth
||screen.availWidth){
top.window.outerHeight = screen.availHeight; top.window.outerWidth =
screen.availWidth; }
}

</script>
        
    </head>
    <jsp:useBean id="appSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.APPSrBean"/>
    <body>
        <%
                    int pckid = 0;
                    if (!request.getParameter("pckid").equals("")) {
                        pckid = Integer.parseInt(request.getParameter("pckid"));
                    }
                    String logUserId="0";
                    if(session.getAttribute("userId")!=null){
                        logUserId=session.getAttribute("userId").toString();
                    }
                    appSrBean.setLogUserId(logUserId);
                    
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Middle Content Table Start-->
                <div style=" display: none;">
                    <jsp:include page="../resources/common/AfterLoginTop.jsp"/>
                </div>
                <div class="contentArea_1">
                                <div class="pageHead_1">
                                    <p>Estimated Cost</p>
                                </div>
                                <div>&nbsp;</div>
                                <%
                                            if (request.getParameter("fq") != null) {
                                                if (request.getParameter("fq").equals("Removed") || request.getParameter("fq").equals("Uploaded")) {
                                %>
                                <div class="responseMsg successMsg" style="margin-top: 10px;">File <%=request.getParameter("fq")%> Sucessfully</div>
                                <%} else {%>
                                <div> &nbsp;</div>
                                <div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
                                <%
                                                }
                                            }

                                %>
                                <table  width="100%" cellspacing="0" class="tableList_1" >
                                    <tr>
                                    <th class="t-align-center">
                                        S No.
                                    </th>
                                    <th class="t-align-center">
                                        File Name
                                    </th>
                                    <th class="t-align-center">
                                        Description
                                    </th>
                                    <th class="t-align-center">
                                        Download
                                    </th>
                                    </tr>
                                    <% int srno = 1;
                                                for (TblAppEngEstDoc tblAppEngEstDoc : appSrBean.getDownloadDetails(pckid)) {
                                    %>
                                    <tr>
                                        <td class="t-align-center">
                                            <%=srno%>
                                        </td>
                                        <td>
                                            <%= tblAppEngEstDoc.getDocumentName()%>
                                        </td>
                                        <td>
                                            <%= tblAppEngEstDoc.getDocumentDesc()%>
                                        </td>
                                        <td class="t-align-center">
                                            <form method="post" id="frnAPPWorkFlowDocs" name="frnAPPWorkFlowDocs" action="">
                                                <a href="<%=request.getContextPath()%>/AppDownloadDocs?docName=<%=tblAppEngEstDoc.getDocumentName()%>&appId=<%=tblAppEngEstDoc.getTblAppMaster().getAppId()%>&docId=<%=tblAppEngEstDoc.getAppEngEstId()%>&docSize=<%= tblAppEngEstDoc.getDocSize()%>&funName=download" ><img src="../resources/images/Dashboard/Download.png" /></a>
                                                <%--<a href="<%=request.getContextPath()%>/AppDownloadDocs?docName=<%=tblAppEngEstDoc.getDocumentName()%>&docId=<%=tblAppEngEstDoc.getAppEngEstId()%>&appId=<%=tblAppEngEstDoc.getTblAppMaster().getAppId()%>&docSize=<%= tblAppEngEstDoc.getDocSize()%>" ><img src="../resources/images/Dashboard/downloadIcn.png" alt="download" /></a>--%>
                                            </form>
                                        </td>
                                    </tr>
                                    <%                                                                }
                                    %>

                                </table>
                    <center>
                        <table style="width: auto;" class="formStyle_1">
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td class="formBtn_1"  align="center">
                                    <input type="button" value="close" id="close" name="close" onclick="window.close()"/>
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>
            </div>
        </div>
    </body>
    <%
                appSrBean = null;
    %>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabApp");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
</html>
