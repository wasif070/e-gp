<%-- 
    Document   : ViewWCCertificate
    Created on : Aug 3, 2011, 12:21:36 PM
    Author     : Sreenu.Durga
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertDoc"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertDocServiceBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<jsp:useBean id="cmsWcCertificateServiceBean" class="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Work completion certificate</title>
    </head>
    <%
                String tenderId = "";
                String userId = "";
                String lotId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                int wcCertId = 0;
                if (request.getParameter("wcCertId") != null) {
                    wcCertId = Integer.parseInt(request.getParameter("wcCertId"));
                }
                if (session.getAttribute("userId") == null) {
                    response.sendRedirect("SessionTimedOut.jsp");
                } else {
                    userId = session.getAttribute("userId").toString();
                }
                if (request.getParameter("lotId") != null) {
                    pageContext.setAttribute("lotId", request.getParameter("lotId"));
                }
                if (request.getParameter("lotId") != null) {
                    lotId = request.getParameter("lotId");
                }
                
                
    %>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                              <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                            <br/>
                            <div class="pageHead_1">Work Completion Certificates
                                <span style="float: right; text-align: right;">
                                    <%
                                    CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
                                    String procnature = commonservice.getProcNature(request.getParameter("tenderId")).toString();
                                    String serviceType = commonservice.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
                                    if("services".equalsIgnoreCase(procnature))
                                    {
                                        if("Time based".equalsIgnoreCase(serviceType.toString()))
                                        {
                                    %>
                                            <a class="action-button-goback" href="ProgressReportMain.jsp?tenderId=<%=tenderId%>">Go back</a>
                                       <%}else{%>     
                                            <a class="action-button-goback" href="SrvLumpSumPr.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}}else{%>
                                    <a class="action-button-goback" href="ProgressReport.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}%>
                                </span>
                            </div>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                               <tbody>
                                  <tr>
                                   <th width="70%" class="t-align-center">Certificates</th>
                                   <th width="15%" class="t-align-center">Creation Date</th>
                                   <th width="15%" class="t-align-center">Action</th>
                                </tr>
                                <%
                                    List<Object[]> listCertificate = cmsWcCertificateServiceBean.getWcCetificateDetails(wcCertId);
                                    int size = 0;
                                    int contractSignId = 0;
                                    if(!listCertificate.isEmpty()){
                                        size = listCertificate.size();
                                        contractSignId = Integer.parseInt(listCertificate.get(0)[2].toString());
                                    }
                                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(c_obj[7].toString()), "contractId", EgpModule.Work_Completion_Certificate.getName(), "View Work Completion Certificate History", "PE side");
                                    for(Object[] certificate : listCertificate){
                                %>
                                <tr>
                                    <td width="60%" class="t-align-left">Work plan completion certificate <%=size%></th>
                                    <td width="20%" class="t-align-center"><%=DateUtils.gridDateToStrWithoutSec((Date) certificate[1])%></th>
                                    <td width="20%" class="t-align-center">
                                        <a href="../resources/common/ViewWCCertificateDetails.jsp?wcCertId=<%=wcCertId%>&wcCertHistId=<%=certificate[0]%>&tenderId=<%=tenderId%>&lotId=<%=lotId%>&msg=goback">View</a>
                                    </td>
                                </tr>
                                <%  size--;
                                    } %>
                               </tbody>
                            </table>
                               <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="4%" class="t-align-center">Sl. No.</th>
                                    <th class="t-align-center" width="23%">File Name</th>
                                    <th class="t-align-center" width="32%">File Description</th>
                                    <th class="t-align-center" width="7%">File Size <br />(in KB)</th>
                                    <th class="t-align-center" width="18%">Action</th>
                                </tr>
                                <%
                                        CmsWcCertDocServiceBean cmsWcCertDocServiceBean = new CmsWcCertDocServiceBean();
                                        cmsWcCertDocServiceBean.setLogUserId(userId);
                                        int docCnt = 0;
                                        List<TblCmsWcCertDoc> tblCmsWcCertDocList = cmsWcCertDocServiceBean.getAllCmsWcCertDocForWccId(wcCertId);
                                        if (tblCmsWcCertDocList != null && !tblCmsWcCertDocList.isEmpty()) {
                                            for (TblCmsWcCertDoc tblCmsWcCertDoc : tblCmsWcCertDocList) {
                                                docCnt++;
                                                out.print("<tr>");
                                                out.print("<td class='t-align-center'> " + docCnt + "</td>");
                                                out.print("<td class='t-align-left'> " + tblCmsWcCertDoc.getDocumentName() + "</td>");
                                                out.print("<td class='t-align-left'> " + tblCmsWcCertDoc.getDocDescription() + "</td>");
                                                out.print("<td class='t-align-center'> " + (Long.parseLong(tblCmsWcCertDoc.getDocSize()) / 1024) + "</td>");
                                                out.print("<td class='t-align-center'> "
                                                        + "<a href =" + request.getContextPath() + "/WCCUploadServlet?functionName=download&wcCertDocId="
                                                        + tblCmsWcCertDoc.getWcCertDocId() + "&wcCertId=" + wcCertId + "&tenderId=" + tenderId + "&contractSignId=" + contractSignId
                                                        + "><img src='../resources/images/Dashboard/Download.png' alt='Download' /></a>");
                                                
                                                out.print("</td>");
                                                out.print("</tr>");
                                            }
                                        } else {
                                            out.print("<tr>");
                                            out.print(" <td colspan='5' class='t-align-center'>No records found.</td>");
                                            out.print("</tr>");
                                        }
                                %>
                            </table>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>

