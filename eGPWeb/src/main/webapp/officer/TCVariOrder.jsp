<%-- 
    Document   : TCVariOrder
    Created on : Dec 21, 2011, 2:29:12 PM
    Author     : shreyansh Jogi
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTcvari"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvStaffSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTeamComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <% ResourceBundle srbd = null;
                    srbd = ResourceBundle.getBundle("properties.cmsproperty");
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                    
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Variation Order</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
                <%
                    String referpage = request.getHeader("referer");
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String type = "";
                    int tenderId = 0;
                    int lotId = 0;
                    int formMapId = 0;
                    int srvBoqId = 0;
                    int varId = 0;
                    String styleClass = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    if (request.getParameter("varId") != null) {
                        varId = Integer.parseInt(request.getParameter("varId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                    }
                    if (request.getParameter("formMapId") != null) {
                        formMapId = Integer.parseInt(request.getParameter("formMapId"));
                    }
                    if (request.getParameter("srvBoqId") != null) {
                        srvBoqId = Integer.parseInt(request.getParameter("srvBoqId"));
                    }
                    List<TblCmsSrvTcvari> tcsts = cmss.getDetailOfTCForVari(varId);
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));

        %>
        <script>
            var count = 0;
            </script>
        <%if(request.getParameter("isEdit")!=null){%>
        <script>

              count = <%=tcsts.size()%>
            </script>
            <%}else{%>
                <script>
                   count = 0;
                </script>
                <%}%>
                    <script>
            function addrow()
            {
                var newTxt = '<tr><td class="t-align-left">\n\
                    <input class="formTxtBox_1" type="checkbox" name="chk'+count+' " id="chk'+count+'" /></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="sno_'+count+'" style=width:40px id="sno_'+count+'" /></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="name_'+count+'" id="name_'+count+'" /></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="firm_'+count+'" id="firm_'+count+'" /></td>'+
                    '<td class="t-align-left"><select class="formTxtBox_1" name="staffcat_'+count+'" id="staffcat_'+count+'"><option value="key">key</option><option value="Support">Support</option></select></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="position_'+count+'" id="position_'+ count+'"  /></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="cnsltcat_'+count+'" id="cnsltcat_'+count+'" /></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="AOE_'+count+'" id="AOE_'+count+'" /></td>'+
                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="TA_'+count+'" id="TA_'+count+'" /></td>'+
                    '</tr>';
                    
                $("#resultTable tr:last").after(newTxt);
                count++;
            }
            </script>
            <%if(request.getParameter("isEdit")!=null){%>
            <script>
   function delRow(){
                var listcount = count;
                var trackid="";
                var counter = 0;
                for(var i=0;i<listcount;i++){
                    if(document.getElementById("chk"+i)!=null){
                        if(document.getElementById("chk"+i).checked){
                            if(document.getElementById("varDtlId"+i)!=null){
                                trackid=trackid+document.getElementById("varDtlId"+i).value+",";
                                document.getElementById("count").value = listcount-1;
                            }
                        }
                    }
                }
                $(":checkbox[checked='true']").each(function(){
                    if(document.getElementById("count")!= null){
                        var curRow = $(this).parent('td').parent('tr');
                        curRow.remove();
                        count--;
                        counter++;
                    }
                });
                if(counter==0){
                      jAlert("Please select at least one item to remove","Repeat Order", function(RetVal) {
                        });
                }else{
                    $.post("<%=request.getContextPath()%>/CMSSerCaseServlet", {action:'deleterowforvariationForTC', val: trackid,tenderId:<%=request.getParameter("tenderId")%> },  function(j){
                    jAlert("Selected item(s) deleted successfully","Variation Order", function(RetVal) {
                    });

                });
                }
                

            }
                </script>
                <%}else{%>
                <script>

                    function delRow(){
                var counter = 0;
                $(":checkbox[checked='true']").each(function(){

                        var curRow = $(this).parent('td').parent('tr');
                        curRow.remove();
                        count--;
                        counter++;

                });
                if(counter==0){
                      jAlert("Please select at least one item to remove","Repeat Order", function(RetVal) {
                        });
                }else{
                         calculateGrandTotal();
                }
            }
                    </script>
                    <%}%>
                    <script>
            function onFire(){

                var flag = true;
                var allow = false;
                for(var i=0;i<count;i++){
                    allow = true;
                    if(document.getElementById("sno_"+i)==null){
                        continue;
                    }
                        if(document.getElementById("sno_"+i).value=="" || document.getElementById("name_"+i).value=="" || document.getElementById("firm_"+i).value=="" || document.getElementById("staffcat_"+i).value=="" || document.getElementById("position_"+i).value=="" || document.getElementById("cnsltcat_"+i).value=="" || document.getElementById("AOE_"+i).value=="" ||  document.getElementById("TA_"+i).value==""){
                            jAlert("It is necessary to enter all data","Variation Order", function(RetVal) {
                            });
                            flag=false;
                        }
                }
                if(flag){
                    if(!allow){
                        jAlert("It is necessary to enter at least one Employee detail","Variation Order", function(RetVal) {
                            });
                             return false;
                    }else{
                        document.getElementById("count").value=count;
                    document.forms["frm"].submit();
                    }
                    
                }else{
                    return false;
                }

            }
        </script>
    </head>
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">Team Composition and Task Assignments
                        <%
                                    if (request.getParameter("tenderId") != null) {
                                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                                    }
                        %>
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="<%=referpage%>" title="Go Back">Go Back</a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%if (request.getParameter("lotId") != null) {%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <%}%>
    <body>
        <form name="frm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post" >
             <%if(request.getParameter("isEdit")!=null){%>
             <input type="hidden" name="isEdit" value ="TCvari" />
             <%}%>
            <input type="hidden" name="action" value ="TCvari" />
            <input type="hidden" name="count" id="count" value ="" />
            <input type="hidden" name="srvFormMapId" value ="<%=formMapId%>" />
            <input type="hidden" name="tenderId" value ="<%=request.getParameter("tenderId")%>" />
            <input type="hidden" name="varId" value ="<%=request.getParameter("varId")%>" />
            <%
                if(request.getParameter("isEdit")!=null){
            %>
                <input type="hidden" name="isEdit" id="isEdit" value="true" />
            <%
                }
            %>
             <table width="100%" cellspacing="0" class="t_space">
                                    <tr><td colspan="5" style="text-align: right;">
                                            <a class="action-button-add" id="addRow" onclick="addrow()">Add Item</a>
                                            <a class="action-button-delete" id="delRow" onclick="delRow()">Remove Item</a>
                                        </td></tr>
                                </table>
            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                    <tr>
                                        <th>Check</th>
                                        <th>Sl. No.</th>
                                        <th>Name of  Staff </th>
                                        <th>Firm/Organization</th>
                                        <th>Staff Category    (Key / Support)</th>
                                        <th>Position Defined by Consultant </th>
                                        <th>Category of Consultant proposed Consultant Category</th>
                                        <th>Area of Expertise</th>
                                        <th>Task Assigned</th>
                                    </tr>
                                    <%
                                         List<TblCmsSrvTeamComp> list = cmss.getTeamCompositionData(formMapId);
                                         if (!list.isEmpty()) {
                                             for (int i = 0; i < list.size(); i++) {
                                                
                                    %>

                                    <tr>
                                        <td class="t-align-left">&nbsp;</td>
                                        <td class="t-align-left"><%=list.get(i).getSrno()%></td>
                                        <td class="t-align-left"><%=list.get(i).getEmpName()%></td>
                                        <td class="t-align-left"><%=list.get(i).getOrganization()%></td>
                                        <td class="t-align-left"><%=list.get(i).getStaffCat()%></td>
                                        <td class="t-align-left"><%=list.get(i).getPositionDefined()%></td>
                                        <td class="t-align-left"><%=list.get(i).getConsultPropCat()%></td>
                                        <td class="t-align-left"><%=list.get(i).getAreaOfExpertise()%></td>
                                        <td class="t-align-left"><%=list.get(i).getTaskAssigned()%></td>
                                    </tr>
                                    <%}
                                         }%>
                                          <%
                                          if(request.getParameter("isEdit")!=null){
                                          for(int i=0;i<tcsts.size();i++){
    %>
                    <tr><td class="t-align-left">
                            <input type="hidden" name="varDtlId<%=i%>" id="varDtlId<%=i%>" value="<%=tcsts.get(i).getSrvTcvariId()%>" />
                    <input class="formTxtBox_1" type="checkbox" id="chk<%=i%>" name="chk<%=i%>" /></td>
                    <td class="t-align-left"><input class="formTxtBox_1" type="text" value="<%=tcsts.get(i).getSrno()%>" name="sno_<%=i%>" style=width:40px id="sno_<%=i%>" /></td>
                    <td class="t-align-left"><input class="formTxtBox_1" type="text" value="<%=tcsts.get(i).getEmpName()%>" name="name_<%=i%>" id="name_<%=i%>" /></td>
                    <td class="t-align-left"><input class="formTxtBox_1" type="text" value="<%=tcsts.get(i).getOrganization()%>" name="firm_<%=i%>" id="firm_<%=i%>" /></td>
                    <td class="t-align-left"><select class="formTxtBox_1" name="staffcat_<%=i%>" id="staffcat_<%=i%>" >
                            <option value="key" <%if("key".equalsIgnoreCase(tcsts.get(i).getStaffCat())){%> selected<%}%>>key</option>
                            <option value="support" <%if("support".equalsIgnoreCase(tcsts.get(i).getStaffCat())){%> selected<%}%>>Support</option>
                        </select></td>
                    <td class="t-align-left"><input class="formTxtBox_1" type="text" value="<%=tcsts.get(i).getPositionDefined()%>" name="position_<%=i%>" id="position_<%=i%>"  /></td>
                    <td class="t-align-left"><input class="formTxtBox_1" type="text" value="<%=tcsts.get(i).getConsultPropCat()%>" name="cnsltcat_<%=i%>" id="cnsltcat_<%=i%>" /></td>
                    <td class="t-align-left"><input class="formTxtBox_1" type="text" value="<%=tcsts.get(i).getAreaOfExpertise()%>" name="AOE_<%=i%>" id="AOE_<%=i%>" /></td>
                    <td class="t-align-left"><input class="formTxtBox_1" type="text" value="<%=tcsts.get(i).getTaskAssigned()%>" name="TA_<%=i%>" id="TA_<%=i%>" /></td>
                    </tr>
            <%}}%>
            <input type="hidden" id="count" name="count" value="<%=tcsts.size()%>" />
            </table>
             <table width="100%" cellspacing="0" class="t_space">
                                    <tr><td colspan="5" style="text-align: right;">
                                            <a class="action-button-add" id="addRow" onclick="addrow()">Add Item</a>
                                            <a class="action-button-delete" id="delRow" onclick="delRow()">Remove Item</a>
                                        </td></tr>
                                </table>
                        <center>
                            <label class="formBtn_1">
                                <input type="button" name="Boqbutton" id="Boqbutton" value="Submit" onclick="onFire();" />
                            </label>
                        </center>
        </form>

                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </body>
</html>
