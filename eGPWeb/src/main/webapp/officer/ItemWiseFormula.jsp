<%-- 
    Document   : ItemWiseFormula
    Created on : Dec 23, 2010, 11:20:34 AM
    Author     : TaherT
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTables" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderColumns" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCells" %>
<%@page import="com.cptu.egp.eps.model.table.TblReportColumnMaster" %>
<%@page import="com.cptu.egp.eps.web.servicebean.ReportCreationSrBean" %>
<%@page import="com.cptu.egp.eps.model.table.TblReportFormulaMaster" %>
<jsp:useBean id="frmView"  class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Formula</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">            
             $(function() {
                $('#frmItem').submit(function() {
                     var i=0;
                     $(":checkbox[checked='true']").each(function(){
                         var chkbox = $(this).val();                         
                         if(chkbox.indexOf("_", 0)!=-1){
                            i++;
                         }
                     });
                     if(i==0){
                         jAlert("Please Select Government Column.","Column Alert", function(RetVal) {
                         });
                         return false;
                     }
                     var j=0;
                     if($("input[type='radio']").val()!=null){
                        $(":radio[checked='true']").each(function(){
                         j++;
                     });
                     if(j==0){
                         jAlert("Please Select Governing(Auto) Column.","Column Alert", function(RetVal) {
                         });
                         return false;
                     }
                     }                     
                });
            });
            $(function() {
                $('#frmDel').submit(function() {
                     var i=0;
                     $(":checkbox[checked='true']").each(function(){
                         var chkbox = $(this).val();
                         if(chkbox.indexOf("_", 0)==-1){
                            i++;
                         }
                     });
                     if(i==0){
                         jAlert("Please select Column for delete.","Column Alert", function(RetVal) {
                         });
                         return false;
                     }
                });
            });
        </script>
    </head>
    <body>
        <%
                    String tenderid = request.getParameter("tenderid");
                    ReportCreationSrBean rcsb = new ReportCreationSrBean();                    
                    String repId = request.getParameter("repId");
                    int reportTableId = Integer.parseInt(rcsb.getReportTableId(repId));                            
                    List<TblReportFormulaMaster> formList = rcsb.getRepFormulaItemWise(reportTableId);
                    if("Submit".equalsIgnoreCase(request.getParameter("submit"))){
                        rcsb.createItemWiseFormula(repId,request.getParameter("repForm"),request.getParameterValues("repHead"));
                        response.sendRedirect("ItemWiseFormula.jsp?tenderid="+tenderid+"&repId="+repId);
                    }
                    if("Delete".equalsIgnoreCase(request.getParameter("delRep"))){
                        rcsb.delRepColFormula(request.getParameterValues("reportFormulaId"));
                        response.sendRedirect("ItemWiseFormula.jsp?tenderid="+tenderid+"&repId="+repId);
                    }
                    Object formId = rcsb.getFormsForReport(repId).toArray()[0];
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <%
                    pageContext.setAttribute("tenderId", tenderid);
        %>
        <div class="pageHead_1">Create Formula<span style="float: right;"><a class="action-button-goback" href="Notice.jsp?tenderid=<%=tenderid%>">Go back to Dashboard</a></span></div>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <form action="ItemWiseFormula.jsp?tenderid=<%=tenderid%>&repId=<%=repId%>" method="post" name="frmItem" id="frmItem">
            <table width="100%" cellspacing="0" class="tableList_1">
                <%
                            List<TblTenderTables> tblTenderTables = frmView.getTenderTables(Integer.parseInt(formId.toString()));
                            for (TblTenderTables tbl : tblTenderTables) {
                                java.util.List<com.cptu.egp.eps.model.table.TblTenderTables> tblInfo = frmView.getTenderTablesDetail(tbl.getTenderTableId());
                                short cols = 0;
                                short rows = 0;
                                String tableName = "";
                                if (tblInfo != null) {
                                    if (tblInfo.size() >= 0) {
                                        tableName = tblInfo.get(0).getTableName();
                                        cols = tblInfo.get(0).getNoOfCols();
                                        rows = tblInfo.get(0).getNoOfRows();
                                    }
                                    tblInfo = null;
                                }                                
                                java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderColumns> tblColumnsDtl = frmView.getColumnsDtls(tbl.getTenderTableId(), true).listIterator();                                
                                int[] colId = new int[cols];
                                byte[] filledBy = new byte[cols];
                                byte[] dataType = new byte[cols];
                                int rad=0;
%>
                <table width="100%" cellspacing="0" class="tableView_1 t_space b_space">
                    <tr>
                        <td width="10%" class="ff">Table Name : </td>
                        <td width="90%"><%=tableName%></td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" class="tableList_1" id="FormMatrix">
                    <tbody>
                        <%
                                                    String colHeads[] = new String[cols];
                                                    for (short i = -1; i <= rows; i++) {
                                                        if (i == 0) {
                        %>
                        <tr>
                            <%
                                
                                for (short j = 0; j < cols; j++) {
                                    if (tblColumnsDtl.hasNext()) {
                                        TblTenderColumns ttc = tblColumnsDtl.next();
                                        colHeads[j] = ttc.getColumnHeader();
                                        dataType[j] = ttc.getDataType();
                                        colId[j] = ttc.getColumnId();
                                        filledBy[j] = ttc.getFilledBy();
                                        for(TblReportFormulaMaster master : formList){
                                            if(master.getColumnId()!=0){
                                                if(Integer.parseInt(master.getFormula().split("_")[1])==colId[j]){
                                                    rad++;
                                                }
                                            }
                                        }
                                        ttc = null;
                                       }
                               }                                
                               for (short j = 0; j < cols; j++) {
                             %>
                            <th class="t-align-center">
                                <%                                
                                if (filledBy[j] == 1) {
                                    boolean chk1=true;
                                    for(TblReportFormulaMaster master : formList){
                                        if(Integer.parseInt(master.getFormula().split("_")[1])==colId[j]){
                                            chk1=false;
                                        }                                        
                                    }
                                    if(chk1){
                                            out.print("<input type=\"checkbox\" name=\"repHead\" value=\"" + colHeads[j]+"@#"+tbl.getTenderTableId() + "_" + colId[j] + "\"/>");
                                    }
                                } else if (filledBy[j] == 2) {                                                                        
                                    if(rad==0){
                                            if(dataType[j]!=1 && dataType[j]!=2){
                                            out.print("<input type=\"radio\" name=\"repForm\" value=\"" + colHeads[j]+"@#"+tbl.getTenderTableId() + "_" + colId[j] + "\"/>");
                                            }
                                    }
                                } else if (filledBy[j] == 3) {                                    
                                    if(rad==0){
                                            if(dataType[j]!=1 && dataType[j]!=2){
                                               out.print("<input type=\"radio\" name=\"repForm\" value=\"" + colHeads[j]+"@#"+tbl.getTenderTableId() + "_" + colId[j] + "\"/>");
                                            }
                                        }
                                }
                                %>
                            </th>
                            <%
                                                                            }
                            %>
                        </tr>
                        <tr id="ColumnRow">
                            <%
                                         for (short j = 0; j < cols; j++) {
                            %>
                            <td id="addTD<%= j + 1%>">
                                <%= colHeads[j]%>
                            </td>
                            <%
                                                                            }
                            %>
                        </tr>
                        <%
                                            }
                                        }
                                    }
                        %>
                    </tbody>
                </table>
            </table>
            <br/>
            <div class="t-align-center">
                <label class="formBtn_1">
                    <input type="submit" name="submit" id="submit" value="Submit"/>
                </label>
            </div>
            </form>
            <br/>
            <form action="ItemWiseFormula.jsp?tenderid=<%=tenderid%>&repId=<%=repId%>" method="post" name="frmDel" id="frmDel">
            <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th style="text-align:left; width:4%;">Sl. No.</th>
                            <th style="text-align:left;">Column Name</th>
                            <th style="text-align:left;">Column Type</th>
                            <th style="text-align:left; width:10%;">Delete</th>
                        </tr>
                        <%
                            int q=1;                            
                            for(TblReportFormulaMaster formulaList : formList){
                         %>
                         <tr class="<%if(q%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                             <td><%=q%></td>
                             <td><%=formulaList.getDisplayFormula()%></td>
                             <td><%if(formulaList.getColumnId()==0){out.print("Government Column");}else{out.print("Governing Column");}%></td>
                             <td>
                                 <input type="checkbox" name="reportFormulaId" value="<%=formulaList.getReportFormulaId()%>"/>
                             </td>
                         </tr>
                         <%q++;}%>
                          <tr>
                              <td colspan="4" class="t-align-center">
                                  <label class="<%if(formList.isEmpty()){out.print("formBtn_disabled");}else{out.print("formBtn_1");}%>">
                                      <input type="submit" name="delRep" id="delRep" value="Delete" <% if(formList.isEmpty()){%>disabled<%}%>/>
                                </label>
                              </td>
                          </tr>
                    </table>
                   </form>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
