<%-- 
    Document   : SrvReExpense
    Created on : Dec 5, 2011, 11:52:47 AM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvAttSheetMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<jsp:useBean id="cmsVendorReqWccSrBean" class="com.cptu.egp.eps.web.servicebean.CmsVendorReqWccSrBean" />
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment Schedule(Lump Sum - Progress Report)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>


    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Payment Schedule(Lump Sum - Progress Report)</div>
            <%
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                pageContext.setAttribute("tab", "14");
                ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
                String tenderId = "";
                if(request.getParameter("tenderId")!=null){
                    tenderId = request.getParameter("tenderId");
                }

                String lotId = "";
                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                for (SPCommonSearchDataMore lotList : packageLotList) {
                    lotId = lotList.getFieldName5();
                }

                System.out.println("lotId" +lotId);
                int srvFormMapId = 0;
                CMSService cmsSrv = (CMSService) AppContext.getSpringBean("CMSService");
                List<Object> lstGetSrvFormMapId = cmsSrv.getSrvFormMapId(Integer.parseInt(tenderId), 12);
                if(lstGetSrvFormMapId!=null && !lstGetSrvFormMapId.isEmpty()){
                    srvFormMapId = (Integer) lstGetSrvFormMapId.get(0);
                }
                
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                pageContext.setAttribute("lotId", lotId);
                CommonService commonServicee = (CommonService) AppContext.getSpringBean("CommonService");
                String procnaturee = commonServicee.getProcNature(request.getParameter("tenderId")).toString();
                String strProcNaturee = "";                    
                if("Services".equalsIgnoreCase(procnaturee))
                {
                    strProcNaturee = "Consultant";
                }else if("goods".equalsIgnoreCase(procnaturee)){
                    strProcNaturee = "Supplier";
                }else{
                    strProcNaturee = "Contractor";
                }
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%@include file="../resources/common/ContractInfoBar.jsp"%>
            <div>&nbsp;</div>
            <%
                        
            %>
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "2");

                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1">
                    <%--<% if (request.getParameter("msg") != null) {
                         if ("Updated".equalsIgnoreCase(request.getParameter("msg"))) {
                    %>
                    <div class='responseMsg successMsg'>Selected MileStone Completed SuccessFully</div>
                    <%}else if("fail".equalsIgnoreCase(request.getParameter("msg"))){%>
                    <div class='responseMsg errorMsg'>Selected MileStone Completion Failed</div>
                    <%}}%>--%>
                    <div align="center">                        
                        <form id="frmReExpenses" name="frmReExpenses" method="post" action='<%=request.getContextPath()%>/CMSSerCaseServlet?srvFormMapId=<%=srvFormMapId%>&action=SrvPaymentSchedule&tenderId=<%=tenderId%>&lotId=<%=lotId%>'>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <th>S No</th>                                    
                                <th>Milestone Name </th>                                
                                <th>Description</th>                                
                                <th>Payment as % of Contract Value</th>                                
                                <th>Mile Stone Date proposed by PE </th>
                                <th>Mile Stone Date proposed by Consultant</th>                                
                                <th>Action</th>
                                <%                                    
                                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                                    List<TblCmsSrvPaymentSch> list = cmss.getPaymentScheduleData(srvFormMapId);
                                    if(!list.isEmpty()){
                                    for(int i=0; i<list.size(); i++)
                                    {    
                                %>
                                <tr>
                                    <td class="t-align-right" style="text-align: right"><%=list.get(i).getSrNo()%></td>                                    
                                    <td class="t-align-left"><%=list.get(i).getMilestone()%></td>                                    
                                    <td class="t-align-left"><%=list.get(i).getDescription()%></td>                                    
                                    <td class="t-align-right" style="text-align: right"><%=list.get(i).getPercentOfCtrVal()%></td>                                    
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(list.get(i).getPeenddate())%></td>
                                    <td class="t-align-center"><%=DateUtils.customDateFormate(list.get(i).getEndDate())%></td> 
                                    <td class="t-align-center">
                                    <%if(!"Completed".equalsIgnoreCase(list.get(i).getStatus())){%>
                                    If deliverable verified and milestone fulfilled , click the following link<br/>
                                    <a href="#" onclick="validate('<%=tenderId%>','<%=list.get(i).getSrvPsid()%>','<%=lotId%>');"style="">Completed</a>
<!--                                    <a class="anchorLink" onclick="validate('<%=tenderId%>','<%=list.get(i).getSrvPsid()%>','<%=lotId%>');"style="text-decoration: none; color: #fff; ">Completed</a>                                    -->
                                    <%}else{%>
                                    Completed
                                    &nbsp;|&nbsp;<a href="SrvLumpSumUploadDoc.jsp?tenderId=<%=tenderId%>&wpId=0&keyId=<%=list.get(i).getSrvPsid()%>&docx=LumpSumPR&module=PR">Upload / Download Document</a>
                                    <%}%>
                                    </td>
                                    <input type="hidden" value="<%=list.get(i).getSrvPsid()%>" name="SrvPsid"/>                                                                         
                                </tr>                                
                                <%}}%>                                
                            </table>
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">    
                                <tr>
                                    <td colspan="2">
                                        <span style="float: left; text-align: left;font-weight:bold;">
                                            <a href="ViewTotalInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>" class="">Financial Progress Report</a>
                                        </span>
                                    </td>
                                </tr>    
                            </table>
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <td class="ff" width="15%" class="t-align-left">Attendance sheet</td>

                                    <td  class="t-align-left">
                                        <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                            <tr>
                                                <th width="15%" class="t-align-left">Sr.No</th>
                                                <th class="t-align-left">Attendance Sheet</th>
                                                <th class="t-align-left">Month-Year</th>
                                                <th class="t-align-left">Created Date</th>
                                                <th class="t-align-left">Action</th>
                                                <%
                                                    List<TblCmsSrvAttSheetMaster> os = cmss.getAllAttSheetDtls(Integer.parseInt(tenderId));
                                                    List<Object> oss = cmss.getWpIdFrmLotId(Integer.parseInt(lotId));
                                                    boolean flag = true;


                                                    MonthName monthName = new MonthName();
                                                    if (os != null && !os.isEmpty()) {
                                                        int cc = 1;
                                                        for (TblCmsSrvAttSheetMaster master : os) {
                                                            if (oss != null && !oss.isEmpty()) {
                                                                flag = cmss.editLinkForAttOrNot(oss.get(0).toString(), master.getAttSheetId());
                                                            }
                                                %>
                                            <tr>
                                                <td width="12%" class="t-align-left"><%=cc%></td>
                                                <td class="t-align-left">Attendance Sheet - <%=cc%></td>
                                                <td class="t-align-left"><%=monthName.getMonth(master.getMonthId()) + " - " + master.getYear()%></td>
                                                <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec(master.getCreatedDt())%></td>

                                                <td class="t-align-left"><a href="ViewSheetdtls.jsp?sheetId=<%=master.getAttSheetId()%>&tenderId=<%=tenderId%>">View</a>
                                                    <%
                                                        if ("pending".equalsIgnoreCase(cmss.checkAttStatus(master.getAttSheetId() + ""))) {%>&nbsp;|&nbsp;
                                                    <a href="EditSheetdtls.jsp?sheetId=<%=master.getAttSheetId()%>&tenderId=<%=tenderId%>&lotId=<%=lotId%>">Approval/Rejection</a>
                                                    <%}%>
                                                    &nbsp;|&nbsp;<a href="AttanshtUploadDoc.jsp?tenderId=<%=tenderId%>&wpId=0&keyId=<%=master.getAttSheetId()%>&docx=AttSheet&module=PR">Upload / Download Document</a>
                                                </td>
                                            </tr>

                                            <%cc++;
                                }
                            } else {%>
                                            <tr>
                                                <td colspan="5">No records found</td>
                                            </tr>
                                            <%}%>
                                        </table>
                                    </td>
                                </tr>
                            </table>            
                            <%
                                List<Object[]> objectlist =    issueNOASrBean.getDetailsNOAforInvoice(Integer.parseInt(tenderId), Integer.parseInt(lotId));
                                 if(!objectlist.isEmpty()){
                                     Object[] object = objectlist.get(0);
                                     if (issueNOASrBean.isAvlTCS((Integer) object[1])) {
                                             int contractSignId = 0;
                                             contractSignId = issueNOASrBean.getContractSignId();
                                             CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
                                             cmsWcCertificateServiceBean.setLogUserId(userId);
                                             TblCmsWcCertificate tblCmsWcCertificate =
                                                     cmsWcCertificateServiceBean.getCmsWcCertificateForContractSignId(contractSignId);
                                             out.print("<table cellspacing='0' class='tableList_1' width='100%'>");
                                             if (tblCmsWcCertificate == null) {
                                                 out.print("<tr><td class='ff' width='20%' colspan='2' class='t-align-left'><div  class='responseMsg noticeMsg t-align-left'>"
                                                 + "On receiving the request of Issue Work Completion Certificate from "+strProcNaturee+" ");
                                                 out.print("system will display a link 'Issue Work Completion Certificate'</div></td></tr>");
                                             }
                                             out.print("<tr>");
                                             out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                             out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                             CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                                             cmsContractTerminationBean.setLogUserId(userId);
                                             //if(!isConTer)
                                             //{
                                                if(tblCmsWcCertificate == null){
                                                    if(cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotId),0)){
                                                        out.print("<a href ='WorkCompletionCertificate.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                             + "&contractSignId=" + contractSignId + "&lotId=" + lotId + "'>Issue Work Completion Certificate</a>&nbsp;&nbsp;&nbsp;&nbsp;");
                                                    }
                                                 }
                                                 if(tblCmsWcCertificate != null){
                                                    if(cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotId),0) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                         out.print("<a href ='WorkCompletionCertificate.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                         + "&contractSignId=" + contractSignId + "&lotId=" + lotId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()+"'>Issue Work Completion Certificate</a>&nbsp;&nbsp;&nbsp;&nbsp;");
                                                    }
                                                    out.print("<a href ='ViewWCCertificate.jsp?wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                         + "&tenderId=" + tenderId + "&lotId=" + lotId + "'>View Work Completion Certificate</a>");
                                                    //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                   // if(!"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                            out.print("&nbsp|&nbsp<a href ='WCCUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                                 + "&contractSignId=" + contractSignId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                                 + "'>Upload / Download Files</a>");
                                                  //  }
                                                }
                                             //}else{
                                             //    out.print("<a href='#' onclick='CTerminationMsg()'>Issue Work Completion Certificate</a>");
                                             //}
                                             out.print(" </td>");
                                             out.print(" </tr>");
                                             /*out.print("<td class='ff' width='20%'  class='t-align-left'> Contract Termination </td>");
                                             out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                             if (tblCmsContractTermination == null) {
                                                 out.print("<a href ='ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                         + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Terminate</a> ");
                                             } else {
                                                 out.print("<a href ='ViewContractTermination.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                         + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>ViewContractTermination</a>");
                                                 out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                                 out.print("<a href ='ContractTeminationUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                         + "&contractSignId=" + contractSignId + "&contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                         + "'>Upload/Download Files</a>");
                                             }*/
                                             out.print("</table>");
                                         }
                                 }
                            %>
                        </form>
                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
<script>
function validate(tenderId,SrvPsid,lotId)
{    
    jConfirm("Are you sure MileStone has been Completed?","Payment Schedule - PR", function(RetVal){
        if(RetVal)
        {
           dynamicFromSubmit("<%=request.getContextPath()%>/CMSSerCaseServlet?tenderId="+tenderId+"&SrvPsid="+SrvPsid+"&lotId="+lotId+"&action=UpdateSrvPSPr");
        }
    });  
}
var headSel_Obj = document.getElementById("headTabTender");
if(headSel_Obj != null){
headSel_Obj.setAttribute("class", "selected");
}
</script>
</html>

