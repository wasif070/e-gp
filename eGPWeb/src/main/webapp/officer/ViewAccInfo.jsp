<%-- 
    Document   : ViewAccInfo
    Created on : Mar 1, 2012, 4:15:19 PM
    Author     : shreyansh Jogi
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblNoaAcceptance"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Account Information</title>
    </head>
    <body onload="getQueryData('getmyprebidquery');">
        <div class="mainDiv">
            <div class="contentArea_1">
                <div class="pageHead_1">View Account information</div>
                <%
                    NOAServiceImpl noasi = (NOAServiceImpl)AppContext.getSpringBean("NOAServiceImpl");
                    List<Object[]> list = noasi.getNOAListing(Integer.parseInt(request.getParameter("id")));
                    int NOAID = 0;
                    if(list!=null && !list.isEmpty()){
                        NOAID = (Integer)list.get(0)[1];
                        }
                        TblNoaAcceptance tna = tenderSrBean.getTblNoaAcceptance(NOAID);
                        if(tna!=null && "approved".equalsIgnoreCase(tna.getAcceptRejStatus())){
                    %>
                    <div>&nbsp;</div>
                    <table width="100%" class="tableList_1" cellpadding="0" cellspacing="5">
                        
                        <tr>
                            <td width="15%" class="ff">Title of the Account :  </td>
                            <td width="35%"><%=tna.getAccountTitle()!=null?tna.getAccountTitle():"-"%></td>
                            <td width="15%" class="ff">Name of Bank :  </td>
                            <td width="35%"><%=tna.getBankName()!=null?tna.getBankName():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Name of Branch :  </td>
                            <td><%=tna.getBranchName()!=null?tna.getBranchName():"-"%></td>
                            <td class="ff">Account Number :  </td>
                            <td><%=tna.getAccountNo()!=null?tna.getAccountNo():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Telephone :  </td>
                            <td><%=tna.getTelephone()!=null?tna.getTelephone():"-"%></td>
                            <td class="ff">Fax No :  </td>
                            <td><%=tna.getFax()!=null?tna.getFax():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch e-mail ID :  </td>
                            <td><%=tna.getEmail()!=null?tna.getEmail():"-"%></td>
                            <td class="ff">SWIFT Code :  </td>
                            <td><%=tna.getSwiftCode()!=null?tna.getSwiftCode():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Address : </td>
                            <td><%=tna.getAddress()!=null?tna.getAddress():"-"%></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    </div>
                    <%}%>

            </div>

</html>
