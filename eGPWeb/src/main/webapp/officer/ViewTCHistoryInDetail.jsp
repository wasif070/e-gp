<%-- 
    Document   : ViewDSHistory
    Created on : Sep 15, 2011, 12:00:54 PM
    Author     : dixit
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTchistory"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTchistory"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetailHistory"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    ResourceBundle srbd = null;
                    srbd = ResourceBundle.getBundle("properties.cmsproperty");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View History</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <%
                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        tenderId = request.getParameter("tenderId");
                    }
                    String lotId = "";
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = request.getParameter("lotId");
                    }
                    String formMapId = "";
                    if (request.getParameter("formMapId") != null) {
                        formMapId = request.getParameter("formMapId");
                    }
                    if (request.getParameter("srvFormMapId") != null) {
                        formMapId = request.getParameter("srvFormMapId").toString();
                    }
                    String wpId = "";
                    if (request.getParameter("wpId") != null) {
                        wpId = request.getParameter("wpId");
                    }
                    String historyCount = "";
                    if (request.getParameter("historyCount") != null) {
                        historyCount = request.getParameter("historyCount");
                    }
                    String userId = "";
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = session.getAttribute("userId").toString();

                    }
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                    CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                    List<Object[]> list = cs.getLotDetailsByPkgLotId(lotId, tenderId);
                    String procnature = cs.getProcNature(tenderId).toString();
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div  id="print_area">
                <div class="contentArea_1">
                    <div class="pageHead_1">View History
                        <span class="noprint" style="float: right; text-align: right;">
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                            <a class="action-button-goback" href="ViewTCHistory.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&wpId=<%=wpId%>&formMapId=<%=formMapId%>" title="Delivery Schedule">Go Back</a>
                        </span>
                    </div>

                    <%@include file="../resources/common/TenderInfoBar.jsp"%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <div>&nbsp;</div>

                    <div class="tabPanelArea_1">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                            <%for (Object[] obj : list) {%>
                            <tr>
                                <td width="20%">Package No.</td>
                                <td width="80%"><%=obj[0]%></td>
                            </tr>
                            <tr>
                                <td>Package Description</td>
                                <td class="t-align-left"><%=obj[1]%></td>
                            </tr>
                            <%}%>
                        </table>

                        <br />
                        <table width="100%" cellspacing="0" id="resultTable" class="tableList_3">
                            <tr>
                                <th width="10%"><%=srbd.getString("CMS.service.workSchedule.srNo")%></th>
                                <th width="10%"><%=srbd.getString("CMS.service.workSchedule.positionAssign")%></th>
                                <th>Name of  Staff</th>
                                <th>Firm/Organization</th>
                                <th>Staff Category (Key / Support)</th>
                                <th>Position Defined by Consultant</th>
                                <th>Category of Consultant proposed</th>
                                <th>Area of Expertise</th>
                                <th>Task Assigned</th>
                                <th>Creation Date</th>
                            </tr>
                            <%
                                        List<Object[]> getHistoryDetail = cmss.getSrvTcHistoryDetailData(Integer.parseInt(formMapId), Integer.parseInt(historyCount));
                                        if (!getHistoryDetail.isEmpty()) {
                                            for (int j = 0; j < getHistoryDetail.size(); j++) {
                            %>
                            <tr>
                                <td class="t-align-left"><%=getHistoryDetail.get(j)[6]%></td>
                                <td class="t-align-left"><%=getHistoryDetail.get(j)[7]%></td>
                                <td class="t-align-left"><%=getHistoryDetail.get(j)[0]%></td>
                                <td class="t-align-left"><%=getHistoryDetail.get(j)[1]%></td>
                                <td class="t-align-left"><%=getHistoryDetail.get(j)[8]%></td>
                                <td class="t-align-left"><%=getHistoryDetail.get(j)[2]%></td>
                                <td class="t-align-left"><%=getHistoryDetail.get(j)[3]%></td>
                                <td class="t-align-left"><%=getHistoryDetail.get(j)[4]%></td>
                                <td class="t-align-left"><%=getHistoryDetail.get(j)[5]%></td>
                                <%if(getHistoryDetail.get(j)[9]!=null){%>
                                    <td class="t-align-center"><%=DateUtils.gridDateToStr((Date)getHistoryDetail.get(j)[9])%></td>
                                <%}else{%>
                                    <td class="t-align-center">-</td>
                                <%}%>
                            </tr>
                            <%}
                                    } else {%>
                            <td class="t-align-center" colspan="5">No Records Found</td>
                            <%}%>
                        </table>
                        `</div>
                </div>
            </div></div>
            <%
                ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId",EgpModule.Work_Schedule.getName(), "View Team Composition and Task Assignment history detail", "");
            %>
        <div>&nbsp;</div>
        <%@include file="../resources/common/Bottom.jsp" %>
    </body>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#print").click(function() {
                printElem({ leaveOpen: true, printMode: 'popup' });
            });

        });
        function printElem(options){
            $('#print_area').printElement(options);
        }
    </script>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

