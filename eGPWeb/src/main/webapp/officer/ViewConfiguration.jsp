<%--
    Document   : ViewConfiguration
    Created on : Jan 19, 2011, 12:12:19 PM
    Author     : Karan
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">

            <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
            %>
            <%
                        HttpSession hs = request.getSession();

                        String tenderId = "";
                        String strUserId = "0";

                        boolean isCurrentUser_TEC_CP = false;

                        if (request.getParameter("tenderid") != null) {
                            tenderId = request.getParameter("tenderid");
                        }

                        if (hs.getAttribute("userId") != null) {
                            strUserId = hs.getAttribute("userId").toString();
                        }
                         String strComType = "null";
                            if (request.getParameter("comType") != null && !"null".equalsIgnoreCase(request.getParameter("comType"))) {
                                strComType = request.getParameter("comType");
                            }
            %>

            <div class="pageHead_1">Evaluation Configuration
                <span class="c-alignment-right"><a href="EvalComm.jsp?tenderid=<%=tenderId%>&comType=<%=strComType%>" title="Tender/Proposal Document" class="action-button-goback">Go Back</a></span>
            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>

            <%
                // Get TEC chairperson information
                List<SPTenderCommonData> lstTenderTEC_CPInfo =
                        tenderCommonService.returndata("GetTecCpEmail", tenderId, null);

                if(!lstTenderTEC_CPInfo.isEmpty()){
                    if(!"0".equalsIgnoreCase(strUserId)){
                        if(strUserId.equalsIgnoreCase(lstTenderTEC_CPInfo.get(0).getFieldName3())){
                            isCurrentUser_TEC_CP=true; // Current User is the TEC chairperson
                        }
                    }
                }

                lstTenderTEC_CPInfo=null;
            %>

            <div class="tabPanelArea_1">
                <%-- Start: CODE TO DISPLAY MESSAGES --%>
                 <%if (request.getParameter("msgId")!=null){
                    boolean isError = false;
                    String msgId = "";
                    String msgTxt = "";

                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("tscrequested")){
                            msgTxt="TSC formation request sent successfully.";
                        } else  if(msgId.equalsIgnoreCase("tscrequesterr")){
                            isError=true; msgTxt="Error while requesting TSC formation.";
                        }  else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                   <div class="responseMsg errorMsg" style="margin-bottom: 12px;" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                   <%}%>
                <%}
                 msgId=null;
                 msgTxt=null;
                }%>
                <%-- End: CODE TO DISPLAY MESSAGES --%>

                <%-- Start: Common Evaluation Table --%>
                <% // @include file="/officer/EvalCommCommon.jsp" %>
               <%-- End: Common Evaluation Table --%>

<%
               boolean isConfigExists = false;
               boolean isTscReq = false;
               String strEvalConfigId = "0";
               String strConfigType="";
               String strSelectedTECUserId="0";
               String strSelectedTECMemberNm="";
               String strEvalStatus = "";

               // Get Evlauation Configuration information
               List<SPTenderCommonData> lstTenderEvalConfigStatus =
                       tenderCommonService.returndata("getTenderEvalConfigStatus", tenderId, null);

               if (!lstTenderEvalConfigStatus.isEmpty()) {
                   if ("yes".equalsIgnoreCase(lstTenderEvalConfigStatus.get(0).getFieldName1())) {
                       isConfigExists = true;
                       strEvalConfigId = lstTenderEvalConfigStatus.get(0).getFieldName2();

                       if ("yes".equalsIgnoreCase(lstTenderEvalConfigStatus.get(0).getFieldName3())) {
                           isTscReq = true; // TSC Formation is required
                       }
                       strEvalStatus = lstTenderEvalConfigStatus.get(0).getFieldName4();

                       if (!"0".equalsIgnoreCase(lstTenderEvalConfigStatus.get(0).getFieldName6())) {
                        strSelectedTECUserId=lstTenderEvalConfigStatus.get(0).getFieldName6();
                       }

                       if (!"null".equalsIgnoreCase(lstTenderEvalConfigStatus.get(0).getFieldName8())) {
                        strSelectedTECMemberNm=lstTenderEvalConfigStatus.get(0).getFieldName8();
                       }
                       strConfigType=lstTenderEvalConfigStatus.get(0).getFieldName7();
                   }
               }

               lstTenderEvalConfigStatus=null;
%>

               <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1">
                    <tr style="display: none;">
                        <td width="18%" class="t-align-left ff">TSC Formation Required</td>
                        <td width="82%" class="t-align-left">
                            <%if(isTscReq){%>
                            Yes
                            <%} else {%>
                            No
                            <%if (isCurrentUser_TEC_CP){%>
                            &nbsp;&nbsp;<a href="<%=request.getContextPath()%>/EvalConfigurationServlet?func=requesttsc&tenderId=<%=tenderId%>&refNo=<%=toextTenderRefNo%>&action=add">Make Request for TSC formation to PE</a>
                            <%}%>
                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%" class="t-align-left ff">Evaluation Type</td>
                        <td width="82%" class="t-align-left">
                            <%=strConfigType%>
                        </td>
                    </tr>
                    <%if("Team".equalsIgnoreCase(strConfigType)){%>
                     <tr >
                        <td valign="middle" class="t-align-left ff"><span class="ff">TEC Team Member :</span></td>
                        <td valign="top" class="t-align-left">
                            <%if(!"0".equalsIgnoreCase(strSelectedTECUserId)){%>
                                <%=strSelectedTECMemberNm%>
                            <%}%>
                        </td>
                    </tr>
                    <%}%>
                </table>
            </div>

                <%
// SET ALL THE STRINGS VARIABLES TO NULL
strEvalConfigId = null;
strConfigType=null;
strSelectedTECUserId=null;
strSelectedTECMemberNm=null;
strEvalStatus = null;
%>

        </div>

        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabConfig");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

