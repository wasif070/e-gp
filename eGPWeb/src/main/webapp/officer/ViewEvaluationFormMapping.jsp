<%--
    Document   : ViewEvaluationFormMapping
    Created on : Jan 21, 2011, 1:46:01 AM
    Author     : Karan
--%>
<%@page import="com.cptu.egp.eps.web.servicebean.MapEvalFormsSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Shared Forms for TSC</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>

          <%
MapEvalFormsSrBean srBean = new MapEvalFormsSrBean();

boolean isTenderer=false, isPEUser=false, isBankUser=false;
String strUserId="";
String currUserTypeId="";
String strActionType="";
String strCcomType="";
String tenderId="";

   HttpSession hs = request.getSession();
     if (hs.getAttribute("userId") != null) {
             strUserId = hs.getAttribute("userId").toString();
      }

   if(request.getParameter("tenderId")!=null){
            tenderId=request.getParameter("tenderId");
        }

   if(request.getParameter("comType")!=null){
            strCcomType=request.getParameter("comType");
        }

    if ( request.getParameter("action") != null) {
            if ( !"".equalsIgnoreCase(request.getParameter("action")) && !"null".equalsIgnoreCase(request.getParameter("action"))){
                   strActionType = request.getParameter("action");
               }
        }

   /* Start: CODE TO SET CURRENT USER TYPE */
      if (hs.getAttribute("userTypeId") != null) {
             currUserTypeId = hs.getAttribute("userTypeId").toString();
             if(currUserTypeId.equalsIgnoreCase("2")){
                isTenderer=true; // userType is Tenderer";
             }else if(currUserTypeId.equalsIgnoreCase("3")){
                isPEUser=true; // userType is Officer";
             }else if(currUserTypeId.equalsIgnoreCase("7")){
                isBankUser=true; // userType is ScheduleBank";
             }
       }
       /* End: CODE TO SET CURRENT USER TYPE */

      String strComType = "null";
                            if (request.getParameter("comType") != null && !"null".equalsIgnoreCase(request.getParameter("comType"))) {
                                strComType = request.getParameter("comType");
                            }

    // Variable tenderId is defined by u on ur current page.
    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
  %>

       <div class="dashboard_div">
  <!--Dashboard Header Start-->
  <%@include  file="../resources/common/AfterLoginTop.jsp" %>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1">
      View Shared Forms for TSC
      <span style="float: right;" ><a href="EvalComm.jsp?tenderid=<%=tenderId%>&comType=<%=strComType%>" class="action-button-goback">Go Back</a></span>
  </div>

  <%@include file="../resources/common/TenderInfoBar.jsp" %>
  <%//boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");%>
  <div>&nbsp;</div>

  <div class="tabPanelArea_1">
      <%-- Start: CODE TO DISPLAY MESSAGES --%>
      <%if (request.getParameter("msgId") != null) {
                      String msgId = "", msgTxt = "";
                      boolean isError = false;
                      msgId = request.getParameter("msgId");
                      if (!msgId.equalsIgnoreCase("")) {
                          if (msgId.equalsIgnoreCase("mapped")) {
                              msgTxt = "Mapping done successfully.";
                          } else if (msgId.equalsIgnoreCase("error")) {
                              isError = true;
                              msgTxt = "There was some error.";
                          } else {
                              msgTxt = "";
                          }
      %>
      <%if (isError) {%>
      <div class="responseMsg errorMsg" style="margin-bottom: 12px;" ><%=msgTxt%></div>
      <%} else {%>
      <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
      <%}%>
      <%}
                      msgId = null;
                      msgTxt = null;
                  }%>
      <%-- End: CODE TO DISPLAY MESSAGES --%>
<%tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");%>

<%-- Start: Common Evaluation Table --%>
<%@include file="/officer/EvalCommCommon.jsp" %>
<%-- End: Common Evaluation Table --%>

<%
List<SPTenderCommonData> PackageList =
        tenderCommonService.returndata("getlotorpackagebytenderid",tenderId,"Package,1");

if(!PackageList.isEmpty()){%>
<table width="100%" cellspacing="0" class="tableList_1" style="margin-bottom: 12px;">
    <tr>
        <td width="15%" class="t-align-left ff">Package No. :</td>
        <td class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
    </tr>
    <tr>
        <td class="t-align-left ff">Package Description :</td>
        <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
    </tr>
</table>
<%}
PackageList=null;
%>

<%if(isTenPackageWis){%>

<table width="100%" cellspacing="0" cellpadding="5" class="tableList_1">
    <tr>
        <th width="58%" class="ff">Form Name </th>
        <th width="24%">Committee</th>
    </tr>
    <%
        int frmCnt = 0;
        //List<SPTenderCommonData> formList = tenderCommonServiceFrm.returndata("getformnamebytenderidandlotid",thisTenderId,thisLotId);
        List<SPTenderCommonData> formList = srBean.getLotOrPackageForms(tenderId, null);

        if (!formList.isEmpty()) {
            for (SPTenderCommonData formname : formList) {
                if ("tscCom".equalsIgnoreCase(formname.getFieldName3())) {
                    frmCnt++;
    %>
    <tr
        <%if (Math.IEEEremainder(frmCnt, 2) == 0) {%>
        class="bgColor-Green"
        <%} else {%>
        class="bgColor-white"
        <%   }%>
        >
        <td class="ff"><p><strong><%=formname.getFieldName1()%></strong></p>
        </td>
        <td class="t-align-center">
            <%if ("teccom".equalsIgnoreCase(formname.getFieldName3())) {%>
            <label>TEC</label>
            <%} else if ("tsccom".equalsIgnoreCase(formname.getFieldName3())) {%>
            <label>TSC</label>
            <%} else {%>
            <label>N.A.</label>
            <%}%>
        </td>
    </tr>
    <%}
                }
            }
            if (frmCnt == 0) {%>
    <tr>
        <td class="t-align-left" colspan="2">No forms found!</td>
    </tr>
    <%}
    formList=null;
    %>
</table>

<%} else {%>
    <%
     int j = 0;

     for (SPTenderCommonData sptcd : tenderCommonService.returndata("geTenderLotsForEvaluationMapping", tenderId, null)){
         j++;
     %>
         <table width="100%" cellspacing="0" class="tableList_1">
        <tr>
            <th width="7%" class="t-align-center">Lot No.</th>
            <th width="67%" class="t-align-center">Lot Description</th>
            <th class="t-align-center" width="10%">Status</th>
        </tr>
         <tr
              <%if(Math.IEEEremainder(j,2)==0) {%>
                class="bgColor-Green"
            <%} else {%>
                class="bgColor-white"
            <%}%>
             >
             <td class="t-align-center"><%=sptcd.getFieldName1()%></td>
             <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
             <td class="t-align-center"><%=sptcd.getFieldName4()%></td>
         </tr>

         <tr>
             <td colspan="3">
             <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
            <tr>
                <th width="58%" class="ff">Form Name </th>
                <th width="24%">Select Committee</th>
            </tr>
        <%
    int frmCnt=0;

    List<SPTenderCommonData> formList = srBean.getLotOrPackageForms(tenderId, sptcd.getFieldName3());

    if (!formList.isEmpty()) {
        for (SPTenderCommonData formname : formList) {
            if("tscCom".equalsIgnoreCase(formname.getFieldName3())){
            frmCnt++;
        %>
            <tr
                 <%if(Math.IEEEremainder(frmCnt,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
                >
                <td class="ff"><p><strong><%=formname.getFieldName1()%></strong></p>
                </td>
                <td class="t-align-center">
                     <%if ("teccom".equalsIgnoreCase(formname.getFieldName3())) {%>
                    <label>TEC</label>
                    <%} else if ("tsccom".equalsIgnoreCase(formname.getFieldName3())) {%>
                    <label>TSC</label>
                    <%} else {%>
                    <label>N.A.</label>
                    <%}%>
                </td>
            </tr>
        <%
        }
        }
        } if (frmCnt==0) {%>
         <tr>
            <td class="t-align-left" colspan="2">No forms found!</td>
        </tr>
        <%}
        formList=null;
%>
        </table>
            </td>
         </tr>
          <tr ><td colspan="3">&nbsp;</td></tr>
</table>
          <%}%>


<%}%>


  </div>
  </div>

<%
// Set Objects and String variables to Null
srBean=null;

strUserId=null;
currUserTypeId=null;
strActionType=null;
strCcomType=null;
tenderId=null;

%>
    </div>
    <!--Dashboard Content Part End-->
    <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->

    </body>
</html>

