<%-- 
    Document   : LotWiseFormula
    Created on : Dec 23, 2010, 11:20:57 AM
    Author     : TaherT
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTables" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderColumns" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCells" %>
<%@page import="com.cptu.egp.eps.model.table.TblReportColumnMaster" %>
<%@page import="com.cptu.egp.eps.web.servicebean.ReportCreationSrBean" %>
<jsp:useBean id="frmView"  class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Form</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>        
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <script type="text/javascript" src="../resources/js/form/CommonValidation.js"></script>
        <script type="text/javascript" src="../resources/js/form/ReportFormula.js"></script>
        <script type="text/javascript" src="../resources/js/form/FormulaCalculation.js"></script>
        <script type="text/javascript" src="../resources/js/form/Add.js"></script>
        <script type="text/javascript" src="../resources/js/form/ConvertToWord.js"></script>
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <script  type="text/javascript">
            var ReportFormula = "",ReportFormulaToSave="";
            var arrIds = new Array();
            var ResultStr = "";
            var arrHeader = new Array();
            var OpenedBrace = 0, ClosedBrace = 0;
            var arrColIds = new Array();
            var arrDataTypesforCell = new Array();
            var ColDataType =  new Array();
            var ApplyTo = "";
            var TestReportFormula ="";
            var verified=true;
        </script>
        <script type="text/javascript">            
            $(function() {
                $('#SaveGov').click(function() {
                 if($("input[type='radio']").val()!=null){
                    if($("input[type='radio']:checked").length==0){
                        jAlert("Please select Governing column.","Report Alert", function(RetVal) {
                        });
                        return false;
                    }
                 }                 
                });
            });
            $(function() {
                $('#delRep').click(function() {
                 if($("input[type='checkbox']").val()!=null){
                    if($("input[type='checkbox']:checked").length==0){
                        jAlert("Please select Formula for delete.","Report Alert", function(RetVal) {
                        });
                        return false;
                    }
                 }
                });
            });
            $(function() {
                $('#ApplyTo').change(function() {
                    if($('#ApplyTo').val()==$('#inwordchk').val()){
                        $('#inToWords').removeAttr("checked");
                        $('#inToWords').attr("disabled", "disabled");
                    }else{
                        $('#inToWords').removeAttr("disabled");
                    }
                });
            });
            $(function() {                
                    $( "#dialog:ui-dialog" ).dialog( "destroy" );
                    $( "#dialog-form" ).dialog({
                        autoOpen: false,
                        resizable:false,
                        draggable:false,
                        height: 500,
                        width: 700,
                        modal: true,
                        buttons: {
                         "OK": function() {
                                 $(this).dialog("close");
                         }},
                        close: function() {
                            $(this).dialog("close");
                        }
                    });
                    $("#sfblock").click(function(){
                        $("#dialog-form").dialog("open");
                    });
            });
            
    </script>
    </head>
    <%
                ReportCreationSrBean rcsb = new ReportCreationSrBean();
                String tenderId = request.getParameter("tenderid");
                String repId = request.getParameter("repId");
                Object data[] = rcsb.getReportTabIdNoOfCols(repId);
                int reportTableId = Integer.parseInt(data[0].toString());                                
                Object formId[] = rcsb.getFormsForReport(repId).toArray();
                
                // Coad added by Dipal for Audit Trail Log.
                String reportType="TOR/TER";
                if(request.getParameter("reportType")!= null)
                {
                    reportType=request.getParameter("reportType");
                }
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                String idType="tenderId";
                int auditId=Integer.parseInt(request.getParameter("tenderid"));
                String auditAction=null;
                String moduleName=EgpModule.Tender_Notice.getName();
                String remarks="For "+reportType;



                if("Governing Column".equalsIgnoreCase(request.getParameter("SaveGov"))){
                    rcsb.updateGovernCol(request.getParameter("govCol"));
                    response.sendRedirect("LotWiseFormula.jsp?tenderid="+tenderId+"&repId="+repId+"&reportType="+reportType);
                }
                if("Save Formula".equalsIgnoreCase(request.getParameter("SaveFormula")))
                {
                    auditAction="Formula created for TOR/TER by PE";
                    
                    String formula=null;
                    String hideFormula=null;
                    if("on".equalsIgnoreCase(request.getParameter("inToWords"))){
                        formula=request.getParameter("ReportFormula");
                        formula=formula.substring(formula.indexOf("=")+1, formula.length());
                        formula="WORD("+formula+")";
                        hideFormula="WORD("+request.getParameter("hideReportFormula")+")";
                    }else{
                        formula=request.getParameter("ReportFormula");
                        formula=formula.substring(formula.indexOf("=")+1, formula.length());
                        hideFormula=request.getParameter("hideReportFormula");
                    }
                         
                    try
                    {
                         rcsb.addRepColFormula(String.valueOf(reportTableId), hideFormula.replace("estcost_", ""), request.getParameter("ApplyTo"),formula);
                    }
                    catch(Exception ex)
                    {
                        auditAction="Error in "+auditAction;
                    }
                    finally
                    {
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    }
                    formula=null;
                    hideFormula=null;
                    response.sendRedirect("LotWiseFormula.jsp?tenderid="+tenderId+"&repId="+repId+"&reportType="+reportType);
                }
                if("Delete".equalsIgnoreCase(request.getParameter("delRep")))
                {
                     auditAction="Formula deleted for TOR/TER by PE";
                     try
                    {
                          rcsb.delRepColFormula(request.getParameterValues("reportFormulaId"),request.getParameterValues("reportColId"));
                    }
                    catch(Exception ex)
                    {
                        auditAction="Error in "+auditAction;
                        ex.printStackTrace();
                    }
                    finally
                    {
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    }
                   
                    response.sendRedirect("LotWiseFormula.jsp?tenderid="+tenderId+"&repId="+repId+"&reportType="+reportType);
                }
    %>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>            
            <div class="contentArea_1">
            <div class="pageHead_1">
                Form View
                <span style="float: right;"><a class="action-button-goback" href="Notice.jsp?tenderid=<%=tenderId%>">Go back to Dashboard</a></span>
            </div>

            <%
                //pageContext.setAttribute("tenderId", tenderId);
            %><%--
            <%@include file="../resources/common/TenderInfoBar.jsp" %>--%>
            <!--Middle Content Table Start-->
            <b style="color: red;"><br/>Instructions :</b>
            <ol type="a" style="list-style-type: lower-alpha;color: red;font-weight: bold;margin-left: 20px;line-height: 16px;margin-top: 12px;">
                <li>Please must ensure that Grand Total of all BSR / Price Schedule/ Price Proposal forms (Form1, Form2, Form3 ...) is selected while preparing a formula except in case of LTM Works [e-PW2(B)].</li>
                <li>Once the report formula is prepared, please test it by entering numeric value and then save it.</li>
                <li>In case of query, please contact GPPMD Office </li>
                <li>
                    <span  class="ReadMore">
                        <a id="sfblock" href="javascript:void(0);" style="color: red;">Suggested Formula</a>
                    </span>
                </li>
            </ol>

            <form id="frmFormulaCreation" name="frmFormulaCreation" method="post" action="LotWiseFormula.jsp?tenderid=<%=tenderId%>&repId=<%=repId%>&reportType=<%=reportType%>">
            <%for(int z=0;z<formId.length;z++){%>            
                <table width="100%" cellspacing="0" class="tableList_1">
                    <%
                        List<TblTenderTables> tblTenderTables = frmView.getTenderTables(Integer.parseInt(formId[z].toString()));
                        for(TblTenderTables tbl : tblTenderTables){
                            java.util.List<com.cptu.egp.eps.model.table.TblTenderTables> tblInfo = frmView.getTenderTablesDetail(tbl.getTenderTableId());
                            short cols = 0;
                            short rows = 0;
                            String tableName = "";
                            if (tblInfo != null) {
                                if (tblInfo.size() >= 0) {
                                    tableName = tblInfo.get(0).getTableName();
                                    cols = tblInfo.get(0).getNoOfCols();
                                    rows = tblInfo.get(0).getNoOfRows();
                                }
                                tblInfo = null;
                            }
                            //cols = frmView.getNoOfColsInTable(tbl.getTenderTableId());
                            //rows = frmView.getNoOfRowsInTable(tbl.getTenderTableId(), (short) 1);

                            java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderColumns> tblColumnsDtl = frmView.getColumnsDtls(tbl.getTenderTableId(), true).listIterator();
                            java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderCells> tblCellsDtl = frmView.getCellsDtlsTestForm(tbl.getTenderTableId()).listIterator();

                            String colHeader = "";
                            byte filledBy = 0;
                            byte dataType = 0;
                            String colType = "";
                            boolean isTotalPresent=frmView.isTotalFormulaCreated(tbl.getTenderTableId());
                            java.util.HashMap<Integer, Integer> totalColId=frmView.getGTColumns(tbl.getTenderTableId());
                            //int totalColId[]={7};
                            short fillBy[] = new short[cols];
    %>
                            <table width="100%" cellspacing="0" class="tableView_1 t_space b_space">
                                <tr>
                                    <td width="10%" class="ff">Table Name : </td>
                                    <td width="90%"><%=tableName%></td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1" id="FormMatrix">
                                <tbody>
                                    <%
                                        String colHeads[]= new String[cols];
                                        String strDtType = "";
                                        for (short i = -1; i <= rows; i++) {
                                            if(i == 0){
                                    %>
                                     <tr id="ColumnRow">
                                    <%                                                
                                                for(short j=0;j<cols;j++){
                                                    if(tblColumnsDtl.hasNext()){
                                                        TblTenderColumns ttc = tblColumnsDtl.next();
                                                        colHeads[j]=ttc.getColumnHeader();
                                                        colHeader = ttc.getColumnHeader();
                                                        colType = ttc.getColumnType();
                                                        filledBy = ttc.getFilledBy();
                                                        dataType = ttc.getDataType();
                                                        ttc = null;
                                                    }
                                                    fillBy[j] = filledBy;
                                    %>
                                                <th id="addTD<%= j + 1 %>">
                                                    <%= colHeader %>
                                                </th>
                                    <%
                                                }
                                    %>
                                     </tr>
                                    <%
                                            }

                                            if(i > 0){
                                           if(i==rows && isTotalPresent){
                                               %>
                                               <tr id="TR<%=i%>">
                                    <%
                                                for(int j=1; j<=cols; j++){
                                                    if(totalColId.containsValue(j)){
                                                       if(tblCellsDtl.hasNext()){
                                                          TblTenderCells cells = tblCellsDtl.next();
                                                        %>
                                                        <td id="TD<%= i %>_<%= j + 1 %>" align="center"><input type="text" name="<%=cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()%>" id="<%=cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()%>"  class='formTxtBox_1' onclick="return BuildFormula(document.getElementById('frmFormulaCreation'),this,this);" nametodisplay="<%=tbl.getTableName()+"_"+colHeads[j-1]+"_"+i%>"/></td>
                                                        <%
                                                        }
                                                    }else{
                                                        if(tblCellsDtl.hasNext()){
                                                            tblCellsDtl.next();
                                                        }
                                                            %>
                                                        <td id="TD<%= i %>_<%= j + 1 %>" align="center"></td>
                                                        <%
                                                        }
                                                    }
                                                %>
                                                        </tr>
                                                        <%
                                                }else{
                                    %>
                                        <tr id="TR<%=i%>">

                                            <%
                                                int cnt = 0;
                                                for(int j=0; j<cols; j++){                                                    
                                                        String cellValue = "";
                                            %>
                                            <td id="TD<%= i %>_<%= j + 1 %>" align="center">
                                            <%
                                                        if(tblCellsDtl.hasNext()){
                                                            cnt++;
                                                            TblTenderCells cells = tblCellsDtl.next();
                                                            dataType = cells.getCellDatatype();
                                                            filledBy = cells.getCellDatatype();
                                                            cellValue = cells.getCellvalue();
                                                            /*if(dataType == 1){
                                                                //strDtType = "Small Text";
                                                            }
                                                            if(dataType == 2){
                                                                //strDtType = "Long Text";
                                                            }
                                                            if(dataType == 3){
                                                                //strDtType = "Money Positive";
                                                            }
                                                            if(dataType == 8){
                                                                //strDtType = "Money All";
                                                            }
                                                            if(dataType == 4){
                                                                //strDtType = "Numeric";
                                                            }*/
                                                            if(dataType==2){
                                                                strDtType="<textarea cols='10'rows='3' class='formTxtBox_1' name='"+cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()+"' id='"+cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()+"' nametodisplay='"+tbl.getTableName()+"_"+colHeads[j]+"_"+i+"'  onclick=\"return BuildFormula(document.getElementById('frmFormulaCreation'),this,this);\"></textarea>";
                                                            }else if(dataType==11){
                                                                strDtType="<input type='text' name='"+cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()+"' nametodisplay='"+tbl.getTableName()+"_"+colHeads[j]+"_"+i+"' id='"+cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()+"' class='formTxtBox_1'  onclick=\"return BuildFormula(document.getElementById('frmFormulaCreation'),this,this);\" btype=\"5m5\"/>";
                                                            }else{
                                                                strDtType="<input type='text' name='"+cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()+"' nametodisplay='"+tbl.getTableName()+"_"+colHeads[j]+"_"+i+"' id='"+cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()+"' class='formTxtBox_1'  onclick=\"return BuildFormula(document.getElementById('frmFormulaCreation'),this,this);\"/>";
                                                            }
                                                            if(fillBy[j] == 2){
                                                                //out.print("Fill By Tenderer - " + strDtType);
                                                                out.print(strDtType);
                                                            }else if(fillBy[j] == 1){
                                                                if(dataType==2 || dataType==1 || dataType==12){//
                                                                out.print(cellValue);
                                                                }else{
                                                                    out.print("<input type='text' name='"+cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()+"' id='"+cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()+"' class='formTxtBox_1' nametodisplay='"+tbl.getTableName()+"_"+colHeads[j]+"_"+i+"'  onclick=\"return BuildFormula(document.getElementById('frmFormulaCreation'),this,this);\"/>");
                                                                }
                                                            }else{
                                                                if(dataType==2 || dataType==1 || dataType==12){//
                                                                    if(dataType==1){
                                                                        out.print("Auto - Small Text");
                                                                    }else if(dataType==2){
                                                                        out.print("Auto - Long Text");
                                                                    }else if(dataType==12){
                                                                        out.print("Date");
                                                                    }
                                                                   //out.print("Auto");
                                                                } else{
                                                                    out.print("<input type='text'  nametodisplay='"+tbl.getTableName()+"_"+colHeads[j]+"_"+i+"' name='"+cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()+"' id='"+cells.getTblTenderTables().getTenderTableId()+"_"+cells.getColumnId()+"_"+cells.getCellId()+"' class='formTxtBox_1'  onclick=\"return BuildFormula(document.getElementById('frmFormulaCreation'),this,this);\"/>");
                                                                }
                                                            }
                                                            cells = null;
                                                        }
                                            %>
                                                    </td>
                                            <%
                                                }
                                            %>
                                        </tr>
                                    <%
                                                }
                                            }
                                        }
                                    %>
                                </tbody>
                            </table>                           
                    <%
                            tbl = null;
                            if (tblColumnsDtl != null) {
                                tblColumnsDtl = null;
                            }
                            if (tblCellsDtl != null) {
                                tblCellsDtl = null;
                            }
                        }
                    %>
                </table>
                <%
                    }
                    int noOfCols = Integer.parseInt(data[1].toString());
                    java.util.List<TblReportColumnMaster>  list = rcsb.getReportColumns(reportTableId);
                %>
                <br/>
                <table class="tableList_1" width="100%" cellspacing="0">
                <tbody><tr>
                                
                        <th colspan="<%=noOfCols + noOfCols%>" class="t-align-left ff"><%=data[3]%></th>
                    </tr>
                    <tr>
                        <%
                            List<Object[]> formList = rcsb.getReportFormula(String.valueOf(reportTableId));
                            boolean isGovSelected=false;
                            for (TblReportColumnMaster master : list) {
                                if(master.getGovCol().equals("yes")){
                                    isGovSelected=true;
                                }
                            }
                            for (TblReportColumnMaster master : list) {
                        %>
                        <th class="t-align-left ff">
                            <%=master.getColHeader()%>
                            <%
                                if(!isGovSelected){
                                    if (master.getFilledBy() == 2) {
                             %>
                             <br/><input type="radio" name="govCol" value="<%=master.getReportColId()%>" id="govCol"/>
                            <%
                                    }
                                }else{
                                    if(master.getGovCol().equals("yes")){
                                        out.print("<br/><span style='color:green;'>Governing Column</span>");
                                        out.print("<input type='hidden' value='"+master.getColumnId()+"' name='inwordchk' id='inwordchk'/>");
                                    }
                                }
                            %>
                        </th>
                        <%}%>
                    </tr>

                    <tr>
                        <%for (TblReportColumnMaster master : list) {%>
                        <td class="t-align-left"><%if (master.getFilledBy() == 1) {
                                    out.print("Company");
                                }/* else if (master.getFilledBy() == 2) {
                                    out.print("Government");
                                } */else if (master.getFilledBy() == 2) {
                                    out.print("<input type='text' class='formTxtBox_1' readonly name='gov_"+master.getColumnId()+"' id='gov_"+master.getColumnId()+"'/>");
                                } else if (master.getFilledBy() == 3) {
                                    out.print("Auto Number");
                                }else if (master.getFilledBy() == 4) {
                                    //out.print("<input type='text' class='formTxtBox_1' name='estcost_"+master.getColumnId()+"' id='estcost_"+rcsb.getReportLots(repId)+"' onclick='BuildFormula(document.getElementById(\"frmFormulaCreation\"),this,this);'  nametodisplay='Est_Cost'/>");
                                    out.print("<input type='text' class='formTxtBox_1' name='estcost_"+master.getColumnId()+"' id='estcost_"+rcsb.getCREstCost(tenderId, repId)+"' onclick='BuildFormula(document.getElementById(\"frmFormulaCreation\"),this,this);'  nametodisplay='Est_Cost'/>");
                                    //out.print("<input type='text' class='formTxtBox_1' name='estcost_"+master.getColumnId()+"' id='"+rcsb.getCREstCost(tenderId, repId)+"' onclick='BuildFormula(document.getElementById(\"frmFormulaCreation\"),this,this);'  nametodisplay='Est_Cost'/>");
                                }%></td>
                        <%}%>
                    </tr>
                    <tr>
                        <td colspan="<%=noOfCols + noOfCols%>" class="t-align-left formSubHead_1"><%=data[4]%></td>
                    </tr>
                </tbody></table>
                <br/>
                <table width="100%" border="0" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td style="text-align:center;">
                                <label class="formBtn_1"><input type="button" name="plus" id="plus" value="+" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                <label class="formBtn_1"><input type="button" name="minus" id="minus" value="-" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                <label class="formBtn_1"><input type="button" name="mul" id="mul" value="*" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                <label class="formBtn_1"><input type="button" name="divide" id="divide" value="/" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                <label class="formBtn_1"><input type="button" name="startBrace" id="startBrace" value="(" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                <label class="formBtn_1"><input type="button" name="endBrace" id="endBrace" value=")" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                <label class="formBtn_1"><input type="button" name="Percentage" id="Percentage" value="%" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                <label class="formBtn_1"><input type="button" name="CustomeNumber" id="CustomeNumber" value="Number" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);" /></label>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align:center;">
                                <label class="formBtn_1">
                                    <input type="button" name="clear" id="clear" value="Clear" onclick="return clearAll(document.getElementById('frmFormulaCreation'));" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; vertical-align: middle;">
                                <select name="ApplyTo" onchange="setFormulaTo(document.getElementById('frmFormulaCreation'), this);" id="ApplyTo" class="formTxtBox_1" style="width:180px;">
                                            <option value="0">-Select-</option>
                                            <%
                                                for(TblReportColumnMaster master : list){
                                                    if(master.getFilledBy()==2){
                                                        boolean flag=true;
                                                      for(Object[] objects : formList){
                                                          int objectid=Integer.parseInt(objects[3].toString());
                                                          if(objectid==master.getReportColId()){
                                                              flag=false;
                                                          }
                                                      }
                                                      if(flag){
                                            %>
                                            <option value="<%=master.getColumnId()%>"><%=master.getColHeader()%></option>
                                            <%}}}rcsb=null;data=null;list=null;%>
                                </select>
                            </td>
                            <td style="text-align:center;">
                                <textarea name="ReportFormula" rows="7" class="formTxtBox_1" id="ReportFormula" style="width:500px;" readonly ></textarea>
                                <input type="hidden" id="hideReportFormula" name="hideReportFormula"/>
                                <input type="hidden" id="hidReportFormula"/>
                            </td>
                            <td valign="middle" style="text-align:center; vertical-align: middle;">
                                <input type="checkbox" name="inToWords" id="inToWords"/> In Words<br /><br />
                                <label class="formBtn_1"><input type="button" name="Undo" value="Undo" onClick="UndoChange(document.getElementById('frmFormulaCreation'));" /></label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">
                                <label class="formBtn_1">
                                    <input type="button" name="TestFormula" id="TestFormula" value="Test Formula" onClick="testformula(document.getElementById('frmFormulaCreation'))"/>
                                    <input type="hidden" id="testedornot" value="n" />
                                </label>
                            </td>
                            <td style="text-align:center;">
                                <label class="<%if(!isGovSelected){out.print("formBtn_disabled");}else{out.print("formBtn_1");}%>">
                                    <input type="submit" name="SaveFormula" id="SaveFormula" value="Save Formula" onClick="return FormulaSave(document.getElementById('frmFormulaCreation'))" <%if(!isGovSelected){%>disabled<%}%>/>
                                    <input type="hidden" name="hiddenSubmit"  id="hiddenSubmit" value="" />
                                </label>
                            </td>
                            <td>
                                <label class="<%if(isGovSelected){out.print("formBtn_disabled");}else{out.print("formBtn_1");}%>">
                                    <input type="submit" name="SaveGov" id="SaveGov" value="Governing Column" <%if(isGovSelected){out.print("disabled");}%>/>
                                </label>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <div class="tableHead_1 t_space">Existing Formula(s)</div>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th style="text-align:left; width:4%;">Sl. No.</th>
                            <th style="text-align:left;">Apply To Column</th>
                            <th style="text-align:left;">Formula</th>
                            <th style="text-align:left; width:10%;">Delete</th>
                        </tr>
                        <%
                            int q=1;
                            for(Object[] formulaList : formList){
                         %>
                         <tr class="<%if(q%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                             <td><%=q%></td>
                             <td><%=formulaList[2]%></td>
                             <td><%=formulaList[1]%></td>
                             <td>
                                 <input type="checkbox" name="reportFormulaId" value="<%=formulaList[0]%>"/>
                                 <input type="hidden" value="<%=formulaList[3]%>" name="reportColId"/>
                             </td>
                         </tr>
                         <%q++;}%>
                          <tr>
                              <td colspan="4" class="t-align-center">
                                  <label class="<%if(formList.isEmpty()){out.print("formBtn_disabled");}else{out.print("formBtn_1");}%>">
                                      <input type="submit" name="delRep" id="delRep" value="Delete" <% if(formList.isEmpty()){%>disabled<%}%>/>
                                </label>
                              </td>
                          </tr>
                    </table>
                </form>
                </div>
                <div id="dialog-form" title="Formula for Tender/Proposal Opening Report (TOR) & Tender/Proposal Evaluation Report (TER/PER)">
                    <fieldset>
                        <table width="100%" cellspacing="0" border="1" class="tableList_1">
                            <tr>
                                <th colspan="5">Formula for Tender Opening Report (TOR)</th>
                            </tr>
                            <tr>
                                <th width="4%">Sl. No</th>
                                <th width="26%">Name of Column to be shown in the Report</th>
                                <th width="15%">Data Type</th>
                                <th width="15%">Formula</th>
                                <th width="40%">Remarks</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Sl. No.</td>
                                <td>Auto Number</td>
                                <td></td>
                                <td>This column will be used to Display Rank as L1, L2, ...</td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Name of the Tenderer</td>
                                <td>Company</td>
                                <td></td>
                                <td>Bidders/Suppliers/Contractors participated in the tender will be shown</td>
                            </tr>                            
                            <tr>
                                <td>3.</td>
                                <td>Quoted Amount (in Nu.)(X)</td>
                                <td>Auto</td>
                                <td>Summation of Grand Total of Each Form</td>
                                <td>
                                    Grand Total of each Priced BSR or Schedule of Items will be used as input for Quoted Amount.<br/>
                                    For E.g. there are 3 Forms namely
                                    <ol type="1" style="list-style-type: decimal;margin-left: 20px;line-height: 16px;margin-top: 12px;margin-bottom: 5px;">
                                        <li>Price and Delivery Schedule (Form1)</li>
                                        <li>Price and Delivery Schedule_1 (Form2)</li>
                                        <li>Price and Completion Schedule for related Services (Form3)</li>
                                    </ol>                                    
                                    The formula of Quoted Amount (in Nu.) = Form1_Grand Total + Form2_Grand Total + Form3_Grand Total
                                </td>
                            </tr>                            
                        </table>
                        <table width="100%" cellspacing="0" border="1" class="tableList_1 t_space">
                            <tr>
                                <th colspan="5">Formula for Tender Evaluation Report (TER/PER)</th>
                            </tr>
                            <tr>
                                <th width="4%">Sl. No</th>
                                <th width="26%">Name of Column to be shown in the Report</th>
                                <th width="15%">Data Type</th>
                                <th width="15%">Formula</th>
                                <th width="40%">Remarks</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Rank</td>
                                <td>Auto Number</td>
                                <td></td>
                                <td>This column will be used to Display Rank as L1, L2, ...</td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Name of the Bidder/Consultant</td>
                                <td>Company</td>
                                <td></td>
                                <td>Tenderers/Consultants participated in the tender will be shown</td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Official Cost Estimate (E)</td>
                                <td>Official Cost Estimate</td>
                                <td></td>
                                <td>Official Cost Estimate will be punched by PE</td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Quoted Amount (in Nu.) For ICB Tender Currency should be as decided by PE (X)</td>
                                <td>Auto</td>
                                <td>Summation of Grand Total of Each Form</td>
                                <td>
                                    Grand Total of each Priced BSR or Schedule of Items will be used as input for Quoted Amount.
                                    For E.g. there are 3 Forms namely
                                    <ol type="1" style="list-style-type: decimal;margin-left: 20px;line-height: 16px;margin-top: 12px;margin-bottom: 5px;">
                                        <li>Price and Delivery Schedule (Form1)</li>
                                        <li>Price and Delivery Schedule_1 (Form2)</li>
                                        <li>Price and Completion Schedule for related Services (Form3)</li>
                                    </ol>
                                    The formula of Quoted Amount (in Nu.) = Form1_Grand Total + Form2_Grand Total + Form3_Grand Total
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Deviation Amount (Y)</td>
                                <td>Auto</td>
                                <td>E-X</td>
                                <td>
                                    Official Cost Estimate - Quoted Amount
                                    <br/>
                                    Note : If the deviation amount is positive, then it is saving
                                 </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>% of Deviation (Z)</td>
                                <td>Auto</td>
                                <td>100 - X * 100 / E</td>
                                <td>Note : If the % of deviation is positive, then it is saving</td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>