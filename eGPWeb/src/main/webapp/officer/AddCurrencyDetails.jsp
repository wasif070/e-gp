<%--
    Document   : AddCurrencyDetails
    Created on : Sep 2, 2012, 11:58:08 AM
    Author     : Md. khaled Ben Islam
    Company    : Dohatec
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigureCurrencySrBean"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <%
            // Variable tenderId is defined by u on ur current page.
            pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Currency Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <!--jalert -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function checkCurrencyExchangeRate(currencyRateFieldId){
                $("#" + currencyRateFieldId).siblings().remove(".error");

                var exchangeRate = document.getElementById(currencyRateFieldId).value;
                if(isNaN(+exchangeRate) || parseFloat(parseFloat(exchangeRate).toFixed(7)) <= 0){ //maximum 6 zero after decimal point is valid (e.g. 0.00000001 is considered zero)
                    $('#' + currencyRateFieldId).after("<span class='error' style='color:#f00;'><br/>Invalid Exchange Rate<span>");
                    return false;
                }
                else {
                    return true;
                }
            }

            function validateAllRates(){
                var bAllRatesValid = true;
                $('.currency-rate').each(function () {
                   if(!checkCurrencyExchangeRate($(this).attr('id'))) {
                        bAllRatesValid = false;
                    }
                });

                /*  Dohatec Start    */
                if(bAllRatesValid == true)
                    {                    
                       bAllRatesValid = confirm("Be sure that Currency Conversion Factor is correct?");
                    }
                 /*  Dohatec End    */
                return bAllRatesValid;
            }
        </script>

       
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">
            <%TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            if(tenderCommonService1.getSBDCurrency(Integer.parseInt(request.getParameter("tenderid"))).equalsIgnoreCase("Yes")) {  %>
                <div class="pageHead_1">Currency Details <span style="float:right;"><a href="Notice.jsp?tenderid=<%=request.getParameter("tenderid")%>" class="action-button-goback">Go Back To Dashboard</a></span>
            <% } else { %>
                <div class="pageHead_1">Currency Details <span style="float:right;"><a href="OpenComm.jsp?tenderid=<%=request.getParameter("tenderid")%>" class="action-button-goback">Go Back To Dashboard</a></span>
            <% } %>
                </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                HttpSession htSession = request.getSession();
                int  iUserid = Integer.parseInt(htSession.getAttribute("userId").toString());
                int iCurrencyId = 0;
                ConfigureCurrencySrBean configureCurrencySrBean = new ConfigureCurrencySrBean();
                iCurrencyId = configureCurrencySrBean.getCurrencyId("BTN");
                int iTenderId = Integer.parseInt(request.getParameter("tenderid"));
                BigDecimal exchangeRate = new BigDecimal(1.00);
                boolean bSuccess = configureCurrencySrBean.updateTenderCurrencyExchangeRate(iTenderId, iCurrencyId, exchangeRate, iUserid);
            %>
            <div class="tabPanelArea_1 ">
                <form name="currencyInfo" method="post" action="<%= request.getContextPath() %>/TenderCurrencyConfigServlet">
                    <table id="tblCurrency" width="100%" cellspacing="0" class="tableList_1">
                        <thead>
                            <tr>
                                <th width="15%" class="t-align-center">No.</th>
                                <th width="43%" class="t-align-center">Currency Name</th>
                                <th width="42%" class="t-align-center">Rates (In Nu.)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                String currencyRows = ""; //will hold the result
                                String addedCurrencies = ""; //comma separated IDs of Currencies already added to the tender
                                String strInputFieldId = "";

                                TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                                List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
                                listCurrencyObj = tenderCurrencyService.getCurrencyTenderwise(iTenderId);

                                if(listCurrencyObj.size() > 0){
                                    int rowNo = 0;
                                    for(Object[] obj :listCurrencyObj){
                                        rowNo +=1;
                                        currencyRows+= "<tr>";
                                        currencyRows+= "<td width='15%' class='t-align-center'>" + rowNo + "</td>";
                                        currencyRows+= "<td width='43%' class='t-align-left'>" + obj[3] + " - " + obj[0] + "</td>";  //Obj[0] holds Currency Name, Obj[1] holds Currency Rate, Obj[2] holds Currency ID, obj[3] holds Cyrrency Short Name (e.g. BTN)
                                        strInputFieldId = "currencyRate_" + obj[2];
                                        if(obj[3].equals("BTN") || obj[3].equals("bdt")){//obj[3] holds the CurrencyShortName (e.g. BTN), BTN is the default currency and BTN must not allowed to be deleted
                                            currencyRows+= "<td width='42%' class='t-align-center'><input id='" + strInputFieldId + "' " + " name='" + strInputFieldId + "' ' class='formTxtBox_1 currency-rate' type='text' value='"+ obj[1] + "' readonly='readonly' onblur='checkCurrencyExchangeRate(&apos;"+ strInputFieldId +"&apos;);'></td>";
                                        }
                                        else {
                                            currencyRows+= "<td width='42%' class='t-align-center'><input id='" + strInputFieldId + "' " + " name='" + strInputFieldId + "' ' class='formTxtBox_1 currency-rate' type='text' value='"+ obj[1] + "' onblur='checkCurrencyExchangeRate(&apos;"+ strInputFieldId +"&apos;);'></td>";
                                        }

                                        currencyRows+= "</tr>";
                                        addedCurrencies += obj[2] + ",";
                                    }
                                }
                                out.print(currencyRows);
                            %>
                        </tbody>
                    </table>
                    <table width="100%" cellspacing="10" cellpadding="0" border="0" class="formStyle_1 t_space">
                        <tbody><tr>
                            <td align="center" colspan="4">
                                <label class="formBtn_1">
                                    <input type="submit" value="Submit" id="btnsubmit" name="submit" onclick="return validateAllRates();">
                                </label>&nbsp;&nbsp;
                                <input type="hidden" name="tenderid" value="<%=request.getParameter("tenderid")%>" id="tenderid">
                                <input type="hidden" name="action" value="updateCurrencyExchangeRate" id="tenderid">
                                <input type="hidden" name="count" value="<%=listCurrencyObj.size()%>" id="count">
                                <input type="hidden" name="addedCurrencies" value="<%= addedCurrencies %>" id="addedCurrencies">
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <div>&nbsp;</div>

        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

    </body>
</html>