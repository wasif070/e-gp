<%-- 
    Document   : EvalBidderMarks
    Created on : Mar 17, 2011, 4:45:51 PM
    Author     : TaherT
--%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Consultant's Evaluation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%            
            String tendID = request.getParameter("tenderId");
            String bidderID = request.getParameter("uId");
            EvalServiceCriteriaService criteriaService = (EvalServiceCriteriaService)AppContext.getSpringBean("EvalServiceCriteriaService");            
            CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
            boolean isREOI = false;
            boolean isService = false;
            if(commonService.getEventType(String.valueOf(tendID)).toString().equals("REOI")){
                isREOI = true;
            }
            if(commonService.getProcNature(String.valueOf(tendID)).toString().equals("Services")){
                isService = true;
            }
            String cpuserId = null;
            String cppuserId = null;
            CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            List<SPCommonSearchDataMore> memusersId = dataMoreService.geteGPDataMore("getMemUserIdMadeEval",tendID);
            if(false && memusersId!=null && !memusersId.isEmpty()){
                cpuserId = memusersId.get(0).getFieldName1();
                cppuserId = memusersId.get(0).getFieldName2();
            }else{
                cpuserId = session.getAttribute("userId").toString();
                cppuserId = session.getAttribute("userId").toString();
            }
            String memUid = request.getParameter("memUid");
            if(memUid!=null && !"0".equals(memUid)){
                cpuserId = memUid;
            }            
            String hideHTML = isREOI ? " style='display: none;' " : " ";
        %>
        <div class="tabPanelArea_1">
<!--            <div class="pageHead_1">Consultant's Evaluation<span style="float: right;"><a class="action-button-goback" href="Evalclarify.jsp?tenderId=< %=tendID%>&st=cl&comType=TEC">Go Back to Dashboard</a></span>-->
<div class="pageHead_1">Consultant's Evaluation<span style="float: right;"><a class="action-button-goback" href="<%=request.getHeader("referer")%>">Go Back to Dashboard</a></span>
            </div>
            <%
                        pageContext.setAttribute("tenderId", tendID);
         %>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <table class="tableList_1 t_space" cellspacing="0" width="33%">
                <tr>
                    <td width="15%">Tender ID :</td>
                    <td width="15%"><%=tendID%></td>
                </tr>
                <tr>
                    <td>Name of Consultant :</td>
                    <td><%=commonService.getConsultantName(bidderID)%></td>
                </tr>
                <tr>
                    <td>Name of PEC Member :</td>
                    <td><%=objUserName%></td>
                </tr>
            </table>
            <table class="tableList_1 t_space" cellspacing="0" width="100%" id="marksTab">
                <tr>
                    <!--                        <th class="t-align-center">Select</th>-->
<!--                    <th class="t-align-center">Form ID</th>-->
<!--                    <th class="t-align-center">Bid Id(remove)</th>-->
                    <th class="t-align-center">Form Name</th>
                    <th class="t-align-center">Criteria</th>
                    <th class="t-align-center">Sub Criteria</th>
                    <th <%=hideHTML%> class="t-align-center">Points</th>
                    <th class="t-align-center">Assigned</th>
                    <th <%=hideHTML%> class="t-align-center">Rated</th>
                    <th  <%=hideHTML%> class="t-align-center">Scored</th>
                </tr>
                <%
                    //0 esd.maxMarks ,1 esd.actualMarks,2 esd.ratingWeightage,3 esd.ratedScore,
                    //4 tf.tenderFormId,5 tf.formName,6 esc.subCriteria,7 esd.bidId,8 esc.mainCriteria
                    int cnt=0;
                    double mainCol4=0;
                    double mainCol6=0;
                    for(Object formId : criteriaService.getBiddersFormId(bidderID, tendID)){
                        List<Object[]> list = criteriaService.getEvalBidderMarks(bidderID,cpuserId, tendID,formId.toString());
                        double col4=0;
                        double col6=0;
                        for(Object[] data : list){
                %>
                <tr>                    
                    <!--Form IDs td class="t-align-center">< %
                    if(!("Adequacy For The Assignment".equals(data[6].toString())||
                            "Time With Consultant".equals(data[6].toString())
                            ||"Experience In Region & Language".equals(data[6].toString()))){
                        out.print(data[4]+"<input type='hidden' id='formId_"+cnt+"' value='"+data[4]+"'>");cnt++;}else{out.print("");}
                                       %></td-->
<!--                    <td Bid ids><!%=data[7]%></td>-->
                    <td>
                    <%if(true || !("Work plan".equals(data[6].toString())||
                            "Organization and Staffing".equals(data[6].toString()))){
                        out.print(data[5]);}else{out.print("");}
                    %></td>
<!--                    <td><!%=data[7]%></td>-->
                    <td><%
                    if(true || !("Work plan".equals(data[6].toString())||
                            "Organization and Staffing".equals(data[6].toString()))){
                        out.print(data[8]+"<input type='hidden' id='formId_"+cnt+"' value='"+data[4]+"'>");cnt++;}else{out.print("");}
                                       %></td>
                    <td><%=data[6]%></td>
                    <td <%=hideHTML%> class="t-align-center" opt="hide"><%out.print(data[0]);col4+=(data[0].toString().equals("")?0.00: Double.parseDouble(data[0].toString()));%></td>
                    <td class="t-align-center"><%=data[2]%></td>
                    <td <%=hideHTML%> class="t-align-center"><%out.print(data[3]);%></td>
                    <td <%=hideHTML%> class="t-align-center"><%out.print(data[1]);col6+=(data[1].toString().equals("")?0.00:Double.parseDouble(data[1].toString()));%></td>
                </tr>                
                <%
                        }
                        int count = criteriaService.getBidIdFrmFormId(bidderID, formId.toString()).size();
                        if(count>1){
                           col4 = col4/count;
                           col6 = col6/count;
                           mainCol4+=col4;
                           mainCol6+=col6;
                           //out.print("<tr"+hideHTML+"><th></th><th></th><th></th><th>"+col4+"</th><th></th><th>"+(col6/col4)+"</th><th>"+col6+"</th></tr>");
                        }else{
                            mainCol4+=col4;
                            mainCol6+=col6;
                        }
                    }
                    out.print("<tr"+hideHTML+"><th></th><th></th><th align='right'>Total : </th><th>"+new BigDecimal(String.valueOf(mainCol4)).setScale(2,0)+"</th><th></th><th></th><th>"+new BigDecimal(String.valueOf(mainCol6)).setScale(2,0)+"</th></tr>");
                %>
                <input type="hidden" id="count" value="<%=cnt%>">
            </table>
            <%if(isService && !isREOI){%>
            <script type="text/javascript">
                $('#marksTab td[opt="hide"]').each(function() {
                    var ctd = this.innerHTML;
                    if(ctd=='0'){
                        $(this).parent().hide();
                    }                    
                });
            </script>
            <%}%>
        </div>        
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>
