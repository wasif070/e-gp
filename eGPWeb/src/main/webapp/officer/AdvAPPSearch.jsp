<%--
Document   : Advance APP Search
Created on : Oct 23, 2010, 6:13:00 PM
Author     : Sanjay
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Advance APP Search</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmAdvAPPSearch").validate({
                    rules: {
                        ministry: {required: true},
                        procurenature: {required: true},
                        budgettype: {required: true},
                        value: {digits: true}
                    },
                    messages: {
                        ministry: { required: "<div class='reqF_1'>Please Select Ministry.</div>"},
                        procurenature: { required: "<div class='reqF_1'>Please Select Procurement Category.</div>"},
                        budgettype: { required: "<div class='reqF_1'>Please Select Budget Type.</div>"},
                        value: { digits: "<div class='reqF_1'>Please Enter Numerals Only.</div>"}
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp">
                            <jsp:param name="hdnUserType" value="<%=userType%>"/>
                        </jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <div class="t_space">
                                            <div class="pageHead_1">Advance APP Search</div>
                                            <div>&nbsp;</div>
                                            <form method="POST" id="frmAdvAPPSearch" action="">
                                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                                    <tr>
                                                        <td class="ff">Ministry : <span>*</span></td>
                                                        <td colspan="3"><select name="ministry" class="formTxtBox_1" id="cmbMinistry" style="width:200px;">
                                                                <option value="">- Select Ministry -</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">Division :</td>
                                                        <td colspan="3"><select name="division" class="formTxtBox_1" id="cmbDivision" style="width:200px;">
                                                                <option value="Divison 1">Divison 1</option>
                                                                <option value="Divison 2">Divison 2</option>
                                                                <option value="Divison 3">Divison 3</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">Organization :</td>
                                                        <td colspan="3"><select name="organization" class="formTxtBox_1" id="cmbOrganization" style="width:200px;">
                                                                <option value="Organization 1">Organization 1</option>
                                                                <option value="Organization 2">Organization 2</option>
                                                                <option value="Organization 3">Organization 3</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">Procure Entity :</td>
                                                        <td colspan="3"><select name="procureentity" class="formTxtBox_1" id="cmbProcureEntity" style="width:200px;">
                                                                <option value="Procure Entity 1">Procure Entity 1</option>
                                                                <option value="Procure Entity 2">Procure Entity 2</option>
                                                                <option value="Procure Entity 3">Procure Entity 3</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr colspan="3">
                                                        <td class="ff">CPV Category :</td>
                                                        <td><a href="#" class="action-button-anchor">Select CPV</a> </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="ff">Project Name :</td>
                                                        <td width="202" colspan="3"><select name="projectname" class="formTxtBox_1" id="cmbProjectName" style="width:200px;">
                                                                <option value="Project Name 1">Project Name 1</option>
                                                                <option value="Project Name 2">Project Name 2</option>
                                                                <option value="Project Name 3">Project Name 3</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="133" class="ff">Financial Year :</td>
                                                        <td width="211"><select name="financialyear" class="formTxtBox_1" id="cmbFinancialYear" style="width:100px;">
                                                                <option value="2010-2011" selected="selected">2010-2011</option>
                                                                <option value="2009-2010">2009-2010</option>
                                                                <option value="2008-2009">2008-2009</option>
                                                            </select>
                                                        </td>
                                                        <td width="120" class="ff">Budget Type : <span>*</span></td>
                                                        <td><select name="budgettype" class="formTxtBox_1" id="cmbBudgetType" style="width:150px;">
                                                                <option value="" selected="selected">- Select Budget Type -</option>
                                                                <option value="Development">Development</option>
                                                                <option value="Revenue">Revenue</option>
                                                                <option value="Own fund">Own fund</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">Procurement Category : <span>*</span></td>
                                                        <td><select name="procurenature" class="formTxtBox_1" id="cmbProcureNature" style="width:200px;">
                                                                <option value="" selected="selected">- Select Procurement Category -</option>
                                                                <option value="Goods">Goods</option>
                                                                <option value="Works">Works</option>
                                                                <option value="Service">Service</option>
                                                            </select>
                                                        </td>
                                                        <td class="ff">Procurement Type:</td>
                                                        <td><select name="procuretype" class="formTxtBox_1" id="cmbProcureType" style="width:200px;">
                                                                <option value="NCT">NCB</option>
                                                                <option value="ICT">ICB</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">App ID :</td>
                                                        <td><input name="appid" type="text" class="formTxtBox_1" id="txtAppId" style="width:200px;" /></td>
                                                        <td class="ff">Letter Ref. No. :</td>
                                                        <td><input name="appcode" type="text" class="formTxtBox_1" id="txtAppCode" style="width:200px;" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">Package No. :</td>
                                                        <td colspan="3"><input name="packageno" type="text" class="formTxtBox_1" id="txtPackageNo" style="width:200px;" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">Package Est. Cost :</td>
                                                        <td><select name="packagecost" class="formTxtBox_1" id="cmbPackageCost" style="width:200px;">
                                                                <option value="=">=</option>
                                                                <option value="&lt;=">&lt;=</option>
                                                                <option value="&gt;=">&gt;=</option>
                                                                <option value="Between">Between</option>
                                                            </select>
                                                        </td>
                                                        <td class="ff">Value :</td>
                                                        <td><input name="value" type="text" class="formTxtBox_1" id="txtValue" style="width:200px;" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">&nbsp;</td>
                                                        <td colspan="3"><label class="formBtn_1">
                                                                <input type="submit" name="submit" id="btnSubmit" value="Search" /></label></td>
                                                    </tr>
                                                </table>

                                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                    <tr>
                                                        <th class="t-align-center">Sl. <br/> No.</th>
                                                        <th class="t-align-center">APP Id <br />
				Letter Ref. No.</th>
                                                        <th class="t-align-center">Ministry / Division &amp; Organization <br /> Procuring Entity</th>
                                                        <th class="t-align-center">Dzongkhag / District</th>
                                                        <th class="t-align-center">Procurement Category Project Name</th>
                                                        <th class="t-align-center">Package No &amp; Description</th>
                                                        <th class="t-align-center">Est. Cost Procurement Method</th>
                                                    </tr>
                                                    <tr>
                                                        <td class="t-align-center"></td>
                                                        <td class="t-align-center"></td>
                                                        <td class="t-align-center"></td>
                                                        <td class="t-align-center"></td>
                                                        <td class="t-align-center"></td>
                                                        <td class="t-align-center"><a href="#" title="Link Name">Link</a></td>
                                                        <td class="t-align-center"></td>
                                                    </tr>
                                                </table>
                                                <div>&nbsp;</div>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
<script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabApp");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
