<%--
    Document   : EvalProcessInclude
    Created on : Jun 18, 2011, 12:22:38 PM
    Author     : TaherT
--%>
<%--
Modified almost every function and add parameter for re-evaluation by dohatec
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvaluationMatrix"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderOpening"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SpgetCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <% String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <body>
    <%
                //String tenderid_eval = pageContext.getAttribute("tenderid");
        CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
        CommonSearchService commonSearchService = (CommonSearchService)AppContext.getSpringBean("CommonSearchService");
        
        String tenderid_eval = request.getParameter("tenderid"); //To be comment and above to be uncomment
        /* Dohatec Start*/
        String activity_id = "13";
        String object_id = tenderid_eval;
        String child_id = tenderid_eval;
        String event_id = "12";
        String from_action = "eval";
        String msgUrl = "";
        boolean isTERSentToAA = false;
        boolean isClariAsked = false;
        boolean isTERSentToReviewer = false;
         boolean isClariAnswered = false;

         String aaID = null;
        Boolean evaluationWorkflowCreated = false;
        List<SPCommonSearchDataMore> evaluationWorkflowList = dataMore.getEvalProcessInclude("isEvaluationWorkFlowCreated",tenderid_eval,session.getAttribute("userId").toString(), "1");
        if(evaluationWorkflowList !=null && (!evaluationWorkflowList.isEmpty())){
            evaluationWorkflowCreated = true;
        }
          boolean NonResponsiveBidder = true;
        /*Dohatec End*/
           // Start for Sign Status

           String  strReport1SingStatus = "0";
           String  strReport2SingStatus = "0";
            String  strReport1SingStatusLTM = "0";
           String  strReport2SingStatusLTM = "0";
           String  strReport3SingStatus = "0";
           String  strReport4SingStatus = "0";

           CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
             // End


        boolean isCommCP = false;
        boolean isCommMem = false;
        String strProcurementNature = request.getParameter("pnature");
        String strComType = request.getParameter("comtype");
        int evalCount = Integer.parseInt(request.getParameter("evalCount").toString());
        String strAllowFinalResponseProcess = request.getParameter("allowFinalResponseProcess");
        List<SPCommonSearchDataMore> commCP = dataMore.getEvalProcessInclude("evaluationMemberCheck", tenderid_eval, session.getAttribute("userId").toString(), "1");
        List<SPCommonSearchDataMore> commMem = dataMore.getEvalProcessInclude("evaluationMemberCheck", tenderid_eval, session.getAttribute("userId").toString(), "0");
        if (commCP != null && (!commCP.isEmpty()) && (!commCP.get(0).getFieldName1().equals("0"))) {
            isCommCP = true;
        }
        if (commMem != null && (!commMem.isEmpty()) && (!commMem.get(0).getFieldName1().equals("0"))) {
            isCommMem = true;
        }
        commCP=null;
        commMem=null;
        if(isCommCP || isCommMem){
        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        String pMethod = commonService.getProcMethod(tenderid_eval).toString();
        String eventType = commonService.getEventType(tenderid_eval).toString();
        boolean is2Env = false;
        boolean isT1L1 = false;
        boolean isT1 = false;
        List<SPCommonSearchDataMore> envDataMores = dataMore.getEvalProcessInclude("GetTenderEnvCount", tenderid_eval, null, null);
        if(envDataMores!=null && (!envDataMores.isEmpty())){
            if(envDataMores.get(0).getFieldName1().equals("2")){
                is2Env = true;
            }
            if(envDataMores.get(0).getFieldName2().equals("3")){
                //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                isT1L1 = true;
            }
            if(envDataMores.get(0).getFieldName2().equals("1")){
                //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                isT1 = true;
            }
        }
        boolean isPerfSecurityAvail = false;
        String procNature = commonService.getProcNature(tenderid_eval).toString();
        EvaluationMatrix matrix = new EvaluationMatrix();
        boolean[] cases = matrix.getEvaluationCase(procNature, eventType, pMethod, is2Env);
        boolean isCase1 = cases[0];
        boolean isCase2 = cases[1];
        boolean isCase3 = cases[2];
        matrix = null;
        cases = null;
        boolean isSentToAADecryptAll=isCase3;
        //Below code is to activate performance security for CP
        //Just uncomment it and also  JS fn-chkPrfAlert
        /*TenderSrBean tenderSrBean = new TenderSrBean();
        List<Object[]> os = tenderSrBean.getConfiForTender(Integer.parseInt(tenderid_eval));
        if(os!=null && !os.isEmpty() && "yes".equalsIgnoreCase(os.get(0)[1].toString())){
            isPerfSecurityAvail = true;
        }
        os=null;
        tenderSrBean=null;*/
        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        String repLabel = null;
        if (procNature.equals("Services")) {
            repLabel = "Proposal";
        } else {
            repLabel = "Tender";
        }

        String evalType = request.getParameter("stat");


        List<SPCommonSearchDataMore> getreportID = null;
        //String reportID = null;
        String reportID = "0";// commented by dohatec for PQ WORKS tender

        ReportCreationService creationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");

        List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("getProcurementNatureForEval", tenderid_eval, null);
        if (!tenderLotList.isEmpty()) {
            /*****************************************
            For Package Wise*******************
        
             *****************************************/
            if ("Proposal".equalsIgnoreCase(repLabel)) {
                boolean isAllBidderFail = false;
                List<SPTenderCommonData> packageList = tenderCommonService.returndata("getlotorpackagebytenderid",
                        tenderid_eval,
                        "Package,1");
             
                if (!packageList.isEmpty()) {
                    boolean isterdisp = false;//for displaying TER's and Technical Evaluation Report Lot Wise
                    List<SPCommonSearchDataMore> isFinalize = dataMore.getEvalProcessInclude("isFinalizeDoneForLot", tenderid_eval,"0", null);
                    if(isFinalize!=null && (!isFinalize.isEmpty()) && isFinalize.get(0).getFieldName1().equals("1")){
                        isterdisp = true;
                    }

                    /*boolean isTER1Config = false;
                    boolean isTER2Config = false;
                    boolean isTER1ConfigMem = false;
                    boolean isTER2ConfigMem = false;*/
                    boolean isTER1Sign = false;
                   // boolean isTER2Sign = false;
                    boolean isTER1Notified = false;
                    //boolean isTER2Notified = false;
                    boolean isCase2AASent = false;
                    boolean isCRFormulaMadeTER = false;
                    List<SPCommonSearchDataMore> AllBOQDecrypted = null;
                    boolean isAllBOQDecrypted = false;
                    if((isCase2 || isCase3)){
                       AllBOQDecrypted =  dataMore.getEvalProcessInclude("isAllBOQDecrypted", tenderid_eval, "0", null);
                       if(AllBOQDecrypted!=null && (!AllBOQDecrypted.isEmpty()) && AllBOQDecrypted.get(0).getFieldName1().equals("1")){
                           isAllBOQDecrypted = true;
                       }
                    }
                    %>
                   <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td width="20%" class="t-align-left ff">Package No :</td>
                        <td width="80%" class="t-align-left"><%=packageList.get(0).getFieldName1()%></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Package Description :</td>
                        <td class="t-align-left"><%=packageList.get(0).getFieldName2()%></td>
                    </tr>
                    <%if(isCommCP || (isCommMem && isterdisp)){%>
                    <tr>
                        <td class="t-align-left ff">Finalize Evaluation Status :</td>
                        <td class="t-align-center">
                            <jsp:include page="CPEvalReport.jsp">
                                    <jsp:param name="tenderid" value="<%=tenderid_eval%>"/>
                                    <jsp:param name="pnature" value="<%=strProcurementNature%>"/>
                                    <jsp:param name="comtype" value="<%=strComType%>"/>
                                    <jsp:param name="lotId" value="0"/>
                                    <jsp:param  name="isCommCP" value="<%=isCommCP%>"/>
                                    <jsp:param name="allowFinalResponseProcess" value="<%=strAllowFinalResponseProcess%>"/>
                             </jsp:include>
                        </td>
                    </tr>
                    <tr>
                            <th>Reports</th>
                            <th>Action</th>
                    </tr>
                    <%if(isCommCP && isterdisp && !eventType.equals("REOI")){%>
                    <tr>
                        <td class="t-align-left ff">
                            Technical Evaluation Report
                        </td>
                        <td class="t-align-left">
                            <a href="TenderersCompRpt.jsp?tenderid=<%=tenderid_eval%>&st=rp&comType=<%=strComType%>&lotId=0">View</a>
                        </td>
                    </tr>
                    <%
                    }}
                    if(isterdisp){
                        /*if (creationService.evalRepCount(tenderid_eval, "0", evalType, "TER1","tecRole='cp'") != 0) {
                            isTER1Config = true;
                        }
                        if (creationService.evalRepCount(tenderid_eval, "0", evalType, "TER2","tecRole='cp'") != 0) {
                            isTER2Config = true;
                        }
                        if (creationService.evalRepCount(tenderid_eval, "0", evalType, "TER1","tecRole='m'") != 0) {
                            isTER1ConfigMem = true;
                        }
                        if (creationService.evalRepCount(tenderid_eval, "0", evalType, "TER2","tecRole='m'") != 0) {
                            isTER2ConfigMem = true;
                        }*/
                        List<SPCommonSearchDataMore> TER1Notified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, "0", "0");
                       // List<SPCommonSearchDataMore> TER2Notified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, "0", "ter2","0");
                        if(TER1Notified!=null && (!TER1Notified.isEmpty()) && TER1Notified.get(0).getFieldName1().equals("1")){
                            isTER1Notified = true;
                        }
                       // if(TER2Notified!=null && (!TER2Notified.isEmpty()) && TER2Notified.get(0).getFieldName1().equals("1")){
                      //      isTER2Notified = true;
                      //  }
                        List<SPCommonSearchDataMore> TER1Sign = dataMore.getEvalProcessInclude("isEvalReportSigned", tenderid_eval, "0", "ter1","0",String.valueOf(evalCount));
                        List<SPCommonSearchDataMore> TER2Sign = dataMore.getEvalProcessInclude("isEvalReportSigned", tenderid_eval, "0", "ter2","0",String.valueOf(evalCount));
                        if (!TER1Sign.isEmpty() && !TER1Sign.get(0).getFieldName1().equals("0")) {
                            isTER1Sign = true;
                        }
                      /*  if (!TER2Sign.isEmpty() && !TER2Sign.get(0).getFieldName1().equals("0")) {
                            isTER2Sign = true;
                        }*/
                        TER1Sign=null;
                        //TER2Sign=null;
                        %>
                        <tr>
                            <td class="t-align-left ff"><%=repLabel%> Evaluation Report 1</td>
                            <td>
                                <%
                                            String viewSignLabel="View and Sign";
                                            List<SPCommonSearchDataMore> viewSignList = dataMore.getEvalProcessInclude("isTERSignedLabel", tenderid_eval,"0", "TER1", session.getAttribute("userId").toString(),"0");
                                            if(!viewSignLabel.isEmpty()){
                                                viewSignLabel = viewSignList.get(0).getFieldName1();
                                            }
                                             List<SPCommonSearchDataMore> listReportSingStatus1 = commonSearchDataMoreService.getEvalProcessInclude("getTenderReportSignStatus", tenderid_eval, "PEC" , "TER1", "0", "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            if(!listReportSingStatus1.isEmpty()){
                              strReport1SingStatus = listReportSingStatus1.get(0).getFieldName1();
                        }
                               %>
                                <%if(isCommCP){%>
                                    <!--%if(isTER1ConfigMem){%>
                                    <a href="< %=request.getContextPath()%>/officer/ViewTER.jsp?tenderid=< %=tenderid_eval%>&lotId=0&type=1&stat=< %=evalType%>" target="_blank">View Members Evaluation</a>
                                    &nbsp;|&nbsp;
                                    < %}%>
                                    < %if(!isTER1Sign){%>
                                        <a href="< %=request.getContextPath()%>/officer/TER1.jsp?tenderid=< %=tenderid_eval%>&lotId=0&config=y&stat=< %=evalType%>&sign=y">Configure</a>
                                    < %}% >
                                    < %if(isTER1Config){%-->
                                        <%if(!isTER1Sign){%>
                                            <%if(!isTER1Notified){%>
                                            <a href="<%=request.getContextPath()%>/EvaluationServlet?tenderid=<%=tenderid_eval%>&lotId=0&action=notifymail&rp=ter1" onclick="return confirm('Do you really want to Notify Members?')">Notify Members to Sign the Report</a>
                                            <%}else{out.print("Members Notified");}%>
                                            &nbsp;|&nbsp;
                                        <%}%>
                                        <a href="<%=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%=tenderid_eval%>&lotId=0&isview=y&stat=<%=evalType%>&sign=y&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                                             <b id="SignStatus1" style="color:red">
                                             <%  if(!"0".equalsIgnoreCase(strReport1SingStatus)){%>
                                             <%=  " ("+strReport1SingStatus+")" %>
                                             <%}%>  </b>
                                        <!--%}%-->
                                <%}%>
                                <%if(isCommMem){%>
                                    <!--%if(isTER1Config){%-->
                                        <a href="<%=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%=tenderid_eval%>&lotId=0&isview=y&stat=<%=evalType%>&sign=y&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                                                     <b id="SignStatus1" style="color:red">
                                             <%  if(!"0".equalsIgnoreCase(strReport1SingStatus)){%>
                                             <%=  " ("+strReport1SingStatus+")" %>
                                             <%}%>  </b>
                                        <!--%}else{out.print("<div class='responseMsg noticeMsg'>Report has not been configured by the Committee Chairperson</div>");}%-->
                                <%}%>
                            </td>
                        </tr>
                        <%
                        List<SPCommonSearchDataMore> bidderRounds = new ArrayList<SPCommonSearchDataMore>();//Round List Initialized
                        List<SPCommonSearchDataMore> winnerNames = new ArrayList<SPCommonSearchDataMore>();//Round List Initialized
                         if((isCase2 || isCase3) && isTER1Sign && isAllBOQDecrypted){
                            getreportID = dataMore.getEvalProcessInclude("isCRFormulaMadeTORTER", tenderid_eval,"0", "TER");
                            if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                                isCRFormulaMadeTER = true;
                                getreportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid_eval,"0", null);
                                if(getreportID!=null && (!getreportID.isEmpty())){
                                    reportID = getreportID.get(0).getFieldName1();
                                }
                            }
                            bidderRounds = dataMore.getEvalProcessInclude("getRoundsforEvaluation",tenderid_eval,reportID,"L1");
                            if(isT1 || isT1L1){
                                winnerNames = dataMore.getEvalProcessInclude("getRoundsforEvalByMethod",tenderid_eval,isT1 ? "T1":"T1L1");
                            }
                        }
                        // This Case was written as Send to AA has gone under round so to manage it for TER 1,2 sending
                        if(isCase1 || isCase2){
                        boolean isTERAll = false;
                            /* Start By Dohatec BidderFail Added for Service*/
                                List<SPCommonSearchDataMore> isFinalize1 = null;                               
                            /* End By Dohatec */
                         isClariAnswered = false;
                         aaID = null;
                        List<SPCommonSearchDataMore> TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, "0", "1", "'TER1'","0",String.valueOf(evalCount));
                        if(TERALL!=null && (!TERALL.isEmpty()) && TERALL.get(0).getFieldName1().equals("1")){
                            isTERAll = true;
                            TERALL.clear();
                            if(pMethod.equals("QCBS"))
                            TERALL = dataMore.getEvalProcessInclude("isTERSentToAAService", tenderid_eval, "0", "0","0",String.valueOf(evalCount));
                            else
                            TERALL = dataMore.getEvalProcessInclude("isTERSentToAA", tenderid_eval, "0", "0","0",String.valueOf(evalCount));

                            if(TERALL!=null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))){
                                isTERSentToAA = true;
                                TERALL = dataMore.getEvalProcessInclude("isTERSentToAAApp", tenderid_eval, "0", "0","0");
                                if(TERALL!=null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))){
                                   isCase2AASent = true;
                                   isSentToAADecryptAll = true;
                                }
                            }
                            TERALL.clear();
                            TERALL = dataMore.getEvalProcessInclude("isClarificationAsked", tenderid_eval, "0","0");
                            if(TERALL!=null && (!TERALL.isEmpty())){
                                isClariAsked = true;
                                aaID = TERALL.get(0).getFieldName1();
                                isClariAnswered = !TERALL.get(0).getFieldName2().trim().equals("");
                             }
                            /*Start By Dohatec BidderFail Added for Service*/
                            isFinalize1 = dataMore.getEvalProcessInclude("getResponsiveBidderCountService", tenderid_eval);
                            if(isFinalize1!=null && (!isFinalize1.isEmpty()) && isFinalize1.get(0).getFieldName1().equals("0")){
                                    isAllBidderFail = true;
                                }
                            /* End By Dohatec */
                        }
                        if(isCommCP && isTERAll){
                            if(!isAllBidderFail){
                %>
                <tr>
                    <td class="ff" width="25%">Send Evaluation Report to AA through Workflow</td>
                    <%} else if(isClariAsked){%>
                    <td class="ff" width="25%">Send Evaluation Report to AA</td>
                    <%}else{%>
                    <td class="ff" width="25%">Send Evaluation Report to HOPA</td>
                    <%}%>
                    <td width="75%">
                        <!-- if Clarification Asked  dohatec Start!-->
                         <% if(!isAllBidderFail){%>
                        <%if(isClariAsked){%>
                         <%if(!isTERSentToAA){%>
                        <a href="SendReportAA.jsp?tenderid=<%=tenderid_eval%>&lotId=0&rId=0">Send</a>
                        <%}else{%>
                        <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=0&rId=0">View</a>
                        <%if(isClariAsked){%>
                        &nbsp;|&nbsp;
                        <a href="GiveClarificationAA.jsp?tenderid=<%=tenderid_eval%>&evalRptToAaid=<%=aaID%>&lotId=0&rId=0"><%if(isClariAnswered){%>Clarification Given<%}else{%>Give Clarification<%}%></a>
                        <%}}%>
                        <%}else{%>
                        <!-- if Clarification Asked  dohatec End!-->
                       <%/* Dohatec Start */%>
                          <%if(evaluationWorkflowCreated){
                          if(!isTERSentToAA){%>
                        <a href="FileProcessing.jsp?activityid=<%=activity_id%>&objectid=<%=object_id%>&childid=<%=child_id%>&eventid=<%=event_id%>&fromaction=<%=from_action%>">Process file in Workflow</a>
                        <%}else{%>
                        <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=0&rId=0">View</a>
                        <%}}
                        else{%>
                        <label>Workflow yet not configured</label>
                        <%
                            List<SPCommonSearchDataMore> PENotified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, "0", "wf","0");
                            boolean isPENotified = false;
                            if(PENotified!=null && (!PENotified.isEmpty()) && PENotified.get(0).getFieldName1().equals("1")){
                                isPENotified = true;
                            }
                            if(!isPENotified){
                                msgUrl = "tenderid="+tenderid_eval+"&lotId=0&action=notifype&rp=wf&rId=0";
                        %>
                        <!--<a href="<%//=request.getContextPath()%>/EvaluationServlet?tenderid=<%//=tenderid_eval%>&lotId=0&action=notifype&rp=wf&rId=0" onclick="return confirm('Do you really want to notify PE to create Contract Approval workflow?')">Notify To PE</a>-->
                        <label class="notifyPE"> | </label> <a class="notifyPE" style="cursor: pointer;">Notify To PE</a>
                        <%}else{%>
                         <label> | PE Notified</label>
                        <%}}}%>
                        <%}else{%>
                        <%if(!isTERSentToAA){%>
                        <a href="SendReportAA.jsp?tenderid=<%=tenderid_eval%>&lotId=0&rId=0&hopeOnly=1">Send</a>
                        <%}else{%>
                        <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=0&rId=0">View</a>
                        <%if(isClariAsked){%>
                        &nbsp;|&nbsp;
                        <a href="GiveClarificationAA.jsp?tenderid=<%=tenderid_eval%>&evalRptToAaid=<%=aaID%>&lotId=0&rId=0"><%if(isClariAnswered){%>Clarification Given<%}else{%>Give Clarification<%}%></a>
                        <%}}%>
                        <%}%>
                        <% /* Dohatec End*/%>
                    </td>
                </tr>
                <%}}%>
                <!--Round Starts Here-->
               <%
                /*    Dohatec Start All Bidder Negotiation Failed */
                    Boolean AllBidderNegotiationFailed = false;
                    List<SPCommonSearchDataMore> isFinalize1 = null;
                    getreportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid_eval,"0","0");
                               if(getreportID!=null && (!getreportID.isEmpty())){
                                        reportID = getreportID.get(0).getFieldName1();
                                    }
                    isFinalize1 = dataMore.getEvalProcessInclude("AllBidderDisqualifiedService", tenderid_eval,reportID);
                            if(isFinalize1!=null && (!isFinalize1.isEmpty()) && isFinalize1.get(0).getFieldName1().equals("1")){
                                    AllBidderNegotiationFailed = true;
                                }                    
                    int rounds=1;
                    String finalRound = "0";
                    for(SPCommonSearchDataMore round : bidderRounds){
                        boolean isCRSaved = false;
                        boolean isTER2Sign = false;
                        boolean isTER3Sign = false;
                        boolean isTER4Sign = false;
                        boolean isTERAll = false;
                        boolean isTER2Notified = false;
                        boolean isTER3Notified = false;
                        boolean isTER4Notified = false;
                         isTERSentToAA = false;
                         isTERSentToReviewer = false;
                         isClariAsked = false;
                         isClariAnswered =false;
                        boolean isPerfAvail = false;
                        boolean isPerfView = false;
                        boolean isT1L1Case = true;
                        String T1L1Round = null;
                        String bidderName = round.getFieldName3();
                         aaID = null;
                        String roundId = round.getFieldName1();
                        if(isT1L1){
                            List<SPCommonSearchDataMore> getT1L1RoundWise = dataMore.getEvalProcessInclude("getT1L1RoundWise",tenderid_eval,"T1L1");
                            for(int isize = 0; isize<getT1L1RoundWise.size();isize++){
                                SPCommonSearchDataMore tll1data = getT1L1RoundWise.get(isize);
                                bidderName = tll1data.getFieldName3();
                                T1L1Round = tll1data.getFieldName1();
                            }
                        }
                        finalRound = roundId;
                        if (isCase2 || isCase3) {
                            TER2Sign = dataMore.getEvalProcessInclude("isEvalReportSigned", tenderid_eval, "0", "ter2",roundId,String.valueOf(evalCount));
                            List<SPCommonSearchDataMore> TER3Sign = dataMore.getEvalProcessInclude("isEvalReportSigned", tenderid_eval, "0", "ter3",roundId,String.valueOf(evalCount));
                            List<SPCommonSearchDataMore> TER4Sign = dataMore.getEvalProcessInclude("isEvalReportSigned", tenderid_eval, "0", "ter4",roundId,String.valueOf(evalCount));
                            if (!TER2Sign.isEmpty() && !TER2Sign.get(0).getFieldName1().equals("0")) {
                                isTER2Sign = true;
                            }
                            if (!TER3Sign.isEmpty() && !TER3Sign.get(0).getFieldName1().equals("0")) {
                                isTER3Sign = true;
                            }
                            if (!TER4Sign.isEmpty() && !TER4Sign.get(0).getFieldName1().equals("0")) {
                                isTER4Sign = true;
                            }
                            TER2Sign=null;
                            TER3Sign=null;
                            TER4Sign=null;
                        }
                        List<SPCommonSearchDataMore> TER2Notified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, "0", "ter2",roundId);
                        List<SPCommonSearchDataMore> TER3Notified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, "0", "ter3",roundId);
                        List<SPCommonSearchDataMore> TER4Notified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, "0", "ter4",roundId);

                        if(TER2Notified!=null && (!TER2Notified.isEmpty()) && TER2Notified.get(0).getFieldName1().equals("1")){
                            isTER2Notified = true;
                        }
                        if(TER3Notified!=null && (!TER3Notified.isEmpty()) && TER3Notified.get(0).getFieldName1().equals("1")){
                            isTER3Notified = true;
                        }
                        if(TER4Notified!=null && (!TER4Notified.isEmpty()) && TER4Notified.get(0).getFieldName1().equals("1")){
                            isTER4Notified = true;
                        }
                        if((isCase2 || isCase3) && (isTER1Sign) && isAllBOQDecrypted){
                            getreportID = dataMore.getEvalProcessInclude("isCRFormulaMadeTORTER", tenderid_eval,"0", "TER");
                            if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                                isCRFormulaMadeTER = true;
                                getreportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid_eval,"0",roundId);
                                if(getreportID!=null && (!getreportID.isEmpty())){
                                    reportID = getreportID.get(0).getFieldName1();
                                    if(getreportID.get(0).getFieldName2().equals("1")){
                                        isCRSaved = true;
                                    }
                                }
                            }
                        }

                        List<SPCommonSearchDataMore> TERALL = null;
                        //TAHER------------>
                        if (isCase1) {
                            TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, "0", "1", "'TER1'",roundId,String.valueOf(evalCount));
                        }else if (isCase2) {
                            if(isCRSaved){
                                 TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, "0", "4", "'TER1','TER2','TER3','TER4'",roundId,String.valueOf(evalCount));
                            }else{
                                TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, "0", "1", "'TER1'",roundId,String.valueOf(evalCount));
                            }

                        }else if (isCase3) {
                            TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, "0", "4", "'TER1','TER2','TER3','TER4'",roundId,String.valueOf(evalCount));
                        }
                        if (TERALL != null && (!TERALL.isEmpty()) && TERALL.get(0).getFieldName1().equals("1")) {
                            TERALL.clear();
                            if((isCase2 || isCase3) && isCRSaved){
                                TERALL = dataMore.getEvalProcessInclude("isTERSentToAA", tenderid_eval, "0", "1",roundId,"");
                            }else{
                                TERALL = dataMore.getEvalProcessInclude("isTERSentToAA", tenderid_eval, "0", "0",roundId,"");
                            }

                            if (TERALL != null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))) {
                                isTERSentToAA = true;
                            }

                            /**
                                For Introducing Reviewer (Secretary) in Evaluation Approve
                            */
                            TERALL.clear();
                            TERALL = dataMore.getEvalProcessInclude("isTERSentForReview", tenderid_eval, "0", roundId);
                            if(TERALL!=null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))){
                                isTERSentToReviewer = true;
                             }
                             /*
                                Reviewer Code Complied
                            */
                            TERALL.clear();
                            TERALL = dataMore.getEvalProcessInclude("isClarificationAsked", tenderid_eval, "0", roundId);
                            if (TERALL != null && (!TERALL.isEmpty())) {
                                isClariAsked = true;
                                aaID = TERALL.get(0).getFieldName1();
                                isClariAnswered = !TERALL.get(0).getFieldName2().trim().equals("");
                            }
                            isTERAll = true;
                        }
                        TERALL=null;
                        if((isCase2 || isCase3) && isTER1Sign && isAllBOQDecrypted){
                            getreportID = dataMore.getEvalProcessInclude("isCRFormulaMadeTORTER", tenderid_eval,"0", "TER");
                            if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                                isCRFormulaMadeTER = true;
                                getreportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid_eval,"0",roundId);
                                if(getreportID!=null && (!getreportID.isEmpty())){
                                    reportID = getreportID.get(0).getFieldName1();
                                    if(getreportID.get(0).getFieldName2().equals("1")){
                                        isCRSaved = true;
                                    }
                                }
                            }
                        }

                        if(isCommCP && isPerfSecurityAvail){
                            List<SPCommonSearchDataMore> perfList = dataMore.getEvalProcessInclude("PerfSecurityAvailChk",roundId);
                            if(perfList!=null && (!perfList.isEmpty()) && perfList.get(0).getFieldName1().equals("1")){
                                isPerfAvail = true;
                                perfList.clear();
                            }
                            perfList = dataMore.getEvalProcessInclude("PerfSecurityNaoChk",roundId);
                            if(perfList!=null && (!perfList.isEmpty()) && perfList.get(0).getFieldName1().equals("1")){
                                isPerfView = true;
                                perfList.clear();
                            }
                        }

                if((isCase2 || isCase3) && isTER1Sign && isAllBOQDecrypted){
                 String repQString="";
                 String crLabel="View and Save";
                 if(isCRSaved){
                     repQString="&sv=y";
                     crLabel="View";
                 }
                 if(isCommCP || (isCommMem && isCRSaved)){
            %>
            <!--Name of the Bidder for the Round-->
            <tbody id="round_<%=rounds%>">
            <%if(isT1 || isT1L1){if(!winnerNames.isEmpty()){%>
            <tr><th colspan="2"><%=winnerNames.get(rounds-1).getFieldName1()%> <%=(bidderRounds.size()==rounds)?"(Winning Consultant)":""%></th></tr>
            <%}}else{%>
            <tr><th colspan="2"><%=round.getFieldName3()%> <%=(bidderRounds.size()==rounds)?"(Winning Consultant)":""%></th></tr>
            <%}%>
            <!--\Name of the Bidder for the Round-->
            <tr>
                <td class="t-align-left ff">Price Comparison Report</td>
                <td>
                    <%if(isCRFormulaMadeTER){%>
                    <%if(reportID!=null && !reportID.equalsIgnoreCase("null")){%>
                        <a href="<%=request.getContextPath()%>/officer/TenderReport.jsp?tenderid=<%=tenderid_eval%>&repId=<%=reportID%>&istos=y&from=ter<%=repQString%>&rId=<%=roundId%>"><%=crLabel%></a>
                    <%}else{out.print("<div class='responseMsg noticeMsg'>Please Prepare a Price Comparison Report first</div>");}%>
                    <%}else{out.print("<div class='responseMsg noticeMsg'>Report Formula not created.</div>");}%>
                </td>
            </tr>
            <%if(isT1L1){%>
            <tr>
                <td class="t-align-left ff">T1L1 Report</td>
                <td>
                    <%
                        if(T1L1Round==null){
                           if(isCommCP){
                              isCRSaved = false;
                    %>
                        <a href="<%=request.getContextPath()%>/officer/T1L1Rpt.jsp?tenderid=<%=tenderid_eval%>&reportId=<%=reportID%>&rId=<%=roundId%>&roundId=0&cnt=<%=rounds%>">View and Save</a>
                    <%}else{out.print("<div class='responseMsg noticeMsg'>T1L1 Report yet not saved</div>");}}else{%>
                        <a href="<%=request.getContextPath()%>/officer/T1L1Rpt.jsp?tenderid=<%=tenderid_eval%>&reportId=<%=reportID%>&roundId=<%=T1L1Round%>&rId=<%=roundId%>&cnt=<%=rounds%>">View</a>
                    <%}%>
                </td>
            </tr>
            <%}}}%>
            <%if(((isCase2 || isCase3)&& isTER1Sign && isAllBOQDecrypted && isCRSaved)){%>
            <%if(isCommCP && isPerfSecurityAvail){%>
            <tr>
                <td class="t-align-left ff">Performance Security</td>
                <td>
                    <%if(!isPerfAvail){%>
                    <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderid_eval%>&pkgId=0&rId=<%=roundId%>">Add</a>
                    <%}else{if(!isPerfView){%>
                    <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderid_eval%>&pkgId=0&isedit=y&rId=<%=roundId%>">Edit</a>&nbsp;|&nbsp;
                    <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderid_eval%>&pkgId=0&viewAction=view&isedit=y&rId=<%=roundId%>">View</a>
                    <%}else{%>
                    <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderid_eval%>&pkgId=0&viewAction=view&isedit=y&rId=<%=roundId%>">View</a>
                    <%}}%>
                </td>
            </tr>
            <%}%>
            <tr>
                <td class="t-align-left ff"><%=repLabel%> Evaluation Report 2</td>
                <td>
                    <%
                        viewSignLabel="View and Sign";
                        viewSignList = dataMore.getEvalProcessInclude("isTERSignedLabel", tenderid_eval,"0","TER2", session.getAttribute("userId").toString(),roundId);
                        if(!viewSignLabel.isEmpty()){
                          viewSignLabel = viewSignList.get(0).getFieldName1();
                        }
                          // report signstatus
                           List<SPCommonSearchDataMore> listReportSingStatus2 = commonSearchDataMoreService.getEvalProcessInclude("getTenderReportSignStatus", tenderid_eval, "PEC" , "TER2","0" ,roundId , null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                           if(!listReportSingStatus2.isEmpty()){
                              strReport2SingStatus = listReportSingStatus2.get(0).getFieldName1();
                        }
                        List<SPCommonSearchDataMore> bidderEvaluated = dataMore.getEvalProcessInclude("isAllBidderEvaluated",tenderid_eval,"0",null);
                        if(bidderEvaluated !=null && (!bidderEvaluated.isEmpty()) && (bidderEvaluated.get(0).getFieldName1().equals("1"))){
                    %>
                        <%if(isCommCP){%>
                            <!--%if(isTER2ConfigMem){%>
                                <a href="< %=request.getContextPath()%>/officer/ViewTER.jsp?tenderid=< %=tenderid_eval%>&lotId=0&type=2&stat=< %=evalType%>" target="_blank">View Members Evaluation</a>
                                &nbsp;|&nbsp;
                            < %}%>
                            < %if(!isTER2Sign){%>
                                <a href="< %=request.getContextPath()%>/officer/TER2.jsp?tenderid=< %=tenderid_eval%>&lotId=0&config=y&stat=< %=evalType%>&sign=y">Configure</a>
                            < %}%-->
                            <!--%if(isTER2Config){%-->
                                <%if(!isTER2Sign){%>
                                    <%if(!isTER2Notified){%>
                                    <a href="<%=request.getContextPath()%>/EvaluationServlet?tenderid=<%=tenderid_eval%>&lotId=0&rId=<%=roundId%>&action=notifymail&rp=ter2" onclick="return confirm('Do you really want to Notify Members?')">Notify Members to Sign the Report</a>
                                    <%}else{out.print("Members Notified");}%>
                                    &nbsp;|&nbsp;
                                <%}%>
                                <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderid_eval%>&lotId=0&isview=y&stat=<%=evalType%>&sign=y&roundId=<%=T1L1Round%>&rId=<%=roundId%>&cnt=<%=rounds%>&grId=<%=bidderRounds.get(0).getFieldName1()%>&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                            <b id="SignStatus2" style="color:red">
                                             <%  if(!"0".equalsIgnoreCase(strReport2SingStatus)){%>
                                             <%=  " ("+strReport2SingStatus+")" %>
                                             <%}%>  </b>
                                <!--%}%-->
                        <%}%>
                        <%if(isCommMem){%>
                            <!--%if(isTER2Config){%-->
                                <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderid_eval%>&lotId=0&isview=y&stat=<%=evalType%>&sign=y&roundId=<%=T1L1Round%>&rId=<%=roundId%>&cnt=<%=rounds%>&grId=<%=bidderRounds.get(0).getFieldName1()%>&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                               <b id="SignStatus2" style="color:red">
                                             <%  if(!"0".equalsIgnoreCase(strReport2SingStatus)){%>
                                             <%=  " ("+strReport2SingStatus+")" %>
                                             <%}%>  </b>
                                <!--%}else{out.print("<div class='responseMsg noticeMsg'>Report has not been configured by the Committee Chairperson</div>");}%-->
                        <%}%>
                    <%}else{out.print("<div class='responseMsg noticeMsg'>Technical Evaluation Pending</div>");}%>
                </td>
            </tr>
            <tr>
                <td class="t-align-left ff"><%=repLabel%> Evaluation Report 3</td>
                <td>
                    <%
                        viewSignLabel="View and Sign";
                        viewSignList = dataMore.getEvalProcessInclude("isTERSignedLabel", tenderid_eval,"0", "TER3", session.getAttribute("userId").toString(),roundId);
                        if(!viewSignLabel.isEmpty()){
                          viewSignLabel = viewSignList.get(0).getFieldName1();
                        }
                         // report signstatus
                          List<SPCommonSearchDataMore> listReportSingStatus3 = commonSearchDataMoreService.getEvalProcessInclude("getTenderReportSignStatus", tenderid_eval, "PEC" , "TER3","0" ,roundId , null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                           if(!listReportSingStatus3.isEmpty()){
                              strReport3SingStatus = listReportSingStatus3.get(0).getFieldName1();
                        }
                    %>
                    <%if(isCommCP){%>
                         <%if(!isTER3Sign){%>
                         <%if(!isTER3Notified){%>
                            <a href="<%=request.getContextPath()%>/EvaluationServlet?tenderid=<%=tenderid_eval%>&lotId=0&action=notifymail&rp=ter3&rId=<%=roundId%>" onclick="return confirm('Do you really want to Notify Members?')">Notify Members to Sign the Report</a>
                         <%}else{out.print("Members Notified");}%>
                            &nbsp;|&nbsp;
                        <%}%>
                        <a href="<%=request.getContextPath()%>/officer/TER3.jsp?tenderid=<%=tenderid_eval%>&lotId=0&rId=<%=roundId%>&isview=y&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                     <b id="SignStatus3" style="color:red">
                                             <%  if(!"0".equalsIgnoreCase(strReport3SingStatus)){%>
                                             <%=  " ("+strReport3SingStatus+")" %>
                                             <%}%>  </b>
                        <%}%>
                    <%if(isCommMem){%>
                        <a href="<%=request.getContextPath()%>/officer/TER3.jsp?tenderid=<%=tenderid_eval%>&lotId=0&rId=<%=roundId%>&isview=y&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                     <b id="SignStatus3" style="color:red">
                                             <%  if(!"0".equalsIgnoreCase(strReport3SingStatus)){%>
                                             <%=  " ("+strReport3SingStatus+")" %>
                                             <%}%>  </b>
                        <%}%>
                </td>
            </tr>
            <tr>
                <td class="t-align-left ff"><%=repLabel%> Evaluation Report 4</td>
                <td>
                    <%
                        viewSignLabel="View and Sign";
                        viewSignList = dataMore.getEvalProcessInclude("isTERSignedLabel", tenderid_eval,"0", "TER4", session.getAttribute("userId").toString(),roundId);
                        if(!viewSignLabel.isEmpty()){
                          viewSignLabel = viewSignList.get(0).getFieldName1();
                        }
                         // report signstatus
                          List<SPCommonSearchDataMore> listReportSingStatus4 = commonSearchDataMoreService.getEvalProcessInclude("getTenderReportSignStatus", tenderid_eval, "PEC" , "TER4","0" ,roundId , null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                           if(!listReportSingStatus4.isEmpty()){
                              strReport4SingStatus = listReportSingStatus4.get(0).getFieldName1();
                        }
                    %>
                    <%if(isCommCP){%>
                         <%if(!isTER4Sign){%>
                         <%if(!isTER4Notified){%>
                            <a href="<%=request.getContextPath()%>/EvaluationServlet?tenderid=<%=tenderid_eval%>&lotId=0&action=notifymail&rp=ter4&rId=<%=roundId%>" onclick="return confirm('Do you really want to Notify Members?')">Notify Members to Sign the Report</a>
                            <%}else{out.print("Members Notified");}%>
                            &nbsp;|&nbsp;
                        <%}%>
                        <a href="<%=request.getContextPath()%>/officer/TER4.jsp?tenderid=<%=tenderid_eval%>&lotId=0&rId=<%=roundId%>&isview=y&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                    <b id="SignStatus4" style="color:red">
                                             <%  if(!"0".equalsIgnoreCase(strReport4SingStatus)){%>
                                             <%=  " ("+strReport4SingStatus+")" %>
                                             <%}%>  </b>
                        <%}%>
                    <%if(isCommMem){%>
                        <a href="<%=request.getContextPath()%>/officer/TER4.jsp?tenderid=<%=tenderid_eval%>&lotId=0&rId=<%=roundId%>&isview=y&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                    <b id="SignStatus4" style="color:red">
                                             <%  if(!"0".equalsIgnoreCase(strReport4SingStatus)){%>
                                             <%=  " ("+strReport4SingStatus+")" %>
                                             <%}%>  </b>
                        <%}%>
                </td>
            </tr>
            <%}%>
            <%if(isCommCP && isTERAll && rounds == bidderRounds.size()){%>
            <tr>
                <%
                    String aaQString = "";
                    String aaLabel = "";
                    if(isCase2 || isCase3){
                        if(isCRSaved){
                            aaLabel="Financial ";
                            aaQString="&rpt=1";
                        }else{
                            aaLabel="Technical ";
                            aaQString="&rpt=0";
                        }
                    }
                %>
                <%if(!AllBidderNegotiationFailed){%>
                <td class="ff" width="25%">Send Evaluation Report to AA through Workflow</td>
                <%} else {%>
                <td class="ff" width="25%">Send Evaluation Report to HOPA</td>
                <% } %>
                 <%if(!AllBidderNegotiationFailed){%>
                <td width="75%">
                     <!-- if Clarification Asked  dohatec Start!-->
                     <%if(isClariAsked){%>
                    <%if(!isTERSentToAA && !isTERSentToReviewer){%>
                    <a href="SendReportAA.jsp?tenderid=<%=tenderid_eval%>&lotId=0<%=aaQString%>&rId=<%=roundId%>">Send</a>
                    <%}else{%>
                    <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=0<%=aaQString%>&rId=<%=roundId%>">View</a>
                    <%if(isClariAsked){%>
                    &nbsp;|&nbsp;
                    <a href="GiveClarificationAA.jsp?tenderid=<%=tenderid_eval%>&evalRptToAaid=<%=aaID%>&lotId=0&rId=<%=roundId%>"> <%if(isClariAnswered){%>Clarification Given<%}else{%>Give Clarification<%}%></a>
                    <%}}%>
                     <%}else{%>
                    <!-- if Clarification Asked  dohatec End!-->
                    <%/*Dohatec Start*/%>
                        <%if(evaluationWorkflowCreated){
                        if(!isTERSentToAA /*&& !isTERSentToReviewer*/) {%>
                        <a href="FileProcessing.jsp?activityid=<%=activity_id%>&objectid=<%=object_id%>&childid=<%=child_id%>&eventid=<%=event_id%>&fromaction=<%=from_action%>">Process file in Workflow</a>
                        <%}
                            else{%>
                        <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=0<%=aaQString%>&rId=<%=roundId%>">View</a>
                        <%}}
                         else{%>
                        <label>Workflow yet not configured</label>
                        <%
                            List<SPCommonSearchDataMore> PENotified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, "0", roundId);
                            boolean isPENotified = false;
                            if(PENotified!=null && (!PENotified.isEmpty()) && PENotified.get(0).getFieldName1().equals("1")){
                                isPENotified = true;
                            }
                            if(!isPENotified){
                                msgUrl = "tenderid="+tenderid_eval+"&lotId=0&action=notifype&rp=wf&rId="+roundId;
                        %>
                        <!--<a href="<%//=request.getContextPath()%>/EvaluationServlet?tenderid=<%//=tenderid_eval%>&lotId=0&action=notifype&rp=wf&rId=<%//=roundId%>" onclick="return confirm('Do you really want to notify PE to create Contract Approval workflow?')">Notify To PE</a>-->
                        <label class="notifyPE"> | </label> <a class="notifyPE" style="cursor: pointer;">Notify To PE</a>
                        <%}else{%>
                         <label> | PE Notified</label>
                        <%}}}%>
                <%/*Dohatec End*/%>
                </td>
                <%} else {%>
                 <td width="75%">
                 <% if(!isTERSentToAA){%>
                    <a href="SendReportAA.jsp?tenderid=<%=tenderid_eval%>&lotId=0<%=aaQString%>&rId=<%=roundId%>&hopeOnly=1">Send</a>
                    <%}else{%>
                    <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=0<%=aaQString%>&rId=<%=roundId%>">View</a>
                    <%if(isClariAsked){%>
                    &nbsp;|&nbsp;
                    <a href="GiveClarificationAA.jsp?tenderid=<%=tenderid_eval%>&evalRptToAaid=<%=aaID%>&lotId=0&rId=<%=roundId%>"> <%if(isClariAnswered){%>Clarification Given<%}else{%>Give Clarification<%}%></a>
                <%}}%>
                <% } %>
                </td>
            </tr>
            </tbody>
            <%}%>
            <%
                    rounds++;
                }
            %>
            <!--Below script for hiding failed rounds-->
            <script type="text/javascript">
            <%for(int k = 1; k<(rounds-1); k++){%>
                $('#round_<%=k%>').hide();
            <%}%>
            </script>
            <!--Round Ends Here-->
            <!--Report here is for viewing and saving in subsquent rounds-->
            <%
               //TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, "0", "4", "'TER1','TER2','TER3','TER4'",roundId);
              List<SPCommonSearchDataMore> TER1ALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, "0", "1", "'TER1'","0",String.valueOf(evalCount));
              boolean isTER1ALL = false;

              if(TER1ALL!=null && !TER1ALL.isEmpty() && TER1ALL.get(0).getFieldName1().equals("1")){
                    isTER1ALL = true;
              }
              if(((isCase2 && isCase2AASent) || isCase3) && isTER1ALL && isAllBOQDecrypted && isCommCP){
                 getreportID = dataMore.getEvalProcessInclude("isCRFormulaMadeTORTER", tenderid_eval,"0", "TER");
                  if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                          isCRFormulaMadeTER = true;
                   }
                getreportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid_eval,"0","0");
                if(getreportID!=null && (!getreportID.isEmpty())){
                    reportID = getreportID.get(0).getFieldName1();
                }
                getreportID = dataMore.getEvalProcessInclude("FirstRoundAndNoaBiddersChk", tenderid_eval,"0",reportID,String.valueOf(evalCount));
               if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                 getreportID = dataMore.getEvalProcessInclude("BidderRoundsforEvaluationChk", finalRound,tenderid_eval,"0");
                if(!isAllBidderFail && !AllBidderNegotiationFailed){
                 if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                 String repQString="";
                 String crLabel="View and Save";
            %>
            <tbody>
            <tr>
                <td class="t-align-left ff">Price Comparison Report</td>
                <td>
                    <%if(isCRFormulaMadeTER){%>
                    <%if(reportID!=null && !reportID.equalsIgnoreCase("null")){
                        int decryptFormCount = 0;
                        if(pMethod.equals("QCBS")){

                         TenderOpening opening = new TenderOpening();
                         List<SPCommonSearchData> priceFormList = null;
                         List<SPCommonSearchData> lotIdDesc = commonSearchService.searchData("get2EnvLoIdLotDesc", tenderid_eval, null, null, null, null, null, null, null, null);
                         for (SPCommonSearchData lotdsc : lotIdDesc) {
                         priceFormList = commonSearchService.searchData("getPriceBidfromLotId", tenderid_eval, lotdsc.getFieldName1(), null, null, null, null, null, null, null);
                         }
                         for (SPCommonSearchData priceList : priceFormList) {
                         if(opening.getBidPlainDataCount(priceList.getFieldName5()) != 0) {
                             decryptFormCount++;
                             }
                         }
                         if(decryptFormCount == priceFormList.size()){ %>
                            <a href="<%=request.getContextPath()%>/officer/TenderReport.jsp?tenderid=<%=tenderid_eval%>&repId=<%=reportID%>&istos=y&from=ter<%=repQString%>&frId=<%=finalRound%><%=isT1?"&evalm=t1":""%>"><%=crLabel%></a>
                         <%  } else { %>
                          <a href="<%=request.getContextPath()%>/officer/TenderReport.jsp?tenderid=<%=tenderid_eval%>&repId=<%=reportID%>&istos=y&from=ter<%=repQString%>&frId=<%=finalRound%><%=isT1?"&evalm=t1":""%>" onclick="return chkAllBOQDecrypted()"><%=crLabel%></a>
                     <% }}else{ %>
    
                        <a href="<%=request.getContextPath()%>/officer/TenderReport.jsp?tenderid=<%=tenderid_eval%>&repId=<%=reportID%>&istos=y&from=ter<%=repQString%>&frId=<%=finalRound%><%=isT1?"&evalm=t1":""%>"><%=crLabel%></a>
                    <%}}else{out.print("<div class='responseMsg noticeMsg'>Please Prepare a Price Comparison Report first</div>");}%>
                    <%}else{out.print("<div class='responseMsg noticeMsg'>Report Formula not created.</div>");}%>
                </td>
            </tr>
            </tbody>
            <%}
                }
                 /*Dohatec Start Added for all bidder failed and all bidder Disqualified*/
                else{
                    //if(pMethod
                   // List<SPCommonSearchDataMore> TER12 = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, "0", "1", "'TER1','TER2'","0",String.valueOf(evalCount));
                   // if(TER12!=null && (!TER12.isEmpty()) && TER12.get(0).getFieldName1().equals("0")){
                    List<SPCommonSearchDataMore> TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, "0", "1", "'TER1'","0",String.valueOf(evalCount));
                        if(TERALL!=null && (!TERALL.isEmpty()) && TERALL.get(0).getFieldName1().equals("1")){
                            TERALL.clear();
                            TERALL = dataMore.getEvalProcessInclude("isTERSentToAA", tenderid_eval, "0", "0","0","");
                            if(TERALL!=null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))){
                                isTERSentToAA = true;
                            }
                            TERALL.clear();
                            TERALL = dataMore.getEvalProcessInclude("isClarificationAsked", tenderid_eval, "0","0");
                            if(TERALL!=null && (!TERALL.isEmpty())){
                                isClariAsked = true;
                                aaID = TERALL.get(0).getFieldName1();
                                isClariAnswered = !TERALL.get(0).getFieldName2().trim().equals("");
                             }
                        }

            %>
                <tbody>
                <tr>
                <td class="t-align-left ff">Send Evaluation Report to HOPA</td>
                <td>
                   <%if(!isTERSentToAA){%>
                        <a href="SendReportAA.jsp?tenderid=<%=tenderid_eval%>&lotId=0&rId=0&hopeOnly=1">Send</a>
                   <%}else{%>
                        <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=0&rId=0">View</a>
                   <%if(isClariAsked){%>
                        &nbsp;|&nbsp;
                        <a href="GiveClarificationAA.jsp?tenderid=<%=tenderid_eval%>&evalRptToAaid=<%=aaID%>&lotId=0&rId=0"><%if(isClariAnswered){%>Clarification Given<%}else{%>Give Clarification<%}%></a>
                   <%}}%>
                </td>
                </tr>
                </tbody>
            <%}     /*Dohatec End Added for all bidder failed and all bidder Disqualified*/
             // } //
   }}%>
            <!--/Report here is for viewing and saving in subsquent rounds-->
         <%}%>
         <%if(!isterdisp && isCommMem){%>
         <tbody>
         <tr>
             <td class="t-align-center" colspan="2">
                 <div class="responseMsg noticeMsg">Chairperson has not finalized Evaluation status yet.</div>
             </td>
         </tr>
         </tbody>
         <%}%>
        </table>
        <%
            packageList=null;
           } }else {
            /*****************************************
            For Lot Wise
            *****************************************/

            out.print("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
            String docAvail = commonService.getDocAvlMethod(tenderid_eval).toString();

             boolean isterdisp = false;//for displaying TER's and Technical Evaluation Report Lot Wise
             List<SPCommonSearchDataMore> isFinalize = null;
             //BidderFail Added
                boolean isAllBidderFail = false;
                isFinalize = dataMore.getEvalProcessInclude("getResponsiveBidderCount", tenderid_eval);
                if(isFinalize!=null && (!isFinalize.isEmpty()) && isFinalize.get(0).getFieldName1().equals("0")){
                    isAllBidderFail = true;
                }
                /*Dohatec Start*/
                List<SPCommonSearchDataMore> isFinalize1 = null;
                boolean isAllBidderDisqualified = false;
                if(!isAllBidderFail)
                    {
                              for (SPTenderCommonData tenderLot : tenderLotList) {
                               getreportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid_eval,tenderLot.getFieldName3(),"0");
                               if(getreportID!=null && (!getreportID.isEmpty())){
                                        reportID = getreportID.get(0).getFieldName1();
                                    }
                               }
                        isFinalize1 = dataMore.getEvalProcessInclude("AllBidderDisqualified", tenderid_eval,reportID.toString());
                        if(isFinalize1!=null && (!isFinalize1.isEmpty()) && isFinalize1.get(0).getFieldName1().equals("1")){ isAllBidderDisqualified = true;    }
                     //   if (!isFinalize1.isEmpty()){ System.out.println(" Field Value 1 is = " + isFinalize1.get(0).getFieldName1()); }
                       // else {System.out.println(" Is Finalize is null " );}
                        //isAllBidderFail = isAllBidderDisqualified;
                     //   bidderRounds = null;
                    }
                /*Dohatec End*/

              
               if(docAvail.equalsIgnoreCase("Package")){
                isFinalize = dataMore.getEvalProcessInclude("isFinalizeDoneForLot", tenderid_eval,"0",String.valueOf(evalCount));
                if(isFinalize!=null && (!isFinalize.isEmpty()) && isFinalize.get(0).getFieldName1().equals("1")){
                    isterdisp = true;
                }
            }
            if((isCommCP || (isCommMem && isterdisp)) && docAvail.equalsIgnoreCase("Package")){%>
            <tr>
                <td class="t-align-left ff"  width="20%">Finalize Evaluation Status :</td>
                <td class="t-align-center"  width="80%">
                    <jsp:include page="CPEvalReport.jsp">
                            <jsp:param name="tenderid" value="<%=tenderid_eval%>"/>
                            <jsp:param name="pnature" value="<%=strProcurementNature%>"/>
                            <jsp:param name="comtype" value="<%=strComType%>"/>
                            <jsp:param name="dmethod" value="<%=docAvail%>"/>
                            <jsp:param  name="lotId" value="0"/>
                            <jsp:param  name="isCommCP" value="<%=isCommCP%>"/>
                            <jsp:param name="allowFinalResponseProcess" value="<%=strAllowFinalResponseProcess%>"/>
                            <jsp:param name="evalCount" value="<%=evalCount%>"/>
                     </jsp:include>
                </td>
            </tr>
            <%}
            for (SPTenderCommonData tenderLot : tenderLotList) {
                    boolean isTER1Config = false;
                    boolean isTER2Config = false;
                    boolean isTER1ConfigMem = false;
                    boolean isTER2ConfigMem = false;

                    boolean isTER1Sign = false;
                    boolean isTER2Sign = false;

                    boolean isTER1Notified = false;
                    boolean isTER2Notified = false;

                    boolean isCRFormulaMadeTER = false;
                    boolean isCase2AASent = false;

                    if(docAvail.equalsIgnoreCase("Lot")){
                        isFinalize = dataMore.getEvalProcessInclude("isFinalizeDoneForLot", tenderid_eval,tenderLot.getFieldName3());
                        if(isFinalize!=null && (!isFinalize.isEmpty()) && isFinalize.get(0).getFieldName1().equals("1")){
                            isterdisp = true;
                        }else{
                            isterdisp = false;
                        }
                    }
                     List<SPCommonSearchDataMore> AllBOQDecrypted = null;
                        boolean isAllBOQDecrypted = false;
                        if((isCase2 || isCase3)){
                           AllBOQDecrypted =  dataMore.getEvalProcessInclude("isAllBOQDecrypted", tenderid_eval, tenderLot.getFieldName3(), null);
                           if(AllBOQDecrypted!=null && (!AllBOQDecrypted.isEmpty()) && AllBOQDecrypted.get(0).getFieldName1().equals("1")){
                               isAllBOQDecrypted = true;
                           }
                        }
                %>
                <tr>
                <td width="20%" class="t-align-left ff">Lot No:</td>
                <td width="80%" class="t-align-left"><%=tenderLot.getFieldName1()%></td>
            </tr>
            <tr>
                <td class="t-align-left ff">Lot Description:</td>
                <td class="t-align-left"><%=tenderLot.getFieldName2()%></td>
            </tr>
            <%if(isCommCP && docAvail.equalsIgnoreCase("Lot")){%>
            <tr>
                <td class="t-align-left ff">Finalize Evaluation Status :</td>
                <td class="t-align-center">
                    <jsp:include page="CPEvalReport.jsp">
                            <jsp:param name="tenderid" value="<%=tenderid_eval%>"/>
                            <jsp:param name="pnature" value="<%=strProcurementNature%>"/>
                            <jsp:param name="comtype" value="<%=strComType%>"/>
                            <jsp:param name="dmethod" value="<%=docAvail%>"/>
                            <jsp:param name="lotId" value="<%=tenderLot.getFieldName3()%>"/>
                            <jsp:param name="allowFinalResponseProcess" value="<%=strAllowFinalResponseProcess%>"/>
                     </jsp:include>
                </td>
            </tr>
            <%if(isCommCP && isterdisp){%>
            <tr>
                <td class="t-align-left ff">
                    Technical Evaluation Report
                </td>
                <td class="t-align-left">
                    <a href="TenderersCompRpt.jsp?tenderid=<%=tenderid_eval%>&st=rp&comType=<%=strComType%>&lotId=<%=tenderLot.getFieldName3()%>">View</a>
                </td>
            </tr>
            <%}%>
            <%}
                    if(isterdisp){
                        if(creationService.evalRepCount(tenderid_eval, tenderLot.getFieldName3(), evalType, "TER1","tecRole='cp'",evalCount)!=0){
                            isTER1Config = true;
                        }
                        if(creationService.evalRepCount(tenderid_eval, tenderLot.getFieldName3(), evalType, "TER2","tecRole='cp'",evalCount)!=0){
                            isTER2Config = true;
                        }
                        if(creationService.evalRepCount(tenderid_eval, tenderLot.getFieldName3(), evalType, "TER1","tecRole='m'",evalCount)!=0){
                            isTER1ConfigMem = true;
                        }
                        if(creationService.evalRepCount(tenderid_eval, tenderLot.getFieldName3(), evalType, "TER2","tecRole='m'",evalCount)!=0){
                            isTER2ConfigMem = true;
                        }
                        List<SPCommonSearchDataMore> TER1Notified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, tenderLot.getFieldName3(),"ter1", "0");
                        List<SPCommonSearchDataMore> TER2Notified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, tenderLot.getFieldName3(), "ter2", "0");
                        if(TER1Notified!=null && (!TER1Notified.isEmpty()) && TER1Notified.get(0).getFieldName1().equals("1")){
                            isTER1Notified = true;
                        }
                        if(TER2Notified!=null && (!TER2Notified.isEmpty()) && TER2Notified.get(0).getFieldName1().equals("1")){
                            isTER2Notified = true;
                        }
                        List<SPCommonSearchDataMore> TER1Sign = dataMore.getEvalProcessInclude("isEvalReportSigned", tenderid_eval, tenderLot.getFieldName3(), "ter1","0",String.valueOf(evalCount));
                        List<SPCommonSearchDataMore> TER2Sign = dataMore.getEvalProcessInclude("isEvalReportSigned", tenderid_eval, tenderLot.getFieldName3(), "ter2","0",String.valueOf(evalCount));
                        if(!TER1Sign.isEmpty() && !TER1Sign.get(0).getFieldName1().equals("0")){
                            isTER1Sign = true;
                        }
                        if(!TER2Sign.isEmpty() && !TER2Sign.get(0).getFieldName1().equals("0")){
                            isTER2Sign = true;
                        }
                        TER1Sign = null;
                        TER2Sign = null;
                     %>
                        <tr>
                <td class="t-align-left ff"><%=repLabel%> Evaluation Report 1</td>
                <td>
                    <%
                                String viewSignLabel="View and Sign";
                                List<SPCommonSearchDataMore> viewSignList = dataMore.getEvalProcessInclude("isTERSignedLabel", tenderid_eval,tenderLot.getFieldName3(), "TER1", session.getAttribute("userId").toString(),"0");
                                if(!viewSignLabel.isEmpty()){
                                    viewSignLabel = viewSignList.get(0).getFieldName1();
                                }
                                 List<SPCommonSearchDataMore> listReportSingStatus1 = commonSearchDataMoreService.getEvalProcessInclude("getTenderReportSignStatus", tenderid_eval, "TEC" , "TER1", "0", "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                           if(!listReportSingStatus1.isEmpty()){
                              strReport1SingStatus = listReportSingStatus1.get(0).getFieldName1();
                        }
                    %>
                    <%if(isCommCP){%>
                        <%if(isTER1ConfigMem){%>
                            <a href="<%=request.getContextPath()%>/officer/ViewTER.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&type=1&stat=<%=evalType%>" target="_blank">View Members Evaluation</a>
                            &nbsp;|&nbsp;
                        <%}%>
                        <%if(!isTER1Sign){%>
                            <a href="<%=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&config=y&stat=<%=evalType%>&sign=y&parentLink=937&evalCount=<%=evalCount%>">Configure</a>
                        <%}%>
                        <%if(isTER1Config){%>
                            <%if(!isTER1Sign){%>
                                &nbsp;|&nbsp;
                                <%if(!isTER1Notified){%>
                                <a href="<%=request.getContextPath()%>/EvaluationServlet?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&action=notifymail&rp=ter1" onclick="return confirm('Do you really want to Notify Members?')">Notify Members to Sign the Report</a>
                                <%}else{out.print("Members Notified");}%>
                                &nbsp;|&nbsp;
                            <%}%>
                            <a href="<%=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&isview=y&stat=<%=evalType%>&sign=y&parentLink=749&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                        
                                            <b id="SignStatus1" style="color:red">
 <%  if(!"0".equalsIgnoreCase(strReport1SingStatus)){%>
 <%=  " ("+strReport1SingStatus+")" %>
 <%}%>  </b>
                            <%}%>
                    <%}%>
                    <%if(isCommMem){%>
                        <%if(isTER1Config){%>
                            <a href="<%=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&isview=y&stat=<%=evalType%>&sign=y&parentLink=749&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                       
                                            <b id="SignStatus1" style="color:red">
 <%  if(!"0".equalsIgnoreCase(strReport1SingStatus)){%>
 <%=  " ("+strReport1SingStatus+")" %>
 <%}%>  </b>
                            <%}else{out.print("<div class='responseMsg noticeMsg'>Report has not been configured by the Committee Chairperson</div>");}%>
                    <%}%>
                </td>
            </tr>
            <tr>
                <td class="t-align-left ff"><%=repLabel%> Evaluation Report 2</td>
                <td>
                     <%
                        viewSignLabel="View and Sign";
                        viewSignList = dataMore.getEvalProcessInclude("isTERSignedLabel", tenderid_eval,tenderLot.getFieldName3(), "TER2", session.getAttribute("userId").toString(),"0");
                        if(!viewSignLabel.isEmpty()){
                            viewSignLabel = viewSignList.get(0).getFieldName1();
                        }
                        List<SPCommonSearchDataMore> listReportSingStatus2 = commonSearchDataMoreService.getEvalProcessInclude("getTenderReportSignStatus", tenderid_eval, "TEC" , "TER2", "0", "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                           if(!listReportSingStatus2.isEmpty()){
                              strReport2SingStatus = listReportSingStatus2.get(0).getFieldName1();
                        }
                            
                        List<SPCommonSearchDataMore> bidderEvaluated = dataMore.getEvalProcessInclude("isAllBidderEvaluated",tenderid_eval,tenderLot.getFieldName3(),null, null);
                        if(bidderEvaluated !=null && (!bidderEvaluated.isEmpty()) && (bidderEvaluated.get(0).getFieldName1().equals("1"))){
                    %>
                        <%if(isCommCP){%>
                            <%if(isTER2ConfigMem){%>
                                <a href="<%=request.getContextPath()%>/officer/ViewTER.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&type=2&stat=<%=evalType%>" target="_blank">View Members Evaluation</a>
                                &nbsp;|&nbsp;
                            <%}%>
                            <%if(!isTER2Sign){%>
                                <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&config=y&stat=<%=evalType%>&sign=y&parentLink=856&evalCount=<%=evalCount%>">Configure</a>
                            <%}%>
                            <%if(isTER2Config){%>
                                <%if(!isTER2Sign){%>
                                    &nbsp;|&nbsp;
                                    <%if(!isTER2Notified){%>
                                    <a href="<%=request.getContextPath()%>/EvaluationServlet?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&action=notifymail&rp=ter2" onclick="return confirm('Do you really want to Notify Members?')">Notify Members to Sign the Report</a>
                                    <%}else{out.print("Members Notified");}%>
                                    &nbsp;|&nbsp;
                                <%}%>
                                <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&isview=y&stat=<%=evalType%>&sign=y&parentLink=930&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                             <b id="SignStatus2" style="color:red">
 <%  if(!"0".equalsIgnoreCase(strReport2SingStatus)){%>
 <%=  " ("+strReport2SingStatus+")" %>
 <%}%>  </b>
                                <%}%>
                        <%}%>
                        <%if(isCommMem){%>
                            <%if(isTER2Config){%>
                                <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&isview=y&stat=<%=evalType%>&sign=y&parentLink=930&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                             <b id="SignStatus2" style="color:red">
 <%  if(!"0".equalsIgnoreCase(strReport2SingStatus)){%>
 <%=  " ("+strReport2SingStatus+")" %>
 <%}%> </b>
 <%}else{out.print("<div class='responseMsg noticeMsg'>Report has not been configured by the Committee Chairperson</div>");}%>
                        <%}%>
                    <%}else{out.print("<div class='responseMsg noticeMsg'>Technical Evaluation Pending</div>");}%>
                </td>
            </tr>
            <%
                List<SPCommonSearchDataMore> bidderRounds = new ArrayList<SPCommonSearchDataMore>();//Round List Initialized
                if((isCase2 || isCase3)&& !isAllBidderDisqualified){
                   getreportID = dataMore.getEvalProcessInclude("isCRFormulaMadeTORTER", tenderid_eval,tenderLot.getFieldName3(), "TER");
                   if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                          isCRFormulaMadeTER = true;
                   }
                   getreportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid_eval,tenderLot.getFieldName3(),"0");
                   if(getreportID!=null && (!getreportID.isEmpty())){
                        reportID = getreportID.get(0).getFieldName1();
                    }
                   bidderRounds = dataMore.getEvalProcessInclude("getRoundsforEvaluation",tenderid_eval,reportID,"L1");
                 
                }
                // This Case was written as Send to AA has gone under round so to manage it for TER 1,2 sending
                //BidderFail Added
                if((isCase1 || isCase2) || isAllBidderFail || isAllBidderDisqualified ){
                    boolean isTERAll = false;
                     isTERSentToAA = false;
                     isTERSentToReviewer = false;
                     isClariAsked = false;
                     isClariAnswered = false;
                    aaID = null;
                    List<SPCommonSearchDataMore> TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, tenderLot.getFieldName3(), "2", "'TER1','TER2'","0",String.valueOf(evalCount));
                    if(TERALL!=null && (!TERALL.isEmpty()) && TERALL.get(0).getFieldName1().equals("1")){
                        isTERAll = true;
                        TERALL.clear();
                        if(pMethod.equals("OSTETM") || pMethod.equals("TSTM"))
                            TERALL = dataMore.getEvalProcessInclude("isTERSentToAA1s2e", tenderid_eval, tenderLot.getFieldName3(), "0","0",String.valueOf(evalCount));
                        else
                            TERALL = dataMore.getEvalProcessInclude("isTERSentToAA", tenderid_eval, tenderLot.getFieldName3(), "0","0",String.valueOf(evalCount));

                        if(TERALL!=null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))){
                            isTERSentToAA = true;
                            TERALL = dataMore.getEvalProcessInclude("isTERSentToAAApp", tenderid_eval, tenderLot.getFieldName3(), "0","0");
                            if(TERALL!=null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))){
                               isCase2AASent = true;
                               isSentToAADecryptAll = true;
                            }
                        }
                        /**
                            For Introducing Reviewer (Secretary) in Evaluation Approve
                        */
                        TERALL.clear();
                        TERALL = dataMore.getEvalProcessInclude("isTERSentForReview", tenderid_eval, tenderLot.getFieldName3(), "0");
                        if(TERALL!=null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))){
                            isTERSentToReviewer = true;
                         }
                         /*
                            Reviewer Code Complied
                        */
                        TERALL.clear();
                        TERALL = dataMore.getEvalProcessInclude("isClarificationAsked", tenderid_eval, tenderLot.getFieldName3(),"0");
                        if(TERALL!=null && (!TERALL.isEmpty())){
                            isClariAsked = true;
                            aaID = TERALL.get(0).getFieldName1();
                            isClariAnswered = !TERALL.get(0).getFieldName2().trim().equals("");
                         }
                    }
                    if(isCommCP && isTERAll){
                       if(!isAllBidderDisqualified && !isAllBidderFail){
            %>
            <tr>
                <td class="ff" width="25%">Send Evaluation Report to AA through Workflow</td>
                <%} else if(isClariAsked){%>
                <td class="ff" width="25%">Send Evaluation Report to AA</td>
                <%} else{%>
                <td class="ff" width="25%">Send Evaluation Report to HOPA</td>
                 <%}%>
                <td width="75%">
                    <!-- if Clarification Asked  dohatec Start!-->
                     <%if(isClariAsked){%>
                    <%if(!isTERSentToAA && !isTERSentToReviewer){%>
                    <a href="SendReportAA.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&rId=0">Send</a>
                    <%}else{%>
                    <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&rId=0">View</a>
                    <%if(isClariAsked){%>
                    &nbsp;|&nbsp;
                    <a href="GiveClarificationAA.jsp?tenderid=<%=tenderid_eval%>&evalRptToAaid=<%=aaID%>&lotId=<%=tenderLot.getFieldName3()%>&rId=0"><%if(isClariAnswered){%>Clarification Given<%}else{%>Give Clarification<%}%></a>
                    <%}}%> 
                    <%} else if(isAllBidderDisqualified || isAllBidderFail ){%>
                    <%if(!isTERSentToAA && !isTERSentToReviewer){%>
                    <a href="SendReportAA.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&rId=0&hopeOnly=1">Send</a>
                    <%}else{%>
                    <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&rId=0">View</a>
                    <%if(isClariAsked){%>
                    &nbsp;|&nbsp;
                    <a href="GiveClarificationAA.jsp?tenderid=<%=tenderid_eval%>&evalRptToAaid=<%=aaID%>&lotId=<%=tenderLot.getFieldName3()%>&rId=0"><%if(isClariAnswered){%>Clarification Given<%}else{%>Give Clarification<%}%></a>
                    <%}}%> 
                     <%}else{%>
                    <!-- if Clarification Asked  dohatec End!-->
                     <%/* Dohatec Start*/%>
                        <%if(evaluationWorkflowCreated){
                        if(!isTERSentToAA && !isTERSentToReviewer){%>
                        <a href="FileProcessing.jsp?activityid=<%=activity_id%>&objectid=<%=object_id%>&childid=<%=child_id%>&eventid=<%=event_id%>&fromaction=<%=from_action%>">Process file in Workflow</a>
                        <%}else{%>
                        <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&rId=0">View</a>
                        <%}}
                        else{%>
                        <label>Workflow yet not configured</label>
                        <%
                            List<SPCommonSearchDataMore> PENotified = dataMore.geteGPData("isTERNotified", tenderid_eval, tenderLot.getFieldName3(), "wf","0");
                            boolean isPENotified = false;
                            if(PENotified!=null && (!PENotified.isEmpty()) && PENotified.get(0).getFieldName1().equals("1")){
                                isPENotified = true;
                            }
                            if(!isPENotified){
                                msgUrl = "tenderid="+tenderid_eval+"&lotId="+tenderLot.getFieldName3()+"&action=notifype&rp=wf&rId=0";
                        %>
                        <!--<a href="<%//=request.getContextPath()%>/EvaluationServlet?tenderid=<%//=tenderid_eval%>&lotId=<%//=tenderLot.getFieldName3()%>&action=notifype&rp=wf&rId=0" onclick="return confirm('Do you really want to notify PE to create Contract Approval workflow?')">Notify To PE</a>-->
                        <label class="notifyPE"> | </label> <a class="notifyPE" style="cursor: pointer;">Notify To PE</a>
                        <%}else{%>
                         <label> | PE Notified</label>
                        <%}}}%>
                <%/*Dohatec End*/%>
                </td>
            </tr>
            <%}}%>
            <!--Round Starts Here-->
               <%
                    int rounds=1;
                    String finalRound = "0";
                    for(SPCommonSearchDataMore round : bidderRounds){
                        boolean isCRSaved = false;
                        boolean isTER3Sign = false;
                        boolean isTER4Sign = false;
                        boolean isTERAll = false;
                        boolean isTER3Notified = false;
                        boolean isTER4Notified = false;
                         isTERSentToAA = false;
                         isTERSentToReviewer = false;
                         isClariAsked = false;
                        boolean isPerfAvail = false;
                        boolean isPerfView = false;
                        boolean isTer12LTM = true;
                        isClariAnswered = false;
                        aaID = null;
                        String roundId = round.getFieldName1();
                        finalRound = roundId;

                        if (isCase2 || isCase3) {
                            List<SPCommonSearchDataMore> TER3Sign = dataMore.getEvalProcessInclude("isEvalReportSigned", tenderid_eval, tenderLot.getFieldName3(), "ter3",roundId,String.valueOf(evalCount));
                            List<SPCommonSearchDataMore> TER4Sign = dataMore.getEvalProcessInclude("isEvalReportSigned", tenderid_eval, tenderLot.getFieldName3(), "ter4",roundId,String.valueOf(evalCount));
                            if(!TER3Sign.isEmpty() && !TER3Sign.get(0).getFieldName1().equals("0")){
                                isTER3Sign = true;
                            }
                            if(!TER4Sign.isEmpty() && !TER4Sign.get(0).getFieldName1().equals("0")){
                                isTER4Sign = true;
                            }
                            TER3Sign = null;
                            TER4Sign = null;
                        }
                        List<SPCommonSearchDataMore> TER3Notified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, tenderLot.getFieldName3(), "ter3",roundId);
                        List<SPCommonSearchDataMore> TER4Notified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, tenderLot.getFieldName3(), "ter4",roundId);
                        if(TER3Notified!=null && (!TER3Notified.isEmpty()) && TER3Notified.get(0).getFieldName1().equals("1")){
                            isTER3Notified = true;
                        }
                        if(TER4Notified!=null && (!TER4Notified.isEmpty()) && TER4Notified.get(0).getFieldName1().equals("1")){
                            isTER4Notified = true;
                        }
                        if((isCase2 || isCase3) && (isTER1Sign && isTER2Sign) && isAllBOQDecrypted){
                            getreportID = dataMore.getEvalProcessInclude("isCRFormulaMadeTORTER", tenderid_eval,tenderLot.getFieldName3(), "TER");
                            if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                                isCRFormulaMadeTER = true;
                                getreportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid_eval,tenderLot.getFieldName3(),roundId);
                                if(getreportID!=null && (!getreportID.isEmpty())){
                                    reportID = getreportID.get(0).getFieldName1();
                                    if(getreportID.get(0).getFieldName2().equals("1")){
                                        isCRSaved = true;
                                    }
                                }
                            }
                        }
                        List<SPCommonSearchDataMore> TERALL = null;
                            if (isCase2) {
                            if(isCRSaved){
                                TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, tenderLot.getFieldName3(), "4", "'TER1','TER2','TER3','TER4'",roundId,String.valueOf(evalCount));
                            }else{
                                TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, tenderLot.getFieldName3(), "2", "'TER1','TER2'",roundId,String.valueOf(evalCount));
                            }
                        }else if(isCase3){
                            TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, tenderLot.getFieldName3(), "4", "'TER1','TER2','TER3','TER4'",roundId,String.valueOf(evalCount));
                        }
                        if(TERALL!=null && (!TERALL.isEmpty()) && TERALL.get(0).getFieldName1().equals("1")){
                            TERALL.clear();
                            if((isCase2 || isCase3) && isCRSaved){
                                TERALL = dataMore.getEvalProcessInclude("isTERSentToAA", tenderid_eval, tenderLot.getFieldName3(), "1",roundId,"");
                            }else{
                               TERALL = dataMore.getEvalProcessInclude("isTERSentToAA", tenderid_eval, tenderLot.getFieldName3(), "0",roundId,"");
                            }

                            if(TERALL!=null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))){
                                isTERSentToAA = true;
                            }

                            /**
                                For Introducing Reviewer (Secretary) in Evaluation Approve
                            */
                            TERALL.clear();
                            TERALL = dataMore.getEvalProcessInclude("isTERSentForReview", tenderid_eval, tenderLot.getFieldName3(), roundId);
                            if(TERALL!=null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))){
                                isTERSentToReviewer = true;
                             }
                             /*
                                Reviewer Code Complied
                            */

                            TERALL.clear();
                            TERALL = dataMore.getEvalProcessInclude("isClarificationAsked", tenderid_eval, tenderLot.getFieldName3(), roundId);
                            if(TERALL!=null && (!TERALL.isEmpty())){
                                isClariAsked = true;
                                aaID = TERALL.get(0).getFieldName1();
                                isClariAnswered = !TERALL.get(0).getFieldName2().trim().equals("");
                            }
                            isTERAll = true;
                        }
                        TERALL=null;
                        if(isCommCP && isPerfSecurityAvail){
                            List<SPCommonSearchDataMore> perfList = dataMore.getEvalProcessInclude("PerfSecurityAvailChk",roundId);
                            if(perfList!=null && (!perfList.isEmpty()) && perfList.get(0).getFieldName1().equals("1")){
                                isPerfAvail = true;
                                perfList.clear();
                            }
                            perfList = dataMore.getEvalProcessInclude("PerfSecurityNaoChk",roundId);
                            if(perfList!=null && (!perfList.isEmpty()) && perfList.get(0).getFieldName1().equals("1")){
                                isPerfView = true;
                                perfList.clear();
                            }
                        }
            %>
            <!--Name of the Bidder for the Round-->
            <tbody id="round_<%=rounds%>">
            <tr><th colspan="2"><%=round.getFieldName3()%> <%=(bidderRounds.size()==rounds)?"(Winning Bidder/Consultant)":""%></th></tr>
            <!--\Name of the Bidder for the Round-->
            <%
              if((isCase2 || isCase3) && (isTER1Sign && isTER2Sign) && isAllBOQDecrypted){
                 String repQString="";
                 String crLabel="View and Save";
                 if(isCRSaved){
                     repQString="&sv=y";
                     crLabel="View";
                 }
                 if(isCommCP || (isCommMem && isCRSaved)){
            %>
            <tr>
                <td class="t-align-left ff">Price Comparison Report</td>


                <td>
                    <%if(isCRFormulaMadeTER){%>
                    <%if(reportID!=null && !reportID.equalsIgnoreCase("null")){%>
                        <a href="<%=request.getContextPath()%>/officer/TenderReport.jsp?tenderid=<%=tenderid_eval%>&repId=<%=reportID%>&istos=y&from=ter<%=repQString%>&rId=<%=roundId%>"><%=crLabel%></a>
                    <%}else{out.print("<div class='responseMsg noticeMsg'>Please Prepare a Price Comparison Report first</div>");}%>
                    <%}else{out.print("<div class='responseMsg noticeMsg'>Report Formula not created.</div>");}%>
                </td>
            </tr>
            <%}}%>
            <%if(((isCase2 || isCase3) && (isTER1Sign && isTER2Sign) && isAllBOQDecrypted && isCRSaved)){%>
            <%if(isCommCP && isPerfSecurityAvail){%>
            <tr>
                <td class="t-align-left ff">Performance Security</td>
                <td>
                    <%if(!isPerfAvail){%>
                    <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderid_eval%>&pkgId=<%=tenderLot.getFieldName3()%>&rId=<%=roundId%>">Add</a>
                    <%}else{if(!isPerfView){%>
                    <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderid_eval%>&pkgId=<%=tenderLot.getFieldName3()%>&isedit=y&rId=<%=roundId%>">Edit</a>&nbsp;|&nbsp;
                    <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderid_eval%>&pkgId=<%=tenderLot.getFieldName3()%>&viewAction=view&isedit=y&rId=<%=roundId%>">View</a>
                    <%}else{%>
                    <a href="AddPerfSecAmount.jsp?tenderId=<%=tenderid_eval%>&pkgId=<%=tenderLot.getFieldName3()%>&viewAction=view&isedit=y&rId=<%=roundId%>">View</a>
                    <%}}%>
                </td>
            </tr>
            <%}%>
            <!--TER 1,2 roundwise in case of pMethod - LTM - Starts-->
            <%
                boolean isTER1SignLTM = false;
                boolean isTER1ConfigLTM = false;
                boolean isTER1NotifiedLTM = false;
                boolean isTER2SignLTM = false;
                boolean isTER2ConfigLTM = false;
                boolean isTER2NotifiedLTM = false;
               if(pMethod.equals("LTM")){
                    if(creationService.evalRepCount(tenderid_eval, tenderLot.getFieldName3(), evalType, "TER1","tecRole='cp' and roundId="+roundId,evalCount)!=0){
                        isTER1ConfigLTM = true;
                    }
                    if(creationService.evalRepCount(tenderid_eval, tenderLot.getFieldName3(), evalType, "TER2","tecRole='cp' and roundId="+roundId,evalCount)!=0){
                        isTER2ConfigLTM = true;
                    }
                    TER1Notified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, tenderLot.getFieldName3(),"ter1", roundId);
                    TER2Notified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, tenderLot.getFieldName3(), "ter2", roundId);
                    if(TER1Notified!=null && (!TER1Notified.isEmpty()) && TER1Notified.get(0).getFieldName1().equals("1")){
                        isTER1NotifiedLTM = true;
                    }
                    if(TER2Notified!=null && (!TER2Notified.isEmpty()) && TER2Notified.get(0).getFieldName1().equals("1")){
                        isTER2NotifiedLTM = true;
                    }
                    TER1Sign = dataMore.getEvalProcessInclude("isEvalReportSigned", tenderid_eval, tenderLot.getFieldName3(), "ter1",roundId,String.valueOf(evalCount));
                    TER2Sign = dataMore.getEvalProcessInclude("isEvalReportSigned", tenderid_eval, tenderLot.getFieldName3(), "ter2",roundId,String.valueOf(evalCount));
                    if(!TER1Sign.isEmpty() && !TER1Sign.get(0).getFieldName1().equals("0")){
                        isTER1SignLTM = true;
                    }
                    if(!TER2Sign.isEmpty() && !TER2Sign.get(0).getFieldName1().equals("0")){
                        isTER2SignLTM = true;
                    }
                    TER1Sign = null;
                    TER2Sign = null;

                    // Start- Rport Sign status for TER1,TER2 (LTM)

                     List<SPCommonSearchDataMore> listReportSingStatus1LTM = commonSearchDataMoreService.getEvalProcessInclude("getTenderReportSignStatus", tenderid_eval, "TEC" , "TER1", "LTM", roundId, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                           if(!listReportSingStatus1LTM.isEmpty()){
                              strReport1SingStatusLTM = listReportSingStatus1LTM.get(0).getFieldName1();
                        }
                          List<SPCommonSearchDataMore> listReportSingStatus2LTM = commonSearchDataMoreService.getEvalProcessInclude("getTenderReportSignStatus", tenderid_eval, "TEC" , "TER2","LTM", roundId, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                           if(!listReportSingStatus2LTM.isEmpty()){
                              strReport2SingStatusLTM = listReportSingStatus2LTM.get(0).getFieldName1();
                        }
                       // End- Rport Sign status for TER1,TER2 (LTM)
            %>
            <tr>
                <td class="t-align-left ff"><%=repLabel%> Evaluation Report 1</td>
                <td>
                    <%
                                viewSignLabel="View and Sign";
                                viewSignList = dataMore.getEvalProcessInclude("isTERSignedLabel", tenderid_eval,tenderLot.getFieldName3(), "TER1", session.getAttribute("userId").toString(),roundId);
                                if(!viewSignLabel.isEmpty()){
                                    viewSignLabel = viewSignList.get(0).getFieldName1();
                                }
                    %>
                    <%if(isCommCP){%>
                        <%if(isTER1ConfigMem){%>
                            <a href="<%=request.getContextPath()%>/officer/ViewTER.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&type=1&stat=<%=evalType%>" target="_blank">View Members Evaluation</a>
                            &nbsp;|&nbsp;
                        <%}%>
                        <%if(!isTER1SignLTM){%>
                            <a href="<%=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&config=y&stat=<%=evalType%>&sign=y&evalCount=<%=evalCount%>&rId=<%=roundId%>">Configure</a>
                        <%}%>
                        <%if(isTER1ConfigLTM){%>
                            <%if(!isTER1SignLTM){%>
                                &nbsp;|&nbsp;
                                <%if(!isTER1NotifiedLTM){%>
                                <a href="<%=request.getContextPath()%>/EvaluationServlet?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&action=notifymail&rp=ter1&rId=<%=roundId%>" onclick="return confirm('Do you really want to Notify Members?')">Notify Members to Sign the Report</a>
                                <%}else{out.print("Members Notified");}%>
                                &nbsp;|&nbsp;
                            <%}%>
                            <a href="<%=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&isview=y&stat=<%=evalType%>&sign=y&evalCount=<%=evalCount%>&rId=<%=roundId%>"><%=viewSignLabel%></a>
                        <b id="SignStatus1" style="color:red">
 <%  if(!"0".equalsIgnoreCase(strReport1SingStatusLTM)){%>
 <%=  " ("+strReport1SingStatusLTM+")" %>
 <%}%>  </b>
                            <%}%>
                    <%}%>
                    <%if(isCommMem){%>
                        <%if(isTER1ConfigLTM){%>
                            <a href="<%=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&isview=y&stat=<%=evalType%>&sign=y&evalCount=<%=evalCount%>&rId=<%=roundId%>"><%=viewSignLabel%></a>
                        <b id="SignStatus1" style="color:red"> 
 <%  if(!"0".equalsIgnoreCase(strReport1SingStatusLTM)){%>
 <%=  " ("+strReport1SingStatusLTM+")" %>
 <%}%> </b>
                            <%}else{out.print("<div class='responseMsg noticeMsg'>Report has not been configured by the Committee Chairperson</div>");}%>
                    <%}%>
                </td>
            </tr>
            <tr>
                <td class="t-align-left ff"><%=repLabel%> Evaluation Report 2</td>
                <td>
                     <%
                        viewSignLabel="View and Sign";
                        viewSignList = dataMore.getEvalProcessInclude("isTERSignedLabel", tenderid_eval,tenderLot.getFieldName3(), "TER2", session.getAttribute("userId").toString(),roundId);
                        if(!viewSignLabel.isEmpty()){
                            viewSignLabel = viewSignList.get(0).getFieldName1();
                        }
                        if(bidderEvaluated !=null && (!bidderEvaluated.isEmpty()) && (bidderEvaluated.get(0).getFieldName1().equals("1"))){
                    %>
                        <%if(isCommCP){%>
                            <%if(isTER2ConfigMem){%>
                                <a href="<%=request.getContextPath()%>/officer/ViewTER.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&type=2&stat=<%=evalType%>" target="_blank">View Members Evaluation</a>
                                &nbsp;|&nbsp;
                            <%}%>
                            <%if(!isTER2SignLTM){%>
                                <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&config=y&stat=<%=evalType%>&sign=y&rId=<%=roundId%>&evalCount=<%=evalCount%>">Configure</a>
                            <%}%>
                            <%if(isTER2ConfigLTM){%>
                                <%if(!isTER2SignLTM){%>
                                    &nbsp;|&nbsp;
                                    <%if(!isTER2NotifiedLTM){%>
                                    <a href="<%=request.getContextPath()%>/EvaluationServlet?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&action=notifymail&rp=ter2&rId=<%=roundId%>" onclick="return confirm('Do you really want to Notify Members?')">Notify Members to Sign the Report</a>
                                    <%}else{out.print("Members Notified");}%>
                                    &nbsp;|&nbsp;
                                <%}%>
                                <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&isview=y&stat=<%=evalType%>&sign=y&rId=<%=roundId%>&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                                           <b id="SignStatus2" style="color:red"> <%  if(!"0".equalsIgnoreCase(strReport2SingStatusLTM)){%>
                            <%=  " ("+strReport2SingStatusLTM+")" %>
 <%}%>  </b>
                                <%}%>
                        <%}%>
                        <%if(isCommMem){%>
                            <%if(isTER2ConfigLTM){%>
                                <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&isview=y&stat=<%=evalType%>&sign=y&rId=<%=roundId%>&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                                            <b id="SignStatus2" style="color:red">
                                                <%  if(!"0".equalsIgnoreCase(strReport2SingStatusLTM))
                                                {%> <%=  " ("+strReport2SingStatusLTM+")" %>
 <%}%>  </b>
                                <%}else{out.print("<div class='responseMsg noticeMsg'>Report has not been configured by the Committee Chairperson</div>");}%>
                        <%}%>
                    <%}else{out.print("<div class='responseMsg noticeMsg'>Technical Evaluation Pending</div>");}%>
                </td>
            </tr>
            <%}%>
            <%
                if(pMethod.equals("LTM")){
                    TERALL = dataMore.geteGPDataMore("isTERSignedByAllLTM", tenderid_eval, tenderLot.getFieldName3(), "2", "'TER1','TER2'",roundId,String.valueOf(evalCount));
                    if(TERALL!=null && (!TERALL.isEmpty()) && TERALL.get(0).getFieldName1().equals("0")){
                        isTer12LTM = false;
                    }
                    //dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, tenderLot.getFieldName3(), "4", "'TER1','TER2','TER3','TER4'",roundId);
                }
            %>

            <!--TER 1,2 roundwise in case of pMethod - LTM - Ends-->
            <%if(isTer12LTM){%>
            <tr>
                <td class="t-align-left ff"><%=repLabel%> Evaluation Report 3</td>
                <td>
                    <%
                        viewSignLabel="View and Sign";
                        viewSignList = dataMore.getEvalProcessInclude("isTERSignedLabel", tenderid_eval,tenderLot.getFieldName3(), "TER3", session.getAttribute("userId").toString(),roundId);
                        if(!viewSignLabel.isEmpty()){
                            viewSignLabel = viewSignList.get(0).getFieldName1();
                        }

                        // report signstatus
                          List<SPCommonSearchDataMore> listReportSingStatus3 = commonSearchDataMoreService.getEvalProcessInclude("getTenderReportSignStatus", tenderid_eval, "TEC" , "TER3","0" ,roundId , null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                           if(!listReportSingStatus3.isEmpty()){
                              strReport3SingStatus = listReportSingStatus3.get(0).getFieldName1();
                        }
                    %>
                    <%if(isCommCP){%>

                      
                        <!--<a href="< %=request.getContextPath()%>/officer/TER3.jsp?tenderid=< %=tenderid_eval%>&lotId=< %=tenderLot.getFieldName3()%>&config=y">Configure</a>
                        &nbsp;|&nbsp;-->
                        <%if(!isTER3Sign){%>
                         <%if(!isTER3Notified){%>
                            <a href="<%=request.getContextPath()%>/EvaluationServlet?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&action=notifymail&rp=ter3&rId=<%=roundId%>" onclick="return confirm('Do you really want to Notify Members?')">Notify Members to Sign the Report</a>
                         <%}else{out.print("Members Notified");}%>
                            &nbsp;|&nbsp;
                        <%}%>
                        <a href="<%=request.getContextPath()%>/officer/TER3.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&rId=<%=roundId%>&isview=yTER3&evalCount=<%=evalCount%>" ><%=viewSignLabel%></a>
                           <!--For Performance security check on TER3 sign-->
                           <!--%if(isPerfSecurityAvail && !isPerfAvail){out.print("onclick=\"return chkPrfAlert();\"");}%-->
                    <b id="SignStatus3" style="color:red">
 <%  if(!"0".equalsIgnoreCase(strReport3SingStatus)){%>
 <%=  " ("+strReport3SingStatus+")" %>
 <%}%>  </b>
                           <%}%>
                    <%if(isCommMem){%>
                        <a href="<%=request.getContextPath()%>/officer/TER3.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&rId=<%=roundId%>&isview=y&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                     <b id="SignStatus3" style="color:red">
 <%  if(!"0".equalsIgnoreCase(strReport3SingStatus)){%>
 <%=  " ("+strReport3SingStatus+")" %>
 <%}%>  </b>
                        <%}%>
                </td>
            </tr>
            <tr>
                <td class="t-align-left ff"><%=repLabel%> Evaluation Report 4</td>
                <td>
                    <%
                        viewSignLabel="View and Sign";
                        viewSignList = dataMore.getEvalProcessInclude("isTERSignedLabel", tenderid_eval,tenderLot.getFieldName3(), "TER4", session.getAttribute("userId").toString(),roundId);
                        if(!viewSignLabel.isEmpty()){
                            viewSignLabel = viewSignList.get(0).getFieldName1();
                        }
                          List<SPCommonSearchDataMore> listReportSingStatus4 = commonSearchDataMoreService.getEvalProcessInclude("getTenderReportSignStatus", tenderid_eval, "TEC" , "TER4","0" , roundId, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                           if(!listReportSingStatus4.isEmpty()){
                              strReport4SingStatus = listReportSingStatus4.get(0).getFieldName1();
                        }
                    %>
                    <%if(isCommCP){%>
                        <!--<a href="< %=request.getContextPath()%>/officer/TER4.jsp?tenderid=< %=tenderid_eval%>&lotId=< %=tenderLot.getFieldName3()%>&config=y">Configure</a>
                        &nbsp;|&nbsp;-->
                        <%if(!isTER4Sign){%>
                         <%if(!isTER4Notified){%>
                            <a href="<%=request.getContextPath()%>/EvaluationServlet?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&action=notifymail&rp=ter4&rId=<%=roundId%>" onclick="return confirm('Do you really want to Notify Members?')">Notify Members to Sign the Report</a>
                            <%}else{out.print("Members Notified");}%>
                            &nbsp;|&nbsp;
                        <%}%>
                        <a href="<%=request.getContextPath()%>/officer/TER4.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&rId=<%=roundId%>&isview=y&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                    <b id="SignStatus4" style="color:red">
 <%  if(!"0".equalsIgnoreCase(strReport4SingStatus)){%>
 <%=  " ("+strReport4SingStatus+")" %>
 <%}%>  </b>
                        <%}%>
                    <%if(isCommMem){%>
                        <a href="<%=request.getContextPath()%>/officer/TER4.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&rId=<%=roundId%>&isview=y&evalCount=<%=evalCount%>"><%=viewSignLabel%></a>
                    <b id="SignStatus4" style="color:red">
 <%  if(!"0".equalsIgnoreCase(strReport4SingStatus)){%>
 <%=  " ("+strReport4SingStatus+")" %>
 <%}%>  </b>
                        <%}%>
                </td>
            </tr>
            <%}%>
            <%}%>
            <%if(isCommCP && isTERAll && isTer12LTM && rounds==bidderRounds.size()){%>
            <tr>
                <%
                    String aaQString = "";
                    String aaLabel = "";
                    if(isCase2 || isCase3){
                        if(isCRSaved){
                            aaLabel="Financial ";
                            aaQString="&rpt=1";
                        }else{
                            aaLabel="Technical ";
                            aaQString="&rpt=0";
                        }
                    }
                %>
                <td class="ff" width="25%">Send Evaluation Report to AA through Workflow</td>
                <td width="75%">
                    <!-- if Clarification Asked  dohatec Start!-->
                    <%if(isClariAsked){
                    if(!isTERSentToAA && !isTERSentToReviewer){%>
                    <a href="SendReportAA.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%><%=aaQString%>&rId=<%=roundId%>">Send</a>
                    <%}else{%>
                    <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%><%=aaQString%>&rId=<%=roundId%>">View</a>
                    <%if(isClariAsked){%>
                    &nbsp;|&nbsp;
                    <a href="GiveClarificationAA.jsp?tenderid=<%=tenderid_eval%>&evalRptToAaid=<%=aaID%>&lotId=<%=tenderLot.getFieldName3()%>&rId=<%=roundId%>"><%if(isClariAnswered){%>Clarification Given<%}else{%>Give Clarification<%}%></a>
                    <%}}%>
                    <%}else{%>
                    <!-- if Clarification Asked  dohatec End!-->
                 <%/*Dohatec Start*/%>
                        <%if(evaluationWorkflowCreated){
                        if(!isTERSentToAA && !isTERSentToReviewer){%>
                        <a href="FileProcessing.jsp?activityid=<%=activity_id%>&objectid=<%=object_id%>&childid=<%=child_id%>&eventid=<%=event_id%>&fromaction=<%=from_action%>">Process file in Workflow</a>
                        <%}else{%>
                         <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%><%=aaQString%>&rId=<%=roundId%>">View</a>
                        <%}}
                        else{%>
                        <label>Workflow yet not configured</label>
                        <%
                            List<SPCommonSearchDataMore> PENotified = dataMore.getEvalProcessInclude("isTERNotified", tenderid_eval, tenderLot.getFieldName3(), "wf",roundId);
                            boolean isPENotified = false;
                            if(PENotified!=null && (!PENotified.isEmpty()) && PENotified.get(0).getFieldName1().equals("1")){
                                isPENotified = true;
                            }
                            if(!isPENotified){
                                msgUrl = "tenderid="+tenderid_eval+"&lotId="+tenderLot.getFieldName3()+"&action=notifype&rp=wf&rId="+roundId;
                        %>
                        <!--<a href="<%//=request.getContextPath()%>/EvaluationServlet?tenderid=<%//=tenderid_eval%>&lotId=<%//=tenderLot.getFieldName3()%>&action=notifype&rp=wf&rId=<%//=roundId%>" onclick="return confirm('Do you really want to notify PE to create Contract Approval workflow?')">Notify To PE</a>-->
                        <label class="notifyPE"> | </label> <a class="notifyPE" style="cursor: pointer;">Notify To PE</a>
                        <%}else{%>
                         <label> | PE Notified</label>
                        <%}}}%>
                 <%/*Dohatec end*/%>
                </td>
            </tr>
            </tbody>
            <%}%>
            <%
                    rounds++;
                }
                    System.out.print("bidderRounds"+bidderRounds.size());
            %>
            <!--Below script for hiding failed rounds-->
            <script type="text/javascript">
            <%for(int k = 1; k<(rounds-1); k++){%>
                $('#round_<%=k%>').hide();
            <%}%>
            </script>
            <!--Round Ends Here-->
            <!--Report here is for viewing and saving in subsquent rounds-->
            <%
                  List<SPCommonSearchDataMore> TER12ALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, tenderLot.getFieldName3(), "2", "'TER1','TER2'","0",String.valueOf(evalCount));
                  boolean isTER12ALL = false;
                  if(TER12ALL!=null && !TER12ALL.isEmpty() && TER12ALL.get(0).getFieldName1().equals("1")){
                        isTER12ALL = true;
                  }
              //BidderFail Added
              if(((isCase2 && isCase2AASent) || isCase3) && (isTER12ALL) && isAllBOQDecrypted && isCommCP && (!isAllBidderFail)){
                 getreportID = dataMore.getEvalProcessInclude("isCRFormulaMadeTORTER", tenderid_eval,tenderLot.getFieldName3(), "TER");
                  if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                          isCRFormulaMadeTER = true;
                   }
                getreportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid_eval,tenderLot.getFieldName3(),"0");
                if(getreportID!=null && (!getreportID.isEmpty())){
                    reportID = getreportID.get(0).getFieldName1();
                }
                getreportID = dataMore.getEvalProcessInclude("FirstRoundAndNoaBiddersChk", tenderid_eval,tenderLot.getFieldName3(),reportID,String.valueOf(evalCount));
               if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                 getreportID = dataMore.getEvalProcessInclude("BidderRoundsforEvaluationChk", finalRound,tenderid_eval,tenderLot.getFieldName3());
                 if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                 String repQString="";
                 String crLabel="View and Save";
                boolean isAllBidderFailAfterNOA = false;
                isFinalize = dataMore.getEvalProcessInclude("getResponsiveBidderCountAfterNOA", tenderid_eval);
                if(isFinalize!=null && (!isFinalize.isEmpty()) && isFinalize.get(0).getFieldName2().equals("0")){
                    isAllBidderFailAfterNOA = true;
                }
                boolean isAllBidderDisqualifiedAfterNOA = false;
                isFinalize1 = dataMore.getEvalProcessInclude("AllBidderDisqualifiedAfterNOA", tenderid_eval,reportID.toString());
                if(isFinalize1!=null && (!isFinalize1.isEmpty()) && isFinalize1.get(0).getFieldName1().equals("1")){
                      isAllBidderDisqualifiedAfterNOA = true;
                  }

              if(!isAllBidderDisqualified)    {
                 if(!isAllBidderFailAfterNOA && !isAllBidderDisqualifiedAfterNOA){

            %>
            <tbody>
            <tr>
                <td class="t-align-left ff">Price Comparison Report</td>

                <td>
                    <%if(isCRFormulaMadeTER){%>
                    <%if(reportID!=null && !reportID.equalsIgnoreCase("null")){%>
                    <% int decryptFormCount = 0;
                        if(pMethod.equals("OSTETM")){

                         TenderOpening opening = new TenderOpening();
                         List<SPCommonSearchData> priceFormList = null;
                         List<SPCommonSearchData> lotIdDesc = commonSearchService.searchData("get2EnvLoIdLotDesc", tenderid_eval, null, null, null, null, null, null, null, null);
                         for (SPCommonSearchData lotdsc : lotIdDesc) {
                         priceFormList = commonSearchService.searchData("getPriceBidfromLotId", tenderid_eval, lotdsc.getFieldName1(), null, null, null, null, null, null, null);
                         }
                         for (SPCommonSearchData priceList : priceFormList) {
                         if(opening.getBidPlainDataCount(priceList.getFieldName5()) != 0) {
                             decryptFormCount++;
                             }
                         }
                         if(decryptFormCount == priceFormList.size()){ %>
                            <a href="<%=request.getContextPath()%>/officer/TenderReport.jsp?tenderid=<%=tenderid_eval%>&repId=<%=reportID%>&istos=y&from=ter<%=repQString%>&frId=<%=finalRound%>"><%=crLabel%></a>
                         <%  } else { %>
                          <a href="<%=request.getContextPath()%>/officer/TenderReport.jsp?tenderid=<%=tenderid_eval%>&repId=<%=reportID%>&istos=y&from=ter<%=repQString%>&frId=<%=finalRound%>" onclick="return chkAllBOQDecrypted()"><%=crLabel%></a>
                     <% }} else { %>
                            <a href="<%=request.getContextPath()%>/officer/TenderReport.jsp?tenderid=<%=tenderid_eval%>&repId=<%=reportID%>&istos=y&from=ter<%=repQString%>&frId=<%=finalRound%>"><%=crLabel%></a>
                    <%}}else{out.print("<div class='responseMsg noticeMsg'>Please Prepare a Price Comparison Report first</div>");}%>
                    <%}else{out.print("<div class='responseMsg noticeMsg'>Report Formula not created.</div>");}%>
                </td>
            </tr>
            </tbody>
            <%}else{
            boolean isTERAll = false;
            isTERSentToAA = false;
            List<SPCommonSearchDataMore> TERALL = dataMore.getEvalProcessInclude("isTERSignedByAll", tenderid_eval, tenderLot.getFieldName3(), "2", "'TER1','TER2'","0",String.valueOf(evalCount));
            if(TERALL!=null && (!TERALL.isEmpty()) && TERALL.get(0).getFieldName1().equals("1")){
            isTERAll = true;
            TERALL.clear();
			if(pMethod.equals("OSTETM") || pMethod.equals("TSTM"))
                    TERALL = dataMore.getEvalProcessInclude("isTERSentToAA1s2eafter", tenderid_eval, tenderLot.getFieldName3(), "0","0",String.valueOf(evalCount));
                        else
					TERALL = dataMore.getEvalProcessInclude("isTERSentToAA", tenderid_eval, tenderLot.getFieldName3(), "0","0",String.valueOf(evalCount));
                if(TERALL!=null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))){
                    isTERSentToAA = true;
                }
                       TERALL.clear();
            TERALL = dataMore.getEvalProcessInclude("isClarificationAsked", tenderid_eval, tenderLot.getFieldName3(),"0");
            if(TERALL!=null && (!TERALL.isEmpty())){
                isClariAsked = true;
                aaID = TERALL.get(0).getFieldName1();
                isClariAnswered = !TERALL.get(0).getFieldName2().trim().equals("");
             }
            }
    //    }

        %>


        <tbody>
            <tr>
                  <td class="ff" width="25%">Send Evaluation Report to HOPA</td>
                  <td>
                    <%if(!isTERSentToAA){%>
                    <a href="SendReportAA.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&rId=0&hopeOnly=1">Send</a>
                    <%}else{%>
                    <a href="ViewEvalReport.jsp?tenderid=<%=tenderid_eval%>&lotId=<%=tenderLot.getFieldName3()%>&rId=0">View</a>
                    <%if(isClariAsked){%>
                    &nbsp;|&nbsp;
                    <a href="GiveClarificationAA.jsp?tenderid=<%=tenderid_eval%>&evalRptToAaid=<%=aaID%>&lotId=<%=tenderLot.getFieldName3()%>&rId=0"><%if(isClariAnswered){%>Clarification Given<%}else{%>Give Clarification<%}%></a>
                    <%}}%> 
                  </td>
            </tr>
        </tbody>



            <%}}%>
            <!--Below script for hiding failed rounds-->
            <script type="text/javascript">
            <%if(rounds>1){%>
                $('#round_<%=(rounds-1)%>').hide();
            <%}%>
            </script>
            <%}}}%>
            <!--/Report here is for viewing and saving in subsquent rounds-->
        <%}%>
        <%if(!isterdisp && isCommMem){%>
        <tbody>
         <tr>
             <td class="t-align-center" colspan="2">
                 <div class="responseMsg noticeMsg">Chairperson has not finalized Evaluation status yet.</div>
             </td>
         </tr>
         </tbody>
         <%}%>
        <%
                            } // END LOT FOR LOOP
            out.print("</table>");
                        } // END IF
                    }
                    tenderLotList = null;

        %>
        <!--Below mention portion is for Decryption of BOQ forms(2 envelope) and is available for Eval/Open Common Committee Member-->
        <!--Price Bid Listing starts by Taher -->
        <%
            List<SPCommonSearchDataMore> getEvalOpenCommonMember = dataMore.geteGPData("getEvalOpenCommonMember", tenderid_eval);
            boolean isDecyptAvail = (!getEvalOpenCommonMember.isEmpty() && getEvalOpenCommonMember.get(0).getFieldName1().equals(session.getAttribute("userId").toString()));
            if (is2Env && isSentToAADecryptAll) {
                TenderOpening opening = new TenderOpening();
                

        %>
        <table width="100%" cellspacing="0" class="tableList_1 t_space">
            <%
                int lotCnt = 0;
                List<SPCommonSearchData> lotIdDesc = commonSearchService.searchData("get2EnvLoIdLotDesc", tenderid_eval, null, null, null, null, null, null, null, null);
                for (SPCommonSearchData lotdsc : lotIdDesc) {
                    if(isDecyptAvail){
                        out.print("<tr id='decLink_"+lotCnt+"'>"
                                     + "<td colspan='3' class='t-align-center'>"
                                     + "(<a href='DecryptAll.jsp?tenderId=" + tenderid_eval + "&stat=eval&lotId=" + lotdsc.getFieldName1() + "'  onClick=\"return decryptAll('0');\">Decrypt All</a>)"
                                     + "</td></tr>");
                    }
                    out.print("<tr><td colspan='3'><table width='100%' class='tableList_1' cellspacing='0'>"
                            + "<tr>"
                                + "<td width='15%' class='t-align-left ff'>"+(procNature.equals("Services")?"Package":"Lot")+" No.</td>"
                                + "<td width='85%'>" + lotdsc.getFieldName3() + "</td>"
                            + "</tr>"
                            + "<tr>"
                                + "<td class='t-align-left ff'>"+(procNature.equals("Services")?"Package":"Lot")+" Description</td>"
                                + "<td>" + lotdsc.getFieldName2() + "</td>"
                                + "</tr>"
                   + "</table></td></tr>");
                    List<SPCommonSearchData> priceFormList = commonSearchService.searchData("getPriceBidfromLotId", tenderid_eval, lotdsc.getFieldName1(), null, null, null, null, null, null, null);
                    int cnt = 1;
                    int cntDec = 1;
                    if (priceFormList.isEmpty()) {
                        out.print("<tr><td colspan='3' style='color: red;' class='t-align-center'>No BOQ Forms Available</td></tr>");
                    } else {%>
                    <tr>
                        <th width="10%" class="t-align-center ff">S No.</th>
                        <th width="65%" class="t-align-center ff">Form Name</th>
                        <th width="25%" class="t-align-center ff">Action</th>
                        <!--<th width="21%" valign="middle" class="t-align-center ff">Action <span id="decLink">(<a href="DecryptAll.jsp?tenderId=< %=tenderId%>&stat=eval">Decrypt All</a>)</span></th>-->
                    </tr>
                    <%
                        for (SPCommonSearchData priceList : priceFormList) {
                            boolean isMultipleFillingYes = false;
                            List<SPTenderCommonData> mulitiList =  tenderCommonService.returndata("isCompReportMultiFilling", priceList.getFieldName5(), null);
                            if(mulitiList!=null && (!mulitiList.isEmpty())){
                                isMultipleFillingYes = Boolean.parseBoolean(mulitiList.get(0).getFieldName1());
                            }
                    %>
            <tr>
                <td class="t-align-center"><%=cnt%></td>
                <td><%=priceList.getFieldName1()%></td>
                <td class="t-align-center">
                    <%
                          //[Already Placed above  if (opening.getCommonMemberCount(tenderid_eval, session.getAttribute("userId").toString()) != 0) {
                                //(No need of this condition(sachin))if (opening.getBidderStatusCount(tenderid_eval) != 0) {
                                    if (opening.getBidPlainDataCount(priceList.getFieldName5()) == 0) {
                                       if(isDecyptAvail){
                    %>
                    <a href="Decrypt.jsp?tenderId=<%=tenderid_eval%>&formId=<%=priceList.getFieldName5()%>&comType=<%=strComType%>" onclick = "return decryptAll('<%=priceList.getFieldName5()%>');">Decrypt</a>
                    <%
                                }else{out.print("Decryption Pending");}} else {
                                   cntDec++;
                                   if(isMultipleFillingYes){
                    %>
                                        <a href="SrvComReport.jsp?formId=<%=priceList.getFieldName5()%>&tenderId=<%=tenderid_eval%>&frm=eval">Comparative Report</a>
                    <%
                                   }else{
                    %>
                                        <a href="ComReport.jsp?formId=<%=priceList.getFieldName5()%>&tenderId=<%=tenderid_eval%>&comType=<%=strComType%>&frm=eval">Comparative Report</a>
                    <%
                                   }
                    %>
                    &nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="IndReport.jsp?tenderId=<%=tenderid_eval%>&formId=<%=priceList.getFieldName5()%>&frm=eval&comType=<%=strComType%>">Individual Report</a>
                    <%
                                }
                            //}
                        //}
                    %>
                </td>
            </tr>
            <%
                                cnt++;
                            }
                            }
                           if (cnt == 1 || cntDec-1==priceFormList.size()) {
             %>
                    <script type="text/javascript">
                        $('#decLink_<%=lotCnt%>').hide();
                    </script>
             <%
                        }
                 lotCnt++;
                    }
            %>
        </table>
        <%
                opening = null;
               }
            }
        %>

        <!-- Dohatec Start --> <!-- Comment for Notifying PE -->
        <table width="100%" cellspacing="0" class="tableList_1" id="tblNotifyPe">
            <tr>
                <td class="ff" width="25%">Comments: </td>
                <td>
                    <textarea name="txtNotifyPe" rows="3" class="formTxtBox_1" id="txtNotifyPe" style="width:400px;"></textarea>
                    <br/>
                    <label id="req_msg" style="color:red;font-weight:bold"></label>
                    <br/>
                    <br/>
                    <label class="btnInput_1">
                        <input type="button" value="Submit" id="btnSubmit" name="submit"/>
                    </label>
                </td>
            </tr>
        </table>

        <input type="hidden" id="hdnMsgUrl" value="<%=msgUrl%>"/>

        <!-- Dialog Box for Notify PE -->
        <div id="dialog-confirm" title="Notify To PE">
            <p id="msgConfirm"></p>
        </div>

        <!-- Dohatec End -->

        <!--Price Bid Listing ends by Taher -->
        <div id="dialog-form" title="Enter Password">
            <label for="password">Password : </label>
            <input type="password" name="password" id="password" value="" class="formTxtBox_1" autocomplete="off" />
            <br/>
            <p align="center" class="validateTips"></p>
        </div>
        <input type="hidden" value="" id="decFormId"/>
    </body>

	<!-- Dohatec Start -->
	<script type="text/javascript">

            $(document).ready(function(){
                $("#tblNotifyPe").hide();
            });

            $(".notifyPE").click(function(){
                $(".notifyPE").hide();
                $("#tblNotifyPe").show();;
            });

            $("#btnSubmit").click(function(){
                $('#req_msg').text("");
                if($("#txtNotifyPe").val() == ""){
                    $('#req_msg').text("Please enter comment");
                }else if($("#txtNotifyPe").val().length > 400){
                    $("#req_msg").text("Comment must not be greater than 400 character");;
                }else{
                    /*var flag = confirm('Do you really want to notify PE to create Contract Approval workflow?');
                    if(flag){
                        var qString  = "&txtRemark=" + $("#txtNotifyPe").val();
                        dynamicFromSubmit("<%=request.getContextPath()%>/EvaluationServlet?"+$('#hdnMsgUrl').val() + qString);
                    */
                    $("#msgConfirm").text("Please be sure that you mentioned the evaluated lowest price and requested to create approval workflow based on that evaluated price as per DOFP.");
                    $(function() {
                        $( "#dialog-confirm" ).dialog({
                            resizable: false,
                            height:175,
                            width:430,
                            modal: true,
                            buttons: {
                                "Yes": function() {
                                    $(this).dialog("close");
                                    var qString  = "&txtRemark=" + $("#txtNotifyPe").val();
                                    dynamicFromSubmit("<%=request.getContextPath()%>/EvaluationServlet?"+$('#hdnMsgUrl').val() + qString);
                                },
                                "No": function() {
                                    $(this).dialog("close");
                                }
                            }
                        });
                    });
                }
            });

        </script>
	<!-- Dohatec End -->

    <script type="text/javascript">
        var password = $( "#password" ),
        allFields = $( [] ).add( password ),
        tips = $( ".validateTips" );

        var check = false;
        var encryptFlag = false;
        function updateTips( t ) {
            tips
                    .text( t )
                    .addClass( "ui-state-highlight" );
            setTimeout(function() {
                    tips.removeClass( "ui-state-highlight", 1500 );
            }, 500 );
        }
        $("#dialog-form").dialog({
                autoOpen: false,
                height: 180,
                width: 280,
                modal: true,
                resizable: false,
                position: 'center',
                buttons: {
                        "Verify Password": function() {
                                var bValid = true;

                                allFields.removeClass( "ui-state-error" );
                                bValid = bValid && checkLength( password, "password", 5, 16 );

                                if (bValid) {
                                    if(document.getElementById("password").value!=""){
                                        $.post("<%=request.getContextPath()%>/LoginSrBean", {param1:$('#password').val(),param2:'<%=session.getAttribute("userId")%>',funName:'verifyPWD'},  function(j){
                                            if(j.charAt(0)=="1"){
                                                //document.getElementById("hdnPwd").value = j.toString().substring(2, j.toString().length);
                                                if($('#decFormId').val()=='0'){
                                                    document.forms[0].action="DecryptAll.jsp?tenderId="+<%=request.getParameter("tenderid")%>+"&lotId="+<%=request.getParameter("lotId")%>+"&stat=eval";
                                                }else{
                                                    document.forms[0].action="Decrypt.jsp?tenderId=<%=tenderid_eval%>&formId="+$('#decFormId').val()+"&comType=<%=strComType%>";
                                                }
                                                document.forms[0].submit();
                                                updateTips("Done");
                                                $("#dialog-form").dialog("close");
                                            }else{
                                                updateTips("Please enter valid password");
                                                $('#password').val("");
                                            }
                                        });
                                    }
                                }
                        }
                },
                close: function() {
                        allFields.val("").removeClass("ui-state-error");
                }
        });

        function decryptAll(formId){
            $("#decFormId").val(formId);
            $("#dialog-form").dialog("open");
            return false;
        }
        function checkLength( o, n, min, max ) {
                if(o.val()==""){
                    o.addClass( "ui-state-error" );
                    updateTips("Please enter password");
                }else{
                    if ( o.val().length > max || o.val().length < min ) {
                            o.addClass( "ui-state-error" );
                            updateTips( "Length of " + n + " must be between " +
                                    min + " and " + max + "." );
                            return false;
                    } else {
                            return true;
                    }
                }
        }

        function checkRegexp( o, regexp, n ) {
                if ( !( regexp.test( o.val() ) ) ) {
                        o.addClass( "ui-state-error" );
                        updateTips( n );
                        return false;
                } else {
                        return true;
                }
        }
        function chkPrfAlert(){
            jAlert("Please add Performance Security.","Performance Security Alert", function(RetVal) {
            });
            return false;
        }
        function chkAllBOQDecrypted(){
            jAlert("Please decrypt all BOQ form first","BOQ Decrypt Alert", function(RetVal) {
            });
            return false;
        }
    </script>
</html>
