<%--
    Document   : EvalClariPostBidder
    Created on : Dec 31, 2010, 3:21:10 PM
    Author     : Administrator
--%>

<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalClariPostBidderSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.BidderClarificationSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Queries / Send Queries To Bidder/Consultant</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function CompareToWithEqual(value,params)
            {
                var mdy = value.split('/')  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');


                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                {
                    //alert('Both Date');
                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                }
                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                {
                    //alert('Both DateTime');
                    var mdyhrsec=mdyhr[1].split(':');
                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec=mdyphr[1].split(':');
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                }
                else
                {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if(a == undefined && b != undefined)
                    {
                        //alert('First Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('Second Date');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                    }
                }
                return Date.parse(date) >= Date.parse(datep);
            }

            //Onclick Event
            function ValidateReposponseDate(){
                var vbool = false;
                     var intSelCnt=0;
                     alert(elm);
                    for (i = 0; i < elm.length; i++) {
                        if (elm[i].type=="checkbox" && elm[i].name=="chkQuestion"){
                            if(elm[i].checked){
                                intSelCnt++;
                            }

                        }

                    }
                    alert(intSelCnt+'intSelCnt')
              if(intSelCnt==0){
                  //alert('here');
                  document.getElementById('spanckboxResponse').innerHTML='<div class="reqF_1">Please Select at least one query</div>';
                     vbool = true;
                }
                if(document.getElementById('txtResponseDate')!= null) {
                    if(CompareToWithEqual(document.getElementById('txtResponseDate').value,document.getElementById('hdnvalidityDate').value))
                    {
                        document.getElementById('spantxtResponse').innerHTML='<div class="reqF_1">Last Date of Response can not be greater than tender validity date.</div>';
                        vbool = true;
                    }
                    else
                    {
                        document.getElementById('spantxtResponse').innerHTML='';
                    }
                }
                if(vbool==true)
                {

                    return false;
                }
                else
                    return true;
            }

        </script>

        <script type="text/javascript">

            $(document).ready(function() {
                $("#frmEvalPost").validate({
                    rules: {
                         txtResponseDate:{required:true,CompareToForToday:"#egp"},
                        txtRemarks: {required:true,maxlength:1000},
                        chkQuestion:{required:true}
                    },
                    messages: {
                        txtResponseDate: {required:"<div class='reqF_1'>Please Enter Date.</div>",
                            CompareToForToday:"<div class='reqF_1'>Last Date of Response must be greater than current date."},
                        txtRemarks: {required:"<div class='reqF_1'>Please enter Remarks.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 1000 characters are allowed.</div>"},
                        chkQuestion:{required:"<div class='reqF_1'>Please Select atleast one query.</div>"}
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "txtResponseDate"){
                            error.insertAfter("#imgDtOfIns");
                        }
                        else if(element.attr("name") == "chkQuestion"){
                            error.insertAfter("#spanckboxResponse");
                        }
                        else{
                            error.insertAfter(element);
                    }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function showHideVal(abc){
    var i = abc.id;
    //alert(i);
    var len = i.length;
    var index = i.indexOf('_');
    i = i.substr(index+1, len);
    //len = i.length;
    //alert(i);
    $('#editTxttd_'+i).show()
    $('#updateValtd_'+i).hide()
    $('#editLbltd_'+i).hide()

}

function editData(abc){
    var i  = abc.id;
    var len = i.length;
    var index = i.indexOf('_');
    i  = i.substr(index+1, len);
    var oldVal = document.getElementById("editHdn_"+i).value;
    var curVal = document.getElementById("editTxtArea_"+i).value;
    var queId = document.getElementById("queId_"+i).value;
    if(oldVal==curVal){

        $('span.#updateVal_'+i).html(curVal);
        $('#updateVal_'+i).show();
         $('#updateValtd_'+i).show();
          $('#editTxttd_'+i).hide()
        return false;
    }
    $('span.#updateMsg_'+i).show("Question Updating please wait...");
    $('span.#updateMsg_'+i).show();
    $.post("<%=request.getContextPath()%>/CommonServlet", {updateQuest:curVal,queId:queId,funName:'updateQue'},  function(j){
                    var msg = j.toString();
                    if(msg.length==2){
                        //$('#editLbltd_'+i).hide()
                         $('#editTxttd_'+i).hide()
                         $('span.#updateVal_'+i).html(curVal);
                         $('#editHdn_'+i).val(curVal);
                         $('#updateValtd_'+i).show();
                    }

                                            });

}
</script>
<script type="text/javascript">
function CheckAll(obj){
    //alert(obj.checked);
    var state=obj.checked;
    //alert(state);
    var elm = obj.form.elements;
    for (i = 0; i < elm.length; i++) {
        if (elm[i].type=="checkbox"){
            elm[i].checked=state;
        }
    }


}
</script>
<%--<script type="text/javascript">
function selectAll(ckBox){
    alert(ckBox+'ckBox');
    var formloop = ckBox.id;
    var len = formloop.length;
    var index = formloop.indexOf('_');
    formloop  = formloop.substr(index+1, len);
    alert(formloop+'formloop');
   // alert(formloop);
   var counter = document.getElementById('counterQuest_'+formloop).value;
    alert(counter+'counter');
    var allChk = document.getElementById('chkQuestion_'+formloop);
    if(allChk.checked){
            for(j=1;j<counter;j++){
            var id = "chkQuestion_"+j+"_"+formloop;
            alert(id+'id');
            document.getElementById(id).checked = true;
            }
    }else{
           for(j=1;j<counter;j++){
            var id = "chkQuestion_"+j+"_"+formloop;
            alert(id+'id');
                document.getElementById(id).checked = false;
            }
    }
}
</script>--%>
    </head>
    <body>
        <%
                String tenderId;
                tenderId = request.getParameter("tenderId");
                String userId = "", usId = "";
                String referer = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                if (request.getParameter("uId") != null && !"".equalsIgnoreCase(request.getParameter("uId"))) {
                    usId = request.getParameter("uId");
                }

                if(request.getParameter("hdnReferer")!=null && !"".equalsIgnoreCase(request.getParameter("hdnReferer"))){
                    referer = request.getParameter("hdnReferer");
                }
                 else if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                }


             if (request.getParameter("btnPost") != null) {
                EvalClariPostBidderSrBean evalClSrBean = new EvalClariPostBidderSrBean();

                // UPDATE queSentByTec BY QUESTION-ID
                for (String chkQuestion : request.getParameterValues("chkQuestion")) {
                    boolean flg = evalClSrBean.updEvalFormQues(chkQuestion);
                }
                String s_tenRefNo="";
                if (request.getParameter("tenRefNo") != null && !"".equalsIgnoreCase(request.getParameter("tenRefNo"))) {
                    s_tenRefNo = request.getParameter("tenRefNo");
                }
                // INSERT RESPONSE DATE AND REMARKS
                String remarks = request.getParameter("txtRemarks");
                String respDt = request.getParameter("txtResponseDate");
                String[] dtArr = respDt.split("/");
                respDt = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];
                evalClSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                /* Dohatec Start */
                respDt = respDt + " 23:58:59"; // Added by dohatec for insert response date time
                /*Dohatec End */
                boolean flgResp = evalClSrBean.insBidderResponse(tenderId, usId, userId, remarks, respDt,s_tenRefNo);
                                
                if (flgResp) {

                    if (request.getParameter("lnk")!=null && "view".equalsIgnoreCase(request.getParameter("lnk"))) {
                       response.sendRedirect("ViewClarify.jsp?tenderId=" + tenderId + "&msgId=success&st=" + request.getParameter("st"));
                    } else {
                    response.sendRedirect("Evalclarify.jsp?tenderId=" + tenderId + "&msgId=success&st=" + request.getParameter("st"));
                    }



                } else {
                    response.sendRedirect("EvalClariPostBidder.jsp?tenderId=" + tenderId + "&uId=" + usId + "&msgId=error&st=" + request.getParameter("st"));
                }
            }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <%
                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                // Get tender validity date
                List<SPTenderCommonData> tenderValidity = tenderCS.returndata("GetTendervaliditydate",tenderId,"0");
                String validityDate = "";
                if (!tenderValidity.isEmpty()) {
                    validityDate = tenderValidity.get(0).getFieldName1();
                    }
            %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">View Queries / Send Queries To Bidder/Consultant
                    <span style="float:right;">

                        <%if (request.getParameter("lnk")!=null && "view".equalsIgnoreCase(request.getParameter("lnk"))){%>
                        <a href="ViewClarify.jsp?tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>" class="action-button-goback">Go Back</a>
                        <%} else {%>
                        <a href="Evalclarify.jsp?tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>" class="action-button-goback">Go Back</a>
                    </span>
                        <%}%>
                        </div>



                <input type="hidden" name="hdnvalidityDate" id="hdnvalidityDate"  value="<%=validityDate%>"/>
                <%
                       pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%
                       boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                %>
                <div>&nbsp;</div>

                    <div class="tabPanelArea_1" style="border: none; padding: 0px;">
                    <%if (request.getParameter("msgId") != null) {
                        String msgId = "", msgTxt = "";
                        boolean isError = false;
                        msgId = request.getParameter("msgId");
                        if (!msgId.equalsIgnoreCase("")) {
                            if (msgId.equalsIgnoreCase("error")) {
                                isError = true;
                                msgTxt = "There was some error.";
                            } else {
                                msgTxt = "";
                            }
                    %>
                    <%if (isError) {%>
                    <div class="responseMsg errorMsg"><%=msgTxt%></div>
                    <%}%>
                    <%}
                                }%>

                    <% for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", usId, tenderId))
                        {
                    %>
                        <table width="100%" cellspacing="0" class="tableList_k">
                            <tr>
                                <th colspan="2" class="t_align_left ff">Company Details</th>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Company Name :</td>
                                <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                            </tr>
                        </table>
                    <%
                        } // END FOR LOOP OF COMPANY NAME
                    %>
                    <form id="frmEvalPost" action="EvalClariPostBidder.jsp?&uId=<%=usId%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>&lnk=<%=request.getParameter("lnk")%>" name="frmEvalPost" method="POST">
                        <input type="hidden" name="tenRefNo" value="<%=toextTenderRefNo%>" />
                        <input type="hidden" name="tenRefNo" value="<%=toextTenderRefNo%>" />
                        <%
                            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            if (isTenPackageWis) {
                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <td colspan="2" class="t-align-left ff">Package Information</td>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space b_space">
                        <tr>
                            <th style="float:right;"> <b>Select All : </b>
                                <input type="checkbox" name="cbAll" id="cbAll" onclick="return CheckAll(this);">
                                <span id="spanckboxResponse"></span>
                            </th>
                        </tr>
                        </table>
                        <%
                            }  // END packageList FOR LOOP
                            // START FOR LOOP OF FORM NAME
                                int formCnt = 0;
                            for (SPCommonSearchData formNames : commonSearchService.searchData("GetTenderFormsForSeekClari", tenderId, "0",
                                    "0", usId, userId, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_k t_space b_space">
                        <tr>
                            <th> <b>Form Name : </b><a href="ViewEvalBidform.jsp?tenderId=<%=tenderId%>&uId=<%=usId%>&formId=<%=formNames.getFieldName1()%>&lotId=0&bidId=<%=formNames.getFieldName3()%>&action=Edit&isSeek=true" target="_blank"><%=formNames.getFieldName2()%></a>
                                <span class="c-alignment-right"><a href="EvalDocList.jsp?tenderId=<%=tenderId%>&formId=<%=formNames.getFieldName1()%>&uId=<%=usId%>" class="action-button-download">Download Documents</a></span>
                            </th>
                        </tr>
                        </table>

                              <!-- START: TSC Comments-->
                        <table id="tblTSCComments" width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="4%" class="t-align-center">Sl. No.</th>
                                <th width="21%" class="t-align-center">Posted By</th>
                                <th class="t-align-center">Comments</th>
                            </tr>
                            <%
                                TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                int cntCom = 0;
                                for (SPTenderCommonData objSPTCD : objTSC.returndata("getTSCComments", formNames.getFieldName1(), usId)) {
                                    cntCom++;
                            %>
                            <tr
                                <%if (Math.IEEEremainder(cntCom, 2) == 0) {%>
                                class="bgColor-Green"
                                <%} else {%>
                                class="bgColor-white"
                                <%   }%>
                                >
                                <td class="t-align-center"><%=cntCom%></td>
                                <td><%=objSPTCD.getFieldName3()%></td>
                                <td><%=objSPTCD.getFieldName4()%></td>
                            </tr>
                            <%}%>
                            <%if (cntCom == 0) {%>
                            <tr>
                                <td colspan="3">
                                    No Comments found.
                                </td>
                            </tr>
                            <%}%>
                        </table>
                        <!-- END: TSC Comments-->

                        <%
                            // START FOR LOOP OF TEC / TSC MEMBER NAME BY FORM-ID AND TENDER-ID.
                            int cntMem = 1;
                            for (SPCommonSearchData sptcd : commonSearchService.searchData("GetEvalComMemForBidderClari", tenderId, formNames.getFieldName1(), userId, null, null, null, null, null, null)) {
                        %>
                       <table width="100%" cellspacing="0" class="tableList_k t_space">
                            <tr>
                                <th colspan="4" class="t-align-left"><b>TEC / TSC Member Name :</b> <%=sptcd.getFieldName3()%>
                                </th>
                            </tr>
                            <tr>
                                <th class="t-align-center" width="4%">Sl. No.</th>
                                <th class="t-align-center" width="76%">Queries</th>
                                <th class="t-align-center" width="10%">Edit</th>
                                <th class="t-align-center" width="10%">Select
                                <%--<input type="checkbox" id="chkQuestion_<%=cntMem%>_<%=formCnt%>" onclick="return selectAll(this);"/>--%>
                                </th>
                            </tr>
                            <%
                                int cntQuest = 1;
                                // START FOR LOOP OF QUESTION POSTED BY TEC / TSC MEMBERS.
                                for (SPCommonSearchData question : commonSearchService.searchData("GetEvalComMemQuestionByMem", tenderId,
                                        formNames.getFieldName1(),
                                        sptcd.getFieldName2(), usId, null, null, null, null, null)) {
                                        // HIGHLIGHT THE ALTERNATE ROW
                                        if(Math.IEEEremainder(cntQuest,2)==0) {
                            %>

                            <tr class="bgColor-Green">
                             <% } else { %>
                            <tr>
                            <% } %>

                                <td class="t-align-center"><%=cntQuest%>
                                </td>
                                <td width="80%" class="t-align-left" id="editLbltd_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>"><%=URLDecoder.decode(question.getFieldName3(),"UTF-8")%>
                                </td>
                                <td id="updateValtd_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" width="80%" style="display: none;">
                                    <span id="updateVal_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>"></span>
                                </td>
                                <td id="editTxttd_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" width="80%" style="display: none;">
                                    <textarea rows="5" id="editTxtArea_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" name="txtEdit__<%=cntQuest%>" class="formTxtBox_1" style="width: 99%;"><%=URLDecoder.decode(question.getFieldName3(),"UTF-8")%></textarea>
                                    &nbsp;
                                    <label style="width: 50px;" >
                                        <a class="anchorLink"id="btnUpdate_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" onclick="return editData(this);" style="text-decoration: none; color: white;">Update</a>
                                    </label>
                                    <span id="updateMsg_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" class="reqF_1" style="display: nono;"></span>
                                </td>
                                 <td width="5%" class="t-align-center">
                                   <label style="width: 40px;" class="t-align-center" >
                                       <a onclick="return showHideVal(this);" id="btnEdit_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>"  class="anchorLink t-align-center" style="text-decoration: none; color: white;" >Edit</a>
                                   </label>
                                 </td>
                                <td width="9%" class="t-align-center">
                                    <input type="checkbox" checked="checked" value="<%=question.getFieldName1()%>" name="chkQuestion" id="chkQuestion_<%=cntQuest%>_<%=formCnt%>" />
                                    <label for="checkbox2"></label>
                                    <input type="hidden" id="queId_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" value="<%=question.getFieldName1()%>" />
                                    <input type="hidden" id="editHdn_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" value="<%=question.getFieldName3()%>" />
                                </td>

                            </tr>
<!--                            <tr id="" style="display: none;">

                                <td valign="middle" class="t-align-center">


                                   <input type="hidden" name="questionId_<%=cntQuest%>" value="<%=question.getFieldName1()%>">
                                </td>
                            </tr>-->
                            <%
                                    cntQuest++;
                                }if(cntQuest==1){
                            %>
                            <tr>
                                <td colspan="4" class="t-align-center">No queries are posted yet</td>
                            </tr>
                            <%}%>
                             <input type="hidden" value="<%=cntQuest%>" id="counterQuest_<%=formCnt%>">
                        </table>



                        <%      cntMem++;} // END FOR LOOP of Member
                            formCnt++;}  // END formNames FOR LOOP
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <td width="12%" class="t-align-left ff">Last Date of Response : <span class="mandatory">*</span></td>
                                <td width="88%" class="t-align-left">
                                    <input name="txtResponseDate" type="text" class="formTxtBox_1" onclick="GetCal('txtResponseDate','txtResponseDate');" id="txtResponseDate" style="width:70px;" readonly="true" />
                                    <img id="imgDtOfIns" name="imgDtOfIns" onclick="GetCal('txtResponseDate','imgDtOfIns');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                                    <span id="spantxtResponse" class="mandatory"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="t-align-left ff">Remarks : <span class="mandatory">*</span></td>
                                <td width="88%" class="t-align-left">
                                    <textarea rows="5" id="txtRemarks" name="txtRemarks" class="formTxtBox_1" style="width:99%"></textarea>
                                </td>
                            </tr>
                        </table>
                        <div class="t-align-center t_space">
                            <label class="formBtn_1">
                                <input name="btnPost" type="submit" onclick="return ValidateReposponseDate();" id="btnPost" value="Post Questions to Tenderer" />
                            </label>
                        </div>
                        <%
                            } else {
                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-center ff">Package Information</td>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%  } // END PACKAGE FOR LOOP
                                int formCnt = 0;
                            for (SPCommonSearchData lotList : commonSearchService.searchData("GetBidderLotsForClarification", tenderId, "Lot", usId, userId, null, null, null, null, null)) {

                        %>
                        <div class="tabPanelArea_1 t_space">
                        <table width="100%" cellspacing="0" class="tableList_1">

                            <tr>
                                <td width="22%" class="t-align-left ff">Lot No. :</td>
                                <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Lot Description :</td>
                                <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%

                            // START FOR LOOP OF FORM NAME BY LotId
                                for (SPCommonSearchData formNames : commonSearchService.searchData("GetTenderFormsForSeekClari", tenderId, "1",
                                        lotList.getFieldName3(), usId, userId, null, null, null, null)) {
                                    formCnt++;
                        %>
                        <table width="100%" cellspacing="0" class="tableList_k t_space">
                        <tr>
                            <th  class="t-align-left"> <b>Form Name : </b><a href="ViewEvalBidform.jsp?tenderId=<%=tenderId%>&uId=<%=usId%>&formId=<%=formNames.getFieldName1()%>&lotId=0&bidId=<%=formNames.getFieldName3()%>&action=Edit&isSeek=true" target="_blank"><%=formNames.getFieldName2()%></a>
                                <span class="c-alignment-right"><a href="EvalDocList.jsp?tenderId=<%=tenderId%>&formId=<%=formNames.getFieldName1()%>&uId=<%=usId%>" class="action-button-download">Download Documents</a></span>
                            </th>
                        </tr>
                        </table>
                        <%
                            // START FOR LOOP OF TEC / TSC MEMBER NAME BY FORM-ID AND TENDER-ID.
                        int cntMem = 1;
                            for (SPCommonSearchData sptcd : commonSearchService.searchData("GetEvalComMemForBidderClari", tenderId, formNames.getFieldName1(), userId, null, null, null, null, null, null)) {
                                cntMem++;
                        %>
                        <table width="100%" cellspacing="0" class="tableList_k t_space">
                            <tr>
                                <th colspan="4" class="t-align-left"><b>TEC / TSC Member Name :</b> <%=sptcd.getFieldName3()%>
                                </th>
                            </tr>
                            <tr>
                                <th class="t-align-center" width="4%">Sl. No.</th>
                                <th class="t-align-center" width="76%">Queries</th>
                                <th class="t-align-center" width="10%">Edit</th>
                                <th class="t-align-center" width="10%">Select
                                <%--<input type="checkbox" id="chkQuestion_<%=cntMem%>_<%=formCnt%>" onclick="return selectAll(this);"/>--%>
                                </th>
                            </tr>
                            <%
                                int cntQuest = 1;
                                // START FOR LOOP OF QUESTION POSTED BY TEC / TSC MEMBERS.
                                for (SPCommonSearchData question : commonSearchService.searchData("GetEvalComMemQuestionByMem", tenderId,
                                        formNames.getFieldName1(),
                                        sptcd.getFieldName2(), usId, null, null, null, null, null)) {
                                        // HIGHLIGHT THE ALTERNATE ROW
                                        if(Math.IEEEremainder(cntQuest,2)==0) {
                            %>

                            <tr class="bgColor-Green">
                             <% } else { %>
                            <tr>
                            <% } %>

                                <td class="t-align-center"><%=cntQuest%>
                                </td>
                                <td width="80%" class="t-align-left" id="editLbltd_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>"><%=question.getFieldName3()%>
                                </td>
                                <td id="updateValtd_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" width="80%" style="display: none;">
                                    <span id="updateVal_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>"></span>
                                </td>
                                <td id="editTxttd_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" width="80%" style="display: none;">
                                    <textarea rows="5" id="editTxtArea_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" name="txtEdit__<%=cntQuest%>" class="formTxtBox_1" style="width: 99%;"><%=question.getFieldName3()%></textarea>
                                    &nbsp;
                                    <label style="width: 50px;">
                                        <a class="anchorLink"id="btnUpdate_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" onclick="return editData(this);" style="text-decoration: none; color: white;">Update</a>
                                    </label>
                                    <span id="updateMsg_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" class="reqF_1" style="display: nono;"></span>
                                </td>
                                 <td width="5%" class="t-align-center">
                                   <label style="width: 40px;" >
                                       <a onclick="return showHideVal(this);" id="btnEdit_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>"  class="anchorLink" style="text-decoration: none; color: white;" >Edit</a>
                                   </label>
                                 </td>
                                <td width="9%" class="t-align-center">
                                    <input type="checkbox" checked="checked" value="<%=question.getFieldName1()%>" name="chkQuestion" id="chkQuestion_<%=cntQuest%>_<%=formCnt%>" />
                                    <label for="checkbox2"></label>
                                    <input type="hidden" id="queId_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" value="<%=question.getFieldName1()%>" />
                                    <input type="hidden" id="editHdn_<%=cntQuest%>_<%=cntMem%>_<%=formCnt%>" value="<%=question.getFieldName3()%>" />
                                </td>

                            </tr>
<!--                            <tr id="" style="display: none;">

                                <td valign="middle" class="t-align-center">


                                   <input type="hidden" name="questionId_<%=cntQuest%>" value="<%=question.getFieldName1()%>">
                                </td>
                            </tr>-->
                            <%
                                    cntQuest++;
                                }
                            %>
                             <input type="hidden" value="<%=cntQuest%>" id="counterQuest_<%=formCnt%>">
                             <input type="hidden" value="<%=cntQuest%>" id="memQuest_<%=formCnt%>">
                        </table>
                        <%
                                    } // END FOR LOOP OF TEC / TSC MEMBER
                                }  // END FOR LOOP OF FORM NAME
                        %>

                         </div>
                        <% } // END FOR LOOP OF LOT  %>

                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <td width="18%" class="t-align-left ff">Last Date of Response :<span class="mandatory">*</span>
                                <input type="hidden" value="<%=formCnt%>" id="formCount">
                                </td>
                                <td width="82%" class="t-align-left">
                                    <input name="txtResponseDate" type="text" class="formTxtBox_1" id="txtResponseDate" onclick="GetCal('txtResponseDate','imgDtOfIns');" style="width:70px;" readonly="true" />
                                    <img id="imgDtOfIns" name="imgDtOfIns" onclick="GetCal('txtResponseDate','imgDtOfIns');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                                    <span id="spantxtResponse" class="mandatory"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="t-align-left ff">Remarks :<span class="mandatory">*</span></td>
                                <td width="88%" class="t-align-left">
                                    <textarea rows="5" id="txtRemarks" name="txtRemarks" class="formTxtBox_1" style="width:99%"></textarea>
                                </td>
                            </tr>
                        </table>
                        <div class="t-align-center t_space">

                            <label class="formBtn_1">
                                <input name="btnPost" id="btnPost" type="submit" value="Post to Tenderer" /></label>
                        </div>
                        <% }%>

                        <input type="hidden" id="hdnReferer" name="hdnReferer" value="<%=referer%>">
                    </form>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
