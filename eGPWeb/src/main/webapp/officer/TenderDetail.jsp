<%-- 
    Document   : TenderDetail
    Created on : Nov 16, 2010, 11:02:01 AM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
    </head>
    <body>
      <div class="tableHead_1 t_space">Tender Detail</div>
            <table width="100%" cellspacing="10" class="tableView_1">
                <tr>
                    <td width="100" class="ff">Tender Id :</td>
                    <td>11</td>
                    <td width="124" class="ff">Tender No : </td>
                        <td>Test - UVM-01</td>
                </tr>

                <tr>
                    <td class="ff">Due date &amp; time :</td>
                    <td>02/06/2010 17:00</td>
                    <td class="ff">Opening date &amp; time : </td>
                    <td>02/06/2010 17:00</td>
                </tr>
                <tr>
                    <td class="ff">Department :</td>
                    <td colspan="3">NPCIL Group</td>
                </tr>
                <tr>
                    <td class="ff">Brief :</td>
                    <td colspan="3">Chittagong</td>
                </tr>
                <tr>
                    <td class="ff">&nbsp;</td>
                    <td colspan="3">
                        <a href="#" class="action-button-notice" title="Tender/Proposal Notice">Tender Notice</a>
                        <a href="#" class="action-button-download" title="Download Documents">Download Documents</a>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
