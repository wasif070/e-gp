
<%@page import="java.net.URLEncoder"%>
<%@page import="org.json.JSONObject"%>
<%--
    Document   : CreateForm
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>
<%@page import="com.cptu.egp.eps.web.utility.FilePathUtility"%>
<%@page import="com.csvreader.CsvReader"%>
<%@page import="com.csvreader.CsvReader"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.model.table.TblTenderListBox"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListCellDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblListBoxMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblListCellDetail"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ListIterator,java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderColumns" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCells" %>
<jsp:useBean id="tableMatrix"  class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />
<jsp:useBean id="tenderFrmSrBean" class="com.cptu.egp.eps.web.servicebean.TenderFormSrBean" />
<jsp:useBean id="comboService" class="com.cptu.egp.eps.web.servicebean.TenderComboSrBean" />

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<html xmlns="http://www.w3.org/1999/xhtml" style="overflow-x: no-content;">
    <head>
       
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <% if (request.getParameter("action") != null && "readFile".equalsIgnoreCase(request.getParameter("action"))) { %>
        <script type="text/javascript">
            window.onerror = function () {
                //alert(e);
                if (document.getElementById("errorFlag").value == 0) {
                    alert('Improper data file. Actual columns and CSV columns are not same or Data in a file are not comma(,) separated');
                    document.getElementById("errorFlag").value = 1;
                }
            }
        </script>
        <script type="text/javascript">
            var csv_data = new Array();
            var arr = new Array();
            var CSVRowCnt = 0;
            var CSVColumnCnt = 0;
            <%
            String CsvPath = FilePathUtility.getFilePath().get("CSVFiles");
            String pageName = "";

            if (request.getParameter("pageName") != null && !"".equalsIgnoreCase(request.getParameter("pageName"))) {
                pageName = request.getParameter("pageName");
            }
            CsvReader CSVRecords = new CsvReader(CsvPath + pageName);
            %>
            <%      int i = 0;
            while (CSVRecords.readRecord()) {
                String[] Records = CSVRecords.getValues();
                for (String Record : Records) {
            %>
            arr.push("<%=Record%>");
            <%              i++;
            }
            %>
            csv_data.push(arr);
            CSVColumnCnt = <%=Records.length%>;
            arr = [];
            <%
                        }
                        CSVRecords.close();
            %>
            CSVRowCnt = csv_data.length;
            <%
                    }
            %>
        </script>
        <%
            int tenderId = 0;
            int sectionId = 0;
            int formId = 0;
            int tableId = 0;
            int pkgOrLotId = -1;
            short cols = 0;
            short rows = 0;
            String tableName = "";
            String tableHeader = "";
            String tableFooter = "";
            int templateTableId = 0;
            boolean isBOQForm = false;
            boolean isInEditMode;
            String colHeader = "";
            byte filledBy = 0;
            byte dataType = 0;
            String showHide = "";
            String colType = "";
            short sortOrder = 0;
            String frmName = "";
            StringBuffer frmHeader = new StringBuffer();
            StringBuffer frmFooter = new StringBuffer();
            String isBOQ = "";
            int templateFormId = 0;
            String strBrowserInfo = "";
            String procNature = "";
            boolean isWorks = false;
            String contractType = "";

            if (request.getParameter("tenderId") != null) {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
            }
            if (request.getParameter("sectionId") != null) {
                sectionId = Integer.parseInt(request.getParameter("sectionId"));
            }
            if (request.getParameter("formId") != null) {
                formId = Integer.parseInt(request.getParameter("formId"));
            }
            if (request.getParameter("tableId") != null) {
                tableId = Integer.parseInt(request.getParameter("tableId"));
            }
            if (request.getParameter("porlId") != null) {
                pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
            }

            String formType = "";
            /* Dohatec Start   */
            String tenderFormType = "";
            int frmCount = 0;
            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            List<SPCommonSearchData> tenderFormTypeList = commonSearchService.searchData("getFormTypeForAlertMessage", String.valueOf(tenderId), String.valueOf(sectionId), String.valueOf(formId), null, null, null, null, null, null);
            if (!tenderFormTypeList.isEmpty()) {
                for (SPCommonSearchData formname : tenderFormTypeList) {
                    frmCount = frmCount + 1;
                    tenderFormType = formname.getFieldName11();
                    System.out.println("aaaaa" + formname.getFieldName11());

                }

            }
        %>

        <input type="hidden" name="FormTypeTender" id="FormTypeTender" value="<%=tenderFormType%>"/>
        <%
            /* Dohatec End  */

            List<Object[]> objComboCalWise = comboService.getTenderListBoxCatWise(formId, "Yes");
            List<Object[]> objComboWOCalWise = new ArrayList<Object[]>();

            List<Object> listFormType = tenderFrmSrBean.getFormType(formId);
            if (listFormType != null && !listFormType.isEmpty()) {
                formType = (listFormType.get(0)).toString();
            }

            List<TblTenderDetails> list = new ArrayList<TblTenderDetails>();
            list = tenderFrmSrBean.getTenderDetails(tenderId);
            if (list != null && !list.isEmpty()) {
                contractType = list.get(0).getContractType();
            }

            if ("12".equalsIgnoreCase(formType) && "Lump - Sum".equalsIgnoreCase(contractType)) {
                objComboWOCalWise = comboService.getTenderListBoxForPS(formId, "No");
            } else {
                objComboWOCalWise = comboService.getTenderListBoxCatWise(formId, "No");
            }

            List<com.cptu.egp.eps.model.table.TblTenderForms> frm = tenderFrmSrBean.getFormDetail(formId);
            List<com.cptu.egp.eps.model.table.TblTenderTables> tblInfo = tableMatrix.getTenderTablesDetail(tableId);
            ListIterator<TblTenderColumns> tblColumnsDtl = tableMatrix.getColumnsDtls(tableId, true).listIterator();
            ListIterator<TblTenderCells> tblCellsDtl = tableMatrix.getCellsDtls(tableId).listIterator();

            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            procNature = commonService.getProcNature("" + tenderId).toString();

            if ("Works".equalsIgnoreCase(procNature)) {
                isWorks = true;
            }

            isBOQForm = tableMatrix.isPriceBidForm(formId);
            if (tblInfo != null) {
                if (tblInfo.size() >= 0) {
                    tableName = tblInfo.get(0).getTableName();
                    cols = tblInfo.get(0).getNoOfCols();
                    rows = tblInfo.get(0).getNoOfRows();
                    templateTableId = tblInfo.get(0).getTemplatetableId();
                    tableHeader = tblInfo.get(0).getTableHeader();
                    tableFooter = tblInfo.get(0).getTableFooter();
                }
            }

            isInEditMode = tableMatrix.isEntryPresentForCols(tableId);
            //out.println("Edit  rows : "+rows+" cols : "+cols+" isBOQForm : "+isBOQForm+" templateTableId :"+templateTableId+"edit moede : "+isInEditMode);

            if (templateTableId == 0) { // Form created in Tender Document

                // Following code for delete formula is commented due to Bug Id: 5482
                /*
           short obj = tableMatrix.getFormulaCountOfTotal(tableId);
           if (isInEditMode) {
               if (obj > 0) {
                   tableMatrix.deleteTotalFormula(tableId);
               }
           }
           if (isInEditMode) {
               cols = tableMatrix.getNoOfColsInTable(tableId);
               rows = tableMatrix.getNoOfRowsInTable(tableId, (short) 1);
           }
                 */
                boolean isTotFormulaCreated = tableMatrix.isTotalFormulaCreated(tableId);
                if (isTotFormulaCreated) {
                    rows = (short) (rows - 1);
                }

                if (isBOQForm && (rows == -1 || rows == 0)) {
                    rows = 1;
                }
            } else { // Form dumped from STD
                // STD Form Edit in Tender (2nd Time, 3rd Time..., nth Time)
                if (isBOQForm && !(rows == -1 || rows == 0)) {
                    boolean isTotFormulaCreated = tableMatrix.isTotalFormulaCreated(tableId);
                    if (isTotFormulaCreated) {
                        rows = (short) (rows - 1);
                    }
                    //System.out.println("in STD Form Edit in Tender (2nd Time, 3rd Time..., n : rows : "+rows+"isTotFormulaCreated : "+isTotFormulaCreated);
                }
                // STD Form First Time Edit in Tender
                if (isBOQForm && (rows == -1 || rows == 0)) {
                    rows = 1;
                }
            }
            //   System.out.println("final  rows : "+rows+"cols : "+cols+"isBOQForm : "+isBOQForm);

            short fillBy[] = new short[cols];
            pageContext.setAttribute("tenderId", tenderId);

            if (frm != null) {
                if (frm.size() > 0) {
                    frmName = frm.get(0).getFormName();
                    frmHeader.append(frm.get(0).getFormHeader());
                    frmFooter.append(frm.get(0).getFormFooter());
                    isBOQ = frm.get(0).getIsPriceBid();
                    templateFormId = frm.get(0).getTemplateFormId();
                    // tenderFormType = frm.get(0).getFormType();
                    //  tenderFormType = frm.get(0).getFormType();
                }
                frm = null;
            }

            strBrowserInfo = request.getHeader("User-Agent");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create Table </title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/form/Add.js"></script>
        <script type="text/javascript" src="../resources/js/form/CommonValidation.js"></script>
        <script type="text/javascript" src="../resources/js/form/CreateFormula.js" ></script>
        <script type="text/javascript" src="../resources/js/form/TableMatrix.js" ></script>

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alertsMatrix.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script language="javascript">
    var cmbWithCalcText = new Array();
    var cmbWithCalcValue = new Array();
    var cmbWoCalcText = new Array();
    var cmbWoCalcValue = new Array();
    var isWorks = <%=isWorks%>;
            <%
    if (objComboCalWise.size() > 0) {
        for (int i = 0; i < objComboCalWise.get(0).length; i++) {
            %>
    cmbWithCalcText.push("<%=objComboCalWise.get(0)[i]%>");
    cmbWithCalcValue.push("<%=objComboCalWise.get(1)[i]%>");
            <%                  }
    }

    if (objComboWOCalWise.size() > 0) {
        for (int i = 0; i < objComboWOCalWise.get(0).length; i++) {
            %>
    cmbWoCalcText.push("<%=objComboWOCalWise.get(0)[i]%>");
    cmbWoCalcValue.push("<%=objComboWOCalWise.get(1)[i]%>");
            <%              }
    }
            %>

    function changecombowithCal(value, colCount) {
        var rowcount = document.getElementById("rows").value;
        for (var i = 1; i <= rowcount; i++)
        {
            var newId = parseInt(colCount) + 1;
            id = "selComboWithCalc" + i + "_" + newId;
            document.getElementById(id).options[value].selected = true;
        }
    }

    function changecomboWOCal(value, colCount) {
        var rowcount = document.getElementById("rows").value;
        for (var i = 1; i <= rowcount; i++)
        {
            var newId = parseInt(colCount) + 1;
            id = "selComboWithOutCalc" + i + "_" + newId;
            document.getElementById(id).options[value].selected = true;
        }
    }

    /*
     function GetElementId(text)
     {                
     var l = $('#FormMatrix').find('tr:first').children().length;
     
     for (var i = 1; i < parseInt(l); i++) 
     {
     alert(i);
     var v = $("#addTD"+ i).find('#TBD2')[0].innerHTML;
     
     if (v.includes(text)) 
     {                
     return i;
     } 
     }
     
     return '';
     }
     */

    function GetElementId(text)
    {
        var retVal = '';
        var formInfo = $("#hiddenFormInfo").val();
        var json = jQuery.parseJSON(decodeURIComponent(formInfo));

        $.each(json, function (k, v) {
            if (k.includes(text))
            {
                retVal = v;
                return false;
            }
        });

        return retVal;
    }

    function addRowToTable(id, code, name, unit)
    {
        var hiddenTTId = document.getElementById("hiddenTTId");

        var rowNo = $('table#FormMatrix tr:last').index();
        var d = GetElementId('Code');

        var c = document.getElementById('Cell1_' + d);

        if (c.value != '')
        {
            if (hiddenTTId == "0")
            {
                addRow(document.getElementById('frmTableCreation'), cmbWithCalcText, cmbWithCalcValue, cmbWoCalcText, cmbWoCalcValue, isWorks, 'NotDumped', 'No');
            } else
            {
                addRow(document.getElementById('frmTableCreation'), cmbWithCalcText, cmbWithCalcValue, cmbWoCalcText, cmbWoCalcValue, isWorks, 'Dumped', 'No');
            }

            ++rowNo;
        }

        if (d != '')
        {
            document.getElementById('Cell' + rowNo + '_' + d).value = code;
        }

        d = GetElementId('Description');
        if (d != '')
        {
            document.getElementById('Cell' + rowNo + '_' + d).value = name;
        }

        d = GetElementId('Measurement+Unit');

        if (d != '')
        {
            document.getElementById('Cell' + rowNo + '_' + d).value = unit;
        }
    }

    function checkForContractVal(obj) {
        if (obj.value != "" && parseInt(obj.value) > 100) {
            jAlert("You can't enter value above 100", "Alert");
            obj.value("");
        }
    }

    function changeGroupVal(textObj) {
        var textId = textObj.id;
        var index = 0;
        var curIdex = textId.substring(3, textId.indexOf("_"));

        var rowCnt = document.getElementById("rows").value;
        var textVal = document.getElementById("Cell" + curIdex + "_" + "2").value;
        // bugid #4672
        /*for(index=1;index<parseInt(curIdex);index++){
         document.getElementById("Cell"+index+"_"+"2").readOnly = true;
         }*/

        for (index = curIdex; index <= parseInt(rowCnt); index++) {
            document.getElementById("Cell" + index + "_" + "2").value = textVal;
        }

        //getUniqueGroup();
    }

    function getUniqueGroup() {

        var groupArray = new Array();
        var index = 0;
        var groupVal = "";
        var rowCnt = document.getElementById("rows").value;

        for (index = 1; index <= parseInt(rowCnt); index++) {
            if (groupVal != document.getElementById("Cell" + index + "_" + "2").value) {
                groupVal = document.getElementById("Cell" + index + "_" + "2").value
                groupArray.push(groupVal);
            }
        }

        if (window.confirm('Are you sure want to add Group : ' + groupArray.join(", ") + ' ?')) {
            return true;
        } else {
            return false;
        }
    }

    function resetAllValue() {

        var index = 0;
        var rowCnt = document.getElementById("rows").value;

        for (index = 1; index <= parseInt(rowCnt); index++) {
            document.getElementById("Cell" + index + "_" + "2").readOnly = false;
            document.getElementById("Cell" + index + "_" + "2").value = "";
        }

    }

    function validateExt()
    {
        document.getElementById("extMsg").innerHTML = "";
        if (document.getElementById("txtFile").value == "")
        {
            document.getElementById("extMsg").innerHTML = 'Please Select a CSV/xlsx File To Be Uploaded';
            return false;
        }

        var ext = document.getElementById("txtFile").value;
        ext = ext.substring(ext.lastIndexOf(".") + 1, ext.length);
        ext = ext.toLowerCase();
        if (ext == 'csv' || ext == 'xlsx') {
            return true;
        } else {
            document.getElementById("extMsg").innerHTML = 'System supports CSV/xlsx file type only and hence the File extension must be .CSV';
            return false;
        }
    }

    function checkParentVal(childId, parentId, selectVal) {
        var templateTableId = 0;
        var isBOQForm;

        if (document.getElementById("templateTableId"))
        {
            templateTableId = document.getElementById("templateTableId").value;
        }
        if (document.getElementById("isBOQForm"))
        {
            isBOQForm = document.getElementById("isBOQForm").value;
        }
        if (templateTableId != 0) // Form-Table dumped from STD
        {
            var selIndex = document.getElementById(parentId).selectedIndex;
            document.getElementById(childId).options[selIndex].selected = true;
            jAlert("You can't Change Data Type", "Alert");
            return false;
        } else // Form-Table created in Tender Document
        {
            if (isBOQForm == "true") {
                if (document.getElementById(parentId).value != selectVal) {
                    var selIndex = document.getElementById(parentId).selectedIndex;
                    document.getElementById(childId).options[selIndex].selected = true;
                    jAlert("You can't Change Data Type", "Alert");
                    return false;
                }
            } else
            {
                return true;
            }
        }
    }
    var is_for_set_OverFlow_jCal = true;
    function GetCal(txtname, controlname)
    {

        new Calendar({
            inputField: txtname,
            trigger: controlname,
            showTime: 24,
            dateFormat: "%d-%b-%Y",
            position: function () {
                $('#' + textname).after('<span class="datepicker"></span>');
            },
            onSelect: function () {
                var date = Calendar.intToDate(this.selection.get());
                LEFT_CAL.args.min = date;
                LEFT_CAL.redraw();
                this.hide();
                document.getElementById(txtname).focus();
            }
        });

        var LEFT_CAL = Calendar.setup({
            weekNumbers: false
        })
    }
    function changeTextVal(obj) {
        var textId = obj.id;
        textId = textId.replace("selComboWithOutCalc", "Cell");
        document.getElementById(textId).value = obj.value;
    }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->
            <div class="contentArea_1">

                <div class="pageHead_1">
                    Create Table
                    <%if (pkgOrLotId == -1) {%>
                    <span style="float: right; text-align: right;">
                        <a class="action-button-goback" href="TenderTableDashboard.jsp?tenderId=<%= tenderId%>&sectionId=<%= sectionId%>&formId=<%= formId%>" title="Form Dashboard">Form Dashboard</a>
                    </span>
                    <%} else {%>

                    <span style="float: right; text-align: right;">
                        <a class="action-button-goback" href="TenderTableDashboard.jsp?tenderId=<%= tenderId%>&sectionId=<%= sectionId%>&formId=<%= formId%>&porlId=<%= pkgOrLotId%>" title="Form Dashboard">Form Dashboard</a>
                    </span>
                    <%}%>
                </div>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%
                    List<SPTenderCommonData> tcd = null;
                    tcd = tenderCommonService.GetBOQ("SelectColumnsByFormId", request.getParameter("formId"), null);

                    JSONObject json = new JSONObject();

                    for (int i = 0; i < tcd.size(); i++) {
                        json.put(tcd.get(i).getFieldName2(), tcd.get(i).getFieldName1());
                    }

                    String formInfoJson = URLEncoder.encode(json.toString());
                //if (!"".equals(strBrowserInfo) && strBrowserInfo.indexOf("Firefox/") == -1) {

                    String corriId = null;
                    //Corrigendum Created and its status is Pending
                    boolean corriCrtNPending = false;
                    List<SPTenderCommonData> lstCorri = tenderCommonService.returndata("getCorrigenduminfo", tenderId + "", null);
                    if (lstCorri != null) {
                        if (lstCorri.size() > 0) {
                            for (SPTenderCommonData data : lstCorri) {
                                if (data.getFieldName4().equalsIgnoreCase("pending")) {
                                    corriCrtNPending = true;
                                    corriId = data.getFieldName1();
                                }
                            }
                        }
                    }

                    int grandSumid = 0;
                    if (corriId != null && !corriId.equalsIgnoreCase("0") && !corriId.equalsIgnoreCase("null")) {
                        grandSumid = tenderFrmSrBean.CheckExistInGrandSum(tableId, formId, tenderId, "CheckExistInCorriGrandSum", Integer.parseInt(corriId));
                    } else {
                        grandSumid = tenderFrmSrBean.CheckExistInGrandSum(tableId, formId, tenderId, "CheckExistInGrandSum");
                    }
                %>
                <table width="100%" cellspacing="10" class="formBg_1 t_space">
                    <tr>
                        <td align="left">
                            <form id="myForm" enctype="multipart/form-data" method="post" action="UploadCSV.jsp?hidtenderId=<%=tenderId%>&hidsectionId=<%=sectionId%>&hidformId=<%=formId%>&hidtableId=<%=tableId%>&hidporlotId=<%=pkgOrLotId%>">
                                <input type="file" name="txtFile" id="txtFile" class="formTxtBox_1" style="background-color: #fff;" />&nbsp;&nbsp;<span style="color: red; text-align: left;" id="extMsg"></span><br />
                                <label class="formBtn_1">
                                    <input type="submit" id="submitID" name="submit" value="Upload CSV/xlsx File" onclick="return validateExt();" />
                                </label>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px; color: red;">
                            <ul>
                                <li>System supports CSV file type only and hence the File extension must be .CSV</li>
                                <li>Data in a file must be comma (,) separated</li>
                            </ul>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="errorFlag" id="errorFlag" value="0" />
                <form action="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=tableMatrix" method="post" name="frmTableCreation" id="frmTableCreation">
                    <input type="hidden" id="hiddenTTId" name="hiddenTTId" value="<%=templateTableId%>"/>
                    <input type="hidden" id="hiddenFormInfo" name="hiddenFormInfo" value="<%=formInfoJson%>"/>
                    <input type="hidden" name="hdnTblChange" id="hdnTblChange" value="false"/>
                    <input type="hidden" name="hdnGrandSumExist" id="hdnGrandSumExist" value="<%= grandSumid%>"/>
                    <input type="hidden" name="corriId" value="<%=corriId%>" />
                    <input type="hidden" name="tenderId" value="<%=tenderId%>" />
                    <input type="hidden" name="sectionId" value="<%=sectionId%>" />
                    <input type="hidden" name="formId" value="<%=formId%>" />
                    <input type="hidden" name="tableId" value="<%=tableId%>" />
                    <input type="hidden" name="cols" id="cols" value="<%=cols%>" />
                    <input type="hidden" name="rows" id="rows" value="<%=rows%>" />
                    <input type="hidden" name="rcount" id="rcount" value="<%=cols%>" />
                    <input type="hidden" name="clcount" id="clcount" value="<%=rows%>" />
                    <input type="hidden" name="delRow" id="delRow" value="0"/>
                    <input type="hidden" name="delCol" id="delCol" value="0"/>
                    <input type="hidden" name="delColArray" id="delColArr" value="0"/>
                    <input type="hidden" name="templateTableId" id="templateTableId" value="<%=templateTableId%>"/>
                    <input type="hidden" id="isInEditMode" name="isInEditMode" value="<%=isInEditMode%>" />
                    <input type="hidden" name="isBOQForm" id="isBOQForm" value="<%=isBOQForm%>" />
                    <input type="hidden" name="porlId" id="porlId" value="<%=pkgOrLotId%>"/>

                    <%                 // }
                        if (frmName != null && !"".equals(frmName.trim())) {
                    %>
                    <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                        <tr>
                            <td width="100" class="ff">Form Name : </td>
                            <td><%= frmName%></td>                          
                        </tr>
                    </table>
                    <%                  }
                        if (frmHeader != null && frmHeader.length() > 0 && !"".equals(frmHeader.toString().trim())) {
                    %>

                    <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                        <tr>
                            <td width="100" class="ff">Form Header : </td>
                            <td style="line-height: 1.75"><%= frmHeader.toString()%></td>
                        </tr>
                    </table>
                    <%                  }
                        if (tableName != null && !"".equals(tableName.trim())) {
                    %>
                    <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                        <tr>
                            <td width="100" class="ff">Table Name : </td>
                            <td><%=tableName%></td>
                        </tr>
                    </table>
                    <%                  }
    if (tableHeader != null && !"".equals(tableHeader.trim())) {%>
                    <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                        <tr>
                            <td width="100" class="ff">Table Header : </td>
                            <td style="line-height: 1.75"><%=tableHeader%></td>
                        </tr>
                    </table>                            
                    <%                  }
                        if (templateTableId == 0) {// Form created in Tender Document
                    %>
                    <table width="100%" cellspacing="0" class="tableView_1 t_space">
                        <tr>
                            <td width="50%" align="left">
                                <a class="action-button-add" href="#" onclick="document.getElementById('hdnTblChange').value = 'true';
                                        addRow(document.getElementById('frmTableCreation'), cmbWithCalcText, cmbWithCalcValue, cmbWoCalcText, cmbWoCalcValue, isWorks, 'NotDumped', 'No');" title="Add Row">Add Row</a> &nbsp;
                                <a class="action-button-delete" href="#" onclick="document.getElementById('hdnTblChange').value = 'true';delTableRow(document.getElementById('frmTableCreation'));" title="Delete Row">Delete Row</a>                                                                                                
                                <%
                                    boolean isShowInBOQ=false;
                                    isShowInBOQ =  tenderFormTypeList.get(0).getFieldName11()==null || tenderFormTypeList.get(0).getFieldName11().equalsIgnoreCase("na") || tenderFormTypeList.get(0).getFieldName11().startsWith("Copied form.");
                                    if(!isShowInBOQ){
                                        isShowInBOQ=tenderFormTypeList.get(0).getFieldName11().equals("BOQsalvage");
                                    }
                                    
                                    if (tenderFormTypeList.size() > 0 && tenderFormTypeList.get(0).getFieldName3().equals("yes") && isShowInBOQ && isWorks) {
                                %> 
                                <a class="action-button-add" href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/admin/SelectBOQ.jsp', '', 'width=500px,height=400px,scrollbars=1', '');" title="Select from BSR">Select from BSR</a> &nbsp;
                                <%
                                    }
                                %>

                            </td>
                            <%
                                //if(!isBOQForm){
                            %>
                            <td width="50%" align="right">
                                <a class="action-button-add" href="#" onclick="document.getElementById('hdnTblChange').value = 'true';
                                        addCol(document.getElementById('frmTableCreation'), '<%= isBOQForm%>', cmbWithCalcText, cmbWithCalcValue, cmbWoCalcText, cmbWoCalcValue);" title="Add Column">Add Column</a>&nbsp;
                                <a class="action-button-delete" href="#" onclick="document.getElementById('hdnTblChange').value = 'true';
                                        delTableCol(document.getElementById('frmTableCreation'), '<%= isBOQForm%>', cmbWithCalcText, cmbWithCalcValue, cmbWoCalcText, cmbWoCalcValue);" title="Delete Column">Delete Column</a>
                            </td>
                            <%
                                //}
                            %>
                        </tr>
                    </table>
                    <%
                    } else {
                        // Form dumped from STD
                        // Add column, Delete column not available here
                    %>
                    <% //Code to identify discount form
                        String isDis = request.getParameter("isDis");
                        boolean isDisForm = Boolean.parseBoolean(isDis);
                    %>
                    <table width="100%" cellspacing="0" class="tableView_1 t_space">
                        <tr>
                            <%  if (true) { %>
                            <td width="50%" align ="left">
                            <%    if (!isDisForm) {%>
                                <a class="action-button-add" href="#" onclick="document.getElementById('hdnTblChange').value = 'true';
                                        addRow(document.getElementById('frmTableCreation'), cmbWithCalcText, cmbWithCalcValue, cmbWoCalcText, cmbWoCalcValue, isWorks, 'Dumped', 'No');" title="Add Row">Add Row</a> &nbsp;
                                        
                                <a class="action-button-delete" href="#" onclick="document.getElementById('hdnTblChange').value = 'true';
                                        delTableRow(document.getElementById('frmTableCreation'));" title="Delete Row">Delete Row</a>
                                        <% } %>
                                <%
                                    boolean isShowInBOQ=false;
                                    isShowInBOQ =  tenderFormTypeList.get(0).getFieldName11()==null || tenderFormTypeList.get(0).getFieldName11().equalsIgnoreCase("na") || tenderFormTypeList.get(0).getFieldName11().startsWith("Copied form.");
                                    if(!isShowInBOQ){
                                        isShowInBOQ=tenderFormTypeList.get(0).getFieldName11().equals("BOQsalvage");
                                    }
                                    
                                    if (tenderFormTypeList.size() > 0 && tenderFormTypeList.get(0).getFieldName3().equals("yes") && isShowInBOQ && isWorks) {
                                        if (!isDisForm) {
                                %> 
                                <a class="action-button-add" href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/admin/SelectBOQ.jsp', '', 'width=500px,height=400px,scrollbars=1', '');" title="Select from BSR">Select from BSR</a> &nbsp;
                                <%
                                    } }
                                %>
                            </td>
                            <%                          }
                                if (false) {
                            %>
                            <td width="50%" align="right">
                                <a class="action-button-add" href="#" onclick="document.getElementById('hdnTblChange').value = 'true';
                                        addCol(document.getElementById('frmTableCreation'), '<%= isBOQForm%>');" title="Add Column">Add Column</a>&nbsp;
                                <a class="action-button-delete" href="#" onclick="document.getElementById('hdnTblChange').value = 'true';
                                        delTableCol(document.getElementById('frmTableCreation'));" title="Delete Column">Delete Column</a>
                            </td>
                            <%                          }%>
                        </tr>
                    </table>
                    <%                  }%>
                    
                    <%if(frmName.contains("Bill of Quantities")){%>
                        <br/><div class="responseMsg noticeMsg" style="color:red; font-size: 15px; font-style: italic;"><b>We are suggesting to add not more than 10 items in one form. If you have more than 10 items, please copy this form.</div><br/>
                    <%}%>
                    <div style="overflow: auto; width: 100%;">
                        <table width="100%" cellspacing="0" class="tableList_2 t_space" id="FormMatrix">
                            <tbody>
                                <%
                                    for (short i = -1; i <= rows; i++) {
                                        if (i == -1) {
                                            if (templateTableId == 0) {
                                %>
                                <tr id="ChkCol">
                                    <th width="2%" colspan="2">&nbsp;</th>
                                    <%--<th width="2%">&nbsp;</th>--%>
                                    <%
                                        for (short j = 0; j < cols; j++) {
                                    %>
                                    <th style="text-align:center;" id="TDChkDel<%=  j + 1%>">
                                        <input type="checkbox" name="ChkDel<%= j + 1%>" id="ChkDel<%= j + 1%>" />
                                    </th>
                                    <%
                                        }
                                    %>
                                </tr>
                                <%
                                        }
                                    }
                                    if (i == 0) {
                                %>
                                <tr id="ColumnRow" style="background-color: #f0e68c;">
                                    <td width="2%" colspan="2">
                                        <table width="100%" id="TB1" border="0" cellspacing="5" cellpadding="0">
                                            <tr id="TBR1" style="height: 48px;">
                                                <td id="TBD1" style="border: none; border-bottom: 1px solid black;">Header</td>
                                            </tr>
                                            <tr >
                                                <td id="TBD1" style="border: none;">Fill By</td>
                                            </tr>
                                            
                                        </table>
                                    </td>
                                    <%--<th width="2%">&nbsp;</th>--%>
                                    <%
                                        String toShowOrNot = "block";
                                        String hideFixedData = "table-row";
                                        String hideFixedDataNew = "table-row";
                                        boolean boolToShowOrNot = false;
                                        for (short j = 0; j < cols; j++) {
                                            sortOrder = (short) (j + 1);
                                            if (tblColumnsDtl.hasNext()) {
                                                TblTenderColumns ttc = tblColumnsDtl.next();
                                                colHeader = ttc.getColumnHeader();
                                                colHeader = colHeader.replace("?s", "'s");
                                                colType = ttc.getColumnType();
                                                filledBy = ttc.getFilledBy();
                                                dataType = ttc.getDataType();
                                                showHide = ttc.getShoworhide();
                                                sortOrder = ttc.getSortOrder();
                                            }
                                            fillBy[j] = filledBy;
                                    %>
                                    <td id="addTD<%= j + 1%>" colid="<%= j + 1%>" <% if(cols==2 && j== 0) { %> width="70%" <% } %> >
                                        <table width="100%" id="TB1" cellpadding="0" class="formStyle_1">
                                            <tr id="TBR1">
                                              <!--  <td width="30%" class="ff" id="TBD1">Column Header :</td> -->
                                                <td id="TBD2" style="border: none; width: 100%; height: 50px; border-bottom: 1px solid black; text-align: center;">
                                                <% if (templateTableId != 0) {
                                                    if(colHeader.contains("BTN")){
                                                        out.print("Unit Cost in Figure (Nu.)");
                                                    }
                                                    else{
                                                        out.print(colHeader.replace("<br/>", "\n"));
                                                    }
                                                  }
                                                %>
                                                    <textarea rows="3" class="formTxtBox_1" name="Header<%=j%>" id="Header<%=j%>" style="<%if (templateTableId != 0) {%> display:none;<%}%> height: 70px"><%= colHeader.replace("<br/>", "\n")%></textarea>
                                                </td>
                                            </tr>
                                            <%
                                                if (templateTableId != 0) {
                                                    toShowOrNot = "none";
                                                    if (dataType == 9 || dataType == 10) {
                                                        hideFixedDataNew = "block";
                                                    } else {
                                                        hideFixedDataNew = "none";
                                                    }
                                                    hideFixedData = "none";
                                                    boolToShowOrNot = true;
                                                }
                                            %>
                                            <tr>
                                                <td style="border: none; font-weight: bold; vertical-align: bottom;  text-align: center;" >
                                                    <select style="display: <%= toShowOrNot%>;" class="formTxtBox_1" name="FillBy<%=j%>" id="FillBy<%=j%>" onChange="setDisable(this.form, this,<%=(j + 1)%>);" style="width: 150px">
                                                            <option value="1" <%if (filledBy == 1) {
                                                                out.print("selected");
                                                            }%> >PA User</option>
                                                                <option value="2" <%if (filledBy == 2) {
                                                                out.print("selected");
                                                            }%>>Bidder/Consultant</option>
                                                                <option value="3" <%if (filledBy == 3) {
                                                                out.print("selected");
                                                            }%>>Auto</option>
                                                                <option value="4" <%if (filledBy == 4) {
                                                                out.print("selected");
                                                            }%>>Bidder Quoted Amount</option>
                                                                <option value="5" <%if (filledBy == 5) {
                                                                out.print("selected");
                                                            }%>>Discount Amount Percentage</option>
                                                    </select>
                                                    <%
                                                        if (boolToShowOrNot) {
                                                            switch (filledBy) {
                                                                case (1):
                                                                    out.print("PA User");
                                                                    break;
                                                                case (2):
                                                                    out.print("Bidder/Consultant");
                                                                    break;
                                                                case (3):
                                                                    out.print("Auto");
                                                                    break;
                                                                case (4):
                                                                    out.print("Bidder Quoted Amount");
                                                                    break;
                                                                case (5):
                                                                    out.print("Discount Amount Percentage");
                                                                    break;
                                                            }
                                                        }
                                                    %>
                                                </td>
                                            </tr>
                                            <% if (j == 1 && isWorks && isBOQForm) { //yrj bugid #4672 %>
                                            <!--<tr>
                                                <td class="ff">Reset Value :</td>
                                                <td>
                                                    <input type="button" name="resetVal" od="resetVal" value="Reset" onclick="resetAllValue();"  />
                                                </td>
                                            </tr>-->
                                            <% } %>
                                            <tr style="display: <%= hideFixedDataNew%>" id="dt<%=j%> ">
                                            <td class="ff">Data Type :</td> 
                                                <td id="DataTypeTd<%=j%>">
                                                    <select  style="display: <%= toShowOrNot%>" class="formTxtBox_1" name="DataType<%= j%>" id="DataType<%= j%>" onChange="changeAllCombo(this.form, this,<%= j + 1%>)" style="width: 150px" >
                                                            <option value="1" <%if (dataType == 1) {
                                                                out.print("selected");
                                                            }%>>Small Text</option>
                                                                <option value="2" <%if (dataType == 2) {
                                                                out.print("selected");
                                                            }%>>Long Text</option>
                                                                <option value="3" <%if (dataType == 3) {
                                                                out.print("selected");
                                                            }%>>Money Positive</option>
                                                                <option value="4" <%if (dataType == 4) {
                                                                out.print("selected");
                                                            }%>>Numeric</option>
                                                        <%--<option value="6" <%if(dataType == 6){out.print("selected");}%>>List Box</option>--%>
                                                                <option value="8" <%if (dataType == 8) {
                                                                out.print("selected");
                                                            }%>>Money All</option>
                                                                <option value="9" <%if (dataType == 9) {
                                                                out.print("selected");
                                                            }%>>Combo Box with Calculation</option>
                                                    <option value="10" <%if (dataType == 10) {
                                                                out.print("selected");
                                                            }%>> </option>  <!--Combo Box w/o Calculation -->
                                                            <option value="11" <%if (dataType == 11) {
                                                                out.print("selected");
                                                            }%>>Money All (-5 to +5)</option>
                                                                    <option value="12" <%if (dataType == 12) {
                                                                out.print("selected");
                                                            }%>>Date</option>
                                                                    <option value="13" <%if (dataType == 13) {
                                                                out.print("selected");
                                                            }%>>Money Positive(3 digits after decimal)</option>
                                                    </select>
                                                    <%
                                                        if (boolToShowOrNot) {
                                                            switch (dataType) {
                                                                case (1):
                                                                    out.print("Small Text");
                                                                    break;
                                                                case (2):
                                                                    out.print("Long Text");
                                                                    break;
                                                                case (3):
                                                                    out.print("Money Positive");
                                                                    break;
                                                                case (4):
                                                                    out.print("Numeric");
                                                                    break;
                                                                case (8):
                                                                    out.print("Money All");
                                                                    break;
                                                                case (9):
                                                                    out.print("Combo Box with Calculation");
                                                                    break;
                                                                case (10):
                                                                    out.print(""); 
                                                                    break;
                                                                case (11):
                                                                    out.print("Money All (+5 to +5)");
                                                                    break;
                                                                case (12):
                                                                    out.print("Date");
                                                                    break;
                                                                case (13):
                                                                    out.print("Money Positive(3 digits after decimal)");
                                                                    break;
                                                            }
                                                        }

                                                        int cmbIndex = 0;
                                                    %>

                                                    <span id="comboWithCalc<%= j%>" <% if (dataType == 9) {%>style="display: block;"<% } else {%>style="display: none;"<% }%>>
                                                        <select name="selComboWithCalc<%=j%>" id="selComboWithCalc<%=j%>"  class="formTxtBox_1" onchange="changecombowithCal(this.selectedIndex,<%=j%>);">
                                                            <% for (cmbIndex = 0; cmbIndex < objComboCalWise.get(0).length; cmbIndex++) {%>
                                                            <option value="<%=objComboCalWise.get(1)[cmbIndex]%>" <%if(cmbIndex==1){out.print("selected");}%>><%=objComboCalWise.get(0)[cmbIndex]%></option>
                                                            <% }%>
                                                        </select>
                                                    </span>
                                                    <span id="comboWithOutCalc<%= j%>" <% if (dataType == 10) {%>style="display: block;"<% } else {%>style="display: none;"<% }%>>
                                                        <select name="selComboWithOutCalc<%=j%>" id="selComboWithOutCalc<%=j%>"  class="formTxtBox_1" onchange="changecomboWOCal(this.selectedIndex,<%=j%>);">
                                                            <% for (cmbIndex = 0; cmbIndex < objComboWOCalWise.get(0).length; cmbIndex++) {%>
                                                            <option value="<%=objComboWOCalWise.get(1)[cmbIndex]%>" <%if(cmbIndex==1){out.print("selected");}%>><%=objComboWOCalWise.get(0)[cmbIndex]%></option>
                                                            <% }%>
                                                        </select>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr style="display: <%= hideFixedData%>" >
                                                <td class="ff">Show / Hide :</td>
                                                <td>
                                                    <select style="display: <%= toShowOrNot%>" class="formTxtBox_1" name="ShowOrHide<%= j%>" id="ShowOrHide<%= j%>" style="width: 150px">
                                                    <option value="1" <%if ("1".equals(showHide)) {
                                                                out.print("selected");
                                                            }%>>Show</option>
                                                        <option value="2" <%if ("2".equals(showHide)) {
                                                            out.print("selected");
                                                        }%>>Hide</option>
                                                        </select>
                                                        <%
                                                            if (boolToShowOrNot) {
                                                                switch (Integer.parseInt(showHide.trim())) {
                                                                    case (1):
                                                                        out.print("Show");
                                                                        break;
                                                                    case (2):
                                                                        out.print("Hide");
                                                                        break;
                                                                }
                                                            }
                                                        %>
                                                </td>
                                            </tr>
                                            <tr <%if (!isBOQForm) {%>style ="display: none"<%} else {%> style="display: <%= hideFixedData%>" <%}%>>
                                                <td class="ff">Add Column Type :</td>
                                                <td>
                                                    <select style="display: <%= toShowOrNot%>" class="formTxtBox_1" id="columnType<%= j%>" name="columnType<%= j%>" style="width: 150px">
                                                        <option value="1" <%if ("1".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Normal</option>
                                                        <option value="2" <%if ("2".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Qty</option>
                                                        <option value="3" <%if ("3".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Qty by Bidder/Consultant</option>
                                                        <option value="4" <%if ("4".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>EE Unit Rate</option>
                                                        <option value="5" <%if ("5".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>EE Total Rate</option>
                                                        <option value="6" <%if ("6".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Unit Rate</option>
                                                        <option value="7" <%if ("7".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Total Rate</option>
                                                        <option value="8" <%if ("8".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Currency</option>
                                                        <option value="9" <%if ("9".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Local Labour</option>
                                                        <option value="10" <%if ("10".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Supplier VAT</option>
                                                        <option value="11" <%if ("11".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Inland and Others</option>

                                                    </select>
                                                    <%                                                       if (boolToShowOrNot) {
                                                            switch (Integer.parseInt(colType.trim())) {
                                                                case (1):
                                                                    out.print("Normal");
                                                                    break;
                                                                case (2):
                                                                    out.print("Qty");
                                                                    break;
                                                                case (3):
                                                                    out.print("Qty By Bidder/Consultant");
                                                                    break;
                                                                case (4):
                                                                    out.print("EE Unit Rate");
                                                                    break;
                                                                case (5):
                                                                    out.print("EE Total Rate");
                                                                    break;
                                                                case (6):
                                                                    out.print("Unit Rate");
                                                                    break;
                                                                case (7):
                                                                    out.print("Total Rate");
                                                                    break;
                                                                case (8):
                                                                    out.print("Currency");
                                                                    break;
                                                                case (9):
                                                                    out.print("Local Labour");
                                                                    break;
                                                                case (10):
                                                                    out.print("Supplier VAT");
                                                                    break;
                                                                case (11):
                                                                    out.print("Inland and Others");
                                                                    break;
                                                            }
                                                        }
                                                    %>
                                                </td>
                                            </tr>
                                            <tr style="display: <%= hideFixedData%>" >
                                                <td class="ff">Sort Order :</td>
                                                <td>
                                                    <input style="display: <%= toShowOrNot%>" type="text" class="formTxtBox_1" id="SortOrder<%=j%>" name="SortOrder<%=j%>" style="width:25px;" value="<%= sortOrder%>" />
                                                    <%
                                                        if (boolToShowOrNot) {
                                                            out.print(sortOrder);
                                                        }
                                                    %>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <%
                                        }
                                        if (templateTableId > 0) {
                                            for (short c = 0; c < cols; c++) {
                                                if (tblColumnsDtl.hasPrevious()) {
                                                    tblColumnsDtl.previous();
                                                }
                                            }
                                        }
                                    %>
                                </tr>
                                <%
                                    }
                                    if (i > 0) {
                                %>
                                <tr id="TR<%=i%>">
                                    <td align="center" >
                                       <input type="checkbox" name="chk" id="chk<%=i%>" /> 
                                    </td>
                                    <td align="center">
                                        <select class="formTxtBox_1" name="rowsort<%=i%>" id="rowsort<%=i%>">
                                            <%
                                                for (int scount = 1; scount <= rows; scount++) {
                                            %>
                                            <option value=<%= scount%>
                                                    <%
                                                        if (i == scount) {
                                                            out.print(" selected ");
                                                        }
                                                    %>
                                                    ><%=scount%></option>
                                            <%
                                                }
                                            %>
                                        </select>
                                    </td>
                                    <%
                                        int cnt = 0;
                                        short columnId;
                                        int cellId = 0;
                                        List<TblTenderListCellDetail> listTenderListCellDetail = new ArrayList<TblTenderListCellDetail>();
                                        for (int j = 0; j < cols; j++) {
                                            String comboStyle = "block";
                                            if (isInEditMode) {
                                                String cellValue = "";
                                                if (tblCellsDtl.hasNext()) {
                                    %>
                                    <td id="TD<%= i%>_<%= j + 1%>" align="center" style="/*text-align: center;*/">
                                        <%
                                            cnt++;
                                            TblTenderCells cells = tblCellsDtl.next();
                                            dataType = cells.getCellDatatype();
                                            filledBy = cells.getCellDatatype();
                                            cellValue = cells.getCellvalue();
                                            cellValue = cellValue.replace("?s", "'s");
                                            columnId = cells.getColumnId();
                                            cellId = cells.getCellId();
                                            //out.print(tableId+"-");
                                            //out.print(columnId+"-");
                                            //out.print(cellId);
                                            listTenderListCellDetail = tableMatrix.getTenderListCellDetail(tableId, columnId, cellId);
                                            if (templateTableId != 0 && (dataType == 9 || dataType == 10)) {
                                                comboStyle = "none";
                                            }
                                            if (fillBy[j] == 2 || fillBy[j] == 5) { // Filled By Tenderer
%>
                                        <select <%
                                                out.print("disabled");
                                            %>
 class="formTxtBox_1" name="DataType<%= i%>_<%= j + 1%>" id="DataType<%= i%>_<%= j + 1%>" style="margin: 5px; padding: 2px; display:  <%=comboStyle%>" onchange="checkParentVal('DataType<%= i%>_<%= j + 1%>', 'DataType<%=j%>', this.value);">
                                        <option value="1" <%if (dataType == 1) {
                                                out.print("selected");
                                            }%>>Small Text</option>
                                            <option value="2" <%if (dataType == 2) {
                                                    out.print("selected");
                                                }%>>Long Text</option>
                                            <option value="3" <%if (dataType == 3) {
                                                    out.print("selected");
                                                }%>>Money Positive</option>
                                            <option value="4" <%if (dataType == 4) {
                                                    out.print("selected");
                                                }%>>Numeric</option>
                                            <option value="8" <%if (dataType == 8) {
                                                    out.print("selected");
                                                }%>>Money All</option>
                                            <option value="9" <%if (dataType == 9) {
                                                    out.print("selected");
                                                }%>>Combo Box with Calculation</option>
                                            <option value="10" <%if (dataType == 10) {
                                                    out.print("selected");
                                                }%>> </option> <!-- Combo Box w/o Calculation -->
                                            <option value="11" <%if (dataType == 11) {
                                                    out.print("selected");
                                                }%>>Money All (-5 to +5)</option>
                                            <option value="12" <%if (dataType == 12) {
                                                    out.print("selected");
                                                }%>>Date</option>
                                            <option value="13" <%if (dataType == 13) {
                                                    out.print("selected");
                                                }%>>Money Positive(3 digits after decimal)</option>
                                            </select>
                                            <%              } else if (dataType == 1 || dataType == 3 || dataType == 4 || dataType == 8 || dataType == 11 || dataType == 13) {
                                            %>
                                            <%
                                                //out.print(j+" ");
                                                //out.print(formType+" ");
                                                //out.print(procNature+" ");
%><input type="text" class="formTxtBox_1 newinputwidth" style="padding: 2px; margin: 5px;"
                                                   name="Cell<%= i%>_<%= j + 1%>" id="Cell<%= i%>_<%= j + 1%>"

                                                   <% if (dataType == 11) {%>
                                                   onblur ="chkMinus5Plus5(this);"
                                                   <%} else if (dataType == 13) {%>
                                                   onblur ="checkValufor3Decimal(this);"
                                                   <% } else { %>
                                                   onblur="validateThisObject(this);"    
                                                   <% }
                                                       if ("Services".equalsIgnoreCase(procNature)) {
                                                           if ("Lump - Sum".equalsIgnoreCase(contractType)) {
                                                               if ("12".equalsIgnoreCase(formType) && j == 4) {
                                                   %>
                                                   onblur ="checkForContractVal(this);"
                                                   <%
                                                               }
                                                           }
                                                       }
                                                       if (fillBy[j] == 3 || fillBy[j] == 4) {
                                                           out.print(" disabled ");
                                                           out.print(" value=\"disabled\" ");
                                                       } else {%>
                                                   value="<%= cellValue.replace("<br/>", "\n")%>"
                                                   <%}%>
                                                   />
                                            <%  if (j == 1 && isWorks && isBOQForm) {%> <%--hide copy group button --%>
                                            <!--<input type="button" name="btn<%= i%>_<%= j + 1%>" id="btn<%= i%>_<%= j + 1%>" value="Copy Group" onclick="changeGroupVal(this);" /> -->
                                            <%   } %>
                                            <%
                                            } else if (dataType == 2) { // Data Type - Long Text
%>
                                            <textarea name="Cell<%= i%>_<%= j + 1%>"
                                              id="Cell<%= i%>_<%= j + 1%>" class="formTxtBox_1 newTAwidth"
                                              <% if (fillBy[j] == 3 || fillBy[j] == 4) {
                                                    out.print(" disabled ");
                                                    cellValue = "disabled";
                                                }%> ><%= cellValue.replace("<br/>", "\n")%></textarea>
                                              <%
                                            } else if (dataType == 12) { // DataType -  Date
%>
                                              <input type="text" class="formTxtBox_1" style="width: 80%;height: 20px;" readonly="true" name="Cell<%= i%>_<%= j + 1%>" id="Cell<%= i%>_<%= j + 1%>"
                                                     <% if (fillBy[j] == 3 || fillBy[j] == 4) { // Filled By Auto
                                                             out.print(" disabled ");
                                                             out.print(" value=\"disabled\" ");

                                                   } else { // Filled By PE User%>
                                                     value="<%= cellValue.replace("<br/>", "\n")%>" onfocus="GetCal('Cell<%= i%>_<%= j + 1%>', 'Cell<%= i%>_<%= j + 1%>')"
                                                     <%}%>
                                                     <%
                                                         if ("Services".equalsIgnoreCase(procNature)) {
                                                             if ("Lump - Sum".equalsIgnoreCase(contractType)) {
                                                                 if ("12".equalsIgnoreCase(formType) && j == 4) {
                                                     %>
                                                     onblur ="checkForContractVal(this);"
                                                     <%
                                                                 }
                                                             }
                                                         }
                                                     %>
                                                     />
                                              <% if (fillBy[j] != 3 && fillBy[j] != 4) { // Filled By PE User%>
                                              &nbsp;<a onclick="GetCal('Cell<%= i%>_<%= j + 1%>', 'Cell<%= i%>_<%= j + 1%>')" id='imgCell<%= i%>_<%= j + 1%>' name='imgCell<%= i%>_<%= j + 1%>' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick="GetCal('Cell<%= i%>_<%= j + 1%>', 'imgCell<%= i%>_<%= j + 1%>')" id='imgCell<%= i%>_<%= j + 1%>' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>
                                              <%}%>

                                              <%
                                                  }
                                              %>
                                              <%
                                                  int cmbIndex = 0;
                                                  String selected = "";
                                                  int listcellId = 0;
                                                  String listcell = "";

                                                  if (listTenderListCellDetail.size() > 0) {
                                                      listcellId = listTenderListCellDetail.get(0).getTblTenderListBox().getTenderListId();
                                                  }
                                              %>
                                              <% if ((dataType == 9 || dataType == 10) && "12".equalsIgnoreCase(formType)) {%>
                                              <input type="text" class="formTxtBox_1" style="padding: 2px; margin: 5px; height: 30px; display: none;"
                                                     name="Cell<%= i%>_<%= j + 1%>" id="Cell<%= i%>_<%= j + 1%>" value="<%=cellValue%>"
                                                     <%
                                                         if ("Services".equalsIgnoreCase(procNature)) {
                                                             if ("Lump - Sum".equalsIgnoreCase(contractType)) {
                                                                 if ("12".equalsIgnoreCase(formType) && j == 4) {
                                                     %>
                                                     onblur ="checkForContractVal(this);"
                                                     <%
                                                                 }
                                                             }
                                                         }
                                                     %>
                                                     />
                                              <% }%>
                                              <span id="comboWithCalc<%= i%>_<%= j + 1%>" <% if (dataType == 9) {%>style="display: block;text-align:center;"<% } else {%>style="display: none;"<% }%>>
                                                  <select style="" name="selComboWithCalc<%= i%>_<%= j + 1%>" id="selComboWithCalc<%= i%>_<%= j + 1%>"  class="formTxtBox_1" onchange="changeTextVal(this);">
                                                      <%
                                                          for (cmbIndex = 0; cmbIndex < objComboCalWise.get(0).length; cmbIndex++) {
                                                              if (objComboCalWise.get(1)[cmbIndex].toString().equalsIgnoreCase(Integer.toString(listcellId))
                                                                      || objComboCalWise.get(1)[cmbIndex].toString().equalsIgnoreCase(cellValue)) {
                                                                  selected = "selected='selected'";
                                                                  listcell = objComboCalWise.get(0)[cmbIndex].toString();
                                                              } else {
                                                                  selected = "";
                                                              }
                                                      %>
                                                      <option value="<%=objComboCalWise.get(1)[cmbIndex]%>" <%=selected%>><%=objComboCalWise.get(0)[cmbIndex]%></option>
                                                      <% }%>
                                                  </select>
                                              </span>
                                              <span id="comboWithOutCalc<%= i%>_<%= j + 1%>" <% if (dataType == 10) {%>style="display: block;text-align:center;"<% } else {%>style="display: none;"<% }%>>
                                                  <select style="width:80%;" name="selComboWithOutCalc<%= i%>_<%= j + 1%>" id="selComboWithOutCalc<%= i%>_<%= j + 1%>" class="formTxtBox_1" onchange="changeTextVal(this);">
                                                      <% for (cmbIndex = 0; cmbIndex < objComboWOCalWise.get(0).length; cmbIndex++) {%>
                                                      <%
                                                          if (objComboWOCalWise.get(1)[cmbIndex].toString().equalsIgnoreCase(Integer.toString(listcellId))
                                                                  || objComboWOCalWise.get(1)[cmbIndex].toString().equalsIgnoreCase(cellValue)) {
                                                              selected = "selected='selected'";
                                                              listcell = objComboWOCalWise.get(0)[cmbIndex].toString();
                                                          } else {
                                                              selected = "";
                                                          }
                                                      %>
                                                      <option value="<%=objComboWOCalWise.get(1)[cmbIndex]%>" <%=selected%>><%=objComboWOCalWise.get(0)[cmbIndex]%></option>
                                                      <% }%>
                                                  </select>
                                              </span>

                                    </td>
                                    <%
                                    } else {
                                        if (tblColumnsDtl.hasNext()) {
                                            TblTenderColumns ttc = tblColumnsDtl.next();
                                            filledBy = ttc.getFilledBy();
                                            dataType = ttc.getDataType();
                                            if (ttc != null) {
                                                ttc = null;
                                            }
                                            if (templateTableId != 0 && (dataType == 9 || dataType == 10)) {
                                                comboStyle = "none";
                                            }
                                        }
                                    %>
                                    <td id="TD<%= i%>_<%= j + 1%>">
                                        <%
                                            if (filledBy == 2 || filledBy == 5) { // Filled By Tenderer
                                        %>
                                        <select <%
                                                out.print("disabled");
                                            %> class="formTxtBox_1" name="DataType<%= i%>_<%= j + 1%>" id="DataType<%= i%>_<%= j + 1%>" style="width: 95%; padding: 2px; margin:5px; display: <%=comboStyle%>" onchange="checkParentVal('DataType<%= i%>_<%= j + 1%>', 'DataType<%=j%>', this.value);">
                                        <option value="1" <%if (dataType == 1) {
                                                out.print("selected");
                                            }%>>Small Text</option>
                                            <option value="2" <%if (dataType == 2) {
                                                    out.print("selected");
                                                }%>>Long Text</option>
                                            <option value="3" <%if (dataType == 3) {
                                                    out.print("selected");
                                                }%>>Money Positive</option>
                                            <option value="8" <%if (dataType == 8) {
                                                    out.print("selected");
                                                }%>>Money All</option>
                                            <option value="4" <%if (dataType == 4) {
                                                    out.print("selected");
                                                }%>>Numeric</option>
                                            <option value="9" <%if (dataType == 9) {
                                                    out.print("selected");
                                                }%>>Combo Box with Calculation</option>
                                            <option value="10" <%if (dataType == 10) {
                                                    out.print("selected");
                                                }%>> </option> <!-- Combo Box w/o Calculation -->
                                            <option value="11" <%if (dataType == 11) {
                                                    out.print("selected");
                                                }%>>Money All (-5 to +5)</option>
                                            <option value="12" <%if (dataType == 12) {
                                                    out.print("selected");
                                                }%>>Date</option>
                                            <option value="13" <%if (dataType == 13) {
                                                    out.print("selected");
                                                }%>>Money Positive(3 digits after decimal)</option>
                                            </select>
                                            <%
                                            } else if (dataType == 1 || dataType == 3 || dataType == 4 || dataType == 8 || dataType == 11 || dataType == 13) {
                                            %>
                                            <input type="text" class="formTxtBox_1" style="padding: 2px; margin: 5px;"
                                                   name="Cell<%= i%>_<%= j + 1%>" id="Cell<%= i%>_<%= j + 1%>"
                                                   <% if (dataType == 11) {%>
                                                   onblur ="chkMinus5Plus5(this);"
                                                   <%} else if (dataType == 13) {%>
                                                   onblur ="checkValufor3Decimal(this);"
                                                   <%}
                                                       if (fillBy[j] == 3 || fillBy[j] == 4) {
                                                           out.print(" disabled ");
                                                           out.print(" value=\"disabled\" ");
                                                       } else {%>
                                                   value="<%= cellValue.replace("<br/>", "\n")%>"
                                                   <% } %>
                                                   <%
                                                       if ("Services".equalsIgnoreCase(procNature)) {
                                                           if ("Lump - Sum".equalsIgnoreCase(contractType)) {
                                                               if ("12".equalsIgnoreCase(formType) && j == 4) {
                                                   %>
                                                   onblur ="checkForContractVal(this);"
                                                   <%
                                                               }
                                                           }
                                                       }
                                                   %>
                                                   />
                                            <%  if (j == 1 && isWorks && isBOQForm) {%>  <%-- hide copy group button --%>
                                           <!-- <input type="button" name="btn<%= i%>_<%= j + 1%>" id="btn<%= i%>_<%= j + 1%>" value="Copy Group" onclick="changeGroupVal(this);" /> -->
                                            <%   }
                                            } else if (dataType == 2) { // Data Type - Long Text
%>
                                            <textarea class="formTxtBox_1 newTAwidth" rows="3" name="Cell<%= i%>_<%= j + 1%>"
                                                      id="Cell<%= i%>_<%= j + 1%>" 
                                                      <% if (fillBy[j] == 3 || fillBy[j] == 4) {
                                                              out.print(" disabled ");
                                                              cellValue = "disabled";
                                                          }%> ><%= cellValue.replace("<br/>", "\n")%></textarea>
                                            <%
                                            } else if (dataType == 12) { // DataType -  Date
%>
                                            <input type="text" class="formTxtBox_1" style="width: 80%;height: 20px;" readonly="true" name="Cell<%= i%>_<%= j + 1%>" id="Cell<%= i%>_<%= j + 1%>"
                                                   <% if (fillBy[j] == 3 || fillBy[j] == 4) { // Filled By Auto
                                                           out.print(" disabled ");
                                                           out.print(" value=\"disabled\" ");

                                                   } else { // Filled By PE User%>
                                                   value="<%= cellValue.replace("<br/>", "\n")%>" onfocus="GetCal('Cell<%= i%>_<%= j + 1%>', 'Cell<%= i%>_<%= j + 1%>')"
                                                   <%}%>
                                                   <%
                                                       if ("Services".equalsIgnoreCase(procNature)) {
                                                           if ("Lump - Sum".equalsIgnoreCase(contractType)) {
                                                               if ("12".equalsIgnoreCase(formType) && j == 4) {
                                                   %>
                                                   onblur ="checkForContractVal(this);"
                                                   <%
                                                               }
                                                           }
                                                       }
                                                   %>
                                                   />
                                            <% if (fillBy[j] != 3 && fillBy[j] != 4) { // Filled By PE User%>
                                            &nbsp;<a onclick="GetCal('Cell<%= i%>_<%= j + 1%>', 'Cell<%= i%>_<%= j + 1%>')" id='imgCell<%= i%>_<%= j + 1%>' name='imgCell<%= i%>_<%= j + 1%>' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick="GetCal('Cell<%= i%>_<%= j + 1%>', 'imgCell<%= i%>_<%= j + 1%>')" id='imgCell<%= i%>_<%= j + 1%>' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>
                                            <%}%>

                                            <%
                                                }
                                                int cmbIndex = 0;
                                                String selected = "";
                                                int listcellId = 0;
                                                String listcell = "";
                                                if (listTenderListCellDetail.size() > 0) {
                                                    TblTenderListBox tblTenderListBox = listTenderListCellDetail.get(0).getTblTenderListBox();
                                                    listcellId = tblTenderListBox.getTenderListId();
                                                }
                                            %>
                                            <% if ((dataType == 9 || dataType == 10) && "12".equalsIgnoreCase(formType)) {%>
                                            <input type="text" class="formTxtBox_1" style="padding: 2px; margin: 5px; height: 30px; display: none;"
                                                   name="Cell<%= i%>_<%= j + 1%>" id="Cell<%= i%>_<%= j + 1%>"
                                                   value="<%=cellValue%>"
                                                   <%
                                                       if ("Services".equalsIgnoreCase(procNature)) {
                                                           if ("Lump - Sum".equalsIgnoreCase(contractType)) {
                                                               if ("12".equalsIgnoreCase(formType) && j == 4) {
                                                   %>
                                                   onblur ="checkForContractVal(this);"
                                                   <%
                                                               }
                                                           }
                                                       }
                                                   %>
                                                   />
                                            <% }%>
                                            <span id="comboWithCalc<%= i%>_<%= j + 1%>" <% if (dataType == 9) {%>style="display: block;"<% } else {%>style="display: none;"<% }%>>
                                                <select name="selComboWithCalc<%= i%>_<%= j + 1%>" id="selComboWithCalc<%= i%>_<%= j + 1%>"  class="formTxtBox_1" onchange="changeTextVal(this);">
                                                    <%
                                                        for (cmbIndex = 0; cmbIndex < objComboCalWise.get(0).length; cmbIndex++) {
                                                            if (Integer.parseInt(objComboCalWise.get(1)[cmbIndex].toString()) == listcellId) {
                                                                selected = "selected='selected'";
                                                                listcell = objComboCalWise.get(0)[cmbIndex].toString();
                                                            }else if(cmbIndex ==1)
                                                            {
                                                                selected = "selected='selected'";
                                                            }
                                                            else {
                                                                selected = "";
                                                            }
                                                    %>
                                                    <option value="<%=objComboCalWise.get(1)[cmbIndex]%>" <%=selected%>><%=objComboCalWise.get(0)[cmbIndex]%></option>
                                                    <% }%>
                                                </select>
                                            </span>
                                            <span id="comboWithOutCalc<%= i%>_<%= j + 1%>" <% if (dataType == 10) {%>style="display: block;"<% } else {%>style="display: none;"<% }%>>
                                                <select name="selComboWithOutCalc<%= i%>_<%= j + 1%>" id="selComboWithOutCalc<%= i%>_<%= j + 1%>"  class="formTxtBox_1" onchange="changeTextVal(this);">
                                                    <%                                           for (cmbIndex = 0; cmbIndex < objComboWOCalWise.get(0).length; cmbIndex++) {

                                                            if (objComboWOCalWise.get(1)[cmbIndex].toString().equalsIgnoreCase(Integer.toString(listcellId))
                                                                    || objComboWOCalWise.get(1)[cmbIndex].toString().equalsIgnoreCase(cellValue)) {
                                                                selected = "selected='selected'";
                                                                listcell = objComboWOCalWise.get(0)[cmbIndex].toString();
                                                            }
                                                            else if(cmbIndex == 1)
                                                            {
                                                                selected = "selected='selected'";
                                                            }
                                                            else {
                                                                selected = "";
                                                            }
                                                    %>
                                                    <option value="<%=objComboWOCalWise.get(1)[cmbIndex]%>" <%=selected%>><%=objComboWOCalWise.get(0)[cmbIndex]%></option>
                                                    <% }%>
                                                </select>
                                            </span>
                                            <%
                                                if (!"--Select--".equals(listcell)) {
                                                    out.print(listcell);
                                                }
                                            %>
                                    </td>
                                    <%
                                        }
                                    } else {
                                        int cmbIndex = 0;
                                    %>
                                    <td id="TD<%= i%>_<%= j + 1%>">
                                        <input type="text" class="formTxtBox_1" style="padding: 2px; margin: 5px; height: 30px"
                                               name="Cell<%= i%>_<%= j + 1%>" id="Cell<%= i%>_<%= j + 1%>"
                                               <%
                                                   if ("Services".equalsIgnoreCase(procNature)) {
                                                       if ("Lump - Sum".equalsIgnoreCase(contractType)) {
                                                           if ("12".equalsIgnoreCase(formType) && j == 4) {
                                               %>
                                               onblur ="checkForContractVal(this);"
                                               <%
                                                           }
                                                       }
                                                   }
                                               %>
                                               />
                                        <% if (templateTableId == 0) {%>
                                        <span id="comboWithCalc<%= i%>_<%= j + 1%>" style="display: none;">
                                            <select name="selComboWithCalc<%= i%>_<%= j + 1%>" id="selComboWithCalc<%= i%>_<%= j + 1%>"  class="formTxtBox_1" onchange="changeTextVal(this);">
                                                <%
                                                    for (cmbIndex = 0; cmbIndex < objComboCalWise.get(0).length; cmbIndex++) {
                                                %>
                                                <option value="<%=objComboCalWise.get(1)[cmbIndex]%>"><%=objComboCalWise.get(0)[cmbIndex]%></option>
                                                <% }%>
                                            </select>
                                        </span>
                                        <span id="comboWithOutCalc<%= i%>_<%= j + 1%>" style="display: none;">
                                            <select name="selComboWithOutCalc<%= i%>_<%= j + 1%>" id="selComboWithOutCalc<%= i%>_<%= j + 1%>"  class="formTxtBox_1" onchange="changeTextVal(this);">
                                                <% for (cmbIndex = 0; cmbIndex < objComboWOCalWise.get(0).length; cmbIndex++) {%>
                                                <option value="<%=objComboWOCalWise.get(1)[cmbIndex]%>"><%=objComboWOCalWise.get(0)[cmbIndex]%></option>
                                                <% }%>
                                            </select>
                                        </span>
                                        <% }%>
                                        <%if (j == 1 && isWorks && isBOQForm) {%> <%--hide copy group button --%>
                                       <!-- <input type="button" name="btn<%= i%>_<%= j + 1%>" id="btn<%= i%>_<%= j + 1%>" value="Copy Group" onclick="changeGroupVal(this);" /> -->
                                        <%}%>
                                    </td>
                                    <%
                                            }
                                        }
                                    %>
                                </tr>
                                <%
                                        }
                                    }
                                %>
                            </tbody>
                        </table>

                    </div>
                    <% if (tableFooter != null && !"".equals(tableFooter.trim())) {%>
                    <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                        <tr>
                            <td width="100" class="ff">Table Footer : </td>
                            <td style="line-height: 1.75"><%=tableFooter%></td>
                        </tr>
                    </table>
                    <%}%>
                    <% if (frmFooter != null && frmFooter.length() > 0 && !"".equals(frmFooter.toString().trim())) {%>
                    <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                        <tr>
                            <td width="100" class="ff">Form Footer : </td>
                            <td style="line-height: 1.75"><%=frmFooter.toString()%></td>
                        </tr>
                    </table>
                    <%}%>
                    <div align="center" class="t_space">
                        <label class="formBtn_1">
                            <% if (isWorks && isBOQForm) { //yrj for bugid #4672%>
                            <input type="submit" name="subBtnCreateEdit" id="subBtnCreateEdit" onclick="if (getUniqueGroup()) {
                                        return checkValidData(document.getElementById('frmTableCreation'));
                                    } else {
                                        return false;
                                    }"
                                   <%if (isInEditMode) {%>
                                   value="Save"
                                   <%} else {%>
                                   value="Next Step"
                                   <%}%>
                                   />
                            <% }else{ %>
                            <input type="submit" name="subBtnCreateEdit" id="sucolumnbBtnCreateEdit" onclick="return checkValidData(document.getElementById('frmTableCreation'));"
                                   <%if (isInEditMode) {%>
                                   value="Save"
                                   <%} else {%>
                                   value="Next Step"
                                   <%}%>
                                   />
                            <% } %>
                        </label>
                    </div>
                </form>
            </div>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </body>
    <% if(request.getParameter("action")!=null && "readFile".equalsIgnoreCase(request.getParameter("action"))){ %>
    <script type="text/javascript">

        var theform = document.getElementById('frmTableCreation');

        var colCount = parseInt(theform.cols.value);
        var rowcount = parseInt(theform.rows.value);

        rowcount = rowcount + 1;
        if (confirm("Are you sure to upload " + CSVRowCnt + " row and " + CSVColumnCnt + " col"))
        {
            document.getElementById('hdnTblChange').value = 'true';
            var temprowcount = CSVRowCnt;
            var tempcolcount = CSVColumnCnt;

            if (eval(rowcount) != eval(CSVRowCnt))
            {
                for (i = 0; i < (eval(CSVRowCnt) - eval(rowcount)); i++)
                {
                    addRow(theform, cmbWithCalcText, cmbWithCalcValue, cmbWoCalcText, cmbWoCalcValue);
                    // temprowcount++;
                }
            }
            if (eval(colCount) != eval(CSVColumnCnt))
            {
                for (i = 0; i < (eval(CSVColumnCnt) - eval(colCount)); i++)
                {
                    addCol(theform, <%=isBOQForm%>, cmbWithCalcText, cmbWithCalcValue, cmbWoCalcText, cmbWoCalcValue);
                    // tempcolcount++;
                }
            }
            colCount = tempcolcount;
            rowcount = temprowcount;

            var i;
            var j;

            for (i = 0; i < rowcount; i++)
            {
                for (j = 0; j < colCount; j++)
                {
                    if (csv_data[i][j] == null)
                    {
                        document.getElementById("Cell" + i + (j + 1)).value = "";
                    } else
                    {
                        if (i != 0)
                        {
                            document.getElementById("Cell" + i + "_" + (j + 1)).value = csv_data[i][j];
                        } 
                        else
                        {
                            document.getElementById("Header" + j).value = csv_data[i][j];
                        }
                    }
                }
            }
        }
    </script>
    <% } %>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <%
        if (tableMatrix != null) {
            tableMatrix = null;
        }
    %>
</html>