<%--
    Document   : CreateTenderFormTable
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.List" %>
<jsp:useBean id="tenderTblSrBean" class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create Table</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="include/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/form/Add.js"></script>
        <script type="text/javascript" src="../resources/js/form/CommonValidation.js"></script>
        <script type="text/javascript" src="../resources/js/form/CreateTable.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script>
            var $jq = jQuery.noConflict();

            function validate(form,isBoQ){
                var flag = true;
                var id = "";
                $jq('textArea[id^=TableName]').each(function(){
                    if($jq.trim($jq('#'+$jq(this).attr("id")).val()).length==0){
                        jAlert("Please enter Table Name" , 'Alert');
                        
                        flag = false;
                    }
                });
                 $jq('textArea[id^=TableHeader]').each(function(){
                    if($jq.trim(CKEDITOR.instances[$jq(this).attr("id")].getData()).length > 2000){
                        jAlert("only 2000 characters are allowed for Table Header" , 'Alert');
                        flag = false;
                    }


                });
                $jq('textArea[id^=TableHeader]').each(function(){
                    if($jq.trim(CKEDITOR.instances[$jq(this).attr("id")].getData()).length > 2000){
                        jAlert("only 2000 characters are allowed for Table Header" , 'Alert');
                        flag = false;
                    }


                });
                if(flag == true){
                  $jq('textArea[id^=TableFooter]').each(function(){
                    if($jq.trim(CKEDITOR.instances[$jq(this).attr("id")].getData()).length > 2000){
                        jAlert("only 2000 characters are allowed for Table Footercre" , 'Alert');
                        flag = false;
                    }

                });
                }
               if(flag == true){
                if(!checkTableEntryOk(form, isBoQ)){
                        flag = false;
                    }
                    else{
                         flag = true;
                    }
               }

                return flag;
            }

        </script>
    </head>
    <%
                short tenderId = 0;
                int sectionId = 0;
                int formId = 0;
                short noOfTable = 0;
                int tableId = 0;
                int pkgOrLotId = -1;
                boolean isEdit = false;

                if (request.getParameter("tenderId") != null) {
                    tenderId = Short.parseShort(request.getParameter("tenderId"));
                }
                if (request.getParameter("sectionId") != null) {
                    sectionId = Integer.parseInt(request.getParameter("sectionId"));
                }
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }
                if (request.getParameter("porlId") != null) {
                    pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                }
                if (request.getParameter("tableId") != null) {
                    isEdit = true;
                    tableId = Integer.parseInt(request.getParameter("tableId"));
                }
                if (request.getParameter("noOfTables") != null) {
                    noOfTable = Short.parseShort(request.getParameter("noOfTables"));
                } else {
                    noOfTable = 1;
                }
                String tblName = "";
                StringBuffer tblHeader = new StringBuffer();
                StringBuffer tblFooter = new StringBuffer();
                String isMultipleFilling = "";
                short noOfRows = 0;
                short noOfCols = 0;
                int templateTableId = 0;
                if (isEdit) {
                    List<com.cptu.egp.eps.model.table.TblTenderTables> tblDtl = tenderTblSrBean.getTenderTablesDetail(tableId);
                    if (tblDtl != null) {
                        if (tblDtl.size() > 0) {
                            tblName = tblDtl.get(0).getTableName();
                            tblHeader.append(tblDtl.get(0).getTableHeader());
                            tblFooter.append(tblDtl.get(0).getTableFooter());
                            isMultipleFilling = tblDtl.get(0).getIsMultipleFilling();
                            noOfCols = tblDtl.get(0).getNoOfCols();
                            noOfRows = tblDtl.get(0).getNoOfRows();
                            templateTableId = tblDtl.get(0).getTemplatetableId();
                        }
                        tblDtl = null;
                    }
                }

                boolean isBOQForm = tenderTblSrBean.isPriceBidForm(formId);
    %>
    <body>
        <div class="dashboard_div">
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <%
                //pageContext.setAttribute("tenderId", tenderId);
            %>
            <%--<%@include file="../resources/common/TenderInfoBar.jsp" %>--%>
            <!--Middle Content Table Start-->
            <div class="contentArea_1">
                <form action="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=formTableCreation" name="frmFormTable" id="frmFormTable" action="CreateFormMatrix.jsp" method="post">
                    <div class="pageHead_1">
                        <%if (isEdit) {
                                    out.print("Edit Table");
                                } else {
                                    out.print("Create Table");
                                }%>
                        <%if (pkgOrLotId == -1) {%>
                        <span style="float: right; text-align: right;">
                            <a class="action-button-goback" href="TenderTableDashboard.jsp?tenderId=<%= tenderId%>&sectionId=<%= sectionId%>&formId=<%= formId%>" title="Form Dashboard">Form Dashboard</a>
                        </span>
                        <%} else {%>
                        <span style="float: right; text-align: right;">
                            <a class="action-button-goback" href="TenderTableDashboard.jsp?tenderId=<%= tenderId%>&sectionId=<%= sectionId%>&formId=<%= formId%>&porlId=<%= pkgOrLotId%>" title="Form Dashboard">Form Dashboard</a>
                        </span>
                        <%}%>
                    </div>
                    <%if (!isEdit) {%>
                    <table width="100%" cellspacing="10" class="tableView_1 t_space">
                        <tr>
                            <td align="right">
                                <a class="action-button-add" href="#" onclick="AddNewTable('<%= isBOQForm%>');" title="Add Table">Add Table</a>
                                <a class="action-button-delete" href="#" onclick="DelTable(document.getElementById('frmFormTable'));" title="Delete Table">Delete Table</a>
                            </td>
                        </tr>
                    </table>
                    <%} else {%>
                    <input type="hidden" value="<%= tableId%>" name="tableId" id="tableId" />
                    <input type="hidden" value="<%= templateTableId%>" name="templateTableId" id="templateTableId" />
                    <%}%>
                    <input type="hidden" name="TableCount" id="TableCount" value="<%=noOfTable%>" />
                    <input type="hidden" value="0" name="noOfDelTables" id="noOfDelTables" />
                    <input type="hidden" value="" name="delTableArr" id="delTableArr" />

                    <input type="hidden" value="<%= tenderId%>" name="tenderId" id="tenderId" />
                    <input type="hidden" value="<%= sectionId%>" name="sectionId" id="sectionId" />
                    <input type="hidden" value="<%= formId%>" name="formId" id="formId" />
                    <%if(pkgOrLotId != -1){%>
                        <input type="hidden" name="porlId" id="porlId" value="<%= pkgOrLotId %>"/>
                    <%}%>.

                    <table width="100%" cellspacing="10" class="tableView_1">
                        <tr id="TableRow">
                            <%
                                        for (short i = 1; i <= noOfTable; i++) {
                            %>
                            <td id="tdTable_<%= i%>">
                                <%if (isEdit) {%>
                                <div class="formSubHead_1">Edit Table Details</div>
                                <%} else {%>
                                <div class="formSubHead_1">Enter Table Details
                                    <input type="checkbox" name="delTable" id="delTable_<%= i%>" />
                                </div>
                                <%}%>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                        <td width="20%" class="ff">Table Name : <span>*</span></td>
                                        <td width="80%"><textarea name="TableName<%=i%>" rows="3" class="formTxtBox_1" id="TableName<%=i%>" style="width:200px;"><%if (isEdit) {
                                                                                out.print(tblName.replace("<br/>", "\n"));
                                                                            }%></textarea></td>
                                    </tr>
                                    <tr>
                                        <td width="125" class="ff">Header : </td>
                                        <td>
                                            <textarea cols="80" id="TableHeader<%=i%>" name="TableHeader<%=i%>" rows="10"><%if (isEdit) {
                                                                                    out.print(tblHeader);
                                                                                }%></textarea>
                                            <script type="text/javascript">
                                                //<![CDATA[

                                                CKEDITOR.replace( 'TableHeader<%=i%>',
                                                {
                                                    //fullPage : false
                                                });

                                                //]]>
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="125" class="ff">Footer : </td>
                                        <td>
                                            <textarea cols="80" id="TableFooter<%=i%>" name="TableFooter<%=i%>" rows="10"><%if (isEdit) {
                                                                                    out.print(tblFooter);
                                                                                }%></textarea>
                                            <script type="text/javascript">
                                                //<![CDATA[

                                                CKEDITOR.replace( 'TableFooter<%=i%>',
                                                {
                                                    //fullPage : false
                                                });

                                                //]]>
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="125" class="ff">To be Filled multiple time :  <%if (!isEdit) {%><span>*</span><%}%></td>
                                        <td>
                                            <%
                                                                                    if (!isEdit) {
                                            %>
                                            <input type="radio" name="MultipleFilling<%=i%>" id="MultipleFillingY<%=i%>" value="yes" />Yes
                                            <input type="radio" name="MultipleFilling<%=i%>" id="MultipleFillingN<%=i%>" value="no" checked />No
                                            <%
                                                                                        } else {
                                                                                            if (templateTableId > 0) {
                                                                                                if ("yes".equals(isMultipleFilling)) {
                                                                                                    out.print("Yes");
                                                                                                } else {
                                                                                                    out.print("No");
                                                                                                }
                                            %>
                                            <input type="hidden" name="MultipleFilling<%=i%>" value="<%= isMultipleFilling%>" />
                                            <%
                                                                                            } else {
                                            %>
                                            <input type="radio" name="MultipleFilling<%=i%>" id="MultipleFillingY<%=i%>" value="yes" <%if ("yes".equals(isMultipleFilling)) {
                                                                                                    out.print("checked");
                                                                                                }%> />Yes
                                            <input type="radio" name="MultipleFilling<%=i%>" id="MultipleFillingN<%=i%>" value="no" <%if ("no".equals(isMultipleFilling)) {
                                                                                                    out.print("checked");
                                                                                                }%> />No
                                            <%
                                                                                        }
                                                                                    }
                                            %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="125" class="ff">No of Columns required : <%if (!isEdit) {%><span>*</span><%}%></td>
                                        <td>
                                            <%if (isEdit) {%>
                                            <%= noOfCols%><input name="NoOfCols<%=i%>" type="hidden" class="formTxtBox_1" id="NoOfCols<%=i%>" value="<%= noOfCols%>" maxlength="2" style="width:50px;" />
                                            <%} else {%>
                                            <input name="NoOfCols<%=i%>" type="text" class="formTxtBox_1" id="NoOfCols<%=i%>"  maxlength="2" style="width:50px;" />
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr <%if (isBOQForm) {
                                                                            out.print(" style='display:none' ");
                                                                        }%> >
                                        <td width="125" class="ff">No of Rows Required : <%if (!isEdit) {%><span>*</span><%}%></td>
                                        <td>
                                            <%if (isEdit) {%>
                                            <%= noOfRows%><input name="NoOfRows<%=i%>" type="hidden" class="formTxtBox_1" id="NoOfRows<%=i%>" value="<%= noOfRows%>" maxlength="2" style="width:50px;" />
                                            <%} else {%>
                                            <input name="NoOfRows<%=i%>" type="text" class="formTxtBox_1" id="NoOfRows<%=i%>" <%if (isBOQForm) {
                                                    out.print(" value='0' ");
                                                }%> maxlength="2" style="width:50px;" />
                                            <%}%>
                                        </td>
                                        <td colspan="2" style="display: none;">
                                            Save : <input type="checkbox" name="Check" value="<%=i%>" id="Check_<%=i%>" checked />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <%
                                        }
                            %>
                        </tr></table>
                    <div align="center">
                        <label class="formBtn_1">
                            <input type="submit" name="btnCreateEdit" id="btnCreateEdit"
                                   <%if (isEdit) {%>
                                   value="Save"
                                   <%} else {%>
                                   value="Next Step"
                                   <%}%>
                                    onclick="return validate(this.form, '<%= isBOQForm%>');" />
                        </label>
                    </div>
                </form>
            </div>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>

        <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
<%
            if (tblHeader != null) {
                tblHeader = null;
            }
            if (tblFooter != null) {
                tblFooter = null;
            }
            if (tenderTblSrBean != null) {
                tenderTblSrBean = null;
            }
%>