<%-- 
    Document   : NOAListing
    Created on : Jan 8, 2011, 6:15:28 PM
    Author     : rishita
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPayment"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    boolean isPerformanceSecurityPaid = false;
        %>
<%
    boolean isIctTender = false; String tendID=""; String pkgLotID=""; String roundID="";
    tendID = request.getParameter("tenderId");

    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    List<SPTenderCommonData> listDP1 = tenderCommonService1.returndata("chkDomesticPreference", tendID, null);
    System.out.println("**********************************SIZE"+listDP1.size());
    if(!listDP1.isEmpty() && listDP1.get(0).getFieldName1().equalsIgnoreCase("true"))
    {
        isIctTender = true;
    }

%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Contract Signing</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Contract Signing</div>
                <%
                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        tenderId = request.getParameter("tenderId");
                    }
                    String userId = "";
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = session.getAttribute("userId").toString();
                        issueNOASrBean.setLogUserId(userId);
                    }
                    pageContext.setAttribute("tab", "15");
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
                    String strProcNature = "";
                    String pck = "Lot";
                    if("Services".equalsIgnoreCase(procnature))
                    {
                        strProcNature = "Consultant";
                        pck = "Package";
                    }else if("goods".equalsIgnoreCase(procnature)){
                        strProcNature = "Supplier";
                    }else{
                        strProcNature = "Contractor";
                    }

                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>

                <%@include  file="officerTabPanel.jsp"%>
                <div class="tabPanelArea_1">
                    <div align="center">
                        <%for (Object[] obj : issueNOASrBean.getNOAListing(Integer.parseInt(tenderId))) {
                                        List<TblTenderPayment> detailsPayment = issueNOASrBean.getPaymentDetails(Integer.parseInt(tenderId), (Integer) obj[0], (Integer) obj[8]);
                                        SimpleDateFormat simpl = new SimpleDateFormat("dd-MMM-yyyy HH:mm");

                                        //added by salahuddin
                                        roundID = obj[11].toString();
                                        pkgLotID = obj[0].toString();
                        %>

                        <table cellspacing="0" class="tableList_1" width="100%">
                            <tr>
                                <td class="ff" width="12%" class="t-align-left"><%=pck%> No. :</td>
                                <td   class="t-align-left"><%=obj[6]%></td>
                            </tr><tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=pck%> Description :</td>
                                <td  class="t-align-left"><%=obj[7]%></td>
                            </tr>
                        </table>
                        <jsp:include page="../resources/common/NOADocInclude.jsp" >
                            <jsp:param name="comtenderId" value="<%=tenderId%>" />
                            <jsp:param name="comlotId" value="<%=obj[0]%>" />
                            <jsp:param name="comuserId" value="<%=obj[8]%>" />
                            <jsp:param name="comroundId" value="<%=obj[11]%>" />
                        </jsp:include>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="15%" class="t-align-left">Name of <%=strProcNature%> </th>
                                <th width="10%" class="t-align-left">Contract No.</th>
                                <%if(!isIctTender){%>
                                    <th width="15%" class="t-align-left">Contract Amount in Figure (in Nu.)</th>
                                    <th width="15%" class="t-align-left">Advance Contract Amount in Figure <br/> (in Nu.)</th>
                               <%}else{%>
                                    <th width="30%" class="t-align-left">Contract Details</th>
                               <%}%>
                                <th width="15%" class="t-align-left">Date of Issue</th>
                                <th width="15%" class="t-align-left">Last Acceptance Date and Time</th>
                                <th width="15%" class="t-align-left">Letter of Acceptance (LOA) Acceptance Status</th>
                                <%
                                List<Object[]> objPerSec = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                                if(!objPerSec.isEmpty() && objPerSec.get(0)[1].toString().equalsIgnoreCase("yes")){
                                %>
                                <th width="15%" class="t-align-left">Performance Security </th>
                                <%}%>
                                <th width="10%" class="t-align-left">Action</th>
                            </tr>
                            <tr>
                                <td style="text-align: left;"><%=obj[9]%></td>
                                <td class="t-align-center" > <%=obj[2]%></td>
                                <%if(!isIctTender){%>
                                <td style="text-align: right"> <%=obj[13]%></td>
                                <td style="text-align: right" >
                                <%
                                    if(obj[12]!=null){
                                        out.print(new BigDecimal((new BigDecimal(obj[13].toString()).doubleValue()*new BigDecimal(obj[12].toString()).doubleValue())/100).setScale(3,0));
                                    }else{
                                        out.print("-");
                                    }
                                %>
                                </td>
                                <%}else{%>
                                  <td class="t-align-center"><a onclick="javascript:window.open('ViewContractAmountNOA.jsp?tenderid=<%=tendID%>&roundId=<%=roundID%>&pckLotId=<%=pkgLotID%>', '', 'resizable=yes,scrollbars=1','');" href="javascript:void(0);">View</a> </td>
                                <%}%>

                                <td class="t-align-center" > <%=DateUtils.gridDateToStrWithoutSec((Date) obj[3])%></td>
                                <%if (obj[5] != null) {%>
                                <td class="t-align-center" > <%=DateUtils.gridDateToStrWithoutSec((Date) obj[5])%></td>
                                <%}
                                    if (obj[4].toString().equalsIgnoreCase("approved")) {
                                %>
                                <td class="t-align-center">Accepted</td>
                                <%} else if (obj[4].toString().equalsIgnoreCase("declined")) {%>
                                <td class="t-align-center">Declined</td>
                                <% }
                                if(!objPerSec.isEmpty() && objPerSec.get(0)[1].toString().equalsIgnoreCase("yes")){
                                if (detailsPayment.isEmpty()) {%>
                                <td class="t-align-center" >Pending</td>
                                <%} else {
                                            isPerformanceSecurityPaid = true;
                                            %>
                                <td class="t-align-center" >Received</td>

                                <% }
                                }else{
                                    isPerformanceSecurityPaid  = true;
                                }
                                    if (issueNOASrBean.isAvlTCS((Integer) obj[1])) {
                                %>
                                <td class="t-align-center"><a href="ManageContractAgreement.jsp?noaIssueId=<%=obj[1]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>">View</a></td>
                                <%} else if(isPerformanceSecurityPaid) {%>
                                <td class="t-align-center"><a href="ContractAgreement.jsp?noaIssueId=<%=obj[1]%>&tenderId=<%=tenderId%>">Enter Contract Details</a></td>
                                <%}else{%>
                                <td class="t-align-center">-</td>
                                <%}%>
                            </tr>
                        </table>
                        <%}%>
                    </div>
                </div>
            </div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
<%
            issueNOASrBean = null;
            tenderSrBean = null;
%>
