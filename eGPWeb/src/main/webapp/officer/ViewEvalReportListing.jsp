<%-- 
    Document   : ViewEvalReportListing
    Created on : Aug 9, 2011, 3:33:13 PM
    Author     : nishit
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View Evaluation Reports</title>
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
</head>
<body>
<div class="dashboard_div"> 
  <!--Dashboard Header Start-->
  <div class="topHeader">
   <%@include file="../resources/common/AfterLoginTop.jsp" %>
  </div>
  <!--Dashboard Header End--> 
  <%
        String s_tenderid = "0";
        String s_lotId = "0";
        String s_rId = "0";
        String strEvalFFId = "0";
        String strSecretory = "";
        if(request.getParameter("tenderid")!=null){
                 s_tenderid = request.getParameter("tenderid");
                }
        if(request.getParameter("evalRptFFid")!=null){
            strEvalFFId = request.getParameter("evalRptFFid");
        }

                if(request.getParameter("lotId")!=null){
                 s_lotId = request.getParameter("lotId");
                }

        if(request.getParameter("isSU")!=null){
         strSecretory = request.getParameter("isSU");
        }
        if(request.getParameter("rId")!=null){
             s_rId = request.getParameter("rId");
        }
                List<Object[]> list_status = new ArrayList<Object[]>();
                List<Object[]> list_roundadd = new ArrayList<Object[]>();
                EvaluationService evalService = (EvaluationService)AppContext.getSpringBean("EvaluationService");
                
                List<Object[]> list_lorPkg = new ArrayList<Object[]>();
                CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                boolean b_isTenPackageWise = false;
                String s_title = commonService.getProcNature(s_tenderid).toString();
                int i_cnt = 0;
               pageContext.setAttribute("tenderId", s_tenderid);

                    s_lotId = "0";
                    if("Services".equalsIgnoreCase(s_title)){
                         s_title = "Package";
                         b_isTenPackageWise = true;
                        list_lorPkg = commonService.getPkgDetialByTenderId(Integer.parseInt(s_tenderid));
                    }else{
                          s_title = "Lot";
                        list_lorPkg = commonService.getLotDetailsByTenderIdforEval(s_tenderid);
        }
        for(Object[] list_detial : list_lorPkg){
            if("Lot".equalsIgnoreCase(s_title)){
                s_lotId = list_detial[2].toString();
                break;
            }
        }
     %>
  <!--Dashboard Content Part Start-->
  <div class="DashboardContainer">
      <div class="contentArea_1">
    <div class="pageHead_1">View Evaluation Reports
    <span class="c-alignment-right">
    <%if("yes".equalsIgnoreCase(request.getParameter("isSU"))){%>
        <a href="ReviewReportProcessing.jsp?tenderid=<%=s_tenderid%>&lotId=<%if(b_isTenPackageWise){%>0<%}else{%><%=s_lotId%><%}%>&rId=<%=s_rId%>&evalRptFFid=<%=strEvalFFId%>" title="Go Back" class="action-button-goback">Go Back</a>
    <%} else {%>
        <a href="EvalComm.jsp?tenderid=<%=s_tenderid%>" title="Go Back" class="action-button-goback">Go Back</a>
    <%} %>
    </span>
    </div>
    <div class="t_space">
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
    </div>
                    <%for(Object[] list_detial : list_lorPkg){
                        if("Lot".equalsIgnoreCase(s_title)){
                            s_lotId = list_detial[2].toString();
                          }
                    %>
  <table width="100%" cellspacing="0" class="tableList_3 t_space">
        <tr>
            <td width="15%" class="ff"><%=s_title%> No. </td> <td><%=list_detial[0]%></td>
        </tr>
        <tr>
          <td class="ff"><%=s_title%> Description</td> <td class="table-description"><%=list_detial[1]%></td>
        </tr>
        <%
        boolean is2Env = false;
        CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
        List<SPCommonSearchDataMore> envDataMores = dataMore.geteGPData("GetTenderEnvCount", s_tenderid);
        if(envDataMores!=null && (!envDataMores.isEmpty()) && envDataMores.get(0).getFieldName1().equals("2")){
            is2Env = true;
        }
        list_status =  evalService.forReprotProcessView(Integer.parseInt(s_tenderid), Integer.parseInt(s_lotId));
            for(int i=0;i<list_status.size();i++){
                if("0".equalsIgnoreCase(list_status.get(i)[4].toString())){
                 %>
        <table width="100%" cellspacing="0" class="tableList_3">
            <tr>
                    <th width="15%">Report Type</th>
                    <th width="60%">Status</th>
                    <th width="25%" class="t-align-center">Action</th>
            </tr>
            <tr>
                <td>
                    <%
                    String s_rptName = "Evaluation Report";
                    if(is2Env){
                        if("0".equals(list_status.get(i)[3].toString().trim()) ){
                            s_rptName = "Technical Report";
                        }else{
                            s_rptName ="Financial Report";
                        }
                    }
                    %>
                    <%=s_rptName%>
                </td>
                <td>
                    <%=list_status.get(i)[0]%>
                </td>
          
                <td class="t-align-center">
                     <a href="ViewEvalReport.jsp?tenderid=<%=s_tenderid%>&lotId=<%if(b_isTenPackageWise){%>0<%}else{%><%=list_detial[2]%><%}%>&evalRptToAaid=<%=list_status.get(i)[2]%>&rId=<%=s_rId%>&isSU=<%=strSecretory%>&lnk=view&s_aaFFId=<%=strEvalFFId%>">View</a>
                </td>
            </tr>
        </table>
          <%} else { /*if Conditon ends here*/
                                  list_roundadd.add(list_status.get(i));
                               }
                               }/*For Loop ends here*/
        if(list_roundadd!=null && !list_roundadd.isEmpty()){
        List<SPCommonSearchDataMore> getRoundDetial = dataMore.geteGPData("getRoundsforEvaluationForLotId", s_tenderid, s_lotId, "L1");
        String cmpName = "";
        for(SPCommonSearchDataMore getRoundBidder : getRoundDetial){
        for(int i=0;i<list_roundadd.size();i++){
                if(list_roundadd.get(i)[4].toString().equals(getRoundBidder.getFieldName1())){
                    s_rId = list_roundadd.get(i)[4].toString();
                    cmpName = getRoundBidder.getFieldName3();
%>
<table width="100%" cellspacing="0" class="tableList_3">
    <tr>
        <th width="25%" class="t-align-center">
            <%=cmpName%>
        </th>
    </tr>
</table>
        <table width="100%" cellspacing="0" class="tableList_3">
            <tr>
                <th width="15%">Report Type</th>
                <th width="60%">Status</th>
                <th width="25%" class="t-align-center">Action</th>
            </tr>
            <tr>
                <td>
                    <%
                    String s_rptName = "Evaluation Report";
                    if(is2Env){
                        if("0".equalsIgnoreCase(list_status.get(i_cnt)[3].toString()) ){
                            s_rptName = "Technical Report";
                        }else{
                            s_rptName ="PriceBid Report";
                        }
                    }
                    %>
                    <%=s_rptName%>
                </td>
                      <td>
                          <%=list_roundadd.get(i)[0]%>
                      </td>
          
                <td class="t-align-center">
                    <a href="ViewEvalReport.jsp?tenderid=<%=s_tenderid%>&lotId=<%if(b_isTenPackageWise){%>0<%}else{%><%=list_detial[2]%><%}%>&evalRptToAaid=<%=list_roundadd.get(i)[2]%>&rId=<%=s_rId%>&isSU=<%=strSecretory%>&lnk=view&s_aaFFId=<%=strEvalFFId%>">View</a>
                </td>
            </tr>
        </table>
          <%}/*If condtion ends here*/
            if(getRoundDetial!=null){
                getRoundDetial = null;
            }
            }/*For of spCommentSearchdataMore ends here*/
                }/*For of list round Details ends here*/
     }/*If size is not zero condtion ends here*/%>
      </table>
          <%
            if(list_detial!=null){
                list_detial = null;
            }
        //list_status.clear();
       // list_status = null;
        //list_roundadd.clear();
       // list_roundadd = null;
            
        }%>
      </div>
  </div>
  <!--Dashboard Content Part End--> 
  
  <!--Dashboard Footer Start-->
 <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End--> 
</div>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabEval");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
</script>
</body>
</html>
<%
if(list_status!=null){
    list_status.clear();
    list_status=null;
} 
if(list_lorPkg!=null){
    list_lorPkg.clear();
    list_lorPkg=null;
} 
        
%>
