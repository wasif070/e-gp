<%-- 
    Document   : EvalViewSeekClari
    Created on : Jan 4, 2011, 5:12:17 PM
    Author     : Swati
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService,com.cptu.egp.eps.service.serviceimpl.CommonSearchService,com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<html>
    <head>
       <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Post Query</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmPostQuestion").validate({
                    rules: {
                        txtQuestion: {required: true,maxlength:2000}
                    },
                    messages: {
                        txtQuestion: { required: "<div class='reqF_1'>Please Enter Question</div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 2000 characters only</div>"}
                  }
                });
            });
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <%

            String cuserId = "", strCPGovUserId = "0", strMemGovUserId = "0";
            String userId = "0",evalMemStatusId="",tenderFormId="",memAnswerBy="";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    cuserId = session.getAttribute("userId").toString();
                }

            if (session.getAttribute("govUserId") != null) {
                strCPGovUserId = session.getAttribute("govUserId").toString();
            }
            
             if (request.getParameter("userId") != null) {
                     userId = request.getParameter("userId").toString();
                }
             if (request.getParameter("evalMemStatusId") != null) {
                     evalMemStatusId = request.getParameter("evalMemStatusId").toString();
                }
            if (request.getParameter("tenderFormId") != null) {
                     tenderFormId = request.getParameter("tenderFormId").toString();
                }
            if (request.getParameter("memAnswerBy") != null) {
                     memAnswerBy = request.getParameter("memAnswerBy").toString();
                }

            List<SPCommonSearchData> lstMemGovUserId =
                    commonSearchService.searchData("getGovUserIdFromUserId",memAnswerBy, null,  null, null, null, null, null, null, null);
            if(!lstMemGovUserId.isEmpty()){
                strMemGovUserId=lstMemGovUserId.get(0).getFieldName1(); // Member GovUserId
            }
            lstMemGovUserId = null;
            
            if ("Submit".equals(request.getParameter("btnSubmit"))) {
                try{
                            HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                            String postXml = "";
                            String memAnswer = "",memAnwserDt ="",noteOfDescent="";
                             postXml = "<root><tbl_EvalCpMemClarification evalMemStatusId=\"" + evalMemStatusId
                                    + "\" cpQuestion=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtQuestion")) + "\" memAnswer=\"" + memAnswer
                                    + "\" cpQuestionDt=\"" + format.format(new Date()) + "\" memAnwserDt=\"" + memAnwserDt
                                    + "\" cpQuestionBy=\"" + cuserId + "\" memAnswerBy=\"" + memAnswerBy+ "\" tenderFormId=\"" + tenderFormId + "\" userId=\"" + userId+ "\" noteOfDescent=\"" + noteOfDescent
                                    + "\" cpcomments=\"\" cpGovUserId=\"" + strCPGovUserId + "\" memGovUserId=\"" + strMemGovUserId
                                    + "\" /></root>";
                                    
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert","tbl_EvalCpMemClarification",postXml,"").get(0);
                   }catch(Exception e){
                       e.printStackTrace();
                       }
                   }
            // Delete the question
             if (request.getParameter("qId") != null && request.getParameter("st") != null) {

                            String strCond = "cpQuestionBy=" + cuserId + " And evalCpQueId=" + request.getParameter("qId") ;
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("delete","tbl_EvalCpMemClarification","",strCond).get(0);
                        }
            //http://localhost:8080/eGPWeb/officer/EvalTenderer.jsp?tenderId=1218&userId=35#
            //http://localhost:8080/eGPWeb/officer/SeekEvalClari.jsp?uId=6&tenderId=1218
            //http://localhost:8080/eGPWeb/officer/EvalClari.jsp?uId=6&tenderId=1218#
            %>
             <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Post Query</div>

                <form id="frmPostQuestion" name="frmPostQuestion" method="POST" action="EvalViewSeekClari.jsp?cuserId=<%=cuserId%>&evalMemStatusId=<%=evalMemStatusId%>&userId=<%=userId%>&tenderFormId=<%=tenderFormId%>&memAnswerBy=<%=memAnswerBy%>">
                    <table width="80%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td width="15%" valign="middle" class="t-align-left ff">Clarification :<span style="color: red;"> *</span></td>
                            <td width="90%" class="t-align-left">
                                <textarea rows="5" cols="105" id="txtQuestion" name="txtQuestion" class="formTxtBox_1"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-center t_space" colspan="2">
                                <label class="formBtn_1">
                                    <input name="btnSubmit" type="submit" value="Submit" />
                                </label>
                            </td>
                        </tr>
                    </table>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th class="t-align-left">Sl. No.</th>
                            <th>Asked To</th>
                            <th>Query</th>
                            <th>Clarification</th>
                            <th>Action</th>
                        </tr>
                        <%
                            int QusCnt = 0;
                            
                            List<SPCommonSearchData> QuestionList = commonSearchService.searchData("SeekClarTECCP",tenderFormId, userId,  null, null, null, null, null, null, null);
                             if (!QuestionList.isEmpty() && QuestionList.size() > 0) {
                             for (SPCommonSearchData Question : QuestionList)
                               {
                                 QusCnt++;
                        %>
                        <tr>
                            <td width="5%" class="t-align-center"><%=QusCnt%></td>
                            <td width="15%" class="t-align-left"><%=Question.getFieldName7()%></td>
                            <td width="35%" class="t-align-left"><div class="break-word" style="width: 400px;"><%=Question.getFieldName1()%></div></td>
                            <td width="35%" class="t-align-left"><div class="break-word" style="width: 400px;"><%=Question.getFieldName2()%></div></td>
                            <td width="10%" class="t-align-center"><a href="EvalViewSeekClari.jsp?qId=<%=Question.getFieldName5()%>&st=d&cuserId=<%=cuserId%>&evalMemStatusId=<%=evalMemStatusId%>&userId=<%=userId%>&tenderFormId=<%=tenderFormId%>&memAnswerBy=<%=memAnswerBy%>">Remove</a></td>
                        </tr>
                        <%}
                            } else {%>
                            <tr>
                                <td colspan="5">No Questions Found</td>
                            </tr>
                       <% } %>
                    </table>
                </form>
            </div>
            <div>&nbsp;</div>
        </div>
    </body>

<script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
