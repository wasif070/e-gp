
<%--
    Document   : EvalConfiguration
    Created on : Feb 21, 2011, 11:54:17 AM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommitteMemDtBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    HttpSession session1 = request.getSession();
                    //if ((session1.getAttribute("userId") == null || session1.getAttribute("userName") == null || session1.getAttribute("userTypeId") == null)) {
                        //response.sendRedirectFilter(request.getContextPath() + "/Logout.jsp"); //Index.jsp
                   // }
        %>

        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <script type="text/javascript">
            function Clear(){
                $('#err1').html('');
                $('#err2').html('');
            }

            function SetTECMemberId(val){
                //alert(val);
                document.getElementById("hdnTECMemberId").value=val;
            }


            $(document).ready(function(){

                if($('#hdnAction').val()=="add"){
                    document.getElementById("rbEvalTypeInd").checked="checked";
                    if(document.getElementById("rbEvalTypeInd").checked){
                        $('#hdnEvalType').val('ind');
                        $('#hdnTECMemberId').val('0');
                        $('#dvMembers').hide();
                    }
                }

            $('#rbEvalTypeInd').click(function(){
                //alert('Individual');
                $('#hdnEvalType').val('ind');
                $('#hdnTECMemberId').val('0');
                Clear();
                $('#dvMembers').hide();
            });

            $('#rbEvalTypeTeam').click(function(){
                //alert('Team');
                $('#hdnEvalType').val('team');
                Clear();
                $('#dvMembers').show();

            });

            $('#frmEvalConfig').submit(function(){
                var errFlag=false;
                isInd = document.getElementById("rbEvalTypeInd").checked;
                isTeam = document.getElementById("rbEvalTypeTeam").checked;

                if(!isInd && !isTeam){
                    $('#err1').html('Please select Evaluation Type');
                    errFlag=true;
                } else{
                    $('#err1').html('');
                }

                if(isTeam){

                    var isTSCMemSelected=false;
                    var TSCMemCnt=$('#hdnTSCMemberCnt').val();
                    //alert(TSCMemCnt);
                    for(i=1;i<=TSCMemCnt;i++){
                        var currObj=document.getElementById("rbTSCMember_" + i.toString());
                        //alert(currObj);
                        if(currObj.checked){
                            isTSCMemSelected=true;
                        }
                    }

                    if(!isTSCMemSelected){
                        $('#err2').html('Please Select TEC Team Member');
                        errFlag=true;
                    } else {$('#err2').html('');}
                }

                if(errFlag){
                    return false;
                }else{
                    $('#btnSubmit').attr("disabled","disabled");                    
                }
            });
            });
        </script>

    </head>
    <body>
        <%
String tenderId = request.getParameter("tenderid");
String currAction="";
    if(request.getParameter("action")!=null){
        currAction = request.getParameter("action");
    }

%>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">

            <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
            %>
            <div class="pageHead_1">Configuration of Evaluation Methodology
                <span class="c-alignment-right"><a href="EvalComm.jsp?tenderid=<%=request.getParameter("tenderid")%>" title="Tender/Proposal Document" class="action-button-goback">Go Back</a></span>
            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>

            <div>&nbsp;</div>

            <form method="post" name="frmEvalConfig" id="frmEvalConfig" action="<%=request.getContextPath()%>/EvalConfigurationServlet?func=configuration&tenderId=<%=tenderId%>&action=<%=currAction%>&govUserNm=<%=objUserName%>" method="post">
                <div class="tabPanelArea_1">
                    <%
                                pageContext.setAttribute("TSCtab", "0");
                    %>

                    <%-- Start: CODE TO DISPLAY MESSAGES --%>
                    <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("tscpublished")){
                            msgTxt="TSC publised successfully.";
                        } else  if(msgId.equalsIgnoreCase("tscrequested")){
                            msgTxt="TSC formation request sent successfully.";
                        } else  if(msgId.equalsIgnoreCase("tscrequesterr")){
                            isError=true; msgTxt="Error while requesting TSC formation.";
                        }  else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                   <div class="responseMsg errorMsg" style="margin-bottom: 12px;" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                   <%}%>
                <%}
                msgId = null;
                msgTxt = null;
                }%>
                <%-- End: CODE TO DISPLAY MESSAGES --%>

                  <%-- Start: Common Evaluation Table --%>
                <%@include file="/officer/EvalCommCommon.jsp" %>
               <%-- End: Common Evaluation Table --%>

                <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1">
                    <tr>
                        <th colspan="2">Configure Evaluation Methodology</th>
                    </tr>
                    <tr style="display: none;">
                        <td style="width: 15%" class="ff">Request PE for Formation of TSC</td>
                        <td>
                            <%
                            boolean isConfigExists=false;
                            boolean isTscReq=false;
                            boolean isTSC_SentToAA=false;
                            String evalCommittee="";
                            String evalConfigId="0";
                            String evalStatus="";
                            String tsc_Status="";

                            List<SPTenderCommonData> lstTenderEvalConfigStatus =
                                    tenderCommonService.returndata("getTenderEvalConfigStatus", tenderId, null);

                            if(!lstTenderEvalConfigStatus.isEmpty()){
                                 if("yes".equalsIgnoreCase(lstTenderEvalConfigStatus.get(0).getFieldName1())){
                                    isConfigExists=true;
                                    evalConfigId=lstTenderEvalConfigStatus.get(0).getFieldName2();

                                    if("yes".equalsIgnoreCase(lstTenderEvalConfigStatus.get(0).getFieldName3())){
                                        isTscReq=true;
                                    }
                                    evalStatus=lstTenderEvalConfigStatus.get(0).getFieldName4();
                                }
                            }
                            lstTenderEvalConfigStatus = null;

                            List<SPTenderCommonData> lstTSCRequestStatus =
                                    tenderCommonService.returndata("getTSCRequestStatus", tenderId, null);

                            if(!lstTSCRequestStatus.isEmpty()){

                                if("yes".equalsIgnoreCase(lstTSCRequestStatus.get(0).getFieldName1())){
                                    isTscReq=true;
                                    isTSC_SentToAA=true;
                                }

                                tsc_Status=lstTSCRequestStatus.get(0).getFieldName2();

                            }
                           // lstTSCRequestStatus = null;
                            %>

                            <%if(isConfigExists){%>
                                <%if(isTscReq){%>
                                    <%if(!lstTSCRequestStatus.isEmpty()){%>
                                        <%if(isTSC_SentToAA){%>
                                            <%if("Pending".equalsIgnoreCase(tsc_Status)){%>
                                                Waiting for Approval from AA
                                            <%} else if("Reject".equalsIgnoreCase(tsc_Status)){%>
                                                TSC Rejected by AA
                                            <%} else if("Approve".equalsIgnoreCase(tsc_Status)){%>
                                                TSC Approved by AA
                                            <%}%>
                                        <%}%>
                                    <%} else {%>
                                        PE has yet not sent TSC to AA for Approval
                                    <%}%>
                                <%}%>
                            <%} else {%>
                                    <%if(!lstTSCRequestStatus.isEmpty()){%>
                                        <%if(isTSC_SentToAA){%>
                                            <%if("Pending".equalsIgnoreCase(tsc_Status)){%>
                                                Waiting for Approval from AA
                                            <%} else if("Reject".equalsIgnoreCase(tsc_Status)){%>
                                                TSC Rejected by AA
                                            <%} else if("Approve".equalsIgnoreCase(tsc_Status)){%>
                                                TSC Approved by AA
                                            <%}%>
                                        <%}%>
                                    <%} else {%>
                                        <%if(request.getParameter("msgId")!=null){%>
                                            <%if("tscrequested".equalsIgnoreCase(request.getParameter("msgId"))){%>
                                                PE has yet not sent TSC to AA for Approval
                                            <%} else {%>
                                            <a href="<%=request.getContextPath()%>/EvalConfigurationServlet?func=requesttsc&tenderId=<%=tenderId%>&refNo=<%=toextTenderRefNo%>&action=<%=currAction%>">Make Request</a>
                                            <%}%>
                                        <%} else {%>
                                            <a href="<%=request.getContextPath()%>/EvalConfigurationServlet?func=requesttsc&tenderId=<%=tenderId%>&refNo=<%=toextTenderRefNo%>&action=<%=currAction%>">Make Request</a>
                                    <%}%>
                            <%}%>
                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 22%" class="ff">Evaluation Type</td>
                        <td>                            
                            <label><input type="radio" name="rbEvalType" id="rbEvalTypeInd" value="ind" checked/>Individual</label>
                            &nbsp;                            
                            <label><input type="radio" name="rbEvalType" id="rbEvalTypeTeam" value="team" />Team</label>
                            <span id="err1" class="reqF_1"></span>
                            <div id="dvMembers">
                                <%
                                int tscMemberCnt = 0;
            for (SPTenderCommonData commonTenderDetails : tenderCommonService1.returndata("getTenderEvaluationinfo", tenderId, null)) {
                if (!"chairperson".equalsIgnoreCase(commonTenderDetails.getFieldName3())) {
                    tscMemberCnt++;
                    evalCommittee = evalCommittee + commonTenderDetails.getFieldName1() + ",";
                                %>

                                <%if(tscMemberCnt==1){%>
                                <br>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1">
                                    <th colspan="2">Select Member</th>
                                <%}%>
                                <%if(!"chairperson".equalsIgnoreCase(commonTenderDetails.getFieldName3())){%>
                                <tr>
                                    <td>
                                        <label>
                                        <input type="radio" name="rbTSCMember" id="rbTSCMember_<%=tscMemberCnt%>" value="<%=commonTenderDetails.getFieldName7()%>" onclick="return SetTECMemberId(this.value)" />
                                        <%=commonTenderDetails.getFieldName1()%>
                                        </label>
                                    </td>
                                </tr>
                                <%}%>
                                <%}%>
                                <%}%>
                                <%if(tscMemberCnt>0){%>
                                </table>
                                <%
                                // Start: Do formatting for Eval Committe String
                                    if(evalCommittee.indexOf(",")>0){
                                    evalCommittee=evalCommittee.substring(0, evalCommittee.lastIndexOf(","));
                                    }

                                    evalCommittee=evalCommittee.trim();
                                // End: Do formatting for Eval Committe String
                                %>
                                <%}%>
                                <span id="err2" class="reqF_1"></span>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <div class="t-align-left">
                                <label class="formBtn_1">
                                    <input name="btnSubmit" id="btnSubmit" type="submit" value="Submit" />
                                        <input type="hidden" name="hdnTSCMemberCnt" id="hdnTSCMemberCnt" value="<%=tscMemberCnt%>">
                                </label>
                            </div>

                        </td>
                    </tr>
                </table>

                </div>


               <input type="hidden" name="hdnTSCReq" id="hdnTSCReq"
                      <%if(isTscReq){%>
                      value="Yes"
                      <%} else {%>
                        <%if(request.getParameter("msgId")!=null){%>
                            <%if("tscrequested".equalsIgnoreCase(request.getParameter("msgId"))){%>
                                value="Yes"
                            <%} else {%>
                                value="No"
                            <%}%>
                        <%} else {%>
                            value="No"
                        <%}%>
                      <%}%>
                      >

               <input type="hidden" name="hdnAction" id="hdnAction" value="<%=currAction%>">
               <input type="hidden" name="hdnEvalType" id="hdnEvalType" >
               <input type="hidden" name="hdnEvalCommittee" id="hdnEvalCommittee" value="<%=evalCommittee%>" >
               <input type="hidden" name="hdnTECMemberId" id="hdnTECMemberId" >
               <input type="hidden" name="hdnTenderRefNo" id="hdnTenderRefNo" value="<%=toextTenderRefNo%>" >

               <%
                evalCommittee=null;
                evalConfigId=null;
                evalStatus=null;
                tsc_Status=null;
                tenderId=null;
                currAction=null;
                %>
            </form>
        </div>

        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>

    </body>

</html>

