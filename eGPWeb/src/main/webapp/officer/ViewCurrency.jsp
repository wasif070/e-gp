<%--
    Document   : ViewCurrency
    Created on : Aug 30, 2012, 3:29:07 PM
    Author     : Md. khaled Ben Islam
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <%
            // Variable tenderId is defined by u on ur current page.
            pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Currency</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <!--jalert -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">
            <div class="pageHead_1">View Currency <span style="float:right;"><a href="Notice.jsp?tenderid=<%=request.getParameter("tenderid")%>" class="action-button-goback">Go Back To Dashboard</a></span>
            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>

            <div>&nbsp;</div>
            <div class="tabPanelArea_1 ">
                <form name="currencyInfo" method="post" action="">
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <thead>
                            <tr>
                                <th width="15%" class="t-align-center">No.</th>
                                <th width="70%" class="t-align-left">Currency Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                String currencyRows = ""; //will hold the result
                                int iTenderId = Integer.parseInt(request.getParameter("tenderid"));

                                List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
                                TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                                listCurrencyObj = tenderCurrencyService.getCurrencyTenderwise(iTenderId);

                                if(listCurrencyObj.size() > 0)
                                {
                                    int rowNo = 0;
                                    for(Object[] obj :listCurrencyObj)
                                    {
                                        rowNo +=1;
                                        currencyRows+= "<tr>";
                                        currencyRows+= "<td width='15%' class='t-align-center'>" + rowNo + "</td>";
                                        if(obj[3].equals("BTN") || obj[3].equals("bdt"))
                                        {
                                            currencyRows+= "<td width='70%' class='t-align-left'> BTN - Bhutanese Ngultrum</td>"; //Obj[0] holds Currency Name, Obj[1] holds Currency Rate, Obj[2] holds Currency ID, obj[3] holds Currency Short Name (e.g. BTN)
                                        }
                                        else
                                        {
                                            currencyRows+= "<td width='70%' class='t-align-left'>" + obj[3] + " - " + obj[0] + "</td>"; //Obj[0] holds Currency Name, Obj[1] holds Currency Rate, Obj[2] holds Currency ID, obj[3] holds Currency Short Name (e.g. BTN)
                                        }
                                    }

                                        currencyRows+= "</tr>";
                                }
                                out.print(currencyRows);
                            %>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <div>&nbsp;</div>

        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

    </body>
</html>