<%-- 
    Document   : EvalComMemberReport
    Created on : Mar 22, 2011, 10:21:52 AM
    Author     : TaherT
--%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Finalize Responsiveness of Consultants</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script type="text/javascript">
            function validate(){
                $(".err").remove();
                var valid=true;
                if($.trim($("#comments").val()) == "") {
                    $("#comments").parent().append("<div class='err' style='color:red;'>Please enter comments.</div>");
                    valid=false;
                }else{
                    if($.trim($("#comments").val()).length>500) {
                        $("#comments").parent().append("<div class='err' style='color:red;'>Maximum 500 characters allowed.</div>");
                        valid=false;
                    }
                }
                return valid;
            }
        </script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%

                    String Status = "";
                    String tendID = request.getParameter("tenderId");
                    String bidderID = request.getParameter("uId");
                    EvalServiceCriteriaService criteriaService = (EvalServiceCriteriaService) AppContext.getSpringBean("EvalServiceCriteriaService");
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    List<Object> formIds = criteriaService.getBiddersFormId(bidderID, tendID);
                    List<Object[]> memberName = criteriaService.getComMemberName(tendID, bidderID);
                    boolean isREOI = false;
                    boolean isRFP = false;
                    boolean isService = false;
                    if (commonService.getEventType(String.valueOf(tendID)).toString().equals("REOI")) {
                        isREOI = true;
                    }
                    else if (commonService.getEventType(String.valueOf(tendID)).toString().equals("RFP")) {
                        isRFP = true;
                    }
                    if (commonService.getProcNature(String.valueOf(tendID)).toString().equals("Services")) {
                        isService = true;
                    }
                    String hideHTML = isREOI ? " style='display: none;' " : " ";
                    double marksObtained = 0;
                    double passingMarks = 0;
                    String result = null;
                    String stat = null;
                    if ("eval".equals(request.getParameter("stat"))) {
                        stat = "evaluation";
                    } else {
                        stat = "reevaluation";
                    }
                    int empId = 0;
                    if (!memberName.isEmpty()) {
                        empId = Integer.parseInt(memberName.get(0)[0].toString());
                    }
                    int[] marksCount = new int[memberName.size()];
                    
					//Change by dohatec for re-evaluation
                    EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                    int evalCount = evalService.getEvaluationNo(Integer.parseInt(tendID));
                    List<Object[]> bidderResult = criteriaService.getBidderResult(tendID, bidderID, stat,evalCount);
                    
        %>
        <div class="tabPanelArea_1">
            <div class="pageHead_1">Finalize Responsiveness of Consultants
                <!--<span style="float: right;"><a class="action-button-goback" href="Evalclarify.jsp?tenderId=<%=tendID%>&st=rp&comType=TEC">Go Back to Dashboard</a></span>-->
                <span style="float: right;"><a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=tendID%>">Go Back to Dashboard</a></span>
            </div>
            <%
                        pageContext.setAttribute("tenderId", tendID);
            %>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <table class="tableList_1 t_space" cellspacing="0" width="30%">
                <tr>
                    <td width="15%">Tender ID :</td>
                    <td width="15%"><%=tendID%></td>
                </tr>
                <tr>
                    <td>Name of Consultant :</td>
                    <td><%=commonService.getConsultantName(bidderID)%></td>
                </tr>                
            </table>
            <form action="<%=request.getContextPath()%>/ServletEvalCertiService?funName=insPassMarks" method="post">
                <input type="hidden" name="evalCount" value="<%=evalCount%>"/>
                <table <%=hideHTML%> class="tableList_1 t_space" cellspacing="0" width="100%" id="marksTab">
                    <%if (isREOI) {%>
                    <tr>
                        <td colspan="3"></td>
                        <th colspan="<%=memberName.size()%>">Rating</th>
                    </tr>
                    <%}%>
                    <tr>
                        <!--<th class="t-align-center">Form ID</th>-->
                        <!--<th class="t-align-center">Form Name</th>-->
                        <!--<th class="t-align-center">Criteria</th>-->
                        <!--<th class="t-align-center">Sub Criteria</th>-->
                        <!--<th <%//=hideHTML%> class="t-align-center">Points</th>-->
                        <%
                                    int memCnt = 0;
                                    for (Object[] members : memberName) {
                                        if (!members[0].toString().equals(session.getAttribute("userId").toString())) {
                                            marksCount[memCnt] = (int) criteriaService.countForMarksAvail(tendID, members[0].toString(), bidderID);
                                        } else {
                                            marksCount[memCnt] = 0;
                                        }
                                        //out.print("<th class='t-align-center'>" +members[1]+ "<br/><a href='"+request.getContextPath()+"/ServletEvalCertiService?funName=dropComMember&tId="+tendID+"&mId="+members[0]+"&bId="+bidderID+"&dropMem=yes' class='action-button-delete'>Drop Member</a></th>");
                                        //out.print("<th class='t-align-center'>" + members[1] + "</th>");
                                        memCnt++;
                                    }
                        %>
                        <!--<th <%//=hideHTML%> >Average Points<br/>of all Members</th>-->
                    </tr>
                    <%
                                //0 esd.maxMarks ,1 esd.actualMarks,2 esd.ratingWeightage,3 esd.ratedScore,
                                //4 tf.tenderFormId,5 tf.formName,6 esc.subCriteria,7 esd.bidId,8 esc.subCriteriaId
                                int cnt = 0;
                                double mainTotal = 0;
                                for (Object formId : formIds) {
                                    List<SPCommonSearchDataMore> list = criteriaService.getBidderMarksMemSP(bidderID, tendID, formId.toString(), "" + empId);
                                    for (SPCommonSearchDataMore data : list) {
                    %>
                    <tr>
                        <td style="display: none;"><%
                                                            if (!("Work plan".equals(data.getFieldName3())
                                                                    || "Organization and Staffing".equals(data.getFieldName3()))) {
                                                                //out.print("<input type='hidden' name='formId_" + cnt + "' value='" + data.getFieldName1() + "'> ");
                                                            }
                                                            //out.print(data.getFieldName1());
                            %></td>
                        <!--                    <td><!%=data[7]%></td>-->
                        <td><%
                                                            if (true || !("Work plan".equals(data.getFieldName3())
                                                                    || "Organization and Staffing".equals(data.getFieldName3()))) {
                                                                //out.print(data.getFieldName2());
                                                            } else {
                                                                //out.print("");
                                                            }
                            %></td>
                        <td><%
                                                            if (true || !("Work plan".equals(data.getFieldName3())
                                                                    || "Organization and Staffing".equals(data.getFieldName3()))) {
                                                                //out.print(data.getFieldName8());
                                                            } else {
                                                                //out.print("");
                                                            }
                            %></td>
                        <td><%=data.getFieldName3()%></td>
                        <td <%=hideHTML%>  opt="hide"><%//out.print(data.getFieldName5());
                                                        mainTotal += Double.parseDouble(data.getFieldName5());%></td>
                        <%
                                                            double avgMarks = 0;
                                                            int memCnt2 = 0;
                                                            for (Object[] members : memberName) {
                                                                StringBuilder htmlString = new StringBuilder();
                                                                //htmlString.append("<td class='t-align-center'>");
                                                                if (!members[0].toString().equals(session.getAttribute("userId").toString())) {
                                                                    if (marksCount[memCnt2] == 0) {
                                                                        String aLabel = null;
                                                                        List<SPCommonSearchDataMore> clariLinkList = criteriaService.getSeekClariLinkTextLinkSer(formId.toString(), data.getFieldName4(), session.getAttribute("userId").toString(), members[0].toString(), bidderID);
                                                                        if (clariLinkList != null && clariLinkList.isEmpty()) {
                                                                            aLabel = "Seek Clarification";
                                                                        } else {
                                                                            if ("".equals(clariLinkList.get(0).getFieldName1())) {
                                                                                aLabel = "Justification Asked";
                                                                            } else {
                                                                                aLabel = "Clarification Received";
                                                                            }
                                                                        }
                                                                        //htmlString.append("<a href='javascript:void(0)' onClick=window.open('../officer/EvalViewSeekClari.jsp?evalMemStatusId=" + data.getFieldName4() + "&userId=" + bidderID + "&tenderFormId=" + formId + "&memAnswerBy=" + members[0] + "')>" + aLabel + "</a>");
                                                                    }
                                                                }
                                                                if (!isREOI) {
                                                                    //htmlString.append("<table><tr><th class='t-align-center' width='10%'>Assigned</th><th class='t-align-center' width='10%'>Rating</th><th class='t-align-center' width='10%'>Scored</th></tr><tr>");
                                                                }
                                                                //List<Object[]> eachData = criteriaService.getDataForMembers(tendID,members[0].toString(),bidderID,formId.toString(), data.getFieldName4(),data.getFieldName3());
                                                                List<SPCommonSearchDataMore> eachData = criteriaService.getBidderMarksMemWithCrit(bidderID, tendID, formId.toString(), members[0].toString(), data.getFieldName4());
                                                                double assigned = 0;
                                                                double scored = 0;
                                                                if (!eachData.isEmpty()) {
                                                                    assigned = Double.parseDouble(eachData.get(0).getFieldName2());
                                                                    scored = Double.parseDouble(eachData.get(0).getFieldName1());

                                                                }
                                                                String rateValue = null;
                                                                /*if (scored == 0) {
                                                                rateValue = "Very Poor";
                                                                } else if (scored <= 0.4) {
                                                                rateValue = "Poor";
                                                                } else if (scored > 0.4 && scored <= 0.7) {
                                                                rateValue = "Good";
                                                                } else if (scored > 0.7 && scored <= 0.9) {
                                                                rateValue = "Very Good";
                                                                } else if (scored > 0.9 && scored <= 1) {
                                                                rateValue = "Excellent";
                                                                }*/
                                                                if (scored <= 0.4) {
                                                                    rateValue = "Poor";
                                                                } else if (scored > 0.4 && scored <= 0.5) {
                                                                    rateValue = "Average";
                                                                } else if (scored > 0.5 && scored <= 0.7) {
                                                                    rateValue = "Satisfactory";
                                                                } else if (scored > 0.7 && scored <= 0.8) {
                                                                    rateValue = "Most Satisfactory";
                                                                } else if (scored > 0.8 && scored <= 0.9) {
                                                                    rateValue = "Good";
                                                                } else if (scored > 0.9 && scored <= 0.95) {
                                                                    rateValue = "Very Good";
                                                                } else if (scored > 0.95 && scored <= 1) {
                                                                    rateValue = "Excellent";
                                                                }
                                                                if (isREOI && !eachData.isEmpty()) {
                                                                    if (eachData.size() > 1) {
                                                                        int bidCount = eachData.size();
                                                                        double rating = 0.0;
                                                                        for (SPCommonSearchDataMore thisData : eachData) {
                                                                            if (thisData.getFieldName3().equals("Poor")) {
                                                                                rating += 0.4;
                                                                            } else if (thisData.getFieldName3().equals("Average")) {
                                                                                rating += 0.5;
                                                                            } else if (thisData.getFieldName3().equals("Satisfactory")) {
                                                                                rating += 0.7;
                                                                            } else if (thisData.getFieldName3().equals("Most Satisfactory")) {
                                                                                rating += 0.8;
                                                                            } else if (thisData.getFieldName3().equals("Good")) {
                                                                                rating += 0.9;
                                                                            } else if (thisData.getFieldName3().equals("Very Good")) {
                                                                                rating += 0.95;
                                                                            } else if (thisData.getFieldName3().equals("Excellent")) {
                                                                                rating += 1;
                                                                            }
                                                                        }
                                                                        //System.out.println("--> "+rating);
                                                                        //System.out.println("--| "+rating/bidCount);
                                              /*if (rating/bidCount == 0) {
                                                                        rateValue = "Very Poor";
                                                                        } else if (rating/bidCount <= 0.4) {
                                                                        rateValue = "Poor";
                                                                        } else if (rating/bidCount > 0.4 && rating/bidCount <= 0.7) {
                                                                        rateValue = "Good";
                                                                        } else if (rating/bidCount > 0.7 && rating/bidCount <= 0.9) {
                                                                        rateValue = "Very Good";
                                                                        } else if (rating/bidCount > 0.9 && rating/bidCount <= 1) {
                                                                        rateValue = "Excellent";
                                                                        }*/
                                                                        if (rating / bidCount <= 0.4) {
                                                                            rateValue = "Poor";
                                                                        } else if (rating / bidCount > 0.4 && rating / bidCount <= 0.5) {
                                                                            rateValue = "Average";
                                                                        } else if (rating / bidCount > 0.5 && rating / bidCount <= 0.7) {
                                                                            rateValue = "Satisfactory";
                                                                        } else if (rating / bidCount > 0.7 && rating / bidCount <= 0.8) {
                                                                            rateValue = "Most Satisfactory";
                                                                        } else if (rating / bidCount > 0.8 && rating / bidCount <= 0.9) {
                                                                            rateValue = "Good";
                                                                        } else if (rating / bidCount > 0.9 && rating / bidCount <= 0.95) {
                                                                            rateValue = "Very Good";
                                                                        } else if (rating / bidCount > 0.95 && rating / bidCount <= 1) {
                                                                            rateValue = "Excellent";
                                                                        }
                                                                    } else if (eachData.size() == 1) {
                                                                        rateValue = eachData.get(0).getFieldName3();
                                                                    }
                                                                }
                                                                if (isREOI) {

                                                                    //htmlString.append("<br/>" + rateValue);
                                                                } else {
                                                                    //htmlString.append("<td>" + new BigDecimal(assigned).setScale(2, 0) + "</td><td>" + rateValue + "</td><td>" + new BigDecimal(scored).setScale(2, 0) + "</td>");
                                                                    //htmlString.append("</tr></table>");
                                                                }
                                                                //htmlString.append("</td>");
                                                                //out.print(htmlString.toString());
                                                                avgMarks += assigned;
                                                                memCnt2++;
                                                            }
                                                            //out.print("<td " + hideHTML + " >" + new BigDecimal((avgMarks / memberName.size())).setScale(2, 0) + "<input type='hidden' value='" + new BigDecimal((avgMarks / memberName.size())).setScale(2, 0) + "' name='avgMarks_" + cnt + "'/><input type='hidden' value='" + data.getFieldName4() + "' name='criteriaId_" + cnt + "'/></td>");
                        %>
                    </tr>
                    <%
                                    }
                                    if (!list.isEmpty()) {
                                        cnt++;
                                    }
                                }
                                //out.print("<input type='hidden' value='" + cnt + "' name='counter'>");
                                StringBuilder genHtml = new StringBuilder();
                                for (Object[] mems : memberName) {
                                    double marks = 0.0;
                                    List<SPCommonSearchDataMore> totalList = criteriaService.getTotal4ComMemReportSP(bidderID, tendID, mems[0].toString());
                                    for (SPCommonSearchDataMore dataMore : totalList) {
                                        marks += Double.parseDouble(dataMore.getFieldName1());
                                    }
                                    marksObtained += marks;
                                    genHtml.append("<th>" + new BigDecimal(String.valueOf(marks)).setScale(2, 0) + "</th>");
                                }
                                
                                marksObtained = memberName.size()==0 ? 0.00 : marksObtained / memberName.size();
                                if (!isREOI) {
                                    genHtml.append("<th>" + new BigDecimal(marksObtained).setScale(2, 0) + "</th>");
                                    //genHtml.append("<th></th>");
                                    //out.print("<tr><th></th><th></th><th>Total : </th><th>" + mainTotal + "</th>" + genHtml.toString() + "</tr>");
                                }
                                passingMarks = Double.parseDouble(criteriaService.getPassingMarksofTender(tendID).toString());
                                if (passingMarks <= marksObtained) {

                                    Status = criteriaService.getBidderExtensionStatus(tendID, bidderID).toString();
                                    if (Status.length() == 2) {
                                        result = "Pass";
                                    } else {
                                        result = "Fail";
                                    }
                                } else {
                                    result = "Fail";
                                }

                    %>
                </table>
                <input type="hidden" value="<%=session.getAttribute("userId")%>" name="mId"/>
                <input type="hidden" value="<%=bidderID%>" name="uId"/>
                <input type="hidden" value="<%=tendID%>" name="tId">
                <input type="hidden" value="<%=marksObtained%>" name="bmarks">
                <input type="hidden" value="<%=passingMarks%>" name="pmarks">
                <%if (!isREOI && !isRFP) {%>
                <input type="hidden" value="<%=result%>" name="result">
                <%}%>
                <input type="hidden" value="<%=stat%>" name="stat">

                <table class="tableList_1 t_space" cellspacing="0" width="50%">
                    <%
                                String fcomments = "";
                                String fresult = "";
                                if (bidderResult != null && !bidderResult.isEmpty()) {
                                    fresult = bidderResult.get(0)[0].toString();
                                    fcomments = bidderResult.get(0)[1].toString();
                                }
                    %>
                    <%if (isREOI) {%>
                    <tr>
                        <td>Evaluation Status </td>
                        <td>
                            <select name="result" class="formTxtBox_1" style="width: 200px;">
                                <option value="Pass" <%=fresult.equals("") ? "" : fresult.equals("Pass") ? "selected" : ""%>>Qualify</option>
                                <option value="Fail" <%=fresult.equals("") ? "" : fresult.equals("Fail") ? "selected" : ""%>>Disqualify</option>
                            </select>
                        </td>
                    </tr>
                    <%} else if (isRFP) {%>
                    <tr>
                        <td width="20%">Minimum Passing Points </td>
                        <td width="30%"><%=passingMarks%></td>
                    </tr>
                    <tr>
                        <td>Evaluation Status </td>
                        <td>
                            <select name="result" class="formTxtBox_1" style="width: 200px;">
                                <option value="Pass" <%=fresult.equals("") ? "" : fresult.equals("Pass") ? "selected" : ""%>>Pass</option>
                                <option value="Fail" <%=fresult.equals("") ? "" : fresult.equals("Fail") ? "selected" : ""%>>Fail</option>
                            </select>
                        </td>
                    </tr>
                    <%} else {%>
                    <tr>
                        <td width="20%">Minimum Passing Points </td>
                        <td width="30%"><%=passingMarks%></td>
                    </tr>
                    <tr>
                        <td>Result </td>
                        <td><%if (!"OK".equalsIgnoreCase(Status)) {%>
                            <div style="color: red;"><%=Status%></div>
                            <%}%><%=result%></td>
                    </tr>
                    <%}%>
                    <tr>
                        <td>Comments <span style="color: red">*</span></td>
                        <td><textarea name="comments" cols="10" rows="3" class="formTxtBox_1" id="comments" style="width:350px;" ><%=fcomments%></textarea></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="t-align-center">
                            <label class="formBtn_1">
                                <input type="submit" name="upload" id="submit" value="Submit" onclick="return validate();"/>
                            </label>
                        </td>
                    </tr>
                </table>
            </form>
            <%if (isService && !isREOI) {%>
            <script type="text/javascript">
                $('#marksTab td[opt="hide"]').each(function() {
                    var ctd = this.innerHTML;
                    if(ctd=='0'){
                        $(this).parent().hide();
                    }
                });
            </script>
            <%}%>
        </div>        
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>