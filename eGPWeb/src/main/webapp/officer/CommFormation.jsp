<%--
    Document   : CommFormation
    Created on : Nov 16, 2010, 11:06:27 AM
    Author     : TaherT
--%>
 
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommitteDtBean" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.OfficeMemberDtBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.PEOfficeCreationService" %>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum" %>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<%@page import="com.cptu.egp.eps.web.databean.TenderCommitteDtBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommitteMemDtBean"%>
<jsp:useBean id="tenderCommitteSrBean" class="com.cptu.egp.eps.web.servicebean.TenderCommitteSrBean"/>
<jsp:useBean id="tenderCommitteDtBean" class="com.cptu.egp.eps.web.databean.TenderCommitteDtBean"/>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />        
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

         
        <title>Evaluation Committee Details</title>
        <script type="text/javascript">
            $(function() {
                $('#btnSearch').click(function() {
                    $(".err").remove();
                    if($('#txtDeptName').val()==""){
                        $('#txtDeptName').parent().append("<div class='err' style='color:red;'>Please select Organisation</div>");
                    }else{
                    if($('#cmboffice').val()!=null){
                        $.post("<%=request.getContextPath()%>/CommMemServlet", {officeId:$('#cmboffice').val(),pe1count:$('#pe1count').val(),tenderId:'<%=request.getParameter("tenderid")%>',funName:'memberSearch'}, function(j){
                            var i = j.toString();
                            $("#tbodype").html(j.substring(0, i.indexOf("#",0)));
                        });
                    }else{
                        $("#tbodype").html(null);
                    }
                    }
                });
            });            
            $(function() {
                $('#frmCommPub').submit(function() {
                    if($.trim($('#txtaRemarks').val())==""){
                            $('#divRem').html("Please enter Remarks");
                            return false;
                        }else{
                            $('#divRem').html(null);
                            document.getElementById("btnPublish").style.display = 'none';
                        }
                });
            });
            $(function() {
                $('#frmComm').submit(function() {                                        
                    if($.trim($('#txtcommitteeName').val())==""){
                        $('#errmsg').html("Please enter Committe Name");
                        $('#errmsg').css("color", "red");
                        return false;
                    }else{                        
                        if(($('#txtcommitteeName').val().length)>100){
                            $('#errmsg').html("Maximum 100 Characters are allowed.");
                            $('#errmsg').css("color", "red");
                            return false;
                        }else{
                            $('#errmsg').html(null);                            
                        }
                    }
                    var minout = $('#minoutmem').val();
                    var minin = $('#minmem').val();
                    var maxin = $('#maxmem').val();
                    var mininmem = $('#mininmem').val();
                    var len=$('#members').children()[1].children.length;
                    if(len<minin){
                        jAlert("Minimum "+minin+" members required.","Min member alert", function(RetVal) {
                        });
                        return false;
                    }
                    if(len>maxin){
                        jAlert("Committee can have Maximum "+maxin+" Members only.","Max member alert", function(RetVal) {
                        });
                        return false;
                    }
                    var minouttmp=0;
                    var minintmp=0;
                    var cp=0;
                    var ms=0;                    
                    for(var i=0;i<len;i++){
                        if($('#memfrm'+i).val()=="Other PA"){
                            minouttmp++;
                        }
                        if($('#memfrm'+i).val()=="Same PA"){
                            minintmp++;
                        }
                        if($('#cmbMemRole'+i).val()=="cp"){
                            cp++;
                        }
                        if($('#cmbMemRole'+i).val()=="ms"){
                            ms++;
                        }
                    }
                    if(minouttmp<minout){
                        jAlert("Minimum "+minout+" members required from other PA.","Min member alert", function(RetVal) {
                        });
                        return false;
                    }
                    if(minintmp<mininmem){
                        jAlert("Minimum "+mininmem+" members required from same PA.","Min member alert", function(RetVal) {
                        });
                        return false;
                    }                    
                    if(cp>1){
                        jAlert("There must be 1 Chairperson.","Chairperson alert", function(RetVal) {
                        });
                        return false;
                    }
                    if(cp==0){
                        jAlert("There must be 1 Chairperson.","Chairperson alert", function(RetVal) {
                        });
                        return false;
                    }
                    if(ms>1){
                        jAlert("There should be only 1 or 0 Member Secretary.","Member Secretary alert", function(RetVal) {
                        });
                        return false;
                    }                    
                    if($('#frmComm').valid()){
                        $('#btnSubmit').attr("disabled","disabled");
                        $('#hdnSubmit').val("Submit");
                    }
                    <%--return false; --%><%--remove this--%>
                });
            });
            $(function() {
                $('#spe').click(function() {
                    $("#pe1").css("display","block");
                    $("#pe2").css("display","none");
                    $("#spe").addClass('sMenu');
                    $("#ope").removeClass('sMenu');
                });
                $('#ope').click(function() {
                    $("#pe2").css("display","block");
                    $("#pe1").css("display","none");
                    $("#ope").addClass('sMenu');
                    $("#spe").removeClass('sMenu');
                });
            });
            function delRow(row){
                var curRow = $(row).parents('tr');
                var len=$('#members').children()[1].children.length;                
                var id=$(row).attr("id").substring(6,$(row).attr("id").length);                
                for(var i=id;i<len;i++){                                        
                    $('#memid'+eval(eval(i)+eval(1))).attr("id","memid"+(i));
                    $('#memgovid'+eval(eval(i)+eval(1))).attr("id","memgovid"+(i));
                    $('#cmbMemRole'+eval(eval(i)+eval(1))).attr("id","cmbMemRole"+(i));
                    $('#memfrm'+eval(eval(i)+eval(1))).attr("id","memfrm"+(i));
                    $('#delRow'+eval(eval(i)+eval(1))).attr("id","delRow"+(i));
                }
                curRow.remove();
            }

            //$(function() {
                //$('#cmborg').change(function() {
                    //$.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:$('#cmborg').val(),funName:'Office'}, function(j){
                        //$("select#cmboffice").html(j);
                    //});
                //});
            //});
            function getOfficesForDept(){
                $.post("<%=request.getContextPath()%>/CommMemServlet", {deptId:$('#cmborg').val(),tendId:'<%=request.getParameter("tenderid")%>',funName:'Office'}, function(j){
                    $("select#cmboffice").html(j);
                });
            }
            $(function() {
                // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
                $( "#dialog:ui-dialog" ).dialog( "destroy" );
                $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    resizable:false,
                    draggable:true,
                    height: 500,
                    width: 600,
                    modal: true,
                    buttons: {
                        "Add": function() {
                            var memcnt=0;
                            $(function() {                                
                                //if(eval(($(":checkbox[checked='true']").length)+($('#members').children()[1].children.length))<=$('#maxmem').val()){
                                                                var len=$('#members').children()[1].children.length;                                                                
                                                                var i =len;                                                               
                                                                $(":checkbox[checked='true']").each(function(){
                                                                    memcnt++;
                                                                    var a = $(this).attr("id");
                                                                    var b = a.substring(3, a.length);
                                                                    var addUser=true;
                                                                    for(var j=0;j<len;j++){                                                                        
                                                                        if($('#memid'+j).val()+"_"+$('#memgovid'+j).val()==$("#chk"+b).val()){
                                                                            addUser=false;                                                                            
                                                                            break; 
                                                                        }
                                                                    }
                                                                    if(addUser){
                                                                        var temp = $("#chk"+b).val();
                                                                        var uId = temp.split("_", temp.length)[0];
                                                                        var gId = temp.split("_", temp.length)[1];
                                                                        $( "#members tbody" ).append("<tr>"+
                                                                            "<td class='t-align-left'><input id='memgovid"+i+"' type='hidden' name='memGovIds' value='"+gId+"'/><input id='memid"+i+"' type='hidden' name='memUserIds' value='"+uId+"'/>"+$("#spn"+b).val()+"</td>"+
                                                                            "<td class='t-align-left'>"+
                                                                            "<select name='memberRoles' class='formTxtBox_1' id='cmbMemRole"+i+"' style='width:200px;'>"+
                                                                            "<option selected='selected' value='cp'>Chairperson</option>"+
                                                                            "<option value='ms'>Member Secretary</option>"+
                                                                            "<option value='m'>Member</option>"+
                                                                            "</select>"+
                                                                            "</td>"+
                                                                            "<td class='t-align-center'><input id='memfrm"+i+"' type='hidden' name='memberFroms' value='"+$("#pestat"+b).val()+"'/>"+$("#pestat"+b).val()+"</td>"+
                                                                            "<td class='t-align-center'><a id='delRow"+i+"' class='action-button-delete' onclick='delRow(this)'>Remove</a></td>"+
                                                                            "</tr>");
                                                                        i++;                                                                        
                                                                    }else{
                                                                        jAlert("Member already added in the committee.","Same member alert", function(RetVal) {
                                                                        });
                                                                    }
                                                                    });
                                                            <%--}else{
                                                                jAlert("Maximum "+$('#maxmem').val()+" members.","Max member alert", function(RetVal) {
                                                                });
                                                            }--%>
                                                            
                                                        });
                                                        if(memcnt==0){
                                                                jAlert("Please select the member.","Evaluation member alert", function(RetVal) {
                                                                });
                                                        }else{
                                                        $( this ).dialog( "close" );
                                                        }
                                                    },
                                                    Cancel: function() {
                                                        $( this ).dialog( "close" );
                                                    }
                                                },
                                                close: function() {
                                                }
                                            });

                                            $("#addmem" ).click(function(){
                                                $("#dialog-form").dialog("open");
                                                $("input[type='checkbox']").removeAttr("checked");
                                            });
                                        });
        </script>
    </head>
    <body>
        <%

            boolean isEdit = false;
            int tenderId = 0;
            int j=0;
            if (request.getParameter("tenderid") != null) {
                tenderId = Integer.parseInt(request.getParameter("tenderid"));
            }
            
            String ReplacePE = "";
            
            if("y".equalsIgnoreCase(request.getParameter("isview"))) 
            {
                // Coad added by Dipal for Audit Trail Log.
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                String idType="tenderId";
                int auditId=tenderId;
                String auditAction="View Evaluation Committee";
                String moduleName=EgpModule.Evaluation.getName();
                String remarks="";
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                
            }
            
            
            tenderCommitteSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
            if ("Submit".equals(request.getParameter("hdnSubmit"))) {
                tenderCommitteDtBean.setCommitteeName(request.getParameter("committeeName"));
                tenderCommitteDtBean.setCommitteeType(request.getParameter("committeeType"));
                tenderCommitteDtBean.setMaxMembers(request.getParameter("maxMembers"));
                tenderCommitteDtBean.setMemUserIds(request.getParameterValues("memUserIds"));
                tenderCommitteDtBean.setMemGovIds(request.getParameterValues("memGovIds"));
                tenderCommitteDtBean.setMemberFroms(request.getParameterValues("memberFroms"));
                tenderCommitteDtBean.setMemberRoles(request.getParameterValues("memberRoles"));
                tenderCommitteDtBean.setMinMemFromPe(request.getParameter("minMemFromPe"));
                //tenderCommitteDtBean.setMinMemFromTec(request.getParameter(""));
                tenderCommitteDtBean.setMinMemOutSide(request.getParameter("minMemOutSide"));
                tenderCommitteDtBean.setMinMembers(request.getParameter("minMembers"));
                tenderCommitteDtBean.setTenderid(request.getParameter("tenderid"));
                tenderCommitteDtBean.setUserid(request.getSession().getAttribute("userId").toString());
                tenderCommitteDtBean.setCommitteeId(request.getParameter("committeeId"));
                if("true".equals(request.getParameter("isedit"))){
                    tenderCommitteSrBean.createUpdateCommitte(tenderCommitteDtBean, true);
                }else{
                    tenderCommitteSrBean.createUpdateCommitte(tenderCommitteDtBean, false);
                }

                tenderId = Integer.parseInt(request.getParameter("tenderid"));
                response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&isedit=y");
            }else if ("Notify".equals(request.getParameter("publish"))) {
                tenderCommitteSrBean.publishComm(request.getParameter("committeeId"),request.getParameter("remarks"),""+tenderId,"eval");
                response.sendRedirect("EvalComm.jsp?tenderid=" + tenderId + "&ispub=y");
            }else{                                            
                int i = 0;
                 Map<String,String> userTransMap = new HashMap<String, String>();
                java.util.List<OfficeMemberDtBean> officeMemberDtBeans = tenderCommitteSrBean.findOfficeMember(Integer.parseInt(request.getParameter("tenderid")), "SamePEEval");
                CommitteDtBean bean = tenderCommitteSrBean.findCommitteDetails(Integer.parseInt(request.getParameter("tenderid")), "evalcom");
                if ("y".equals(request.getParameter("isedit")) || "y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub"))) {
                    isEdit = true;
                 }
                 java.util.List<CommitteMemDtBean> comMemberDtBeans = new java.util.ArrayList<CommitteMemDtBean>();
                 if (isEdit) {
                    comMemberDtBeans = tenderCommitteSrBean.committeUser(Integer.parseInt(request.getParameter("tenderid")), "TECMember");
                 }
                 if(!("y".equalsIgnoreCase(request.getParameter("ispub"))|| "y".equals(request.getParameter("isview")))){
                %>
                <div id="dialog-form" title="Add Committee Member">
            <%--<form>--%>
            <fieldset>
                <table width="100%" cellspacing="0">
                    <tr>
                        <td colspan="4">
                            <ul class="tabPanel_1">                                                                
                                <li><a href="javascript:void(0);" class="sMenu" id="spe">Same PA</a></li>
                                <li><a href="javascript:void(0);" id="ope">Other PA</a></li>                                
                            </ul>
                            <div class="tabPanelArea_1">

                                <%--Same PE--%>

                                <div id="pe1" style="display : block;">
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th width="10%" class="t-align-center">Select</th>
                                            <th class="t-align-center" width="30%">Member Name</th>
                                            <th class="t-align-center" width="30%">Member Designation</th>
                                            <th class="t-align-center" width="30%">Procurement Role</th>
                                        </tr>
                                        <%
                                            for (OfficeMemberDtBean omdb : officeMemberDtBeans) {
                                                //if(omdb.getProcureRole().contains("TEC/PEC")){ 
                                        %>
                                        <tr>
                                            <td class="t-align-center">
                                                <label><input id="chk<%=i%>" type="checkbox" value="<%=omdb.getUserid()%>_<%=omdb.getGovUserId()%>" /></label></td>
                                            <td class="t-align-center"><input type="hidden" id="spn<%=i%>" value="<%=omdb.getEmpName()%>"/><%=omdb.getEmpName()%></td>
                                            <td class="t-align-center"><input type="hidden" id="pestat<%=i%>" value="Same PA"/><%=omdb.getDesgName()%></td>
                                            <%
                                                String ReplaceHOPE = omdb.getProcureRole();
                                                if(ReplaceHOPE.contains("HOPE"))
                                                {
                                                    ReplaceHOPE = ReplaceHOPE.replaceAll("HOPE", "HOPA");
                                                    
                                                }
                                                /** change by Nitish  
                                                when it reads from PA,TOC/POC, TEC/PEC, TC from database
                                                then replace PA with PE and PEC and PEC will be vanished **/
                                                
                                                if(ReplaceHOPE.contains("/POC"))
                                                {
                                                    ReplaceHOPE = ReplaceHOPE.replaceAll("/POC", "");
                                                }
                                                if(ReplaceHOPE.contains("/PEC"))
                                                {
                                                    ReplaceHOPE = ReplaceHOPE.replaceAll("/PEC", "");
                                                }
                                                /** change by nitish **/
                                                if(ReplaceHOPE.contains("PE"))
                                                {
                                                    int LocationOfPE = ReplaceHOPE.indexOf("PE");
                                                    char[] ReplaceHOPEChars = ReplaceHOPE.toCharArray();
                                                    if(LocationOfPE==0)
                                                    {

                                                        ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                                        ReplaceHOPE = String.valueOf(ReplaceHOPEChars);

                                                    }
                                                    else
                                                    {
                                                        if(ReplaceHOPE.charAt(LocationOfPE-1)==',')
                                                        {
                                                            ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                                            ReplaceHOPE = String.valueOf(ReplaceHOPEChars);
                                                        }

                                                    }
                                                }
                        
                                            %>
                                            <td class="t-align-center"><%=ReplaceHOPE%></td>
                                        </tr>
                                        <%i++;
											}//} //Modified by dohatec to resolve online tracking id : 6869 %>
                                    </table>
                                </div>

                                <%--Other PE Search--%>

                                <div class="formBg_1"  id="pe2" style="display : none;">
                                    <input type="hidden" id="pe1count" value="<%=officeMemberDtBeans.size()%>">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">
                                        <tr>
                                            <td width="26%" class="ff t-align-left">Organisation Name :<span class="mandatory">*</span></td>
                                            <td width="74%" class="ff t-align-left">
                                                <input type="text" id="txtDeptName" style="width: 200px;" class="formTxtBox_1" readonly/>
                                                <input type="hidden" id="cmborg">
                                                <a id="imgTree" href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=createCommitee', '', 'width=350px,height=400px,scrollbars=1','');">
                                                    <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                </a>                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="26%" class="ff t-align-left">PA Office :</td>
                                            <td width="74%" class="ff t-align-left">
                                                <select id="cmboffice" style="width: 200px;" class="formTxtBox_1">
                                                </select>                                                 
                                            </td>                                       
                                        </tr>
                                        <tr>                                            
                                            <td class="t-align-center" colspan="2">
                                                <label class="formBtn_1">
                                                    <input type="button" id="btnSearch" value="Search"/>
                                                </label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th width="10%" class="t-align-center">Select</th>
                                            <th class="t-align-center" width="30%">Member Name</th>
                                            <th class="t-align-center" width="30%">Member Designation</th>
                                            <th class="t-align-center" width="30%">Procurement Role</th>
                                        </tr>
                                        <tbody id="tbodype">

                                        </tbody>
                                    </table>
                                </div>                                
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <%--</form>--%>
        </div>
        <%}%>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="pageHead_1">Evaluation Committee Details<span style="float: right;"><a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=request.getParameter("tenderid")%>">Go Back to Tender Dashboard</a></span></div>
        <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
         %>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <%--<%@include file="officerTabPanel.jsp"%>--%>
        <div id="users-contain" class="ui-widget">            
                <div class="tabPanelArea_1">
                    <form action="CommFormation.jsp" method="post" id="frmComm">
                    <input type="hidden" value="<%=tenderId%>" name="tenderid"/>
                    <input type="hidden" value="<%=isEdit%>" name="isedit"/>                    
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <%--<tr>
                            <td width="26%" colspan="2" class="t-align-left ff">Evaluation Committee</td>
                        </tr>--%>
                        <tr>
                            <td width="27%" class="t-align-left ff">Committee Name : <%if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){%><span class="mandatory">*</span><%}%></td>
                            <td width="73%" class="t-align-left">
                                <label><%if("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub"))){out.print(comMemberDtBeans.get(0).getCommName());}else{%><input name="committeeName" type="text" class="formTxtBox_1" id="txtcommitteeName" style="width:400px;" value="<%if(isEdit){%><%=comMemberDtBeans.get(0).getCommName()%><%}%>"/><%}%></label>
                                <input name="committeeId" type="hidden" class="formTxtBox_1" value="<%if(isEdit){%><%=comMemberDtBeans.get(0).getCommId()%><%}else{%>0<%}%>"/>
                                <div class='reqF_1' id="errmsg"></div>
                            </td>
                        </tr>
                        <%if(!"y".equals(request.getParameter("isview"))){%>
                        <tr>
                            <td class="t-align-left ff">Minimum Members Required :</td>
                            <td class="t-align-left"><input type="hidden" name="minMembers" id="minmem" value="<%=bean.getMinMemReq()%>"/><%=bean.getMinMemReq()%></td>
                        </tr>
<!--                        <tr>
                            <td class="t-align-left ff">Maximum Members Required :</td>
                            <td class="t-align-left"><input type="hidden" name="maxMembers" id="maxmem" value="<%=bean.getMaxMemReq()%>"/><%=bean.getMaxMemReq()%></td>
                        </tr>-->
                        <tr>
                            <td class="t-align-left ff">Minimum Members from the Same PA :</td>
                            <td class="t-align-left"><input name="minMemFromPe" type="hidden" id="mininmem" value="<%=bean.getMinMemFromPe()%>"/><%=bean.getMinMemFromPe()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Minimum Members Outside PA :</td>
                            <td class="t-align-left" id="outmem"><input name="minMemOutSide" type="hidden" id="minoutmem" value="<%=bean.getMinMemOutSidePe()%>"/><%=bean.getMinMemOutSidePe()%></td>
                        </tr>
                        <%}%>
                    </table>
                    <!--<input name="committeeType" type="hidden" id="comType" value="<%=bean.getpNature()%>"/>-->
                    <input name="committeeType" type="hidden" id="comType" value="TEC"/> <!---- changed temporarily by aprojit 20th oct 2016 -->
                    <div>&nbsp;</div>
                    <%if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){%><div class="t-align-center"><a id="addmem" class="action-button-add">Add Members</a></div><%}%>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="members">
                        <thead>
                            <tr>
                                <th width="40%" class="t-align-left ff">Committee Member's Name</th>
                                <th width="30%" class="t-align-left ff">Committee Role <%/*if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){*/%><%--<span class="mandatory">*</span><%}%>--%></th>
                                <th width="17%" class="t-align-center ff">Members From</th>
                                 <%if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){%><th width="13%" class="t-align-center ff">Action</th><%}%>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <%if(isEdit){
                                for(CommitteMemDtBean cmdb : comMemberDtBeans){
                                    userTransMap.put(String.valueOf(cmdb.getUserId()), "3");
                            %>
                            <tr>
                                <td class="t-align-left">
                                    <input id="memgovid<%=j%>" type="hidden" name="memGovIds" value="<%=cmdb.getGovUserId()%>"/>
                                    <input id="memid<%=j%>" type="hidden" name="memUserIds" value="<%=cmdb.getUserId()%>"/><%=cmdb.getEmpName()%></td>
                                <td class="t-align-left">
                                     <%if("y".equals(request.getParameter("isview"))||"y".equals(request.getParameter("ispub"))){
                                        if (cmdb.getMemRole().equals("cp")) {
                                            out.print("Chairperson");
                                        }else if (cmdb.getMemRole().equals("ms")) {
                                            out.print("Member Secretary");
                                        }else if (cmdb.getMemRole().equals("m")) {
                                            out.print("Member");
                                        }
                                    }else{%>
                                    <select name="memberRoles" class="formTxtBox_1" id="cmbMemRole<%=j%>" style="width:200px;">
                                        <option <%if(cmdb.getMemRole().equals("cp")){%>selected<%}%> value="cp">Chairperson</option>
                                        <option <%if(cmdb.getMemRole().equals("ms")){%>selected<%}%> value="ms">Member Secretary</option>
                                        <option <%if(cmdb.getMemRole().equals("m")){%>selected<%}%> value="m">Member</option>
                                        </select>
                                    </td>
                                    <%}%>
                                    <%
                                        ReplacePE = cmdb.getMemFrom();
                                        ReplacePE = ReplacePE.replaceAll("PE","PA");
                                    %>
                                <td class="t-align-center"><input id="memfrm<%=j%>" type="hidden" name="memberFroms" value="<%=cmdb.getMemFrom()%>"/><%=ReplacePE%></td>
                                <%if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){%><td class="t-align-center"><a id="delRow<%=j%>" class="action-button-delete" onclick="delRow(this)">Remove</a></td><%}%>
                                </tr>
                                <%j++;}}%>
                        </tbody>
                    </table>
                    <div>&nbsp;</div>
                    <%if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){%>
                    <div class="t-align-center">
                        <label class="formBtn_1">
                            <input name="submit" type="submit" value="Submit" id="btnSubmit"/>
                            <input type="hidden" name="hdnSubmit" id="hdnSubmit" value=""/>
                        </label>
                    </div>                    
                    <%}%>
                    </form>
                    <%if("y".equals(request.getParameter("ispub"))){%>
                    <form action="CommFormation.jsp" method="post" id="frmCommPub">
                    <input type="hidden" value="<%=tenderId%>" name="tenderid"/>
                    <input type="hidden" value="<%=isEdit%>" name="isedit"/>
                    <input name="committeeId" type="hidden" class="formTxtBox_1" value="<%if(isEdit){%><%=comMemberDtBeans.get(0).getCommId()%><%}else{%>0<%}%>"/>
                    <table>
                        <tr>
                            <td>Remarks <span class="mandatory">*</span>: </td>
                            <td>
                                <textarea name="remarks" rows="3" class="formTxtBox_1" id="txtaRemarks" style="width:400px;"></textarea>
                                <div id="divRem" style="color: red;"></div>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <div class="t-align-center">
                        <label class="formBtn_1">
                            <input name="publish"  type="submit" value="Notify" id="btnPublish" onclick="return validate();"/>
                        </label>
                    </div>
                    </form>
                    <%}%>

                    <%
                        if(isEdit){
                        List<CommitteDtBean> hisBean = tenderCommitteSrBean.findCommitteDetailsHistory(Integer.parseInt(request.getParameter("tenderid")), "evalcomHistory");                        
                        int kComm=1;                        
                        for(CommitteDtBean dtBean : hisBean){
                        comMemberDtBeans.clear();
                        comMemberDtBeans = tenderCommitteSrBean.committeUserHistory(Integer.parseInt(request.getParameter("tenderid")), "TECMemberHistory",dtBean.getCommitteeId());
                    %>                           
                  <br/><div style="text-align: center; background-color: #e5dfec; font-weight: bold;height: 18px; padding-top: 5px">Committee History <%=kComm %></div>
                  <table width="100%" cellspacing="0" class="tableList_1 t_space">                        
                        <tr>
                            <td width="27%" class="t-align-left ff">Committee Name : </td>
                            <td width="73%" class="t-align-left">
                                <label><%=comMemberDtBeans.get(0).getCommName()%></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Minimum Members Required :</td>
                            <td class="t-align-left"><input type="hidden" name="minMembers" id="minmem" value="<%=dtBean.getMinMemReq()%>"/><%=dtBean.getMinMemReq()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Maximum Members Required :</td>
                            <td class="t-align-left"><input type="hidden" name="maxMembers" id="maxmem" value="<%=dtBean.getMaxMemReq()%>"/><%=dtBean.getMaxMemReq()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Minimum Members from same PA :</td>
                            <td class="t-align-left"><input name="minMemFromPe" type="hidden" id="mininmem" value="<%=dtBean.getMinMemFromPe()%>"/><%=dtBean.getMinMemFromPe()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Minimum Members Outside PA :</td>
                            <td class="t-align-left" id="outmem"><input name="minMemOutSide" type="hidden" id="minoutmem" value="<%=dtBean.getMinMemOutSidePe()%>"/><%=dtBean.getMinMemOutSidePe()%></td>
                        </tr>
                    </table>
                  <div>&nbsp;</div>
                  <table width="100%" cellspacing="0" class="tableList_1 t_space" id="members">
                        <thead>
                            <tr>
                                <th width="17%" class="t-align-left ff">Committee Member's Name</th>
                                <th width="53%" class="t-align-left ff">Committee Role </th>
                                <th width="17%" class="t-align-left ff">Members From</th>                                 
                            </tr>
                        </thead>
                        <tbody>

                            <%
                                for(CommitteMemDtBean cmdb : comMemberDtBeans){
                            %>
                            <tr>
                                <td class="t-align-left">
                                    <input id="memgovid<%=j%>" type="hidden" name="memGovIds" value="<%=cmdb.getGovUserId()%>"/>
                                    <input id="memid<%=j%>" type="hidden" name="memUserIds" value="<%=cmdb.getUserId()%>"/><%=cmdb.getEmpName()%></td>
                                <td class="t-align-left">
                                     <%
                                        if (cmdb.getMemRole().equals("cp")) {
                                            out.print("Chairperson");
                                        }else if (cmdb.getMemRole().equals("ms")) {
                                            out.print("Member Secretary");
                                        }else if (cmdb.getMemRole().equals("m")) {
                                            out.print("Member");
                                        }
                                    %>                                    
                               </td>  
                               <%
                                    ReplacePE = cmdb.getMemFrom();
                                    ReplacePE = ReplacePE.replaceAll("PE","PA");
                                %>
                               <td class="t-align-left"><input id="memfrm<%=j%>" type="hidden" name="memberFroms" value="<%=cmdb.getMemFrom()%>"/><%=ReplacePE%></td>                                
                           </tr>
                                <%j++;}%>
                        </tbody>
                    </table>
        </div>
        <%kComm++;}}
        if("y".equalsIgnoreCase(request.getParameter("isview"))) 
            {
                request.setAttribute("comMemberDtBeans", comMemberDtBeans);
                request.setAttribute("listEmpTras", userTransMap);
    %>
        
            <jsp:include page="../resources/common/EmpTransferHist.jsp" />
            <jsp:include page="../resources/common/OpeningComCommentsInclude.jsp">
          
            <jsp:param name="tenderId" value="<%=tenderId %>" />
            <jsp:param name="comType" value="2" />
        </jsp:include>
            <%}%>
           </div>        
        <br/>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%
        }
        %>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>