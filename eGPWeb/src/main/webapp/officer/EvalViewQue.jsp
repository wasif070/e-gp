<%--
    Document   : EvalViewQue
    Created on : Dec 31, 2010, 11:58:02 AM
    Author     : Administrator
--%>

<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Evaluation Clarification</title>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
    <%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
    <%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
    <%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
                String tenderId = "0";
                String formId = "0";
                String userId = "0";
                String st = "";
                String referer = "";
                String qType = null;
                 st = request.getParameter("st");
                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");
                }
                 if (request.getParameter("qtype") != null) {
                    qType = request.getParameter("qtype");
                }
                if (request.getParameter("uid") != null && !"".equalsIgnoreCase(request.getParameter("uid"))) {
                            userId = request.getParameter("uid");
                        }

                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                }

                String strComType = "null";
                if (request.getParameter("comType") != null && !"null".equalsIgnoreCase(request.getParameter("comType"))) 
                {
                    strComType = request.getParameter("comType");
                }
                   
                // Coad added by Dipal for Audit Trail Log.
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                String idType="tenderId";
                int auditId=Integer.parseInt(request.getParameter("tenderId"));
                String auditAction="View Clarification";
                String moduleName=EgpModule.Evaluation.getName();
                String remarks="";
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);


    %>
</head>
<body>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->

        <div class="topHeader">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
        </div>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
        <div class="pageHead_1">Evaluation Query and Clarification
            <span style="float:right;">
                <%
                    if (request.getParameter("lnk")!=null && "view".equalsIgnoreCase(request.getParameter("lnk"))) {
                %>
                    <a href="ViewClarify.jsp?tenderId=<%=tenderId%>&st=<%=st%>&comType=<%=strComType%>" class="action-button-goback">Go back to Dashboard</a>
                <%} else {%>
                <a href="Evalclarify.jsp?tenderId=<%=tenderId%>&st=<%=st%>&comType=<%=strComType%>" class="action-button-goback">Go back to Dashboard</a>
                <%}%>

            </span>
        </div>
        <%pageContext.setAttribute("tenderId", tenderId);%>

        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <div>&nbsp;</div>
         <%for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid",userId,"0")){%>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                    <th colspan="2" class="t_align_left ff">Company Details
                                    </th>
                             </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Company Name :</td>
                                <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                            </tr>
                        </table>
                    <%} // END FOR LOOP OF COMPANY NAME
                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            int formCnt = 0;
                            //for (SPCommonSearchData formNames : commonSearchService.searchData("getPostedQueFormsForBidder", tenderId, userId,
                            for (SPCommonSearchData formNames : commonSearchService.searchData("getPostedQueFormsForBidderWithoutBids", tenderId, userId,
                                    null, null, null, null, null, null, null)) {
                                formCnt++;
                                formId = formNames.getFieldName1();
                            for (SPCommonSearchData bidFormNames : commonSearchService.searchData("getPostedQueFormsForBidderWithBids", tenderId, userId,
                                    formId, null, null, null, null, null, null)) {                        
%>
                        <table width="100%" cellspacing="0" class="tableList_k t_space">
                            <tr>
                                <th colspan="3" class="ff" valign="middle"> Form Name : <a href="ViewEvalBidform.jsp?tenderId=<%=tenderId%>&uId=<%=userId%>&formId=<%=bidFormNames.getFieldName1()%>&lotId=0&bidId=<%=bidFormNames.getFieldName3()%>&action=Edit&isSeek=true" target="_blank"><%=formNames.getFieldName2()%></a>
                                <span class="c-alignment-right"><a href="EvalDocList.jsp?tenderId=<%=tenderId%>&formId=<%=formId%>&uId=<%=userId%>&st=<%=st%>" class="action-button-download">Download Documents</a></span>
                                </th>
                            </tr>
                        </table>
                                <%}%>

                        <%
                                int memCnt = 0;
                               for (SPCommonSearchData sptcd : commonSearchService.searchData("GetEvalComMem", tenderId, formId, null, null, null, null, null, null, null)) {
                                   memCnt++;
                        %>
                       <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="3" class="t-align-left"><b>TEC / TSC Member Name :</b> <%=sptcd.getFieldName3()%>
                                </td>
                            </tr>
                            <tr>
                                <th width="6%" class="t-align-center">Sl. No.</th>
                                <th width="47%" class="t-align-center">Query</th>
                                <th width="47%" class="t-align-center">Clarification</th>
                            </tr>
                            <%
                                int cntQuest = 0;
                                for (SPCommonSearchData question : commonSearchService.searchData("GetEvalComMemQuestionByMem",
                                                                                                   tenderId, formId,
                                                                                                   sptcd.getFieldName2(), userId, "officer", qType, null, null, null)) {
                                    cntQuest++;
                            %>
                            <tr>
                                <td class="t-align-left"><%=cntQuest%></td>
                                <td class="t-align-left"><%=URLDecoder.decode(question.getFieldName3(),"UTF-8")%></td>
                                <%if(question.getFieldName4().equals("")){%>
                                <td class="t-align-left">-</td>
                                <%}else{%>
                                <td class="t-align-left"><%=question.getFieldName4() %></td>
                                <%}%>
                            </tr>
                            <%
                                if(question!=null){
                                        question = null;
                                    }
                                }/*Quetion Loop Ends Here*/if(cntQuest==0){%>
                                    <tr>
                                    <td class="t-align-left" colspan="3">No Record Found</td>
                                    </tr>
                                  <%  }
                            %>
                        </table>



                        <%
                                if(sptcd!=null){
                                        sptcd = null;
                                    }
                               }//for table Member Name
                               if(memCnt==0){%>
                                   <tr>
                                    <td class="t-align-center">No Record Found</td>
                            </tr>

                                   <%}

                            if(formNames!=null){
                                    formNames = null;
                               }%>

                               <!-- START: TSC Comments-->
                                    <table id="tblTSCComments" width="100%" cellspacing="0" class="tableList_1">
                                        <tr>
                                            <th width="5%" class="t-align-center">Sl. No.</th>
                                            <th width="20%" class="t-align-center">Posted By</th>
                                            <th class="t-align-center">TSC Comments</th>
                                        </tr>
                                        <%
                                                TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                int cntCom = 0;
                                                for (SPTenderCommonData objSPTCD : objTSC.returndata("getTSCComments", formId, userId)) {
                                                    cntCom++;
                                        %>
                                        <tr
                                            <%if (Math.IEEEremainder(cntCom, 2) == 0) {%>
                                            class="bgColor-Green"
                                            <%} else {%>
                                            class="bgColor-white"
                                            <%   }%>
                                            >
                                            <td class="t-align-center"><%=cntCom%></td>
                                            <td><%=objSPTCD.getFieldName3()%></td>
                                            <td><%=objSPTCD.getFieldName4()%></td>
                                        </tr>
                                        <%}%>
                                        <%if (cntCom == 0) {%>
                                        <tr>
                                            <td colspan="3">
                                                No Comments found.
                                            </td>
                                        </tr>
                                        <%}%>
                                    </table>
                                    <!-- END: TSC Comments-->

                            <%}//Form Name ends
                            if(formCnt==0){%>
                            <tr>
                                    <td class="t-align-center">No Record Found</td>
                            </tr>

                                <%}
                            if(commonSearchService!=null){
                                    commonSearchService=null;
                                }
                        %>



        </div>
        <div>&nbsp;</div>
        <!--Dashboard Content Part End-->
        <!--Dashboard Footer Start-->
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        <!--Dashboard Footer End-->
    </div>
</body>
<script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>

</html>

