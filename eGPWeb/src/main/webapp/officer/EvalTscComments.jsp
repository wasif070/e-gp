<%-- 
    Document   : EvalTscComments
    Created on : Feb 21, 2011, 3:01:58 PM
    Author     : Administrator
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalTscnotification"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalTsccomments"%>
<%@page import="com.cptu.egp.eps.web.servicebean.BidderClarificationSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData,com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
          <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Post Comments</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
<jsp:useBean id="evalTscCommentsSrBean" class="com.cptu.egp.eps.web.servicebean.EvalTscCommentsSrBean" scope="request" />
<script type="text/javascript">
  function validate(){
            var validatebool="";
            $(".reqF_1").remove();
                    if(CKEDITOR.instances.comments.getData() == 0){
                        $("#comments").parent().append("<div class=reqF_1>Please enter comments</div>");
                            validatebool="false";
                        }else{
                             if(CKEDITOR.instances.comments.getData().length <= 5000){
                                    document.getElementById("errMsg").innerHTML="";
                                }else{
                                   $("#comments").parent().append("<div class=reqF_1>Allows maximum 5000 characters only.</div>");
                                   validatebool="false";
                                }
                          }
                            if (validatebool=='false'){
                                return false;
                            }
 }
</script>
<script type="text/javascript">
/*function updateQue(cmtId){    
    alert(cmtId);
    //return false;
    $.post("<%=request.getContextPath()%>/EvalTscCommentsServlet", {commentId:cmtId,action:'updateAjax'}, function(j){
            alert(j.toString());
            //CKEDITOR.instances.comments.setData(j);
            $("#addCmt").html('');
            alert($("#addCmt").html(j)+'clear ');
            $("#addCmt").html(j);
            alert($("#addCmt").html(j)+'add');
                                            
            //alert(CKEDITOR.instances.comments.getData()+'Data')
            //$("#txtQuestionAdd").html(j)
        });
}*/
</script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <%
                String tenderId = "0", uId = "0", poBy = "0", pkgLotId = "0", frmId = "0", bidId = "", st = "";
                st = request.getParameter("st");
                String s_action = "add";
                String s_buttonName = "Submit";
                boolean b_isNotify = true;
                TblEvalTsccomments tblEvalTsccomments = new  TblEvalTsccomments();
                int commentId = 0;
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                boolean bol_isUpdate = false;
                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");
                }
                b_isNotify =  evalTscCommentsSrBean.getNotification(tenderId);
                if (request.getParameter("uId") != null) {
                    uId = request.getParameter("uId");
                }
                if (request.getParameter("frmId") != null) {
                    frmId = request.getParameter("frmId");
                }
                poBy = session.getAttribute("userId").toString();
                if (request.getParameter("pkgLotId") != null) {
                    pkgLotId = request.getParameter("pkgLotId");
                }
                 if (request.getParameter("action") != null) {
                    s_action = request.getParameter("action");
                }
                if("update".equalsIgnoreCase(s_action)){
                    commentId = Integer.parseInt(request.getParameter("cmtId"));
                    tblEvalTsccomments =  evalTscCommentsSrBean.getComments(commentId);
                    bol_isUpdate = true;
                    s_buttonName = "Save";
                }

            %>
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">View and Post Comments
                 <span style="float:right;">
                <a href="ViewTscForm.jsp?tenderId=<%=tenderId%>&uId=<%=request.getParameter("uId")%>&st=<%=st%>&comType=null<%
                    if(b_isNotify){%>&notify=y<%}
            %>" class="action-button-goback">Go back to Dashboard</a>
<!--                <a href="SeekEvalClari.jsp?uId=<-%=request.getParameter("uId")%>&st=<-%=st%>&tenderId=<-%=tenderId%>" class="action-button-goback">Go back to Dashboard</a>-->
                </span>
                </div>
                <%--<div class="t_space">
                    <%if("update".equalsIgnoreCase(request.getParameter("msg"))){%>
                    <div class="responseMsg successMsg" style="margin-top: 12px;">Comment Update successfully</div>
                    <%}%>
                    <%if("notupdate".equalsIgnoreCase(request.getParameter("msg"))){%>
                    <div class="responseMsg errorMsg" style="margin-top: 12px;">There was some error, Please try again</div>
                    <%}%>
                </div>--%>
                 <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <form id="frmPostQuestion" name="frmPostQuestion" method="POST" action="<%=request.getContextPath()%>/EvalTscCommentsServlet">
                    <input type="hidden" name="tenderId" value="<%=tenderId%>" />
                    <input type="hidden" name="formId" value="<%=frmId%>" />
                    <input type="hidden" name="pkgLotId" value="<%=pkgLotId%>" />
                    <input type="hidden" name="bidderId" value="<%=uId%>" />
                    <input type="hidden" name="action" value="<%=s_action%>" />
                    <%if(bol_isUpdate){%>
                    <input type="hidden" name="commentId" value="<%=commentId%>" />
                    <input type="hidden" name="commentId" value="<%=tblEvalTsccomments.getComments()%>" />
                    <%}if(!b_isNotify){%>
                    
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th colspan="2" class="t-align-left">
                                Post Comments
                            </th>
                        </tr>
                        <tr>
                            <td width="15%" valign="middle" class="t-align-left ff">Comments : <span class="mandatory">*</span></td>
                            <td width="90%" class="t-align-left" id="addCmt">
                                <textarea rows="5" id="comments" name="comments" class="formTxtBox_1" style="width: 99%;">
                                <%if(bol_isUpdate){
                                    out.print(tblEvalTsccomments.getComments());
                                }%>
                                </textarea>
                            </td>
                            <script type="text/javascript">
                                            //<![CDATA[

                                            CKEDITOR.replace( 'comments',
                                            {
                                                toolbar : "egpToolbar"

                                            });

                                            //]]>
                                        </script>
                        </tr>

                    </table>
                    <div class="t_space t-align-center">
                        <label class="formBtn_1">
                            <input name="btnSubmit" type="submit" value="<%=s_buttonName%>" onclick="return validate();"/>
                        </label>
                    </div>
                    <%}%>
                    <%if (request.getParameter("msg") != null) {
                    String msgId = "", msgTxt = "";
                    boolean isError = false;
                    msgId = request.getParameter("msg");
                    if (!msgId.equalsIgnoreCase("")) {
                        if (msgId.equalsIgnoreCase("error")) {
                            isError = true;
                            msgTxt = "There was some error.";
                        } else {
                            //msgTxt = "Question "+msgId+" successfully.";
                            msgTxt = "Comment "+msgId+" successfully";
                        }
                    %>
                    <%if (isError) {%>
                    <div class="responseMsg errorMsg" style="margin-top: 12px;"><%=msgTxt%></div>
                    <%} else {%>
                    <div class="responseMsg successMsg" style="margin-top: 12px;"><%=msgTxt%></div>
                    <%}%>
                    <%}
                         }
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th class="t-align-center">Sl. No.</th>
                            <th class="t-align-center">Member Name</th>
                            <th class="t-align-center">Comments</th>
                            <%if(!b_isNotify){%>
                            <th class="t-align-center">Action</th>
                            <%}%>
                        </tr>
                        <%
                            //List<Object[]> list = evalTscCommentsSrBean.getComments(Integer.parseInt(tenderId),Integer.parseInt(frmId));
                            CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> list = dataMore.geteGPData("getPostCommentsData", tenderId, frmId,uId);
                            
                            if (list!=null &&  !list.isEmpty() && list.size() > 0) {
                                int cnt = 0;
                                for (int k=0;k<list.size();k++) {
                                            cnt++;                            // HIGHLIGHT THE ALTERNATE ROW
                                    if(Math.IEEEremainder(k,2)==0) {
                        %>
                        <tr class="bgColor-Green">
                         <% } else { %>
                        <tr>
                        <% } %>
                            <td width="5%" class="t-align-center"><%=cnt%></td>
                            <td width="15%" class="t-align-left"><%=list.get(k).getFieldName3() %></td>
                            <td width="68%" class="t-align-left"><%=list.get(k).getFieldName2()%></td>
                            <%
                                if(!b_isNotify){
                              if(poBy.trim().equalsIgnoreCase((list.get(k).getFieldName4()).toString().trim())){
                                  
                              
                            %>
                        <td width="12%" class="t-align-center">    
                        <a href="<%=request.getContextPath()%>/EvalTscCommentsServlet?commentId=<%=list.get(k).getFieldName1() %>&bidderId=<%=uId%>&tenderId=<%=tenderId%>&formId=<%=frmId%>&pkgLotId=<%=pkgLotId%>&action=updateLink">Edit</a>&nbsp;&nbsp;&nbsp; |
                            &nbsp;&nbsp;&nbsp; <a href="<%=request.getContextPath()%>/EvalTscCommentsServlet?commentId=<%=list.get(k).getFieldName1() %>&bidderId=<%=uId%>&tenderId=<%=tenderId%>&formId=<%=frmId%>&pkgLotId=<%=pkgLotId%>&action=delete">Delete</a>
                                
                            </td>
                            <%
                                                           }else{  %>
                                <td width="15%" class="t-align-center"></td>
                      <%  }
                               // break;
                               }} %>
                        </tr>
                        <%}
                                else {%>
                            <tr>
                                <td colspan="4" class="t-align-center"><span class="reqF_1 ff">No Comments Found</span></td>
                            </tr>
                        <%}%>
                    </table>
                </form>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>

