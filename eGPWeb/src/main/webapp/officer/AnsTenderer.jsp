<%-- 
    Document   : ansTenderer
    Created on : Dec 8, 2010, 7:33:25 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.Calendar" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        
        <script type="text/javascript" >
            function checkTender(){
                jAlert("there is no Tender Notice"," Alert ", "Alert");
            }

            function checkdefaultconf(){
                jAlert("Default workflow configuration hasn't crated"," Alert ", "Alert");
            }
            function checkDates(){
                jAlert(" please configure dates"," Alert ", "Alert");
            }
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <%
                            boolean flag = false;
                            String replyAction = "";
                            String msg = "";

                            if (request.getParameter("flag") != null) {
                                if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                                    flag = true;
                                }
                            }

                            if (request.getParameter("replyAction") != null) {
                                replyAction = request.getParameter("replyAction");
                            }

                            int userId = 0;
                            byte suserTypeId = 0;
                            int tenderId = 20;

                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId"))) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }

                            
                            
                            preTendDtBean.setUserId(userId);
                            preTendDtBean.setTenderId(tenderId);
                            List<SPTenderCommonData> getPreBidDates = preTendDtBean.getDataFromSP("GetPrebidDate", tenderId, 0);

                            

                            /* List<SPTenderCommonData> getConfigQus = preTendDtBean.getDataFromSP("GetConfigPrebid",tenderId,0);
                            List<SPTenderCommonData> getClosingDt = preTendDtBean.getDataFromSP("GetClosingDt",tenderId,0);


                            java.util.Calendar currentDate = java.util.Calendar.getInstance();
                            java.text.SimpleDateFormat sd= new java.text.SimpleDateFormat("dd/MM/yyyy hh:mm");
                            java.util.Calendar calendar = java.util.Calendar.getInstance();
                            int ansEndDate = 0;

                            if (getConfigQus.size() > 0) {
                            if (getConfigQus != null) {
                            if(getClosingDt.size() > 0){
                            if(getClosingDt != null){
                            if (getClosingDt.get(0).getFieldName2() != null) {
                            calendar.setTime(sd.parse(getClosingDt.get(0).getFieldName2()));
                            if (getConfigQus.get(0).getFieldName2() != null) {
                            ansEndDate = Integer.parseInt(getConfigQus.get(0).getFieldName2());
                            calendar.add(Calendar.DATE, -ansEndDate);
                            }
                            }
                            }
                            }
                            }
                            }*/

                            if (flag) {
                                if ("Replied".equalsIgnoreCase(replyAction)) {
                                    msg = " Query Replied Successfully.";
                                }
                            }
                %>
                <div class="dashboard_div">

                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">Clarification</div>
                        <div class="t_space">
                            <%if (flag) {%>
                            <div id="successMsg" class="responseMsg successMsg" style="display:block"><%=msg%></div>
                            <%} else {%>
                            <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                            <%}%>
                        </div>
                        <%pageContext.setAttribute("tenderId", "" + tenderId);%>
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div>&nbsp;</div>
                        <% pageContext.setAttribute("tab", "11");%>
                        <%@include  file="officerTabPanel.jsp"%>
                        
                                    <table width="100%" cellspacing="0" class="tableList_1">
                                        <tr>
                                            <%-- <td width="15%" class="t-align-left ff">Meeting Start Date & Time :</td>
                                             <td width="20%" class="t-align-left"><%=preBidStartDate %>  </td>
                                             <td width="15%" class="t-align-left ff">Meeting End Date & Time :</td>
                                             <td width="20%" class="t-align-left"><%=preBidEndDate%> </td>--%>
                                            <td width="26%" class="t-align-left ff">View Queries And Replies :</td>
                                            <td width="10%" class="t-align-center">
                                                <a href="<%=request.getContextPath()%>/resources/common/QuestionAnswer.jsp?tenderId=<%=tenderId%>&viewType=Question" target="_blank"  >View</a>
                                            </td>
                                            <td class="txt_2">Reply of the Query must be given within 5 days from the posting of the query</td>
                                        </tr>
                                       <tr>
                                           <td class="ff">
                                                <%
                                                    Date dtLastDate = null;
                                                    if(!listt.isEmpty())
                                                    {
                                                        dtLastDate = listt.get(0).getPostQueLastDt();
                                                    }
                                                %>Last Date and Time for posting of Query :  <td colspan="2"><%=DateUtils.gridDateToStrWithoutSec(dtLastDate)%></td>
                                           </td>
                                       </tr>
                                    </table>
                               
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td >
                                    <ul class="tabPanel_1">
                                        <li><a href="javascript:void(0);" class="sMenu" id="hrfPending" onclick="getQueryData('GetPendingQuestion');" >Pending</a></li>
                                        <li><a href="#" id="hrfReply" onclick="getQueryData('GetRepliedQuestion');">Replied</a></li>
                                    </ul>
                                </td>
                            </tr>                           
                            <tr>
                                <td>                                    
                                    <div id="queryData">

                                    </div>
                                </td>
                            </tr>
                        </table>

<!--                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr><td>
                                    <div id="docData">
                                    </div>
                                </td>
                            </tr>
                        </table>-->

                        <div>&nbsp;</div>
                    </div>
                    <!--Dashboard Content Part End-->
                    <!--Dashboard Footer Start-->
                    <%@include file="../resources/common/Bottom.jsp" %>
                    <!--Dashboard Footer End-->
                </div>
            </div>
        </div>

    </body>
                    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
<script>
    function getQueryData(queryData){
    $.ajax({
     url: "<%=request.getContextPath()%>/PreTendQuerySrBean?tenderId=<%=tenderId%>&action=getQuestionData&queryAction="+queryData,
     method: 'POST',
     async: false,
     cache : false,
     success: function(j) {
         document.getElementById("queryData").innerHTML = j;
     }
  });
//             $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,action:'getQuestionData',queryAction:queryData}, function(j){
//                 document.getElementById("queryData").innerHTML = j;
//             });
             if(queryData == "GetPendingQuestion"){
                 document.getElementById("hrfPending").className ="sMenu" ;
                 document.getElementById("hrfReply").className="";
             }else if(queryData == "GetRepliedQuestion"){
                 document.getElementById("hrfPending").className ="" ;
                 document.getElementById("hrfReply").className="sMenu";
             }
         }

         getQueryData('GetPendingQuestion');

         function ruleAlert(dateValue){
             jAlert('Reply can be posted till '+dateValue,'Query - Answer', function(RetVal) {
             });
         }

</script>
<%
            preTendDtBean = null;
            getPreBidDates = null;
%>
