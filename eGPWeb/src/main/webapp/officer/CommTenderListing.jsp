<%-- 
    Document   : CommTenderListing
    Created on : Mar 28, 2016, 3:44:39 PM
    Author     : nishith
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Committee Listing</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <!--        <script src="< %=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js" type="text/javascript"></script>-->
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
    </head>
    <%
                String comType = "", userId = "", status="", pendingStyle = "", approvedStyle = "";
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userId = hs.getAttribute("userId").toString();
                }

                if (!request.getParameter("comType").equals("")) {
                    comType = request.getParameter("comType");
                }

                if(!request.getParameter("status").equals("")){
                    status = request.getParameter("status");
                }
    %>
    <script type="text/javascript">
        function GetCal(txtname,controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: false,
                dateFormat:"%d/%m/%Y",
                onSelect: function() {
                    var date = Calendar.intToDate(this.selection.get());
                    LEFT_CAL.args.min = date;
                    LEFT_CAL.redraw();
                    this.hide();
                    $('#openingDt').focus();
                }
            });

            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })
        }

        $(document).ready(function(){
            $(".mytr").click(function(){
                $("tr[class='bgColor-Green mytr']").each(function(){
                    $(this).removeAttr('style');
                });
                $("tr[class='bgColor-white mytr']").each(function(){
                    $(this).removeAttr('style');
                });
                $(this).css("background-color", "#FFF0A5");
            });
        });
        function chkdisble(pageNo){
            $('#dispPage').val(Number(pageNo));
            if(parseInt($('#pageNo').val(), 10) != 1){
                $('#btnFirst').removeAttr("disabled");
                $('#btnFirst').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnFirst').attr("disabled", "true");
                $('#btnFirst').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnPrevious').attr("disabled", "true")
                $('#btnPrevious').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) > 1){
                $('#btnPrevious').removeAttr("disabled");
                $('#btnPrevious').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnLast').attr("disabled", "true");
                $('#btnLast').css('color', 'gray');
            }

            else{
                $('#btnLast').removeAttr("disabled");
                $('#btnLast').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnNext').attr("disabled", "true")
                $('#btnNext').css('color', 'gray');
            }
            else{
                $('#btnNext').removeAttr("disabled");
                $('#btnNext').css('color', '#333');
            }
        }
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);

                if(totalPages>0 && $('#pageNo').val()!="1")
                {
                    $('#pageNo').val("1");
                    loadTable();
                    $('#dispPage').val("1");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0)
                {
                    $('#pageNo').val(totalPages);
                    loadTable();
                    $('#dispPage').val(totalPages);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo <= totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)+1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();

                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            });
        });
    </script>
    <script type="text/javascript" >
        function ShowAlertMsg(){
            alert("Procuring Agency (PA) is yet to share the Tender with TEC for evaluation purpose. Kindly contact PA for details.");
        }
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnSearch').click(function() {
                var countVal = 0;
                if($('#txtdepartment').val() == '' && $('#cmbOffice').val() == '' && $('#txtTenderId').val() == '' && $('#openingDt').val() == '' && $('#txtRefNo').val() == ''){
                    countVal = 1;
                    $('#valMsg').html('Please enter at least One search criteria');
                }else{
                    $('#valMsg').html('');
                }
                if(countVal == 0){
                    $('#pageNo').val('1');
                    loadTable();
                }
            });
            $('#btnReset').click(function() {
                $('#txtdepartment').val('');
                $('#txtdepartmentid').val('');
                $('#cmbOffice').html('');
                document.getElementById('cmbOffice').options[0] = new Option('-- Select Office --', '');
                $('#txtTenderId').val('');
                $('#openingDt').val('');
                $('#txtRefNo').val('');
                loadTable();
            });
        });
        function loadTable()
        {
            $.post("<%=request.getContextPath()%>/CommListingServlet", {comType: '<%=comType%>',status: '<%=status%>',pageNo: $("#pageNo").val(),deptId: $("#txtdepartmentid").val(),officeId: $("#cmbOffice").val(),tenderId: $("#txtTenderId").val(),openingDt: $("#openingDt").val(),refNo: $("#txtRefNo").val(),size: $("#size").val()},  function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                sortTable();
                //sortTable();
                if($('#noRecordFound').attr('value') == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageNoTot").html($("#pageNo").val());
                chkdisble($("#pageNo").val());
                $("#pageTot").html($("#totalPages").val());
            });
        }
    </script>
    <script type="text/javascript">
        function loadOffice() {
            var deptId=$('#txtdepartmentid').val();
            $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId,funName:'officeCombo'},  function(j){
                $('#cmbOffice').children().remove().end()
                $("select#cmbOffice").html(j);
            });
        }

    </script>
    <body onload="loadTable();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1"><%=(comType.equalsIgnoreCase("TC") ? "Tender " :comType.equalsIgnoreCase("TOC") ? "Opening " : comType.equalsIgnoreCase("TEC")  ?"Evaluation " : "Tech. Sub. ")%>Committee Listing
                    <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('7');">Save as PDF</a></span>
                </div>

                <div>&nbsp;</div>
                <div class="formBg_1">
                    <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">
                        <tr>
                            <td width="17%" class="ff">Select Ministry/Division/ <br/> Organization :</td>
                            <td width="83%" colspan="3">
                                <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 400px;" tabindex="1"
                                       id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                                <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                <a href="javascript:void(0);" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                    <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                </a>
                            </td>

                        </tr>
                        <tr>
                            <td class="ff">Procuring Agency :</td>
                            <td colspan="3">
                                <select name="office" tabindex="2" class="formTxtBox_1" id="cmbOffice" style="width:400px;">
                                    <option value="">-- Select Office --</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="17%" class="ff">Tender ID :</td>
                            <td width="33%"><input tabindex="3" type="text" class="formTxtBox_1" name="tenderId" id="txtTenderId" onkeypress="checkKey(event);"/></td>
                            <td width="17%" class="ff">Ref.No :</td>
                            <td width="33%"><input type="text" tabindex="4" class="formTxtBox_1" name="refNo" id="txtRefNo" onkeypress="checkKey(event);"/></td>
                        </tr>
                        <tr>
                            <td class="ff">Opening Date and Time :</td>
                            <td><input name="openingDt" id="openingDt" tabindex="5" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('openingDt','openingDt');" />&nbsp;
                                <a  href="javascript:void(0);" title="Calender"><img id="openingDtImg" src="<%=request.getContextPath()%>/resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('openingDt','openingDtImg');"/></a></td>
                        </tr>
                        <tr>
                            <td colspan="4" class="t-align-center">
                                <label class="formBtn_1"><input type="button" tabindex="6" name="search" id="btnSearch" value="Search"/>
                                </label>&nbsp;&nbsp;
                                <label class="formBtn_1"><input type="button" tabindex="7" name="reset" id="btnReset" value="Reset"/>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="t-align-center">
                                <div class="reqF_1" id="valMsg"></div>
                            </td>
                        </tr>
                    </table>
                </div>


                        <%

                        if(status.equalsIgnoreCase("pending")){
                            pendingStyle = "background-color:#FF9326;color:#fff";
                        }else if(status.equalsIgnoreCase("approved")){
                            approvedStyle = "background-color:#FF9326;color:#fff";
                        }




%>


                <div>&nbsp;</div>
                <ul class="tabPanel_1 t_space" id="tabForApproved">
                    <li><a style="<%=pendingStyle%>" href="<%=contextPath%>/officer/CommTenderListing.jsp?comType=<%=comType%>&status=pending">Pending</a></li>
                    <li><a style="<%=approvedStyle%>" href="<%=contextPath%>/officer/CommTenderListing.jsp?comType=<%=comType%>&status=approved">Approved</a></li>
                </ul>



                <table style="padding: 12px;" width="100%" cellspacing="0" id="resultTable" class="tableList_3 t_space" cols="@0,7">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th width="7%" class="t-align-center">Tender ID</th>
                        <th width="13%" class="t-align-center">Ref. No.</th>
                        <th width="21%" class="t-align-left">Brief</th>
                        <th width="17%" class="t-align-left">Organization</th>
                        <th width="14%" class="t-align-left">Office</th>
                        <th width="15%" class="t-align-center">Opening Date & Time</th>
                        <th width="9%" class="t-align-center">Dashboard</th>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                    <tr>
                        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                        <td align="center"><input name="textfield3" onkeypress="checkKeyGoTo(event);" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                            &nbsp;
                            <label class="formBtn_1">
                                <input   type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                            </label></td>
                        <td  class="prevNext-container"><ul>
                                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                            </ul></td>
                    </tr>
                </table>
                <div align="center">
                    
                    <input type="hidden" id="pageNo" value="1"/>
                    <input type="hidden" name="size" id="size" value="10"/>
                </div>
            </div>
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                            String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="<%=(comType.equalsIgnoreCase("TOC") ? "Opening" : "Evaluation")%>CommitteeListing_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="<%=(comType.equalsIgnoreCase("TOC") ? "Opening" : "Evaluation")%>CommitteeListing" />
            </form>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabEval");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

        function checkKey(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                $('#pageNo').val('1')
                $('#btnSearch').click();
                //loadTable();
            }
        }
        function checkKeyGoTo(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                $(function() {
                    //$('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTable();
                            $('#btnGoto').click();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
                //});
            }
        }
    </script>
</html>

