<%-- 
    Document   : SeekClarificationAA
    Created on : Jun 18, 2011, 11:56:28 AM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.EvaluationMatrix"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalReportClarification"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Seek Clarification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
    </head>
    <body>
        <%
                String lotId = "0";
                if (request.getParameter("lotId") != null && !"".equals(request.getParameter("lotId"))) {
                    lotId = request.getParameter("lotId");
                }
                int evalRptToAaid = 0;
                if (request.getParameter("evalRptToAaid") != null && !"".equals(request.getParameter("evalRptToAaid"))) {
                    evalRptToAaid = Integer.parseInt(request.getParameter("evalRptToAaid"));
                }
                String rId = "";
                if (request.getParameter("rId") != null && !"".equals(request.getParameter("rId"))) {
                    rId = request.getParameter("rId");
                }
                String tenderId = "0";
                if (request.getParameter("tenderid") != null && !"".equals(request.getParameter("tenderid"))) {
                    tenderId = request.getParameter("tenderid");
                }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            </div>
            <div class="contentArea_1">
                <div class="pageHead_1">Seek Clarification<span style="float:right;"><a href="EvalReportListing.jsp?tenderid=<%=request.getParameter("tenderid")%>&rId<%=rId %>&lotId=<%=request.getParameter("lotId")%>" class="action-button-goback">Go Back To Dashboard</a></span></div>
                <!--Dashboard Header End-->
                <%
                            String userId = "";
                            HttpSession hs = request.getSession();
                            if (hs.getAttribute("userId") != null) {
                                userId = hs.getAttribute("userId").toString();
                            }
                            pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                            
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <form id="frmSeekClari" name="frmSeekClari" action="<%=request.getContextPath()%>/EvaluationServlet?action=seekClari" method="post">
                    <%                                
                                String evalType = "eval";
                                if(request.getParameter("stat")!=null){
                                    evalType = request.getParameter("stat");
                                }
                                List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("GetLotPkgDetail", tenderId, lotId);
                                String lotPkgNo = null;
                                String lotPkgDesc = null;
                                if ("0".equals(lotId)) {
                                    lotPkgNo = "Package No.";
                                    lotPkgDesc = "Package Description";
                                } else {
                                    lotPkgNo = "Lot No.";
                                    lotPkgDesc = "Lot Description";
                                }
                    %>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <jsp:include page="TERInclude.jsp">
                                <jsp:param name="tenderId" value="<%=tenderId%>"/>
                                <jsp:param name="lotId" value="<%=lotId%>"/>
                                <jsp:param name="roundId" value="<%=rId%>"/>
                                <jsp:param name="evalType" value="<%=evalType%>"/>
                            </jsp:include>
                        <tr id="trHideQuery">
                            <td valign="top" class="ff">Query : <span class="mandatory">*</span></td>
                            <td><input type="hidden" name="rfpNo" value="<%=tenderLotList.get(0).getFieldName3()%>"/>
                                <input type="hidden" name="lotPkgNo" value="<%=lotPkgNo%>"/>
                                <input type="hidden" name="lotPkgDesc" value="<%=lotPkgDesc%>"/>
                                <textarea cols="100" rows="5" id="txtaQuery" name="query" class="formTxtBox_1"></textarea>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    CKEDITOR.replace( 'query',
                                    {
                                        toolbar : "egpToolbar"

                                    });
                                    //]]>
                                </script>
                            </td>
                        </tr>
                        <tr id="trHideDate">
                            <td valign="top" class="ff">Last Date and Time for Response: <span class="mandatory">*</span></td>
                            <td class="formStyle_1"><input name="lastDateTime" type="text" class="formTxtBox_1" id="txtLastDateTime" style="width:100px;" readonly="true"  onfocus="GetCal('txtLastDateTime','txtLastDateTime');"/>
                                <img id="txtLastDateTimeImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtLastDateTime','txtLastDateTimeImg');"/>
                            </td>
                        </tr>
                    </table>
                    <div id="btnHide" class="t_space t-align-center">
                        <label class="formBtn_1">
                            <input type="hidden" name="evalRptToAaid" value="<%=evalRptToAaid%>"/>
                            <input type="hidden" name="tenderId" value="<%=tenderId%>"/>
                            <input type="hidden" name="lotId" value="<%=lotId%>"/>
                            <input type="hidden" name="rId" value="<%=rId%>"/>
                            <input  id="Submit" name="Submit" value="Submit" type="submit" onclick="return validation();" />
                        </label>
                    </div>
                </form>
                <table width="100%" cellspacing="0" class="tableList_3 t_space">
                    <tr>
                        <th width="4%" class="ff">Sl. No.</th>
                        <th width="38%">Query</th>
                        <th width="13%">Date and Time<br />of Query </th>
                        <th width="33%">Response</th>
                        <th width="12%">Date and Time<br />of Response </th>
                    </tr>
                    <%
                                int srno = 1;
                                EvaluationService evaluationService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                                for (TblEvalReportClarification listing : evaluationService.fetchDataFromEvalReportClari(evalRptToAaid)) {
                    %>
                    <tr>
                        <td class="t-align-center"><%=srno%></td>
                        <td><%=listing.getQuery()%></td>
                        <td><%=DateUtils.gridDateToStr(listing.getQueryDt())%></td>
                        <td><%if (!listing.getAnswer().equals("")) {%><%=listing.getAnswer()%><% } else {%> - <% }%></td>
                        <td><%if (listing.getAnswerDt() != null) {%><%=DateUtils.gridDateToStr(listing.getAnswerDt())%><% } else {%> -
                            <script type="text/javascript">
                                $('#btnHide').hide();
                                $('#trHideQuery').hide();
                                $('#trHideDate').hide();
                            </script>
                            <% }%></td>
                    </tr>
                    <% srno++;
                                }%>
                </table>
            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        function validation(){
            var bVal = true;
            $(".err").remove();
            if(CKEDITOR.instances.txtaQuery != null){
                //if(document.getElementById('txtaexpRequired')!=null){
                if(!required($.trim(CKEDITOR.instances.txtaQuery.getData())) || isCKEditorFieldBlank($.trim(CKEDITOR.instances.txtaQuery.getData().replace(/<[^>]*>|\s/g, ''))))
                {
                    $("#txtaQuery").parent().append("<div class='err' style='color:red;'>Please enter Query</div>");
                    bVal=false;
                }
                else
                {
                    if(!Maxlenght(CKEDITOR.instances.txtaQuery.getData(),'2000')){
                        $("#txtaQuery").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed</div>");
                        bVal=false;
                    }
                }
            }
            if($('#txtLastDateTime') != null){
                if($('#txtLastDateTime').val() == ''){
                    $("#txtLastDateTime").parent().append("<div class='err' style='color:red;'>Please enter Last Date and Time for Response</div>");
                    bVal=false;
                }else if(!CompareToForToday(document.getElementById('txtLastDateTime').value)){
                    $("#txtLastDateTime").parent().append("<div class='err' style='color:red;'>Date must be greater than the current date and time</div>");
                    bVal=false;
                }
            }

            if(!bVal){
                return false;
            } else {
                document.getElementById("Submit").style.display = 'none';
            }
        }

        function required(controlid)
        {
            var temp=controlid.length;
            if(temp <= 0 ){
                return false;
            }else{
                return true;
            }
        }
        function Maxlenght(controlid,maxlenght)
        {
            var temp=controlid.length;
            if(temp>=maxlenght){
                return false;
            }else
                return true;
        }
        function CompareToForToday(first)
        {
            var mdy = first.split('/')  //Date and month split
            var mdyhr= mdy[2].split(' ');  //Year and time split
            var mdyhrtime=mdyhr[1].split(':');
            if(mdyhrtime[1] == undefined){
                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
            }else
            {
                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
            }

            var d = new Date();
            if(mdyhrtime[1] == undefined){
                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
            }
            else
            {
                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
            }
            return Date.parse(valuedate) > Date.parse(todaydate);
        }
    </script>
</html>
