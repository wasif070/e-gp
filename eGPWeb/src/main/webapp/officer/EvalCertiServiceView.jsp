<%-- 
    Document   : EvalCertiServiceView
    Created on : Mar 18, 2011, 12:11:59 PM
    Author     : Administrator
For viewing the Score
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalServiceForms"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalServiceCriteria"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<jsp:useBean id="evalSerCertiSrBean" class="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean" scope="request" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="texxt/html; charset=UTF-8">
        <title>Evaluation Points View</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%--<script type="text/javascript">
            function ForReoi(isREOI){
                if(isREOI){
                    //$('#Score').hide();
                    alert($('#count').val());
                    for(var d=1;d<$('#count').val();d++){
                        alert($('#criLoopCount_'+d).val()+' Inner Loop')
                        for(var k=0;k<$('#criLoopCount_'+d).val();k++){
                            alert('Score_'+d+'_'+k);
                            $('#Score_'+d+'_'+k).hide();
                        }
                    }
                    $('#totalMarkTr').hide();
                }
            }
        </script>--%>
    </head>
    <%
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    //userId = Integer.parseInt(session.getAttribute("userId").toString());
                    userId = session.getAttribute("userId").toString();
                }
                String isCorrigen = "No";
                //boolean isSubDt = false;
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                if (request.getParameter("iscorri") != null && "true".equalsIgnoreCase(request.getParameter("iscorri"))) {
                    isCorrigen = "yes";
                }
                CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                boolean isREOI = false;
                if(commonService.getEventType(tenderId).toString().equals("REOI")){
                    isREOI = true;
                }
    %>
    <body>
        <div class="dashboard_div">
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
                <%
                    List<Object[]> mainCriteriaList = evalSerCertiSrBean.getMainCirteriaList();
                    List<Object []> viewList=evalSerCertiSrBean.viewEvalSerMark(Integer.parseInt(tenderId),isCorrigen);
                    String msg = request.getParameter("flag");
                %>
                <div class="contentArea_1 t_space">
                    <div class="pageHead_1">Evaluation Points View<span style="float:right;"><a href="TenderDocPrep.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a></span>
                    </div>
                    <% if (msg != null ) {%>
                    <div class="responseMsg successMsg t_space" style="margin-top: 10px;"><%=isREOI?"Criteria":"Points"%> configured successfully</div>
                    <%  }%>
                    <%
                                pageContext.setAttribute("tenderId", tenderId);
                                pageContext.setAttribute("tab", "2");
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%@include file="officerTabPanel.jsp" %>
                    <div class="tabPanelArea_1">
                        <%
                                    Map<Integer,String> mainCrit = new HashMap<Integer, String>();
                                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="4%" class="t-align-center ff">Sl. No.</th>
                                <th width="36%" class="t-align-left ff">Form Name</th>
                                <th width="30%" class="t-align-left ff">Criteria</th>
                                <th width="30%" class="t-align-left ff">Sub Criteria</th>
                            </tr>
                            <%
                                                                        int l = 0;
                                                                        int i_cnt = 0;
                                                                        /*Form Loop stats here*/
                                                                        for (SPCommonSearchData formdetails : commonSearchService.searchData("EvalSerFormsList", tenderId, userId, isCorrigen, null, null, null, null, null, null)) {
                                                                            l++;
                            %>
                            <tr>
                                <td class="t-align-center ff"><%=l%></td>
                                <td class="t-align-left ff"><%=formdetails.getFieldName2()%>
                                </td>
                            <%
                                if(l==1){
                                    for(int d = 0;d<viewList.size();d++){
                                        mainCrit.put(Integer.parseInt(viewList.get(d)[2].toString()), viewList.get(d)[4].toString());
                                    }
                                    //System.out.println("mainCrit.size-------->"+mainCrit.size());
                               }
                                
                            %>
                            <td><%=mainCrit.get(Integer.parseInt(formdetails.getFieldName1()))!=null? mainCrit.get(Integer.parseInt(formdetails.getFieldName1())) : "<label style='color: red;font-weight: bold;'>No Criteria Selected</label>"%></td>
                                <td>
                                    <table width="100%" cellspacing="0" cellpadding="0"  class="tableList_1">
                                        <%
                                        boolean bole_isNotMark = false;
                                        
                                            /*For view the Records */
                                            for(int i = 0; i<viewList.size(); i++){
                                                /*cheking subcritera id is 4 or not using if formId is same or not*/
                                                if(viewList.get(i)[2].toString().trim().equals(formdetails.getFieldName1())){
                                                    bole_isNotMark = true;
                                                   

                                        %>
                                        <tr>
                                            <td width="90%">
                                                <%out.print(viewList.get(i)[0]);%>
                                            </td>
                                            <%if(!isREOI){%>
                                            <td id="Score_<%=l%>_<%=i%>" width="10%">
                                                <%out.print(viewList.get(i)[1]);%>
                                                <input type="hidden" id="criLoopCount_<%=l%>" value="<%=i%>" />
                                            </td>
                                            <%}%>
                                        </tr>
                                        <%}/*If condition ends here*/
                                            }/*For loop ends here for viewList*/%>
                                    </table>
                                    <%if(!bole_isNotMark){
                                        i_cnt++;
                                    %>
                                <label style="color: red;font-weight: bold;">No Criteria Selected</label>
                                <%}%>
                                </td>
                                
                            </tr>
                            <%}/*Form Loop Ends here*/%>
                            <%if(!isREOI){%>
                            <tr id="totalMarkTr">
                                <td colspan="3" width="90%" class="t-align-left ff">
                                    Total Points
                                </td>
                                <td class="t-align-right ff" width="10%">
                                    <span  id="totalMark" style="color: red; padding-left: 410px;"><%if(i_cnt==l){out.print("");}else{out.print("100");}%></span>
                                    <input type="hidden" id="count" value="<%=l%>" />
                                 </td>
                             </tr>
                             <%}%>
                        </table>
                    </div>
                    <div>&nbsp;</div>
                    <!--Dashboard Content Part End-->
                </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
     <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
<%
    /*For null the objects and list*/
    if(commonSearchService!=null){
            commonSearchService = null;
        }
    if(viewList!=null){
            viewList.clear();
            viewList = null;
        }
%>

