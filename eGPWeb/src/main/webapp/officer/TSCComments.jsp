<%-- 
    Document   : TSCComments
    Created on : Feb 26, 2011, 11:41:04 AM
    Author     : Karan
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
         <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />        

         <script>
             $(document).ready(function(){
                 ShowComments();
                 //HideBidderInfo();
             });

             function ShowComments(){
                 $('#tblTSCComments').show();
                 $('#dvHideCom').show();
                 $('#dvViewCom').hide();
             }

             function HideComments(){
                $('#tblTSCComments').hide();
                 $('#dvHideCom').hide();
                 $('#dvViewCom').show();
             }

            
         </script>

 <%
 if (request.getParameter("frmId") != null || request.getParameter("formId")!=null) {
    TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    String conTenderId="0", conLotId="0", comfrmId = "0", conUId="0", conBidId="0";

        if(request.getParameter("formId")!=null){
            comfrmId = request.getParameter("formId");
        } else if (request.getParameter("frmId")!=null) {
            comfrmId = request.getParameter("frmId");
        }

     if(request.getParameter("lotId")!=null){
            conLotId = request.getParameter("lotId");
        } else if (request.getParameter("pkgLotId")!=null) {
            conLotId = request.getParameter("pkgLotId");
        }

    if(request.getParameter("tenderId")!=null){
        conTenderId=request.getParameter("tenderId");
    }

    if(request.getParameter("uId")!=null){
        conUId=request.getParameter("uId");
    } else if(request.getParameter("uid")!=null){
        conUId=request.getParameter("uid");
    }

    if(request.getParameter("bidId")!=null){
        conBidId=request.getParameter("bidId");
    }

    

%>


<div class="t_space t-align-right"  >    
    <span ><a id="dvViewCom" onclick="ShowComments()" class="action-button-viewTender">TSC Comments +</a></span>
    <span ><a id="dvHideCom" onclick="HideComments()" class="action-button-viewTender">TSC Comments -</a></span>
    <span ><a href="EvalDocList.jsp?tenderId=<%=conTenderId%>&formId=<%=comfrmId%>&uId=<%=conUId%>&st=<%=request.getParameter("st")%>" class="action-button-download">Download Documents</a></span>
</div>
            
            <table id="tblTSCComments" width="100%" cellspacing="0" class="tableList_1 t_space" >
    <tr>
        <th width="4%" class="t-align-center">Sl. No.</th>
        <th width="26%" class="t-align-center">Form Name</th>
        <th width="20%" class="t-align-center">Posted By</th>
        <th class="t-align-center">TSC Comments</th>
    </tr>
    <%
    int cntCom=0;//
    List<SPTenderCommonData> listCmTscCmt = new ArrayList<SPTenderCommonData>();
    listCmTscCmt = objTSC.returndata("getTSCComments", comfrmId, conUId);
    System.out.println(listCmTscCmt.size()+"   After list.size");
    HashMap<Integer,SPTenderCommonData> comMap = new HashMap<Integer, SPTenderCommonData>();
    for(SPTenderCommonData commonData: listCmTscCmt){
        comMap.put(Integer.parseInt(commonData.getFieldName1()), commonData);
    }
    listCmTscCmt.clear();
    for(Integer key : comMap.keySet()){
        listCmTscCmt.add(comMap.get(key));
    }
    comMap.clear();
    comMap=null;
    System.out.println(listCmTscCmt.size()+"   After list.add");
for (SPTenderCommonData sptcd : listCmTscCmt) {
    cntCom++;
%>
<tr
     <%if(Math.IEEEremainder(cntCom,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
    >
    <td class="t-align-center"><%=cntCom%></td>
    <td class="t-align-center"><a href="ViewEvalBidform.jsp?tenderId=<%=request.getParameter("tenderId")%>&uId=<%=conUId%>&formId=<%=comfrmId%>&lotId=<%=conLotId%>&bidId=<%=sptcd.getFieldName7()%>&action=Edit&isSeek=true" target="_blank"><%=sptcd.getFieldName6()%></a></td>
    <td><%=sptcd.getFieldName3()%></td>
    <td><%=sptcd.getFieldName4()%></td>
</tr>
<%
}
if(cntCom==0){%>
<tr>
    <td colspan="4">
        No Comments found.
    </td>
</tr>
<%}
listCmTscCmt.clear();
listCmTscCmt = null;
%>
</table>

<%--<%
CommonSearchService objCSS = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
for (SPCommonSearchData objForms : objCSS.searchData("GetEvalFormByFormId", conTenderId, null, null, conUId, comfrmId, "showbids", null, null, null)) {%>

<table width="100%" cellspacing="0" class="tableList_k t_space">
    <tr>
        <th valign="middle">
<!--            Form Name : <a href="ViewEvalBidform.jsp?tenderId=<%=request.getParameter("tenderId")%>&uId=<%=conUId%>&formId=<%=comfrmId%>&lotId=<%=conLotId%>&bidId=<%=conBidId%>&action=Edit&isSeek=true" target="_blank"><%=objForms.getFieldName2()%></a>-->
            Form Name : <a href="ViewEvalBidform.jsp?tenderId=<%=request.getParameter("tenderId")%>&uId=<%=conUId%>&formId=<%=comfrmId%>&lotId=<%=conLotId%>&bidId=<%=objForms.getFieldName3()%>&action=Edit&isSeek=true" target="_blank"><%=objForms.getFieldName2()%></a>
            <input type="hidden" name="formId" value="<%=objForms.getFieldName1()%>" />
            
        </th>
    </tr>
</table>

        <%}%>--%>










<%}%>










