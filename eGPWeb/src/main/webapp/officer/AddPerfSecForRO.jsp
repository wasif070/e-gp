<%-- 
    Document   : AddPerfSecForRO
    Created on : Nov 16, 2011, 11:19:10 AM
    Author     : shreyansh Jogi
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TblTenderPerformanceSec"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPerfSecurity"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    <script type="text/javascript">
        function regForNumber(value)
        {
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

        }
        function validate()
        {

            $('.err').remove();

            var count = document.getElementById('taka').value;

            //
            //            alert(document.getElementById('taka_0').value);
            //            alert(document.getElementById('taka_1').value);

            var vbool=true;

            if(!regForNumber(document.getElementById('taka').value)){

                $("#taka").parent().append("<div class='err' style='color:red;'>Please enter Performance Security Amount in BD. Tk.</div>");
                vbool = false;
            }else if(document.getElementById('taka').value.indexOf("-")!=-1){
                    $("#taka").parent().append("<div class='err' style='color:red;'>Only positve values are allowed</div>");
                    vbool = false;
                }
            else if(document.getElementById('taka').value.split(".")[1] != undefined){
                if(!regForNumber(document.getElementById('taka').value) || document.getElementById('taka').value.split(".")[1].length > 3 || document.getElementById('taka').value.split(".")[0].length > 12){
                    $("#taka").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }

            }else{

                if(!regForNumber(document.getElementById('taka').value)){
                    $("#taka").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }else if(document.getElementById('taka').value.indexOf("-")!=-1){
                    $("#taka").parent().append("<div class='err' style='color:red;'>Only positve values are allowed</div>");
                    vbool = false;
                }
                else if(document.getElementById('taka').value.length > 12){
                    $("#taka").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }
            }

            if(!regForNumber(document.getElementById('percentage').value)){

                $("#percentage").parent().append("<div class='err' style='color:red;'>Please enter Percentage.</div>");
                vbool = false;
            }else if(eval(document.getElementById('percentage').value) == 0){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Performance Security Percentage cannot be 0</div>");
                    vbool = false;
            }else if(document.getElementById('percentage').value > 25){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Performance Security Percentage cannot be greater than 25</div>");
                    vbool = false;
            }else if(document.getElementById('percentage').value.indexOf("-")!=-1){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Only positve values are allowed</div>");
                    vbool = false;
                }
            else if(document.getElementById('percentage').value.split(".")[1] != undefined){
                if(!regForNumber(document.getElementById('percentage').value) || document.getElementById('percentage').value.split(".")[1].length > 3 || document.getElementById('percentage').value.split(".")[0].length > 12){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }

            }else{

                if(!regForNumber(document.getElementById('percentage').value)){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }else if(document.getElementById('percentage').value.indexOf("-")!=-1){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Only positve values are allowed</div>");
                    vbool = false;
                }
                else if(document.getElementById('percentage').value.length > 12){
                    $("#percentage").parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                    vbool = false;
                }
            }
            if(!vbool){
                return false;
            }
        }
        function calcPer(rPrice){
            var per = $('#percentage').val();
            if(per>100){
                $('#percentage').val('');
                jAlert("Percentage can not be greater than 100.","Performance Security alert", function(RetVal) {
                });
            }else if(per.indexOf("-")!=-1){
                 $('#percentage').val('');
                jAlert("Please enter positive number(s).","Performance Security alert", function(RetVal) {
                });
            }
            else if(!isNaN(per)){
                $('#taka').val(Math.round(((rPrice*per)/100)*1000)/1000);
            }else{
                $('#percentage').val('');
                jAlert("Please enter digits only.","Performance Security alert", function(RetVal) {
                });
            }
        }
    </script>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <%
                    // Variable tenderId is defined by u on ur current page.
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    pageContext.setAttribute("tab", "2");
                    TenderDocumentSrBean tenderDocumentSrBean1 = new TenderDocumentSrBean();
                    List<TblTenderLotSecurity> lotss = null;
                    int tendid = 0;
                    int pckgId = 0;
                    String roundId="0";
                    if(request.getParameter("rId")!=null){
                        roundId = request.getParameter("rId");
                    }
                    if (request.getParameter("tenderId") != null) {
                        tendid = Integer.parseInt(request.getParameter("tenderId"));
                        pckgId = Integer.parseInt(request.getParameter("pkgId"));
                        lotss = tenderDocumentSrBean1.getLotDetailsByLotId(tendid, pckgId);
                    }
                    TblTenderPerformanceSec tenderPerformanceSec = (TblTenderPerformanceSec) AppContext.getSpringBean("TblTenderPerformanceSec");
                    List<TblTenderPerfSecurity> listt = tenderPerformanceSec.getData(tendid, pckgId + "",roundId);
                    boolean action = false;
                    if("view".equals(request.getParameter("viewAction"))){
                        action = true;
                    }
                    BigDecimal roundAmount = new BigDecimal(0);
                     RepeatOrderService ros = (RepeatOrderService)AppContext.getSpringBean("RepeatOrderService");
                    List<Object> roundAmountLst = ros.countNewContractValue(Integer.parseInt(request.getParameter("romId")));
                    if(roundAmountLst!=null && (!roundAmountLst.isEmpty())){
                        roundAmount = (BigDecimal) roundAmountLst.get(0);
                    }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Performance Security</title>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>

        <div class="contentArea_1">
            <div class="pageHead_1">Performance Security for Repeat Order
<!--                <span class="c-alignment-right"><a href="Evalclarify.jsp?tenderId=<-%=tendid%>&st=rp&comType=TEC" class="action-button-goback">Go back</a></span>-->
                <span class="c-alignment-right"><a href="repeatOrderMain.jsp?tenderId=<%=tendid%>" class="action-button-goback">Go back</a></span>
            </div>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                                 String lotId="";
                                if (request.getParameter("pkgId") != null) {
                                    pageContext.setAttribute("lotId", request.getParameter("pkgId"));
                                    lotId = request.getParameter("pkgId");
                                }

                    %>
              
                    <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                    <div>&nbsp;</div>

            <%
                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                        String procnature = commonService.getProcNature(tendid + "").toString();

            %>


            <div class="tabPanelArea_1 ">
                <form name="frmPerSec" method="post" action="<%= request.getContextPath() %>/ConsolidateServlet">

                    <input type="hidden" name="roudId" value="<%=request.getParameter("rId")%>" />
                    <input type="hidden" name="romId" value="<%=request.getParameter("romId")%>" />
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <%
                                        if (procnature.equalsIgnoreCase("services")) {
                            %>

                            <th width="10%" class="t-align-center">Package No.</th>
                            <th width="70%" class="t-align-left">Package Description</th>

                            <%} else {%>                <th width="10%" class="t-align-center">Lot No.</th>
                            <th width="40%" class="t-align-left">Lot Description</th>
                            <%}%>

                            <th width="15%" class="t-align-left">Repeat Order Amount</th>
                            <th width="15%" class="t-align-left">Percentage</th>
                            <th width="20%" class="t-align-left">Performance Security Amount in BD. Tk.</th>
                        </tr>



                        <%

                                    int i = 0;
                                    Iterator it = lotss.iterator();
                                    if (!procnature.equalsIgnoreCase("services")) {
                                        while (it.hasNext()) {
                                            TblTenderLotSecurity tblTenderLotSecurity = (TblTenderLotSecurity) it.next();
                        %>

                        <tr>
                            <td class="t-align-center"><%= tblTenderLotSecurity.getLotNo()%></td>
                            <td><%=tblTenderLotSecurity.getLotDesc()%></td>

                            <td class="t-align-center">
                                <%=roundAmount.setScale(3, 0) %>
                                <input type="hidden" value="<%=roundAmount%>" name="lamount"/>
                                <input type="hidden" value="perfsec" name="action" />
                            </td>
                            <td class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    if(action){
                                        if (!listt.isEmpty() && listt != null) {
                                            out.println(listt.get(0).getPercentage().setScale(3, 0));
                                        }
                                    }else {%>
                                    <input type="text" class="formTxtBox_1"name="percentage" id="percentage" onblur="calcPer(<%=roundAmount%>);"
                                           value="<%if (!listt.isEmpty() && listt != null) {
                                        out.println(listt.get(0).getPercentage().setScale(3, 0));
                                    }
                                       %>"/>
                                <%}} else {%>
                                <input type="text" class="formTxtBox_1"name="percentage" id="percentage" onblur="calcPer(<%=roundAmount%>);"/>
                                <%}%>
                            </td>
                            <td class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {
                                    if(action){
                                        if (!listt.isEmpty() && listt != null) {
                                            out.println(listt.get(0).getPerSecurityAmt().setScale(3, 0));
                                        }
                                    }else {%>
                                    <input type="text" readonly class="formTxtBox_1"name="taka" id="taka" value="<%if (!listt.isEmpty() && listt != null) {
                                        out.println(listt.get(0).getPerSecurityAmt().setScale(3, 0));
                                    }
                                       %>"/>
                                <%}} else {%>
                                <input type="text" readonly class="formTxtBox_1"name="taka" id="taka" />
                                <%}%>

                            </td>
                        </tr>
                        <%
                                                                    i++;

                                                                }
                                                            } else {
                                                                int j = 0;
                                                                List<Object[]> list = commonService.getPkgDetailWithAppPkgId(tendid);
                                                                for (Object[] obj : list) {
                        %>
                        <tr>
                            <td class="t-align-center"><%= obj[0].toString()%></td>
                            <td><%=obj[1].toString()%></td>
                            <td class="t-align-center">
                                <%if (request.getParameter("isedit") != null) {%>
                                <input type="text" class="formTxtBox_1"name="taka" id="taka" value="<%if (!listt.isEmpty() && listt != null) {
                                        out.println(listt.get(0).getPerSecurityAmt().setScale(3, 0));
                                    }
                                       %>"/>
                                <%} else {%>
                                <input type="text" class="formTxtBox_1"name="taka" id="taka" />
                                <%}%>
                            </td></tr>
                            <%
                                                j++;

                                            }

                                        }
                            %>
                        <input type="hidden" name="tenderId" value="<%=tendid%>" />
                        <input type="hidden" name="uId" value="<%=c_obj[14].toString()%>" />
                        <input type="hidden" name="pckid" value="<%=request.getParameter("pkgId")%>" />
                        <input type="hidden" name="isEdit" value="<%if (request.getParameter("isedit") != null) {
                                        out.print(request.getParameter("isedit"));
                                    }
                               %>" />
                        <input type="hidden" name="forupdate" value="<%if (!listt.isEmpty() && listt != null) {
                                        out.print(listt.get(0).getPerCostLotId());
                                    }
                               %>"/>
                        <tr>
                            <td colspan="5" class="t-align-center ff">
                                <% if(!action){ %>
                                <span class="formBtn_1">
                                    <input type="submit" name="submit" id="submit" value="<%if (request.getParameter("isedit") != null) {out.print("Update");}
                                    else{out.print("Submit");}
                               %>" onclick="return validate();" /></span>
                                    <% } %>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div>&nbsp;</div>

        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

    </body>

</html>
