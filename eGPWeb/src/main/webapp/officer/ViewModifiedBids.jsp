<%-- 
    Document   : ViewModifiedBids
    Created on : Jun 15, 2011, 5:09:20 PM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>

<html>
    <head>
         <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            <%if ("withdrawal".equalsIgnoreCase(request.getParameter("view"))) {%>
            Tender Withdrawal Details
            <%} else if ("modify".equalsIgnoreCase(request.getParameter("view"))) {%>
            Tender Substitution / Modification Details
            <%}%>
        </title>
         <%String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />

        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>        
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

    </head>
    <body>
         <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                    boolean isCurUserPE = false;
                    String strUserId="";
                    String strTenderId="0", strPkgLotId="0", strViewType="";
                    String strReferer = "";
                    //CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");

                 HttpSession hs = request.getSession();
                 if (hs.getAttribute("userId") != null) {
                         strUserId = hs.getAttribute("userId").toString();
                  } else {
                  //   response.sendRedirectFilter(request.getContextPath() + "/SessionTimedOut.jsp");
                     response.sendRedirect("SessionTimedOut.jsp");
                  }


                if (request.getHeader("referer") != null) {
                    strReferer = request.getHeader("referer");

                }

                  if (request.getParameter("tenderid") !=null){
                        strTenderId=request.getParameter("tenderid");
                    }
                     if (request.getParameter("lotId") != null){
                        if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
                           strPkgLotId=request.getParameter("lotId");
                        }
                    }

                     if (request.getParameter("view") != null){
                        if (request.getParameter("view")!="" && !request.getParameter("view").equalsIgnoreCase("null")){
                           strViewType=request.getParameter("view");

                        }
                    }

                 System.out.println("strViewType :" + strViewType);

                %>


                <div class="contentArea_1">
                    <div class="pageHead_1">
                        <%if ("withdrawal".equalsIgnoreCase(request.getParameter("view"))) {%>
            Tender Withdrawal Details
            <%} else if ("modify".equalsIgnoreCase(request.getParameter("view"))) {%>
            Tender Substitution / Modification Details
            <%}%>

                        <span style="float: right;" >
                            <%if ("withdrawal".equalsIgnoreCase(request.getParameter("view"))) {%>
                                <a href="OpenComm.jsp?tenderid=<%=strTenderId%>" class="action-button-goback">Go Back</a>
                            <%} else if ("modify".equalsIgnoreCase(request.getParameter("view"))) {%>
                                <a href="OpenComm.jsp?tenderid=<%=strTenderId%>" class="action-button-goback">Go Back</a>
                            <%}%>
                            
                        </span>
                    </div>

                <!--  START: TENDER INFO BAR -->
                <% pageContext.setAttribute("tenderId", strTenderId); %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <!--  END: TENDER INFO BAR -->

                
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
        <tr>
            <th width="4%" class="t-align-center">Sl. No.</th>
            <th width="41%" class="t-align-center">Bidder / Consultant Name</th>
            <th class="t-align-center" width="15%">

                <%if ("modify".equalsIgnoreCase(strViewType)) {%>
                Date and Time of Tender Substitution / Modification

                <%} else {%>
                Date and Time of Tender Withdrawal
                <%}%>


            </th>
            <th class="t-align-center" width="40%">
                <%if("modify".equalsIgnoreCase(strViewType)) {%>
                Reason for Substitution / Modification
                <%} else {%>
                Reason for Withdrawal
                <%}%>
            </th>
            
        </tr>

    <%  int j = 0;

     for (SPTenderCommonData sptcd : tenderCommonService.returndata("getBidWithdrawalModificationList", strTenderId, strViewType)){
       
         j++;  %>
         <tr
            <%if(Math.IEEEremainder(j,2)==0) {%>
                class="bgColor-Green"
            <%} else {%>
                class="bgColor-white"
            <%}%>
             >
             <td class="t-align-center"><%=j%></td>
             <td class="t-align-left"><%=sptcd.getFieldName4()%></td>
             <td class="t-align-center"><%=sptcd.getFieldName5()%></td>
             <td class="t-align-left"><%=sptcd.getFieldName6()%></td>
         </tr>
   <%}%>
   <%if(j==0){%>
   <tr>
       <td colspan="4">No records available</td>
   </tr>
   <%}%>
</table>

</div>
                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->

        </div>
    </body>
</html>
