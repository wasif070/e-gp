<%-- 
    Document   : EvalRptDocToTC
    Created on : Apr 27, 2016, 1:52:53 PM
    Author     : HP-Ahsan
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<!DOCTYPE html>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation Document</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript">
            function doUnload()
            {   if(window.opener.document.getElementById("dataDoc")){
                    var cntObj = document.getElementById("csDocCnt");
                    var count = 0;
                    if(cntObj!=null){
                     count = parseInt(cntObj.value);
                    }
                    for(var i=1;i<=count;i++){
                        document.getElementById("anchorDelImg_"+i).style.display = "none";
                    }
                    window.opener.document.getElementById("dataDoc").innerHTML = document.getElementById('listingDoc').innerHTML;
                    window.opener.document.getElementById("manUpload").value = document.getElementById('manUpload').value;
                }




            }
            /*Validation*/
            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:100}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please Select Document.</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please Enter Description.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                    }
                });
            });
        </script>
        <script type="text/javascript">
            
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                            browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#button').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body onunload="doUnload();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->


            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
            String roundId = "";
            String tenderId = "";
            String lotId = "";

            if (request.getParameter("roundId") != null) {
                roundId = request.getParameter("roundId");
            }
            if (request.getParameter("tenderId") != null) {
                tenderId = request.getParameter("tenderId");
            }
             if (request.getParameter("lotId") != null) {
                lotId = request.getParameter("lotId");
            }
             System.out.println(lotId);
            %>
             <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <div class="contentArea_1">
                <div class="pageHead_1">Upload Evaluation Document
                         <span class="c-alignment-right">
                            <a href="EvalComm.jsp?tenderid=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
                        </span>
                </div>
             <%
                   pageContext.setAttribute("tenderId", tenderId);
             %>
             <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <form id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/ServletEvalRptDoc" enctype="multipart/form-data" name="frmUploadDoc">
                    <input type="hidden" name="roundId" value="<%=roundId%>"/>
                    <input type="hidden" name="tenderId" value="<%=tenderId%>"/>
                    <input type="hidden" name="lotId" value="<%=lotId%>"/>


                    <%
                                if (request.getParameter("fq") != null) {
                                    if (request.getParameter("fq").equals("Removed") || request.getParameter("fq").equals("Uploaded")) {
                    %>
                    <div class="responseMsg successMsg" style="margin-top: 10px;">File <%=request.getParameter("fq")%> Sucessfully</div>
                    <%} else {%>
                    <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=request.getParameter("fq")%></div>
                    <%
                                    }
                                }
                                if (request.getParameter("fs") != null) {
                    %>

                    <div class="responseMsg errorMsg"  style="margin-top: 10px;">

                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                                }
                    %>
                    <%
                    CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                     List<SPCommonSearchDataMore> listReportID = null;
                    String evalRptStatus = "";
                    listReportID = dataMore.getEvalProcessInclude("getEvalRptSent", tenderId,lotId,roundId);
                    if(listReportID!=null && (!listReportID.isEmpty())){
                        evalRptStatus = listReportID.get(0).getFieldName1();
                    }
                    //if((usid.equalsIgnoreCase(peId) && evalRptStatus.equalsIgnoreCase("senttopa")) || (usid.equalsIgnoreCase(userId_TEC_CP) && lstTORReport.size() > 0) || (usid.equalsIgnoreCase(userId_TC) && isRptSentToTC)){
                    if(!evalRptStatus.equalsIgnoreCase("senttopa")){
                    %>
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>

                            <td colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>
                        <tr>
                            <td width="15%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                            <td width="85%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Description : <span>*</span></td>
                            <td>
                                <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                <div id="dvDescpErMsg" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>

                                <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" /></label>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");%>
                            <td class="t-align-left">
                                Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
                    <%}%>
                </form>
                    <div id="listingDoc">
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="4%" class="t-align-center">Sl. <br/> No.</th>
                            <th class="t-align-center" width="27%">File Name</th>
                            <th class="t-align-center" width="28%">File Description</th>
                            <th class="t-align-center" width="7%">File Size <br />
                                (in KB)</th>
                            <th class="t-align-center" width="18%">Action</th>
                        </tr>
                        <%

                                    int docCnt1 = 0;
                                    TenderCommonService tenderCS1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    /*Listing of data*/
                                    for (SPTenderCommonData sptcd : tenderCS1.returndata("evaluationDoc", tenderId, roundId)) {
                                        docCnt1++;
                        %>
                        <tr>
                            <td class="t-align-left"><%=docCnt1%>
                                <input type="hidden" name="manUpload" id="manUpload" value="<%=docCnt1%>"/>
                            </td>
                            <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                            <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                            <td class="t-align-center">
                                <a href="<%=request.getContextPath()%>/ServletEvalRptDoc?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&roundId=<%=roundId%>&tenderId=<%=tenderId%>&lotId=<%=lotId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                &nbsp;
                                <%if (request.getParameter("noDelete") != null) {%>
                                <a href="<%=request.getContextPath()%>/ServletEvalRptDoc?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&roundId=<%=roundId%>&tenderId=<%=tenderId%>&lotId=<%=lotId%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" id="anchorDelImg_<%= docCnt1 %>" alt="Remove" width="16" height="16" /></a>
                                <%}%>
                            </td>
                        </tr>

                        <%   if (sptcd != null) {
                                            sptcd = null;
                                        }
                                    }%>
                        <% if (docCnt1 == 0) {%>
                        <tr>
                            <td colspan="5" class="t-align-center">No records found.</td>
                        </tr>
                        <%}%>
                         <input type="hidden" name="csDocCnt" id="csDocCnt" value="<%=docCnt1%>" />
                    </table>
                    </div>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%-- <table width="100%" cellspacing="0" class="footerCss">
                 <tr>
                     <td align="left">e-GP &copy; All Rights Reserved
                         <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
                     <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a>


                    </td>
                </tr>
            </table>--%>
            <!-- Bottom            -->
            <!--Dashboard Footer End-->


        </div>
             <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
