<%-- 
    Document   : TOS
    Created on : Nov 20, 2010, 4:51:04 PM
    Author     : Rajesh Singh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <title>Tender Opening Sheet</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript">
            function checkfile()
            {
                
                var temp=true;
                if (document.getElementById('documentBrief').value=="")
                {
                    document.getElementById('dvDescpErMsg').innerHTML="<div class='reqF_1'>Please enter Description.</div>";
                    temp=false;
                }
                
                value1=document.getElementById('uploadDocFile').value;
                param="document";
                var ext = value1.split(".");
                var fileType = ext[ext.length-1];
                fileType=fileType.toLowerCase();
                
                if (param=="document" && ( fileType =='pdf'|| fileType =='zip'|| fileType =='jpeg'||
                    fileType =='jpg' || fileType =='gif' || fileType =='doc' || fileType =='docx' ||fileType =='xls' || fileType =='xlsx' ||
                    fileType =='dwg' || fileType =='dwg' || fileType =='dxf'))
                {
                    document.getElementById('dvUploadFileErMsg').innerHTML='';
                }
                else
                {
                    document.getElementById('dvUploadFileErMsg').innerHTML="<div class='reqF_1'>Please select file.</div>";
                    temp=false;
                }
                if(temp==false)
                {return false;}
            }
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpld').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>

        <%--<div class="dashboard_div">--%>
            <!--Dashboard Header Start-->
            <div class="topHeader">
<%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
<div class="contentArea_1">
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
                <div class="pageHead_1">Tender Opening Sheet
                <%
                String url=request.getHeader("referer");
                if(url.substring(url.lastIndexOf("/")+1, url.lastIndexOf(".jsp")).equalsIgnoreCase("evalcomm")){%>
                    <span style="float:right;"><a href="EvalComm.jsp?tenderid=<%=request.getParameter("tid")%>" class="action-button-goback">Go Back</a></span></div>
                <%}else{%>
                    <span style="float:right;"><a href="OpenComm.jsp?tenderid=<%=request.getParameter("tid")%>" class="action-button-goback">Go Back</a></span></div>
                <%}%>
                <div class="t_space">
                    <%
                                if(request.getParameter("errMsg")!=null){
                            String errMsg = request.getParameter("errMsg");
                            if (errMsg != null && errMsg.equals("1")) {%>
                <div class="responseMsg successMsg" style="margin-top: 10px;">Document uploaded successfully</div>
                <%} else if (errMsg != null) {%>
                <div class="responseMsg successMsg" style="margin-top: 10px;">Document Removed successfully</div>
                <%}}%>
                </div>
                <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", request.getParameter("tid"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                 <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("approved")){
                            msgTxt="TOS Report signed successfully.";
                        } else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError && msgTxt!=""){%>
                   <div>&nbsp;</div>
                   <div class="responseMsg errorMsg"><%=msgTxt%></div>
                   <%} else if (msgTxt!="") {%>
                   <div>&nbsp;</div>
                   <div class="responseMsg successMsg"><%=msgTxt%></div>
                   <%}%>
                <%}}%>


                <%
                            String tenderId =request.getParameter("tid");
                            boolean isPackage = false, isLot = false;
                            List<SPTenderCommonData> listTenderType = tenderCommonService.returndata("gettenderlotbytenderid", tenderId, null);

                            if (!listTenderType.isEmpty()) {
                                String tenderType = "";
                                tenderType = listTenderType.get(0).getFieldName1();

                                if (tenderType.equalsIgnoreCase("package")) {
                                    isPackage = true;
                                } else {
                                    isLot = true;
                                }
                            }
                            
                            String i = "", userId="";
                            //Fetch tenderid from querysting and get corrigendumid
                            boolean isDecrypter=true, isTOCChairPerson = false;
                            if (session.getAttribute("userId") == null) {
                                response.sendRedirect("SessionTimedOut.jsp");
                            } else {
                                userId = session.getAttribute("userId").toString();
                            }
                            if (!request.getParameter("tid").equals("")) {
                                i = request.getParameter("tid");
                                
                            }


                            /*START : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/
                            List<SPTenderCommonData> lstCurUserInfo = tenderCommonService.returndata("getUserInfoForOpeningProcess", i, userId);
                            if (!lstCurUserInfo.isEmpty() && lstCurUserInfo.size()>0){
                                
                                if ("Chair Person".equalsIgnoreCase(lstCurUserInfo.get(0).getFieldName1())){
                                   isTOCChairPerson = true;
                                }
                                if ("yes".equalsIgnoreCase(lstCurUserInfo.get(0).getFieldName2())){
                                            isDecrypter = true;
                                }
                            }
                            /*END : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/

                            List<SPTenderCommonData> listas = tenderCommonService.returndata("getTenderLotSecurityByTenderId", i, null);

                %>
                

                <%
                            //TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> list = tenderCommonService.returndata("getTenderPackageByTenderId", i, null);
                            if (!list.isEmpty()) {
                %>
                <table border="0" cellspacing="10" cellpadding="0" width="100%" class="tableList_1  t_space">
                    <tr><td colspan="2" class="ff">Document Lot Details:</td></tr>
                    <tr>
                        <td width="14%" class="ff">Package  No. :</td>
                        <td width="86%"><%=list.get(0).getFieldName2()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Package  Description : </td>
                        <td><%=list.get(0).getFieldName3()%></td>
                    </tr>
                </table>
                <%}%>

                <%
                            if (!listas.isEmpty()) {
                                if (!listas.get(0).getFieldName4().equals("3")) {
                                    if(!listas.get(0).getFieldName5().equals("0.00")){

                %>
                <table width="100%" cellspacing="0" class="tableList_1  t_space">
                    <tr><td colspan="4" class="ff">Bidder/Consultant Security Details :</td></tr>
                    <tr>
                        <th width="8%" class="t-align-center">Lot  No.</th>
                        <th width="60%" class="t-align-left">Lot Description</th>
                        <th width="22%" class="t-align-left">Tender security</th>
                        <%if (!listas.get(0).getFieldName3().equals("0.00")) {%>
                        <th width="12%" class="t-align-center">View Payment</th>
                        <%}%>
                    </tr>
                    <%


                                             for (SPTenderCommonData commonTenderDetails : listas) {
                    %>
                    <tr>
                        <td class="t-align-center"><%=commonTenderDetails.getFieldName1()%></td>
                        <td class="t-align-left"><%=commonTenderDetails.getFieldName2()%></td>
                        <td class="t-align-left"><%=commonTenderDetails.getFieldName3()%></td>
                        <%if (!listas.get(0).getFieldName3().equals("0.00")) {%>
                        <td class="t-align-center"><a href="LotWisePaymentView.jsp?tenderId=<%=i%>&lotId=<%=commonTenderDetails.getFieldName1()%>">View</a></td>
                        <%}%>
                    </tr>
                    <%}%>
                </table>
                <%}
                        }}%>

                <table width="100%" cellspacing="0" class="tableList_1  t_space">
                    <tr><td colspan="5" class="ff">Tender Opening Committee Details :</td></tr>
                    <tr>
                        <th width="20%" class="t-align-left">Committee Member </th>
                        <th width="25%" class="t-align-left">Designation</th>
                        <th width="26%" class="t-align-left">Committee Role</th>
                        <th width="17%" class="t-align-center">Opening Date and Time</th>
                        <th width="12%" class="t-align-center">Status</th>
                    </tr>
                    <%
                                List<SPTenderCommonData> listcomm = tenderCommonService.returndata("getEmployeeinfo", i, null);
                                if (!listcomm.isEmpty()) {
                                for (SPTenderCommonData commonTenderDetails : listcomm) {
                    %>
                    <tr>
                        <td class="t-align-left"><%=commonTenderDetails.getFieldName1()%></td>

                        <td class="t-align-left"><%=commonTenderDetails.getFieldName2()%></td>
                        <td class="t-align-left">
                            <%if(commonTenderDetails.getFieldName3().equalsIgnoreCase("chair person")){%>
                            Chairperson
                            <%}else{%>
                            <%=commonTenderDetails.getFieldName3()%>
                            <%}%>
                            

                        </td>
                        <% if (commonTenderDetails.getFieldName5().equals("pending")) {%>
                        <td class="t-align-left"></td>
                        <%} else {%>
                        <td class="t-align-center"><%=commonTenderDetails.getFieldName4()%></td>
                        <%}%>
                        <td class="t-align-center">
                            <%if(commonTenderDetails.getFieldName5().equalsIgnoreCase("approved")){%>
                            Opened
                            <%}else{%>
                            Pending
                            <%}%>
                        </td>
                    </tr>
                    <%}}else{%>
                    <td colspan="5">No Record Found.</td>
                    <%}%>
                </table>

                

                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr><td colspan="2" class="ff">Bidder/Consultant Participation Details :</td></tr>
                    <tr>
                        <th width="20%" class="t-align-left">Bidder/Consultant Name</th>
                        <th width="25%" class="t-align-left">Tender  Submission Details</th>
                    </tr>
                    <%
                                List<SPTenderCommonData> list1 = tenderCommonService.returndata("getTenderNamebyTenderId", i, null);
                                if (!list1.isEmpty()) {

                                    for (SPTenderCommonData commonTenderDetails : list1) {
                    %>
                    <tr>
                        <td class="t-align-left"><%=commonTenderDetails.getFieldName4()%></td>
                        <td class="t-align-center"><a href="TenderSubmission.jsp?tenderId=<%=request.getParameter("tid")%>&Uid=<%=commonTenderDetails.getFieldName3()%>">View</a></td>
                    </tr>
                    <%}
                                            } else {%>
                    <tr>
                        <td class="t-align-left" colspan="2">No Record Found</td>
                    </tr>
                    <%}
                    %>
                </table>

                <table width="100%" cellspacing="0" class="tableList_1  t_space">
                    <tr>
                        <td class="t-align-left" width="50%"><strong>Tender Tampered :</strong></td>
                        <%
                                    List<SPTenderCommonData> list11 = tenderCommonService.returndata("getBidTemperedDetail", request.getParameter("tid"), null);
                                    if (!list11.isEmpty()) {
                        %>
                        <td class="t-align-left" width="50%">Yes</td>
                        <%} else {%>
                        <td class="t-align-left" width="50%">No</td>
                        <%}%>
                    </tr>

                </table>

                <table width="100%" cellspacing="0" class="tableList_1  t_space">
                    <tr><td colspan="4" class="ff">Bid Modification / Withdrawal Details :</td></tr>
                    <tr>
                        <th width="22%" class="t-align-left">Bidder/Consultant Name</th>
                        <th width="25%" class="t-align-left">Withdrawal / Modification</th>
                        <th width="40%" class="t-align-left">Comments</th>
                        <th width="13%" class="t-align-center">Date and Time</th>
                    </tr>
                    <%
                                List<SPTenderCommonData> list3 = tenderCommonService.returndata("getBidModificationbyTenderId", i, null);
                                if (!list3.isEmpty()) {

                                    for (SPTenderCommonData commonTenderDetails : list3) {
                    %>
                    <tr>
                        <td class="t-align-left"><%=commonTenderDetails.getFieldName1()%></td>
                        <td class="t-align-center"><%=commonTenderDetails.getFieldName2()%></td>
                        <td class="t-align-left"><%=commonTenderDetails.getFieldName3()%></td>
                        <td class="t-align-center"><%=commonTenderDetails.getFieldName4()%></td>
                    </tr>
                    <%}
                                            } else {%>
                    <tr>
                        <td class="t-align-left" colspan="4">No Record Found</td>
                    </tr>
                    <%}
                    %>
                </table>
                <%

                List<SPTenderCommonData> forChairPerson = tenderCommonService.returndata("TOSSentToPE", i, "");
                boolean ischairPerson = false;
                        if(forChairPerson.isEmpty()){
                            ischairPerson = true;
                }
%>
                <div class="ff t_space">TOS Reference Document</div>
                <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/ServletTOSDocUpload" enctype="multipart/form-data" name="frmUploadDoc">
                    <input type="hidden" name=tenderId value="<%=i%>" />
                    
                    <%
                                if (request.getParameter("fq") != null) {
                    %>
                    <div> &nbsp;</div>
                    <div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
                    <%
                                }
                                if (request.getParameter("fs") != null) {
                    %>
                    <div> &nbsp;</div>
                    <div class="responseMsg errorMsg">

                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                                }
                    %>
                   <%if(request.getParameter("view")!=null && (request.getParameter("view").equals("view") || request.getParameter("view").equals(""))){%>
                   <%}else{%>
                    <%if (isTOCChairPerson){%>
                    <%if(ischairPerson){%>
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>

                            <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>
                        <tr>
                            <td width="10%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                            <td width="90%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                                <div id="dvUploadFileErMsg" class="reqF_1"></div>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Description : <span>*</span></td>
                            <td>
                                <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                <div id="dvDescpErMsg" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>

                                <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" onclick="return checkfile();" /></label>
                            </td>
                        </tr>
                    </table>
                  <%}%>
                  <%if(ischairPerson){%>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("tenderer");%>
                            <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
                    <%}}%>
                    <%}%>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space t_space" >
                        <tr>
                            <th width="4%" class="t-align-center">Sl. No.</th>
                            <th class="t-align-center" width="24%">File Name</th>
                            <th class="t-align-center" width="40%">File Description</th>
                            <th class="t-align-center" width="10%">File Size<br />
                                (in KB)</th>

                            <th class="t-align-center" width="13%">Action</th>
                        </tr>
                        <%

                                    //String tender = request.getParameter("tid");

                                    int docCnt = 0;


                                    for (SPTenderCommonData sptcd : tenderCommonService.returndata("TosDocInfo", i, "")) {
                                        docCnt++;
                        %><tr>
                            <td class="t-align-center"><%=docCnt%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                            <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>

                            <td class="t-align-center">
                                <a href="<%=request.getContextPath()%>/ServletTOSDocUpload?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tender=<%=i%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                &nbsp;
                                <a href="<%=request.getContextPath()%>/ServletTOSDocUpload?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tender=<%=i%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                            </td>
                        </tr>

                        <%   if (sptcd != null) {
                                        sptcd = null;
                                    }
                                }%>
                        <% if (docCnt == 0) {%>
                        <tr>
                            <td colspan="5" align="center">No Records Found.</td>
                        </tr>
                        <%}%>
                    </table>
                </form>

                <table width="100%" cellspacing="0" class="tableList_1  t_space">
                    <tr><td colspan="6" class="ff">TOS Signing Details:</td></tr>
                    <tr>
                        <th width="18%" class="t-align-left">Committee Member</th>
                        <th width="17%" class="t-align-left">Designation</th>
                        <th width="14%" class="t-align-left">Committee Role</th>
                        <th width="27%" class="t-align-left">Comments</th>
                        <th width="14%" class="t-align-center">Signing Date and Time</th>
                        <th width="10%" class="t-align-center">Status</th>
                    </tr>
                    <%
                                List<SPTenderCommonData> list4 = tenderCommonService.returndata("getTOSSigningInfo", i, null);
                                if (!list4.isEmpty()) {
                                    for (SPTenderCommonData commonTenderDetails : list4) {

                                        String currUserId = "";

                            /*START : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/
                                String CurrUserCommRole="", CurrUserProcRole="", isCurrUserDecrypterVal="no";
                                currUserId = commonTenderDetails.getFieldName9();
                                 List<SPTenderCommonData> lstUserCommInfo = tenderCommonService.returndata("getUserCommitteeInfoForOpeningProcess", i, currUserId);
                                 if (!lstUserCommInfo.isEmpty() && lstUserCommInfo.size() > 0) {

                                     CurrUserCommRole = lstUserCommInfo.get(0).getFieldName1();
                                     CurrUserProcRole = lstUserCommInfo.get(0).getFieldName1();
                                     if ("yes".equalsIgnoreCase(lstUserCommInfo.get(0).getFieldName2())) {
                                         isCurrUserDecrypterVal = "yes";
                                     }
                                 }
                        /*END : CODE TO GET USER INFORMATIN FOR OPENING PROCESS*/
                                        
                    %>
                    <tr>
                        <td class="t-align-left">
                            <% if (userId.equalsIgnoreCase(commonTenderDetails.getFieldName9())){%>
                                <%if ("Pending".equalsIgnoreCase(commonTenderDetails.getFieldName5())){%>
                                    <a href="TOSSigningApp.jsp?id=<%=commonTenderDetails.getFieldName6()%>&uid=<%=commonTenderDetails.getFieldName9()%>&tid=<%=request.getParameter("tid")%>"><%=commonTenderDetails.getFieldName1()%></a>
                                <%} else {%>
                                    <%=commonTenderDetails.getFieldName1()%>
                                <%}%>
                            <%} else {%>
                                <%=commonTenderDetails.getFieldName1()%>
                            <%}%>
                            
                        </td>
                        <td class="t-align-left"><%=commonTenderDetails.getFieldName2()%></td>
                        <td class="t-align-left">
                            <%if(CurrUserCommRole.equalsIgnoreCase("chair person")){%>
                            	Chairperson
                            <%}else{%>
                            <%=CurrUserCommRole%>
                            <%}%>

                        </td>
                        <td class="t-align-left"><%=commonTenderDetails.getFieldName7()%></td>
                        <td class="t-align-center"><%=commonTenderDetails.getFieldName8()%></td>
                        <td class="t-align-center">
                            <%if(commonTenderDetails.getFieldName5().equalsIgnoreCase("approved")){%>
                            Signed
                            <%}else{%>
                            <%=commonTenderDetails.getFieldName5()%>
                            <%}%>
                        </td>
                    </tr>
                    <%}
                                            } else {%>
                    <td class="t-align-left" colspan="6">No Record Found</td>
                    <%}%>
                </table>
                <%
                    if(isPackage){
                        //This is for package %>
                        <%@include file="TenderFormsList.jsp" %>
                    <%}
                    else
                    {%>
                      <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th width="11%" class="t-align-left">Lot. No.</th>
                    <th class="t-align-left" width="79%">Lot Description</th>
                </tr>
                <%  int j = 0;
                     for (SPTenderCommonData sptcd : tenderCommonService.returndata("gettenderlotbytenderid", tenderId, null)) {
                         j++;                         
         %>
                <tr>
                    <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                    <td class="t-align-left"><%=sptcd.getFieldName2()%>
                        <jsp:include page="TenderFormsList.jsp">
                            <jsp:param name="lotId" value="<%=sptcd.getFieldName3()%>"/>
                        </jsp:include>
                    </td>
                </tr>
                <%}%>
                <%if (j == 0) {%>
                <tr>
                    <td colspan="3" class="t-align-left">No lots found!</td>
                </tr>
                <%}%>
            </table>
                   <% }
                %>
                
            <table width="100%" cellspacing="0" class="tableList_1  t_space">
                    <tr>
                        <th  width="58%" class="t-align-center">Report Name</th>
                        <th  width="24%" class="t-align-center">Action</th>
                    </tr>
                    <%
                            String tendId= request.getParameter("tid");
                            ReportCreationService creationService = (ReportCreationService)AppContext.getSpringBean("ReportCreationService");
                            String evalType = creationService.getTenderEvalType(tendId);
                            String pgName=null;
                            if(evalType.equalsIgnoreCase("item wise")){
                                pgName = "ItemWiseReport";
                            }else{
                                pgName = "TenderReport";
                            }
                            for(Object[] obj : creationService.getRepNameId(tendId)){
                     %>
                    <tr>
                        <td><%=obj[1]%></td>
                        <td class="t-align-center"><a href="<%=pgName%>.jsp?tenderid=<%=tendId%>&repId=<%=obj[0]%>&istos=y">Price Bid Comparative</a></td>
                    </tr>
                    <%}%>
                 </table>
            </div>

            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->

            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
       <%-- </div>--%>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
