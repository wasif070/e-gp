<%-- 
    Document   : ManageContractAgreement
    Created on : Jan 10, 2011, 11:14:17 AM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.model.table.TblContractSign"%>
<%@page import="com.cptu.egp.eps.model.table.TblNoaIssueDetails"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Agreement</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
        <script type="text/javascript">
            function required(){
                jAlert("Atleast one witness required.","Contract Agreement", function(RetVal) {
                });
                return false;
            }
        </script>
    </head>
    <%
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }

                int contractSignId = 0;
                if (request.getParameter("contractSignId") != null) {
                    contractSignId = Integer.parseInt(request.getParameter("contractSignId"));
                }
                
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    //userId = Integer.parseInt(session.getAttribute("userId").toString());
                    userId = session.getAttribute("userId").toString();
                    issueNOASrBean.setLogUserId(userId);
                }
                //dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat simpl = new SimpleDateFormat("dd/MM/yyyy");
                int counter;
                int noaIssueId = 0;
                if (request.getParameter("noaIssueId") != null) {
                    noaIssueId = Integer.parseInt(request.getParameter("noaIssueId"));
                }
    %>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">
            <div class="pageHead_1">Contract  Agreement
                <span class="c-alignment-right">
                    <%
                        if (request.getParameter("ro") != null) {
                    %>        
                        <a href="repeatOrderMain.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
                    <%}else{%>
                        <a href="NOAListing.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
                    <%}%>
                </span>
            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <form id="frmContractAgreement" name="frmContractAgreement" action="" method="post">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                    <%
                                for (TblContractSign details : issueNOASrBean.getDetailsContractSign(contractSignId)) {
                    %>
                    <tr>
                        <td align="left" valign="middle" class="ff">Last Date of Signing of Agreement :</td>
                        <td align="left" valign="middle"><%=DateUtils.gridDateToStr(details.getLastDtContractDt()).toString().split(" ")[0] %>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Date  of Signing of Agreement :</td>
                        <td align="left" valign="middle">
                            <%=DateUtils.gridDateToStrWithoutSec(details.getContractSignDt())%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Witnesses  Name &amp; Address From PE:</td>
                        <td align="left" valign="middle">
                            <table width="70%" id="dataTable" cellpadding="0" cellspacing="0" class="formStyle_1">
                                <% String nameAddPE = details.getWitnessInfo();
                                                                    String[] splitnameAddPE = nameAddPE.split("@");
                                                                    for (counter = 0; counter < splitnameAddPE.length; counter++) {
                                %>
                                <tr>
                                    <%if (counter == 0 || counter == 1) {
                                    %>
                                    <td class="t-align-left" width="510px">
                                        <%=splitnameAddPE[counter]%>
                                    </td>
                                </tr>
                                <% }
                                                                    }%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Witnesses  Name &amp; Address From Bidder/Consultant:</td>
                        <td align="left" valign="middle">
                            <table width="70%" id="dataTable" cellpadding="0" cellspacing="0" class="formStyle_1">
                                <% String nameAddTenderer = details.getWitnessInfo();
                                                                    String[] splitnameAddTenderer = nameAddTenderer.split("@");
                                                                    for (counter = 0; counter < splitnameAddTenderer.length; counter++) {
                                                                        if (counter == 0 || counter == 1) {
                                %>
                                <tr>
                                    <% } else {%>
                                    <td class="t-align-left" width="510px">
                                        <%=splitnameAddTenderer[counter]%>
                                    </td>
                                </tr>
                                <% }
                                                                    }%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Place of Signing Agreement :</td>
                        <td align="left" valign="middle">
                            <%=details.getContractSignLocation()%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" class="ff">Publish Agreement on website :</td>
                        <td align="left" valign="middle"><%=details.getIsPubAggOnWeb()%></td>
                    </tr>
                    <% }%>
                </table>
                <div id="dataDoc">
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="4%" class="t-align-center">Sl. No.</th>
                            <th class="t-align-center" width="30%">File Name</th>
                            <th class="t-align-center" width="46%">File Description</th>
                            <th class="t-align-center" width="10%">File Size <br />
                                (in KB)</th>
                            <th class="t-align-center" width="10%">Action</th>
                            <!--                            <th class="t-align-center" width="18%">Action</th>-->
                        </tr>
                        <%

                                    int docCnt = 0;
                                    TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                    makeAuditTrailService.generateAudit(objAuditTrail,contractSignId, "ContractId",EgpModule.Contract_Signing.getName(), "View Contract by PE","");         
                                    /*Listing of data*/
                                    for (SPTenderCommonData sptcd : tenderCS.returndata("contractSignDoc", String.valueOf(noaIssueId), null)) {
                                        docCnt++;
                        %>
                        <tr>
                            <td class="t-align-left"><%=docCnt%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                            <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                            <td class="t-align-left">
                                <a href="<%=request.getContextPath()%>/ServletContractSignDoc?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&noaIssueId=<%=noaIssueId%>&tenderId=<%=tenderId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
<!--                                &nbsp;
                                <a href="<-%=request.getContextPath()%>/ServletContractSignDoc?&docName=<-%=sptcd.getFieldName1()%>&docId=<-%=sptcd.getFieldName4()%>&noaIssueId=<-%=noaIssueId%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>-->
                            </td>
                        </tr>

                        <%   if (sptcd != null) {
                                            sptcd = null;
                                        }
                                    }%>
                        <% if (docCnt == 0) {%>
                        <tr>
                            <td colspan="5" class="t-align-center">No records found.</td>
                        </tr>
                        <%}%>
                    </table>
                </div>
            </form>
        </div>
        <!--            </div>-->
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        <!--        </div>-->
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<%
            issueNOASrBean = null;
%>