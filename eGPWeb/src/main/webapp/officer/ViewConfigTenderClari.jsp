<%--
    Document   : ConfigTenderClari
    Created on : Jun 3, 2011, 5:47:56 PM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.model.table.TblTenderPostQueConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderPostQueConfigService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Clarification on Tender Configuration Detail</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <!--jalert -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <title>View Clarification on Tender Configuration Detail</title>
        <script type="text/javascript">
            /*For jquery calander*/
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
    </head>
    <body>
        <%
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
        %>
        <%
                TenderPostQueConfigService tenderPostQueConfigService = (TenderPostQueConfigService) AppContext.getSpringBean("TenderPostQueConfigService");
                if (session.getAttribute("userId") != null) {
                        tenderPostQueConfigService.setLogUserId(session.getAttribute("userId").toString());
                }
                int tenderID =0;
                if(request.getParameter("tenderId")!=null)
                {
                    tenderID = Integer.parseInt(request.getParameter("tenderId"));
                }
                List<TblTenderPostQueConfig> listt = tenderPostQueConfigService.getTenderPostQueConfig(tenderID);
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1"><div>
            <div class="pageHead_1">View Clarification on Tender Configuration Detail
            <span style="float:right;"><a href="Notice.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back To Dashboard</a></span>
            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div class="dividerBorder t_space" >&nbsp;</div>      </div>
            
            <form id="configtenderclari" name="configtenderclarification" method="post" action='<%=request.getContextPath()%>/UserPrefSrBean'>                
                <div class="tableHead_1 t_space">Clarification Details
                </div>
                <div class="contactUs_border t_space">
                <table  cellspacing="10" cellpadding="0" widthe="100%"class="tableView_1 ">

<!--                <tr>
                    <td class="ff">Clarification on Tender/Proposal is to be allowed :</td>
                    <td><%=listt.get(0).getIsQueAnsConfig()%>
                    </td>
                </tr>-->
                <%if("yes".equalsIgnoreCase(listt.get(0).getIsQueAnsConfig().toString())){%>
                <tr>
                    <td class="ff">Last Date and Time for posting of query :</td>                    
                    <td><%=DateUtils.gridDateToStrWithoutSec(listt.get(0).getPostQueLastDt())%>
                    </td>
                </tr>
                <%}%>
                </table></div>
            </form> 
            </div>
            
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

