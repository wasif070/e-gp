<%-- 
    Document   : APPWorkflowView
    Created on : Nov 11, 2010, 7:50:19 PM
    Author     : rishita,Rajesh,Swati
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppExceptionHandler"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.utility.SHA1HashEncryption"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.APPService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="java.util.List,java.util.ArrayList,com.cptu.egp.eps.model.table.TblAppPackages" %>



<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<jsp:useBean id="appserviceSrBean" class="com.cptu.egp.eps.web.servicebean.APPSrBean" />
<jsp:useBean id="appServlet" class="com.cptu.egp.eps.web.servlet.APPServlet" />


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>App Workflow View</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>


        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
<!--
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
-->
        
        <script type="text/javascript">
/*          $(document).ready(function() {
                $("#frmremark").validate({
                    rules: {
                        txtremark: { required: true}
                    },
                    messages: {
                        txtremark: { required: "<div class='reqF_1'>Please enter Comments</div>"}
                    }
                })
            });
*/
            function validate(){
                if($.trim(document.getElementById("txtremark").value)==""){
                    document.getElementById("errMsg").innerHTML = "</br>Please enter Comments";
                    return false;
                }
                return true;
            }
        </script>

        <%
                    Logger LOGGER = Logger.getLogger("APPWorkflowView.jsp");
                    //System.out.println("--------------------------------------------APP Process Start");
                    int appid = 0;
                    String pkgStatus = "";
                    String hdnPkgPdf = request.getParameter("hdnPkgPdf");
                    String status = "Pending";
                    
                    if (!"".equals(request.getParameter("action")) && request.getParameter("action") != null) {
                        status = "Pending";
                    }

                    if (!request.getParameter("appid").equals("")) {
                        appid = Integer.parseInt(request.getParameter("appid"));
                    }
                    if(request.getParameter("pkgstatus")!= null){
                        pkgStatus = request.getParameter("pkgstatus");
                    }
                    String logUserId="0";
                    if(session.getAttribute("userId")!=null){
                        logUserId=session.getAttribute("userId").toString();
                    }
                    appserviceSrBean.setLogUserId(logUserId);
                    appServlet.setLogUserId(logUserId);
                                       
                    if (request.getParameter("btnsubmit") != null)
                    {
                            int countapp = 0; String countPkgStatus = "";
                            if(request.getParameter("checkapp")!= null){
                               countapp = Integer.parseInt(request.getParameter("checkapp"));
                             }
                             if(request.getParameter("checkpkgStatus")!= null){
                               countPkgStatus = request.getParameter("checkpkgStatus");
                                if(countPkgStatus.equalsIgnoreCase("BeingRevised")){
                                   String count =  appServlet.CountAPPRevise(countapp);
                                   System.out.println("count increased sucessfully"+count);
                                }
                             }
                        WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                        short eventid = 1;
                        int objectid = appid;
                        short activityid = 1;
                        int childid = appid;
                        int initiator = 0;
                        int approver = 0;
                        boolean evntexist = false;
                        boolean iswfLevelExist = false;
                        String donor = "";
                        donor = workFlowSrBean.isDonorReq(String.valueOf(eventid),
                        Integer.valueOf(objectid));
                        String[] norevs = donor.split("_");
                        int norevrs = -1;
                        String strrev = norevs[1];
                        if(!strrev.equals("null")){
                           norevrs = Integer.parseInt(norevs[1]);
                        }
                        evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
                        List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);

                         if (tblWorkFlowLevelConfig.size() > 0) {
                             Iterator twflc = tblWorkFlowLevelConfig.iterator();
                             iswfLevelExist = true;
                             while (twflc.hasNext()) {
                                 TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                 TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                 if (workFlowlel.getWfRoleId() == 1) {
                                     initiator = lmaster.getUserId();
                                 }
                                 if (workFlowlel.getWfRoleId() == 2) {
                                     approver = lmaster.getUserId();
                                 }
                             }
                         }

                        try{
                            String reqURL = request.getRequestURL().toString();
                            String folderName = pdfConstant.APPVIEW;
                            String strpkgNo = "";

                            if(request.getParameter("hdnpkgId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnpkgId"))){
                                strpkgNo = request.getParameter("hdnpkgId");
                            }

                            HandleSpecialChar hsc = new HandleSpecialChar();
                            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                            APPService appService = (APPService) AppContext.getSpringBean("APPService");
                            
                            List<TblAppPackages> listPackages = appserviceSrBean.getCPVCodeByappId(appid);
                            CommonMsgChk commonMsgChk = null;
                            CommonMsgChk commonMsgChk1 = null;
                            String[] arrCPVCode;
                            StringBuilder condCPVCode = new StringBuilder("(");
                            String condFinal = "";
                           String CPVCode = "";
                            String[] pkgNo = new String[listPackages.size()];
                            String[] pkgDesc = new String[listPackages.size()];
                            int[] pkgId = new int[listPackages.size()];
                            int i=0;
                            if(listPackages.size() > 0){
                                for(TblAppPackages Package:listPackages){
                                    CPVCode = CPVCode + Package.getCpvCode() + ";";
                                    pkgNo[i]  = Package.getPackageNo().toString();
                                    pkgDesc[i] = Package.getPackageDesc();
                                    pkgId[i] = Package.getPackageId();
                                    i++;
                                }
                                CPVCode = CPVCode.substring(0, CPVCode.length()-1);
                            }

                            arrCPVCode = CPVCode.split("\\;");
                            if(arrCPVCode.length > 0){
                                for(String code:arrCPVCode){
                                   condCPVCode.append("specialization like \'%"+code.toString()+"%\' or ");
                                }
                                condFinal = condCPVCode.toString();
                                condFinal = condFinal.substring(0,condFinal.length()-4);
                            }
                            condFinal = condFinal + ")";
                            
                            List<CommonSPReturn> commonSPReturn = null;
                            if(!"".equalsIgnoreCase(strpkgNo))
                                    {
                                        commonSPReturn= appService.publishApp("publishApp", Integer.parseInt(request.getParameter("appid")), strpkgNo);
                                    }
                            else
                                    {
                                        if(listPackages.size() > 0)
                                        {
                                            for(TblAppPackages Package:listPackages)
                                            {
						if((initiator == approver && norevrs == 0) || (Package.getWorkflowStatus()!=null && Package.getWorkflowStatus().equalsIgnoreCase("approved")))
                                                {
                                                    commonSPReturn= appService.publishApp("publishApp", Integer.parseInt(request.getParameter("appid")), Package.getPackageNo());
                                                }
                                            }
                                        }
                                    }
                            if(commonSPReturn.size()>0){
                                if(commonSPReturn.get(0).getFlag()==true)
                                    response.sendRedirect("APPDashboard.jsp?appID=" + request.getParameter("appid") + "&from=msg");
                                else
                                    response.sendRedirect("APPDashboard.jsp?appID=" + request.getParameter("appid") + "&msg=error");
                            }
                            //comment out by ahsan. 
                            //if (commonMsgChk.getFlag().equals(true)) 
                            //{
                            /*start by ahsan
                            String table = "", updateString = "", whereCondition = "";
                                table = "tbl_AppMaster";
                                updateString = "appStatus='Approved'";
                                whereCondition = "appId=" + request.getParameter("appid");

                                CommonXMLSPService commonXMLSPService1 = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                commonXMLSPService1.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                
                                // Added for Audit Trail
                                commonXMLSPService.setModuleName(EgpModule.APP.getName());
                                String auditValues[]=new String[4];
                                auditValues[0]="appId";
                                auditValues[1]= appid+"";
                                auditValues[2]="Publish APP";
                                auditValues[3]= request.getParameter("txtremark");
                                commonMsgChk1 = commonXMLSPService1.insertDataBySP("updatetable", "tbl_AppMaster", updateString, whereCondition,auditValues).get(0);

                                if (commonMsgChk1.getFlag().equals(true)) 
                                {
                                    auditValues[2]="Publish Package";
                                    
                                    table = "tbl_AppPackages";
                                    updateString = "appStatus='Approved',workflowStatus='Approved' ";
                                    CommonMsgChk commonMsgChk2 = null;
                                    CommonXMLSPService commonXMLSPService2 = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    if(!"".equalsIgnoreCase(strpkgNo))
                                    {
                                        auditValues[3] = auditValues[3] +" </BR> Package No: "+strpkgNo;
                                        whereCondition = "appId=" + request.getParameter("appid") + " and packageNo = '"+ strpkgNo + "' and Appstatus in ('BeingRevised','Pending')";
                                        commonXMLSPService2.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                        commonMsgChk2 = commonXMLSPService2.insertDataBySP("updatetable", "tbl_AppPackages", updateString, whereCondition ,auditValues).get(0);

                                    }else
                                    {
                                        if(listPackages.size() > 0)
                                        {
                                            for(TblAppPackages Package:listPackages)
                                            {
						if((initiator == approver && norevrs == 0) || (Package.getWorkflowStatus()!=null && Package.getWorkflowStatus().equalsIgnoreCase("approved")))
//                                                if((initiator == approver && norevrs == 0) || (Package.getWorkflowStatus()!=null && (Package.getWorkflowStatus().equalsIgnoreCase("approved")||Package.getWorkflowStatus().equalsIgnoreCase("Conditional Approval"))))
                                                {
                                                    whereCondition = "appId=" + request.getParameter("appid") + " and packageNo = '"+ Package.getPackageNo() + "' and Appstatus in('Pending','BeingRevised')";
                                                    commonMsgChk2 = commonXMLSPService2.insertDataBySP("updatetable", "tbl_AppPackages", updateString, whereCondition,auditValues).get(0);
                                                }
                                            }
                                        }
                                    }
                                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    tenderCommonService.removeTempAppData(request.getParameter("appid"));
                                  //  System.out.println("--------------------------------------APP Publish Successfully");
                                    //Code is commented due to CPTU requirement by Dohatec
                                  //  if (commonMsgChk2.getFlag()) {
                                  //      APPService appService = (APPService) AppContext.getSpringBean("APPService");
                                    //    List<CommonAppData> commonAppData = appService.getUserDetailsForEmail("getAppEmailId", condFinal,"");
                                     //   for(CommonAppData appData:commonAppData){
                                         //   userRegisterService.contentAdmMsgBox(appData.getFieldName1().toString(), XMLReader.getMessage("emailIdNoReply"), "e-GP: APP - New Package published of your choice.", msgBoxContentUtility.contAppPublish(pkgNo, pkgDesc, pkgId, appid));
                                            
                                      //  }
                                  //  }
                                }
                                i=0;
                                String dtXml = "";

                                System.out.println("*-*-*-*-*-*-*-* APP AuditTrail Start *-*-*-*-*-*-*-*");
                                if(logUserId !=null && (!"0".equals(logUserId)) &&  listPackages.size() > 0){
                                    for(TblAppPackages Package:listPackages){
                                        pkgNo[i]  = Package.getPackageNo().toString();
                                        pkgDesc[i] = Package.getPackageDesc();
                                        pkgId[i] = Package.getPackageId();

                                        dtXml = "<root><tbl_AppAuditTrial appId=\"" + request.getParameter("appid") + "\" childId=\"" + Package.getPackageId() + "\"  actionDate=\"" + format.format(new Date()) + "\" actionBy=\"" + logUserId + "\" "
                                        + "action=\"Approved\" signature=\"" + " " + "\" remarks=\"" + SHA1HashEncryption.encodeStringSHA1(hsc.handleSpecialChar(request.getParameter("txtremark").toString())) + "\"/></root>";

                                        System.out.println("*-*-*-*-*-*-*-* APP Audit Data "+dtXml);
                                        commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_AppAuditTrial", dtXml, "").get(0);
                                        System.out.println("*-*-*-*-*-*-*-* APP Audit Done "+commonMsgChk.getMsg());
                                        i++;
                                    }

                                   // CPVCode = CPVCode.substring(0, CPVCode.length()-1);
                                }
                                System.out.println("*-*-*-*-*-*-*-* APP AuditTrail Done *-*-*-*-*-*-*-*");
                                //} else {
                                //    out.println(commonMsgChk.getMsg());
                                //}
                                */ //ahsan end
                            
                            
                        }catch(Exception ex){
                            ex.printStackTrace();
                           AppExceptionHandler appExceptionHandler = new AppExceptionHandler();
                           appExceptionHandler.stack2string(ex);
                        }
                    }

                    String sts = "Pending";
                    if(request.getParameter("action")!=null && "pub".equalsIgnoreCase(request.getParameter("action"))){

                        if("true".equalsIgnoreCase(request.getParameter("isIniAppSame")))
                            {
                                sts = "appiniappsame";
                             }
                             else
                            {
                             sts = "publish";
                             }
                    }
                    String pkgno = "";
                    if(request.getParameter("action")!=null && "inisame".equalsIgnoreCase(request.getParameter("action"))){
                        sts = "isiniappsame";
                        if(request.getParameter("pkgno")!=null && !"".equalsIgnoreCase(request.getParameter("pkgno"))){
                            pkgno = request.getParameter("pkgno");
                        }
                    }
        %>
    </head>
    <script type="text/javascript">
        function loadFirtTime(){
            $.post("<%=request.getContextPath()%>/APPWorkflowServlet", {pkgNo: "<%=pkgno%>",estimatedCost:0 ,status:"<%=sts%>",procurementNature:" ",procurementType:" ",appid:<%=appid%>},  function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                
            });
        }
    </script>
    <body onload="loadFirtTime();">
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                                <!--Page Content Start-->
                                <% java.util.List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
                                            appListDtBean = appServlet.getAPPDetails("App", request.getParameter("appid"), "");%>

                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr valign="top">
                                        <td>
                                            <% String isfileproce = request.getParameter("isfileproc");  %>
                                            <div class="pageHead_1">
                                               <% if(isfileproce != null){
                                                   if(isfileproce.equals("Yes"))
                                                     out.print("View APP");  %>
                                                     <% }else{
                                                      out.print(" Publish APP");
                                                      %>
                                               <span class="c-alignment-right">
                                                    <a href="javascript:void(0);" onclick="printElem({ overrideElementCSS: false,leaveOpen: true, printMode: 'popup' })"   id="print" class="action-button-view"> Print</a>
                                                    <a href="APPDashboard.jsp?appID=<%=request.getParameter("appid")%>" class="action-button-goback"> Go Back To Dashboard</a>
                                                </span>
                                                <%
                                                      }
                                                   %>
                                            </div>
                                            <div>&nbsp;</div>
                                            <div id="print_area">
                                            <form method="POST" id="frmAPPDashboard" action="">
                                                <div class="tableHead_1 t_space">APP Information Bar :</div>
                                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                    <tr><% for (CommonAppData details : appListDtBean) {%>
                                                        <td width="180" class="ff">APP ID	: </td>
                                                        <td width="287"><%=details.getFieldName1()%></td>
                                                        <td width="180" class="ff">Letter Ref. No. : </td>
                                                        <td width="287"><%=details.getFieldName3()%></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff">Financial Year : </td>
                                                        <td><%=details.getFieldName4()%></td>
                                                        <td class="ff">Budget Type : </td>
                                                        <td><%=details.getFieldName5()%></td>
                                                    </tr>

                                                    <tr>
                                                        <td class="ff">Project Name (If Applicable) : </td>
                                                        <td><%=details.getFieldName6()%></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <% }%>

                                                </table>
                                                <form id="frmappDashboadtbl" <%--action="<%=request.getContextPath()%>/APPFileUploadServlet"--%> name="frmappDashboadtbl" method="post">
                                                    <table width="100%" cellspacing="0" id="resultTable" class="tableList_1" >
                                                        <tr>
                                                            <th width="4%" class="t-align-center">Sl. <br/>No.</th>
                                                            <th width="32%" class="t-align-left">Package No. and <br />Package Description</th>
                                                            <th width="16%" class="t-align-left">Procurement Category and <br />Procurement Type</th>
                                                            <th width="15%" class="t-align-center">Package Est. Cost <br />(In Nu.)</th>
                                                            <th width="12%" class="t-align-center" style="display: none">Estimated Cost</th>
                                                            <th width="17%" class="t-align-center noprint">Action</th>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </form>
                                </div>
                                    </td>
                                    </tr>
                                    <% String pub = request.getParameter("action");
                                                if (pub != null) {
                                    %>
                                    <tr>
                                        <td>
                                            <form id="frmremark" name="frmremark" action="APPWorkflowView.jsp?appid=<%=request.getParameter("appid")%>" method="POST">
                                                <input id="hdnPkgPdf" name="hdnPkgPdf" type="hidden"/>
                                                <input type="hidden" name="hdnpkgId" id="hdnpkgId" value="<%=pkgno%>" />
                                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                    <tr>
                                                        <td width="10%" class="t-align-left">Comments : <span class="mandatory">*</span></td>
                                                        <td width="90%"><textarea cols="20" id="txtremark" name="txtremark" rows="3" style="width: 400px" class="formTxtBox_1" onblur="if(document.getElementById('txtremark').value != ''){document.getElementById('errMsg').innerHTML = '';} "></textarea>
                                                            <span id="errMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                        
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="ff"></td>
                                                        <td>
                                                            <label class="formBtn_1">
                                                                <input type="submit" name="btnsubmit" id="btnsubmit" value="Submit" onClick="return validate();"/>
                                                                
                                                            </label>
                                                            <input type="hidden" name="checkapp" value="<%=appid%>"/>
                                                            <input type="hidden" name="checkpkgStatus" value="<%=pkgStatus%>"/>
                                                            <script type="text/javascript">
                                                                function printElem(options){
                                                                    $('#print_area').printElement(options);
                                                                }
                                                            </script>
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </form>
                                        </td></tr>
                                        <% }%>
                                    </table>
                                </div>
                                    </td>
                                    </tr>
                                    
                                    </table>
                                    <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                                </div>
</div>
</body>
<% appServlet = null; %>
<script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabApp");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
</script>
</html>