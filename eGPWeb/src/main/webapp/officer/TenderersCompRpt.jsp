<%-- 
    Document   : TenderersCompRpt
    Created on : Jan 1, 2011, 3:43:00 PM
    Author     : Swati
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService,com.cptu.egp.eps.web.utility.AppContext" %>


<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Technical Evaluation Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <%
                    String tendid = "";
                    String s_lotId = "0";
                    String s_chkLotId = "0";
                    if (request.getParameter("tenderid") != null && !"null".equalsIgnoreCase(request.getParameter("tenderid"))) {
                        tendid = request.getParameter("tenderid");
                    } else if (request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId"))) {
                        tendid = request.getParameter("tenderId");
                    }

                    if (request.getParameter("lotId") != null) {
                        s_lotId = request.getParameter("lotId");
                        s_chkLotId = request.getParameter("lotId");
                    }
                     boolean isNotPDF = true;
                    if (request.getParameter("isPDF") != null && "true".equalsIgnoreCase(request.getParameter("isPDF"))) {
                        isNotPDF = false;
                    }
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    boolean isREOI = commonService.getEventType(tendid).toString().equalsIgnoreCase("REOI");
                    pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                     pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                     pageContext.setAttribute("userId", 0);
                    boolean isT1 = false;
                    CommonSearchDataMoreService moreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> envDataMores = moreService.geteGPData("GetTenderEnvCount", tendid);
                    if (envDataMores != null && (!envDataMores.isEmpty())) {
                        if (envDataMores.get(0).getFieldName2().equals("1")) {
                            //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                            isT1 = true;
                        }
                    }
        %>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%if(isNotPDF){%>
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            </div>
            <%}%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div id="print_area">
                    <div class="pageHead_1">Technical Evaluation Report
                        <%if(isNotPDF){%>
                        <span style="float:right;">
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                            <span id="svpdft1">
                                <a class="action-button-savepdf" id="saveASPDF" href="<%=request.getContextPath()%>/TorRptServlet?tenderId=<%=tendid%>&action=pdfT1">Save As PDF</a>
                            </span>
                            <a id="goBack" href="Evalclarify.jsp?tenderId=<%=tendid%>&st=rp" class="action-button-goback">Go back </a>
                        </span>
                        <%}%>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div class="tabPanelArea_1">
                        <%                pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                                    pageContext.setAttribute("comType", request.getParameter("comType"));

                                    if (isTenPackageWise) {
                                        s_lotId = "0";
                                    }
                                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                    List<SPCommonSearchData> lstdata = commonSearchService.searchData("evalBidderStatusRpt", tendid, s_lotId, null, null, null, null, null, null, null);


                                    List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("GetLotPkgDetail", tendid, s_lotId);
                                    String lotPkgNo = null;
                                    String lotPkgDesc = null;
                                    if ("0".equals(s_lotId)) {
                                        lotPkgNo = "Package No.";
                                        lotPkgDesc = "Package Description";
                                    } else {
                                        lotPkgNo = "Lot No.";
                                        lotPkgDesc = "Lot Description";
                                    }
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <td width="15%" class="t-align-left ff"><%=lotPkgNo%> :</td>
                                <td width="85%" class="t-align-left"><%=tenderLotList.get(0).getFieldName1()%>
                                    <input type="hidden" value="<%=tenderLotList.get(0).getFieldName1()%>" name="lotPckNoTxt"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff"><%=lotPkgDesc%> :</td>
                                <td class="t-align-left"><%=tenderLotList.get(0).getFieldName2()%>
                                    <input type="hidden" value="<%=tenderLotList.get(0).getFieldName2()%>" name="lotPckDescTxt"/>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <%if ("0".equalsIgnoreCase(s_lotId) && "0".equalsIgnoreCase(s_chkLotId)) {%>
                            <tr>
                                <th colspan="5" class="t-align-left">
      	Evaluation Report of  all Tenderers/Consultants
                                </th>
                            </tr>
                            <tr class="t_space">
                                <th width="5%" class="t-align-center">Rank</th>
                                <th width="50%" class="t-align-left">Consultant's Name</th>
                                <%if (!isREOI) {%>
                                <th width="10%" class="t-align-center">Passing Points</th>
                                <th width="10%" class="t-align-center">Consultant's Points</th>
                                <%}%>
                                <th width="10%" class="t-align-center">Result</th>
                            </tr>
                            <%} else {%>
                            <tr>
                                <th colspan="2" class="t-align-left">
      	Evaluation Report of  all Tenderers/Consultants
                                </th>
                            </tr>
                            <tr class="t_space">
                                <th width="82%" class="t-align-left">Consultant's Name</th>
                                <th width="18%" class="t-align-left">Status</th>
                            </tr>
                            <%}%>
                            <%
                                        int i = 0;
                                        for (SPCommonSearchData data : lstdata) {
                                            i++;
                            %>
                            <tr>
                                <%if ("0".equalsIgnoreCase(s_lotId) && "0".equalsIgnoreCase(s_chkLotId)) {%>
                                <td class="t-align-center"><%= i%></td>
                                <td class="t-align-left"><%= data.getFieldName1()%><%=((isT1 && i == (Integer.parseInt(data.getFieldName6()))) ? "<span style='color:green;'> <b>(Current Highest Scorer)</b></span>" : "")%></td>
                                <%if (!isREOI) {%>
                                <td class="t-align-center"><%= data.getFieldName4()%></td>
                                <td class="t-align-center"><%= data.getFieldName3()%></td>
                                <%}%>
                                <td class="t-align-center"><%= isREOI ? data.getFieldName5().equals("Pass") ? "Qualified" : "Disqualified" : data.getFieldName5()%></td>
                                <%} else {%>
                                <td class="t-align-left"><%= data.getFieldName1()%></td>
                                <%
                                    String s_status = "Technically Responsive";
                                    if ("Technically Unresponsive".equalsIgnoreCase(data.getFieldName2())) {
                                        s_status = "Technically Non-responsive";
                                    }
                                %>
                                <td class="t-align-left"><%= s_status%></td>
                                <%}%>
                            </tr>
                            <%
                                            if (data != null) {
                                                data = null;
                                            }
                                        }
                            %>
                        </table>
                    </div>
                </div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%if(isNotPDF){%>
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <%}%>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#print").click(function() {
                printElem({ leaveOpen: true, printMode: 'popup' });
            });
        });
        function printElem(options){
            $('#print').hide();
            $('#saveASPDF').hide();
            $('#goBack').hide();
            //$('#repSaveBtn').hide();
            $('#print_area').printElement(options);
            $('#print').show();
            $('#saveASPDF').show();
            $('#goBack').show();
            //$('#repSaveBtn').show();
        }
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<%
            if (lstdata != null) {
                lstdata.clear();
                lstdata = null;
            }
%>

