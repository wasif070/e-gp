<%-- 
    Document   : CreateTenderForm
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="tenderFrmSrBean" class="com.cptu.egp.eps.web.servicebean.TenderFormSrBean" />
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<jsp:useBean id="procurementNatureBean" class="com.cptu.egp.eps.web.servicebean.OfficerTabSrBean" scope="page"/>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9"/>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    int tenderId = 0;
                    String strProcNature = "";
                    int sectionId = 0;
                    int formId = 0;
                    int pkgOrLotId = -1;
                    boolean isEdit = false;
                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    if (request.getParameter("sectionId") != null) {
                        sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    }
                    if (request.getParameter("formId") != null) {
                        formId = Integer.parseInt(request.getParameter("formId"));
                        isEdit = true;
                    }
                    if (request.getParameter("porlId") != null) {
                        pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                    }
                    
                    //Corrigendum Created and its status is Pending
                    boolean corriCrtNPending = false;
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> lstCorri = tenderCommonService.returndata("getCorrigenduminfo", tenderId + "", null);
                    if (lstCorri != null) {
                        if (lstCorri.size() > 0) {
                            for (SPTenderCommonData data : lstCorri) {
                                if (data.getFieldName4().equalsIgnoreCase("pending")) {
                                    corriCrtNPending = true;
                                }
                            }
                        }
                    }
                    List<TblTenderDetails> tblTenderDetail = procurementNatureBean.getProcurementNature(tenderId);
                    strProcNature = tblTenderDetail.get(0).getProcurementNature();
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("isPackageLotForForm", String.valueOf(tenderId), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    boolean isPck = false;
                    if(!packageLotList.isEmpty() && packageLotList.size() != 0){
                        isPck = true;
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            <%if (isEdit) {%>
            Edit Tender form
            <%} else {%>
            Create Tender form
            <%}%>
        </title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        

        <script type="text/javascript">
            $(document).ready(function(){
            
                $("#frmCreateForm").validate({

                    rules:{
                        formName:{required:true},
                        noOfTables:{required:true,digits:true,range:[1,99]},
                        multipleTimeFill:{required:true},
                        boqForm:{required:true},
                        isMandatory:{required:true},
                        rdoTemplateIdFromType:{required:true}
                    },

                    messages:{
                        formName:{required:"<div class='reqF_1'>Please enter Form Name</div>"},
                        noOfTables:{required:"<div class='reqF_1'>Please enter No. of Tables required</div>",
                            range:"<div class='reqF_1'>Please enter value between 1 and 99</div>",
                            digits:"<div class='reqF_1'>Please enter digits only</div>"},
                        multipleTimeFill:{required:"<div class='reqF_1'>Please select Form to be filled Multiple Times</div>"},
                        boqForm:{required:"<div class='reqF_1'>Please select Is BOQ Form</div>"},
                        isMandatory:{required:"<div class='reqF_1'>Please select Is Mandatory form</div>"},
                        rdoTemplateIdFromType:{required:"<div class='reqF_1'>Please select Type of Form</div>"}
                    }
                });
            });

            function setTemplateIdOfCanceledForm(rdoTemplIdObj){
                var objTemplId = document.getElementById("templateFormId");
                var valTemplId = objTemplId.value;
                if(confirm("Are you sure you want this form of type " + rdoTemplIdObj.title)){
                    objTemplId.value = rdoTemplIdObj.value;
                }else{
                    rdoTemplIdObj.checked = false;
                    objTemplId.value = valTemplId;
                }
            }
        </script>


    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->
            <%
                        String frmName = "";
                        StringBuffer frmHeader = new StringBuffer();
                        StringBuffer frmFooter = new StringBuffer();
                        byte noOfTable = 0;
                        String isMultipleFill = "";
                        String isBOQ = "";
                        int lotSelection = 0;
                        String isMandatory = "";
                        int templateFormId = 0;
                        if (isEdit) {
                            List<com.cptu.egp.eps.model.table.TblTenderForms> frm = tenderFrmSrBean.getFormDetail(formId);
                            if (frm != null) {
                                if (frm.size() > 0) {
                                    frmName = frm.get(0).getFormName();
                                    frmHeader.append(frm.get(0).getFormHeader());
                                    frmFooter.append(frm.get(0).getFormFooter());
                                    noOfTable = frm.get(0).getNoOfTables();
                                    isMultipleFill = frm.get(0).getIsMultipleFilling();
                                    isBOQ = frm.get(0).getIsPriceBid();
                                    lotSelection = frm.get(0).getPkgLotId();
                                    isMandatory = frm.get(0).getIsMandatory();
                                    templateFormId = frm.get(0).getTemplateFormId();
                                }
                                frm = null;
                            }
                        }
            %>
            <form action="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=formCreation" method="post" id="frmCreateForm">
                <input type="hidden" name="tenderId" id="templateId" value="<%= tenderId%>" />
                <input type="hidden" name="sectionId" id="sectionId" value="<%= sectionId%>" />
                <input type="hidden" name="porlId" id="porlId" value="<%= pkgOrLotId%>" />
                <%if (isEdit) {%>
                <input type="hidden" name="formId" id="formId" value="<%= formId%>" />
                <input type="hidden" name="templateFormId" id="templateFormId" value="<%= templateFormId%>" />
                <input type="hidden" name="porlId" id="porlId" value="<%= pkgOrLotId%>" />
                <%}%>

                <%if (corriCrtNPending) {%>
                <input type="hidden" name="corriCrtNPending" id="corriCrtNPending" value="<%= corriCrtNPending%>" />
                    <%if ("Services".equalsIgnoreCase(strProcNature)) {%>
                        <input type="hidden" name="strProcNature" id="strProcNature" value="<%= strProcNature %>" />
                        <input type="hidden" name="templateFormId" id="templateFormId" value="<%= templateFormId %>" />
                    <%}%>
                <%}%>
                <div class="contentArea_1">
                    <div class="pageHead_1">
                        <%if (isEdit) {%>
                        Edit Form
                        <%} else {%>
                        Create Form
                        <%}%>
                        <span class="c-alignment-right">
                            <%if (pkgOrLotId == -1) {%>
                            <a href="TenderDocPrep.jsp?tenderId=<%= tenderId%>" title="Tender/Proposal Doc. Preparation" class="action-button-goback">Tender Doc. Preparation</a>
                            <%} else {%>
                            <a href="TenderDocPrep.jsp?tenderId=<%= tenderId%>&porlId=<%= pkgOrLotId%>" title="Tender/Proposal Doc. Preparation" class="action-button-goback">Tender Doc. Preparation</a>
                            <%}%>
                        </span>
                    </div>
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>
                            <td style="font-style: italic" colspan="2" class="" align="right">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>
                        <tr>
                            <td width="20%" class="ff">Form Name : <span>*</span></td>
                            <%if (isEdit) {%>
                            <td width="80%"><textarea name="formName" rows="3" class="formTxtBox_1" id="formName" <%--onkeypress="checkKey(event);"--%> style="width:500px;"><%= frmName.replace("<br/>", "\n") %></textarea></td>
                            <%} else {%>
                            <td><textarea name="formName" rows="3" class="formTxtBox_1" id="formName" <%--onkeypress="checkKey(event);"--%> style="width:500px;"></textarea></td>
                            <%}%>
                        </tr>
                        <tr>
                            <td class="ff">Form Header :</td>
                            <td>
                                <%if (isEdit) {%>
                                <textarea cols="80" id="formHeader" name="formHeader" rows="10"><%= frmHeader%></textarea>
                                <%} else {%>
                                <textarea cols="80" id="formHeader" name="formHeader" rows="10"></textarea>
                                <%}%>
                                <script type="text/javascript">
                                    CKEDITOR.replace( 'formHeader',
                                    {
                                        //fullPage : false
                                    });
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Form Footer : </td>
                            <td>
                                <%if (isEdit) {%>
                                <textarea cols="80" id="formFooter" name="formFooter" rows="10"><%= frmFooter%></textarea>
                                <%} else {%>
                                <textarea cols="80" id="formFooter" name="formFooter" rows="10"></textarea>
                                <%}%>
                                <script type="text/javascript">
                                    CKEDITOR.replace( 'formFooter',
                                    {
                                        //fullPage : false
                                    });
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">No. of Tables :  <%if (!isEdit) {%><span>*</span><%}%></td>
                            <td>
                                <%if (isEdit) {%>
                                <%= noOfTable%>
                                <input name="noOfTables" type="hidden" class="formTxtBox_1" id="noOfTables" style="width:200px;" value="<%= noOfTable%>" />
                                <%} else {%>
                                <input name="noOfTables" type="text" class="formTxtBox_1" id="noOfTables" style="width:200px;" value="" />
                                <%}%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Form to be filled Multiple Times : <%if (!isEdit) {%><span>*</span><%}%></td>
                            <td>
                                <%
                                            if (templateFormId > 0) {
                                                if ("yes".equals(isMultipleFill)) {
                                                    out.print("Yes");
                                                } else {
                                                    out.print("No");
                                                }
                                %>
                                <input type="hidden" name="multipleTimeFill" id="multipleTimeFill" value="<%= isMultipleFill%>" />
                                <%
                                                                } else {
                                %>
                                <select name="multipleTimeFill" class="formTxtBox_1" id="multipleTimeFill" style="width:206px;">
                                    <option value="">Select</option>
                                            <option value="yes" <%if ("yes".equals(isMultipleFill)) {
                                                                        out.print("selected");
                                                                    }%>>Yes</option>
                                            <option value="no" <%if ("no".equals(isMultipleFill)) {
                                                                        out.print("selected");
                                                                                            }%>>No</option>
                                </select>
                                <%
                                            }
                                %>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Is BOQ Form : <%if (!isEdit) {%><span>*</span><%}%></td>
                            <td>
                                <%
                                            if (templateFormId > 0) {
                                                if ("yes".equals(isBOQ)) {
                                                    out.print("Yes");
                                                } else {
                                                    out.print("No");
                                                }
                                %>
                                <input type="hidden" name="boqForm" id="boqForm" value="<%= isBOQ%>" />
                                <%
                                                                } else {
                                %>
                                <input type="hidden" name="pkgOrLotId" value="<%=pkgOrLotId%>"/>
                                <input type="hidden" name="isPck" value="<%=isPck%>"/>
                                <select name="boqForm" class="formTxtBox_1" <% if ("services".equalsIgnoreCase(strProcNature) && corriCrtNPending) {%> onchange="showTypeOfForms(this);"<% }%> <% if (isPck) {%> onchange="lotSelection(this);"<% }%> id="boqForm" style="width:206px;">
                                    <option value="">Select</option>
                                            <option value="yes" <%if ("yes".equals(isBOQ)) {
                                                                        out.print("selected");
                                                                    }%>>Yes</option>
                                            <option value="no" <%if ("no".equals(isBOQ)) {
                                                                        out.print("selected");
                                                                                            }%>>No </option>
                                </select>
                                <%
                                            }
                                %>
                            </td>
                        </tr>
                        <tr id="trLotSel"  style="display: none">
                            <td class="ff">Lot selection :</td>
                            <td>
                                <select name="lotIdSelection" class="formTxtBox_1" id="txtLotIdSelection" style="width:206px;">
                                    <%
                                    TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                                                for (TblTenderLotSecurity ttls : tenderDocumentSrBean.getLotList(tenderId)) {%>
                                    <option <% if (ttls.getAppPkgLotId() == lotSelection) {%>selected<%}%> value="<%=ttls.getAppPkgLotId()%>"><%=ttls.getLotNo()%></option>
                                    <% }
                                                tenderDocumentSrBean = null;%>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Is Form Mandatory : <%if (!isEdit) {%><span>*</span><%}%></td>
                            <td>
                                <%
                                            if (templateFormId > 0) {
                                                if ("yes".equals(isMandatory)) {
                                                    out.print("Yes");
                                                } else {
                                                    out.print("No");
                                                }
                                %>
                                <input type="hidden" name="isMandatory" id="isMandatory" value="<%= isMandatory%>" />
                                <%
                                                                } else {
                                %>
                                <select name="isMandatory" class="formTxtBox_1" id="isMandatory" style="width:206px;">
                                    <option value="">Select</option>
                                            <option value="yes" <%if ("yes".equals(isMandatory)) {
                                                                        out.print("selected");
                                                                    }%>>Yes</option>
                                            <option value="no" <%if ("no".equals(isMandatory)) {
                                                                        out.print("selected");
                                                                    }%>>No</option>
                                </select>
                                <%
                                            }
                                %>
                            </td>
                        </tr>
                        <%
                            if("Services".equalsIgnoreCase(strProcNature) && corriCrtNPending){
                                List<Object[]> lstCancelInCorri = tenderFrmSrBean.getFormsCancelledInCorriForSrv(sectionId);
                        %>
                        <tr>
                            <td class="ff">Type of Form : <span>*</span></td>
                            <td>
                                <table class="formStyle_1" id="tblTypeOfForm">
                                    <tr id="trOthers">
                                        <td><input type="radio" name="rdoTemplateIdFromType" id="rdoOthers" value="0" title="Others" onclick="setTemplateIdOfCanceledForm(this);"/> Others</td>
                                    </tr>
                        <%
                                String strIdOfTr = "";
                                int typeI = 1;
                                if(lstCancelInCorri != null && !lstCancelInCorri.isEmpty()){
                                    for (Object[] lstCInC : lstCancelInCorri) {
                                        if(Integer.parseInt(lstCInC[3].toString()) == 15 || Integer.parseInt(lstCInC[3].toString()) == 16){
                                            strIdOfTr = "boqTr_" + typeI;
                                        }else{
                                            strIdOfTr = "techTr_" + typeI;
                                        }
                        %>
                                    <tr id="<%= strIdOfTr %>" style="display: none;">
                                        <td><input type="radio" name="rdoTemplateIdFromType" id="rdo_<%= typeI %>" title="<%= lstCInC[2].toString() %>" value="<%= lstCInC[1].toString() %>" onclick="setTemplateIdOfCanceledForm(this);" /> <%= lstCInC[2].toString() %> (<%= lstCInC[0].toString().replace("br", "") %>)</td>
                                    </tr>
                        <%
                                        typeI++;
                                    }
                                }

                                if(isEdit){
                                    String typeOfForm = "";
                                    typeOfForm = tenderFrmSrBean.getTypeOfFormForService(formId);
                                    if("yes".equalsIgnoreCase(isBOQ)){
                                        strIdOfTr = "boqTr_" + typeI;
                                    }else{
                                        strIdOfTr = "techTr_" + typeI;
                                    }
                        %>
                                    <tr id="<%= strIdOfTr %>" style="display: block;">
                                        <td><input type="radio" name="rdoTemplateIdFromType" id="rdo_<%= typeI %>" title="" value="" 
                                                   onclick="setTemplateIdOfCanceledForm(this);" checked /> <%= typeOfForm %> (<%= frmName.replace("br", "") %>)</td>
                                    </tr>
                        <%
                                }
                        %>
                                </table>
                            </td>
                        </tr>
                        <%
                            }
                        %>
                        <tr>
                            <td class="ff">&nbsp;</td>
                            <td><label class="formBtn_1">
                                    <%if (isEdit) {%>
                                    <input type="submit" name="btnCreateEdit" id="btnCreateEdit" value="Save" />
                                    <%} else {%>
                                    <input type="submit" name="btnCreateEdit" id="btnCreateEdit" value="Create" />
                                    <%}%>
                                </label></td>
                        </tr>
                    </table>
                </div>
            </form>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
        <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
            var pkgOrLotId = '<%=pkgOrLotId%>';
            var formId = '<%=formId%>';
            if(pkgOrLotId == -1 && formId != 0){
                document.getElementById("trLotSel").style.display = 'none';
            }
            function lotSelection(obj){
                var procNature = '<%=strProcNature%>';
                if(obj.value == 'yes' && procNature.toLowerCase() != 'services'){
                    document.getElementById("trLotSel").style.display = 'table-row';
                }else{
                    document.getElementById("trLotSel").style.display = 'none';
                }
            }

            function showTypeOfForms(obj){
                var procNature = '<%=strProcNature%>'; //goods, works, services
                var corrigendumCnP = '<%= corriCrtNPending %>'; //Create and Pending flag
                if(procNature.toLowerCase() == 'services' && corrigendumCnP == 'true'){
                    if(obj.value == 'yes'){
                        $('#tblTypeOfForm tr').each(function() {
                                $this = $(this)
                                var trObjId = $this.attr('id');
                                if(trObjId == "trOthers"){
                                    // do nothing
                                }else{
                                    if(trObjId.indexOf("boqTr_") == 0){
                                        $(this).show();
                                    }else{
                                        $(this).hide();
                                    }
                                }
                            }
                         );
                    }else{
                        $('#tblTypeOfForm tr').each(function() {
                                $this = $(this)
                                var trObjId = $this.attr('id');
                                if(trObjId == "trOthers"){
                                    // do nothing
                                }else{
                                    if(trObjId.indexOf("techTr_") == 0){
                                        $(this).show();
                                    }else{
                                        $(this).hide();
                                    }
                                }
                            }
                         );
                    }
                }
            }
        </script>
            <%
                System.out.println(" isEdit " + isEdit);
                System.out.println(" corriCrtNPending " + corriCrtNPending);
                System.out.println(" strProcNature " + strProcNature);
                if(isEdit && corriCrtNPending && "services".equalsIgnoreCase(strProcNature)){
                    if ("yes".equals(isBOQ)) {
            %>
                    <script>
                        $('#tblTypeOfForm tr').each(function() {
                                $this = $(this)
                                var trObjId = $this.attr('id');
                                if(trObjId == "trOthers"){
                                    // do nothing
                                }else{
                                    if(trObjId.indexOf("boqTr_") == 0){
                                        $(this).show();
                                    }else{
                                        $(this).hide();
                                    }
                                }
                            }
                         );
                    </script>
            <%
                    }else{
            %>
                    <script>
                        $('#tblTypeOfForm tr').each(function() {
                                $this = $(this)
                                var trObjId = $this.attr('id');
                                if(trObjId == "trOthers"){
                                    // do nothing
                                }else{
                                    if(trObjId.indexOf("techTr_") == 0){
                                        $(this).show();
                                    }else{
                                        $(this).hide();
                                    }
                                }
                            }
                         );
                    </script>
            <%
                    }
                }
            %>
<script type="text/javascript">
function checkKey(e)
    {
     var keyValue = (window.event)? e.keyCode : e.which;
        if(keyValue == 13){
        //Validate();
        $('#formName').val($('#formName').val()+'\n');
        
       // alert($('#formName').val()+'  Abc')
        //loadTable();
        }
    }
</script>
</html>
<%
            if (tenderFrmSrBean != null) {
                tenderFrmSrBean = null;
            }
%>
