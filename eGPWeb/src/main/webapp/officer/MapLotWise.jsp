<%-- 
    Document   : MapLotWise
    Created on : Jun 6, 2011, 5:04:51 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lot wise Mapping of Authorized Bidders/Consultants</title>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%
                    String tenderId = request.getParameter("tenderid");
                    String chkpe = request.getParameter("cpe");
                    String tenderStatus = null;
                    boolean ispublish = false;
                    if ("true".equals(request.getParameter("ispub"))) {
                        ispublish = true;
                    }
                    boolean isView = false;
                    if ("view".equals(request.getParameter("action"))) {
                        isView = true;
                    }
                    pageContext.setAttribute("tenderId", tenderId);
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
        %>
        <div class="contentArea_1">
            <div class="pageHead_1">Lot wise Mapping of Authorized Bidders/Consultants<span style="float: right;"><a class="action-button-goback" href="Notice.jsp?tenderid=<%=tenderId%>">Go Back to Dashboard</a></span></div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <table class="tableList_1 t_space" width="100%" cellspacing="0">
                <tr>
                    <th>Lot No</th>
                    <th>Lot Description</th>
                    <th class="t-align-center ff">Map</th>
                </tr>                
                    <%
                        //0. tls.appPkgLotId,1. tls.lotNo,2. tls.lotDesc
                        for(Object[] lots : commonService.getLotDetails(tenderId)){
                    %>
                    <tr>
                        <td class="t-align-center"><%=lots[1]%></td>
                        <td><%=lots[2]%></td>
                        <td class="t-align-center">
                            <%
                                        TenderSrBean tenderSrBean = new TenderSrBean();
                                        long archiveCnt = tenderCommonService.tenderArchiveCnt(tenderId);
                                        boolean isCancelled = true;
                                        List<TblTenderDetails> tblTenderDetailTS = tenderSrBean.getTenderStatus(request.getParameter("tenderid"));
                                        if (!tblTenderDetailTS.isEmpty()) {
                                            tenderStatus = tblTenderDetailTS.get(0).getTenderStatus();
                                            if ("Cancelled".equalsIgnoreCase(tblTenderDetailTS.get(0).getTenderStatus())) {
                                                isCancelled = false;
                                            }
                                        }
                                        if(isView){ %>
                                        <a href="MapTenderers.jsp?tenderid=<%=tenderId%>&isview=y&lotId=<%=lots[0]%>">View</a>
                                        <% }else if ((isCancelled && archiveCnt != 0) || !ispublish) {
                                           if (chkpe.equalsIgnoreCase("y") && !tenderStatus.equalsIgnoreCase("Approved")) {
                                               int size = tenderSrBean.chkTblLimitedTenders(tenderId,Integer.parseInt(lots[0].toString()),true);
                                               if(size != 0){%>
                                               <a href="MapTenderers.jsp?tenderid=<%=tenderId%>&lotId=<%=lots[0]%>">Edit</a>&nbsp;|&nbsp;
                                               <a href="MapTenderers.jsp?tenderid=<%=tenderId%>&isview=y&lotId=<%=lots[0]%>">View</a>
                                               <%}else{%>
                                               <a href="MapTenderers.jsp?tenderid=<%=tenderId%>&lotId=<%=lots[0]%>">Create a Bidder's/Consultant's List</a>
                                               <% } %>
                            <%}else{%>
                            <a href="MapTenderers.jsp?tenderid=<%=tenderId%>&isview=y&lotId=<%=lots[0]%>">View</a>
                            <%}}%>
                        </td>
                    </tr>
                    <%}%>
            </table>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
