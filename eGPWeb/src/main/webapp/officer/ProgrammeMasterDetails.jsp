<%-- 
    Document   : ProgrammeMasterDetails
    Created on : Nov 1, 2010, 8:24:50 PM
    Author     : Naresh.Akurathi
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="progmasterDtBean" class="com.cptu.egp.eps.web.databean.ProgrammeMasterDtBean"/>
<jsp:useBean id="progmasterSrBean" class="com.cptu.egp.eps.web.servicebean.ProgrammeMasterSrBean"/>
<jsp:setProperty property="*" name="progmasterDtBean"/>
<%@page import="com.cptu.egp.eps.model.table.TblProgrammeMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Programme Master Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

    </head>
    <body>
        <%!
            TblProgrammeMaster tblPrgMaster = new TblProgrammeMaster();
            List<TblProgrammeMaster> lstPrgMaster = null;
            int id=0;

        %>


        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/Top.jsp"%>
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Programme Master Details</div>
            <table width="70%" cellspacing="0" class="tableList_1 t_space">
          <tr>
            <th width="1%" style="text-align:center; width:4%;">Sl. No.</th>
            <th style="text-align:center; width:12%;">Programme Code</th>
            <th style="text-align:center; width:16%;">Programme Name</th>
            <th style="text-align:center; width:12%;">Action</th>

          </tr>
           <%

                Iterator it = progmasterSrBean.getAllProgrammeMaster().iterator();

                while(it.hasNext())
                    {
                        tblPrgMaster = (TblProgrammeMaster)it.next();
                        id=tblPrgMaster.getProgId();

           %>
          <tr>
            <td style="text-align:center;"><%= tblPrgMaster.getProgId()%></td>
            <td style="text-align:center;"><%= tblPrgMaster.getProgCode() %></td>
            <td style="text-align:center;"><%= tblPrgMaster.getProgName()%></td>
            <td style="text-align:center;"><a href="ProgrammeMaster.jsp?id=<%= id%>"><img src="../resources/images/Dashboard/viewIcn.png" alt="Go Back" class="linkIcon_1"/><b>Edit</b></a></td>

          </tr>



           <%
                }

           %>
           </table>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@include file="/resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
            <script>
                var obj = document.getElementById('lblProgInfoEdit');
                if(obj != null){
                    if(obj.innerHTML == 'Edit'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabContent");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>