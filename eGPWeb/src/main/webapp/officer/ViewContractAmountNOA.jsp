<%-- 
    Document   : ViewContractAmountNOA
    Created on : 19-Nov-2012, 15:53:24
    Author     : salahuddin
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.PerformanceSecurityICT"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.GetCurrencyAmount"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TblTenderPerformanceSec"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPerfSecurity"%>
<%@page import="java.util.List"%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%
    response.setHeader("Expires", "-1");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");
    if (session.getAttribute("userId") == null) {
        response.sendRedirect("SessionTimedOut.jsp");
    }
 %>

<%
    Double conAmount;
    String cAmount="";
    String tenderID= request.getParameter("tenderid");
    String roundID= request.getParameter("roundId");
    String bidderID = request.getParameter("bidderId");
    String pkgID= request.getParameter("pckLotId");
        
    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
    String procnature = commonService.getProcNature(tenderID + "").toString();

    TblTenderPerformanceSec tenderPerformanceSec = (TblTenderPerformanceSec) AppContext.getSpringBean("TblTenderPerformanceSec");
    List<TblTenderPerfSecurity> listt = tenderPerformanceSec.getData(Integer.parseInt(tenderID), pkgID + "",roundID);
    int noOfPerfsecurity = listt.size();    
%>
<%
        //Added by Salahuddin
                Double percentage1, percentage2, percentage3, percentage4;
                PerformanceSecurityICT perfSecurityICT  = (PerformanceSecurityICT) AppContext.getSpringBean("PerformanceSecurityICT");
                List<GetCurrencyAmount> currencyAmount = new ArrayList<GetCurrencyAmount>();
                currencyAmount= perfSecurityICT.getCurrencyAmount(Integer.parseInt(tenderID), Integer.parseInt(roundID));
                int count=0;
                if(currencyAmount.get(0).getValue1()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue2()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue3()!=0)
                    count=count+1;
                if(currencyAmount.get(0).getValue4()!=0)
                    count=count+1;

                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> perSecAmt1 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getAdvPayment",tenderID, roundID);
                //List<SPCommonSearchDataMore> perSecAmt1 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getAdvPayment",tenderID, bidderID);

            %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <%
                                if (procnature.equalsIgnoreCase("services")) {
                            %>
                                <th width="10%" class="t-align-center">Package No.</th>
                                <th width="30%" class="t-align-left">Package Description</th>
                            <%} else {%>
                                <th width="10%" class="t-align-center">Lot No.</th>
                                <th width="30%" class="t-align-left">Lot Description</th>
                            <%}%>
                            <th width="10%" class="t-align-left">Currency</th>
                            <th width="15%" class="t-align-left">Contract Amount</th>
                            <th width="15%" class="t-align-left">% for Advanced Amount</th>
                            <th width="20%" class="t-align-left">Advance Contract Amount</th>
                        </tr>

                        <%if(count>=1){%>
                        <tr>                            
                            <td rowspan="<%=count%>" class="t-align-center">1</td>
                            <td rowspan="<%=count%>">Lot Description</td>

                            <td width="11%" class="t-align-center"><%=currencyAmount.get(0).getCurrencyName1()%></td>
                            <td width="17%" class="t-align-center"><%conAmount = new Double(currencyAmount.get(0).getValue1()); cAmount  = new DecimalFormat("##.##").format(conAmount); out.println(cAmount); %></td>
                            <%
                            //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>test:"+perSecAmt1.get(0).getFieldName1().toString());
                            //if(perSecAmt1.get(0).getFieldName1().toString() =="NULL" ){percentage1=0.0;}
                                percentage1 = new Double(perSecAmt1.get(0).getFieldName1());
                                //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>test:"+percentage1);
                                Double conValue = new Double(currencyAmount.get(0).getValue1());
                                Double advAmount = (conValue*percentage1)/100;
                                cAmount  = new DecimalFormat("##.##").format(advAmount);
                            %>

                            <td width="10%" class="t-align-center"><%=percentage1%></td>
                            <td width="22%" class="t-align-center"><%=cAmount%></td>
                        </tr>
                        <%}%>

                        <%if(count>=2){%>
                        <tr>
                            <td width="11%" class="t-align-center"><%=currencyAmount.get(0).getCurrencyName2()%></td>
                            <td width="17%" class="t-align-center"><%conAmount = new Double(currencyAmount.get(0).getValue2()); cAmount  = new DecimalFormat("##.##").format(conAmount); out.println(cAmount);%></td>
                            <%
                                Double percentage = new Double(perSecAmt1.get(0).getFieldName1());
                                Double conValue = new Double(currencyAmount.get(0).getValue2());
                                Double advAmount = (conValue*percentage)/100;
                                cAmount  = new DecimalFormat("##.##").format(advAmount);
                            %>

                            <td width="10%" class="t-align-center"><%=percentage%></td>
                            <td width="22%" class="t-align-center"><%=cAmount%></td>
                        </tr>
                        <%}%>

                        <%if(count>=3){%>
                        <tr>
                            <td width="11%" class="t-align-center"><%=currencyAmount.get(0).getCurrencyName3()%></td>
                            <td width="17%" class="t-align-center"><%conAmount = new Double(currencyAmount.get(0).getValue3()); cAmount  = new DecimalFormat("##.##").format(conAmount); out.println(cAmount);%></td>
                            <%
                                Double percentage = new Double(perSecAmt1.get(0).getFieldName1());
                                Double conValue = new Double(currencyAmount.get(0).getValue3());
                                Double advAmount = (conValue*percentage)/100;
                                cAmount  = new DecimalFormat("##.##").format(advAmount);
                            %>

                            <td width="10%" class="t-align-center"><%=percentage%></td>
                            <td width="22%" class="t-align-center"><%=cAmount%></td>
                        </tr>
                        <%}%>

                        <%if(count>=4){%>
                        <tr>
                            <td width="11%" class="t-align-center"><%=currencyAmount.get(0).getCurrencyName4()%></td>
                            <td width="17%" class="t-align-center"><%conAmount = new Double(currencyAmount.get(0).getValue4()); cAmount  = new DecimalFormat("##.##").format(conAmount); out.println(cAmount);%></td>
                            <%
                                Double percentage = new Double(perSecAmt1.get(0).getFieldName1());
                                Double conValue = new Double(currencyAmount.get(0).getValue4());
                                Double advAmount = (conValue*percentage)/100;
                                cAmount  = new DecimalFormat("##.##").format(advAmount);
                            %>

                            <td width="10%" class="t-align-center"><%=percentage%></td>
                            <td width="22%" class="t-align-center"><%=cAmount%></td>
                        </tr>
                        <%}%>


                    </table>
    </body>
</html>
