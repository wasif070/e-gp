<%--
    Document   : TestFormTable
    Created on : Jan 09, 2011, 12:00:21 PM
    Author     : yanki
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblListCellDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListBox"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderComboSrBean"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListCellDetail"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCells"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderColumns"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderFormula"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="frmView"  class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />
<jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />
<%--<jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />--%>
    <table width="100%" cellspacing="0" class="tableList_1" id="FormMatrix">
<%
    int isTotal = 0;
    int FormulaCount = 0;
    int cols = 0;
    int rows = 0;
    int tableId = 0;
    int showOrHide = 0;
    int cellId = 0;
    int tenderId = 0;
    int userId = 0;
    int formId = 0;
    
    int arrFormulaColid[] = null;
    int arrWordFormulaColid[] = null;
    int arrOriValColId[] = null;

    short columnId = 0;
    short filledBy = 0;
    short dataType = 0;

    String colHeader = "";
    String colType = "";
    String tableIndex = "1";
    String inputtype = "";
    String textAreaValue = "";

    if(request.getParameter("tableId")!=null && !"".equalsIgnoreCase(request.getParameter("tableId"))){
        tableId = Integer.parseInt(request.getParameter("tableId"));
    }
    if(request.getParameter("formId")!=null && !"".equalsIgnoreCase(request.getParameter("formId"))){
        formId = Integer.parseInt(request.getParameter("formId"));
    }
    if(request.getParameter("cols")!=null && !"".equalsIgnoreCase(request.getParameter("cols"))){
        cols = Integer.parseInt(request.getParameter("cols"));
    }

    if(request.getParameter("rows")!=null && !"".equalsIgnoreCase(request.getParameter("rows"))){
        rows = Integer.parseInt(request.getParameter("rows"));
    }

    if(request.getParameter("TableIndex")!=null && !"".equalsIgnoreCase(request.getParameter("TableIndex"))){
        tableIndex = request.getParameter("TableIndex");
    }

    if(request.getParameter("tenderId")!=null && !"".equalsIgnoreCase(request.getParameter("tenderId"))){
        tenderId = Integer.parseInt(request.getParameter("tenderId"));
    }

    if(request.getParameter("userId")!=null && !"".equalsIgnoreCase(request.getParameter("userId"))){
        userId = Integer.parseInt(request.getParameter("userId"));
    }

    int bidId = 0;
    if (request.getParameter("bidId") != null) {
        bidId = Integer.parseInt(request.getParameter("bidId"));
    }

    String action = "";
    boolean isMultiTable = false;
    if(request.getParameter("isMultiTable")!=null && !"".equalsIgnoreCase(request.getParameter("isMultiTable"))){
        if("yes".equalsIgnoreCase(request.getParameter("isMultiTable"))){
            isMultiTable = true;
        }
    }

%>
        <tbody id="MainTable<%=tableId%>">
            <input type="hidden" name="tableCount<%=tableId%>" value="1"  id="tableCount<%=tableId%>">
            <input type="hidden" name="originalTableCount<%=tableId%>" value="1"  id="originalTableCount<%=tableId%>">
            <script>
                var lbl_ids=new Array();
                var lbl_countid=0;
            </script>
<%
        short fillBy[] = new short[cols];
        int arrColId[] = new int[cols];
        String arrColType[] = new String[cols];
        int arrShowOrHide[]=new int[cols];

        arrFormulaColid = new int[cols];
        arrWordFormulaColid = new int[cols];
        arrOriValColId = new int[cols];
        for(int i=0;i<arrFormulaColid.length;i++)
        {
                arrFormulaColid[i] = 0;
                arrWordFormulaColid[i] = 0;
                arrOriValColId[i] = 0;
        }

        java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderColumns> tblColumnsDtl = frmView.getColumnsDtls(tableId, true).listIterator();
        java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderCells> tblCellsDtl = frmView.getCellsDtlsTestForm(tableId).listIterator();
        java.util.ListIterator<com.cptu.egp.eps.model.table.TblTenderFormula> tblFormulaDtl = frmView.getTableFormulas(tableId).listIterator();
        int grandTotalCell = tenderBidSrBean.getCellForTotalWordsCaption(formId, tableId);
        
        //ListIterator<CommonFormData> tblColumnsDtl = tenderBidSrBean.getTableColumns(tableId, true).listIterator();
        //ListIterator<CommonFormData> tblCellsDtl = tenderBidSrBean.getTableCells(tableId).listIterator();
        //ListIterator<CommonFormData> tblFormulaDtl = tenderBidSrBean.getTableFormulas(tableId).listIterator();

        int cntRow = 0;
        int TotalBidCount=0;
        int editedRowCount = 0;
        StringBuilder bidTableIds = new StringBuilder();

        List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
        TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
        listCurrencyObj = tenderCurrencyService.getCurrencyTenderwise(tenderId, userId);

        cntRow = 1;
%>
            <script>
		var rowcount = parseInt("<%=rows%>")*parseInt("<%=cntRow%>");
		arrCompType[<%=tableIndex%>] = new Array(rowcount);
		arrCellData[<%=tableIndex%>] = new Array(rowcount);
		arrDataTypesforCell[<%=tableIndex%>] = new Array(rowcount);
                for (i=1;i<=rowcount;i++)
		{
			arrCompType[<%=tableIndex%>][i]=new Array(<%=cols%>);
			arrCellData[<%=tableIndex%>][i]=new Array(<%=cols%>);
			arrDataTypesforCell[<%=tableIndex%>][i]=new Array(<%=cols%>);
		}
                
                arrColFillBy[<%=tableIndex%>] = new Array(<%=cols%>);
		if(rowcount == 1)
		{
			i = <%=cols%>;
		}
            </script>
        <%
        if(true){
            int cnt = 0;
            int counter = 0;
            while(tblFormulaDtl.hasNext()){
                TblTenderFormula formulaData = tblFormulaDtl.next();
                if(formulaData.getFormula().contains("TOTAL")){
                    isTotal = 1;
                    arrFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
%>
                    <script>
                        isColTotalforTable[<%=tableIndex%>] = 1;
                        for(var i=1;i<=<%=cols%>;i++){
                            if(i == <%=formulaData.getColumnId()%>){
                                arrColTotalIds[<%=tableIndex%>][i-1] = '<%=formulaData.getColumnId()%>';
                            }
                        }
                    </script>
<%
                }
                cnt++;
                FormulaCount = cnt;
            }

            /*for(int x=0;x<arrFormulaColid.length;x++){
                out.println(" ==================  " + arrFormulaColid[x]);
            }*/
%>
            <script>
                    var totalWordArr = new Array();
                    var q = 0;
            </script>
<%
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }

            while(tblFormulaDtl.hasNext()){
                TblTenderFormula formulaData = tblFormulaDtl.next();
                for(int x=0;x<arrFormulaColid.length;x++){
                    if(formulaData.getFormula().contains("WORD("+arrFormulaColid[x]+")")){
%>
                    <script>
                        var TotalWordColId = "<%=formulaData.getColumnId()%>";
                        totalWordArr[q] = "<%=formulaData.getColumnId()%>";
                        q++;
                        arrColTotalWordsIds[<%=tableIndex%>][<%=x%>] = '<%=formulaData.getColumnId()%>';
                    </script>
<%
			arrWordFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                    }
                }
            }
%>
            <script>
                arrTableFormula[<%=tableIndex%>] = new Array(<%=FormulaCount%>); // Stores Formula
                arrFormulaFor[<%=tableIndex%>] = new Array(<%=FormulaCount%>); // Stores where to Apply Formula
                arrIds[<%=tableIndex%>] = new Array(<%=FormulaCount%>);
            </script>
<%
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                TblTenderFormula formulaData = tblFormulaDtl.next();
%>
		<script>
                    arrTableFormula[<%=tableIndex%>][<%=counter%>] = '<%=formulaData.getFormula()%>';
                    arrFormulaFor[<%=tableIndex%>][<%=counter%>] = '<%=formulaData.getColumnId()%>';
		</script>
<%
                counter++;
            }
%>
            <script language="JavaScript" >
                arrColIds[<%=tableIndex%>] =new Array();
                arrStaticColIds[<%=tableIndex%>] =new Array();
                arrForLabelDisp[<%=tableIndex%>] = new Array();
            </script>
<%
        }else{
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                TblTenderFormula formulaData = tblFormulaDtl.next();
                if(formulaData.getFormula().contains("TOTAL")){
                    isTotal = 1;
                    arrFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                }
            }

            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                TblTenderFormula formulaData = tblFormulaDtl.next();
                for(int x=0;x<arrFormulaColid.length;x++){
                    if(formulaData.getFormula().contains("WORD("+arrFormulaColid[x]+")")){
			arrWordFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                    }
                }
            }
        }

        if(true) // Form Add Start
        {
            int colId = 0;
            for (short i = -1; i <= rows; i++) {
                if(i == 0){
%>
             <tr id="ColumnRow">
<%
                    for(short j=0;j<cols;j++){
                        if(tblColumnsDtl.hasNext()){
                            TblTenderColumns colData = tblColumnsDtl.next();
                            //BTN - Nu. by Emtaz on 24/April/2016
                            colHeader = colData.getColumnHeader().replaceAll("BTN", "Nu.");
                            colType = colData.getColumnType();
                            filledBy = colData.getFilledBy();
                            showOrHide = Integer.parseInt(colData.getShoworhide());
                            dataType = colData.getDataType();
                            colId = colData.getColumnId();

                            colData = null;
                        }
                        arrColType[j] = colType;
                        fillBy[j] = filledBy;
                        arrColId[j] = colId;
                        arrShowOrHide[j]=showOrHide;
%>
                <th id="addTD_<%=tableId%>_<%= j + 1 %>">
                    <script>
                       arrColFillBy[<%=tableIndex%>] [<%=colId%>]=<%=filledBy%>;
                    </script>
                    <%= colHeader %>
                    <%if(arrShowOrHide[j]==2){%>
                    <script>
                         document.getElementById("addTD_<%= tableId %>_<%= j + 1 %>").style.display = "none";
                    </script>
                    <%}%>
                </th>
<%
                    }
%>
             </tr>
<%
                }
                if(i > 0){
%>
             <tr id="row<%=tableId%>_<%=i%>">
<%
                    int cnt = 0;
                    for(int j=0; j<cols; j++){
                        String cellValue = "";
%>
                <td id="td<%=tableId%>_<%=i%>_<%=columnId%>" align="center">
                <% if(arrShowOrHide[j]==2){ %>
                    <script>
                        document.getElementById("td<%=tableId%>_<%=i%>_<%=columnId%>").style.display = "none";
                    </script>
                <%}%>
<%
                        if(tblCellsDtl.hasNext()){
                            cnt++;

                            TblTenderCells cellData = tblCellsDtl.next();
                            dataType = cellData.getCellDatatype();
                            cellId = cellData.getCellId();
                            //filledBy = cellData.getCellDatatype();
                            //filledBy = cellData.getFillBy();
                            cellValue = cellData.getCellvalue();
                            columnId = cellData.getColumnId();


                            if(true){
                                String cValue = "";
                                cValue = cellValue.replaceAll("\r\n", "");
%>
                            <script>
                                arrCellData[<%=tableIndex%>][<%=i%>][<%=columnId%>] = '<%=cValue.trim()%>';
                                arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = '';
                             </script>
<%
                            }
                            if(grandTotalCell==cellId){
                                out.print("<div style='text-align:center; font-weight:bold;'>Grand Total :</div>");
                            }
                            if(fillBy[j] == 1){ // Filled By PE User

%>
                                   <label  name="lbl@<%=tableId%>_<%=i%>_<%=columnId%>" id="lbl@<%=tableId%>_<%=i%>_<%=columnId%>"
                                                <%if(dataType==3 || dataType==4 || dataType==8 || dataType == 13 ){%>
                                                value="<%=cellValue%>"
                                                <%}else{%>
                                                value=""
                                                <%}%>
                                                >
                                                <%=cellValue%>
                                        </label>
                                <script>
                                    lbl_ids[lbl_countid]="lbl@<%=tableId%>_<%=i%>_<%=columnId%>";
                                    lbl_countid++;
				</script>
<%
                                if(true){
%>
                                <script>
                                    arrStaticColIds[<%=tableIndex%>].push('<%=columnId%>');
				</script>
<%
                               }
                            } else if(fillBy[j] == 2 || fillBy[j] == 3){ // Filled By Tenderer or Auto
                                if(dataType != 2){
                                    if(i == (rows)){
                                        if(FormulaCount>0){
                                            if(isTotal == 1){
                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    if(arrWordFormulaColid[columnId-1] == columnId){
                                                        if(fillBy[j] == 3 && dataType == 3){
%>
                                                        <label
								readonly
								tabindex="-1"
								name="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>"
								id="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>"
                                                                title="label" value="<%=textAreaValue%>">
                                                        </label>
                                                        <!--<textarea style="display:none" class="formTxtBox_1"
                                                                readonly
								tabindex="-1"
								cols="30"
								name="row<%=tableId%>_<%=i%>_<%=columnId%>"
								id="row<%=tableId%>_<%=i%>_<%=columnId%>"
							><%//=textAreaValue%></textarea>-->
<%
                                                        }else {
%>
                                                        <textarea class="formTxtBox_1"
                                                            readonly
                                                            tabindex="-1"
                                                            cols="30"
                                                            name="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            style="width: 98%;"
                                                        ><%=textAreaValue%></textarea>
<%
                                                        }
                                                    }
                                                    out.print("</td>");
                                                    continue;
                                                 }else{
                                                    inputtype = "text";
                                                 }
                                            }
                                        }
                                    }
                                   if(i != (rows))
                                   {
                                       if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                       {
                                            if(listCurrencyObj.size() > 0){
                                        %>
                                        <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');" style="width: 98%;">
                                            <% for(Object[] obj :listCurrencyObj){ %>
                                            <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                            <% } %>
                                        </select>
                                        <%
                                            }
                                       }
%>
                                       <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1 newTestFormWidth"
                                        <%
                                        if(fillBy[j] == 3){%>
                                        readOnly style="width: 98%;"
                                        <%}%>
<%
                                        if(fillBy[j] != 3){
                                            if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                            }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                            } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                            } else if(dataType==11){
%>
                                            onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"
    <%
                                                        }
                                            else if(dataType == 1 || dataType == 2)
                                                {
%>
                                                    style="width: 98%;"
<%
                                                }
                                            else if(dataType==12){
%>
                                         style="width: 80%;height: 20px;" readonly="true" onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }
                                            else if(dataType==13){
%>
                                            onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
    <%
                                                        }
                                        }

                                          if(dataType==9 || dataType==10 || (fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))){
                                         %>
                                            style="display: none;"
                                            <% if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){ %>
                                            value = "<% if(listCurrencyObj.size() > 0){ out.print(listCurrencyObj.get(0)[1]); } %>"
                                            <% } %>
                                         <%
                                            }
%>
                                        />
<%
                                            if(fillBy[j] != 3 && dataType==12){
%>
                                         &nbsp;<a onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','imgrow<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>
<%
                                            }
                                        if(dataType==9 || dataType==10){
                                            List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                            TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                            listCellDetail = frmView.getTenderListCellDetail(tableId,columnId,cellId);
                                            if(listCellDetail.size() > 0){
                                            TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                            List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                            /* Dohatec: ICT-Goods Start */
                                            //Get Currency List to be shown at 'Currency' Combo box
                                            List<Object[]> currencyObjectList = null;
                                            if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                            }

                                            //Procurement Type (i.e. NCT/ICT) retrieval
                                            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                            List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                            boolean isIctTender = false;
                                            if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                isIctTender = true;
                                            }

                                            //Procurement Nature (i.e. Goods/Works/Service) retrival
                                            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                            String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                            /* Dohatec: ICT-Goods End */
                                             %>
                                             <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeTextVal(this,'<%=tableId%>');" style="width: 98%;">
                                                <%  /* Dohatec: ICT-Goods Start */
                                                if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                    for(Object[] obj :currencyObjectList){
                                                        String selected = "";
                                                        if(obj[3].toString().equalsIgnoreCase("BTN")){selected="selected='selected'";} //BTN is the default currency
                                                        out.print("<option value='" + obj[2] + "' " + selected + ">" + obj[3] + "</option>");
                                                    }
                                                }   /* Dohatec: ICT-Goods End */
                                                else {
                                                    for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                        out.print("<option value='" + tblTenderListDetail.getItemValue() + "'>" + tblTenderListDetail.getItemText() + "</option>");
                                                    }
                                                }
                                            %>
                                            </select>
                                         <script language="javascript">
                                            combo_val.push("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>");
                                            combo_val_tableid.push("<%=tableId%>");
                                            combo_val_tableIndex.push('<%=tableIndex%>');
                                         </script>
<%                                             }
                                   }
                                   }
                                   else if(i == (rows))
                                   {
                                        if(FormulaCount > 0)
                                        {
                                            if(isTotal == 1)
                                            {

                                               if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                               {
                                                    if(listCurrencyObj.size() > 0){
                                                %>
                                                <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');" style="width: 98%;">
                                                    <% for(Object[] obj :listCurrencyObj){ %>
                                                    <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                    <% } %>
                                                </select>
                                                <%
                                                    }
                                               }

                                                if(arrFormulaColid[columnId-1] == columnId)
                                                {
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1 newTestFormWidth"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly style="width: 98%;"
                                        <%}%>
<%
                                                    if(fillBy[j] != 3){
                                                        if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                        }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                        } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                        } else if(dataType==11){
%>
                                            onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"
    <%
                                                        }
                                                         else if(dataType == 1 || dataType == 2)
                                                        {
%>
                                                            style="width: 98%;"
<%
                                                        }
                                                         else if(dataType==12){
%>
                                         style="width: 80%;height: 20px;" readonly="true" onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }
                                            else if(dataType==13){
%>
                                            onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
    <%
                                                        }
                                                    }
                                                if(dataType==9 || dataType==10 || (fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))){
                                         %>
                                            style="display: none;"
                                            <% if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){ %>
                                            value = "<% if(listCurrencyObj.size() > 0){ out.print(listCurrencyObj.get(0)[1]); } %>"
                                            <% } %>
                                         <%
                                            }
%>
                                        />
<%
                                            if(fillBy[j] != 3 && dataType==12){
%>
                                         &nbsp;<a onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','imgrow<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>
<%
                                            }
                                                if(dataType==9 || dataType==10){
                                                    List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                    TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                    listCellDetail = frmView.getTenderListCellDetail(tableId,columnId,cellId);
                                                    if(listCellDetail.size() > 0){
                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();

                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                    /* Dohatec: ICT-Goods Start */
                                                    //Get Currency List to be shown at 'Currency' Combo box
                                                    List<Object[]> currencyObjectList = null;
                                                    if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                        currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                    }

                                                    //Procurement Type (i.e. NCT/ICT) retrieval
                                                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                    List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                    boolean isIctTender = false;
                                                    if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                        isIctTender = true;
                                                    }

                                                    //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                    String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                    /* Dohatec: ICT-Goods End */
                                                     %>
                                                     <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeTextVal(this,'<%=tableId%>');" style="width: 98%;">
                                                        <%  /* Dohatec: ICT-Goods Start */
                                                        if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                            for(Object[] obj :currencyObjectList){
                                                                String selected = "";
                                                                if(obj[3].toString().equalsIgnoreCase("BTN")){selected="selected='selected'";} //BTN is the default currency
                                                                out.print("<option value='" + obj[2] + "' " + selected + ">" + obj[3] + "</option>");
                                                            }
                                                        }   /* Dohatec: ICT-Goods End */
                                                        else {
                                                            for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                out.print("<option value='" + tblTenderListDetail.getItemValue() + "'>" + tblTenderListDetail.getItemText() + "</option>");
                                                            }
                                                        }
                                                        %>
                                                    </select>
                                                     <script language="javascript">
                                                        combo_val.push("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>");
                                                        combo_val_tableid.push("<%=tableId%>");
                                                        combo_val_tableIndex.push('<%=tableIndex%>');
                                                    </script>
        <%                                             }
                                                }
                                              }
                                            }else{

                                                if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                                {
                                                    if(listCurrencyObj.size() > 0){
                                                %>
                                                <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');" style="width: 98%;">
                                                    <% for(Object[] obj :listCurrencyObj){ %>
                                                    <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                    <% } %>
                                                </select>
                                                <%
                                                    }
                                                }
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1 newTestFormWidth"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly style="width: 98%;"
                                        <%}%>
<%
                                                if(fillBy[j] != 3){
                                                    if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                    }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                    } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                    } else if(dataType==11){
%>
                                            onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"
    <%
                                                        }
                                                     else if(dataType == 1 || dataType == 2)
                                                {
%>
                                                    style="width: 98%;"
<%
                                                }
                                                    else if(dataType==12){
%>
                                                        style="width: 80%;height: 20px;" readonly="true" onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                    }
                                                    else if(dataType==13){
%>
                                            onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
    <%
                                                        }
                                                }

                                             if(dataType==9 || dataType==10 || (fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))){
                                                 %>
                                                    style="display: none;"
                                                    <% if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){ %>
                                                    value = "<% if(listCurrencyObj.size() > 0){ out.print(listCurrencyObj.get(0)[1]); } %>"
                                                    <% } %>
                                                 <%
                                            }
%>
                                        />
<%
                                            if(fillBy[j] != 3 && dataType==12){
%>
                                         &nbsp;<a onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','imgrow<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>
<%
                                            }
                                                if(dataType==9 || dataType==10){
                                                    List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                    TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                    listCellDetail = frmView.getTenderListCellDetail(tableId,columnId,cellId);
                                                    if(listCellDetail.size() > 0){
                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();
                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                    /* Dohatec: ICT-Goods Start */
                                                    //Get Currency List to be shown at 'Currency' Combo box
                                                    List<Object[]> currencyObjectList = null;
                                                    if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                        currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                    }

                                                    //Procurement Type (i.e. NCT/ICT) retrieval
                                                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                    List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                    boolean isIctTender = false;
                                                    if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                        isIctTender = true;                                                       
                                                    }                                                     

                                                    //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                    String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                    /* Dohatec: ICT-Goods End */
                                                   %>
                                                     <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeTextVal(this,'<%=tableId%>');" style="width: 98%;">
                                                        <%  /* Dohatec: ICT-Goods Start */
                                                            if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                                for(Object[] obj :currencyObjectList){
                                                                    String selected = "";
                                                                    if(obj[3].toString().equalsIgnoreCase("BTN")){selected="selected='selected'";} //BTN is the default currency
                                                                    out.print("<option value='" + obj[2] + "' " + selected + ">" + obj[3] + "</option>");
                                                                }
                                                            }   /* Dohatec: ICT-Goods End */
                                                            else {
                                                                for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                                    out.print("<option value='" + tblTenderListDetail.getItemValue() + "'>" + tblTenderListDetail.getItemText() + "</option>");
                                                                }
                                                            }
                                                        %>
                                                    </select>
                                                     <script language="javascript">
                                                        combo_val.push("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>");
                                                        combo_val_tableid.push("<%=tableId%>");
                                                        combo_val_tableIndex.push('<%=tableIndex%>');
                                                    </script>
        <%                                             }
                                            }
                                            }
                                        }else{
                                            if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                            {
                                                if(listCurrencyObj.size() > 0){
                                            %>
                                            <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');" style="width: 98%;">
                                                <% for(Object[] obj :listCurrencyObj){ %>
                                                <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                <% } %>
                                            </select>
                                            <%
                                                }
                                            }
                                    %>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1 newTestFormWidth"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly style="width: 98%;"
                                        <%}%>
<%
                                            if(fillBy[j] != 3){
                                                if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                } else if(dataType==11){
%>
                                            onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');"
    <%
                                                        }
                                                 else if(dataType == 1 || dataType == 2)
                                                {
%>
                                                    style="width: 98%;"
<%
                                                }
                                                else if(dataType==12){
%>
                                                        style="width: 80%;height: 20px;" readonly="true" onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                }
                                                else if(dataType==13){
%>
                                            onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');"
    <%
                                                        }
                                            }
                                            if(dataType==9 || dataType==10 || (fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))){
                                                     %>
                                                        style="display: none;"
                                                        <% if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j])){ %>
                                                        value = "<% if(listCurrencyObj.size() > 0){ out.print(listCurrencyObj.get(0)[1]); } %>"
                                                        <% } %>
                                                     <%
                                            }
%>
                                        />
<%
                                            if(fillBy[j] != 3 && dataType==12){
%>
                                         &nbsp;<a onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','imgrow<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>
<%
                                            }
                                            if(dataType==9 || dataType==10){
                                                List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                TenderComboSrBean cmbSrBean = new TenderComboSrBean();

                                                listCellDetail = frmView.getTenderListCellDetail(tableId,columnId,cellId);
                                                if(listCellDetail.size() > 0){
                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();
                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());

                                                    /* Dohatec: ICT-Goods Start */
                                                    //Get Currency List to be shown at 'Currency' Combo box
                                                    List<Object[]> currencyObjectList = null;
                                                    if(tenderCurrencyService.isTenderListBoxForCurrency(tblListBoxMaster.getTenderListId())){
                                                        currencyObjectList = tenderCurrencyService.getCurrencyTenderwise(tenderId);
                                                    }

                                                    //Procurement Type (i.e. NCT/ICT) retrieval
                                                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                    List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                                    boolean isIctTender = false;
                                                    if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                        isIctTender = true;
                                                    }

                                                    //Procurement Nature (i.e. Goods/Works/Service) retrival
                                                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                    String procurementNature = commonService.getProcNature(tenderId + "").toString();
                                                    /* Dohatec: ICT-Goods End */
                                            %>
                                                <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeTextVal(this,'<%=tableId%>');" style="width: 98%;">
                                                    <%  /* Dohatec: ICT-Goods Start */
                                                    if(isIctTender && procurementNature.equalsIgnoreCase("Goods") && currencyObjectList != null){
                                                        for(Object[] obj :currencyObjectList){
                                                            String selected = "";
                                                            if(obj[3].toString().equalsIgnoreCase("BTN")){selected="selected='selected'";} //BTN is the default currency
                                                            out.print("<option value='" + obj[2] + "' " + selected + ">" + obj[3] + "</option>");
                                                        }
                                                    }   /* Dohatec: ICT-Goods End */
                                                    else {
                                                        for(TblTenderListDetail tblTenderListDetail:listBoxDetail){
                                                            out.print("<option value='" + tblTenderListDetail.getItemValue() + "'>" + tblTenderListDetail.getItemText() + "</option>");
                                                        }
                                                    }
                                                    %>
                                                </select>
                                                <script language="javascript">
                                                    combo_val.push("idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>");
                                                    combo_val_tableid.push("<%=tableId%>");
                                                    combo_val_tableIndex.push('<%=tableIndex%>');
                                                </script>
    <%                                          }
                                            }
                                        }
                                   }
                           }

                                if(dataType == 2){
                                    if(i == (rows)){
                                        if(FormulaCount > 0){
                                            if(isTotal == 1){
                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    out.print("</td>");
                                                    continue;
                                                }else{
                                                    inputtype = "text";
                                                }
                                            }
                                        }
                                    }

                                //out.print("cellId : "+cellData.getCellId());
                                    if(fillBy[j] == 3){
%>
                                        <label name="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>"
                                        title="label" value="<%=textAreaValue%>">
                                        </label>
                                        <textarea cols="30" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" readOnly class="formTxtBox_1" style="width: 98%;"></textarea>
<%
                                        if(true){
%>
                                        <script>
					if(arrForLabelDisp[<%=tableIndex%>].length != i+1)
						arrForLabelDisp[<%=tableIndex%>].push('<%=tableId%>_<%=columnId%>');
                                        </script>
<%
                                        }
                                    }else{
%>
                                        <textarea cols="30" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" class="formTxtBox_1 newTestFormWidth" style="width: 98%;" ></textarea>
<%

                                    }
                                }
                                if(true){
%>
                                    <script>
                                       <%if(dataType==2){%>
                                        arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "TEXTAREA";
                                       <%}else{%>
                                           arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "TEXT";
                                       <%}%>
                                        arrDataTypesforCell[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "<%=dataType%>";

                                        if(arrColIds[<%=tableIndex%>].length != i+1)
                                                arrColIds[<%=tableIndex%>].push('<%=columnId%>');

                                    </script>
<%
                                }
                            }

                        } // celldtl
%>
                </td>
<%
                    }
%>
            </tr>
<%
                } // if(i>0)
            } //for(row)
        }
%>
        </tbody>
    <div>
    <tr>
        <td colspan="<%=cols%>" style="display:none">
            <label class="formBtn_1" id="lblAddTable">
                <input type="button" name="btn<%=tableId%>" id="bttn<%=request.getParameter("tableIndex")%>" value="Add Table" onClick="AddTable(this.form,<%=tableId%>,this)"/>
            </label>
        </td>
    </tr>
    </div>
</table>

<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>
