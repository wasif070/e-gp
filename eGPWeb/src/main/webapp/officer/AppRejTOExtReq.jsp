<%-- 
    Document   : AppRejTOExtReq
    Created on : Dec 21, 2010, 6:15:27 PM
    Author     : rishita
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page  import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="java.util.List"%>
<%@page buffer="15kb"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Approve Opening Date and Time Extension Request</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    String userId = "";
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        //userId = Integer.parseInt(session.getAttribute("userId").toString());
                        userId = session.getAttribute("userId").toString();
                    }

                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        tenderId = request.getParameter("tenderId");
                    }
                    String extId = "";
                    if (request.getParameter("extId") != null) {
                        extId = request.getParameter("extId");
                    }

        %>
    </head>
    <script type="text/javascript">
        function GetCal(txtname,controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: 24,
                onSelect: function() {
                    var date = Calendar.intToDate(this.selection.get());
                    LEFT_CAL.args.min = date;
                    LEFT_CAL.redraw();
                    this.hide();
                    document.getElementById(txtname).focus();
                }
            });

            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })
        }
    </script>

    <body>
        <div class="mainDiv">
            <div class="fixDiv">

                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <div class="contentArea_1">
                    <div class="pageHead_1">Approve Opening Date and Time Extension Request<span style="float:right;"><a href="TOExtReqListing.jsp" class="action-button-goback">Go Back To Dashboard</a></span></div>
                    <%if ("no".equalsIgnoreCase(request.getParameter("succ"))) {%>
                    <br/><div class="responseMsg errorMsg">Opening date & time extension request not approved</div>
                    <%}%>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <%
                                if ("Submit".equals(request.getParameter("Submit"))) {
                                    String hdextReqBy = "";
                                    if (request.getParameter("hdextReqBy") != null) {
                                        hdextReqBy = request.getParameter("hdextReqBy");
                                    }
                                    String updateString = "", whereCondition = "";
                                    whereCondition = "ExtDtId=" + extId;
                                    String finalOpenDate = request.getParameter("newProDt");
                                    String[] fOpenDate = new String[6];
                                    String[] fOpenDateSpace = new String[6];
                                    fOpenDate = finalOpenDate.split("/");
                                    fOpenDateSpace = fOpenDate[2].split(" ");
                                    String insertFOD = fOpenDateSpace[0] + "-" + fOpenDate[1] + "-" + fOpenDate[0] + " " + fOpenDateSpace[1];

                                    updateString = "extStatus='Approved',approvalDt=getdate(),hopecomments='" + request.getParameter("hopeComments") + "',finalOpenDt='" + insertFOD + "'";
                                    String updateStringTOD = "",updateStringTOD2 = "", whereConditionTOD = "";
                                    updateStringTOD = "entryDt=getdate(),openingDt='" + insertFOD + "'";
                                    updateStringTOD2 = "openingDt='" + insertFOD + "'";
                                    whereConditionTOD = "tenderId=" + tenderId;
                                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    //TenderCommonService tenderCommonServicemail = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    CommonMsgChk commonMsgChk, commonMsgChkTOD, commonMsgChkTD;
                                    
                          
                                     // Coad added by Dipal for Audit Trail Log.
                                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                                    String idType="tenderId";
                                    int auditId=Integer.parseInt(request.getParameter("tenderId"));
                                    String auditAction="Approve Oppening Date Extension Request";
                                    String moduleName=EgpModule.Tender_Opening.getName();
                                    String remarks= request.getParameter("hopeComments");
                                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    
                                    
                                    commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderOpenExt", updateString, whereCondition).get(0);
                                    commonMsgChkTOD = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderOpenDates", updateStringTOD, whereConditionTOD).get(0);
                                    commonMsgChkTD = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderDetails", updateStringTOD2, whereConditionTOD).get(0);
                                    
                                    if (commonMsgChk.getFlag().equals(true) && commonMsgChkTOD.getFlag().equals(true)) 
                                    {
                                        // If operation success then make success event entry.
                                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                                        SimpleDateFormat dateFormatForMsg = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                        CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                        List<SPCommonSearchDataMore> emails = dataMore.geteGPData("getEmailIdOfTenderer", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                        for (int i = 0; i < emails.size() ; i++) 
                                        {
                                            CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                            commonSearchDataMoreService.setLogUserId(userId);
                                            List<SPCommonSearchDataMore> detailsForMsg = commonSearchDataMoreService.geteGPData("getPENameODER", tenderId,null , null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                            //List<SPTenderCommonData> emails = tenderCommonService.returndata("getEmailIdfromUserId", hdextReqBy, null);
                                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                                            String[] mail = {emails.get(i).getFieldName1()};
                                            SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                            MailContentUtility mailContentUtility = new MailContentUtility();
                                            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                            String mailText = mailContentUtility.contOEReqApproval(tenderId, toextTenderRefNo,detailsForMsg.get(0).getFieldName1(),DateUtils.gridDateToStrWithoutSec(dateFormatForMsg.parse(insertFOD)));
                                            sendMessageUtil.setEmailTo(mail);
                                            sendMessageUtil.setEmailSub(HandleSpecialChar.handleSpecialChar("Opening date and time extension request approval"));
                                            sendMessageUtil.setEmailMessage(mailText);
                                            userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("emailIdNoReply"), HandleSpecialChar.handleSpecialChar("Tender Opening New Date and Time Notification"), msgBoxContentUtility.contOEReqApproval(tenderId, toextTenderRefNo,detailsForMsg.get(0).getFieldName1(),DateUtils.gridDateToStrWithoutSec(dateFormatForMsg.parse(insertFOD))));
                                            sendMessageUtil.sendEmail();
                                            sendMessageUtil = null;
                                            mailContentUtility = null;
                                            mail = null;
                                            msgBoxContentUtility = null;
                                        }
                                        response.sendRedirect("TOExtReqListing.jsp?succ=yes");
                                    } else 
                                    {
                                        // If operation fail then make fail event entry.
                                        auditAction="Error in Approve Oppening Date Extension Request";
                                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                                        response.sendRedirect("AppRejTOExtReq.jsp?tenderId=" + tenderId + "&extId=" + extId + "&succ=no");
                                    }
                                }
                    %>
                    <div style="font-style: italic" class="t-align-left t_space">
			Fields marked with (<span class="mandatory">*</span>) are mandatory
                    </div>
                    <form name="frmAppRejTOExtReq" method="post" action="" id="frmAppRejTOExtReq">
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <% TenderCommonService tCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                        List<SPTenderCommonData> todDetails = tCommonService.returndata("getInfoForOpeningDateApproval", tenderId, extId);
                                        for (SPTenderCommonData details : todDetails) {%>

                            <tr>
                                <td width="21%" class="t-align-left ff">Current Opening Date & Time :</td>
                                <td width="79%" class="t-align-left"><%=details.getFieldName1()%></td>
                            </tr>

                            <tr>
                                <td class="t-align-left ff">Newly Proposed Opening Date & Time: <span class="mandatory">*</span></td>
                                <td class="t-align-left"><input name="newProDt" type="text" class="formTxtBox_1" id="txtnewProDt" style="width:100px;" value="<%=details.getFieldName2()%>" readonly="true" onfocus="GetCal('txtnewProDt','txtnewProDt');"/>
                                    <a id="imgnewproDt" onclick ="GetCal('txtnewProDt','imgnewproDt');"><img src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" /></a>
                                    <br/>
                                    <div id="validationNewProDt" class="mandatory"></div>
                                    <div id="vCurNewProDt" class="mandatory"></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Reason for Delay: </td>
                                <td class="t-align-left"><%=details.getFieldName3()%>
                                    <input type="hidden" value="<%=details.getFieldName4()%>" name="hdextReqBy" id="hdextReqBy"/></td>
                            </tr>
                            <% }%>
                            <tr>
                                <td class="t-align-left ff">Comments: <span class="mandatory">*</span></td>
                                <td class="t-align-left">
                                    <textarea cols="100" rows="5" id="hopeComments" name="hopeComments" class="formTxtBox_1"></textarea>
                                    <br/>
                                    <div id="mVHopeComments" class="mandatory"></div>
                                </td>
                            </tr>

                        </table>
                        <div class="t-align-center t_space">
                            <label class="formBtn_1">
                                <input name="Submit" type="submit" value="Submit" onclick="return validate();" />
                            </label>

                        </div>
                    </form>
                </div>
            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        
    </body>
    <script type="text/javascript">
        function required(controlid)
        {
            var temp=controlid.length;
            if(temp <= 0 ){
                return false;
            }else{
                return true;
            }
        }
        //Function for CompareToForToday
        function CompareToForToday(first)
        {
            var mdy = first.split('/')  //Date and month split
            var mdyhr= mdy[2].split(' ');  //Year and time split
            var mdyhrtime=mdyhr[1].split(':');
            var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);

            var d = new Date();
            var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
            return Date.parse(valuedate) > Date.parse(todaydate);
        }
        //        function CompareToForToday(first)
        //        {
        //            var mdy = first.split('/')  //Date and month split
        //            var mdyhr= mdy[2].split(' ');  //Year and time split
        //            var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
        //            var d = new Date();
        //            var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
        //            return Date.parse(valuedate) > Date.parse(todaydate);
        //        }
        //Function for MaxLength
        function Maxlenght(controlid,maxlenght)
        {
            var temp=controlid.length;
            if(temp>maxlenght){
                return false;
            }else
                return true;
        }
        function validate(){
            var vbool='true';
            //validation for newly proposed date
            if(document.getElementById('txtnewProDt')!=null){
                if(!required(document.getElementById('txtnewProDt').value))
                {
                    document.getElementById('validationNewProDt').innerHTML='<div class="reqF_1">Please enter newly proposed opening date & time.</div>';
                    vbool='false';
                }
                else
                {   document.getElementById('validationNewProDt').innerHTML='';
                    if(!CompareToForToday(document.getElementById('txtnewProDt').value))
                    {
                        document.getElementById('vCurNewProDt').innerHTML='<div class="reqF_1">Newly proposed opening date & time must be greater than the current date & time.</div>';
                        vbool='false';
                    }else
                    {
                        document.getElementById('vCurNewProDt').innerHTML='';
                    }
                }
            }
            
            //validation for comments
            if(document.getElementById('hopeComments')!=null){
                if(!required($.trim(document.getElementById('hopeComments').value)))
                {
                    document.getElementById('mVHopeComments').innerHTML='<div class="reqF_1">Please enter Comments.</div>';//bug id : 5235
                    vbool='false';
                }
                else
                {
                    if(!Maxlenght(document.getElementById('hopeComments').value,'2000')){
                        document.getElementById('mVHopeComments').innerHTML='<div class="reqF_1">Maximum 2000 characters are allowed.</div>';
                        vbool='false';
                    }
                    else
                    {
                        document.getElementById('mVHopeComments').innerHTML='';
                    }
                }
            }
            if(vbool=='false'){
                return false;
            }
        }
    </script>


    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabApp");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
