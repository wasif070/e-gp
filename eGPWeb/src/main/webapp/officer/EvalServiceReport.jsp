<%-- 
    Document   : EvalServiceReport
    Created on : Mar 19, 2011, 3:52:18 PM
    Author     : TaherT
--%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                     int evalCount=0;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Consultants Evaluation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%
                    String tendID = request.getParameter("tenderId");
                    String stat = request.getParameter("fr");
                    EvalServiceCriteriaService criteriaService = (EvalServiceCriteriaService) AppContext.getSpringBean("EvalServiceCriteriaService");
                    List<Object[]> biddersName = criteriaService.getFinalSubBidders(tendID);
                    StringBuilder tfootData1 = new StringBuilder();
                    boolean isREOI = false;
                    boolean isService = false;
                    double [] totMarks = new double[biddersName.size()];
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    if(commonService.getEventType(String.valueOf(tendID)).toString().equals("REOI")){
                        isREOI = true;
                    }
                    if(commonService.getProcNature(String.valueOf(tendID)).toString().equals("Services")){
                        isService = true;
                    }
					//Change by dohatec for re-evaluation
                    if(request.getParameter("evalCount")!=null){
                        evalCount = Integer.parseInt(request.getParameter("evalCount"));
                    }
                    String hideHTML = isREOI ? " style='display: none;' " : " ";
        %>
        <div class="contentArea_1">
            <div class="tabPanelArea_1">
            <div class="pageHead_1">
                Consultants Evaluation
                <span style="float: right;"><a class="action-button-goback" href="Evalclarify.jsp?tenderId=<%=tendID%>&st=cl&comType=TEC">Go Back to Dashboard</a></span>
            </div>
            <%
                        pageContext.setAttribute("tenderId", tendID);
         %>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <table class="tableList_1 t_space" cellspacing="0" width="30%">
                <tr>
                    <td width="15%">Tender ID :</td>
                    <td width="15%"><%=tendID%></td>
                </tr>                
                <tr>
                    <td>Name of PEC Member :</td>
                    <td><%=objUserName%></td>
                </tr>
            </table>
            </div>
            <table class="tableList_1 t_space" cellspacing="0" width="100%"  id="marksTab">
                <%if(isREOI){%>
                <tr>
                    <td class="t-align-center" colspan="3">&nbsp;</td>
                    <th class="t-align-center" colspan="<%=biddersName.size()%>">Rating</th>
                </tr>
                <%}%>
                <tr>                    
<!--                    <th class="t-align-center">Form ID</th>                    -->
                    <th class="t-align-center">Form Name</th>
                    <th class="t-align-center">Criteria</th>
                    <th class="t-align-center">Sub Criteria</th>
                    <th <%=hideHTML%> class="t-align-center">Points</th>
                    <%
                                int tempCnt=0;
                                for (Object[] bidder : biddersName) {
                                    totMarks[tempCnt]=0;
                                    String name = bidder[1].toString();
                                    if ("-".equals(name)) {
                                        name = bidder[2].toString() + " " + bidder[3].toString();
                                    }
                                    out.print("<th class='t-align-center'>" + name + "</th>");
                                    tempCnt++;
                                }
                    %>
                    <!--                    -->
                </tr>
                <%
                             double mainTotal=0;
                            //0 esd.maxMarks ,1 esd.actualMarks,2 esd.ratingWeightage,3 esd.ratedScore,
                            //4 tf.tenderFormId,5 tf.formName,6 esc.subCriteria,7 esd.bidId
                            for (Object formId : criteriaService.getAllFormId(tendID)) {
                                List<Object[]> list = criteriaService.evalServiceRepData(formId.toString(), session.getAttribute("userId").toString(), tendID,evalCount); //Change by dohatec for re-evaluation
                               // double secCount=0;
                                for (Object[] data : list) {
                %>
                <tr>
                    <!-- Form ID td>< %
                               if (!("Adequacy For The Assignment".equals(data[2].toString())
                                       || "Time With Consultant".equals(data[2].toString())
                                       || "Experience In Region & Language".equals(data[2].toString()))) {
                                   out.print(data[0]);
                               } else {
                                   out.print("");
                               }
                        %></td-->
                    <td><%
                            if(true || !("Work plan".equals(data[2].toString())||
                                            "Organization and Staffing".equals(data[2].toString()))){                               
                                   out.print(data[1]);
                               } else {
                                   out.print("");
                               }
                        %></td>
                     <td><%
                     if(true || !("Work plan".equals(data[2].toString())||
                            "Organization and Staffing".equals(data[2].toString()))){
                                   out.print(data[5]);
                               } else {
                                   out.print("");
                               }
                        %></td>
                    <td><%out.print(data[2]);%></td>
                    <td <%=hideHTML%> opt="hide"><%out.print(data[3]);mainTotal+=Double.parseDouble(data[3].toString());%></td>
                    <%
                           int totCount = 0;
                           for (Object[] bidder : biddersName) {
                               StringBuilder htmlString = new StringBuilder();
                               htmlString.append("<td>");
                               if(isREOI){
                                   List<Object[]> eachData = criteriaService.evalServiceRepBidderData(tendID, formId.toString(), session.getAttribute("userId").toString(), bidder[0].toString(), data[4].toString(),evalCount); //Change by dohatec for re-evaluation
                                   if (eachData.size() > 1) {
                                       int bidCount = eachData.size();                                      
                                       double rating = 0.0;
                                       for (Object[] thisData : eachData) {

                                           /*if (thisData[1].toString().equals("Very Poor")) {
                                           } else if (thisData[1].toString().equals("Poor")) {
                                               rating+=0.4;
                                           } else if (thisData[1].toString().equals("Good" )) {
                                               rating+=0.7;
                                           } else if (thisData[1].toString().equals("Very Good")){
                                               rating+=0.9;
                                           } else if (thisData[1].toString().equals("Excellent")){
                                               rating+=1;
                                           }*/

                                           if (thisData[1].toString().equals("Poor")) {
                                               rating+=0.4;
                                           }else if (thisData[1].toString().equals("Average")) {
                                               rating+=0.5;
                                           }else if (thisData[1].toString().equals("Satisfactory")) {
                                               rating+=0.7;
                                           }else if (thisData[1].toString().equals("Most Satisfactory")) {
                                               rating+=0.8;
                                           } else if (thisData[1].toString().equals("Good")) {
                                               rating+=0.9;
                                           } else if (thisData[1].toString().equals("Very Good")){
                                               rating+=0.95;
                                           } else if (thisData[1].toString().equals("Excellent")){
                                               rating+=1;
                                           }
                                       }
                                       //System.out.println("--> "+rating);
                                       //System.out.println("--| "+rating/bidCount);
                                       String rateValue = null;
                                       /*if (rating/bidCount == 0) {
                                           rateValue = "Very Poor";
                                       } else if (rating/bidCount <= 0.4) {
                                           rateValue = "Poor";
                                       } else if (rating/bidCount > 0.4 && rating/bidCount <= 0.7) {
                                           rateValue = "Good";
                                       } else if (rating/bidCount > 0.7 && rating/bidCount <= 0.9) {
                                           rateValue = "Very Good";
                                       } else if (rating/bidCount > 0.9 && rating/bidCount <= 1) {
                                           rateValue = "Excellent";
                                       }*/
                                       if (rating/bidCount <= 0.4) {
                                            rateValue = "Poor";
                                           } else if (rating/bidCount > 0.4 && rating/bidCount <= 0.5) {
                                               rateValue = "Average";
                                           }else if (rating/bidCount > 0.5 && rating/bidCount <= 0.7) {
                                               rateValue = "Satisfactory";
                                           }else if (rating/bidCount > 0.7 && rating/bidCount <= 0.8) {
                                               rateValue = "Most Satisfactory";
                                           }else if (rating/bidCount > 0.8 && rating/bidCount <= 0.9) {
                                               rateValue = "Good";
                                           } else if (rating/bidCount > 0.9 && rating/bidCount <= 0.95) {
                                               rateValue = "Very Good";
                                           } else if (rating/bidCount > 0.95 && rating/bidCount <= 1) {
                                               rateValue = "Excellent";
                                           }
                                       htmlString.append(rateValue);
                                      // secCount+=sum;
                                   } else if (eachData.size() == 1) {
                                       htmlString.append(eachData.get(0)[1]);
                                       //secCount+=Double.parseDouble(eachData.get(0)[0].toString());
                                   }                                   
                               }else{
                                   htmlString.append("<table class='t-align-center'><tr><th class='t-align-center'>Assigned</th><th class='t-align-center'>Rating</th><th class='t-align-center'>Scored</th></tr><tr>");
                                   List<Object[]> eachData = criteriaService.evalServiceRepBidderData(tendID, formId.toString(), session.getAttribute("userId").toString(), bidder[0].toString(), data[4].toString(),evalCount); //Change by dohatec for re-evaluation
                                   if (eachData.size() > 1) {
                                       double sum = 0;
                                       int bidCount = eachData.size();
                                       double rating = 0;
                                       double total = Double.parseDouble(data[3].toString());
                                       for (Object[] thisData : eachData) {
                                           sum += Double.parseDouble(thisData[0].toString());
                                       }
                                       sum = sum / bidCount;
                                       if(total!=0){
                                            rating = sum / total;
                                       }
                                       String rateValue = null;
                                       /*if (rating == 0) {
                                           rateValue = "Very Poor";
                                       } else if (rating <= 0.4) {
                                           rateValue = "Poor";
                                       } else if (rating > 0.4 && rating <= 0.7) {
                                           rateValue = "Good";
                                       } else if (rating > 0.7 && rating <= 0.9) {
                                           rateValue = "Very Good";
                                       } else if (rating > 0.9 && rating <= 1) {
                                           rateValue = "Excellent";
                                       }*/
                                       if (rating <= 0.4) {
                                        rateValue = "Poor";
                                       } else if (rating > 0.4 && rating <= 0.5) {
                                           rateValue = "Average";
                                       }else if (rating > 0.5 && rating <= 0.7) {
                                           rateValue = "Satisfactory";
                                       }else if (rating > 0.7 && rating <= 0.8) {
                                           rateValue = "Most Satisfactory";
                                       }else if (rating > 0.8 && rating <= 0.9) {
                                           rateValue = "Good";
                                       } else if (rating > 0.9 && rating <= 0.95) {
                                           rateValue = "Very Good";
                                       } else if (rating > 0.95 && rating <= 1) {
                                           rateValue = "Excellent";
                                       }
                                       totMarks[totCount]+=sum;
                                       htmlString.append("<td>" + new BigDecimal(String.valueOf(sum)).setScale(2,0) + "</td><td>" + rateValue + "</td><td>" + total + "</td>");
                                      // secCount+=sum;
                                   } else if (eachData.size() == 1) {
                                       totMarks[totCount]+= (eachData.get(0)[0].toString().equals("")  ? 0.00 : Double.parseDouble(eachData.get(0)[0].toString()));
                                       htmlString.append("<td>" + new BigDecimal(eachData.get(0)[0].toString()).setScale(2,0) + "</td><td>" + eachData.get(0)[1] + "</td><td>" + eachData.get(0)[2] + "</td>");
                                       //secCount+=Double.parseDouble(eachData.get(0)[0].toString());
                                   }
                                   htmlString.append("</tr></table>");
                               }
                               htmlString.append("</td>");
                               out.print(htmlString.toString());
                               totCount++;
                           }                          
                    %>

                </tr>
                <%}}
                    if(!isREOI){
                        /*for (Object[] bidder : biddersName) {
                             double marks = criteriaService.getTotal4ComMemReport(tendID, session.getAttribute("userId").toString(), bidder[0].toString());*/
                        for (Double bidder : totMarks) {                             
                             tfootData1.append("<th>"+new BigDecimal(String.valueOf(bidder)).setScale(3, 0)+"</th>");
                        }
                        out.print("<tr><th></th><th></th><th align='right'>Total : </th><th>"+mainTotal+"</th>"+tfootData1.toString()+"</tr>");
                    }
                %>
            </table>
            <%if(isService && !isREOI){%>
            <script type="text/javascript">
                $('#marksTab td[opt="hide"]').each(function() {
                    var ctd = this.innerHTML;
                    if(ctd=='0'){
                        $(this).parent().hide();
                    }
                });
            </script>
            <%}%>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>
