<%--
    Document   : ServiceReVariationOrder
    Created on : Dec 22, 2011, 6:21:23 PM
    Author     : shreyansh.shah
--%>



<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvRevari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Work Schedule</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript">
            /* check if pageNO is disable or not */
            function chkdisble(pageNo){
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>

        <script type="text/javascript">
            /*  Handle First click event */
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    $('#first').val(0);
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Last Button click event */
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var size = (parseInt($('#size').val())*totalPages)-(parseInt($('#size').val()));
                    $('#first').val(size);
                    if(totalPages>0){
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });



        </script>
        <script type="text/javascript">
            /*  Handle Next Button click event */
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val());
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first+max);

                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo <= totalPages) {

                        loadTable();
                        $('#pageNo').val(Number(pageNo)+1);
                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            /*  Handle Previous click event */
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first-max);
                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    $('#pageNo').val(Number(pageNo)-1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)-1);
                });
            });
        </script>
        <script type="text/javascript">
            var count=0;
            var addcount = 0;
            function Search(){
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            }
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var size = (parseInt($('#size').val())*pageNo)-$('#size').val();
                    $('#first').val(size);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }

                });
            });

            function delRow(){
                var counter = 0;
                var listcount = document.getElementById("listcount").value;
                var trackid="";
                for(var i=0;i<listcount;i++){
                    if(document.getElementById("chk"+i)!=null){
                        if(document.getElementById("chk"+i).checked){
                            if(document.getElementById("srvReVarId_"+i).value != "")
                            {
                                trackid=trackid+document.getElementById("srvReVarId_"+i).value+",";
                            }
                            // document.getElementById("listcount").value = listcount-1;

                        }
                    }
                }
                $(":checkbox[checked='true']").each(function(){
                    if(document.getElementById("listcount")!= null){
                        var curRow = $(this).parent('td').parent('tr');
                        curRow.remove();
                        listcount--;
                        counter++;
                    }
                });
                
                if(counter==0){
                    jAlert("Please select at least one item to remove","Reimbursable Expenses Variation Order", function(RetVal) {
                    });
                }
                else
                {
                    $.post("<%=request.getContextPath()%>/CMSSerCaseServlet", {action:'deletesrvrevariation', val: trackid,tenderId:<%=request.getParameter("tenderId")%> },  function(j){
                        jAlert("Selected item(s) deleted successfully","Variation Order", function(RetVal) {
                            document.getElementById("listcount").value = listcount;
                                var totAmt = countGrandTotal();//eval(document.getElementById("total").innerHTML)+eval(f_value);
                                totAmt  = Math.round(totAmt*Math.pow(10,3))/Math.pow(10,3)+"";
                                if(totAmt.indexOf(".") < 0){
                                    totAmt = totAmt+".000"
                                }
                                document.getElementById("total").innerHTML = totAmt;
                        });


                    });

                }
               
            }
            function changetable(){
                var pageNo=parseInt($('#pageNo').val(),10);
                var first = parseInt($('#first').val());
                var max = parseInt($('#size').val());
                var totalPages=parseInt($('#totalPages').val(),10);
                loadTable();

            }
            function countGrandTotal(){
                var count = document.getElementById("totalcnt").value;
                var total = 0;
                for(var i = 0; i<count ; i++){
                    total = total + eval(document.getElementById("totalamt"+i).value);
                }
                                if(document.getElementById("listcount").value > 0){
                                     for(var i = 0; i<document.getElementById("listcount").value ; i++){
                                         if(document.getElementById("muliplytxt"+i)!=null){
                                             total = total + eval(document.getElementById("muliplytxt"+i).value);
                                         }

                                    }
                                }
                                var f_value = Math.round(total*Math.pow(10,3))/Math.pow(10,3)+"";
                                if(f_value.indexOf(".") < 0){
                                    f_value = f_value+".000"
                                }
                document.getElementById("total").innerHTML = f_value;
                return f_value;
            }
            function countnewitemTotal(){
                var total = countGrandTotal();
                if(document.getElementById("listcount").value > 0){
                                     for(var i = 0; i<document.getElementById("listcount").value ; i++){
                                         if(document.getElementById("muliplytxt"+i)!=null){
                                             total = eval(total) + eval(document.getElementById("muliplytxt"+i).value);
                                         }

                                    }
                                }
                                return total;
            }
            function onFire(){
                var countt = document.getElementById("listcount").value;
                var flag = true;
                 var allow = false;
                for(var i=0;i<countt;i++){
                     allow = true;
                    if(document.getElementById("srNo_"+i) == null)
                    {
                        continue;
                    }
                    if(document.getElementById("srNo_"+i).value=="" || document.getElementById("categoryDesc_"+i).value=="" || document.getElementById("Description_"+i).value=="" || document.getElementById("Unit_"+i).value=="" || document.getElementById("qtytxt_"+i).value=="" || document.getElementById("unitCosttxt_"+i).value==""){
                        jAlert("It is mandatory to specify all Groups","Reimbursable Expenses Variation Order", function(RetVal) {
                        });
                        flag=false;
                    }

                }
                if(flag){
                    if(!allow){
                        jAlert("It is necessary to enter at least one item","Variation Order", function(RetVal) {
                            });
                             return false;
                    }else{
                        document.getElementById("addcount").value=document.getElementById("listcount").value;
                    document.getElementById("action").value="addsrvrevari";
                    document.forms["frm"].submit();
                    }

                }else{
                    return false;
                }

            }

        </script>

    </head>
    <body onload="countGrandTotal()">
        <%
                    String referpage = request.getHeader("referer");
                    ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String type = "";
                    int tenderId = 0;
                    int lotId = 0;
                    int formMapId = 0;
                    int varOrdId = 0;
                    int srvBoqId = 0;
                    String styleClass = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                    }
                    if (request.getParameter("formMapId") != null) {
                        formMapId = Integer.parseInt(request.getParameter("formMapId"));
                    }
                    if (request.getParameter("varOrdId") != null) {
                        varOrdId = Integer.parseInt(request.getParameter("varOrdId"));
                    }
                    if (request.getParameter("srvBoqId") != null) {
                        srvBoqId = Integer.parseInt(request.getParameter("srvBoqId"));
                    }
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                    boolean flag = false;
                    CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                    String serviceType = commService.getServiceTypeForTender(tenderId);
                    if ("Time based".equalsIgnoreCase(serviceType.toString())) {
                        flag = true;
                    }
                    List<SPCommonSearchDataMore> mores = cmss.getListBoxItemNameForRe(tenderId, 2);
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">
                        <%       out.print("Reimbursable Expenses Variation Order");
                        %>
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" title="Go Back">Go Back</a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%if (request.getParameter("lotId") != null) {%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <%}%>
                    <form name="frm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post" >
                        <input type="hidden" name="addcount" id="addcount" value="" />
                        <input type="hidden" name="action" id="action" value="" />
                        <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>" />
                        <input type="hidden" name="SrvFormMapId" id="SrvFormMapId" value="<%=request.getParameter("formMapId")%>" />
                        <input type="hidden" name="lotId" id="lotId" value="<%=lotId%>" />
                        <input type="hidden" name="varOrdId" id="varOrdId" value="<%=varOrdId%>" />
                        <input type="hidden" name="srvBoqId" id="srvBoqId" value="<%=srvBoqId%>" />
                        <%
                            if(request.getParameter("isEdit")!=null){
                        %>
                            <input type="hidden" name="isEdit" id="isEdit" value="true" />
                        <%
                            }
                        %>
                        <div id="resultDiv" style="display: block;">
                            <div  id="print_area">

                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                    <tr><td colspan="10" style="text-align: right;">
                                            <a class="action-button-add" id="addRow" onclick="addrow()">Add Item</a>
                                            <a class="action-button-delete" id="delRow" onclick="delRow()">Remove Item</a>
                                        </td></tr>
                                    <tr>
                                        <th width="5%">Check</th>
                                        <th>Sl. No.</th>
                                        <th>Description Category</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                        <th>Unit Cost</th>
                                        <th>Total Amount (In Nu.)</th>
                                    </tr>
                                    <%
                                                List<TblCmsSrvReExpense> list = cmss.getSrvReExpensesData(formMapId);
                                                if (!list.isEmpty()) {
                                                    for (int i = 0; i < list.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"></td>
                                        <td class="t-align-left"><%=list.get(i).getSrNo()%></td>
                                        <td class="t-align-left"><%=list.get(i).getCatagoryDesc()%></td>
                                        <td class="t-align-left"><%=list.get(i).getDescription()%></td>
                                        <td class="t-align-left"><%=list.get(i).getUnit()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getQty()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getUnitCost()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getTotalAmt()%></td>
                                    <input type="hidden" name="muliplytxt<%=i%>" id="muliplytxt<%=i%>" />
                                    <input type="hidden" name="totalamt<%=i%>" id="totalamt<%=i%>" value="<%=list.get(i).getTotalAmt()%>" />
                                    <input type="hidden" value="<%=list.get(i).getSrvReid()%>" name="srvReId" />
                                    <input type="hidden" value="<%=list.size() %>" name="totalcnt" id="totalcnt" />
                                    </tr>
                                    <%}%>

                                                <%}%>

                                    <%    List<TblCmsSrvRevari> listVarOrder = cmss.getSrvReVariationOrder(varOrdId);
                                                if (listVarOrder != null && !listVarOrder.isEmpty()) {
                                                    int i = 0;
                                                    for (i = 0; i < listVarOrder.size(); i++) {
                                                        if (i % 2 == 0) {
                                                            styleClass = "bgColor-white";
                                                        } else {
                                                            styleClass = "bgColor-Green";
                                                        }

                                    %>
                                    <tr>
                                        <td> <input class="formTxtBox_1" type="checkbox" name="chk<%=i%>" id="chk<%=i%>" /></td>
                                        <td class="t-align-left" style="width:40px"><input type="text" name="srNo_<%=i%>"  id="srNo_<%=i%>" value="<%=listVarOrder.get(i).getSrNo()%>" class="formTxtBox_1" /></td>
                                        <td class="t-align-left">
                                            <select name="categoryDesc_<%=i%>" class="formTxtBox_1"  id="categoryDesc_<%=i%>" >
                                                <%
                                                                                                        if (!mores.isEmpty()) {
                                                                                                            for (SPCommonSearchDataMore spcsdm : mores) {
                                                %>

                                                <option value="<%=spcsdm.getFieldName2()%>" <%if (spcsdm.getFieldName2().equalsIgnoreCase(listVarOrder.get(i).getCatagoryDesc())) {%> selected <%}%> ><%=spcsdm.getFieldName2()%></option>
                                                <%}
                                                                                                                                                        } else {%>
                                                <option value="0">Select</option><%}%>
                                            </select>
                                        </td>
                                        <td class="t-align-left"><input type="text" name="Description_<%=i%>"  id="Description_<%=i%>" value="<%=listVarOrder.get(i).getDescription()%>" class="formTxtBox_1" /></td>
                                        <td class="t-align-left"><input type="text" name="Unit_<%=i%>"  id="Unit_<%=i%>" value="<%=listVarOrder.get(i).getUnit()%>" class="formTxtBox_1" /></td>
                                        <td class="t-align-left"><input type="text" name="qtytxt_<%=i%>"  id="qtytxt_<%=i%>" value="<%=listVarOrder.get(i).getQty()%>" class="formTxtBox_1" onchange="checkQty(<%=i%>)" /><span id="qtyidspan<%=i%>" class="reqF_1"></span></td>
                                        <td class="t-align-left"><input type="text" name="unitCosttxt_<%=i%>"  id="unitCosttxt_<%=i%>" value="<%=listVarOrder.get(i).getUnitCost()%>" class="formTxtBox_1" onchange="checkUnitCost(<%=i%>)" /><span id="unitCostidspan<%=i%>" class="reqF_1"></span></td>
                                        <td class="t-align-left" style="text-align: right;"><span id="muliply<%=i%>"></span>
                                            <input type="hidden" name="muliplytxt<%=i%>" id="muliplytxt<%=i%>" />
                                            <input type="hidden" value="<%=list.get(i).getSrvReid()%>" name="srvReId"/>
                                        </td>

                                    <input type="hidden" value="<%=listVarOrder.get(i).getSrvRevariId()%>" name="srvReVarId_<%=i%>" id="srvReVarId_<%=i%>" />

                                    </tr>
                                    <%}
                                                    out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");
                                                    out.print("<input type=hidden name=lstcnt id=lstcnt value=" + i + " />");
                                                } else {
                                                    out.print("<input type=hidden name=listcount id=listcount value=0 />");
                                                    out.print("<input type=hidden name=lstcnt id=lstcnt value=0 />");
                                                }
                                    %>
                                    <input type="hidden" name="forms" <%if (flag) {%>value="timebase"<%} else {%>value="lumpsum"<%}%> />
                                    <tr>
                                        <td colspan="6"></td>
                                        <td class="ff">Grand Total :</td>
                                        <td class="ff" style="text-align:  right"><label id="total"></lable></td>
                                    </tr>
                                </table>
                                <table width="100%" cellspacing="0" class="t_space">
                                    <tr><td colspan="10" style="text-align: right;">
                                            <a class="action-button-add" id="addRow" onclick="addrow()">Add item</a>
                                            <a class="action-button-delete" id="delRow" onclick="delRow()">Remove item</a>
                                        </td></tr>
                                </table>
                            </div>
                        </div>


                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" id="first" value="0"/>
                        <br />
                        <center>
                            <label class="formBtn_1">
                                <input type="button" name="swpbuttonadd" id="swpbuttonadd" value="Submit" onclick="onFire();" />
                            </label>
                        </center>
                    </form>
                </div>

                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
        <script language="javascript">
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function checkZero(id){
                var qty = document.getElementById('qtytxt_'+id).value;
                if(qty.indexOf('0') == 0 && qty.indexOf('.') != 1){
                    jAlert("Please remove all leading zero(s)","Reimbursable Expenses Variation Order", function(RetVal) {
                    });
                    document.getElementById('qtytxt_'+id).value="";
                }else{
                    return true;
                }
            }
            function checkQty(id){
                $('.err').remove();
                var rate = 0;
                if(checkZero(id)){
                    if(!regForNumber(document.getElementById('qtytxt_'+id).value)){
                        $("#qtytxt_"+id).parent().append("<div class='err' style='color:red;'><%=bdl.getString("CMS.PR.qtynumeric")%></div>");
                        vbool = false;
                        document.getElementById('qtytxt_'+id).value="";
                    }else if(document.getElementById('qtytxt_'+id).value.indexOf("-")!=-1){
                        $("#qtytxt_"+id).parent().append("<div class='err' style='color:red;'>Quantity must not be negative value</div>");
                        document.getElementById('qtytxt_'+id).value = "";
                    } else if(document.getElementById('qtytxt_'+id).value.split(".")[1] != undefined){
                        if(!regForNumber(document.getElementById('qtytxt_'+id).value) || document.getElementById('qtytxt_'+id).value.split(".")[1].length > 3 || document.getElementById('qtytxt_'+id).value.split(".")[0].length > 12){
                            $("#qtytxt_"+id).parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                            document.getElementById('qtytxt_'+id).value = "";
                            vbool = false;
                        }else{
                            var qty =  eval(document.getElementById('qtytxt_'+id).value);
                            if(document.getElementById('unitCosttxt_'+id).value != "")
                            {
                                rate = eval(document.getElementById('unitCosttxt_'+id).value);
                            }
                                var amt = qty*rate;
                                var f_value = Math.round(amt*Math.pow(10,3))/Math.pow(10,3)+"";
                                if(f_value.indexOf(".") < 0){
                                    f_value = f_value+".000"
                                }
                                $('#muliply'+id).html(f_value);
                                //  $('#muliplytxt'+id).val(f_value);
                                document.getElementById("muliplytxt"+id).value =f_value;
                               // var retVal = countGrandTotal();
                                var totAmt = countGrandTotal();//eval(document.getElementById("total").innerHTML)+eval(f_value);
                                totAmt  = Math.round(totAmt*Math.pow(10,3))/Math.pow(10,3)+"";
                                if(totAmt.indexOf(".") < 0){
                                    totAmt = totAmt+".000"
                                }
                                document.getElementById("total").innerHTML = totAmt;
                                
                                //    countCntVal();

                            }
                        }else{
                           
                            var qty =  eval(document.getElementById('qtytxt_'+id).value);
                            if(document.getElementById('unitCosttxt_'+id).value != "")
                            {
                                rate = eval(document.getElementById('unitCosttxt_'+id).value);
                                 var amt = qty*rate;
                            var f_value = Math.round(amt*Math.pow(10,3))/Math.pow(10,3)+"";
                            if(f_value.indexOf(".") < 0){
                                f_value = f_value+".000"
                            }
                            $('#muliply'+id).html(f_value);
                            $('#muliplytxt'+id).val(f_value);
                            document.getElementById("muliplytxt"+id).value = f_value;
                           

                                var totAmt = countGrandTotal();//eval(document.getElementById("total").innerHTML)+eval(f_value);
                                totAmt  = Math.round(totAmt*Math.pow(10,3))/Math.pow(10,3)+"";
                                if(totAmt.indexOf(".") < 0){
                                    totAmt = totAmt+".000"
                                }
                                document.getElementById("total").innerHTML = totAmt;
                                
                            }
                           
                            //     countCntVal();
                        }
                    }
                }
                function checkZero1(id){
                    var qty = document.getElementById('unitCosttxt_'+id).value;
                    if(qty.indexOf('0') == 0 && qty.indexOf('.') != 1){
                        jAlert("Please remove all leading zero(s)","Reimbursable Expenses Variation Order", function(RetVal) {
                        });
                        document.getElementById('qtytxt_'+id).value="";
                    }else{
                        return true;
                    }
                }
                function checkUnitCost(id){
                    $('.err').remove();
                    if(checkZero1(id)){
                        if(!regForNumber(document.getElementById('unitCosttxt_'+id).value)){
                            $("#unitCosttxt_"+id).parent().append("<div class='err' style='color:red;'><%=bdl.getString("CMS.PR.qtynumeric")%></div>");
                            vbool = false;
                            document.getElementById('unitCosttxt_'+id).value="";
                        }else if(document.getElementById('unitCosttxt_'+id).value.indexOf("-")!=-1){
                            $("#unitCosttxt_"+id).parent().append("<div class='err' style='color:red;'>Quantity must not be negative value</div>");
                            document.getElementById('unitCosttxt_'+id).value = "";
                        } else if(document.getElementById('unitCosttxt_'+id).value.split(".")[1] != undefined){
                            if(!regForNumber(document.getElementById('unitCosttxt_'+id).value) || document.getElementById('unitCosttxt_'+id).value.split(".")[1].length > 3 || document.getElementById('unitCosttxt_'+id).value.split(".")[0].length > 12){
                                $("#unitCosttxt_"+id).parent().append("<div class='err' style='color:red;'>Maximum 12 digits are allowed and 3 digits after decimal</div>");
                                document.getElementById('unitCosttxt_'+id).value = "";
                                vbool = false;
                            }else{
                                var qty =  eval(document.getElementById('qtytxt_'+id).value);
                                var rate = eval(document.getElementById('unitCosttxt_'+id).value);
                                var amt = qty*rate;
                                var f_value = Math.round(amt*Math.pow(10,3))/Math.pow(10,3)+"";
                                if(f_value.indexOf(".") < 0){
                                    f_value = f_value+".000"
                                }
                                $('#muliply'+id).html(f_value);
                                $('#muliplytxt'+id).val(f_value);
                                var totAmt = countGrandTotal();//eval(document.getElementById("total").innerHTML)+eval(f_value);
                                totAmt  = Math.round(totAmt*Math.pow(10,3))/Math.pow(10,3)+"";
                                if(totAmt.indexOf(".") < 0){
                                    totAmt = totAmt+".000"
                                }
                                document.getElementById("total").innerHTML = totAmt;
                               
                                //   countCntVal();
                            }
                        }else{

                            var qty =  eval(document.getElementById('qtytxt_'+id).value);
                            var rate = eval(document.getElementById('unitCosttxt_'+id).value);
                            var amt = qty*rate;
                            var f_value = Math.round(amt*Math.pow(10,3))/Math.pow(10,3)+"";
                            if(f_value.indexOf(".") < 0){
                                f_value = f_value+".000"
                            }
                            $('#muliply'+id).html(f_value);
                            //$('#muliplytxt'+id).val(f_value);
                            document.getElementById("muliplytxt"+id).value = f_value;
                           //var retVal = countGrandTotal();
                                var totAmt = countGrandTotal()//eval(document.getElementById("total").innerHTML)+eval(f_value);
                                totAmt  = Math.round(totAmt*Math.pow(10,3))/Math.pow(10,3)+"";
                                if(totAmt.indexOf(".") < 0){
                                    totAmt = totAmt+".000"
                                }
                                document.getElementById("total").innerHTML = totAmt;
                            //   countCntVal();
                        }
                    }
                }
                function countCntVal(){
                    var count = <%=list.size()%>
                    var actualamount = 0;
                    for(var i=0;i<count;i++){
                        actualamount = actualamount+eval(document.getElementById('muliply'+i).innerHTML);
                    }
                    var f_value = Math.round(actualamount*Math.pow(10,3))/Math.pow(10,3)+"";
                    if(f_value.indexOf(".") < 0){
                        f_value = f_value+".000"
                    }
                    $('#total').html(f_value);
                }
                function allcountCntVal(){
                    for(var id = 0; id<<%=list.size()%>; id++)
                    {
                        var qty =  eval(document.getElementById('qtytxt_'+id).value);
                        var rate = eval(document.getElementById('unitCosttxt_'+id).value);
                        var amt = qty*rate;
                        var f_value = Math.round(amt*Math.pow(10,3))/Math.pow(10,3)+"";
                        if(f_value.indexOf(".") < 0){
                            f_value = f_value+".000"
                        }
                        $('#muliply'+id).html(f_value);
                        //$('#muliplytxt'+id).val(f_value);
                        document.getElementById("muliplytxt"+id).value = f_value;
                    }
                    countCntVal();
                }
                function validate()
                {
                    var vbool = true;
                    for(var id = 0; id<<%=list.size()%>; id++)
                    {
                        if(document.getElementById('qtytxt_'+id).value!="")
                        {
                            document.getElementById('qtyidspan'+id).innerHTML="";
                        }else{
                            document.getElementById('qtyidspan'+id).innerHTML="Please enter Quantity";
                            vbool = false;
                        }
                        if(document.getElementById('unitCosttxt_'+id).value!="")
                        {
                            document.getElementById('unitCostidspan'+id).innerHTML="";
                        }else{
                            document.getElementById('unitCostidspan'+id).innerHTML="Please enter UnitCost";
                            vbool = false;
                        }

                    }
                    return vbool;
                }
                allcountCntVal();
                function positive(value)
                {
                    return /^\d*$/.test(value);

                }
                function addrow()
                {
                    addcount = document.getElementById("listcount").value;
                    count = document.getElementById("listcount").value;
                    var servtxt = '<tr><td class="t-align-left">\
              <input class="formTxtBox_1" type="checkbox" name="chk'+count+' " id="chk'+count+'" /></td>'+
                        '<td class="t-align-left"><input class="formTxtBox_1" type="text" style="width:40px" name="srNo_'+count+'" style=width:40px id="srNo_'+count+'" /></td>'+
                        '<td class="t-align-left"><select class="formTxtBox_1" style=width:100px name="categoryDesc_'+count+'" id="categoryDesc_'+count+'" >'+
                        '<%if (!mores.isEmpty()) {
                                    for (SPCommonSearchDataMore spcsdm : mores) {%><option value="<%=spcsdm.getFieldName2()%>"><%=spcsdm.getFieldName2()%></option><%}
                                    } else {%><option value="0">Select</option><%}%></select></td>'+
                                                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="Description_'+count+'" id="Description_'+count+'" /></td>'+
                                                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="Unit_'+count+'" id="Unit_'+count+'" /></td>'+
                                                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="qtytxt_'+count+'" id="qtytxt_'+count+'"  onchange="checkQty('+count+')"/><span id="qtyidspan'+count+'" class="reqF_1"></span></td>'+
                                                    '<td class="t-align-left"><input class="formTxtBox_1" type="text" name="unitCosttxt_'+count+'" id="unitCosttxt_'+count+'" onchange="checkUnitCost('+count+')" /><span id="unitCostidspan'+count+'" class="reqF_1"></span></td>'+
                                                    '<td class="t-align-left"><span id="muliply'+count+'"></span></td>'+
                                                    '<input type="hidden" name="muliplytxt'+count+'" id="muliplytxt'+count+'" />'+
                                                    '<input type=hidden name="srvReVarId_'+count+ '" id="srvReVarId_'+count+'" value="" /></td>';
                                                // onblur=checkqtyin('+count+')
                                                $("#resultTable tr:last").before(servtxt);
                                                count++;
                                                document.getElementById("listcount").value = count;
                                            }


        </script>
    </body>
</html>





